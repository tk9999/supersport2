﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PostProductionReport.aspx.vb"
  Inherits="NewOBWeb.PostProductionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
     td.LButtons, th.LButtons
    {
      width: 24px !important;
    }
    td.CellWait
    {
      background-image: url('../Singular/Images/LoadingSmall.gif');
      background-repeat: no-repeat;
      background-position: 3px 5px;
      color: #bbb;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      With h.Toolbar
        .Helpers.HTML.Heading2("Post Production Report : " + ViewModel.ProductionDescription)
           
        h.MessageHolder().AddClass("HoverMsg")
      With .Helpers.Div()
        With .Helpers.Button(Singular.Web.DefinedButtonType.Save)
          .PostBackType = Singular.Web.PostBackType.Ajax
        End With
          With .Helpers.Button("GenerateData", "Generate Data")
            .AddBinding(Singular.Web.KnockoutBindingString.click, "GenerateData()")
            .PostBackType = Singular.Web.PostBackType.None
                         
          End With
          With .Helpers.Button(Singular.Web.DefinedButtonType.Export, "PrintReport", "Print Report")
            .PostBackType = Singular.Web.PostBackType.Full
            '  .Image.Src = "~/Singular/Images/IconPDF.png"
          End With
       
          
        With h.MessageHolder("Info")
          .Style.Display = Singular.Web.Display.inlineblock
        End With
      End With
  
      End With
      
      With h.TableFor(Of OBLib.PostProductionReports.PostProductionReport)(Function(c) c.PostProductionReportList, False, False)
        
        .FirstRow.AddColumn(Function(c) c.PostProductionReportSectionID, 200)
        .FirstRow.AddColumn(Function(c) c.Column1, 200)
        .FirstRow.AddColumn(Function(c) c.Column2, 200)
        .FirstRow.AddColumn(Function(c) c.Comments, 300)
       
         
      End With
      
    End Using
    
  %>

  <script type="text/javascript">

    function GenerateData() {

      var questionMessage = "Do you want to generate the post production report data for this production?";
 
      if (ViewModel.PostProductionReportList().length > 0) {
        questionMessage = questionMessage + " All your previous comments will be lost."
      }

      Singular.ShowMessageQuestion("Generate Post Production Report", questionMessage, function () {
        
          Singular.SendCommand("GenerateData");
       
      });

    }
     

    function GetFilteredTimesheets() {
      var fList = [];
      var FilterID = ViewModel.FilterManagerID();

      if (FilterID == null) {
        return ViewModel.NSWTimesheetsAuthorisationList();
      }

      for (var i = 0; i < ViewModel.NSWTimesheetsAuthorisationList().length; i++) {
        var obj = ViewModel.NSWTimesheetsAuthorisationList()[i];
        if (obj.ManagerHumanResourceID() == FilterID) {
          fList(obj);

        }
      }
      return fList;
    }

  
 

  </script>
</asp:Content>
