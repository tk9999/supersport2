﻿<%@ Page Title="View Report" Language="vb" AutoEventWireup="false" CodeBehind="ReportData.aspx.vb" Inherits="NewOBWeb.ReportData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Report Data</title>

    <%= Singular.Web.Scripts.RenderLibraryScripts%>
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= ViewModel.IncludeGridReportResources%>
</head>
<body>
    <form id="form1" runat="server">
     <% Using h = Helpers
                    
        Dim GridReport As New Singular.Web.CustomControls.SGrid.GridReportContainer
        h.Control(GridReport)
            
      End Using%>

    <asp:ScriptManager ID="SCMMain" runat="server" />
    <singular:PageModelRenderer ID="pmrMain" runat="server" />
    </form>
</body>
</html>
