﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
  CodeBehind="ICRReports.aspx.vb" Inherits="NewOBWeb.ICRReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
    .form-control-sm
    {
      display: block !important;
      width: 100% !important;
      height: 26px !important;
      padding: 4px 8px !important;
      font-size: 10px !important;
      line-height: 1.428571429 !important;
      color: #555 !important;
      vertical-align: middle !important;
      background-color: #fff !important;
      background-image: none !important;
      border: 1px solid #ccc !important;
      border-radius: 4px !important;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
      box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
      -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      With h.DivC("row")
        With .Helpers.BootstrapDivColumn(12, 12, 12)
          .Helpers.BootstrapPageHeader(2, "ICR Reports", "")
        End With
      End With
    
      With h.DivC("row CriteriaContainer")
        With .Helpers.DivC("row top-buffer-10")
          With .Helpers.BootstrapDivColumn(12, 2, 1)
            .Helpers.EditorFor(Function(d) ViewModel.StartDate).AddClass("form-control-sm")
          End With
          With .Helpers.BootstrapDivColumn(12, 2, 1)
            .Helpers.EditorFor(Function(d) ViewModel.EndDate).AddClass("form-control-sm")
          End With
          With .Helpers.BootstrapDivColumn(12, 2, 1)
            .Helpers.EditorFor(Function(d) ViewModel.TeamID).AddClass("form-control-sm")
          End With
          With .Helpers.BootstrapDivColumn(12, 2, 1)
            .Helpers.EditorFor(Function(d) ViewModel.HumanResourceID).AddClass("form-control-sm")
          End With
          With .Helpers.BootstrapDivColumn(12, 2, 1)
            With .Helpers.BootstrapButton("Print", "Print Excel", "btn-sm btn-success", "glyphicon-download-alt", Singular.Web.PostBackType.Full, False, , , "")
              
            End With
          End With
          With .Helpers.BootstrapDivColumn(12, 2, 1)
            With .Helpers.BootstrapButton("PrintPDF", "Print PDF", "btn-sm btn-primary", "glyphicon-download-alt", Singular.Web.PostBackType.Full, False, , , "")
              
            End With
          End With
        End With
      End With

    End Using
  %>
</asp:Content>
