﻿Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports OBLib.Maintenance.ICR.ReadOnly
Imports Singular
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations
Imports System

Public Class ICRReportsVM
  Inherits OBViewModel(Of ICRReportsVM)

#Region " Properties "

  Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate)
  ''' <summary>
  ''' Gets and sets the Start Date value
  ''' </summary>
  <Display(Name:="Start Date", Description:="")>
  Public Property StartDate As DateTime?
    Get
      Return GetProperty(StartDateProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(StartDateProperty, Value)
    End Set
  End Property

  Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate)
  ''' <summary>
  ''' Gets and sets the End Date value
  ''' </summary>
  <Display(Name:="End Date", Description:="")>
  Public Property EndDate As DateTime?
    Get
      Return GetProperty(EndDateProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(EndDateProperty, Value)
    End Set
  End Property

  Public Shared TeamIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TeamID, "Team", Nothing)
  ''' <summary>
  ''' Gets and sets the Team value
  ''' </summary>
  <Display(Name:="Team", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROTeamList), DropDownType:=DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Select Team")>
  Public Property TeamID As Integer?
    Get
      Return GetProperty(TeamIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(TeamIDProperty, Value)
    End Set
  End Property

  Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Team", Nothing)
  ''' <summary>
  ''' Gets and sets the Team value
  ''' </summary>
  <Display(Name:="Team", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROHumanResourceList),
                                         DropDownType:=DataAnnotations.DropDownWeb.SelectType.Combo,
                                         Source:=DataAnnotations.DropDownWeb.SourceType.ViewModel,
                                         DisplayMember:="PreferredFirstSurname",
                                         UnselectedText:="Select HumanResource")>
  Public Property HumanResourceID As Integer?
    Get
      Return GetProperty(HumanResourceIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(HumanResourceIDProperty, Value)
    End Set
  End Property

  Private mROHumanResourceList As ROHumanResourceList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROHumanResourceList As ROHumanResourceList
    Get
      Return mROHumanResourceList
    End Get
  End Property

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.Setup()

    mROHumanResourceList = ROHumanResourceList.GetROHumanResourceList(Nothing, Nothing, "", "", "", "", "", "",
                                                                      Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID,
                                                                      OBLib.Security.Settings.CurrentUser.ProductionAreaID, Nothing, Nothing, Nothing)

    StartDate = Now
    EndDate = Now

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "Print"
        Dim rpt As New OBWebReports.ICRReports.HRShiftPayment
        rpt.ReportCriteria.StartDate = Me.StartDate
        rpt.ReportCriteria.EndDate = Me.EndDate
        rpt.ReportCriteria.TeamID = Me.TeamID
        rpt.ReportCriteria.HumanResourceID = Me.HumanResourceID
        'rpt.ReportCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
        'rpt.ReportCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
        Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.ExcelData)
        SendFile(rfi.FileName, rfi.FileBytes)


        'Dim rpt As New OBWebReports.ICRReports.HRShiftPayment With {.StartDate = Me.StartDate, _
        '                                                            .EndDate = Me.EndDate, _
        '                                                            .TeamID = Me.TeamID, _
        '                                                            .HumanResourceID = Me.HumanResourceID, _
        '                                                            .SystemID = OBLib.Security.Settings.CurrentUser.SystemID, _
        '                                                            .ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID}
        'Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.ExcelData)
        'SendFile(rfi.FileName, rfi.FileBytes)

      Case "PrintPDF"
        Dim rpt As New OBWebReports.ICRReports.HRShiftPayment
        rpt.ReportCriteria.StartDate = Me.StartDate
        rpt.ReportCriteria.EndDate = Me.EndDate
        rpt.ReportCriteria.TeamID = Me.TeamID
        rpt.ReportCriteria.HumanResourceID = Me.HumanResourceID
        'rpt.ReportCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
        'rpt.ReportCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
        Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        SendFile(rfi.FileName, rfi.FileBytes)

    End Select

  End Sub

#End Region

End Class
