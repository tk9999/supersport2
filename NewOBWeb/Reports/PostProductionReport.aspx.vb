﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports OBLib.NSWTimesheets
Imports System.Linq
Imports OBLib.HR.ReadOnly

Public Class PostProductionReport
  Inherits OBPageBase(Of PostProductionReportVM)

End Class

Public Class PostProductionReportVM
  Inherits OBViewModel(Of PostProductionReportVM)


  Public Property ProductionID As Integer
  Public Property ProductionDescription As String

  Public Property PostProductionReportList As OBLib.PostProductionReports.PostProductionReportList

  Protected Overrides Sub Setup()
    MyBase.Setup()
    If Page.Request.QueryString("ProductionID") IsNot Nothing Then
      ProductionID = Page.Request.QueryString("ProductionID")
      Dim ROProductionList As OBlib.Productions.ReadOnly.ROProductionList = OBLib.Productions.ReadOnly.ROProductionList.GetROProductionList(ProductionID)
      If ROProductionList.Count = 1 Then
        ProductionDescription = ROProductionList(0).ToString
      End If
    End If

    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    SetupDataSource()
    MessageFadeTime = 2000
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With TrySave(PostProductionReportList)
          If .Success Then
            PostProductionReportList = .SavedObject
          End If
        End With
      Case "GenerateData"
        GenerateData()
      Case "PrintReport"
        PrintReport()
    End Select
  End Sub

  Private Sub SetupDataSource()

    PostProductionReportList = OBLib.PostProductionReports.PostProductionReportList.GetPostProductionReportList(ProductionID)

  End Sub

  Private Sub GenerateData()

    Dim cmd As New Singular.CommandProc("CmdProcs.cmdGeneratePostProductionReport", "@ProductionID", ProductionID)
    cmd.Execute()
    PostProductionReportList = PostProductionReports.PostProductionReportList.GetPostProductionReportList(ProductionID)

  End Sub

  Private Sub PrintReport()
    If Not Singular.Misc.IsNullNothing(ProductionID, True) Then
      Dim lf As OBWebReports.ProductionReports.PostProductionReport = New OBWebReports.ProductionReports.PostProductionReport
      lf.ReportCriteria.ProductionID = ProductionID
      Dim df = lf.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
      SendFile(df.FileName, df.FileBytes)
    End If
  End Sub




End Class