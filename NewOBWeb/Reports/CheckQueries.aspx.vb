﻿Public Class CheckQueries
  Inherits OBPageBase(Of CheckQueriesVM)

End Class


Public Class CheckQueriesVM
  Inherits OBViewModel(Of CheckQueriesVM)

  Public Shared CheckQueryListProperty As Csla.PropertyInfo(Of Singular.CheckQueries.CheckQueryList) = RegisterProperty(Of Singular.CheckQueries.CheckQueryList)(Function(c) c.CheckQueryList)

  Public Property CheckQueryList() As Singular.CheckQueries.CheckQueryList
    Get
      Return GetProperty(CheckQueryListProperty)
    End Get
    Set(value As Singular.CheckQueries.CheckQueryList)
      SetProperty(CheckQueryListProperty, value)
    End Set
  End Property

  Protected Overrides Sub Setup()
    MyBase.Setup()

    CheckQueryList = Singular.CheckQueries.CheckQueryList.GetCheckQueriesList()

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "RunAll"
        For Each cq In CheckQueryList
          If cq.Status = 1 Then
            cq.Run()
          End If
        Next

      Case "Run"
        Dim cq = CheckQueryList.Where(Function(c) c.Description = CommandArgs.ClientArgs).FirstOrDefault()
        If cq.Status = 1 Then
          cq.Run()
        End If

      Case "Download"
        Dim cq = CheckQueryList.Where(Function(c) c.Description = CommandArgs.ClientArgs).FirstOrDefault()
        Dim ee As Singular.Data.ExcelExporter = New Singular.Data.ExcelExporter
        ee.PopulateData(cq.CheckQueryDataSet)
        SendFile(cq.Name & ".xls", ee.GetStream.ToArray())
    End Select

  End Sub

End Class