﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master"
  CodeBehind="Reports.aspx.vb" Inherits="NewOBWeb.Reports" %>

<%@ Import Namespace="OBLib.Maintenance.Productions.ReadOnly" %>

<%@ Import Namespace="Singular.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    span.input-group-btn > button {
      border-radius: 0 !important;
    }

    #ROContractTypeModal {
      height: 500px !important;
    }

    #ROTeamListModal {
      height: 500px !important;
    }

    #ROSystemTeamListModal {
      height: 500px !important;
    }

    #ROCityModal {
      height: 500px !important;
    }

    #ROProvinceModal {
      height: 500px !important;
    }

    .SelectedSpanClass {
    }

    fieldset {
      margin: 1em 0;
      padding: 1em;
      border: 1px solid #ccc;
      border-radius: 3px;
      background-color: #fcfcfc;
    }

      fieldset p {
        margin: 2px 12px 10px 10px;
      }

      fieldset.login label, fieldset.register label, fieldset.changePassword label {
        display: block;
      }

      fieldset label.inline {
        display: inline;
      }

    legend {
      font-size: 1.1em;
      font-weight: 600;
      padding: 0 4px 0 4px;
    }

    div.accountInfo {
      width: 42%;
    }

    .clear {
      clear: both;
    }

    .failureNotification {
      font-size: 1.2em;
      color: red;
    }

    .bold {
      font-weight: bold;
    }

    .submitButton {
      text-align: right;
      padding-right: 10px;
    }

    select {
      cursor: pointer;
    }

    tr.Selected td {
      background-color: rgba(128,128,128,.5);
    }

    .DivToolbar {
      background: #4285f4;
      color: #fff;
      padding: 10px;
    }

    .DivToolbar {
      box-shadow: 0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28);
    }

    .Negative {
      color: red;
    }

    .ImgIcon {
      background-image: url('../Images/IconsSmall.png');
    }

    .ImgLoading {
      background-image: url('../Singular/Images/LoadingSmall.gif');
    }

    .SavingDiv {
      position: absolute;
      top: 5px;
      left: 5px;
      padding: 5px;
      background-color: #111;
      color: #eee;
      display: inline-block;
    }

    .ReportSection {
      display: inline-block;
      float: left;
      vertical-align: top;
      min-width: 400px;
      background-color: #fff;
      border-radius: 3px;
      margin: 10px;
      box-shadow: 0 0 3px #bbb;
    }

      .ReportSection .Heading {
        background-color: #3a4f63;
        border-radius: 3px 3px 0 0;
        padding: 5px;
        font-size: 16px;
        font-variant: small-caps;
        color: #fff;
        font-weight: 600;
      }

      .ReportSection a {
        text-decoration: none;
        font-size: 9.5pt;
        font-weight: 400;
        color: #304150;
        transition: padding .5s ease-in-out;
      }

        .ReportSection a:hover {
          padding-left: 5px;
          font-weight: 600;
          color: #304150;
        }

      .ReportSection li {
        padding-bottom: 5px;
        color: #304150;
      }

      .ReportSection:hover {
        box-shadow: 0 0 5px 1px #bbb;
        transition: all .1s;
      }

        .ReportSection:hover .Heading {
          background-color: #304150;
          transition: all .5s;
        }

    .FadeHide {
      visibility: hidden;
      opacity: 0;
      transition: visibility 1s, opacity 1s;
    }

    .FadeDisplay {
      visibility: visible;
      opacity: 1;
      transition: visibility 1s, opacity 1s linear;
    }

    #ProductionAreas {
      visibility: hidden;
    }

    .buttontext {
      overflow: hidden;
      white-space: nowrap;
      display: block;
      text-overflow: ellipsis;
    }

    #HRDiv {
      display: block;
      max-height: 400px;
      overflow-y: scroll;
      overflow-x: hidden;
    }

    .add-vertical-scroll tbody td {
      width: 100%;
    }

    .add-vertical-scroll {
      display: block;
      max-height: 400px;
      overflow: hidden;
      overflow-y: auto;
      overflow-x: hidden;
    }

    .add-vertical-scroll-600 {
      display: block;
      max-height: 600px;
      overflow: hidden;
      overflow-y: auto;
      overflow-x: hidden;
    }

    #ProductionDiv {
      display: block;
      max-height: 451px;
      overflow-y: scroll;
    }

    div.modal-dg {
      display: none;
    }

    .add-text-align-left {
      text-align: left;
    }

  </style>
  <script type="text/javascript" src="../Scripts/Tools/PagingManagers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/FindROHumanResourceControl.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/Reports.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/Reports2.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Highcharts-4.0.1/js/highcharts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Highcharts-4.0.1/js/modules/exporting.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      If Model IsNot Nothing Then
        If Model.Report Is Nothing Then
          h.HTML.Heading2("Reports")
          h.HTML("Please select a report below.")
          h.HTML.NewLine()
        End If
      End If
         
      h.Control(New Singular.Web.Reporting.ReportStateControl)
      
      With h.DivC("modals")

         
        With h.Bootstrap.Dialog("RORoomListModal", "Search For Room", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
          'With .Helpers.DivC("modal-dg")
          '.Attributes("id") = "RORoomListModal"
          With .Body
            .AddClass("row colour-tone-modal")
            With .Helpers.Bootstrap.FlatBlock("", True)
              With .AboveContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.RORoomListCriteria.Keyword, BootstrapEnums.InputSize.Small)
                        '.AddClass("form-control input-sm")
                        .Editor.Attributes("placeholder") = "Search for Room"
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RORoomListPaged.DelayedRefreshList() }")
                      End With
                    End With
                  End With
                End With
              End With
                          
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Rooms.ReadOnly.RORoomPaged)(Function(d) ViewModel.RORoomListPagedManager,
                                                                                               Function(d) ViewModel.RORoomList,
                                                                                               False, False, False, False, True, True, False,
                                                                                               , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                               "RORoomListPaged.OnItemSelected", False)
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    With .FirstRow
                      .AddClass("items")
                      With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Rooms.ReadOnly.RORoomPaged) c.Room)
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Rooms.ReadOnly.RORoomPaged) c.RoomType)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
              
          
          
        
            
        
        'With h.Bootstrap.Dialog("ROChannelListModal", "Search for Channel")
        '    .ModalDialogDiv.AddClass("modal-med")
        '    With .Body
        '        .AddClass("row")
        '        With .Helpers.Bootstrap.FlatBlock("", True)
        '            With .AboveContentTag
        '                With .Helpers.Bootstrap.Row
        '                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
        '                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '                            With .Helpers.EditorFor(Function(d) ViewModel.ROChannelListCriteria.KeyWord)
        '                                .AddClass("form-control input-sm")
        '                                .Attributes("placeholder") = "Search for Channel"
        '                                .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
        '                                .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROChannels.DelayedRefreshList() }")
        '                            End With
        '                        End With
        '                    End With
        '                End With
        '            End With
        '            With .ContentTag
        '                With .Helpers.Bootstrap.Row
        '                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '                        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.General.ROPagedChannel)(Function(d) ViewModel.ROChannelListPagingManager,
        '                                                                                                        Function(d) ViewModel.ROChannelList,
        '                                                                                                        False, False, False, False, True, True, False,
        '                                                                                                        , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
        '                                                                                                        "ROChannels.OnItemSelected", True)
        '                            .AddClass("no-border hover list")
        '                            .TableBodyClass = "no-border-y"
        '                            .Pager.PagerListTag.ListTag.AddClass("pull-left")
        '                            With .FirstRow
        '                                .AddClass("items")
        '                                With .AddColumn("")
        '                                    .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
        '                                    With .Helpers.HTMLTag("span")
        '                                        .AddBinding(KnockoutBindingString.css, "ROChannels.ItemIsSelectedCss($data)")
        '                                        .AddBinding(KnockoutBindingString.html, "ROChannels.ItemIsSelectedHtml($data)")
        '                                    End With
        '                                End With
        '                                With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.General.ROPagedChannel) c.ChannelName, 250)
        '                                End With
        '                                With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.General.ROPagedChannel) c.ChannelShortName, 250)
        '                                End With
        '                                With .AddColumn("HD")
        '                                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Maintenance.General.ROPagedChannel) c.HDInd, "Yes", "No", "btn-success", , "", "", )
        '                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
        '                                    End With
        '                                End With
        '                            End With
        '                        End With
        '                    End With
        '                End With
        '            End With
        '        End With
        '    End With
        'End With
        
      End With
              
              
      With h.Bootstrap.Dialog("ROAdHocBookingListModal", "Search for Ad Hoc Booking", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
        With .Body
          .AddClass("row modal-background-gray")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROAdHocBookingListCriteria.Title, Singular.Web.BootstrapEnums.InputSize.Small)
                      '.AddClass("form-control input-sm")
                      .Editor.Attributes("placeholder") = "Search Ad Hoc Booking"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROAdHocBookings.DelayedRefreshList() }")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.StateButton(Function(c) ViewModel.SelectAllAdHocBookings, "Select All", "Select All", , , , , "btn-sm")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SelectAllCurrentBookingsInList($root.ROAdHocBookingListCriteria().StartDate(), $root.ROAdHocBookingListCriteria().EndDate())")
                      .Button.AddClass("btn-block")
                      .Button.Attributes("id") = "SelectAllBookingsBtn"
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.PagedGridFor(Of OBLib.AdHoc.ReadOnly.ROAdHocBooking)(Function(d) ViewModel.ROAdHocBookingListPagingManager,
                                                                                                Function(d) ViewModel.ROAdHocBookingList,
                                                                                                False, False, False, False, True, True, True,
                                                                                                , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                "ROAdHocBookings.OnItemSelected", False)
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    .Attributes("id") = "BookingsPagedGrid"
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        With .Helpers.HTMLTag("span")
                          .AddBinding(KnockoutBindingString.css, "ROAdHocBookings.ItemIsSelectedCss($data)")
                          .AddBinding(KnockoutBindingString.html, "ROAdHocBookings.ItemIsSelectedHtml($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Title)
                        .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingID, 100, "Ref No", "")
                        .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                        .Style.TextAlign = Singular.Web.TextAlign.left
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingType)
                        .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Description)
                        .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.StartDateTime)
                        .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.EndDateTime)
                        .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
              
              
      With h.Bootstrap.Dialog("ROAdHocBookingListSelectAllModal", "Search for Ad Hoc Booking(s)", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-search", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("", True)
                With .AboveContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.EditorFor(Function(d) ViewModel.ROAdHocBookingListCriteria.Title)
                          .AddClass("form-control input-sm")
                          .Attributes("placeholder") = "Search for Title"
                          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROAdHocBookings.DelayedRefreshList() }")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.StateButton(Function(c) ViewModel.SelectAllAdHocBookings, "Select All", "Select All", , , , , "btn-sm")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SelectAllCurrentBookingsInList($root.ROAdHocBookingListCriteria().StartDate(), $root.ROAdHocBookingListCriteria().EndDate())")
                          .Button.AddClass("btn-block")
                          .Button.Attributes("id") = "SelectAllBookingsBtn"
                        End With
                      End With
                    End With
                  End With
                End With
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.PagedGridFor(Of OBLib.AdHoc.ReadOnly.ROAdHocBooking)(Function(d) ViewModel.ROAdHocBookingListPagingManager,
                                                                                                    Function(d) ViewModel.ROAdHocBookingList,
                                                                                                    False, False, False, False, True, True, True,
                                                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                    "ROAdHocBookings.OnItemSelected", False)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        .Attributes("id") = "AllBookingsPagedGrid"
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn("")
                            .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                            With .Helpers.HTMLTag("span")
                              .AddBinding(KnockoutBindingString.css, "ROAdHocBookings.ItemIsSelectedCss($data)")
                              .AddBinding(KnockoutBindingString.html, "ROAdHocBookings.ItemIsSelectedHtml($data)")
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Title)
                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingID, 100, "Ref No", "")
                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                            .Style.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingType)
                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Description)
                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.StartDateTime)
                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.EndDateTime)
                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROAdHocBookingListSelectAllModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROAdHocBookingListCriteria.Title)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for Title"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROAdHocBookings.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.Bootstrap.StateButton(Function(c) ViewModel.SelectAllAdHocBookings, "Select All", "Select All", , , , , )
      '                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SelectAllCurrentBookingsInList($root.ROAdHocBookingListCriteria().StartDate(), $root.ROAdHocBookingListCriteria().EndDate())")
      '                    .Button.AddClass("btn-block")
      '                    .Button.Attributes("id") = "SelectAllBookingsBtn"
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.AdHoc.ReadOnly.ROAdHocBooking)(Function(d) ViewModel.ROAdHocBookingListPagingManager,
      '                                                                                        Function(d) ViewModel.ROAdHocBookingList,
      '                                                                                        False, False, False, False, True, True, False,
      '                                                                                        , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                        "ROAdHocBookings.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            .Attributes("id") = "BookingsPagedGrid"
      '            With .FirstRow
      '                .AddClass("items")
      '                With .AddColumn("")
      '                    .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
      '                    With .Helpers.HTMLTag("span")
      '                        .AddBinding(KnockoutBindingString.css, "ROAdHocBookings.ItemIsSelectedCss($data)")
      '                        .AddBinding(KnockoutBindingString.html, "ROAdHocBookings.ItemIsSelectedHtml($data)")
      '                    End With
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Title)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingID, 100, "Ref No", "")
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                    .Style.TextAlign = Singular.Web.TextAlign.left
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingType)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Description)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.StartDateTime)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.EndDateTime)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      'With h.Bootstrap.Dialog("ROTeamListModal", "Search For Tean", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
      '    With .Body
      '        .AddClass("row colour-tone-modal")
      '        With .Helpers.Bootstrap.FlatBlock("", True)
      '            With .AboveContentTag
      '                With .Helpers.Bootstrap.Row
      '                With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROSystemTeamListCriteria.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
      '                                .AddClass("form-control input-sm")
      '                                .Attributes("placeholder") = "Search for Team"
      '                                .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                                .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROSystemTeams.DelayedRefreshList() }")
      '                            End With
      '                    End With
      '                End With
      '                End With
      '            End With
                      
      '            With .Helpers.Div
      '                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.ICR.ReadOnly.ROTeam)(Function(d) ViewModel.ROSystemTeamListManager,
      '                                                                                    Function(d) ViewModel.ROSystemTeamList,
      '                                                                                    False, False, False, False, True, True, True,
      '                                                                                    "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                    "ROTeams.OnItemSelected", True)
      '                    .AddClass("no-border hover list")
      '                    .TableBodyClass = "no-border-y"
      '                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '                    With .FirstRow
      '                        .Style.PaddingAll("0px")
      '                        .AddClass("items")
      '                        'With .AddColumn("")
      '                        '  .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
      '                        '  With .Helpers.Bootstrap.StateButton(Function(c) c.SelectedInd, "", "", , , , , )
      '                        '    .Button.AddClass("btn-block")
      '                        '  End With
      '                        'End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.ICR.ReadOnly.ROTeam) c.TeamName)
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.ICR.ReadOnly.ROTeam) c.PatternName)
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
           
          
                  
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROSystemTeamListModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROSystemTeamListCriteria.KeyWord)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for Team"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROSystemTeams.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.PlayOutOps.ReadOnly.ROSystemTeam)(Function(d) ViewModel.ROSystemTeamListManager,
      '                                                                            Function(d) ViewModel.ROSystemTeamList,
      '                                                                            False, False, False, False, True, True, True,
      '                                                                            "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                            "ROSystemTeams.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            With .FirstRow
      '                .Style.PaddingAll("0px")
      '                .AddClass("items")
      '                With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.PlayOutOps.ReadOnly.ROSystemTeam) c.TeamName)
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
          
          
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROAdHocBookingListModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROAdHocBookingListCriteria.Title)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for Title"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROAdHocBookings.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.AdHoc.ReadOnly.ROAdHocBooking)(Function(d) ViewModel.ROAdHocBookingListPagingManager,
      '                                                                                        Function(d) ViewModel.ROAdHocBookingList,
      '                                                                                        False, False, False, False, True, True, False,
      '                                                                                        , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                        "ROAdHocBookings.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            With .FirstRow
      '                .AddClass("items")
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Title)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingID, 100, "Ref No", "")
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                    .Style.TextAlign = Singular.Web.TextAlign.left
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.AdHocBookingType)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.Description)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.StartDateTime)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.AdHoc.ReadOnly.ROAdHocBooking) c.EndDateTime)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROContractTypeModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROContractTypeListCriteria.ContractType)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for Contract Type"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROContractTypes.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.HR.ReadOnly.ROContractTypePaged)(Function(d) ViewModel.ROContractTypeListManager,
      '                                                                                        Function(d) ViewModel.ROContractTypeList,
      '                                                                                        False, False, False, False, True, True, False,
      '                                                                                        , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                        "ROContractTypes.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            With .FirstRow
      '                .AddClass("items")
      '                With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.HR.ReadOnly.ROContractTypePaged) c.ContractType)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROProductionTypes"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionTypePagedListCriteria.ProductionType, BootstrapEnums.InputSize.Small)
      '                .Editor.Attributes("placeholder") = "Search for Production Type"
      '                .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionTypes.DelayedRefreshList() }")
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged)("$root.ROProductionTypePagedListManager",
      '                                                                                                              "$root.ROProductionTypePagedList",
      '                                                                                                              False, False, False, False, True, True, False,
      '                                                                                                              "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                              "ROProductionTypes.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            With .FirstRow
      '                With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged) d.ProductionType)
      '                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      With h.Bootstrap.Dialog("TestGraphs", "")
        .ModalDialogDiv.AddClass("modal-med")
        With .Body
          With .Helpers.Bootstrap.Div
            .Attributes("id") = "container"
          End With
        End With
      End With
        
      With h.Bootstrap.Dialog("ROProductionTypes", "Search Production Type", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", "fa-2x", False)
        '.ModalDialogDiv.AddClass("modal-med")
        With .Body
          .AddClass("row colour-tone-modal")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionTypePagedListCriteria.ProductionType, BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Search for Production Type"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionTypes.DelayedRefreshList() }")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged)("$root.ROProductionTypePagedListManager",
                                                                                                                      "$root.ROProductionTypePagedList",
                                                                                                                      False, False, False, False, True, True, False,
                                                                                                                      "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                                      "ROProductionTypes.OnItemSelected", True)
                  .AddClass("no-border hover list")
                  With .FirstRow
                    With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged) d.ProductionType)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
              
              
      'With h.Bootstrap.Dialog("ROHumanResourcesListModal", "Search For Crew Member", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
      '    '.ModalDialogDiv.AddClass("modal-med")
      '    With .Body
      '        .AddClass("row colour-tone-modal")
      '        With .Helpers.Bootstrap.FlatBlock("", True)
      '            With .AboveContentTag
      '                With .Helpers.Bootstrap.Row
      '                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROHumanResourceListCriteria.HumanResourceID, BootstrapEnums.InputSize.Small)
      '                                .Editor.Attributes("placeholder") = "Search for Crew Member"
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROHumanResourceListModal.DelayedRefreshList() }")
      '                            End With
      '                        End With
      '                    End With
      '                End With
      '            End With
      '            With .ContentTag
      '                With .Helpers.Bootstrap.Row
      '                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.HR.ReadOnly.ROContractTypePaged)("$root.ROHumanResourceListManager",
      '                                                                                                                          "$root.ROHumanResourceListManager",
      '                                                                                                                          False, False, False, False, True, True, False,
      '                                                                                                                          "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                                          "ROHumanResourceListModal.OnItemSelected", True)
      '                        .AddClass("no-border hover list")
      '                        With .FirstRow
      '                            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.HR.ReadOnly.ROContractTypePaged) d.ContractType)
      '                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                            End With
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
              
      'With h.Bootstrap.Dialog("ROProductionTypes", "Select Production Type", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", "fa-2x", False)
      '    '.ModalDialogDiv.AddClass("modal-med")
      '    With .Body
      '        .AddClass("row colour-tone-modal")
      '        With .Helpers.Bootstrap.FlatBlock("", True)
      '            With .AboveContentTag
      '                With .Helpers.Bootstrap.Row
      '                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionTypePagedListCriteria.ProductionType, BootstrapEnums.InputSize.Small)
      '                                .Editor.Attributes("placeholder") = "Select Production Type"
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionTypes.DelayedRefreshList() }")
      '                            End With
      '                        End With
      '                    End With
      '                End With
      '            End With
      '            With .ContentTag
      '                With .Helpers.Bootstrap.Row
      '                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged)("$root.ROProductionTypePagedListManager",
      '                                                                                                                          "$root.ROProductionTypePagedList",
      '                                                                                                                          False, False, False, False, True, True, False,
      '                                                                                                                          "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                                          "ROProductionTypes.OnItemSelected", True)
      '                        .AddClass("no-border hover list")
      '                        With .FirstRow
      '                            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged) d.ProductionType)
      '                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                            End With
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
              
              
      With h.DivC("modal-dg")
        .Attributes("id") = "RODisciplineModal"
        With .Helpers.Div
          With .Helpers.Bootstrap.Column(12, 12, 4, 4)
            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
              With .Helpers.EditorFor(Function(d) ViewModel.RODisciplineListCriteria.Discipline)
                .AddClass("form-control input-sm")
                .Attributes("placeholder") = "Search for Discipline"
                .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RODisciplines.DelayedRefreshList() }")
              End With
            End With
          End With
        End With
        With .Helpers.Div
          With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.General.ReadOnly.RODisciplinePaged)(Function(d) ViewModel.RODisciplineListManager,
                                                                                          Function(d) ViewModel.RODisciplineList,
                                                                                          False, False, False, False, True, True, False,
                                                                                          , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                          "RODisciplines.OnItemSelected", False)
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y"
            .Pager.PagerListTag.ListTag.AddClass("pull-left")
            With .FirstRow
              .AddClass("items")
              With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.General.ReadOnly.RODisciplinePaged) c.Discipline)
                .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
              End With
            End With
          End With
        End With
      End With
              
      With h.Bootstrap.Dialog("RODisciplines", "Search For Disciplines", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
        With .Body
          .AddClass("row colour-tone-modal")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.RODisciplineListCriteria.Discipline, BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Search for Disciplines"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RODisciplines.DelayedRefreshList() }")
                    End With
                  End With
                End With
              End With
            End With
                          
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.General.ReadOnly.RODisciplinePaged)("$root.RODisciplineListManager",
                                                                                                              "$root.RODisciplineList",
                                                                                                              False, False, False, False, True, True, False,
                                                                                                               "DisciplinesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                              "RODisciplines.OnItemSelected", True)
                  .AddClass("no-border hover list")
                  With .FirstRow
                    With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.General.ReadOnly.RODisciplinePaged) d.Discipline)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                        
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
          
              
      With h.Bootstrap.Dialog("ROCities", "Search For Cities", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
        With .Body
          .AddClass("row colour-tone-modal")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROCityListCriteria.City, BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Search for Cities"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROCities.DelayedRefreshList() }")
                    End With
                  End With
                End With
              End With
            End With
                          
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Locations.ReadOnly.ROCityPaged)("$root.ROCityListManager",
                                                                                                          "$root.ROCityPagedList",
                                                                                                           False, False, False, False, True, True, False,
                                                                                                           , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                           "ROCities.OnItemSelected", True)
                  .AddClass("no-border hover list")
                  With .FirstRow
                    With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.Locations.ReadOnly.ROCityPaged) d.City)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                        
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
                    
              
      'With h.Bootstrap.Dialog("ROProductionTypes", "Search For Provinces", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", "fa-2x", False)
      '    '.ModalDialogDiv.AddClass("modal-med")
      '    With .Body
      '        .AddClass("row colour-tone-modal")
      '        With .Helpers.Bootstrap.FlatBlock("", True)
      '            With .AboveContentTag
      '                With .Helpers.Bootstrap.Row
      '                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionTypePagedListCriteria.ProductionType, BootstrapEnums.InputSize.Small)
      '                                .Editor.Attributes("placeholder") = "Search For Provinces"
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionTypes.DelayedRefreshList() }")
      '                            End With
      '                        End With
      '                    End With
      '                End With
      '            End With
      '            With .ContentTag
      '                With .Helpers.Bootstrap.Row
      '                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged)("$root.ROProvincePagedListManager",
      '                                                                                                                          "$root.ROProductionTypePagedList",
      '                                                                                                                          False, False, False, False, True, True, False,
      '                                                                                                                          "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                                          "ROProductionTypes.OnItemSelected", True)
             
              
      '                        .AddClass("no-border hover list")
      '                        With .FirstRow
      '                            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePaged) d.ProductionType)
      '                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                              
      '                            End With
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      With h.Bootstrap.Dialog("ROHumanResourceListModal", "Search for Human Resources", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-user", "fa-2x", False)
              
        With .Body
          .AddClass("row colour-tone-modal")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROHumanResourceListCriteria.Keyword, Singular.Web.BootstrapEnums.InputSize.Small)
                      '.AddClass("form-control input-sm")
                      .Editor.Attributes("placeholder") = "Search by Keyword"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROHumanResources.DelayedRefreshList() }")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourcePaged)("$root.ROHumanResourceListManager",
                                                                                                "$root.ROHumanResourceList",
                                                                                                 False, False, False, False, True, True, False,
                                                                                                 "HumanResourcesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                 "ROHumanResources.OnItemSelected", False)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn("")
                      .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                      With .Helpers.HTMLTag("span")
                        .AddBinding(KnockoutBindingString.css, "ROHumanResources.ItemIsSelectedCss($data)")
                        .AddBinding(KnockoutBindingString.html, "ROHumanResources.ItemIsSelectedHtml($data)")
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.EmployeeCode)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.IDNo)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Firstname)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.PreferredName)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Surname)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROHumanResourceListModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROHumanResourceListCriteria.Keyword)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search by Keyword"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROHumanResources.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourcePaged)(Function(d) ViewModel.ROHumanResourceListManager,
      '                                                                                        Function(d) ViewModel.ROHumanResourceList,
      '                                                                                        False, False, False, False, True, True, False,
      '                                                                                        , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                        "ROHumanResources.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            With .FirstRow
      '                .AddClass("items")
      '                'With .AddColumn("")
      '                '  .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
      '                '  With .Helpers.HTMLTag("span")
      '                '    .AddBinding(KnockoutBindingString.css, "ROHumanResources.ItemIsSelectedCss($data)")
      '                '    .AddBinding(KnockoutBindingString.html, "ROHumanResources.ItemIsSelectedHtml($data)")
      '                '  End With
      '                'End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.EmployeeCode)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.IDNo)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Firstname)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.PreferredName)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Surname)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      With h.Bootstrap.Dialog("ROProductionListSelectAllModal", "Search for Production(s)", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-search", "fa-2x", True)
        With .Body
          .AddClass("row")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search for Production"
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductions.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(c) ViewModel.SelectAllProductions, "Select All", "Select All", , , , , "btn-sm")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SelectAllCurrentProductionsInList($root.ROProductionListCriteria().TxDateFrom(), $root.ROProductionListCriteria().TxDateTo())")
                    .Button.AddClass("btn-block")
                    .Button.Attributes("id") = "SelectAllBtn"
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) ViewModel.ROProductionListPagingManager,
                                                                                                   Function(d) ViewModel.ROProductionList,
                                                                                                   False, False, False, False, True, True, False,
                                                                                                   , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                   "ROProductions.OnItemSelected", False)
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    .Attributes("id") = "ProdPagedGrid"
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        .Attributes("id") = "SelectedCol"
                        With .Helpers.HTMLTag("span")
                          .AddClass("SelectedSpanClass")
                          .Attributes("id") = "SelectedSpan"
                          .AddBinding(KnockoutBindingString.css, "ROProductions.ItemIsSelectedCss($data)")
                          .AddBinding(KnockoutBindingString.html, "ROProductions.ItemIsSelectedHtml($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionRefNo)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.Title)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionType)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventType)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxStartDateTime)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxEndDateTime)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventManager)
                        '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROProductionListSelectAllModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for Production"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductions.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.Bootstrap.StateButton(Function(c) ViewModel.SelectAllProductions, "Select All", "Select All", , , , , )
      '                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SelectAllCurrentProductionsInList($root.ROProductionListCriteria().TxDateFrom(), $root.ROProductionListCriteria().TxDateTo())")
      '                    .Button.AddClass("btn-block")
      '                    .Button.Attributes("id") = "SelectAllBtn"
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) ViewModel.ROProductionListPagingManager,
      '                                                                                         Function(d) ViewModel.ROProductionList,
      '                                                                                         False, False, False, False, True, True, False,
      '                                                                                         , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                         "ROProductions.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            .Attributes("id") = "ProdPagedGrid"
      '            With .FirstRow
      '                .AddClass("items")
      '                With .AddColumn("")
      '                    .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
      '                    .Attributes("id") = "SelectedCol"
      '                    With .Helpers.HTMLTag("span")
      '                        .AddClass("SelectedSpanClass")
      '                        .Attributes("id") = "SelectedSpan"
      '                        .AddBinding(KnockoutBindingString.css, "ROProductions.ItemIsSelectedCss($data)")
      '                        .AddBinding(KnockoutBindingString.html, "ROProductions.ItemIsSelectedHtml($data)")
      '                    End With
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionRefNo)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.Title)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionType)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventType)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxStartDateTime)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxEndDateTime)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventManager)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROProductionListModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for Production"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductions.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) ViewModel.ROProductionListPagingManager,
      '                                                                                         Function(d) ViewModel.ROProductionList,
      '                                                                                         False, False, False, False, True, True, False,
      '                                                                                         , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                         "ROProductions.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            With .FirstRow
      '                .AddClass("items")
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionRefNo)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.Title)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionType)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventType)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxStartDateTime)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxEndDateTime)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventManager)
      '                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
              
      With h.Bootstrap.Dialog("ROProductionListModal", "Search for Production", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", "fa-2x", False)
        '.ModalDialogDiv.AddClass("modal-med")
        With .Body
          .AddClass("row colour-tone-modal")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionListCriteria.Keyword, Singular.Web.BootstrapEnums.InputSize.Small)
                    '.AddClass("form-control input-sm")
                    .Editor.Attributes("placeholder") = "Search for Production"
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductions.DelayedRefreshList() }")
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.ReadOnly.ROProduction)("$root.ROProductionListPagingManager",
                                                                                               "$root.ROProductionList",
                                                                                               False, False, False, False, True, True, False,
                                                                                               , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                               "ROProductions.OnItemSelected", False)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                .Pager.PagerListTag.ListTag.AddClass("pull-left")
                With .FirstRow
                  .AddClass("items")
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionRefNo)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.Title)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionType)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventType)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxStartDateTime)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxEndDateTime)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventManager)
                    '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
              
              
      'With h.Bootstrap.Dialog("ROProductionTypes", "Search Production Type", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", "fa-2x", False)
      '    '.ModalDialogDiv.AddClass("modal-med")
      '    With .Body
      '        .AddClass("row colour-tone-modal")
      '        With .Helpers.Bootstrap.FlatBlock("", True)
      '            With .AboveContentTag
      '                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                        With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
      '                            .AddClass("form-control input-sm")
      '                            .Attributes("placeholder") = "Search Production Type"
      '                            .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                            .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionTypes.DelayedRefreshList() }")
      '                        End With
      '                    End With
      '                End With
      '            End With
      '            With .ContentTag
      '                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) ViewModel.ROProductionTypePagedListManager,
      '                                                                                                 Function(d) ViewModel.ROProductionTypePagedList,
      '                                                                                                 False, False, False, False, True, True, False,
      '                                                                                                 , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                 "ROProductionTypes.OnItemSelected", True)
      '                    .AddClass("no-border hover list")
      '                    .TableBodyClass = "no-border-y"
      '                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '                    With .FirstRow
      '                        .AddClass("items")
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionTypeID)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionType)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.ProductionType)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventType)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxStartDateTime)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.TxEndDateTime)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                        With .AddReadOnlyColumn(Function(c As OBLib.Productions.ReadOnly.ROProduction) c.EventManager)
      '                            '.AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
              
              
      With h.Bootstrap.Dialog("ROEventTypesModal", "Search for Event Type", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)

        '.ModalDialogDiv.AddClass("modal-med")
        With .Body
          .AddClass("row")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROEventTypePagedListCriteria.ProductionType, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Search by Genre"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEventTypes.DelayedRefreshList() }")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROEventTypePagedListCriteria.EventType, Singular.Web.BootstrapEnums.InputSize.Small)
                      '.AddClass("form-control input-sm")
                      .Editor.Attributes("placeholder") = "Search by Series"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEventTypes.DelayedRefreshList() }")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  .AddClass("pull-right")
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                                   Singular.Web.PostBackType.None, "ROEventTypes.RefreshList()")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Productions.ReadOnly.ROEventTypePaged)("$root.ROEventTypePagedListManager",
                                                                                                                 "$root.ROEventTypePagedList",
                                                                                                                  False, False, False, False, True, True, False,
                                                                                                                  "EventTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                                  "ROEventTypes.OnItemSelected")
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn("")
                      .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                      With .Helpers.HTMLTag("span")
                        .AddBinding(KnockoutBindingString.css, "ROEventTypes.ItemIsSelectedCss($data)")
                        .AddBinding(KnockoutBindingString.html, "ROEventTypes.ItemIsSelectedHtml($data)")
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.ProductionType)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.EventType)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
        
      'With h.Div
      '    .AddClass("modal-dg big-text-mode black-text")
      '    .Attributes("id") = "ROEventTypesModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROEventTypePagedListCriteria.ProductionType)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search by Genre"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEventTypes.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROEventTypePagedListCriteria.EventType)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search by Series"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEventTypes.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            .AddClass("pull-right")
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
      '                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
      '                                               Singular.Web.PostBackType.None, "ROEventTypes.RefreshList()")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '            With .Helpers.DivC("table-responsive")
      '                With .Helpers.Bootstrap.PagedGridFor(Of ROEventTypePaged)(Function(d) ViewModel.ROEventTypePagedListManager,
      '                                                                          Function(d) ViewModel.ROEventTypePagedList,
      '                                                                          False, False, False, False, True, True, False,
      '                                                                          "EventTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                          "ROEventTypes.OnItemSelected")
      '                    .AddClass("no-border hover list")
      '                    .TableBodyClass = "no-border-y"
      '                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '                    With .FirstRow
      '                        .AddClass("items")
      '                        With .AddColumn("")
      '                            .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
      '                            With .Helpers.HTMLTag("span")
      '                                .AddBinding(KnockoutBindingString.css, "ROEventTypes.ItemIsSelectedCss($data)")
      '                                .AddBinding(KnockoutBindingString.html, "ROEventTypes.ItemIsSelectedHtml($data)")
      '                            End With
      '                        End With
      '                        With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.ProductionType)
      '                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                        End With
      '                        With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.EventType)
      '                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
        
        
      'With h.Bootstrap.Dialog("ROEquipmentScheduleSelectModal", "Bookings", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
      '  With .Body
      '    .AddClass("row colour-tone-modal")
      '    With .Helpers.Bootstrap.FlatBlock("", True)
      '      With .AboveContentTag
      '        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROEquipmentListCriteria.FilterName, BootstrapEnums.InputSize.Small)
      '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEquipments.DelayedRefreshList() }")
      '              .Editor.Attributes("placeholder") = "Search by Name"
      '            End With
      '          End With
      '        End With
      '      End With
             
                  
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          '.AddClass("flat-block-paged")
      '          With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Equipment.ReadOnly.ROEquipment)(Function(d) ViewModel.ROEquipmentListPagingManager,
      '                                                                                                    Function(d) ViewModel.ROEquipmentList,
      '                                                                                                    False, False, False, False, True, True, False,
      '                                                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                    "ROEquipments.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            With .FirstRow
      '              With .AddColumn("")
      '                .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
      '                With .Helpers.HTMLTag("span")
      '                  .AddBinding(KnockoutBindingString.css, "ROEquipments.ItemIsSelectedCss($data)")
      '                  .AddBinding(KnockoutBindingString.html, "ROEquipments.ItemIsSelectedHtml($data)")
      '                End With
      '              End With
      '              With .AddReadOnlyColumn(Function(d) d.EquipmentDescription)
      '                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '              End With
      '              With .AddReadOnlyColumn(Function(d) d.EquipmentType)
      '                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '              End With
      '              With .AddReadOnlyColumn(Function(d) d.Serial)
      '                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '              End With
      '              With .AddReadOnlyColumn(Function(d) d.AssetTagNumber)
      '                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '              End With
      '              With .AddReadOnlyColumn(Function(d) d.VehicleName)
      '                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '              End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
          
      '    With .Helpers.Bootstrap.Column(6, 6, 6, 6)
      '        With .Helpers.FieldSet("Select Bookings")
      '            With .Helpers.Div
      '                .AddClass("flat-block-paged")
      '                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.SatOps.ReadOnly.ROEquipmentSchedule)(Function(d) ViewModel.ROEquipmentScheduleListPagingManager,
      '                                                                                                   Function(d) ViewModel.ROEquipmentScheduleList,
      '                                                                                                   False, False, False, False, True, True, False,
      '                                                                                                   "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                                   "ROEquipmentSchedules.OnItemSelected", True)
      '                    .AddClass("no-border hover list")
      '                    With .FirstRow
      '                        With .AddReadOnlyColumn(Function(d As OBLib.SatOps.ReadOnly.ROEquipmentSchedule) d.EquipmentScheduleID)
      '                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                        End With
      '                        With .AddReadOnlyColumn(Function(d As OBLib.SatOps.ReadOnly.ROEquipmentSchedule) d.EquipmentScheduleTitle)
      '                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
      '                        End With
      '                        With .AddReadOnlyColumn(Function(d As OBLib.SatOps.ReadOnly.ROEquipmentSchedule) d.StartDateTime)
      '                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
      '                        End With
      '                        With .AddReadOnlyColumn(Function(d As OBLib.SatOps.ReadOnly.ROEquipmentSchedule) d.EndDateTime)
      '                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
      '                        End With
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With

      'End With
        
      'With .Helpers.DivC("modal-dg")
      '    .Attributes("id") = "ROCityModal"
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                'With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROCityListCriteria.City, BootstrapEnums.InputSize.Small)
      '                '  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                '  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROCities.DelayedRefreshList() }")
      '                '  .Editor.Attributes("placeholder") = "Search for City"
      '                'End With
      '                With .Helpers.EditorFor(Function(d) ViewModel.ROCityListCriteria.City)
      '                    .AddClass("form-control input-sm")
      '                    .Attributes("placeholder") = "Search for City"
      '                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROCities.DelayedRefreshList() }")
      '                End With
      '            End With
      '        End With
      '    End With
      '    With .Helpers.Div
      '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Locations.ReadOnly.ROCityPaged)(Function(d) ViewModel.ROCityListManager,
      '                                                                                        Function(d) ViewModel.ROCityPagedList,
      '                                                                                        False, False, False, False, True, True, False,
      '                                                                                        , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '                                                                                        "ROCities.OnItemSelected", True)
      '            .AddClass("no-border hover list")
      '            .TableBodyClass = "no-border-y"
      '            .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '            With .FirstRow
      '                .AddClass("items")
      '                With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Locations.ReadOnly.ROCityPaged) c.City)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '                With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Locations.ReadOnly.ROCityPaged) c.CityCode)
      '                    .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
          
        
              
            
              
              
      With h.Bootstrap.Dialog("ROProvinces", "Search For Provinces", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , , , False)
        'With .Helpers.DivC("modal-dg")
        '    .Attributes("id") = "ROProvinceModal"
        With .Body
          .AddClass("row colour-tone-modal")
          With .Helpers.Bootstrap.FlatBlock("", True)
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProvincePagedListCriteria.Province, BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Search For Provinces"
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProvinces.DelayedRefreshList() }")
                    End With
                                   
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Locations.ReadOnly.ROProvincePaged)((Function(d) ViewModel.ROProvincePagedListManager),
                                                                                                    Function(d) ViewModel.ROProvincePagedList,
                                                                                                    False, False, False, False, True, True, False,
                                                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                    "ROProvinces.OnItemSelected", False)
                  '    End With
                  'End With
                  'With .Helpers.Div
                  '    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.Locations.ReadOnly.ROProvincePaged)(Function(d) ViewModel.ROProvincePagedListManager,
                  '                                                                                    Function(d) ViewModel.ROProvincePagedList,
                  '                                                                                    False, False, False, False, True, True, False,
                  '                                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                  '                                                                                    "ROProvinces.OnItemSelected", True)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Locations.ReadOnly.ROProvincePaged) c.Province)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                  End With
                End With
              End With
                       
            End With
                      
          End With
        End With
      End With
              
      'End With
      
      h.Control(New NewOBWeb.Controls.FindHumanResourceModal(Of NewOBWeb.HumanResourceVM)("FindROHumanResourceModal", "FindHRControl"))
          
    
    End Using
  %>
</asp:Content>
