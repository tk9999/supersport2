﻿Imports Singular.DataAnnotations
Imports OBLib.Maintenance.HR
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBWebReports
Imports Singular
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports Singular.Web
Imports OBLib.Security
Imports OBLib.Maintenance.General.ReadOnly
Imports System

Public Class Reports
  Inherits Singular.Web.PageBase(Of ReportVM)

End Class

Public Class ReportVM
  Inherits Singular.Web.Reporting.ReportVM

  Public ReadOnly Property CurrentUserName As String
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.LoginName
      Else
        Return "No Current User"
      End If
    End Get
  End Property

  Public ReadOnly Property CurrentUserID As Integer?
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.UserID
      Else
        Return Nothing
      End If
    End Get
  End Property

  Protected Overrides Sub WriteInitialiseVariables(JSW As Utilities.JavaScriptWriter)
    MyBase.WriteInitialiseVariables(JSW)
    Dim mSiteInfo As New OBLib.Helpers.SiteInfo(Me.Page.Request)
    JSW.Write("window.SiteInfo = " & Singular.Web.Data.JSonWriter.SerialiseObject(mSiteInfo, , False, Singular.Web.Data.OutputType.Javascript) & ";")
  End Sub

  Public Property CurrentUserSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID
  Public Property CurrentUserProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

  Public Property SelectAllProductions As Boolean = False
  Public Property SelectAllAdHocBookings As Boolean = False

  <ClientOnly>
  Public Property IsReportBusy As Boolean = False

  'Dim usr As OBLib.Security.User = OBLib.Security.UserList.GetUser(OBLib.Security.Settings.c
  Public Property UserSystemList As OBLib.Security.UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

  'Production Paging Managers--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  'Teams
  '<InitialDataOnly>
  'Public Property ROTeamList As OBLib.Maintenance.ICR.ReadOnly.ROTeamList = New OBLib.Maintenance.ICR.ReadOnly.ROTeamList
  'Public Property ROTeamListCriteria As OBLib.Maintenance.ICR.ReadOnly.ROTeamList.Criteria = New OBLib.Maintenance.ICR.ReadOnly.ROTeamList.Criteria
  'Public Property ROTeamListManager As Singular.Web.Data.PagedDataManager(Of TeamsVM) = New Singular.Web.Data.PagedDataManager(Of TeamsVM)(Function(d) Me.ROTeamList,
  '                                                                                                                                         Function(d) Me.ROTeamListCriteria,
  '                                                                                                                                         "StartDate", 15)
  'Ad Hoc Bookings
  <InitialDataOnly>
  Public Property ROAdHocBookingList As OBLib.AdHoc.ReadOnly.ROAdHocBookingList = New OBLib.AdHoc.ReadOnly.ROAdHocBookingList
  Public Property ROAdHocBookingListCriteria As OBLib.AdHoc.ReadOnly.ROAdHocBookingList.Criteria = New OBLib.AdHoc.ReadOnly.ROAdHocBookingList.Criteria
  Public Property ROAdHocBookingListPagingManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROAdHocBookingList,
                                                                                                                                              Function(d) Me.ROAdHocBookingListCriteria,
                                                                                                                                              "StartDateTime", 15)

  'Rooms
  <InitialDataOnly>
  Public Property RORoomList As OBLib.Maintenance.Rooms.ReadOnly.RORoomListPaged = New OBLib.Maintenance.Rooms.ReadOnly.RORoomListPaged
  Public Property RORoomListCriteria As OBLib.Maintenance.Rooms.ReadOnly.RORoomListPaged.Criteria = New OBLib.Maintenance.Rooms.ReadOnly.RORoomListPaged.Criteria
  Public Property RORoomListPagedManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.RORoomList,
                                                                                                                                                     Function(d) Me.RORoomListCriteria,
                                                                                                                                                     "Room", 15)
  'Disciplines
  <InitialDataOnly>
  Public Property RODisciplineList As OBLib.Maintenance.General.ReadOnly.RODisciplinePagedList = New OBLib.Maintenance.General.ReadOnly.RODisciplinePagedList
  Public Property RODisciplineListCriteria As OBLib.Maintenance.General.ReadOnly.RODisciplinePagedList.Criteria = New OBLib.Maintenance.General.ReadOnly.RODisciplinePagedList.Criteria
  Public Property RODisciplineListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.RODisciplineList,
                                                                                                                                                   Function(d) Me.RODisciplineListCriteria,
                                                                                                                                                   "Discipline", 15)

  'Contract Types
  <InitialDataOnly>
  Public Property ROContractTypeList As OBLib.Maintenance.HR.ReadOnly.ROContractTypePagedList = New OBLib.Maintenance.HR.ReadOnly.ROContractTypePagedList
  Public Property ROContractTypeListCriteria As OBLib.Maintenance.HR.ReadOnly.ROContractTypePagedList.Criteria = New OBLib.Maintenance.HR.ReadOnly.ROContractTypePagedList.Criteria
  Public Property ROContractTypeListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROContractTypeList,
                                                                                                                                                     Function(d) Me.ROContractTypeListCriteria,
                                                                                                                                                     "ContractType", 15)

  'Production Types
  <InitialDataOnly>
  Public Property ROProductionTypePagedList As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePagedList = New OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePagedList
  Public Property ROProductionTypePagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePagedList.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePagedList.Criteria
  Public Property ROProductionTypePagedListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROProductionTypePagedList,
                                                                                                                                                            Function(d) Me.ROProductionTypePagedListCriteria,
                                                                                                                                                            "ProductionType", 15)

  'Event Types
  <InitialDataOnly>
  Public Property ROEventTypePagedList As OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList = New OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList
  Public Property ROEventTypePagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList.Criteria
  Public Property ROEventTypePagedListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROEventTypePagedList,
                                                                                                                                                       Function(d) Me.ROEventTypePagedListCriteria,
                                                                                                                                                       "EventType", 15)

  'Production Venues
  <InitialDataOnly>
  Public Property ROProductionVenueList As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList = New OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList
  Public Property ROProductionVenueListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria
  Public Property ROProductionVenueListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROProductionVenueList,
                                                                                                                                                        Function(d) Me.ROProductionVenueListCriteria,
                                                                                                                                                        "ProductionVenue", 15)
  'Productions
  <InitialDataOnly>
  Public Property ROProductionList As OBLib.Productions.ReadOnly.ROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROProductionList,
                                                                                                                                                         Function(d) Me.ROProductionListCriteria,
                                                                                                                                                         "ProductionRefNo", 15)

  'Production Find
  <InitialDataOnly>
  Public Property ROProductionFindList As OBLib.Productions.ReadOnly.ROProductionFindList = New OBLib.Productions.ReadOnly.ROProductionFindList
  Public Property ROProductionFindListCriteria As OBLib.Productions.ReadOnly.ROProductionFindList.Criteria = New OBLib.Productions.ReadOnly.ROProductionFindList.Criteria
  Public Property ROProductionFindListPagingManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROProductionFindList,
                                                                                                                                                         Function(d) Me.ROProductionFindListCriteria,
                                                                                                                                                         "PlayStartDateTime", 15)


  'Human Resource Paging Managers---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  'Human Resources
  <InitialDataOnly>
  Public Property ROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourcePagedList = New OBLib.HR.ReadOnly.ROHumanResourcePagedList
  Public Property ROHumanResourceListCriteria As OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria = New OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria
  Public Property ROHumanResourceListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) Me.ROHumanResourceList,
                                                                                                                                                                    Function(d) Me.ROHumanResourceListCriteria,
                                                                                                                                                                    "FirstName", 15)

  <InitialDataOnly>
  Public Property ROHumanResourceFindList As OBLib.HR.ReadOnly.ROHumanResourceFindList = New OBLib.HR.ReadOnly.ROHumanResourceFindList
  Public Property ROHumanResourceFindListCriteria As OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria = New OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria
  Public Property ROHumanResourceFindListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROHumanResourceFindList,
                                                                                                                                                                    Function(d) Me.ROHumanResourceFindListCriteria,
                                                                                                                                                                    "FirstName", 15)

  'Equipment Paging Managers--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  'Equipment List
  <InitialDataOnly>
  Public Property ROEquipmentList As OBLib.Equipment.ReadOnly.ROEquipmentList = New OBLib.Equipment.ReadOnly.ROEquipmentList
  Public Property ROEquipmentListCriteria As OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria = New OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria
  Public Property ROEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROEquipmentList,
                                                                                                                                                        Function(d) Me.ROEquipmentListCriteria,
                                                                                                                                                        "EquipmentDescription", 15)

  'Equipment Schedule List
  <InitialDataOnly>
  Public Property ROEquipmentScheduleList As OBLib.SatOps.ReadOnly.ROEquipmentScheduleList = New OBLib.SatOps.ReadOnly.ROEquipmentScheduleList
  Public Property ROEquipmentScheduleListCriteria As OBLib.SatOps.ReadOnly.ROEquipmentScheduleList.Criteria = New OBLib.SatOps.ReadOnly.ROEquipmentScheduleList.Criteria
  Public Property ROEquipmentScheduleListPagingManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROEquipmentScheduleList,
                                                                                                                                                                Function(d) Me.ROEquipmentScheduleListCriteria,
                                                                                                                                                                "EquipmentScheduleComments", 15)
  'City List Paged
  <InitialDataOnly>
  Public Property ROCityPagedList As OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList = New OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList
  Public Property ROCityListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList.Criteria = New OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList.Criteria
  Public Property ROCityListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROCityPagedList,
                                                                                                                                                                Function(d) Me.ROCityListCriteria,
                                                                                                                                                                "City", 15)
  'Province List Paged
  <InitialDataOnly>
  Public Property ROProvincePagedList As OBLib.Maintenance.Locations.ReadOnly.ROProvincePagedList = New OBLib.Maintenance.Locations.ReadOnly.ROProvincePagedList
  Public Property ROProvincePagedListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROProvincePagedList.Criteria = New OBLib.Maintenance.Locations.ReadOnly.ROProvincePagedList.Criteria
  Public Property ROProvincePagedListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROProvincePagedList,
                                                                                                                                                                Function(d) Me.ROProvincePagedListCriteria,
                                                                                                                                                                "Province", 15)

  'Event Type Report Paged
  Public Property ROEventTypeReportPagedList As OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport = New OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport
  Public Property ROEventTypeReportPagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport.Criteria
  Public Property ROEventTypeReportPagedListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROEventTypeReportPagedList,
                                                                                                                                                             Function(d) Me.ROEventTypeReportPagedListCriteria,
                                                                                                                                                             "EventType", 15)
  'Production Type Report Paged
  Public Property ROProductionTypeReportPagedList As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport = New OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport
  Public Property ROProductionTypeReportPagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria
  Public Property ROProductionTypeReportPagedListManager As Singular.Web.Data.PagedDataManager(Of ReportVM) = New Singular.Web.Data.PagedDataManager(Of ReportVM)(Function(d) Me.ROProductionTypeReportPagedList,
                                                                                                                                                                  Function(d) Me.ROProductionTypeReportPagedListCriteria,
                                                                                                                                                                  "ProductionType", 15)

  Public Property ROProductionAreaAllowedDisciplineList As List(Of ROProductionAreaAllowedDiscipline) = New List(Of ROProductionAreaAllowedDiscipline)

  Public Property ROFilteredHumanResourceList As List(Of ROHumanResource) = New List(Of ROHumanResource)

  Public Property ROFilteredEventTypeReportList As List(Of ROEventTypeReport) = New List(Of ROEventTypeReport)

  Public Property ROFilteredProductionTypeList As List(Of ROProductionTypeReport) = New List(Of ROProductionTypeReport)

  Protected Overrides Sub Setup()
    Singular.Reporting.ProjectReportHierarchy = New OBWebReports.OBWebReportHierarchy
    MyBase.Setup()

    For Each us As UserSystem In OBLib.Security.Settings.CurrentUser.UserSystemList
      us.IsSelected = False
      For Each usa As UserSystemArea In us.UserSystemAreaList
        usa.IsSelected = False
      Next
    Next

    For Each us As ROUserSystem In OBLib.Security.Settings.CurrentUser.ROUserSystemList
      us.ResetSelected()
    Next

    For Each usa As ROUserSystemArea In OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList
      usa.ResetSelected()
    Next

    If Report IsNot Nothing Then
      Select Case Report.ReportName
        'Case "Production Status Report"
        '  ROProductionTypePagedListManager.SingleSelect = True
        '  ROProductionTypePagedListManager.MultiSelect = False
        '  ROProductionTypePagedListManager.GetInitialData()

        'Case "Equipment Booking Report"
        '  ROEquipmentListPagingManager.MultiSelect = True
        '  ROEquipmentList = ROEquipmentListPagingManager.GetInitialData()

        Case "Monthly Production Crew Schedule"
          ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
          ClientDataProvider.AddDataSource("ROEventTypeList", OBLib.CommonData.Lists.ROEventTypeList, False)

        Case "Production Overview"
          ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
          ClientDataProvider.AddDataSource("ROEventTypeList", OBLib.CommonData.Lists.ROEventTypeList, False)
          ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)

        Case "Human Resource S&T"
          ClientDataProvider.AddDataSource("ROGroupSnTList", OBLib.CommonData.Lists.ROGroupSnTList, False)

        Case "Meal Reimbursements Detailed"
          ClientDataProvider.AddDataSource("ROMRMonthList", OBLib.CommonData.Lists.ROMRMonthList, False)

        Case "Meal Reimbursements Summary (Production Content)"
          ClientDataProvider.AddDataSource("ROMRMonthList", OBLib.CommonData.Lists.ROMRMonthList, False)

        Case "Meal Reimbursements Detail (Production Content)"
          ClientDataProvider.AddDataSource("ROMRMonthList", OBLib.CommonData.Lists.ROMRMonthList, False)

        Case "Meal Reimbursements - Pending Shift Authorisations"
          ClientDataProvider.AddDataSource("ROMRMonthList", OBLib.CommonData.Lists.ROMRMonthList, False)

        Case "Meal Reimbursements Summary"
          ClientDataProvider.AddDataSource("ROMRMonthList", OBLib.CommonData.Lists.ROMRMonthList, False)

        Case "Production Venue Report"
          ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
          ClientDataProvider.AddDataSource("ROEventTypeList", OBLib.CommonData.Lists.ROEventTypeList, False)
          ClientDataProvider.AddDataSource("ROCityList", OBLib.CommonData.Lists.ROCityList, False)
          ClientDataProvider.AddDataSource("ROProvinceList", OBLib.CommonData.Lists.ROProvinceList, False)
          ClientDataProvider.AddDataSource("ROProductionVenueList", OBLib.CommonData.Lists.ROProductionVenueList, False)

        Case "Production Overview"
          ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
          ClientDataProvider.AddDataSource("ROEventTypeList", OBLib.CommonData.Lists.ROEventTypeList, False)
          ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)

        Case "Production Status Report"
          ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)

        Case "Annual Leave Report"
          ClientDataProvider.AddDataSource("ROOffReasonList", OBLib.CommonData.Lists.ROOffReasonList, False)

        Case "Print HR Timesheets"
          ClientDataProvider.AddDataSource("ROManagerList", OBLib.CommonData.Lists.ROManagerList, False)
          ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)

        Case "ICR No Meal Credit"
          ClientDataProvider.AddDataSource("ROMRMonthList", OBLib.CommonData.Lists.ROMRMonthList, False)

        Case "MCR Bookings Report"
          ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
          ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)

        Case "SCCR Bookings Report"
          ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
          ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)

        Case "Bookings by Controller Report"
          ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
          ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)

        Case "Bookings Report"
          ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
          ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)

        Case "Sms Delivery"
          ClientDataProvider.AddDataSource("ROSmsSearchScenarioList", OBLib.CommonData.Lists.ROSmsSearchScenarioList, False)

      End Select

      ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
      ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
      ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
      ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
      ClientDataProvider.AddDataSource("ROMRMonthLastElevenList", OBLib.CommonData.Lists.ROMRMonthLastElevenList, False)
      ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
      ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)

    End If

  End Sub

  'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)

  '  MyBase.HandleCommand(Command, CommandArgs)

  '  Select Case Command
  '    Case "Refresh"
  '      CommandArgs.ReturnData = True
  '  End Select
  'End Sub

End Class