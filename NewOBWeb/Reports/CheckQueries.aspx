﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CheckQueries.aspx.vb" Inherits="NewOBWeb.CheckQueries" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link rel="Stylesheet" href="../Styles/NewStyle.css" />
  <link rel="Stylesheet" href="../Styles/FlatDream.css" />
  <style type="text/css">
    .TravelButtons
    {
      margin-left: 20px !important;
    }

    .GroupButtonOverride span.input-group-addon
    {
      width: auto !important;
    }

    .btn-custom
    {
      font-size: 11px;
      font-weight: normal;
      line-height: 1.428571429;
      text-align: center;
      white-space: nowrap;
      /*display: inline-block;
      padding: 3px 6px;
      margin-bottom: 0;
      font-size: 11px;
      font-weight: normal;
      line-height: 1.428571429;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      cursor: pointer;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 4px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      -o-user-select: none;
      user-select: none;*/
    }

    .AdHocHumanResourceID
    {
      float: left;
      width: 160px;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    td.LButtons, th.LButtons
    {
      width: 24px !important;
    }

    .modal-lrg
    {
      width: 90%;
    }

    .modal-med
    {
      width: 50%;
    }

    .CustomRow
    {
      margin-right: -5px;
      margin-left: -5px;
    }

    .ComboDropDown
    {
      z-index: 2001 !important;
    }

    .custom-error-box
    {
      background-color: #FBECEB;
      border-color: #c44;
      border-style: solid;
      border-width: 1px;
      height: 80px !important;
      width: 100%;
      overflow-y: scroll !important;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    
    Using h = Helpers
      
      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Check Queries", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                .Helpers.Button("RunAll", "Run All").PostBackType = Singular.Web.PostBackType.Ajax
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.MessageHolder()
                  .AddClass("HoverMsg")
                End With
              End With
            End With
          End With
        End With
      End With
      
      ''With h.Toolbar
      ''  .Helpers.HTML.Heading2("Check Queries")
        
      ''  .Helpers.Button("RunAll", "Run All").PostBackType = Singular.Web.PostBackType.Ajax
      ''End With
      
      h.MessageHolder()
          
      With h.Div
        With .Helpers.TableFor(Of Singular.CheckQueries.CheckQuery)(Function(c) c.CheckQueryList, False, False)
   
          .FirstRow.AddReadOnlyColumn(Function(c) c.Description)
          
          'Status Image.
          With .FirstRow.AddColumn("")
            With .Helpers.Image(SrcDefined:=Singular.Web.DefinedImageType.BlankPage)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) c.Status = 1)
            End With
            With .Helpers.Image(SrcDefined:=Singular.Web.DefinedImageType.Yes_GreenTick)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) c.Status = 2)
            End With
            With .Helpers.Image(SrcDefined:=Singular.Web.DefinedImageType.No_RedCross)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) c.Status = 3)
            End With
          End With
          
          'Buttons
          With .FirstRow.AddColumn()
            With .Helpers.Button("Run", "Run")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) c.Status = 1)
              .AddBinding(Singular.Web.KnockoutBindingString.click, "RunCheckQuery($data)")
              .AddBinding(Singular.Web.KnockoutBindingString.ButtonArgument, Function(c) c.Description)
              .PostBackType = Singular.Web.PostBackType.None
            End With
            With .Helpers.Button("Download", "View Results")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) c.Status = 3)
              .Image.SrcDefined = Singular.Web.DefinedImageType.Print
              .PostBackType = Singular.Web.PostBackType.Full
              .AddBinding(Singular.Web.KnockoutBindingString.ButtonArgument, Function(c) c.Description)
            End With
          End With
        End With
      End With
            
    End Using
    
  %>
  <script type="text/javascript">

    function RunCheckQuery(obj) {

      if (obj.Status() == 1) {
        Singular.SendROCommand('Run', obj.Description())
      } else {
        ViewModel.CurrentCheckQuery(obj)
      };
    }

  </script>
</asp:Content>
