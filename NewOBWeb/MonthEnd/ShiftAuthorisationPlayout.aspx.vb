﻿Imports OBLib.Shifts.Auth
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ICR.ReadOnly
Imports Singular
Imports System.ComponentModel
Imports OBLib.Maintenance.Company.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Biometrics
Imports Singular.Misc
Imports Singular.Web
Imports System

Public Class ShiftAuthorisationPlayout
  Inherits OBPageBase(Of ShiftAuthorisationPlayoutVM)

End Class

Public Class ShiftAuthorisationPlayoutVM
  Inherits OBViewModel(Of ShiftAuthorisationPlayoutVM)
  Implements ControlInterfaces(Of ShiftAuthorisationPlayoutVM).IPlayoutOpsShift
  Implements ControlInterfaces(Of ShiftAuthorisationPlayoutVM).IPlayoutOpsShiftMCR
  Implements ControlInterfaces(Of ShiftAuthorisationPlayoutVM).IPlayoutOpsShiftSCCR

#Region " Properties "

  <InitialDataOnly>
  Public Property ROContractTypeList As OBLib.Maintenance.HR.ReadOnly.ROContractTypeList

  <InitialDataOnly>
  Public Property ROSystemTeamList As OBLib.TeamManagement.ReadOnly.ROSystemTeamList = New OBLib.TeamManagement.ReadOnly.ROSystemTeamList
  <InitialDataOnly>
  Public Property ROSystemTeamListCriteria As OBLib.TeamManagement.ReadOnly.ROSystemTeamList.Criteria = New OBLib.TeamManagement.ReadOnly.ROSystemTeamList.Criteria
  <InitialDataOnly>
  Public Property ROTeamListManager As Singular.Web.Data.PagedDataManager(Of ShiftAuthorisationPlayoutVM) = New Singular.Web.Data.PagedDataManager(Of ShiftAuthorisationPlayoutVM)(Function(d) Me.ROSystemTeamList,
                                                                                                                                           Function(d) Me.ROSystemTeamListCriteria,
                                                                                                                                           "StartDate", 15)

  Public Property CurrentPlayoutOpsShift As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift Implements ControlInterfaces(Of ShiftAuthorisationPlayoutVM).IPlayoutOpsShift.CurrentPlayoutOpsShift
  Public Property CurrentPlayoutOpsShiftMCR As OBLib.Shifts.PlayoutOperations.MCRShift Implements ControlInterfaces(Of ShiftAuthorisationPlayoutVM).IPlayoutOpsShiftMCR.CurrentPlayoutOpsShiftMCR
  Public Property CurrentPlayoutOpsShiftSCCR As OBLib.Shifts.PlayoutOperations.SCCRShift Implements ControlInterfaces(Of ShiftAuthorisationPlayoutVM).IPlayoutOpsShiftSCCR.CurrentPlayoutOpsShiftSCCR
  Public Property CurrentICRShift As OBLib.Shifts.ICR.ICRShift
  Public Property CurrentStudioSupervisorShift As OBLib.Shifts.Studios.StudioSupervisorShift
  Public Property CurrentGenericShift As OBLib.Shifts.GenericShift

  Public Property ShiftAuth As ShiftAuth
  Public Property ShiftAuthCriteria As OBLib.Shifts.Auth.ShiftAuthList.Criteria

#Region " Biometrics "

  Public ReadOnly Property MissingShiftsCount As Integer
    Get
      If ShiftAuth Is Nothing Then
        Return 0
      End If
      Return ShiftAuth.AccessMissingShiftList.Count
    End Get
  End Property

  Public Property MissingShiftsDisciplines As New List(Of Integer)
  Public Property HumanResourceShiftID As Integer?
  Public Property ROAccessTerminalShiftList As ROAccessTerminalShiftList = New ROAccessTerminalShiftList
  Public Property ROAccessShifFlagstList As ROAccessFlagList = New ROAccessFlagList
  Public Property ROAccessTerminalShiftListCriteria As ROAccessTerminalShiftList.Criteria = New ROAccessTerminalShiftList.Criteria
  Public Property ROAccessTerminalShiftListManager As Singular.Web.Data.PagedDataManager(Of ShiftAuthorisationPlayoutVM) = New Singular.Web.Data.PagedDataManager(Of ShiftAuthorisationPlayoutVM)(Function(d) Me.ROAccessTerminalShiftList, Function(d) Me.ROAccessTerminalShiftListCriteria,
                                                                                                                                                                                              "EventType", 200)
#End Region

#Region " Data "

  '<ClientOnly()>
  'Public ReadOnly Property SupervisorAuth As Boolean
  '  Get
  '    Return Singular.Security.HasAccess("ICR", "Can Authorise HR Shift - Supervisor")
  '  End Get
  'End Property

  '<Display(AutoGenerateField:=False), Key()>
  'Public ReadOnly Property ShowMissingShifts As Boolean
  '  Get
  '    If SupervisorAuth OrElse ManagerAuth Then
  '      Return True
  '    End If
  '    Return False  'default to false
  '  End Get
  'End Property

  'Public Shared DisableMissingShiftsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DisableMissingShifts, "")
  'Public Property DisableMissingShifts As Boolean = False
  <System.ComponentModel.DataAnnotations.Display(Name:="Team", Order:=1)>
  Public Property Team As String = Nothing

  '<ClientOnly()>
  'Public ReadOnly Property ManagerAuth As Boolean
  '  Get
  '    Return Singular.Security.HasAccess("ICR", "Can Authorise HR Shift - Manager")
  '  End Get
  'End Property

  Private Function GetAccessMissingShiftDiscipline() As List(Of Integer)
    Dim newList As New List(Of Integer)

    If ShiftAuth IsNot Nothing Then

      Dim qry = (From data In ShiftAuth.AccessMissingShiftList
                 Group By DisciplineID = data.DisciplineID Into Group, Count()
                 Select DisciplineID)
      For Each item In qry
        newList.Add(item)
      Next
      Return newList

    End If
    Return newList

  End Function

#End Region

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()

    ValidationMode = Singular.Web.ValidationMode.OnLoad
    MessageFadeTime = 4000
    ROContractTypeList = OBLib.CommonData.Lists.ROContractTypeList
    'ROAccessTerminalShiftListCriteria.AccessTerminalGroupID = OBLib.CommonData.Enums.AccessTerminalGroup.Playout_Operations_Randburg

    'Dim mROSystemAreaShiftTypeList As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList = OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList.GetROSystemAreaShiftTypeList(OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID)

    ShiftAuthCriteria = New OBLib.Shifts.Auth.ShiftAuthList.Criteria
    ShiftAuthCriteria.SystemID = Nothing
    ShiftAuthCriteria.StartDate = Now.AddDays(-2)
    ShiftAuthCriteria.EndDate = Now.AddDays(1)

    ClientDataProvider.AddDataSource("ROSystemAreaShiftTypeList", OBLib.CommonData.Lists.ROSystemAreaShiftTypeList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaStatusSelectList", OBLib.CommonData.Lists.ROSystemProductionAreaStatusSelectList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplineList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplineList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplinePositionTypeList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplinePositionTypeList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        If ShiftAuth IsNot Nothing Then
          'Dim mal As New ShiftAuthList
          'mal.Add(ShiftAuth)
          Dim sh As Singular.SaveHelper = ShiftAuth.TrySave(GetType(ShiftAuthList))

          If Not sh.Success Then
            'Error
            AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
          Else
            'Success
            GetShiftAuthList(CommandArgs)
          End If
        End If

      Case "GetShiftAuth"
        GetShiftAuthList(CommandArgs)

      Case "FetchAccessTransactions"
        FetchAccessTransactions(CommandArgs)

      Case "CreateMissingShifts"
        'If Not Singular.Misc.IsNullNothing(TeamID, True) Then
        '  ShiftAuth.AccessMissingShiftList.CheckAllRules()
        '  If ShiftAuth.AccessMissingShiftList.IsValid Then
        '    ShiftAuth.AccessMissingShiftList.CreateHRShifts(TeamID)
        '    GetShiftAuthList(CommandArgs)
        '  Else
        '    AddMessage(Singular.Web.MessageType.Validation, "Missing shifts", "Please fix the errors and try again.")
        '  End If
        'Else
        '  AddMessage(Singular.Web.MessageType.Warning, "Team", "Please select a team to create shifts")
        'End If

    End Select

  End Sub

#End Region

#Region " Methods "

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetReimbursementValues(HRID As Integer, JSon As String, SyncID As Integer, SystemID As Integer) As Singular.Web.Result
    Return New Singular.Web.Result(
      Function()
        Return OBLib.Shifts.Auth.ShiftAuthHumanResourceShiftList.GetReimbursementValues(HRID, JSon, SyncID, SystemID) 'NSWTimesheets.NSWTimesheetsAuthorisationList.GetReimbursementValues(HRID, JSon, SyncID)
      End Function)
  End Function

  <WebCallable(LoggedInOnly:=True, Roles:={"Shifts.Can Access Shift Authorisation Playouts"})>
  Public Function GetUpdatedHRShifts(HumanResourceID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?,
                                     StartDate As Date?, EndDate As Date?, HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim lst As OBLib.Shifts.Auth.ShiftAuthList = OBLib.Shifts.Auth.ShiftAuthList.GetShiftAuthList(OBLib.Security.Settings.CurrentUserID, Nothing,
                                                                                                    SystemID, ProductionAreaID,
                                                                                                    StartDate, EndDate, HumanResourceID, HumanResourceShiftID)
      Return New Singular.Web.Result(True) With {.Data = lst(0)}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True, Roles:={"Shifts.Can Access Shift Authorisation Playouts"})>
  Public Function GetUpdatedHRShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim lst As OBLib.Shifts.Auth.ShiftAuthList = OBLib.Shifts.Auth.ShiftAuthList.GetShiftAuthList(OBLib.Security.Settings.CurrentUserID, Nothing,
                                                                                                    Nothing, Nothing,
                                                                                                    Nothing, Nothing, Nothing, HumanResourceShiftID)
      Dim shift As ShiftAuthHumanResourceShift = lst(0).ShiftAuthHumanResourceList(0).ShiftAuthHumanResourceShiftList(0)
      Return New Singular.Web.Result(True) With {.Data = shift}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True, Roles:={"Shifts.Can Access Shift Authorisation Playouts"})>
  Public Function GetUpdatedHR(HumanResourceID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?,
                              StartDate As Date?, EndDate As Date?) As Singular.Web.Result

    Try
      Dim lst As OBLib.Shifts.Auth.ShiftAuthList = OBLib.Shifts.Auth.ShiftAuthList.GetShiftAuthList(OBLib.Security.Settings.CurrentUserID, Nothing,
                                                                                                    SystemID, ProductionAreaID,
                                                                                                    StartDate, EndDate, HumanResourceID, Nothing)
      Dim humanResource As ShiftAuthHumanResource = lst(0).ShiftAuthHumanResourceList(0)
      Return New Singular.Web.Result(True) With {.Data = humanResource}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  Private Sub GetShiftAuthList(CommandArgs As Singular.Web.CommandArgs)
    ShiftAuth = ShiftAuthList.GetShiftAuthList(ShiftAuthCriteria).FirstOrDefault
    MissingShiftsDisciplines = GetAccessMissingShiftDiscipline()
  End Sub

  Private Sub GetShiftAuth(CommandArgs As Singular.Web.CommandArgs)
    ShiftAuthCriteria.HumanResourceID = CommandArgs.ClientArgs.HumanResourceID
    ShiftAuth = ShiftAuthList.GetShiftAuthList(ShiftAuthCriteria).FirstOrDefault()

  End Sub

  Private Sub FetchAccessTransactions(CommandArgs As Singular.Web.CommandArgs)

    ROAccessTerminalShiftList = ROAccessTerminalShiftList.GetROAccessTerminalShiftList(OBLib.CommonData.Enums.AccessTerminalGroup.ICR, CommandArgs.ClientArgs.HumanResourceShiftID)
    'CommandArgs.ReturnData = RoomList

  End Sub

  <WebCallable(LoggedInOnly:=True, Roles:={"Shifts.Can Access Shift Authorisation Playouts"})>
  Public Function SaveHRAuthList(HRAuth As ShiftAuthHumanResource) As Singular.Web.Result

    Dim sh As Singular.SaveHelper = HRAuth.TrySave(GetType(ShiftAuthHumanResourceList))
    Return New Singular.Web.Result With {.Success = sh.Success, .ErrorText = sh.ErrorText, .Data = sh.SavedObject}

  End Function

#End Region

End Class