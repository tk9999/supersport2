﻿var CurrentICRShiftControl = new ICRShiftModal({
  afterSaveSuccess: function (ctrl, response) {
    //HRShiftAuthorisationPage.GetUpdatedHRShift(response.Data)
    HRShiftAuthorisationPage.GetUpdatedHR(response.Data)
  }
});
var CurrentPlayoutOpsShiftControl = new PlayoutOpsShiftModal({
  afterSaveSuccess: function (ctrl, response) {
    //HRShiftAuthorisationPage.GetUpdatedHRShift(response.Data)
    HRShiftAuthorisationPage.GetUpdatedHR(response.Data)
  }
});
var CurrentPlayoutOpsShiftControlMCR = new PlayoutOpsShiftModalMCR({
  onSaveSuccess: function (response) {
    HRShiftAuthorisationPage.GetUpdatedHR(response.Data)
  }
});
var CurrentStageHandShiftControl = new StageHandShiftModal({
  afterSaveSuccess: function (ctrl, response) {
    //HRShiftAuthorisationPage.GetUpdatedHRShift(response.Data)
    HRShiftAuthorisationPage.GetUpdatedHR(response.Data)
  }
});
var CurrentStudioSupervisorShiftControl = new StudioSupervisorShiftModal({
  afterSaveSuccess: function (ctrl, response) {
    //HRShiftAuthorisationPage.GetUpdatedHRShift(response.Data)
    HRShiftAuthorisationPage.GetUpdatedHR(response.Data)
  }
});

HRShiftAuthorisationPage = {
  ShowCriteria: function () {
    $("#CriteriaModal").modal()
  },
  criteriaButtonCss: function (crit) {
    if (crit.IsValid()) {
      return "btn btn-sm btn-primary"
    }
    else {
      return "btn btn-sm btn-danger animated rubberBand go infinite"
    }
  },
  GetShiftAuth: function () {
    Singular.SendCommand("GetShiftAuth", {

    },
      function (response) {
        $("#CriteriaModal").modal('hide')
      })
  },

  RefreshShiftAuth: function (ShiftAuth) {
    if (ShiftAuth.IsProcessing() == false) {
      ShiftAuth.IsProcessing(true)
      Singular.GetDataStateless("OBLib.Shifts.Auth.ShiftAuthList, OBLib", { HumanResourceID: ShiftAuth.HumanResourceID(), StartDate: ViewModel.ShiftAuthCriteria().StartDate(), EndDate: ViewModel.ShiftAuthCriteria().EndDate(), SystemID: ViewModel.ShiftAuthCriteria().SystemID() },
        function (response) {
          if (response.Success) {
            if (response.Data) {
              var item = ViewModel.ShiftAuth().ShiftAuthHumanResourceList().Find("HumanResourceID", ShiftAuth.HumanResourceID())
              var newItem = response.Data[0].ShiftAuthHumanResourceList[0]
              newItem.IsExpanded = true
              KOFormatter.Deserialise(newItem, item)
              OBMisc.Notifications.GritterInfo("Refreshed Successfully", "", 250)
              //ViewModel.ShiftAuth().ShiftAuthHumanResourceList().Find("HumanResourceID", ShiftAuth.HumanResourceID()).Set(response.Data[0]);
            }
          }
          ShiftAuth.IsProcessing(false)
        })
    }
  },

  FetchAccessTransactions: function (ShiftAuthHumanResourceShift) {

    $("#ROAccesTransactionsModal").off('shown.bs.modal')
    $("#ROAccesTransactionsModal").on('shown.bs.modal', function () {
      var StartTime = new Date(ShiftAuthHumanResourceShift.StartDateTime())
      var EndTime = new Date(ShiftAuthHumanResourceShift.EndDateTime())
      ViewModel.ROAccessTerminalShiftList([])
      ViewModel.ROAccessShifFlagstList([])
      ViewModel.ROAccessTerminalShiftListCriteria().HumanResourceShiftID(ShiftAuthHumanResourceShift.HumanResourceShiftID())
      ViewModel.ROAccessTerminalShiftListCriteria().AccessTerminalGroupID(ShiftAuthHumanResourceShift.AccessTerminalGroupID())
      ViewModel.ROAccessShifFlagstList(ShiftAuthHumanResourceShift.AccessFlagList())
      ViewModel.ROAccessTerminalShiftListManager().Refresh()
    })

    $("#ROAccesTransactionsModal").modal()

  },
  ShowMissingShifts: function () {

    $("#MissingShiftModal").on('shown.bs.modal', function () {
      $("#MissingShiftModal").draggable({ handle: ".modal-header" })
    })

    $("#MissingShiftModal").modal()

    //{
    //  title: "Missing Shift - " + ViewModel.MissingShiftsCount(),
    //  closeText: "Close",
    //  height: 700,
    //  width: "80%",
    //  resizeable: true,
    //  open: function (event, ui) {


    //  },
    //  beforeClose: function (event, ui) {

    //  }
    //}

  },
  BiometricslogsCss: function (obj) {

    if (obj.AccessFlagID() == 64) {
      return "btn btn-xs btn-danger"
    } else if (obj.AccessFlagID() > 0) {
      return "Pending"
    }
    else {
      return "btn btn-xs btn-success"
    }
  },
  BiometricslogsHtml: function (obj) {

    if (obj.AccessFlagID() > 0) {
      return "flags"
    }
    else {
      return "logs"
    }
  },
  SupervisorPending: function (obj) {
    obj.SupervisorAuthInd(null);
  },
  SupervisorAuthorised: function (obj) {
    obj.SupervisorAuthInd(true);
  },
  SupervisorRejected: function (obj) {
    obj.SupervisorAuthInd(false);
  },
  SupervisorPendingCss: function (obj) {
    if (obj.SupervisorAuthInd() == null) { return "btn btn-xs btn-warning" } else { return "btn btn-xs btn-default" }
  },
  SupervisorAuthorisedCss: function (obj) {
    if (obj.SupervisorAuthInd() == true) { return "btn btn-xs btn-success" } else { return "btn btn-xs btn-default" }
  },
  SupervisorRejectedCss: function (obj) {
    if (obj.SupervisorAuthInd() == false) { return "btn btn-xs btn-danger" } else { return "btn btn-xs btn-default" }
  },
  ManagerPending: function (obj) {
    obj.ManagerAuthInd(null);
  },
  ManagerAuthorised: function (obj) {
    obj.ManagerAuthInd(true);
  },
  ManagerRejected: function (obj) {
    obj.ManagerAuthInd(false);
  },
  ManagerPendingCss: function (obj) {
    if (obj.ManagerAuthInd() == null) { return "btn btn-xs btn-warning" } else { return "btn btn-xs btn-default" }
  },
  ManagerAuthorisedCss: function (obj) {
    if (obj.ManagerAuthInd() == true) { return "btn btn-xs btn-success" } else { return "btn btn-xs btn-default" }
  },
  ManagerRejectedCss: function (obj) {
    if (obj.ManagerAuthInd() == false) { return "btn btn-xs btn-danger" } else { return "btn btn-xs btn-default" }
  },
  PendingHtml: function () {
    return "Pnd.";
  },
  AuthorisedHtml: function () {
    return "Auth.";
  },
  RejectedHtml: function () {
    return "Rej.";
  },
  StaffAcknowledgePending: function (obj) {
    if (obj.StaffAcknowledgeInd() == null) {
      obj.StaffAcknowledgeInd(true)
    } else if (obj.StaffAcknowledgeInd()) {
      obj.StaffAcknowledgeInd(false)
    } else if (obj.StaffAcknowledgeInd() == false) {
      obj.StaffAcknowledgeInd(null)
    }
  },
  StaffAcknowledgePendingCss: function (obj) {
    if (obj.StaffAcknowledgeInd() == null) {
      return "btn btn-xs btn-warning"
    }
    else if (obj.StaffAcknowledgeInd() == true) {
      return "btn btn-xs btn-success"
    }
    else if (obj.StaffAcknowledgeInd() == false) {
      return "btn btn-xs btn-danger"
    }
  },
  StaffAcknowledgePendingHtml: function (obj) {
    if (obj.StaffAcknowledgeInd() == null) {
      return "Pnd."
    }
    else if (obj.StaffAcknowledgeInd() == true) {
      return "Ackn."
    }
    else if (obj.StaffAcknowledgeInd() == false) {
      return "Disp."
    }
  },
  MissingShiftRejected: function (obj) {
    obj.AccessShiftOptionInd(false);
  },
  MissingShiftValid: function (obj) {
    obj.AccessShiftOptionInd(true);
  },
  MissingShiftValidCss: function (obj) {
    if (obj.AccessShiftOptionInd() == true) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  MissingShiftRejectedCss: function (obj) {
    if (obj.AccessShiftOptionInd() == false) {
      return "btn btn-xs btn-danger"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  MissingShiftRejectedhtml: function () {
    return "Ignore";
  },
  MissingShiftValidhtml: function () {
    return "Valid";
  },
  CreateSelectedMissingShifts: function () {
    Singular.SendCommand('CreateMissingShifts', {}, function (args) {

      $("#MissingShiftModal").dialog('destroy');


    });
  },
  EditShift: function (ShiftAuthHumanResourceShift) {
    if (ShiftAuthHumanResourceShift.SystemID() == 1) {
      //Production Services (Studio Supervisors)

      //need ResourceBookingTypeID in order to determine if stagehand or studio supervisor shift

      //OBMisc.Shifts.getStudioSupervisorShift(ShiftAuthHumanResourceShift.HumanResourceShiftID(),
      //  function (response) {
      //    CurrentICRShiftControl.setAndShowShiftModal(response.Data.Shift)
      //  },
      //  function (response) {
      //    OBMisc.Notifications.GritterError("Fetch Shift Failed", response.ErrorText, 3000)
      //  })

      //OBMisc.Shifts.getStageHandShift(ShiftAuthHumanResourceShift.HumanResourceShiftID(),
      //  function (response) {
      //    CurrentICRShiftControl.setAndShowShiftModal(response.Data.Shift)
      //  },
      //  function (response) {
      //    OBMisc.Notifications.GritterError("Fetch Shift Failed", response.ErrorText, 3000)
      //  })

    }
    else if (ShiftAuthHumanResourceShift.SystemID() == 4) {
      OBMisc.Shifts.getICRShift(ShiftAuthHumanResourceShift.HumanResourceShiftID(),
        function (response) {
          CurrentICRShiftControl.setAndShowShiftModal(response.Data.Shift)
        },
        function (response) {
          OBMisc.Notifications.GritterError("Fetch Shift Failed", response.ErrorText, 3000)
        })
    }
    else if (ShiftAuthHumanResourceShift.SystemID() == 5) {
      OBMisc.Shifts.getPlayoutShift(ShiftAuthHumanResourceShift.HumanResourceShiftID(),
        function (response) {
          CurrentPlayoutOpsShiftControl.setAndShowShiftModal(response.Data.Shift)
        },
        function (response) {
          OBMisc.Notifications.GritterError("Fetch Shift Failed", response.ErrorText, 3000)
        })
    }
    else if (ShiftAuthHumanResourceShift.SystemID() == 8) {
      OBMisc.Shifts.getProgrammingShift(ShiftAuthHumanResourceShift.HumanResourceShiftID(),
        function (response) {
          CurrentProgrammingShiftControl.setAndShowShiftModal(response.Data.Shift)
        },
        function (response) {
          OBMisc.Notifications.GritterError("Fetch Shift Failed", response.ErrorText, 3000)
        })
    }
  },
  GetUpdatedHRShift: function (shift) {
    //HumanResourceID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, StartDate As Date?, EndDate As Date?
    ViewModel.CallServerMethod("GetUpdatedHRShift", {
      HumanResourceShiftID: shift.HumanResourceShiftID
    }, function (response) {
      if (response.Success) {
        var HRToUpdate = ViewModel.ShiftAuth().ShiftAuthHumanResourceList().Find("HumanResourceID", shift.HumanResourceID)
        var ShiftToUpdate = HRToUpdate.ShiftAuthHumanResourceShiftList().Find("HumanResourceShiftID", shift.HumanResourceShiftID)
        var updatedShift = response.Data
        ShiftToUpdate.SystemID(updatedShift.SystemID)
        ShiftToUpdate.ProductionAreaID(updatedShift.ProductionAreaID)
        ShiftToUpdate.ShiftTypeID(updatedShift.ShiftTypeID)
        ShiftToUpdate.ScheduleDate(updatedShift.ScheduleDate)
        ShiftToUpdate.StartDateTime(updatedShift.StartDateTime)
        ShiftToUpdate.EndDateTime(updatedShift.EndDateTime)
        ShiftToUpdate.ExtraShiftInd(updatedShift.ExtraShiftInd)
        ShiftToUpdate.ShiftDuration(updatedShift.ShiftDuration)
        ShiftToUpdate.SystemTeamID(updatedShift.SystemTeamID)
        ShiftToUpdate.AccessFlagID(updatedShift.AccessFlagID)
        ShiftToUpdate.AccessTerminalGroupID(updatedShift.AccessTerminalGroupID)
        ShiftToUpdate.AllowanceAmount(updatedShift.AllowanceAmount)
      }
      else {
        OBMisc.Notifications.GritterError("Error Updating Shifts", response.ErrorText, 3000)
      }
    })
  },
  GetUpdatedHR: function (shift) {
    //HumanResourceID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, StartDate As Date?, EndDate As Date?
    ViewModel.CallServerMethod("GetUpdatedHR", {
      HumanResourceID: shift.HumanResourceID,
      StartDate: new Date(ViewModel.ShiftAuthCriteria().StartDate()).format('dd MMM yyyy'),
      EndDate: new Date(ViewModel.ShiftAuthCriteria().EndDate()).format('dd MMM yyyy'),
      SystemID: ViewModel.ShiftAuthCriteria().SystemID(),
      ProductionAreaID: ViewModel.ShiftAuthCriteria().ProductionAreaID()
    }, function (response) {
      if (response.Success) {
        var updatedHR = response.Data
        var HRToUpdate = ViewModel.ShiftAuth().ShiftAuthHumanResourceList().Find("HumanResourceID", shift.HumanResourceID)
        //date range stats
        HRToUpdate.NoOfShifts(updatedHR.NoOfShifts)
        HRToUpdate.TotalShiftHours(updatedHR.TotalShiftHours)
        HRToUpdate.ExtraShiftHours(updatedHR.ExtraShiftHours)
        HRToUpdate.PublicHolidayHours(updatedHR.PublicHolidayHours)
        HRToUpdate.WeekendHours(updatedHR.WeekendHours)
        HRToUpdate.AuthorisedNoOfMeals(updatedHR.AuthorisedNoOfMeals)
        HRToUpdate.AuthorisedReimbursementAmount(updatedHR.AuthorisedReimbursementAmount)
        HRToUpdate.AccessFlagCount(updatedHR.AccessFlagCount)
        HRToUpdate.AllowanceAmount(updatedHR.AllowanceAmount)
        //shifts
        HRToUpdate.ShiftAuthHumanResourceShiftList().Iterate(function (hrShift, indx) {
          var updatedShift = response.Data.ShiftAuthHumanResourceShiftList.Find("HumanResourceShiftID", hrShift.HumanResourceShiftID())
          if (updatedShift) {
            hrShift.SystemID(updatedShift.SystemID)
            hrShift.ProductionAreaID(updatedShift.ProductionAreaID)
            hrShift.ShiftTypeID(updatedShift.ShiftTypeID)
            hrShift.ScheduleDate(updatedShift.ScheduleDate)
            hrShift.StartDateTime(updatedShift.StartDateTime)
            hrShift.EndDateTime(updatedShift.EndDateTime)
            hrShift.ExtraShiftInd(updatedShift.ExtraShiftInd)
            hrShift.ShiftDuration(updatedShift.ShiftDuration)
            hrShift.SystemTeamID(updatedShift.SystemTeamID)
            hrShift.AccessFlagID(updatedShift.AccessFlagID)
            hrShift.AccessTerminalGroupID(updatedShift.AccessTerminalGroupID)
            hrShift.AllowanceAmount(updatedShift.AllowanceAmount)
          }
        })
      }
      else {
        OBMisc.Notifications.GritterError("Error Updating Shifts", response.ErrorText, 3000)
      }
    })
  },
  contractTypeClicked: function (roct) {
    ViewModel.ShiftAuthCriteria().ContractTypeIDs([])
    roct.IsSelected(!roct.IsSelected())
    ViewModel.ROContractTypeList().Iterate(function (ct, ctIndx) {
      if (ct.IsSelected()) {
        ViewModel.ShiftAuthCriteria().ContractTypeIDs().push(ct.ContractTypeID())
      }
    })
  },
  AuthoriseAll: function (obj) {
    //Authorises all shifts for both supervisor and manager level/column (for ICR managers only)
    obj.ShiftAuthHumanResourceShiftList().forEach(
      function (CurrentShift, index) {
        CurrentShift.SupervisorAuthInd(true);
        CurrentShift.ManagerAuthInd(true);

      })
  },
  AuthoriseByManager: function (obj) {
    //Auth all shift in the Manager column
    obj.ShiftAuthHumanResourceShiftList().forEach(
      function (CurrentShift, index) {
        CurrentShift.ManagerAuthInd(true);

      })
  },
  AuthoriseBySupervisor: function (obj) {
    //Auth all shifts in the Supervisor column
    obj.ShiftAuthHumanResourceShiftList().forEach(
      function (CurrentShift, index) {
        CurrentShift.SupervisorAuthInd(true);

      })
  },
  async saveShiftAuthHumanResource(humanResource) {
    try {
      let result = await ViewModel.CallServerMethodPromise("SaveShiftAuthHumanResource", { ShiftAuthHumanResource: humanResource.Serialise() });
      KOFormatter.Deserialise(result, humanResource);
      OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250);
    } catch (err) {
      OBMisc.Notifications.GritterError("Save Failed", err, 1000);
    }
  }
}

HumanResourceShiftHelper = {
  Methods: {
    //FindTeam: function (element) {
    //  ROTeams.Init();
    //  ROTeams.Manager().SingleSelect(true);
    //  ROTeams.Manager().MultiSelect(false);
    //  ROTeams.SetOptions({
    //    AfterRowSelected: function (ROTeam) {
    //      ViewModel.TeamID(ROTeam.TeamID());
    //      ViewModel.Team(ROTeam.TeamName());
    //    },
    //    AfterRowDeslected: function (DeSelectedItem) {
    //      ViewModel.TeamID(null);
    //      ViewModel.Team("");
    //    }
    //  });
    //  $('#ROTeamListModal').dialog({
    //    title: 'Select Team',
    //    height: '500px',
    //    width: '900px',
    //    position: {
    //      my: 'left top',
    //      at: 'right top',
    //      of: $(element)
    //    },
    //    open: function (event, ui) {
    //      ROTeams.RefreshList();
    //    }
    //  });
    //},
    GetAllowedDisciplines: function (List, Item) {
      var AllowedDisciplines = [];
      List.Iterate(function (Discipline, Index) {
        if (Discipline.SystemID == ViewModel.CurrentSystemID() && Discipline.ProductionAreaID == ViewModel.CurrentProductionAreaID()) {
          AllowedDisciplines.push(Discipline)
        } else {
          if (Item.DisciplineID() == Discipline.DisciplineID) {
            AllowedDisciplines.push(Discipline)
          }
        }
      })
      return AllowedDisciplines;

    }
  }
};

ShiftAuthHumanResourceBO = {
  refreshButtonCSS: function (ShiftAuthHumanResource) {

    if (ShiftAuthHumanResource.IsProcessing()) {
      return "fa-refresh fa-spin"
    }
    else {
      return "fa-refresh"
    }
  }
};