﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ShiftAuthorisation.aspx.vb" Inherits="NewOBWeb.ShiftAuthorisation" %>

<%@ Import Namespace="OBLib.Biometrics" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.Shifts.Auth" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    .table > thead > tr > th.thFlags {
      background-color: #ff392e;
      vertical-align: bottom;
      border-bottom: 0px solid #dddddd;
      color: white;
      font-size: 11px;
    }

    .Pending {
      background-color: #ed9c28;
      font-weight: bold;
    }

    .Auth {
      background-color: #47a447;
    }

    td.CellWait {
      background-image: url('../Singular/Images/LoadingSmall.gif');
      background-repeat: no-repeat;
      background-position: 3px 5px;
      color: #bbb;
    }

    .body {
      background-color: #F0F0F0;
    }

    .SelectRow {
      background-color: #CFCFC4 !important;
    }

    .UnSelectRow {
    }

    .modal-dg {
      display: none;
    }

    /*.table > tbody {
      overflow-y: scroll !important;
      max-height: 450px !important;
      min-height: 0px;
    }*/

    .Flags {
      font-size: 10px !important;
      line-height: 1.101 !important;
      color: #ff392e !important;
      vertical-align: middle !important;
    }

    .FlagsFont {
      color: red !important;
    }

    .InformationFlagsFont {
      color: blue !important;
    }

    .NoFlagsFont {
    }

    /*.table-fixed thead {
      width: 97%;
    }*/

    /*.table-fixed tbody {
      height: 450px;
      overflow-y: auto;
      width: 97%;
    }*/

    /*.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
      display: block;
    }

      .table-fixed tbody td, .table-fixed thead > tr > th {
        float: left;
        border-bottom-width: 0;
      }*/

    div.ShiftAuthHumanResourceList table tr table > tbody > tr td:first-child > button {
      color: white;
    }

    /*#ROTeamListModal {
      height: 500px !important;
    }*/

    /*.ValidationPopup {
      min-height: 120px;
    }*/
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js"></script>
  <script type="text/javascript" src="../Scripts/Tools/SoberControls.js"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceHelpers.js"></script>
  <script type="text/javascript" src="ShiftAuthorisationPage.js?v=2"></script>
  <script type="text/javascript">

    // var ROTeams = new ROTeamListManager();

    function GetFlagCss() {
      return 'thFlags'
    }
    function GetStartEnd(AccessLog) {
      if (AccessLog.StartEndInd()) {
        return 'SelectRow'
      }
      return 'UnSelectRow'
    }

    function GetlogsCss() {
      return 'scoll-y'
    }

    function AccessFlagIDCSS(HRShiftAuth) {

      if (HRShiftAuth.AccessFlagCount() > 0) {
        return "FlagsFont";
      }
      else {
        return "NoFlagsFont";
      }
    }

    function GetAllowedDisciplines(List, Item) {
      var AllowedDisciplines = [];
      List.Iterate(function (Discipline, Index) {
        if (Discipline.SystemID == ViewModel.CurrentSystemID() && Discipline.ProductionAreaID == ViewModel.CurrentProductionAreaID()) {
          AllowedDisciplines.push(Discipline)
        } else {
          if (Item.DisciplineID() == Discipline.DisciplineID) {
            AllowedDisciplines.push(Discipline)
          }
        }
      })
      return AllowedDisciplines;
    };

    function CheckSupervisorRejectedReason(Value, Rule, RuleArgs) {
      var SupervisorAuthInd = RuleArgs.Object.SupervisorAuthInd();
      if (!RuleArgs.IgnoreResults) {
        if (RuleArgs.Object.SupervisorAuthInd() == false && RuleArgs.Object.SupervisorRejectedReason() == '') {
          RuleArgs.AddError("Rejected Reason is required");
          return;
        }
        if (RuleArgs.Object.SupervisorAuthInd() == true && RuleArgs.Object.SupervisorRejectedReason() == '' && RuleArgs.Object.AccessFlagID() > 0) {
          RuleArgs.AddError("Authorising a flagged Shift, Reason is required");
          return;
        }
      }
    };

    function CheckManagerRejectedReason(Value, Rule, RuleArgs) {
      var ManagerAuthInd = RuleArgs.Object.ManagerAuthInd();
      if (!RuleArgs.IgnoreResults) {
        if (RuleArgs.Object.ManagerAuthInd() == false && RuleArgs.Object.ManagerRejectedReason() == '') {
          RuleArgs.AddError("Rejected Reason is required");
          return;
        }
      }
    };

    function CheckShiftType(Value, Rule, RuleArgs) {
      var AccessShiftOptionInd = RuleArgs.Object.AccessShiftOptionInd();
      if (!RuleArgs.IgnoreResults) {
        if (RuleArgs.Object.AccessShiftOptionInd() == true && (RuleArgs.Object.ShiftTypeID() == null || RuleArgs.Object.ShiftTypeID() == 0)) {
          RuleArgs.AddError("Shift Type is required");
          return;
        }
      }
    };

    function CheckDiscipline(Value, Rule, RuleArgs) {
      var AccessShiftOptionInd = RuleArgs.Object.AccessShiftOptionInd();
      if (!RuleArgs.IgnoreResults) {
        if (RuleArgs.Object.AccessShiftOptionInd() == true && (RuleArgs.Object.DisciplineID() == null || RuleArgs.Object.DisciplineID() == 0)) {
          RuleArgs.AddError("Discipline is required");
          return;
        }
      }
    };

    function CheckDates(Value, Rule, RuleArgs) {

      if (RuleArgs.Object.IsDirty && RuleArgs.Object.IsDirty()) {
        if (RuleArgs.Object.AccessShiftOptionInd() == true) {
          if (RuleArgs.Object.ShiftStartDateTime() && RuleArgs.Object.ShiftEndDateTime()) {
            var StartDate = new Date(RuleArgs.Object.ShiftStartDateTime()).getTime();
            var EndDate = new Date(RuleArgs.Object.ShiftEndDateTime()).getTime();

            if (!RuleArgs.IgnoreResults) {
              if (EndDate <= StartDate) {
                RuleArgs.AddError("Start date must be less than end date");
                return;
              }
            }
          }
        }
      }
    };

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Toolbar()
            .Helpers.MessageHolder()
          End With
        End With
      End With

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
          .AddClass("hidden-xs hidden-sm hidden-md")
          With .Helpers.Bootstrap.FlatBlock("Select Criteria", False, False, , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.With(Of OBLib.Shifts.Auth.ShiftAuthList.Criteria)(Function(vm) ViewModel.ShiftAuthCriteria)
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.SystemID).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.ProductionAreaID).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.StartDate).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.EndDate).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.Primary,
                             , BootstrapEnums.ButtonSize.Small, ,
                           "fa-download", , PostBackType.Ajax, "HRShiftAuthorisationPage.GetShiftAuth()")
                      '.Button.AddClass("btn-block")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.ShiftAuthCriteria().IsValid()")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
          'With .Helpers.With(Of ShiftAuth)(Function(vm) ViewModel.ShiftAuth)
          With .Helpers.Bootstrap.FlatBlock("Shifts", True, False)
            With .AboveContentTag
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.Button(, "Select Criteria", BootstrapEnums.Style.Primary, ,
                                               BootstrapEnums.ButtonSize.Small, , "fa-filter", ,
                                               PostBackType.None, "HRShiftAuthorisationPage.ShowCriteria()")
                  '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                  .Button.AddClass("hidden-xl hidden-lg")
                End With
                With .Helpers.With(Of ShiftAuth)(Function(vm) ViewModel.ShiftAuth)
                  With .Helpers.Bootstrap.Button("Save", "Save", BootstrapEnums.Style.Success, ,
                               BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                               PostBackType.Ajax, )
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                  End With
                  With .Helpers.Bootstrap.Button("showMissingShifts", "View Missing Shifts", BootstrapEnums.Style.Danger,
                                                   , BootstrapEnums.ButtonSize.Small, , "fa-server", , PostBackType.None)
                    With .Button
                      .AddBinding(KnockoutBindingString.click, "HRShiftAuthorisationPage.ShowMissingShifts()")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.With(Of ShiftAuth)(Function(vm) ViewModel.ShiftAuth)
                With .Helpers.DivC("table-responsive")
                  With .Helpers.Bootstrap.TableFor(Of ShiftAuthHumanResource)(Function(c As ShiftAuth) c.ShiftAuthHumanResourceList, False, False, True, False, True, True, True)
                    With .FirstRow
                      With .AddColumn("")
                        .Style.Width = "50px"
                        With .Helpers.Bootstrap.Button(, "Auth", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       BootstrapEnums.ButtonSize.ExtraSmall, , "fa-check-square-o", , PostBackType.None,
                                                       "HRShiftAuthorisationPage.AuthoriseByManager($data)", )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsICRManager()")
                        End With
                        With .Helpers.Bootstrap.Button(, "Auth", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       BootstrapEnums.ButtonSize.ExtraSmall, , "fa-check-square-o", , PostBackType.None,
                                                       "HRShiftAuthorisationPage.AuthoriseBySupervisor($data)", )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsICRManager() == false")
                        End With
                      End With
                      With .AddColumn("")
                        .Style.Width = "50px"
                        With .Helpers.Bootstrap.Button(, "Auth All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       BootstrapEnums.ButtonSize.ExtraSmall, , "fa-check-square", , PostBackType.None,
                                                       "HRShiftAuthorisationPage.AuthoriseAll($data)", )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsICRManager()")
                        End With
                      End With
                      With .AddColumn("")
                        .Style.Width = "50px"
                        With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                       BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , PostBackType.None,
                                                       "HRShiftAuthorisationPage.saveShiftAuthHumanResource($data)", )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                        End With
                      End With
                      .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.HumanResource).AddClass("bold")
                      '.AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.MRMonth).AddClass("bold")
                      .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.TotalShiftHours)
                      .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.NoOfShifts)
                      .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.ExtraShiftHours)
                      .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.WeekendHours)
                      .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.PublicHolidayHours)
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.AuthorisedNoOfMeals, 50)
                        .CellBindings.Add(Singular.Web.KnockoutBindingString.css, "IsLoading() ? 'CellWait' : ''")
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.AuthorisedReimbursementAmount, 70)
                        .CellBindings.Add(Singular.Web.KnockoutBindingString.css, "IsLoading() ? 'CellWait' : ''")
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.AccessFlagCount)
                        .CellBindings.Add(KnockoutBindingString.css, "AccessFlagIDCSS($data)")
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.AllowanceAmount)
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.SupervisorPendShiftCount)
                        .HeaderStyle("background-color") = "#ed9c28"
                        .HeaderStyle("font-weight") = "700"
                        .HeaderStyle("color") = "#ffffff"
                        'With .FieldDisplay
                        .Style("background-color") = "#ed9c28"
                        .Style("font-weight") = "700"
                        .Style("color") = "#ffffff"
                        'End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.SupervisorAuthShiftCount)
                        .HeaderStyle("background-color") = "#47a447"
                        .HeaderStyle("font-weight") = "700"
                        .HeaderStyle("color") = "#ffffff"
                        'With .FieldDisplay
                        .Style("background-color") = "#47a447"
                        .Style("font-weight") = "700"
                        .Style("color") = "#ffffff"
                        'End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.SupervisorRejectShiftCount)
                        .HeaderStyle("background-color") = "#ff392e"
                        .HeaderStyle("font-weight") = "700"
                        .HeaderStyle("color") = "#ffffff"
                        'With .FieldDisplay
                        .Style("background-color") = "#ff392e"
                        .Style("font-weight") = "700"
                        .Style("color") = "#ffffff"
                        'End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.ManagerPendShiftCount)
                        .HeaderStyle("background-color") = "#ed9c28"
                        .HeaderStyle("font-weight") = "700"
                        .HeaderStyle("color") = "#ffffff"
                        'With .FieldDisplay
                        .Style("background-color") = "#ed9c28"
                        .Style("font-weight") = "700"
                        .Style("color") = "#ffffff"
                        'End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.ManagerAuthShiftCount)
                        .HeaderStyle("background-color") = "#47a447"
                        .HeaderStyle("font-weight") = "700"
                        .HeaderStyle("color") = "#ffffff"
                        'With .FieldDisplay
                        .Style("background-color") = "#47a447"
                        .Style("font-weight") = "700"
                        .Style("color") = "#ffffff"
                        'End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResource) c.ManagerRejectShiftCount)
                        .HeaderStyle("background-color") = "#ff392e"
                        .HeaderStyle("font-weight") = "700"
                        .HeaderStyle("color") = "#ffffff"
                        'With .FieldDisplay
                        .Style("background-color") = "#ff392e"
                        .Style("font-weight") = "700"
                        .Style("color") = "#ffffff"
                        'End With
                      End With
                    End With
                    With .AddChildTable(Of ShiftAuthHumanResourceShift)(Function(s As ShiftAuthHumanResource) s.ShiftAuthHumanResourceShiftList,
                                                                        False, False, True, False, True, True, True)
                      '.Style.Width = "800px"
                      With .FirstRow

                        With .AddColumn("")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                         BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None,
                                                         "HRShiftAuthorisationPage.EditShift($data)", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.SystemID() != 8")
                          End With
                        End With
                        With .AddColumn("Bio. Logs")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "",
                                                         BootstrapEnums.ButtonSize.ExtraSmall, , , , PostBackType.None,
                                                         "HRShiftAuthorisationPage.FetchAccessTransactions($data)")
                            .Button.AddBinding(KnockoutBindingString.css, "HRShiftAuthorisationPage.BiometricslogsCss($data)")
                            .Button.AddBinding(KnockoutBindingString.html, "HRShiftAuthorisationPage.BiometricslogsHtml($data)")
                          End With
                        End With
                        '.AddReadOnlyColumn(Function(e As ShiftAuthHumanResourceShift) e.MRMonth)
                        With .AddReadOnlyColumn(Function(e As ShiftAuthHumanResourceShift) e.ShiftDate)
                          .FieldDisplay.Style.Width = "60px"
                          '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(e) False)
                        End With
                        With .AddReadOnlyColumn(Function(e As ShiftAuthHumanResourceShift) e.ShiftType)
                          .FieldDisplay.Style.Width = "50px"
                        End With
                        'With .AddColumn(Function(e As ShiftAuthHumanResourceShift) e.StartDateTime)
                        '  .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(e) False)
                        'End With
                        'With .AddColumn(Function(e As ShiftAuthHumanResourceShift) e.EndDateTime)
                        '  .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(e) False)
                        'End With
                        .AddReadOnlyColumn(Function(e As ShiftAuthHumanResourceShift) e.ShiftDuration)
                        With .AddColumn("Staff Ackn.")
                          .Style.Width = "50px"
                          With .Helpers.BootstrapStateButton("",
                                                             "HRShiftAuthorisationPage.StaffAcknowledgePending($data)",
                                                             "HRShiftAuthorisationPage.StaffAcknowledgePendingCss($data)",
                                                             "HRShiftAuthorisationPage.StaffAcknowledgePendingHtml($data)",
                                                             False)
                          End With
                        End With
                        'With .AddColumn(Function(e As ShiftAuthHumanResourceShift) e.StaffDisputeReason)
                        'End With
                        With .AddReadOnlyColumn(Function(c As ShiftAuthHumanResourceShift) c.AllowanceAmount)
                        End With
                        With .AddColumn("Sup. Auth.")
                          '.Style.Width = "200px"
                          With .Helpers.DivC("btn-group")
                            .Style.Width = "120px"
                            With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.SupervisorPending($data)", "HRShiftAuthorisationPage.SupervisorPendingCss($data)", "HRShiftAuthorisationPage.PendingHtml()", False)
                            End With
                            With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.SupervisorAuthorised($data)", "HRShiftAuthorisationPage.SupervisorAuthorisedCss($data)", "HRShiftAuthorisationPage.AuthorisedHtml()", False)
                            End With
                            With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.SupervisorRejected($data)", "HRShiftAuthorisationPage.SupervisorRejectedCss($data)", "HRShiftAuthorisationPage.RejectedHtml()", False)
                            End With
                          End With
                        End With
                        With .AddColumn(Function(e As ShiftAuthHumanResourceShift) e.SupervisorRejectedReason)
                          .Editor.Style.Width = "120px"
                        End With
                        With .AddColumn("Man. Auth.")
                          '.Style.Width = "200px"
                          With .Helpers.DivC("btn-group")
                            .Style.Width = "120px"
                            With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.ManagerPending($data)", "HRShiftAuthorisationPage.ManagerPendingCss($data)", "HRShiftAuthorisationPage.PendingHtml()", False)
                            End With
                            With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.ManagerAuthorised($data)", "HRShiftAuthorisationPage.ManagerAuthorisedCss($data)", "HRShiftAuthorisationPage.AuthorisedHtml()", False)
                            End With
                            With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.ManagerRejected($data)", "HRShiftAuthorisationPage.ManagerRejectedCss($data)", "HRShiftAuthorisationPage.RejectedHtml()", False)
                            End With
                          End With
                        End With
                        With .AddColumn(Function(e As ShiftAuthHumanResourceShift) e.ManagerRejectedReason)
                          .Editor.Style.Width = "120px"
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          'End With
        End With
      End With

      h.Control(New NewOBWeb.Controls.EditICRShiftModal(Of NewOBWeb.ShiftAuthorisationVM)("CurrentICRShiftModal", "CurrentICRShiftControl"))
      'h.Control(New NewOBWeb.Controls.EditStudioSupervisorShiftModal(Of NewOBWeb.ShiftAuthorisationVM)("CurrentStudioSupervisorShiftModal", "CurrentStudioSupervisorShiftControl"))

      With h.Bootstrap.Dialog("ROAccesTransactionsModal", "Access Logs", , "modal-md", Singular.Web.BootstrapEnums.Style.Danger, , "fa-binoculars", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 3)
              .AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.ROAccessShifFlagstList.Count <> 0)
              With .Helpers.Bootstrap.FlatBlock("Legend", , , , )
                With .ContentTag
                  With .Helpers.Bootstrap.TableFor(Of ROAccessFlag)(Function(c) ViewModel.ROAccessShifFlagstList, False, False)
                    With .FirstRow
                      .AddClass("Flags")
                      '.Style.Height = 20
                      With .AddReadOnlyColumn(Function(c) c.FlagReason)
                        .HeaderBindings.Add(KnockoutBindingString.css, "GetFlagCss()")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 8, 9, 9)
              With .Helpers.Bootstrap.FlatBlock("Access Logs", , , , )
                With .ContentTag
                  With .Helpers.Bootstrap.PagedGridFor(Of ROAccessTerminalShift)(Function(d) ViewModel.ROAccessTerminalShiftListManager,
                                                               Function(d) ViewModel.ROAccessTerminalShiftList,
                                                               False, False, False, False, True, True, True, , BootstrapEnums.PagerPosition.None)
                    .AddClass("table-fixed")
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .FirstRow.AddBinding(KnockoutBindingString.css, "GetStartEnd($data)")
                    With .FirstRow
                      With .AddReadOnlyColumn(Function(c) c.AccessTime)
                        '.AddClass("col-xs-2")
                      End With
                      With .AddReadOnlyColumn(Function(d) d.Terminal)
                        '.AddClass("col-xs-4")
                      End With
                      With .AddReadOnlyColumn(Function(d) d.StartEndInd)
                        '.AddClass("col-xs-3")
                      End With
                      With .AddReadOnlyColumn(Function(d) d.TimedInd)
                        '.AddClass("col-xs-3")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With


      With h.Bootstrap.Dialog("CriteriaModal", "Select Criteria", , "modal-xs", Singular.Web.BootstrapEnums.Style.Danger, , "fa-filter", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Select Criteria", False, True, , )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Shifts.Auth.ShiftAuthList.Criteria)(Function(vm) ViewModel.ShiftAuthCriteria)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.ProductionAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.StartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Shifts.Auth.ShiftAuthList.Criteria) p.EndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Auth.ShiftAuthList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.Primary,
                               , BootstrapEnums.ButtonSize.Small, ,
                               "fa-download", , PostBackType.Ajax, "HRShiftAuthorisationPage.GetShiftAuth()")
                          '.Button.AddClass("btn-block")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.ShiftAuthCriteria().IsValid()")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With


      With h.Bootstrap.Dialog("MissingShiftModal", "Missing Shifts", , "modal-md", Singular.Web.BootstrapEnums.Style.Danger, , "fa-exclamation-triangle", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.MessageHolder()
            End With
            With .Helpers.Bootstrap.Button("CreateMissingShifts", "Save / Create Shifts", BootstrapEnums.Style.Primary,
                   , BootstrapEnums.ButtonSize.Small, , "fa-save", , PostBackType.None)
              With .Button
                .AddBinding(KnockoutBindingString.click, "HRShiftAuthorisationPage.CreateSelectedMissingShifts()")
                '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.ShiftAuth.AccessMissingShiftList.IsValid)
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.DivC("table-responsive")
              With .Helpers.Bootstrap.TableFor(Of AccessShift)(Function(c) ViewModel.ShiftAuth.AccessMissingShiftList, False, False)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                With .FirstRow
                  With .AddReadOnlyColumn(Function(d) d.HumanResource)
                    '.AddClass("col-xs-2")
                  End With
                  With .AddColumn(Function(d) d.ShiftStartDateTime, 100)
                    .Style.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddColumn("Start Time")
                    .Style.TextAlign = Singular.Web.TextAlign.center
                    .AddClass("Time")
                    With .Helpers.TimeEditorFor(Function(d) d.ShiftStartDateTime)
                      .Style.Width = 50
                    End With
                  End With
                  With .AddColumn(Function(d) d.ShiftEndDateTime, 100)
                    .Style.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddColumn("End Time")
                    .AddClass("Time")
                    .Style.TextAlign = Singular.Web.TextAlign.center
                    With .Helpers.TimeEditorFor(Function(c) c.ShiftEndDateTime)
                      .Style.Width = 50
                    End With
                  End With
                  With .AddColumn(Function(d) d.TotalShiftHours, 50, "Hours")
                  End With
                  With .AddColumn("Shift")
                    .Style.Width = 150
                    .Style.TextAlign = Singular.Web.TextAlign.center
                    .AddClass("btn-group")
                    With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.MissingShiftValid($data)", "HRShiftAuthorisationPage.MissingShiftValidCss($data)", "HRShiftAuthorisationPage.MissingShiftValidhtml()", False)
                      With .Button
                        .Style.Width = 70
                      End With
                    End With
                    With .Helpers.BootstrapStateButton("", "HRShiftAuthorisationPage.MissingShiftRejected($data)", "HRShiftAuthorisationPage.MissingShiftRejectedCss($data)", "HRShiftAuthorisationPage.MissingShiftRejectedhtml()", False)
                      With .Button
                        .Style.Width = 70
                      End With
                    End With
                  End With
                  With .AddColumn(Function(d) d.ShiftTypeID)
                  End With
                  With .AddColumn(Function(d) d.DisciplineID)
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row

          End With
        End With
      End With

      'With h.DivC("modal-dg")
      '  .Attributes("id") = "ROTeamListModal"
      '  With .Helpers.Div
      '    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '        With .Helpers.EditorFor(Function(d) ViewModel.ROTeamListCriteria.KeyWord)
      '          .AddClass("form-control input-sm")
      '          .Attributes("placeholder") = "Search for Team"
      '          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROTeams.DelayedRefreshList() }")
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Helpers.Div

      '  End With
      'End With

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
