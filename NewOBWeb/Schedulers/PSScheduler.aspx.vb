﻿Imports OBLib.ResourceSchedulers
Imports OBLib.ResourceSchedulers.Base
Imports OBLib.RoomScheduling
Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports Singular.DataAnnotations
Imports OBLib.Scheduling.Rooms

Public Class PSScheduler
  Inherits ResourceSchedulerPageBase(Of ProductionServicesSchedulerVM)

End Class

Public Class ProductionServicesSchedulerVM
  Inherits ResourceSchedulerVMBaseStateless(Of ProductionServicesSchedulerVM)

#Region " Base VM Overrides "

  Public Property ContentServicesDifferenceList As New OBLib.Scheduling.Rooms.ContentServicesDifferenceList
  Public Property ContentServicesDifferenceListCriteria As New OBLib.Scheduling.Rooms.ContentServicesDifferenceList.Criteria

  Public Overrides Sub SetupResourceScheduler()
    ResourceSchedulerID = 1
  End Sub

#End Region

End Class