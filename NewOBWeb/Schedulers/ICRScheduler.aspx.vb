﻿Imports OBLib.ResourceSchedulers
Imports OBLib.ResourceSchedulers.Base
Imports OBLib.RoomScheduling
Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports Singular.DataAnnotations

Public Class ICRScheduler
  Inherits ResourceSchedulerPageBase(Of ICRSchedulerVM)

End Class

Public Class ICRSchedulerVM
  Inherits ResourceSchedulerVMBaseStateless(Of ICRSchedulerVM)

  Public Property AdHocBookingList As OBLib.AdHoc.Old.AdHocBookingList = New OBLib.AdHoc.Old.AdHocBookingList
  Public Property ROAdHocBookingList As OBLib.AdHoc.ReadOnly.ROAdHocBookingList = New OBLib.AdHoc.ReadOnly.ROAdHocBookingList()
  Public Property ROAdHocBookingListCriteria As OBLib.AdHoc.ReadOnly.ROAdHocBookingList.Criteria = New OBLib.AdHoc.ReadOnly.ROAdHocBookingList.Criteria
  Public Property ROAdHocBookingListPagingManager As Singular.Web.Data.PagedDataManager(Of ICRSchedulerVM) = New Singular.Web.Data.PagedDataManager(Of ICRSchedulerVM)(Function(d) Me.ROAdHocBookingList,
                                                                                                                                                                       Function(d) Me.ROAdHocBookingListCriteria,
                                                                                                                                                                       "StartDate", 15, False)

  Public Property CurrentRSResource As RSResource

#Region " Base VM Overrides "

  Public Overrides Sub SetupResourceScheduler()
    ROAdHocBookingListCriteria.SystemID = OBLib.CommonData.Enums.System.ICR
    ResourceSchedulerID = 12
  End Sub

#End Region

End Class