﻿Imports OBLib.ResourceSchedulers
Imports OBLib.ResourceSchedulers.Base
Imports OBLib.RoomScheduling
Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports Singular.DataAnnotations

Public Class ProgrammingScheduler
  Inherits ResourceSchedulerPageBase(Of ProgrammingSchedulerVM)

End Class

Public Class ProgrammingSchedulerVM
  Inherits ResourceSchedulerVMBaseStateless(Of ProgrammingSchedulerVM)

#Region " Base VM Overrides "

  Public Overrides Sub SetupResourceScheduler()
    ResourceSchedulerID = 11
  End Sub

#End Region

End Class