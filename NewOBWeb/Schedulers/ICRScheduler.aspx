﻿<%@ Page Title="ICR Scheduler" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ICRScheduler.aspx.vb" Inherits="NewOBWeb.ICRScheduler" %>

<%@ Import Namespace="OBLib.AdHoc.Old" %>
<%@ Import Namespace="OBLib.Resources" %>
<%@ Import Namespace="OBLib.ResourceSchedulers.New" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.RoomScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.10/vis.min.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/ResourceSchedulerBase.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/daterangepicker-bs3.css" rel="stylesheet" />
  <link type="text/css" href="../Styles/split.css" rel="stylesheet" />
  <link type="text/css" href="../Styles/flatDream/bootstrap-slider.min.css" rel="stylesheet" />
  <style type="text/css">
    div.hr-dropdown {
      width: 400px;
    }

    .icr-extra-shift {
      border-color: #2480c2;
      color: #fff !important;
      background: #54b3ff;
    }

    .icr-reg-shift {
      border-color: #630030;
      color: #fff !important;
      background: #A9014B;
    }

    .icr-off-day {
      border-color: #c3c6c0;
      color: #fff !important;
      background: #c3c6c0;
    }
  </style>
  <script type="text/javascript" src="../Scripts/Tools/ResourceHelpers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceSchedulerBase.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/timelineContextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/SoberControls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/PagingManagers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ResourceScheduler.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.10/vis.custom.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/split/split.min.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/flatDream/bootstrap-slider.min.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/ResourceSchedulerPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">

    ICRSchedulerPage = {
      editAdHocBookingOld: function (adHocBookingOld) {
        ViewModel.CallServerMethod("GetAdHocBookingListOld",
          {
            AdHocBookingID: adHocBookingOld.AdHocBookingID()
          },
          function (response) {
            if (response.Success) {
              ViewModel.AdHocBookingList.Set(response.Data)
              OBMisc.Notifications.GritterSuccess("Fetched Successfully", "", 500)
              $('.nav-tabs a[href="#EditAdHocBooking"]').tab('show')
            }
            else {
              OBMisc.Notifications.GritterError("Error Fetching Booking", "", 1500)
            }
          })
      },
      saveAdHocBookingList: function (list) {
        var obj = list[0]
        AdHocBookingBOOld.save(obj,
          function (response) {
            ViewModel.AdHocBookingList.Set(response.Data)
          },
          function (response) {

          })
      },
      isAdHocBookingListValid: function (list) {
        var isValid = true
        list.Iterate(function (itm, itmIndx) {
          if (!itm.IsValid()) {
            isValid = false
          }
        })
        return isValid
      },
      newAdHocHumanResource: function (prnt, booking) {
        var newItem = booking.AdHocBookingHumanResourceList.AddNew()
      },
      addAdHocBooking: function () {
        var newItem = ViewModel.AdHocBookingList.AddNew()
        newItem.SystemID(4)
      },
      refresh: function () {
        ViewModel.ROAdHocBookingListPagingManager().Refresh()
      }
    }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers

      With h.Div
        .Attributes("id") = "ResourceScheduler"
      End With

      h.Control(New NewOBWeb.Controls.EditICRShiftModal(Of NewOBWeb.ICRSchedulerVM)())
      h.Control(New NewOBWeb.Controls.EquipmentFeedModal(Of NewOBWeb.ICRSchedulerVM)())

      With h.DivC("popovermodal animated fadeIn go")
        .Attributes("id") = "RSResourcePopoverModal"
        With .Helpers.With(Of RSResource)("ViewModel.CurrentRSResource()")
          With .Helpers.DivC("popover-arrow")
          End With
          With .Helpers.DivC("popovermodal-header")
            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ResourceName()")
          End With
          With .Helpers.DivC("popover-content")
            With .Helpers.HTMLTag("h4")
              .Helpers.HTML("Statistics")
            End With
            With .Helpers.DivC("counters")
              With .Helpers.HTMLTag("h1")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.StartingBalanceHours()")
              End With
              With .Helpers.HTMLTag("h1")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.TargetHours()")
              End With
              With .Helpers.HTMLTag("h1")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.TotalHours()")
              End With
              With .Helpers.HTMLTag("h1")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ReqHours()")
              End With
            End With
          End With
        End With
      End With

      '<div class="popover">
      '   <div class="arrow"></div>
      '   <h3 class="popover-title"></h3>
      '   <div class="popover-content"></div>
      '</div>'

      '<div class="progress">
      '	  <div class="progress-bar progress-bar-primary" style="width: 20%">20%</div>
      '</div>

      With h.Bootstrap.Dialog("AdHocShiftBookings", "AdHoc Bookings", , "modal-lg", Singular.Web.BootstrapEnums.Style.Warning, , "fa-american-sign-language-interpreting", , )
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.TabControl(, "nav-tabs", )
                With .AddTab("FindAdHocBookings", "fa-search", "ICRSchedulerPage.refresh()", "Find", )
                  'RefreshAdhocList()
                  With .TabPane
                    With .Helpers.DivC("")

                    End With
                    With .Helpers.DivC("")
                      With .Helpers.Bootstrap.PagedGridFor(Of OBLib.AdHoc.ReadOnly.ROAdHocBooking)(Function(vm) ViewModel.ROAdHocBookingListPagingManager,
                                                                                                   Function(vm) ViewModel.ROAdHocBookingList,
                                                                                                   False, False, True, False, True, True, True, "",
                                                                                                   Singular.Web.BootstrapEnums.PagerPosition.Bottom, )
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit",
                                                           , Singular.Web.PostBackType.None, "ICRSchedulerPage.editAdHocBookingOld($data)", )
                            End With
                          End With
                          .AddReadOnlyColumn(Function(d As OBLib.AdHoc.ReadOnly.ROAdHocBooking) d.AdHocBookingType)
                          .AddReadOnlyColumn(Function(d As OBLib.AdHoc.ReadOnly.ROAdHocBooking) d.Title)
                          .AddReadOnlyColumn(Function(d As OBLib.AdHoc.ReadOnly.ROAdHocBooking) d.Description)
                          .AddReadOnlyColumn(Function(d As OBLib.AdHoc.ReadOnly.ROAdHocBooking) d.StartDateTimeString)
                          .AddReadOnlyColumn(Function(d As OBLib.AdHoc.ReadOnly.ROAdHocBooking) d.EndDateTimeString)
                        End With
                        With .FooterRow
                        End With
                      End With
                    End With
                  End With
                End With
                With .AddTab("EditAdHocBooking", " fa-briefcase", "", "Create/Edit", )
                  With .TabPane
                    With .Helpers.Bootstrap.TableFor(Of OBLib.AdHoc.Old.AdHocBooking)(Function(VM) ViewModel.AdHocBookingList, False, True, False, True, True, True, True, , "AdHocBookingList")
                      With .FirstRow
                        .AddColumn(Function(d As OBLib.AdHoc.Old.AdHocBooking) d.AdHocBookingTypeID)
                        .AddColumn(Function(d As OBLib.AdHoc.Old.AdHocBooking) d.ProductionAreaID)
                        .AddColumn(Function(d As OBLib.AdHoc.Old.AdHocBooking) d.Title)
                        .AddColumn(Function(d As OBLib.AdHoc.Old.AdHocBooking) d.Description)
                        With .AddColumn("Start Date")
                          With .Helpers.EditorFor(Function(e As OBLib.AdHoc.Old.AdHocBooking) e.StartDateTime)
                            .Style.Width = 100
                          End With
                        End With
                        With .AddColumn("Start Time")
                          With .Helpers.TimeEditorFor(Function(e As OBLib.AdHoc.Old.AdHocBooking) e.StartDateTime)
                            .Style.Width = 70
                          End With
                        End With
                        With .AddColumn("End Date")
                          With .Helpers.EditorFor(Function(e As OBLib.AdHoc.Old.AdHocBooking) e.EndDateTime)
                            .Style.Width = 100
                          End With
                        End With
                        With .AddColumn("End Time")
                          With .Helpers.TimeEditorFor(Function(e As OBLib.AdHoc.Old.AdHocBooking) e.EndDateTime)
                            .Style.Width = 70
                          End With
                        End With
                      End With
                      With .FooterRow
                        With .AddColumn("")
                          With .Helpers.Bootstrap.Button("", "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus-circle", , , "ICRSchedulerPage.addAdHocBooking()", )
                            .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
                          End With
                        End With
                      End With
                      With .AddChildTable(Of OBLib.AdHoc.Old.AdHocBookingHumanResource)(Function(d) d.AdHocBookingHumanResourceList, False, True, False, True, True, True, True, , "AdHocBookingHumanResourceList")
                        With .FirstRow
                          .AddColumn(Function(e As OBLib.AdHoc.Old.AdHocBookingHumanResource) e.HumanResourceID)
                          With .AddColumn("Start Date")
                            With .Helpers.EditorFor(Function(e As OBLib.AdHoc.Old.AdHocBookingHumanResource) e.StartDateTime)
                              .Style.Width = 100
                            End With
                          End With
                          With .AddColumn("Start Time")
                            With .Helpers.TimeEditorFor(Function(e As OBLib.AdHoc.Old.AdHocBookingHumanResource) e.StartDateTime)
                              .Style.Width = 70
                            End With
                          End With
                          With .AddColumn("End Date")
                            With .Helpers.EditorFor(Function(e As OBLib.AdHoc.Old.AdHocBookingHumanResource) e.EndDateTime)
                              .Style.Width = 100
                            End With
                          End With
                          With .AddColumn("End Time")
                            With .Helpers.TimeEditorFor(Function(e As OBLib.AdHoc.Old.AdHocBookingHumanResource) e.EndDateTime)
                              .Style.Width = 70
                            End With
                          End With
                          '.AddColumn(Function(e As OBLib.AdHoc.Old.AdHocBookingHumanResource) e.TeamID)
                        End With
                        With .FooterRow
                          With .AddColumn("")
                            .ColSpan = 7
                            With .Helpers.Bootstrap.Button(, "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, ,
                                                            "fa-plus-circle", , PostBackType.None, "ICRSchedulerPage.newAdHocHumanResource($parent, $data)")
                            End With
                          End With
                        End With
                      End With
                    End With
                    '      With .Helpers.Bootstrap.TableFor(Of OBLib.AdHoc.AdHocBooking)(Function(VM) VM.AdHocBookingList, False, True, , , , , )
                    '        ' .AddNewButton.Button.AddBinding(KnockoutBindingString.enable, "AdHocBookingBO.AddNewButtonEnabledICRShiftScreen()")
                    '        With .FirstRow
                    '          .AddColumn(Function(d As OBLib.AdHoc.AdHocBooking) d.AdHocBookingTypeID)
                    '          .AddColumn(Function(d As OBLib.AdHoc.AdHocBooking) d.Title)
                    '          .AddColumn(Function(d As OBLib.AdHoc.AdHocBooking) d.Description)
                    '          With .AddColumn("Start Date")
                    '            With .Helpers.EditorFor(Function(e As OBLib.AdHoc.AdHocBooking) e.StartDateTime)
                    '              .Style.Width = 100
                    '            End With
                    '          End With
                    '          With .AddColumn("Start Time")
                    '            With .Helpers.TimeEditorFor(Function(e As OBLib.AdHoc.AdHocBooking) e.StartDateTime)
                    '              .Style.Width = 70
                    '            End With
                    '          End With
                    '          With .AddColumn("End Date")
                    '            With .Helpers.EditorFor(Function(e As OBLib.AdHoc.AdHocBooking) e.EndDateTime)
                    '              .Style.Width = 100
                    '            End With
                    '          End With
                    '          With .AddColumn("End Time")
                    '            With .Helpers.TimeEditorFor(Function(e As OBLib.AdHoc.AdHocBooking) e.EndDateTime)
                    '              .Style.Width = 70
                    '            End With
                    '          End With
                    '        End With
                    '        With .AddChildTable(Of OBLib.AdHoc.AdHocBookingHumanResource)(Function(d) d.AdHocBookingHumanResourceList, False, True)

                    '          With .FooterRow
                    '            With .AddColumn("")
                    '              .ColSpan = 7
                    '              With .Helpers.Bootstrap.Button(, "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , PostBackType.None, "NewAdHocHumanResource($parent, $data)")

                    '              End With
                    '            End With
                    '          End With
                    '        End With
                    '        With .FooterRow
                    '          With .AddColumn("")
                    '            With .Helpers.Bootstrap.Button("", "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus-circle", , , "AddAdHocBooking()", )
                    '              .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
                    '            End With
                    '          End With
                    '        End With
                    '      End With
                    '    End With
                    '    'End With
                    '    ''.AddClass("cont")
                    '    'With .Helpers.Bootstrap.Row
                    '    '  With .Helpers.BootstrapDivColumn(12, 12, 12)
                    '    '  End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of AdHocBookingList)("ViewModel.AdHocBookingList()")
            With .Helpers.BootstrapButton(, "Save", "btn-sm btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.None, False, , , "ICRSchedulerPage.saveAdHocBookingList($data)")
              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ICRSchedulerPage.isAdHocBookingListValid($data)")
            End With
          End With
        End With
      End With

    End Using
  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
