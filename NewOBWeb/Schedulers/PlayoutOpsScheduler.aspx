﻿<%@ Page Title="Playout Scheduler" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="PlayoutOpsScheduler.aspx.vb" Inherits="NewOBWeb.PlayoutOpsScheduler" %>

<%@ Import Namespace="OBLib.Resources" %>

<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.RoomScheduling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.10/vis.min.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/ResourceSchedulerBase.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/daterangepicker-bs3.css" rel="stylesheet" />
  <link type="text/css" href="../Styles/split.css" rel="stylesheet" />
  <link type="text/css" href="../Styles/flatDream/bootstrap-slider.min.css" rel="stylesheet" />
  <style type="text/css">
    div.hr-dropdown {
      width: 400px;
    }

    .vis-foreground .vis-group {
      position: relative;
      box-sizing: border-box;
      border-bottom: 1px solid #4d4d4d;
    }

    .vis-labelset > .vis-label {
      background: #333 !important;
      font-size: 11px;
      font-weight: 800;
      color: #fff;
    }

    div.human-resource-name {
      color: #000;
    }

    .vis-labelset .vis-label {
      left: 0;
      top: 0;
      width: 100%;
      border-bottom: 1px solid #4d4d4d;
    }

    .vis-background {
      background: #f0f0f0;
    }

    .vis-time-axis .vis-grid.vis-vertical {
      position: absolute;
      border-left: 1px solid;
    }

    .vis-time-axis .vis-grid.vis-minor {
      border-color: #000;
    }

    .vis-time-axis .vis-grid.vis-major {
      border-color: #000;
    }

    .vis-group {
      background: #67646b;
    }

      .vis-group:hover {
        background: #aba8af;
      }

    .vis-item.vis-range {
      border: 1px solid #000;
    }
  </style>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceHelpers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceSchedulerBasePlayouts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/timelineContextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/SoberControls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/RoomScheduleControl.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/PagingManagers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ResourceScheduler.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.10/vis.custom.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/split/split.min.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/flatDream/bootstrap-slider.min.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/ResourceSchedulerPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers

      With h.Div
        .Attributes("id") = "ResourceScheduler"
      End With

      h.Control(New NewOBWeb.Controls.EditPlayoutOpsShiftModal(Of NewOBWeb.PlayoutOpsSchedulerVM)("CurrentPlayoutOpsShiftModal", "CurrentPlayoutOpsShiftControl"))
      h.Control(New NewOBWeb.Controls.EditPlayoutOpsShiftModalMCR(Of NewOBWeb.PlayoutOpsSchedulerVM)("CurrentPlayoutOpsShiftModalMCR", "CurrentPlayoutOpsShiftControlMCR"))
      h.Control(New NewOBWeb.Controls.EditPlayoutOpsShiftModalSCCR(Of NewOBWeb.PlayoutOpsSchedulerVM)())

      With h.DivC("popovermodal animated fadeIn go")
        .Attributes("id") = "RSResourcePopoverModal"
        With .Helpers.With(Of RSResource)("ViewModel.CurrentRSResource()")
          With .Helpers.DivC("popover-arrow")
          End With
          With .Helpers.DivC("popovermodal-header")
            With .Helpers.DivC("pull-left")
              With .Helpers.Span
                .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ResourceName()")
              End With
            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.FontAwesomeIcon("fa-times", )
                .IconContainer.AddClass("icon-click")
                .IconContainer.AddBinding(Singular.Web.KnockoutBindingString.click, "window.Scheduler.closeHRPopover()")
              End With
            End With
          End With
          With .Helpers.DivC("popover-content")
            'With .Helpers.HTMLTag("h4")
            '  .Helpers.HTML("Statistics")
            'End With
            With .Helpers.DivC("counters-row")
              With .Helpers.DivC("row")
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h3")
                    .Helpers.HTML("Dates")
                  End With
                End With
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h4")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.DatesDescription()")
                  End With
                End With
              End With

              With .Helpers.DivC("row")
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h3")
                    .Helpers.HTML("Opening Balance")
                  End With
                End With
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h4")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.StartingBalanceHours()")
                  End With
                End With
              End With

              With .Helpers.DivC("row")
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h3")
                    .Helpers.HTML("Monthly Requirement")
                  End With
                End With
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h4")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ReqHours()")
                  End With
                End With
              End With

              With .Helpers.DivC("row")
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h3")
                    .Helpers.HTML("Target Hours")
                  End With
                End With
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h4")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.TargetHours()")
                  End With
                End With
              End With

              With .Helpers.DivC("row")
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h3")
                    .Helpers.HTML("Current Hours")
                  End With
                End With
                With .Helpers.DivC("col-md-6")
                  With .Helpers.HTMLTag("h4")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.TotalHours()")
                  End With
                End With
              End With

            End With
          End With
        End With
      End With

    End Using
  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
