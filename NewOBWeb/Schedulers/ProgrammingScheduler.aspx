﻿<%@ Page Title="Programming Scheduler" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ProgrammingScheduler.aspx.vb" Inherits="NewOBWeb.ProgrammingScheduler" %>

<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.RoomScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.20/vis.min.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/controls.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/resource-scheduler.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/daterangepicker-bs3.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/Tools/timelineContextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/controls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/scheduler.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.20/vis.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">
    Singular.OnPageLoad(() => {
      window.scheduler = new scheduler()
    })
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers

      With h.Div
        .Attributes("id") = "scheduler-container"
      End With

      With Helpers.Bootstrap.Dialog("SAShiftPatternModal", "Shift Pattern", , "modal-md", Singular.Web.BootstrapEnums.Style.Primary, , " fa-user", "fa-2x", True)

        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Button(, "New Pattern", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small,
                                               , "fa-plus-circle",,, "SAShiftPatternBO.newPatternModal()")
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 3)
              With .Helpers.Bootstrap.FlatBlock("Filters")
                With .ContentTag
                  With .Helpers.With(Of OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPatternList.Criteria)("ViewModel.CurrentSAShiftPatternCriteria()")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.SystemID)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.SystemID, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.ProductionAreaID)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.ProductionAreaID, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.StartDate)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.StartDate, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.EndDate)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.EndDate, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small,
                                                             , "fa-refresh",,, "SAShiftPatternCriteriaBO.refresh($data)",)
                        .Button.AddClass("btn-block")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 8, 9, 9)
              With .Helpers.Bootstrap.FlatBlock("Patterns")
                With .ContentTag
                  With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPattern)("CurrentSAShiftPatternListManager", "ViewModel.CurrentSAShiftPatternList()",
                                                                                                           False, False, False, True, True, True, True, , , , )
                    With .FirstRow
                      With .AddColumn("")
                        With .Helpers.Bootstrap.Button(, "Edit",,,,, "fa-edit",,, "SAShiftPatternBO.editModal($data)",)
                          .Button.AddBinding(KnockoutBindingString.visible, "!$data.IsProcessing()")
                        End With
                        With .Helpers.Bootstrap.Button(,,,,,, "fa-refresh fa-spin",,,,)
                          .Button.AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPattern) c.PatternName)
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPattern) c.NumWeeks)
                      End With
                      'With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Pattern.ReadOnlys.SAShiftPattern) c.System)
                      'End With                                                  
                      With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPattern) c.ProductionArea)
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPattern) c.ShiftCount)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("sober-modal sober-modal-gray")
            .Attributes("id") = "NewSAShiftPatternModal"
            .Helpers.HTML.Heading3("Edit Pattern")
            With .Helpers.Bootstrap.FontAwesomeIcon(" fa-close sober-modal-close pull-right", "fa-2x")
              .IconContainer.AddBinding(KnockoutBindingString.click, "modalBase.hideModal($element)")
            End With
            With .Helpers.DivC("sober-modal-body row")
              With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                With .Helpers.Bootstrap.FlatBlock("Basic Info")
                  With .ContentTag
                    With .Helpers.With(Of OBLib.Shifts.Patterns.SAShiftPattern)("ViewModel.CurrentSAShiftPattern()")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(c) c.PatternName)
                            With .Helpers.Bootstrap.FormControlFor(Function(c) c.PatternName, BootstrapEnums.InputSize.Small)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternBO.canEdit('PatternName', $data)")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(c) c.SystemID)
                            With .Helpers.Bootstrap.FormControlFor(Function(c) c.SystemID, BootstrapEnums.InputSize.Small)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternBO.canEdit('SystemID', $data)")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(c) c.ProductionAreaID)
                            With .Helpers.Bootstrap.FormControlFor(Function(c) c.ProductionAreaID, BootstrapEnums.InputSize.Small)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternBO.canEdit('ProductionAreaID', $data)")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(c) c.NumWeeks)
                            With .Helpers.Bootstrap.FormControlFor(Function(c) c.NumWeeks, BootstrapEnums.InputSize.Small)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternBO.canEdit('NumWeeks', $data)")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(c) c.ShiftCount)
                            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(c) c.ShiftCount, BootstrapEnums.InputSize.Small)
                              '.Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternBO.canEdit('NumWeeks', $data)")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success,, BootstrapEnums.ButtonSize.Small,,
                                                         "fa-floppy-o",,, "SAShiftPatternBO.saveModal($data)",)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 9, 9)
                With .Helpers.With(Of OBLib.Shifts.Patterns.SAShiftPattern)("ViewModel.CurrentSAShiftPattern()")
                  With .Helpers.Bootstrap.TabControl(, "nav-tabs",, BootstrapEnums.TabAlignment.Left)
                    With .AddTab("PatternDays", " fa-retweet", , "Days", ,)
                      With .TabPane
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Shifts.Patterns.SAShiftPatternDay)("$data.SAShiftPatternDayList()", False, False, False, True, True, True, True,
                                                                                                   "SAShiftPatternDayList",,)
                          With .FirstRow
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternDay) c.WeekNo)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternDayBO.canEdit('WeekNo', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternDay) c.WeekDay)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternDayBO.canEdit('WeekDay', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternDay) c.ShiftTypeID)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternDayBO.canEdit('ShiftTypeID', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternDay) c.StartTime)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternDayBO.canEdit('StartTime', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternDay) c.EndTime)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternDayBO.canEdit('EndTime', $data)")
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternDay) c.ShiftCount)
                              '.Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('ShiftCount', $data)")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .AddTab("PatternCrew", " fa-users", , "Crew", ,)
                      With .TabPane
                        With .Helpers.Bootstrap.FlatDreamAlert("Please ensure that all other information is saved before editing crew", BootstrapEnums.FlatDreamAlertColor.Warning, , "Warning!", False, False)
                          .AlertTag.AddBinding(KnockoutBindingString.visible, "!SAShiftPatternBO.canEdit('Crew', $data)")
                        End With
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Shifts.Patterns.SAShiftPatternHR)("$data.SAShiftPatternHRList()", True, True, False, True, True, True, True,
                                                                                                  "SAShiftPatternHRList",,)
                          .AddNewButton.Button.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canAdd($data)")
                          .RemoveButton.Button.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('RemoveButton', $data)")
                          With .FirstRow
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternHR) c.DisciplineID)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('DisciplineID', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternHR) c.HumanResourceID)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('HumanResourceID', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternHR) c.StartDate)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('StartDate', $data)")
                            End With
                            With .AddColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternHR) c.EndDate)
                              .Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('EndDate', $data)")
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Shifts.Patterns.SAShiftPatternHR) c.ShiftCount)
                              '.Editor.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('ShiftCount', $data)")
                            End With
                            With .AddColumn("")
                              With .Helpers.Bootstrap.StateButtonNew(Function(d) d.IsProcessing, "Generating...", "", "btn-info",, "fa-refresh fa-spin",,)
                                .Button.AddBinding(KnockoutBindingString.visible, "SAShiftPatternHRBO.canView('GeneratingButton', $data)")
                              End With
                              With .Helpers.Bootstrap.Button(, "Generate", BootstrapEnums.Style.Success,, BootstrapEnums.ButtonSize.ExtraSmall,,
                                                             "fa-cloud-upload",,, "SAShiftPatternHRBO.generateShifts($data)",)
                                .Button.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('GenerateButton', $data)")
                                '.Button.AddBinding(KnockoutBindingString.visible, "SAShiftPatternHRBO.canView('GenerateButton', $data)")
                              End With
                              'With .Helpers.Bootstrap.StateButtonNew(Function(d) d.IsProcessing, "Updating...", "", "btn-info",, "fa-refresh fa-spin",,)
                              '  .Button.AddBinding(KnockoutBindingString.visible, "SAShiftPatternHRBO.canView('UpdatingButton', $data)")
                              'End With
                              'With .Helpers.Bootstrap.Button(, "Update", BootstrapEnums.Style.Success,, BootstrapEnums.ButtonSize.ExtraSmall,,
                              '                               "fa-cloud-upload",,, "SAShiftPatternHRBO.updateShifts($data)",)
                              '  .Button.AddBinding(KnockoutBindingString.enable, "SAShiftPatternHRBO.canEdit('UpdateButton', $data)")
                              '  .Button.AddBinding(KnockoutBindingString.visible, "SAShiftPatternHRBO.canView('UpdateButton', $data)")
                              'End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.With(Of OBLib.Shifts.Patterns.SAShiftPattern)("ViewModel.CurrentSAShiftPattern()")
              With .Helpers.DivC("loading-custom")
                .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
              End With
            End With
            'End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(KnockoutBindingString.visible, "ViewModel.IsProcessing()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
          End With
        End With

      End With

    End Using

  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
