﻿Imports OBLib.ResourceSchedulers
Imports OBLib.ResourceSchedulers.Base
Imports OBLib.RoomScheduling
Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports Singular.DataAnnotations

Public Class PlayoutOpsScheduler
  Inherits ResourceSchedulerPageBase(Of PlayoutOpsSchedulerVM)

End Class

Public Class PlayoutOpsSchedulerVM
  Inherits ResourceSchedulerVMBaseStateless(Of PlayoutOpsSchedulerVM)

#Region " Base VM Overrides "

  Public Overrides Sub SetupResourceScheduler()
    ResourceSchedulerID = 5
  End Sub

#End Region

End Class