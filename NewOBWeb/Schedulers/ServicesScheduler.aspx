﻿<%@ Page Title="Services Scheduler" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ServicesScheduler.aspx.vb" Inherits="NewOBWeb.ServicesScheduler" %>

<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.RoomScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.20/vis.min.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/controls.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/resource-scheduler.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/daterangepicker-bs3.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/Tools/timelineContextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/controls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/scheduler.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.20/vis.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">
    Singular.OnPageLoad(() => {
      window.scheduler = new scheduler()
    })  
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers

      With h.Div
        .Attributes("id") = "scheduler-container"
      End With

      h.Control(New NewOBWeb.Controls.EditStudioSupervisorShiftModal(Of NewOBWeb.ServicesSchedulerVM)())
      h.Control(New NewOBWeb.Controls.ContentServicesDifferencesModal(Of NewOBWeb.ServicesSchedulerVM)())

    End Using
  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
  <%--  <%  
    Using h = Helpers
      
      If Singular.Security.HasAccess("Resource Scheduler", "Can View Synergy Events By Channel") Then
        With h.Bootstrap.Nav("SynergyMenu", "cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right side-chat")
          With .NavTag
            With .Helpers.DivC("header")
              With .Helpers.Bootstrap.FontAwesomeIcon("fa-skype", "fa-2x")
                .IconContainer.AddClass("fa-synergy-menu-header-icon")
              End With
              With .Helpers.HTMLTag("h3")
                .Helpers.HTML("Synergy Events")
              End With
            End With
            With .Helpers.DivC("content")
              .Helpers.Control(New NewOBWeb.Controls.SynergyEventsControl(Of NewOBWeb.ProductionServicesSchedulerVM)("SynergyEvents", "SynergyEvents"))
            End With
          End With
        End With
      End If
      
    End Using
  %>--%>
</asp:Content>
