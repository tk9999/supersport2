﻿Public Class ResourceSchedulerPageBase(Of VM As Singular.Web.ViewModel(Of VM))
  Inherits OBPageBase(Of VM)

  Protected Overrides Sub Setup()
    MyBase.Setup()

    Me.Controls.Add(Helpers.Control(New Controls.RoomScheduleControl(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.RoomScheduleTemplateModal(Of VM)))
    Me.Controls.Add(Helpers.Control(New Controls.UserNotificationsModal(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.RORoomScheduleControl(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.EquipmentFeedModal(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.EquipmentMaintenanceModal(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.EditGenericShiftModal(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.EditSchedulerModalOld(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.EditSchedulerModalNew(Of VM)()))
    Me.Controls.Add(Helpers.Control(New Controls.SAShiftPatternModal(Of VM)))

  End Sub

End Class
