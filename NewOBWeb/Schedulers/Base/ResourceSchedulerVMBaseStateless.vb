﻿Imports OBLib.ResourceSchedulers
Imports Singular.Web
Imports OBLib.Resources
Imports Singular.Rules
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Quoting.ReadOnly
Imports Singular.Misc
Imports OBLib.Maintenance.AdHoc.ReadOnly
Imports OBLib.Helpers.ResourceHelpers
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports OBLib.Copying
Imports OBLib.SatOps
Imports OBLib.Scheduling.Rooms
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.Shifts.ReadOnly
Imports System

Public MustInherit Class ResourceSchedulerVMBaseStateless(Of ModelType As Singular.Web.StatelessViewModel(Of ModelType))
  Inherits OBViewModelStateless(Of ModelType)
  Implements ControlInterfaces(Of ModelType).IRoomScheduleAreaControl
  Implements ControlInterfaces(Of ModelType).IEditResourceScheduler
  Implements ControlInterfaces(Of ModelType).IPlayoutOpsShift
  Implements ControlInterfaces(Of ModelType).IPlayoutOpsShiftMCR
  Implements ControlInterfaces(Of ModelType).IPlayoutOpsShiftSCCR

#Region " Resource Scheduler "

  Public Property EditResourceScheduler As OBLib.ResourceSchedulers.[New].ResourceScheduler Implements ControlInterfaces(Of ModelType).IEditResourceScheduler.EditResourceScheduler
  Public Property ResourceScheduler As OBLib.ResourceSchedulers.[New].ResourceScheduler

#End Region

#Region " Properties "

#Region " General "

  Public Property IsSchedulerAdministrator As Boolean = False
  Public Property ResourceSchedulerID As Integer?

  <Browsable(False)>
  Public Property Scheduler As OBLib.ResourceSchedulers.[New].ResourceScheduler
  Public Property CopySetting As CopySetting
  Public Property CurrentProduction As OBLib.Productions.Production
  Public Property CurrentAdHocBooking As OBLib.AdHoc.AdHocBooking
  Public Property ResourceBookingClean As ResourceBookingClean = Nothing
  Public Property CurrentRSResource As RSResource

  <InitialDataOnly, AlwaysClean>
  Public Property ApplySelectionTemplateList As List(Of OBLib.Helpers.ResourceHelpers.ApplySelectionTemplate)

  <InitialDataOnly, AlwaysClean>
  Public Property ApplyResultList As List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)

  Public Property CurrentUserSystemID As Integer?

#End Region

#Region " Room Schedule "

  Public Property CurrentRoomScheduleArea As RoomScheduleArea Implements ControlInterfaces(Of ModelType).IRoomScheduleAreaControl.CurrentRoomScheduleArea
  Public Property CurrentRORoomBooking As OBLib.Rooms.ReadOnly.RORoomBooking
  Public Property CurrentRoomScheduleTemplate As RoomScheduleTemplate

#End Region

#Region " Shifts "

  Public Property CurrentPlayoutOpsShift As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift Implements ControlInterfaces(Of ModelType).IPlayoutOpsShift.CurrentPlayoutOpsShift
  Public Property CurrentPlayoutOpsShiftMCR As OBLib.Shifts.PlayoutOperations.MCRShift Implements ControlInterfaces(Of ModelType).IPlayoutOpsShiftMCR.CurrentPlayoutOpsShiftMCR
  Public Property CurrentPlayoutOpsShiftSCCR As OBLib.Shifts.PlayoutOperations.SCCRShift Implements ControlInterfaces(Of ModelType).IPlayoutOpsShiftSCCR.CurrentPlayoutOpsShiftSCCR
  Public Property CurrentStudioSupervisorShift As OBLib.Shifts.Studios.StudioSupervisorShift
  Public Property CurrentICRShift As OBLib.Shifts.ICR.ICRShift
  Public Property CurrentGenericShift As OBLib.Shifts.GenericShift

  Public Property RORoomScheduleListSelectShiftList As RORoomScheduleListSelectShiftList
  Public Property RORoomScheduleListSelectShiftListCriteria As RORoomScheduleListSelectShiftList.Criteria
  Public Property RORoomScheduleListSelectShiftListManager As Singular.Web.Data.PagedDataManager(Of ModelType)

  Public Property CurrentSAShiftPatternCriteria As OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPatternList.Criteria
  Public Property CurrentSAShiftPatternList As OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPatternList
  Public Property CurrentSAShiftPatternListManager As Singular.Web.Data.PagedDataManager(Of ModelType)
  Public Property CurrentSAShiftPattern As OBLib.Shifts.Patterns.SAShiftPattern

#End Region

#Region " Off Periods "

  'Public Property CurrentHumanResourceOffPeriod As OBLib.HR.HumanResourceOffPeriod Implements ControlInterfaces(Of ModelType).IEditHumanResourceOffPeriod.CurrentHumanResourceOffPeriod
  '<InitialDataOnly>
  'Public Property CurrentROHumanResourceOffPeriod As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod

#End Region

#Region " Secondments "

  'Public Property CurrentHumanResourceSecondment As OBLib.HR.HumanResourceSecondment Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentHumanResourceSecondment
  '<InitialDataOnly>
  'Public Property CurrentSecondmentROCountryList As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentSecondmentROCountryList
  '<InitialDataOnly>
  'Public Property CurrentSecondmentROCountryListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentSecondmentROCountryListCriteria
  '<InitialDataOnly>
  'Public Property CurrentSecondmentROCountryListManager As Singular.Web.Data.PagedDataManager(Of ModelType) Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentSecondmentROCountryListManager
  '<InitialDataOnly>
  'Public Property CurrentSecondmentRODebtorList As RODebtorList Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentSecondmentRODebtorList
  '<InitialDataOnly>
  'Public Property CurrentSecondmentRODebtorListCriteria As OBLib.Quoting.ReadOnly.RODebtorList.Criteria Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentSecondmentRODebtorListCriteria
  '<InitialDataOnly>
  'Public Property CurrentSecondmentRODebtorListManager As Singular.Web.Data.PagedDataManager(Of ModelType) Implements ControlInterfaces(Of ModelType).IEditHumanResourceSecondment.CurrentSecondmentRODebtorListManager

#End Region

#Region " Equipment "

  <InitialDataOnly>
  Public Property FeedProductionGuid As String = ""
  Public Property CurrentEquipmentFeed As EquipmentFeed
  Public Property CurrentEquipmentMaintenance As EquipmentMaintenance

#End Region

#End Region

#Region " Base Methods "

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetResourceBookingsClean(StartDate As DateTime?, EndDate As DateTime?, ResourceXmlIDs As String,
                                           SystemID As Integer?, ProductionAreaID As Integer?) As Singular.Web.Result

    Dim wr As Singular.Web.Result
    Try
      Dim ResourceBookings As ResourceBookingCleanList = OBLib.Resources.ResourceBookingCleanList.GetResourceBookingCleanList(ResourceXmlIDs, StartDate, EndDate, "", SystemID, ProductionAreaID)
      wr = New Singular.Web.Result(True)
      wr.Data = New With {
                          .Bookings = ResourceBookings
                }
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

#End Region

#Region " Overrides "

  Public MustOverride Sub SetupResourceScheduler()

  Public Overridable Function GetResourceScheduler() As OBLib.ResourceSchedulers.[New].ResourceScheduler
    If ResourceSchedulerID IsNot Nothing Then
      Dim lst As OBLib.ResourceSchedulers.[New].ResourceSchedulerList = OBLib.ResourceSchedulers.[New].ResourceSchedulerList.GetResourceSchedulerList(ResourceSchedulerID, CurrentUserID, True, True, True)
      If lst.Count = 1 Then
        Return lst(0)
      Else
        OBLib.Helpers.ErrorHelpers.LogClientError(CurrentUserName, "ResourceSchedulerVMBase", "GetResourceScheduler", lst.Count.ToString & ": as scheduler count")
      End If
    End If
    Return Nothing
  End Function

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    MyBase.Setup()

    ApplyResultList = New List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)

    SetupResourceScheduler()
    Scheduler = GetResourceScheduler()

    IsSchedulerAdministrator = Scheduler.IsAdministrator

    ClientDataProvider.AddDataSource("ROResourceTypeList", OBLib.CommonData.Lists.ROResourceTypeList, False)
    ClientDataProvider.AddDataSource("RORoomTypeList", OBLib.CommonData.Lists.RORoomTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROAdHocBookingTypeList", OBLib.CommonData.Lists.ROAdHocBookingTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemAreaAdHocBookingTypeList", OBLib.CommonData.Lists.ROSystemAreaAdHocBookingTypeList, False)
    ClientDataProvider.AddDataSource("ROSiteList", OBLib.CommonData.Lists.ROSiteList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaStatusSelectList", OBLib.CommonData.Lists.ROSystemProductionAreaStatusSelectList, False)
    ClientDataProvider.AddDataSource("ResourceSchedulerData", Scheduler, False)
    ClientDataProvider.AddDataSource("ROEquipmentTypeList", OBLib.CommonData.Lists.ROEquipmentTypeList, False)
    ClientDataProvider.AddDataSource("RORoomScheduleTypeList", OBLib.CommonData.Lists.RORoomScheduleTypeList, False)
    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("ROCostCentreList", OBLib.CommonData.Lists.ROCostCentreList, False)
    ClientDataProvider.AddDataSource("ROOffReasonList", OBLib.CommonData.Lists.ROOffReasonList, False)
    ClientDataProvider.AddDataSource("ROShiftTypeList", OBLib.CommonData.Lists.ROShiftTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemAreaShiftTypeList", OBLib.CommonData.Lists.ROSystemAreaShiftTypeList, False)
    ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
    ClientDataProvider.AddDataSource("ROCountryList", OBLib.CommonData.Lists.ROCountryList, False)
    ClientDataProvider.AddDataSource("RODebtorList", OBLib.CommonData.Lists.RODebtorList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplineList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplineList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplinePositionTypeList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplinePositionTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplinePositionTypeListFlat", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplinePositionTypeListFlat, False)
    ClientDataProvider.AddDataSource("ROFeedTypeList", OBLib.CommonData.Lists.ROFeedTypeList, False)
    ClientDataProvider.AddDataSource("ROIngestTypeList", OBLib.CommonData.Lists.ROIngestSourceTypeList, False)
    ClientDataProvider.AddDataSource("ROAudioSettingList", OBLib.CommonData.Lists.ROAudioSettingList, False)
    ClientDataProvider.AddDataSource("ROStudioSupervisorList", OBLib.CommonData.Lists.ROStudioSupervisorList, False)
    ClientDataProvider.AddDataSource("ROCustomResourceList", OBLib.CommonData.Lists.ROCustomResourceList, False)
    ClientDataProvider.AddDataSource("ROCostTypeList", OBLib.CommonData.Lists.ROCostTypeList, False)
    ClientDataProvider.AddDataSource("ROCurrencyList", OBLib.CommonData.Lists.ROCurrencyList, False)
    ClientDataProvider.AddDataSource("ROVATTypeList", OBLib.CommonData.Lists.ROVatTypeList, False)
    ClientDataProvider.AddDataSource("ROShiftDurationList", OBLib.CommonData.Lists.ROShiftDurationList, False)
    ClientDataProvider.AddDataSource("ROShiftPatternWeekDayList", OBLib.CommonData.Lists.ROShiftPatternWeekDayList, False)

    RORoomScheduleListSelectShiftList = New RORoomScheduleListSelectShiftList
    RORoomScheduleListSelectShiftListCriteria = New RORoomScheduleListSelectShiftList.Criteria
    RORoomScheduleListSelectShiftListManager = New Singular.Web.Data.PagedDataManager(Of ModelType)(Function(d) Me.RORoomScheduleListSelectShiftList,
                                                                                                    Function(d) Me.RORoomScheduleListSelectShiftListCriteria,
                                                                                                    "CallTimeStart", 15, True)

    CurrentSAShiftPatternListManager = New Singular.Web.Data.PagedDataManager(Of ModelType)(Function(d) Me.CurrentSAShiftPatternList,
                                                                                            Function(d) Me.CurrentSAShiftPatternCriteria,
                                                                                            "CreatedDate", 15, False)

    Me.CurrentUserSystemID = OBLib.Security.Settings.CurrentUser.SystemID

  End Sub

#Region " Apply "

  '<WebCallable(LoggedInOnly:=True)>
  'Public Overridable Function ApplySelections(selectionTemplates As List(Of OBLib.Helpers.ResourceHelpers.ApplySelectionTemplate)) As Singular.Web.Result

  '  Try
  '    Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerApplySelections]")
  '    Dim templatesParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
  '    templatesParam.Name = "@ApplySelectionTemplates"
  '    templatesParam.SqlType = SqlDbType.Structured
  '    templatesParam.Value = OBLib.Helpers.ResourceHelpers.CreateApplySelectionTemplateTableParameter(selectionTemplates)
  '    cmdProc.Parameters.Add(templatesParam)
  '    cmdProc.Parameters.AddWithValue("@SystemID", SystemID)
  '    cmdProc.Parameters.AddWithValue("@ProductionAreaID", DefaultProductionAreaID)
  '    cmdProc.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
  '    cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
  '    cmdProc = cmdProc.Execute

  '    'Read the results of the command proc and convert to ApplyResult business objects
  '    Dim ApplyList As New List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)
  '    For Each dr As DataRow In cmdProc.Dataset.Tables(0).Rows
  '      ApplyList.Add(New OBLib.Helpers.ResourceHelpers.ApplyResult(dr))
  '    Next

  '    WebsiteHelpers.SendResourceBookingUpdateNotifications()
  '    Return New Singular.Web.Result(True) With {.Data = ApplyList}

  '  Catch ex As Exception
  '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try

  'End Function

#End Region

#End Region

End Class
