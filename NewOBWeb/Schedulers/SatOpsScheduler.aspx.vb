﻿Imports OBLib.SatOps
Imports Singular.DataAnnotations
Imports OBLib.Resources
Imports Singular.Web
Imports OBLib.Maintenance.SatOps.ReadOnly

Public Class SatOpsScheduler
  Inherits ResourceSchedulerPageBase(Of SatOpsSchedulerVM)

End Class

Public Class SatOpsSchedulerVM
  Inherits ResourceSchedulerVMBaseStateless(Of SatOpsSchedulerVM)
  Implements ControlInterfaces(Of SatOpsSchedulerVM).IROAudioSettingListModal

#Region " Properties "

  Public Property ROAudioSettingList As OBLib.Maintenance.SatOps.ReadOnly.ROAudioSettingList Implements ControlInterfaces(Of SatOpsSchedulerVM).IROAudioSettingListModal.ROAudioSettingList

#End Region

#Region " Base VM Overrides "

  Protected Overrides Sub Setup()
    MyBase.Setup()
    ROAudioSettingList = OBLib.CommonData.Lists.ROAudioSettingList

  End Sub

  Public Overrides Sub SetupResourceScheduler()
    ResourceSchedulerID = 2
  End Sub

  'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
  '  MyBase.HandleCommand(Command, CommandArgs)

  '  Select Case Command


  '    Case "PrintSatOpsBookingReport"
  '      PrintSatOpsBookingReport(CommandArgs)

  '    Case "AddFeedGenRef"
  '      Dim sh As Singular.Web.Result = CurrentEquipmentSchedule.FeedList(0).AddGenRef()
  '      If sh.Success Then
  '        SaveEquipmentSchedule(CurrentEquipmentSchedule, CommandArgs)
  '        CurrentEquipmentSchedule = FetchEquipmentSchedule(CurrentEquipmentSchedule.EquipmentScheduleID)
  '      End If
  '      CommandArgs.ReturnData = sh

  '    Case "AddFeedGenRefs"
  '      Dim sh As Singular.Web.Result = CurrentEquipmentSchedule.FeedList(0).AddGenRefs()
  '      If sh.Success Then
  '        SaveEquipmentSchedule(CurrentEquipmentSchedule, CommandArgs)
  '        CurrentEquipmentSchedule = FetchEquipmentSchedule(CurrentEquipmentSchedule.EquipmentScheduleID)
  '      End If
  '      CommandArgs.ReturnData = sh

  '  End Select

  'End Sub

  'Private Sub PrintSatOpsBookingReport(CommandArgs As Singular.Web.CommandArgs)

  '  If CurrentEquipmentSchedule IsNot Nothing Then
  '    Dim rpt As New OBWebReports.SatOpsReports.EquipmentBookingReport
  '    rpt.ReportCriteria.EquipmentIDs = New List(Of Integer)
  '    rpt.ReportCriteria.EquipmentScheduleIDs = New List(Of Integer)
  '    rpt.ReportCriteria.EquipmentScheduleIDs.Add(CurrentEquipmentSchedule.EquipmentScheduleID)
  '    Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
  '    SendFile(rfi.FileName, rfi.FileBytes)
  '  End If

  'End Sub

#End Region

End Class