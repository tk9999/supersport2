﻿Imports OBLib.ResourceSchedulers
Imports OBLib.ResourceSchedulers.Base
Imports OBLib.RoomScheduling
Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports Singular.DataAnnotations

Public Class MaximoScheduler
  Inherits ResourceSchedulerPageBase(Of MaximoSchedulerVM)

End Class

Public Class MaximoSchedulerVM
  Inherits ResourceSchedulerVMBaseStateless(Of MaximoSchedulerVM)

#Region " Base VM Overrides "

  Public Overrides Sub SetupResourceScheduler()
    ResourceSchedulerID = 9
  End Sub

#End Region

End Class