﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="OutsideBroadcast.aspx.vb" Inherits="NewOBWeb.OutsideBroadcast" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js"></script>
  <script type="text/javascript" src="OutsideBroadcastPage.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% 
    Using h = Helpers

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Production Details", , , , )
            With .ContentTag
              .Helpers.Control(New NewOBWeb.Controls.ProductionOBLayout(Of NewOBWeb.ProductionVM)())
            End With
          End With
        End With
      End With
      
    End Using
  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
