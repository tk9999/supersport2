﻿Imports Singular.DataAnnotations
Imports OBLib.Productions.Correspondence
Imports OBLib.Productions

Public Class OutsideBroadcast
  Inherits OBPageBase(Of OutsideBroadcastVM)

End Class

Public Class OutsideBroadcastVM
  Inherits OBViewModelStateless(Of OutsideBroadcastVM)
  Implements ControlInterfaces(Of ProductionVM).IROProductionOutsourceServiceList
  Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements

  Public ReadOnly Property CurrentProduction As OBLib.Productions.Production
    Get
      Return SessionData.CurrentProduction
    End Get
  End Property

  'Outsource Services
  <InitialDataOnly>
  Public Property ROProductionOutsourceServiceList As OBLib.ProductionSystemAreas.ReadOnly.ROProductionOutsourceServiceList Implements ControlInterfaces(Of ProductionVM).IROProductionOutsourceServiceList.ProductionOutsourceServiceList
  <InitialDataOnly>
  Public Property ROProductionOutsourceServiceListCriteria As OBLib.ProductionSystemAreas.ReadOnly.ROProductionOutsourceServiceList.Criteria Implements ControlInterfaces(Of ProductionVM).IROProductionOutsourceServiceList.ProductionOutsourceServiceListCriteria
  <InitialDataOnly>
  Public Property ROProductionOutsourceServiceListManager As Singular.Web.Data.PagedDataManager(Of ProductionVM) Implements ControlInterfaces(Of ProductionVM).IROProductionOutsourceServiceList.ProductionOutsourceServiceListManager

  'Public Property ProductionOutsourceServiceList As OBLib

  'Required Equipment
  <InitialDataOnly>
  Property ROProductionSpecRequirementEquipmentTypeList As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementEquipmentTypeList Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements.ROProductionSpecRequirementEquipmentTypeList
  <InitialDataOnly>
  Property ROProductionSpecRequirementEquipmentTypeListCriteria As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementEquipmentTypeList.Criteria Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements.ROProductionSpecRequirementEquipmentTypeListCriteria
  <InitialDataOnly>
  Property ROProductionSpecRequirementEquipmentTypeListManager As Singular.Web.Data.PagedDataManager(Of ProductionVM) Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements.ROProductionSpecRequirementEquipmentTypeListManager

  'Required Positions
  <InitialDataOnly>
  Property ROProductionSpecRequirementPositionList As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementPositionList Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements.ROProductionSpecRequirementPositionList
  <InitialDataOnly>
  Property ROProductionSpecRequirementPositionListCriteria As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementPositionList.Criteria Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements.ROProductionSpecRequirementPositionListCriteria
  <InitialDataOnly>
  Property ROProductionSpecRequirementPositionListManager As Singular.Web.Data.PagedDataManager(Of ProductionVM) Implements ControlInterfaces(Of ProductionVM).IROProductionSpecRequirements.ROProductionSpecRequirementPositionListManager

  'Comments
  Public Property ProductionCommentSectionList As OBLib.OutsideBroadcast.ProductionCommentSectionList

  'Audio
  Public Property ProductionAudioConfigurationList As ProductionAudioConfigurationList

  'Documents
  Public Property ProductionDocumentList As ProductionDocumentList

  'General Comments
  Public Property ProductionGeneralCommentList As ProductionGeneralCommentList

  'Petty Cash
  Public Property ProductionPettyCashList As ProductionPettyCashList


End Class