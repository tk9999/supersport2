﻿<%@ Page Title="Services Scheduler" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="PSScheduler.aspx.vb" Inherits="NewOBWeb.PSScheduler" %>

<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.RoomScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.10/vis.min.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/ResourceSchedulerBase.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/daterangepicker-bs3.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="stylesheet" />
  <link type="text/css" href="../Styles/split.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="stylesheet" />
  <link type="text/css" href="../Styles/flatDream/bootstrap-slider.min.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="stylesheet" />
  <style type="text/css">
    div.hr-dropdown {
      width: 400px;
    }

    .vis-foreground .vis-group {
      position: relative;
      box-sizing: border-box;
      border-bottom: 1px solid #4d4d4d;
    }

    .vis-labelset > .vis-label {
      background: #333 !important;
      color: #f0f0f0 !important;
      font-size: 11px;
      font-weight: 800;
    }

    .vis-labelset .vis-label {
      left: 0;
      top: 0;
      width: 100%;
      border-bottom: 1px solid #4d4d4d;
    }

    .vis-background {
      background: #f0f0f0;
    }

    .vis-time-axis .vis-grid.vis-vertical {
      position: absolute;
      border-left: 1px solid;
    }

    .vis-time-axis .vis-grid.vis-minor {
      border-color: #000;
    }

    .vis-time-axis .vis-grid.vis-major {
      border-color: #000;
    }

    .vis-group {
      background: #67646b;
    }

      .vis-group:hover {
        background: #aba8af;
      }

    .vis-item.vis-range {
      border: 1px solid #000;
    }
  </style>
  <script type="text/javascript" src="../Scripts/Tools/ResourceHelpers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceSchedulerBase.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/timelineContextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/SoberControls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/PagingManagers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ResourceScheduler.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.10/vis.custom.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/split/split.min.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/flatDream/bootstrap-slider.min.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/ResourceSchedulerPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers

      With h.Div
        .Attributes("id") = "ResourceScheduler"
      End With

      h.Control(New NewOBWeb.Controls.EditStudioSupervisorShiftModal(Of NewOBWeb.ProductionServicesSchedulerVM)())

      h.Control(New NewOBWeb.Controls.ContentServicesDifferencesModal(Of NewOBWeb.ProductionServicesSchedulerVM)())

    End Using
  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
  <%--  <%  
    Using h = Helpers
      
      If Singular.Security.HasAccess("Resource Scheduler", "Can View Synergy Events By Channel") Then
        With h.Bootstrap.Nav("SynergyMenu", "cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right side-chat")
          With .NavTag
            With .Helpers.DivC("header")
              With .Helpers.Bootstrap.FontAwesomeIcon("fa-skype", "fa-2x")
                .IconContainer.AddClass("fa-synergy-menu-header-icon")
              End With
              With .Helpers.HTMLTag("h3")
                .Helpers.HTML("Synergy Events")
              End With
            End With
            With .Helpers.DivC("content")
              .Helpers.Control(New NewOBWeb.Controls.SynergyEventsControl(Of NewOBWeb.ProductionServicesSchedulerVM)("SynergyEvents", "SynergyEvents"))
            End With
          End With
        End With
      End If
      
    End Using
  %>--%>
</asp:Content>
