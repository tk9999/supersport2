﻿<%@ Page Title="Programming Scheduler" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="MaximoScheduler.aspx.vb" Inherits="NewOBWeb.MaximoScheduler" %>

<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.RoomScheduling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.20/vis.min.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/controls.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/resource-scheduler.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="Stylesheet" />
  <link type="text/css" href="../Styles/daterangepicker-bs3.css?v=<%= OBLib.OBMisc.VersionNo %>" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/Tools/timelineContextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/controls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/scheduler.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.20/vis.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">
    Singular.OnPageLoad(() => {
      window.scheduler = new scheduler()
    })
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers

      With h.Div
        .Attributes("id") = "scheduler-container"
      End With

    End Using

  %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
