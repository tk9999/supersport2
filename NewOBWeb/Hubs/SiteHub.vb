﻿Imports System.IO
Imports System.Runtime.Serialization.Json
Imports OBLib.Resources
Imports System
Imports System.Web
Imports Microsoft.AspNet.SignalR
Imports System.Threading.Tasks
Imports Singular.Misc
Imports System.Reflection

Public Class SiteHub
  Inherits Microsoft.AspNet.SignalR.Hub

  Public Sub New()
  End Sub

  Public Overrides Function OnConnected() As Threading.Tasks.Task
    Return MyBase.OnConnected()
  End Function

  Public Overrides Function OnDisconnected(stopCalled As Boolean) As Threading.Tasks.Task

    Dim schedulerConnections As List(Of ResourceSchedulerConnection) = ConnectedResourceSchedulers.Where(Function(d As ResourceSchedulerConnection) CompareSafe(d.ConnectionID, Context.ConnectionId)).ToList
    Clients.All.SchedulerDisconnected(Context.ConnectionId)
    If schedulerConnections.Count > 0 Then
      schedulerConnections.RemoveAll(Function(d As ResourceSchedulerConnection) CompareSafe(d.ConnectionID, Context.ConnectionId))
    End If

    Dim synergyConnections As List(Of SynergyImporterConnection) = ConnectedSynergyUsers.Where(Function(d As SynergyImporterConnection) CompareSafe(d.ConnectionID, Context.ConnectionId)).ToList
    Clients.All.SchedulerDisconnected(Context.ConnectionId)
    If synergyConnections.Count > 0 Then
      synergyConnections.RemoveAll(Function(d As SynergyImporterConnection) CompareSafe(d.ConnectionID, Context.ConnectionId))
    End If

    Dim siteConnections As List(Of SiteConnection) = ConnectedUsers.Where(Function(d As SiteConnection) CompareSafe(d.ConnectionID, Context.ConnectionId)).ToList
    If siteConnections.Count > 0 Then
      siteConnections.RemoveAll(Function(d As SiteConnection) CompareSafe(d.ConnectionID, Context.ConnectionId))
    End If

    Return MyBase.OnDisconnected(stopCalled)

  End Function

  Public Overrides Function OnReconnected() As Threading.Tasks.Task
    Return MyBase.OnReconnected()
  End Function

#Region " General "

  Public Shared Function DeserialiseJSON(Of TObj)(json As String) As TObj
    Using mstream As New MemoryStream(Encoding.Unicode.GetBytes(json))
      Dim serializer As New DataContractJsonSerializer(GetType(TObj))
      Return DirectCast(serializer.ReadObject(mstream), TObj)
    End Using
  End Function

  Private Function CreateDataTable(Of T)(list As IEnumerable(Of T)) As DataTable
    Dim tp As Type = GetType(T)
    Dim properties = tp.GetProperties()
    Dim dataTable As DataTable = New DataTable()
    For Each propInfo As PropertyInfo In properties
      dataTable.Columns.Add(New DataColumn(propInfo.Name, propInfo.PropertyType))
    Next
    For Each entity As T In list
      Dim values(properties.Length - 1) As Object '= {}
      For i As Integer = 0 To properties.Length - 1 Step 1
        values(i) = properties(i).GetValue(entity)
      Next
      dataTable.Rows.Add(values)
    Next
    Return dataTable
  End Function

#End Region

#Region " Site "

  Public Class SiteConnection
    Public Property ConnectionID As String
    Public Property UserID As Integer
    Public Property Username As String
  End Class

  Public Class ConnectedUserDetail
    Public Property UserID As Integer
    Public Property Username As String
    Public Property ImageName As String
    Public Property ConnectionID As String
    Public Property ResourceSchedulerID As Integer
  End Class

  Public Shared Property ConnectedUsers As New List(Of SiteConnection)

  Public Sub RegisterWithSiteHub(obj As String)
    Dim usr As SiteConnection = DeserialiseJSON(Of SiteConnection)(obj)
    Dim Cn As Integer = ConnectedUsers.Where(Function(d) d.ConnectionID = usr.ConnectionID).Count
    If ConnectedUsers.Where(Function(d As SiteConnection) CompareSafe(d.UserID, usr.UserID)).Count = 0 Then
      ConnectedUsers.Add(usr)
    End If
    Clients.Caller.RegisteredWithSiteHub()
  End Sub

#End Region

#Region " Notifications "

  Public Sub SendNotifications()

    'Setup the criteria
    Dim crit As New OBLib.Users.ReadOnly.ROUNSummaryList.Criteria()
    Dim UserList As New List(Of Integer)
    For Each conn As SiteConnection In ConnectedUsers
      If Not UserList.Contains(conn.UserID) Then
        UserList.Add(conn.UserID)
      End If
    Next
    crit.UserIDs = UserList
    'Fetch and send the notifications
    Dim UnsentNotifications As OBLib.Users.ReadOnly.ROUNSummaryList = OBLib.Users.ReadOnly.ROUNSummaryList.GetROUNSummaryList(crit)
    Dim serialisedNotifications As String = Singular.Web.Data.JSonWriter.SerialiseObject(UnsentNotifications)
    Clients.All.ReceiveNotifications(serialisedNotifications)

  End Sub

  Public Sub SendSynergyImportNotifications(SynergyImportID As Integer)

    'Setup the criteria
    Dim crit As New OBLib.Users.ReadOnly.ROUNSummaryList.Criteria()
    crit.SynergyImportID = SynergyImportID
    'Fetch and send the notifications
    Dim UnsentNotifications As OBLib.Users.ReadOnly.ROUNSummaryList = OBLib.Users.ReadOnly.ROUNSummaryList.GetROUNSummaryList(crit)
    Dim serialisedNotifications As String = Singular.Web.Data.JSonWriter.SerialiseObject(UnsentNotifications)
    Clients.All.ReceiveNotifications(serialisedNotifications)

  End Sub

  Public Sub GetNotifications(UserID As Integer)

    If UserID > 0 Then
      'Setup the criteria
      Dim crit As New OBLib.Users.ReadOnly.ROUNSummaryList.Criteria()
      crit.UserID = UserID
      'Fetch and send the notifications
      Dim UnsentNotifications As OBLib.Users.ReadOnly.ROUNSummaryList = OBLib.Users.ReadOnly.ROUNSummaryList.GetROUNSummaryList(crit)
      Dim serialisedNotifications As String = Singular.Web.Data.JSonWriter.SerialiseObject(UnsentNotifications)
      Clients.All.ReceiveNotifications(serialisedNotifications)
    End If

  End Sub

#End Region

#Region " Resources "

  Public Class ResourceSchedulerConnection
    Public Property ConnectionID As String
    Public Property ResourceSchedulerID As Integer
    Public Property UserID As Integer
    Public Property Username As String
  End Class
  Public Shared Property ConnectedResourceSchedulers As New List(Of ResourceSchedulerConnection)

  Public Class EditDetails
    Public Property ResourceBookingID As Integer?
    Public Property Username As String
    Public Property UserID As Integer?
  End Class

  Public Class ChangeStatusRequest
    Public Property ResourceBookingID As Integer = 0
    Public Property ResourceBookingTypeID As Integer = 0
    Public Property ProductionSystemAreaID As Integer = 0
    Public Property NewStatusID As Integer = 0
    Public Property UserID As Integer = 0
  End Class

  Public Class UpdateHumanResourceStatDetail
    Public Property HumanResourceID As Integer?
    Public Property BookingDate As Date?
    Public Property UserID As Integer?
  End Class

  Public Sub RegisterWithResourceScheduler(obj As String)
    Dim usr As ResourceSchedulerConnection = DeserialiseJSON(Of ResourceSchedulerConnection)(obj)
    'Dim Cn As Integer = ConnectedResourceSchedulers.Where(Function(d) d.ConnectionID = usr.ConnectionID).Count
    If ConnectedResourceSchedulers.Where(Function(d As ResourceSchedulerConnection) CompareSafe(d.UserID, usr.UserID)).Count = 0 Then
      ConnectedResourceSchedulers.Add(usr)
    End If
    'Notify myself / my page
    Clients.Caller.RegisteredWithResourceScheduler()
    'Notify everyone else
    'Dim usersToNotify As List(Of ResourceSchedulerConnection) = ConnectedResourceSchedulers.Where(Function(d As ResourceSchedulerConnection) CompareSafe(d.ResourceSchedulerID, usr.ResourceSchedulerID) _
    '                                                                                                                                         AndAlso Not CompareSafe(d.UserID, usr.UserID)).ToList

    'If usersToNotify.Count > 0 Then
    Dim results As New List(Of ConnectedUserDetail)
    results.Add(New ConnectedUserDetail With {
                                              .UserID = usr.UserID,
                                              .Username = usr.Username,
                                              .ImageName = usr.UserID.ToString & "_2.jpg",
                                              .ConnectionID = usr.ConnectionID,
                                              .ResourceSchedulerID = usr.ResourceSchedulerID
                                             })
    Dim dataString = Singular.Web.Data.JSonWriter.SerialiseObject(results)
    Clients.AllExcept({Context.ConnectionId}).SchedulerConnected(dataString)
    'Clients(usersToNotify.Select(Function(d) d.ConnectionID).ToList)
    'End If

  End Sub

  Public Sub GetConnectedSchedulers(ResourceSchedulerID As String)

    Dim results As New List(Of ConnectedUserDetail)
    ConnectedResourceSchedulers.ForEach(Sub(connectedScheduler)
                                          If CompareSafe(connectedScheduler.ResourceSchedulerID, CType(ResourceSchedulerID, Integer)) Then
                                            results.Add(New ConnectedUserDetail With {
                                                        .UserID = connectedScheduler.UserID,
                                                        .Username = connectedScheduler.Username,
                                                        .ImageName = connectedScheduler.UserID.ToString & "_2.jpg",
                                                        .ConnectionID = connectedScheduler.ConnectionID,
                                                        .ResourceSchedulerID = connectedScheduler.ResourceSchedulerID
                                                        })
                                          End If
                                        End Sub)

    Dim obj As String = Singular.Web.Data.JSonWriter.SerialiseObject(results)
    Clients.Caller.ReceiveConnectedSchedulers(obj)

  End Sub

  Public Sub ChangeMade()

    Dim SerialisedChanges As String = OBLib.Resources.ResourceBookingList.GetSerialisedChanges(True, True)
    Clients.All.ReceiveChanges(SerialisedChanges)
    Clients.All.ReceiveUpdatedBookings(SerialisedChanges)

  End Sub

  Public Sub BookingUpdated()

    'Dim SerialisedChanges As String = OBLib.Resources.ResourceBookingList.GetSerialisedChanges(True)
    'Clients.All.ReceiveUpdatedBookings(SerialisedChanges)

  End Sub

  Public Sub SetEdit(obj As String)
    Dim editDetails As EditDetails = DeserialiseJSON(Of EditDetails)(obj)
    Dim cmd As New Singular.CommandProc("cmdProcs.cmdSetResourceBookingInEdit",
                                        New String() {"@ResourceBookingID", "@CurrentUserID", "@CurrentUser"},
                                        New Object() {editDetails.ResourceBookingID,
                                                      editDetails.UserID,
                                                      editDetails.Username})
    cmd = cmd.Execute()
    ChangeMade()
  End Sub

  Public Sub ReleaseEdit(obj As String)
    Dim editDetails As EditDetails = DeserialiseJSON(Of EditDetails)(obj)
    Dim cmd As New Singular.CommandProc("cmdProcs.cmdSetResourceBookingOutOfEdit",
                                        New String() {"@ResourceBookingID", "@CurrentUserID", "@CurrentUser"},
                                        New Object() {editDetails.ResourceBookingID,
                                                      editDetails.UserID,
                                                      editDetails.Username})
    cmd = cmd.Execute()
    ChangeMade()
  End Sub

  Public Sub ChangeStatuses(obj As String)
    Dim changeStatesRequests As List(Of ChangeStatusRequest) = DeserialiseJSON(Of List(Of ChangeStatusRequest))(obj)
    Dim dt As DataTable = CreateDataTable(Of ChangeStatusRequest)(changeStatesRequests)
    Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceBookingsUpdateBookingStatuses")
    cmd.AddTableParameter("@ChangeStatusRequests", dt)
    cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    cmd.UseTransaction = True
    Try
      cmd = cmd.Execute()
      ChangeMade()
      Dim feedbackList As List(Of ChangeStatusFeedBack) = CreateFeedbackResults(cmd.Dataset.Tables(0))
      If feedbackList.Count > 0 Then
        SendFeedbackResults(feedbackList)
      End If
    Catch ex As Exception
      OBLib.OBMisc.LogClientError("SiteHub", "SiteHub", "ChangeStatuses", ex.Message)
    End Try
  End Sub

  Private Class ChangeStatusFeedBack

    Public Property FeedbackTypeID As Integer?
    Public Property FeedID As Integer?
    Public Property FeedDate As DateTime?
    Public Property FeedbackInfo As String = ""

    Public Sub New()

    End Sub

    Public Sub New(dr As DataRow)
      FeedbackTypeID = dr(0)
      FeedID = dr(1)
      FeedDate = dr(2)
      FeedbackInfo = dr(3)
    End Sub

  End Class

  Private Function CreateFeedbackResults(resultContainer As DataTable) As List(Of ChangeStatusFeedBack)
    Dim feedbackList As New List(Of ChangeStatusFeedBack)
    For Each dr As DataRow In resultContainer.Rows
      feedbackList.Add(New ChangeStatusFeedBack(dr))
    Next
    Return feedbackList
  End Function

  Private Sub SendFeedbackResults(feedbackList As List(Of ChangeStatusFeedBack))
    feedbackList.ForEach(Sub(fb)
                           If CompareSafe(fb.FeedbackTypeID, CInt(FeedBackType.EquipmentScheduleStatusChange)) Then
                             OBLib.ServiceHelpers.SSWSDHelpers.SendFeedChangeNotification(fb.FeedID, fb.FeedDate, fb.FeedbackInfo)
                           End If
                         End Sub)
  End Sub

  Private Enum FeedBackType
    EquipmentScheduleStatusChange = 1
  End Enum

#End Region

#Region " Timesheets "

  Public Sub UpdateHumanResourceStats(obj As String)
    Dim editDetails As UpdateHumanResourceStatDetail = DeserialiseJSON(Of UpdateHumanResourceStatDetail)(obj)
    OBLib.Helpers.TimesheetsHelper.UpdateHumanResourceTimesheet(editDetails.BookingDate, editDetails.HumanResourceID, editDetails.UserID)
    NotifyHumanResourceStatsUpdate(obj)
  End Sub

  Public Sub NotifyHumanResourceStatsUpdate(obj As String)
    Dim editDetails As UpdateHumanResourceStatDetail = DeserialiseJSON(Of UpdateHumanResourceStatDetail)(obj)
    Dim statList As OBLib.HR.ReadOnly.ROHRTimesheetRequirementStatList = OBLib.HR.ReadOnly.ROHRTimesheetRequirementStatList.GetROHRTimesheetRequirementStatList(editDetails.HumanResourceID, Nothing, editDetails.BookingDate)
    Dim wr As Singular.Web.Result = New Singular.Web.Result(True)
    wr.Data = statList
    Dim serialisedResult As String = Singular.Web.Data.JSonWriter.SerialiseObject(wr)
    Clients.All.ReceiveUpdatedTimesheetStats(serialisedResult)
  End Sub

#End Region

#Region " Synergy "

  Public Class SynergyImporterConnection
    Public Property ConnectionID As String
    Public Property UserID As Integer
    Public Property Username As String
  End Class

  Public Shared Property ConnectedSynergyUsers As New List(Of SynergyImporterConnection)

  Public Sub RegisterWithSynergyImporter(obj As String)

    'check if the user must be added to the list
    Dim usr As SynergyImporterConnection = DeserialiseJSON(Of SynergyImporterConnection)(obj)
    If ConnectedSynergyUsers.Where(Function(d As SynergyImporterConnection) CompareSafe(d.UserID, usr.UserID)).Count = 0 Then
      ConnectedSynergyUsers.Add(usr)
    End If

    'Notify myself / my page
    Clients.Caller.RegisteredWithSynergyImporter()

    'Notify everyone else
    Dim results As New List(Of ConnectedUserDetail)
    results.Add(New ConnectedUserDetail With {
                                              .UserID = usr.UserID,
                                              .Username = usr.Username,
                                              .ImageName = usr.UserID.ToString & "_2.jpg",
                                              .ConnectionID = usr.ConnectionID,
                                              .ResourceSchedulerID = Nothing
                                             })
    Dim dataString = Singular.Web.Data.JSonWriter.SerialiseObject(results)
    Clients.AllExcept({Context.ConnectionId}).SchedulerConnected(dataString)

  End Sub

  Public Sub GetOnlineSynergyUsers()

    Dim results As New List(Of ConnectedUserDetail)
    ConnectedSynergyUsers.ForEach(Sub(connectedUser)
                                    results.Add(New ConnectedUserDetail With {
                                                      .UserID = connectedUser.UserID,
                                                      .Username = connectedUser.Username,
                                                      .ImageName = connectedUser.UserID.ToString & "_2.jpg",
                                                      .ConnectionID = connectedUser.ConnectionID
                                                      })
                                  End Sub)

    Dim obj As String = Singular.Web.Data.JSonWriter.SerialiseObject(results)
    Clients.Caller.ReceiveOnlineSynergyUsers(obj)

  End Sub

  Public Sub SendSynergyChanges(SynergyImportID As String)

    Dim SynergyChangeSummaryList As OBLib.Synergy.SynergyChangeSummaryList = OBLib.Synergy.SynergyChangeSummaryList.GetSynergyChangeSummaryList(CType(SynergyImportID, Integer))
    Dim SynergyChangeSummary As OBLib.Synergy.SynergyChangeSummary = SynergyChangeSummaryList(0)
    Dim obj As String = Singular.Web.Data.JSonWriter.SerialiseObject(SynergyChangeSummary)
    Clients.All.ReceiveSynergyChanges(obj)
    'Clients.Clients(ConnectedSynergyUsers.Select(Function(d) d.ConnectionID).ToList).ReceiveSynergyChanges(obj)

  End Sub

  Public Sub SendRoomScheduleImported(conn As String, obj As String)

    Try
      Clients.AllExcept(New String() {conn}).ReceiveRoomScheduleImported(obj)
    Catch ex As Exception
      OBLib.OBMisc.LogClientError("SiteHub", "SiteHub", "SendRoomScheduleImported", ex.Message)
    End Try

  End Sub

  Public Sub SendRoomScheduleAreaRemoved(conn As String, obj As String)

    Try
      Clients.AllExcept(New String() {conn}).ReceiveRoomScheduleAreaRemoved(obj)
    Catch ex As Exception
      OBLib.OBMisc.LogClientError("SiteHub", "SiteHub", "SendRoomScheduleImported", ex.Message)
    End Try

  End Sub

#End Region

End Class
