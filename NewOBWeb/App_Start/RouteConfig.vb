﻿Imports System.Web.Routing
Imports Microsoft.AspNet.FriendlyUrls
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc

Public Module RouteConfig
    Sub RegisterRoutes(ByVal routes As RouteCollection)
    'routes.EnableFriendlyUrls()

    routes.IgnoreRoute("{resource}.axd/{*pathInfo}")

    routes.MapRoute( _
        name:="Default", _
        url:="signalr/hubs/{id}", _
            defaults:=New With {.controller = "Home", .action = "Index", .id = UrlParameter.Optional} _
    )
    End Sub
End Module
