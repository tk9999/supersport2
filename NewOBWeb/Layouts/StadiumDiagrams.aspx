﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="StadiumDiagrams.aspx.vb" Inherits="NewOBWeb.StadiumDiagrams" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <style type="text/css">
    .resize-container {
      position: relative;
      display: inline-block;
      cursor: move;
      margin: 0 auto;
    }

      .resize-container img {
        display: block;
      }

      .resize-container:hover img,
      .resize-container:active img {
        outline: 2px dashed rgba(222,60,80,.9);
      }

    .resize-handle-ne,
    .resize-handle-ne,
    .resize-handle-se,
    .resize-handle-nw,
    .resize-handle-sw {
      position: absolute;
      display: block;
      width: 10px;
      height: 10px;
      background: rgba(222,60,80,.9);
      z-index: 999;
    }

    .resize-handle-nw {
      top: -5px;
      left: -5px;
      cursor: nw-resize;
    }

    .resize-handle-sw {
      bottom: -5px;
      left: -5px;
      cursor: sw-resize;
    }

    .resize-handle-ne {
      top: -5px;
      right: -5px;
      cursor: ne-resize;
    }

    .resize-handle-se {
      bottom: -5px;
      right: -5px;
      cursor: se-resize;
    }
  </style>
  <script type="text/javascript">

    Singular.HandleFileSelected = function () { };

    Singular.OnPageLoad(function () {

      var resizeableImage = function (image_target, url) {

        var $container,
        orig_src = new Image(),
        image_target = $(image_target).get(0),
        event_state = {},
        constrain = false,
        min_width = 1024,
        min_height = 768,
        max_width = 1680,
        max_height = 1050,
        resize_canvas = document.createElement('canvas');

        startResize = function (e) {
          e.preventDefault();
          e.stopPropagation();
          saveEventState(e);
          $(document).on('mousemove', resizing);
          $(document).on('mouseup', endResize);
        };

        endResize = function (e) {
          e.preventDefault();
          $(document).off('mouseup touchend', endResize);
          $(document).off('mousemove touchmove', resizing);
        };

        saveEventState = function (e) {
          // Save the initial event details and container state
          event_state.container_width = $container.width();
          event_state.container_height = $container.height();
          event_state.container_left = $container.offset().left;
          event_state.container_top = $container.offset().top;
          event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
          event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

          // This is a fix for mobile safari
          // For some reason it does not allow a direct copy of the touches property
          if (typeof e.originalEvent.touches !== 'undefined') {
            event_state.touches = [];
            $.each(e.originalEvent.touches, function (i, ob) {
              event_state.touches[i] = {};
              event_state.touches[i].clientX = 0 + ob.clientX;
              event_state.touches[i].clientY = 0 + ob.clientY;
            });
          }
          event_state.evnt = e;
        };

        resizing = function (e) {
          var mouse = {}, width, height, left, top, offset = $container.offset();
          mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
          mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

          width = mouse.x - event_state.container_left;
          height = mouse.y - event_state.container_top;
          left = event_state.container_left;
          top = event_state.container_top;

          if (constrain || e.shiftKey) {
            height = width / orig_src.width * orig_src.height;
          }

          if (width > min_width && height > min_height && width < max_width && height < max_height) {
            resizeImage(width, height);
            // Without this Firefox will not re-calculate the the image dimensions until drag end
            $container.offset({ 'left': left, 'top': top });
          }
        };

        resizeImage = function (width, height) {
          resize_canvas.width = width;
          resize_canvas.height = height;
          resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);
          $(image_target).attr('src', resize_canvas.toDataURL("image/png"));
        };

        init = function () {

          // Create a new image with a copy of the original src
          // When resizing, we will always use this original copy as the base
          orig_src.src = image_target.src;

          // Add resize handles
          $(image_target).wrap('<div class="resize-container"></div>')
          .before('<span class="resize-handle resize-handle-nw"></span>')
          .before('<span class="resize-handle resize-handle-ne"></span>')
          .after('<span class="resize-handle resize-handle-se"></span>')
          .after('<span class="resize-handle resize-handle-sw"></span>');

          // Get a variable for the container
          $container = $(image_target).parent('.resize-container');

          // Add events
          $container.on('mousedown', '.resize-handle', startResize);

          if (url) {
            $(image_target).attr('src', url);
          }

        };

        init();

      };

      function imageSelected(e) {

        var url = null;
        var fr = new FileReader();
        fr.onload = function (e) {
          var img = document.getElementById("diagramImage");
          img = new Image();
          img.onload = function () {
            c = document.createElement("canvas");
            c.width = ViewModel.Width();
            c.height = ViewModel.Height();
            c.getContext("2d").drawImage(this, 0, 0, ViewModel.Width(), ViewModel.Height());
            this.src = c.toDataURL();
            //document.body.appendChild(this);
            console.log(c.toDataURL("image/png"));
            url = c.toDataURL("image/png");
            resizeableImage($('.resize-image'), url);
          }
          img.src = e.target.result;
        };
        fr.readAsDataURL(e.target.files[0]);
      };

      document.getElementById('diagram').addEventListener('change', imageSelected, false);

    });

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    Using h = Helpers
      
      '----Toolbar------------------------------------------------------------------------------------------------------------------
      'With h.Bootstrap.Row
      '  With .Helpers.Bootstrap.FlatBlock("Camera Position Layouts", False)
      '    With .ContentTag
      '      With .Helpers.Bootstrap.Row
      '        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '          With .Helpers.Bootstrap.Button(, "Find Production", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindProduction()")
        
      '          End With
      '          With .Helpers.Bootstrap.Button(, "Find Position Layout", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindPositionLayout()")
          
      '          End With
      '          With .Helpers.Bootstrap.Button(, "New Position Layout", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.None, "UploadImage()")
               
      '          End With
      '          With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
               
      '          End With
      '          With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
               
      '          End With
      '        End With
      '        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '          With .Helpers.Toolbar
      '            .Helpers.MessageHolder()
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With

      '----Image------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Layout", False)
          With .ContentTag
            With .Helpers.HTMLTag("input")
              .Attributes("type") = "file"
              .Attributes("id") = "diagram"
            End With
            'Image
            With .Helpers.DivC("component")
              With .Helpers.HTMLTag("img")
                .AddClass("resize-image")
                .Attributes("id") = "diagramImage"
                .Attributes("alt") = "image for resizing"
              End With
            End With
            'With .Helpers.DivC("canvas-container")
            '  With .Helpers.HTMLTag("canvas")
            '    .Attributes("id") = "ImageCanvas"
            '  End With
            'End With
            'With .Helpers.DivC("component")
            '  With .Helpers.DivC("overlay")
            '    With .Helpers.DivC("overlay-inner")
            '      With .Helpers.DivC("resize-container")
            '        With .Helpers.HTMLTag("span")
            '          .AddClass("resize-handle resize-handle-nw")
            '        End With
            '        With .Helpers.HTMLTag("span")
            '          .AddClass("resize-handle resize-handle-ne")
            '        End With

            '        With .Helpers.HTMLTag("span")
            '          .AddClass("resize-handle resize-handle-sw")
            '        End With
            '        With .Helpers.HTMLTag("span")
            '          .AddClass("resize-handle resize-handle-se")
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
            '	<button class="btn-crop js-crop">Crop<img class="icon-crop" src="img/crop.svg"></button>
          End With
        End With
      End With

      
    End Using
  %>
</asp:Content>
