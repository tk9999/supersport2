﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports OBLib.Productions.ReadOnly
Imports OBLib.PositionLayouts
Imports System

Public Class AudioPositionLayouts
  Inherits OBPageBase(Of AudioPositionLayoutsVM)

End Class

Public Class AudioPositionLayoutsVM
  Inherits OBViewModel(Of AudioPositionLayoutsVM)




  Public Property PositionLayoutList As OBLib.PositionLayouts.PositionLayoutList
  Public Property ROPositionLayoutList As OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList
  Public Property ROProductionVenueImageList As OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList
  Public Property ROProduction As ROProduction
  Private mROProductionList As ROProductionList

  Public Property CurrPositionLayout As OBLib.PositionLayouts.PositionLayout

  Public ReadOnly Property ROProductionList As ROProductionList
    Get
      Return mROProductionList
    End Get
  End Property

  Public Property ProductionName As String


  Public Property ROProductionVenueImageListCriteria As OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList.Criteria
  Public Property ROProductionVenueImageListPagingManager As Singular.Web.Data.PagedDataManager(Of AudioPositionLayoutsVM)

  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of AudioPositionLayoutsVM)

  Public Property ROPositionLayoutListCriteria As OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList.Criteria
  Public Property ROPositionLayoutListPagingManager As Singular.Web.Data.PagedDataManager(Of AudioPositionLayoutsVM)

  Protected Overrides Sub Setup()
    MyBase.PreSetup()

    mROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
    ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
    ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of AudioPositionLayoutsVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "TxStartDateTime", 20)
    ROProductionListCriteria.TxDateFrom = Now.Date.AddDays(-7)
    ROProductionListCriteria.TxDateTo = Now.Date.AddMonths(1)
    ROProductionListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROProductionListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    ROProductionListCriteria.PageNo = 1
    ROProductionListCriteria.PageSize = 20
    ROProductionListCriteria.SortAsc = True
    ROProductionListCriteria.SortColumn = "TxStartDateTime"

    ROProductionListCriteria.TxDateFrom = Singular.Dates.DateMonthStart(Now).AddDays(-7)
    ROProductionListCriteria.TxDateTo = Singular.Dates.DateMonthEnd(Now.AddMonths(1))
    ROProductionListCriteria.EventManagerID = OBLib.Security.Settings.CurrentUser.HumanResourceID
    mROProductionList = OBLib.Productions.[ReadOnly].ROProductionList.GetROProductionList(ROProductionListCriteria.ProductionTypeID, ROProductionListCriteria.EventTypeID, ROProductionListCriteria.ProductionVenueID,
                                                                                          Nothing, Nothing, Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID, _
                                                                                          ROProductionListCriteria.TxDateFrom, ROProductionListCriteria.TxDateTo, ROProductionListCriteria.EventManagerID,
                                                                                          ROProductionListCriteria.ProductionRefNo, Nothing, OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                                          ROProductionListCriteria.PageNo, ROProductionListCriteria.PageSize,
                                                                                          ROProductionListCriteria.SortAsc, ROProductionListCriteria.SortColumn)

    ROPositionLayoutList = New OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList
    ROPositionLayoutListCriteria = New OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList.Criteria
    ROPositionLayoutListPagingManager = New Singular.Web.Data.PagedDataManager(Of AudioPositionLayoutsVM)(Function(d) Me.ROPositionLayoutList, Function(d) Me.ROPositionLayoutListCriteria, "LayoutName", 20)
    ROPositionLayoutListCriteria.PositionLayoutType = CommonData.Enums.PositionLayoutType.Audio
    ROPositionLayoutListCriteria.PageNo = 1
    ROPositionLayoutListCriteria.PageSize = 20
    ROPositionLayoutListCriteria.SortAsc = True
    ROPositionLayoutListCriteria.SortColumn = "LayoutName"

    ROProductionVenueImageList = New OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList
    ROProductionVenueImageListCriteria = New OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList.Criteria
    ROProductionVenueImageListPagingManager = New Singular.Web.Data.PagedDataManager(Of AudioPositionLayoutsVM)(Function(d) Me.ROProductionVenueImageList, Function(d) Me.ROProductionVenueImageListCriteria, "ImageName", 20)

    ROProductionVenueImageListCriteria.PageNo = 1
    ROProductionVenueImageListCriteria.PageSize = 20
    ROProductionVenueImageListCriteria.SortAsc = True
    ROProductionVenueImageListCriteria.SortColumn = "ImageName"



    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
     
    MessageFadeTime = 2000
    ClientDataProvider.AddDataSource("TypePositionlist", CommonData.Lists.ROPositionList.FilterList("DisciplineID", CommonData.Enums.Discipline.AudioAssistant), False)
    ClientDataProvider.AddDataSource("MainEquipmentSubType", CommonData.Lists.ROEquipmentSubTypeList.FilterList("EquipmentTypeID", CommonData.Enums.EquipmentType.FXMics, CommonData.Enums.EquipmentType.PresentationMics, CommonData.Enums.EquipmentType.RadioMics, CommonData.Enums.EquipmentType.Floormonitors), False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"

        If ROProduction IsNot Nothing Then

          CurrPositionLayout.ProductionID = ROProduction.ProductionID
        End If

        If Not CurrPositionLayout.IsValid Then
          AddMessage(Singular.Web.MessageType.Validation, "Valid", CurrPositionLayout.BrokenRulesCollection(0).Description)
          Exit Select
        End If
        With CurrPositionLayout.TrySave(GetType(PositionLayoutList))
          If .Success Then
            CurrPositionLayout = .SavedObject
            AddMessage(Singular.Web.MessageType.Success, "Save", "Layout Saved Successfully")
          Else
            AddMessage(Singular.Web.MessageType.Error, "Save", "Layout Save Error. " & .ErrorText)
          End If
        End With
      Case "Delete"
        Try
          Dim PositionLayoutList As New PositionLayoutList()
          PositionLayoutList.Add(CurrPositionLayout)
          PositionLayoutList.Remove(CurrPositionLayout)
          PositionLayoutList = TrySave(PositionLayoutList).SavedObject
          CurrPositionLayout = Nothing
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try
      Case "EditPositionLayout"

        CurrPositionLayout = PositionLayoutList.GetPositionLayoutList(Nothing, CommandArgs.ClientArgs.PositionLayoutID).FirstOrDefault
      Case "NewPositionLayout"
        CurrPositionLayout = OBLib.PositionLayouts.PositionLayout.NewPositionLayout
        CurrPositionLayout.PositionLayoutTypeID = CommonData.Enums.PositionLayoutType.Audio
        CurrPositionLayout.CheckAllRules()
         
    End Select
  End Sub
    
End Class