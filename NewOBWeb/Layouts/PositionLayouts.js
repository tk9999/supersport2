﻿

function InitialiseCameras(ContainerID, Obj) {

  LoadCameras(ContainerID);

  for (i = 0; i < Obj.length; i++) {
    new Camera(Obj[i]);
  }
  

}

function GetCameraObjects() {

  var ObjList = [];

  for (i = 0; i < window.CameraList.length; i++) {
    ObjList.push(window.CameraList[i].Obj);
  }

  return ObjList;

}

function LoadCameras(ContainerID)
{
  window.CameraList = [];
  window.Main = document.getElementById(ContainerID);
  window.Main.innerHTML = '';
  window.Main.style.position = 'relative';

  window.Main.oncontextmenu = function () { return false };

  window.Main.addEventListener('mousemove', function (event) {

    var doc = document.documentElement;
    var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    var x = event.clientX - parseInt($(window.Main).offset().left) + left;
    var y = event.clientY - parseInt($(window.Main).offset().top) + top;
    //y = event.clientY;
    //var x = event.x;
    //var y = event.y;
    for (i = 0; i < window.CameraList.length; i++) {
      window.CameraList[i].Move(x, y);
      window.CameraList[i].Rotate(x, y);
    }

  }, true);

  window.Main.addEventListener('mouseup', function (event) {

    for (i = 0; i < window.CameraList.length; i++) {
      var Camera = window.CameraList[i];
      Camera.MoveDown = false;
      Camera.RotateDown = false;
    }

  }, true);

}

function Camera(Obj) {

  if (!window.CameraCounter) {
    window.CameraCounter = 0;
  }

  window.CameraCounter = window.CameraCounter + 1;

  this.Obj = Obj;
  this.XPos = Obj.XPosServer();
  this.YPos = Obj.YPosServer();
  this.Angle = Obj.RotationServer();
  this.MoveDown = false;
  this.RotateDown = false;
  this.div = document.createElement('div');
  this.div.className = 'Camera'
  this.div.id = 'Camera' + Obj.PositionLayoutPositionID().toString();
  this.div.style.position = 'absolute'
  window.Main.appendChild(this.div);
  window.CameraList.push(this);


  var Me = this;

  this.Update = function ()
  {

    Me.div.style.marginLeft = Me.XPos + 'px';
    Me.div.style.marginTop = Me.YPos + 'px';
    Me.div.style.transform = 'rotate(' + Me.Angle + 'deg)';
    Obj.XPosServer(Me.XPos);
    Obj.YPosServer(Me.YPos);
    Obj.RotationServer(Me.Angle);

  }

  this.Rotate = function (x, y) {

    if (!Me.RotateDown) {
      return;
    }

    var cX = (x - (Me.div.clientWidth / 2)) - Me.XPos;
    var cY = (y - (Me.div.clientHeight / 2)) - Me.YPos;

    var theta = Math.atan((cY / cX));

    var Angle = parseInt((theta * 180) / Math.PI) + 90;

    if (cX < 0) {

      Angle = Angle + 180;

    }

    Me.Angle = Angle;
    Me.Obj.Angle = Angle;

    Me.Update();
  }

  this.Move = function (x, y) {

    if (!Me.MoveDown) {
      return;
    }

    if ((x - (Me.div.clientWidth / 2)) < 0) {
      return;
    }

    if ((y - (Me.div.clientHeight / 2)) < 0) {
      return;
    }

    if ((x + (Me.div.clientWidth / 2)) > parseInt(window.Main.clientWidth)) {
      return;
    }

    if ((y + (Me.div.clientHeight / 2)) > parseInt(window.Main.clientHeight)) {
      return;
    }

    Me.XPos = x - (Me.div.clientWidth / 2);
    Me.YPos = y - (Me.div.clientHeight / 2);
    Me.Obj.XPos = Me.XPos;
    Me.Obj.YPos = Me.YPos;
    Me.Update();

   


  }

  $('#' + this.div.id).mousedown(function (event) {

    //Left Mouse click
    if (event.which == 1) {
      Me.MoveDown = true;
      return;
    }

    //Right Mouse click
    if (event.which == 3) {
      Me.RotateDown = true;
    }

  });

  this.Update();

}

CameraPositionLayoutsVMBO = {
  SelectedProductionNameValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.SelectedOption() == 2 && CtlError.Object.SelectedProductionName().trim().length == 0) {
      CtlError.AddError('Production is required');
    }
  },
  SelectedTemplateNameValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.SelectedOption() == 2 && CtlError.Object.SelectedTemplateLayoutName().trim().length == 0) {
      CtlError.AddError('Template is required');
    }
  }
};