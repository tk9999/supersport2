﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AudioPositionLayouts.aspx.vb"
  Inherits="NewOBWeb.AudioPositionLayouts" %>
 
<%@ Import Namespace="OBLib.PositionLayouts.ReadOnly" %>

<%@ Import Namespace="OBLib.PositionLayouts" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
    <script src="PositionLayouts.js" type="text/javascript"></script>
  <style type="text/css">
    div.ComboDropDown {
      z-index: 35000;
    }

    body, #CurrentAttendees {
      background-color: #F0F0F0;
    }

   .Camera
    {
      width: 50px;
      height: 50px;
      background-image: url('../Images/CameraImage.png');
      background-size:100%;
    }

    #Main
    {
      background-color: #F0F0F0;
    }
  </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Audio Position Layouts", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Button(, "Find Position Layout", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindPositionLayout()")
          
                End With
                With .Helpers.Bootstrap.Button(, "Find Production", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindProduction()")
        
                End With
                With .Helpers.Bootstrap.Button(, "New Position Layout", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.None, "NewPositionLayout()")
               
                End With
                With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
               
                End With
                With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
               
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Toolbar
                  .Helpers.MessageHolder()
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----Image------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Layout", False)
          With .ContentTag
            .Style.Width = "1400px"
            .Style.Height = "800px"
            .AddBinding(Singular.Web.KnockoutBindingString.style, "LayoutBackgroundImage()")
            .Attributes("id") = "Main"
          End With
        End With
      End With
      
      '----Positions------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Positions", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.TableFor(Of PositionLayoutPosition)(Function(d) ViewModel.CurrPositionLayout.PositionLayoutPositionList, True, True, False, , True, True, False)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                With .FirstRow
                  .AddClass("items")
                  With .AddColumn(Function(d) d.PositionID)
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                  End With
                  With .AddColumn(Function(d) d.MainEquipmentSubTypeID)
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                  End With
                  With .AddColumn(Function(d) d.LensEquipmentSubTypeID, "Lens")
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                  End With
                  With .AddColumn(Function(d) d.TripodEquipmentSubTypeID, "Tripod")
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                  End With
                  With .AddColumn(Function(d) d.EVSPositionID, "EVS")
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                  End With
                  With .AddColumn(Function(d) d.Notes)
                    ' .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                  End With
                  .AddColumn(Function(d) d.XPosServer)
                  .AddColumn(Function(d) d.YPosServer)
                End With
                '----Child List------------------------------------------------------------------------------------------------
                With .AddChildTable(Of PositionLayoutSubPosition)(Function(c) c.PositionLayoutSubPositionList, True, True)
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn(Function(d) d.PositionCode)
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.TripodEquipmentSubTypeID, "Tripod")
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.Notes)
                      .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-8 text-left")
                
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      'With h.Bootstrap.Row
      '  .Helpers.HTML.Heading2("Audio Position Layout")
      '  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '    With .Helpers.Bootstrap.Button(, "New Position Layout", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.None, "NewPositionLayout()")
               
      '    End With
      '    With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
               
      '    End With
                    
      '    With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
               
      '    End With
      '    With .Helpers.Bootstrap.Button(, "Find Position Layout", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindPositionLayout()")
      '    End With
                
      '  End With
      '  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '    With .Helpers.Toolbar
      '      .Helpers.MessageHolder()
            
      '    End With
      '  End With
      'End With
      
    

       
  
      'With h.FieldSet(" ")
      '  h.MessageHolder().AddClass("HoverMsg")
      '  With .Helpers.Bootstrap.Column(12, 6, 3, 3)
         
      '    With .Helpers.Bootstrap.Button(, "Find Production", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-search", , Singular.Web.PostBackType.None, "FindProduction()")
        
      '    End With
      '    ''.Helpers.LabelFor(Function(p) ViewModel.ProductionName)
      '    With .Helpers.ReadOnlyFor(Function(p) ViewModel.ProductionName)
      '      .AddClass("form-control filter-field")
      '      .Style.Padding(0, 0, 20, 0)
      '    End With
      '    'End With
      '  End With
      '  With .Helpers.With(Of PositionLayout)(Function(d) ViewModel.CurrPositionLayout)
      '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
      '      .Helpers.LabelFor(Function(p) p.LayoutName)
      '      With .Helpers.EditorFor(Function(p) p.LayoutName)
      '        '.Attributes("placeholder") = "Filter..."
      '        .AddClass("form-control  input-sm")
      '      End With
      '      .Helpers.LabelFor(Function(p) p.ProductionVenueImageID)
      '      With .Helpers.Bootstrap.FormGroup
              
      '        With .Helpers.EditorFor(Function(p) p.ProductionVenueImageName)
      '          '.AddClass("form-control  input-sm")
      '          .Style.Height = 30
      '          .Style.Width = 340
      '        End With
      '        With .Helpers.BootstrapButton(, "", "btn-primary", "glyphicon-search", Singular.Web.PostBackType.None, False, , )
      '          .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "FindProductionVenueImage()")
      '          .Button.Style.Height = 30
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
      
      'With h.Bootstrap.Row ' Image Row
      '  .Style.Height = 200
        
      'End With
          
      ''------------ Production Dialogs ----------------
      With h.Bootstrap.Dialog("FindProductionDialogs", "Find Production")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          With .Helpers.Bootstrap.FlatBlock("Find Production", True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .AboveContentTag
              .AddClass("form-horizontal")
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.ProductionRefNo)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Reference Number"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateFrom)
                    .AddClass("form-control input-sm")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateTo)
                    .AddClass("form-control input-sm")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventManagerID, "Event Manager", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                    .InputGroup.AddClass("margin-bottom-override")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionTypeID, "Production Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                    .InputGroup.AddClass("margin-bottom-override")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventTypeID, "Event Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                    .InputGroup.AddClass("margin-bottom-override")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionVenueID, "Production Venue", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                    .InputGroup.AddClass("margin-bottom-override")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search by Keyword"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "Refresh()")
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ViewModel.ROProductionListPagingManager,
                                                  Function(vm) ViewModel.ROProductionList,
                                                  False, False, True, False, True, True, False,
                                                  , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    '.AddBinding(Singular.Web.KnockoutBindingString.css, "GetProductionStatColour($data)")
                    With .AddColumn("")
                      .Style.Width = 80
                      .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SelectProduction($data)")
                    End With
                    
                    'With .AddColumn("")
                    '  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    '  With .Helpers.Bootstrap.Anchor(, , , , Singular.Web.LinkTargetType.NotSet, "fa-pencil", )
                    '    .AnchorTag.AddClass("btn btn-sm btn-primary")
                    '    .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.href, "SelectProduction($data)")
                    '  End With
                    'End With
                    With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                    End With
                    With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionType)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c As ROProduction) c.EventType)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionVenue)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.EventManager)
                      .AddClass("col-xs-1 col-sm-2 col-md-2 col-lg-1")
                    End With
                   
                  End With
                  With .FooterRow
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      ''------------ Position Layout Dialogs ----------------
      With h.Bootstrap.Dialog("FindPositionLayoutDialogs", "Find Position Layout")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          With .Helpers.Bootstrap.FlatBlock("Find Layout", True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .AboveContentTag
              .AddClass("form-horizontal")
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROPositionLayoutListCriteria.LayoutName)
                    .Attributes("placeholder") = "Search by Layout Name"
                    .AddClass("form-control input-sm")
                  End With
                End With
              End With

              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                .Helpers.LabelFor(Function(d) d.ROPositionLayoutListCriteria.TemplateInd)
                '.Style.Width = 50
                With .Helpers.BootstrapStateButton("", "TemplateIndClick($data)", "TemplateIndCss($data)", "TemplateIndHtml($data)", False)
                End With
              End With

              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "RefreshROPositionLayoutListPagingManager()")
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROPositionLayout)(Function(vm) ViewModel.ROPositionLayoutListPagingManager,
                                                  Function(vm) ViewModel.ROPositionLayoutList,
                                                  False, False, True, False, True, True, False,
                                                  , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    '.AddClass("items")
                    With .AddColumn("")
                      .Style.Width = 80
                      .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SelectPositionLayout($data)")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.LayoutName, 300)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.ImageName, 150)
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.Production, 300)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.CreatedByName, 150)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
      
                   
                  End With
                  With .FooterRow
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      ''------------ Production Vanue Image Dialogs ----------------
      With h.Bootstrap.Dialog("FindProductionVenueImageDialog", "Find Production Vanue Image ")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          With .Helpers.Bootstrap.FlatBlock("Find Vanue Image ", True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .AboveContentTag
              .AddClass("form-horizontal")
              With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROProductionVenueImageListCriteria.ImageName)
                    .Attributes("placeholder") = "Search by Image Name"
                    .AddClass("form-control input-sm")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "RefreshVenueImagePagingManager()")
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROProductionVenueImage)(Function(vm) ViewModel.ROProductionVenueImageListPagingManager,
                                                  Function(vm) ViewModel.ROProductionVenueImageList,
                                                  False, False, True, False, True, True, False,
                                                  , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    '.AddClass("items")
                    With .AddColumn("")
                      .Style.Width = 80
                      .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SelectProductionVenueImage($data)")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.ImageName, 400)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.ProductionVenue, 400)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                  End With
                  With .FooterRow
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
    End Using
    
  %>

  <script type="text/javascript">

     
     
    function FindProductionVenueImage() {
      $("#FindProductionVenueImageDialog").modal();
    };

    function FindPositionLayout() {
      $("#FindPositionLayoutDialogs").modal();
    }

    function FindProduction() {
      $("#FindProductionDialogs").modal();
    }

    function Refresh() {
      ViewModel.ROProductionListPagingManager().Refresh();
    };

    function RefreshROPositionLayoutListPagingManager() {
      ViewModel.ROPositionLayoutListPagingManager().Refresh();
    };
    function RefreshVenueImagePagingManager() {
      ViewModel.ROProductionVenueImageListPagingManager().Refresh();
    };


    function NewPositionLayout() {
      Singular.SendCommand("NewPositionLayout", {}, function (response) {
        Singular.Validation.CheckRules(ViewModel.CurrPositionLayout());
      })
    };


    function LayoutBackgroundImage() {
      if (ViewModel.CurrPositionLayout()) {
        return { background: 'grey url(' + ViewModel.CurrPositionLayout().ImgString() + ')', padding: '0px' };
      } else {
        return "";
      }
    }


    function SelectProduction(ROProduction) {

      ViewModel.ProductionName(ROProduction.ProductionDescription());
      ViewModel.ROProduction(ROProduction);
      $("#FindProductionDialogs").modal('hide');

    };

    function SelectPositionLayout(ROPositionLayout) {

      Singular.SendCommand("EditPositionLayout",
                               {
                                 PositionLayoutID: ROPositionLayout.PositionLayoutID()
                               },
                               function (response) {
                                 $("#FindPositionLayoutDialogs").modal('hide');
                                 InitialiseCameras('Main', ViewModel.CurrPositionLayout().PositionLayoutPositionList());
                               });

    };

    function SelectProductionVenueImage(ROProductionVenueImage) {

      ViewModel.CurrPositionLayout().ProductionVenueImageID(ROProductionVenueImage.ProductionVenueImageID());
      ViewModel.CurrPositionLayout().ProductionVenueImageName(ROProductionVenueImage.ImageName());
      $("#FindProductionVenueImageDialog").modal('hide');
    };
    
    //Template Ind
    function TemplateIndClick() { 
       
      if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == true) {
        ViewModel.ROPositionLayoutListCriteria().TemplateInd(false);
      }
      else if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == false) {
        ViewModel.ROPositionLayoutListCriteria().TemplateInd(null)
      } else {
        ViewModel.ROPositionLayoutListCriteria().TemplateInd(true)
      }
       
    };

    function TemplateIndCss() {
      if (ViewModel.ROPositionLayoutListCriteria().TemplateInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
    };

    function TemplateIndHtml() {
    
      if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == true) {
        return "<span class='glyphicon glyphicon-check'></span> Yes"
      }
      else if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == false) {
        return "<span class='glyphicon glyphicon-minus'></span> No"
      } else {
        return "All"
      }

    };

  </script>
</asp:Content>
