﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CameraPositionLayouts.aspx.vb"
  Inherits="NewOBWeb.CameraPositionLayouts" %>

<%@ Import Namespace="OBLib.ProductionLayouts" %>

<%@ Import Namespace="OBLib.PositionLayouts.ReadOnly" %>

<%@ Import Namespace="OBLib.PositionLayouts" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <script src="PositionLayouts.js" type="text/javascript"></script>
  <style type="text/css">
    div.ComboDropDown {
      z-index: 35000;
    }

    body, #CurrentAttendees {
      background-color: #F0F0F0;
    }

    .Camera {
      width: 50px;
      height: 50px;
      background-image: url('../Images/CameraImage.png');
      background-size: 100%;
    }

    #Main {
      background-color: #F0F0F0;
    }

    #ROProductionList > tbody {
      height: 280px;
      overflow-y: scroll;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
      
      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Camera Position Layouts", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                
                With .Helpers.Bootstrap.Button(, "Create New Template", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.None, "OptionSelected(1)")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!ViewModel.SelectedLayout()")
                End With
                With .Helpers.Bootstrap.Button(, "Link Template to a Production", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "OptionSelected(2)")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!ViewModel.SelectedLayout()")
                End With
                With .Helpers.With(Of PositionLayout)(Function(d) ViewModel.SelectedLayout)
                  With .Helpers.Bootstrap.Button("SaveCurrentLayout", "Save Current Layout", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.CanViewLayout()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.SelectedLayout().IsValid()")
                  End With
                  With .Helpers.Bootstrap.Button("StartOver", "Start Over", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
               
                  End With
                End With
                'With .Helpers.Bootstrap.Button(, "Find Position Layout", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindPositionLayout()")
          
                'End With
                'With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
               
                'End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Toolbar
                  .Helpers.MessageHolder()
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----New Template---------------------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.SelectedOption() == 1")
        With .Helpers.Bootstrap.FlatBlock("Criteria", False)
          With .ContentTag
            With .Helpers.With(Of PositionLayout)(Function(d) ViewModel.SelectedLayout)
              With .Helpers.Bootstrap.Row
                With .Helpers.Div
                  With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                    .Helpers.Bootstrap.LabelDisplay("Template Name")
                    With .Helpers.Div
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.LayoutName, Singular.Web.BootstrapEnums.InputSize.Small)
                    
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  '  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.SelectedLayout.LayoutName, Singular.Web.BootstrapEnums.InputSize.Small)
                    
                  '  End With
                  'End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Div
                  With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                    .Helpers.Bootstrap.LabelDisplay("Image to Use?")
                  End With
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                      .Helpers.Bootstrap.LabelDisplay("Upload New Image")
                      With .Helpers.Div
                        'With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.SelectedProductionName, "FindProduction()", "Select Production",
                        '                                         , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                        'End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                      .Helpers.Bootstrap.LabelDisplay("Create From Existing Image")
                      With .Helpers.Div
                        With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.SelectedImageName, "FindImage()", "Find Image",
                                               , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----Production Layout Options------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.SelectedOption() == 2")
        With .Helpers.Bootstrap.FlatBlock("Criteria", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                .Helpers.LabelFor(Function(d) d.SelectedProductionName)
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.SelectedProductionName, "FindProduction()", "Select Production",
                                                         , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 4, 4)
              '  With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.SelectedImageName, "FindImage()", "Select Layout",
              '                                           , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-search", True)
              '  End With
              'End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                .Helpers.LabelFor(Function(d) d.SelectedTemplateLayoutName)
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.SelectedTemplateLayoutName, "FindLayoutTemplate()", "Select Template",
                                                         , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----Image----------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.CanViewLayout()")
        With .Helpers.Bootstrap.FlatBlock(, False)
          With .HeaderTag
            .AddBinding(Singular.Web.KnockoutBindingString.text, "LayoutHeader()")
          End With
          With .ContentTag
            .Style("background-repeat") = "no-repeat"
            .AddBinding(Singular.Web.KnockoutBindingString.style, "LayoutBackgroundImage()")
            .Attributes("id") = "Main"
          End With
        End With
      End With
      
      '----Positions------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.CanViewLayout()")
        With .Helpers.Bootstrap.FlatBlock("Positions", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.With(Of PositionLayout)(Function(c) ViewModel.SelectedLayout)
                With .Helpers.Bootstrap.TableFor(Of PositionLayoutPosition)(Function(d) d.PositionLayoutPositionList,
                                                                          True, True, False, , True, True, False)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn(Function(d) d.PositionID)
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.MainEquipmentSubTypeID)
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.LensEquipmentSubTypeID, "Lens")
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.TripodEquipmentSubTypeID, "Tripod")
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.EVSPositionID, "EVS")
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    With .AddColumn(Function(d) d.Notes)
                      ' .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                    End With
                    .AddColumn(Function(d) d.XPosServer)
                    .AddColumn(Function(d) d.YPosServer)
                  End With
                  '----Child List------------------------------------------------------------------------------------------------
                  With .AddChildTable(Of PositionLayoutSubPosition)(Function(c) c.PositionLayoutSubPositionList, True, True)
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn(Function(d) d.PositionCode)
                        .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                      End With
                      With .AddColumn(Function(d) d.TripodEquipmentSubTypeID, "Tripod")
                        .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                      End With
                      With .AddColumn(Function(d) d.Notes)
                        .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-8 text-left")
                
                      End With
                    End With
                  End With
                End With
              End With
              

            End With
          End With
        End With
      End With
          
      ''------------ Production Dialogs ----------------
      With h.Bootstrap.Dialog("FindProductionDialog", "Find Production")
        .ModalDialogDiv.AddClass("modal-lg")
        With .Body
          
          With .Helpers.Bootstrap.Row
            .AddClass("form-horizontal")
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.ProductionRefNo)
                .AddClass("form-control input-sm")
                .Attributes("placeholder") = "Reference Number"
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateFrom)
                .AddClass("form-control input-sm")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateTo)
                .AddClass("form-control input-sm")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventManagerID, "Event Manager", , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-caret-down")
                .InputGroup.AddClass("margin-bottom-override")
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionTypeID, "Production Type", , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-caret-down")
                .InputGroup.AddClass("margin-bottom-override")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventTypeID, "Event Type", , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-caret-down")
                .InputGroup.AddClass("margin-bottom-override")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionVenueID, "Production Venue", , Singular.Web.BootstrapEnums.Style.DefaultStyle, "fa-caret-down")
                .InputGroup.AddClass("margin-bottom-override")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 2)
              With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
                .AddClass("form-control input-sm")
                .Attributes("placeholder") = "Search by Keyword"
              End With
            End With

          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.DivC("pull-left")
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "Refresh()")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ViewModel.ROProductionListPagingManager,
                                                  Function(vm) ViewModel.ROProductionList,
                                                  False, False, True, False, True, True, True,
                                                  , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
              .Attributes("id") = "ROProductionList"
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y"
              .Pager.PagerListTag.ListTag.AddClass("pull-left")
              With .FirstRow
                .AddClass("items")
                '.AddBinding(Singular.Web.KnockoutBindingString.css, "GetProductionStatColour($data)")
                With .AddColumn("")
                  .Style.Width = 80
                  .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "ProductionSelected($data)")
                End With
                    
                'With .AddColumn("")
                '  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                '  With .Helpers.Bootstrap.Anchor(, , , , Singular.Web.LinkTargetType.NotSet, "fa-pencil", )
                '    .AnchorTag.AddClass("btn btn-sm btn-primary")
                '    .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.href, "SelectProduction($data)")
                '  End With
                'End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                  .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionType)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.EventType)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionVenue)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c) c.EventManager)
                  .AddClass("col-xs-1 col-sm-2 col-md-2 col-lg-1")
                End With
                   
              End With
              With .FooterRow
              End With
            End With
          End With
          
          'With .Helpers.Bootstrap.FlatBlock("Find Production", True)
          '  .FlatBlockTag.AddClass("flat-block-paged")
          '  With .AboveContentTag
          '    .AddClass("form-horizontal")
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.ProductionRefNo)
          '          .AddClass("form-control input-sm")
          '          .Attributes("placeholder") = "Reference Number"
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateFrom)
          '          .AddClass("form-control input-sm")
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateTo)
          '          .AddClass("form-control input-sm")
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventManagerID, "Event Manager", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
          '          .InputGroup.AddClass("margin-bottom-override")
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionTypeID, "Production Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
          '          .InputGroup.AddClass("margin-bottom-override")
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventTypeID, "Event Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
          '          .InputGroup.AddClass("margin-bottom-override")
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionVenueID, "Production Venue", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
          '          .InputGroup.AddClass("margin-bottom-override")
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 6, 3, 3)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
          '          .AddClass("form-control input-sm")
          '          .Attributes("placeholder") = "Search by Keyword"
          '        End With
          '      End With
          '    End With
          '    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "Refresh()")
          '        End With
          '      End With
          '    End With
          '  End With
          '  With .ContentTag
          '    With .Helpers.DivC("table-responsive")
                
          '    End With
          '  End With
          'End With
        End With
      End With
      ''------------ Position Layout Dialogs ----------------
      With h.Bootstrap.Dialog("FindPositionLayout", "Find Position Layout")
        .ModalDialogDiv.AddClass("modal-lg")
        With .Body
          With .Helpers.Bootstrap.FlatBlock("Find Layout", True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .AboveContentTag
              .AddClass("form-horizontal")
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.LabelFor(Function(d) d.ROPositionLayoutListCriteria.LayoutName)
                  With .Helpers.Div
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROPositionLayoutListCriteria.LayoutName, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Search by Layout Name"
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.LabelFor(Function(d) d.ROPositionLayoutListCriteria.TemplateInd)
                  With .Helpers.Div
                    With .Helpers.Bootstrap.StateButton("ViewModel.ROPositionLayoutListCriteria().TemplateInd", "Yes", "No", , , , , "")
                  
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh",
                                                 , Singular.Web.PostBackType.None, "RefreshROPositionLayoutListPagingManager()")
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROPositionLayout)(Function(vm) ViewModel.ROPositionLayoutListPagingManager,
                                                                          Function(vm) ViewModel.ROPositionLayoutList,
                                                                          False, False, True, False, True, True, True,
                                                                          , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    '.AddClass("items")
                    With .AddColumn("")
                      .Style.Width = 80
                      .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "PositionLayoutSelected($data)")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.LayoutName, 300)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.ImageName, 150)
                      .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.Production, 300)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                    With .AddReadOnlyColumn(Function(c) c.CreatedByName, 150)
                      .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                    End With
                  End With
                  With .FooterRow
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      ''------------ Production Venue Image Dialogs ----------------
      With h.Bootstrap.Dialog("FindProductionVenueImageDialog", "Find Venue Image")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          .AddClass("row")
          With .Helpers.Div
            With .Helpers.Bootstrap.Column(12, 6, 4, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROProductionVenueImageListCriteria.ImageName, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Search by Image Name"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 4, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "RefreshVenueImagePagingManager()")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.Div
            With .Helpers.Bootstrap.PagedGridFor(Of ROProductionVenueImage)(Function(vm) ViewModel.ROProductionVenueImageListPagingManager,
                                                  Function(vm) ViewModel.ROProductionVenueImageList,
                                                  False, False, True, False, True, True, True,
                                                  , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y"
              .Pager.PagerListTag.ListTag.AddClass("pull-left")
              With .FirstRow
                '.AddClass("items")
                With .AddColumn("")
                  .Style.Width = 80
                  .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SelectProductionVenueImage($data)")
                End With
                With .AddReadOnlyColumn(Function(c) c.ImageName, 400)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c) c.ProductionVenue, 400)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
              End With
              With .FooterRow
              End With
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("NewLayOutDialog", "Create new layout ")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          With .Helpers.Div
            .AddClass("form-horizontal row")
            With .Helpers.Bootstrap.FlatBlock("New Production Layout", True)
              .FlatBlockTag.AddClass("flat-block-paged")
              With .AboveContentTag
                .AddClass(" row")
                With .Helpers.With(Of PositionLayout)(Function(c) ViewModel.SelectedLayout)
                  With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.EditorFor(Function(d) ViewModel.SelectedLayout.LayoutName)
                        .AddClass("form-control input-sm")
                        .Attributes("placeholder") = "Layout Name"
                      End With
                    End With
                  End With
                End With
                
                With .Helpers.With(Of ProductionVenueImage)(Function(c) ViewModel.ProductionVenueImage)
                  With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.DocumentManager(Function(d) d.VenueImage)
                        .AddClass("form-control input-sm")
                      End With
                    End With
                  End With
                End With
                
              End With
              With .AboveContentTag
                .AddClass("row")
                With .Helpers.With(Of PositionLayout)(Function(c) ViewModel.SelectedLayout)
                  With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ProductionID, "Production", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                        .InputGroup.AddClass("margin-bottom-override")
                      End With
                    End With
                  End With
                End With
                
                With .Helpers.With(Of ProductionVenueImage)(Function(c) ViewModel.ProductionVenueImage)
                  With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ProductionTypeID, "Production Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                        .InputGroup.AddClass("margin-bottom-override")
                      End With
                    End With
                  End With
                End With

              End With

              With .Helpers.With(Of ProductionVenueImage)(Function(c) ViewModel.ProductionVenueImage)
                With .Helpers.Bootstrap.Column(12, 6, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ProductionVenueID, "Production Venue", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                      .InputGroup.AddClass("margin-bottom-override")
                    End With
                  End With
                End With
              End With
              

              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-save", , Singular.Web.PostBackType.None, "NewPositionLayout()")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      'With h.Bootstrap.Dialog("NewLayOutDialog", "Create new layout ")
      '  .ModalDialogDiv.AddClass("modal-md")
      '  With .Body
      '    With .Helpers.FieldSetFor(Of ProductionVenueImage)(Function(c) c.ProductionVenueImage)
      
      '      .Helpers.EditorRowFor(Function(c) c.ProductionTypeID)
      '      .Helpers.EditorRowFor(Function(c) c.ProductionVenueID)
      '      .Helpers.DocumentManager(Function(c) c.VenueImage)
      '    End With
          
      '    With .Helpers.FieldSetFor(Of PositionLayout)(Function(c) c.CurrPositionLayout)
      '      .Helpers.EditorRowFor(Function(c) c.ProductionID)
      '      .Helpers.EditorRowFor(Function(c) c.LayoutName)
      '    End With
          
      '    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '        With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "NewPositionLayout()")
      '        End With
      '      End With
      '    End With
      '  End With
      'End With

    End Using
    
  %>

  <script type="text/javascript">

    //Singular.OnPageLoad(function (c, d, e) {

    //});

    //function SetupDragDrop() {
    //  $('.LayoutDraggable').draggable({
    //    revert: false,
    //    helper: function () {
    //      $copy = $(this).clone();
    //      return $copy;
    //    },
    //    appendTo: 'body',
    //    scroll: false
    //  });
    //  $('.LayoutDroppable').droppable({
    //    drop: function (event, ui) {
    //      var plpID = ui.draggable[0].id;
    //      var layoutPos = $('.LayoutDroppable').position();
    //      var layoutOffset = $('.LayoutDroppable').offset();
    //      var newPos = ui.position;


    //      var plp = ViewModel.CurrPositionLayout().PositionLayoutPositionList().Find("PositionLayoutPositionID", plpID)
    //      plp.XPos(newPos.left - layoutOffset.left);
    //      plp.YPos(newPos.top - layoutOffset.top);
    //    }
    //  });
    //}

    //function GetCameraPositionStyle(obj) {
    //  if (obj.XPos() == 0 && obj.YPos() == 0) {
    //    return {};
    //  }
    //  return { position: "relative", top: obj.YPos() + 'px', left: obj.XPos() + 'px' };
    //}

    function OptionSelected(OptionNum) {
      Singular.SendCommand("OptionSelected", { Option: OptionNum }, function (response) {
        Singular.Validation.CheckRules(ViewModel);
      })
    };

    function LayoutBackgroundImage() {
      if (ViewModel.SelectedLayout()) {
        return { background: 'grey url(' + ViewModel.SelectedLayout().ImgString() + ')', padding: '0px', width: ViewModel.SelectedLayout().Width() + 'px', height: ViewModel.SelectedLayout().Height() + 'px' };
      } else {
        return "";
      }
    };

    function FindImage() {
      //ViewModel.ROProductionVenueImageListCriteria().TemplateInd(false);
      $("#FindProductionVenueImageDialog").modal();
    };

    function FindPositionLayout() {
      $("#FindPositionLayout").modal();
    };

    function FindProduction() {
      $("#FindProductionDialog").modal();
    };

    function UploadImage() {
      Singular.SendCommand("NewPositionLayout", {}, function (response) {
        Singular.Validation.CheckRules(ViewModel.ProductionVenueImage());
        Singular.Validation.CheckRules(ViewModel.SelectedLayout());
        $("#NewLayOutDialog").modal();
        ViewModel.SelectedLayout().ProductionID(null);
        ViewModel.SelectedLayout().LayoutName("");
        ViewModel.ProductionVenueImage().ProductionVenueID(null);
        ViewModel.ProductionVenueImage().ProductionTypeID(null);
      })
    };

    function Refresh() {
      ViewModel.ROProductionListPagingManager().Refresh();
    };

    function RefreshROPositionLayoutListPagingManager() {
      ViewModel.ROPositionLayoutListPagingManager().Refresh();
    };

    function RefreshVenueImagePagingManager() {
      ViewModel.ROProductionVenueImageListPagingManager().Refresh();
    };

    function NewPositionLayout() {
      Singular.SendCommand("SaveImage", {}, function (response) {
        Singular.Validation.CheckRules(ViewModel.ProductionVenueImage());
        Singular.Validation.CheckRules(ViewModel.SelectedLayout());
      })
      $("#NewLayOutDialog").modal('hide');
    };

    function ProductionSelected(ROProduction) {
      ViewModel.SelectedROProduction(ROProduction);
      ViewModel.ProductionName(ROProduction.ProductionDescription())
      Singular.SendCommand("ProductionSelected", {
        ProductionID: ROProduction.ProductionID(),
        ProductionName: ROProduction.ProductionDescription()
      }, function (response) {
        Singular.Validation.CheckRules(ViewModel);
        InitLayout();
        $("#FindProductionDialog").modal('hide');
      });
    };

    function PositionLayoutSelected(ROPositionLayout) {
      if (ViewModel.SelectedOption() == 2) {
        Singular.SendCommand("TemplateLayoutSelected",
                             {
                               PositionLayoutID: ROPositionLayout.PositionLayoutID(),
                               LayoutName: ROPositionLayout.LayoutName()
                             },
                             function (response) {
                               Singular.Validation.CheckRules(ViewModel);
                               InitLayout();
                               $("#FindPositionLayout").modal('hide');
                             });
      };
      //if (ROPositionLayout.TemplateInd()) {
      //  Singular.SendCommand("TemplateLayoutSelected",
      //                       {
      //                         PositionLayoutID: ROPositionLayout.PositionLayoutID(),
      //                         LayoutName: ROPositionLayout.LayoutName()
      //                       },
      //                       function (response) {

      //                       });
      //} else {
      //  Singular.SendCommand("EditPositionLayout",
      //                        {
      //                          PositionLayoutID: ROPositionLayout.PositionLayoutID()
      //                        },
      //                        function (response) {
      //                          $("#FindPositionLayoutDialog").modal('hide');
      //                          InitialiseCameras('Main', ViewModel.SelectedLayout().PositionLayoutPositionList());
      //                        });
      //}
    };

    function SelectProductionVenueImage(ROProductionVenueImage) {
      Singular.SendCommand("ImageSelected", {
        ProductionVenueImageID: ROProductionVenueImage.ProductionVenueImageID(),
        ProductionVenueImage: ROProductionVenueImage.ImageName()
      }, function (response) {
        //ViewModel.SelectedLayout().ProductionVenueImageID(ROProductionVenueImage.ProductionVenueImageID());
        //ViewModel.SelectedLayout().ProductionVenueImageName(ROProductionVenueImage.ImageName());
        InitLayout();
        $("#FindProductionVenueImageDialog").modal('hide');
      });
    };

    function FindLayoutTemplate() {
      ViewModel.ROPositionLayoutListCriteria().TemplateInd(true);
      $("#FindPositionLayout").modal();
    };

    function InitLayout() {
      if (ViewModel.SelectedLayout() && ViewModel.SelectedLayout().PositionLayoutID()) {
        InitialiseCameras('Main', ViewModel.SelectedLayout().PositionLayoutPositionList())
      }
    };

    //Template Ind
    function TemplateIndClick() {

      if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == true) {
        ViewModel.ROPositionLayoutListCriteria().TemplateInd(false);
      }
      else if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == false) {
        ViewModel.ROPositionLayoutListCriteria().TemplateInd(null)
      } else {
        ViewModel.ROPositionLayoutListCriteria().TemplateInd(true)
      }

    };

    function TemplateIndCss() {
      if (ViewModel.ROPositionLayoutListCriteria().TemplateInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
    };

    function TemplateIndHtml() {

      if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == true) {
        return "<span class='glyphicon glyphicon-check'></span> Yes"
      }
      else if (ViewModel.ROPositionLayoutListCriteria().TemplateInd() == false) {
        return "<span class='glyphicon glyphicon-minus'></span> No"
      } else {
        return "All"
      }

    };

    function LayoutHeader() {
      if (ViewModel.SelectedROProduction()) {
        return "Layout- " + ViewModel.ProductionName();
      }
    }

    $('#NewLayOutDialog').on('hide.bs.modal', function (e) {
      //ViewModel.SelectedLayout(null);
      //ViewModel.ProductionVenueImage(null);
      Singular.Validation.CheckRules(ViewModel)
    })

  </script>
</asp:Content>
