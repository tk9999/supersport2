﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports OBLib.Productions.ReadOnly
Imports OBLib.PositionLayouts
Imports Singular
Imports System.ComponentModel
Imports System

Public Class CameraPositionLayouts
  Inherits OBPageBase(Of CameraPositionLayoutsVM)

End Class

Public Class CameraPositionLayoutsVM
  Inherits OBViewModel(Of CameraPositionLayoutsVM)

  Public Shared SelectedOptionProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SelectedOption, Nothing)
  ''' <summary>
  ''' Gets and sets the Keyword value
  ''' </summary>
  <Display(Name:="Selected Option", Description:="")>
  Public Property SelectedOption() As Integer?
    Get
      Return GetProperty(SelectedOptionProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(SelectedOptionProperty, Value)
    End Set
  End Property

  Public Property CanViewLayout As Boolean

  Public Property SelectedROProduction As ROProduction

  Public Shared SelectedProductionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SelectedProductionID, Nothing)
  ''' <summary>
  ''' Gets and sets the Keyword value
  ''' </summary>
  <Display(Name:="Production", Description:="")>
  Public Property SelectedProductionID() As Integer?
    Get
      Return GetProperty(SelectedProductionIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(SelectedProductionIDProperty, Value)
    End Set
  End Property

  Public Shared SelectedProductionNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SelectedProductionName, "")
  ''' <summary>
  ''' Gets and sets the Keyword value
  ''' </summary>
  <Display(Name:="Production", Description:="")>
  Public Property SelectedProductionName() As String
    Get
      Return GetProperty(SelectedProductionNameProperty)
    End Get
    Set(ByVal Value As String)
      SetProperty(SelectedProductionNameProperty, Value)
    End Set
  End Property

  Public Property SelectedProductionVenueImageID As Integer?
  Public Property SelectedImageName As String

  Public Property NewLayoutName As String
  Public Property SelectedLayoutName As String

  Public Shared SelectedTemplateLayoutNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SelectedTemplateLayoutName, "")
  ''' <summary>
  ''' Gets and sets the Keyword value
  ''' </summary>
  <Display(Name:="Template", Description:="")>
  Public Property SelectedTemplateLayoutName() As String
    Get
      Return GetProperty(SelectedTemplateLayoutNameProperty)
    End Get
    Set(ByVal Value As String)
      SetProperty(SelectedTemplateLayoutNameProperty, Value)
    End Set
  End Property

  Public Shared SelectedTemplatePositionLayoutIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SelectedTemplatePositionLayoutID, Nothing)
  ''' <summary>
  ''' Gets and sets the Keyword value
  ''' </summary>
  <Display(Name:="Template", Description:="")>
  Public Property SelectedTemplatePositionLayoutID() As Integer?
    Get
      Return GetProperty(SelectedTemplatePositionLayoutIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(SelectedTemplatePositionLayoutIDProperty, Value)
    End Set
  End Property

  Public Shared ProductionNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionName, "")
  ''' <summary>
  ''' Gets and sets the Keyword value
  ''' </summary>
  <Display(Name:="Production", Description:="")>
  Public Property ProductionName() As String
    Get
      Return GetProperty(ProductionNameProperty)
    End Get
    Set(ByVal Value As String)
      SetProperty(ProductionNameProperty, Value)
    End Set
  End Property


  <Browsable(False)>
  Public Property SelectedTemplate As OBLib.PositionLayouts.PositionLayout

  Public Property SelectedLayout As OBLib.PositionLayouts.PositionLayout

  <InitialDataOnly>
  Public Property ROProductionVenueImageList As OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList
  Public Property ROProductionVenueImageListCriteria As OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList.Criteria
  Public Property ROProductionVenueImageListPagingManager As Singular.Web.Data.PagedDataManager(Of CameraPositionLayoutsVM)

  <InitialDataOnly>
  Public Property ROProductionList As ROProductionList
  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of CameraPositionLayoutsVM)

  <InitialDataOnly>
  Public Property ROPositionLayoutList As OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList
  Public Property ROPositionLayoutListCriteria As OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList.Criteria
  Public Property ROPositionLayoutListPagingManager As Singular.Web.Data.PagedDataManager(Of CameraPositionLayoutsVM)

  Public Property ProductionVenueImage As ProductionLayouts.ProductionVenueImage

  Protected Overrides Sub Setup()
    MyBase.PreSetup()

    'Setup ROProductionList
    ROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
    ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
    ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of CameraPositionLayoutsVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "TxStartDateTime", 10)
    ROProductionListCriteria.TxDateFrom = Now.Date.AddDays(-7)
    ROProductionListCriteria.TxDateTo = Now.Date.AddMonths(1)
    ROProductionListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROProductionListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    ROProductionListCriteria.PageNo = 1
    ROProductionListCriteria.PageSize = 10
    ROProductionListCriteria.SortAsc = True
    ROProductionListCriteria.SortColumn = "TxStartDateTime"
    ROProductionListCriteria.TxDateFrom = Singular.Dates.DateMonthStart(Now).AddDays(-7)
    ROProductionListCriteria.TxDateTo = Singular.Dates.DateMonthEnd(Now.AddMonths(1))
    ROProductionListCriteria.EventManagerID = Nothing
    If OBLib.Security.Settings.CurrentUser.IsEventManager Then
      ROProductionListCriteria.EventManagerID = OBLib.Security.Settings.CurrentUser.HumanResourceID
    End If
    ROProductionList = OBLib.Productions.[ReadOnly].ROProductionList.GetROProductionList(ROProductionListCriteria.ProductionTypeID, ROProductionListCriteria.EventTypeID, ROProductionListCriteria.ProductionVenueID,
                                                                                          Nothing, Nothing, Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID, _
                                                                                          ROProductionListCriteria.TxDateFrom, ROProductionListCriteria.TxDateTo, ROProductionListCriteria.EventManagerID,
                                                                                          ROProductionListCriteria.ProductionRefNo, Nothing, OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                                          ROProductionListCriteria.PageNo, ROProductionListCriteria.PageSize,
                                                                                          ROProductionListCriteria.SortAsc, ROProductionListCriteria.SortColumn)


    'Setup ROPositionLayoutList
    ROPositionLayoutList = New OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList
    ROPositionLayoutListCriteria = New OBLib.PositionLayouts.ReadOnly.ROPositionLayoutList.Criteria
    ROPositionLayoutListPagingManager = New Singular.Web.Data.PagedDataManager(Of CameraPositionLayoutsVM)(Function(d) Me.ROPositionLayoutList, Function(d) Me.ROPositionLayoutListCriteria, "LayoutName", 10)
    ROPositionLayoutListCriteria.PositionLayoutType = CommonData.Enums.PositionLayoutType.Camera
    ROPositionLayoutListCriteria.PageNo = 1
    ROPositionLayoutListCriteria.PageSize = 10
    ROPositionLayoutListCriteria.SortAsc = True
    ROPositionLayoutListCriteria.SortColumn = "LayoutName"

    'Setup ROProductionVenueImageList
    ROProductionVenueImageList = New OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList
    ROProductionVenueImageListCriteria = New OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList.Criteria
    ROProductionVenueImageListPagingManager = New Singular.Web.Data.PagedDataManager(Of CameraPositionLayoutsVM)(Function(d) Me.ROProductionVenueImageList, Function(d) Me.ROProductionVenueImageListCriteria, "ImageName", 10)
    ROProductionVenueImageListCriteria.PageNo = 1
    ROProductionVenueImageListCriteria.PageSize = 10
    ROProductionVenueImageListCriteria.SortAsc = True
    ROProductionVenueImageListCriteria.SortColumn = "ImageName"


    'Other Setup
    SelectedOption = Nothing
    SelectedProductionID = Nothing
    CanViewLayout = False
    SelectedLayoutName = ""
    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    MessageFadeTime = 2000
    ClientDataProvider.AddDataSource("TypePositionlist", CommonData.Lists.ROPositionList.FilterList("DisciplineID", CommonData.Enums.Discipline.CameraOperator), False)
    ClientDataProvider.AddDataSource("MainEquipmentSubType", CommonData.Lists.ROEquipmentSubTypeList.FilterList("EquipmentTypeID", CommonData.Enums.EquipmentType.Camera), False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "OptionSelected"
        OptionSelected(CommandArgs)

      Case "ProductionSelected"
        ProductionSelected(CommandArgs)

      Case "ImageSelected"
        ImageSelected(CommandArgs)

      Case "TemplateLayoutSelected"
        TemplateLayoutSelected(CommandArgs)

      Case "SaveCurrentLayout"
        SaveCurrentLayout(CommandArgs)

      Case "StartOver"
        StartOver(CommandArgs)

      Case "DeleteCurrentLayout"
        DeleteCurrentLayout(CommandArgs)

      Case "EditPositionLayout"
        'CurrPositionLayout = PositionLayoutList.GetPositionLayoutList(Nothing, CommandArgs.ClientArgs.PositionLayoutID).FirstOrDefault

      Case "NewPositionLayout"
        'LoadProperty(ProductionVenueImageProperty, New ProductionLayouts.ProductionVenueImage)
        'ProductionVenueImage = ProductionLayouts.ProductionVenueImage.NewProductionVenueImage
        'CurrPositionLayout = OBLib.PositionLayouts.PositionLayout.NewPositionLayout
        'ProductionVenueImage.VenueImage = New Documents.TemporaryDocument()

      Case "SaveImage"
        'If Not ProductionVenueImage.IsValid Then
        '  'AddMessage(Singular.Web.MessageType.Validation, "Valid", ProductionVenueImage.BrokenRulesCollection(0).Description)
        '  Exit Select
        'End If

        'Dim pvi As New ProductionLayouts.ProductionVenueImageList
        'pvi.Add(ProductionVenueImage)
        'Dim sh As SaveHelper = TrySave(pvi)
        'If Not sh.Success Then
        '  'Error
        '  AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
        'Else
        '  'Success
        '  'LoadProperty(ProductionVenueImageProperty, CType(sh.SavedObject, ProductionLayouts.ProductionVenueImageList)(0))
        '  ProductionVenueImage = CType(sh.SavedObject, ProductionLayouts.ProductionVenueImageList)(0)
        '  CurrPositionLayout.ProductionVenueImageName = ProductionVenueImage.VenueImage.DocumentName
        '  CurrPositionLayout.ProductionVenueImageID = ProductionVenueImage.ProductionVenueImageID
        '  If Not CurrPositionLayout.IsValid Then
        '    'AddMessage(Singular.Web.MessageType.Validation, "Valid", CurrPositionLayout.BrokenRulesCollection(0).Description)
        '    Exit Select
        '  End If
        '  Dim pll As New OBLib.PositionLayouts.PositionLayoutList
        '  pll.Add(CurrPositionLayout)
        '  Dim sh1 As SaveHelper = TrySave(pll)
        '  If Not sh1.Success Then
        '    'Error
        '    AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh1.ErrorText)
        '  Else
        '    'Success
        '    CurrPositionLayout = CType(sh1.SavedObject, OBLib.PositionLayouts.PositionLayoutList)(0)
        '    CurrPositionLayout.PositionLayoutTypeID = CommonData.Enums.PositionLayoutType.Camera
        '    AddMessage(Web.MessageType.Success, "Save ", "Saved Successfully")
        '  End If
        'End If

    End Select
  End Sub

  Private Sub OptionSelected(CommandArgs As Singular.Web.CommandArgs)
    SelectedOption = CommandArgs.ClientArgs.Option
    SelectedProductionName = ""
    SelectedProductionID = Nothing
    SelectedROProduction = Nothing
    SelectedImageName = ""
    SelectedLayout = Nothing
    NewLayoutName = ""
    'New Position Layout
    If SelectedOption = 1 Then
      SetupNewTemplate()
    End If
  End Sub

  Private Sub StartOver(CommandArgs As Singular.Web.CommandArgs)

    SelectedProductionName = ""
    SelectedProductionID = Nothing
    SelectedLayout = Nothing
    SelectedLayoutName = ""
    SelectedImageName = ""
    SelectedOption = Nothing
    SelectedProductionVenueImageID = Nothing
    CanViewLayout = False

  End Sub

  Private Sub ProductionSelected(CommandArgs As Singular.Web.CommandArgs)

    SelectedProductionName = CommandArgs.ClientArgs.ProductionName
    SelectedProductionID = CommandArgs.ClientArgs.ProductionID
    SetupNewProductionLayout()

  End Sub

  Private Sub TemplateLayoutSelected(CommandArgs As Singular.Web.CommandArgs)

    SelectedTemplateLayoutName = CommandArgs.ClientArgs.LayoutName
    SelectedTemplatePositionLayoutID = CommandArgs.ClientArgs.PositionLayoutID
    Dim pl As PositionLayoutList = OBLib.PositionLayouts.PositionLayoutList.GetPositionLayoutList(Nothing, SelectedTemplatePositionLayoutID)
    If pl.Count = 1 Then
      SelectedTemplate = pl(0)
      SetupNewProductionLayout()
    Else
      'Throw Error?
    End If

  End Sub

  Private Sub SetupNewProductionLayout()

    If SelectedProductionID IsNot Nothing AndAlso SelectedTemplate IsNot Nothing AndAlso SelectedOption = 2 Then
      Dim LayoutID As Integer? = Nothing
      Dim pll As New OBLib.PositionLayouts.PositionLayoutList
      SelectedLayout = pll.AddNew
      SelectedLayout.PositionLayoutTypeID = OBLib.CommonData.Enums.PositionLayoutType.Camera
      SelectedLayout.SetupNewPositionLayoutFromTemplate(SelectedProductionID, SelectedProductionName, SelectedTemplate)
      Dim sh As SaveHelper = TrySave(pll)
      If Not sh.Success Then
        'Error
        AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
      Else
        'Success
        SelectedLayout = CType(sh.SavedObject, OBLib.PositionLayouts.PositionLayoutList)(0)
        Dim LayoutList As PositionLayoutList = PositionLayoutList.GetPositionLayoutList(Nothing, SelectedLayout.PositionLayoutID)
        If LayoutList.Count = 1 Then
          SelectedLayout = LayoutList(0)
        End If
        SelectedOption = Nothing
        CanViewLayout = True
      End If
    End If

    'SelectedLayout.ProductionID = SelectedProductionID
    'SelectedLayout.ProductionVenueImageID = SelectedProductionVenueImageID
    'NewLayoutName = SelectedProductionName & " - " & SelectedImageName
    'SelectedLayout.LayoutName = NewLayoutName

  End Sub

  Private Sub SetupNewTemplate()

    Dim LayoutID As Integer? = Nothing
    Dim pll As New OBLib.PositionLayouts.PositionLayoutList
    SelectedLayout = pll.AddNew
    SelectedLayout.PositionLayoutTypeID = OBLib.CommonData.Enums.PositionLayoutType.Camera
    SelectedLayout.ProductionVenueImageID = Nothing
    SelectedLayout.ProductionVenueImageName = ""
    SelectedLayout.NewImage = True

    'If SelectedProductionVenueImageID IsNot Nothing AndAlso SelectedImageName.Trim.Length > 0 AndAlso SelectedOption = 1 Then
    'End If
    'SelectedLayout.ProductionID = SelectedProductionID
    'SelectedLayout.ProductionVenueImageID = SelectedProductionVenueImageID
    'NewLayoutName = SelectedProductionName & " - " & SelectedImageName
    'SelectedLayout.LayoutName = NewLayoutName

  End Sub

  Private Sub ImageSelected(CommandArgs As Singular.Web.CommandArgs)
    SelectedProductionVenueImageID = CommandArgs.ClientArgs.ProductionVenueImageID
    SelectedImageName = CommandArgs.ClientArgs.ProductionVenueImage
    SelectedLayout.ProductionVenueImageName = SelectedImageName
    SelectedLayout.ProductionVenueImageID = SelectedProductionVenueImageID
  End Sub

  Private Sub SaveCurrentLayout(CommandArgs As Singular.Web.CommandArgs)
    If Not SelectedLayout.IsValid Then
      AddMessage(Singular.Web.MessageType.Validation, "Valid", SelectedLayout.BrokenRulesCollection(0).Description)
    End If
    Dim pll As New OBLib.PositionLayouts.PositionLayoutList
    pll.Add(SelectedLayout)
    Dim sh As SaveHelper = TrySave(pll)
    If Not sh.Success Then
      'Error
      AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
    Else
      'Success
      SelectedLayout = CType(sh.SavedObject, OBLib.PositionLayouts.PositionLayoutList)(0)
    End If
  End Sub

  Private Sub DeleteCurrentLayout(CommandArgs As Singular.Web.CommandArgs)

    Try
      Dim PositionLayoutList As New PositionLayoutList()
      PositionLayoutList.Add(SelectedLayout)
      PositionLayoutList.Remove(SelectedLayout)
      PositionLayoutList = TrySave(PositionLayoutList).SavedObject
      SelectedLayout = Nothing
    Catch ex As Exception
      AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
    End Try

  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

    With AddWebRule(SelectedProductionNameProperty)
      .JavascriptRuleFunctionName = "CameraPositionLayoutsVMBO.SelectedProductionNameValid"
      .ServerRuleFunction = AddressOf SelectedProductionNameValid
      .AddTriggerProperty(SelectedOptionProperty)
      .AffectedProperties.Add(SelectedProductionIDProperty)
    End With

    With AddWebRule(SelectedTemplateLayoutNameProperty)
      .JavascriptRuleFunctionName = "CameraPositionLayoutsVMBO.SelectedTemplateNameValid"
      .ServerRuleFunction = AddressOf SelectedProductionNameValid
      .AddTriggerProperty(SelectedOptionProperty)
      .AffectedProperties.Add(SelectedTemplatePositionLayoutIDProperty)
    End With

  End Sub

  Public Function SelectedProductionNameValid(VM As CameraPositionLayoutsVM) As String

    If VM.SelectedOption = 2 AndAlso VM.SelectedProductionName.Trim.Length = 0 Then
      Return "Production is required"
    End If
    Return ""

  End Function

  Public Function SelectedTemplateNameValid(VM As CameraPositionLayoutsVM) As String

    If VM.SelectedOption = 2 AndAlso VM.SelectedTemplateLayoutName.Trim.Length = 0 Then
      Return "Template is required"
    End If
    Return ""

  End Function

End Class