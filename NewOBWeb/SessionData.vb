﻿Imports OBLib.Productions
Imports OBLib.Travel

Public Class SessionData

#Region " Productions "

  Public Shared Property CurrentProduction As OBLib.Productions.Old.Production
    Get
      Return HttpContext.Current.Session("SessionData.CurrentProduction")
    End Get
    Set(value As OBLib.Productions.Old.Production)
      If HttpContext.Current.Session("SessionData.CurrentProduction") IsNot value Then
        HttpContext.Current.Session("SessionData.CurrentProduction") = value
      End If
    End Set
  End Property

  Private Shared mProductionSystemAreaID As Integer? = Nothing
  Public Shared Property ProductionSystemAreaID As Integer?
    Get
      Return mProductionSystemAreaID
    End Get
    Set(value As Integer?)
      mProductionSystemAreaID = value
    End Set
  End Property

  Private Shared mProductionHumanResourceID As Integer? = Nothing
  Public Shared Property ProductionHumanResourceID As Integer?
    Get
      Return mProductionHumanResourceID
    End Get
    Set(value As Integer?)
      mProductionHumanResourceID = value
    End Set
  End Property

#End Region

End Class
