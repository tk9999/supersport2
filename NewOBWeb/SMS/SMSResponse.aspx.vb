﻿Imports System.IO
Imports Singular
Imports System.Net

Public Class SMSResponse2
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    Dim req As String = Context.Request.Url.ToString
    Dim params = req.Split("?")
    Dim writer As New StreamWriter("c:\\SmS\sms.txt", True)
    ''set the location of the file.
    'Try
    '  'writer = File.AppendText("c:\\SmS\sms.txt")
    '  'write to the file and close it
    '  writer.WriteLine("ProcessRequest - Response")
    '  writer.WriteLine(req)
    '  'writer.WriteLine(resp)
    'Catch ex As Exception
    '  'OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SmSReceiver", "ProcessRequest - Response", ex.Message)
    'End Try

    Try
      writer.WriteLine("ProcessRequest - QS")
      'writer = File.CreateText("c:\\SmS\sms.txt")
      Dim apiID As String = Context.Request.QueryString("api_id")
      Dim fromnum As String = Context.Request.QueryString("from")
      Dim tonum As String = Context.Request.QueryString("to")
      Dim timestamp As String = Context.Request.QueryString("timestamp")
      Dim apiMsgId As String = Context.Request.QueryString("apiMsgId")
      Dim cliMsgId As String = Context.Request.QueryString("cliMsgId")
      Dim status As String = Context.Request.QueryString("status")
      Dim charge As String = Context.Request.QueryString("charge")

      If apiID <> "" Then
        writer.WriteLine(apiID)
      Else
        writer.WriteLine("apiID not found")
      End If
      If fromnum <> "" Then
        writer.WriteLine(fromnum)
      Else
        writer.WriteLine("fromnum not found")
      End If
      If tonum <> "" Then
        writer.WriteLine(tonum)
      Else
        writer.WriteLine("tonum not found")
      End If
      If timestamp <> "" Then
        writer.WriteLine(timestamp)
      Else
        writer.WriteLine("timestamp not found")
      End If
      If apiMsgId <> "" Then
        writer.WriteLine(apiMsgId)
      Else
        writer.WriteLine("apiMsgId not found")
      End If
      If cliMsgId <> "" Then
        writer.WriteLine(cliMsgId)
      Else
        writer.WriteLine("cliMsgId not found")
      End If
      If status <> "" Then
        writer.WriteLine(status)
      Else
        writer.WriteLine("status not found")
      End If
      If charge <> "" Then
        writer.WriteLine(charge)
      Else
        writer.WriteLine("charge not found")
      End If

      'OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SmSReceiver", "", cliMsgId & ", " & cliMsgId.Contains("UAT").ToString)
      'OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SmSReceiver", "", Request.RawUrl.Split("?")(1))

      If Not cliMsgId.Contains("UAT") Then
        Dim SmsResponseList As New OBLib.Notifications.SmS.SMSResponseList
        Dim SmsResponse As OBLib.Notifications.SmS.SMSResponse = New OBLib.Notifications.SmS.SMSResponse(apiID, apiMsgId, cliMsgId, timestamp, tonum, fromnum, status, "", charge)
        SmsResponseList.Add(SmsResponse)
        SmsResponse.CheckAllRules()
        writer.WriteLine(SmsResponse.IsValid.ToString)
        writer.WriteLine(SmsResponse.GetErrorsAsString)
        SmsResponseList = SmsResponseList.Save()
      Else

        Try
          Dim internalSiteRequest As HttpWebRequest = HttpWebRequest.Create("http://03rnb-qasober02/SMS/SMSResponse.aspx?" & Request.RawUrl.Split("?")(1))
          Dim prxy2 As IWebProxy = WebRequest.GetSystemWebProxy()
          internalSiteRequest.Proxy = prxy2
          'OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SendToUATServer", "beforeResponse", "http://03rnb-qasober02/SMS/SMSResponse.aspx?" & Request.RawUrl.Split("?")(1))
          Dim intResponse As HttpWebResponse = internalSiteRequest.GetResponse()
          'OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SendToUATServer", "afterResponse", "http://03rnb-qasober02/SMS/SMSResponse.aspx?" & Request.RawUrl.Split("?")(1))
        Catch ex As Exception
          OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SendToUATServer", "wrGETURL", ex.Message)
        End Try
      End If

      'writer.WriteLine(params(1))
    Catch ex As Exception
      OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SmSReceiver", "ProcessRequest - QS", ex.Message)
    End Try

    'Try
    '  'writer = File.CreateText("c:\\SmS\sms.txt")
    '  writer.WriteLine("ProcessRequest - Json")
    '  Dim reader As StreamReader = New StreamReader(Context.Response.OutputStream)
    '  Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer()
    '  Dim jsonObject As Object = serializer.DeserializeObject(reader.ReadToEnd())
    '  writer.WriteLine(jsonObject.ToString)
    'Catch ex As Exception
    '  OBLib.Helpers.ErrorHelpers.LogClientError("SmsReceiver", "SmSReceiver", "ProcessRequest - Json", ex.Message)
    'End Try

    writer.Close()

  End Sub

End Class