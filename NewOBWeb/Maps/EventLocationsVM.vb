﻿Imports OBLib.Maps
Imports OBLib.Maps.ReadOnly
Imports Geocoding.Google
Imports Csla
Imports System.ComponentModel.DataAnnotations

Public Class EventLocationsVM
  Inherits OBViewModel(Of EventLocationsVM)

#Region " Properties "

  Private mROImportedEventLocationList As ROImportedEventLocationList
  Public ReadOnly Property ROImportedEventLocationList As ROImportedEventLocationList
    Get
      Return mROImportedEventLocationList
    End Get

  End Property

  Public Shared CurrentDateProperty As PropertyInfo(Of Date) = RegisterSProperty(Of Date)(Function(c) c.CurrentDate) '_
  '.AddSetExpression("RefetchData")
  <Display(Name:="Current Date", Description:=""), Required()>
  Public Property CurrentDate As Date
    Get
      Return GetProperty(CurrentDateProperty)
    End Get
    Set(ByVal Value As Date)
      SetProperty(CurrentDateProperty, Value)
    End Set
  End Property

  Public Shared FilterStringProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FilterString) '_
  '.AddSetExpression("FilterMarkers")
  <Display(Name:="Filter", Description:="")>
  Public Property FilterString As String
    Get
      Return GetProperty(FilterStringProperty)
    End Get
    Set(ByVal Value As String)
      SetProperty(FilterStringProperty, Value)
    End Set
  End Property

  Public Property CurrentImportedEventID As Integer?

  Public Property SelectedSports As Integer()

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    'If OBLib.CommonData.HasPageAccess("EventLocations") Then
    MyBase.Setup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad
    CurrentDate = Now
    SelectedSports = New Integer() {}
    'Dim roel As ROEventLocationList = ROEventLocationList.GetROEventLocationList()
    'For Each ROEventLocation In roel
    '  If ROEventLocation.Latitude Is Nothing And ROEventLocation.Longitude Is Nothing Then
    '    Dim geoCoder As GoogleGeocoder = New GoogleGeocoder() 'With {.ApiKey = "AIzaSyDf2lJkuHFY8VUWx5iqaZ89iGcm22604co"}
    '    Try
    '      Dim addresses As IEnumerable(Of GoogleAddress) = geoCoder.Geocode(ROEventLocation.GoogleLocationString)
    '      If addresses.Count = 1 Then
    '        ROEventLocation.UpdateCoOrdinates(addresses.First)
    '      End If
    '    Catch ex As Exception
    '      Continue For
    '    End Try
    '  End If
    'Next
    mROImportedEventLocationList = ROImportedEventLocationList.GetROImportedEventLocationList(CurrentDate, CurrentDate)
    ''For Each ROImportedEventLocation In mROImportedEventLocationList
    ''  If ROImportedEventLocation.Latitude Is Nothing And ROImportedEventLocation.Longitude Is Nothing AndAlso ROImportedEventLocation.GoogleLocationString.Trim.Length > 0 Then
    ''    Dim geoCoder As GoogleGeocoder = New GoogleGeocoder() 'With {.ApiKey = "AIzaSyDf2lJkuHFY8VUWx5iqaZ89iGcm22604co"}
    ''    Try
    ''      Dim addresses As IEnumerable(Of GoogleAddress) = geoCoder.Geocode(ROImportedEventLocation.GoogleLocationString)
    ''      If addresses.Count = 1 Then
    ''        ROImportedEventLocation.UpdateCoOrdinates(addresses.First)
    ''      End If
    ''    Catch ex As Exception
    ''      Continue For
    ''    End Try
    ''  End If
    ''Next

    'OBLib.Maps.ROImportedEventLocationList, OBLib
    'Else
    '  HttpContext.Current.Response.Redirect("~/Account/Login.aspx")
    'End If
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "RefetchData"
        mROImportedEventLocationList = ROImportedEventLocationList.GetROImportedEventLocationList(CurrentDate, CurrentDate)

    End Select

  End Sub

#End Region

End Class
