﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
  CodeBehind="EventLocations.aspx.vb" Inherits="NewOBWeb.EventLocations" %>

<%@ Import Namespace="OBLib.Maps" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <style type="text/css">
    html {
      height: 100%;
    }

    body, .page, .main {
      height: 100%;
      margin: 0;
      padding: 0;
    }

    #map-canvas {
      height: 100%;
    }

    #ImportedEventDetails {
      font-size: large;
    }

    .navbar {
      margin-bottom: 0px !important;
    }

    .Criteria {
      background-color: #FFF !important;
      margin-bottom: 0px !important;
    }
  </style>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf2lJkuHFY8VUWx5iqaZ89iGcm22604co&sensor=false&region=ZA">
  </script>
  <script type="text/javascript" src="../Scripts/oms.min.js">
  </script>
  <script type="text/javascript">

    var ImportedEvents = [];
    var map = null;
    var SA = null;
    var CurrentImportedEvent = null;
    var oms = null;
    var iw = null;
    var MarkersList = [];

    function detectBrowser() {
      var useragent = navigator.userAgent;
      var mapdiv = document.getElementById("map-canvas");
      mapdiv.style.width = '100%';
      mapdiv.style.height = '100%';
      //      console.log(mapdiv.style.width);
      //      console.log(mapdiv.style.height);
      //      if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1) {
      //        mapdiv.style.width = '100%';
      //        mapdiv.style.height = '100%';
      //      } else {
      //        //mapdiv.style.width = '600px';
      //        //mapdiv.style.height = '800px';
      //        mapdiv.style.width = '100%';
      //        mapdiv.style.height = '100%';
      //      }
    };

    function SetWidthHeight() {
      var height = document.body.clientHeight - 80;
      var width = document.body.clientWidth - 0;
      //console.log(height, width);
      $("#map-canvas").css({ 'height': height, 'width': width });
    };

    function initialize(GetEventDataCallback) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': 'Chad' }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          SetupDefaultMap(results);
          GetEventDataCallback();
        }
      });
    };

    function SetupDefaultMap(results) {
      SA = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
      var mapOptions = {
        center: SA,
        zoom: 3
      };
      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
      oms = new OverlappingMarkerSpiderfier(map);
      oms.addListener('click', function (marker, event) {
        ShowModal(marker);
      });
    };

    //    function GetAllCoords() {
    //      GetData(function (args) {
    //        SetupAllEvents(args);
    //      });
    //    };

    //    function GetData(callback) {
    //      Singular.GetDataStateless('OBLib.Maps.ROImportedEventLocationList, OBLib', { StartDate: new Date(), EndDate: new Date() }, function (args) {
    //        callback(args);
    //      });
    //    };

    //    function SetupAllEvents(args) {
    //      ImportedEvents = args.Data;
    //      ImportedEvents.Iterate(function (ImportedEvent, Index) {
    //        GetEventCoords(ImportedEvent, Index)
    //      });
    //    };

    function SetupAllEvents() {
      ViewModel.ROImportedEventLocationList().Iterate(function (ImportedEvent, Index) {
        AddEventMarker(ImportedEvent);
      });
    };

    //function GetEventCoords(ImportedEvent, Index) {
    //  var geocoder = new google.maps.Geocoder();
    //  geocoder.geocode({ 'address': ImportedEvent.GoogleLocationString }, function (results, status) {
    //    if (status == google.maps.GeocoderStatus.OK) {
    //      ImportedEvent.latitude = results[0].geometry.location.lat();
    //      ImportedEvent.longitude = results[0].geometry.location.lng();
    //      AddEventMarker(ImportedEvent);
    //    } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
    //      setTimeout(function () {
    //        GetEventCoords(ImportedEvent, Index);
    //      }, 200);
    //    }
    //  })
    //};

    function AddEventMarker(ImportedEvent) {
      //console.log(ImportedEvent);
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(ImportedEvent.Latitude(), ImportedEvent.Longitude()),
        map: map,
        animation: google.maps.Animation.DROP,
        title: ImportedEvent.SeriesTitle() + ': ' + ImportedEvent.Title() //,
        //icon: GetSportSymbol(ImportedEvent)
      });
      marker.ImportedEvent = ImportedEvent;
      //google.maps.event.addListener(marker, 'click', OnMarkerClick);
      //tell the OverlappingMarkerSpiderfier instance about each marker as you add it
      //marker.desc = '<a href="#" class="showDetail" onclick="OnMarkerClick(' + ImportedEvent.GenRefNumber() + ')">' + ImportedEvent.SeriesTitle() + ': ' + ImportedEvent.Title() + '</a>';
      oms.addMarker(marker);
      MarkersList.push(marker);
      //google.maps.event.addListener(marker, 'click', OnMarkerClick);
      //google.maps.event.addListener(marker, 'click', ShowModal);
    };

    function ShowModal(marker) {
      //var marker = this;
      ViewModel.CurrentImportedEventID(marker.ImportedEvent.ImportedEventID());
      $("#ImportedEventDetails").modal('show');
    };

    function ProductionSymbol() {
      return {
        path: "M14.263,2.826H7.904L2.702,8.028v6.359L18.405,30.09l11.561-11.562L14.263,2.826zM6.495,8.859c-0.619-0.619-0.619-1.622,0-2.24C7.114,6,8.117,6,8.736,6.619c0.62,0.62,0.619,1.621,0,2.241C8.117,9.479,7.114,9.479,6.495,8.859z",
        fillColor: 'black',
        fillOpacity: 1,
        scale: 1
      }
    };

    function GetSportSymbol(ImportedEvent) {
      var ps = new ProductionSymbol();
      switch (ImportedEvent.GenreDesc()) {
        case 'Football':
          ps.fillColor = 'blue'
      };
      return ps;
    };

    function OnMarkerClick(GenRefNumber) {
      //      var marker = this;
      //      if (marker.getAnimation() != null) {
      //        marker.setAnimation(null);
      //      } else {
      //        marker.setAnimation(google.maps.Animation.BOUNCE);
      //      }
      var ClickedEvent = ImportedEvents.Find('GenRefNumber', GenRefNumber);
      ShowModal(ClickedEvent);
    };

    //    function ShowModal(ImportedEvent) {
    //      $("#SeriesTitle").text(ImportedEvent.SeriesTitle());
    //      $("#Title").text(ImportedEvent.Title());
    //      $("#Genre").text(ImportedEvent.GenreDesc());
    //      $("#LiveDate").text(ImportedEvent.LiveDate());
    //      $("#Location").text(ImportedEvent.Location());
    //      $("#Venue").text(ImportedEvent.Venue());
    //      $("#ImportedEventDetails").modal('show');
    //    };

    $(function () {
      initialize(function () {
        SetupAllEvents();
        //GetAllCoords();
        SetWidthHeight();
      });
    });

    function FilterMarkers(data, event) {
      if (!ViewModel.FilterString() || ViewModel.FilterString() == '') {
        MarkersList.Iterate(function (Marker, Index) {
          Marker.setVisible(true);
        })
      } else {
        MarkersList.Iterate(function (Marker, Index) {
          var ie = Marker.ImportedEvent;
          var SearchString = ViewModel.FilterString().toLowerCase();
          var Title = ie.Title().toLowerCase();
          var SeriesTitle = ie.SeriesTitle().toLowerCase();
          var Country = ie.Country().toLowerCase();
          var Location = ie.Location().toLowerCase();
          var Venue = ie.Venue().toLowerCase();
          var GenreDesc = ie.GenreDesc().toLowerCase();
          var TitleMatch = (Title.indexOf(SearchString) >= 0);
          var SeriesTitleMatch = (SeriesTitle.indexOf(SearchString) >= 0);
          var CountryMatch = (Country.indexOf(SearchString) >= 0);
          var LocationMatch = (Location.indexOf(SearchString) >= 0);
          var VenueMatch = (Venue.indexOf(SearchString) >= 0);
          var GenreDescMatch = (GenreDesc.indexOf(SearchString) >= 0);
          if (TitleMatch || SeriesTitleMatch || CountryMatch || LocationMatch || VenueMatch || GenreDescMatch) {
            Marker.setVisible(true);
          } else {
            Marker.setVisible(false);
          }
        });
      }
      return true;
    };

    function CurrentImportedEventLocation() {
      if (ViewModel.CurrentImportedEventID()) {
        return ViewModel.ROImportedEventLocationList().Find('ImportedEventID', ViewModel.CurrentImportedEventID());
      };
      return null;
    };

    function ClearCurrentImportedEventID() {
      ViewModel.CurrentImportedEventID(null);
    };

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%  
    Using h = Helpers
      
      With h.DivC("Criteria")
        .Helpers.EditorFor(Function(vm) ViewModel.CurrentDate)
        .Helpers.BootstrapButton("RefetchData", "Update", "btn-sm btn-primary", "glyphicon-refresh", Singular.Web.PostBackType.Full, True)
        With .Helpers.EditorFor(Function(d) d.FilterString)
          .Attributes("placeholder") = "Filter..."
          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterMarkers}")
        End With
      End With
    
      With h.Div
        .Attributes("id") = "map-canvas"
      End With
      
      'With h.Div
      '  .Attributes("id") = "ImportedEventDetails"
      With h.With(Of ROImportedEventLocation)("CurrentImportedEventLocation()")
        With .Helpers.BootstrapDialog("", "")
          With .Modal
            .Attributes("id") = "ImportedEventDetails"
          End With
          'With .DismissButton
          '  '.AddBinding(Singular.Web.KnockoutBindingString.click, "ClearCurrentImportedEventID()")
          'End With
          With .Heading
            .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.SeriesTitle & ": " & d.Title)
          End With
          With .Body
            With .Helpers.DivC("row")
              .Helpers.LabelFor(Function(d As ROImportedEventLocation) d.GenreDesc)
              .Helpers.EditorFor(Function(d As ROImportedEventLocation) d.GenreDesc)
            End With
            With .Helpers.DivC("row")
              .Helpers.LabelFor(Function(d As ROImportedEventLocation) d.LiveDate)
              .Helpers.EditorFor(Function(d As ROImportedEventLocation) d.LiveDate)
            End With
            With .Helpers.DivC("row")
              .Helpers.LabelFor(Function(d As ROImportedEventLocation) d.Venue)
              .Helpers.EditorFor(Function(d As ROImportedEventLocation) d.Venue)
            End With
            With .Helpers.DivC("row")
              .Helpers.LabelFor(Function(d As ROImportedEventLocation) d.Location)
              .Helpers.EditorFor(Function(d As ROImportedEventLocation) d.Location)
            End With
          End With
        End With
      End With
      'End With
      
    End Using
  %>
</asp:Content>
