﻿Imports OBLib.HR
Imports OBLib.HR.ReadOnly
Imports System.ComponentModel
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports Csla
Imports System.IO
Imports Singular.Web
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Security
Imports OBLib.HR.Timesheets
Imports OBLib.HR.Timesheets.ReadOnly
Imports System

Public Class HumanResource
  Inherits OBPageBase(Of HumanResourceVM)

End Class

Public Class HumanResourceVM
  Inherits OBViewModel(Of HumanResourceVM)
  Implements ControlInterfaces(Of HumanResourceVM).IFindHumanResource
  Implements ControlInterfaces(Of HumanResourceVM).IROCountrySelect
  Implements ControlInterfaces(Of HumanResourceVM).IROCitySelect
  Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSecondmentsPagedGrid
  Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment
  Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceOffPeriod
  Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceOffPeriodPagedGrid
  Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSkillPagedGrid
  Implements ControlInterfaces(Of HumanResourceVM).IProductionHumanResourceTBCGrid
  Implements ControlInterfaces(Of HumanResourceVM).IEditHRSkill
  Implements ControlInterfaces(Of HumanResourceVM).IEditHRTimesheet

#Region " Properties "

#Region "User Systems "

  Public Property UserSystemList As OBLib.Security.UserSystemList

#End Region

#Region "Human Resource"

  Public Property CurrentHumanResource As OBLib.HR.HumanResource

  Public Property HumanResourceAddToBooking As HumanResourceAddToBooking

#End Region

  Public ReadOnly Property CurrentSystemName() As String
    Get
      Dim ROSystem As OBLib.Maintenance.ReadOnly.ROSystem = OBLib.CommonData.Lists.ROSystemList.GetItem(OBLib.Security.Settings.CurrentUser.SystemID)
      If ROSystem Is Nothing Then
        Return ""
      End If
      Return ROSystem.System
    End Get
  End Property

  Public ReadOnly Property CurrentProductionAreaName() As String
    Get
      Dim ROProductionArea As OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionArea = OBLib.CommonData.Lists.ROProductionAreaList.GetItem(OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      If ROProductionArea Is Nothing Then
        Return ""
      End If
      Return ROProductionArea.ProductionArea
    End Get
  End Property

  <InitialDataOnly>
  Public Property ROHumanResourceList As ROHumanResourcePagedList Implements ControlInterfaces(Of HumanResourceVM).IFindHumanResource.ROHumanResourceList
  <InitialDataOnly>
  Public Property ROHumanResourceListCriteria As ROHumanResourcePagedList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IFindHumanResource.ROHumanResourceListCriteria
  <InitialDataOnly>
  Public Property ROHumanResourceListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IFindHumanResource.ROHumanResourceListManager

  <InitialDataOnly>
  Public Property ROCountryList As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList Implements ControlInterfaces(Of HumanResourceVM).IROCountrySelect.ROCountryList
  <InitialDataOnly>
  Public Property ROCountryListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IROCountrySelect.ROCountryListCriteria
  <InitialDataOnly>
  Public Property ROCountryListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IROCountrySelect.ROCountryListManager

  <InitialDataOnly>
  Public Property ROCityList As OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList Implements ControlInterfaces(Of HumanResourceVM).IROCitySelect.ROCityList
  <InitialDataOnly>
  Public Property ROCityListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IROCitySelect.ROCityListCriteria
  <InitialDataOnly>
  Public Property ROCityListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IROCitySelect.ROCityListManager

#End Region

#Region " Skills "

  Public Property CurrentHumanResourceSkill As HumanResourceSkill Implements ControlInterfaces(Of HumanResourceVM).IEditHRSkill.CurrentHumanResourceSkill

  <InitialDataOnly>
  Public Property ROHumanResourceSkillList As OBLib.HR.ReadOnly.ROHumanResourceSkillList Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSkillPagedGrid.ROHumanResourceSkillList
  <InitialDataOnly>
  Public Property ROHumanResourceSkillListCriteria As OBLib.HR.ReadOnly.ROHumanResourceSkillList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSkillPagedGrid.ROHumanResourceSkillListCriteria
  <InitialDataOnly>
  Public Property ROHumanResourceSkillListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSkillPagedGrid.ROHumanResourceSkillListManager

#End Region

#Region " Off Periods "

  Public Property CurrentHumanResourceOffPeriod As OBLib.HR.HumanResourceOffPeriod Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceOffPeriod.CurrentHumanResourceOffPeriod
  <InitialDataOnly>
  Public Property ROHumanResourceOffPeriodList As OBLib.HR.ReadOnly.ROHumanResourceOffPeriodList Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceOffPeriodPagedGrid.ROHumanResourceOffPeriodList
  <InitialDataOnly>
  Public Property ROHumanResourceOffPeriodListCriteria As OBLib.HR.ReadOnly.ROHumanResourceOffPeriodList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceOffPeriodPagedGrid.ROHumanResourceOffPeriodListCriteria
  <InitialDataOnly>
  Public Property ROHumanResourceOffPeriodListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceOffPeriodPagedGrid.ROHumanResourceOffPeriodListManager
  <InitialDataOnly>
  Public Property CurrentROHumanResourceOffPeriod As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod

  <InitialDataOnly>
  Public Property CurrentHumanResourceUnAvailabilityTemplate As OBLib.HR.HumanResourceUnAvailabilityTemplate
  <InitialDataOnly>
  Public Property ROHumanResourceUnAvailabilityList As OBLib.HR.ROHumanResourceUnAvailabilityList
  <InitialDataOnly>
  Public Property ROHumanResourceUnAvailabilityListCriteria As OBLib.HR.ROHumanResourceUnAvailabilityList.Criteria
  <InitialDataOnly>
  Public Property ROHumanResourceUnAvailabilityListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM)

#End Region

#Region " Secondments "

  Public Property CurrentHumanResourceSecondment As OBLib.HR.HumanResourceSecondment Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentHumanResourceSecondment
  <InitialDataOnly>
  Public Property CurrentSecondmentROCountryList As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentSecondmentROCountryList
  <InitialDataOnly>
  Public Property CurrentSecondmentROCountryListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentSecondmentROCountryListCriteria
  <InitialDataOnly>
  Public Property CurrentSecondmentROCountryListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentSecondmentROCountryListManager
  <InitialDataOnly>
  Public Property CurrentSecondmentRODebtorList As RODebtorList Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentSecondmentRODebtorList
  <InitialDataOnly>
  Public Property CurrentSecondmentRODebtorListCriteria As OBLib.Quoting.ReadOnly.RODebtorList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentSecondmentRODebtorListCriteria
  <InitialDataOnly>
  Public Property CurrentSecondmentRODebtorListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IEditHumanResourceSecondment.CurrentSecondmentRODebtorListManager

  <InitialDataOnly>
  Public Property ROHumanResourceSecondmentList As OBLib.HR.ReadOnly.ROHumanResourceSecondmentList Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSecondmentsPagedGrid.ROHumanResourceSecondmentList
  <InitialDataOnly>
  Public Property ROHumanResourceSecondmentListCriteria As OBLib.HR.ReadOnly.ROHumanResourceSecondmentList.Criteria Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSecondmentsPagedGrid.ROHumanResourceSecondmentListCriteria
  <InitialDataOnly>
  Public Property ROHumanResourceSecondmentListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) Implements ControlInterfaces(Of HumanResourceVM).IROHumanResourceSecondmentsPagedGrid.ROHumanResourceSecondmentListManager

#End Region

#Region " TBC "

  <InitialDataOnly>
  Public Property ProductionHumanResourceTBCList As OBLib.HR.ProductionHumanResourcesTBCList Implements ControlInterfaces(Of HumanResourceVM).IProductionHumanResourceTBCGrid.ProductionHumanResourceTBCList
  <InitialDataOnly>
  Public Property ProductionHumanResourceTBCListIsBusy As Boolean Implements ControlInterfaces(Of HumanResourceVM).IProductionHumanResourceTBCGrid.ProductionHumanResourceTBCListIsBusy

#End Region

#Region " Documents "

  Public Property HRDocumentList As HRDocumentList

#End Region

#Region " Areas "

  Public Property HRSystemSelectList As HRSystemSelectList

#End Region

#Region " Duplicates "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SubmitHRDuplicates(list As OBLib.HR.ROHRDuplicateList) As Singular.Web.Result
    Try
      If list.Count > 0 Then
        Dim sh As Singular.Web.SaveHelper = TrySave(list)
        If sh.Success Then
          list = sh.SavedObject
          Return New Singular.Web.Result(True) With {.Data = list}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Timesheets "

  Public Property CurrentHumanResourceTimesheet As OBLib.HR.Timesheets.HumanResourceTimesheet Implements ControlInterfaces(Of HumanResourceVM).IEditHRTimesheet.CurrentHumanResourceTimesheet
  Public Property ROHumanResourceTimesheetPagedList As ROHumanResourceTimesheetPagedList
  Public Property ROHumanResourceTimesheetPagedListCriteria As ROHumanResourceTimesheetPagedList.Criteria
  Public Property ROHumanResourceTimesheetPagedListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM)

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.Setup()
    MyBase.PreSetup()

    ValidationMode = Singular.Web.ValidationMode.OnLoad

    '/*************************** Find
    ROHumanResourceList = New OBLib.HR.ReadOnly.ROHumanResourcePagedList
    ROHumanResourceListCriteria = New OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria
    ROHumanResourceListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) Me.ROHumanResourceList,
                                                                                            Function(d) Me.ROHumanResourceListCriteria,
                                                                                            "FirstName", 25)
    ROHumanResourceListManager.SingleSelect = True

    '/*************************** Country
    ROCountryList = New OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList
    ROCountryListCriteria = New OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria
    ROCountryListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROCountryList,
                                                                                      Function(d) d.ROCountryListCriteria,
                                                                                      "Country", 15, True)
    ROCountryListManager.SingleSelect = True

    '/*************************** City
    ROCityList = New OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList
    ROCityListCriteria = New OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList.Criteria
    ROCityListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROCityList,
                                                                                      Function(d) d.ROCityListCriteria,
                                                                                      "City", 15, True)
    ROCityListManager.SingleSelect = True

    '/*************************** Skills
    ROHumanResourceSkillList = New OBLib.HR.ReadOnly.ROHumanResourceSkillList
    ROHumanResourceSkillListCriteria = New OBLib.HR.ReadOnly.ROHumanResourceSkillList.Criteria
    ROHumanResourceSkillListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROHumanResourceSkillList,
                                                                                                 Function(d) d.ROHumanResourceSkillListCriteria,
                                                                                                 "EndDate", 15, True)
    ROHumanResourceSkillListManager.SingleSelect = True

    '/*************************** Off Periods
    ROHumanResourceOffPeriodList = New OBLib.HR.ReadOnly.ROHumanResourceOffPeriodList
    ROHumanResourceOffPeriodListCriteria = New OBLib.HR.ReadOnly.ROHumanResourceOffPeriodList.Criteria
    ROHumanResourceOffPeriodListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROHumanResourceOffPeriodList,
                                                                                                     Function(d) d.ROHumanResourceOffPeriodListCriteria,
                                                                                                    "EndDate", 15, True)
    ROHumanResourceOffPeriodListManager.SingleSelect = True

    ROHumanResourceUnAvailabilityList = New OBLib.HR.ROHumanResourceUnAvailabilityList
    ROHumanResourceUnAvailabilityListCriteria = New OBLib.HR.ROHumanResourceUnAvailabilityList.Criteria
    ROHumanResourceUnAvailabilityListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROHumanResourceUnAvailabilityList,
                                                                                                          Function(d) d.ROHumanResourceUnAvailabilityListCriteria,
                                                                                                          "EndDate", 15, False)

    '/*************************** Secondments
    ROHumanResourceSecondmentList = New OBLib.HR.ReadOnly.ROHumanResourceSecondmentList
    ROHumanResourceSecondmentListCriteria = New OBLib.HR.ReadOnly.ROHumanResourceSecondmentList.Criteria
    ROHumanResourceSecondmentListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROHumanResourceSecondmentList,
                                                                                                      Function(d) d.ROHumanResourceSecondmentListCriteria,
                                                                                                      "FromDate", 15, True)
    ROHumanResourceSecondmentListManager.SingleSelect = True

    CurrentSecondmentROCountryList = New OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList
    CurrentSecondmentROCountryListCriteria = New OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria
    CurrentSecondmentROCountryListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.CurrentSecondmentROCountryList,
                                                                                                       Function(d) d.CurrentSecondmentROCountryListCriteria,
                                                                                                       "Country", 15, True)
    CurrentSecondmentROCountryListManager.SingleSelect = True
    CurrentSecondmentRODebtorList = New RODebtorList
    CurrentSecondmentRODebtorListCriteria = New OBLib.Quoting.ReadOnly.RODebtorList.Criteria
    CurrentSecondmentRODebtorListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) Me.CurrentSecondmentRODebtorList,
                                                                                                       Function(d) Me.CurrentSecondmentRODebtorListCriteria,
                                                                                                       "Debtor", 15)

    UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

    '/*************************** TBC
    ProductionHumanResourceTBCList = New OBLib.HR.ProductionHumanResourcesTBCList
    ProductionHumanResourceTBCListIsBusy = False

    '/*************************** Documents
    HRDocumentList = New HRDocumentList

    '/*************************** Timesheets
    ROHumanResourceTimesheetPagedList = New ROHumanResourceTimesheetPagedList
    ROHumanResourceTimesheetPagedListCriteria = New ROHumanResourceTimesheetPagedList.Criteria
    ROHumanResourceTimesheetPagedListManager = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) d.ROHumanResourceTimesheetPagedList,
                                                                                                          Function(d) d.ROHumanResourceTimesheetPagedListCriteria,
                                                                                                          "EndDate", 20, False)


    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
    ClientDataProvider.AddDataSource("ROImageTypeList", OBLib.CommonData.Lists.ROImageTypeList, False)
    ClientDataProvider.AddDataSource("RORaceList", OBLib.CommonData.Lists.RORaceList, False)
    ClientDataProvider.AddDataSource("ROGenderList", OBLib.CommonData.Lists.ROGenderList, False)
    ClientDataProvider.AddDataSource("ROCostCentreList", OBLib.CommonData.Lists.ROCostCentreList, False)
    ClientDataProvider.AddDataSource("ROOffReasonList", OBLib.CommonData.Lists.ROOffReasonList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSupplierList", OBLib.CommonData.Lists.ROSupplierList, False)
    ClientDataProvider.AddDataSource("ROHRDocumentTypeList", OBLib.CommonData.Lists.ROHRDocumentTypeList, False)
    ClientDataProvider.AddDataSource("ROPositionList", OBLib.CommonData.Lists.ROPositionList, False)
    ClientDataProvider.AddDataSource("ROPositionTypeList", OBLib.CommonData.Lists.ROPositionTypeList, False)
    ClientDataProvider.AddDataSource("RORoomTypeList", OBLib.CommonData.Lists.RORoomTypeList, False)
    ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
    ClientDataProvider.AddDataSource("ROSkillProductionTypeList", OBLib.CommonData.Lists.ROSkillProductionTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROManagerList", OBLib.CommonData.Lists.ROManagerList, False)
    ClientDataProvider.AddDataSource("ROCountryList", OBLib.CommonData.Lists.ROCountryList, False)
    ClientDataProvider.AddDataSource("RODebtorList", OBLib.CommonData.Lists.RODebtorList, False)
    ClientDataProvider.AddDataSource("ROCityPagedList", OBLib.CommonData.Lists.ROCityPagedList, False)
    ClientDataProvider.AddDataSource("ROSystemTeamNumberList", OBLib.CommonData.Lists.ROSystemTeamNumberList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "FetchHumanResource"
        FetchHumanResource(CommandArgs)

      Case "Save"
        SaveHumanResource(CommandArgs)

      Case "NewHumanResource"
        CurrentHumanResource = New OBLib.HR.HumanResource

      Case "EditSecondment"
        EditSecondment(CurrentHumanResourceSecondment, CommandArgs)

      Case "SaveSecondment"
        SaveSecondment(CurrentHumanResourceSecondment, CommandArgs)

      Case "EditOffPeriod"
        EditOffPeriod(CurrentHumanResourceOffPeriod, CommandArgs)

      Case "SaveOffPeriod"
        SaveOffPeriod(CurrentHumanResourceOffPeriod, CommandArgs)

      Case "EditHumanResourceSkill"
        EditHumanResourceSkill(CurrentHumanResourceSkill, CommandArgs)

      Case "SaveHumanResourceSkill"
        SaveHumanResourceSkill(CurrentHumanResourceSkill, CommandArgs)

      Case "SaveHRDocuments"
        SaveHRDocumentsByCommand(HRDocumentList, CommandArgs)

      Case "DeleteHRDocuments"
        DeleteHRDocumentsByCommand(HRDocumentList, CommandArgs)

      Case "FetchHRDocuments"
        FetchHRDocumentsByCommand(CurrentHumanResource.HumanResourceID, HRDocumentList, CommandArgs)

      Case "AddHRToSystem"
        AddHRToSystem(CommandArgs)

    End Select

  End Sub

#End Region

  Private Sub FetchHumanResource(CommandArgs As CommandArgs)
    Dim hrl As HumanResourceList = HumanResourceList.GetHumanResourceList(CommandArgs.ClientArgs.HumanResourceID)
    If hrl.Count = 1 Then
      CurrentHumanResource = hrl(0)
      'Check for duplicates after Human Resource is not nothing 
      CurrentHumanResource.CheckAllRules()
    End If
  End Sub

  Private Function GetHumanResource(HumanResourceID As Integer?) As OBLib.HR.HumanResource
    Dim hrl As HumanResourceList = HumanResourceList.GetHumanResourceList(HumanResourceID)
    If hrl.Count = 1 Then
      Return hrl(0)
    End If
    Return Nothing
  End Function

  Private Sub SaveHumanResource(CommandArgs As CommandArgs)
    CurrentHumanResource.ROHRDuplicateList.Clear()
    CurrentHumanResource.RefreshDuplicates()
    If CurrentHumanResource IsNot Nothing Then
      CurrentHumanResource.CheckAllRules()
      Dim sh As Singular.SaveHelper
      If CurrentHumanResource.IsValid Then
        Dim hrl As New OBLib.HR.HumanResourceList
        hrl.Add(CurrentHumanResource)
        sh = TrySave(hrl)
        If Not sh.Success Then
          'Error
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        Else
          'Success
          CurrentHumanResource = CType(sh.SavedObject, OBLib.HR.HumanResourceList)(0)
          CurrentHumanResource = GetHumanResource(CurrentHumanResource.HumanResourceID)
          WebsiteHelpers.SendResourceBookingUpdateNotifications()
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        End If
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = CurrentHumanResource.Firstname & " has invalid data"}
      End If
    End If
  End Sub

  Private Sub AddHRToSystem(CommandArgs As Singular.Web.CommandArgs)

    Dim cmd As New Singular.CommandProc("CmdProcs.AddHRToSystem",
                      New String() {
                                   "@HumanResourceID",
                                   "@SystemID",
                                   "@ProductionAreaID",
                                   "@UserID"
                                   },
                      New Object() {
                                   CommandArgs.ClientArgs.HumanResourceID,
                                   OBLib.Security.Settings.CurrentUser.SystemID,
                                   OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                   OBLib.Security.Settings.CurrentUser.UserID
                                   })
    cmd.FetchType = CommandProc.FetchTypes.DataSet
    cmd.CommandTimeout = 0
    cmd = cmd.Execute()

    Dim hrl As HumanResourceList = HumanResourceList.GetHumanResourceList(CommandArgs.ClientArgs.HumanResourceID)
    If hrl.Count = 1 Then
      CurrentHumanResource = hrl(0)
    End If

    'CommandArgs.ReturnData = VirtualPathUtility.ToAbsolute("~/HR/HumanResource.aspx")

  End Sub

End Class
