﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports OBLib.HR.ReadOnly

Public Class HRManagers
  Inherits OBPageBase(Of HRManagersVM)

End Class

Public Class HRManagersVM
  Inherits OBViewModel(Of HRManagersVM)

  Public Property FutureDate As Date = Now.Date.AddDays(1)

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROHumanResourceFilter As String = ""
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property FindHRType As Integer = 1 ' 1: SubstituteManager, 2 New Manger, 3 Manager's humanresoures
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property CurrentManagerID As Integer
  Public Property CanChangeNewManager As Boolean
  Public Property HRManagersList As OBLib.HR.ManagerList



  Protected Overrides Sub Setup()
    MyBase.Setup()
    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"

    SetupDataSource()
    CanChangeNewManager = Singular.Security.HasAccess("Managers", "Change Manager")
    MessageFadeTime = 6000
    ManagerHumanResourceList = New OBLib.NSWTimesheets.ManagerHumanResourceList
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With TrySave(HRManagersList)
          If .Success Then
            SetupDataSource()
          End If
        End With

      Case "GetManagedHumanResources"
        ManagerHumanResourceList = OBLib.NSWTimesheets.ManagerHumanResourceList.GetManagerHumanResourceList(CommandArgs.ClientArgs.HumanResourceID)
 
      Case "SaveMHR"

        With TrySave(ManagerHumanResourceList, False, False)
          If .Success Then
            UpdateNoOfHR()
          End If
        End With
      Case "UpdateNoOfHR"
        UpdateNoOfHR()

    End Select
  End Sub

  Private Sub SetupDataSource()

    HRManagersList = OBLib.HR.ManagerList.GetManagerList()

  End Sub

  Private Sub UpdateNoOfHR()

    HRManagersList.UpdateNoOfHR(ManagerHumanResourceList.ManagerHumanResourceID, ManagerHumanResourceList.Count)

  End Sub 
  Public Property ManagerHumanResourceList As OBLib.NSWTimesheets.ManagerHumanResourceList

  Private mROHumanResourceList As HR.ReadOnly.ROHumanResourceList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROHumanResourceList As HR.ReadOnly.ROHumanResourceList
    Get
      Return mROHumanResourceList
    End Get
  End Property

  Public ReadOnly Property ROSubstituteManagerList() As HR.ReadOnly.ROSubstituteManagerList
    Get
      Return OBLib.HR.ReadOnly.ROSubstituteManagerList.GetROSubstituteManagerList
    End Get
  End Property



End Class