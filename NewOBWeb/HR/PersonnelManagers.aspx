﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="PersonnelManagers.aspx.vb" Inherits="NewOBWeb.PersonnelManagers" %>

<%@ Import Namespace="OBLib.HR.ReadOnly" %>

<%@ Import Namespace="OBLib.TeamManagement.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.General.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.HR.ReadOnly" %>

<%@ Import Namespace="Singular.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="../Styles/production-system-area-status-styles.css" type="text/css" rel="stylesheet"></link>
  <style type="text/css">
    div.left-column {
      width: 230px;
      display: inline-block;
      border-right: solid 1px #808080;
    }

    div.right-column {
      display: inline-block;
      width: calc(100% - 230px);
      vertical-align: top;
      /*white-space: nowrap;
      overflow-x: scroll;
      overflow-y: hidden;*/
    }

    div.days {
      /*overflow-x: hidden;*/
    }

    div.day-column {
      text-align: center;
      /*padding-left: 10px;
      padding-right: 10px;*/
      /*width: 40px;*/
      border-right: solid 1px #808080;
      display: inline-block;
    }


      div.day-column.today:not(.public-holiday) {
        background-color: #ff6a00;
        color: #fff;
      }

      div.day-column.public-holiday {
        background-color: #6d579d;
        color: #fff;
      }

    div.day-column-person {
      height: 25px;
      /*padding: 5px;*/
      border-bottom: solid 1px #808080;
    }

      div.day-column-person.weekend {
        background-color: #c6d9e8;
        color: #fff;
      }

    div.day-name {
      border-bottom: solid 1px #808080;
    }

    div.top-row {
      height: 43px;
      border-bottom: solid 1px #808080;
    }

    div.person-row {
      height: 25px;
      text-align: left;
      padding: 5px;
      border-left: solid 1px #808080;
      border-bottom: solid 1px #808080;
      white-space: nowrap;
    }

    div.person-row-day {
      height: 25px;
    }

    div.person-row:hover {
      background: #6d579d;
      color: #fff;
    }

    div.resource-booking {
      /*border: none !important;*/
    }

      div.resource-booking:hover {
        cursor: default;
      }

    #view-tabs-content {
      background: #ddd;
    }

    #StatusLegend {
      position: absolute;
      width: 230px;
      background-color: #fff;
      z-index: 10;
      padding: 20px;
    }

    span.legendName {
      float: right;
    }

    div.view-legend {
      display: block;
    }

    div.hide-legend {
      display: none;
    }
  </style>
  <script type="text/javascript" src="../Scripts/Tools/PersonnelManagers.js"></script>
  <script type="text/javascript">
    Singular.OnPageLoad(function () {
      window.PersonnelManagers = new PersonnelManagers({})
    })
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      With h.Div()
        .Attributes("id") = "personnel-manager-container"
      End With
      
      With h.Bootstrap.Dialog("SelectHumanResources", "Select Human Resources", False, "modal-xl", Singular.Web.BootstrapEnums.Style.Primary)
        With .Body
          .AddClass("modal-background-gray")
          
          'With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Date Selection", , , , )
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .AddClass("date-picker-reports")
                        .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          .Editor.Attributes("placeholder") = "Start Date"
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .AddClass("date-picker-reports")
                        .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          .Editor.Attributes("placeholder") = "End Date"
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          'End With
          
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 6, 2, 2)
              With .Helpers.Bootstrap.FlatBlock("Areas", , , , )
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      '.AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 2, 2)
              With .Helpers.Bootstrap.FlatBlock("Contract Types")
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of ROContractType)(Function(d) ViewModel.ROContractTypeList)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                            .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.AddContractType($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 2, 2)
              With .Helpers.Bootstrap.FlatBlock("Teams")
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of ROSystemTeamNumber)(Function(d) ViewModel.ROSystemTeamNumberList)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                            .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.AddSystemTeamNumber($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.SystemTeamNumberName)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
              With .Helpers.Bootstrap.FlatBlock("Disciplines")
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .AddClass("max-height-400")
                    
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Button(, "Select All", BootstrapEnums.Style.Primary,
                                                         , BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-check-square-o", , ,
                                                         "PersonnelManagers.selectAllDisciplines(true)")
                          End With
                          With .Helpers.Bootstrap.Button(, "Deselect All", BootstrapEnums.Style.DefaultStyle,
                                                         , BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-minus", , ,
                                                         "PersonnelManagers.selectAllDisciplines(false)")
                          End With
                        End With
                      End With
                    End With
                    
                    
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of RODisciplineSelect)(Function(d) ViewModel.RODisciplineSelectList)
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                            .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.AddDiscipline($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
                        
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Resources", , , , )
                '.FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 9, 9, 8, 8)
                      With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                        With .Helpers.FieldSet("Human Resource Filters")
                          .Attributes("id") = "HumanResourceFilters"
                          .AddClass("FadeHide")
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                              End With
                            End With
                          End With
                          'With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          '    With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                          '    End With
                          '  End With
                          'End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                            With .Helpers.Bootstrap.Column(6, 6, 3, 3)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) ViewModel.OnlyBookedInd, "Exclude non-booked", "Include non-booked", , , , , "btn-md")
                                  .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.OnlyBooked($data)")
                                  .Button.AddClass("btn-block")
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(6, 6, 3, 3)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) ViewModel.SelectAllHrInd, "Select All", "Deselect All", , , , , "btn-md")
                                  .Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.selectAllHumanResources($data)")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.FieldSet("Human Resources")
                        .Attributes("id") = "HumanResources"
                        .AddClass("FadeHide")
                        With .Helpers.ForEachTemplate(Of ROHumanResourcePersonnelManager)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.SelectedInd(), "", "", , , , "", )
                              '.Button.AddBinding(KnockoutBindingString.click, "PersonnelManagers.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.Button(, "Done", Singular.Web.BootstrapEnums.Style.Success, ,
                                         Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                         Singular.Web.PostBackType.Ajax, "PersonnelManagers.criteriaConfirmed()")
          End With
        End With
      End With
          
    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
