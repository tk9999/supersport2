﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="HRManagers.aspx.vb"
  Inherits="NewOBWeb.HRManagers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
    .ValidationPopup
    {
      width: 500px;
    }

    .ManageHumanResources
    {
      overflow-y: scroll !important;
      max-height: 250px !important;
    }

    .SubstituteManager
    {
      float: left;
      width: 160px;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .ManagerInvalid
    {
      border-style: solid;
      border-color: red;
      border-width: 1px;
    }

    .ComboDropDown
    {
      z-index: 2001;
    }
  </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      With h.Toolbar
        .Helpers.HTML.Heading2("HR Managers")
        
        .Helpers.Button(Singular.Web.DefinedButtonType.Save).PostBackType = Singular.Web.PostBackType.Ajax
      End With

      With h.FieldSet(" ")
        .Style.Height = 20
        h.MessageHolder()
      End With
      
      With h.TableFor(Of OBLib.HR.Manager)(Function(c) c.HRManagersList, False, False)
        
        .FirstRow.AddColumn(Function(c) c.CurrentManager, 200)
        
        With .FirstRow.AddColumn("Substitute Manager")
          With .Helpers.DivC("input-group")
          
            With .Helpers.BootstrapButton("", "", "btn-primary", "glyphicon-search", , , , , "FindROHumanResource($data,1)")
              .Button.Style.Height = 20
              .Button.Style.Style("padding-top") = 0
            End With
            
            ' .AddBinding(Singular.Web.KnockoutBindingString.html, Function(m) m.SubstituteManager)
            .Style.Width = 250
            .Style.TextAlign = Singular.Web.TextAlign.left
            With .Helpers.HTMLTag("span")
              .AddBinding(Singular.Web.KnockoutBindingString.html, Function(m) m.SubstituteManager)
         
            End With
          
            .AddBinding(Singular.Web.KnockoutBindingString.css, "myCheckRules($data)")
            
          End With
        End With

        
        .FirstRow.AddColumn(Function(c) c.UseSubstituteInd, 50)
        .FirstRow.AddColumn(Function(c) c.UseSubstituteEndDate, 90)
        .FirstRow.AddReadOnlyColumn(Function(c) c.NoOfManagedPeople, 60)
        With .FirstRow.AddColumn("Managed People")
          .Style.TextAlign = Singular.Web.TextAlign.center
          With .Helpers.BootstrapButton("", "View", "btn btn-sm btn-primary", , , , , , "ViewManagedPeople($data)")
            .Button.Style.Height = 20
            .Button.Style.Style("padding-top") = 0
              
          End With
        End With
        With .FirstRow.AddColumn("New Manager")
          .Style.Width = 200
          With .Helpers.DivC("input-group")
            With .Helpers.BootstrapButton("", "", "btn-primary", "glyphicon-search", , , , , "FindROHumanResource($data,2)")
              .Button.Style.Height = 20
              .Button.Style.Style("padding-top") = 0
              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) ViewModel.CanChangeNewManager)
            End With
            .Style.Width = 250
            .Style.TextAlign = Singular.Web.TextAlign.left
            With .Helpers.HTMLTag("span")
              .AddBinding(Singular.Web.KnockoutBindingString.html, Function(m) m.NewManagerName)
            End With
          End With
        End With
        
        ''ChildList 
        With .AddChildTable(Of OBLib.HR.ManagerSubstitute)(Function(c) c.ManagerSubstituteList, False, True)
          With .FirstRow.AddColumn()
            With .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "SetSelectIcon($data)", Singular.Web.PostBackType.None, False, , , "SetSubstituteManager($data)")
              With .Button
                .Style.Width = 75
                .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanSubstituteManager($data)")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "SetSelectIcon($data)")
              End With
            End With
          End With
          With .FirstRow.AddReadOnlyColumn(Function(d) d.SubstituteManager, 200, "Favorite Substitute")
            .Style.Style("text-align") = "left"
          End With
          With .FirstRow.AddColumn("Primary")
            
            With .Helpers.BootstrapStateButton("", "SetPrimaryInd($data)", "SetPrimaryDefaultCss($data)", "SetPrimaryIndHtml($data)", True)
              .Button.Style.Width = 45
            End With
          End With
           
        End With
      End With
      
      
      ' MODALS
      With h.DivC("Modals")
        'RO Human Resource Select
        With .Helpers.BootstrapDialog("SelectROHumanResource", "Select Manager")
          With .Body
            With .Helpers.DivC("field-box")
              With .Helpers.EditorFor(Function(p) ViewModel.ROHumanResourceFilter)
                .Attributes("placeholder") = "Filter..."
                .AddClass("form-control filter-field")
                .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROHumanResource}")
              End With
            End With
            .Helpers.HTML.Gap()
            With .Helpers.DivC("row ROHumanResourceList")
              With .Helpers.BootstrapTableFor(Of OBLib.HR.ReadOnly.ROHumanResource)(Function(d) ViewModel.ROHumanResourceList, False, False, "") '"$data.VisibleClient()")
                .AddClass("ROHumanResourceList")
                With .FirstRow
                  With .AddColumn("")
                    .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetHRIDs($data)")
                  End With
                  .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResource) hr.Firstname)
                  .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResource) hr.PreferredName)
                  .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResource) hr.Surname)
                End With
              End With
            End With
          End With
        End With
        
        
        'RO Human Resource Select
        With .Helpers.BootstrapDialog("ManagedPeople", "Manager Resources")
          With .Body
            With .Helpers.DivC("field-box")
            End With
            .Helpers.HTML.Gap()
            With .Helpers.DivC("row MHumanResourceList")
              
              ' .Style.Height = 350
              With .Helpers.BootstrapTableFor(Of OBLib.NSWTimesheets.ManagerHumanResource)(Function(d) ViewModel.ManagerHumanResourceList, False, False, "")
                
                .AddClass("ManageHumanResources")
                .Style.Height = 400
                .BodyHeightMax = 400
                With .FirstRow
                  .AddColumn(Function(mhr As OBLib.NSWTimesheets.ManagerHumanResource) mhr.HumanResourceID, 300)
                  .AddColumn(Function(mhr As OBLib.NSWTimesheets.ManagerHumanResource) mhr.HumanResourceCode, 300)
           
                End With
              End With
            End With
          End With
          With .Footer
 
            With .Helpers.BootstrapButton(, "Done", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
              With .Button
                .AddBinding(Singular.Web.KnockoutBindingString.click, "CloseManagerHumanResources()")
                .Style.Width = 80
              End With
            End With
            
          End With
          
        End With
      End With
        
      
    End Using
    
  %>

  <script type="text/javascript">
    function CanSubstituteManager(managerSub) {

      ParentRecord = managerSub.GetParent();
      if (ParentRecord.SubstituteManagerID() != managerSub.SubstituteManagerID()) { return true } else { return false }
    }
    function SetPrimaryInd(managerSub) {

      ParentRecord = managerSub.GetParent();
      managerSub.PrimaryInd(!managerSub.PrimaryInd())
      ParentRecord.ManagerSubstituteList().Iterate(function (item) {
        if (managerSub.SubstituteManagerID() != item.SubstituteManagerID()) {
          if (managerSub.PrimaryInd() != false) {
            item.PrimaryInd(!managerSub.PrimaryInd())
          }
        }
      });

    }

    function SetPrimaryIndHtml(managerSub) {

      if (managerSub.PrimaryInd() == true) {

        return "<span class=\"glyphicon glyphicon-check\"></span> Yes";
      } else {
        return "<span class=\"glyphicon glyphicon-minus\"></span> No";
      }
    }

    function SetSelectIcon(managerSub) {

      if (CanSubstituteManager(managerSub)) {
        return "<span class=\"glyphicon glyphicon-hand-up\"></span> Select";
      } else {
        return "<span class=\"glyphicon glyphicon-ok\"></span> Selected";
      }
    }

    function SetPrimaryDefaultCss(managerSub) {
      if (managerSub.PrimaryInd()) {
        return "btn btn-xs btn-primary";
      } else {
        return "btn btn-xs btn-default";
      }
    }

    function myCheckRules(manager) {

      if (!manager.IsValid()) {
        return "ManagerInvalid"
      }
    }

    function GetHRCss(manager) {
      if (manager.SubstituteManagerID()) {
        return "btn-custom btn-default SubstituteManager";
      } else {
        return "btn-custom btn-default SubstituteManager";
      }
    };

    function SetSubstituteManager(managerSub) {
      ParentRecord = managerSub.GetParent();
      ParentRecord.SubstituteManager(managerSub.SubstituteManager());
      ParentRecord.SubstituteManagerID(managerSub.SubstituteManagerID());

    }

    function CheckUniqueHumanResourceID(Value, Rule, RuleArgs) {

      var HRID = RuleArgs.Object.HumanResourceID();
      ViewModel.ManagerHumanResourceList().Iterate(function (hr) {
        if (RuleArgs.Object.Guid != hr.Guid) {
          if (hr.HumanResourceID() == HRID) {
            RuleArgs.AddError("This person has been selected already");
            return;
          }
        }
      })


    };

    function ViewManagedPeople(manager) {

      $('#ManagedPeople').modal();
      Singular.SendCommand('GetManagedHumanResources', { HumanResourceID: manager.HumanResourceID() }, function (result) {

      });

    };

    //function SaveManagerHumanResources() {
    //  Singular.SendCommand('SaveMHR', { sendModel: true }, function (result) {
    //    $('#ManagedPeople').modal('hide');
    //  });
    //}

    function CloseManagerHumanResources() {
      $('#ManagedPeople').modal('hide');
    }


    function SetHumanResourceCode(obj) {
      var HRID = obj.HumanResourceID();
      ClientData.ROHumanResourceList.Iterate(function (hr, Index) {
        if (HRID == hr.HumanResourceID) {
          if (hr.EmployeeCode != '' && hr.EmployeeCode != '0') {
            obj.HumanResourceCode(hr.EmployeeCode);
          } else {
            obj.HumanResourceCode(hr.IDNo);
          }
        }
      })
    }


    $('#ManagedPeople').on('hide.bs.modal', function (e) {

      var ErrorCount = 0
      ViewModel.ManagerHumanResourceList().Iterate(function (hr, Index) {
        if (hr.IsDirty()) {
          ErrorCount++;
        }
      });

      if (ErrorCount > 0) {
        e.preventDefault();
        alert('Please Save your changes');
        return false;
      }
    })

    function FindROHumanResource(manager, FindHRType) {
      ViewModel.FindHRType(FindHRType);
      ViewModel.CurrentManagerID(manager.HumanResourceID);
      ViewModel.ROHumanResourceFilter("");
      ViewModel.ROHumanResourceList([]);

      $('#SelectROHumanResource').modal();
    };

    function FilterROHumanResource() {
      if (ViewModel.ROHumanResourceFilter().length >= 3) {
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceList, OBLib', {
          FilterName: ViewModel.ROHumanResourceFilter()
        }, function (args) {
          if (args.Success) {
            var list = Singular.MergeSort(args.Data, function (item) { return item.Surname.toLowerCase() }, 1);
            list = Singular.MergeSort(list, function (item) { return item.Firstname.toLowerCase() }, 1);
            KOFormatter.Deserialise(list, ViewModel.ROHumanResourceList);
            Singular.HideLoadingBar();
            $('#SelectROHumanResource').modal();

          }
        });
      }
    };

    function SetHRIDs(ROHumanResource) {

      ViewModel.HRManagersList().Iterate(function (hr, Index) {
        if (hr.HumanResourceID == ViewModel.CurrentManagerID()) {
          if (ViewModel.FindHRType() == 1) { //For Substitute Manager
            hr.SubstituteManager(ROHumanResource.PreferredFirstSurname());
            hr.SubstituteManagerID(ROHumanResource.HumanResourceID());
          } else if (ViewModel.FindHRType() == 2) { //For New Manager
            hr.NewManagerName(ROHumanResource.PreferredFirstSurname());
            hr.NewManagerID(ROHumanResource.HumanResourceID());
          }
        }
      })
      $('#SelectROHumanResource').modal('hide');
    }

  </script>
</asp:Content>
