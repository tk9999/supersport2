﻿Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.Security
Imports OBLib.TeamManagement.ReadOnly
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.HR.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

Public Class PersonnelManagers
  Inherits OBPageBase(Of PersonnelManagerVM)

End Class

Public Class PersonnelManagerVM
  Inherits OBViewModelStateless(Of PersonnelManagerVM)

  Public Property UserSystemList As UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList
  Public Property RODisciplineSelectList As List(Of RODisciplineSelect) = New List(Of RODisciplineSelect)
  Public Property ROContractTypeList As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList
  Public Property ROSystemTeamNumberList As List(Of ROSystemTeamNumber) = New List(Of ROSystemTeamNumber)
  Public Property OnlyBookedInd As Boolean = True
  Public Property SelectAllHrInd As Boolean = True

  <Display(Name:="Start Date", Order:=1), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
  Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

  <Display(Name:="End Date", Order:=2), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
  Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)




  Public Property ROHumanResourceList As ROHumanResourceListPersonnelManager = ROHumanResourceListPersonnelManager.NewROHumanResourceListPersonnelManager
  'Public Property ROHumanResourceSelectList As OBLib.HR.ReadOnly.ROHumanResourceList = OBLib.HR.ReadOnly.ROHumanResourceList.NewROHumanResourceList
  Public Property ROHumanResourceFindListCriteria As ROHumanResourceListPersonnelManager.Criteria = New ROHumanResourceListPersonnelManager.Criteria
  'Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceList) = New List(Of ROHumanResourceList)

  Private Property DefaultSystemID As Integer?

  Protected Overrides Sub Setup()
    MyBase.Setup()

    'For Each ct As ROContractType In ROContractTypeList
    '  ct.SetSelected(True)
    '  ROHumanResourceFindListCriteria.ContractTypeIDs.Add(ct.ContractTypeID)
    'Next

    'Set Default System Selection
    If UserSystemList.Count > 0 Then
      For Each us As UserSystem In UserSystemList
        If Singular.Misc.CompareSafe(us.SystemID, OBLib.Security.Settings.CurrentUser.SystemID) Then
          us.IsSelected = True
          DefaultSystemID = us.SystemID
          'ROHumanResourceFindListCriteria.SystemIDs.Add(us.SystemID)
          'For Each usa As UserSystemArea In us.UserSystemAreaList
          '  If usa.IsSelected Then
          '    ROHumanResourceFindListCriteria.ProductionAreaIDs.Add(usa.ProductionAreaID)
          '  End If
          'Next
        End If
        'If us.IsSelected Then

        'End If
      Next

    End If

    'If we have a default SystemID, then get the SystemTeamNumberList
    If DefaultSystemID IsNot Nothing Then
      Dim rostnl As OBLib.TeamManagement.ReadOnly.ROSystemTeamNumberList = OBLib.TeamManagement.ReadOnly.ROSystemTeamNumberList.GetROSystemTeamNumberList(DefaultSystemID)
      'For Each rostn As OBLib.TeamManagement.ReadOnly.ROSystemTeamNumber In rostnl
      '  rostn.SetSelected(True)
      '  ROHumanResourceFindListCriteria.SystemTeamNumberIDs.Add(rostn.SystemTeamNumberID)
      'Next
      ROSystemTeamNumberList = rostnl.ToList
    End If

    'If we have a default SystemID, then get the DisciplineList
    If DefaultSystemID IsNot Nothing Then
      Dim rodl As OBLib.Maintenance.General.ReadOnly.RODisciplineSelectList = OBLib.Maintenance.General.ReadOnly.RODisciplineSelectList.GetRODisciplineSelectList(DefaultSystemID)
      'For Each rod As OBLib.Maintenance.General.ReadOnly.RODisciplineSelect In rodl
      '  rod.SetSelected(True)
      '  ROHumanResourceFindListCriteria.DisciplineIDs.Add(rod.DisciplineID)
      'Next
      RODisciplineSelectList = rodl.ToList
    End If

    IsProcessing = False

  End Sub

End Class