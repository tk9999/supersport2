﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master"
  CodeBehind="HumanResource.aspx.vb" Inherits="NewOBWeb.HumanResource" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.ShiftPatterns.ReadOnly" %>
<%@ Import Namespace="OBLib.HR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" src="../Scripts/Tools/AccreditationImage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/FindROHumanResourceControl.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/HumanResourceControl.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/FindROCountryControl.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/FindROCityModal.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/SoberControls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/HumanResourcesPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <style type="text/css">
    .modal.fade .modal-dialog {
      -webkit-transition: all 0.5s linear !important;
      -moz-transition: all 0.5s linear !important;
      -o-transition: all 0.5s linear !important;
      transition: all 0.5s linear !important;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      ''----Toolbar------------------------------------------------------------------------------------------------------------------
      'With h.Toolbar
      '  .Helpers.MessageHolder()
      'End With

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Human Resource", True, True)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.DivC("pull-left")
                With .Helpers.With(Of OBLib.HR.HumanResource)(Function(vm) ViewModel.CurrentHumanResource)
                  With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                                 Singular.Web.PostBackType.None, "HumanResourcesPage.saveHumanResource($data)")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceBO.canSave($data)")
                  End With
                End With
                With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", ,
                                               Singular.Web.PostBackType.Ajax, "FindHRControl.show()")
                End With
                With .Helpers.Bootstrap.Button(, "New Human Resource", Singular.Web.BootstrapEnums.Style.Primary, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", ,
                                               Singular.Web.PostBackType.Ajax, "HumanResourcesPage.newHumanResource()")
                End With
              End With
              With .Helpers.DivC("pull-right")
                'With .Helpers.With(Of OBLib.HR.HumanResource)(Function(vm) ViewModel.CurrentHumanResource)
                With .Helpers.Toolbar
                  With .Helpers.MessageHolder
                  End With
                End With
                'End With
              End With
            End With
          End With
        End With
      End With

      With h.With(Of OBLib.HR.HumanResource)(Function(vm) ViewModel.CurrentHumanResource)
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "HumanResourceBO.canView('OldHumanResourceSection', $data)")
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 4)
            .Helpers.Control(New NewOBWeb.HumanResourceMainInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 4)
            .Helpers.Control(New NewOBWeb.HumanResourceEmploymentInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 4)
            .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.DuplicateCount() == 0")
            .Helpers.Control(New NewOBWeb.HumanResourceSecondaryInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
          End With
        End With
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "HumanResourceBO.canView('NewHumanResourceSection', $data)")
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 3)
            .Helpers.Control(New NewOBWeb.HumanResourceMainInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 3)
            .Helpers.Control(New NewOBWeb.HumanResourceEmploymentInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
            .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.DuplicateCount() == 0")
            .Helpers.Control(New NewOBWeb.HumanResourceSecondaryInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
            With .Helpers.Bootstrap.FlatBlock("Primary Skill and Area", False, False, False)
              .FlatBlockTag.AddClass("animated slideInUp fast go")
              With .ContentTag
                'Skills
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.PrimarySkillDisciplineID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PrimarySkillDisciplineID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.PrimarySkillPositionID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PrimarySkillPositionID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                'Primary System and Area
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.PrimarySystemID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PrimarySystemID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.PrimaryProductionAreaID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PrimaryProductionAreaID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With


      With h.With(Of OBLib.HR.HumanResource)(Function(vm) ViewModel.CurrentHumanResource)
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "HumanResourceBO.canView('TabsSection', $data)")
          '.AddBinding(Singular.Web.KnockoutBindingString.if, "!$data.IsNew() && !$data.IsCheckingDuplicates() && $data.DuplicateCount() == 0")
          With .Helpers.Bootstrap.TabControl(, "", )
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "HumanResourceBO.canView('TabsSection', $data)")
            .ParentDiv.AddClass("animated slideInUp fast go")
            With .AddTab("HumanResourcePhotos", " fa-camera-retro", "", "Photos", )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  .Helpers.Control(New NewOBWeb.HumanResourcePhotos(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
                End With
              End With
            End With
            With .AddTab("OtherDetails", " fa-briefcase", "", "Other", )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  .Helpers.Control(New NewOBWeb.HumanResourceOtherInfo(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceControl"))
                End With
              End With
            End With
            With .AddTab("Skills", " fa-briefcase", "HumanResourcesPage.onSkillsTabSelected()", "Skills", )
              With .TabPane
                .Helpers.Control(New NewOBWeb.HumanResourceSkillsTab(Of NewOBWeb.HumanResourceVM)("ROHumanResourceSkillList", "HumanResourcesPage.onSkillSelected"))
              End With
            End With
            With .AddTab("Leave", "fa-photo", "HumanResourcesPage.onOffPeriodsTabSelected()", "Leave", )
              With .TabPane
                .Helpers.Control(New NewOBWeb.HumanResourceOffPeriodsTab(Of NewOBWeb.HumanResourceVM)("ROHumanResourceOffPeriodList", "HumanResourcesPage.onOffPeriodSelected", , , , "HumanResourcesPage.onOffPeriodUnAuthorised"))
              End With
            End With
            With .AddTab("Secondments", "fa-refresh", "HumanResourcesPage.onSecondmentsTabSelected()", "Secondments", )
              With .TabPane
                .Helpers.Control(New NewOBWeb.HumanResourceSecondmentsTab(Of NewOBWeb.HumanResourceVM)("ROHumanResourceSecondmentList", "HumanResourcesPage.onSecondmentSelected"))
              End With
            End With
            With .AddTab("Documents", "fa-book", "HumanResourcesPage.onDocumentsTabSelected()", "Documents", )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                    .AddClass("col-lg-offset-4 col-xl-offset-4")
                    .Helpers.Control(New NewOBWeb.HumanResourceDocumentGrid(Of NewOBWeb.HumanResourceVM)("HumanResourceTBCList", "CurrentHumanResourceControl"))
                  End With
                End With
              End With
            End With
            With .AddTab("Sub-Departments", "fa-group", "HumanResourcesPage.onAreasTabSelected()", "Sub-Depts", , False)
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                    .AddClass("col-lg-offset-4 col-xl-offset-4")
                    .Helpers.Control(New NewOBWeb.HumanResourceSystemGrid(Of NewOBWeb.HumanResourceVM)("HumanResourceAreaList", "CurrentHumanResourceControl"))
                  End With
                End With
              End With
            End With
            With .AddTab("TBCProductions", " fa-video-camera", "HumanResourcesPage.onTBCSelected()", "TBC", )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 10, 10)
                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                    .Helpers.Control(New NewOBWeb.HumanResourceTBCGrid(Of NewOBWeb.HumanResourceVM)("HumanResourceTBCList", "TBCProductions"))
                  End With
                End With
              End With
            End With
            If OBLib.Security.Settings.CurrentUser.IsOBUser Then
              With .AddTab("AddToBooking", " fa-plus-square", "HumanResourcesPage.onAddToBookingSelected()", "Assign To OB", )
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                      .Helpers.Control(New NewOBWeb.HumanResourceAddToBooking(Of NewOBWeb.HumanResourceVM))
                    End With
                  End With
                End With
              End With
            End If
            With .AddTab("HumanResourceTimesheets", "fa-clock-o", "HumanResourcesPage.onTimesheetsTabSelected()", "Timesheets", )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PullLeft
                      With .Helpers.Bootstrap.Button(, "New Timesheet", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                     "fa-plus-circle", , Singular.Web.PostBackType.None, "HumanResourcesPage.newTimesheet($data)", )
                      End With
                    End With
                    With .Helpers.Bootstrap.PullRight
                      With .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                     "fa-trash", , Singular.Web.PostBackType.None, "HumanResourcesPage.deleteSelectedTimesheets()", )
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    .Helpers.Control(New NewOBWeb.ROHumanResourceTimesheetsPagedGrid(Of NewOBWeb.HumanResourceVM)("HumanResourceTimesheetList", "HumanResourcesPage"))
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

      h.Control(New NewOBWeb.Controls.FindHumanResourceModal(Of NewOBWeb.HumanResourceVM)("FindROHumanResourceModal", "FindHRControl"))

      'h.Control(New NewOBWeb.Controls.ROCountrySelectModal(Of NewOBWeb.HumanResourceVM)("ROCountryListModal", "ROCountryList",
      '                                                                                  "ViewModel.ROCountryListCriteria()", "ViewModel.ROCountryListManager", "ViewModel.ROCountryList()",
      '                                                                                  "FindCountryControl",
      '                                                                                  "FindCountryControl.onCountrySelected", "FindCountryControl.delayedRefresh"))

      h.Control(New NewOBWeb.Controls.FindROCityModal(Of NewOBWeb.HumanResourceVM)("FindHomeCityModal", "ROCityListModal", "ROCityList",
                                                                                   "ViewModel.ROCityListCriteria()", "ViewModel.ROCityListManager", "ViewModel.ROCityList()",
                                                                                   "FindHomeCityModal.onCitySelected", "FindHomeCityModal.delayedRefresh"))


      h.Control(New NewOBWeb.Controls.HRDuplicatesModal(Of NewOBWeb.HumanResourceVM)(, ))
      h.Control(New NewOBWeb.Controls.EditHumanResourceOffPeriodModal(Of NewOBWeb.HumanResourceVM)("CurrentHumanResourceOffPeriodModal", "OffPeriodModal"))
      h.Control(New NewOBWeb.Controls.EditHumanResourceSecondmentModal(Of NewOBWeb.HumanResourceVM)("HRSecondmentsModal", "SecondmentModal"))
      h.Control(New NewOBWeb.Controls.EditHRSkillModal(Of NewOBWeb.HumanResourceVM)("CurrentHRSkillModal", "SkillModal"))
      h.Control(New NewOBWeb.Controls.EditHRTimesheetModal(Of NewOBWeb.HumanResourceVM)("CurrentHRTimesheetModal", ""))


      With h.Bootstrap.Dialog("CurrentHumanResourceUnAvailabilityModal", "UnAvailability", False, "modal-md", BootstrapEnums.Style.Warning, , "fa-coffee", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of HumanResourceOffPeriod)("ViewModel.CurrentHumanResourceUnAvailabilityTemplate()")
            With .Helpers.Bootstrap.TabControl(, "nav-tabs", , )
              With .AddTab("FindAvailability", "fa-search", "HumanResourcesPage.onFindAvailabilityTabSelected()", "Find", , True)
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.PullRight
                        With .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger,
                                                             , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash",
                                                             , Singular.Web.PostBackType.None, "HumanResourcesPage.deleteSelectedUnAvailability()", )
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ROHumanResourceUnAvailability)("ViewModel.ROHumanResourceUnAvailabilityListManager", "ViewModel.ROHumanResourceUnAvailabilityList()",
                                                                                                     False, False, False, False, True, True, True,
                                                                                                     "", Singular.Web.BootstrapEnums.PagerPosition.Bottom, , False, True, )
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y no-border-x"
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.StateButton(Function(d As OBLib.HR.ROHumanResourceUnAvailability) d.IsSelected, "Selected", "Select", , , , , )
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.HR.ROHumanResourceUnAvailability) d.StartDateString)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.HR.ROHumanResourceUnAvailability) d.EndDateString)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.HR.ROHumanResourceUnAvailability) d.Detail)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .AddTab("NewAvailability", "fa-plus-circle", , "New", , True)
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                      With .Helpers.FieldSet("Template")
                        'With .ContentTag
                        With .Helpers.With(Of HumanResourceUnAvailabilityTemplate)("ViewModel.CurrentHumanResourceUnAvailabilityTemplate()")
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDateTime)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Start Time")
                                  '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDateTime)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Start Time")
                                  '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d) d.Detail)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Detail, BootstrapEnums.InputSize.Small, , "Reason")
                                  '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              .Helpers.Bootstrap.LabelDisplay("Select Days")
                              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                With .Helpers.HTMLTag("div")
                                  .Attributes("id") = "UnAvailableDays"
                                End With
                              End With
                            End With
                          End With
                        End With
                        'End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                      With .Helpers.FieldSet("UnAvailable Days")
                        'With .ContentTag
                        With .Helpers.With(Of HumanResourceUnAvailabilityTemplate)("ViewModel.CurrentHumanResourceUnAvailabilityTemplate()")
                          With .Helpers.Bootstrap.TableFor(Of HumanResourceUnAvailability)("$data.HumanResourceUnAvailabilityList()", False, False, False, False, True, True, True, "HumanResourceUnAvailabilityList")
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            With .FirstRow
                              .AddClass("items")
                              With .AddColumn(Function(d As HumanResourceUnAvailability) d.StartDate)
                              End With
                              With .AddColumn(Function(d As HumanResourceUnAvailability) d.StartDate)
                                .HeaderText = "Time"
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                                .Helpers.Bootstrap.TimeEditorFor(Function(d As HumanResourceUnAvailability) d.StartDate, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
                              End With
                              With .AddColumn(Function(d As HumanResourceUnAvailability) d.EndDate)
                              End With
                              With .AddColumn(Function(d As HumanResourceUnAvailability) d.EndDate)
                                .HeaderText = "Time"
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                                .Helpers.Bootstrap.TimeEditorFor(Function(d As HumanResourceUnAvailability) d.EndDate, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
                              End With
                              With .AddColumn(Function(d As HumanResourceUnAvailability) d.Detail)

                              End With
                              With .AddColumn("")
                                .Style.Width = "50px"
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, ,
                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                               "fa-trash", , Singular.Web.PostBackType.None, "HumanResourceUnAvailabilityBO.remove($parent, $data)")
                                End With
                              End With
                            End With
                          End With
                        End With
                        'End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.With(Of HumanResourceUnAvailabilityTemplate)("ViewModel.CurrentHumanResourceUnAvailabilityTemplate()")
              With .Helpers.DivC("loading-custom")
                .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of HumanResourceUnAvailabilityTemplate)("ViewModel.CurrentHumanResourceUnAvailabilityTemplate()")
            With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                           PostBackType.None, "AvailabilityModal.saveAvailabilityTemplate($data)", True)
              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
            End With
          End With
          'With .Helpers.With(Of HumanResourceOffPeriod)(mControlInstanceName & ".currentOffPeriod()")
          '  With .Helpers.DivC("pull-left")
          '    With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
          '                                   PostBackType.None, mControlInstanceName & ".cancelOffPeriod($data)", False)
          '    End With
          '  End With
          '  With .Helpers.DivC("pull-right")
          '    With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
          '                                   PostBackType.None, mControlInstanceName & ".save($data)", True)
          '      .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
          '    End With
          '    With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
          '                                   PostBackType.None, mControlInstanceName & ".saveAndNew($data)", True)
          '      .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
          '    End With
          '  End With
          'End With
        End With
      End With

      With Helpers.Bootstrap.Dialog("ProductionHROBSimpleModal", "Booking",, "modal-lg", BootstrapEnums.Style.Primary,, "fa-truck",,)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of HumanResourceAddToBooking)("ViewModel.HumanResourceAddToBooking()")
            With .Helpers.With(Of OBLib.OutsideBroadcast.ProductionHROBSimple)("$data.OBBooking()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
                  With .Helpers.Bootstrap.FlatBlock("Summary")
                    With .ContentTag
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.StartDateTime)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.StartDateTime, BootstrapEnums.InputSize.Small,, "Start Time")
                          .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('StartDateTime', $data)")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.EndDateTime)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.EndDateTime, BootstrapEnums.InputSize.Small,, "End Time")
                          .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('EndDateTime', $data)")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.ResourceBookingDescription)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.ResourceBookingDescription, BootstrapEnums.InputSize.Small,, "Description")
                          .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('ResourceBookingDescription', $data)")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
                  With .Helpers.Bootstrap.FlatBlock("Schedule")
                    With .ContentTag
                      With .Helpers.Bootstrap.TableFor(Of OBLib.OutsideBroadcast.ProductionHROBSimpleDetail)("$data.ProductionHROBSimpleDetailList()", False, True,,,,,, "ProductionHROBSimpleDetailList", , "ProductionHROBSimpleDetailBO.onDetailRemoved")
                        With .FirstRow
                          With .AddReadOnlyColumn(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimpleDetail) d.TimesheetDate)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimpleDetail) d.ProductionTimelineType)
                          End With
                          With .AddColumn(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimpleDetail) d.StartDateTime)
                          End With
                          With .AddColumn(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimpleDetail) d.EndDateTime)
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.FlatBlock("Clashes")
                    With .ContentTag
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashList()", False, False,,,,,, "ClashList", , )
                        With .FirstRow
                          With .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of HumanResourceAddToBooking)("ViewModel.HumanResourceAddToBooking()")
            With .Helpers.With(Of OBLib.OutsideBroadcast.ProductionHROBSimple)("$data.OBBooking()")
              With .Helpers.DivC("pull-right")
                With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , PostBackType.None, "ProductionHROBSimpleBO.saveItem($data)")
                  .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                End With
              End With
            End With
          End With
        End With
      End With

    End Using

  %>
</asp:Content>
