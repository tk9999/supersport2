Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports Singular.Misc

Public Class Login
  Inherits OBPageBase(Of LoginVM)


  'Public Sub LoginError() Handles LoginUser.LoginError

  '  LoginUser.FailureText = OBLib.Security.OBWebSecurity.LoginError

  'End Sub

End Class

Public Class LoginVM
  Inherits OBViewModel(Of LoginVM)

#Region " Properties "

  Public Property Username As String

  Private Property mPassword As String = ""
  <System.ComponentModel.PasswordPropertyText(), Singular.DataAnnotations.TextField()>
  Public Property Password As String
    Get
      Return mPassword
    End Get
    Set(value As String)
      If mPassword <> value Then
        mPassword = value
      End If
    End Set
  End Property

#End Region

  Protected Overrides Sub Setup()
    MyBase.Setup()

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Login"
        ' OBLib.Security.Settings.CurrentUser = Nothing
        If Membership.ValidateUser(Username, Password) Then
          SetupCookie()
          'Page.Response.Redirect("~/Default.aspx")
          CheckUserPasswordOnLogin()
        Else
          AddMessage(Singular.Web.MessageType.Error, "Error Logging In", "Your username or password may be incorrect")
        End If
    End Select

  End Sub

  Private Sub SetupCookie()
    FormsAuthentication.SetAuthCookie(Username, Singular.Debug.InDebugMode)
    Dim Cookie = FormsAuthentication.GetAuthCookie(Username, Singular.Debug.InDebugMode)
    Cookie.HttpOnly = True
    If System.Web.HttpContext.Current.Request.IsSecureConnection Then
      Cookie.Secure = True
    End If
    'Cookie.Domain = "soberms.supersport.com"
    System.Web.HttpContext.Current.Response.Cookies.Add(Cookie)
  End Sub

  Private Sub CheckUserPasswordOnLogin()
    Dim usr As OBLib.Security.User = OBLib.Security.UserList.GetUser(OBLib.Security.Settings.CurrentUser.UserID)
    Dim ValidPassword As Boolean = OBLib.Security.Settings.CurrentUser.IsCurrentPasswordValid()
    'Dim FirstLogin As Boolean = usr.FirstLogin And Not FirstLogin
    If ValidPassword Then
      If OBLib.Security.Settings.CurrentUser.IsSatOpsUser Then
        HttpContext.Current.Response.Redirect("~/Schedulers/SatOpsScheduler.aspx")
      ElseIf Page.Request.QueryString("ReturnUrl") IsNot Nothing Then
        Page.Response.Redirect(Page.Request.QueryString("ReturnUrl"))
      ElseIf CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer)) Then
        HttpContext.Current.Response.Redirect("~/Dashboards/LiveEvents.aspx")
      Else
        Select Case usr.UserTypeID
          'Web Users (Freelancers for timesheets)
          Case OBLib.CommonData.Enums.UserType.Web
            HttpContext.Current.Response.Redirect("~/User/UserProfile.aspx")
          Case OBLib.CommonData.Enums.UserType.EventManager, OBLib.CommonData.Enums.UserType.EventManagerIntern
            HttpContext.Current.Response.Redirect("~/Dashboards/EventManagerDashboard.aspx")
          Case Else
            HttpContext.Current.Response.Redirect("~/Default.aspx")
        End Select
      End If
    Else
      'otherwise tell them to change their password
      Page.Response.Redirect("~/User/ChangePassword.aspx")
    End If
  End Sub

End Class
