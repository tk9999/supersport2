﻿Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

Public Class ForgotPasswordVM
  Inherits OBViewModel(Of ForgotPasswordVM)

  Public Property Username As String
  Public Property PasswordResetSucceededInd As Boolean
  Public Property ResetError As String = ""

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "ResetPassword"
        'Get the user
        Dim User As OBLib.Security.User = OBLib.Security.UserList.GetUserByUserName(Username) 'OBLib.CommonData.Lists.UserList.Where(Function(d) d.LoginName = Username).FirstOrDefault
        If User IsNot Nothing Then
          If Singular.Misc.IsNullNothingOrEmpty(User.EmailAddress) Then
            PasswordResetSucceededInd = False
            ResetError = "No email address found for '" & Username & "'. Please contact your system admin."
            OBLib.OBMisc.LogClientError("Someone", "Reset Password", "Reset Password", ResetError)
            AddMessage(Singular.Web.MessageType.Error, "Error Resetting Password", "No email address found for this User. Please contact your system admin.")
          Else
            'send the email
            Dim wr As Singular.Web.Result = User.ResetPassword()
            If wr.Success Then
              PasswordResetSucceededInd = True
              User.SendLoginDetails("https://soberms.supersport.com/Account/Login.aspx")
            Else
              ResetError = wr.ErrorText
              OBLib.OBMisc.LogClientError("Someone", "Reset Password", "Reset Password", ResetError)
              AddMessage(Singular.Web.MessageType.Error, "Error Resetting Password", wr.ErrorText)
            End If
          End If
        Else
          'user has not been found
          ResetError = "Username '" & Username & "' cannot not found"
          OBLib.OBMisc.LogClientError("Someone", "Reset Password", "Reset Password", ResetError)
          AddMessage(Singular.Web.MessageType.Error, "Error Resetting Password", ResetError)
        End If
      Case "BackToLogin"
        HttpContext.Current.Response.Redirect("~/Account/Login.aspx")
    End Select

  End Sub

End Class