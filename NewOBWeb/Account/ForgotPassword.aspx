﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Login.Master"
  CodeBehind="ForgotPassword.aspx.vb" Inherits="NewOBWeb.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="../Styles/bootstrap/override/signin.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    body, .panel, .panel-heading
    {
      background-image: none !important;
      background-color: #ffffff !important;
      background-repeat: repeat-x !important;
    }
    .panel
    {
      border: none;
    }
    .panel-default > .panel-heading
    {
      border: none;
      background-color: #ffffff !important;
    }
    .container
    {
      margin-top: 100px;
    }
    .Msg div, .ValidationPopup
    {
      border-style: solid;
      border-width: 1px;
      border-radius: 4px;
      margin: 6px 0 5px 0;
      background-repeat: no-repeat;
      background-position: 3px 3px;
      padding: 3px 10px 6px 24px;
      font-size: 0.9em;
      color: #000;
    }
    .Msg div strong
    {
      line-height: 1.5em;
    }
    .Msg-Validation, .Msg-Error
    {
      background-color: #FBECEB;
      border-color: #c44;
    }
    .Msg-Validation
    {
      background-image: url('../Images/IconBRule.png');
      overflow: auto;
    }
    .Msg-Validation > ul
    {
      padding-left: 0;
    }
    .Msg-Validation strong, .Msg-Error strong
    {
      color: #c44;
      line-height: 1.2em;
    }
    .Msg-Error
    {
      background-image: url('../Images/IconError.png');
    }
    .Msg-Success
    {
      background-image: url('../Images/IconAuth.png');
      background-color: #F1F8EE;
      border-color: #4a2;
    }
    .Msg-Success strong
    {
      color: #291;
    }
    
    .Msg-Warning
    {
      background-image: url('../Images/IconWarning.png');
      background-color: #FFF7E5;
      border-color: #D9980D;
    }
    .Msg-Warning strong
    {
      color: #D9980D;
    }
    
    .Msg-Information
    {
      background-image: url('../Images/IconInfo.png');
      background-color: #E9F1FB;
      border-color: #225FAA;
    }
    .Msg-Information strong
    {
      color: #225FAA;
    }
    .MsgBoxMessage > ul
    {
      padding-left: 0;
    }
    .ErrorMessage
    {
      padding: 10px;
      color: red;
      font-size: 14px;
      font-weight: bold;
    }
  </style>
  <script type="text/javascript">
    $('html').bind('keypress', function (e) {
      if (e.keyCode == 13) {
        return false;
      }
    }); 
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      h.MessageHolder()
        
      With h.DivC("login-wrapper")
        
        With .Helpers.DivC("box")
          .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) Not ViewModel.PasswordResetSucceededInd)
          With .Helpers.DivC("content-wrap")
            With .Helpers.HTMLTag("h6")
              .Helpers.HTML("Reset Password")
            End With
            With .Helpers.EditorFor(Function(d) ViewModel.Username)
              .AddClass("form-control")
              .Attributes("type") = "text"
              .Attributes("placeholder") = "Username"
            End With
            With .Helpers.BootstrapButton("ResetPassword", "Reset Password", "btn-glow primary", "", Singular.Web.PostBackType.Full, False, , )
              '.Button.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) Not ViewModel.PasswordResetSucceededInd)
            End With
            With .Helpers.DivC("row")
              With .Helpers.ReadOnlyFor(Function(d) d.ResetError, Singular.Web.FieldTagType.label)
                .AddClass("ErrorMessage")
                .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) Not ViewModel.PasswordResetSucceededInd)
              End With
            End With
          End With
        End With
        
        With .Helpers.DivC("box")
          .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) ViewModel.PasswordResetSucceededInd)
          With .Helpers.DivC("content-wrap")
            With .Helpers.HTMLTag("h6")
              .Helpers.HTML("Password Reset Successfully")
            End With
            With .Helpers.Div
              .Helpers.HTML("Your password has been sent to your email address")
            End With
            With .Helpers.BootstrapButton("BackToLogin", "Back To Login", "btn-glow success", "glyphicon-ok", Singular.Web.PostBackType.Full, False, , )
              '.Button.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) Not ViewModel.PasswordResetSucceededInd)
            End With
          End With
        End With
        
      End With
      
    End Using
      
  %>
</asp:Content>
