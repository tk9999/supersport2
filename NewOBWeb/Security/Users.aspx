﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="Users.aspx.vb"
  Inherits="NewOBWeb.Users" %>

<%@ Import Namespace="OBLib.Maintenance.HR.ReadOnly" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.HR" %>
<%@ Import Namespace="OBLib" %>
<%@ Import Namespace="OBLib.Security" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    .ValidationPopup {
      min-height: 131px;
      max-height: 650px;
    }

    .error-validation-styling {
      height: auto;
      width: 400px;
      visibility: visible;
      overflow-y: scroll;
      max-height: 70px;
      z-index: 9999;
    }
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Users.js"></script>
  <script type="text/javascript" src="../Scripts/Pages/UserSecurityPage.js"></script>
  <script type="text/javascript">

    Singular.FilterSecurityGroupList = function (List) {
      //This function can be overridden in the users page.
      return List;
    }

    ROUserListCriteriaBO.FilterNameSet = function (self) {
      ViewModel.UserPagingInfo().Refresh()
    }

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%

    Using h = Helpers
      'With Helpers.DivC("pull-right")
      '  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '    With .Helpers.Toolbar
      '      With .Helpers.MessageHolder()
      '      End With
      '    End With
      '  End With
      'End With
      If Singular.Security.HasAccess("Security", "Manage Users") Then

        With h.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.TabControl("tab-container", "nav-tabs", )
              With .AddTab("Users", "", , "Users", , True)
                .TabPane.AddClass("active")
                .TabContentContainer.AddClass("active")
                With .TabPane
                  With .Helpers.Bootstrap.FlatBlock("User Security List", True)
                    With .Helpers.DivC("loading-custom")
                      .AddBinding(KnockoutBindingString.visible, "ViewModel.IsProcessing()")
                      .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
                    End With
                    .FlatBlockTag.AddClass("slow slideInUp go")
                    With .AboveContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column
                          With .Helpers.DivC("pull-right")
                            With .Helpers.Bootstrap.Button(, "New User", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-user",
                                                             , Singular.Web.PostBackType.None, "UserSecurityPage.AddNewUser()")
                            End With
                            With .Helpers.Bootstrap.Button(, "Show Advanced Filters", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                                Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-sort-alpha-asc", ,
                                                                    Singular.Web.PostBackType.None, "UserSecurityPage.ShowFilters()")
                            End With
                            With .Helpers.Bootstrap.Button(, "Send Login Details", Singular.Web.BootstrapEnums.Style.Info, ,
                                                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-inbox", ,
                                                                   Singular.Web.PostBackType.None, "UserSecurityPage.BulkSendPasswords()")
                            End With
                            With .Helpers.Bootstrap.Button(, "Reset Password", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-recycle", ,
                                                                   Singular.Web.PostBackType.None, "UserSecurityPage.BulkResetPasswords()")
                            End With
                          End With
                        End With

                      End With

                      With .Helpers.Bootstrap.Row
                        'Little gap to keep things clean af
                      End With

                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormControlFor(Function(p) ViewModel.UserListCriteria.FilterName,
                                                                   Singular.Web.BootstrapEnums.InputSize.Small, , "Employee Name...")
                          End With
                        End With
                      End With

                      With .Helpers.Bootstrap.Row
                        'Little gap to keep things clean af
                      End With

                    End With
                    With .ContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.General.ReadOnly.ROUser)(Function(c) c.UserPagingInfo,
                                                                                                             Function(c) c.ROUserList,
                                                                                                             False, False, False, False, True, True, True,
                                                                                                               , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                            .AllowClientSideSorting = True
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            With .FirstRow
                              .AddClass("items selectable")
                              With .AddColumn("")
                                .Style.Width = "150px"
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "Selected", "Select", "btn-primary",
                                                                       "btn-default", "fa fa-check-square-o", , )
                                End With
                                With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil",
                                                                 , Singular.Web.PostBackType.Ajax, "UserSecurityPage.EditUser($data)")
                                End With
                              End With
                              With .AddReadOnlyColumn(Function(c) c.FirstName)
                              End With
                              With .AddReadOnlyColumn(Function(c) c.Surname)
                              End With
                              With .AddReadOnlyColumn(Function(c) c.LoginName)
                              End With
                              With .AddReadOnlyColumn(Function(c) c.ContractType)
                              End With
                              With .AddReadOnlyColumn(Function(c) c.System)
                              End With
                              With .AddReadOnlyColumn(Function(c) c.ProductionArea)
                              End With
                              With .AddReadOnlyColumn(Function(c) c.EmailAddress)
                              End With
                              If Singular.Security.HasAccess("Security", "Can Delete Users") Then
                                With .AddColumn("")
                                  With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, ,
                                                                 Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall,
                                                                   , "fa-trash", , Singular.Web.PostBackType.None,
                                                                 "UserSecurityPage.DeleteROUser($data)")
                                  End With
                                End With
                              End If
                            End With
                          End With
                        End With
                      End With
                    End With

                  End With
                End With
              End With
            End With
          End With
        End With

        With h.Bootstrap.Dialog("ShowFilters", "Filter Users", , "modal-xs", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", , True)
          With .Body
            .AddClass("modal-background-gray")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)    'System&AreaFilter
                With .Helpers.Bootstrap.FlatBlock("Sub-Depts and Areas", True)
                  With .ContentTag
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      .AddClass("row")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ROUserBO.AddSystem($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                          .Button.AddClass("btn-block buttontext")
                        End With
                        With .Helpers.Bootstrap.Row
                          .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                            .AddClass("row")
                            With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                              .AddClass("col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xl-offset-1")
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                .Button.AddBinding(KnockoutBindingString.click, "ROUserBO.AddProductionArea($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                .Button.AddClass("btn-block buttontext")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 8, 8)   'Contract Filter
                With .Helpers.Bootstrap.FlatBlock("Contract Types", True)
                  With .ContentTag
                    With .Helpers.ForEachTemplate(Of ROContractType)(Function(d As NewOBWeb.UserVM) d.ROContractTypeList)
                      .AddClass("row")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ROUserBO.AddContractType($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

        With h.Bootstrap.Dialog("UserModal", "Manage User Account", , "modal-md", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", , False)
          With .Body
            .AddClass("modal-background-gray")
            With .Helpers.Bootstrap.Row
              With .Helpers.DivC("pull-right")
                With .Helpers.Toolbar
                  .Helpers.MessageHolder()
                End With
              End With
              With .Helpers.With(Of User)(Function(vm) ViewModel.SelectedUser)
                With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                  With .Helpers.Bootstrap.FlatBlock("Current User", True)
                    With .AboveContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, , 12, 12)
                          With .Helpers.Bootstrap.Button(, "Send Login Details", Singular.Web.BootstrapEnums.Style.Info, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-inbox", ,
                                                         Singular.Web.PostBackType.None, "UserBO.sendLoginDetails($data)", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "UserSecurityPage.CanSendLoginDetails()")
                          End With
                          With .Helpers.Bootstrap.Button(, "Reset Password", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-recycle", ,
                                                         Singular.Web.PostBackType.None, "UserBO.resetPassword($data)", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "UserSecurityPage.CanResetPassword()")
                          End With
                        End With
                      End With
                    End With
                    With .ContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.FirstName)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.FirstName, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.Surname)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.Surname, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.PreferredName)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.PreferredName, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.EmailAddress)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.EmailAddress, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.LoginName)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.LoginName, Singular.Web.BootstrapEnums.InputSize.Small)
                              .Attributes("autocomplete") = "false"
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.NewPassword)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.NewPassword, Singular.Web.BootstrapEnums.InputSize.Small)
                              .Attributes("autocomplete") = "false"
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.SystemID)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.ProductionAreaID)
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As User) c.HumanResourceID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(c As User) c.HumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With

                With .Helpers.Bootstrap.Column(12, 12, 6, 8, 8)
                  With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Top)
                    With .AddTab("SecurityGroups", "fa-lock", , "Security Groups", , )
                      With .TabPane
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.DivC("table-responsive")
                              With .Helpers.Bootstrap.TableFor(Of Singular.Security.SecurityGroupUser)(Function(c) c.SecurityGroupUserList,
                                                                                                       True, True, False, True, True, True, False,
                                                                                                       "SecurityGroupUserList")
                                .AllowClientSideSorting = True
                                .AddClass("hover list")
                                .TableBodyClass = "no-border-y no-border-x"
                                With .FirstRow
                                  .AddClass("items selectable")
                                  With .AddColumn(Function(c As Singular.Security.SecurityGroupUser) c.SecurityGroupID)
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .AddTab("UserSystems", "fa-lock", , "Sub-Depts and Areas", , )
                      With .TabPane
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.DivC("table-responsive")
                              With .Helpers.Bootstrap.TableFor(Of OBLib.Security.UserSystem)(Function(c) c.UserSystemList,
                                                                                             True, True, False, True, True, True, False,
                                                                                             "UserSystemList")
                                .AllowClientSideSorting = True
                                .AddClass("hover list")
                                .TableBodyClass = "no-border-y no-border-x"
                                With .FirstRow
                                  .AddClass("items selectable")
                                  With .AddColumn(Function(c As OBLib.Security.UserSystem) c.SystemID)
                                  End With
                                End With
                                With .AddChildTable(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList,
                                                                                      True, True, False, False, True, True, False,
                                                                                      "UserSystemAreaList")
                                  .AllowClientSideSorting = True
                                  .AddClass("no-border hover list")
                                  .TableBodyClass = "no-border-y no-border-x"
                                  With .FirstRow
                                    .AddClass("items selectable")
                                    With .AddColumn(Function(c As OBLib.Security.UserSystemArea) c.ProductionAreaID)
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .AddTab("UserSchedulers", "fa-lock", , "Schedulers", , )
                      With .TabPane
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.DivC("table-responsive")
                              With .Helpers.Bootstrap.TableFor(Of UserResourceScheduler)(Function(c) c.UserResourceSchedulerList,
                                                                                         True, True, False, True, True, True, False,
                                                                                         "UserResourceSchedulerList")
                                .AllowClientSideSorting = True
                                .AddClass("hover list")
                                .TableBodyClass = "no-border-y no-border-x"
                                With .FirstRow
                                  .AddClass("items selectable")
                                  With .AddColumn(Function(c As OBLib.Security.UserResourceScheduler) c.ResourceSchedulerID)
                                  End With
                                  With .AddColumn("Read Only?")
                                    .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserResourceScheduler) c.ReadOnlyAccess, , , , , , , )
                                  End With
                                  With .AddColumn("Is Administrator?")
                                    .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserResourceScheduler) c.IsAdministrator, , , , , , , )
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With

              End With
            End With
          End With
          With .Footer
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , , "UserSecurityPage.SaveCurrentUser()", )
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "UserSecurityPage.UserValid()")
              End With
            End With
            'With .Helpers.Bootstrap.Button(, "Cancel", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-remove", , , "UserSecurityPage.CancelCurrentUser()", )
            '  With .Button
            '  End With
            'End With
          End With
        End With

      End If

      'h.Control(New NewOBWeb.Controls.BulkAddUserTimesheetAccess(Of NewOBWeb.UserVM)("BulkAddTimesheetAccess", "UserSecurityPage", "UserSecurityBO"))

    End Using

  %>
</asp:Content>