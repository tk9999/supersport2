﻿Imports Singular.DataAnnotations
Imports OBWebReports
Imports Singular
Imports OBLib.Productions.ReadOnly
Imports Singular.Web
Imports Singular.Web.Security
Imports Singular.Security
Imports System

Public Class SecurityGroups
  Inherits OBPageBase(Of NewSecurityVM)

End Class

Public Class NewSecurityVM
  Inherits OBViewModel(Of NewSecurityVM)

  Public Property ROFilteredSecurityRoles As List(Of ROSecurityRole) = New List(Of ROSecurityRole)
  Public Shared Property EditRoles_SecurityRole = "Security.Edit Roles"
  Public Property SecurityRoleID As Integer
  Public Property SectionName As String
  Public Property CurrentGroup As Singular.Security.SecurityGroup
  Private mSecurityGroupList As Singular.Security.SecurityGroupList
  Private mROSecurityRoleList As Singular.Security.ROSecurityRoleHeaderList

  Protected Overrides Sub Setup()
    MyBase.Setup()

    mSecurityGroupList = Singular.Security.SecurityGroupList.GetSecurityGroupList
    mROSecurityRoleList = Singular.Security.ROSecurityRoleHeaderList.GetROSecurityRoleHeaderList
  End Sub

  Public ReadOnly Property SecurityGroupList As Singular.Security.SecurityGroupList
    Get
      Return mSecurityGroupList
    End Get
  End Property

  <Singular.DataAnnotations.ClientOnly(False)>
  Public ReadOnly Property ROSecurityRoleHeaderList As Singular.Security.ROSecurityRoleHeaderList
    Get
      Return mROSecurityRoleList
    End Get
  End Property

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "Undo"
        AddMessage(Singular.Web.MessageType.Information, "Undo", "Changes to " & CurrentGroup.SecurityGroup & " roles have been un-done.")
        CurrentGroup = Nothing

      Case "Save"
        SaveSecurityGroup(CommandArgs)
        CurrentGroup = Nothing

      Case "SaveGroups"
        SaveGroups(SecurityGroupList, CommandArgs)
        CurrentGroup = Nothing

    End Select
  End Sub

  Private Sub SaveSecurityGroup(CommandArgs As CommandArgs)

    If CurrentGroup IsNot Nothing Then
      CurrentGroup.UpdateFromSecurityRoleList(ROSecurityRoleHeaderList)
    End If
    Dim WasDirty As Boolean = mSecurityGroupList.IsDirty
    With TrySave(mSecurityGroupList)
      If Not .Success Then
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Error"}
      Else
        mSecurityGroupList = .SavedObject
        CommandArgs.ReturnData = New Singular.Web.Result(True) With {.ErrorText = "Success"}
      End If
    End With
  End Sub

  Private Sub SaveGroups(GroupList As Singular.Security.SecurityGroupList, CommandArgs As CommandArgs) ' As Singular.Web.SaveResult

    Dim WasDirty = GroupList.IsDirty
    If Singular.Security.HasAccess(EditRoles_SecurityRole) Then
      Dim Result As New Singular.Web.SaveResult(GroupList.TrySave)
      CommandArgs.ReturnData = Result
    Else
      Throw New Exception("Access denied")
    End If
  End Sub
End Class