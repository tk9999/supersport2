﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="NewSecurity.aspx.vb" Inherits="NewOBWeb.SecurityGroups" %>

<%@ Import Namespace="Singular.Web.Security" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="Singular.Security" %>

<%@ Import Namespace="Singular.Server.Security" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

    .add-text-align-left {
      text-align: left;
                        }

    .buttontext {
      overflow: hidden;
      white-space: nowrap;
      display: block;
      text-overflow: ellipsis;
                }
    
        .FadeHide {
      visibility : hidden;
      opacity: 0;
      transition: visibility 1s, opacity 1s;
    }

    .FadeDisplay {
      visibility : visible;
      opacity: 1;
      transition: visibility 1s, opacity 1s linear;
    }


    .security-group-editroles {
      width: 10%
    }

    .security-group-securitygroup {
      width: 40%;
      word-wrap: break-word;
      overflow: hidden
    }

    .security-group-securitygroupdescription {
      width: 50%;
      word-wrap: break-word;
    }

    tr > td.LButtons, tr > td.select-button {
      width: 50px;
    }

    .select-column {
      width: 11%;
    }

    .security-role-rolename {
      width: 25%;
    }

    .security-role-roledescription {
      width: 64%;
    }

    </style>

    <script type="text/javascript" src="../Scripts/Pages/NewSecurityPage.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
      Dim SectionName As String = ""
     
      Using h = Helpers
      
          With h.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                   .Attributes("id") = "SecurityGroupsRoles"
                  With .Helpers.With(Of SecurityGroup)(Function(d) ViewModel.SecurityGroupList)
                      With .Helpers.Bootstrap.FlatBlock("Security Groups and Roles", True)
                           .FlatBlockTag.AddClass("animated slideInUp")
                          With .AboveContentTag
                              With .Helpers.Button(Singular.Web.DefinedButtonType.Save)
                                   .AddBinding(KnockoutBindingString.click, "SecurityGroupPage.SaveSecurityGroup()")
                                   .PostBackType = PostBackType.None
                              End With
                          End With
                          With .ContentTag
                              With .Helpers.Bootstrap.Row
                                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                       .AddClass("top-buffer-10")
                                      With .Helpers.Bootstrap.TableFor(Of SecurityGroup)(Function(d) ViewModel.SecurityGroupList, True, False, False, True, True, True, False)
                                           .TableBodyClass = "no-border-y"
                                           '.RemoveButton.AddBinding(Singular.Web.KnockoutBindingString.disable, "SecurityGroupPage().DisableDeleteButton()")
                                          With .FirstRow
                                              With .AddColumn("")
                                                   .AddClass("security-group-editroles")
                                                  With .Helpers.Bootstrap.Button(, "Edit Roles", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, "SecurityGroupPage.EditSecurityGroupRoles($data)", False)
                                                  End With
                                              End With
                                              With .AddColumn(Function(d As SecurityGroup) d.SecurityGroup)
                                                   .AddClass("security-group-securitygroup")
                                              End With
                                              With .AddColumn(Function(d As SecurityGroup) d.Description)
                                                   .AddClass("security-group-securitygroupdescription")
                                              End With
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                      End With
                  End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                   .Attributes("id") = "GroupsRoles"
                   .AddClass("FadeHide")
                  With .Helpers.Bootstrap.FlatBlock("Groups and Roles", , , , )
                      With .ContentTag
                          With .Helpers.FieldSet("")
                               .Legend.AddBinding(KnockoutBindingString.text, "SecurityGroupPage.CurrentGroupName()")
                              With .Helpers.Button(Singular.Web.DefinedButtonType.Save)
                                   .AddBinding(KnockoutBindingString.click, "SecurityGroupPage.SaveSecurityRoles()")
                                   .PostBackType = PostBackType.Ajax
                              End With
                              With .Helpers.Button(Singular.Web.DefinedButtonType.Undo)
                                   .AddBinding(KnockoutBindingString.visible, Function(c) c.CurrentGroup IsNot Nothing)
                                   .AddBinding(KnockoutBindingString.click, "SecurityGroupPage.Undo()")
                                   .PostBackType = PostBackType.Ajax
                              End With
                              With .Helpers.Bootstrap.TableFor(Of Singular.Security.ROSecurityRoleHeader)(Function(c) c.ROSecurityRoleHeaderList, False, False, False, True, True, True, False)
                                   .FirstRow.AddReadOnlyColumn(Function(c) c.SectionName, "Sections").FieldDisplay.TagType = Singular.Web.FieldTagType.strong
                                  With .AddChildTable(Of Singular.Security.ROSecurityRole)(Function(c) c.ROSecurityRoleList, False, False)
                                      With .FirstRow.AddColumn("")
                                           .AddClass("select-column")
                                          With .Helpers.Bootstrap.StateButton(Function(c) c.SelectedInd(), "Selected", "Select", "btn-success", "btn-default", "fa-check-square-o", , )
                                               .Button.AddBinding(KnockoutBindingString.click, "SecurityGroupPage.AddSecurityRole($data)")
                                          End With
                                      End With
                                      With .FirstRow.AddColumn("Role")
                                           .AddClass("security-role-rolename")
                                          With .Helpers.ReadOnlyFor(Function(c) c.SecurityRole, Singular.Web.FieldTagType.label)
                                          End With
                                      End With
                                      With .FirstRow.AddColumn("Description")
                                           .AddClass("security-role-roledescription")
                                          With .Helpers.ReadOnlyFor(Function(c) c.Description, Singular.Web.FieldTagType.label)
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                      End With
                  End With
              End With
          End With
      End Using
    %><asp:Image ID="Image1" runat="server" />

</asp:Content>
