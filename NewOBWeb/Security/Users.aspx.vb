﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports Singular
Imports OBLib.Maintenance.General.ReadOnly
Imports System.ComponentModel
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.Security
Imports Singular.Web
Imports OBLib.Timesheets.OBCity
Imports System

Public Class Users
  Inherits OBPageBase(Of UserVM)

End Class

Public Class UserVM
  Inherits OBViewModelStateless(Of UserVM)

  Public Property ROUserList As OBLib.Maintenance.General.ReadOnly.ROUserList
  Public Property UserPagingInfo As Singular.Web.Data.PagedDataManager(Of UserVM)
  Public Property UserListCriteria As OBLib.Maintenance.General.ReadOnly.ROUserList.Criteria

  Public Property UsersToAddList As List(Of OBLib.Security.User)
  Public Property UserIDsToRemoveList As List(Of Integer)

  <InitialDataOnly>
  Public Property ROUserFindListPagedList As ROUserFindListPagedList
  <InitialDataOnly>
  Public Property ROUserFindListPagedListCriteria As ROUserFindListPagedList.Criteria
  <InitialDataOnly>
  Public Property ROUserFindListPagedListManager As Singular.Web.Data.PagedDataManager(Of UserVM)

  Public Property ROContractTypeList As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

  Public Property SelectedUser As OBLib.Security.User = Nothing

#Region "User Systems"

  Public Property UserSystemList As OBLib.Security.UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

#End Region

  Protected Overrides Sub Setup()
    MyBase.Setup()

    OBLib.CommonData.Refresh("UserList")
    OBLib.CommonData.Refresh("ROUserList")

    UsersToAddList = New List(Of OBLib.Security.User)
    UserIDsToRemoveList = New List(Of Integer)

    ROUserList = OBLib.Maintenance.General.ReadOnly.ROUserList.NewROUserList
    UserListCriteria = New OBLib.Maintenance.General.ReadOnly.ROUserList.Criteria '(True, NameFilter, Nothing)
    UserPagingInfo = New Singular.Web.Data.PagedDataManager(Of UserVM)(Function(d) Me.ROUserList,
                                                                       Function(d) Me.UserListCriteria,
                                                                       "FirstName", 20, True)

    UserListCriteria.PageSize = 20
    UserListCriteria.SortColumn = "FirstName"
    UserListCriteria.SortAsc = True
    SetupDataSource()

    ROUserFindListPagedList = New ROUserFindListPagedList
    ROUserFindListPagedListCriteria = New ROUserFindListPagedList.Criteria
    ROUserFindListPagedListManager = New Singular.Web.Data.PagedDataManager(Of UserVM)(Function(d) d.ROUserFindListPagedList,
                                                                                       Function(d) d.ROUserFindListPagedListCriteria,
                                                                                       "FirstName", 15)

    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.CommonData.Lists.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("SecurityGroupList", SecurityGroupList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    Dim rors As OBLib.Scheduling.ReadOnly.ROResourceSchedulerList = OBLib.Scheduling.ReadOnly.ROResourceSchedulerList.GetROResourceSchedulerList()
    ClientDataProvider.AddDataSource("ROResourceSchedulerList", rors, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplineList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplineList, False)

  End Sub

  Public Shared Function SaveUser(User As OBLib.Security.User) As Singular.Web.Result
    'Creating a UserList 
    Dim ulist As OBLib.Security.UserList = New OBLib.Security.UserList
    ulist.Add(User)
    'Checking if the User is a New user and if the new password is not blank, then using the allocated password
    If User.IsNew AndAlso User.NewPassword <> "" Then
      User.Password = User.NewPassword
    End If
    'Check if the user is a new user before saving
    Dim WasNew As Boolean = User.IsNew
    User.CheckRules()

    'Try to save UserList
    Dim sh As Singular.SaveHelper = ulist.TrySave
    If sh.Success Then
      'If saving is a success, the SaveHelper returns a copy of Ulist that is being saved
      ulist = CType(sh.SavedObject, OBLib.Security.UserList)
      If WasNew Then
        'If User was newly created, send the login details
        User.SendLoginDetails("https://soberms.supersport.com/Account/Login.aspx")
      End If
      'Change the "New" user state to false (now that it has been created and is no longer a new user)
      WasNew = False
      User = ulist(0)
      'Refresh the CommonData and return the results
      OBLib.CommonData.Refresh("UserList")
      OBLib.CommonData.Refresh("ROUserList")
      Return New Singular.Web.Result(True)
    Else
      'If SaveHelp was not a success, return an error message
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If

  End Function

  Public Shared Function SetCurrentUser(UserID As Integer) As Singular.Web.Result
    'Make a le variable to hold the user
    Dim UserToFetch As OBLib.Security.User = Nothing
    Try
      'Get the selected user through the ID passed into the method and assign it to the variable
      UserToFetch = OBLib.Security.UserList.GetUser(UserID)
    Catch ex As Exception
      'Display error message on exception
      Dim x = New Singular.Web.Result(False)
      x.ErrorText = ex.Message
      Return x
    End Try
    'Return a successful result with the selected user as .Data
    Return New Singular.Web.Result(True) With {.Data = UserToFetch}
  End Function

  Public Shared Function NewCurrentUser(User As OBLib.Security.User) As Singular.Web.Result

    'Sets the selected user to blank and passes it through as .Data (Allows for blank fields on modal for New User Click)
    User = New OBLib.Security.User
    Return New Singular.Web.Result(True) With {.Data = User}

  End Function

  Public Shared Function DeleteUser(UserID As Integer) As Singular.Web.Result
    'instantiate variable and make it equal to the selected user
    Dim UserToFetch As OBLib.Security.User = OBLib.Security.UserList.GetUser(UserID)
    If UserToFetch IsNot Nothing Then
      'Uses the magical backend process of adding the user to the deleted user list (by clearing) then saving the list to delete from DB (trysave)
      Dim ul As New UserList
      ul.Add(UserToFetch)
      ul.Clear()
      'Make the TrySave equal to a SaveHelper so we can tell if it was a great success or not
      Dim sh As Singular.SaveHelper = ul.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True)
      Else
        'Try save failed, meaning even though the user was there, but it could not be deleted
        Dim x = New Singular.Web.Result(False)
        x.ErrorText = "Could not delete user"
        Return x
      End If
    Else
      'UserToFetch was nothing
      Dim x = New Singular.Web.Result(False)
      x.ErrorText = "Could not find user"
      Return x
    End If

  End Function


  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:=New String() {"Security.Apply Access Changes"})>
  Public Function CleanUserList(UsersList As List(Of OBLib.Security.User)) As Singular.Web.Result
    Dim Rslt As Singular.Web.Result = New Singular.Web.Result(True)
    UsersList.Clear()
    Rslt.Data = UsersList
    Return Rslt
  End Function

  <Singular.Web.WebCallable(loggedInOnly:=True, Roles:=New String() {"Security.Apply Access Changes"})>
  Public Function UpdateBulkUserList(UserID As Integer, ThisUserList As List(Of OBLib.Security.User)) As Singular.Web.Result
    Dim Rslt As Singular.Web.Result = New Singular.Web.Result(True)
    Dim THisUser As OBLib.Security.User = UserList.GetUserList(UserID)(0)
    Dim Element As OBLib.Security.User = ThisUserList.Where(Function(c) Singular.Misc.CompareSafe(c.UserID, UserID))(0)
    If Element IsNot Nothing Then
      ThisUserList.Remove(Element)
      Rslt.Data = Element.UserID
    Else
      ThisUserList.Add(THisUser)
    End If
    Rslt.Data = ThisUserList
    Return Rslt
  End Function

  Private Sub SetupDataSource()
    ROUserList = OBLib.Maintenance.General.ReadOnly.ROUserList.GetROUserList(UserListCriteria)
  End Sub

  <System.ComponentModel.Browsable(False)>
  Public ReadOnly Property SecurityGroupList As Singular.Security.SecurityGroupList
    Get
      Return Singular.Security.SecurityGroupList.GetSecurityGroupList
    End Get
  End Property

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function SendLoginDetails(User As OBLib.Security.User) As Singular.Web.Result
    Return User.SendLoginDetails("https://soberms.supersport.com/Account/Login.aspx")
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function ResetPassword(User As OBLib.Security.User) As Singular.Web.Result
    Dim wr As Singular.Web.Result = User.ResetPassword()
    If wr.Success Then
      Return User.SendLoginDetails("https://soberms.supersport.com/Account/Login.aspx")
    Else
      Return wr
    End If
  End Function
  'End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function BulkSendPasswords(SystemIDs As List(Of Integer), ProductionAreaIDs As List(Of Integer), ContractTypeIDs As List(Of Integer), FilterName As String, SelectedUserIDs As List(Of Integer)) As Singular.Web.Result
    If SystemIDs.Count = 0 And ProductionAreaIDs.Count = 0 And ContractTypeIDs.Count = 0 And FilterName = "" And SelectedUserIDs.Count = 0 Then
      Return New Singular.Web.Result(False) With {.ErrorText = "Please choose some users"}
    End If
    Try
      'Get a filtered UserList based on these parameters
      Dim crit As OBLib.Security.UserList.Criteria = New OBLib.Security.UserList.Criteria With {
                                                                                                    .SystemIDs = SystemIDs,
                                                                                                    .ProductionAreaIDs = ProductionAreaIDs,
                                                                                                    .ContractTypeIDs = ContractTypeIDs,
                                                                                                    .FilterName = FilterName,
                                                                                                    .SelectedUserIDs = SelectedUserIDs
                                                                                                }
      Dim UserList As OBLib.Security.UserList = OBLib.Security.UserList.GetUserList(crit)
      'Once we have the filtered UserList, loop through each user and call the send password method
      For Each User In UserList
        User.SendLoginDetails("https://soberms.supersport.com/Account/Login.aspx")
      Next
    Catch ex As Exception
      Dim x = New Singular.Web.Result(False)
      x.ErrorText = ex.Message
      Return x
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function BulkResetPasswords(SystemIDs As List(Of Integer), ProductionAreaIDs As List(Of Integer), ContractTypeIDs As List(Of Integer), FilterName As String, SelectedUserIDs As List(Of Integer)) As Singular.Web.Result
    If SystemIDs.Count = 0 And ProductionAreaIDs.Count = 0 And ContractTypeIDs.Count = 0 And FilterName = "" And SelectedUserIDs.Count = 0 Then
      Return New Singular.Web.Result(False) With {.ErrorText = "Please choose some users"}
    End If
    Try
      'Get a filtered UserList based on these parameters
      Dim crit As OBLib.Security.UserList.Criteria = New OBLib.Security.UserList.Criteria With {
                                                                                                    .SystemIDs = SystemIDs,
                                                                                                    .ProductionAreaIDs = ProductionAreaIDs,
                                                                                                    .ContractTypeIDs = ContractTypeIDs,
                                                                                                    .FilterName = FilterName,
                                                                                                    .SelectedUserIDs = SelectedUserIDs
                                                                                                }
      Dim UserList As OBLib.Security.UserList = OBLib.Security.UserList.GetUserList(crit)
      'Once we have the filtered UserList, loop through each user and call the send password method
      For Each User In UserList
        User.ResetPassword()
      Next
    Catch ex As Exception
      Dim x = New Singular.Web.Result(False)
      x.ErrorText = ex.Message
      Return x
    End Try
    Return New Singular.Web.Result(True)
  End Function

End Class