﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="LiveEvents.aspx.vb" Inherits="NewOBWeb.LiveEvents" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Playout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    div.live-event-card {
      /*padding: 1.5rem;
      box-shadow: 0 1px 2px #aaa;
      background: white;
      margin: 0 1rem 1rem;
      border-radius: 3px;
      user-select: none;*/
      animation: fly-in-from-left .5s 0.35s ease both;
      /*transform-origin: top left;*/
      height: 600px;
      min-height: 600px;
      max-height: 600px;
      overflow-y: auto;
    }

      div.live-event-card img.face-image {
        /*display: inline-flex;*/
        width: 100%;
        width: 130px;
        height: 100px;
      }

    @keyframes fly-in-from-left {
      from {
        transform: translateY(15rem);
        opacity: 0;
      }
    }
    /*rotate(15deg)*/

    .loader {
      font-size: 10px;
      margin: 50px auto;
      text-indent: -9999em;
      width: 11em;
      height: 11em;
      border-radius: 50%;
      background: #0dc5c1;
      background: -moz-linear-gradient(left, #0dc5c1 10%, rgba(255, 255, 255, 0) 42%);
      background: -webkit-linear-gradient(left, #0dc5c1 10%, rgba(255, 255, 255, 0) 42%);
      background: -o-linear-gradient(left, #0dc5c1 10%, rgba(255, 255, 255, 0) 42%);
      background: -ms-linear-gradient(left, #0dc5c1 10%, rgba(255, 255, 255, 0) 42%);
      background: linear-gradient(to right, #0dc5c1 10%, rgba(255, 255, 255, 0) 42%);
      position: relative;
      -webkit-animation: load3 1.4s infinite linear;
      animation: load3 1.4s infinite linear;
      -webkit-transform: translateZ(0);
      -ms-transform: translateZ(0);
      transform: translateZ(0);
      top: calc(25vh);
    }

      .loader:before {
        width: 50%;
        height: 50%;
        background: #0dc5c1;
        border-radius: 100% 0 0 0;
        position: absolute;
        top: 0;
        left: 0;
        content: '';
      }

      .loader:after {
        background: #f0f0f0;
        width: 75%;
        height: 75%;
        border-radius: 50%;
        content: '';
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
      }

    @-webkit-keyframes load3 {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @keyframes load3 {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    #plotarea {
      max-width: 100%;
      height: 400px;
    }

    #pie {
      max-width: 100%;
      height: 400px;
    }

    #barmonth {
      max-width: 100%;
      height: 400px;
    }

    #baryear {
      max-width: 100%;
      height: 400px;
    }

    .block-flat-leave{
  margin-bottom: 40px;
  padding: 20px 20px;
  background: #FFFFCC;
  border-radius: 3px;
  position: relative;
  -webkit-border-radius: 3px;
  border-left: 1px solid #efefef;
  border-right: 1px solid #efefef;
  border-bottom: 1px solid #e2e2e2;
  box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
  transition: padding 500ms;
  -moz-transition: padding 500ms;
  -webkit-transition: padding 500ms;
}

    div.condensedView {
      height: 250px;
      min-height: 250px;
      max-height: 250px;
    }

      div.condensedView > div.live-event-card {
        height: unset;
        min-height: unset;
        max-height: unset;
      }

    .live-event-card > h4 {
      text-align: center;
    }

    .live-event-card > span {
      width: 100%;
      display: inline-block;
      padding: 6px;
    }

    .condensedActive {
      background-color: rgb(119, 97, 167) !important;
      color: white;
    }
  </style>
  <%--  <script type="text/javascript" src="../Scripts/flatDream/jquery.flot.min.js"></script>
  <script type="text/javascript" src="../Scripts/flatDream/jquery.flot.pie.min.js"></script>--%>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Playout.js"></script>
  <script type="text/javascript" src="../Scripts/Tools/liveEvents.js"></script>
  <script type="text/javascript">
    //test
    Singular.OnPageLoad(function () {

      var condensed = LiveEventHRCardCriteriaBO.getCookie("CondensedView");
      if (condensed == 'true') {
        ViewModel.Condensed(true);
      } else {
        ViewModel.Condensed(false);
      }

      window.liveEventsControl = new liveEvents("LiveEventsContainer", { condensedView: condensed == 'true'})

      LiveEventHRCardCriteriaBO.StartDateSet = function (criteriaObj) {
        window.liveEventsControl.criteria.startDate = new Date(criteriaObj.StartDate())
        window.liveEventsControl.refresh()
      }

      LiveEventHRCardCriteriaBO.EndDateSet = function (criteriaObj) {
        window.liveEventsControl.criteria.endDate = new Date(criteriaObj.EndDate())
        window.liveEventsControl.refresh()
      }

      LiveEventHRCardCriteriaBO.RoomIDSet = function (criteriaObj) {
        window.liveEventsControl.criteria.roomID = criteriaObj.RoomID()
        window.liveEventsControl.refresh()
      }

      LiveEventHRCardCriteriaBO.EventBasedIndSet = function (criteriaObj) {
        window.liveEventsControl.criteria.EventBasedInd = criteriaObj.EventBasedInd()
        window.liveEventsControl.refresh()
      }

      LiveEventHRCardCriteriaBO.ShiftBasedIndSet = function (criteriaObj) {
        window.liveEventsControl.criteria.ShiftBasedInd = criteriaObj.ShiftBasedInd()
        window.liveEventsControl.refresh()
      }

      LiveEventHRCardCriteriaBO.LeaveIndSet = function (criteriaObj) {
        window.liveEventsControl.criteria.LeaveInd = criteriaObj.LeaveInd()
        window.liveEventsControl.refresh()
      }

      ViewModel.LiveEventHRCardListCriteria().SystemIDs([])
      ViewModel.LiveEventHRCardListCriteria().ProductionAreaIDs([])

      ViewModel.UserSystemList().forEach(UserSystem => {
        UserSystem.IsSelected(true)
        ViewModel.LiveEventHRCardListCriteria().SystemIDs().push(UserSystem.SystemID())
          UserSystem.UserSystemAreaList().forEach(UserSystemArea => {
            UserSystemArea.IsSelected(true)
            ViewModel.LiveEventHRCardListCriteria().ProductionAreaIDs().push(UserSystem.UserSystemAreaList().ProductionAreaID())
          })
      })
      
    })

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      If OBLib.Security.Settings.CurrentUser.SystemID = 5 Then
        h.HTML.Heading1("Welcome to Playout Operations")
      Else
        h.HTML.Heading1("Live Event Personnel")
      End If

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Filters", False, False, , )
            With .ContentTag
              With .Helpers.With(Of OBLib.Playout.LiveEventHRCardList.Criteria)("ViewModel.LiveEventHRCardListCriteria()")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.StartDate)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.StartDate,
                                                             Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.EndDate)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.EndDate,
                                                             Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                    .Helpers.Bootstrap.LabelDisplay("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "window.liveEventsControl.addSystem($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "window.liveEventsControl.addProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                    .Helpers.Bootstrap.LabelDisplay("Included")
                    With .Helpers.Bootstrap.Row

                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 4)
                        With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.EventBasedInd, "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) "Events")
                          .Button.AddClass("buttontext dashboard-state-button")
                        End With
                      End With

                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 4)
                        With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.ShiftBasedInd, "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) "Shifts")
                          .Button.AddClass("buttontext dashboard-state-button")
                        End With
                      End With

                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 4)
                        With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.LeaveInd, "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) "Leave")
                          .Button.AddClass("buttontext dashboard-state-button")
                        End With
                      End With

                    End With

                  End With

                  With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.RoomID)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.RoomID,
                                                           Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                    .Helpers.Bootstrap.LabelDisplay("Last Refreshed")
                    With .Helpers.HTMLTag("h3")
                      .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Playout.LiveEventHRCardList.Criteria) d.LastRefreshTime)
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                    .Helpers.Bootstrap.LabelDisplay("Condensed view")
                    With .Helpers.DivC("btn-group")
                      .ID = "CondensedViewGroup"
                      With .Helpers.Button("On", ButtonMainStyle.Default, ButtonSize.Normal, FontAwesomeIcon.None)
                        .ClickJS = "LiveEventHRCardCriteriaBO.CondenseView()"
                        .AddClass("btn-group")
                        .AddBinding(KnockoutBindingString.css, Function(c) If(ViewModel.Condensed, "condensedActive", ""))
                        .AddBinding(KnockoutBindingString.enable, Function(c) Not ViewModel.Condensed)
                      End With
                      With .Helpers.Button("Off", ButtonMainStyle.Default, ButtonSize.Normal, FontAwesomeIcon.None)
                        .ClickJS = "LiveEventHRCardCriteriaBO.CondenseView()"
                        .AddClass("btn-group")
                        .AddBinding(KnockoutBindingString.css, Function(c) If(Not ViewModel.Condensed, "condensedActive", ""))
                        .AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.Condensed)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        'With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
        '  With .Helpers.DivC("block-flat dark-box visitors")
        '    With .Helpers.HTMLTag("h4")
        '      .AddClass("no-margin")
        '      .Helpers.HTML("Arrivals")
        '    End With
        '    With .Helpers.Bootstrap.Row
        '      With .Helpers.DivC("counters col-md-4")
        '        With .Helpers.HTMLTag("h1")
        '          .Helpers.HTML("477")
        '        End With
        '        With .Helpers.HTMLTag("h1")
        '          .Helpers.HTML("23")
        '        End With
        '      End With
        '      With .Helpers.DivC("col-md-8")
        '        With .Helpers.Div
        '          .Attributes("id") = "ticket-chart"
        '        End With
        '      End With
        '    End With
        '    With .Helpers.Bootstrap.Row
        '      .AddClass("footer")
        '      With .Helpers.Bootstrap.Column(6, 6, 6, 6, 6)
        '        With .Helpers.HTMLTag("p")
        '          .Helpers.Bootstrap.FontAwesomeIcon("fa-square")
        '          With .Helpers.Span
        '            .Helpers.HTML("Arrived")
        '          End With
        '        End With
        '      End With
        '      With .Helpers.Bootstrap.Column(6, 6, 6, 6, 6)
        '        With .Helpers.HTMLTag("p")
        '          With .Helpers.Bootstrap.FontAwesomeIcon("fa-square")
        '            .IconContainer.AddClass("return")
        '          End With
        '          With .Helpers.Span
        '            .Helpers.HTML("Not Arrived")
        '          End With
        '        End With
        '      End With
        '    End With
        '  End With
        'End With
      End With


      With h.Div
        .Attributes("id") = "LiveEventsContainer"
      End With

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
