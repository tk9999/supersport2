﻿Imports Singular.Misc
Imports OBLib.Invoicing.ReadOnly

Public Class DashboardRedirect
  Inherits OBPageBase(Of DashboardRedirectVM)

End Class

Public Class DashboardRedirectVM
  Inherits OBViewModel(Of DashboardRedirectVM)

  Protected Overrides Sub Setup()
    MyBase.PreSetup()

    Select Case OBLib.Security.Settings.CurrentUser.UserTypeID
      'Web Users (Freelancers for timesheets)
      Case OBLib.CommonData.Enums.UserType.Web
        If CompareSafe(OBLib.Security.Settings.CurrentUser.ContractTypeID, CType(OBLib.CommonData.Enums.ContractType.Freeleancer, Integer)) Then
          Dim PaymentRun As ROPaymentRun = OBLib.Helpers.TimesheetsHelper.GetCurrentPaymentRun(OBLib.Security.Settings.CurrentUser.SystemID)
          If PaymentRun IsNot Nothing Then
            HttpContext.Current.Response.Redirect("~/Timesheets/FreelancerTimesheet.aspx?" & "P=" & PaymentRun.PaymentRunID.ToString)
          End If
        Else
          Page.Response.Redirect("~/Default.aspx")
        End If

        'Event Managers (Outside Broadcast)
      Case OBLib.CommonData.Enums.UserType.EventManager,
           OBLib.CommonData.Enums.UserType.EventAdministrator,
           OBLib.CommonData.Enums.UserType.LineManagerDheshnie,
           OBLib.CommonData.Enums.UserType.EventManagerIntern
        HttpContext.Current.Response.Redirect("~/Dashboards/EventManagerDashboard.aspx")

        'Production Managers
      Case OBLib.CommonData.Enums.UserType.ProductionManager
        HttpContext.Current.Response.Redirect("~/Dashboards/ProductionManagerDashboard.aspx")

        'Super Users
      Case OBLib.CommonData.Enums.UserType.SuperUser
        'Chevani
        If OBLib.Security.Settings.CurrentUserID = 152 Then
          HttpContext.Current.Response.Redirect("~/Dashboards/LMDashboard.aspx")
          'Developers
        ElseIf OBLib.Security.Settings.CurrentUserID = 1 Then
          HttpContext.Current.Response.Redirect("~/Dashboards/DevDashboard.aspx")
        Else
          'Other (Vashni, Johan Chandler)
          HttpContext.Current.Response.Redirect("~/Default.aspx")
        End If



      Case OBLib.CommonData.Enums.UserType.ResourceScheduler
        If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer) _
          AndAlso OBLib.Security.Settings.CurrentUser.ProductionAreaID = CType(OBLib.CommonData.Enums.ProductionArea.Studio, Integer) Then
          HttpContext.Current.Response.Redirect("~/Dashboards/PSRSDashboard.aspx")
        ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) _
               AndAlso OBLib.Security.Settings.CurrentUser.ProductionAreaID = CType(OBLib.CommonData.Enums.ProductionArea.Studio, Integer) Then
          HttpContext.Current.Response.Redirect("~/Dashboards/ResourceSchedulerPCDashboard.aspx")
        End If

      Case OBLib.CommonData.Enums.UserType.UnitSupervisor
        If Singular.Security.HasAccess("Dashboards", "Can View Manager of Additional Facilities Dashboard") Then
          HttpContext.Current.Response.Redirect("~/Dashboards/MOAFDashboard.aspx")
        Else
          HttpContext.Current.Response.Redirect("~/Default.aspx")
        End If

      Case Else
        HttpContext.Current.Response.Redirect("~/Default.aspx")

    End Select

  End Sub

End Class