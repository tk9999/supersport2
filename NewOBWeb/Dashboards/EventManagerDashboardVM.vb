﻿Imports OBLib.Productions
Imports Singular.DataAnnotations
Imports System.ComponentModel
Imports OBLib.HR.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports OBLib.Productions.Old
Imports System

Public Class EventManagerDashboardVM
  Inherits OBViewModel(Of EventManagerDashboardVM)

#Region " Properties "

  Private mROEventManagerProductionList As OBLib.Dashboards.ReadOnly.ROEventManagerProductionList
  Public ReadOnly Property ROEventManagerProductionList As OBLib.Dashboards.ReadOnly.ROEventManagerProductionList
    Get
      Return mROEventManagerProductionList
    End Get
  End Property

  <ClientOnly()>
  Public Property ProductionFilter As String = ""
  Public Property StartDate As DateTime?
  Public Property EndDate As DateTime?

  Public Shared EventManagerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventManagerID, "Event Manager", Nothing)
  ''' <summary>
  ''' Gets and sets the Production Venue value
  ''' </summary>
  <Display(Name:="Event Manager", Description:=""),
  Singular.DataAnnotations.DropDownWeb(GetType(ROEventManagerList), DisplayMember:="PreferredFirstSurname", ValueMember:="HumanResourceID", DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
  Public Property EventManagerID() As Integer?
    Get
      Return GetProperty(EventManagerIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(EventManagerIDProperty, Value)
    End Set
  End Property

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    MyBase.Setup()

    ValidationMode = Singular.Web.ValidationMode.OnLoad
    StartDate = Singular.Dates.DateMonthStart(Now).AddDays(-7)
    EndDate = Singular.Dates.DateMonthEnd(Now.AddMonths(1))
    EventManagerID = OBLib.Security.Settings.CurrentUser.HumanResourceID
    mROEventManagerProductionList = OBLib.Dashboards.ReadOnly.ROEventManagerProductionList.GetROEventManagerProductionList(OBLib.Security.Settings.CurrentUser.HumanResourceID, StartDate, EndDate)

    ClientDataProvider.AddDataSource("ROEventManagerList", OBLib.CommonData.Lists.ROEventManagerList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)

    Select Case Command

      Case "EditProduction"
        If CommandArgs.ClientArgs.ProductionID IsNot Nothing Then
          If IsNumeric(CommandArgs.ClientArgs.ProductionID) Then
            Dim ProductionID As Integer = CommandArgs.ClientArgs.ProductionID
            'OBLib.Security.Settings.CurrentUser.ProductionAreaID
            Dim pl As OBLib.Productions.Old.ProductionList = OBLib.Productions.Old.ProductionList.GetProductionList(ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, 1) '1 = OB
            If pl.Count = 1 Then
              SessionData.CurrentProduction = pl(0)
            End If
            CommandArgs.ReturnData = VirtualPathUtility.ToAbsolute("~/Productions/Production.aspx")
          End If
        End If

      Case "FindProductions"
        mROEventManagerProductionList = OBLib.Dashboards.ReadOnly.ROEventManagerProductionList.GetROEventManagerProductionList(EventManagerID, StartDate, EndDate)

    End Select

  End Sub

#End Region

End Class
