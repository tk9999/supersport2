﻿Imports OBLib.Playout

Public Class LiveEvents
  Inherits OBPageBase(Of LiveEventsVM)

End Class

Public Class LiveEventsVM
  Inherits OBViewModelStateless(Of LiveEventsVM)

  Public Property LiveEventHRCardListCriteria As LiveEventHRCardList.Criteria = New LiveEventHRCardList.Criteria
  Public Property UserSystemList As OBLib.Security.UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

  Protected Overrides Sub Setup()
    MyBase.Setup()
    LiveEventHRCardListCriteria.StartDate = Now
    LiveEventHRCardListCriteria.EndDate = Now
  End Sub

End Class