﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="MOAFDashboard.aspx.vb" Inherits="NewOBWeb.MOAFDashboard" %>

<%@ Import Namespace="OBLib.Dashboards.ReadOnly" %>

<%@ Import Namespace="Singular.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <style type="text/css">
    body {
      background-color: #F0F0F0;
    }
  </style>
  <script type="text/javascript">

    Singular.OnPageLoad(function () {
      ViewModel.PositionBookingsCriteria().PositionIDs('<DataSet><Table ID="153" /><Table ID="154" /></DataSet>')
    });

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <% Using h = Helpers
      
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("SteadyCam Bookings", True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .AboveContentTag
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.PositionBookingsCriteria.TxDateFrom, BootstrapEnums.InputSize.Small)
                    
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.PositionBookingsCriteria.TxDateTo, BootstrapEnums.InputSize.Small)
                    
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", , , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, "ViewModel.PositionBookingsPagingManager().Refresh()")
                    .Button.AddClass("btn-block")
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROPositionBooking)(Function(vm) ViewModel.PositionBookingsPagingManager, Function(vm) ViewModel.PositionBookings, False, False,
                                                                                       False, False, True, True, False, "PositionBookings", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.Position)
                      .AddClass("bold col-sm-2 col-md-2 col-2")
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.TxStartDateTime)
                      .AddClass("bold col-sm-2 col-md-2 col-2")
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.TxEndDateTime)
                      .AddClass("bold col-sm-2 col-md-2 col-2")
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.ProductionRefNo)
                      .AddClass("bold col-sm-2 col-md-1 col-1")
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.ProductionDescription)
                      .AddClass("bold col-sm-3 col-md-3 col-3")
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.EventManager)
                      .AddClass("bold col-sm-2 col-md-2 col-2")
                    End With
                    '.AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROPositionBooking) rop.TeamsPlaying)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
    End Using%>
</asp:Content>
