﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
  CodeBehind="ProductionManagerDashboard.aspx.vb" Inherits="NewOBWeb.Production_ManagerDashboard" %>

<%@ Import Namespace="OBLib.HR.ReadOnly" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <link href="../Productions/Dashboard.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    div.ROProductionList td, div.ROProductionList th
    {
      font-size: 11px;
    }

    div.scroll-content
    {
      overflow-y: scroll;
      height: 500px;
    }

    div.ROProductionList th
    {
      background-color: #5e5e5e;
      color: White;
    }

    .modal-xl
    {
      width: 90%;
    }

    .modal-md
    {
      width: 70%;
    }

    .modal-sm
    {
      width: 40%;
    }

    .FindList
    {
      overflow-y: scroll !important;
      max-height: 250px !important;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    Using h = Helpers

      '-------------------------------------------Heading-------------------------------------------
      '-------------------------------------------Toolbar-------------------------------------------
      With h.DivC("row")
        With .Helpers.BootstrapDivColumn(12, 12, 12)
          With .Helpers.BootstrapDivColumn(12, 12, 6)
            .Helpers.BootstrapPageHeader(2, "Dashboard", "")
          End With
          With .Helpers.BootstrapDivColumn(12, 12, 6)
            With .Helpers.Toolbar
              .Helpers.MessageHolder()
            End With
          End With
        End With
      End With
      
      With h.DivC("row")
        If OBLib.Security.Settings.CurrentUserID = 152 Then
          With .Helpers.BootstrapDivColumn(12, 3, 1)
            .Style.Padding(, "10px", , )
            With .Helpers.BootstrapButton(, "New Production", "btn-sm btn-success", "glyphicon-plus", Singular.Web.PostBackType.Ajax, True, , , "CreateProduction()")
            End With
          End With
          
          With .Helpers.BootstrapDivColumn(12, 3, 1)
            .Style.Padding(, "10px", , )
            'With .Helpers.Bootstrap.Button(, "Daily Schedule Report", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-table", , Singular.Web.PostBackType.Ajax, "OpenDailyScheduleReport()")              
            'End With
            With .Helpers.BootstrapButton(, "Daily Schedule Report", "btn-sm btn-warning", "glyphicon-list", Singular.Web.PostBackType.Ajax, True, , , "OpenDailyScheduleReport()")
              
            End With
          End With
          
        End If
      End With
      
      '-------------------------------------------Criteria-------------------------------------------
      With h.DivC("row criteria well")
        With .Helpers.BootstrapDivColumn(12, 12, 12)
          With .Helpers.BootstrapDivColumn(12, 12, 2)
            .AddClass("top-buffer-15")
            With .Helpers.DivC("field-box")
              .Helpers.LabelFor(Function(p) ViewModel.StartDate)
              With .Helpers.EditorFor(Function(p) ViewModel.StartDate)
                .AddClass("form-control")
              End With
            End With
          End With
          With .Helpers.BootstrapDivColumn(12, 12, 2)
            .AddClass("top-buffer-15")
            With .Helpers.DivC("field-box")
              .Helpers.LabelFor(Function(p) ViewModel.EndDate)
              With .Helpers.EditorFor(Function(p) ViewModel.EndDate)
                .AddClass("form-control")
              End With
            End With
          End With
        End With
        With .Helpers.BootstrapDivColumn(12, 12, 12)
          .AddClass("top-buffer-15")
          .Style.MarginLeft("15px")
          .Helpers.BootstrapButton("FindProductions", "Find", "btn-md btn-primary", "glyphicon-search", Singular.Web.PostBackType.Ajax, False, , , )
        End With
      End With
      
      '-------------------------------------------Production List-------------------------------------------
      With h.DivC("row ROProductionList")
        
        With .Helpers.BootstrapDivColumn(12, 12, 12)
          With .Helpers.BootstrapPanel
            With .PanelHeading
              .Helpers.HTML("Productions")
            End With
            With .PanelBody
              With .Helpers.DivC("field-box")
                With .Helpers.EditorFor(Function(p) ViewModel.ProductionFilter)
                  .Attributes("placeholder") = "Filter..."
                  .AddClass("form-control filter-field")
                  .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterProductions}")
                End With
              End With
              .Helpers.DivC("top-buffer-10")
              With .Helpers.DivC("scroll-content")
                With .Helpers.BootstrapTableFor(Of OBLib.Dashboards.ReadOnly.ROProductionManagerProduction)(Function(vm) ViewModel.ROProductionManagerProductionList, False, False, "$data.Visible()")
                  With .FirstRow
                    .AddBinding(Singular.Web.KnockoutBindingString.css, "GetStateCssClass($data)")
                    With .AddColumn("")
                      .Helpers.BootstrapButton(, "Edit", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.Ajax, , , , "EditSelectedProduction($data)")
                    End With
                    With .AddColumn("Status")
                      .Style.Width = "30px"
                      .Helpers.BootstrapStateButton("", "CurrentStateClick($data)", "CurrentStateCss($data)", "CurrentStateHtml($data)", False)
                    End With
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.TxStartDateTime)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.TxEndDateTime)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProductionRefNo).AddClass("bold")
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProductionDescription).AddClass("bold")
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.TeamsPlaying)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.EventManager)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProductionManager)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.Languages)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.HRCount)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.OBVans)
                    With .AddColumn("Vision View")
                      .Helpers.BootstrapStateButton("", "", "VisionViewCss($data)", "VisionViewHtml($data)", False)
                    End With
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProducerNames)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.DirectorNames)
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.City)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      With h.DivC("Modal") 
        With .Helpers.BootstrapDialog("DailyScheduleReportCriteria", "Daily Schedule Report")
          .ModalDialogDiv.AddClass("modal-xl")
          With .Body
            .AddClass("row")
            With .Helpers.BootstrapDivColumn(12, 12, 12)
              With .Helpers.BootstrapDivColumn(12, 12, 2)
                .AddClass("top-buffer-15")
                With .Helpers.DivC("field-box")
                  .Helpers.LabelFor(Function(p) ViewModel.ReportStartDate)
                  With .Helpers.EditorFor(Function(p) ViewModel.ReportStartDate)
                    .AddClass("form-control")
                  End With
                End With
              End With
              With .Helpers.BootstrapDivColumn(12, 12, 2)
                .AddClass("top-buffer-15")
                With .Helpers.DivC("field-box")
                  .Helpers.LabelFor(Function(p) ViewModel.ReportEndDate)
                  With .Helpers.EditorFor(Function(p) ViewModel.ReportEndDate)
                    .AddClass("form-control")
                  End With
                End With
              End With
            End With
            
            With .Helpers.BootstrapDivColumn(12, 12, 12)
              .AddClass("top-buffer-15")
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Find Productions")
                End With
                With .PanelBody
                  With .Helpers.DivC("field-box")
                    With .Helpers.EditorFor(Function(p) ViewModel.ROProductionFilter)
                      .Attributes("placeholder") = "Filter..."
                      .AddClass("form-control filter-field")
                      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROProduction}")
                    End With
                  End With
                  With .Helpers.BootstrapDivColumn(12, 7, 7)
                    With .Helpers.DivC("FindList")
                      With .Helpers.BootstrapTableFor(Of ROProduction)(Function(d) d.ROProductionList, False, False, "", False)
                        .RemoveClass("Grid")
                        .AddClass("table table-striped table-bordered table-hover table-condensed ROProductionList")
                        With .FirstRow.AddColumn("")
                          .Style.Width = 50
                          .Helpers.BootstrapButton(, "Select", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.None, , , , "AddProductionToSelectedList($data)")
                        End With
                        .FirstRow.AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                        .FirstRow.AddReadOnlyColumn(Function(c) c.ProductionDescription, "Description")
                        .FirstRow.AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                        .FirstRow.AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                      End With
                    End With

                  End With
                  
                  With .Helpers.BootstrapDivColumn(12, 5, 5)
                    With .Helpers.DivC("FindList")
                      With .Helpers.BootstrapTableFor(Of OBLib.Helpers.SelectedROProduction)(Function(d) d.SelectedROProductionList, False, False, "", False)
                        .RemoveClass("Grid")
                        .AddClass("table table-striped table-bordered table-hover table-condensed SelectedROProductionList")
                        With .FirstRow.AddColumn("")
                          .Style.Width = 50
                          .Helpers.BootstrapButton(, "Remove", "btn-xs btn-danger", "glyphicon-pencil", Singular.Web.PostBackType.None, , , , "RemoveProductionFromSelectedList($data)")
                        End With
                        .FirstRow.AddReadOnlyColumn(Function(c As OBLib.Helpers.SelectedROProduction) c.ProductionRefNo)
                        .FirstRow.AddReadOnlyColumn(Function(c) c.ProductionDescription, "Description")
                        .FirstRow.AddReadOnlyColumn(Function(c As OBLib.Helpers.SelectedROProduction) c.PlayStartDateTime)
                        .FirstRow.AddReadOnlyColumn(Function(c As OBLib.Helpers.SelectedROProduction) c.PlayEndDateTime)
                      End With
                    End With

                  End With

                End With
                
              End With
              
            End With
            
            With .Helpers.BootstrapDivColumn(12, 12, 12)
              .AddClass("top-buffer-15")
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Find Human Resources")
                End With
                With .PanelBody
                  With .Helpers.DivC("field-box")
                    With .Helpers.EditorFor(Function(p) ViewModel.ROHumanResourceFilter)
                      .Attributes("placeholder") = "Filter..."
                      .AddClass("form-control filter-field")
                      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROHumanResource}")
                    End With
                  End With
                  
                  With .Helpers.BootstrapDivColumn(12, 6, 6)
                    With .Helpers.DivC("FindList")
                      With .Helpers.BootstrapTableFor(Of ROHumanResource)(Function(d) d.ROHumanResourceList, False, False, "", False)
                        .RemoveClass("Grid")
                        .AddClass("table table-striped table-bordered table-hover table-condensed ROHumanResourceList")
                        With .FirstRow.AddColumn("")
                          .Style.Width = 50
                          .Helpers.BootstrapButton(, "Select", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.None, , , , "AddHRToSelectedList($data)")
                        End With
                        With .FirstRow
                          .AddReadOnlyColumn(Function(hr As ROHumanResource) hr.PreferredFirstSurname)
                        End With
                      End With
                    End With
                  End With
                  
                  With .Helpers.BootstrapDivColumn(12, 6, 6)
                    With .Helpers.DivC("FindList")
                      With .Helpers.BootstrapTableFor(Of OBLib.Helpers.SelectedROHumanResource)(Function(d) d.SelectedROHumanResourceList, False, False, "", False)
                        .RemoveClass("Grid")
                        .AddClass("table table-striped table-bordered table-hover table-condensed SelectedROHumanResourceList")
                        With .FirstRow.AddColumn("")
                          .Style.Width = 50
                          .Helpers.BootstrapButton(, "Remove", "btn-xs btn-danger", "glyphicon-pencil", Singular.Web.PostBackType.None, , , , "RemoveHRFromSelectedList($data)")
                        End With
                        With .FirstRow
                          .AddReadOnlyColumn(Function(hr As OBLib.Helpers.SelectedROHumanResource) hr.PreferredFirstSurname)
                        End With
                      End With
                    End With
                  End With
                  
                End With
              End With
              
              
            End With
            
          End With
          
          With .Footer
            'With .Helpers.BootstrapButton(, "Save", "btn-sm btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.None, False, , , "PrintDailyScheduleReport()")
              
            'End With
            With .Helpers.BootstrapButton("PrintDailyScheduleReport", "Print Report", "btn-sm btn-success", "glyphicon-print", Singular.Web.PostBackType.Full, False, , , ) '"PrintDailyScheduleReport()")
              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "DailyScheduleReportValid()")
            End With
          End With
          
        End With
        
      End With
    
    End Using
  %>
  <script type="text/javascript">
    function CurrentStateClick(ROEventManagerProduction) {
    }
    function CurrentStateCss(ROEventManagerProduction) {
      if (ROEventManagerProduction.Cancelled()) {
        return 'btn btn-xs btn-danger';
      } else if (ROEventManagerProduction.Reconciled()) {
        return 'btn btn-xs btn-warning';
      } else if (ROEventManagerProduction.PlanningFinalised()) {
        return 'btn btn-xs btn-success';
      } else if (ROEventManagerProduction.CrewFinalised()) {
        return 'btn btn-xs btn-info';
      } else {
        return 'btn btn-xs btn-default';
      }
    }
    function CurrentStateHtml(ROEventManagerProduction) {
      if (ROEventManagerProduction.Cancelled()) {
        return '<span class="glyphicon glyphicon-remove"></span> Cancelled';
      } else if (ROEventManagerProduction.Reconciled()) {
        return '<span class="glyphicon glyphicon-ok"></span> Reconciled';
      } else if (ROEventManagerProduction.PlanningFinalised()) {
        return '<span class="glyphicon glyphicon-thumbs-up"></span> Planning';
      } else if (ROEventManagerProduction.CrewFinalised()) {
        return '<span class="glyphicon glyphicon-user"></span> Crew';
      } else {
        return '<span class="glyphicon glyphicon-asterisk"></span> New';
      }
    }
    function GetStateCssClass(ROEventManagerProduction) {
      if (ROEventManagerProduction.Cancelled()) {
        return 'Cancelled';
      } else if (ROEventManagerProduction.Reconciled()) {
        return 'Reconciled';
      } else if (ROEventManagerProduction.PlanningFinalised()) {
        return 'PlanningFinalised';
      } else if (ROEventManagerProduction.CrewFinalised()) {
        return 'CrewFinalised';
      }
    };
    function EditSelectedProduction(ROPRoduction) {
      Singular.SendCommand('EditProduction', { ProductionID: ROPRoduction.ProductionID() }, function (d) {
        window.location.href = d;
      });
    };
    function FilterProductions() {
      Singular.ShowLoadingBar();
      if (!ViewModel.ProductionFilter() || ViewModel.ProductionFilter() == '') {
        ViewModel.ROProductionManagerProductionList().Iterate(function (Production, Index) {
          Production.Visible(true);
        });
      } else {
        ViewModel.ROProductionManagerProductionList().Iterate(function (Production, Index) {
          var FilterString = ViewModel.ProductionFilter().toLowerCase();
          var DescMatchInd = (Production.ProductionDescription().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          var RefNoMatchInd = (Production.ProductionRefNo().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          //var PTypeMatchInd = (Production.ProductionType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          //var ETypeMatchInd = (Production.EventType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          //PTypeMatchInd || ETypeMatchInd || 
          var TeamsMatchInd = (Production.TeamsPlaying().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          if (DescMatchInd || RefNoMatchInd || TeamsMatchInd) {
            Production.Visible(true);
          } else {
            Production.Visible(false);
          }
        });
      };
      Singular.HideLoadingBar();
    };
    function VisionViewCss(ROEventManagerProduction) {
      if (ROEventManagerProduction.VisionView()) {
        return 'btn btn-xs btn-primary';
      } else {
        return 'btn btn-xs btn-default';
      }
    }
    function VisionViewHtml(ROEventManagerProduction) {
      if (ROEventManagerProduction.VisionView()) {
        return '<span class="glyphicon glyphicon-check"></span> Yes';
      } else {
        return '<span class="glyphicon glyphicon-minus"></span> No';
      }
    }
    function CreateProduction() {
      window.location.href = "../Productions/Production.aspx?New=1"
    }

    function OpenDailyScheduleReport() {
      $('#DailyScheduleReportCriteria').modal();
      ViewModel.ReportPopupInd(true);
    };

    function FilterROProduction() {
      if (ViewModel.ReportStartDate() || ViewModel.ReportEndDate()) {
        if (ViewModel.ROProductionFilter().length >= 3) {
          Singular.ShowLoadingBar();
          Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionList, OBLib', {
            FilterName: ViewModel.ROProductionFilter(),
            TxDateFrom: ViewModel.ReportStartDate(),
            TxDateTo: ViewModel.ReportEndDate()
          }, function (args) {
            if (args.Success) {
              KOFormatter.Deserialise(args.Data, ViewModel.ROProductionList);
              Singular.HideLoadingBar();
            }
          });
        }
        else if (ViewModel.ROProductionFilter().length == 0) {
          ViewModel.ROProductionList.Set([]);
        }
      }
      else {
        //alert('Please provide a Start and End Date');
        //Singular.ShowMessage("Error", "Please provide a Start and End Date", 2, function () {
        //  return false;
        //});
      }
    };

    function FilterROHumanResource() {
      if (ViewModel.ROHumanResourceFilter().length >= 3) {
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceList, OBLib', {
          FilterName: ViewModel.ROHumanResourceFilter(),
          SystemID: ViewModel.CurrentSystemID(),
          ProductionAreaID: ViewModel.CurrentProductionAreaID()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.ROHumanResourceList);
            Singular.HideLoadingBar();
          }
        });
      }
      else if (ViewModel.ROHumanResourceFilter().length == 0) {
        ViewModel.ROHumanResourceList.Set([]);
      }
    };

    function AddProductionToSelectedList(ROProduction) {
      
      var newSelectProd = ViewModel.SelectedROProductionList.AddNew();
      newSelectProd.ProductionID(ROProduction.ProductionID());
      newSelectProd.ProductionRefNo(ROProduction.ProductionRefNo());
      newSelectProd.ProductionDescription(ROProduction.ProductionDescription());
      newSelectProd.PlayStartDateTime(ROProduction.PlayStartDateTime());
      newSelectProd.PlayEndDateTime(ROProduction.PlayEndDateTime());

      ViewModel.ROProductionList.Remove(ROProduction)
    };

    function RemoveProductionFromSelectedList(SelectROProduction) {
      var newROProd = SelectROProduction;
      ViewModel.ROProductionList.push(newROProd);
      ViewModel.SelectedROProductionList.Remove(SelectROProduction)
    };

    function AddHRToSelectedList(ROHumanResource) {      
      var newSelectHR = ViewModel.SelectedROHumanResourceList.AddNew();
      newSelectHR.HumanResourceID(ROHumanResource.HumanResourceID());
      newSelectHR.PreferredFirstSurname(ROHumanResource.PreferredFirstSurname());
      ViewModel.ROHumanResourceList.Remove(ROHumanResource)
    };

    function RemoveHRFromSelectedList(SelectROHR) {
      var newROHR = SelectROHR;
      ViewModel.ROHumanResourceList.push(newROHR);
      ViewModel.SelectedROHumanResourceList.Remove(SelectROHR)
    };

    function ReportStartDateValid(Value, Rule, RuleArgs) {
      if (!Value && RuleArgs.Object.ReportPopupInd()) {
        RuleArgs.AddError("Start Date is required");
        return;
      }
    };

    function ReportEndDateValid(Value, Rule, RuleArgs) {
      if (!Value && RuleArgs.Object.ReportPopupInd()) {
        RuleArgs.AddError("End Date is required");
        return;
      }
    };

    function DailyScheduleReportValid() {
      if (ViewModel.ReportStartDate() && ViewModel.ReportEndDate() && ViewModel.ReportPopupInd()) {
        return true;
      }
      else {
        return false;
      }
    };

    //function PrintDailyScheduleReport(ROPRoduction) {
    //  Singular.SendCommand('PrintDailyScheduleReport', {}, function (d) { });
    //};

    $('#DailyScheduleReportCriteria').on('hide.bs.modal', function (e) {
      ViewModel.ROProductionList.Set([]);
      ViewModel.SelectedROProductionList.Set([]);
      ViewModel.ROProductionFilter('');
      ViewModel.ROHumanResourceList.Set([]);
      ViewModel.SelectedROHumanResourceList.Set([]);
      ViewModel.ROHumanResourceFilter('');
      ViewModel.ReportStartDate(null);
      ViewModel.ReportEndDate(null);
      ViewModel.ReportPopupInd(false);
    })

  </script>
</asp:Content>
