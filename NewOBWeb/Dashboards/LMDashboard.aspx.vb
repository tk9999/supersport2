﻿Imports OBLib.Productions
Imports Singular.DataAnnotations
Imports System.ComponentModel
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Areas.ReadOnly.ROProductionDisciplineTimelineType
Imports OBLib.Productions.ContentOB

Public Class LMDashboard
  Inherits OBPageBase(Of LMDashboardVM)

End Class

Public Class LMDashboardVM
  Inherits OBViewModel(Of LMDashboardVM)

#Region " Properties "

  Enum ProductionSupplierInfo
    OB = 32
    Graphics = 27
    VisionView = 95
  End Enum

  Public Property CurrentBudgetedProduction As OBLib.Productions.Budgeting.ProductionBudgeted
  Public Property CurrentProductionList As OBLib.Productions.ReadOnly.ROProductionList
  Public Property CurrentProductionList2 As List(Of ProductionDetails)
  Public Property CurrentGraphicsSupplierList As OBLib.Productions.ContentOB.Readonly.ROGraphicsSupplierList
  Public Property AddSupplierList As List(Of Integer)
  Public Property AddSupplierListStrings As List(Of String)

  Public Property StartDate As DateTime?
  Public Property EndDate As DateTime?
  Public Property CurrentSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID
  Public Property CurrentProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

  <InitialDataOnly>
  Public Property ROProductionList As OBLib.Productions.ReadOnly.ROProductionList
  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  <InitialDataOnly>
  Public Property RODebtorList As OBLib.Quoting.ReadOnly.RODebtorList
  Public Property RODebtorListCriteria As OBLib.Quoting.ReadOnly.RODebtorList.Criteria
  Public Property RODebtorListManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  <InitialDataOnly>
  Public Property ProductionsBudgeted As Budgeting.ReadOnly.ROProductionBudgetedList
  Public Property ProductionsBudgetedCriteria As Budgeting.ReadOnly.ROProductionBudgetedList.Criteria
  Public Property ProductionsBudgetedPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  <InitialDataOnly>
  Public Property Productions As OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList
  Public Property ProductionsCriteria As OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList.Criteria
  Public Property ProductionsPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  <InitialDataOnly>
  Public Property MostBookedPersonnel As OBLib.HR.ReadOnly.ROMostBookedPersonnelList
  Public Property MostBookedPersonnelCriteria As OBLib.HR.ReadOnly.ROMostBookedPersonnelList.Criteria
  Public Property MostBookedPersonnelPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  Public Property EventsImportedSinceLastLoginIsLoading As Boolean = False
  Public Property EventsImportedSinceLastLoginCount As Integer = 0
  '<InitialDataOnly>
  'Public Property EventsImportedSinceLastLogin As OBLib.Synergy.ReadOnly.ROImportedEventList
  'Public Property EventsImportedSinceLastLoginCriteria As OBLib.Synergy.ReadOnly.ROImportedEventList.Criteria
  'Public Property EventsImportedSinceLastLoginPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  Public Property EventsImportedSinceLastLoginSAIsLoading As Boolean = False
  Public Property EventsImportedSinceLastLoginSACount As Integer = 0
  '<InitialDataOnly>
  'Public Property EventsImportedSinceLastLoginSA As OBLib.Synergy.ReadOnly.ROImportedEventList
  'Public Property EventsImportedSinceLastLoginSACriteria As OBLib.Synergy.ReadOnly.ROImportedEventList.Criteria
  'Public Property EventsImportedSinceLastLoginSAPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  Public Property ProductionsToBeReconciledIsLoading As Boolean = False
  Public Property ProductionsToBeReconciledCount As Integer = 0
  <InitialDataOnly>
  Public Property ProductionsToBeReconciled As OBLib.Productions.ReadOnly.ROProductionList
  Public Property ProductionsToBeReconciledCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ProductionsToBeReconciledPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

  'Public Property ProductionUpdatesSinceLastLoginIsLoading As Boolean = False
  'Public Property ProductionUpdatesSinceLastLoginCount As Integer = 0
  '<InitialDataOnly>
  'Public Property ProductionUpdatesSinceLastLogin As OBLib.Synergy.ReadOnly.ROProductionSynergyChangeList
  'Public Property ProductionUpdatesSinceLastLoginCriteria As OBLib.Synergy.ReadOnly.ROProductionSynergyChangeList.Criteria
  'Public Property ProductionUpdatesSinceLastLoginPagingManager As Singular.Web.Data.PagedDataManager(Of LMDashboardVM)

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    MyBase.Setup()

    CurrentBudgetedProduction = Nothing
    CurrentProductionList2 = New List(Of ProductionDetails)
    'CurrentProductionList2 = New OBLib.Productions.ReadOnly.ROProductionList
    CurrentProductionList = New OBLib.Productions.ReadOnly.ROProductionList
    CurrentGraphicsSupplierList = OBLib.Productions.ContentOB.Readonly.ROGraphicsSupplierList.GetROGraphicsSupplierList
    AddSupplierList = New List(Of Integer)
    AddSupplierListStrings = New List(Of String)

    If Not Singular.Security.HasAccess("Dashboards", "Can View Logistics Manager Dashboard") Then
      RedirectToNoAccess()
    End If

    ValidationMode = Singular.Web.ValidationMode.OnLoad
    StartDate = Singular.Dates.DateMonthStart(Now).AddDays(-7)
    EndDate = Singular.Dates.DateMonthEnd(Now.AddDays(14))

    'Productions for Graphics and Vision View
    ROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
    ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
    ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "PlayStartDateTime", 10)
    ROProductionListCriteria.TxDateFrom = Singular.Dates.DateMonthStart(Now)
    ROProductionListCriteria.TxDateTo = Singular.Dates.DateMonthEnd(Now)
    ROProductionListCriteria.PageNo = 1
    ROProductionListCriteria.PageSize = 10
    ROProductionListCriteria.SortAsc = True
    ROProductionListCriteria.SortColumn = "PlayStartDateTime"

    'Debtors for Productions Budgeted 
    RODebtorList = New OBLib.Quoting.ReadOnly.RODebtorList
    RODebtorListCriteria = New OBLib.Quoting.ReadOnly.RODebtorList.Criteria
    RODebtorListManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.RODebtorList, Function(d) Me.RODebtorListCriteria, "Debtor", 10)
    RODebtorListCriteria.PageNo = 1
    RODebtorListCriteria.PageSize = 10
    RODebtorListCriteria.SortAsc = True
    RODebtorListCriteria.SortColumn = "Debtor"

    'Productions Budgeted
    ProductionsBudgeted = New Budgeting.ReadOnly.ROProductionBudgetedList
    ProductionsBudgetedCriteria = New Budgeting.ReadOnly.ROProductionBudgetedList.Criteria
    ProductionsBudgetedPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.ProductionsBudgeted, Function(d) Me.ProductionsBudgetedCriteria, "PlayStartDateTime", 10)
    ProductionsBudgetedCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
    ProductionsBudgetedCriteria.EndDate = Singular.Dates.DateMonthEnd(Now)
    ProductionsBudgetedCriteria.PageNo = 1
    ProductionsBudgetedCriteria.PageSize = 10
    ProductionsBudgetedCriteria.SortAsc = True
    ProductionsBudgetedCriteria.SortColumn = "PlayStartDateTime"

    'Todays Bookings
    Productions = New OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList
    ProductionsCriteria = New OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList.Criteria
    ProductionsPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.Productions, Function(d) Me.ProductionsCriteria, "CallTimeStart", 10)
    ProductionsCriteria.StartDate = Now
    ProductionsCriteria.EndDate = Now
    ProductionsCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ProductionsCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    ProductionsCriteria.PageNo = 1
    ProductionsCriteria.PageSize = 10
    ProductionsCriteria.SortAsc = True
    ProductionsCriteria.SortColumn = "CallTimeStart"

    'Most Booked Personnel
    MostBookedPersonnel = New OBLib.HR.ReadOnly.ROMostBookedPersonnelList
    MostBookedPersonnelCriteria = New OBLib.HR.ReadOnly.ROMostBookedPersonnelList.Criteria
    MostBookedPersonnelPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.MostBookedPersonnel, Function(d) Me.MostBookedPersonnelCriteria, "HoursBooked", 10)
    MostBookedPersonnelCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
    MostBookedPersonnelCriteria.EndDate = Singular.Dates.DateMonthEnd(Now)
    MostBookedPersonnelCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    MostBookedPersonnelCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    MostBookedPersonnelCriteria.PageNo = 1
    MostBookedPersonnelCriteria.PageSize = 10
    MostBookedPersonnelCriteria.SortAsc = True
    MostBookedPersonnelCriteria.SortColumn = "HoursBooked"

    ''Non-SA New Events
    'EventsImportedSinceLastLogin = New OBLib.Synergy.ReadOnly.ROImportedEventList
    'EventsImportedSinceLastLoginCriteria = New OBLib.Synergy.ReadOnly.ROImportedEventList.Criteria
    'EventsImportedSinceLastLoginPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.EventsImportedSinceLastLogin, Function(d) Me.EventsImportedSinceLastLoginCriteria, "Genre", 10)
    'EventsImportedSinceLastLoginCriteria.StartDate = OBLib.Security.Settings.CurrentUser.LastLoginDate
    'EventsImportedSinceLastLoginCriteria.EndDate = Nothing
    'EventsImportedSinceLastLoginCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    'EventsImportedSinceLastLoginCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    'EventsImportedSinceLastLoginCriteria.PageNo = 1
    'EventsImportedSinceLastLoginCriteria.PageSize = 10
    'EventsImportedSinceLastLoginCriteria.SortAsc = True
    'EventsImportedSinceLastLoginCriteria.SAInd = False
    'EventsImportedSinceLastLoginCriteria.SortColumn = "Genre"

    ''SA New Events
    'EventsImportedSinceLastLoginSA = New OBLib.Synergy.ReadOnly.ROImportedEventList
    'EventsImportedSinceLastLoginSACriteria = New OBLib.Synergy.ReadOnly.ROImportedEventList.Criteria
    'EventsImportedSinceLastLoginSAPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.EventsImportedSinceLastLoginSA, Function(d) Me.EventsImportedSinceLastLoginSACriteria, "Genre", 10)
    'EventsImportedSinceLastLoginSACriteria.StartDate = OBLib.Security.Settings.CurrentUser.LastLoginDate
    'EventsImportedSinceLastLoginSACriteria.EndDate = Nothing
    'EventsImportedSinceLastLoginSACriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    'EventsImportedSinceLastLoginSACriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    'EventsImportedSinceLastLoginSACriteria.PageNo = 1
    'EventsImportedSinceLastLoginSACriteria.PageSize = 10
    'EventsImportedSinceLastLoginSACriteria.SortAsc = True
    'EventsImportedSinceLastLoginSACriteria.SAInd = True
    'EventsImportedSinceLastLoginSACriteria.SortColumn = "Genre"

    'Events to be reconciled
    ProductionsToBeReconciled = New OBLib.Productions.ReadOnly.ROProductionList
    ProductionsToBeReconciledCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
    ProductionsToBeReconciledPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.ProductionsToBeReconciled, Function(d) Me.ProductionsToBeReconciledCriteria, "TxStartDateTime", 10)
    ProductionsToBeReconciledCriteria.TxDateFrom = Now.Date.AddDays(-62)
    ProductionsToBeReconciledCriteria.TxDateTo = Now.Date.AddDays(62)
    ProductionsToBeReconciledCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ProductionsToBeReconciledCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    ProductionsToBeReconciledCriteria.PageNo = 1
    ProductionsToBeReconciledCriteria.PageSize = 10
    ProductionsToBeReconciledCriteria.SortAsc = True
    ProductionsToBeReconciledCriteria.SortColumn = "Genre"

    ''Updated Productions
    'ProductionUpdatesSinceLastLogin = New OBLib.Synergy.ReadOnly.ROProductionSynergyChangeList
    'ProductionUpdatesSinceLastLoginCriteria = New OBLib.Synergy.ReadOnly.ROProductionSynergyChangeList.Criteria
    'ProductionUpdatesSinceLastLoginPagingManager = New Singular.Web.Data.PagedDataManager(Of LMDashboardVM)(Function(d) Me.ProductionUpdatesSinceLastLogin, Function(d) Me.ProductionUpdatesSinceLastLoginCriteria, "DetectedDateTime", 10)
    'ProductionUpdatesSinceLastLoginCriteria.StartDate = OBLib.Security.Settings.CurrentUser.LastLoginDate
    'ProductionUpdatesSinceLastLoginCriteria.EndDate = Now
    'ProductionUpdatesSinceLastLoginCriteria.PageNo = 1
    'ProductionUpdatesSinceLastLoginCriteria.PageSize = 10
    'ProductionUpdatesSinceLastLoginCriteria.SortAsc = True
    'ProductionUpdatesSinceLastLoginCriteria.SortColumn = "DetectedDateTime"

    ClientDataProvider.AddDataSource("ROProductionList", OBLib.CommonData.Lists.ROProductionList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)

    Select Case Command

      Case "AddProductionBudgeted"
        Dim ProductionID = CommandArgs.ClientArgs.ProductionID
        CurrentBudgetedProduction = OBLib.Productions.Budgeting.ProductionBudgetedList.GetProductionBudgetedList(ProductionID).FirstOrDefault

      Case "SaveProductionBudgeted"
        Dim blst As New OBLib.Productions.Budgeting.ProductionBudgetedList
        blst.Add(CurrentBudgetedProduction)
        blst.CheckAllRules()
        Dim shr As Singular.SaveHelper = TrySave(blst)
        If Not shr.Success Then
          AddMessage(Singular.Web.MessageType.Error, "Error During Save", shr.ErrorText)
        Else
          CurrentBudgetedProduction = CType(shr.SavedObject, OBLib.Productions.Budgeting.ProductionBudgetedList).FirstOrDefault
        End If

      Case "AddProductionToList"
        Dim ProductionID As Integer = CommandArgs.ClientArgs.ProductionID
        Dim ProductionSystemAreaID As Integer = CommandArgs.ClientArgs.ProductionSystemAreaID
        Dim Title As String = CommandArgs.ClientArgs.Title
        Dim PlayStartDateTime As DateTime = CommandArgs.ClientArgs.PlayStartDateTime
        Dim PlayEndDateTime As DateTime = CommandArgs.ClientArgs.PlayEndDateTime
        Dim ProductionDescription As String = CommandArgs.ClientArgs.ProductionDescription
        CurrentProductionList2.Add(New ProductionDetails(ProductionID, ProductionSystemAreaID, Title, PlayStartDateTime, PlayEndDateTime, ProductionDescription))

      Case "RemoveProductionFromList"
        Dim ProductionID As Integer = CommandArgs.ClientArgs.ProductionID
        Dim ProductionSystemAreaID = CommandArgs.ClientArgs.ProductionSystemAreaID
        CurrentProductionList.Remove(CurrentProductionList.Where(Function(c) c.ParentProductionID = ProductionID).FirstOrDefault)

      Case "AddSupplier"
        Dim SupplierID = CommandArgs.ClientArgs.SupplierID
        Dim Supplier = CommandArgs.ClientArgs.Supplier
        AddSupplierList.Add(SupplierID)
        AddSupplierListStrings.Add(Supplier)

      Case "GenerateProductionUpdate"
        'For Each Production In CurrentProductionList
        Dim ProductionsString As String = ""
        Dim ProdCount As Integer = 0
        For Each Production In CurrentProductionList2
          If Production.ProductionSystemAreaID = 0 Then
            ProdCount += 1
            ProductionsString = ProductionsString + ProdCount.ToString + ") " + Production.ProductionDescription + ", "
            'Exit For
          End If
          For Each Supplier In AddSupplierList
            Dim cmd As New Singular.CommandProc("CmdProcs.cmdUpdateProductionSupplierMapping", _
                                            New String() {"ProductionID",
                                                          "SupplierID",
                                                          "ProductionSystemAreaID",
                                                          "OutSourceServiceTypeID",
                                                          "ModifiedBy",
                                                          "SystemID",
                                                          "ProductionAreaID"}, _
                                            New Object() {Production.ProductionID,
                                                          Supplier,
                                                          Production.ProductionSystemAreaID,
                                                          IIf(Supplier = ProductionSupplierInfo.VisionView, ProductionSupplierInfo.OB, ProductionSupplierInfo.Graphics),
                                                          OBLib.Security.Settings.CurrentUser.UserID,
                                                          OBLib.Security.Settings.CurrentUser.SystemID,
                                                          OBLib.Security.Settings.CurrentUser.ProductionAreaID})
            cmd.FetchType = Singular.CommandProc.FetchTypes.DataRow
            cmd.Execute()
          Next
        Next
        CurrentProductionList2 = New List(Of ProductionDetails)
        AddSupplierList = New List(Of Integer)
        AddSupplierListStrings = New List(Of String)
        If ProductionsString.Trim.Length > 2 Then
          ProductionsString = ProductionsString.Trim().Remove(ProductionsString.Length - 2)
        End If
        CommandArgs.ReturnData = ProductionsString

      Case "ClearProductionList"
        CurrentProductionList2 = New List(Of ProductionDetails)

    End Select
  End Sub

  Class ProductionDetails
    Public ProductionID As Integer
    Public ProductionSystemAreaID As Integer
    Public Title As String
    Public PlayStartDateTime As DateTime
    Public PlayEndDateTime As DateTime
    Public ProductionDescription As String

    Public Sub New(ProductionID, ProductionSystemAreaID, Title, PlayStartDateTime, PlayEndDateTime, ProductionDescription)
      Me.ProductionID = ProductionID
      Me.ProductionSystemAreaID = ProductionSystemAreaID
      Me.Title = Title
      Me.PlayStartDateTime = PlayStartDateTime
      Me.PlayEndDateTime = PlayEndDateTime
      Me.ProductionDescription = ProductionDescription
    End Sub
  End Class

  <Singular.Web.WebCallable()>
  Public Function UpdateProduction(ProductionID As Integer, SystemID As Integer, ProductionAreaID As Integer, Title As String, PlayStartDateTime As DateTime, PlayEndDateTime As DateTime, ProductionDescription As String)
    Return New Singular.Web.Result(Sub()
                                     Dim PSAID As Integer = 0
                                     Dim ProductionSystemArea As OBLib.Productions.Areas.ReadOnly.ROProductionSystemArea
                                     'ProductionSystemArea = OBLib.Productions.Areas.ReadOnly.ROProductionSystemAreaList.GetROProductionSystemAreaList(ProductionID). _
                                     '                                       Where(Function(c) c.SystemID = SystemID AndAlso _
                                     '                                                          c.ProductionAreaID = ProductionAreaID).FirstOrDefault
                                     ProductionSystemArea = OBLib.Productions.Areas.ReadOnly.ROProductionSystemAreaList.GetROProductionSystemAreaList(ProductionID).FirstOrDefault
                                     If ProductionSystemArea IsNot Nothing Then
                                       PSAID = ProductionSystemArea.ProductionSystemAreaID
                                     End If
                                     CurrentProductionList2.Add(New ProductionDetails(ProductionID, PSAID, Title, PlayStartDateTime, PlayEndDateTime, ProductionDescription))
                                   End Sub)
  End Function

  Public Function GetEventsImportedSinceLastLoginCount(StartDate As Date, EndDate As Date,
                                                       PageNo As Integer, PageSize As Integer,
                                                       SortColumn As String, SortAsc As Boolean) As Singular.Web.Result

    'Try
    '  Dim TempROImportedEventList As OBLib.Synergy.ReadOnly.ROImportedEventList = OBLib.Synergy.ReadOnly.ROImportedEventList.GetROImportedEventList(OBLib.Security.Settings.CurrentUser.LastLoginDate,
    '                                                                                                                                                Nothing,
    '                                                                                                                                                OBLib.Security.Settings.CurrentUser.SystemID,
    '                                                                                                                                                OBLib.Security.Settings.CurrentUser.ProductionAreaID, 1, 10, True, "GenreDesc", "", False)


    '  Return New Singular.Web.Result(True) With {
    '                                             .Data = TempROImportedEventList.TotalRecords
    '                                            }
    'Catch ex As Exception
    Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = "disabled"}
    'End Try

  End Function

  Public Function GetEventsImportedSinceLastLoginSACount(StartDate As Date, EndDate As Date,
                                                       PageNo As Integer, PageSize As Integer,
                                                       SortColumn As String, SortAsc As Boolean) As Singular.Web.Result

    'Try
    '  Dim TempROImportedEventList As OBLib.Synergy.ReadOnly.ROImportedEventList = OBLib.Synergy.ReadOnly.ROImportedEventList.GetROImportedEventList(OBLib.Security.Settings.CurrentUser.LastLoginDate,
    '                                                                                                                                                Nothing,
    '                                                                                                                                                OBLib.Security.Settings.CurrentUser.SystemID,
    '                                                                                                                                                OBLib.Security.Settings.CurrentUser.ProductionAreaID, 1, 10, True, "GenreDesc", "", True)


    '  Return New Singular.Web.Result(True) With {
    '                                             .Data = TempROImportedEventList.TotalRecords
    '                                            }
    'Catch ex As Exception
    Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = "disabled"}
    'End Try

  End Function

  Public Function GetEventsToBeReconciledCount(StartDate As Date?, EndDate As Date?,
                                               PageNo As Integer, PageSize As Integer,
                                               SortColumn As String, SortAsc As Boolean) As Singular.Web.Result

    Try
      Dim TempROProductionList As OBLib.Productions.ReadOnly.ROProductionList = OBLib.Productions.ReadOnly.ROProductionList.GetROProductionList(Nothing, Nothing, Nothing, Nothing, Nothing, StartDate, EndDate,
                                                                                                                                                OBLib.Security.Settings.CurrentUser.SystemID, StartDate, EndDate,
                                                                                                                                                Nothing, Nothing, OBLib.CommonData.Enums.ProductionAreaStatus.Reconciled,
                                                                                                                                                OBLib.Security.Settings.CurrentUser.ProductionAreaID, PageNo, PageSize, SortAsc, SortColumn)


      Return New Singular.Web.Result(True) With {
                                                 .Data = TempROProductionList.TotalRecords
                                                }
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = ex.Message}
    End Try

  End Function

  Public Function GetProductionUpdatesSinceLastLoginCount(StartDate As Date, EndDate As Date,
                                                          ProductionIDs As String, ProductionID As Integer?,
                                                          PageNo As Integer, PageSize As Integer,
                                                          SortColumn As String, SortAsc As Boolean) As Singular.Web.Result

    'Try
    '  Dim TempROProductionSynergyChangeList As OBLib.Synergy.ReadOnly.ROProductionSynergyChangeList = OBLib.Synergy.ReadOnly.ROProductionSynergyChangeList.GetROProductionSynergyChangeList(StartDate, EndDate, ProductionIDs, ProductionID, PageNo, PageSize, SortAsc, SortColumn)


    '  Return New Singular.Web.Result(True) With {
    '                                             .Data = TempROProductionSynergyChangeList.TotalRecords
    '                                            }
    'Catch ex As Exception
    Return New Singular.Web.Result(False) 'With {.Data = Nothing, .ErrorText = ex.Message}
    ' End Try

  End Function

#End Region

End Class