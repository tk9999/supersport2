﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master"
  CodeBehind="EventManagerDashboard.aspx.vb" Inherits="NewOBWeb.EventManagerDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%--  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <link href="../Productions/Dashboard.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    div.ROProductionList td, div.ROProductionList th
    {
      font-size: 11px;
    }
  </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    Using h = Helpers

      '-------------------------------------------Toolbar-------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          .Helpers.BootstrapPageHeader(2, "Dashboard", "")
        End With
      End With

      '-------------------------------------------Criteria-------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Filters", , , , )
            With .ContentTag
              'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 4, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.LabelFor(Function(p) ViewModel.EventManagerID)
                    With .Helpers.Bootstrap.FormControlFor(Function(p) ViewModel.EventManagerID, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 6, 4, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.LabelFor(Function(p) ViewModel.StartDate)
                    With .Helpers.Bootstrap.FormControlFor(Function(p) ViewModel.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 6, 4, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.LabelFor(Function(p) ViewModel.EndDate)
                    With .Helpers.Bootstrap.FormControlFor(Function(p) ViewModel.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 6, 4, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.LabelFor(Function(p) ViewModel.ProductionFilter)
                    With .Helpers.Bootstrap.FormControlFor(Function(p) ViewModel.ProductionFilter, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterProductions}")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 6, 4, 3, 2)
                  .Helpers.Bootstrap.Button("FindProductions", "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-search", , Singular.Web.PostBackType.Ajax, , )
                End With
              End With
              'End With
            End With
          End With
        End With
        
      End With
      
      '-------------------------------------------Production List-------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("My Productions", , , , )
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.TableFor(Of OBLib.Dashboards.ReadOnly.ROEventManagerProduction)(Function(vm) ViewModel.ROEventManagerProductionList, False, False, False, False, True, True, True)
                  With .FirstRow
                    '.AddBinding(Singular.Web.KnockoutBindingString.css, "GetStateCssClass($data)")
                    With .AddColumn("")
                      .Style.Width = 50
                      .Helpers.BootstrapButton(, "Edit", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.Ajax, , , , "EditSelectedProduction($data)")
                    End With
                    With .AddColumn("")
                      .Helpers.BootstrapStateButton("", "CurrentStateClick($data)", "CurrentStateCss($data)", "CurrentStateHtml($data)", False)
                    End With
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.ProductionRefNo).AddClass("bold")
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.ProductionDescription)
                      .AddClass("bold")
                      .Style.Width = "30%"
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.ProductionType)
                      .Style.Width = "12%"
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.EventType)
                      .Style.Width = "12%"
                    End With
                    .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.TeamsPlaying)
                    '.AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.SynergyGenRefNo)  
                    '.AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.ProductionVenue)
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.PlayStartDateTime)
                      .Style.Width = "8%"
                    End With
                    With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROEventManagerProduction) rop.PlayEndDateTime)
                      .Style.Width = "8%"
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    
    End Using
  %>
  <script type="text/javascript">
    function CurrentStateClick(ROEventManagerProduction) {
    }
    function CurrentStateCss(ROEventManagerProduction) {
      if (ROEventManagerProduction.Cancelled()) {
        return 'btn btn-xs btn-danger';
      } else if (ROEventManagerProduction.Reconciled()) {
        return 'btn btn-xs btn-warning';
      } else if (ROEventManagerProduction.PlanningFinalised()) {
        return 'btn btn-xs btn-success';
      } else if (ROEventManagerProduction.CrewFinalised()) {
        return 'btn btn-xs btn-info';
      } else {
        return 'btn btn-xs btn-default';
      }
    }
    function CurrentStateHtml(ROEventManagerProduction) {
      if (ROEventManagerProduction.Cancelled()) {
        return '<span class="glyphicon glyphicon-remove"></span> Cancelled';
      } else if (ROEventManagerProduction.Reconciled()) {
        return '<span class="glyphicon glyphicon-ok"></span> Reconciled';
      } else if (ROEventManagerProduction.PlanningFinalised()) {
        return '<span class="glyphicon glyphicon-thumbs-up"></span> Planning Finalised';
      } else if (ROEventManagerProduction.CrewFinalised()) {
        return '<span class="glyphicon glyphicon-user"></span> Crew Finalised';
      } else {
        return '<span class="glyphicon glyphicon-asterisk"></span> New';
      }
    }
    function GetStateCssClass(ROEventManagerProduction) {
      if (ROEventManagerProduction.Cancelled()) {
        return 'Cancelled';
      } else if (ROEventManagerProduction.Reconciled()) {
        return 'Reconciled';
      } else if (ROEventManagerProduction.PlanningFinalised()) {
        return 'PlanningFinalised';
      } else if (ROEventManagerProduction.CrewFinalised()) {
        return 'CrewFinalised';
      }
    };
    function EditSelectedProduction(ROPRoduction) {
      Singular.SendCommand('EditProduction', { ProductionID: ROPRoduction.ProductionID() }, function (d) {
        window.location.href = d;
      });
    };
    function FilterProductions() {
      Singular.ShowLoadingBar();
      if (!ViewModel.ProductionFilter() || ViewModel.ProductionFilter() == '') {
        ViewModel.ROEventManagerProductionList().Iterate(function (Production, Index) {
          Production.Visible(true);
        });
      } else {
        ViewModel.ROEventManagerProductionList().Iterate(function (Production, Index) {
          var FilterString = ViewModel.ProductionFilter().toLowerCase();
          var DescMatchInd = (Production.ProductionDescription().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          var RefNoMatchInd = (Production.ProductionRefNo().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          var PTypeMatchInd = (Production.ProductionType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          var ETypeMatchInd = (Production.EventType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          var TeamsMatchInd = (Production.TeamsPlaying().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
          if (DescMatchInd || RefNoMatchInd || PTypeMatchInd || ETypeMatchInd || TeamsMatchInd) {
            Production.Visible(true);
          } else {
            Production.Visible(false);
          }
        });
      };
      Singular.HideLoadingBar();
    };
    function CreateProduction() {
      window.location.href = "../Productions/Production.aspx?New=1"
    }
  </script>
</asp:Content>
