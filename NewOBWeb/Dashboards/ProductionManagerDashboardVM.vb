﻿Imports OBLib.Productions.Old
Imports Singular.DataAnnotations
Imports System.ComponentModel
Imports OBLib.Productions.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports System

Public Class ProductionManagerDashboardVM
  Inherits OBViewModel(Of ProductionManagerDashboardVM)

#Region " Properties "

  Private mROProductionManagerProductionList As OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList
  Public ReadOnly Property ROProductionManagerProductionList As OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList
    Get
      Return mROProductionManagerProductionList
    End Get
  End Property

  <ClientOnly()>
  Public Property ProductionFilter As String = ""
  Public Property StartDate As DateTime?
  Public Property EndDate As DateTime?

  Public Property CurrentSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID
  Public Property CurrentProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

#Region " Report "

  Public Property ReportPopupInd As Boolean = False

  Public Property ROProductionList As ROProductionList
  Public Property SelectedROProductionList As List(Of OBLib.Helpers.SelectedROProduction) = New List(Of OBLib.Helpers.SelectedROProduction)

  Public Property ROHumanResourceList As ROHumanResourceList
  Public Property SelectedROHumanResourceList As List(Of OBLib.Helpers.SelectedROHumanResource) = New List(Of OBLib.Helpers.SelectedROHumanResource)

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROProductionFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROHumanResourceFilter As String = ""

  Public Shared ReportStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReportStartDate, "Report Start Date")
  <System.ComponentModel.DataAnnotations.Display(Name:="Start Date")>
  Public Property ReportStartDate As DateTime?
    Get
      Return GetProperty(ReportStartDateProperty)
    End Get
    Set(value As DateTime?)
      SetProperty(ReportStartDateProperty, value)
    End Set
  End Property

  Public Shared ReportEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReportEndDate, "Report End Date")
  <System.ComponentModel.DataAnnotations.Display(Name:="End Date")>
  Public Property ReportEndDate As DateTime?
    Get
      Return GetProperty(ReportEndDateProperty)
    End Get
    Set(value As DateTime?)
      SetProperty(ReportEndDateProperty, value)
    End Set
  End Property

#End Region

#End Region
#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    MyBase.Setup()

    ValidationMode = Singular.Web.ValidationMode.OnLoad
    StartDate = Singular.Dates.DateMonthStart(Now).AddDays(-7)
    EndDate = Singular.Dates.DateMonthEnd(Now.AddMonths(1))
    mROProductionManagerProductionList = OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList.GetROProductionManagerProductionList(OBLib.Security.Settings.CurrentUser.HumanResourceID, StartDate, EndDate)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)

    Select Case Command
      Case "EditProduction"
        If CommandArgs.ClientArgs.ProductionID IsNot Nothing Then
          If IsNumeric(CommandArgs.ClientArgs.ProductionID) Then
            Dim ProductionID As Integer = CommandArgs.ClientArgs.ProductionID
            'OBLib.Security.Settings.CurrentUser.ProductionAreaID
            Dim pl As ProductionList = ProductionList.GetProductionList(ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, 1) '1 = OB
            If pl.Count = 1 Then
              SessionData.CurrentProduction = pl(0)
            End If
            CommandArgs.ReturnData = VirtualPathUtility.ToAbsolute("~/Productions/Production.aspx")
          End If
        End If

      Case "FindProductions"
        mROProductionManagerProductionList = OBLib.Dashboards.ReadOnly.ROProductionManagerProductionList.GetROProductionManagerProductionList(OBLib.Security.Settings.CurrentUser.HumanResourceID, StartDate, EndDate)

    End Select

  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

    With AddWebRule(ReportStartDateProperty)
      .JavascriptRuleFunctionName = "ReportStartDateValid"
    End With

    With AddWebRule(ReportEndDateProperty)
      .JavascriptRuleFunctionName = "ReportEndDateValid"
    End With

  End Sub

#End Region

End Class
