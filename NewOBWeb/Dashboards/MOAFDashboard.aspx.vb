﻿Public Class MOAFDashboard
  Inherits OBPageBase(Of MOAFDashboardVM)

End Class

Public Class MOAFDashboardVM
  Inherits OBViewModel(Of MOAFDashboardVM)

  Public Property PositionBookings As OBLib.Dashboards.ReadOnly.ROPositionBookingList
  Public Property PositionBookingsCriteria As OBLib.Dashboards.ReadOnly.ROPositionBookingList.Criteria
  Public Property PositionBookingsPagingManager As Singular.Web.Data.PagedDataManager(Of MOAFDashboardVM)

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    MyBase.Setup()

    If Not Singular.Security.HasAccess("Dashboards", "Can View Manager of Additional Facilities Dashboard") Then
      RedirectToNoAccess()
    End If

    ValidationMode = Singular.Web.ValidationMode.OnLoad

    'SteadyCam Bookings
    PositionBookings = New OBLib.Dashboards.ReadOnly.ROPositionBookingList
    PositionBookingsCriteria = New OBLib.Dashboards.ReadOnly.ROPositionBookingList.Criteria
    PositionBookingsPagingManager = New Singular.Web.Data.PagedDataManager(Of MOAFDashboardVM)(Function(d) Me.PositionBookings, Function(d) Me.PositionBookingsCriteria, "TxStartDateTime", 15)
    PositionBookingsCriteria.TxDateFrom = Now.AddDays(-3)
    PositionBookingsCriteria.TxDateTo = Now.AddDays(7)
    PositionBookingsCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    PositionBookingsCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    PositionBookingsCriteria.PageNo = 1
    PositionBookingsCriteria.PageSize = 15
    PositionBookingsCriteria.SortAsc = True
    PositionBookingsCriteria.SortColumn = "TxStartDateTime"
    PositionBookingsCriteria.PositionIDs = ""

  End Sub

End Class