﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="LMDashboard.aspx.vb" Inherits="NewOBWeb.LMDashboard" %>

<%@ Import Namespace="OBLib.Productions.Budgeting" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>
<%@ Import Namespace="OBLib.Dashboards.ReadOnly" %>
<%@ Import Namespace="OBLib.Synergy.ReadOnly" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.HR.ReadOnly" %>
<%@ Import Namespace="OBLib.RoomScheduling.ReadOnly" %>
<%@ Import Namespace="OBLib.RoomScheduling.ProductionServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%--  <%= Singular.Web.CSSFile.RenderLibraryStyles%>  --%>
  <%--  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>  --%>
  <%--  <script type="text/javascript" src="../Scripts/core.js"></script>--%>
  <script type="text/javascript" src="../Scripts/Tools/PagingManagers.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionsOld.js"></script>
  <script type="text/javascript">

    var fetchTimeout = null,
        DELAY = 250

    var ROProductionsBudgeted = new ProductionsBudgetedPagingManager();
    var RODebtors = new RODebtorListManager();
    var ROProductions = new ROProductionListManager();

    function CurrentContext(Obj) {
      console.log(Obj);
    };

    function PSAStatusCss(RORoomScheduleObject) {
      return "label " + RORoomScheduleObject.CssClass();
    };

    function PSAStatusHtml(RORoomScheduleObject) {
      return RORoomScheduleObject.ProductionAreaStatus();
    };

    function ROProdStatusCss(ROProd) {
      return "label " + ''; //ROProd.CssClass();
    };

    function ROProdStatusHtml(ROProd) {
      return ROProd.ProductionAreaStatus();
    };

    function GetHoursWorkedCss(PersonnelHoursBooked) {
      var css = "progress-bar ";
      if (PersonnelHoursBooked.HoursBooked() < 135) {
        css += "progress-bar-success";
      } else if (PersonnelHoursBooked.HoursBooked() < 162) {
        css += "progress-bar-info";
      } else if (PersonnelHoursBooked.HoursBooked() < 180) {
        css += "progress-bar-warning";
      } else {
        css += "progress-bar-danger";
      }
      return css;
    };

    function GetHoursWorkedStyle(PersonnelHoursBooked) {
      var PercWorked = parseInt((PersonnelHoursBooked.HoursBooked() / 180) * 100);
      if (PercWorked > 100) {
        PercWorked = 100;
      }
      return { width: PercWorked.toString() + '%' };
    };

    function GetHoursWorkedHtml(PersonnelHoursBooked) {
      var PercWorked = parseInt((PersonnelHoursBooked.HoursBooked() / 180) * 100);
      return PercWorked.toString() + '%';
    };

    function GetContractDaysWorkedCss(PersonnelHoursBooked) {
      var css = "progress-bar ";
      var perc = parseInt((PersonnelHoursBooked.DaysBooked() / PersonnelHoursBooked.ContractDays()) * 100);
      if (perc < 70) {
        css += "progress-bar-success";
      } else if (PersonnelHoursBooked.HoursBooked() < 80) {
        css += "progress-bar-info";
      } else if (PersonnelHoursBooked.HoursBooked() < 90) {
        css += "progress-bar-warning";
      } else {
        css += "progress-bar-danger";
      }
      return css;
    };

    function GetContractDaysWorkedStyle(PersonnelHoursBooked) {
      var PercWorked = parseInt((PersonnelHoursBooked.DaysBooked() / PersonnelHoursBooked.ContractDays()) * 100);
      if (PercWorked > 100) {
        PercWorked = 100;
      }
      return { width: PercWorked.toString() + '%' };
    };

    function GetContractDaysWorkedHtml(PersonnelHoursBooked) {
      var PercWorked = parseInt((PersonnelHoursBooked.DaysBooked() / PersonnelHoursBooked.ContractDays()) * 100);
      return PercWorked.toString() + '%';
    };

    //most booked personnel
    function RefreshMostBookedPersonnel() {
      ViewModel.MostBookedPersonnelPagingManager().Refresh();
    };

    //new events
    function NewEventCount() {
      return ViewModel.EventsImportedSinceLastLoginCount();
    };

    function ShiftsRequiringAuthorisationCount() {
      return 1;
    };

    function RefreshEventsImportedSinceLastLoginCount() {
      ViewModel.EventsImportedSinceLastLoginIsLoading(true);
      ViewModel.CallServerMethod("GetEventsImportedSinceLastLoginCount", {}, function (response) {
        ViewModel.EventsImportedSinceLastLoginCount(response.Data);
        ViewModel.EventsImportedSinceLastLoginIsLoading(false);
      });
    };

    function ShowEventsImportedSinceLastLogin() {
      ViewModel.EventsImportedSinceLastLoginPagingManager().Refresh();
      $("#EventsImportedSinceLastLogin").modal();
    };

    function RefreshROImportedEventList() {
      ViewModel.EventsImportedSinceLastLoginPagingManager().Refresh();
      ViewModel.EventsImportedSinceLastLoginSAPagingManager().Refresh();
    };

    function NewEventCountSA() {
      return ViewModel.EventsImportedSinceLastLoginSACount();
    };

    function RefreshEventsImportedSinceLastLoginCountSA() {
      ViewModel.EventsImportedSinceLastLoginSAIsLoading(true);
      ViewModel.CallServerMethod("GetEventsImportedSinceLastLoginSACount", {}, function (response) {
        ViewModel.EventsImportedSinceLastLoginSACount(response.Data);
        ViewModel.EventsImportedSinceLastLoginSAIsLoading(false);
      });
    };

    function RefreshEventsImportedSinceLastLoginCountSA() {
      ViewModel.EventsImportedSinceLastLoginSAIsLoading(true);
      ViewModel.CallServerMethod("GetEventsImportedSinceLastLoginSACount", {}, function (response) {
        ViewModel.EventsImportedSinceLastLoginSACount(response.Data);
        ViewModel.EventsImportedSinceLastLoginSAIsLoading(false);
      });
    };

    function ShowEventsImportedSinceLastLoginSA() {
      ViewModel.EventsImportedSinceLastLoginSAPagingManager().Refresh();
      $("#EventsImportedSinceLastLoginSA").modal();
    };

    //updated events
    function ProductionUpdatesCount() {
      return ViewModel.ProductionUpdatesSinceLastLoginCount();
    };

    function RefreshProductionUpdatesSinceLastLoginCount() {
      ViewModel.ProductionUpdatesSinceLastLoginIsLoading(true);
      ViewModel.CallServerMethod("GetProductionUpdatesSinceLastLoginCount", {
        StartDate: ViewModel.ProductionUpdatesSinceLastLoginCriteria().StartDate(),
        EndDate: ViewModel.ProductionUpdatesSinceLastLoginCriteria().EndDate(),
        ProductionIDs: ViewModel.ProductionUpdatesSinceLastLoginCriteria().ProductionIDs(),
        ProductionID: ViewModel.ProductionUpdatesSinceLastLoginCriteria().ProductionID(),
        PageNo: ViewModel.ProductionUpdatesSinceLastLoginPagingManager().PageNo(),
        PageSize: ViewModel.ProductionUpdatesSinceLastLoginPagingManager().PageSize(),
        SortColumn: ViewModel.ProductionUpdatesSinceLastLoginPagingManager().SortColumn(),
        SortAsc: ViewModel.ProductionUpdatesSinceLastLoginPagingManager().SortAsc()
      }, function (response) {
        ViewModel.ProductionUpdatesSinceLastLoginCount(response.Data);
        ViewModel.ProductionUpdatesSinceLastLoginIsLoading(false);
      });
    };

    function ShowProductionUpdatesSinceLastLogin() {
      ViewModel.ProductionUpdatesSinceLastLoginPagingManager().Refresh();
      $("#ProductionUpdatesSinceLastLogin").modal();
    };

    function RefreshROSynergyChangeList() {
      ViewModel.ProductionUpdatesSinceLastLoginPagingManager().Refresh();
    };

    //events to be reconciled
    function RefreshProductionsToBeReconciledCount() {
      ViewModel.ProductionsToBeReconciledIsLoading(true);
      ViewModel.CallServerMethod("GetEventsToBeReconciledCount", {
        StartDate: ViewModel.ProductionsToBeReconciledCriteria().TxDateFrom(),
        EndDate: ViewModel.ProductionsToBeReconciledCriteria().TxDateTo(),
        PageNo: ViewModel.ProductionsToBeReconciledPagingManager().PageNo(),
        PageSize: ViewModel.ProductionsToBeReconciledPagingManager().PageSize(),
        SortColumn: ViewModel.ProductionsToBeReconciledPagingManager().SortColumn(),
        SortAsc: ViewModel.ProductionsToBeReconciledPagingManager().SortAsc()
      }, function (response) {
        ViewModel.ProductionsToBeReconciledCount(response.Data);
        ViewModel.ProductionsToBeReconciledIsLoading(false);
      });
    };

    function RefreshProductionsToBeReconciled() {
      ViewModel.ProductionsToBeReconciledPagingManager().Refresh();
    };

    function ShowProductionsToBeReconciled() {
      ViewModel.ProductionsToBeReconciledPagingManager().Refresh();
      $("#ProductionsToBeReconciled").modal();
    };

    function RefreshROProductionList() {
      ViewModel.ProductionsToBeReconciledPagingManager().Refresh();
    };

    function RefreshProductionsBudgeted() {
      ViewModel.ROProductionListPagingManager().Refresh();
    };

    function RefreshProductions() {
      ViewModel.ProductionsBudgetedPagingManager().Refresh();
    };

    function VisionViewCss(ROEventManagerProduction) {
      if (ROEventManagerProduction.VisionView()) {
        return 'btn btn-xs btn-primary';
      } else {
        return 'btn btn-xs btn-default';
      }
    }
    function VisionViewHtml(ROEventManagerProduction) {
      if (ROEventManagerProduction.VisionView()) {
        return '<span class="glyphicon glyphicon-check"></span> Yes';
      } else {
        return '<span class="glyphicon glyphicon-minus"></span> No';
      }
    };

    function ShowMostBookedPersonnelCriteria() {
      $("#MostBookedPersonnelModal").modal();
    };

    $(function () {
      Singular.OnPageLoad(function () {
        ROProductionsBudgeted.Init();
        ROProductionsBudgeted.Manager().SingleSelect(true);
        ROProductionsBudgeted.Manager().MultiSelect(false);
        ROProductionsBudgeted.SetOptions({
          AfterRowSelected: function (ROProductionsBudgeted) {
            ROProductionsBudgeted.SelectedInd(!ROProductionsBudgeted.SelectedInd());
            Singular.SendCommand("AddProductionBudgeted", { ProductionID: ROProductionsBudgeted.ProductionID() }, function (response) { });
            //ViewModel.CallServerMethod("AddProductionBudgeted", {
            //  ProductionID: ROProductionsBudgeted.ProductionID(),
            //}, function (args) {
            //});
            $('#ProductionsBudgetedModal').modal();
          },
          AfterRowDeslected: function (DeSelectedItem) {
          }
        });
        ROProductions.Init();
        ROProductions.Manager().SingleSelect(false);
        ROProductions.Manager().MultiSelect(true);
        ROProductions.SetOptions({
          AfterRowSelected: function (ROProduction) {
            ViewModel.CallServerMethod("UpdateProduction", {
              ProductionID: ROProduction.ProductionID(),
              //ProductionSystemAreaID: ROProduction.ProductionSystemAreaID(),
              SystemID: ViewModel.CurrentSystemID(),
              ProductionAreaID: ViewModel.CurrentProductionAreaID(),
              Title: ROProduction.Title(),
              PlayStartDateTime: ROProduction.PlayStartDateTime(),
              PlayEndDateTime: ROProduction.PlayEndDateTime(),
              ProductionDescription: ROProduction.ProductionDescription()
            }, function (args) {
            });
          },
          AfterRowDeslected: function (DeSelectedItem) {
            Singular.SendCommand("RemoveProductionFromList", { ProductionID: ROProduction.ProductionID() }, function () {
              ViewModel.ROProductionListPagingManager().Refresh();
            });
          }
        });
        ViewModel.ProductionsPagingManager().Refresh();
        ViewModel.MostBookedPersonnelPagingManager().Refresh();
        ViewModel.ProductionsBudgetedPagingManager().Refresh();
        ViewModel.ROProductionListPagingManager().Refresh();
        RefreshEventsImportedSinceLastLoginCount();
        RefreshProductionsToBeReconciledCount();
        RefreshProductionUpdatesSinceLastLoginCount();
        RefreshEventsImportedSinceLastLoginCountSA();
        RefreshProductionsBudgeted();
        RefreshProductions();
      });
    });

    function GetProductionStatColour(ROProduction) {
      var defaultStyle = "";
      switch (ROProduction.ProductionAreaStatusID()) {
        case 5:
          defaultStyle = 'Cancelled';
          break;
      }
      if (ROProduction.VisionView()) {
        defaultStyle = 'VisionView';
      }
      return defaultStyle;
    };

    function AddDebtor(Debtor) {
      ViewModel.CurrentBudgetedProduction().DebtorID(Debtor.DebtorID());
      ViewModel.RODebtorListCriteria().DebtorID(null);
      ViewModel.RODebtorListCriteria().Keyword('');
      ViewModel.RODebtorListManager().Refresh();
    };

    function FindBudgetedProductionDebtor(element) {
      RODebtors.Init()
      RODebtors.Manager().SingleSelect(true);
      RODebtors.Manager().MultiSelect(false);
      RODebtors.SetOptions({
        AfterRowSelected: function (RODebtor) {
          AddDebtor(RODebtor);
          $('#RODebtorListModal').modal('hide');
        },
        AfterRowDeslected: function (DeSelectedItem) {
          ViewModel.RODebtorListCriteria().DebtorID(null);
        }
      });
      $('#RODebtorListModal').modal();
    };

    function WorkingOnThisProduction(obj) {
      var defaultStyle = "";
      if (obj.SelectedInd()) {
        defaultStyle = 'SelectedProduction';
      }
      return defaultStyle;
    };

    function SaveProductionBudgeted(obj) {
      var ProductionBudgeted = ViewModel.CurrentBudgetedProduction();
      if (ProductionBudgeted.IsValid()) {
        Singular.SendCommand("SaveProductionBudgeted", {}, function (response) { });
        alert('Production Saved');
        $('#ProductionsBudgetedModal').modal('hide');
      }
      else
        alert('Please Fix Validation Errors');
    };

    function IsBudgetedProductionValid() {
      var valid = true;
      if (ViewModel.CurrentBudgetedProduction()) {
        if (!ViewModel.CurrentBudgetedProduction().IsValid()) {
          valid = false
        }
      }
      return valid;
    };

    function GetBudgetedProductionBrokenRulesHTML() {
      var ErrorString = ''
      if (ViewModel.CurrentBudgetedProduction().IsValid() == false) {
        ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(ViewModel.CurrentBudgetedProduction());
      }
      ErrorString = ErrorString.replace('undefined', ViewModel.CurrentBudgetedProduction().Title());
      return ErrorString;
    };

    function AddSupplier(ROProduction, SupplierID, Supplier) {
      ViewModel.AddSupplierList.push(SupplierID);
      ViewModel.AddSupplierListStrings.push(Supplier);
      ROProduction.SelectedInd(!ROProduction.SelectedInd());
    };

    function CheckIfSelected(SupplierID) {
      var SupplierExists = ViewModel.AddSupplierList().indexOf(SupplierID);
      return SupplierExists > -1;
    };

    function GenerateProductionUpdate() {
      Singular.SendCommand("GenerateProductionUpdate", {}, function (response) {
        ViewModel.ROProductionListPagingManager().Refresh();
        ViewModel.ROProductionListPagingManager().SelectedItems([]);
        if (response.trim().length == 0)
          alert("Productions Updated");
        else
          alert("Productions: " + response + " could not be updated as your area has not yet been attached to it/them");
      });
    };

    function ClearProductions() {
      Singular.SendCommand("ClearProductionList", {}, function () {
        ViewModel.ROProductionListPagingManager().SelectedItems([]);
        ViewModel.ROProductionListPagingManager().Refresh();
      });
    };

    function OBSupplier(ROProduction) {
      if (ROProduction.OBFacilitySuppliers().length > 0)
        return 'OB';
      else
        return '';
    };

    function GraphicsSupplier(ROProduction) {
      if (ROProduction.GraphicSuppliers().length > 0)
        return 'Graphics';
      else
        return '';
    };

    function SupplierCss(ROProduction, GRorOB) {
      if (GRorOB == 'Graphics')
        return "btn btn-xs btn-primary";
      else if (GRorOB == 'OB')
        return "btn btn-xs btn-danger";
      else
        return "btn btn-xs btn-default";
    };

    function isOB(ROProduction) {
      if (ROProduction.OBFacilitySuppliers().length > 0)
        return false;
      else
        return true;
    };

    function isGraphics(ROProduction) {
      if (ROProduction.GraphicSuppliers().length > 0)
        return false;
      else
        return true;
    };

  </script>
  <%--  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />--%>
  <link type="text/css" href="../Styles/production-system-area-status-styles.css" rel="Stylesheet" />
  <style type="text/css">
    /*body {
      background-color: #F0F0F0;
    }*/

    #ProductionHeader {
      white-space: normal !important;
      word-wrap: break-word;
    }

    #BookingHeader {
      white-space: normal !important;
      word-wrap: break-word;
    }

    #TodaysEvents li > a,
    #TodaysEvents .pager li > span,
    #MostBookedPersonnel li > a,
    #MostBookedPersonnel .pager li > span,
    #EventsImportedSinceLastLogin li > a,
    #EventsImportedSinceLastLogin .pager li > span,
    #ProductionsToBeReconciled li > a,
    #ProductionsToBeReconciled .pager li > span,
    #ProductionUpdatesSinceLastLogin li > a,
    #ProductionUpdatesSinceLastLogin .pager li > span {
      display: inline-block;
      padding: 4px 8px;
      background-color: #ffffff;
      border: 1px solid #dddddd;
      border-radius: 0px;
      color: #428bca;
    }

    #TodaysEvents li > a,
    #TodaysEvents .pager li > span,
    #MostBookedPersonnel li > a,
    #MostBookedPersonnel .pager li > span,
    #EventsImportedSinceLastLogin li > a,
    #EventsImportedSinceLastLogin .pager li > span,
    #ProductionsToBeReconciled li > a,
    #ProductionsToBeReconciled .pager li > span,
    #ProductionUpdatesSinceLastLogin li > a,
    #ProductionUpdatesSinceLastLogin .pager li > span {
      display: inline-block;
      padding: 4px 8px;
      background-color: #ffffff;
      border: 1px solid #dddddd;
      border-radius: 0px;
      color: #428bca;
    }

      #TodaysEvents li > a:hover,
      #TodaysEvents .pager li > span :hover,
      #MostBookedPersonnel li > a:hover,
      #MostBookedPersonnel .pager li > span :hover,
      #EventsImportedSinceLastLogin li > a:hover,
      #EventsImportedSinceLastLogin .pager li > span :hover,
      #ProductionsToBeReconciled li > a:hover,
      #ProductionsToBeReconciled .pager li > span :hover,
      #ProductionUpdatesSinceLastLogin li > a:hover,
      #ProductionUpdatesSinceLastLogin .pager li > span :hover {
        cursor: pointer;
        background: #428bca;
        color: white;
      }

    .flat-block-paged {
      padding-bottom: 40px;
    }

    .block-flat ul.pager {
      margin: 0;
    }

    .table > thead > tr > th {
      vertical-align: bottom;
      border-bottom: 0px solid #dddddd;
      background-color: #323232;
      color: white;
    }

    tr.VisionView td {
      background-color: #BFF1F1 !important;
    }

    tr.Cancelled td {
      background-color: #d2322d !important;
      color: white;
    }

    .GraphicsSuppliers {
      text-overflow: ellipsis;
    }

    .SelectedProduction {
      background-color: #4C4CFF !important;
    }

    tr.SelectedProduction td {
      background-color: #4C4CFF !important;
      color: white;
    }

    .ValidationPopup {
      min-height: 131px;
      max-height: 650px;
    }

    #ProdModal .modal-content .modal-footer {
      display: none;
    }

    #ProdDetails {
      background-color: #f1f1f1 !important;
      padding-bottom: 0;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  &nbsp;<% Using h = Helpers

            ''Heading----------------------------------------------------------------------------------------------------------------------
            'With h.Bootstrap.Row
            '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            '    .Helpers.BootstrapPageHeader(2, "Dashboard", , )
            '  End With
            'End With
      
            With h.Bootstrap.Row
              With .Helpers.Bootstrap.Column(6, 6, 4, 2)
                With .Helpers.Bootstrap.StaticDetailTile("fa-eye", , "NewEventCount()", "Events since last login (Non-SA)", , True, True, Singular.Web.BootstrapEnums.FDTileColor.Purple, "ShowEventsImportedSinceLastLogin()")
                  With .ContentTag
                    With .Helpers.Div
                      .AddClass("loading")
                      .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
                      .Bindings.AddVisibilityBinding("ViewModel.EventsImportedSinceLastLoginIsLoading()", VisibleFadeType.Fade, VisibleFadeType.Fade)
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(6, 6, 4, 2)
                With .Helpers.Bootstrap.StaticDetailTile("fa-eye", , "NewEventCountSA()", "Events since last login (SA)", , True, True, Singular.Web.BootstrapEnums.FDTileColor.Purple, "ShowEventsImportedSinceLastLoginSA()")
                  With .ContentTag
                    With .Helpers.Div
                      .AddClass("loading")
                      .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
                      .Bindings.AddVisibilityBinding("ViewModel.EventsImportedSinceLastLoginSAIsLoading()", VisibleFadeType.Fade, VisibleFadeType.Fade)
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(6, 6, 4, 2)
                With .Helpers.Bootstrap.StaticDetailTile("fa-bolt", , "ViewModel.ProductionUpdatesSinceLastLoginCount()", "Synergy changes since last login", , True, True, Singular.Web.BootstrapEnums.FDTileColor.Orange, "ShowProductionUpdatesSinceLastLogin()")
                  With .ContentTag
                    With .Helpers.Div
                      .AddClass("loading")
                      .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
                      .Bindings.AddVisibilityBinding("ViewModel.ProductionUpdatesSinceLastLoginIsLoading()", VisibleFadeType.Fade, VisibleFadeType.Fade)
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(6, 6, 4, 2)
                With .Helpers.Bootstrap.StaticDetailTile("fa-flag", , "ViewModel.ProductionsToBeReconciledCount()", "Productions need to be reconciled", , True, True, Singular.Web.BootstrapEnums.FDTileColor.Red, "ShowProductionsToBeReconciled()")
                  With .ContentTag
                    With .Helpers.Div
                      .AddClass("loading")
                      .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
                      .Bindings.AddVisibilityBinding("ViewModel.ProductionsToBeReconciledIsLoading()", VisibleFadeType.Fade, VisibleFadeType.Fade)
                    End With
                  End With
                End With
              End With
            End With
      
            With h.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.FlatBlock("Productions", True)
                  .FlatBlockTag.AddClass("flat-block-paged")
                  With .AboveContentTag
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ProductionsCriteria.StartDate, BootstrapEnums.InputSize.Small)
                    
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ProductionsCriteria.EndDate, BootstrapEnums.InputSize.Small)
                    
                        End With
                      End With
                    End With
                    'With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.TodaysRoomSchedulesCriteria.SystemID, "Sub-Dept")
                    
                    '    End With
                    '  End With
                    'End With
                    'With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.ProductionsCriteria.ProductionAreaID, "Area")
                    '      .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                    '    End With
                    '  End With
                    'End With
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Button(, "Refresh", , , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, "ViewModel.ProductionsPagingManager().Refresh()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                    End With
                  End With
                  With .ContentTag
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.PagedGridFor(Of ROProductionManagerProduction)(Function(vm) ViewModel.ProductionsPagingManager, Function(vm) ViewModel.Productions, False, False,
                                                                                             False, False, True, True, False, "TodaysEvents", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        With .FirstRow
                          .AddClass("items")
                          
                          .AddBinding(Singular.Web.KnockoutBindingString.css, "GetProductionStatColour($data)")
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.TxStartDateTime)
                          
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.TxEndDateTime)
                          
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProductionRefNo).AddClass("bold")
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProductionDescription).AddClass("bold")
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.TeamsPlaying)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.EventManager)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProductionManager)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.Languages)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.HRCount)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.OBVans)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.City)
                          With .AddColumn("Vision View")
                            .Helpers.BootstrapStateButton("", "", "VisionViewCss($data)", "VisionViewHtml($data)", False)
                          End With
                          
                          If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
                            With .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.GraphicsSuppliers)
                              ' .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1 GraphicsSuppliers")
                            End With
                          End If
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.ProducerNames)
                          .AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.DirectorNames)
                          '.AddReadOnlyColumn(Function(rop As OBLib.Dashboards.ReadOnly.ROProductionManagerProduction) rop.City)
                        End With
                        
                      End With
                    End With
                  End With
                End With
              End With
            End With
            
            If OBLib.Security.Settings.CurrentUser.UserID = 152 Then
              With h.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Budgeting", True)
                    .FlatBlockTag.AddClass("flat-block-paged")
                    With .AboveContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ProductionsBudgetedCriteria.StartDate, BootstrapEnums.InputSize.Small)
                              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'None'")
                              ' .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionsBudgeted.DelayedRefreshList() }")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ProductionsBudgetedCriteria.EndDate, BootstrapEnums.InputSize.Small)
                              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'None'")
                              ' .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionsBudgeted.DelayedRefreshList() }")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.EditorFor(Function(d) ViewModel.ProductionsBudgetedCriteria.Keyword)
                              .AddClass("form-control input-sm")
                              .Attributes("placeholder") = "Search Outside Budget Events..."
                              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                              .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionsBudgeted.DelayedRefreshList() }")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "Refresh", , , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, "ViewModel.ProductionsBudgetedPagingManager().Refresh()")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.StateButton("ViewModel.ProductionsBudgetedCriteria().BudgetedInd", "Budgeted", "Unbudgeted", "btn-success", "btn-danger", , , "btn-sm")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.event, "{ onclick: ROProductionsBudgeted.DelayedRefreshList() }")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.StateButton("ViewModel.ProductionsBudgetedCriteria().RingFencedInd", "Ring-Fenced", "Not Ring-Fenced", "btn-success", "btn-danger", , , "btn-sm")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.event, "{ onclick: ROProductionsBudgeted.DelayedRefreshList() }")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.StateButton("ViewModel.ProductionsBudgetedCriteria().DomesticInd", "Domestic", "International", "btn-success", "btn-primary", "fa-home", " fa-globe", "btn-sm")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.event, "{ onclick: ROProductionsBudgeted.DelayedRefreshList() }")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    'End With
                    With .ContentTag
                      With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted)(Function(d) ViewModel.ProductionsBudgetedPagingManager,
                                                                                 Function(d) ViewModel.ProductionsBudgeted,
                                                                                 False, False, False, False, True, True, True,
                                                                                 , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                 "ROProductionsBudgeted.OnItemSelected", False)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        With .FirstRow
                          .AddClass("items")
                          .AddBinding(Singular.Web.KnockoutBindingString.css, "WorkingOnThisProduction($data)")
                          .AddReadOnlyColumn(Function(obe As OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted) obe.Title)
                          .AddReadOnlyColumn(Function(obe As OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted) obe.PlayStartDateTime)
                          .AddReadOnlyColumn(Function(obe As OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted) obe.PlayEndDateTime)
                          .AddReadOnlyColumn(Function(obe As OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted) obe.ProductionDescription)
                          .AddReadOnlyColumn(Function(obe As OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted) obe.EventType)
                          .AddReadOnlyColumn(Function(obe As OBLib.Productions.Budgeting.ReadOnly.ROProductionBudgeted) obe.ProductionVenue)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If
            
            If OBLib.Security.Settings.CurrentUser.UserID = 152 Then
              With h.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Vision View and Graphics", True)
                    .FlatBlockTag.AddClass("flat-block-paged")
                    With .AboveContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionListCriteria.TxDateFrom, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionListCriteria.TxDateTo, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
                              .AddClass("form-control input-sm")
                              .Attributes("placeholder") = "Search for Productions..."
                              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                              .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductions.DelayedRefreshList() }")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "Refresh", , , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, "ViewModel.ROProductionListPagingManager().Refresh()")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .ContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 7, 7, 7)
                          With .Helpers.Bootstrap.FlatBlock("Productions", False)
                            .FlatBlockTag.AddClass("flat-block-paged")
                            With .ContentTag
                              With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) ViewModel.ROProductionListPagingManager,
                                                                                           Function(d) ViewModel.ROProductionList,
                                                                                           False, False, False, False, True, True, False, ,
                                                                                           Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                           "ROProductions.OnItemSelected", False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y"
                                .Pager.PagerListTag.ListTag.AddClass("pull-left")
                                With .FirstRow
                                  .AddClass("items")
                                  .AddReadOnlyColumn(Function(prod As OBLib.Productions.ReadOnly.ROProduction) prod.Title)
                                  .AddReadOnlyColumn(Function(prod As OBLib.Productions.ReadOnly.ROProduction) prod.PlayStartDateTime)
                                  .AddReadOnlyColumn(Function(prod As OBLib.Productions.ReadOnly.ROProduction) prod.PlayEndDateTime)
                                  .AddReadOnlyColumn(Function(prod As OBLib.Productions.ReadOnly.ROProduction) prod.ProductionDescription)
                                  With .AddColumn("")
                                    With .Helpers.HTMLTag("span")
                                      .AddBinding(KnockoutBindingString.css, "SupplierCss($data,'Graphics')")
                                      .AddBinding(KnockoutBindingString.html, "GraphicsSupplier($data)")
                                      .AddBinding(KnockoutBindingString.visible, "!isGraphics($data)")
                                    End With
                                    With .Helpers.HTMLTag("span")
                                      .AddBinding(KnockoutBindingString.css, "SupplierCss($data,'OB')")
                                      .AddBinding(KnockoutBindingString.html, "OBSupplier($data)")
                                      .AddBinding(KnockoutBindingString.visible, "!isOB($data)")
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                          With .Helpers.Bootstrap.FlatBlock("Selected Productions", True)
                            .FlatBlockTag.AddClass("flat-block-paged")
                            With .AboveContentTag
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 4, 4, 4)
                                  With .Helpers.Bootstrap.Button("", "Clear Productions", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , " fa-refresh", , PostBackType.None, "ClearProductions()")
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.ROProductionListPagingManager().SelectedItems().length > 0")
                                    '.Button.AddClass("btn-block")
                                  End With
                                End With
                              End With
                            End With
                            With .ContentTag
                              With .Helpers.Bootstrap.TableFor(Of Singular.Web.Data.SelectedItem)("ViewModel.ROProductionListPagingManager().SelectedItems()", False, False, False, False, True, True, True)
                                With .FirstRow
                                  .AddClass("items")
                                  .AddReadOnlyColumn(Function(prod As Singular.Web.Data.SelectedItem) prod.Description)
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 2, 2, 2)
                          With .Helpers.Bootstrap.FlatBlock("Suppliers", False)
                            .FlatBlockTag.AddClass("flat-block-paged")
                            With .ContentTag
                              With .Helpers.ForEachTemplate(Of OBLib.Productions.ContentOB.Readonly.ROGraphicsSupplier)(Function(vm) ViewModel.CurrentGraphicsSupplierList)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  With .Helpers.Bootstrap.StateButton(Function(c) c.SelectedInd(), "", "", "btn-success", "btn-default", , , )
                                    .Button.AddBinding(KnockoutBindingString.click, "AddSupplier($data, $data.SupplierID(), $data.Supplier())")
                                    .Button.AddBinding(KnockoutBindingString.enable, "ViewModel.ROProductionListPagingManager().SelectedItems().length > 0")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Supplier)
                                    .Button.AddClass("btn-block")
                                  End With
                                End With
                              End With
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                With .Helpers.Bootstrap.Button("", "Update Productions", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , " fa-plus", , PostBackType.None, "GenerateProductionUpdate()")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(vm) ViewModel.AddSupplierList.Count > 0)
                                  .Button.AddClass("btn-block")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If
            
            With h.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Most Booked Personnel", True)
                  .FlatBlockTag.AddClass("flat-block-paged")
                  With .AboveContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.Button(, "Criteria", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-filter", , PostBackType.None, "ShowMostBookedPersonnelCriteria()")
                    
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Dialog("MostBookedPersonnelModal", "Criteria")
                      .ModalDialogDiv.AddClass("modal-md")
                      With .Body
                        .AddClass("row")
                        With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.MostBookedPersonnelCriteria.StartDate, BootstrapEnums.InputSize.Small)
                    
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.MostBookedPersonnelCriteria.EndDate, BootstrapEnums.InputSize.Small)
                    
                            End With
                          End With
                        End With
                        'With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.MostBookedPersonnelCriteria.DisciplineID, "Area")
                        '      .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                        '    End With
                        '  End With
                        'End With
                        'With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.TodaysRoomSchedulesCriteria.SystemID, "Sub-Dept")
                    
                        '    End With
                        '  End With
                        'End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.MostBookedPersonnelCriteria.DisciplineID, "Discipline")
                              '.Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.MostBookedPersonnelCriteria.ContractTypeID, "Area")
                              '.Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "Refresh", , , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, "ViewModel.MostBookedPersonnelPagingManager().Refresh()")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .ContentTag
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.PagedGridFor(Of ROMostBookedPersonnel)(Function(vm) ViewModel.MostBookedPersonnelPagingManager, Function(vm) ViewModel.MostBookedPersonnel, False, False,
                                                                                     False, False, True, True, False, "MostBookedPersonnel", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn(Function(d As ROMostBookedPersonnel) d.HumanResource)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                            .AddClass("col-xs-4 col-sm-4 col-md-3 col-lg-3 text-left")
                          End With
                          With .AddColumn(Function(d As ROMostBookedPersonnel) d.ContractType)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                            .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-2 text-left")
                          End With
                          With .AddColumn(Function(d As ROMostBookedPersonnel) d.Discipline)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                            .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-2 text-left")
                          End With
                          If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent _
                            And OBLib.Security.Settings.CurrentUser.ProductionAreaID = OBLib.CommonData.Enums.ProductionArea.OB Then
                            With .AddColumn(Function(d As ROMostBookedPersonnel) d.DaysBooked)
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                              .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right")
                            End With
                            With .AddColumn(Function(d As ROMostBookedPersonnel) d.ContractDays)
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                              .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right")
                            End With
                            With .AddColumn("Percentage")
                              .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right")
                              With .Helpers.DivC("progress")
                                With .Helpers.Div
                                  .AddBinding(Singular.Web.KnockoutBindingString.css, "GetContractDaysWorkedCss($data)")
                                  .AddBinding(Singular.Web.KnockoutBindingString.style, "GetContractDaysWorkedStyle($data)")
                                  .AddBinding(Singular.Web.KnockoutBindingString.html, "GetContractDaysWorkedHtml($data)")
                                End With
                              End With
                            End With
                          ElseIf OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices _
                               And OBLib.Security.Settings.CurrentUser.ProductionAreaID = OBLib.CommonData.Enums.ProductionArea.OB Then
                            With .AddColumn(Function(d As ROMostBookedPersonnel) d.HoursBooked)
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                              .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right")
                            End With
                            With .AddColumn("Percentage")
                              .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right")
                              With .Helpers.DivC("progress")
                                With .Helpers.Div
                                  .AddBinding(Singular.Web.KnockoutBindingString.css, "GetHoursWorkedCss($data)")
                                  .AddBinding(Singular.Web.KnockoutBindingString.style, "GetHoursWorkedStyle($data)")
                                  .AddBinding(Singular.Web.KnockoutBindingString.html, "GetHoursWorkedHtml($data)")
                                End With
                              End With
                            End With
                          End If
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            
            With h.Bootstrap.Dialog("ProductionsBudgetedModal", "New Production Budgeted")
              .ModalDialogDiv.AddClass("modal-lg")
              .Attributes("id") = "ProdModal"
              With .Body
                .Attributes("id") = "ProdDetails"
                With .Helpers.With(Of OBLib.Productions.Budgeting.ProductionBudgeted)(Function(vm) ViewModel.CurrentBudgetedProduction)
                  With .Helpers.Bootstrap.FlatBlock("", False)
                    With .HeaderTag
                      With .Helpers.Bootstrap.Button("SaveProductionBudgetedBtn", "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-save", , PostBackType.None, "SaveProductionBudgeted($data)")
                        .Button.AddBinding(KnockoutBindingString.click, "SaveProductionBudgeted($data)")
                      End With
                    End With
                    With .ContentTag
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!IsBudgetedProductionValid()")
                        .AddBinding(Singular.Web.KnockoutBindingString.html, "GetBudgetedProductionBrokenRulesHTML()")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        With .Helpers.Bootstrap.FlatBlock("Production Details", False)
                          .HeaderContainer.Attributes("id") = "ProductionHeader"
                          With .ContentTag
                            With .Helpers.Bootstrap.Row
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.ProductionType)
                              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ProductionBudgeted) d.ProductionType, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.Title)
                              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ProductionBudgeted) d.Title, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.TeamsPlaying)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionBudgeted) d.TeamsPlaying, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.PlayStartDateTime)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionBudgeted) d.PlayStartDateTime, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.PlayEndDateTime)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionBudgeted) d.PlayEndDateTime, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.EventType)
                              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ProductionBudgeted) d.EventType, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.ProductionVenue)
                              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ProductionBudgeted) d.ProductionVenue, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                              .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.ProductionDescription)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionBudgeted) d.ProductionDescription, BootstrapEnums.InputSize.Small, )
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        With .Helpers.Bootstrap.FlatBlock("Booking Details", True)
                          .HeaderContainer.Attributes("id") = "BookingHeader"
                          With .ContentTag
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 6, 4, 4)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  With .Helpers.Bootstrap.StateButton(Function(c) c.BudgetedInd, "Budgeted", "Unbudgeted", "btn-success", "btn-danger", "", "", )
                                    .Button.AddBinding(KnockoutBindingString.enable, "ProductionBudgetedBO.CanEdit($data, 'BudgetedInd')")
                                    .Button.AddClass("btn-block")
                                    .Button.Attributes("id") = "btnBudgeted"
                                  End With
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 6, 4, 4)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  With .Helpers.Bootstrap.StateButton(Function(c) c.RingFencedInd, "Ring-Fenced", "Not Ring-Fenced", "btn-success", "btn-danger", "", "", )
                                    .Button.AddBinding(KnockoutBindingString.enable, "ProductionBudgetedBO.CanEdit($data, 'RingFencedInd')")
                                    .Button.AddClass("btn-block")
                                    .Button.Attributes("id") = "btnRingFenced"
                                  End With
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 6, 4, 4)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  With .Helpers.Bootstrap.StateButton(Function(c) c.RecoupedInd, "Recouped", "Not Recouped", "btn-success", "btn-danger", "", "", )
                                    .Button.AddBinding(KnockoutBindingString.enable, "ProductionBudgetedBO.CanEdit($data, 'RecoupedInd')")
                                    .Button.AddClass("btn-block")
                                    .Button.Attributes("id") = "btnRecouped"
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionBudgeted) d.DebtorID, "FindBudgetedProductionDebtor($element)",
                                                                           "Search For Debtor", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                                    .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "FindBudgetedProductionDebtor($element)")
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 6, 6, 6)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  .Helpers.LabelFor(Function(d) d.TotalRecoupedAmount)
                                  With .Helpers.EditorFor(Function(d) d.TotalRecoupedAmount)
                                    .AddClass("form-control")
                                    .Attributes("placeholder") = "Total Recouped"
                                  End With
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 6, 6, 6)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  .Helpers.LabelFor(Function(d) d.RealCostAmount)
                                  With .Helpers.EditorFor(Function(c) c.RealCostAmount)
                                    .AddClass("form-control")
                                    .Attributes("placeholder") = "Real Cost of Production"
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  With .Helpers.EditorFor(Function(d) d.NoRecoupReason)
                                    .AddClass("form-control")
                                    .Attributes("placeholder") = "Reason for no Recoupment"
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                  .Helpers.LabelFor(Function(d As OBLib.Productions.Budgeting.ProductionBudgeted) d.ProductionQuotes)
                                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d) d.ProductionQuotes, BootstrapEnums.InputSize.Small, )
                                    .AddClass("form-control")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  ' End With
                End With
              End With
            End With
      
            With h.Bootstrap.Dialog("EventsImportedSinceLastLogin", "")
              .ModalDialogDiv.AddClass("modal-lg")
              With .Body
                .AddClass("row")
                With .Helpers.Div
                  With .Helpers.Bootstrap.FDTableBlock("Events Scheduled Since Last Login")
                    .FlatBlockTag.AddClass("flat-block-paged")
                    With .AboveTableContentTag
                      .AddClass("form-horizontal")
                      With .Helpers.Bootstrap.Column(12, 3, 2, 2)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          '.Helpers.LabelFor(Function(d) ViewModel.EventsImportedSinceLastLoginCriteria.Keyword)
                          With .Helpers.EditorFor(Function(d) ViewModel.EventsImportedSinceLastLoginCriteria.Keyword)
                            .AddClass("form-control input-sm")
                            .Attributes("placeholder") = "Keyword search"
                          End With
                        End With
                      End With
                    End With
                    With .TableResponsiveTag
                      With .Helpers.Bootstrap.PagedGridFor(Of ROImportedEvent)(Function(vm) ViewModel.EventsImportedSinceLastLoginPagingManager, Function(vm) ViewModel.EventsImportedSinceLastLogin, False, False,
                                                                                False, False, True, True, False, "EventsImportedSinceLastLoginNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn(Function(d As ROImportedEvent) d.Title)
                            .AddClass("col-xs-4 col-sm-4 col-md-3 col-lg-2 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.GenreDesc)
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.SeriesTitle)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.Location)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.Venue)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.ProductionAreaStatus)
                            .AddClass("col-xs-4 col-sm-4 col-md-1 col-lg-1 text-left")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            
            With h.Bootstrap.Dialog("EventsImportedSinceLastLoginSA", "")
              .ModalDialogDiv.AddClass("modal-lg")
              With .Body
                .AddClass("row")
                With .Helpers.Div
                  With .Helpers.Bootstrap.FDTableBlock("Events Scheduled Since Last Login (SA)")
                    .FlatBlockTag.AddClass("flat-block-paged")
                    With .AboveTableContentTag
                      .AddClass("form-horizontal")
                      With .Helpers.Bootstrap.Column(12, 3, 2, 2)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          '.Helpers.LabelFor(Function(d) ViewModel.EventsImportedSinceLastLoginCriteria.Keyword)
                          With .Helpers.EditorFor(Function(d) ViewModel.EventsImportedSinceLastLoginSACriteria.Keyword)
                            .AddClass("form-control input-sm")
                            .Attributes("placeholder") = "Keyword search"
                          End With
                        End With
                      End With
                    End With
                    With .TableResponsiveTag
                      With .Helpers.Bootstrap.PagedGridFor(Of ROImportedEvent)(Function(vm) ViewModel.EventsImportedSinceLastLoginSAPagingManager, Function(vm) ViewModel.EventsImportedSinceLastLoginSA, False, False,
                                                                                False, False, True, True, False, "EventsImportedSinceLastLoginNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn(Function(d As ROImportedEvent) d.Title)
                            .AddClass("col-xs-4 col-sm-4 col-md-3 col-lg-2 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.GenreDesc)
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.SeriesTitle)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.Location)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.Venue)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROImportedEvent) d.ProductionAreaStatus)
                            .AddClass("col-xs-4 col-sm-4 col-md-1 col-lg-1 text-left")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
      
            With h.Bootstrap.Dialog("ProductionsToBeReconciled", "")
              .ModalDialogDiv.AddClass("modal-lg")
              With .Body
                .AddClass("row")
                With .Helpers.Div
                  With .Helpers.Bootstrap.FDTableBlock("Productions To Be Reconciled")
                    .FlatBlockTag.AddClass("flat-block-paged")
                    With .AboveTableContentTag
                      .AddClass("form-horizontal")
                      With .Helpers.Bootstrap.Column(12, 3, 2, 2)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          '.Helpers.LabelFor(Function(d) ViewModel.EventsImportedSinceLastLoginCriteria.Keyword)
                          With .Helpers.EditorFor(Function(d) ViewModel.ProductionsToBeReconciledCriteria.Keyword)
                            .AddClass("form-control input-sm")
                            .Attributes("placeholder") = "Keyword search"
                          End With
                        End With
                      End With
                    End With
                    With .TableResponsiveTag
                      With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ViewModel.ProductionsToBeReconciledPagingManager, Function(vm) ViewModel.ProductionsToBeReconciled, False, False,
                                                                                False, False, True, True, False, "EventsToBeReconciledNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y"
                        .Pager.PagerListTag.ListTag.AddClass("pull-left")
                        With .FirstRow
                          .AddClass("items")
                          With .AddColumn(Function(d As ROProduction) d.ProductionRefNo)
                            .AddClass("col-xs-4 col-sm-4 col-md-1 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROProduction) d.ProductionDescription)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROProduction) d.ProductionAreaStatus)
                            .AddClass("col-xs-4 col-sm-4 col-md-1 col-lg-1 text-left")
                            'With .Helpers.Bootstrap.Label
                            '  .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.css, "ROProdStatusCss($data)")
                            '  .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.html, "ROProdStatusHtml($data)")
                            'End With
                          End With
                          With .AddColumn(Function(d As ROProduction) d.ProductionVenue)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                          With .AddColumn(Function(d As ROProduction) d.Room)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .AddClass("col-xs-2 col-sm-2 col-md-2 col-lg-1 text-left")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
      
            'With h.Bootstrap.Dialog("ProductionUpdatesSinceLastLogin", "")
            '  .ModalDialogDiv.AddClass("modal-lg")
            '  With .Body
            '    .AddClass("row")
            '    With .Helpers.Div
            '      With .Helpers.Bootstrap.FlatBlock("Production Updates Since Last Login")
            '        .FlatBlockTag.AddClass("flat-block-paged")
            '        With .ContentTag
            '          With .Helpers.DivC("table-resposive")
            '            With .Helpers.Bootstrap.PagedGridFor(Of ROProductionSynergyChange)(Function(vm) ViewModel.ProductionUpdatesSinceLastLoginPagingManager, Function(vm) ViewModel.ProductionUpdatesSinceLastLogin, False, False,
            '                                                                    False, False, True, True, False, "ProductionUpdatesSinceLastLoginNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
            '              .AddClass("no-border hover list")
            '              .TableBodyClass = "no-border-y"
            '              .Pager.PagerListTag.ListTag.AddClass("pull-left")
            '              With .FirstRow
            '                .AddClass("items")
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.GenRefNumber)
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.ProductionRefNo)
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                End With
            '                'With .AddColumn(Function(d As ROProductionSynergyChange) d.ProductionID)
            '                '  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                'End With
            '                'With .AddColumn(Function(d As ROProductionSynergyChange) d.Genre)
            '                '  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
            '                '  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                'End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.SeriesTitle)
            '                  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.Title)
            '                  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.SynergyChangeType)
            '                  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.DetectedDateTime)
            '                  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-center")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.ChannelName)
            '                  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
            '                  .AddClass("col-xs-4 col-sm-4 col-md-2 col-lg-1 text-left")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.PreviousValue)
            '                  .AddClass("col-xs-4 col-sm-4 col-md-3 col-lg-2 text-left")
            '                End With
            '                With .AddColumn(Function(d As ROProductionSynergyChange) d.NewValue)
            '                  .AddClass("col-xs-4 col-sm-4 col-md-3 col-lg-2 text-left")
            '                End With
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
            
            With h.Bootstrap.Dialog("RODebtorListModal", "Select Debtor")
              .ModalDialogDiv.AddClass("modal-med")
              With .Body
                .AddClass("row")
                With .Helpers.Bootstrap.FlatBlock("", True)
                  With .AboveContentTag
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.EditorFor(Function(d) ViewModel.RODebtorListCriteria.Keyword)
                          .AddClass("form-control input-sm")
                          .Attributes("placeholder") = "Search for Debtor"
                          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RODebtors.DelayedRefreshList() }")
                        End With
                      End With
                    End With
                  End With
                  With .ContentTag
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Quoting.ReadOnly.RODebtor)(Function(d) ViewModel.RODebtorListManager,
                                                                                                     Function(d) ViewModel.RODebtorList,
                                                                                                     False, False, False, False, True, True, True,
                                                                                                     , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                     "RODebtors.OnItemSelected", False)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y"
                      .Pager.PagerListTag.ListTag.AddClass("pull-left")
                      With .FirstRow
                        .AddClass("items")
                        With .AddReadOnlyColumn(Function(c As OBLib.Quoting.ReadOnly.RODebtor) c.Debtor)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

          End Using%>
</asp:Content>
