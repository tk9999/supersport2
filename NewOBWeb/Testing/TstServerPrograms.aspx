﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="TstServerPrograms.aspx.vb" Inherits="NewOBWeb.TstServerPrograms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <% Using h = Helpers
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("SmsScheduler", "Sms Scheduler", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-mobile", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        .Helpers.EditorFor(Function(d) d.ImportStartDate)
        .Helpers.EditorFor(Function(d) d.ImporteEndDate)
        '.Helpers.EditorFor(Function(d) d.sy)
        With .Helpers.Bootstrap.Button("SynergyScheduler", "Synergy Scheduler", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-mobile", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("SynergyScheduler2", "Synergy Scheduler 2", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-mobile", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        .Helpers.EditorFor(Function(d) d.ImportStartDate)
        .Helpers.EditorFor(Function(d) d.ImporteEndDate)
        With .Helpers.Bootstrap.Button("EquipmentService", "Equipment Service", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-mobile", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("InvoiceChecks", "Invoice Checks", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("PlayoutChecks", "Playout Checks", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("PlayoutWeeklyChecks", "Playout Weekly Checks", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("StudioChecks", "Studio Checks", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("UnloadAppDomain", "Unload AppDomain", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("WebService", "Web Service", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("SynergyScheduler3", "Synergy Scheduler Nightly", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-mobile", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Button("EmailSender", "Email Sender", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-mail", ,
                                       Singular.Web.PostBackType.Ajax, )
        End With
      End With
      'With h.Bootstrap.Row
      '  With .Helpers.Bootstrap.Button("Biometrics", "Biometrics", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
      '                                 Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
      '                                 Singular.Web.PostBackType.Ajax, )
      '  End With
      'End With
      'With h.Bootstrap.Row
      '  With .Helpers.Bootstrap.Button("Escalate Feed", "Escalate Feed", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
      '                                 Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
      '                                 Singular.Web.PostBackType.Ajax, )
      '  End With
      'End With

    End Using%>
</asp:Content>
