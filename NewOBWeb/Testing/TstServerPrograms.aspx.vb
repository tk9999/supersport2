﻿Imports Sober.Wcf
Imports OBLib.MealReimbursements
Imports Singular.DataAnnotations

Public Class TstServerPrograms
  Inherits OBPageBase(Of TstServerProgramsVM)

End Class

Public Class TstServerProgramsVM
  Inherits OBViewModel(Of TstServerProgramsVM)

  Public Property Count As Integer = 0
  Public Property DateNow As DateTime = Now

  <DateField>
  Public Property ImportStartDate As DateTime?
  <DateField>
  Public Property ImporteEndDate As DateTime?

  Protected Overrides Sub Setup()
    MyBase.Setup()
    'Dim ws As OBLib.SatOps.Integration.ROEquipmentScheduleICRList = OBLib.SatOps.Integration.ROEquipmentScheduleICRList.GetROEquipmentScheduleICRList(Nothing, Nothing, Nothing, Nothing)
    'Dim msc As OBLib.Maintenance.Misc = OBLib.Maintenance.MiscList.GetMiscList().FirstOrDefault
    'Dim MailCred As Singular.Emails.SingularMailSettings.MailCredential = New Singular.Emails.SingularMailSettings.MailCredential With {.FromServer = msc.MailServer,
    '                                                                                                                                    .FriendlyFrom = msc.MailFriendlyFrom,
    '                                                                                                                                    .FromAccount = msc.MailFromAccount,
    '                                                                                                                                    .FromAddress = msc.MailFromAddress,
    '                                                                                                                                    .FromPassword = msc.MailFromPassword}
    'Dim x As New OBScheduler.Service.Schedules.EmailScheduler(MailCred)
    'x.Test()

    'Dim x As New OBScheduler.Service.Schedules.SynergyScheduler()
    'x.Test()

    ' OBLib.Synergy.Importer.[New].SynergyImporter.FetchDataUAT()

    'Dim MonthList = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList()

    'Dim OpenMonth = MonthList.GetOpenMonth()

    'Dim CanCloseFromDate = CDate(OpenMonth.MonthEndDate).AddDays(2)

    'If False AndAlso Now.Date < CanCloseFromDate Then
    '  Throw New FaultException(String.Format("Cannot close {0} before {1}", CDate(OpenMonth.MonthEndDate).ToString("MMM-yy"), CanCloseFromDate.ToString("dd-MMM-yy")))
    'End If

    ' load the open months data
    'Dim list = ROMealReimbursementList.GetROMealReimbursementList(False)

    'Return ReturnList

    'Dim s As New SOBERBiometricsImport.Service.CentralSecurityImport()
    's.TimeUp()

    'Dim anviz As New SOBERBiometricsImport.Service.AnvizImport

    'anviz.TimeUp()

    'Dim ai As New Sober.Wcf.SoberWcfService
    'ai.GetOpenMonthMealReimbursements(False)
    'Dim ai As New SOBERBiometricsImport.Service.Main
    'SOBERBiometricsImport.Service.Main.SyncData()
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)
    Select Case Command
      Case "SmsScheduler"
        SmsScheduler(CommandArgs)
      Case "SynergyScheduler"
        OBLib.Synergy.Importer.[New].SynergyImporter.ScheduledImport(ImportStartDate, ImporteEndDate, Nothing)
      Case "SynergyScheduler2"
        Dim x As New OBScheduler.Service.Schedules.SynergyScheduler()
        x.Test()
      Case "InvoiceChecks"
        InvoiceChecks(CommandArgs)
      Case "EquipmentService"
        'Case "EscalateFeed"
        '  EscalateFeed()
      Case "PlayoutChecks"
        PlayoutChecks(CommandArgs)
      Case "PlayoutWeeklyChecks"
        PlayoutWeeklyChecks(CommandArgs)
      Case "StudioChecks"
        StudioChecks(CommandArgs)
      Case "UnloadAppDomain"
        UnloadAppDomain(CommandArgs)
        'Case "WebService"
        '  WebService(CommandArgs)
        '  'Case "Biometrics"
        '  Biometrics(CommandArgs)
      Case "SynergyScheduler3"
        Dim x As New OBScheduler.Service.Schedules.SynergySchedulerNightly()
        x.Test()
      Case "PlayoutWeeklyChecks"
        Dim x As New OBScheduler.Service.Schedules.PlayoutWeeklyChecks()
        x.Test()
      Case "EmailSender"
        Dim msc As OBLib.Maintenance.Misc = OBLib.Maintenance.MiscList.GetMiscList().FirstOrDefault
        Dim MailCred As Singular.Emails.SingularMailSettings.MailCredential = New Singular.Emails.SingularMailSettings.MailCredential With {.FromServer = msc.MailServer,
                                                                                                                                       .FriendlyFrom = msc.MailFriendlyFrom,
                                                                                                                                       .FromAccount = msc.MailFromAccount,
                                                                                                                                       .FromAddress = msc.MailFromAddress,
                                                                                                                                       .FromPassword = msc.MailFromPassword}
        Dim zzz As New OBScheduler.Service.Schedules.EmailScheduler(MailCred)
        zzz.Test()
    End Select
  End Sub

  Private Sub SmsScheduler(CommandArgs As Singular.Web.CommandArgs)

    Dim msc As OBLib.Maintenance.Misc = OBLib.Maintenance.MiscList.GetMiscList().FirstOrDefault
    Dim MailCred As Singular.Emails.SingularMailSettings.MailCredential = New Singular.Emails.SingularMailSettings.MailCredential With {.FromServer = msc.MailServer,
                                                                                                                                        .FriendlyFrom = msc.MailFriendlyFrom,
                                                                                                                                        .FromAccount = msc.MailFromAccount,
                                                                                                                                        .FromAddress = msc.MailFromAddress,
                                                                                                                                        .FromPassword = msc.MailFromPassword}

    Dim smsCred As Singular.SmsSending.ClickatellSettings = New Singular.SmsSending.ClickatellSettings With {.ApiID = msc.SmSApiID,
                                                                                                             .Password = msc.SMSPassword,
                                                                                                             .UserName = msc.SMSUsername,
                                                                                                             .CallbackStatusDetailLevel = Singular.SmsSending.ClickatellCallbackStatusDetailLevel.FinalAndErrorStatuses}

    Dim x As New OBScheduler.Service.Schedules.SmsScheduleCustom(13, smsCred)
    x.Test()

  End Sub

  Private Sub InvoiceChecks(CommandArgs As Singular.Web.CommandArgs)

    Dim x As New OBScheduler.Service.Schedules.InvoiceChecks()
    x.Test()

  End Sub

  Private Sub PlayoutChecks(CommandArgs As Singular.Web.CommandArgs)

    Dim x As New OBScheduler.Service.Schedules.PlayoutChecks()
    x.Test()

  End Sub

  Private Sub PlayoutWeeklyChecks(CommandArgs As Singular.Web.CommandArgs)

    Dim x As New OBScheduler.Service.Schedules.PlayoutWeeklyChecks()
    x.Test()

  End Sub

  Private Sub StudioChecks(CommandArgs As Singular.Web.CommandArgs)

    Dim x As New OBScheduler.Service.Schedules.ProductionServicesStudioChecks()
    x.Test()

  End Sub

  Private Sub UnloadAppDomain(CommandArgs As Singular.Web.CommandArgs)

    System.Web.HttpRuntime.UnloadAppDomain()

    '// Use an ArrayList to transfer objects to the client.
    'ArrayList arrayOfApplicationBags = new ArrayList();

    'ServerManager serverManager = new ServerManager();
    'ApplicationPoolCollection applicationPoolCollection = serverManager.ApplicationPools;
    'foreach (ApplicationPool applicationPool in applicationPoolCollection)
    '{
    '    PropertyBag applicationPoolBag = new PropertyBag();
    '    applicationPoolBag[ServerManagerDemoGlobals.ApplicationPoolArray] = applicationPool;
    '    arrayOfApplicationBags.Add(applicationPoolBag);
    '    // If the applicationPool is stopped, restart it.
    '    if (applicationPool.State == ObjectState.Stopped)
    '    {
    '        applicationPool.Start();
    '    }

    '}

    '// CommitChanges to persist the changes to the ApplicationHost.config.
    'serverManager.CommitChanges();
    'return arrayOfApplicationBags;

  End Sub

  'Private Sub WebService(CommandArgs As Singular.Web.CommandArgs)

  '  Dim c As New ServiceReference1.SoberWcfServiceClient()
  '  c.GetSatOpsBookings(Now, Now, Nothing, "96A7668E-9C3D-48D5-BEFB-ECAC17265F46")
  '  Dim x As Object = Nothing

  'End Sub

  'Private Sub Biometrics(CommandArgs As Singular.Web.CommandArgs)

  '  Dim x As New SOBERBiometricsImport.Service.CentralSecurityImport()
  '  x.TimeUp()

  'End Sub

End Class