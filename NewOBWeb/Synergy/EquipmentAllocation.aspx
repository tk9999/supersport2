﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="EquipmentAllocation.aspx.vb" Inherits="NewOBWeb.EquipmentAllocation" %>

<%@ Import Namespace="OBLib.Equipment" %>

<%@ Import Namespace="Singular.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    td input {
      height: 20px !important;
      /*width: 100px !important;*/
    }

    .ValidationPopup {
      min-height: 100px;
    }

    #ResultHistory {
      padding: 8px 12px;
      cursor: pointer;
    }

    div.btn-channel-select {
      width: 80px;
      max-width: 80px;
      text-overflow: ellipsis;
      text-align: left;
    }

    div.channel-select-panel {
      display: none;
      width: 1024px;
      z-index: 2000;
    }

    button.Find {
      display: none;
    }

    div.circuit-dropdown {
      min-width: 300px !important;
    }
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Synergy.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/EquipmentAllocatorPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">

    ROChannelBO.IsSelectedSet = function (self) {

      if (self.IsSelected()) {
        var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
        if (!item) {
          self.ChannelID()
          var ni = new SelectedItemObject()
          ni.ID(self.ChannelID())
          ni.Description(self.ChannelShortName())
          ViewModel.SelectedChannelItems().push(ni)
        }
      }
      else {
        var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
        if (item) {
          ViewModel.SelectedChannelItems().pop(item)
        }
      }

      var xmlCriteria = ""
      if (ViewModel.SelectedChannelItems().length > 0) {
        xmlCriteria = '<DataSet>'
        ViewModel.SelectedChannelItems().Iterate(function (item, indx) {
          xmlCriteria += "<Table ID='" + item.ID().toString() + "' />";
        })
        xmlCriteria += '</DataSet>'
      }

      ViewModel.EquipmentAllocationCriteria().SelectedChannelIDsXML(xmlCriteria)

    }

    EquipmentAllocatorListCriteriaBO = {
      SystemIDSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().SystemID(self.SystemID())
      },
      ProductionAreaIDSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().ProductionAreaID(self.ProductionAreaID())
      },
      StartDateSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().StartDate(self.StartDate())
      },
      EndDateSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().EndDate(self.EndDate())
      },
      LiveSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().Live(self.Live())
      },
      DelayedSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().Delayed(self.Delayed())
      },
      PremierSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().Premier(self.Premier())
      },
      GenRefNoSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().GenRefNo(self.GenRefNo())
      },
      SelectedChannelIDsXMLSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().SelectedChannelIDsXML(self.SelectedChannelIDsXML())
      },
      GenreSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().Genre(self.Genre())
      },
      SeriesSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().Series(self.Series())
      },
      TitleSet: function (self) {
        ViewModel.EquipmentAllocationCriteriaByVenue().Title(self.Title())
      },
      PrimaryChannelsOnlySet: function (self) {
        ViewModel.ROChannelList().Iterate(function (ch, chInd) {
          if (self.PrimaryChannelsOnly()) {
            if (ch.PrimaryInd()) { if (!ch.IsSelected()) { ch.IsSelected(true) } }
            else {
              ch.IsSelected(false)
            }
          } else {
            ch.IsSelected(false)
          }
        })
      },
      AllChannelsSet: function (self) {
        if (!ViewModel.FreezeSetExpressions()) {
          ViewModel.FreezeSetExpressions(true)
          self.PrimaryChannelsOnly(false)
          ViewModel.ROChannelList().Iterate(function (ch, chInd) {
            if (self.AllChannels()) {
              ch.IsSelected(true)
            } else {
              ch.IsSelected(false)
            }
          })
          ViewModel.FreezeSetExpressions(false)
        }
      }
    };

    Singular.OnPageLoad(function () {
      OBMisc.UI.activateTab("ByEvent")
    })

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.Bootstrap.Row
        With .Helpers.DivC("pull-right")
          With .Helpers.Toolbar
            .Helpers.MessageHolder()
          End With
        End With
      End With
      
      With h.Bootstrap.Row
        
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", )
            With .AddTab("ByEvent", "", "", "By Event", , )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small,
                                                   , "fa-refresh", , Singular.Web.PostBackType.None, "EquipmentAllocatorPage.Refresh()")
                    End With
                    With .Helpers.Bootstrap.Button(, "Filters", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small,
                                                   , "fa-filter", , Singular.Web.PostBackType.None, "EquipmentAllocatorPage.showFilters()")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Equipment.EquipmentAllocator)("ViewModel.EquipmentAllocationManager",
                                                                                                "ViewModel.EquipmentAllocationList",
                                                                                                False, False, False, False, True, True, True, ,
                                                                                                Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x"
                      With .FirstRow
                        .AddClass("items selectable")
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocator) d.GenRefDisplay)
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocator) d.GenreSeriesDisplay)
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocator) d.TitleDisplay)
                        '.AddReadOnlyColumn(Function(c As OBLib.Equipment.EquipmentAllocator) c.GenreSeriesDisplay)
                        With .AddColumn("Channel")
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ButtonStyleCssClass()")
                          End With
                        End With
                        With .AddColumn("Status")
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.EventStatus()")
                          End With
                        End With
                        With .AddColumn(Function(d As OBLib.Equipment.EquipmentAllocator) d.EquipmentID)
                        End With
                        With .AddColumn(Function(d As OBLib.Equipment.EquipmentAllocator) d.FeedTypeID)
                        End With
                        With .AddColumn("Booking Times")
                          With .Helpers.Bootstrap.Button(, , , , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-clock-o", , , "EquipmentAllocatorPage.showBookingTimes($data, $element)")
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Equipment.EquipmentAllocator) d.BookingTimes)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "EquipmentAllocatorBO.bookingTimesButtonCSS($data)")
                            '.Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "")
                          End With
                        End With
                        With .AddColumn("")
                          '.Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , Singular.Web.PostBackType.None, "EquipmentAllocatorBO.editTimes($data, $element)", False)
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                         "fa-cloud-upload", , Singular.Web.PostBackType.None, "EquipmentAllocatorBO.sendRequest($data)", False)
                            .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "EquipmentAllocatorBO.IconCss($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "EquipmentAllocatorBO.canUpload($data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .AddTab("ByVenue", "", "", "By Venue", , )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small,
                                                   , "fa-refresh", , Singular.Web.PostBackType.None, "EquipmentAllocatorPage.Refresh()")
                    End With
                    With .Helpers.Bootstrap.Button(, "Filters", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small,
                                                   , "fa-filter", , Singular.Web.PostBackType.None, "EquipmentAllocatorPage.showFilters()")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Equipment.EquipmentAllocatorVenue)("ViewModel.EquipmentAllocationManagerByVenue",
                                                                                                     "ViewModel.EquipmentAllocationListByVenue",
                                                                                                     False, False, False, False, True, True, True, ,
                                                                                                     Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                     "", False, True)
                      .AddClass("no-border hover list parent-table")
                      .TableBodyClass = "no-border-y no-border-x"
                      With .FirstRow
                        .AddClass("items selectable")
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.VenueDayString)
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.Venue)
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.Location)
                        .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.EventCount)
                        With .AddColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.EquipmentID)
                          .HeaderStyle.Width = "100px"
                          .Style.Width = "100px"
                        End With
                        With .AddColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.FeedTypeID)
                          .HeaderStyle.Width = "100px"
                          .Style.Width = "100px"
                        End With
                        .AddColumn(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.FeedName)
                        With .AddColumn("Start Buffer")
                          .HeaderStyle.Width = "140px"
                          .Style.Width = "140px"
                          .Helpers.EditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.StartBuffer).Style.Width = "90px"
                          .Helpers.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.StartBuffer).Style.Width = "50px"
                        End With
                        With .AddColumn("On Air Start")
                          .HeaderStyle.Width = "140px"
                          .Style.Width = "140px"
                          .Helpers.EditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.FirstEventStart).Style.Width = "90px"
                          .Helpers.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.FirstEventStart).Style.Width = "50px"
                        End With
                        With .AddColumn("On Air End")
                          .HeaderStyle.Width = "140px"
                          .Style.Width = "140px"
                          .Helpers.EditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.LastEventEnd).Style.Width = "90px"
                          .Helpers.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.LastEventEnd).Style.Width = "50px"
                        End With
                        With .AddColumn("End Buffer")
                          .HeaderStyle.Width = "140px"
                          .Style.Width = "140px"
                          .Helpers.EditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.EndBuffer).Style.Width = "90px"
                          .Helpers.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.EndBuffer).Style.Width = "50px"
                        End With
                        With .AddColumn("")
                          '.Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , Singular.Web.PostBackType.None, "EquipmentAllocatorBO.editTimes($data, $element)", False)
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                         "fa-cloud-upload", , Singular.Web.PostBackType.None, "EquipmentAllocatorVenueBO.sendRequest($data)", False)
                            .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "EquipmentAllocatorVenueBO.IconCss($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "EquipmentAllocatorVenueBO.canUpload($data)")
                          End With
                        End With
                      End With
                      With .AddChildTable(Of OBLib.Equipment.EquipmentAllocatorEvent)(Function(d As OBLib.Equipment.EquipmentAllocatorVenue) d.EquipmentAllocatorEventList, False, False, False, False, True, True, True)
                        .AddClass("no-border hover list  parent-table child-table")
                        .TableBodyClass = "no-border-y no-border-x"
                        With .FirstRow
                          .AddClass("items selectable")
                          .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.GenRefNumber)
                          .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.GenreSeries)
                          .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.Title)
                          With .AddColumn("Highlights?")
                            .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.HighlightsInd, "Highlights", "Highlights", "btn-warning", , , , )
                          End With
                          .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.LiveDateString)
                          .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.FirstBroadcastString)
                        End With
                      
                        With .AddChildTable(Of OBLib.Equipment.EquipmentAllocatorEventChannel)(Function(d As OBLib.Equipment.EquipmentAllocatorEvent) d.EquipmentAllocatorEventChannelList, False, False, False, False, True, True, True)
                          .AddClass("no-border hover list child-table")
                          .TableBodyClass = "no-border-y no-border-x"
                          With .FirstRow
                            .AddClass("items selectable")
                            With .AddColumn("Channel")
                              With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ButtonStyleCssClass()")
                              End With
                            End With
                            
                            .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEventChannel) d.StartDateString)
                            .AddReadOnlyColumn(Function(d As OBLib.Equipment.EquipmentAllocatorEventChannel) d.EndDateString)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        
      End With
      
      With h.Bootstrap.Dialog("BookingTimesModal", "Booking Times", , , Singular.Web.BootstrapEnums.Style.Primary, , "fa-clock-o", , )
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Equipment.EquipmentAllocator)("EquipmentAllocatorPage.currentEquipmentAllocator()")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.EquipmentID)
                  .Style.Width = "100%"
                  .Style.Padding(, , , "10px")
                End With
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.EquipmentID, Singular.Web.BootstrapEnums.InputSize.Small, , "Circuit")
                  .Editor.AddClass("form-control form-control-sm")
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.CallTime)
                  .Style.Width = "100%"
                  .Style.Padding(, , , "10px")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.OnAirStartDateTime)
                  .Style.Width = "100%"
                  .Style.Padding(, , , "10px")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.OnAirStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.OnAirStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.OnAirStartDateTime)
                  .Style.Width = "100%"
                  .Style.Padding(, , , "10px")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.OnAirEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.OnAirEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.WrapTime)
                  .Style.Width = "100%"
                  .Style.Padding(, , , "10px")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Equipment.EquipmentAllocator) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Equipment.EquipmentAllocator)("EquipmentAllocatorPage.currentEquipmentAllocator()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Create Booking", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                             "fa-cloud-upload", , Singular.Web.PostBackType.None, "EquipmentAllocatorBO.sendRequest($data)", False)
                .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "EquipmentAllocatorBO.IconCss($data)")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "EquipmentAllocatorBO.canUpload($data)")
              End With
              'With .Helpers.Bootstrap.Button(, "Done", Singular.Web.BootstrapEnums.Style.Primary, ,
              '                                Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-thumbs-o-up", ,
              '                                Singular.Web.PostBackType.None, "EquipmentAllocatorPage.doneEditingTimes($data, $element)", )
              '  '.Button.AddClass("btn-block")
              'End With
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("Filters", "Filters", , , Singular.Web.BootstrapEnums.Style.Primary, , "fa-filter", , True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Equipment.EquipmentAllocatorList.Criteria)(Function(vm) ViewModel.EquipmentAllocationCriteria)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.StartDate).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocatorList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.EndDate).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocatorList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.GenRefNo).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.GenRefNo, Singular.Web.BootstrapEnums.InputSize.Small, , "Gen Ref")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Genre).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Genre, Singular.Web.BootstrapEnums.InputSize.Small, , "Genre")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Series).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Series, Singular.Web.BootstrapEnums.InputSize.Small, , "Series")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelDisplay("Broadcast Status").Style.Width = "100%"
                  With .Helpers.Div
                    .Helpers.Bootstrap.StateButton(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Live, "Live", "Live", "btn-danger", , , , "btn-sm")
                    .Helpers.Bootstrap.StateButton(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Delayed, "Delayed", "Delayed", "btn-success", , , , "btn-sm")
                    .Helpers.Bootstrap.StateButton(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Premier, "Premier", "Premier", "btn-warning", , , , "btn-sm")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Apply Filters", BootstrapEnums.Style.Primary,
                                                 , BootstrapEnums.ButtonSize.Small, ,
                                                 "fa-download", , PostBackType.Ajax, "EquipmentAllocatorPage.Refresh()")
                    .Button.AddClass("btn-block")
                    .Button.Style.Height = "100%"
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      
      'With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
      '  With .Helpers.Bootstrap.FlatBlock("Filters", , , , )
      '    With .ContentTag
      '      With .Helpers.With(Of OBLib.Equipment.EquipmentAllocatorList.Criteria)(Function(vm) ViewModel.EquipmentAllocationCriteria)
      '        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '          .Helpers.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.SystemID).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocatorList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
      '            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
      '          End With
      '          .Helpers.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.ProductionAreaID).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocatorList.Criteria) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
      '            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
      '          End With
      '          .Helpers.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.StartDate).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocatorList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
      '          End With
      '          .Helpers.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.EndDate).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.EquipmentAllocatorList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
      '          End With
      '          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.GenRefNo).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.GenRefNo, Singular.Web.BootstrapEnums.InputSize.Small, , "Gen Ref")
      '          End With
      '          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Genre).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Genre, Singular.Web.BootstrapEnums.InputSize.Small, , "Genre")
      '          End With
      '          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Series).Style.Width = "100%"
      '          With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Series, Singular.Web.BootstrapEnums.InputSize.Small, , "Series")
      '          End With
      '          .Helpers.Bootstrap.LabelDisplay("Broadcast Status").Style.Width = "100%"
      '          With .Helpers.Div
      '            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Live, "Live", "Live", "btn-danger", , , , "btn-sm")
      '            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Delayed, "Delayed", "Delayed", "btn-success", , , , "btn-sm")
      '            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Equipment.EquipmentAllocatorList.Criteria) p.Premier, "Premier", "Premier", "btn-warning", , , , "btn-sm")
      '          End With
      '        End With
      '        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '          With .Helpers.Bootstrap.Button(, "Apply Filters", BootstrapEnums.Style.Primary,
      '                                         , BootstrapEnums.ButtonSize.Small, ,
      '                                         "fa-download", , PostBackType.Ajax, "EquipmentAllocatorPage.Refresh()")
      '            .Button.AddClass("btn-block")
      '            .Button.Style.Height = "100%"
      '            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
