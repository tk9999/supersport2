﻿Imports Singular.DataAnnotations
Imports Singular.Web.Data
Imports OBLib.Rooms
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Security
Imports OBLib.Synergy
Imports OBLib.Synergy.ReadOnly

Public Class RoomAllocation
  Inherits OBPageBase(Of RoomAllocatorVM)

End Class

Public Class RoomAllocatorVM
  Inherits OBViewModelStateless(Of RoomAllocatorVM)

  <InitialDataOnly>
  Public Property RoomAllocatorList As OBLib.Rooms.RoomAllocatorList = New OBLib.Rooms.RoomAllocatorList
  <InitialDataOnly>
  Public Property RoomAllocatorCriteria As OBLib.Rooms.RoomAllocatorList.Criteria = New OBLib.Rooms.RoomAllocatorList.Criteria
  <InitialDataOnly>
  Public Property RoomAllocatorListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorVM)(Function(d) Me.RoomAllocatorList,
                                                                                                                                                                  Function(d) Me.RoomAllocatorCriteria,
                                                                                                                                                                  "ScheduleDateTime", 5)
  <InitialDataOnly>
  Public Property ROChannelList As OBLib.Maintenance.ReadOnly.ROChannelList

  <InitialDataOnly>
  Public Property AllocationResults As New List(Of Singular.Web.Result)
  <InitialDataOnly>
  Public Property LatestResult As Singular.Web.Result
  <InitialDataOnly>
  Public Property HistoryText As String

  Public Property DisableUpload As Boolean

  Public Property SelectedChannelItems As List(Of SelectedItem)

  <InitialDataOnly>
  Public Property AllocatorChannelCriteria As OBLib.Rooms.AllocatorChannelList.Criteria = New OBLib.Rooms.AllocatorChannelList.Criteria
  Public Property CurrentAllocatorChannelEvent As AllocatorChannelEvent
  Public Property CurrentSynergyEvent As SynergyEvent

  Public Property GraphicalView As Boolean

  Public Property FreezeSetExpressions As Boolean

  <InitialDataOnly>
  Public Property UserSystemList As UserSystemList

  Public Property CurrentRoomAllocatorRoomSchedule As OBLib.Rooms.RoomAllocatorRoomSchedule

  Public Property CurrentRoomScheduleArea As OBLib.Scheduling.Rooms.RoomScheduleArea
  Public Property CurrentProduction As OBLib.Productions.Production
  Public Property CurrentAdHocBooking As OBLib.AdHoc.AdHocBooking

  Public Property CurrentSynergyEventGuid As String
  'Public Property CurrentRoomAllocatorGuid As String
  'Public Property CurrentRoomAllocatorRoomScheduleGuid As String

  Public Property RefreshRoomAvailability As Boolean

  Public Property ROSynergyChangePagedList As ROSynergyChangePagedList = New ROSynergyChangePagedList
  Public Property ROSynergyChangePagedListCriteria As ROSynergyChangePagedList.Criteria = New ROSynergyChangePagedList.Criteria
  Public Property ROSynergyChangePagedListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorVM)(Function(d) d.ROSynergyChangePagedList,
                                                                                                                                                                         Function(d) d.ROSynergyChangePagedListCriteria,
                                                                                                                                                                         "GenRefNumber", 100, True)

  Protected Overrides Sub Setup()
    MyBase.Setup()

    RoomAllocatorCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    RoomAllocatorCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    RoomAllocatorCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
    RoomAllocatorCriteria.EndDate = Singular.Dates.DateMonthEnd(Now)

    AllocatorChannelCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    AllocatorChannelCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    'AllocatorChannelCriteria.StartDate = Now.AddDays(-3)
    'If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(OBLib.CommonData.Enums.System.ProductionServices, Integer)) Then
    '  AllocatorChannelCriteria.EndDate = New Date(2016, 9, 30)
    'End If

    If OBLib.Security.Settings.CurrentUser.ProductionAreaID = CType(OBLib.CommonData.Enums.ProductionArea.MediaOperationsRandburg, Integer) Then
      GraphicalView = True
    Else
      GraphicalView = False
    End If

    ROChannelList = OBLib.CommonData.Lists.ROChannelList
    For Each ch As ROChannel In ROChannelList
      If ch.PrimaryInd Or {23, 24, 49, 50, 51, 52, 53, 73}.Contains(ch.ChannelID) Then
        ch.SetSelected()
      End If
    Next

    UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

    DisableUpload = False
    HistoryText = ""
    RefreshRoomAvailability = False

    'ClientDataProvider.AddDataSource("RORoomTimelineSettingList", OBLib.CommonData.Lists.RORoomTimelineSettingList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("UserSystemList", UserSystemList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaCallTimeSettingList", OBLib.CommonData.Lists.ROSystemProductionAreaCallTimeSettingList, False)
    ClientDataProvider.AddDataSource("ROSynergyChangeTypeList", OBLib.CommonData.Lists.ROSynergyChangeTypeList, False)

    ClientDataProvider.AddDataSource("ROResourceTypeList", OBLib.CommonData.Lists.ROResourceTypeList, False)
    ClientDataProvider.AddDataSource("RORoomTypeList", OBLib.CommonData.Lists.RORoomTypeList, False)
    ClientDataProvider.AddDataSource("ROAdHocBookingTypeList", OBLib.CommonData.Lists.ROAdHocBookingTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemAreaAdHocBookingTypeList", OBLib.CommonData.Lists.ROSystemAreaAdHocBookingTypeList, False)
    ClientDataProvider.AddDataSource("ROSiteList", OBLib.CommonData.Lists.ROSiteList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaStatusSelectList", OBLib.CommonData.Lists.ROSystemProductionAreaStatusSelectList, False)
    ClientDataProvider.AddDataSource("ROEquipmentTypeList", OBLib.CommonData.Lists.ROEquipmentTypeList, False)
    ClientDataProvider.AddDataSource("RORoomScheduleTypeList", OBLib.CommonData.Lists.RORoomScheduleTypeList, False)
    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("ROCostCentreList", OBLib.CommonData.Lists.ROCostCentreList, False)
    ClientDataProvider.AddDataSource("ROOffReasonList", OBLib.CommonData.Lists.ROOffReasonList, False)
    ClientDataProvider.AddDataSource("ROShiftTypeList", OBLib.CommonData.Lists.ROShiftTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemAreaShiftTypeList", OBLib.CommonData.Lists.ROSystemAreaShiftTypeList, False)
    ClientDataProvider.AddDataSource("ROCountryList", OBLib.CommonData.Lists.ROCountryList, False)
    ClientDataProvider.AddDataSource("RODebtorList", OBLib.CommonData.Lists.RODebtorList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplineList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplineList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplinePositionTypeList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplinePositionTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplinePositionTypeListFlat", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplinePositionTypeListFlat, False)
    ClientDataProvider.AddDataSource("ROFeedTypeList", OBLib.CommonData.Lists.ROFeedTypeList, False)
    ClientDataProvider.AddDataSource("ROIngestTypeList", OBLib.CommonData.Lists.ROIngestSourceTypeList, False)
    ClientDataProvider.AddDataSource("ROAudioSettingList", OBLib.CommonData.Lists.ROAudioSettingList, False)
    ClientDataProvider.AddDataSource("ROStudioSupervisorList", OBLib.CommonData.Lists.ROStudioSupervisorList, False)
    ClientDataProvider.AddDataSource("ROCustomResourceList", OBLib.CommonData.Lists.ROCustomResourceList, False)

    OBLib.CommonData.Lists.Refresh("RORoomList")
    'OBLib.CommonData.Lists.Refresh("ROMCRControllerList")
    'OBLib.CommonData.Lists.Refresh("ROSCCROperatorList")
    ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
    'ClientDataProvider.AddDataSource("ROMCRControllerList", OBLib.CommonData.Lists.ROMCRControllerList, False)
    'ClientDataProvider.AddDataSource("ROSCCROperatorList", OBLib.CommonData.Lists.ROSCCROperatorList, False)

    'Dim roRoomAvailability As OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList = OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList.GetRORoomAvailabilityList(DateTime.Now, DateTime.Now)
    'ClientDataProvider.AddDataSource("RORoomAvailabilityList", roRoomAvailability, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)
  End Sub

End Class