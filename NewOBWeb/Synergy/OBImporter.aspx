﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="OBImporter.aspx.vb" Inherits="NewOBWeb.OBImporter" %>

<%@ Import Namespace="OBLib.Productions.Base" %>

<%@ Import Namespace="OBLib.Slugs" %>

<%@ Import Namespace="OBLib.Rooms" %>

<%@ Import Namespace="Singular.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    td input {
      height: 20px !important;
    }

    /*width: 100px !important;*/

    .ValidationPopup {
      min-height: 100px;
    }

    #ResultHistory {
      padding: 8px 12px;
      cursor: pointer;
    }

    div.btn-channel-select {
      width: 80px;
      max-width: 80px;
      text-overflow: ellipsis;
      text-align: left;
    }

    div.channel-select-panel {
      display: none;
      width: 1024px;
      z-index: 2000;
    }

    .event-live:not(.vis-selected), .btn-event-live {
      color: #000;
      border: solid 1px #fff;
      background: #F97D81;
      background: -moz-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #F97D81), color-stop(100%, #F97D81));
      background: -webkit-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: linear-gradient(#F97D81, #F97D81);
      background: -o-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: -ms-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#F97D81', endColorstr='#F97D81',GradientType=0 );
      cursor: pointer;
    }

    .event-delayed:not(.vis-selected), .btn-event-delayed {
      color: #000;
      border: solid 1px #fff;
      background: #7ABA7A;
      background: -moz-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #7ABA7A), color-stop(100%, #7ABA7A));
      background: -webkit-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: linear-gradient(#7ABA7A, #7ABA7A);
      background: -o-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: -ms-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7ABA7A', endColorstr='#7ABA7A',GradientType=0 );
      cursor: pointer;
    }

    .event-premier:not(.vis-selected), .btn-event-premier {
      color: #000;
      border: solid 1px #fff;
      background: #fdfdaf;
      background: -moz-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fdfdaf), color-stop(100%, #fdfdaf));
      background: -webkit-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: linear-gradient(#fdfdaf, #fdfdaf);
      background: -o-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: -ms-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdfdaf', endColorstr='#fdfdaf',GradientType=0 );
      cursor: pointer;
    }

    .event-default:not(.vis-selected), .btn-event-default {
      color: #000;
      border: solid 1px #fff;
      background: #D3E1E4;
      background: -moz-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #D3E1E4), color-stop(100%, #D3E1E4));
      background: -webkit-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: linear-gradient(#D3E1E4, #D3E1E4);
      background: -o-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: -ms-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#D3E1E4', endColorstr='#D3E1E4',GradientType=0 );
      cursor: pointer;
    }

    #AllocatorChannelEvent {
      visibility: hidden;
      /*width: 400px;
      height: 400px;*/
      z-index: 10000;
      background: #fff;
      padding: 8px;
      border-style: solid;
      border-width: 2px;
      border-color: #B4D8E7;
    }

    button.Find {
      display: none;
    }

    div.room-dropdown {
      min-width: 400px !important;
    }

    div.mcrc-dropdown {
      min-width: 400px !important;
    }

    div.sccro-dropdown {
      min-width: 400px !important;
    }

    /*div.table-responsive {
      width: -webkit-calc(100% - 100px) !important;
    }*/

    table tbody td {
      font-size: 11px;
    }

    #EditTimesPanelSynergyEvent {
      width: 400px;
      height: 400px;
      z-index: 1000;
      background: #fff;
      -webkit-box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      -moz-box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      border: solid 1px gray;
    }
  </style>
  <link href="../Scripts/vis4.10/vis.min.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Synergy.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/OBMisc.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/OBImporterPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
          '.AddClass("hidden-xs hidden-sm hidden-md")
          With .Helpers.With(Of OBLib.Synergy.SynergyEventList.Criteria)(Function(vm) ViewModel.SynergyEventListCriteria)
            With .Helpers.Bootstrap.FlatBlock("Filters", , , , )
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.SystemID).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.SynergyEventList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.StartDate).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.SynergyEventList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.EndDate).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.SynergyEventList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Genre)
                      .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Genre, Singular.Web.BootstrapEnums.InputSize.Small, , "Genre")
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Series)
                      .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Series, Singular.Web.BootstrapEnums.InputSize.Small, , "Series")
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.GenRefNoString)
                      .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.GenRefNoString, Singular.Web.BootstrapEnums.InputSize.Small, , "Gen Ref")
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Title)
                      .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Title, Singular.Web.BootstrapEnums.InputSize.Small, , "Title")
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Keyword)
                      .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Keyword")
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelDisplay("Broadcast Status").Style.Width = "100%"
                      With .Helpers.DivC("btn-group")
                        .Style.Width = "100%"
                        .Helpers.Bootstrap.StateButton(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Live, "Live", "Live", , , , , "btn-xs")
                        .Helpers.Bootstrap.StateButton(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Delayed, "Delayed", "Delayed", , , , , "btn-xs")
                        .Helpers.Bootstrap.StateButton(Function(p As OBLib.Synergy.SynergyEventList.Criteria) p.Premier, "Premier", "Premier", , , , , "btn-xs")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Apply Filters", BootstrapEnums.Style.Primary,
                                                     , BootstrapEnums.ButtonSize.Small, ,
                                                     "fa-download", , PostBackType.None,
                                                     "OBImporterPage.refreshSynergyEvents()")
                        .Button.AddClass("btn-block")
                        '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Create Manually", BootstrapEnums.Style.Danger,
                                                     , BootstrapEnums.ButtonSize.Small, ,
                                                     "fa-frown-o", , PostBackType.None,
                                                     "OBImporterPage.createManually()")
                        .Button.AddClass("btn-block")
                        '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Update Data", Singular.Web.BootstrapEnums.Style.Info, ,
                               Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                               "fa-refresh", , Singular.Web.PostBackType.None, "OBImporterPage.doNewImport()", )
                        .Button.AddClass("btn-block")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Top)
            With .AddTab("GridViewByEvent", "fa-tv", "OBImporterPage.refreshSynergyEvents()", "Grid - By Event", , True)
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Synergy.SynergyEvent)("ViewModel.SynergyEventListManager",
                                                                                        "ViewModel.SynergyEventList",
                                                                                        False, False, False, False, True, True, True, ,
                                                                                        Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                        "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      With .FirstRow
                        .AddClass("items selectable")
                        With .AddColumn("")
                          .Style.Width = "50px"
                          With .Helpers.SpanC("btn btn-xs btn-warning")
                            .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.ProductionRefNo().length > 0")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ProductionRefNo()")
                            '.AddBinding(Singular.Web.KnockoutBindingString.click, "SynergyEventBO.setupOB($data)")
                          End With
                        End With
                        'With .AddReadOnlyColumn(Function(d As OBLib.Synergy.SynergyEvent) d.ProductionRefNo)
                        '  .AddClass("select-column")
                        '  .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                        'End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.SynergyEvent) d.GenRefNumber)
                          .AddClass("select-column")
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.SynergyEvent) d.GenreSeriesDisplay)
                          .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.GenreSeries()")
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.SynergyEvent) d.Title)
                        End With
                        With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.TeamsPlaying)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'TeamsPlaying')")
                        End With
                        With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.ProductionVenueID)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'ProductionVenueID')")
                        End With
                        With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.LiveDate)
                          .Style.Width = "70px"
                          .Editor.Style.Width = "70px"
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'LiveDate')")
                        End With
                        With .AddColumn("Time")
                          .Style.Width = "50px"
                          With .Helpers.TimeEditorFor(Function(d As OBLib.Synergy.SynergyEvent) d.LiveDate)
                            .Style.Width = "50px"
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'LiveDate')")
                          End With
                        End With
                        With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.LiveEndDate)
                          .Style.Width = "70px"
                          .Editor.Style.Width = "70px"
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'LiveEndDate')")
                        End With
                        With .AddColumn("Time")
                          .Style.Width = "50px"
                          With .Helpers.TimeEditorFor(Function(d As OBLib.Synergy.SynergyEvent) d.LiveEndDate)
                            .Style.Width = "50px"
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'LiveEndDate')")
                          End With
                        End With
                        If OBLib.Security.Settings.CurrentUser.SystemID = 1 Then
                          With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.VehicleID)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'VehicleID')")
                          End With
                        End If
                        If OBLib.Security.Settings.CurrentUser.SystemID = 2 Then
                          With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.IsVisionView)
                            .Editor.AddBinding(KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.Bootstrap.StateButtonNew(Function(d) d.IsVisionView,,,,,,,)
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'IsVisionView')")
                            End With
                          End With
                          With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.IsAlstonElliot)
                            .Editor.AddBinding(KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.Bootstrap.StateButtonNew(Function(d) d.IsAlstonElliot,,,,,,,)
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'IsAlstonElliot')")
                            End With
                          End With
                        End If
                        With .AddColumn(Function(d As OBLib.Synergy.SynergyEvent) d.ProductionSpecRequirementID)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.CanEdit($data, 'ProductionSpecRequirementID')")
                        End With
                        With .AddColumn("")
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                         "fa-cloud-upload", , Singular.Web.PostBackType.None, "SynergyEventBO.sendRequestOB($data)", False)
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "SynergyEventBO.buttonHTML($data)")
                            .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "SynergyEventBO.iconCss($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SynergyEventBO.buttonCSS($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyEventBO.canUploadOB($data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.With(Of OBLib.Synergy.SynergyEventList.Criteria)(Function(vm) ViewModel.SynergyEventListCriteria)
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
      End With


      With Helpers.Bootstrap.Dialog("ManualProduction", "Manual Production", , "modal-xs", Singular.Web.BootstrapEnums.Style.Danger, , "fa-frown-o", , True)
        With .ContentDiv

        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Event Details", False, True)
                With .ContentTag
                  With .Helpers.Div
                    .AddBinding(KnockoutBindingString.visibleA, Function(d) Not ViewModel.ManualProductionCreated)
                    With .Helpers.With(Of ManualProduction)(Function(f) ViewModel.ManualProduction)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.Bootstrap.LabelDisplay("Sub-Departments")
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
                              With .Helpers.Bootstrap.StateButton(Function(d As ManualProduction) d.OBCityInd, "Production Services", "Production Services", , , , , "btm-sm")
                              End With
                            End If
                            If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
                              With .Helpers.Bootstrap.StateButton(Function(d As ManualProduction) d.OBContentInd, "Production Content", "Production Content", , , , , "btm-sm")
                              End With
                            End If
                          End With
                        End With
                      End With
                      If Singular.Security.HasAccess("Productions.Can Create Placeholder Production") Then
                        With .Helpers.Bootstrap.Row
                          .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              .Helpers.Bootstrap.LabelDisplay("Is this a placeholder production?").Style.Width = "100%"
                              With .Helpers.Bootstrap.StateButton(Function(d As ManualProduction) d.PlaceHolderInd, "Yes", "No", , , , , "btn-sm")
                              End With
                            End With
                          End With
                        End With
                      End If
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.ProductionTypeID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.ProductionTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.EventTypeID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.EventTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.Title)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.Title, BootstrapEnums.InputSize.Custom, "input-xs")
                              .Editor.Attributes("placeholder") = "Title"
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.TeamsPlaying)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.TeamsPlaying, BootstrapEnums.InputSize.Custom, "input-xs")
                              .Editor.Attributes("placeholder") = "Teams"
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                        .Helpers.Bootstrap.LabelFor(Function(d) d.PlayStartDateTime).AddClass("top-buffer-5 col-xs-12 col-sm-12 col-md-12 col-lg-12")
                        With .Helpers.Bootstrap.Column(7, 7, 7, 7)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.PlayStartDateTime, BootstrapEnums.InputSize.Custom, "input-xs")
                              .Editor.Attributes("placeholder") = "Start Date"
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(5, 5, 5, 5)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.TimeEditorFor(Function(d As ManualProduction) d.PlayStartDateTime)
                              .AddClass("form-control input-xs")
                              .Attributes("placeholder") = "Start Time"
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                        .Helpers.Bootstrap.LabelFor(Function(d) d.PlayEndDateTime).AddClass("top-buffer-5 col-xs-12 col-sm-12 col-md-12 col-lg-12")
                        With .Helpers.Bootstrap.Column(7, 7, 7, 7)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.PlayEndDateTime, BootstrapEnums.InputSize.Custom, "input-xs")
                              .Editor.Attributes("placeholder") = "End Date"
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(5, 5, 5, 5)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.TimeEditorFor(Function(d As ManualProduction) d.PlayEndDateTime)
                              .AddClass("form-control input-xs")
                              .Attributes("placeholder") = "End Time"
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.ProductionVenueID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ManualProduction) d.ProductionVenueID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.visible, "ViewModel.ManualProduction().OBCityInd() || ViewModel.ManualProduction().OBContentInd()")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "Create", BootstrapEnums.Style.Danger, ,
                                                           BootstrapEnums.ButtonSize.Medium, , "fa-frown-o", ,
                                                           PostBackType.None, "OBImporterPage.saveManualProduction($data)")
                              .Button.AddClass("btn-block")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With

                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.visible, Function(d) ViewModel.ManualProductionCreated)
                    With .Helpers.With(Of ManualProduction)(Function(f) ViewModel.ManualProduction)
                      With .Helpers.Div
                        With .Helpers.DivC("text-center")
                          With .Helpers.DivC("i-circle success")
                            .Helpers.Bootstrap.FontAwesomeIcon("fa-check", )
                          End With
                          With .Helpers.HTMLTag("h4")
                            .AddBinding(KnockoutBindingString.html, "$data.ProductionRefNo() + ' created successfully'")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                        .AddClass("col-xs-offset-3 col-sm-offset-3")
                        With .Helpers.Bootstrap.Button(, "Create Another One", BootstrapEnums.Style.Danger, ,
                                                       BootstrapEnums.ButtonSize.Medium, , "fa-frown-o", ,
                                                       PostBackType.None, "OBImporterPage.setupNewManualProduction()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                    End With
                  End With

                End With
              End With
            End With
          End With
        End With
      End With

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
