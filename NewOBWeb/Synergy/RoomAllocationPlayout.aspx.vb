﻿Imports Singular.DataAnnotations
Imports Singular.Web.Data
Imports OBLib.Rooms
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Security
Imports OBLib.Synergy
Imports OBLib.Synergy.ReadOnly

Public Class RoomAllocationPlayout
  Inherits OBPageBase(Of RoomAllocatorPlayoutVM)

End Class

Public Class RoomAllocatorPlayoutVM
  Inherits OBViewModelStateless(Of RoomAllocatorPlayoutVM)

  Public Property SchEvtList As OBLib.Rooms.SchEvtList = New OBLib.Rooms.SchEvtList
  Public Property SchEvtListCriteria As OBLib.Rooms.SchEvtList.Criteria = New OBLib.Rooms.SchEvtList.Criteria
  Public Property SchEvtListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorVM)(Function(d) Me.SchEvtList,
                                                                                                                                                           Function(d) Me.SchEvtListCriteria,
                                                                                                                                                           "ScheduleDateTime", 15)

  Public Property CurrentSchEvtChRm As OBLib.Rooms.RoomSched

  Public Property ROChannelList As OBLib.Maintenance.ReadOnly.ROChannelList
  Public Property SelectedChannels As List(Of Integer)
  Public Property DisableUpload As Boolean
  Public Property SelectedChannelItems As List(Of SelectedItem)
  Public Property CurrentAllocatorChannelEvent As AllocatorChannelEvent

  Public Property AllocatorChannelPlayoutCriteria As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria = New OBLib.Rooms.AllocatorChannelPlayoutList.Criteria
  'Public Property CurrentAllocatorChannelEventPlayout As OBLib.Rooms.AllocatorChannelPlayoutEvent
  'Public Property CurrentSynergyEventChannelPlayoutList As SynergyEventChannelPlayoutList
  'Public Property CurrentSynergyEventPlayout As SynergyEventPlayout
  'Public Property CurrentSynergyEventChannelPlayout As SynergyEventChannelPlayout
  Public Property GraphicalView As Boolean
  Public Property FreezeSetExpressions As Boolean

  <InitialDataOnly>
  Public Property UserSystemList As UserSystemList

  '<InitialDataOnly>
  'Public Property SynergyEventChannelPlayoutList As OBLib.Synergy.SynergyEventChannelPlayoutList = New OBLib.Synergy.SynergyEventChannelPlayoutList
  '<InitialDataOnly>
  'Public Property SynergyEventChannelPlayoutListCriteria As OBLib.Synergy.SynergyEventChannelPlayoutList.Criteria = New OBLib.Synergy.SynergyEventChannelPlayoutList.Criteria
  '<InitialDataOnly>
  'Public Property SynergyEventChannelPlayoutListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM)(Function(d) Me.SynergyEventChannelPlayoutList,
  '                                                                                                                                                                                           Function(d) Me.SynergyEventChannelPlayoutListCriteria,
  '                                                                                                                                                                                           "ScheduleDateTime", 25)

  '<InitialDataOnly>
  'Public Property SynergyEventPlayoutList As OBLib.Synergy.SynergyEventPlayoutList = New OBLib.Synergy.SynergyEventPlayoutList
  '<InitialDataOnly>
  'Public Property SynergyEventPlayoutListCriteria As OBLib.Synergy.SynergyEventPlayoutList.Criteria = New OBLib.Synergy.SynergyEventPlayoutList.Criteria
  '<InitialDataOnly>
  'Public Property SynergyEventPlayoutListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM)(Function(d) Me.SynergyEventPlayoutList,
  '                                                                                                                                                                                    Function(d) Me.SynergyEventPlayoutListCriteria,
  '                                                                                                                                                                                    "ScheduleDateTime", 25)

  '<InitialDataOnly>
  'Public Property SynergySchedulePlayoutList As OBLib.Synergy.SynergySchedulePlayoutList = New OBLib.Synergy.SynergySchedulePlayoutList
  '<InitialDataOnly>
  'Public Property SynergySchedulePlayoutListCriteria As OBLib.Synergy.SynergySchedulePlayoutList.Criteria = New OBLib.Synergy.SynergySchedulePlayoutList.Criteria
  '<InitialDataOnly>
  'Public Property SynergySchedulePlayoutListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM)(Function(d) Me.SynergySchedulePlayoutList,
  '                                                                                                                                                                                        Function(d) Me.SynergySchedulePlayoutListCriteria,
  '                                                                                                                                                                                        "ScheduleDateTime", 25)
  'Public Property CurrentSynergyEventGuid As String
  'Public Property CurrentRoomAllocatorGuid As String
  'Public Property CurrentRoomAllocatorRoomSchedule As OBLib.Rooms.RoomAllocatorRoomSchedule
  'Public Property CurrentRoomScheduleArea As OBLib.Scheduling.Rooms.RoomScheduleArea
  'Public Property CurrentBookingGuid As String
  'Public Property RefreshRoomAvailability As Boolean
  'Public Property CurrentSynergyScheduleGuid As String

  Public Property ROSynergyChangePagedList As ROSynergyChangePagedList = New ROSynergyChangePagedList
  Public Property ROSynergyChangePagedListCriteria As ROSynergyChangePagedList.Criteria = New ROSynergyChangePagedList.Criteria
  Public Property ROSynergyChangePagedListManager As Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM) = New Singular.Web.Data.PagedDataManager(Of RoomAllocatorPlayoutVM)(Function(d) d.ROSynergyChangePagedList,
                                                                                                                                                                                       Function(d) d.ROSynergyChangePagedListCriteria,
                                                                                                                                                                                       "GenRefNumber", 100, True)

  Public Property IsBusyRefreshing As Boolean

  Protected Overrides Sub Setup()
    MyBase.Setup()

    'CurrentSynergyEventChannelPlayout = New SynergyEventChannelPlayout

    SchEvtListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    'SchEvtListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    'SynergyEventChannelPlayoutListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    'SynergyEventChannelPlayoutListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    'SynergyEventPlayoutListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    'SynergyEventPlayoutListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    AllocatorChannelPlayoutCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    AllocatorChannelPlayoutCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    AllocatorChannelPlayoutCriteria.StartDate = Now
    AllocatorChannelPlayoutCriteria.EndDate = Now.AddDays(1)

    SelectedChannels = New List(Of Integer)

    If OBLib.Security.Settings.CurrentUser.ProductionAreaID = CType(OBLib.CommonData.Enums.ProductionArea.MediaOperationsRandburg, Integer) Then
      GraphicalView = True
    Else
      GraphicalView = False
    End If

    ROChannelList = OBLib.Maintenance.ReadOnly.ROChannelList.GetUserDefaultChannels(CurrentUser.UserID)

    UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

    DisableUpload = False
    'RefreshRoomAvailability = False

    ClientDataProvider.AddDataSource("RORoomTimelineSettingList", OBLib.CommonData.Lists.RORoomTimelineSettingList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    'ClientDataProvider.AddDataSource("ROSystemProductionAreaSettingList", OBLib.CommonData.Lists.ROSystemProductionAreaSettingList, False)
    ClientDataProvider.AddDataSource("UserSystemList", UserSystemList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaCallTimeSettingList", OBLib.CommonData.Lists.ROSystemProductionAreaCallTimeSettingList, False)
    ClientDataProvider.AddDataSource("ROSynergyChangeTypeList", OBLib.CommonData.Lists.ROSynergyChangeTypeList, False)
    OBLib.CommonData.Lists.Refresh("RORoomList")
    OBLib.CommonData.Lists.Refresh("ROMCRControllerList")
    OBLib.CommonData.Lists.Refresh("ROSCCROperatorList")
    ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
    ClientDataProvider.AddDataSource("ROMCRControllerList", OBLib.CommonData.Lists.ROMCRControllerList, False)
    ClientDataProvider.AddDataSource("ROSCCROperatorList", OBLib.CommonData.Lists.ROSCCROperatorList, False)
    'ClientDataProvider.AddDataSource("RORoomAvailabilityTimeSlotList", OBLib.CommonData.Lists.RORoomAvailabilityTimeSlotList, False)
    'ClientDataProvider.AddDataSource("RORoomAvailabilityList", OBLib.CommonData.Lists.RORoomAvailabilityList, False)

    'ClientDataProvider.AddDataSource("ROSCCROperatorAvailabilityList", OBLib.CommonData.Lists.ROSCCROperatorAvailabilityList, False)
    'ClientDataProvider.AddDataSource("ROMCROperatorAvailabilityList", OBLib.CommonData.Lists.ROMCROperatorAvailabilityList, False)

  End Sub

  'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
  '  MyBase.HandleCommand(Command, CommandArgs)
  'End Sub

  '<Singular.Web.WebCallable()>
  'Public Function CreateRARS() As Singular.Web.Result
  '  Return New Singular.Web.Result(Function()
  '                                   Return RoomAllocatorRoomSchedule.NewRoomAllocatorRoomSchedule()
  '                                 End Function)
  'End Function

  <Singular.Web.WebCallable>
  Public Function SaveSchEvt(SchEvt As OBLib.Rooms.SchEvt) As Singular.Web.Result
    SchEvt.CheckAll()
    If SchEvt.IsValid Then
      SchEvt.MarkAll()
      Dim sh As Singular.SaveHelper = SchEvt.TrySave(GetType(OBLib.Rooms.SchEvtList))
      Dim wr As Singular.Web.Result = New Singular.Web.SaveResult(sh)
      wr.Data = sh.SavedObject
      Return wr
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = "Errors were found on the room bookings", .Data = SchEvt}
    End If
  End Function



  <Singular.Web.WebCallable>
  Public Function DeleteSchEvt(RoomSched As OBLib.Rooms.RoomSched) As Singular.Web.Result
    Dim SchEvtList As New OBLib.Rooms.RoomSchedList
    SchEvtList.Add(RoomSched)
    RoomSched.DeleteSelfGeneric()
    SchEvtList.Clear()
    Dim sh As Singular.SaveHelper = SchEvtList.TrySave()
    Return New Singular.Web.Result With {.Success = sh.Success,
                                         .ErrorText = sh.ErrorText}
  End Function

  <Singular.Web.WebCallable>
  Public Function DeleteSchEvtChRmHr(SchEvtChRmHr As OBLib.Rooms.SchEvtChRmHr) As Singular.Web.Result
    Dim SchEvtChRmHrList As New OBLib.Rooms.SchEvtChRmHrList
    SchEvtChRmHrList.Add(SchEvtChRmHr)
    SchEvtChRmHrList.Clear()
    Dim sh As Singular.SaveHelper = SchEvtChRmHrList.TrySave()
    Return New Singular.Web.Result With {.Success = sh.Success,
                                         .ErrorText = sh.ErrorText}
  End Function

  <Singular.Web.WebCallable>
  Public Function UpdateRmClashes(SchEvtChRm As OBLib.Rooms.RoomSched) As Singular.Web.Result
    SchEvtChRm.UpdateClashes()
    SchEvtChRm.UpdateCrewClashes()
    Return New Singular.Web.Result(True) With {.Data = SchEvtChRm}
  End Function

End Class