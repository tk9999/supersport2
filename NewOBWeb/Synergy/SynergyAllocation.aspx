﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="SynergyAllocation.aspx.vb" Inherits="NewOBWeb.SynergyAllocation" %>

<%@ Import Namespace="OBLib.Slugs" %>

<%@ Import Namespace="OBLib.Rooms" %>

<%@ Import Namespace="Singular.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Styles/daterangepicker-bs3.css" rel="stylesheet" />
  <link type="text/css" href="../Styles/controls.css" rel="stylesheet" />
  <link href="synergyAllocation.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/OBMisc.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Synergy.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/contextMenu.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/controls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/synergyAllocator.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">

    Singular.OnPageLoad(function () {
      window.allocator = new allocator({ container: "allocator", includeEmptyChannels: false })
    })

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.Div
        .Attributes("id") = "allocator"
      End With

      h.Control(New NewOBWeb.Controls.BySeriesModal(Of NewOBWeb.SynergyAllocationVM)())
      h.Control(New NewOBWeb.Controls.ByEventModal(Of NewOBWeb.SynergyAllocationVM)())
      h.Control(New NewOBWeb.Controls.RoomScheduleControl(Of NewOBWeb.SynergyAllocationVM)())

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
