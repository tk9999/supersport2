﻿Imports Singular.DataAnnotations
Imports Singular.Web.Data
Imports OBLib.Equipment.ReadOnly

Public Class EquipmentAllocation
  Inherits OBPageBase(Of EquipmentAllocationVM)

End Class

Public Class EquipmentAllocationVM
  Inherits OBViewModelStateless(Of EquipmentAllocationVM)

  <InitialDataOnly>
  Public Property EquipmentAllocationList As OBLib.Equipment.EquipmentAllocatorList = New OBLib.Equipment.EquipmentAllocatorList
  <InitialDataOnly>
  Public Property EquipmentAllocationCriteria As OBLib.Equipment.EquipmentAllocatorList.Criteria = New OBLib.Equipment.EquipmentAllocatorList.Criteria
  <InitialDataOnly>
  Public Property EquipmentAllocationManager As Singular.Web.Data.PagedDataManager(Of EquipmentAllocationVM) = New Singular.Web.Data.PagedDataManager(Of EquipmentAllocationVM)(Function(d) Me.EquipmentAllocationList,
                                                                                                                                                                                Function(d) Me.EquipmentAllocationCriteria,
                                                                                                                                                                                "ScheduleDateTime", 25)
  <InitialDataOnly>
  Public Property EquipmentAllocationListByVenue As OBLib.Equipment.EquipmentAllocatorVenueList = New OBLib.Equipment.EquipmentAllocatorVenueList
  <InitialDataOnly>
  Public Property EquipmentAllocationCriteriaByVenue As OBLib.Equipment.EquipmentAllocatorVenueList.Criteria = New OBLib.Equipment.EquipmentAllocatorVenueList.Criteria
  <InitialDataOnly>
  Public Property EquipmentAllocationManagerByVenue As Singular.Web.Data.PagedDataManager(Of EquipmentAllocationVM) = New Singular.Web.Data.PagedDataManager(Of EquipmentAllocationVM)(Function(d) Me.EquipmentAllocationListByVenue,
                                                                                                                                                                                       Function(d) Me.EquipmentAllocationCriteriaByVenue,
                                                                                                                                                                                       "ScheduleDateTime", 25)
  <InitialDataOnly>
  Public Property ROChannelList As OBLib.Maintenance.ReadOnly.ROChannelList

  Public Property SelectedChannelItems As List(Of SelectedItem)

  <InitialDataOnly>
  Public Property AllocationResults As New List(Of Singular.Web.Result)
  <InitialDataOnly>
  Public Property LatestResult As Singular.Web.Result
  <InitialDataOnly>
  Public Property HistoryText As String

  Public Property DisableUpload As Boolean

  Public Property FreezeSetExpressions As Boolean

  Public Property CurrentEventGuid As String

  <InitialDataOnly>
  Public Property ROCircuitAvailabilityListCriteria As ROCircuitAvailabilityList.Criteria

  Protected Overrides Sub Setup()
    MyBase.Setup()

    EquipmentAllocationCriteria.SystemID = OBLib.CommonData.Enums.System.ProductionContent
    EquipmentAllocationCriteria.ProductionAreaID = OBLib.CommonData.Enums.ProductionArea.SatOps
    EquipmentAllocationCriteria.StartDate = Now
    EquipmentAllocationCriteria.EndDate = Now.AddDays(14)

    EquipmentAllocationCriteriaByVenue.SystemID = OBLib.CommonData.Enums.System.ProductionContent
    EquipmentAllocationCriteriaByVenue.ProductionAreaID = OBLib.CommonData.Enums.ProductionArea.SatOps
    EquipmentAllocationCriteriaByVenue.StartDate = Now
    EquipmentAllocationCriteriaByVenue.EndDate = Now.AddDays(14)

    ROCircuitAvailabilityListCriteria = New ROCircuitAvailabilityList.Criteria

    ROChannelList = OBLib.CommonData.Lists.ROChannelList

    DisableUpload = False
    HistoryText = ""

    'ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)
    'ClientDataProvider.AddDataSource("RORoomTimelineSettingList", OBLib.CommonData.Lists.RORoomTimelineSettingList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROFeedTypeList", OBLib.CommonData.Lists.ROFeedTypeList, False)
    ClientDataProvider.AddDataSource("ROSatOpsEquipmentList", OBLib.CommonData.Lists.ROSatOpsEquipmentList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)
  End Sub

End Class