﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="RoomAllocation.aspx.vb" Inherits="NewOBWeb.RoomAllocation" %>

<%@ Import Namespace="OBLib.Slugs" %>

<%@ Import Namespace="OBLib.Rooms" %>

<%@ Import Namespace="Singular.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    td input {
      height: 20px !important;
    }

    .btn.bold {
      font-weight: 700;
    }

    .btn.black {
      color: #000;
    }

    .ValidationPopup {
      min-height: 100px;
    }

    #ResultHistory {
      padding: 8px 12px;
      cursor: pointer;
    }

    div.btn-channel-select {
      width: 80px;
      max-width: 80px;
      text-overflow: ellipsis;
      text-align: left;
    }

    div.channel-select-panel {
      display: none;
      width: 1024px;
      z-index: 2000;
    }

    .event-live:not(.vis-selected), .btn-event-live {
      color: #000;
      border: solid 1px #fff;
      background: #F97D81;
      background: -moz-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #F97D81), color-stop(100%, #F97D81));
      background: -webkit-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: linear-gradient(#F97D81, #F97D81);
      background: -o-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: -ms-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#F97D81', endColorstr='#F97D81',GradientType=0 );
      cursor: pointer;
    }

    .event-delayed:not(.vis-selected), .btn-event-delayed {
      color: #000;
      border: solid 1px #fff;
      background: #7ABA7A;
      background: -moz-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #7ABA7A), color-stop(100%, #7ABA7A));
      background: -webkit-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: linear-gradient(#7ABA7A, #7ABA7A);
      background: -o-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: -ms-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7ABA7A', endColorstr='#7ABA7A',GradientType=0 );
      cursor: pointer;
    }

    .event-premier:not(.vis-selected), .btn-event-premier {
      color: #000;
      border: solid 1px #fff;
      background: #fdfdaf;
      background: -moz-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fdfdaf), color-stop(100%, #fdfdaf));
      background: -webkit-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: linear-gradient(#fdfdaf, #fdfdaf);
      background: -o-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: -ms-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdfdaf', endColorstr='#fdfdaf',GradientType=0 );
      cursor: pointer;
    }

    .event-default:not(.vis-selected), .btn-event-default {
      color: #000;
      border: solid 1px #fff;
      background: #D3E1E4;
      background: -moz-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #D3E1E4), color-stop(100%, #D3E1E4));
      background: -webkit-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: linear-gradient(#D3E1E4, #D3E1E4);
      background: -o-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: -ms-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#D3E1E4', endColorstr='#D3E1E4',GradientType=0 );
      cursor: pointer;
    }

    #AllocatorChannelEvent {
      visibility: hidden;
      /*width: 400px;
      height: 400px;*/
      z-index: 10000;
      background: #fff;
      padding: 8px;
      border-style: solid;
      border-width: 2px;
      border-color: #B4D8E7;
    }

    button.Find {
      display: none;
    }

    div.room-dropdown {
      min-width: 400px !important;
    }

    div.mcrc-dropdown {
      min-width: 400px !important;
    }

    div.sccro-dropdown {
      min-width: 400px !important;
    }

    /*div.table-responsive {
      width: -webkit-calc(100% - 100px) !important;
    }*/

    table tbody td {
      font-size: 11px;
    }

    #EditTimesPanelSynergyEvent {
      width: 400px;
      height: 400px;
      z-index: 1000;
      background: #fff;
      -webkit-box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      -moz-box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      border: solid 1px gray;
    }

    span.bold {
      font-weight: 700;
    }

    span.semi-bold {
      font-weight: 400;
    }

  </style>
  <link href="../Scripts/vis4.10/vis.min.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/Tools/SoberControls.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResourceShifts.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Slugs.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Synergy.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.10/vis.custom.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/OBMisc.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceHelpers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/RoomAllocation.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
          .AddClass("hidden-xs hidden-sm hidden-md")
          With .Helpers.Bootstrap.FlatBlock("General Filters", , , , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  .Helpers.Control(New NewOBWeb.Controls.AllocatorChannelListCriteriaControl(Of NewOBWeb.RoomAllocatorVM)())
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  .Helpers.Bootstrap.LabelDisplay("Channels")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.ForEach(Of OBLib.Maintenance.ReadOnly.ROChannel)("ViewModel.ROChannelList()")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.StateDiv("IsSelected", , , , , , , "btn-xs btn-channel-select", )
                          .ButtonText.AddBinding(KnockoutBindingString.html, "$data.ChannelShortName()")
                          .Button.AddBinding(KnockoutBindingString.attr, "{ title: $data.ChannelName() }")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "AllocatorChannelListCriteriaBO.channelSelected($data)")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Top)
            With .AddTab("GridViewByChannel", "fa-th", "RoomAllocatorPage.refreshGridByChannel()", "Grid - By Channel", , )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  '  With .Helpers.Bootstrap.Button(, "Filters", Singular.Web.BootstrapEnums.Style.Info, ,
                  '                                 Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-filter", ,
                  '                                 Singular.Web.PostBackType.None, "RoomAllocatorPage.showFilters()")
                  '  End With
                  'End With
                  With .Helpers.With(Of OBLib.Rooms.AllocatorChannelList.Criteria)(Function(vm) ViewModel.AllocatorChannelCriteria)
                    With .Helpers.Bootstrap.Column(12, 6, 3, 2, 1)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.StartDate).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 6, 3, 2, 1)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.EndDate).Style.Width = "100%"
                        If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(OBLib.CommonData.Enums.System.ProductionServices, Integer)) Then
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.AllocatorChannelList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        Else
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End If
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 4, 4, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Keyword)
                        .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Keyword")
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 4, 8)
                      With .Helpers.Bootstrap.PullRight
                        With .Helpers.HTMLTag("h3")
                          .AddBinding(Singular.Web.KnockoutBindingString.html, "RoomAllocatorPage.selectedDatesHTML($data)")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Rooms.RoomAllocator)("ViewModel.RoomAllocatorListManager",
                                                                        "ViewModel.RoomAllocatorList",
                                                                        False, False, False, False, True, True, True, ,
                                                                        Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                        "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      With .FirstRow
                        .AddClass("items selectable")
                        With .AddColumn("Channel")
                          .Style.Width = "60px"
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ButtonStyleCssClass()")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.ChannelButtonVisible()")
                          End With
                        End With
                        With .AddColumn("Spon?")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Rooms.RoomAllocator) d.Sponsored, "S", "S", "btn-danger", , "", "", )
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocator) d.BookingTimes)
                          .FieldDisplay.AddClass("bold")
                        End With
                        With .AddColumn("Status")
                          .Style.Width = "60px"
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.EventStatus()")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorBO.getEventStatusCssClass($data)")
                          End With
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocator) d.GenRefDisplay)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocator) d.ScheduleNumberDisplay)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocator) d.GenreSeriesDisplay)
                          .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.GenreSeries()")
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocator) d.TitleDisplay)
                          .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.Title()")
                        End With
                        'If Not OBLib.Security.Settings.CurrentUser.IsSingleAreaUser Then
                        '  With .AddColumn(Function(d As OBLib.Rooms.RoomAllocator) d.ProductionAreaID)
                        '  End With
                        'End If
                        With .AddColumn(Function(d As OBLib.Rooms.RoomAllocator) d.RoomID)
                        End With
                        'With .AddColumn("")
                        '  With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                        '                                 "fa-cloud-upload", , Singular.Web.PostBackType.None, "RoomAllocatorBO.sendRequest($data)", False)
                        '    .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "RoomAllocatorBO.buttonHTML($data)")
                        '    .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorBO.iconCss($data)")
                        '    .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorBO.buttonCSS($data)")
                        '    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomAllocatorBO.canUpload($data)")
                        '  End With
                        'End With
                        With .AddColumn("Bookings")
                          With .Helpers.ForEach(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("$data.RoomAllocatorRoomScheduleList()")
                            With .Helpers.HTMLTag("button")
                              .Attributes("type") = "button"
                              .AddClass("btn btn-xs btn-default")
                              'When Busy
                              With .Helpers.HTMLTag("span")
                                .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsProcessing()")
                                With .Helpers.HTMLTag("i")
                                  .AddClass("fa fa-2x fa-spin fa-refresh icon-info")
                                End With
                              End With
                              'Standard View
                              With .Helpers.HTMLTag("span")
                                .AddBinding(Singular.Web.KnockoutBindingString.visible, "(!$data.IsProcessing() && ($data.DeleteRoomSchedule() == false))")
                                '.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorRoomScheduleBO.buttonCss($data)")
                                With .Helpers.HTMLTag("span")
                                  .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.Room()")
                                  .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomAllocatorRoomScheduleBO.getRoomScheduleArea($data)")
                                End With
                                With .Helpers.HTMLTag("i")
                                  .AddClass("fa fa-2x fa-trash icon-danger")
                                  .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomAllocatorRoomScheduleBO.removeItem($data, $parent)")
                                  .Attributes("title") = "Remove Booking"
                                End With
                              End With
                              'Delete RoomSchedule
                              With .Helpers.HTMLTag("span")
                                .AddBinding(Singular.Web.KnockoutBindingString.visible, "(!$data.IsProcessing() && ($data.DeleteRoomSchedule() == true))")
                                With .Helpers.HTMLTag("span")
                                  With .Helpers.HTMLTag("span")
                                    With .Helpers.HTMLTag("i")
                                      .AddClass("fa fa-2x fa-check-square-o icon-success")
                                      .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomAllocatorRoomScheduleBO.confirmDelete($data, $parent)")
                                    End With
                                    With .Helpers.HTMLTag("span")
                                      .AddBinding(Singular.Web.KnockoutBindingString.html, "'Confirm'")
                                    End With
                                  End With
                                End With
                                With .Helpers.HTMLTag("span")
                                  With .Helpers.HTMLTag("span")
                                    With .Helpers.HTMLTag("i")
                                      .AddClass("fa fa-2x fa-trash icon-danger")
                                      .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomAllocatorRoomScheduleBO.cancelDelete($data)")
                                    End With
                                  End With
                                  With .Helpers.HTMLTag("span")
                                    .AddBinding(Singular.Web.KnockoutBindingString.html, "'Cancel'")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                        'With .AddColumn("")
                        '  With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                        '                                 "fa-cloud-upload", , Singular.Web.PostBackType.None, "RoomAllocatorBO.sendRequest($data)", False)
                        '    .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorBO.IconCss($data)")
                        '    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomAllocatorBO.canUpload($data)")
                        '  End With
                        'End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .AddTab("SynergyChanges", "fa-bitcoin", "", "Change Log", , )
              With .TabPane
                With .Helpers.With(Of OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria)("ViewModel.ROSynergyChangePagedListCriteria()")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.GenRefNumber)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.GenRefNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ScheduleNumber)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ScheduleNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeStartDateTime)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeEndDateTime)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.SynergyChangeTypeID)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.SynergyChangeTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ColumnName)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ColumnName, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Synergy.ReadOnly.ROSynergyChangePaged)("ViewModel.ROSynergyChangePagedListManager",
                                                                                                         "ViewModel.ROSynergyChangePagedList",
                                                                                                         False, False, False, False, True, True, True, ,
                                                                                                         Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                         "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      With .FirstRow
                        .AddClass("items selectable")
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.GenRefNumber)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ScheduleNumber)
                        End With
                        With .AddColumn("")
                          .Style.Width = "50px"
                          '.FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                          With .Helpers.Span
                            .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ChangeTypeCss()")
                            With .Helpers.HTMLTag("i")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ChangeTypeIconName()")
                            End With
                            With .Helpers.Span
                              .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChangeTypeString()")
                            End With
                          End With
                        End With
                        'With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ChangeTypeString)
                        'End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ChangeDateTimeString)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ChangeDescription)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ColumnName)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.PreviousValue)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.NewValue)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      h.Control(New NewOBWeb.Controls.RoomScheduleControl(Of NewOBWeb.RoomAllocatorVM)())
      
      Dim ModalSize As String = ""
      If OBLib.Security.Settings.CurrentUser.SystemID = 1 Then
        ModalSize = "modal-xxs"
      Else
        ModalSize = "modal-md"
      End If
      
      Dim ColSize As Integer = 12
      If OBLib.Security.Settings.CurrentUser.SystemID = 1 Then
        ColSize = 12
      Else
        ColSize = 3
      End If
      
      With h.Bootstrap.Dialog("RoomAllocatorRoomScheduleModal", "Room Booking", , ModalSize, Singular.Web.BootstrapEnums.Style.Primary, , "fa-home", , )
        .Heading.AddBinding(Singular.Web.KnockoutBindingString.html, "RoomAllocatorPage.currentRoomScheduleTitle()")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, ColSize, ColSize, ColSize)
              With .Helpers.Bootstrap.FlatBlock("Booking Info", , , , )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("ViewModel.CurrentRoomAllocatorRoomSchedule()")
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.Title).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.Title, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.RoomID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.RoomID, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.CallTime)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.StartDateTime)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.EndDateTime)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.WrapTime)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Wrap Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Wrap Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.OwnerComments).Style.Width = "100%"
                            With .Helpers.Div
                              With .Helpers.TextBoxFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.OwnerComments)
                                .MultiLine = True
                                .Style.Height = "240px"
                                .AddClass("form-control room-schedule-comments")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            If OBLib.Security.Settings.CurrentUser.SystemID = 2 Then
              With .Helpers.Bootstrap.Column(12, 12, 9, 9, 9)
                With .Helpers.With(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("ViewModel.CurrentRoomAllocatorRoomSchedule()")
                  With .Helpers.Bootstrap.FlatBlock("Channels", , , , )
                    With .ContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 21, 12, 12)
                          With .Helpers.Bootstrap.TableFor(Of OBLib.Rooms.RoomAllocatorRoomScheduleChannel)("$data.RoomAllocatorRoomScheduleChannelList()",
                                                                                                            False, False, False, False, True, True, True,
                                                                                                            "RoomAllocatorRoomScheduleChannelList")
                            .AllowClientSideSorting = True
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            With .FirstRow
                              '.AddBinding(KnockoutBindingString.visible, "$data.IsOtherRoomSchedule()")
                              .AddClass("items selectable")
                              With .AddColumn("")
                                .Style.Width = "50px"
                                With .Helpers.Bootstrap.StateButton(Function(d) d.IsSelected, "Selected", "Select", , , , , )
                                  .Button.AddBinding(KnockoutBindingString.enable, "$data.CanEdit()")
                                End With
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.ScheduleNumber)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.ChannelShortName)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.StatusCode)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.ScheduledTimes)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.OtherRoom)
                              End With
                              'With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.OtherRoomScheduleTitle)
                              'End With
                              With .AddColumn("Removed From Synergy?")
                                With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.RemovedFromSynergy, , , "btn-danger", , "fa-frown-o", "fa-smile-o", )
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("ViewModel.CurrentRoomAllocatorRoomSchedule()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Create Booking", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                             "fa-cloud-upload", , Singular.Web.PostBackType.None, "RoomAllocatorPage.saveCurrentRoomAllocatorRoomSchedule($data)", False)
                .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorRoomScheduleBO.iconCss($data)")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomAllocatorRoomScheduleBO.canSave($data)")
                .Button.AddClass("btn-block")
              End With
            End With
          End With
        End With
      End With

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
