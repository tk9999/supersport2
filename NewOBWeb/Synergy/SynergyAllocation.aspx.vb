﻿Imports Singular.DataAnnotations
Imports Singular.Web.Data
Imports OBLib.Rooms
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Security
Imports OBLib.Synergy
Imports OBLib.Synergy.ReadOnly

Public Class SynergyAllocation
  Inherits OBPageBase(Of SynergyAllocationVM)

End Class

Public Class SynergyAllocationVM
  Inherits OBViewModelStateless(Of SynergyAllocationVM)

  Public Property CurrentSeriesItem As OBLib.Synergy.SeriesItem
  Public Property CurrentByEventTemplate As OBLib.Synergy.ByEventTemplate

  Public Property CurrentRoomScheduleArea As OBLib.Scheduling.Rooms.RoomScheduleArea
  Public Property CurrentProduction As OBLib.Productions.Production
  Public Property CurrentAdHocBooking As OBLib.AdHoc.AdHocBooking
  Public Property UserSystemList As UserSystemList
  Public Property ROSynergyChangeList As OBLib.Synergy.ReadOnly.ROSynergyChangeList

  Public Property CurrentScheduleGroup As SynergyScheduleGroup
  Public Property CurrentEquipmentAllocatorVenue As OBLib.Equipment.EquipmentAllocatorVenue
  Public Property CurrentEquipmentScheduleBasic As OBLib.Equipment.EquipmentScheduleBasic

  Public Property ROCircuitSelectList As OBLib.Equipment.ReadOnly.ROCircuitSelectList

  Protected Overrides Sub Setup()

    UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

    ClientDataProvider.AddDataSource("UserSystemList", UserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    ClientDataProvider.AddDataSource("ROChannelList", OBLib.CommonData.Lists.ROChannelList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaChannelDefaultList", OBLib.CommonData.Lists.ROSystemProductionAreaChannelDefaultList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)

  End Sub

End Class