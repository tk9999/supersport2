﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master"
  CodeBehind="RoomAllocationPlayout.aspx.vb" Inherits="NewOBWeb.RoomAllocationPlayout" %>

<%@ Import Namespace="OBLib.Slugs" %>

<%@ Import Namespace="OBLib.Rooms" %>

<%@ Import Namespace="Singular.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    div.page-header {
      margin: 10px 0 10px;
    }

    td input {
      height: 20px !important;
    }

    .custom-error-box {
      background-color: #FBECEB;
      border-color: #c44;
      border-style: solid;
      border-width: 1px;
      height: 80px !important;
      width: 100%;
      overflow-y: scroll !important;
    }

    .error-validation-styling {
      height: auto;
      width: 400px;
      visibility: visible;
      overflow-y: scroll;
      max-height: 70px;
      z-index: 9999;
    }

    .ValidationPopup {
      min-height: 131px;
      max-height: 650px;
    }

    .ChannelDiv {
      display: block;
      max-height: 400px;
      overflow-y: scroll;
      overflow-x: hidden;
    }

    #byEventTableColumn > div.table-responsive {
      width: calc(100% - 375px);
    }

    /*width: 100px !important;*/

    .ValidationPopup {
      min-height: 100px;
    }

    #ResultHistory {
      padding: 8px 12px;
      cursor: pointer;
    }

    div.btn-channel-select {
      width: 80px;
      max-width: 80px;
      text-overflow: ellipsis;
      text-align: left;
    }

    div.channel-select-panel {
      display: none;
      width: 1024px;
      z-index: 2000;
    }

    .event-live:not(.vis-selected), .btn-event-live {
      color: #000;
      border: solid 1px #fff;
      background: #F97D81;
      background: -moz-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #F97D81), color-stop(100%, #F97D81));
      background: -webkit-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: linear-gradient(#F97D81, #F97D81);
      background: -o-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      background: -ms-linear-gradient(top, #F97D81 0%, #F97D81 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#F97D81', endColorstr='#F97D81',GradientType=0 );
      cursor: pointer;
    }

    .event-delayed:not(.vis-selected), .btn-event-delayed {
      color: #000;
      border: solid 1px #fff;
      background: #7ABA7A;
      background: -moz-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #7ABA7A), color-stop(100%, #7ABA7A));
      background: -webkit-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: linear-gradient(#7ABA7A, #7ABA7A);
      background: -o-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      background: -ms-linear-gradient(top, #7ABA7A 0%, #7ABA7A 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7ABA7A', endColorstr='#7ABA7A',GradientType=0 );
      cursor: pointer;
    }

    .event-premier:not(.vis-selected), .btn-event-premier {
      color: #000;
      border: solid 1px #fff;
      background: #fdfdaf;
      background: -moz-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fdfdaf), color-stop(100%, #fdfdaf));
      background: -webkit-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: linear-gradient(#fdfdaf, #fdfdaf);
      background: -o-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      background: -ms-linear-gradient(top, #fdfdaf 0%, #fdfdaf 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdfdaf', endColorstr='#fdfdaf',GradientType=0 );
      cursor: pointer;
    }

    .event-default:not(.vis-selected), .btn-event-default {
      color: #000;
      border: solid 1px #fff;
      background: #D3E1E4;
      background: -moz-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #D3E1E4), color-stop(100%, #D3E1E4));
      background: -webkit-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: linear-gradient(#D3E1E4, #D3E1E4);
      background: -o-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      background: -ms-linear-gradient(top, #D3E1E4 0%, #D3E1E4 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#D3E1E4', endColorstr='#D3E1E4',GradientType=0 );
      cursor: pointer;
    }

    .event-row-deleted {
      border: 3px solid red;
    }

    #AllocatorChannelEvent {
      visibility: hidden;
      /*width: 400px;
      height: 400px;*/
      z-index: 10000;
      background: #fff;
      padding: 8px;
      border-style: solid;
      border-width: 2px;
      border-color: #B4D8E7;
    }

    button.Find {
      display: none;
    }

    div.room-dropdown {
      min-width: 900px !important;
      padding-right: 25px;
    }

    div.mcrc-dropdown {
      min-width: 600px !important;
      padding-right: 25px;
    }

    div.sccro-dropdown {
      min-width: 600px !important;
      padding-right: 25px;
    }

    /*div.table-responsive {
      width: -webkit-calc(100% - 100px) !important;
    }*/

    table tbody td {
      font-size: 11px;
    }

    #EditTimesPanelSynergyEvent {
      width: 400px;
      height: 400px;
      z-index: 1000;
      background: #fff;
      -webkit-box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      -moz-box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      box-shadow: 7px 9px 17px -4px rgba(2,125,191,0.7);
      border: solid 1px gray;
    }

    .status-col:hover .status-popup {
      display: block;
    }

    .status-popup {
      display: none;
      position: absolute;
      background-color: #54ade9;
      color: white;
      font-size: 18px;
      min-width: 20px;
      padding: 3px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    }

    td span.room-booking button.btn {
      margin-left: -5px;
    }

    td span.productionhr button.btn {
      margin-left: -5px;
    }
  </style>
  <link href="../Scripts/vis4.10/vis.min.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/SynergyPlayout.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/vis4.10/vis.custom.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/OBMisc.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ResourceHelpers.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Tools/ROResourceBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/daterangepicker.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/RoomAllocationPlayout.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          .Helpers.BootstrapPageHeader(, "Room Allocator", , )
          '.AddClass("hidden-xs hidden-sm hidden-md")
        End With
      End With

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Top)
            .TabHeaderContainer.Attributes("id") = "ViewTabs"

            With .AddTab("GraphicalView", "fa-map", "RoomAllocatorPage.refreshGraphical()", "Graphical", , )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Filters", Singular.Web.BootstrapEnums.Style.Info, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-filter", ,
                                                   Singular.Web.PostBackType.None, "RoomAllocatorPage.showFilters()")
                    End With
                  End With
                End With
                With .Helpers.Div
                  .Attributes("id") = "timeline"
                End With
              End With
            End With

            With .AddTab("GridViewByChannel", "fa-th", "RoomAllocatorPage.refreshGridByChannel()", "Grid - By Channel", , )
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Filters", Singular.Web.BootstrapEnums.Style.Info, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-filter", ,
                                                   Singular.Web.PostBackType.None, "RoomAllocatorPage.showFilters()")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Rooms.SchEvt)("ViewModel.SchEvtListManager",
                                                                                "ViewModel.SchEvtList",
                                                                                 False, False, False, False, True, True, True, ,
                                                                                 Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                 "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      .Style.Margin(, , "50px", )
                      With .FirstRow
                        .AddClass("items selectable")
                        With .AddColumn("Event Info")
                          .Style.Display = Singular.Web.Display.tablecell
                          .Style.Width = "60px"
                          With .Helpers.Div
                            .Style.Display = Singular.Web.Display.block
                            .Helpers.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.GenRefNumber)
                            .Helpers.ReadOnlyFor(Function(d As OBLib.Rooms.SchEvt) d.GenRefNumber)
                          End With
                          With .Helpers.Div
                            .Style.Display = Singular.Web.Display.block
                            .Helpers.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.GenreSeries)
                            .Helpers.ReadOnlyFor(Function(d As OBLib.Rooms.SchEvt) d.GenreSeries)
                          End With
                          With .Helpers.Div
                            .Style.Display = Singular.Web.Display.block
                            .Helpers.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.Title)
                            .Helpers.ReadOnlyFor(Function(d As OBLib.Rooms.SchEvt) d.Title)
                          End With
                          With .Helpers.Div
                            .Style.Display = Singular.Web.Display.block
                            .Helpers.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.LiveTimeString)
                            .Helpers.ReadOnlyFor(Function(d As OBLib.Rooms.SchEvt) d.LiveTimeString)
                          End With
                        End With
                        'With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorPlayout) d.GenreSeriesDisplay)
                        '  .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.GenreSeries()")
                        'End With
                        'With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorPlayout) d.TitleDisplay)
                        '  .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.Title()")
                        'End With
                        With .AddColumn("Channel info")
                          .Style.Width = "350px"
                          With .Helpers.BootstrapTableFor(Of OBLib.Rooms.SchEvtCh)("$data.SchEvtChList()", False, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                            With .FirstRow
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ScheduleCssClass()")
                              With .AddColumn("Channel")
                                .Style.Width = "40px"
                                With .Helpers.Bootstrap.ButtonGroup
                                  .AddClass("channel")
                                  .AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorPage.ChannelCSS($data)")
                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, "SchEvtChBO.canEdit($data, 'ChannelShortName')")
                                  With .Helpers.HTMLTag("button")
                                    .AddBinding(Singular.Web.KnockoutBindingString.css, "SchEvtChBO.channelCss($data)")
                                    .Attributes("type") = "button"
                                    .Attributes("data-toggle") = "dropdown"
                                    .Attributes("aria-haspopup") = "true"
                                    .Attributes("aria-expanded") = "false"
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, "SchEvtChBO.canEdit($data, 'ChannelShortName')")
                                    With .Helpers.Span
                                      .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                                    End With
                                    With .Helpers.SpanC("caret")

                                    End With
                                  End With
                                  With .Helpers.HTMLTag("ul")
                                    .AddClass("dropdown-menu")
                                    With .Helpers.HTMLTag("li")
                                      With .Helpers.HTMLTag("a")
                                        .Attributes("href") = "#"
                                        .HTML = "Add Room"
                                        .AddBinding(Singular.Web.KnockoutBindingString.click, "SchEvtChBO.addRoom($data)")
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Rooms.SchEvtCh) d.ScheduledTimesString)
                                .Style.Width = "40px"
                              End With
                              With .AddColumn("Status")
                                .Style.Width = "40px"
                                .AddClass("status-col")
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                                  .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.EventStatus()")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SchEvtChBO.getEventStatusCssClass($data)")
                                End With
                              End With
                              With .AddColumn("Spon")
                                .Style.Width = "30px"
                                With .Helpers.Bootstrap.Button(, "S", Singular.Web.BootstrapEnums.Style.Custom, "",
                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , ,
                                                               Singular.Web.PostBackType.None, "", )
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.SponsorshipButtonCss()")
                                End With
                              End With
                              'With .AddColumn(Function(d As OBLib.Rooms.SchEvtCh) d.RoomID)

                              'End With
                              With .AddColumn("")
                                .Style.Width = "30px"
                                With .Helpers.Bootstrap.Button(, "Changes", Singular.Web.BootstrapEnums.Style.Custom, "",
                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , ,
                                                               Singular.Web.PostBackType.None, "", )
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.ChangeAlert().length > 0")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.ChangeAlert()")
                                  .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.ChangeAlert()")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "($data.ChangeAlert().length > 0 ? 'animated rubberBand infinite btn btn-xs btn-danger' : 'btn btn-xs btn-default')")
                                End With
                              End With
                              With .AddColumn("")
                                .Style.Width = "30px"
                                With .Helpers.Bootstrap.Button(, "Ack", Singular.Web.BootstrapEnums.Style.DefaultStyle,,
                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , ,
                                                               Singular.Web.PostBackType.None, "SchEvtChBO.acknowledgeChanges($data)", )
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.title, "'Acknowledge changes'")
                                  .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.title, "'Acknowledge changes'")
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .AddColumn("Room Info")
                          With .Helpers.Bootstrap.Row
                            .Style.MarginAll("0")
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.TableFor(Of OBLib.Rooms.RoomSched)("$data.RoomSchedList()", False, False, False, False, True, True, True, "RoomSchedList")
                                .ShowHeader = False
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                                With .FirstRow
                                  '.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.GetParent().ScheduleCssClass()")
                                  .AddClass("event-row")
                                  With .AddColumn("Rooms")
                                    .Style.Width = "200px"
                                    With .Helpers.Span
                                      .AddClass("room-booking")
                                      .AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomSchedBO.canEdit($data, 'roomBookingNode')")
                                      With .Helpers.Span
                                        .Style.Width = "150px"
                                        With .Helpers.Bootstrap.ButtonGroup
                                          .AddClass("room-booking")
                                          With .Helpers.HTMLTag("button")
                                            .AddBinding(Singular.Web.KnockoutBindingString.css, "RoomSchedBO.roomBookingButtonCss($data)")
                                            .Attributes("type") = "button"
                                            .Attributes("data-toggle") = "dropdown"
                                            .Attributes("aria-haspopup") = "true"
                                            .Attributes("aria-expanded") = "false"
                                            With .Helpers.Span
                                              .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.Room() + ': ' + $data.RoomTimes()")
                                            End With
                                            With .Helpers.SpanC("caret")
                                            End With
                                          End With
                                          With .Helpers.HTMLTag("ul")
                                            .AddClass("dropdown-menu")
                                            With .Helpers.HTMLTag("li")
                                              With .Helpers.HTMLTag("a")
                                                .Attributes("href") = "#"
                                                .HTML = "Save"
                                                .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.save($data)")
                                                .AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                                              End With
                                            End With
                                            With .Helpers.HTMLTag("li")
                                              With .Helpers.HTMLTag("a")
                                                .Attributes("href") = "#"
                                                .HTML = "Edit"
                                                .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.editRoomSchedule($data)")
                                              End With
                                            End With
                                            With .Helpers.HTMLTag("li")
                                              With .Helpers.HTMLTag("a")
                                                .Attributes("href") = "#"
                                                .HTML = "Add Crew"
                                                .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.addCrew($data)")
                                              End With
                                            End With
                                            With .Helpers.HTMLTag("li")
                                              With .Helpers.HTMLTag("a")
                                                .Attributes("href") = "#"
                                                .HTML = "Delete"
                                                .AddBinding(Singular.Web.KnockoutBindingString.click, "SchEvtChBO.removeRoomBooking($parent, $data)")
                                              End With
                                            End With
                                          End With
                                        End With
                                      End With
                                    End With
                                    'With .Helpers.ForEachTemplate(Of OBLib.Rooms.RoomSchedCh)("$data.RoomSchedChList()")
                                    '  With .Helpers.Span
                                    '    .Style.Width = "200px"
                                    '    .AddClass("productionhr")
                                    '    With .Helpers.Span
                                    '      .Style.Width = "150px"
                                    '      With .Helpers.HTMLTag("div")
                                    '        .AddClass("btn-group")
                                    '        With .Helpers.HTMLTag("button")
                                    '          .AddClass("btn btn-xs btn-default")
                                    '          .Attributes("type") = "button"
                                    '          .Attributes("data-toggle") = "dropdown"
                                    '          .Attributes("aria-haspopup") = "true"
                                    '          .Attributes("aria-expanded") = "false"
                                    '          With .Helpers.Span
                                    '            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                                    '          End With
                                    '          With .Helpers.SpanC("caret")

                                    '          End With
                                    '        End With
                                    '        With .Helpers.HTMLTag("ul")
                                    '          .AddClass("dropdown-menu")
                                    '          With .Helpers.HTMLTag("li")
                                    '            With .Helpers.HTMLTag("a")
                                    '              .Attributes("href") = "#"
                                    '              .HTML = "Delete"
                                    '              .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.removeChannel($parent, $data)")
                                    '            End With
                                    '          End With
                                    '        End With
                                    '      End With
                                    '    End With
                                    '  End With
                                    'End With
                                  End With
                                  With .AddColumn("Room Channels")
                                    .Style.Width = "600px"
                                    With .Helpers.Bootstrap.Row
                                      .Style.MarginAll("0")
                                      With .Helpers.ForEachTemplate(Of OBLib.Rooms.RoomSchedCh)("$data.RoomSchedChList()")
                                        With .Helpers.Span
                                          .Style.Width = "200px"
                                          .AddClass("productionhr")
                                          .AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomSchedBO.canEdit($data, 'roomChannelNode')")
                                          With .Helpers.Span
                                            .Style.Width = "150px"
                                            With .Helpers.HTMLTag("div")
                                              .AddClass("btn-group")
                                              With .Helpers.HTMLTag("button")
                                                .AddClass("btn btn-xs btn-default")
                                                .Attributes("type") = "button"
                                                .Attributes("data-toggle") = "dropdown"
                                                .Attributes("aria-haspopup") = "true"
                                                .Attributes("aria-expanded") = "false"
                                                With .Helpers.Span
                                                  .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                                                End With
                                                With .Helpers.SpanC("caret")

                                                End With
                                              End With
                                              With .Helpers.HTMLTag("ul")
                                                .AddClass("dropdown-menu")
                                                With .Helpers.HTMLTag("li")
                                                  With .Helpers.HTMLTag("a")
                                                    .Attributes("href") = "#"
                                                    .HTML = "Delete"
                                                    .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.removeChannel($parent, $data)")
                                                  End With
                                                End With
                                              End With
                                            End With
                                          End With
                                        End With
                                      End With
                                    End With
                                  End With
                                  With .AddColumn("Room Crew")
                                    With .Helpers.Bootstrap.Row
                                      .Style.MarginAll("0")
                                      With .Helpers.ForEachTemplate(Of OBLib.Rooms.SchEvtChRmHr)("$data.SchEvtChRmHrList()")
                                        With .Helpers.Span
                                          .Style.Width = "200px"
                                          .AddClass("productionhr")
                                          .AddBinding(Singular.Web.KnockoutBindingString.enable, "SchEvtChRmHrBO.canEdit($data, 'roomSchedHRNode')")
                                          With .Helpers.Span
                                            .Style.Width = "150px"
                                            With .Helpers.HTMLTag("div")
                                              .AddClass("btn-group")
                                              With .Helpers.HTMLTag("button")
                                                .AddClass("btn btn-xs btn-default")
                                                .Attributes("type") = "button"
                                                .Attributes("data-toggle") = "dropdown"
                                                .Attributes("aria-haspopup") = "true"
                                                .Attributes("aria-expanded") = "false"
                                                With .Helpers.Span
                                                  .AddBinding(Singular.Web.KnockoutBindingString.html, "SchEvtChRmHrBO.crewButtonText($data)")
                                                End With
                                                With .Helpers.SpanC("caret")

                                                End With
                                              End With
                                              With .Helpers.HTMLTag("ul")
                                                .AddClass("dropdown-menu")
                                                With .Helpers.HTMLTag("li")
                                                  With .Helpers.HTMLTag("a")
                                                    .Attributes("href") = "#"
                                                    .HTML = "Edit"
                                                    .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.editRoomSchedule($parent)")
                                                  End With
                                                End With
                                                With .Helpers.HTMLTag("li")
                                                  With .Helpers.HTMLTag("a")
                                                    .Attributes("href") = "#"
                                                    .HTML = "Delete"
                                                    .AddBinding(Singular.Web.KnockoutBindingString.click, "RoomSchedBO.removeCrewMember($parent, $data)")
                                                  End With
                                                End With
                                              End With
                                            End With
                                          End With
                                        End With
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("SynergyChanges", "fa-bitcoin", "", "Change Log", , )
              With .TabPane
                With .Helpers.With(Of OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria)("ViewModel.ROSynergyChangePagedListCriteria()")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.GenRefNumber)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.GenRefNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ScheduleNumber)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ScheduleNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeStartDateTime)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeEndDateTime)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ChangeEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.SynergyChangeTypeID)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.SynergyChangeTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ColumnName)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePagedList.Criteria) d.ColumnName, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Synergy.ReadOnly.ROSynergyChangePaged)("ViewModel.ROSynergyChangePagedListManager",
                                                                                                         "ViewModel.ROSynergyChangePagedList",
                                                                                                         False, False, False, False, True, True, True, ,
                                                                                                         Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                         "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      With .FirstRow
                        .AddClass("items selectable")
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.GenRefNumber)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ScheduleNumber)
                        End With
                        With .AddColumn("")
                          .Style.Width = "50px"
                          '.FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                          With .Helpers.Span
                            .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ChangeTypeCss()")
                            With .Helpers.HTMLTag("i")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ChangeTypeIconName()")
                            End With
                            With .Helpers.Span
                              .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChangeTypeString()")
                            End With
                          End With
                        End With
                        'With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ChangeTypeString)
                        'End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ChangeDateTimeString)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ChangeDescription)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.ColumnName)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.PreviousValue)
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Synergy.ReadOnly.ROSynergyChangePaged) d.NewValue)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

          End With
        End With
      End With

      With h.Bootstrap.Dialog("RoomScheduleModal", "Room Booking", False, "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-house", , True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Rooms.RoomSched)("ViewModel.CurrentSchEvtChRm()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    With .Helpers.With(Of OBLib.Rooms.SchEvt)("ViewModel.CurrentSchEvtChRm().GetParent()")
                      With .Helpers.Bootstrap.FlatBlock("Event Info", , , , )
                        With .ContentTag
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.GenRefNumber)
                                .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.SchEvt) d.GenRefNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.GenreSeries)
                                .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.SchEvt) d.GenreSeries, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.Title)
                                .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.SchEvt) d.Title, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.SchEvt) d.LiveTimeString)
                                .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.SchEvt) d.LiveTimeString, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    With .Helpers.With(Of OBLib.Rooms.RoomSched)("ViewModel.CurrentSchEvtChRm()")
                      With .Helpers.Bootstrap.FlatBlock("Booking Info", , , , )
                        With .ContentTag
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                             Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                                                             Singular.Web.PostBackType.None, "RoomSchedBO.save($data)")
                                .Button.AddClass("btn-block")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomSchedBO.canEdit($data, 'SaveButton')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.Button(, "Add Crew", Singular.Web.BootstrapEnums.Style.Info, ,
                                                             Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                                             Singular.Web.PostBackType.None, "RoomSchedBO.addCrew($data)")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomSched) d.ProductionAreaID).Style.Width = "100%"
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomSched) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomSched) d.RoomID).Style.Width = "100%"
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomSched) d.RoomID, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomSched) d.CallTime).Style.Width = "100%"
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomSched) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              '.Helpers.Bootstrap.LabelDisplay("Time").Style.Width = "100%"
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomSched) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomSched) d.StartDateTime).Style.Width = "100%"
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomSched) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              '.Helpers.Bootstrap.LabelDisplay("Time").Style.Width = "100%"
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomSched) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomSched) d.EndDateTime).Style.Width = "100%"
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomSched) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              '.Helpers.Bootstrap.LabelDisplay("Time").Style.Width = "100%"
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomSched) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                              'End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.With(Of OBLib.Rooms.SchEvt)("ViewModel.CurrentSchEvtChRm().GetParent()")
                      With .Helpers.Bootstrap.FlatBlock("Channels", , , , )
                        With .ContentTag
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.TableFor(Of OBLib.Rooms.SchEvtCh)("$data.SchEvtChList()", False, False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                                With .FirstRow
                                  With .AddColumn("Channel")
                                    .Style.Width = "30px"
                                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                                      .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ChannelShortName()")
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ButtonStyleCssClass()")
                                    End With
                                  End With
                                  With .AddReadOnlyColumn(Function(d As OBLib.Rooms.SchEvtCh) d.ScheduledTimesString)
                                    .Style.Width = "90px"
                                  End With
                                  With .AddColumn("")
                                    .Style.Width = "20px"
                                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, , )
                                      .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.EventStatus()")
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SchEvtChBO.getEventStatusCssClass($data)")
                                    End With
                                  End With
                                  With .AddColumn("")
                                    .Style.Width = "20px"
                                    With .Helpers.Bootstrap.Button(, "S", Singular.Web.BootstrapEnums.Style.Custom, "",
                                                                   Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , ,
                                                                   Singular.Web.PostBackType.None, "", )
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "$data.SponsorshipButtonCss()")
                                    End With
                                  End With
                                  With .AddColumn(Function(d As OBLib.Rooms.SchEvtCh) d.RoomID)

                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.Row
                  With .Helpers.With(Of OBLib.Rooms.RoomSched)("ViewModel.CurrentSchEvtChRm()")
                    With .Helpers.ForEachTemplate(Of OBLib.Rooms.SchEvtChRmHr)("$data.SchEvtChRmHrList()")
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                        With .Helpers.Bootstrap.FlatBlock(, , True, , )
                          With .ContentTag
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 12, 5, 4)
                                .AddClass("col-lg-offset-7 col-xl-offset-8")
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                               "fa-trash", , Singular.Web.PostBackType.None, "RoomSchedBO.removeCrewMember($parent, $data)")
                                  .Button.AddClass("btn-block")
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.HumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small)
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SchEvtChRmHrBO.canEdit($data, 'HumanResourceID')")
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Rooms.SchEvtChRmHr) d.EditTimes, , , "btn-default", , "fa-clock-o", "fa-clock-o", "btn-sm")
                                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.BookingTimes()")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SchEvtChRmHrBO.bookingsTimesButtonCss($data)")
                                '.Button.AddClass("btn-block")
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.EditTimes()")
                              .Helpers.Bootstrap.LabelDisplay("Call Time")
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small)
                                End With
                                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small)
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.EditTimes()")
                              .Helpers.Bootstrap.LabelDisplay("Start Time")
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                                End With
                                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.EditTimes()")
                              .Helpers.Bootstrap.LabelDisplay("End Time")
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                                End With
                                With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                                  .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                                End With
                              End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.EditTimes()")
                              '.Helpers.Bootstrap.LabelDisplay("Wrap Time")
                              'With .Helpers.Bootstrap.Row
                              '  With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              '    .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              '  End With
                              '  With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              '    .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.SchEvtChRmHr) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small)
                              '  End With
                              'End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
            End With
          End With
        End With
      End With



      With h.Bootstrap.Dialog("SynergyFilterModal", "Filters", , "modal-md", Singular.Web.BootstrapEnums.Style.Info, , "fa-filter", , False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Rooms.AllocatorChannelPlayoutList.Criteria)("ViewModel.AllocatorChannelPlayoutCriteria()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                With .Helpers.Bootstrap.FlatBlock("General", False, False, , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.StartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.EndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Genre)
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Genre, Singular.Web.BootstrapEnums.InputSize.Small, , "Genre")
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Series)
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Series, Singular.Web.BootstrapEnums.InputSize.Small, , "Series")
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.GenRefNoString)
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.GenRefNoString, Singular.Web.BootstrapEnums.InputSize.Small, , "Gen Ref")
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Title)
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Title, Singular.Web.BootstrapEnums.InputSize.Small, , "Title")
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Broadcast Status").Style.Width = "100%"
                          With .Helpers.DivC("btn-group")
                            .Style.Width = "100%"
                            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Live, "Live", "Live", , , , , "btn-xs")
                            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Delayed, "Delayed", "Delayed", , , , , "btn-xs")
                            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Rooms.AllocatorChannelPlayoutList.Criteria) p.Premier, "Premier", "Premier", , , , , "btn-xs")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                With .Helpers.Bootstrap.FlatBlock("Channels", False, False, , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Button(, "Select All", BootstrapEnums.Style.Primary,
                                                         , BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-check-square-o", , ,
                                                         "RoomAllocatorPage.selectAllChannels($data, true)")
                          End With
                          With .Helpers.Bootstrap.Button(, "Deselect All", BootstrapEnums.Style.DefaultStyle,
                                                         , BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-minus", , ,
                                                         "RoomAllocatorPage.selectAllChannels($data, false)")
                          End With
                          With .Helpers.Bootstrap.Button(, "Select Defaults", BootstrapEnums.Style.Primary,
                                                         , BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-minus", , ,
                                                         "RoomAllocatorPage.selectDefaultChannels()")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROChannel)("ViewModel.ROChannelList")
                        With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
                          With .Helpers.Bootstrap.StateButton(Function(p As OBLib.Maintenance.ReadOnly.ROChannel) p.IsSelected, "btn-primary", "btn-default", , , , , "btn-xs")
                            .Button.AddBinding(KnockoutBindingString.click, "RoomAllocatorPage.AddChannel($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Maintenance.ReadOnly.ROChannel) c.ChannelShortName)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.PullRight
            With .Helpers.Bootstrap.Button(, "Apply Filters", BootstrapEnums.Style.Primary,
                               , BootstrapEnums.ButtonSize.Small, ,
                               "fa-download", , PostBackType.None,
                               "RoomAllocatorPage.refreshChannelAllocator()")
            End With
          End With
        End With
      End With

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
