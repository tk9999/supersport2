﻿Imports Singular.DataAnnotations
Imports Singular.Web.Data
Imports OBLib.Rooms
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Security
Imports OBLib.Synergy
Imports OBLib.Productions.Base

Public Class OBImporter
  Inherits OBPageBase(Of OBImporterVM)

End Class

Public Class OBImporterVM
  Inherits OBViewModelStateless(Of OBImporterVM)

  Public Property CurrentSynergyEventGuid As String
  Public Property DisableUpload As Boolean
  Public Property CurrentSynergyEvent As SynergyEvent
  Public Property FreezeSetExpressions As Boolean

  Public Property ManualProduction As ManualProduction
  Public Property ManualProductionCreated As Boolean

  <InitialDataOnly>
  Public Property UserSystemList As UserSystemList

  <InitialDataOnly>
  Public Property SynergyEventList As OBLib.Synergy.SynergyEventList = New OBLib.Synergy.SynergyEventList
  <InitialDataOnly>
  Public Property SynergyEventListCriteria As OBLib.Synergy.SynergyEventList.Criteria = New OBLib.Synergy.SynergyEventList.Criteria
  <InitialDataOnly>
  Public Property SynergyEventListManager As Singular.Web.Data.PagedDataManager(Of OBImporterVM) = New Singular.Web.Data.PagedDataManager(Of OBImporterVM)(Function(d) Me.SynergyEventList,
                                                                                                                                                                 Function(d) Me.SynergyEventListCriteria,
                                                                                                                                                                "ScheduleDateTime", 25)

  Protected Overrides Sub Setup()
    MyBase.Setup()

    SynergyEventListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    SynergyEventListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    SynergyEventListCriteria.StartDate = Now.AddDays(-3)
    SynergyEventListCriteria.EndDate = Now.AddDays(14)
    SynergyEventListCriteria.OBOnly = True
    ManualProductionCreated = False

    UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

    DisableUpload = False

    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("UserSystemList", OBLib.Security.Settings.CurrentUser.UserSystemList, False)

  End Sub

  'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
  '  MyBase.HandleCommand(Command, CommandArgs)
  'End Sub

End Class