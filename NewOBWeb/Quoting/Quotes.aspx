﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
  CodeBehind="Quotes.aspx.vb" Inherits="NewOBWeb.Quotes" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>

<%@ Import Namespace="OBLib.Productions" %>

<%@ Import Namespace="OBLib.Maintenance.Productions.ReadOnly" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Quoting.ReadOnly" %>
<%@ Import Namespace="OBLib.Quoting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link rel="Stylesheet" href="../Styles/NewStyle.css" />
  <link rel="Stylesheet" href="../Styles/FlatDream.css" />
  <style type="text/css">
    body, #CurrentAttendees {
      background-color: #F0F0F0;
    }

    .SubManagerInvalid {
      border-style: solid;
      border-color: red;
      border-width: 1px;
    }

    .vis.timeline .item.background.day-off {
      background-color: rgba(255, 0, 0, 0.2);
    }

    #Main {
      background-color: #F0F0F0;
    }
  </style>
  <script type="text/javascript" src="../Scripts/Tools/PagingManagers.js"></script>
  <script type="text/javascript" src="../Scripts/Pages/Quotes.js"></script>
  <script type="text/javascript" src="Quotes.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%Using h = Helpers

      With h.Toolbar
        .Helpers.MessageHolder()
      End With
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Quotes", True, False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Button("Save", "Save", BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", , Singular.Web.PostBackType.Full)
                With .Button
                  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(vm) ViewModel.IsValid)
                End With
              End With
              With .Helpers.Bootstrap.Button(, "Find Quote", BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindQuote()")
                With .Button
                End With
              End With
              '----Add New Quote ----
              With .Helpers.Bootstrap.Button(, "New Quote", BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus", , Singular.Web.PostBackType.None, "NewQuote()")
                With .Button

                End With
              End With
            End With
          End With
        End With
      End With

      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "FindQuote"
        With .Helpers.Bootstrap.FlatBlock(, True)
          .FlatBlockTag.AddClass("block-flat")
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROQuoteListCriteria.EventName)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search by event name"
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    ' .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEquipments.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROQuoteListCriteria.StartDate)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Start Date"
                  End With
                  
                End With
              End With
            
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROQuoteListCriteria.EndDate)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  'With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.ROQuoteListCriteria.ProductionTypeEventType, "ROEventTypes.ShowModal()", "Event Type", , , "fa-search", True)
                  '  .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "ROEventTypes.ShowModal()")
                  'End With
                  
                  With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.ROQuoteListCriteria.ProductionTypeEventType, "ROEventTypes.ShowModal()",
                                       "Event Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ClearProductionTypeEventType()", )
                  End With
                  
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 4, 4)
              '  With .Helpers.Bootstrap.Button(, "Search", BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindQuotes()")
              '    With .Button
              '      '.AddBinding(Singular.Web.KnockoutBindingString.enable, "SynergyImporter.CanFind()")
              '    End With
              '  End With
              'End With
            End With
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12)
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Quoting.ReadOnly.ROQuote)(Function(d) ViewModel.ROQuoteListPagingManager,
                                                                                                  Function(d) ViewModel.ROQuoteList,
                                                                                                  False, False, False, False, True, True, False,
                                                                                                 "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                  "ROQuoteListManagerObj.OnItemSelected", False)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn("")
                      .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                      With .Helpers.HTMLTag("span")
                        .AddBinding(KnockoutBindingString.css, "ROQuoteListManagerObj.ItemIsSelectedCss($data)")
                        .AddBinding(KnockoutBindingString.html, "ROQuoteListManagerObj.ItemIsSelectedHtml($data)")
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.Quoting.ReadOnly.ROQuote) c.QuoteID)
                    
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.Quoting.ReadOnly.ROQuote) c.QuotedDate)
                      .AddClass("col-xs-3 col-sm-2 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.Quoting.ReadOnly.ROQuote) c.EventName)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                    With .AddReadOnlyColumn(Function(c As OBLib.Quoting.ReadOnly.ROQuote) c.EventDate)
                      .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      With h.Div()
        .AddClass("row CurrentQuote")
        .AddBinding(Singular.Web.KnockoutBindingString.visibleA, "ViewModel.CurrentQuoteVisible()")
        'Quote Details
        With .Helpers.With(Of Quote)(Function(d) ViewModel.CurrentQuote)
          With .Helpers.Bootstrap.Column(6, 6, 6, 6)
            With .Helpers.Bootstrap.FlatBlock("Quote", True)
              .FlatBlockTag.AddClass("block-flat")
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Div()
                    .AddClass("form-horizontal")
                    With .Helpers.FieldSet("")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          .AddBinding(KnockoutBindingString.visible, "CanviewReports($data)")
                          With .Helpers.Div()
                            .AddClass("pull-right")
                            With .Helpers.Div()
                              .AddClass("pull-right")
                              With .Helpers.Bootstrap.Button("GenerateQuotationReportDetailed", "Detailed Quotation", BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-print", , Singular.Web.PostBackType.Full)
                              End With
                            End With
                            With .Helpers.Div()
                              .AddClass("pull-right")
                              With .Helpers.Bootstrap.Button("GenerateQuotationReport", "Normal Quotation", BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-print", , Singular.Web.PostBackType.Full)
                              End With
                            End With
                            With .Helpers.Div()
                              .AddClass("pull-right")
                              With .Helpers.Bootstrap.Button("GenerateQuotationReportTemp", "Temp Quotation", BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-print", , Singular.Web.PostBackType.Full)
                              End With
                            End With
                            'End With
                          End With
                        End With
                      End With
                      With .Helpers.Div()
                        .AddClass("row top-buffer-10")
                        'Left Column
                        With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteID)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteID)
                                .AddClass("form-control")
                                .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                              End With
                            End With
                          End With
                        End With
                        'Middle Column
                        With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuotedDate)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuotedDate)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                        'Right Column
                        With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.VersionNo)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.VersionNo)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Div()
                        .AddClass("row top-buffer-10")
                        'Left Column
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.EventName)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.EventName)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                        'Middle Column
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.EventDate)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.EventDate)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                        'Right Column
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.DebtorID)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d) ViewModel.CurrentQuote.Debtor, "RODebtors.ShowModal()", "Debtor", , , "fa-search", True)
                                .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "RODebtors.ShowModal()")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Div()
                        .AddClass("row top-buffer-10")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.EventTypeID)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As Quote) ViewModel.CurrentQuote.ProductionTypeEventType, "ROEventTypes.ShowModal()", "Event Type", , , "fa-search", True)
                                .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "ROEventTypes.ShowModal()")
                              End With
                            End With
                          End With
                        End With
                        'Right Column
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.VenueID)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As Quote) ViewModel.CurrentQuote.ProductionVenue, "ROProductionVenues.ShowModal()", "Venue", , , "fa-search", True)
                                .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "ROProductionVenues.ShowModal()")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.Bootstrap.LabelDisplay("Internal/External?")
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.Bootstrap.StateButton(Function(d As Quote) d.InternalInd,
                                                                  "Internal", "External", ,
                                                                  "btn-danger", , "fa-minus", "btn-sm")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Div()
                        .AddClass("row top-buffer-10")
                        'Left Column
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.TechnicalSpec)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.TechnicalSpec)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                        'Middle Column
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.TxDays)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.TxDays)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                        'Right Column
                    
                      End With
                      With .Helpers.Div()
                        .AddClass("row top-buffer-10")
                        'Right Column
                        With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                          .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteAmount)
                          With .Helpers.Div()
                            .AddClass("input-group")
                            With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteAmount)
                              .AddClass("form-control")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                            End With
                            With .Helpers.HTMLTag("span")
                              .AddClass("input-group-btn")
                              With .Helpers.BootstrapButton(, , "btn btn-warning", "glyphicon-fire", Singular.Web.PostBackType.None, False, , , "ShowQuoteOverride($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanOverrideQuoteAmount($data)")
                              End With
                            End With
                          End With
                        End With
                        'Left Column
                        With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteAcceptedDateTime)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteAcceptedDateTime)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                        'Middle Column
                        With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                          With .Helpers.Div()
                            .AddClass("input-column")
                            .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteDeclinedDateTime)
                            With .Helpers.Div()
                              .AddClass("field-box")
                              With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteDeclinedDateTime)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                      End With
                      '----------------------------------------
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Div()
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) Not ViewModel.CurrentQuote.IsNew)
              With .Helpers.FieldSet("")
                With .Helpers.Bootstrap.FlatBlock("Headings", True)
                  .FlatBlockTag.AddClass("block-flat")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                    
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.BootstrapTableFor(Of QuoteHeading)(Function(d) d.QuoteHeadingList, False, False, "")
                          With .FirstRow
                            With .AddColumn("")
                              With .Helpers.BootstrapButton(, "Details", "btn btn-xs btn-info", "glyphicon-info-sign", Singular.Web.PostBackType.None, False, , , "ShowHeaderDetails($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanViewHeaderDetails($data)")
                              End With
                            End With
                            With .AddColumn(Function(a As QuoteHeading) a.CostTypeID)
                    
                            End With
                            With .AddColumn(Function(a As QuoteHeading) a.Description)
                    
                            End With
                            With .AddColumn(Function(a As QuoteHeading) a.Amount)
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                            End With
                            With .AddColumn("")
                              With .Helpers.BootstrapButton(, "Override", "btn btn-xs btn-warning", "glyphicon-fire", Singular.Web.PostBackType.None, False, , , "ShowQuoteHeaderOverride($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanOverrideHeaderAmount($data)")
                              End With
                            End With
                            With .AddColumn("")
                              .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveQuoteHeading($data)")
                            End With
                          End With
                          With .FooterRow
                            With .AddColumn("")
                              .ColSpan = 9
                              With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddQuoteHeading()")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        
          'Quote Heading Details
          With .Helpers.Bootstrap.Column(6, 6, 6, 6)
            With .Helpers.Div()
              .AddClass("row")
              .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) ViewModel.CurrentHeaderDetailsVisible Or Not ViewModel.CurrentQuote.IsNew)
              With .Helpers.FieldSet("")
                With .Helpers.Bootstrap.FlatBlock("Heading Details", True)
                  .FlatBlockTag.AddClass("block-flat")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.With(Of QuoteHeading)("CurrentHeader()")
                        'With .Helpers.DivC("row")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          With .Helpers.BootstrapTableFor(Of QuoteHeadingDetail)(Function(d As QuoteHeading) d.QuoteHeadingDetailList, False, False, "")
                            With .FirstRow
                              With .AddColumn(Function(a As QuoteHeadingDetail) a.CostTypeID)
                    
                              End With
                              With .AddColumn("Description")
                                With .Helpers.BootstrapStateButton("", "CostDescriptionClick($data)", "CostDescriptionCss($data)", "CostDescriptionHtml($data)", False)
                                End With
                              End With
                              With .AddColumn(Function(a As QuoteHeadingDetail) a.Amount)
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                              End With
                              With .AddColumn("")
                                .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveQuoteHeadingDetail($data)")
                              End With
                            End With
                            With .FooterRow
                              With .AddColumn("")
                                .ColSpan = 14
                                With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddQuoteHeadingDetail()")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "QuoteAmountOverride"
        With .Helpers.Bootstrap.FlatBlock("Quote Override", True)
          .FlatBlockTag.AddClass("block-flat")
          With .ContentTag
            'With .Helpers.Bootstrap.Row
            With .Helpers.With(Of Quote)(Function(d) ViewModel.CurrentQuote)
              'With .Helpers.Bootstrap.Row
              'Left Column
              With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                With .Helpers.Div()
                  .AddClass("input-column")
                  .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideAmount)
                  With .Helpers.Div()
                    .AddClass("field-box")
                    With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideAmount)
                      .AddClass("form-control")
                    End With
                  End With
                End With
              End With
              'Middle Column
              With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                With .Helpers.Div()
                  .AddClass("input-column")
                  .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideReason)
                  With .Helpers.Div()
                    .AddClass("field-box")
                    With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideReason)
                      .AddClass("form-control")
                    End With
                  End With
                End With
              End With
              'Middle Column
              With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                With .Helpers.Div()
                  .AddClass("input-column")
                  .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideByName)
                  With .Helpers.Div()
                    .AddClass("field-box")
                    With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideByName)
                      .AddClass("form-control")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                With .Helpers.Div()
                  .AddClass("input-column")
                  .Helpers.LabelFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideDateTime)
                  With .Helpers.Div()
                    .AddClass("field-box")
                    With .Helpers.EditorFor(Function(d As Quote) ViewModel.CurrentQuote.QuoteOverrideDateTime)
                      .AddClass("form-control")
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.BootstrapButton(, "Cancel", "btn btn-sm btn-danger", "glyphicon-thumbs-down", Singular.Web.PostBackType.None, False, , , "CancelQuoteOverride($data)")
                End With
                With .Helpers.BootstrapButton(, "Done", "btn btn-sm btn-primary", "glyphicon-thumbs-up", Singular.Web.PostBackType.None, False, , , "CommitQuoteOverride($data)")
                  .AddClass("pull-right")
                End With
              End With
            End With
          End With
        End With
      End With
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "CurrentHeaderOverride"
        With .Helpers.Bootstrap.FlatBlock("Header Override", True)
          .FlatBlockTag.AddClass("block-flat")
          With .ContentTag
            'With .Helpers.Bootstrap.Row
            With .Helpers.With(Of QuoteHeading)("CurrentHeader()")
              'Left Column
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                  With .Helpers.Div()
                    .AddClass("input-column")
                    .Helpers.LabelFor(Function(d As QuoteHeading) d.AmountOverride)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeading) d.AmountOverride)
                        .AddClass("form-control")
                      End With
                    End With
                  End With
                End With
                'Middle Column
                With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                  With .Helpers.Div()
                    .AddClass("input-column")
                    .Helpers.LabelFor(Function(d As QuoteHeading) d.AmountOverrideReason)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeading) d.AmountOverrideReason)
                        .AddClass("form-control")
                      End With
                    End With
                  End With
                End With
                'Middle Column
                With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                  With .Helpers.Div()
                    .AddClass("input-column")
                    .Helpers.LabelFor(Function(d As QuoteHeading) d.AmountOverrideByName)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeading) d.AmountOverrideByName)
                        .AddClass("form-control")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                  With .Helpers.Div()
                    .AddClass("input-column")
                    .Helpers.LabelFor(Function(d As QuoteHeading) d.AmountOverrideDateTime)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeading) d.AmountOverrideDateTime)
                        .AddClass("form-control")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.BootstrapButton(, "Cancel", "btn btn-sm btn-danger", "glyphicon-thumbs-down", Singular.Web.PostBackType.None, False, , , "CancelQuoteHeaderOverride($data)")
                  
                  End With
                  With .Helpers.BootstrapButton(, "Done", "btn btn-sm btn-primary", "glyphicon-thumbs-up", Singular.Web.PostBackType.None, False, , , "CommitQuoteHeaderOverride($data)")
                 
                  End With
                End With
              End With
            End With
            'End With
          End With
        End With
      End With
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "ROEventTypesModal"
        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock(, True, True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .AboveContentTag
              'With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROEventTypePagedListCriteria.ProductionType)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search by Genre"
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEventTypes.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROEventTypePagedListCriteria.EventType)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search by Series"
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROEventTypes.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                .AddClass("pull-right")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "ROEventTypes.RefreshList()")
                  End With
                End With
              End With
              'End With
            End With
            With .ContentTag
              'With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of ROEventTypePaged)(Function(d) ViewModel.ROEventTypePagedListManager,
                                                                          Function(d) ViewModel.ROEventTypePagedList,
                                                                          False, False, False, False, True, True, True,
                                                                          "EventTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                          "ROEventTypes.OnItemSelected")
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn("")
                      .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                      With .Helpers.HTMLTag("span")
                        .AddBinding(KnockoutBindingString.css, "ROEventTypes.ItemIsSelectedCss($data)")
                        .AddBinding(KnockoutBindingString.html, "ROEventTypes.ItemIsSelectedHtml($data)")
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.ProductionType)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.EventType)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With
                End With
              End With
              'End With
            End With
          End With
        End With
      End With
      
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "ROProductionVenuesModal"
        With .Helpers.Bootstrap.FlatBlock(, True, True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            'With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.EditorFor(Function(d) ViewModel.ROProductionVenueListCriteria.ProductionVenue)
                  .AddClass("form-control input-sm")
                  .Attributes("placeholder") = "Search by Venue"
                  .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROProductionVenues.DelayedRefreshList() }")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              .AddClass("pull-right")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                               Singular.Web.PostBackType.None, "ROProductionVenues.RefreshList()")
                End With
              End With
            End With
            'End With
          End With
          With .ContentTag
            'With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.PagedGridFor(Of ROProductionVenue)(Function(d) ViewModel.ROProductionVenueListManager,
                                                                             Function(d) ViewModel.ROProductionVenueList,
                                                                             False, False, False, False, True, True, True,
                                                                             "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                             "ROProductionVenues.OnItemSelected")
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                .Pager.PagerListTag.ListTag.AddClass("pull-left")
                With .FirstRow
                  .AddClass("items")
                  With .AddColumn("")
                    .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                    With .Helpers.HTMLTag("span")
                      .AddBinding(KnockoutBindingString.css, "ROProductionVenues.ItemIsSelectedCss($data)")
                      .AddBinding(KnockoutBindingString.html, "ROProductionVenues.ItemIsSelectedHtml($data)")
                    End With
                  End With
                  With .AddReadOnlyColumn(Function(d As ROProductionVenue) d.ProductionVenue)
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                  End With
                End With
              End With
            End With
            'End With
          End With
        End With
      End With
      
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "RODebtorListModal"
        With .Helpers.Bootstrap.FlatBlock(, True, True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.RODebtorListCriteria.Keyword)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search..."
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RODebtors.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                '.AddClass("pull-right")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "RODebtor.RefreshList()")
                  End With
                End With
              End With
            End With
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Div
                  .AddClass("table-responsive")
                  With .Helpers.Bootstrap.PagedGridFor(Of RODebtor)(Function(d) ViewModel.RODebtorListManager,
                                                                                 Function(d) ViewModel.RODebtorList,
                                                                                 False, False, False, False, True, True, False,
                                                                                 "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                 "RODebtors.OnItemSelected")
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        With .Helpers.HTMLTag("span")
                          .AddBinding(KnockoutBindingString.css, "RODebtors.ItemIsSelectedCss($data)")
                          .AddBinding(KnockoutBindingString.html, "RODebtors.ItemIsSelectedHtml($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As RODebtor) c.Debtor)
                        '.AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactName)
                        '.AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                      End With
                      With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactNumber)
                        '.AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                      End With
                      With .AddReadOnlyColumn(Function(c As RODebtor) c.Address)
                        '.AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "ROAllowedDisciplineSelectModal"
        With .Helpers.Bootstrap.FlatBlock(, True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROAllowedDisciplineSelectListCriteria.Keyword)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search..."
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROAllowedDisciplines.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                '.AddClass("pull-right")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "ROAllowedDisciplines.RefreshList()")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Commit", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                                 Singular.Web.PostBackType.None, "UI.CommitSelectedDisciplines()")
                  End With
                End With
              End With
            End With
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                With .Helpers.Div
                  .AddClass("table-responsive")
                  With .Helpers.Bootstrap.PagedGridFor(Of ROAllowedDisciplineSelect)(Function(d) ViewModel.ROAllowedDisciplineSelectListPagingManager,
                                                                                     Function(d) ViewModel.ROAllowedDisciplineSelectList,
                                                                                     False, False, False, False, True, True, False,
                                                                                     "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                     "ROAllowedDisciplines.OnItemSelected")
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        With .Helpers.HTMLTag("span")
                          .AddBinding(KnockoutBindingString.css, "ROAllowedDisciplines.ItemIsSelectedCss($data)")
                          .AddBinding(KnockoutBindingString.html, "ROAllowedDisciplines.ItemIsSelectedHtml($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ROAllowedDisciplineSelect) c.Discipline)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROAllowedDisciplineSelect) c.Position)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      With h.Div
        
        .AddClass("modal-dg")
        .Attributes("id") = "CurrentHeaderDetail"
        With .Helpers.Bootstrap.FlatBlock(, True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.With(Of QuoteHeadingDetail)("CurrentQuoteHeadingDetail()")
                With .Helpers.Div
                  .AddClass("row top-buffer-10")
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.Quantity)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.Quantity)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.Flights)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.Flights)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div()
                  .AddClass("row top-buffer-10")
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.DisciplineID)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As QuoteHeadingDetail) d.DisciplinePosition, "ROAllowedDisciplines.ShowModal()", "Discipline", , , "fa-search", True)
                        .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "ROAllowedDisciplines.ShowModal()")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.Accommodation)
                    With .Helpers.Div
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.Accommodation)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div()
                  .AddClass("row top-buffer-10")
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.CarHire)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.CarHire)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.ProductionAreaID)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.ProductionAreaID)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div()
                  .AddClass("row top-buffer-10")
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.SnT)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.SnT)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.Fee)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.Fee)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div()
                  .AddClass("row top-buffer-10")
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.CompanyRateCardID)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As QuoteHeadingDetail) d.CompanyRateCard, "ROCompanyRateCards.ShowModal()", "Company Rate Cards", , , "fa-search", True)
                        .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "ROCompanyRateCards.ShowModal()")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.SupplierRateCardID)
                    With .Helpers.Div
                      .AddClass("field-box")
                      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As QuoteHeadingDetail) d.SupplierRateCard, "ROSupplierRateCards.ShowModal()", "Supplier Rate Cards", , , "fa-search", True)
                        .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "ROSupplierRateCards.ShowModal()")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div()
                  .AddClass("row top-buffer-10")
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.Rate)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.Rate)
                        .AddClass("form-control")
                        '.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    .Helpers.LabelFor(Function(d As QuoteHeadingDetail) d.Amount)
                    With .Helpers.Div()
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(d As QuoteHeadingDetail) d.Amount)
                        .AddClass("form-control")
                        .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "ROSupplierRateCardsModal"
        With .Helpers.Bootstrap.FlatBlock(, True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROSupplierRateCardListCriteria.Keyword)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search..."
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROSupplierRateCards.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                '.AddClass("pull-right")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "ROSupplierRateCards.RefreshList()")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Commit", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                                 Singular.Web.PostBackType.None, "UI.CommitSelectedDisciplines()")
                  End With
                End With
              End With
            End With
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Div
                  .AddClass("table-responsive")
                  With .Helpers.Bootstrap.PagedGridFor(Of ROSupplierRateCard)(Function(d) ViewModel.ROSupplierRateCardListPagingManager,
                                                                                     Function(d) ViewModel.ROSupplierRateCardList,
                                                                                     False, False, False, False, True, True, False,
                                                                                     "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                     "ROSupplierRateCards.OnItemSelected")
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        With .Helpers.HTMLTag("span")
                          .AddBinding(KnockoutBindingString.css, "ROSupplierRateCards.ItemIsSelectedCss($data)")
                          .AddBinding(KnockoutBindingString.html, "ROSupplierRateCards.ItemIsSelectedHtml($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ROSupplierRateCard) c.ProductionType)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROSupplierRateCard) c.EventType)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROSupplierRateCard) c.Area)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROSupplierRateCard) c.CostType)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROSupplierRateCard) c.Quantity)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROSupplierRateCard) c.Amount)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      With h.Div
        .AddClass("modal-dg")
        .Attributes("id") = "ROCompanyRateCardListModal"
        With .Helpers.Bootstrap.FlatBlock(, True)
          .FlatBlockTag.AddClass("block-flat")
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) ViewModel.ROCompanyRateCardListCriteria.Keyword)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search..."
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROCompanyRateCards.DelayedRefreshList() }")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                '.AddClass("pull-right")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "ROCompanyRateCards.RefreshList()")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Commit", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                                 Singular.Web.PostBackType.None, "UI.CommitSelectedDisciplines()")
                  End With
                End With
              End With
            End With
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Div
                  .AddClass("table-responsive")
                  With .Helpers.Bootstrap.PagedGridFor(Of ROCompanyRateCard)(Function(d) ViewModel.ROCompanyRateCardListPagingManager,
                                                                                     Function(d) ViewModel.ROCompanyRateCardList,
                                                                                     False, False, False, False, True, True, False,
                                                                                     "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                     "ROCompanyRateCards.OnItemSelected")
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y"
                    .Pager.PagerListTag.ListTag.AddClass("pull-left")
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        With .Helpers.HTMLTag("span")
                          .AddBinding(KnockoutBindingString.css, "ROCompanyRateCards.ItemIsSelectedCss($data)")
                          .AddBinding(KnockoutBindingString.html, "ROCompanyRateCards.ItemIsSelectedHtml($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.Discipline)
                        .AddClass("col-xs-2 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.Position)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.HumanResource)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.ProductionType)
                        .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-3")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.EventType)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.Area)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.RateType)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                      With .AddReadOnlyColumn(Function(c As ROCompanyRateCard) c.Rate)
                        .AddClass("col-xs-3 col-sm-2 col-md-1 col-lg-1")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
    End Using%>
  <link href="Quotes.css" type="text/css" rel="Stylesheet" />
</asp:Content>
