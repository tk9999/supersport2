﻿Imports OBLib.Quoting
Imports Singular
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Productions.ReadOnly

Public Class Quotes
  Inherits OBPageBase(Of QuotesVM)

End Class

Public Class QuotesVM
  Inherits OBViewModel(Of QuotesVM)

  'Selected Allowed Disciplines
  Public Property SelectedAllowedDisciplineList As OBLib.Productions.SelectedAllowedDisciplineList


  'RO Quotes List Variables
  Public Property ROQuoteList As OBLib.Quoting.ReadOnly.ROQuoteList
  Public Property ROQuoteListCriteria As OBLib.Quoting.ReadOnly.ROQuoteList.Criteria = New OBLib.Quoting.ReadOnly.ROQuoteList.Criteria
  Public Property ROQuoteListPagingManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROQuoteList,
                                                                                                                                                                        Function(d) Me.ROQuoteListCriteria, "EventName", 15)

  'ROEventType Page Grid Properties
  Public Property ROEventTypePagedList As OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList = New OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList
  Public Property ROEventTypePagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList.Criteria
  Public Property ROEventTypePagedListManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROEventTypePagedList,
                                                                                                                                                                     Function(d) Me.ROEventTypePagedListCriteria,
                                                                                                                                                                     "EventType", 15)

  'ROProductionVenue Page Grid Properties
  Public Property ROProductionVenueList As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList = New OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList
  Public Property ROProductionVenueListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria = New OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria
  Public Property ROProductionVenueListManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROProductionVenueList,
                                                                                                                                                                          Function(d) Me.ROProductionVenueListCriteria,
                                                                                                                                                                          "ProductionVenue", 15)


  'RODebtorList Page Grid Properties
  Public Property RODebtorList As [ReadOnly].RODebtorList = New RODebtorList
  Public Property RODebtorListCriteria As RODebtorList.Criteria = New RODebtorList.Criteria
  Public Property RODebtorListManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.RODebtorList,
                                                                                                                                                                          Function(d) Me.RODebtorListCriteria,
                                                                                                                                                                          "Debtor", 15)


  'ROProduction Page Grid Properties
  Public Property ROAllowedDisciplineSelectList As ROAllowedDisciplineSelectList = New ROAllowedDisciplineSelectList
  Public Property ROAllowedDisciplineSelectListCriteria As OBLib.Productions.ReadOnly.ROAllowedDisciplineSelectList.Criteria = New OBLib.Productions.ReadOnly.ROAllowedDisciplineSelectList.Criteria
  Public Property ROAllowedDisciplineSelectListPagingManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROAllowedDisciplineSelectList,
                                                                                                                                                                                      Function(d) Me.ROAllowedDisciplineSelectListCriteria,
                                                                                                                                                                                      "OrderNo", 15)

  'ROCompanyRateCard Page Grid Properties
  Public Property ROCompanyRateCardList As ROCompanyRateCardList = New ROCompanyRateCardList
  Public Property ROCompanyRateCardListCriteria As ROCompanyRateCardList.Criteria = New ROCompanyRateCardList.Criteria
  Public Property ROCompanyRateCardListPagingManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROCompanyRateCardList,
                                                                                                                                                                                      Function(d) Me.ROCompanyRateCardListCriteria,
                                                                                                                                                                                      "Company", 15)

  'ROCompanyRateCard Page Grid Properties
  Public Property ROSupplierRateCardList As ROSupplierRateCardList = New ROSupplierRateCardList
  Public Property ROSupplierRateCardListCriteria As ROSupplierRateCardList.Criteria = New ROSupplierRateCardList.Criteria
  Public Property ROSupplierRateCardListPagingManager As Singular.Web.Data.PagedDataManager(Of QuotesVM) = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROSupplierRateCardList,
                                                                                                                                                                                      Function(d) Me.ROSupplierRateCardListCriteria,
                                                                                                                                                                                      "Company", 15)

  Public Property FindQuoteVisible As Boolean
  Public Property FoundQuote As Boolean
  Public Property CurrentQuoteVisible As Boolean
  Public Property CurrentQuote As Quote
  Public Property CurrentHeaderID As Integer?
  Public Property CurrentHeaderDetailGuid As String
  Public Property CurrentHeaderDetailsVisible As Boolean
  Public ReadOnly Property CurrentUserID As Integer?
    Get
      Return OBLib.Security.Settings.CurrentUserID
    End Get
  End Property
  Public ReadOnly Property CurrentUserName As String
    Get
      Return OBLib.Security.Settings.CurrentUser.LoginName
    End Get
  End Property

  Public Property TotalPages As Integer



  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    MyBase.Setup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    'TotalPages = 0
    'ROQuoteList = New OBLib.Quoting.ReadOnly.ROQuoteList
    ''ROQuoteListCriteria = New OBLib.Quoting.ReadOnly.ROQuoteList.Criteria()
    ''ROQuoteListPagingInfo = New Singular.Web.Data.PagedDataManager(Of QuotesVM)(Function(d) Me.ROQuoteList, Function(a) Me.ROQuoteListCriteria, "EventName", 50)

    FindQuoteVisible = False
    CurrentQuoteVisible = False
    CurrentHeaderID = Nothing
    CurrentHeaderDetailsVisible = False
    CurrentHeaderDetailGuid = ""


    'Client side Lists
    ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
    ClientDataProvider.AddDataSource("ROPositionList", OBLib.CommonData.Lists.ROPositionList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSupplierList", OBLib.CommonData.Lists.ROSupplierList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "NewQuote"
        CurrentHeaderDetailsVisible = False
        CurrentQuote = New Quote()
        CurrentQuoteVisible = True
        CurrentHeaderDetailsVisible = False
        CurrentHeaderDetailGuid = Nothing
      Case "FetchQuote"
        Dim ql As OBLib.Quoting.QuoteList = OBLib.Quoting.QuoteList.GetQuoteList(CommandArgs.ClientArgs.QuoteID)
        If ql.Count = 1 Then
          CurrentQuote = ql(0)
        End If
        FindQuoteVisible = True
        CurrentQuoteVisible = True
        CurrentHeaderDetailsVisible = False
        FoundQuote = True
      Case "Save"
        CurrentQuote.CheckAllRules()
        If Not CurrentQuote.IsValid Then
          AddMessage(Singular.Web.MessageType.Validation, "Valid", CurrentQuote.BrokenRulesCollection(0).Description)
          Exit Select
        End If
        Dim QuoteList As New QuoteList
        QuoteList.Add(CurrentQuote)
        Dim sh As SaveHelper = TrySave(QuoteList)
        If sh.Success Then
          QuoteList = sh.SavedObject
          CurrentQuote = QuoteList(0)
        Else
          AddMessage(Web.MessageType.Error, "Error During Save", sh.ErrorText)
        End If

      Case "GenerateQuotationReport"
        Dim rpt As New OBWebReports.QuoteReports.QuotationReport
        rpt.ReportCriteria.QuoteID = CurrentQuote.QuoteID
        Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        SendFile(rfi.FileName, rfi.FileBytes)

      Case "GenerateQuotationReportDetailed"
        Dim rpt As New OBWebReports.QuoteReports.QuotationReportDetailed
        rpt.ReportCriteria.QuoteID = CurrentQuote.QuoteID
        Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        SendFile(rfi.FileName, rfi.FileBytes)

      Case "GenerateQuotationReportTemp"
        Dim rpt As New OBWebReports.QuoteReports.QuotationTempReport
        rpt.ReportCriteria.QuoteID = CurrentQuote.QuoteID
        rpt.ReportCriteria.CreatedBy = Settings.CurrentUser.UserID
        Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        SendFile(rfi.FileName, rfi.FileBytes)
    End Select

  End Sub

  Public Sub FindQuote(CommandArgs As Singular.Web.CommandArgs)

    'SelectQuote = True

  End Sub

  Public Sub NewQuote(CommandArgs As Singular.Web.CommandArgs)



  End Sub

End Class