﻿Quotes = {

  QuantitySet: function (self) {

  },
  FlightsSet: function (self) {
    Quotes.SetAllAmounts(self.GetParent().GetParent());
  },
  AccommodationSet: function (self) {
    Quotes.SetAllAmounts(self.GetParent().GetParent());
  },
  CarHireSet: function (self) {
    Quotes.SetAllAmounts(self.GetParent().GetParent());
  },
  SnTSet: function (self) {
    Quotes.SetAllAmounts(self.GetParent().GetParent());
  },
  FeeSet: function (self) {
    Quotes.SetAllAmounts(self.GetParent().GetParent());
  },
  CompanyRateCardIDSet: function (self) {

  },
  SupplierRateCardIDSet: function (self) {

  },
  RateSet: function (self) {

  },
  SetTotalDetailAmount: function (QuoteHeadingDetail) {
    QuoteHeadingDetail.SuspendChanges = true;
    var Flights = (QuoteHeadingDetail.Flights() ? QuoteHeadingDetail.Flights() : 0);
    var Accommodation = (QuoteHeadingDetail.Accommodation() ? QuoteHeadingDetail.Accommodation() : 0);
    var CarHire = (QuoteHeadingDetail.CarHire() ? QuoteHeadingDetail.CarHire() : 0);
    var SnT = (QuoteHeadingDetail.SnT() ? QuoteHeadingDetail.SnT() : 0);
    var Fee = (QuoteHeadingDetail.Fee() ? QuoteHeadingDetail.Fee() : 0);
    QuoteHeadingDetail.Amount(Flights + Accommodation + CarHire + SnT + Fee);
    QuoteHeadingDetail.SuspendChanges = false;
  },
  SetTotalHeaderAmount: function (Header) {
    Header.SuspendChanges = true;
    Header.Amount(0);
    if (Header.AmountOverride()) {
      Header.Amount(Header.AmountOverride());
    } else {
      Header.QuoteHeadingDetailList().Iterate(function (Detail, DetailIndex) {
        Header.Amount(Header.Amount() + Detail.Amount());
      });
    }
    Header.SuspendChanges = false;
  },
  SetTotalQuoteAmount: function (Quote) {
    Quote.SuspendChanges = true;
    Quote.QuoteAmount(0);
    if (Quote.QuoteOverrideAmount()) {
      Quote.QuoteAmount(Quote.QuoteOverrideAmount());
    } else {
      Quote.QuoteHeadingList().Iterate(function (Header, HeaderIndex) {
        Quote.QuoteAmount(Quote.QuoteAmount() + Header.Amount());
      });
    }
    Quote.SuspendChanges = false;
  },
  SetAllAmounts: function (Quote) {
    Quote.QuoteHeadingList().Iterate(function (Header, Hindx) {
      Header.QuoteHeadingDetailList().Iterate(function (HeaderDetail, HDindx) {
        Quotes.SetTotalDetailAmount(HeaderDetail);
      });
      Quotes.SetTotalHeaderAmount(Header);
    });
    Quotes.SetTotalQuoteAmount(Quote);
  },

  QuoteHeadingAmountSet: function (QuoteHeading) {
    //Quotes.SetAllAmounts(QuoteHeading.GetParent());
    //if (QuoteHeading.AmountOverride()) {
    //  Quotes.SetTotalHeaderAmount(QuoteHeading);
    //  Quotes.SetTotalQuoteAmount(QuoteHeading.GetParent());
    //} else {
    //  Quotes.SetTotalHeaderAmount(QuoteHeading);
    //  Quotes.SetTotalQuoteAmount(QuoteHeading.GetParent());
    //}
  },
  QuoteOverrideAmountSet: function (Quote) {
    if (Quote.QuoteOverrideAmount()) {
      Quote.QuoteOverrideBy(ViewModel.CurrentUserID());
      Quote.QuoteOverrideByName(ViewModel.CurrentUserName());
      Quote.QuoteOverrideDateTime(new Date());
      Quote.QuoteOverrideReason("");
    } else {
      Quote.QuoteOverrideBy(null);
      Quote.QuoteOverrideByName("");
      Quote.QuoteOverrideDateTime(null);
      Quote.QuoteOverrideReason("");
    }
    Quotes.SetAllAmounts(Quote);
  },
  QuoteHeadingOverrideAmountSet: function (QuoteHeading) {
    if (QuoteHeading.AmountOverride()) {
      QuoteHeading.AmountOverrideBy(ViewModel.CurrentUserID());
      QuoteHeading.AmountOverrideByName(ViewModel.CurrentUserName());
      QuoteHeading.AmountOverrideDateTime(new Date());
      QuoteHeading.AmountOverrideReason("");
    } else {
      QuoteHeading.AmountOverrideBy(null);
      QuoteHeading.AmountOverrideByName("");
      QuoteHeading.AmountOverrideDateTime(null);
      QuoteHeading.AmountOverrideReason("");
    }
    Quotes.SetAllAmounts(QuoteHeading.GetParent());
  },

  QuoteOverrideByValid: function (Value, Rule, CtlError) {
    var Quote = CtlError.Object;
    if (Quote.QuoteOverrideAmount() && !Quote.QuoteOverrideBy()) {
      CtlError.AddError("Quote Override By is required");
    }
    Quote = null;
  },
  QuoteOverrideDateTimeValid: function (Value, Rule, CtlError) {
    var Quote = CtlError.Object;
    if (Quote.QuoteOverrideAmount() && !Quote.QuoteOverrideDateTime()) {
      CtlError.AddError("Quote Override Date is required");
    }
    Quote = null;
  },
  QuoteOverrideReasonValid: function (Value, Rule, CtlError) {
    var Quote = CtlError.Object;
    if (Quote.QuoteOverrideAmount() && Quote.QuoteOverrideReason().trim().length == 0) {
      CtlError.AddError("Quote Override Reason is required");
    }
    Quote = null;
  },

  QuoteHeadingAmountOverrideByValid: function (Value, Rule, CtlError) {
    var QuoteHeading = CtlError.Object;
    if (QuoteHeading.AmountOverride() && !QuoteHeading.AmountOverrideBy()) {
      CtlError.AddError("Override By is required");
    }
    QuoteHeading = null;
  },
  QuoteHeadingAmountOverrideDateTimeValid: function (Value, Rule, CtlError) {
    var QuoteHeading = CtlError.Object;
    if (QuoteHeading.AmountOverride() && !QuoteHeading.AmountOverrideDateTime()) {
      CtlError.AddError("Override Date is required");
    }
    QuoteHeading = null;
  },
  QuoteHeadingAmountOverrideReasonValid: function (Value, Rule, CtlError) {
    var QuoteHeading = CtlError.Object;
    if (QuoteHeading.AmountOverride() && QuoteHeading.AmountOverrideReason().trim().length == 0) {
      CtlError.AddError("Override Reason is required");
    }
    QuoteHeading = null;
  },

  QuoteToString: function (Quote) {
    return Quote.EventName();
  },
  QuoteHeadingToString: function (QuoteHeading) {
    if (!QuoteHeading.CostTypeID()) {
      return "";
    }
    var CostType = ClientData.ROCostTypeList.Find("CostTypeID", QuoteHeading.CostTypeID());
    return CostType.CostType + " - " + QuoteHeading.Description();
  },
  QuoteHeadingDetailToString: function (QuoteHeadingDetail) {
    if (!QuoteHeadingDetail.CostTypeID()) {
      return ""
    }
    var CostType = ClientData.ROCostTypeList.Find("CostTypeID", QuoteHeadingDetail.CostTypeID());
    return CostType.CostType
  },
  SetSartDate: function (Quote) {
    ViewModel.ROQuoteListPagingManager().Refresh();
  },
  SetEventName: function (Quote) {
    ViewModel.ROQuoteListPagingManager().Refresh();
  },
SetEndDate: function (Quote) {
  ViewModel.ROQuoteListPagingManager().Refresh();
},
SetProductionTypeEventType: function (Quote) {
  ViewModel.ROQuoteListPagingManager().Refresh();
}
}
