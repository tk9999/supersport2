﻿Imports OBLib.Notifications
Imports Singular.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports Singular

Public Class NotificationMaintenance
  Inherits OBPageBase(Of NotificationMaintenanceVM)

End Class

Public Class NotificationMaintenanceVM
  Inherits OBViewModel(Of NotificationMaintenanceVM)

  Public Property CurrentNotificationGroupID As Integer?
  Public Property ShowGroupContacts As Boolean
  Public Property UserNotifictionGroups As UserNotificationGroupList
  Public Property NewNotificationGroup As NotificationGroup
  Public Property NewNotificationGroupContact As NotificationGroupContact
  Public Property NewNotificationGroupContactList As NotificationGroupContactList
  <InitialDataOnly>
  Public Property RONotificationGroups As OBLib.Notifications.ReadOnly.RONotificationGroupList
  <InitialDataOnly>
  Public Property CurrentUserNotifictionGroupContacts As OBLib.Notifications.ReadOnly.ROUserNotificationGroupContactList
  Public Property ContactsToDelete As List(Of Integer)


  <InitialDataOnly>
  Public Property NotificationGroupsBusy As Boolean

  <InitialDataOnly>
  Public Property NotificationGroupContactsBusy As Boolean

  'Public Property NotificationGroupList As NotificationGroupList
  'Public Property NotificationGroupListPagedManager As Singular.Web.Data.PagedDataManager(Of GroupNotificationVM)
  'Public Property NotificationGroupListCriteria As NotificationGroupList.Criteria

  Protected Overrides Sub Setup()

    UserNotifictionGroups = OBLib.Notifications.UserNotificationGroupList.GetUserNotificationGroupList(OBLib.Security.Settings.CurrentUserID, Nothing)
    RONotificationGroups = OBLib.Notifications.ReadOnly.RONotificationGroupList.GetRONotificationGroupList(Nothing, OBLib.Security.Settings.CurrentUserID, Nothing)
    ShowGroupContacts = False
    CurrentUserNotifictionGroupContacts = Nothing
    NewNotificationGroupContactList = New OBLib.Notifications.NotificationGroupContactList
    ContactsToDelete = New List(Of Integer)
    'NotificationGroupList = New NotificationGroupList
    'NotificationGroupListCriteria = New NotificationGroupList.Criteria
    'NotificationGroupListPagedManager = New Singular.Web.Data.PagedDataManager(Of GroupNotificationVM)(Function(d) Me.NotificationGroupList, Function(d) Me.NotificationGroupListCriteria, "", 25)
    'NotificationGroupList = OBLib.Notifications.NotificationGroupList.GetNotificationGroupList()
    'ClientDataProvider.AddDataSource("ROUserNotificationGroupList", OBLib.CommonData.Lists.ROUserNotificationGroupList, False)

  End Sub


  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "NewNotificationGroup"
        CreateNewNotificationGroup(CommandArgs)
      Case "CancelNewNotificationGroup"
        CancelNewNotificationGroup(CommandArgs)
      Case "SaveNewNotificationGroup"
        SaveNewNotificationGroup(CommandArgs)
      Case "LinkToGroup"
        LinkToGroup(CommandArgs)
      Case "UnLinkFromGroup"
        UnLinkFromGroup(CommandArgs)
      Case "SaveNewGroupContacts"
        SaveNewGroupContacts(CommandArgs)
      Case "DeleteSelectedContacts"
        DeleteSelectedContacts(CommandArgs)
    End Select

  End Sub

  Private Sub CreateNewNotificationGroup(CommandArgs As Web.CommandArgs)
    NewNotificationGroup = New NotificationGroup
  End Sub

  Private Sub CancelNewNotificationGroup(CommandArgs As Web.CommandArgs)
    NewNotificationGroup = Nothing
  End Sub

  Private Sub SaveNewNotificationGroup(CommandArgs As Web.CommandArgs)

    Dim ngl As New NotificationGroupList
    ngl.Add(NewNotificationGroup)
    Dim sh As SaveHelper = TrySave(ngl)
    If sh.Success Then
      ngl = sh.SavedObject
      NewNotificationGroup = ngl(0)
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If

  End Sub

  Private Sub LinkToGroup(CommandArgs As Web.CommandArgs)
    Dim GroupID As Integer? = CommandArgs.ClientArgs.NotificationGroupID
    Dim ungl As UserNotificationGroupList = New UserNotificationGroupList
    ungl.AddNew()
    ungl(0).NotificationGroupID = GroupID
    ungl(0).UserID = CurrentUserID
    Dim sh As SaveHelper = TrySave(ungl)
    If sh.Success Then
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False)
    End If
  End Sub

  Private Sub UnLinkFromGroup(CommandArgs As Web.CommandArgs)
    Dim GroupID As Integer? = CommandArgs.ClientArgs.NotificationGroupID
    Dim ungl As UserNotificationGroupList = OBLib.Notifications.UserNotificationGroupList.GetUserNotificationGroupList(CurrentUserID, GroupID)
    ungl.Clear()
    Dim sh As SaveHelper = TrySave(ungl)
    If sh.Success Then
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False)
    End If
  End Sub

  Private Sub SaveNewGroupContacts(CommandArgs As Web.CommandArgs)

    Dim ngl As NotificationGroupList = OBLib.Notifications.NotificationGroupList.GetNotificationGroupList(Nothing, CurrentNotificationGroupID)
    ngl(0).NotificationGroupContactList.AddRange(NewNotificationGroupContactList)
    Dim sh As SaveHelper = TrySave(ngl)
    If sh.Success Then
      ngl = sh.SavedObject
      NewNotificationGroupContactList = New NotificationGroupContactList
      Dim ROGroupList As OBLib.Notifications.ReadOnly.RONotificationGroupList = OBLib.Notifications.ReadOnly.RONotificationGroupList.GetRONotificationGroupList(Nothing, CurrentUserID, CurrentNotificationGroupID)
      CommandArgs.ReturnData = New Singular.Web.Result(True) With {.Data = ROGroupList(0)}
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False)
    End If

  End Sub

  Private Sub DeleteSelectedContacts(CommandArgs As Web.CommandArgs)

    Dim itemXMLIDs As String = OBLib.OBMisc.IntegerListToXML(ContactsToDelete)

    Dim ngl As NotificationGroupContactList = OBLib.Notifications.NotificationGroupContactList.GetNotificationGroupContactList(itemXMLIDs)
    ngl.Clear()
    Dim sh As SaveHelper = TrySave(ngl)
    If sh.Success Then
      ngl = sh.SavedObject
      ContactsToDelete = New List(Of Integer)
      CommandArgs.ReturnData = New Singular.Web.Result(True) 'With {.Data = ROGroupList(0)}
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False)
    End If


  End Sub

End Class