﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="NotificationMaintenance.aspx.vb" Inherits="NewOBWeb.NotificationMaintenance" %>

<%@ Import Namespace="Singular.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <script type="text/javascript" src="../Scripts/BusinessObjects/Notifications.js"></script>
  <script type="text/javascript" src="../Scripts/Pages/NotificationMaintenancePage.js"></script>
  <style type="text/css">
    body {
      background-color: #F0F0F0;
    }

    tr.selected td, tr.selected input, tr.selected span,
    .table-hover > tbody > tr.selected:hover > td {
      background-color: #19b698;
      border-color: transparent;
      color: white !important;
    }

      tr.selected td:hover,
      tr.selected input:hover,
      tr.selected span:hover,
      tr.selected td:active,
      tr.selected input:active,
      tr.selected span:active,
      tr.selected td:focus,
      tr.selected input:focus,
      tr.selected span:focus {
        background-color: #fff;
        border-color: transparent;
        color: black !important;
      }

    .loading {
      display: initial;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <% Using h = Helpers
      
      'With h.Bootstrap.Row
      '  'With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
      '  '  With .Helpers.Bootstrap.FlatBlock("My Groups", False, False)
      '  '    With .ContentTag
      '  '      With .Helpers.Bootstrap.Row
      '  '        With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, ,
      '  '                                       Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
      '  '                                       "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
      '  '          '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
      '  '        End With
      '  '      End With
      '  '    End With
      '  '  End With
      '  'End With
      '  'With .Helpers.Bootstrap.FlatBlock("", False, False)
      '  '  With .ContentTag
            
      '  '  End With
      '  'End With
      'End With
      
      'With h.Bootstrap.Row
      '  With .Helpers.Bootstrap.FlatBlock("My Groups", False, False)
          
      '  End With
      'End With
      
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
          With .Helpers.Bootstrap.FlatBlock("Notification Groups", True, False)
            With .AboveContentTag
              If Singular.Security.HasAccess("Notifications.Can Create Notification Group") Then
                .Helpers.Bootstrap.Button(, "New Group", Singular.Web.BootstrapEnums.Style.Primary, ,
                                          Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                          "fa-plus-circle", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.AddNewGroup()")
              End If
            End With
            With .ContentTag
              'With .Helpers.Bootstrap.Row
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.TableFor(Of OBLib.Notifications.ReadOnly.RONotificationGroup)(Function(c) ViewModel.RONotificationGroups,
                                                        False, False, False, False, True, True, True)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                  With .FirstRow
                    .AddClass("selectable")
                    With .AddReadOnlyColumn(Function(d As OBLib.Notifications.ReadOnly.RONotificationGroup) d.NotificationGroup)
                      '.AddBinding(Singular.Web.KnockoutBindingString.click, "RONotificationGroupBO.ViewContacts($data)")
                    End With
                    With .AddColumn("")
                      With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                     "fa-user", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.GetContacts($data, null)")
                        .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.ContactCount().toString() + ' Contacts'")
                      End With
                    End With
                    With .AddColumn("")
                      With .Helpers.Bootstrap.Button(, "You are linked", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-check-square-o", , Singular.Web.PostBackType.None, "")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.ReadOnly.RONotificationGroup) d.HasAccessToGroup)
                      End With
                      With .Helpers.Bootstrap.Button(, "You are not linked", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-lock", , Singular.Web.PostBackType.None, "")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.ReadOnly.RONotificationGroup) Not d.HasAccessToGroup)
                      End With
                    End With
                    With .AddColumn("")
                      With .Helpers.Bootstrap.Button(, "Unlink", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "", , Singular.Web.PostBackType.None, "RONotificationGroupBO.Unlink($data)")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.ReadOnly.RONotificationGroup) d.HasAccessToGroup)
                      End With
                      With .Helpers.Bootstrap.Button(, "Link", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "", , Singular.Web.PostBackType.None, "RONotificationGroupBO.Link($data)")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.ReadOnly.RONotificationGroup) Not d.HasAccessToGroup)
                      End With
                    End With
                  End With
                End With
              End With
              'End With
            End With
          End With
          With .Helpers.DivC("loading")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.NotificationGroupsBusy)
            .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
          With .Helpers.Bootstrap.FlatBlock("Group Contacts", True, False)
            .FlatBlockTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.ShowGroupContacts)
            With .AboveContentTag
              With .Helpers.DivC("pull-left")
                If Singular.Security.HasAccess("Notifications.Can Add Contacts To Group") Then
                  .Helpers.Bootstrap.Button(, "New Contact", Singular.Web.BootstrapEnums.Style.Primary, ,
                                            Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                            "fa-plus-circle", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.AddNewContact()")
                End If
              End With
              With .Helpers.DivC("pull-right")
                If Singular.Security.HasAccess("Notifications.Can Delete Contacts From Group") Then
                  .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, ,
                                            Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                            "fa-trash", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.DeleteSelectedContacts()")
                End If
              End With
            End With
            With .ContentTag
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.TableFor(Of OBLib.Notifications.ReadOnly.RONotificationGroupContact)(Function(d) ViewModel.CurrentUserNotifictionGroupContacts,
                                                                                             False, False, False, False, True, True, True)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                  With .FirstRow
                    .AddBinding(Singular.Web.KnockoutBindingString.click, "NotificationMaintenancePage.ContactClicked($data, $element)")
                    'With .AddColumn("")
                    '  .Helpers.Bootstrap.StateButton("$data.IsSelected()", 
                    '                                 "Selected", "Select", , , , "fa-minus", )
                    'End With
                    With .AddColumn(Function(d As OBLib.Notifications.ReadOnly.RONotificationGroupContact) d.NotificationGroupContactName)
                    
                    End With
                    With .AddColumn(Function(d As OBLib.Notifications.ReadOnly.RONotificationGroupContact) d.NotificationGroupContactNumber)
                    
                    End With
                    With .AddColumn(Function(d As OBLib.Notifications.ReadOnly.RONotificationGroupContact) d.NotificationGroupContactEmail)
                    
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.NotificationGroupContactsBusy)
            .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("NewNotificationGroup", "New Notification Group",
                              False, "modal-sm", Singular.Web.BootstrapEnums.Style.Primary)
        With .Body
          .AddClass("row")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.FieldSet("Group Details")
              With .Helpers.With(Of OBLib.Notifications.NotificationGroup)(Function(d) ViewModel.NewNotificationGroup)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroup) d.NotificationGroupName)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroup) d.NotificationGroupName,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroup) d.AutoPopulate)
                  With .Helpers.Div
                    With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Notifications.NotificationGroup) d.AutoPopulate, , , , , , , "btn-sm")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.NotificationGroup) d.AutoPopulate)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroup) d.DisciplineID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroup) d.DisciplineID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.NotificationGroup) d.AutoPopulate)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroup) d.SystemID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroup) d.SystemID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.NotificationGroup) d.AutoPopulate)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroup) d.ProductionAreaID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroup) d.ProductionAreaID,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
          'With .Helpers.Bootstrap.Column(12, 12, 8, 8, 9)
          '  With .Helpers.FieldSet("Group Contacts")
          '    With .Helpers.Div
          '      If Singular.Security.HasAccess("Notifications.Can Add Contacts To Group") Then
          '        .Helpers.Bootstrap.Button(, "New Contact", Singular.Web.BootstrapEnums.Style.Primary, ,
          '                                  Singular.Web.BootstrapEnums.ButtonSize.Small, ,
          '                                  "fa-plus-circle", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.AddNewContact()")
          '      End If
          '    End With
          '    With .Helpers.Div
                
          '    End With
          '  End With
          'End With
        End With
        With .Footer
          With .Helpers.DivC("pull-left")
            With .Helpers.Bootstrap.Button(, "Cancel", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                            "fa-arrow-left", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.CancelNewNotificationGroup($data)")
              '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.NewNotificationGroup.IsValid)
            End With
          End With
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                            "fa-floppy-o", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.SaveNewNotificationGroup($data)")
              '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.NewNotificationGroup().IsValid()")
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("NewNotificationGroupContact", "New Group Contact",
                              False, "modal-lg", Singular.Web.BootstrapEnums.Style.Primary)
        With .Body
          .AddClass("row")
          With .Helpers.Bootstrap.Column(12, 12, 4, 4, 4)
            With .Helpers.FieldSet("New Contact Details")
              With .Helpers.With(Of OBLib.Notifications.NotificationGroupContact)(Function(d) ViewModel.NewNotificationGroupContact)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.CustomErrorBox("", Singular.Web.BootstrapEnums.FlatDreamAlertColor.Danger, , "Errors", False, True)
                    .AlertTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.NotificationGroupContact) Not d.IsValid)
                    With .ErrorTextContainerTag
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesHTML($data)")
                    End With
                  End With
                  With .Helpers.Bootstrap.FlatDreamAlert("No errors found on current contact", BootstrapEnums.FlatDreamAlertColor.Success, "fa-check-square-o", "Contact Valid", True, True)
                    .AlertTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Notifications.NotificationGroupContact) d.IsValid)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactName)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactName,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactNumber)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactNumber,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactEmail)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactEmail,
                                                         Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.DivC("pull-right")
                    With .Helpers.Bootstrap.Button(, "Add", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-arrow-right",
                                                   , Singular.Web.PostBackType.None, "NotificationMaintenancePage.CommitContact()")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.NewNotificationGroupContact().IsValid()")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 8, 8, 8)
            With .Helpers.FieldSet("Contacts that will be added")
              With .Helpers.Bootstrap.TableFor(Of OBLib.Notifications.NotificationGroupContact)(Function(d) ViewModel.NewNotificationGroupContactList,
                                                                                                False, True, False, False, True, True, True)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                With .FirstRow
                  With .AddReadOnlyColumn(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactName)
                    
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactNumber)
                    
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Notifications.NotificationGroupContact) d.NotificationGroupContactEmail)
                    
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-left")
            With .Helpers.Bootstrap.Button(, "Cancel", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                            "fa-arrow-left", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.CancelNewGroupContacts($data)")
              '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.NewNotificationGroup.IsValid)
            End With
          End With
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                            "fa-floppy-o", , Singular.Web.PostBackType.None, "NotificationMaintenancePage.SaveNewGroupContacts($data)")
              '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.NewNotificationGroup().IsValid()")
            End With
          End With
        End With
      End With
      
    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideMenu" runat="server">
</asp:Content>
