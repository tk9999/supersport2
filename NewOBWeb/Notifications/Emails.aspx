﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="Emails.aspx.vb" Inherits="NewOBWeb.Emails" %>

<%@ Import Namespace="OBLib.Maintenance.General.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.ReadOnly" %>

<%@ Import Namespace="OBLib.HR.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.HR.ReadOnly" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.Productions.Areas.ReadOnly" %>

<%@ Import Namespace="OBLib.Notifications.ReadOnly" %>

<%@ Import Namespace="OBLib.Notifications" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    body {
      background-color: #F0F0F0;
    }

    tr.recipient-added td {
      background-color: #7761a7;
      background-image: none;
      color: #fff;
    }

    tr.selectable td {
      cursor: pointer;
    }

    .modal-header-blue {
      background: #4285f4;
      color: #fff;
    }

    #NotificationGroups > tbody > tr > td:first-child,
    #NotificationGroups > tbody > tr > td:first-child {
      width: 25px;
    }

    #NotificationGroups > tbody > tr > td:nth-child(2),
    #NotificationGroups > tbody > tr > td:nth-child(2) {
      width: 50px;
    }

    tr.recipient-added td {
      background-color: #7761a7;
      background-image: none;
      color: #fff;
    }

    .sms-recipients li {
      float: left;
      list-style: none;
    }

    .sms-status-group-error {
      background-color: #E85647;
      background-image: none;
      color: #fff;
    }

    .sms-status-group-pending {
      background-color: #b5b5b5;
      background-image: none;
      color: #fff;
    }

    .sms-status-group-in-transit {
      background-color: #7761a7;
      background-image: none;
      color: #fff;
    }

    .sms-status-group-delivered {
      background-color: #19b698;
      background-image: none;
      color: #fff;
    }

    .sms-status-group-cancelled {
      background-color: #F4A425;
      background-image: none;
      color: #fff;
    }

    .sms-status-group-unknown {
      background-color: #b5b5b5;
      background-image: none;
      color: #fff;
    }

    strong.strong-primary {
      color: #4285f4;
    }

    strong.strong-danger {
      color: #E85647;
    }

    .tags-container, .tags-drop, .tags-search, .tags-search input {
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }

    .tags-container {
      margin: 0;
      position: relative;
      display: inline-block;
      zoom: 1;
      vertical-align: middle;
    }

    .tags-container-multi .tags-choices {
      padding: 5px;
      background: #fff;
      box-shadow: none;
    }

    .tags-container-multi .tags-choices {
      min-height: 26px;
    }

    .tags-container-multi .tags-choices {
      /*min-height: 100px;
      max-height: 200px;*/
      margin: 0;
      padding: 0;
      position: relative;
      cursor: text;
      /*overflow: hidden;
      overflow-y: scroll;*/
      background-color: #fff;
    }

      .tags-container-multi .tags-choices .tags-search-choice {
        border: none;
        border-radius: 0;
        box-shadow: none;
        padding: 3px 5px 5px 18px;
      }

      .tags-container-multi .tags-choices .tags-search-choice {
        padding: 4px;
        margin: 2px 2px 2px 2px;
        position: relative;
        line-height: 13px;
        cursor: default;
        background-clip: padding-box;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        width: 300px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 1em;
      }

      .tags-container-multi .tags-choices li {
        float: left;
        list-style: none;
      }

    .tags-container-multi .tags-search-choice-close {
      left: 3px;
    }

    .tags-search-choice-close {
      color: #fff;
    }

    a {
      color: #816bb1;
      text-decoration: none;
      outline: 0 none;
    }

    div.modal-header > button.close {
      background: transparent;
      color: #fff;
      opacity: 1;
    }

    tr.selectable td {
      cursor: pointer;
    }

    div.max-height-400 {
      /*height: 400px;*/
      max-height: 400px;
      /*min-height: 400px;*/
      overflow-y: auto;
    }

    /*.modal-content {
      background: #f0f0f0;
    }*/
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Notifications.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/EmailsPage.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.Toolbar
        .Helpers.MessageHolder()
      End With

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock(, , True, )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                                 "fa-search", , Singular.Web.PostBackType.None, "EmailsPage.Find()")
                  End With
                  With .Helpers.Bootstrap.Button(, "Create an Email", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                                 "fa-envelope-o", , Singular.Web.PostBackType.None, "EmailsPage.NewEmail()")
                  End With
                  With .Helpers.Bootstrap.Button(, "Generate Multiple Emails", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                                 "fa-gears", , Singular.Web.PostBackType.None, "EmailsPage.NewEmailBatch()")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visibleA, "ViewModel.FindEmail()")

        With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
          With .Helpers.Bootstrap.FlatBlock("Search Criteria", , , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.With(Of Email.ReadOnly.ROEmailSearchList.Criteria)("ViewModel.ROEmailSearchListCriteria()")
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedStartDate).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedEndDate).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedByMe).Style.Width = "100%"
                      With .Helpers.Bootstrap.StateButton(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedByMe, , , , , , , "btn-sm")
                        .Button.AddClass("btn-block")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedByOther).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d As Email.ReadOnly.ROEmailSearchList.Criteria) d.CreatedByOther, Singular.Web.BootstrapEnums.InputSize.Small, , )
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.CreatedByMe()")
                      End With
                    End With
                  End With
                End With
                'With .Helpers.DivC("text-center")
                '  With .Helpers.DivC("i-circle warning")
                '    .Helpers.Bootstrap.FontAwesomeIcon("fa-warning")
                '  End With
                '  .Helpers.HTML.Heading4("Under Construction!")
                '  With .Helpers.HTMLTag("p")
                '    .Helpers.HTML("There will be criteria here!")
                '  End With
                'End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 9, 9)
          With .Helpers.Bootstrap.FlatBlock("Emails", , , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.PullLeft
                    With .Helpers.Bootstrap.Button(, "Select All on Page", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                  Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-sort-amount-asc", , ,
                                                  "EmailsPage.SelectAllOnPage()")
                    End With
                  End With
                  With .Helpers.Bootstrap.PullRight
                    With .Helpers.Bootstrap.Button(, "Deleted Selected", Singular.Web.BootstrapEnums.Style.Danger, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-edit", , ,
                                                   "EmailsPage.DeleteSelected()")
                    End With
                    With .Helpers.Bootstrap.Button(, "Send Selected", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                   "fa-cloud-upload", , Singular.Web.PostBackType.None, "EmailsPage.SendSelectedEmails()", )
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.PagedGridFor(Of Email.ReadOnly.ROEmailSearch)("ViewModel.ROEmailSearchListManager",
                                                                                        "ViewModel.ROEmailSearchList()",
                                                                                        False, False, False, False, True, True, True, ,
                                                                                        Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                        "", False, True)
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y no-border-x"
                    With .FirstRow
                      .AddClass("items")
                      With .AddColumn("")
                        .Style.Width = "100px"
                        With .Helpers.Bootstrap.StateButton(Function(d As Email.ReadOnly.ROEmailSearch) d.IsSelected, "Select", "Select", , , , , )
                        End With
                        With .Helpers.Bootstrap.Button(, "Edit Email", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , , "EmailsPage.EditSingleEmail($data.EmailID())")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsBatch()")
                        End With
                        With .Helpers.Bootstrap.Button(, "Edit Batch", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , , "EmailsPage.EditBatch($data.EmailBatchID())")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsBatch()")
                        End With
                      End With
                      With .AddColumn("Send Status", , , , , "center")
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                        .Style.Width = "120px"
                        With .Helpers.Bootstrap.Label("Pending", Singular.Web.BootstrapEnums.Style.DefaultStyle, , "fa-pause", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROEmailSearch) d.SentCount = 0)
                        End With
                        With .Helpers.Bootstrap.Label("Sent", Singular.Web.BootstrapEnums.Style.Success, , "fa-check-square-o", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROEmailSearch) d.AllSent)
                        End With
                        With .Helpers.Bootstrap.Label("Failed to Send", Singular.Web.BootstrapEnums.Style.Danger, , "fa-meh-o", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROEmailSearch) (d.FailedToSendCount = (d.RecipientCount + d.CCCount)) Or (d.FailedToSendCount > 0 And d.SentCount = 0))
                        End With
                        With .Helpers.Bootstrap.Label("Partially Sent", Singular.Web.BootstrapEnums.Style.Warning, , "fa-meh-o", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROEmailSearch) d.SentCount > 0)
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROEmailSearch) d.SentDescription)
                        .Style.Width = "150px"
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROEmailSearch) d.BriefDescription)
                        .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.Description()")
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROEmailSearch) d.MessagePreview)
                      End With
                      With .AddColumn("Attachments")
                        With .Helpers.HTMLTag("a")
                          .Attributes("href") = "#"
                          .AddClass("badge badge-info badge-square")
                          .AddBinding(Singular.Web.KnockoutBindingString.click, "ROEmailSearchBO.AttachmentsClicked($data)")
                          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.AttachmentCount() > 0")
                          With .Helpers.HTMLTag("span")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.AttachmentCount()")
                          End With
                          With .Helpers.HTMLTag("span")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "ROEmailSearchBO.AttachmentCountDescription($data)")
                          End With
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROEmailSearch) d.CreatedBy)
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROEmailSearch) d.CreatedDateString)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

      End With

      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visibleA, "ViewModel.CreateEmail()")
        With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
          .AddClass("col-no-left-padding")
          With .Helpers.Bootstrap.FlatBlock("New Email", , False, )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.With(Of OBLib.Notifications.Email.SoberEmail)(Function(d) ViewModel.CurrentEmail)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                     Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-paper-plane-o",
                                                     , Singular.Web.PostBackType.None, "EmailsPage.SaveEmail()")
                        ', "EmailsPage.SaveEmail()"
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d As OBLib.Notifications.Email.SoberEmail) d.IsValid)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.EmailTemplateTypeID)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.EmailTemplateTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Template to use..")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.Subject)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.Subject, Singular.Web.BootstrapEnums.InputSize.Small, , "Subject...")
                      End With
                    End With
                    'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    '  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.ToEmailAddress)
                    '  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.ToEmailAddress, Singular.Web.BootstrapEnums.InputSize.Small, , "To..")
                    '    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                    '  End With
                    'End With
                    'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    '  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.CCEmailAddresses)
                    '  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.CCEmailAddresses, Singular.Web.BootstrapEnums.InputSize.Small, , "CC..")
                    '    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                    '  End With
                    'End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.HTMLBody)
                      With .Helpers.TextBoxFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.HTMLBody)
                        .AddClass("form-control")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                          .AddClass("col-lg-offset-4 col-xl-offset-4")
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.Email.SoberEmail) d.DateToSend)
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                              .Helpers.Bootstrap.FormControlFor(Function(d) d.DateToSend, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                              .Helpers.Bootstrap.TimeEditorFor(Function(d) d.DateToSend, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.FlatBlock("Attachments", , False, )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.With(Of OBLib.Notifications.Email.SoberEmail)(Function(d) ViewModel.CurrentEmail)
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Notifications.Email.SoberEmailAttachment)(Function(d As OBLib.Notifications.Email.SoberEmail) d.SoberEmailAttachmentList,
                                                                                                          True, True, False, False, True, True, True)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y no-border-x"
                        With .FirstRow
                          With .AddColumn("")
                            .Helpers.DocumentManager()
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 6, 8, 9)
          .AddClass("col-no-right-padding")
          With .Helpers.Bootstrap.FlatBlock("Email Recipients", , False, )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Button(, "Select Recipients", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-users", ,
                                                   Singular.Web.PostBackType.None, "EmailsPage.SelectRecipientsSingleEmail()")
                      '.Button.AddClass("btn-block")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.With(Of OBLib.Notifications.Email.SoberEmail)(Function(d) ViewModel.CurrentEmail)
                    With .Helpers.DivC("tags-container tags-container-multi tags")
                      .Style.Width = "100%"
                      With .Helpers.Bootstrap.UnorderedList("tags-choices")
                        With .ListTag
                          With .Helpers.ForEachTemplate(Of OBLib.Notifications.Email.SoberEmail)(Function(d As OBLib.Notifications.Email.SoberEmail) d.SoberEmailRecipientList)
                            With .Helpers.HTMLTag("li")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "SoberEmailRecipientBO.GetCssClass($data)")
                              .AddClass("tags-search-choice")
                              With .Helpers.DivC("")
                                With .Helpers.HTMLTag("span")
                                  With .Helpers.HTMLTag("a")
                                    .Attributes("href") = "#"
                                    .Attributes("onclick") = "return false;"
                                    .AddClass("tags-search-choice-close")
                                    .Attributes("tabindex") = "-1"
                                    .Helpers.Bootstrap.FontAwesomeIcon("fa-times")
                                    .AddBinding(Singular.Web.KnockoutBindingString.click, "SoberEmailRecipientBO.RemoveFromEmail($data)")
                                    .AddBinding(Singular.Web.KnockoutBindingString.if, "SoberEmailRecipientBO.CanDeleteClient($data)")
                                  End With
                                End With
                                With .Helpers.HTMLTag("span")
                                  .Helpers.Bootstrap.FontAwesomeIcon("fa-user")
                                End With
                                With .Helpers.HTMLTag("span")
                                  .AddBinding(Singular.Web.KnockoutBindingString.text, "$data.EmailAddress()")
                                End With
                                'With .Helpers.HTMLTag("span")
                                '  .AddBinding(Singular.Web.KnockoutBindingString.text, "$data.CellNo()")
                                'End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)

                End With
              End With
            End With
          End With
        End With
      End With




      With h.Bootstrap.Row
        .AddBinding(Singular.Web.KnockoutBindingString.visibleA, "ViewModel.GenerateEmail()")
        With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
          With .Helpers.Bootstrap.FlatBlock("Batch Criteria", , , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.With(Of Email.EmailBatch)(Function(d) ViewModel.CurrentEmailBatch)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.EmailBatch) d.EmailBatchTemplateID)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.EmailBatchTemplateID, Singular.Web.BootstrapEnums.InputSize.Small)
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "EmailBatchBO.CanEdit('EmailBatchTemplateID', $data)")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.EmailBatch) d.BatchName)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.BatchName, Singular.Web.BootstrapEnums.InputSize.Small)
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "EmailBatchBO.CanEdit('BatchName', $data)")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As Email.EmailBatch) d.BatchDescription)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.BatchDescription, Singular.Web.BootstrapEnums.InputSize.Small)
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "EmailBatchBO.CanEdit('BatchDescription', $data)")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.StartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "EmailBatchBO.CanEdit('StartDate', $data)")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.EndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "EmailBatchBO.CanEdit('EndDate', $data)")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.SelectRecipientsManually).Style.Width = "100%"
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButton(Function(d As Email.EmailBatch) d.SelectRecipientsManually, "Yes", "Yes", , , , , "btn-sm")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                          With .Helpers.Bootstrap.StateButton(Function(d As Email.EmailBatch) d.SelectRecipientsManually, "No", "No", "btn-default", "btn-primary", "fa-minus", "fa-check-square-o", "btn-sm")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.EmailBatch) Not d.SelectRecipientsManually)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.ProductionAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.EmailBatch) Not d.SelectRecipientsManually)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.ContractTypeID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.EmailBatch) Not d.SelectRecipientsManually)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.LabelFor(Function(d As Email.EmailBatch) d.DisciplineID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Email.EmailBatch) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.EmailBatch) d.SelectRecipientsManually)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.Button(, "Select Recipients", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-users", , Singular.Web.PostBackType.None, "EmailsPage.SelectEmailBatchRecipientsManually()")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Generate Emails", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                     Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-gears", ,
                                                     Singular.Web.PostBackType.Ajax, "EmailsPage.GenerateBatch()")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid() && $data.IsNew()")
                        .Button.AddClass("btn-block")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.With(Of OBLib.Notifications.Email.EmailBatch)(Function(d) ViewModel.CurrentEmailBatch)
            With .Helpers.Bootstrap.FlatBlock("Batch Recipients", , , )
              .FlatBlockTag.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.SelectRecipientsManually()")
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Notifications.Email.EmailBatchRecipient)(Function(d As OBLib.Notifications.Email.EmailBatch) d.EmailBatchRecipientList,
                                                                                                         False, True, False, False, True, True, True)
                        .RemoveButton.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y no-border-x"
                        With .FirstRow
                          With .AddReadOnlyColumn(Function(d As OBLib.Notifications.Email.EmailBatchRecipient) d.EmailAddress)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 9, 9)
          With .Helpers.Bootstrap.FlatBlock("Generated Emails", , , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.With(Of OBLib.Notifications.Email.EmailBatch)(Function(d) ViewModel.CurrentEmailBatch)
                    With .Helpers.Bootstrap.PullLeft
                      With .Helpers.Bootstrap.Button(, "Send All", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                     "fa-bomb", , Singular.Web.PostBackType.None, "EmailsPage.SendAllEmailsForBatch($data)", )
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
                      End With
                      With .Helpers.Bootstrap.Button(, "Send Selected", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                     "fa-cloud-upload", , Singular.Web.PostBackType.None, "EmailsPage.SendSelectedEmailsForBatch($data)", )
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.PagedGridFor(Of Email.ReadOnly.ROBatchEmail)("ViewModel.ROCurrentBatchEmailListManager",
                                                                                       "ViewModel.ROCurrentBatchEmailList()",
                                                                                       False, False, False, False, True, True, True, ,
                                                                                       Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                       , False, True)
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y no-border-x"
                    With .FirstRow
                      .AddClass("items")
                      .AddBinding(Singular.Web.KnockoutBindingString.css, "EmailsPage.IsROSoberEmailSelectedRowCss($data)")
                      With .AddColumn("")
                        .Style.Width = "50px"
                        With .Helpers.Bootstrap.Button(, , , , , , "fa-check-square-o", , , "EmailsPage.OnROBatchEmailSelected($data)")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "EmailsPage.IsROSoberEmailSelectedButtonCss($data)")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.html, "EmailsPage.IsROSoberEmailSelectedButtonHTML($data)")
                          '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsSelected()")
                          .Button.AddClass("btn-block")
                          .Button.Style("z-index") = "1000" 'Row click is interfering with the click on this button, setting z-index high so that it will always be the first elemt click registered in that cell of that row, instead of the row itself
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROBatchEmail) d.ToEmailAddress)
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROBatchEmail) d.Subject)
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROBatchEmail) d.Body)
                      End With
                      With .AddColumn("Attachment")
                        With .Helpers.HTMLTag("a")
                          .AddClass("badge badge-info badge-square")
                          .Attributes("href") = "#"
                          .AddBinding(Singular.Web.KnockoutBindingString.click, "ROBatchEmailBO.AttachmentClicked($data)")
                          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.EmailAttachmentID() > 0")
                          .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.AttachmentName()")
                          'With .Helpers.HTMLTag("span")
                          '  .AddClass("badge")
                          '  .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.AttachmentName()")
                          'End With
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROBatchEmail) d.SentDate)
                      End With
                      With .AddReadOnlyColumn(Function(d As Email.ReadOnly.ROBatchEmail) d.NotSentError)
                      End With
                      With .AddColumn("Send Status", , , , , "center")
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                        With .Helpers.Bootstrap.Label("Pending", Singular.Web.BootstrapEnums.Style.DefaultStyle, , "fa-pause", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROBatchEmail) d.SendStatus = "Pending")
                        End With
                        With .Helpers.Bootstrap.Label("Failed to Send", Singular.Web.BootstrapEnums.Style.Danger, , "fa-meh-o", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROBatchEmail) d.SendStatus = "Failed to Send")
                        End With
                        With .Helpers.Bootstrap.Label("Sent", Singular.Web.BootstrapEnums.Style.Success, , "fa-check-square-o", , )
                          .LabelTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As Email.ReadOnly.ROBatchEmail) d.SendStatus = "Sent")
                        End With
                      End With
                      With .AddColumn("Actions")
                        With .Helpers.Bootstrap.Button(, "Send", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-rocket", , Singular.Web.PostBackType.None, )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ROBatchEmailBO.CanView('SendButton', $data)")
                        End With
                        With .Helpers.Bootstrap.Button(, "Resend", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-paper-plane-o", , Singular.Web.PostBackType.None, )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ROBatchEmailBO.CanView('ResendButton', $data)")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With



      With h.Bootstrap.Dialog("SelectRecipients", "Select Recipients", False, "modal-xl", Singular.Web.BootstrapEnums.Style.Primary)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
              With .Helpers.Bootstrap.FlatBlock("Areas", , , , )
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "EmailsPage.OnSubDeptSelect($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      '.AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "EmailsPage.OnAreaSelect($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.FlatBlock("Contract Types")
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of ROContractType)(Function(d) ViewModel.SelectableContractTypes)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , "", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "EmailsPage.OnContractTypeSelect($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
              With .Helpers.Bootstrap.FlatBlock("Disciplines")
                .FlatBlockTag.AddClass("block-flat-small")
                With .ContentTag
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                   "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "EmailsPage.SelectAllDisciplines()")
                    End With
                    With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                   "fa-eraser", , Singular.Web.PostBackType.None, "EmailsPage.ClearDisciplines()")
                    End With
                  End With
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .AddClass("max-height-400")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of RODisciplineSelect)(Function(d) ViewModel.SelectableDisciplines)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.StateDiv(Function(c) c.IsSelected(), "", "", , , , "", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "EmailsPage.OnDisciplineSelect($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              With .Helpers.Bootstrap.FlatBlock("Human Resources")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                        With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "EmailsPage.SelectAllHumanResources()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                        With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-eraser", , Singular.Web.PostBackType.None, "EmailsPage.ClearRecipients()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                        With .Helpers.Bootstrap.Button(, "Add Selected To Email", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-plus-circle", , Singular.Web.PostBackType.None, "EmailsPage.AddSelectedToEmail()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                      'With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceContactList.Criteria)("ViewModel.SelectableHumanResourcesCriteria()")
                      '  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, Singular.Web.BootstrapEnums.InputSize.Small, , "First Name...")
                      '    End With
                      '  End With
                      '  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, Singular.Web.BootstrapEnums.InputSize.Small, , "Preferred Name...")
                      '    End With
                      '  End With
                      '  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, Singular.Web.BootstrapEnums.InputSize.Small, , "Surname...")
                      '    End With
                      '  End With
                      'End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddClass("max-height-400")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceContact)(Function(d) ViewModel.SelectableHumanResources)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                        With .Helpers.Div
                          .Style.Width = "100%"
                          .Style.TextAlign = Singular.Web.TextAlign.left
                          .AddBinding(Singular.Web.KnockoutBindingString.enable, "EmailsPage.canSelectContact($data)")
                          .AddBinding(Singular.Web.KnockoutBindingString.click, "EmailsPage.onContactClicked($data)")
                          .AddBinding(Singular.Web.KnockoutBindingString.css, "EmailsPage.ROHumanResourceFindCSS($data)")
                          .AddBinding(KnockoutBindingString.html, "EmailsPage.ROHumanResourceFindHTML($data)")
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.ExtraSmall)
                      With .Helpers.DivC("pull-right")
                        With .Helpers.HTMLTag("span")
                          .Helpers.Bootstrap.Button(, "First", Singular.Web.BootstrapEnums.Style.Custom, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-angle-double-left", , Singular.Web.PostBackType.None, "EmailsPage.FirstHRPage()", False)
                        End With
                        With .Helpers.HTMLTag("span")
                          .Helpers.Bootstrap.Button(, "Previous", Singular.Web.BootstrapEnums.Style.Custom, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-angle-left", , Singular.Web.PostBackType.None, "EmailsPage.PreviousHRPage()", False)
                        End With
                        With .Helpers.HTMLTag("span")
                          .Style.Width = "50px"
                          With .Helpers.HTMLTag("input")
                            .Style.Width = "50px"
                            .AddBinding(Singular.Web.KnockoutBindingString.value, "ViewModel.SelectableHumanResourcesManager().PageNo()")
                          End With
                        End With
                        With .Helpers.HTMLTag("span")
                          .AddBinding(Singular.Web.KnockoutBindingString.html, " ' of ' + ViewModel.SelectableHumanResourcesManager().Pages().toString()")
                        End With
                        With .Helpers.HTMLTag("span")
                          .Helpers.Bootstrap.Button(, "Next", Singular.Web.BootstrapEnums.Style.Custom, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-angle-right", , Singular.Web.PostBackType.None, "EmailsPage.NextHRPage()", True)
                        End With
                        With .Helpers.HTMLTag("span")
                          .Helpers.Bootstrap.Button(, "Last", Singular.Web.BootstrapEnums.Style.Custom, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-angle-double-right", , Singular.Web.PostBackType.None, "EmailsPage.LastHRPage()", True)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.IsFilteringData)
            .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin")
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.Button(, "Done", Singular.Web.BootstrapEnums.Style.Success, ,
                                         Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                         Singular.Web.PostBackType.Ajax, "EmailsPage.DoneSelectingRecipients()")
          End With
        End With
      End With

    End Using%>
</asp:Content>
