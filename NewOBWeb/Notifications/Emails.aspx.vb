﻿Imports Singular.Web
Imports OBLib.Notifications
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Notifications.ReadOnly
Imports Singular.Reporting
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.Misc
Imports OBLib.Notifications.Email
Imports System

Public Class Emails
  Inherits OBPageBase(Of EmailsVM)

End Class


Public Class EmailsVM
  Inherits OBViewModel(Of EmailsVM)

  Public Property ClickedEmailID As Integer
  Public Property ClickedTempID As String

  'Page
  Public Property CreateEmail As Boolean
  Public Property GenerateEmail As Boolean
  Public Property FindEmail As Boolean

  'Email
  Public Property CurrentEmail As OBLib.Notifications.Email.SoberEmail

  'Current Batch
  Public Property CurrentEmailBatch As OBLib.Notifications.Email.EmailBatch
  'Batch Emails
  <InitialDataOnly>
  Public Property ROCurrentBatchEmailList As OBLib.Notifications.Email.ReadOnly.ROBatchEmailList = New OBLib.Notifications.Email.ReadOnly.ROBatchEmailList
  <InitialDataOnly>
  Public Property ROCurrentBatchEmailListCriteria As OBLib.Notifications.Email.ReadOnly.ROBatchEmailList.Criteria = New OBLib.Notifications.Email.ReadOnly.ROBatchEmailList.Criteria
  <InitialDataOnly>
  Public Property ROCurrentBatchEmailListManager As Singular.Web.Data.PagedDataManager(Of EmailsVM) = New Singular.Web.Data.PagedDataManager(Of EmailsVM)(Function(d) Me.ROCurrentBatchEmailList,
                                                                                                                                                          Function(d) Me.ROCurrentBatchEmailListCriteria,
                                                                                                                                                          "RecipientName", 25)


  <InitialDataOnly>
  Public Property ROEmailSearchList As OBLib.Notifications.Email.ReadOnly.ROEmailSearchList = New OBLib.Notifications.Email.ReadOnly.ROEmailSearchList
  <InitialDataOnly>
  Public Property ROEmailSearchListCriteria As OBLib.Notifications.Email.ReadOnly.ROEmailSearchList.Criteria = New OBLib.Notifications.Email.ReadOnly.ROEmailSearchList.Criteria
  <InitialDataOnly>
  Public Property ROEmailSearchListManager As Singular.Web.Data.PagedDataManager(Of EmailsVM) = New Singular.Web.Data.PagedDataManager(Of EmailsVM)(Function(d) Me.ROEmailSearchList,
                                                                                                                                                     Function(d) Me.ROEmailSearchListCriteria,
                                                                                                                                                     "CreatedDate", 25, False)


  'Batches
  <InitialDataOnly>
  Public Property ROEmailBatchList As OBLib.Notifications.Email.ReadOnly.ROEmailBatchList = New OBLib.Notifications.Email.ReadOnly.ROEmailBatchList
  <InitialDataOnly>
  Public Property ROEmailBatchListCriteria As OBLib.Notifications.Email.ReadOnly.ROEmailBatchList.Criteria = New OBLib.Notifications.Email.ReadOnly.ROEmailBatchList.Criteria
  <InitialDataOnly>
  Public Property ROEmailBatchListManager As Singular.Web.Data.PagedDataManager(Of EmailsVM) = New Singular.Web.Data.PagedDataManager(Of EmailsVM)(Function(d) Me.ROEmailBatchList,
                                                                                                                                                   Function(d) Me.ROEmailBatchListCriteria,
                                                                                                                                                   "RecipientName", 25)
  'Batch Emails
  <InitialDataOnly>
  Public Property ROBatchEmailList As OBLib.Notifications.Email.ReadOnly.ROBatchEmailList = New OBLib.Notifications.Email.ReadOnly.ROBatchEmailList
  <InitialDataOnly>
  Public Property ROBatchEmailListCriteria As OBLib.Notifications.Email.ReadOnly.ROBatchEmailList.Criteria = New OBLib.Notifications.Email.ReadOnly.ROBatchEmailList.Criteria
  <InitialDataOnly>
  Public Property ROBatchEmailListManager As Singular.Web.Data.PagedDataManager(Of EmailsVM) = New Singular.Web.Data.PagedDataManager(Of EmailsVM)(Function(d) Me.ROBatchEmailList,
                                                                                                                                                   Function(d) Me.ROBatchEmailListCriteria,
                                                                                                                                                   "RecipientName", 25)

  'Emails To Delete
  <InitialDataOnly>
  Public Property SelectedEmailIDs As New List(Of Integer)
  <InitialDataOnly>
  Public Property SelectedEmailIDsToDelete As New List(Of Integer)


#Region " Criteria "

  <InitialDataOnly>
  Public Property SelectedRecipients As List(Of ROHumanResourceContact) = New List(Of ROHumanResourceContact)

  <InitialDataOnly>
  Public Property SelectableDisciplines As List(Of RODisciplineSelect) = New List(Of RODisciplineSelect)

  <InitialDataOnly>
  Public Property SelectableContractTypes As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

  <InitialDataOnly>
  Public Property SelectableHumanResources As List(Of ROHumanResourceContact) = New List(Of ROHumanResourceContact)
  <InitialDataOnly>
  Public Property SelectableHumanResourcesCriteria As OBLib.HR.ReadOnly.ROHumanResourceContactList.Criteria = New OBLib.HR.ReadOnly.ROHumanResourceContactList.Criteria
  <InitialDataOnly>
  Public Property SelectableHumanResourcesManager As Singular.Web.Data.PagedDataManager(Of EmailsVM) = New Singular.Web.Data.PagedDataManager(Of EmailsVM)(Function(d) Me.SelectableHumanResources,
                                                                                                                                                           Function(d) Me.SelectableHumanResourcesCriteria,
                                                                                                                                                           "RecipientName", 100)

  <InitialDataOnly>
  Public Property IsFilteringData As Boolean = False

  <InitialDataOnly>
  Public Property UserSystemList As OBLib.Security.UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

#End Region

  Protected Overrides Sub Setup()
    MyBase.Setup()

    CreateEmail = False
    GenerateEmail = False
    FindEmail = False
    ROBatchEmailListManager.SortAsc = False

    ROEmailSearchListCriteria.CreatedStartDate = Now.AddDays(-7)
    ROEmailSearchListCriteria.CreatedEndDate = Now
    ROEmailSearchListCriteria.SystemGenerated = False

    SelectableHumanResourcesCriteria.PageNo = 1
    SelectableHumanResourcesCriteria.PageSize = 100
    SelectableHumanResourcesManager.PageSize = 100

    ClientDataProvider.AddDataSource("ROEmailTemplateTypeList", OBLib.CommonData.Lists.ROEmailTemplateTypeList, False)
    ClientDataProvider.AddDataSource("ROEmailBatchTemplateList", OBLib.CommonData.Lists.ROEmailBatchTemplateList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
    ClientDataProvider.AddDataSource("ROSystemAllowedAreaList", OBLib.CommonData.Lists.ROSystemAllowedAreaList, False)
    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaAllowedDisciplineList", OBLib.CommonData.Lists.ROProductionAreaAllowedDisciplineList, False)
    ClientDataProvider.AddDataSource("UserSystemList", UserSystemList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "GenerateBatch"
        GenerateBatch(CommandArgs)

      Case "EditBatch"
        EditBatch(CommandArgs)

      Case "SendCurrentBatchEmails"
        SendCurrentBatchEmails(CommandArgs)

      Case "SaveEmail"
        SaveEmail(CommandArgs)

      Case "DeleteBatchEmails"
        DeleteBatchEmails(CommandArgs)

    End Select

  End Sub

  Private Sub SaveEmail(CommandArgs As CommandArgs)

    Dim smsl As New OBLib.Notifications.Email.SoberEmailList
    Dim sh As SaveHelper = Nothing
    CurrentEmail.Body = CurrentEmail.HTMLBody
    CurrentEmail.SystemGenerated = False
    CurrentEmail.Ignore = True
    If CurrentEmail.EmailTemplateTypeID IsNot Nothing Then
      smsl = CurrentEmail.GenerateEmailListFromTemplate()
      sh = TrySave(smsl)
      If sh.Success Then
        smsl = sh.SavedObject
        CommandArgs.ReturnData = New Singular.Web.Result(True) With {.ErrorText = ""}
        CreateEmail = False
        GenerateEmail = False
        FindEmail = True
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Else
      CurrentEmail.PrepareForSaveAndSend()
      smsl.Add(CurrentEmail)
      sh = TrySave(smsl)
      If sh.Success Then
        smsl = sh.SavedObject
        CommandArgs.ReturnData = New Singular.Web.Result(True) With {.ErrorText = ""}
        CreateEmail = False
        GenerateEmail = False
        FindEmail = True
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    End If

    'CurrentSms = smsl(0)
    'CurrentSms.SendSober()

  End Sub

  Private Sub GenerateBatchLong(progress As Singular.Web.AjaxProgress)
    ', CommandArgs As Singular.Web.CommandArgs
    Dim emailBatchList As New OBLib.Notifications.Email.EmailBatchList
    'Generate the Emails based on the TemplateID provided
    Try
      If CurrentEmailBatch.EmailBatchTemplateID = CType(OBLib.CommonData.Enums.EmailBatchTemplate.Schedule_ProductionServices, Integer) Then
        emailBatchList = GenerateScheduleProductionServices()
        emailBatchList.CheckAllRules()
      ElseIf CurrentEmailBatch.EmailBatchTemplateID = CType(OBLib.CommonData.Enums.EmailBatchTemplate.Schedule_PlayoutOperations, Integer) Then
        emailBatchList = GenerateSchedulePlayoutOperations()
        emailBatchList.CheckAllRules()
      ElseIf CurrentEmailBatch.EmailBatchTemplateID = CType(OBLib.CommonData.Enums.EmailBatchTemplate.Schedule_ProductionContent, Integer) Then
        emailBatchList = GenerateScheduleProductionContent()
        emailBatchList.CheckAllRules()
      End If
    Catch ex As Exception
      'CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      Exit Sub
    End Try

    'Save the list if there are emails in it and if it is valid
    If emailBatchList.Count > 0 AndAlso emailBatchList.IsValid Then
      Dim sh As Singular.SaveHelper = TrySave(emailBatchList)
      If sh.Success Then
        emailBatchList = sh.SavedObject
        CurrentEmailBatch = emailBatchList(0)
        'CommandArgs.ReturnData = New Singular.Web.Result(True) With {.Data = Nothing, .ErrorText = ""}
      Else
        'CommandArgs.ReturnData = New Singular.Web.Result(False) With {.Data = sh.Error, .ErrorText = sh.ErrorText}
      End If
    Else
      If emailBatchList.Count = 0 Then
        'CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Zero emails could be generated"}
      ElseIf Not emailBatchList.IsValid Then
        'CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = emailBatchList.GetErrorsAsString}
      End If
    End If

  End Sub

  Private Sub GenerateBatch(CommandArgs As Singular.Web.CommandArgs)

    Dim emailBatchList As New OBLib.Notifications.Email.EmailBatchList

    'Generate the Emails based on the TemplateID provided
    Try
      If CurrentEmailBatch.EmailBatchTemplateID = CType(OBLib.CommonData.Enums.EmailBatchTemplate.Schedule_ProductionServices, Integer) Then
        emailBatchList = GenerateScheduleProductionServices()
        emailBatchList.CheckAllRules()
      ElseIf CurrentEmailBatch.EmailBatchTemplateID = CType(OBLib.CommonData.Enums.EmailBatchTemplate.Schedule_PlayoutOperations, Integer) Then
        emailBatchList = GenerateSchedulePlayoutOperations()
        emailBatchList.CheckAllRules()
      ElseIf CurrentEmailBatch.EmailBatchTemplateID = CType(OBLib.CommonData.Enums.EmailBatchTemplate.Schedule_ProductionContent, Integer) Then
        emailBatchList = GenerateScheduleProductionContent()
        emailBatchList.CheckAllRules()
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      Exit Sub
    End Try

    'Save the list if there are emails in it and if it is valid
    If emailBatchList.Count > 0 AndAlso emailBatchList.IsValid Then
      Dim sh As Singular.SaveHelper = TrySave(emailBatchList)
      If sh.Success Then
        emailBatchList = sh.SavedObject
        CurrentEmailBatch = emailBatchList(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True) With {.Data = Nothing, .ErrorText = ""}
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.Data = sh.Error, .ErrorText = sh.ErrorText}
      End If
    Else
      If emailBatchList.Count = 0 Then
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Zero emails could be generated"}
      ElseIf Not emailBatchList.IsValid Then
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = emailBatchList.GetErrorsAsString}
      End If
    End If

  End Sub

  Public Class HRMonth
    Public Property HumanResourceID As Integer = 0
    Public Property HRName As String = ""
    Public Property YearNo As Integer = 0
    Public Property MonthNo As Integer = 0
    Public Property MonthName As String = ""
    Public Property HRYMID As Integer = 0
    Public Property BookingCount As Integer = 0
    Public Property EmailAddress As String = ""
    Public Property AlternativeEmailAddress As String = ""
    Public Property CellPhoneNumber As String = ""
    Public Property AlternativeContactNumber As String = ""
    Public Sub New(HumanResourceID As Integer, HRName As String, YearNo As Integer, MonthNo As Integer, MonthName As String,
                   HRYMID As Integer, BookingCount As Integer,
                   EmailAddress As String, AlternativeEmailAddress As String, CellPhoneNumber As String, AlternativeContactNumber As String)
      Me.HumanResourceID = HumanResourceID
      Me.HRName = HRName
      Me.YearNo = YearNo
      Me.MonthNo = MonthNo
      Me.MonthName = MonthName
      Me.HRYMID = HRYMID
      Me.BookingCount = BookingCount
      Me.EmailAddress = EmailAddress
      Me.AlternativeEmailAddress = AlternativeEmailAddress
      Me.CellPhoneNumber = CellPhoneNumber
      Me.AlternativeContactNumber = AlternativeContactNumber
    End Sub
  End Class

  Function GenerateScheduleProductionServices() As OBLib.Notifications.Email.EmailBatchList

    Dim rpt As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport()
    If CurrentEmailBatch.SelectRecipientsManually Then
      rpt.ReportCriteria.StartDate = CurrentEmailBatch.StartDate
      rpt.ReportCriteria.EndDate = CurrentEmailBatch.EndDate
      rpt.ReportCriteria.HumanResourceIDs = New List(Of Integer)
      CurrentEmailBatch.EmailBatchRecipientList.ToList.ForEach(Sub(recip)
                                                                 rpt.ReportCriteria.HumanResourceIDs.Add(recip.HumanResourceID)
                                                               End Sub)
    Else
      rpt.ReportCriteria.StartDate = CurrentEmailBatch.StartDate
      rpt.ReportCriteria.EndDate = CurrentEmailBatch.EndDate
      rpt.ReportCriteria.HRSystemID = CurrentEmailBatch.SystemID
      rpt.ReportCriteria.HRProductionAreaID = CurrentEmailBatch.ProductionAreaID
      rpt.ReportCriteria.HRContractTypeID = CurrentEmailBatch.ContractTypeID
      rpt.ReportCriteria.HRDisciplineID = CurrentEmailBatch.DisciplineID
    End If

    Dim ds As DataSet = rpt.GetData()

    'get all HR
    Dim hrList As New List(Of HRMonth)
    Dim AlreadyAdded As Integer = 0
    Dim newTP As HRMonth
    For Each dr As DataRow In ds.Tables(0).Rows
      AlreadyAdded = hrList.Where(Function(d) d.HumanResourceID = dr(0)).Count()
      If AlreadyAdded = 0 Then
        newTP = Nothing
        newTP = New HRMonth(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6),
                            dr(7), dr(8), dr(9), dr(10))
        hrList.Add(newTP)
      End If
    Next

    'Variables
    Dim newRpt As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
    Dim newDs As OBWebReports.MonthlyScheduleByIndividual
    Dim newFileInfo As Singular.Reporting.ReportFileInfo = Nothing

    Dim emailBatchList As New OBLib.Notifications.Email.EmailBatchList
    Dim email As OBLib.Notifications.Email.BatchEmail = Nothing
    Dim attachment As OBLib.Notifications.Email.BatchEmailAttachment = Nothing

    'Setup
    emailBatchList.Add(CurrentEmailBatch)

    'Process Data
    For Each HR In hrList

      If HR.BookingCount > 0 Then
        'clear variables
        newRpt = Nothing
        newDs = Nothing
        newFileInfo = Nothing
        newRpt = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
        newDs = New OBWebReports.MonthlyScheduleByIndividual

        'Copy the information table
        For Each dr As DataRow In ds.Tables("Information").Rows
          newDs.Information.ImportRow(dr)
        Next

        'Copy the 1st table
        For Each dr1 As DataRow In ds.Tables("Table").Rows
          If CompareSafe(CType(dr1(5), Integer), HR.HRYMID) Then
            newDs.Table.ImportRow(dr1)
          End If
        Next

        'Copy the 2nd table
        For Each dr2 As DataRow In ds.Tables("Table1").Rows
          If CompareSafe(CType(dr2(0), Integer), HR.HRYMID) Then
            newDs.Table1.ImportRow(dr2)
          End If
        Next

        'Copy the 3rd table
        For Each dr3 As DataRow In ds.Tables("Table2").Rows
          If CompareSafe(CType(dr3(11), Integer), HR.HRYMID) Then
            newDs.Table2.ImportRow(dr3)
          End If
        Next

        newRpt.SetDataSet(newDs)
        Dim sd As String = CurrentEmailBatch.StartDate.Value.ToString("dd MMM yy").ToString.Replace(" ", "")
        Dim ed As String = CurrentEmailBatch.EndDate.Value.ToString("dd MMM yy").ToString.Replace(" ", "")
        newFileInfo = newRpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        newFileInfo.FileName = HR.HRName.ToString.Trim.Replace(" ", "").Replace("(", "").Replace(")", "") & "_Schedule_" & sd & "_" & ed & ".pdf"
        'reports.Add(newFileInfo)

        'Send to Primary
        If HR.EmailAddress.Trim.Length > 0 Then
          email = CurrentEmailBatch.BatchEmailList.AddNew()
          email.SystemGenerated = False
          email.ToEmailAddress = HR.EmailAddress
          If HR.AlternativeEmailAddress.Trim.Length > 0 Then
            email.ToEmailAddress &= ";" & HR.AlternativeEmailAddress
          End If
          email.Ignore = True
          email.DateToSend = Nothing
          email.FromEmailAddress = "noreply@supersport.co.za"
          email.FriendlyFrom = "SOBERMS"
          email.Body = "Hi " & HR.HRName & vbCrLf &
                       "Please find your schedule attached"
          email.CCEmailAddresses = ""
          email.Subject = "Monthly Schedule - " & HR.MonthName & " " & HR.YearNo.ToString
          attachment = email.BatchEmailAttachmentList.AddNew
          attachment.AttachmentName = newFileInfo.FileName
          attachment.AttachmentData = newFileInfo.FileStream.ToArray
        End If

        ''Send to Alternate
        'If Singular.Emails.ValidEmailAddress(HR.AlternativeEmailAddress) Then
        '  email = CurrentEmailBatch.BatchEmailList.AddNew()
        '  email.SystemGenerated = False
        '  email.ToEmailAddress = HR.AlternativeEmailAddress
        '  email.Ignore = True
        '  email.DateToSend = Nothing
        '  email.FromEmailAddress = "noreply@supersport.co.za"
        '  email.FriendlyFrom = "SOBERMS"
        '  email.Body = "Hi " & HR.HRName & vbCrLf &
        '               "Please find your schedule attached"
        '  email.CCEmailAddresses = ""
        '  email.Subject = "Schedule"
        '  attachment = email.BatchEmailAttachmentList.AddNew
        '  attachment.AttachmentName = newFileInfo.FileName
        '  attachment.AttachmentData = newFileInfo.FileStream.ToArray
        'End If

      End If

    Next

    Return emailBatchList

  End Function

  Public Function GenerateSchedulePlayoutOperations() As OBLib.Notifications.Email.EmailBatchList

    Dim rpt As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport()
    If CurrentEmailBatch.SelectRecipientsManually Then
      rpt.ReportCriteria.HumanResourceIDs = New List(Of Integer)
      CurrentEmailBatch.EmailBatchRecipientList.ToList.ForEach(Sub(recip)
                                                                 rpt.ReportCriteria.HumanResourceIDs.Add(recip.HumanResourceID)
                                                               End Sub)
    Else
      rpt.ReportCriteria.StartDate = CurrentEmailBatch.StartDate
      rpt.ReportCriteria.EndDate = CurrentEmailBatch.EndDate
      rpt.ReportCriteria.HRSystemID = CurrentEmailBatch.SystemID
      rpt.ReportCriteria.HRProductionAreaID = CurrentEmailBatch.ProductionAreaID
      rpt.ReportCriteria.HRContractTypeID = CurrentEmailBatch.ContractTypeID
      rpt.ReportCriteria.HRDisciplineID = CurrentEmailBatch.DisciplineID
    End If

    Dim ds As DataSet = rpt.GetData()

    'get all HR
    Dim hrList As New List(Of HRMonth)
    Dim AlreadyAdded As Integer = 0
    Dim newTP As HRMonth
    For Each dr As DataRow In ds.Tables(0).Rows
      AlreadyAdded = hrList.Where(Function(d) d.HumanResourceID = dr(0)).Count()
      If AlreadyAdded = 0 Then
        newTP = Nothing
        newTP = New HRMonth(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6),
                            dr(7), dr(8), dr(9), dr(10))
        hrList.Add(newTP)
      End If
    Next

    'Variables
    Dim newRpt As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
    Dim newDs As OBWebReports.MonthlyScheduleByIndividual
    Dim newFileInfo As Singular.Reporting.ReportFileInfo = Nothing

    Dim emailBatchList As New OBLib.Notifications.Email.EmailBatchList
    Dim email As OBLib.Notifications.Email.BatchEmail = Nothing
    Dim attachment As OBLib.Notifications.Email.BatchEmailAttachment = Nothing

    'Setup
    emailBatchList.Add(CurrentEmailBatch)

    'Process Data
    For Each HR In hrList

      If HR.BookingCount > 0 Then
        'clear variables
        newRpt = Nothing
        newDs = Nothing
        newFileInfo = Nothing
        newRpt = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
        newDs = New OBWebReports.MonthlyScheduleByIndividual

        'Copy the information table
        For Each dr As DataRow In ds.Tables("Information").Rows
          newDs.Information.ImportRow(dr)
        Next

        'Copy the 1st table
        For Each dr1 As DataRow In ds.Tables("Table").Rows
          If CompareSafe(CType(dr1(5), Integer), HR.HRYMID) Then
            newDs.Table.ImportRow(dr1)
          End If
        Next

        'Copy the 2nd table
        For Each dr2 As DataRow In ds.Tables("Table1").Rows
          If CompareSafe(CType(dr2(0), Integer), HR.HRYMID) Then
            newDs.Table1.ImportRow(dr2)
          End If
        Next

        'Copy the 3rd table
        For Each dr3 As DataRow In ds.Tables("Table2").Rows
          If CompareSafe(CType(dr3(15), Integer), HR.HRYMID) Then
            newDs.Table2.ImportRow(dr3)
          End If
        Next

        newRpt.SetDataSet(newDs)
        Dim sd As String = CurrentEmailBatch.StartDate.Value.ToString("dd MMM yy").ToString.Replace(" ", "")
        Dim ed As String = CurrentEmailBatch.EndDate.Value.ToString("dd MMM yy").ToString.Replace(" ", "")
        newFileInfo = newRpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        newFileInfo.FileName = HR.HRName.ToString.Trim.Replace(" ", "").Replace("(", "").Replace(")", "") & "_Schedule_" & sd & "_" & ed & ".pdf"
        'reports.Add(newFileInfo)

        'Send to Primary
        If Singular.Emails.ValidEmailAddress(HR.EmailAddress) Then
          email = CurrentEmailBatch.BatchEmailList.AddNew()
          email.SystemGenerated = False
          email.ToEmailAddress = HR.EmailAddress
          If Singular.Emails.ValidEmailAddress(HR.AlternativeEmailAddress) Then
            email.ToEmailAddress &= ";" & HR.AlternativeEmailAddress
          End If
          email.Ignore = True
          email.DateToSend = Nothing
          email.FromEmailAddress = "noreply@supersport.co.za"
          email.FriendlyFrom = "SOBERMS"
          email.Body = "Hi " & HR.HRName & vbCrLf &
                       "Please find your schedule attached"
          email.CCEmailAddresses = ""
          email.Subject = "Monthly Schedule"
          attachment = email.BatchEmailAttachmentList.AddNew
          attachment.AttachmentName = newFileInfo.FileName
          attachment.AttachmentData = newFileInfo.FileStream.ToArray
        End If

        ''Send to Alternate
        'If Singular.Emails.ValidEmailAddress(HR.AlternativeEmailAddress) Then
        '  email = CurrentEmailBatch.BatchEmailList.AddNew()
        '  email.SystemGenerated = False
        '  email.ToEmailAddress = HR.AlternativeEmailAddress
        '  email.Ignore = True
        '  email.DateToSend = Nothing
        '  email.FromEmailAddress = "noreply@supersport.co.za"
        '  email.FriendlyFrom = "SOBERMS"
        '  email.Body = "Hi " & HR.HRName & vbCrLf &
        '               "Please find your schedule attached"
        '  email.CCEmailAddresses = ""
        '  email.Subject = "Schedule"
        '  attachment = email.BatchEmailAttachmentList.AddNew
        '  attachment.AttachmentName = newFileInfo.FileName
        '  attachment.AttachmentData = newFileInfo.FileStream.ToArray
        'End If

      End If

    Next

    Return emailBatchList

  End Function

  Function GenerateScheduleProductionContent() As OBLib.Notifications.Email.EmailBatchList

    Dim rpt As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport()
    If CurrentEmailBatch.SelectRecipientsManually Then
      rpt.ReportCriteria.StartDate = CurrentEmailBatch.StartDate
      rpt.ReportCriteria.EndDate = CurrentEmailBatch.EndDate
      rpt.ReportCriteria.HumanResourceIDs = New List(Of Integer)
      CurrentEmailBatch.EmailBatchRecipientList.ToList.ForEach(Sub(recip)
                                                                 rpt.ReportCriteria.HumanResourceIDs.Add(recip.HumanResourceID)
                                                               End Sub)
    Else
      rpt.ReportCriteria.StartDate = CurrentEmailBatch.StartDate
      rpt.ReportCriteria.EndDate = CurrentEmailBatch.EndDate
      rpt.ReportCriteria.HRSystemID = CurrentEmailBatch.SystemID
      rpt.ReportCriteria.HRProductionAreaID = CurrentEmailBatch.ProductionAreaID
      rpt.ReportCriteria.HRContractTypeID = CurrentEmailBatch.ContractTypeID
      rpt.ReportCriteria.HRDisciplineID = CurrentEmailBatch.DisciplineID
    End If

    Dim ds As DataSet = rpt.GetData()

    'get all HR
    Dim hrList As New List(Of HRMonth)
    Dim AlreadyAdded As Integer = 0
    Dim newTP As HRMonth
    For Each dr As DataRow In ds.Tables(0).Rows
      AlreadyAdded = hrList.Where(Function(d) d.HumanResourceID = dr(0)).Count()
      If AlreadyAdded = 0 Then
        newTP = Nothing
        newTP = New HRMonth(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6),
                            dr(7), dr(8), dr(9), dr(10))
        hrList.Add(newTP)
      End If
    Next

    'Variables
    Dim newRpt As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
    Dim newDs As OBWebReports.MonthlyScheduleByIndividual
    Dim newFileInfo As Singular.Reporting.ReportFileInfo = Nothing

    Dim emailBatchList As New OBLib.Notifications.Email.EmailBatchList
    Dim email As OBLib.Notifications.Email.BatchEmail = Nothing
    Dim attachment As OBLib.Notifications.Email.BatchEmailAttachment = Nothing

    'Setup
    emailBatchList.Add(CurrentEmailBatch)

    'Process Data
    For Each HR In hrList

      If HR.BookingCount > 0 Then
        'clear variables
        newRpt = Nothing
        newDs = Nothing
        newFileInfo = Nothing
        newRpt = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
        newDs = New OBWebReports.MonthlyScheduleByIndividual

        'Copy the information table
        For Each dr As DataRow In ds.Tables("Information").Rows
          newDs.Information.ImportRow(dr)
        Next

        'Copy the 1st table
        For Each dr1 As DataRow In ds.Tables("Table").Rows
          If CompareSafe(CType(dr1(5), Integer), HR.HRYMID) Then
            newDs.Table.ImportRow(dr1)
          End If
        Next

        'Copy the 2nd table
        For Each dr2 As DataRow In ds.Tables("Table1").Rows
          If CompareSafe(CType(dr2(0), Integer), HR.HRYMID) Then
            newDs.Table1.ImportRow(dr2)
          End If
        Next

        'Copy the 3rd table
        For Each dr3 As DataRow In ds.Tables("Table2").Rows
          If CompareSafe(CType(dr3(11), Integer), HR.HRYMID) Then
            newDs.Table2.ImportRow(dr3)
          End If
        Next

        newRpt.SetDataSet(newDs)
        Dim sd As String = CurrentEmailBatch.StartDate.Value.ToString("dd MMM yy").ToString.Replace(" ", "")
        Dim ed As String = CurrentEmailBatch.EndDate.Value.ToString("dd MMM yy").ToString.Replace(" ", "")
        newFileInfo = newRpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        newFileInfo.FileName = HR.HRName.ToString.Trim.Replace(" ", "").Replace("(", "").Replace(")", "") & "_Schedule_" & sd & "_" & ed & ".pdf"
        'reports.Add(newFileInfo)

        'Send to Primary
        If HR.EmailAddress.Trim.Length > 0 Then
          email = CurrentEmailBatch.BatchEmailList.AddNew()
          email.SystemGenerated = False
          email.ToEmailAddress = HR.EmailAddress
          If HR.AlternativeEmailAddress.Trim.Length > 0 Then
            email.ToEmailAddress &= ";" & HR.AlternativeEmailAddress
          End If
          email.Ignore = True
          email.DateToSend = Nothing
          email.FromEmailAddress = "noreply@supersport.co.za"
          email.FriendlyFrom = "SOBERMS"
          email.Body = "Hi " & HR.HRName & vbCrLf &
                       "Please find your schedule attached"
          email.CCEmailAddresses = ""
          email.Subject = "Schedule"
          attachment = email.BatchEmailAttachmentList.AddNew
          attachment.AttachmentName = newFileInfo.FileName
          attachment.AttachmentData = newFileInfo.FileStream.ToArray
        End If

        ''Send to Alternate
        'If Singular.Emails.ValidEmailAddress(HR.AlternativeEmailAddress) Then
        '  email = CurrentEmailBatch.BatchEmailList.AddNew()
        '  email.SystemGenerated = False
        '  email.ToEmailAddress = HR.AlternativeEmailAddress
        '  email.Ignore = True
        '  email.DateToSend = Nothing
        '  email.FromEmailAddress = "noreply@supersport.co.za"
        '  email.FriendlyFrom = "SOBERMS"
        '  email.Body = "Hi " & HR.HRName & vbCrLf &
        '               "Please find your schedule attached"
        '  email.CCEmailAddresses = ""
        '  email.Subject = "Schedule"
        '  attachment = email.BatchEmailAttachmentList.AddNew
        '  attachment.AttachmentName = newFileInfo.FileName
        '  attachment.AttachmentData = newFileInfo.FileStream.ToArray
        'End If

      End If

    Next

    Return emailBatchList

  End Function

  Private Sub EditBatch(CommandArgs As Singular.Web.CommandArgs)

    Dim btl As Email.EmailBatchList = OBLib.Notifications.Email.EmailBatchList.GetEmailBatchList(CommandArgs.ClientArgs.EmailBatchID)
    Dim bt As Email.EmailBatch = Nothing
    If btl.Count = 1 Then
      CurrentEmailBatch = btl(0)
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False, "Could not edit batch: " & IIf(btl.Count > 1, " too many items returned", " zero items returned"))
    End If
    CreateEmail = False
    GenerateEmail = True
    FindEmail = False

  End Sub

  Private Sub SendCurrentBatchEmails(CommandArgs As Singular.Web.CommandArgs)

    Dim btl As Email.EmailBatchList = OBLib.Notifications.Email.EmailBatchList.GetEmailBatchList(CurrentEmailBatch.EmailBatchID)
    'CurrentEmailBatch = btl(0)
    btl(0).BatchEmailList.ToList.ForEach(Sub(em)
                                           If em.Ignore And em.SentDate Is Nothing Then
                                             em.Ignore = False
                                             em.DateToSend = Now
                                           End If
                                         End Sub)
    Dim sh As SaveHelper = TrySave(btl)
    If sh.Success Then
      CommandArgs.ReturnData = New Result(True)
    Else
      CommandArgs.ReturnData = New Result(False) With {.Data = Nothing, .ErrorText = "Could not send emails: " & sh.ErrorText}
    End If

  End Sub

  Private Sub DeleteBatchEmails(CommandArgs As CommandArgs)

    Dim HasItemsWhichCouldNotBeDeletedMessage As String = "<p>There were emails flagged for removal that were already sent</p>" & vbCrLf &
                                                          "<p>These items have not been removed</p>"
    If SelectedEmailIDsToDelete.Count > 0 Then
      Dim emails As OBLib.Notifications.Email.SoberEmailList = OBLib.Notifications.Email.SoberEmailList.GetSoberEmailList(SelectedEmailIDsToDelete, False)
      Dim EmailsWhichCantBeDeleted As New List(Of OBLib.Notifications.Email.SoberEmail)
      emails.ToList.ForEach(Sub(em)
                              If em.SentDate IsNot Nothing Then
                                EmailsWhichCantBeDeleted.Add(em)
                              End If
                            End Sub)
      If EmailsWhichCantBeDeleted.Count > 0 Then
        EmailsWhichCantBeDeleted.ForEach(Sub(em)
                                           emails.RemoveClean(em)
                                         End Sub)
      End If
      emails.Clear()
      Dim sh As SaveHelper = TrySave(emails)
      If sh.Success Then
        If EmailsWhichCantBeDeleted.Count > 0 Then
          CommandArgs.ReturnData = New Singular.Web.Result(True) With {.Data = New With {
                                                                                        .WarningMessage = HasItemsWhichCouldNotBeDeletedMessage
                                                                                        }}
        Else
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        End If
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    End If

  End Sub

End Class