﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="Sms.aspx.vb" Inherits="NewOBWeb.Sms" %>

<%@ Import Namespace="OBLib.Maintenance.General.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.ReadOnly" %>

<%@ Import Namespace="OBLib.HR.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.HR.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.Productions.Areas.ReadOnly" %>

<%@ Import Namespace="OBLib.Notifications.ReadOnly" %>

<%@ Import Namespace="OBLib.Notifications" %>

<%@ Import Namespace="OBLib.Notifications.SmS" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Notifications.Sms.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" src="../Scripts/BusinessObjects/Notifications.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/SmsPageNew.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <style type="text/css">
    div.sms-selection {
      width: 60px !important;
    }

    div.item {
      transition: all 0.2s;
      -moz-transition: all 0.2s;
      -webkit-transition: all 0.2s;
      -o-transition: all 0.2s;
    }

      div.item.sms-selected {
        background: #019dff !important;
        color: #fff !important;
        border-top: solid 1px #fff;
      }

    .mail-inbox .mails .item .date i.fa-3x {
      font-size: 3em;
    }

    .mail-inbox .mails .item .date i.fa-4x {
      font-size: 4em;
    }

    .mail-inbox .mails .item {
      border-bottom: 1px solid #E5E5E5;
      padding: 10px 20px;
      display: table;
      width: 100%;
      cursor: default;
    }

    span.progress-column {
      cursor: alias;
    }

    .mail-inbox .mails .item .msg {
      font-size: 12px;
      margin: 0;
      font-weight: 400;
    }

    div.head {
      transition: all 0.2s;
      -moz-transition: all 0.2s;
      -webkit-transition: all 0.2s;
      -o-transition: all 0.2s;
    }

      div.head.find-sms {
        background: #019dff !important;
        color: #fff !important;
        border-top: solid 1px #fff;
        padding: 10px 15px;
      }

      div.head.create-sms {
        background: #5ccf5c !important;
        color: #fff !important;
        border-top: solid 1px #fff;
        padding: 10px 15px;
      }

      div.head.create-batch {
        background: #ffb500 !important;
        color: #fff !important;
        border-top: solid 1px #fff;
        padding: 10px 15px;
      }

    #sms {
      margin-top: 15px;
    }

    #sms-recipients {
      margin-top: 15px;
    }

    #batch-criteria {
      margin-top: 15px;
    }


    #select-sms-recipients {
      margin-top: 5px;
    }

    #select-recipients {
      margin-top: 5px;
    }

    div.animated-column {
      transition: all 0.4s;
      -moz-transition: all 0.4s;
      -webkit-transition: all 0.4s;
      -o-transition: all 0.4s;
    }

    div.hidden-column {
      visibility: hidden;
    }

    #filter-crew {
      margin-top: 10px;
    }

      #filter-crew > legend {
        margin-bottom: 0px;
      }

    i.head-icon {
      float: left;
      margin-right: 10px;
    }

    div.sms-option {
      transition: all 0.4s;
      -moz-transition: all 0.4s;
      -webkit-transition: all 0.4s;
      -o-transition: all 0.4s;
    }

    img.sms-recipient-avatar {
      border-radius: 100px;
      width: 40px;
      height: 40px;
    }

    div.sms-recipient {
      width: 100%;
    }

    div.sms-recipient-name {
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    }

    div.sms-recipient-cellno {
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    }

    div.sms-recipient {
      height: 100%;
      width: 100%;
      padding: 5px;
      margin-bottom: 10px;
    }

    div.sms-recipient--pending {
      background: #5b85cc;
      color: #fff;
    }

    div.sms-recipient--success {
      background: #5cb85c;
      color: #fff;
    }

    div.sms-recipient--danger {
      background: #e1565a;
      color: #fff;
    }

    div.sms-recipient h5 {
      margin-top: 5px;
      margin-bottom: 5px;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }

    span.progress-column {
      width: 120px;
      margin-right: 20px;
    }

    p.status-description {
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }

    img.sms-recipient-image {
      width: 50px;
      height: 50px;
      border-radius: 60px;
    }

    @media (min-width: 1600px) {
      div.sms-recipient.col-xl-2 {
        width: 12.0%;
        padding-left: 15px;
        padding-right: 15px;
      }
    }

    div.smsbatch-crewgroup-hr {
      /*width: 23%;*/
      background: #4aa3df;
      padding: 10px;
      color: #fff;
      text-align: left;
      -webkit-box-shadow: 5px 5px 9px -2px rgba(89,89,89,1);
      -moz-box-shadow: 5px 5px 9px -2px rgba(89,89,89,1);
      box-shadow: 3px 3px 9px -3px rgba(89,89,89,1);
      margin-right: 5px;
      display: inline-block;
      margin-bottom: 5px;
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    }

      div.smsbatch-crewgroup-hr:hover {
        transform: scale(1.05);
        background: #e1565a;
        cursor: -webkit-grab;
      }

      div.smsbatch-crewgroup-hr > img {
        width: 30px;
        height: 30px;
      }

    div.crewgroup-members {
      /*margin-right: -20px;*/
      height: 100%;
    }

    p.sms-readonly:hover {
      cursor: pointer;
    }

    table.batchcrew-list table.batchcrew-list select,
    table.batchcrew-list input[type='text'],
    table.batchcrew-list input[type='password'],
    table.batchcrew-list input[type='checkbox'],
    table.batchcrew-list input[type='file'],
    table.batchcrew-list textarea {
      background-color: #fff;
      border: 1px solid #ccc;
      /* border-radius: 2px; */
      padding-left: 2px;
    }

    /* Prevent the text contents of draggable elements from being selectable. */
    [draggable] {
      -moz-user-select: none;
      -khtml-user-select: none;
      -webkit-user-select: none;
      user-select: none;
      /* Required to make elements draggable in old WebKit */
      -khtml-user-drag: element;
      -webkit-user-drag: element;
    }

    span.date {
      margin-left: 20px;
    }

    p.msg.bold {
      font-weight: 700 !important;
    }

    table.batchcrew-list tbody tr:first-child td:nth-child(2) {
    }

    div.crewgroup-members {
      border: solid 1px #4aa3df;
      min-width: 200px;
      min-height: 200px;
      display: list-item;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.DivC("page-aside email hidden-xs hidden-sm hidden-md")
        With .Helpers.DivC("")
          With .Helpers.DivC("content")
            With .Helpers.HTMLTag("h2")
              .AddClass("page-title")
              .Helpers.HTML("Smses")
            End With
            With .Helpers.HTMLTag("p")
              .AddClass("description")
              .Helpers.HTML("create and send smses")
            End With
          End With
          With .Helpers.DivC("mail-nav collapse")
            With .Helpers.HTMLTag("ul")
              .AddClass("nav nav-pills nav-stacked")
              With .Helpers.HTMLTag("li")
                .AddClass("menu-option active")
                With .Helpers.HTMLTag("a")
                  .Attributes("href") = "#"
                  .Attributes("id") = "findsms"
                  .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.findSms($element)")
                  With .Helpers.HTMLTag("span")
                    .Helpers.Bootstrap.FontAwesomeIcon("fa-inbox")
                  End With
                  With .Helpers.HTMLTag("span")
                    .Helpers.HTML("Find Smses")
                  End With
                End With
              End With
              With .Helpers.HTMLTag("li")
                .AddClass("menu-option")
                With .Helpers.HTMLTag("a")
                  .Attributes("href") = "#"
                  .Attributes("id") = "newsmsbatch"
                  .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.newSmsBatch($element)")
                  With .Helpers.HTMLTag("span")
                    .Helpers.Bootstrap.FontAwesomeIcon("fa-twitter")
                  End With
                  With .Helpers.HTMLTag("span")
                    .Helpers.HTML("Create Batch")
                  End With
                End With
              End With
              With .Helpers.HTMLTag("li")
                .AddClass("menu-option")
                With .Helpers.HTMLTag("a")
                  .Attributes("href") = "#"
                  .Attributes("id") = "newsms"
                  .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.newSms($element)")
                  With .Helpers.HTMLTag("span")
                    .Helpers.Bootstrap.FontAwesomeIcon("fa-mobile-phone")
                  End With
                  With .Helpers.HTMLTag("span")
                    .Helpers.HTML("Create Manually")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      With h.DivC("content")
        '---------------------------------------------------------------------------------------
        '---------------------------------------------------------------------------------------
        'FIND SMSES-------------------------------------------------------------------FIND SMSES
        '---------------------------------------------------------------------------------------
        '---------------------------------------------------------------------------------------
        With .Helpers.DivC("mail-inbox sms-option sms-option-hidden")
          .Attributes("id") = "find-smses"
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.FindSms()")
          With .Helpers.DivC("head find-sms")
            With .Helpers.Bootstrap.FontAwesomeIcon("fa-search", "fa-4x")
              .IconContainer.AddClass("head-icon")
            End With
            With .Helpers.HTMLTag("h3")
              .Helpers.HTML("Find Smses")
            End With
          End With
          With .Helpers.DivC("filters row")
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.With(Of OBLib.Notifications.Sms.SmsSearchList.Criteria)("ViewModel.SmsSearchListCriteria()")
                With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 0")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.Scenario, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Day")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Day")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , "Created By")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , "Recipient Name")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, "Advanced", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-filter", , Singular.Web.PostBackType.None, "SmsPage.showAdvancedFilters()")
                      .Button.AddClass("btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-refresh", , Singular.Web.PostBackType.None, "SmsPage.refreshSearch()")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 1")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.Scenario, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.StartDay, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Day")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.EndDay, Singular.Web.BootstrapEnums.InputSize.Small, , "End Day")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.RecipientName, Singular.Web.BootstrapEnums.InputSize.Small, , "Recipient Name")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, "Advanced", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-filter", , Singular.Web.PostBackType.None, "SmsPage.showAdvancedFilters()")
                      .Button.AddClass("btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-refresh", , Singular.Web.PostBackType.None, "SmsPage.refreshSearch()")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 2")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.Scenario, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.ProductionSystemAreaStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Day")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.ProductionSystemAreaEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Day")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.ProductionSystemAreaName, Singular.Web.BootstrapEnums.InputSize.Small, , "Event Name")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, "Advanced", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-filter", , Singular.Web.PostBackType.None, "SmsPage.showAdvancedFilters()")
                      .Button.AddClass("btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-refresh", , Singular.Web.PostBackType.None, "SmsPage.refreshSearch()")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 3")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.Scenario, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Created Start")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "150px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Created End")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "200px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , "Created By")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.SmsBatchID, Singular.Web.BootstrapEnums.InputSize.Small, , "Batch Name")
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "200px"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.Sms.SmsSearchList.Criteria) d.SmsBatchCrewTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Crew Type")
                      .Editor.AddClass("input-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, "Advanced", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-filter", , Singular.Web.PostBackType.None, "SmsPage.showAdvancedFilters()")
                      .Button.AddClass("btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-refresh", , Singular.Web.PostBackType.None, "SmsPage.refreshSearch()")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SmsPage.canEditCritria($data, 'RefreshSearchButton')")
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.SmsSearchList().length > 0")
              With .Helpers.Bootstrap.PullLeft
                With .Helpers.Bootstrap.Button(, "Select All on Page", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-sort-amount-asc", , ,
                                               "SmsPage.selectAllOnPage()")
                End With
                With .Helpers.Bootstrap.Button(, "Send Selected", Singular.Web.BootstrapEnums.Style.Info, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-cloud-upload", , ,
                                               "SmsPage.sendSelectedSmses()")
                End With
                With .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-trash", , ,
                                             "SmsPage.deleteSelectedSmses()")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.SmsSearchList().length > 0")
              With .Helpers.Bootstrap.PullRight
                With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "30px"
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-double-left", , ,
                                                   "SmsPage.firstPage()")
                      .Button.AddClass("btn-flat btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "30px"
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-left", , ,
                                                   "SmsPage.previousPage()")
                      .Button.AddClass("btn-flat btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("input")
                    .AddBinding(Singular.Web.KnockoutBindingString.value, "ViewModel.SmsSearchListManager().PageNo()")
                    .AddClass("form-control input-no-radius no-border-right text-center")
                    .Style.Width = "60px"
                  End With
                  With .Helpers.HTMLTag("input")
                    .AddBinding(Singular.Web.KnockoutBindingString.value, "' of ' + ViewModel.SmsSearchListManager().Pages().toString()")
                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                    .AddClass("form-control input-no-radius no-border-right text-center")
                    .Style.Width = "60px"
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "30px"
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-right", , ,
                                                   "SmsPage.nextPage()")
                      .Button.AddClass("btn-flat btn-no-radius no-border-right")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("input-group-btn")
                    .Style.Width = "30px"
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-double-right", , ,
                                                   "SmsPage.lastPage()")
                      .Button.AddClass("btn-flat btn-no-radius")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("mails")
            With .Helpers.ForEach(Of SmsSearch)("ViewModel.SmsSearchList()")
              With .Helpers.DivC("item")
                .AddBinding(Singular.Web.KnockoutBindingString.css, "SmsSearchBO.itemCss($data)")
                With .Helpers.DivC("")
                  .Style.Height = "100%"
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.InEditMode()")
                  With .Helpers.HTMLTag("span")
                    .Style.Height = "100%"
                    .AddClass("date pull-left padding-right-20 progress-column")
                    .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsSearchBO.isSelectedClick($data)")
                    With .Helpers.Bootstrap.FontAwesomeIcon("fa-check fa-4x")
                      .IconContainer.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsSelected()")
                    End With
                    With .Helpers.DivC("progress")
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsSelected()")
                      With .Helpers.Div
                        .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.ProgressBarCss()")
                        .AddBinding(Singular.Web.KnockoutBindingString.style, "SmsSearchBO.progressBarStyle($data)")
                        .AddBinding(Singular.Web.KnockoutBindingString.html, "SmsSearchBO.progressBarHtml($data)")
                      End With
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("date pull-right hidden-xs hidden-sm hidden-md")
                    With .Helpers.HTMLTag("p")
                      .AddClass("msg bold")
                      .Helpers.HTML("Sent Time")
                    End With
                    With .Helpers.HTMLTag("p")
                      .AddClass("msg")
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.FirstSentTimeString()")
                    End With
                  End With
                  With .Helpers.HTMLTag("span")
                    .AddClass("date pull-right hidden-xs hidden-sm hidden-md")
                    With .Helpers.HTMLTag("p")
                      .AddClass("msg bold")
                      .Helpers.HTML("Created")
                    End With
                    With .Helpers.HTMLTag("p")
                      .AddClass("msg")
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.CreatedByName()")
                    End With
                    With .Helpers.HTMLTag("p")
                      .AddClass("msg")
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.CreatedDateString()")
                    End With
                  End With
                  With .Helpers.HTMLTag("p")
                    .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsSearchBO.readOnlyStateClick($data)")
                    .AddClass("msg sms-readonly")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.FullMessage()")
                  End With
                End With
                With .Helpers.DivC("row")
                  .Style.Height = "100%"
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.InEditMode()")
                  '.AddBinding(Singular.Web.KnockoutBindingString.css, "SmsSearchBO.editStateCss($data)")
                  '.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsSearchBO.editStateClick($data)")
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Back", Singular.Web.BootstrapEnums.Style.Danger, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-arrow-left", ,
                                                   Singular.Web.PostBackType.None, "SmsPage.backFromSmsSearch($data, $element)", )
                    End With
                    With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                                                   Singular.Web.PostBackType.None, "SmsPage.saveSmsSearch($data, $element)", )
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "SmsSearchBO.canEdit('SaveButton', $data)")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsSearch) d.FullMessage, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SmsSearchBO.canEdit('FullMessage', $data)")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.ForEachTemplate(Of SmsSearchRecipient)("$data.SmsSearchRecipientList()")
                      .AddClass("row")
                      With .Helpers.Bootstrap.Column(6, 6, 4, 3, 2)
                        .AddClass("text-center sms-recipient")
                        With .Helpers.Div
                          .AddBinding(Singular.Web.KnockoutBindingString.css, "$data.State()")
                          With .Helpers.HTMLTag("h5")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.RecipientName()")
                          End With
                          With .Helpers.HTMLTag("p")
                            With .Helpers.HTMLTag("img")
                              .AddClass("sms-recipient-image")
                              .AddBinding(Singular.Web.KnockoutBindingString.src, "SmsSearchRecipientBO.imgSource($data)")
                            End With
                          End With
                          With .Helpers.HTMLTag("p")
                            .AddClass("msg")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.CellNo()")
                          End With
                          With .Helpers.HTMLTag("p")
                            .AddClass("msg status-description")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.StatusDescription()")
                          End With
                          With .Helpers.HTMLTag("p")
                            .AddClass("msg")
                            .AddBinding(Singular.Web.KnockoutBindingString.html, "$data.StatusDateString()")
                          End With
                        End With
                        With .Helpers.Div
                          With .Helpers.Bootstrap.Button(, "Resend", BootstrapEnums.Style.DefaultStyle, ,
                                                         BootstrapEnums.ButtonSize.ExtraSmall, , "fa-cloud-upload", ,
                                                         PostBackType.None, "SmsSearchRecipientBO.retrySmsRecipient($data)", )
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With


        '---------------------------------------------------------------------------------------
        '---------------------------------------------------------------------------------------
        'CREATE SINGLE SMS-----------------------------------------------------CREATE SINGLE SMS
        '---------------------------------------------------------------------------------------
        '---------------------------------------------------------------------------------------
        With .Helpers.DivC("mail-inbox sms-option sms-option-hidden")
          .Attributes("id") = "create-sms"
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.CurrentSms()")
          With .Helpers.DivC("head create-sms")
            With .Helpers.Bootstrap.FontAwesomeIcon("fa-mobile-phone", "fa-4x")
              .IconContainer.AddClass("head-icon")
            End With
            With .Helpers.HTMLTag("h3")
              .Helpers.HTML("Create Sms")
            End With
          End With
          'With .Helpers.DivC("filters")
          'End With
          With .Helpers.DivC("mails")
            With .Helpers.With(Of OBLib.Notifications.SmS.Sms)("ViewModel.CurrentSms()")
              With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                .Attributes("id") = "sms"
                '.AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.batchCriteriaColumnCss($data)")
                'Batch Details
                With .Helpers.Bootstrap.FlatBlock("Sms")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.MessageHolder()
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As Sms) d.SmsTemplateTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Sms) d.SmsTemplateTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Select Recipients")
                          With .Helpers.Bootstrap.Button(, "Select Recipients", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-hand-pointer-o", ,
                                                         Singular.Web.PostBackType.None, "SmsPage.selectRecipients()", )
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As Sms) d.Message)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Sms) d.Message, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.Style.Height = "200px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 9, 9, 9)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As Sms) d.DateToSend)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As Sms) d.DateToSend, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As Sms) d.SmsesRequired)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As Sms) d.SmsesRequired, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                                                       Singular.Web.PostBackType.None, "SmsPage.saveSms($data)", )
                        End With
                      End With
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As Sms) d.SmsesRequired)
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d As Sms) d.SmsesRequired, Singular.Web.BootstrapEnums.InputSize.Small)
                      '      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                      '    End With
                      '  End With
                      'End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 8, 8)
                .Attributes("id") = "sms-recipients"
                With .Helpers.Bootstrap.FlatBlock("Recipients", , , , , )
                  With .ContentTag
                    With .Helpers.ForEachTemplate(Of SmsRecipient)("$data.SmsRecipientList()")
                      .AddClass("row")
                      With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                        With .Helpers.DivC("sms-recipient btn btn-md btn-default")
                          .AddBinding(Singular.Web.KnockoutBindingString.html, "SmsRecipientBO.smsRecipientHTML($data)")
                        End With
                      End With
                    End With
                  End With
                End With
                ''.AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.selectRecipientsColumnCss($data)")
                '.Attributes("id") = "select-sms-recipients"
                ''.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.SelectRecipientsManually()")
                '.Helpers.Control(New NewOBWeb.SmsNotifications.SelectRecipients(Of NewOBWeb.SmsesVM))
              End With
            End With
          End With
        End With


        '---------------------------------------------------------------------------------------
        '---------------------------------------------------------------------------------------
        'CREATE SMS BATCH-------------------------------------------------------CREATE SMS BATCH
        '---------------------------------------------------------------------------------------
        '---------------------------------------------------------------------------------------
        With .Helpers.DivC("mail-inbox sms-option sms-option-hidden")
          .Attributes("id") = "create-batch"
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.CurrentSmsBatch()")
          With .Helpers.DivC("head create-batch")
            With .Helpers.Bootstrap.FontAwesomeIcon("fa-twitter", "fa-4x")
              .IconContainer.AddClass("head-icon")
            End With
            With .Helpers.HTMLTag("h3")
              .Helpers.HTML("Create Sms Batch")
            End With
          End With
          'With .Helpers.DivC("filters")
          'End With
          With .Helpers.DivC("mails")
            With .Helpers.With(Of OBLib.Notifications.SmS.SmsBatch)("ViewModel.CurrentSmsBatch()")
              With .Helpers.Bootstrap.Column(12, 12, 5, 3, 3)
                .Attributes("id") = "batch-criteria"
                .AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.batchCriteriaColumnCss($data)")
                'Batch Details
                With .Helpers.Bootstrap.FlatBlock("Batch")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.MessageHolder()
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As SmsBatch) d.SmsBatchTemplateID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.SmsBatchTemplateID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As SmsBatch) d.BatchName)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.BatchName, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As SmsBatch) d.BatchDescription)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.BatchDescription, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 1")
                        .Helpers.Control(New NewOBWeb.SmsNotifications.ScheduleProductionServices(Of NewOBWeb.SmsVM))
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 5")
                        .Helpers.Control(New NewOBWeb.SmsNotifications.ScheduleProductionServices(Of NewOBWeb.SmsVM))
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 6")
                        .Helpers.Control(New NewOBWeb.SmsNotifications.ScheduleProductionServices(Of NewOBWeb.SmsVM))
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 2")
                        .Helpers.Control(New NewOBWeb.SmsNotifications.SchedulePlayoutOperations(Of NewOBWeb.SmsVM))
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 4")
                        .Helpers.Control(New NewOBWeb.SmsNotifications.ScheduleICR(Of NewOBWeb.SmsVM))
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 3")
                        .Helpers.Control(New NewOBWeb.SmsNotifications.ScheduleOB(Of NewOBWeb.SmsVM))
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Button(, "Generate Smses", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-gears", ,
                                                         Singular.Web.PostBackType.None, "SmsPage.saveBatch($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid() && $data.IsNew()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 7, 9, 9)
                .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() != 3")
                .AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.selectRecipientsColumnCss($data)")
                .Attributes("id") = "select-recipients"
                '.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.SelectRecipientsManually()")
                .Helpers.Control(New NewOBWeb.SmsNotifications.SelectRecipientsManually(Of NewOBWeb.SmsVM))
              End With
              With .Helpers.Bootstrap.Column(12, 12, 7, 9, 9)
                .AddBinding(Singular.Web.KnockoutBindingString.if, "$data.SmsBatchTemplateID() == 3")
                With .Helpers.Bootstrap.FlatBlock("Crew", True, , , , )
                  With .AboveContentTag
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.HTML.Heading3("Available Keywords - {RecipientFirstName}  {SenderFirstName}")
                    End With
                  End With
                  With .ContentTag
                    With .Helpers.Bootstrap.TableFor(Of SmsBatchCrewGroup)("$data.SmsBatchCrewGroupList()", True, True, False, True, True, True, True, "SmsBatchCrewGroupList", "SmsPage.afterSmsBatchCrewGroupAdded")
                      .AddClass("batchcrew-list")
                      With .FirstRow
                        With .AddColumn("")
                          .Style.Width = "500px"
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As SmsBatchCrewGroup) d.GroupName)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatchCrewGroup) d.GroupName, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As SmsBatchCrewGroup) d.Template)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatchCrewGroup) d.Template, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .AddColumn("Crew")
                          .Style.Height = "100%"
                          .AddClass("crewgroup-members")
                          With .Helpers.ForEach(Of SmsBatchCrewGroupHumanResource)("$data.SmsBatchCrewGroupHumanResourceList()")
                            With .Helpers.DivC("smsbatch-crewgroup-hr col-xs-11 col-sm-11 col-md-5 col-lg-5 col-xl-5")
                              .Attributes("draggable") = "true"
                              .AddBinding(Singular.Web.KnockoutBindingString.title, "$data.HRName()")
                              With .Helpers.HTMLTag("img")
                                .AddBinding(KnockoutBindingString.src, "SmsBatchCrewGroupHumanResourceBO.src($data)")
                              End With
                              With .Helpers.Span
                                .AddBinding(KnockoutBindingString.html, "$data.HRName()")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

      With h.Bootstrap.Dialog("FindSms", "Advanced Filters", False, "modal-sm", Singular.Web.BootstrapEnums.Style.Primary, , "fa-search", , )
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Criteria", , , , , True)
                'With .ActionsDiv
                '  .Helpers.HTML("BLha")
                'End With
                With .ContentTag
                  With .Helpers.With(Of OBLib.Notifications.SmS.SmsSearchList.Criteria)("ViewModel.SmsSearchListCriteria()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.Scenario).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.Scenario, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                    End With
                    '--------------------------------------------------------------------------------------
                    'SCENARIO 0
                    '--------------------------------------------------------------------------------------
                    With .Helpers.Bootstrap.Row
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 0")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                    End With
                    '--------------------------------------------------------------------------------------
                    'SCENARIO 1
                    '--------------------------------------------------------------------------------------
                    With .Helpers.Bootstrap.Row
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 1")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.StartDay).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.StartDay, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.EndDay).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.EndDay, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate).Style.Width = "100%"
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      '    End With
                      '  End With
                      'End With
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate).Style.Width = "100%"
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      '    End With
                      '  End With
                      'End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                    End With
                    '--------------------------------------------------------------------------------------
                    'SCENARIO 2
                    '--------------------------------------------------------------------------------------
                    With .Helpers.Bootstrap.Row
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 2")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.FieldSet("Sub-Depts and Areas")
                          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "SmsSearchListCriteriaBO.SystemSelected($data, ViewModel.SmsSearchListCriteria())")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                              .Button.AddClass("btn-block buttontext")
                            End With
                            With .Helpers.Bootstrap.Row
                              .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                                With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                                  .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                    .Button.AddBinding(KnockoutBindingString.click, "SmsSearchListCriteriaBO.ProductionAreaSelected($data, ViewModel.SmsSearchListCriteria())")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                    .Button.AddClass("btn-block buttontext")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.ProductionSystemAreaStartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.ProductionSystemAreaStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.ProductionSystemAreaEndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.ProductionSystemAreaEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.ProductionSystemAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.ProductionSystemAreaID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate).Style.Width = "100%"
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      '    End With
                      '  End With
                      'End With
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate).Style.Width = "100%"
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      '    End With
                      '  End With
                      'End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                    End With
                    '--------------------------------------------------------------------------------------
                    'SCENARIO 3
                    '--------------------------------------------------------------------------------------                       
                    With .Helpers.Bootstrap.Row
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 3")
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.SmsBatchID).Style.Width = "100%"
                      '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.SmsBatchID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                      '    End With
                      '  End With
                      'End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Notifications.SmS.SmsSearchList.Criteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.Button(, "Search", Singular.Web.BootstrapEnums.Style.Primary, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                             Singular.Web.PostBackType.None, "SmsPage.refreshSearch()", )
          End With
        End With
      End With

      h.Control(New NewOBWeb.SmsNotifications.SelectRecipientsModal(Of NewOBWeb.SmsVM))

    End Using%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomSideMenu" runat="server">
</asp:Content>
