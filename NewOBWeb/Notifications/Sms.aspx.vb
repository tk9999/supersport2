﻿Imports Singular.SmsSending
Imports Singular.DataAnnotations
Imports OBLib.Notifications.SmS
Imports Singular.Web
Imports OBLib.Notifications
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Notifications.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

Public Class Sms
  Inherits OBPageBase(Of SmsVM)

End Class


Public Class SmsVM
  Inherits OBViewModelStateless(Of SmsVM)

  'Page
  Public Property CreateSms As Boolean
  Public Property GenerateSms As Boolean
  Public Property FindSms As Boolean

  Public Property CurrentSms As OBLib.Notifications.SmS.Sms
  Public Property CurrentSmsBatch As SmsBatch

  'Find Sms
  <InitialDataOnly>
  Public Property SmsSearchList As OBLib.Notifications.Sms.SmsSearchList = New OBLib.Notifications.Sms.SmsSearchList
  <InitialDataOnly>
  Public Property SmsSearchListCriteria As OBLib.Notifications.Sms.SmsSearchList.Criteria = New OBLib.Notifications.Sms.SmsSearchList.Criteria
  <InitialDataOnly>
  Public Property SmsSearchListManager As Singular.Web.Data.PagedDataManager(Of SmsVM) = New Singular.Web.Data.PagedDataManager(Of SmsVM)(Function(d) Me.SmsSearchList,
                                                                                                                                            Function(d) Me.SmsSearchListCriteria,
                                                                                                                                            "RecipientName", 50)

  'Recipients
  <InitialDataOnly>
  Public Property SelectedRecipients As List(Of ROHumanResourceContact) = New List(Of ROHumanResourceContact)

  <InitialDataOnly>
  Public Property UserSystemList As OBLib.Security.UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

#Region " Criteria "

  <InitialDataOnly>
  Public Property SelectableDisciplines As List(Of RODisciplineSelect) = New List(Of RODisciplineSelect)

  <InitialDataOnly>
  Public Property SelectableContractTypes As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

  <InitialDataOnly>
  Public Property SelectableHumanResources As List(Of ROHumanResourceContact) = New List(Of ROHumanResourceContact)
  <InitialDataOnly>
  Public Property SelectableHumanResourcesCriteria As OBLib.HR.ReadOnly.ROHumanResourceContactList.Criteria = New OBLib.HR.ReadOnly.ROHumanResourceContactList.Criteria
  <InitialDataOnly>
  Public Property SelectableHumanResourcesManager As Singular.Web.Data.PagedDataManager(Of SmsVM) = New Singular.Web.Data.PagedDataManager(Of SmsVM)(Function(d) Me.SelectableHumanResources,
                                                                                                                                                     Function(d) Me.SelectableHumanResourcesCriteria,
                                                                                                                                                     "RecipientName", 1000)

#End Region

  Protected Overrides Sub Setup()
    MyBase.Setup()

    CreateSms = False
    GenerateSms = False
    FindSms = False
    SmsSearchListManager.SortAsc = True

    SelectableHumanResourcesCriteria.PageNo = 1
    SelectableHumanResourcesCriteria.PageSize = 1000
    SelectableHumanResourcesManager.PageSize = 1000

    ClientDataProvider.AddDataSource("ROSystemAllowedAreaList", OBLib.CommonData.Lists.ROSystemAllowedAreaList, False)
    ClientDataProvider.AddDataSource("ROSmsBatchTemplateList", OBLib.CommonData.Lists.ROSmsBatchTemplateList, False)
    ClientDataProvider.AddDataSource("ROSmsTemplateTypeList", OBLib.CommonData.Lists.ROSmsTemplateTypeList, False)
    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaAllowedDisciplineList", OBLib.CommonData.Lists.ROProductionAreaAllowedDisciplineList, False)
    ClientDataProvider.AddDataSource("UserSystemList", UserSystemList, False)
    ClientDataProvider.AddDataSource("ROSmsSearchScenarioList", OBLib.CommonData.Lists.ROSmsSearchScenarioList, False)

  End Sub

End Class