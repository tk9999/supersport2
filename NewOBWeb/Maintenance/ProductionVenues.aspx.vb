﻿Imports Singular
Imports OBLib.Maintenance.Productions
Imports System

Public Class ProductionVenues
  Inherits OBPageBase(Of ProductionVenuesVM)

End Class

Public Class ProductionVenuesVM
  Inherits OBViewModel(Of ProductionVenuesVM)

  Public Property ROProductionVenuePagedList As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList
  Public Property ROProductionVenuePagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria
  Public Property ROProductionVenuePagedListPagingManager As Singular.Web.Data.PagedDataManager(Of ProductionVenuesVM) 'Singular.Web.Data.EditablePagedDataManager(Of ProductionVenuesVM)

  Public Property CurrentProductionVenue As OBLib.Maintenance.Productions.ProductionVenue = Nothing

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    ROProductionVenuePagedList = New OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList
    ROProductionVenuePagedListCriteria = New OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria
    ROProductionVenuePagedListPagingManager = New Singular.Web.Data.PagedDataManager(Of ProductionVenuesVM)(Function(d) Me.ROProductionVenuePagedList, Function(d) Me.ROProductionVenuePagedListCriteria, "ProductionVenue", 20) 'Singular.Web.Data.EditablePagedDataManager(Of ProductionVenuesVM)(Function(d) Me.ProductionVenueList, Function(d) Me.ProductionVenueListCriteria, "ProductionVenue", 20)
    ROProductionVenuePagedListCriteria.ProductionVenue = ""
    ROProductionVenuePagedListCriteria.SortColumn = "ProductionVenue"
    ROProductionVenuePagedListCriteria.PageNo = 1
    ROProductionVenuePagedListCriteria.PageSize = 20

        ClientDataProvider.AddDataSource("ROCityList", OBLib.CommonData.Lists.ROCityList, False)
        ClientDataProvider.AddDataSource("ROCountryList", OBLib.CommonData.Lists.ROCountryList, False)
        ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
        ClientDataProvider.AddDataSource("ROOutsourceServiceTypeList", OBLib.CommonData.Lists.ROOutsourceServiceTypeList, False)
    End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "AddNew"
        CurrentProductionVenue = New ProductionVenue

      Case "Delete"
        Dim ProductionVenueList As ProductionVenueList = New ProductionVenueList
        ProductionVenueList.Add(CurrentProductionVenue)
        ProductionVenueList.Remove(CurrentProductionVenue)
        Try
          ProductionVenueList = TrySave(ProductionVenueList).SavedObject
          CurrentProductionVenue = Nothing
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try

      Case "EditProductionVenue"
        Try
          Dim ProductionVenueList As ProductionVenueList = OBLib.Maintenance.Productions.ProductionVenueList.GetProductionVenueList(CommandArgs.ClientArgs.ProductionVenueID)
          CurrentProductionVenue = ProductionVenueList(0)
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try

      Case "Save"
        Try
          Dim ProductionVenueList As ProductionVenueList = New ProductionVenueList
          ProductionVenueList.Add(CurrentProductionVenue)
          ProductionVenueList = TrySave(ProductionVenueList).SavedObject
          CurrentProductionVenue = ProductionVenueList(0)
          OBLib.CommonData.Lists.Refresh("ProductionVenueList")
          OBLib.CommonData.Lists.Refresh("ROProductionVenueList")
          OBLib.CommonData.Lists.Refresh("ROProductionVenuePagedList")
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try

    End Select

  End Sub

End Class