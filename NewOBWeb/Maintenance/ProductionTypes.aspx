﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ProductionTypes.aspx.vb" Inherits="NewOBWeb.ProductionTypes" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.Productions.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.Productions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      
      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Manage Production Types", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Button("Back", "Back", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", , Singular.Web.PostBackType.Ajax, )
                End With
                With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindProductionType()")
                End With
                With .Helpers.Bootstrap.Button("AddNew", "New", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Toolbar
                  .Helpers.MessageHolder()
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----Current Production Type------------------------------------------------------------------------------------------------
          With h.Bootstrap.Row
              With .Helpers.With(Of ProductionType)(Function(d) ViewModel.CurrentProductionType)
                  With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                      With .Helpers.Bootstrap.FlatBlock(, True)
                          .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) ViewModel.CurrentProductionType.ProductionType)
                          With .AboveContentTag
                              With .Helpers.Bootstrap.Row
                                  With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                                  End With
                                  With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                                  End With
                              End With
                          End With
                          With .ContentTag
                              With .Helpers.Bootstrap.Row
                                  With .Helpers.With(Of ProductionType)(Function(d) ViewModel.CurrentProductionType)
                                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                              .Helpers.LabelFor(Function(d) d.ProductionType)
                                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionType, Singular.Web.BootstrapEnums.InputSize.Small, )
                      
                                              End With
                                          End With
                                      End With
                                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Large)
                                              .Helpers.LabelFor(Function(d As ProductionType) d.ImportedInd)
                                              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                                  With .Helpers.Bootstrap.ROStateButton(Function(d) d.ImportedInd, , , , , , , "btn-med")
                                                  End With
                                              End With
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                      End With
                  End With
                  
                          
                  With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                      With .Helpers.Bootstrap.FlatBlock("Event Types")
                          With .ContentTag
                              With .Helpers.Bootstrap.Row
                                  With .Helpers.With(Of ProductionType)(Function(d) ViewModel.CurrentProductionType)
                                      With .Helpers.Bootstrap.TableFor(Of EventType)(Function(d) ViewModel.CurrentProductionType.EventTypeList, True, True, False, False, True, True, False)
                                          .AddClass("no-border hover list")
                                          .TableBodyClass = "no-border-y"
                                          With .FirstRow
                                              '.AddClass("items")
                                              With .AddColumn(Function(d As EventType) d.EventType)
                                                  '.AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                                              End With
                                              With .AddColumn("Mag Show?")
                                                  With .Helpers.Bootstrap.StateButton(Function(d) d.MagazineShowInd, , , , , , , )
                                                  End With
                                              End With
                                              With .AddColumn("OB?")
                                                  With .Helpers.Bootstrap.StateButton(Function(d) d.OBInd, , , , , , , )
                                                  End With
                                              End With
                                              With .AddColumn("Playout?")
                                                  With .Helpers.Bootstrap.StateButton(Function(d) d.PlayoutInd, , , , , , , )
                                                  End With
                                              End With
                                              With .AddColumn("Studio?")
                                                  With .Helpers.Bootstrap.StateButton(Function(d) d.StudioInd, , , , , , , )
                                                  End With
                                              End With
                                              With .AddColumn("Highlights")
                                                  With .Helpers.Bootstrap.StateButton(Function(d) d.HighlightsInd, , , , , , , )
                                                  End With
                                              End With
                                              With .AddColumn("Imported?")
                                                  With .Helpers.Bootstrap.ROStateButton(Function(d) d.ImportedInd, , , , , , , )
                                                  End With
                                              End With
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                      End With
                  End With
              End With
          End With

  
      
          With h.Bootstrap.Dialog("FindProductionType", "Find Production Type")
              .ModalDialogDiv.AddClass("modal-sm")
              With .Body
                  .AddClass("modal-background-gray")
                  With .Helpers.Bootstrap.FlatBlock("Production Types", True)
                      .FlatBlockTag.AddClass("flat-block-paged")
                      With .AboveContentTag
                          With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                      With .Helpers.EditorFor(Function(d) ViewModel.ROProductionTypePagedListCriteria.ProductionType)
                                          .AddClass("form-control input-sm")
                                          .Attributes("placeholder") = "Search by Production Type"
                                          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RefreshROProductionTypePagedList() }")
                                      End With
                                  End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                      With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", , Singular.Web.PostBackType.None, "RefreshROProductionVenuePagedList()")
                                          '.Button.AddClass("btn-block")
                                      End With
                                  End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  
                              End With
                          End With
                      End With
                      With .ContentTag
                          With .Helpers.DivC("table-responsive")
                              With .Helpers.Bootstrap.PagedGridFor(Of ROProductionTypePaged)(Function(vm) ViewModel.ROProductionTypePagedListManager,
                                                                                             Function(vm) ViewModel.ROProductionTypePagedList,
                                                                                             False, False, False, False, True, True, False,
                                                                                             "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                                  .AddClass("no-border hover list")
                                  .TableBodyClass = "no-border-y"
                                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                                  With .FirstRow
                                      .AddClass("items")
                                      With .AddColumn("")
                                          .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                                          With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditProductionType($data)")
                                          End With
                                      End With
                                      With .AddReadOnlyColumn(Function(d As ROProductionTypePaged) d.ProductionType)
                                          .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                                          .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                      End With
                                      With .AddReadOnlyColumn(Function(d As ROProductionTypePaged) d.ImportedInd)
                                          .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                                          .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                      End With
                                  End With
                                  With .FooterRow
                    
                                  End With
                              End With
                          End With
                      End With
                  End With
              End With
          End With
          

      
      
      End Using%>
  <script type="text/javascript">

    $(function () {
      Singular.OnPageLoad(function () {

      });
    });

    function FindProductionType() {
      RefreshROProductionTypePagedList();
      $("#FindProductionType").modal();
    }

    function EditProductionType(ROProductionType) {
      //Singular.GetDataStateless("OBLib.Maintenance.Productions.ProductionTypeList, OBLib",
      //  {
      //    ProductionTypeID: ROProductionType.ProductionTypeID(),
      //    SortColumn: "ProductionType",
      //    SortAsc: true,
      //    PageNo: 1,
      //    PageSize: 1
      //  },
      //  function (response) {
      //    ViewModel.CurrentProductionType.Set(response.Data[0]);
      //  })
      Singular.SendCommand("EditProductionType",
                           {
                             ProductionTypeID: ROProductionType.ProductionTypeID()
                           },
                           function (response) {
                             $("#FindProductionType").modal('hide');
                           });
    };

    function RefreshROProductionTypePagedList() {
      ViewModel.ROProductionTypePagedListManager().Refresh();
    };

    //function DeleteProductionType(ProductionType) {
    //  ViewModel.ProductionTypeList.Remove(ProductionType);
    //}

    function AddNewProductionType() {
      var npo = ViewModel.ProductionTypeList.AddNew();
      npo.ProductionType("");
    };

    //function FilterEventTypeList() {
    //  ViewModel.EventTypeListPagingManager().Refresh();
    //}

    function AddNewEventType() {
      var net = ViewModel.CurrentProductionType().EventTypeList.AddNew();
      net.EventType("");
      net.ProductionTypeID(ViewModel.CurrentProductionType().ProductionTypeID());
    };

    function DeleteEventType(EventType) {
      ViewModel.CurrentProductionType().EventTypeList.Remove(EventType);
    }

    //function ShowEventTypes(ProductionType) {
    //  ViewModel.CurrentProductionType(null);
    //  ViewModel.CurrentProductionType(ProductionType);
    //  ViewModel.EventTypeListCriteria().ProductionTypeID(ProductionType.ProductionTypeID());
    //  ViewModel.EventTypeListPagingManager().PageNo(1);
    //  ViewModel.EventTypeListPagingManager().Refresh();
    //  ShowEventTypeDialog();
    //};

    //function ShowEventTypeDialog() {
    //  $("#EventTypesDialog").modal();
    //};

  </script>
</asp:Content>
