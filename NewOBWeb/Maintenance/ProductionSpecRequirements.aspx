﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master"
  CodeBehind="ProductionSpecRequirements.aspx.vb" Inherits="NewOBWeb.ProductionSpecRequirements" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.SystemManagement" %>

<%@ Import Namespace="OBLib.Maintenance.SystemManagement.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSpecRequirements.js"></script>
  <script type="text/javascript" src="../Scripts/Pages/ProductionSpecRequirementsPage.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


  <% Using h = Helpers
         
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Spec Requirements", True)
            With .AboveContentTag
              With .Helpers.With(Of ROProductionSpecRequirementPagedList.Criteria)("ViewModel.ROProductionSpecRequirementPagedListCriteria()")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 1)
                    .Helpers.Bootstrap.LabelDisplay("New")
                    With .Helpers.Bootstrap.Button(, "New", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                    "fa-plus-circle", , , "ProductionSpecRequirementsPage.addNewSpec()", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                    .Helpers.Bootstrap.LabelFor(Function(d As ROProductionSpecRequirementPagedList.Criteria) d.SystemID)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROProductionSpecRequirementPagedList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                    .Helpers.Bootstrap.LabelFor(Function(d As ROProductionSpecRequirementPagedList.Criteria) d.ProductionAreaID)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROProductionSpecRequirementPagedList.Criteria) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                    .Helpers.Bootstrap.LabelFor(Function(d As ROProductionSpecRequirementPagedList.Criteria) d.ProductionSpecRequirementName)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROProductionSpecRequirementPagedList.Criteria) d.ProductionSpecRequirementName, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 2, 1)
                    .Helpers.Bootstrap.LabelDisplay("Refresh")
                    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                    "fa-refresh", , , "ProductionSpecRequirementsPage.refresh()", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.PagedGridFor(Of ROProductionSpecRequirementPaged)("ViewModel.ROProductionSpecRequirementPagedListManager", "ViewModel.ROProductionSpecRequirementPagedList",
                                                                                        False, False, False, False, True, True, True, ,
                                                                                        Singular.Web.BootstrapEnums.PagerPosition.Bottom, , , True, )
                With .FirstRow
                  With .AddColumn("")
                    With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-edit", , , "ProductionSpecRequirementsPage.editSpec($data)")
                    End With
                  End With
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.ProductionSpecRequirementName)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.SubDept)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.Area)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.Room)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.ProductionType)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.EventType)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.StartDate)
                  .AddReadOnlyColumn(Function(d As ROProductionSpecRequirementPaged) d.EndDate)
                End With
              End With
            End With
          End With
        End With
      End With
         
      
      '-----Current Spec------------
      With h.Bootstrap.Dialog("CurrentProductionSpecRequirement", "Spec Requirement", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-video-camera", , )
        With .ContentDiv
          With .Helpers.With(Of ProductionSpecRequirement)(Function(d) ViewModel.ProductionSpecRequirement)
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
              With .Helpers.With(Of ProductionSpecRequirement)(Function(d) ViewModel.ProductionSpecRequirement)
                With .Helpers.Bootstrap.FlatBlock(, False)
                  .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.ProductionSpecRequirementName)
                  'With .AboveContentTag
                  '  With .Helpers.Bootstrap.Row
                  '    With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                  '      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                  '    End With
                  '    With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                  '      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                  '    End With
                  '    With .Helpers.Bootstrap.Button(, "Copy", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-copy", , Singular.Web.PostBackType.None, "CopySpec($data)")
                  '      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                  '    End With
                  '  End With
                  'End With
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.ProductionSpecRequirementName)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionSpecRequirementName, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.SystemID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.ProductionAreaID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.RoomID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.RoomID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.ProductionAreaID() == 2")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.ProductionTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Production Type")
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.ProductionAreaID() == 1")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.EventTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.EventTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Event Type")
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.ProductionAreaID() == 1")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.StartDate)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d) d.EndDate)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 9, 9)
              With .Helpers.Bootstrap.TabControl(, "nav-pills nav-justififed", )
                With .AddTab("CurrentEquipment", "fa fa-database", , " Equipment", )
                  With .TabPane
                    With .Helpers.With(Of ProductionSpecRequirement)(Function(c) ViewModel.ProductionSpecRequirement)
                      With .Helpers.BootstrapTableFor(Of ProductionSpecRequirementEquipmentType)(Function(c) c.ProductionSpecRequirementEquipmentTypeList, False, False, "", False)
                        With .FirstRow
                          With .AddColumn(Function(d As ProductionSpecRequirementEquipmentType) d.EquipmentTypeID)
                          End With
                          With .AddColumn(Function(d As ProductionSpecRequirementEquipmentType) d.EquipmentSubTypeID)
                          End With
                          With .AddColumn(Function(d As ProductionSpecRequirementEquipmentType) d.EquipmentQuantity)
                          End With
                          With .AddColumn("")
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash-o", ,
                                                           Singular.Web.PostBackType.None, "ProductionSpecRequirementsPage.deleteEquipmentType($data)")
                            End With
                          End With
                        End With
                        With .FooterRow
                          With .AddColumn("")
                            .ColSpan = 5
                            With .Helpers.Bootstrap.Button(, "Add New", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus", ,
                                                           Singular.Web.PostBackType.None, "ProductionSpecRequirementsPage.addEquipmentType($data)")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .AddTab("CurrentPositions", "fa fa-dot-circle-o", , "Positions", )
                  With .TabPane
                    With .Helpers.With(Of ProductionSpecRequirement)(Function(c) ViewModel.ProductionSpecRequirement)
                      With .Helpers.BootstrapTableFor(Of ProductionSpecRequirementPosition)(Function(c) c.ProductionSpecRequirementPositionList, False, False, "", False)
                        With .FirstRow
                          With .AddColumn(Function(d As ProductionSpecRequirementPosition) d.DisciplineID)
                          End With
                          With .AddColumn(Function(d As ProductionSpecRequirementPosition) d.PositionID)
                          End With
                          With .AddColumn(Function(d As ProductionSpecRequirementPosition) d.EquipmentSubTypeID)
                          End With
                          With .AddColumn(Function(d As ProductionSpecRequirementPosition) d.EquipmentQuantity)
                          End With
                          With .AddColumn("")
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash-o", ,
                                                           Singular.Web.PostBackType.None, "ProductionSpecRequirementsPage.deletePosition($data)")
                            End With
                          End With
                        End With
                        With .FooterRow
                          With .AddColumn("")
                            .ColSpan = 5
                            With .Helpers.Bootstrap.Button(, "Add New", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus", ,
                                                           Singular.Web.PostBackType.None, "ProductionSpecRequirementsPage.addPosition($data)")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.PullRight
            With .Helpers.With(Of ProductionSpecRequirement)(Function(c) ViewModel.ProductionSpecRequirement)
              With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             Singular.Web.PostBackType.None, "ProductionSpecRequirementsPage.saveSpec($data)")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) d.IsValid)
              End With
            End With
          End With
        End With
      End With
      'With h.Bootstrap.Row
      '  With .Helpers.With(Of ProductionSpecRequirement)(Function(d) ViewModel.CurrentSpec)
      '    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '      With .Helpers.Bootstrap.FlatBlock(, True)
      '        .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) ViewModel.CurrentSpec.ProductionSpecRequirementName)
      '        With .AboveContentTag
      '          With .Helpers.Bootstrap.Row
      '            With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
      '            End With
      '            With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
      '            End With
      '            With .Helpers.Bootstrap.Button(, "Copy", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-copy", , Singular.Web.PostBackType.None, "CopySpec($data)")
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
      '            End With
      '          End With
      '        End With
      '        With .ContentTag
      '          With .Helpers.Bootstrap.Row
      '            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '              'With .Helpers.DivC("form-horizontal group-border-dashed")
      '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                .Helpers.LabelFor(Function(d) ViewModel.CurrentSpec.ProductionSpecRequirementName)
      '                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSpec.ProductionSpecRequirementName, Singular.Web.BootstrapEnums.InputSize.Small)
      '                End With
      '              End With
      '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                .Helpers.LabelFor(Function(d) ViewModel.CurrentSpec.ProductionTypeID)
      '                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSpec.ProductionTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Production Type")
                  
      '                End With
      '              End With
      '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                .Helpers.LabelFor(Function(d) ViewModel.CurrentSpec.EventTypeID)
      '                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSpec.EventTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Event Type")
                  
      '                End With
      '              End With
      '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                .Helpers.LabelFor(Function(d) ViewModel.CurrentSpec.StartDate)
      '                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSpec.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  
      '                End With
      '              End With
      '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                .Helpers.LabelFor(Function(d) ViewModel.CurrentSpec.EndDate)
      '                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSpec.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  
      '                End With
      '              End With
      '              If OBLib.Security.Settings.CurrentUser.ProductionAreaID = OBLib.CommonData.Enums.ProductionArea.Studio Then
      '                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                  .Helpers.LabelFor(Function(d) ViewModel.CurrentSpec.RoomID)
      '                  With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.CurrentSpec.RoomID, Singular.Web.BootstrapEnums.InputSize.Small)
      '                  End With
      '                End With
      '              End If
      '              'End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
        
      
    End Using%>
  <style type="text/css">
    #CurrentSpec {
      display: none;
    }

    body {
      background-color: #F0F0F0;
    }
  </style>
  <script type="text/javascript">

    //function RefreshProductionSpecRequirements() {
    //  ViewModel.ROProductionSpecRequirementPagedListPagingInfo().Refresh();
    //};

    //function EditProductionSpec(ROSpec) {
    //  Singular.SendCommand("GetSpec", { ProductionSpecRequirementID: ROSpec.ProductionSpecRequirementID() }, function (response) {
    //    $("#FindProductionSpecRequirement").modal('hide');
    //  });
    //};

    //function ROProductionSpecRowClicked(ROSpec) {
    //  RefreshProductionSpecRequirements();
    //  ViewModel.CurrentROProductionSpec(ROSpec);
    //  $('a[href="#Equipment"]').tab('show');
    //  $("#Positions").removeClass('active');
    //  $('#Equipment').removeClass('active');
    //  $('#Equipment').addClass('active');
    //};

    //function FindProductionSpecRequirement() {
    //  RefreshProductionSpecRequirements();
    //  $("#FindProductionSpecRequirement").modal();
    //};

    //function CopySpec(Spec) {

    //  var NewSpec = ViewModel.TempProductionSpecRequirementList.AddNew(); //new ProductionSpecRequirementObject();
    //  NewSpec.ProductionSpecRequirementName(); //Spec.ProductionSpecRequirementName()
    //  NewSpec.ProductionTypeID(Spec.ProductionTypeID());
    //  NewSpec.EventTypeID(Spec.EventTypeID());
    //  NewSpec.StartDate(Spec.StartDate());
    //  NewSpec.EndDate(Spec.EndDate());
    //  NewSpec.HDRequiredInd(Spec.HDRequiredInd());
    //  NewSpec.SystemID(Spec.SystemID());
    //  NewSpec.ProductionAreaID(Spec.ProductionAreaID());
    //  NewSpec.RoomID(Spec.RoomID());

    //  ViewModel.CurrentSpec(NewSpec);

    //  Spec.ProductionSpecRequirementEquipmentTypeList().Iterate(function (Equip, Index) {
    //    var NewEquip = NewSpec.ProductionSpecRequirementEquipmentTypeList.AddNew(); //new ProductionSpecRequirementEquipmentTypeObject();
    //    NewEquip.EquipmentTypeID(Equip.EquipmentTypeID());
    //    NewEquip.EquipmentSubTypeID(Equip.EquipmentSubTypeID());
    //    NewEquip.EquipmentQuantity(Equip.EquipmentQuantity());
    //    NewEquip.ProductionID(null);
    //    NewEquip.SystemID(Equip.SystemID());
    //    NewEquip.ProductionAreaID(Equip.ProductionAreaID());
    //    NewEquip.ProductionSystemAreaID(null);
    //    NewEquip.RoomID(Equip.RoomID());
    //    NewSpec.ProductionSpecRequirementEquipmentTypeList.push(NewEquip);
    //  });

    //  Spec.ProductionSpecRequirementPositionList().Iterate(function (Pos, Index) {
    //    var NewPos = NewSpec.ProductionSpecRequirementPositionList.AddNew(); //new ProductionSpecRequirementPositionObject();
    //    NewPos.PositionID(Pos.PositionID());
    //    NewPos.EquipmentSubTypeID(Pos.EquipmentSubTypeID());
    //    NewPos.EquipmentQuantity(Pos.EquipmentQuantity());
    //    NewPos.ProductionID(null);
    //    NewPos.SystemID(Pos.SystemID());
    //    NewPos.ProductionAreaID(Pos.ProductionAreaID());
    //    NewPos.ProductionSystemAreaID(null);
    //    NewPos.RoomID(Pos.RoomID());
    //    NewPos.DisciplineID(Pos.DisciplineID());
    //    //NewSpec.ProductionSpecRequirementPositionList.push(NewPos);
    //  });

    //};

    //function DeleteSpec(ProductionSpec) {
    //  if (ProductionSpec.LinkCount() > 0) {
    //    Singular.ShowMessage("Cannot Delete", "Spec is already linked to a production");
    //  } else {
    //    Singular.SendCommand("DeleteSpec", {}, function (response) {
    //      ViewModel.ROProductionSpecRequirementPagedListPagingInfo().Refresh();
    //      $("#CurrentSpec").dialog('destroy');
    //    });
    //  }
    //}

    //function DeleteEquipment(Equipment) {
    //  if (Equipment.GetParent().LinkCount() > 0) {
    //    Singular.ShowMessage("Cannot Delete", "Spec is already linked to a production");
    //  } else {
    //    ViewModel.CurrentSpec().ProductionSpecRequirementEquipmentTypeList.Remove(Equipment);
    //  }
    //}

    //function DeletePosition(Position) {
    //  if (Position.GetParent().LinkCount() > 0) {
    //    Singular.ShowMessage("Cannot Delete", "Spec is already linked to a production");
    //  } else {
    //    ViewModel.CurrentSpec().ProductionSpecRequirementPositionList.Remove(Position);
    //  }
    //}

    //function AddEquipment() {
    //  var NewItem = ViewModel.CurrentSpec().ProductionSpecRequirementEquipmentTypeList.AddNew()
    //  NewItem.SystemID(ViewModel.ROProductionSpecRequirementPagedListCriteria().SystemID())
    //  NewItem.ProductionAreaID(ViewModel.ROProductionSpecRequirementPagedListCriteria().ProductionAreaID())
    //}

    //function AddPosition() {
    //  var NewItem = ViewModel.CurrentSpec().ProductionSpecRequirementPositionList.AddNew()
    //  NewItem.SystemID(ViewModel.ROProductionSpecRequirementPagedListCriteria().SystemID())
    //  NewItem.ProductionAreaID(ViewModel.ROProductionSpecRequirementPagedListCriteria().ProductionAreaID())
    //}

  </script>
</asp:Content>
