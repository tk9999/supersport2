﻿Imports System
Imports OBLib.Maintenance.Productions
Imports OBLib.Maintenance.Productions.ReadOnly

Public Class ProductionTypes
  Inherits OBPageBase(Of ProductionTypesVM)

End Class

Public Class ProductionTypesVM
  Inherits OBViewModel(Of ProductionTypesVM)

#Region " Properties "

  'Public Property ProductionTypeListCriteria As OBLib.Maintenance.Productions.ProductionTypeList.Criteria
  'Public Property ProductionTypeListPagingManager As Singular.Web.Data.PagedDataManager(Of ProductionTypesVM)

  'Public Property EventTypeList As EventTypeList
  'Public Property EventTypeListCriteria As OBLib.Maintenance.Productions.EventTypeList.Criteria
  'Public Property EventTypeListPagingManager As Singular.Web.Data.PagedDataManager(Of ProductionTypesVM)

  Public Property ProductionTypeList As ProductionTypeList
  Public Property ROProductionTypePagedList As ROProductionTypePagedList
  Public Property ROProductionTypePagedListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePagedList.Criteria
  Public Property ROProductionTypePagedListManager As Singular.Web.Data.PagedDataManager(Of ProductionTypesVM)

  Public Property CurrentProductionType As ProductionType

  Public Property ListToRefresh As String

#End Region

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    ROProductionTypePagedList = New ROProductionTypePagedList
    ROProductionTypePagedListCriteria = New OBLib.Maintenance.Productions.ReadOnly.ROProductionTypePagedList.Criteria
    ROProductionTypePagedListManager = New Singular.Web.Data.PagedDataManager(Of ProductionTypesVM)(Function(d) Me.ROProductionTypePagedList,
                                                                                                    Function(d) Me.ROProductionTypePagedListCriteria,
                                                                                                    "ProductionType", 15)
    ROProductionTypePagedListCriteria.ProductionType = ""
    ROProductionTypePagedListCriteria.PageNo = 1
    ROProductionTypePagedListCriteria.PageSize = 15
    ROProductionTypePagedListCriteria.SortAsc = True
    ROProductionTypePagedListCriteria.SortColumn = "ProductionType"

    ProductionTypeList = New ProductionTypeList

    CurrentProductionType = Nothing
    ListToRefresh = ""

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "AddNew"
        CurrentProductionType = New ProductionType

      Case "EditProductionType"
        ProductionTypeList = OBLib.Maintenance.Productions.ProductionTypeList.GetProductionTypeList(CommandArgs.ClientArgs.ProductionTypeID)
        CurrentProductionType = ProductionTypeList(0)

      Case "Delete"
        ProductionTypeList = New ProductionTypeList
        ProductionTypeList.Add(CurrentProductionType)
        ProductionTypeList.Remove(CurrentProductionType)
        Try
          ProductionTypeList = TrySave(ProductionTypeList).SavedObject
          CurrentProductionType = Nothing
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try

      Case "Save"
        ProductionTypeList = New ProductionTypeList
        ProductionTypeList.Add(CurrentProductionType)
        ProductionTypeList = TrySave(ProductionTypeList).SavedObject
        CurrentProductionType = ProductionTypeList(0)

    End Select

    Dim a As Object = OBLib.CommonData.Lists.ROProductionTypeList
    Dim b As Object = OBLib.CommonData.Lists.ROEventTypeList
    OBLib.CommonData.Lists.Refresh("ProductionTypeList")
    OBLib.CommonData.Lists.Refresh("ROProductionTypeList")
    OBLib.CommonData.Lists.Refresh("EventTypeList")
    OBLib.CommonData.Lists.Refresh("ROEventTypeList")
    OBLib.CommonData.Lists.RefreshCached("ProductionTypeList")
    OBLib.CommonData.Lists.RefreshCached("ROProductionTypeList")
    OBLib.CommonData.Lists.RefreshCached("EventTypeList")
    OBLib.CommonData.Lists.RefreshCached("ROEventTypeList")

  End Sub

End Class