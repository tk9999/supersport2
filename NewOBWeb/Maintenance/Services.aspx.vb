﻿Imports Singular.Web.Data
Imports Singular.Web

Public Class Services
  Inherits OBPageBase(Of ServiceVM)

End Class

Public Class ServiceVM
  Inherits OBViewModel(Of ServiceVM)

  Public Property ServerProgramTypeList As Singular.Service.ServerProgramTypeList
  Public Property ServerProgramType As Singular.Service.ServerProgramType

  <Singular.DataAnnotations.ClientOnlyNoData>
  Public Property LastLogProgram As Singular.Service.ServerProgramType
  Public Property ServerProgressList As Singular.Service.Scheduling.ROScheduleProgressList

  Public Shared LogToDateProperty As Csla.PropertyInfo(Of Date) = RegisterSProperty(Of Date)(Function(c) c.LogToDate).AddSetExpression("GetScheduleProgress()")
  <System.ComponentModel.DataAnnotations.Display(Name:="Log Date")>
  Public Property LogToDate As Date = Now.Date

  Protected Overrides Sub Setup()
    MyBase.Setup()

    ServerProgramTypeList = Singular.Service.ServerProgramTypeList.GetServerProgramTypeList()
    ServerProgramType = Nothing

    ServerProgramTypeList.ToList.ForEach(Sub(c)
                                           If c.Info Is Nothing Then
                                             c.Info = New Singular.Service.Scheduling.Schedule
                                           End If
                                         End Sub)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With ServerProgramTypeList.TrySave()
          If .Success Then
            ServerProgramTypeList = .SavedObject
            AddMessage(MessageType.Success, "Save", "Save Successful")
          Else
            AddMessage(MessageType.Error, "Save", "Error Saving: " & .ErrorText)
          End If
        End With

      Case "Export"
        Dim ee As New Singular.Data.ExcelExporter
        ee.PopulateData(Singular.Service.Scheduling.ROScheduleProgressList.GetROScheduleProgressList(CommandArgs.ClientArgs, LogToDate), True, True)
        SendFile("ServiceLog.xlsx", ee.GetStream().ToArray)
    End Select
  End Sub

  Public Function GetProgress(ScheduleID As Integer, ToDate As Date) As Singular.Web.Result

    LogToDate = ToDate.Date

    Return New Singular.Web.Result(
      Function()
        Return Singular.Service.Scheduling.ROScheduleProgressList.GetROScheduleProgressList(ScheduleID, LogToDate)
      End Function)

  End Function

End Class