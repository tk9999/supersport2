﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Services.aspx.vb" Inherits="NewOBWeb.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <script type="text/javascript">

    function EditSchedule(schedule) {
      ViewModel.ServerProgramType(schedule)
      $("#EditSchedule").dialog();
    };

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    
    Using h = Helpers
      h.Control(New Singular.Web.ServiceHelpers.BootstrapServiceSetup)
    End Using
    
  %>
</asp:Content>
