﻿Imports OBLib.Maintenance.ShiftPatterns
Imports OBLib.Maintenance.General.ReadOnly
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports Singular
Imports OBLib.Maintenance.SystemManagement
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports Csla
Imports Singular.DataAnnotations

Public Class ProductionSpecRequirements
  Inherits OBPageBase(Of ProductionSpecRequirementsVM)

End Class

Public Class ProductionSpecRequirementsVM
  Inherits OBViewModelStateless(Of ProductionSpecRequirementsVM)

#Region " Properties "

  Public Property ROProductionSpecRequirementPagedList As OBLib.Maintenance.SystemManagement.ReadOnly.ROProductionSpecRequirementPagedList
  Public Property ROProductionSpecRequirementPagedListCriteria As OBLib.Maintenance.SystemManagement.ReadOnly.ROProductionSpecRequirementPagedList.Criteria
  Public Property ROProductionSpecRequirementPagedListManager As Singular.Web.Data.PagedDataManager(Of ProductionSpecRequirementsVM)

  Public Property ProductionSpecRequirement As ProductionSpecRequirement = Nothing

  '<ClientOnly()>
  'Public Property CurrentROProductionSpec As ROProductionSpecRequirementPaged = Nothing

  'Public Property TempProductionSpecRequirementList As ProductionSpecRequirementList

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()

    ValidationMode = Singular.Web.ValidationMode.OnLoad

    ROProductionSpecRequirementPagedList = New OBLib.Maintenance.SystemManagement.ReadOnly.ROProductionSpecRequirementPagedList
    ROProductionSpecRequirementPagedListCriteria = New OBLib.Maintenance.SystemManagement.ReadOnly.ROProductionSpecRequirementPagedList.Criteria
    ROProductionSpecRequirementPagedListManager = New Singular.Web.Data.PagedDataManager(Of ProductionSpecRequirementsVM)(Function(d) Me.ROProductionSpecRequirementPagedList,
                                                                                                                             Function(d) Me.ROProductionSpecRequirementPagedListCriteria,
                                                                                                                             "ProductionSpecRequirementName", 25, True)
    ROProductionSpecRequirementPagedListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROProductionSpecRequirementPagedListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    'ROProductionSpecRequirementPagedListCriteria.SortColumn = "ProductionSpecRequirementName"
    'ROProductionSpecRequirementPagedListCriteria.UnlinkedOnly = True
    'ROProductionSpecRequirementPagedListCriteria.PageNo = 1
    'ROProductionSpecRequirementPagedListCriteria.PageSize = 10

    'TempProductionSpecRequirementList = New ProductionSpecRequirementList
    'ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    'ClientDataProvider.AddDataSource("ROProductionSpecRequirementPagedListCriteria", OBLib.CommonData.Lists.ROProductionSpecRequirementList, False)
    'ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    'ClientDataProvider.AddDataSource("ROEventTypeList", OBLib.CommonData.Lists.ROEventTypeList, False)
    ClientDataProvider.AddDataSource("ROEquipmentTypeList", OBLib.CommonData.Lists.ROEquipmentTypeList, False)
    ClientDataProvider.AddDataSource("ROEquipmentSubTypeList", OBLib.CommonData.Lists.ROEquipmentSubTypeList, False)
    'ClientDataProvider.AddDataSource("ROPositionList", OBLib.CommonData.Lists.ROPositionList, False)
    'ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
    'ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    ClientDataProvider.AddDataSource("RORoomList", OBLib.CommonData.Lists.RORoomList, False)

  End Sub

  'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
  '  MyBase.HandleCommand(Command, CommandArgs)

  '  Select Case Command

  '    Case "GetSpec"
  '      Dim ProductionSpecRequirementList As New ProductionSpecRequirementList
  '      ProductionSpecRequirementList = OBLib.Productions.Specs.ProductionSpecRequirementList.GetProductionSpecRequirementList(CommandArgs.ClientArgs.ProductionSpecRequirementID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID, True)
  '      CurrentSpec = ProductionSpecRequirementList(0)

  '    Case "Delete"
  '      Dim ProductionSpecRequirementList As New ProductionSpecRequirementList
  '      ProductionSpecRequirementList = New ProductionSpecRequirementList
  '      ProductionSpecRequirementList.Add(CurrentSpec)
  '      ProductionSpecRequirementList.Clear()
  '      Dim sh As SaveHelper = TrySave(ProductionSpecRequirementList)
  '      If Not sh.Success Then
  '        'Error
  '        AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
  '      Else
  '        'Success
  '        CurrentSpec = Nothing
  '        OBLib.CommonData.Lists.Refresh("ROProductionSpecRequirementList")
  '      End If

  '    Case "Save"
  '      Dim ProductionSpecRequirementList As New ProductionSpecRequirementList
  '      ProductionSpecRequirementList = New ProductionSpecRequirementList
  '      ProductionSpecRequirementList.Add(CurrentSpec)
  '      Dim sh As SaveHelper = TrySave(ProductionSpecRequirementList)
  '      If Not sh.Success Then
  '        'Error
  '        AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
  '      Else
  '        'Success
  '        ProductionSpecRequirementList = CType(sh.SavedObject, ProductionSpecRequirementList)
  '        CurrentSpec = ProductionSpecRequirementList(0)
  '        OBLib.CommonData.Lists.Refresh("ROProductionSpecRequirementList")
  '      End If

  '    Case "NewSpec"
  '      CurrentSpec = New ProductionSpecRequirement
  '      CurrentSpec.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
  '      CurrentSpec.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

  '  End Select

  'End Sub

#End Region

End Class