Imports OBLib.Maintenance
Imports OBLib.Maintenance.General

Public Class Maintenance
  Inherits Singular.Web.PageBase(Of MaintenanceVM)

End Class

Public Class MaintenanceVM
  Inherits Singular.Web.MaintenanceHelpers.MaintenanceVM

  Public Property CurrentSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

  Protected Overrides Sub Setup()
    MyBase.Setup()

    'TODO: Add Maintenance pages here.
    'Please don't leave any commented out text here!

    If Singular.Security.HasAccess("Invoicing", "Access") Or Singular.Security.HasAccess("Invoicing", "Can Access Payment Runs") Then
      With AddMainSection("Invoicing")
        With .AddMaintenancePage(Of OBLib.Maintenance.Company.AccountList)("Accounts")
          .AddRefreshType(Of OBLib.Maintenance.Invoicing.ReadOnly.ROAccountList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.Company.CostCentreList)("Cost Centres")
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROCostCentreList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.Creditors.CreditorList)("Creditors")
          .AddRefreshType(Of OBLib.Maintenance.Creditors.ReadOnly.ROCreditorList)()
        End With
        'With .AddMaintenancePage(Of OBLib.Maintenance.Invoicing.PaymentRunList)("Payment Runs")
        '  .AddRefreshType(Of OBLib.Maintenance.Invoicing.ReadOnly.ROPaymentRunList)()
        'End With
        With .AddMaintenancePage(Of OBLib.Maintenance.Invoicing.PaymentRunStatusList)("Payment Run Statuses")
          .AddRefreshType(Of OBLib.Maintenance.Invoicing.ReadOnly.ROPaymentRunStatusList)()
        End With

      End With
    End If

    If Singular.Security.HasAccess("Maintenance - Locations and Travel", "Access") Then
      With AddMainSection("Locations and Travel")
        If Singular.Security.HasAccess("Maintenance", "Can Access Currencies") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.CurrencyList)("Currencies")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROCurrencyList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Countries") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Locations.CountryList)("Countries")
            .AddRefreshType(Of OBLib.Maintenance.Locations.ReadOnly.ROCountryList)()
            .AddRefreshType(Of OBLib.Maintenance.Locations.ReadOnly.ROProvinceList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Cities") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Locations.CityList)("Cities")
            .AddRefreshType(Of OBLib.Maintenance.Locations.ReadOnly.ROCityList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Locations") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Locations.LocationList)("Locations")
            .AddRefreshType(Of OBLib.Maintenance.Locations.ReadOnly.ROLocationList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Airports") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.AirportList)("Airports")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROAirportList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Accommodation Providers") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.AccommodationProviderList)("Accommodation Providers")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROAccommodationProviderList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Rental Car Agents") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.RentalCarAgentList)("Rental Car Agents")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.RORentalCarAgentList)()
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.RORentalCarAgentBranchList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Travel Req Comment Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.CommentTypeList)("Comment Types")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROCommentTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Travel Req Comment Categories") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.CommentCategoryList)("Comment Categories")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROCommentCategoryList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Car Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.CarTypeList)("Car Types")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROCarTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Flight Classes") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.FlightClassList)("Flight Classes")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROFlightClassList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Travel Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.TravelTypeList)("Travel Types")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROTravelTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Travel Advance Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.TravelAdvanceTypeList)("Travel Advance Types")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROTravelAdvanceTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Group SnT Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Travel.GroupSnTTypeList)("Group S&T Types")
            .AddRefreshType(Of OBLib.Maintenance.Travel.ReadOnly.ROGroupSnTTypeList)()
          End With
        End If
      End With
    End If

    If Singular.Security.HasAccess("Maintenance - General", "Access") Then
      With AddMainSection("General")
        If Singular.Security.HasAccess("Maintenance", "Can Access Disciplines") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.DisciplineList)("Disciplines")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.RODisciplineList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Outsource Service Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.OutsourceServiceTypeList)("Outsource Service Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROOutsourceServiceTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Supplier") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.SupplierList)("Suppliers")
            .AddRefreshType(Of OBLib.Maintenance.ReadOnly.ROSupplierList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Public Holidays") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.PublicHolidayList)("Public Holidays")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROPublicHolidayList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Document Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.DocumentTypeList)("Document Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.RODocumentTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Finance Document Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.FinanceDocumentTypeList)("Finance Document Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROFinanceDocumentTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access ISP Cancellation Rules") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.ISPCancellationRuleList)("ISP Cancellation Rules")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROISPCancellationRuleList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Pay Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.PayTypeList)("Pay Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROPayTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Facility Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.FacilityTypeList)("Facility Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROFacilityTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Facilities") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.FacilityList)("Facilities")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROFacilityList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access ICR Contact Detail") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.ICRContactDetailList)("ICR Contact Details")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROICRContactDetailList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Creation Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.CreationTypeList)("Creation Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROCreationTypeList)()
          End With
        End If
      End With
    End If

    If Singular.Security.HasAccess("Maintenance - Productions", "Access") Then
      With AddMainSection("Productions")
        If Singular.Security.HasAccess("Maintenance", "Can Access Crew Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.CrewTypeList)("Crew Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROCrewTypeList)()
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROCrewTypeDisciplineList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Production Timeline Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.ProductionTimelineTypeList)("Timeline Types")
            .AddRefreshType(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineTypeList)()
            .AddRefreshType(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineTypeDisciplineList)()
          End With
        End If
        If OBLib.Security.Settings.CurrentUser.UserID = 1 Or OBLib.Security.Settings.CurrentUser.UserTypeID = 1 Then
          If Singular.Security.HasAccess("Maintenance", "Can Access Allowed Timeline Types") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Areas.ProductionAreaAllowedTimelineTypeList)("Allowed Timeline Types")
              .AddRefreshType(Of OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedTimelineTypeList)()
            End With
          End If
          If Singular.Security.HasAccess("Maintenance", "Can Access Allowed Disciplines") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Areas.ProductionAreaAllowedDisciplineList)("Allowed Disciplines")
              .AddRefreshType(Of OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedDisciplineList)()
            End With
          End If
          If Singular.Security.HasAccess("Maintenance", "Can Access Allowed Statuses") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Areas.ProductionAreaAllowedStatusList)("Allowed Statuses")
              .AddRefreshType(Of OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedStatusList)()
            End With
          End If
          If Singular.Security.HasAccess("Maintenance", "Can Access Production Area Statuses") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Areas.ProductionAreaStatusList)("Area Statuses")
              .AddRefreshType(Of OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaStatusList)()
            End With
          End If
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Production Type Crew Requirements'") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Specs.ProductionTypeCrewRequirementList)("Production Type Crew Requirements")
            .AddRefreshType(Of OBLib.Maintenance.Productions.Specs.ReadOnly.ROProductionTypeCrewRequirementList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Production Type Basher Requirements") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Specs.ProductionTypeBasherRequirementList)("Production Type Basher Requirements")
            .AddRefreshType(Of OBLib.Maintenance.Productions.Specs.ReadOnly.ROProductionTypeBasherRequirementList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Planning Levels") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.PlanningLevelHourList)("Planning Levels")
            .AddRefreshType(Of OBLib.Maintenance.Productions.ReadOnly.ROPlanningLevelHourList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Planning Level Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.PlanningLevelTypeList)("Planning Level Types")
            .AddRefreshType(Of OBLib.Maintenance.Productions.ReadOnly.ROPlanningLevelTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Petty Cash Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.Correspondence.ProductionPettyCashTypeList)("Petty Cash")
            .AddRefreshType(Of OBLib.Maintenance.Productions.Correspondence.ReadOnly.ROProductionPettyCashTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Position Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.General.PositionTypeList)("Position Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROPositionTypeList)()
          End With
        End If
        If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
          If Singular.Security.HasAccess("Maintenance", "Can Access Audio Configurations") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.AudioConfigurationList)("Audio Configurations")
              .AddRefreshType(Of OBLib.Productions.ReadOnly.ROAudioConfigurationList)()
            End With
          End If
        End If

        If Singular.Security.HasAccess("Maintenance", "Can Access Vehicle Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Productions.VehicleTypeList)("Vehicle Types")
            .AddRefreshType(Of OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleTypeList)()
          End With
        End If

        If Singular.Security.HasAccess("Maintenance", "Can Access Equipment Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.Equipment.EquipmentTypeList)("Equipment Types")
            .AddRefreshType(Of OBLib.Maintenance.General.ReadOnly.ROEquipmentTypeList)()
          End With
        End If

      End With


    End If

    If Singular.Security.HasAccess("Maintenance - HR", "Access") Then
      With AddMainSection("Human Resources")
        If Singular.Security.HasAccess("Maintenance", "Can Access Contract Type") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.HR.ContractTypeList)("Contract Types")
            .AddRefreshType(Of OBLib.Maintenance.HR.ReadOnly.ROContractTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Off Reasons") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.HR.OffReasonList)("Off Reasons")
            .AddRefreshType(Of OBLib.Maintenance.HR.ReadOnly.ROOffReasonList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access HR Document Types") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.HR.HRDocumentTypeList)("HR Document Types")
            .AddRefreshType(Of OBLib.Maintenance.HR.ReadOnly.ROHRDocumentTypeList)()
          End With
        End If
        If Singular.Security.HasAccess("Maintenance", "Can Access Position Levels") Then
          With .AddMaintenancePage(Of OBLib.Maintenance.HR.PositionLevelList)("Position Levels")
            .AddRefreshType(Of OBLib.Maintenance.HR.ReadOnly.ROPositionLevelList)()
          End With
        End If
      End With
    End If

    If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices _
      AndAlso OBLib.Security.Settings.CurrentUser.ProductionAreaID = OBLib.CommonData.Enums.ProductionArea.OB Then
      If Singular.Security.HasAccess("Maintenance - Timesheets", "Access") Then
        With AddMainSection("Timesheets")
          If Singular.Security.HasAccess("Maintenance", "Can Access Timesheet Month") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Timesheets.TimesheetMonthList)("Timesheet Month")
              .AddRefreshType(Of OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetMonthList)()
            End With
          End If
          If Singular.Security.HasAccess("Maintenance", "Can Access Timesheet Categories") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Timesheets.TimesheetCategoryList)("Timesheet Categories")
              .AddRefreshType(Of OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetCategoryList)()
            End With
          End If
          If Singular.Security.HasAccess("Maintenance", "Can Access Timesheet Hour Types") Then
            With .AddMaintenancePage(Of OBLib.Maintenance.Timesheets.TimesheetHourTypeList)("Timesheet Hour Types")
              .AddRefreshType(Of OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetHourTypeList)()
            End With
          End If
        End With
      End If
    End If

    If OBLib.Security.Settings.CurrentUser.UserID = 1 Or OBLib.Security.Settings.CurrentUser.UserTypeID = 1 Or OBLib.Security.Settings.CurrentUser.UserTypeID = OBLib.CommonData.Enums.UserType.AdministratorKerry Then
      With AddMainSection("Company")
        With .AddMaintenancePage(Of OBLib.Maintenance.Company.CompanyList)("Companies")
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROCompanyList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.RODepartmentList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROSystemList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROSystemEmailList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROSystemEmailRecipientList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROClashCCEmailList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROClashCCEmailUserList)()
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROCostCentreList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.Company.SystemEmailTypeList)("Email Types")
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROSystemEmailTypeList)()
        End With
      End With
      With AddMainSection("General")
        With .AddMaintenancePage(Of OBLib.Maintenance.AdHoc.AdHocBookingTypeList)("Ad Hoc Booking Types")
          .AddRefreshType(Of OBLib.Maintenance.AdHoc.ReadOnly.ROAdHocBookingTypeList)()
        End With
      End With
    Else
    End If


    If Singular.Security.HasAccess("Maintenance", "Can Access MR Timesheet Categories") Then
      With AddMainSection("MR Timesheet ")
        With .AddMaintenancePage(Of OBLib.NSWTimesheets.NSWTimesheetCategoryList)("MR Timesheet Category")
          .AddRefreshType(Of OBLib.NSWTimesheets.NSWTimesheetCategoryList)()
        End With
      End With
    End If

    If Singular.Security.HasAccess("Maintenance", "Can Access ICR Months") Then
      With AddMainSection("ICR")
        With .AddMaintenancePage(Of OBLib.Maintenance.ICR.ICRMonthList)("ICR Months")
          .AddRefreshType(Of OBLib.Maintenance.ICR.ReadOnly.ROICRMonthList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.Company.CompanyMonthList)("Company Months")
          .AddRefreshType(Of OBLib.Maintenance.Company.ReadOnly.ROCompanyMonthList)()
        End With
      End With
    End If

    'If Singular.Security.HasAccess("Maintenance", "Can Access System Area Discipline Shift Rates") Then
    '  ClientDataProvider.AddDataSource("ROSystemAllowedAreaList", OBLib.CommonData.Lists.ROSystemAllowedAreaList, False)
    '  With AddMainSection("Rates")
    '    With .AddMaintenancePage(Of OBLib.Maintenance.Company.SystemAreaDisciplineShiftRateList)("Shift Rates")
    '      .AddRefreshType(Of OBLib.Maintenance.Company.SystemAreaDisciplineShiftRateList)()
    '    End With
    '  End With
    'End If

    If Singular.Security.HasAccess("Maintenance", "Can Access Rooms") Then
      With AddMainSection("Rooms")
        With .AddMaintenancePage(Of OBLib.Maintenance.Rooms.RoomList)("Rooms")
          .AddRefreshType(Of OBLib.Maintenance.Rooms.ReadOnly.RORoomList)()
          .AddRefreshType(Of OBLib.Maintenance.Rooms.ReadOnly.RORoomTimelineSettingList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.Rooms.RoomTypeList)("Room Types")
          .AddRefreshType(Of OBLib.Maintenance.Rooms.ReadOnly.RORoomTypeList)()
        End With
      End With
    End If

    If Singular.Settings.CurrentUserID = 152 Or Singular.Settings.CurrentUserID = 1 Then
      With AddMainSection("Quoting")
        With .AddMaintenancePage(Of OBLib.Quoting.CostTypeList)("Cost Types")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.ROCostTypeList)()
        End With
        With .AddMaintenancePage(Of OBLib.Quoting.DebtorList)("Debtors")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.RODebtorList)()
        End With
        With .AddMaintenancePage(Of OBLib.Quoting.RateTypeList)("Rate Types")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.RORateTypeList)()
        End With
        With .AddMaintenancePage(Of OBLib.Quoting.TravelRateList)("Travel Rates")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.ROTravelRateList)()
        End With
        With .AddMaintenancePage(Of OBLib.Quoting.CompanyRateCardList)("Company Rate Cards")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.ROCompanyRateCardList)()
        End With
        With .AddMaintenancePage(Of OBLib.Quoting.SupplierRateCardList)("Supplier Rate Cards")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.ROSupplierRateCardList)()
        End With
        With .AddMaintenancePage(Of OBLib.Quoting.QuoteList)("Quotes")
          .AddRefreshType(Of OBLib.Quoting.ReadOnly.ROQuoteList)()
        End With
      End With
    End If

    If Singular.Security.HasAccess("Maintenance - SatOps", "Access") Then
      With AddMainSection("SatOps")
        With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.FeedTypeList)("Feed Types")
          .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROFeedTypeList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.TurnAroundPointList)("TurnAround Points")
          .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPointList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.AudioSettingList)("Audio Settings")
          .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROAudioSettingList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.VideoSettingList)("Video Settings")
          .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROVideoSettingList)()
        End With
        'With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.AudioConfigurationTemplateList)("Audio Setting Templates")
        '  .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROAudioConfigurationTemplateList)()
        'End With
        'With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.VideoConfigurationTemplateList)("Video Setting Templates")
        '  .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROVideoConfigurationTemplateList)()
        'End With
        With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.EquipmentScheduleTypeList)("Equipment Booking Types")
          .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROEquipmentScheduleTypeList)()
        End With
        With .AddMaintenancePage(Of OBLib.Maintenance.SatOps.EquipmentTypeAllowedScheduleTypeList)("Allowed Bookings Types for Equipment Types")
          .AddRefreshType(Of OBLib.Maintenance.SatOps.ReadOnly.ROEquipmentTypeAllowedScheduleTypeList)()
        End With
      End With
    End If

    'If Singular.Security.HasAccess("Maintenance - Skills Database", "Access") Then
    '  With AddMainSection("Skills Database")
    '    With .AddMaintenancePage(Of OBLib.HR.SkillsDatabase.SkillsDatabaseHeadingList)("Headings")
    '      .AddRefreshType(Of OBLib.HR.SkillsDatabase.ReadOnly.ROSkillsDatabaseHeadingList)()
    '    End With
    '  End With
    'End If

    If Singular.Security.HasAccess("Maintenance - Notifications", "Access") Then
      With AddMainSection("Sms")
        With .AddMaintenancePage(Of OBLib.Notifications.SmS.SmsTemplateTypeList)("Sms Templates")
          .AddRefreshType(Of OBLib.Notifications.SmS.ReadOnly.ROSmsTemplateTypeList)()
        End With
      End With
      'With AddMainSection("Notification Groups")
      '  With .AddMaintenancePage(Of OBLib.Notifications.NotificationGroupList)("Notification Groups")
      '    .AddRefreshType(Of OBLib.Notifications.ReadOnly.RONotificationGroupList)()
      '  End With
      'End With
    End If

    If Singular.Security.CurrentIdentity.UserID = 1 Then
      With AddMainSection("Developer")
        With .AddMaintenancePage(Of OBLib.Maintenance.MiscList)("Misc")
        End With
      End With
    End If

    If Singular.Security.CurrentIdentity.UserID = 1 Then
      With AddMainSection("Developer")
        With .AddMaintenancePage(Of OBLib.Maintenance.MiscList)("Misc")
        End With
      End With
    End If

    'If Singular.Security.HasAccess("Access Log", "Access") Then
    '  With AddMainSection("Access Logs")
    '    .AddMaintenancePage(Of OBLib.AccessLogs.AccessTerminalGroupList)("Access Terminal Groups")
    '  End With
    'End If

  End Sub

End Class

