﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ProductionVenues.aspx.vb" Inherits="NewOBWeb.ProductionVenues" %>

<%@ Import Namespace="OBLib.Maintenance.Productions.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.Productions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      ''Errors----------------------------------------------------------------------------------------------------------------------
      'With h.Bootstrap.Row
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Toolbar
      '      .Helpers.MessageHolder()
      '    End With
      '  End With
      'End With
      
      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Manage Production Venues", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Button("Back", "Back", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", , Singular.Web.PostBackType.Ajax, )
                End With
                With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindProductionVenue()")
                End With
                With .Helpers.Bootstrap.Button("AddNew", "New", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Toolbar
                  .Helpers.MessageHolder()
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----Current Production Venue------------------------------------------------------------------------------------------------
          With h.Bootstrap.Row
              With h.With(Of ProductionVenue)(Function(d) ViewModel.CurrentProductionVenue)
                  With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                      With .Helpers.Bootstrap.FlatBlock(, True)
                          .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) ViewModel.CurrentProductionVenue.ProductionVenue)
                          With .AboveContentTag
                              With .Helpers.Bootstrap.Row
                                  With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                                  End With
                                  With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                                  End With
                              End With
                          End With
                          With .ContentTag
                              With .Helpers.Bootstrap.Row
                                  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                      With .Helpers.With(Of ProductionVenue)(Function(d) ViewModel.CurrentProductionVenue)
                                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                              .Helpers.LabelFor(Function(d) d.ProductionVenue)
                                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionVenue, Singular.Web.BootstrapEnums.InputSize.Small, )
                      
                                              End With
                                          End With
                                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                              .Helpers.LabelFor(Function(d) d.CountryID)
                                              With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.CountryID, Singular.Web.BootstrapEnums.InputSize.Small, )
                      
                                              End With
                                          End With
                                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                              .Helpers.LabelFor(Function(d) d.CityID)
                                              With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.CityID, Singular.Web.BootstrapEnums.InputSize.Small, )
                      
                                              End With
                                          End With
                                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                              With .Helpers.DivC("gm-style")
                                                  .Attributes("id") = "ProductionVenueMap"
                                              End With
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                      End With
                  End With
                  'End With
              
                  With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                      With .Helpers.Bootstrap.TabControl("", "nav-pills", "tablist")
                          .TabHeaderContainer.Attributes("ID") = "PageTabControl"
                          With .AddTab("ProductionTypes", "", , "Production Types", )
                              With .TabPane
                                  With .Helpers.Bootstrap.TableFor(Of ProductionVenueProductionType)(Function(d) ViewModel.CurrentProductionVenue.ProductionVenueProductionTypeList, True, True, False, False, True, True, False)
                                      'With .Helpers.Bootstrap.FlatBlock("Productions")
                                      '    With .ContentTag
                             
                                      'With .Helpers.Bootstrap.TabControl(, "nav-justified nav-pills", )
                                      'With .AddTab("ProductionTypes", "", , "Production Types", )
                                      'With .TabPane
                                      'With .Helpers.Bootstrap.TableFor(Of ProductionVenueProductionType)(Function(d) ViewModel.CurrentProductionVenue.ProductionVenueProductionTypeList, True, True, False, False, True, True, False)
                                      '.AddClass("no-border hover list")
                                      '.TableBodyClass = "no-border-y"
                                      With .FirstRow
                                          .AddClass("items")
                                          With .AddColumn(Function(d As ProductionVenueProductionType) d.ProductionTypeID)
                                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                          With .AddTab("Outsource Service Types", "", , "Outsource Service Types", )
                              With .TabPane
                                  With .Helpers.Bootstrap.TableFor(Of ProductionVenueOutsourceServiceType)(Function(d) ViewModel.CurrentProductionVenue.ProductionVenueOutsourceServiceTypeList, True, True, False, False, True, True, False)
                                      .AddClass("no-border hover list")
                                      .TableBodyClass = "no-border-y"
                                      With .FirstRow
                                          .AddClass("items")
                                          With .AddColumn(Function(d As ProductionVenueOutsourceServiceType) d.OutsourceServiceTypeID)
                                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                              .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                                          End With
                                          With .AddColumn(Function(d As ProductionVenueOutsourceServiceType) d.ProductionTypeID)
                                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                              .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                                          End With
                                      End With
                                      With .AddChildTable(Of ProductionVenueOutsourceServiceTypeDetail)(Function(d) d.ProductionVenueOutsourceServiceTypeDetailList, True, True, False, False, True, True, False)
                                          .AddClass("no-border hover list")
                                          .TableBodyClass = "no-border-y"
                                          With .FirstRow
                                              .AddClass("items")
                                              With .AddColumn(Function(d As ProductionVenueOutsourceServiceTypeDetail) d.ProductionVenueOutsourceServiceTypeDetail)
                                                  .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                                  .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                                              End With
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                          With .AddTab("ProductionVenueContact", "", , "Venue Contacts", )
                              With .TabPane
                                  With .Helpers.Bootstrap.TableFor(Of ProductionVenueContact)(Function(d) ViewModel.CurrentProductionVenue.ProductionVenueContactList, True, True, False, False, True, True, False)
                                      .AddClass("no-border hover list")
                                      .TableBodyClass = "no-border-y"
                                      With .FirstRow
                                          .AddClass("items")
                                          With .AddColumn(Function(d As ProductionVenueContact) d.ContactName)
                                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                              .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                                          End With
                                          With .AddColumn(Function(d As ProductionVenueContact) d.ContactNo)
                                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                              .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                                          End With
                                      End With
                                  End With
                              End With
                          End With
                          'With .AddTab("ProductionVenueAreas", "", , "Venue Areas", )
                          '    With .TabPane
                          '        With .Helpers.Bootstrap.TableFor(Of ProductionVenueArea)(Function(d) ViewModel.CurrentProductionVenue.ProductionVenueAreaList, True, True, False, False, True, True, False)
                          '            .AddClass("no-border hover list")
                          '            .TableBodyClass = "no-border-y"
                          '            With .FirstRow
                          '                .AddClass("items")
                          '                With .AddColumn(Function(d As ProductionVenueArea) d.ProductionVenueArea)
                          '                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                          '                    .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                          '                End With
                          '            End With
                          '        End With
                          '    End With
                          'End With
                      End With
                  End With
              End With
          End With
          'End With
          'End With
          'End With
          'End With
      
          With h.Bootstrap.Dialog("FindProductionVenue", "Find Production Venue")
              .ModalDialogDiv.AddClass("modal-sml")
              With .Body
                  .AddClass("modal-background-gray")
                  With .Helpers.Bootstrap.FlatBlock("Production Venues", True)
                      .FlatBlockTag.AddClass("flat-block-paged")
                      With .AboveContentTag
                          With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                      With .Helpers.EditorFor(Function(d) ViewModel.ROProductionVenuePagedListCriteria.ProductionVenue)
                                          .AddClass("form-control input-sm")
                                          .Attributes("placeholder") = "Search by Production Venue"
                                          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RefreshROProductionVenuePagedList() }")
                                      End With
                                  End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                      With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", , Singular.Web.PostBackType.None, "RefreshROProductionVenuePagedList()")
                                          .Button.AddClass("btn-block")
                                      End With
                                  End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                  
                              End With
                          End With
                      End With
                      With .ContentTag
                          With .Helpers.DivC("table-responsive")
                              With .Helpers.Bootstrap.PagedGridFor(Of ROProductionVenuePaged)(Function(vm) ViewModel.ROProductionVenuePagedListPagingManager,
                                                                                               Function(vm) ViewModel.ROProductionVenuePagedList,
                                                                                               False, False, False, False, True, True, False,
                                                                                               "ProductionVenuesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                                  .AddClass("no-border hover list")
                                  .TableBodyClass = "no-border-y"
                                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                                  With .FirstRow
                                      .AddClass("items")
                                      With .AddColumn("")
                                          .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                                          With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditProductionVenue($data)")
                                          End With
                                      End With
                                      With .AddReadOnlyColumn(Function(d As ROProductionVenuePaged) d.ProductionVenue)
                                          .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                      End With
                                  End With
                                  With .FooterRow
                    
                                  End With
                              End With
                          End With
                      End With
                  End With
              End With
          End With
      
  
      
      End Using%>
  <script type="text/javascript">

    $(function () {
      Singular.OnPageLoad(function () {
        //ViewModel.ROProductionVenuePagedListPagingManager().Refresh();
      });
    });

    function FindProductionVenue() {
      RefreshROProductionVenuePagedList();
      $("#FindProductionVenue").modal();
    };

    function EditProductionVenue(ProductionVenue) {
        Singular.SendCommand("EditProductionVenue",
                             {
                                 ProductionVenueID: ProductionVenue.ProductionVenueID()
                             },
                             function (response) {
                                 $("#FindProductionVenue").modal('hide');
                             });
    };


    function RefreshROProductionVenuePagedList() {
      ViewModel.ROProductionVenuePagedListPagingManager().Refresh();
    };

    //function DeleteProductionVenue(ProductionVenue) {
    //  ViewModel.CurrentProductionVenue(ProductionVenue);
    //  Singular.ShowMessageQuestion("Delete Production Venue", "Are you sure you wish to delete " + ProductionVenue.ProductionVenue() + "?", function () {
    //    Singular.SendCommand("DeleteProductionVenue", {}, function (response) { AfterDelete(response) });
    //  });
    //};

    //function AfterDelete(response) {
    //  Refresh();
    //};

    //function AddNewProductionVenue() {
    //  //var npo = ViewModel.ProductionTypeList.AddNew();
    //  //npo.ProductionType("");
    //};

    //function FilterProductionVenueList() {
    //  ViewModel.ProductionVenueListPagingManager().Refresh();
    //};

    //function CustomFormatter() {
    //  var f = new KOFormatterObject();
    //  f.IncludeChildren = true;
    //  f.IncludeClean = false;
    //  f.IncludeCleanInArray = false;
    //  f.IncludeCleanProperties = true;
    //  f.IncludeIsNew = true;
    //  return f;
    //}

    //function Refresh() {
    //  ViewModel.ProductionVenueListPagingManager().Refresh();
    //};

    //function SaveChanges() {
    //  var List = CustomFormatter().Serialise(ViewModel.ProductionVenueList());
    //  ViewModel.ProductionVenueListPagingManager().IsLoading(true);
    //  ViewModel.CallServerMethod("SaveList", { ProductionVenueList: List }, function (response) {
    //    console.log(response);
    //    if (response.Success) {
    //      Refresh();
    //    } else {
    //      ViewModel.ProductionVenueListPagingManager().IsLoading(false);
    //    }
    //  });
    //};

  </script>
</asp:Content>
