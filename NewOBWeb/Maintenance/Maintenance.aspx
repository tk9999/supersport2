<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Maintenance.aspx.vb" Inherits="NewOBWeb.Maintenance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
<style type="text/css">

.MainSection
{
  float: left;
  width: 250px;
  background-color: #f5f5f5;
  border-radius: 3px;
  margin: 5px;
}
.MainSection .Heading
{
  background-color: #3A4F63;
  border-radius: 3px 3px 0 0;
  padding: 5px;
  font-size:16px;
  font-variant:small-caps;
  color: #fff;
}
.MainSection a
{
  text-decoration:none; 
  font-size: 9.5pt;
  color: #4B6375;
}
.MainSection a:hover
{
  color: #BB1F1F;
}
.MainSection li
{
  font-size:11pt;
}

</style>
<script type="text/javascript">
  function FilterSystemAllowedAreas() {
    var Allowed = [];
    ClientData.ROSystemAllowedAreaList.Iterate(function (Item, Index) {
      if (Item.SystemID == ViewModel.CurrentSystemID()) {
        Allowed.push({ ProductionAreaID: Item.ProductionAreaID, ProductionArea: Item.ProductionArea });
      }
    });
    return Allowed;
  };

  function GetAllowedDisciplines(List, Item) {
    var AllowedDisciplines = [];
    List.Iterate(function (Discipline, Index) {
      if (Discipline.SystemID == Item.SystemID() && Discipline.ProductionAreaID == Item.ProductionAreaID()) {
        AllowedDisciplines.push(Discipline)
      }
    })
    return AllowedDisciplines;
  };
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/General/Disciplines.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/General/Position.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/ICR/ICRMonth.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/Locations.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/ProductionSpecRequirements.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/Travel/RentalCarAgentBranch.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/Travel/RentalCarAgent.js"></script>
<script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/Company.js"></script>
<% Using h = Helpers
    
    h.Control(New Singular.Web.MaintenanceHelpers.MaintenanceStateControl)
    
  End Using%>

</asp:Content>
