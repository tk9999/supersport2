﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TestSmsSender.aspx.vb" Inherits="NewOBWeb.TestSmsSender" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


	<%	
		Using h = Helpers
			
			With h.Toolbar
				.Helpers.HTML.Heading2("Sms Test Page")
				.Helpers.Button(Singular.Web.DefinedButtonType.Save)
				.Helpers.Button(Singular.Web.DefinedButtonType.Undo)
			End With
			
			h.MessageHolder
			
			With h.Div
				
				With .Helpers.FieldSet("Sms")
					.Helpers.EditorRowFor(Function(c) c.Sms.Message)
					With .Helpers.FieldSet("Recipients")
						With .Helpers.TableFor(Of Singular.SmsSending.SmsRecipient)(Function(c) c.Sms.SmsRecipientList, True, True)
							.FirstRow.AddColumn(Function(c) c.CellNo)
						End With
					End With
				End With
				
			End With
			
		End Using
		%>

</asp:Content>
