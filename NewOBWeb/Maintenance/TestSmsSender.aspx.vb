﻿Public Class TestSmsSender
	Inherits OBPageBase(Of TestSmsSenderVM)

End Class

Public Class TestSmsSenderVM
	Inherits OBViewModel(Of TestSmsSenderVM)

	Public Shared SmsProperty As Csla.PropertyInfo(Of Singular.SmsSending.Sms) = RegisterProperty(Of Singular.SmsSending.Sms)(Function(c) c.Sms)

	Public Property Sms() As Singular.SmsSending.Sms
		Get
			Return GetProperty(SmsProperty)
		End Get
		Set(value As Singular.SmsSending.Sms)
			SetProperty(SmsProperty, value)
		End Set
	End Property

	Protected Overrides Sub Setup()
		MyBase.Setup()

		Sms = New Singular.SmsSending.Sms()

	End Sub

	Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
		MyBase.HandleCommand(Command, CommandArgs)

		Select Case Command
			Case "Undo"
				Sms = New Singular.SmsSending.Sms()
			Case "Save"
				With Sms.TrySave(GetType(Singular.SmsSending.SmsList))
					If .Success Then
						Sms = New Singular.SmsSending.Sms
						AddMessage(Singular.Web.MessageType.Success, "Sms", "Sms Sent")
					Else
						AddMessage(Singular.Web.MessageType.Error, "Sms", "Sms Not Sent. " & .ErrorText)
					End If
				End With
		End Select

	End Sub

End Class