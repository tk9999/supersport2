﻿Imports Microsoft.AspNet.SignalR
Imports Microsoft.Owin.Cors
Imports Owin
Namespace NewOBWeb
  Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
      ' Branch the pipeline here for requests that start with "/signalr" '
      app.Map("/signalr", Function(map)
                            map.UseCors(CorsOptions.AllowAll)
                            Dim hubConfiguration = New HubConfiguration()
                            map.RunSignalR(hubConfiguration)
                          End Function)
    End Sub
  End Class
End Namespace
' Setup the CORS middleware to run before SignalR.'
' By default this will allow all origins. You can '
' configure the set of origins and/or http verbs by'
' providing a cors options with a different policy.'

'From { _
' You can enable JSONP by uncommenting line below.'
' JSONP requests are insecure but some older browsers (and some'
' versions of IE) require JSONP to work cross domain'
' EnableJSONP = true'
'}
' Run the SignalR pipeline. We're not using MapSignalR'
' since this branch already runs under the "/signalr"'
' path.'