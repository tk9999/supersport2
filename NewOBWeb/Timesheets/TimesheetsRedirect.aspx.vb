﻿Imports Singular.Misc
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports OBLib.Invoicing.ReadOnly

Public Class TimesheetsRedirect
  Inherits OBPageBase(Of TimesheetsRedirectVM)

End Class


Public Class TimesheetsRedirectVM
  Inherits OBViewModel(Of TimesheetsRedirectVM)

  Protected Overrides Sub Setup()
    'MyBase.Setup()
    MyBase.PreSetup()
    'TODO: this should become a fetch from the database, not via common data
    If Not IsNullNothing(OBLib.Security.Settings.CurrentUser.HumanResourceID, True) Then
      Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(OBLib.Security.Settings.CurrentUser.HumanResourceID)
      If ROHR.ContractTypeID = CType(OBLib.CommonData.Enums.ContractType.Freeleancer, Integer) Then
        Dim PaymentRun As ROPaymentRun = OBLib.Helpers.TimesheetsHelper.GetCurrentPaymentRun(OBLib.Security.Settings.CurrentUser.SystemID)
        If PaymentRun IsNot Nothing Then
          HttpContext.Current.Response.Redirect("~/Timesheets/OBCity/FreelancerTimesheet.aspx?" & "P=" & PaymentRun.PaymentRunID.ToString)
        Else
          'TODO: show some error
        End If
      ElseIf ROHR.ContractTypeID = CType(OBLib.CommonData.Enums.ContractType.Permanent, Integer) _
             OrElse ROHR.ContractTypeID = CType(OBLib.CommonData.Enums.ContractType.Contract, Integer) Then
        Dim CurrentTimesheetMonth As ROTimesheetMonth = OBLib.Helpers.TimesheetsHelper.GetCurrentTimesheetMonth
        If CurrentTimesheetMonth IsNot Nothing Then
          HttpContext.Current.Response.Redirect("~/Timesheets/OBCity/CrewTimesheet.aspx?" & "T=" & CurrentTimesheetMonth.TimesheetMonthID.ToString)
        Else
          'TODO: show some error
        End If
      End If
    Else
      'TODO: show some error
    End If
  End Sub

  'Dim NSWInfo = OBLib.NSWTimesheets.RONSWInfoList.GetRONSWInfoList.GetItem(1)
  'If NSWInfo IsNot Nothing AndAlso NSWInfo.IsNSWInd Then
  '  HttpContext.Current.Response.Redirect("~/Timesheets/GeneralTimesheets.aspx")
  'Else
  '  'TODO: show some error
  'End If

End Class