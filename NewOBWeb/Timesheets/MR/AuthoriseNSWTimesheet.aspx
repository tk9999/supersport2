﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AuthoriseNSWTimesheet.aspx.vb"
  Inherits="NewOBWeb.AuthoriseNSWTimesheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
     td.LButtons, th.LButtons
    {
      width: 24px !important;
    }
    td.CellWait
    {
      background-image: url('../Singular/Images/LoadingSmall.gif');
      background-repeat: no-repeat;
      background-position: 3px 5px;
      color: #bbb;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      With h.Toolbar
        .Helpers.HTML.Heading2("Authorise Timesheet")
        

        With .Helpers.Div() 
          With .Helpers.Button(Singular.Web.DefinedButtonType.Save)
            .PostBackType = Singular.Web.PostBackType.Ajax
            .AddBinding(Singular.Web.KnockoutBindingString.enable, "ButtonsEnabled()")
          End With
          With h.MessageHolder("Info")
            
            .Style.Display = Singular.Web.Display.inlineblock
          End With
        End With
      End With
      h.MessageHolder().AddClass("HoverMsg")
       
  
      With h.FieldSet(" ")
        
        With .Helpers.Div()
          .Style.FloatLeft()
          .Style.Width = 900
          .Style.Display = Singular.Web.Display.inlineblock
           
          With .Helpers.Div() 'StartDate
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("Start Date: ")
              End With
            End With
             
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("90px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "StartDate()")
              End With
            End With
          End With
          
          With .Helpers.Div() 'EndDate 
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("End Date: ")
              End With
            End With
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("85px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "EndDate()")
              End With
            End With
          End With
        End With
       
 
 
      End With
      
      With h.Div()
        
        .Style.Display = Singular.Web.Display.inlineblock
        With .Helpers.HTMLTag("h6", "Filter : ")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.ROSubstituteManagerList.Count > 1)

          With h.EditorFor(Function(p) ViewModel.FilterManagerID)
            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.ROSubstituteManagerList.Count > 1)
            .Style.Width = 200
            .Style.MarginLeft("10px")
          End With
        End With
      End With
      
      With h.TableFor(Of OBLib.NSWTimesheets.NSWTimesheetsAuthorisation)(Function(c) c.NSWTimesheetsAuthorisationList, False, False)
        .DataSourceString = "GetFilteredTimesheets()"
         
        .FirstRow.AddReadOnlyColumn(Function(c) c.FirstName, 100)
        .FirstRow.AddReadOnlyColumn(Function(c) c.Surname, 100)
        .FirstRow.AddReadOnlyColumn(Function(c) c.EmployeeCodeIDNo, 100)
        .FirstRow.AddReadOnlyColumn(Function(c) c.NoEntries, 50)
        .FirstRow.AddReadOnlyColumn(Function(c) c.TotalTime, 50)
        With .FirstRow.AddReadOnlyColumn(Function(c) c.AuthorisedNoOfMeals, 50)
          .CellBindings.Add(Singular.Web.KnockoutBindingString.css, "IsLoading() ? 'CellWait' : ''")
          
        End With
        With .FirstRow.AddReadOnlyColumn(Function(c) c.AuthorisedReimbursementAmount, 70)
          .CellBindings.Add(Singular.Web.KnockoutBindingString.css, "IsLoading() ? 'CellWait' : ''")
        End With
        With .FirstRow.AddColumn(Function(c) c.TotalPending, 70)
          .Style.Style("text-align") = "right"
          With .Editor
            .Style.Style("background-color") = "#ed9c28"
            .Style.Style("font-weight") = "700"
          End With
        End With
        With .FirstRow.AddColumn(Function(c) c.TotalAuthorised, 70)
          .Style.Style("text-align") = "right"
          With .Editor
            .Style.Style("background-color") = "#47a447"
            .Style.Style("font-weight") = "700"
          End With
        End With
        With .FirstRow.AddColumn(Function(c) c.TotalRejected, 70)
          .Style.Style("text-align") = "right"
          With .Editor
            .Style.Style("background-color") = "#d2322d"
            .Style.Style("font-weight") = "700"
          End With
        End With
        
        
        With .AddChildTable(Of OBLib.NSWTimesheets.NSWTimesheetsAuthorisationEntry)(Function(c) c.NSWTimesheetsAuthorisationEntryList, False, False)
       
          With .FirstRow.AddReadOnlyColumn(Function(c) c.StartDateTime, 80)
            .Style.Style("text-align") = "left"
          End With
          With .FirstRow.AddColumn("Start Time")
            With .Helpers.TimeEditorFor(Function(c) c.StartDateTime)
              .Style.Width = 50
            End With
          End With
          With .FirstRow.AddColumn(Function(c) c.EndDateTime, 90)
            .Style.Style("text-align") = "Center"
          End With
          With .FirstRow.AddColumn("End Time")
            With .Helpers.TimeEditorFor(Function(c) c.EndDateTime)
              .Style.Width = 50
            End With
          End With
          .FirstRow.AddColumn(Function(c) c.Production, 200)
          .FirstRow.AddColumn(Function(c) c.NSWTimesheetCategory, 200)
          With .FirstRow.AddColumn(Function(c) c.Hours, 50)
            .Style.Style("text-align") = "right"
          End With
 
          With .FirstRow.AddColumn("Authorised Status /")
            .Style.Width = 230
            With CType(.Helpers.EditorFor(Function(c) c.AuthorisedID), Singular.Web.CustomControls.RadioButtonEditor(Of OBLib.NSWTimesheets.NSWTimesheetsAuthorisationEntry))
              .Horizontal = True
              .Style.Style("background-color") = "#d2322d"
             
              .Style.Width = 230
            End With
              
          End With
        
          With .AddRow
            .AddColumn(Function(c) c.Description).ColSpan = 7
            With .AddColumn(Function(c) c.RejectedReason, "Rejected Reason")
              .ColSpan = 3
              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) c.AuthorisedID = 2)
            End With
          End With

        End With
      End With
      
    End Using
    
  %>

  <script type="text/javascript">

    function Authorise(obj, id) {
      var newid = (obj.AuthorisedID() + 1) % 3

      obj.AuthorisedID(newid);
      obj.AuthorisedByUserID(ViewModel.CurrentUserID());
    }

  
    function ButtonsEnabled() {
      for (var i = 0; i < ViewModel.NSWTimesheetsAuthorisationList().length; i++) {
        var item = ViewModel.NSWTimesheetsAuthorisationList()[i];
        if (item.IsLoading()) return false;
      }
      return true;
    }

    function GetFilteredTimesheets() {
      var fList = [];
      var FilterID = ViewModel.FilterManagerID();

      if (FilterID == null) {
        return ViewModel.NSWTimesheetsAuthorisationList();
      }

      for (var i = 0; i < ViewModel.NSWTimesheetsAuthorisationList().length; i++) {
        var obj = ViewModel.NSWTimesheetsAuthorisationList()[i];
        if (obj.ManagerHumanResourceID() == FilterID) {
          fList(obj);

        }
      }
      return fList;
    }

    function CheckRejectedReason(Value, Rule, RuleArgs) {
      var AuthorisedID = RuleArgs.Object.AuthorisedID();
        if (!RuleArgs.IgnoreResults) { 
          if (RuleArgs.Object.AuthorisedID() ==2 && RuleArgs.Object.RejectedReason() == '') {
              RuleArgs.AddError("Rejected Reason is required");
              return;
            }
          } 
    }

    function PreAuthChanged(authEntry, args) {
      authEntry._OldAuthorisedID = args.OldValue;
    }
    function AuthChanged(authEntry) {
   
      var WasAuthorised = authEntry._OldAuthorisedID == 1,
          IsAuthorised = authEntry.AuthorisedID() == 1,
          ParentRecord = authEntry.GetParent();

      if (authEntry.AuthorisedID() != 2) {
        authEntry.RejectedReason("");
      } else {
        setTimeout(function () {
          var ctl = $(authEntry.RejectedReason.BoundElements[0]);
          ctl.focus();
          ctl.select();
        }, 0);
      }
      
      Singular.ClearMessages();
      if (WasAuthorised != IsAuthorised) {

        var List = [];
        ParentRecord.NSWTimesheetsAuthorisationEntryList().Iterate(function (item) {
          List.push({ NSWTimesheetEntryID: item.NSWTimesheetEntryID(), AuthorisedID: item.AuthorisedID() });
        });
        ParentRecord.IsLoading(true);
        ParentRecord.SyncID(ParentRecord.SyncID() + 1);
        
        ViewModel.CallServerMethod('GetReimbursementValues', { HRID: ParentRecord.HumanResourceID(), JSon: JSON.stringify(List), SyncID: ParentRecord.SyncID() }, function (result) {

          if (ParentRecord.SyncID() == result.Data.SyncID) {
            ParentRecord.AuthorisedNoOfMeals(result.Data.NoOfMeals);
            ParentRecord.AuthorisedReimbursementAmount(result.Data.MRAmount);
            ParentRecord.IsLoading(false); 
          }
        });

      }

    }

    function AuthorisedCss(obj) {
      switch (obj.AuthorisedID()) {
        case 1:
          return 'btn btn-s btn-success';
          break;
        case 2:
          return 'btn btn-s btn-danger';
          break;
        case 0:
          return 'btn btn-s btn-warning';
          break;
      }
    }

    function AuthorisedHTML(AuthorisedID) {
      switch (AuthorisedID) {
        case 1:
          return 'Authorised';
          break;
        case 2:
          return 'Rejected';
          break;
        case 0:
          return 'Pending';
          break;
      }
    }

  </script>
</asp:Content>
