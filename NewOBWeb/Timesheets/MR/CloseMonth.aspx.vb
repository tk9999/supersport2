Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.NSWTimesheets

Public Class CloseMonth
  Inherits OBPageBase(Of CloseMonthVM)


End Class

Public Class CloseMonthVM
  Inherits OBViewModel(Of CloseMonthVM)

  Private ReadOnly Property OpenMonth As MRMonth
    Get
      Return MRMonthList.GetOpenMonth
    End Get
  End Property

  Public Property MonthDetailsVisible As Boolean = True
  Public Property MonthCloseSuccessVisible As Boolean = False
  Public Property PrevMonthAutoClosedInd As Boolean = False


  Public ReadOnly Property CanCloseMonthInd As Boolean
    Get
      If OpenMonth IsNot Nothing Then
        Return OpenMonth.CanCloseMonthInd
      End If
      Return False
    End Get
  End Property

  Public ReadOnly Property CanReOpenMonthInd As Boolean
    Get

      Return MRMonthList.CanReOpenMonth

    End Get
  End Property

  Public ReadOnly Property OpenMonthToString As String
    Get

      Return CType(OpenMonth.MonthStartDate, Date).ToString("dd-MMM-yyyy") & " ~ " & CType(OpenMonth.MonthEndDate, Date).ToString("dd-MMM-yyyy")

    End Get
  End Property

  Public ReadOnly Property PrevMonthToString As String
    Get
      Dim mrm = MRMonthList.GetMonthBy(CType(OpenMonth.MonthStartDate, Date).AddMonths(-1))

      If mrm IsNot Nothing Then
        Return CType(mrm.MonthStartDate, Date).ToString("dd-MMM-yyyy") & " ~ " & CType(mrm.MonthEndDate, Date).ToString("dd-MMM-yyyy")
      Else
        Return OpenMonthToString
      End If
    End Get
  End Property



  Public Property MRMonthList As OBLib.NSWTimesheets.MRMonthList

  Protected Overrides Sub Setup()
    MyBase.Setup()

    MRMonthList = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "CloseMonth"
        If CType(OpenMonth.MonthStartDate, Date) <= Now Then
          MRMonthList.CloseOpenMonth()
          SaveMRMonth(True)
        End If
      Case "OpenPrevMonth"

        PrevMonthAutoClosedInd = MRMonthList.OpenManuallyPrevMonth()
        If Not PrevMonthAutoClosedInd Then
          SaveMRMonth(False)
        End If
      Case "OpenAutoClosedMonth"
        MRMonthList.OpenAutoClosedPrevMonth()
        SaveMRMonth(False)
    End Select
  End Sub

  Private Sub SaveMRMonth(ClosingMonthnd As Boolean)
    With TrySave(MRMonthList)
      If .Success Then
        If ClosingMonthnd Then
          MonthDetailsVisible = False
          MonthCloseSuccessVisible = True
          PrevMonthAutoClosedInd = False

        End If
      Else
      End If

    End With
  End Sub







End Class
