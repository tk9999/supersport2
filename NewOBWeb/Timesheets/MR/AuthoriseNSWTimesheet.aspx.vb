﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports OBLib.NSWTimesheets
Imports System.Linq
Imports OBLib.HR.ReadOnly

Public Class AuthoriseNSWTimesheet
  Inherits OBPageBase(Of AuthoriseNSWTimesheetVM)

End Class

Public Class AuthoriseNSWTimesheetVM
  Inherits OBViewModel(Of AuthoriseNSWTimesheetVM)

  Public Property StartDate As Date
  Public Property EndDate As Date

  Public ReadOnly Property CurrentUserID As Integer
    Get
      Return Singular.Security.CurrentIdentity.UserID
    End Get
  End Property

  <Singular.DataAnnotations.DropDownWeb(GetType(ROSubstituteManagerList), ValueMember:="ManagerHumanResourceID", ComboDeselectText:="All", DisplayMember:="ManagerName", UnselectedText:="All ", Source:=DropDownWeb.SourceType.ViewModel)>
  Public Property FilterManagerID As Integer?


  Public Property NSWTimesheetsAuthorisationList As OBLib.NSWTimesheets.NSWTimesheetsAuthorisationList 

  Protected Overrides Sub Setup()
    MyBase.Setup()
    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    Dim omth = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList.GetOpenMonth
    If omth IsNot Nothing Then
      StartDate = omth.MonthStartDate
      EndDate = omth.MonthEndDate
    End If
    SetupDataSource()

    MessageFadeTime = 5000
    If ROSubstituteManagerList.ShowSubstituteMessage Then
      AddMessage(Singular.Web.MessageType.Information, "Info", "Substitute On", "Please note that you Currently have Substitute manager on.")
    End If
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With TrySave(NSWTimesheetsAuthorisationList)
          If .Success Then
            NSWTimesheetsAuthorisationList = .SavedObject
          End If
        End With
      Case "Undo"
        UndoDataSource()


    End Select
  End Sub

  Private Sub SetupDataSource()

    NSWTimesheetsAuthorisationList = OBLib.NSWTimesheets.NSWTimesheetsAuthorisationList.GetNSWTimesheetsAuthorisationList()

  End Sub

  Private Sub UndoDataSource()
    Dim tempList = NSWTimesheetsAuthorisationList
    SetupDataSource()
    tempList.ToList.ForEach(Sub(itm)
                              Dim Onj = NSWTimesheetsAuthorisationList.GetItem(itm.HumanResourceID)
                              If Onj IsNot Nothing Then
                                Onj.Expanded = itm.Expanded
                              End If
                            End Sub)


  End Sub
  Public Function GetReimbursementValues(HRID As Integer, JSon As String, SyncID As Integer) As Singular.Web.Result

    Return New Singular.Web.Result(
      Function()

        Return NSWTimesheets.NSWTimesheetsAuthorisationList.GetReimbursementValues(HRID, JSon, SyncID)


      End Function)


  End Function


  Public ReadOnly Property ROSubstituteManagerList() As HR.ReadOnly.ROSubstituteManagerList
    Get
      Return OBLib.HR.ReadOnly.ROSubstituteManagerList.GetROSubstituteManagerList
    End Get
  End Property



End Class