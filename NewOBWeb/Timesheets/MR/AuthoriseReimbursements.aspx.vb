﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Public Class AuthoriseReimbursements
  Inherits OBPageBase(Of AuthoriseReimbursementsVM)

End Class

Public Class AuthoriseReimbursementsVM
  Inherits OBViewModel(Of AuthoriseReimbursementsVM)

  Public Property StartDate As Date
  Public Property EndDate As Date = Now.Date

  Public ReadOnly Property CurrentUserID As Integer
    Get
      Return Singular.Security.CurrentIdentity.UserID
    End Get
  End Property

  Public Class MealSummary
    Public Property Type As String
    Public Property Count As Integer
    Public Property Amount As Decimal
    Public Property Color As String
  End Class

  Public ReadOnly Property MealSummaryList As List(Of MealSummary)
    Get
      Dim msl As New List(Of MealSummary)
      msl.Add(New MealSummary() With {.Type = "Pending", .Color = "#ed9c28", .Count = NSWReimbursementAuthorisationList.PendingCount, .Amount = NSWReimbursementAuthorisationList.PendingAmount})
      msl.Add(New MealSummary() With {.Type = "Authorised", .Color = "#47a447", .Count = NSWReimbursementAuthorisationList.AuthorisedCount, .Amount = NSWReimbursementAuthorisationList.AuthorisedAmount})
      msl.Add(New MealSummary() With {.Type = "Rejected", .Color = "#d2322d", .Count = NSWReimbursementAuthorisationList.RejectedCount, .Amount = NSWReimbursementAuthorisationList.RejectedAmount})
      Return msl
    End Get
  End Property

  Public Property NSWReimbursementAuthorisationList As OBLib.NSWTimesheets.NSWReimbursementAuthorisationList

  Protected Overrides Sub Setup()
    MyBase.Setup()

    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    Dim omth = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList.GetOpenMonth
    If omth IsNot Nothing Then
      StartDate = omth.MonthStartDate
      EndDate = omth.MonthEndDate
    End If
    SetupDataSource()
    MessageFadeTime = 2000
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With TrySave(NSWReimbursementAuthorisationList)
          If .Success Then
            NSWReimbursementAuthorisationList = .SavedObject
          End If
        End With
      Case "Undo"
        UndoDataSource()
    End Select
  End Sub

  Private Sub SetupDataSource()

    NSWReimbursementAuthorisationList = OBLib.NSWTimesheets.NSWReimbursementAuthorisationList.GetNSWReimbursementAuthorisationList()

  End Sub


  Private Sub UndoDataSource()
    Dim tempList = NSWReimbursementAuthorisationList
    SetupDataSource()
    tempList.ToList.ForEach(Sub(itm)
                              Dim Onj = NSWReimbursementAuthorisationList.GetItem(itm.HumanResourceID)
                              If Onj IsNot Nothing Then
                                Onj.Expanded = itm.Expanded
                              End If
                            End Sub)

    NSWReimbursementAuthorisationList.CheckAllRules()
  End Sub








End Class