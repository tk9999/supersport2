﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="GeneralTimesheets.aspx.vb"
  Inherits="NewOBWeb.GeneralTimesheets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
    .ValidationPopup
    {
      width: 500px;
    }

    input.Authorised
    {
      background-color: #47a447;
      color: #ffffff;
      font-weight: bold;
    }

    input.Rejected
    {
      background-color: #d2322d ;
      color: #ffffff;
      font-weight: bold;
    }

    input.Pending
    {
      background-color: #ed9c28;
      color: #ffffff;
      font-weight: bold;

    }

  </style>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      With h.Toolbar
        .Helpers.HTML.Heading2("Timesheet Entry")
        
        .Helpers.Button(Singular.Web.DefinedButtonType.Save).PostBackType = Singular.Web.PostBackType.Ajax
      End With
      h.MessageHolder().AddClass("HoverMsg")

       
  
      With h.FieldSet(" ")
        
        With .Helpers.Div()
          .Style.FloatLeft()
          .Style.Width = 1000
          .Style.Display = Singular.Web.Display.inlineblock
          
          With .Helpers.Div() 'StartDate
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("Start Date: ")
              End With
            End With
             
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("90px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "StartDate()")
              End With
            End With

          End With
          
          With .Helpers.Div() 'EndDate 
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("End Date: ")
              End With
            End With
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("85px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "EndDate()")
              End With
            End With

          End With
        End With
       
 
 
      End With
         
      
      With h.TableFor(Of OBLib.NSWTimesheets.NSWTimesheetEntry)(Function(c) c.TimesheetList, True, True)
        .FirstRow.AddBinding(Singular.Web.KnockoutBindingString.enableChildren, Function(c) c.AuthorisedID <> 1)
        .FirstRow.AddColumn(Function(c) c.StartDateTime, 120)
        With .FirstRow.AddColumn("Start Time")
          With .Helpers.TimeEditorFor(Function(c) c.StartDateTime)
            .Style.Width = 60
          End With
        End With
        .FirstRow.AddColumn(Function(c) c.EndDateTime, 120)
        With .FirstRow.AddColumn("End time")
          With .Helpers.TimeEditorFor(Function(c) c.EndDateTime)
            .Style.Width = 60
          End With
        End With
        .FirstRow.AddColumn(Function(c) c.NSWTimesheetCategoryID, 200)
 
        With .FirstRow.AddColumn(Function(c) c.ProductionID, 200)
          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "disableProduction($data)")
        End With
        
        With .FirstRow.AddColumn(Function(c) c.Status, 200, "Status  /")
          With .Editor
            .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) c.AuthorisedID = -1)
            .AddBinding(Singular.Web.KnockoutBindingString.css, "AuthorisedColor($data)")
          End With
        End With
        
        With .AddRow
          .AddBinding(Singular.Web.KnockoutBindingString.enableChildren, Function(c) c.AuthorisedID <> 1)
          .AddColumn(Function(c) c.Description).ColSpan = 6
          .AddReadOnlyColumn(Function(c) c.RejectedReason)
        End With
       
                
      End With
      
    End Using
    
  %>

  <script type="text/javascript">

    function CheckProduction(Value, Rule, RuleArgs) {
      var CategoryID = RuleArgs.Object.NSWTimesheetCategoryID(); 
      if (!RuleArgs.IgnoreResults) {
        var Cat = ViewModel.NSWTimesheetCategoryList().Find("NSWTimesheetCategoryID", CategoryID);
        if (Cat != null && Cat.ProductionRelatedInd()) {
          if (RuleArgs.Object.ProductionID() == 0 || RuleArgs.Object.ProductionID() == null) {
            RuleArgs.AddError("Production is required");
            return;
          }
        } 
      }
    }

    function disableProduction(Object) {
      var CategoryID =  Object.NSWTimesheetCategoryID(); 
      var Cat = ViewModel.NSWTimesheetCategoryList().Find("NSWTimesheetCategoryID", CategoryID);
      if (Cat != null && Cat.ProductionRelatedInd()) {
        return true;
      }else {
        return false;
      } 
    } 

     
    function SetEndDates(obj) {

      if (obj.StartDateTime()) {
        if (obj.StartDateTime()) {
          var StartDateTemp = new Date(obj.StartDateTime());
          var TempDate = new Date(obj.EndDateTime());
          var EndDateTemp = new Date(StartDateTemp);
          if (obj.EndDateTime()) {
            EndDateTemp.setHours(TempDate.getHours(), TempDate.getMinutes());
          }
          //if (StartDateTemp.getTime() > EndDateTemp.getTime()) {
          //  EndDateTemp.setDate(EndDateTemp.getDate() + 1);
          //}
          if (TempDate.getTime() != EndDateTemp.getTime()) {
            obj.SInfo.SuspendChanges = true;
            obj.EndDateTime(EndDateTemp);
            obj.SInfo.SuspendChanges = false;
          }
        }
        Singular.Validation.CheckRules(obj);
      }
    }

    function CheckDates(Value, Rule, RuleArgs) {

      if (RuleArgs.Object.IsDirty && RuleArgs.Object.IsDirty()) {

        if (RuleArgs.Object.StartDateTime() && RuleArgs.Object.EndDateTime()) {
          var StartDate = new Date(RuleArgs.Object.StartDateTime()).getTime();
          var EndDate = new Date(RuleArgs.Object.EndDateTime()).getTime();

          var StartRange = new Date(ViewModel.StartDate()).getTime();
          var EndRange = new Date(ViewModel.EndDate()).getTime();

          if (!RuleArgs.IgnoreResults) {

            if (EndDate <= StartDate) {
              RuleArgs.AddError("Start time must be less than end time");
              return;
            }  

         
              
            for (var i = 0; i < ViewModel.TimesheetList().length; i++) {
              var ts = ViewModel.TimesheetList()[i];
              if (ts.Guid != RuleArgs.Object.Guid) {
                var StartTest = new Date(ts.StartDateTime()).getTime();
                var EndTest = new Date(ts.EndDateTime()).getTime();
                if ((StartDate >= StartTest && StartDate < EndTest) || (EndDate > StartTest && EndDate <= EndTest)) {
                  RuleArgs.AddError("You have already captured a timesheet entry between " + new Date(ts.StartDateTime()).format("dd MMM yyyy - HH:mm") + " & " + new Date(ts.EndDateTime()).format("dd MMM yyyy - HH:mm"));
                  break;
                } else if ((StartTest >= StartDate && StartTest < EndDate) || (EndTest > StartDate && EndTest <= EndDate)) {
                  RuleArgs.AddError("You have already captured a timesheet entry between " + new Date(ts.StartDateTime()).format("dd MMM yyyy - HH:mm") + " & " + new Date(ts.EndDateTime()).format("dd MMM yyyy - HH:mm"));
                  break;
                }
              } 
            }
          }
        }       
      }
    }
     
    function FilterProductionWeekly (List, NSWTS) {
      var Productions = [];

      var StartDate = new Date(NSWTS.StartDateTime());
      var EndDate = new Date(NSWTS.EndDateTime());
      
      StartDate.setDate(StartDate.getDate() - 7)
      EndDate.setDate(EndDate.getDate() + 2)

      StartDate = StartDate.getTime();
      EndDate = EndDate.getTime();
       
      List.Iterate(function (pr, Index) {
        var StartTest =new Date(pr.PlayStartDateTime).getTime();
        var EndTest = new Date(pr.PlayEndDateTime).getTime();

        if (StartTest >= StartDate && EndTest <= EndDate) {
          Productions.push(pr);
        }
      });
      return Productions; 
    }

    function AuthorisedColor(obj) {
      var clss = ''
      switch (obj.AuthorisedID()) {
        case 1:
          clss = 'Authorised';
          break;
        case 2:
          clss = 'Rejected';
          break;
        case 0:
          clss = 'Pending';
          break;
          
      }
      return clss
    }

  </script>
</asp:Content>
