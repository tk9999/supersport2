﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc
Imports System

Public Class SpecialMealReimbursement
  Inherits OBPageBase(Of SpecialMealReimbursementVM)

End Class

Public Class SpecialMealReimbursementVM
  Inherits OBViewModel(Of SpecialMealReimbursementVM)

  Public Property StartDate As Date
  Public Property EndDate As Date
  Public Property MaxDate As Date = Now.AddDays(-1)

  Public Property MRSpecialMealReimbursementList As OBLib.NSWTimesheets.MRSpecialMealReimbursementList

  Protected Overrides Sub Setup()
    MyBase.Setup()
    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    Dim omth = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList.GetOpenMonth
    If omth IsNot Nothing Then
      StartDate = omth.MonthStartDate
      EndDate = omth.MonthEndDate
    End If
    SetupDataSource()
    MessageFadeTime = 2000
    ClientDataProvider.AddDataSource("HolidayList", CommonData.Lists.PublicHolidayList.FilterHolidayList(StartDate, EndDate), False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With TrySave(MRSpecialMealReimbursementList)
          If .Success Then
            SetupDataSource()
          End If
        End With
    End Select
  End Sub

  Private Sub SetupDataSource()

    MRSpecialMealReimbursementList = OBLib.NSWTimesheets.MRSpecialMealReimbursementList.GetMRSpecialMealReimbursementList()
     
  End Sub

  <DebuggerBrowsable(False)>
  Public ReadOnly Property ManagerHumanResourceList() As OBLib.NSWTimesheets.ReadOnly.ROManagerHumanResourceList
    Get
      Return OBLib.NSWTimesheets.ReadOnly.ROManagerHumanResourceList.GetROManagerHumanResourceList()
    End Get
  End Property

  Public Function GetReimbursementValues(HRID As Integer, StartDateTime As DateTime, EndDateTime As DateTime, SyncID As Integer) As Singular.Web.Result

    Return New Singular.Web.Result(
      Function()

        Return NSWTimesheets.MRSpecialMealReimbursementList.GetReimbursementAmount(HRID, StartDateTime, EndDateTime, SyncID)


      End Function)

  End Function

  <DebuggerBrowsable(False)>
  Public ReadOnly Property NSWTimesheetCategoryList() As OBLib.NSWTimesheets.NSWTimesheetCategoryList
    Get
      Return CommonData.Lists.NSWTimesheetCategoryList
    End Get
  End Property



End Class