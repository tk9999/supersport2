﻿Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib
Imports Singular.Misc

Public Class GeneralTimesheets
  Inherits OBPageBase(Of GeneralTimesheetVM)

End Class

Public Class GeneralTimesheetVM
  Inherits OBViewModel(Of GeneralTimesheetVM)

  Public Property StartDate As Date
  Public Property EndDate As Date
  Public Property MaxDate As Date = Now.Date


  Public Property TimesheetList As OBLib.NSWTimesheets.NSWTimesheetEntryList

  Protected Overrides Sub Setup()
    MyBase.Setup()
    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    Dim omth = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList.GetOpenMonth
    If omth IsNot Nothing Then
      StartDate = omth.MonthStartDate
      EndDate = omth.MonthEndDate
    End If
    SetupDataSource()

    MessageFadeTime = 2000

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "Save"
        With TrySave(TimesheetList)
          If .Success Then
            TimesheetList = .SavedObject
          End If
        End With
      Case "Refresh"
        SetupDataSource()
    End Select
  End Sub

  Private Sub SetupDataSource()

    TimesheetList = OBLib.NSWTimesheets.NSWTimesheetEntryList.GetNSWTimesheetEntryList()


  End Sub

  <DebuggerBrowsable(False)>
  Public ReadOnly Property RONSWOpenMonthProductionList() As OBLib.NSWTimesheets.ReadOnly.RONSWOpenMonthProductionList
    Get
      Return CommonData.Lists.RONSWOpenMonthProductionList
    End Get
  End Property

 
  <DebuggerBrowsable(False)>
  Public ReadOnly Property NSWTimesheetCategoryList() As OBLib.NSWTimesheets.NSWTimesheetCategoryList
    Get 
      Return CommonData.Lists.NSWTimesheetCategoryList
    End Get
  End Property

End Class