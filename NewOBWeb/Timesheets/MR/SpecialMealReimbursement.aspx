﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SpecialMealReimbursement.aspx.vb"
   Inherits="NewOBWeb.SpecialMealReimbursement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
.ValidationPopup
    {
      width: 500px;
    }
    td.CellWait
    {
      background-image: url('../Singular/Images/LoadingSmall.gif');
      background-repeat: no-repeat;
      background-position: 3px 5px;
      color: #bbb;
    }

  </style>
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      With h.Toolbar
        .Helpers.HTML.Heading2("Special Meal Reimbursements")
        
        With .Helpers.Button(Singular.Web.DefinedButtonType.Save)
          .PostBackType = Singular.Web.PostBackType.Ajax
          .AddBinding(Singular.Web.KnockoutBindingString.enable, "ButtonsEnabled()")
        End With
        
      End With
      h.MessageHolder().AddClass("HoverMsg")
       
  
      With h.FieldSet(" ")
        
        With .Helpers.Div()
          .Style.FloatLeft()
          .Style.Width = 1000
          .Style.Display = Singular.Web.Display.inlineblock
          
          With .Helpers.Div() 'StartDate
            .Style.FloatLeft()
            .Style.Width = "30%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
               
                .Helpers.HTML.Heading4("Start Date: ")
              End With
            End With
            
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("90px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "StartDate()")
              End With
            End With

          End With
          
          With .Helpers.Div() 'EndDate
            '    .Style.Margin("0", "10px", "0", "15")
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("End Date: ")
              End With
            End With
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("85px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "EndDate()")
              End With 
            End With 
          End With
        End With
       
 
 
      End With
         
      
      With h.TableFor(Of OBLib.NSWTimesheets.MRSpecialMealReimbursement)(Function(c) c.MRSpecialMealReimbursementList, True, True)
        
        .FirstRow.AddColumn(Function(c) c.HumanResourceID, 150)
        .FirstRow.AddColumn(Function(c) c.EmployeeCode, 100)

        With .FirstRow.AddColumn(Function(c) c.StartDateTime, 120)
          .Style.Style("text-align") = "center"
        End With
        With .FirstRow.AddColumn("Start Time")
          .Style.Style("text-align") = "center"
          With .Helpers.TimeEditorFor(Function(c) c.StartDateTime)
            .Style.Width = 60
            .Style.Style("text-align") = "center"
          End With
        End With
        With .FirstRow.AddColumn(Function(c) c.EndDateTime, 120)
          .Style.Style("text-align") = "center"
        End With
        With .FirstRow.AddColumn("End time")
          .Style.Style("text-align") = "center"
          With .Helpers.TimeEditorFor(Function(c) c.EndDateTime)
            .Style.Width = 60
            .Style.Style("text-align") = "center"
          End With
        End With
  
        .FirstRow.AddColumn(Function(c) c.NSWTimesheetCategoryID, 200)
        With .FirstRow.AddColumn(Function(c) c.ProductionID, 200)
          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "disableProduction($data)")
        End With
 
        With .FirstRow.AddColumn(Function(c) c.NoOfMeals, 70)
          .Style.Style("text-align") = "right"
          .CellBindings.Add(Singular.Web.KnockoutBindingString.css, "IsLoading() ? 'CellWait' : ''")
        End With
        With .FirstRow.AddReadOnlyColumn(Function(c) c.Amount, 80)
          .Style.Style("text-align") = "right"
          .CellBindings.Add(Singular.Web.KnockoutBindingString.css, "IsLoading() ? 'CellWait' : ''")
        End With
        
        
        With .AddRow
          .AddColumn(Function(c) c.ReimbursementReason).ColSpan = 10
        End With
    
        
          
      End With
      
    End Using
    
    %>

  <script type="text/javascript">
     
     

    function CheckHRDates(Value, Rule, RuleArgs) { 
      var HRID = RuleArgs.Object.HumanResourceID();
  
        if (RuleArgs.Object.IsDirty && RuleArgs.Object.IsDirty()) {

          if (RuleArgs.Object.StartDateTime() && RuleArgs.Object.EndDateTime() && RuleArgs.Object.HumanResourceID()) {
            var StartDate = new Date(RuleArgs.Object.StartDateTime()).getTime();
            var EndDate = new Date(RuleArgs.Object.EndDateTime()).getTime();
              

            if (!RuleArgs.IgnoreResults) {

              for (var i = 0; i < ClientData.HolidayList.length; i++) {
                var ph = ClientData.HolidayList[i];

                if (new Date(RuleArgs.Object.StartDateTime()).format("dd MMM yyyy") == ph.HolidayDate) {
                  RuleArgs.AddError("Cannot have special meals on a Public Holiday");
                  return;
                }
                if (new Date(RuleArgs.Object.EndDateTime()).format("dd MMM yyyy") == ph.HolidayDate) {
                  RuleArgs.AddError("Cannot have special meals on a Public Holiday");
                  return;
                }

              }
           

              if (new Date(RuleArgs.Object.StartDateTime()).getDay() == 0 || new Date(RuleArgs.Object.StartDateTime()).getDay() == 6) {
                RuleArgs.AddError("Cannot have special meals on a weakend");
                return;
              }

              if (new Date(RuleArgs.Object.EndDateTime()).getDay() == 0 || new Date(RuleArgs.Object.EndDateTime()).getDay() == 6) {
                RuleArgs.AddError("Cannot have special meals on a weakend");
                return;
              }

              if (EndDate <= StartDate) {
                RuleArgs.AddError("Start time must be less than end time");
                return;
              }  

              var HrList = ViewModel.MRSpecialMealReimbursementList().Filter("HumanResourceID", HRID);
         
              
              for (var i = 0; i < HrList.length; i++) {
                var ts = HrList[i];
                if (ts.Guid != RuleArgs.Object.Guid) {
                  var StartTest = new Date(ts.StartDateTime()).getTime();
                  var EndTest = new Date(ts.EndDateTime()).getTime();
                  if ((StartDate >= StartTest && StartDate < EndTest) || (EndDate > StartTest && EndDate <= EndTest)) {
                    RuleArgs.AddError("You have already captured a Special Meal for this person between " + new Date(ts.StartDateTime()).format("dd MMM yyyy - HH:mm") + " & " + new Date(ts.EndDateTime()).format("dd MMM yyyy - HH:mm"));
                    break;
                  } else if ((StartTest >= StartDate && StartTest < EndDate) || (EndTest > StartDate && EndTest <= EndDate)) {
                    RuleArgs.AddError("You have already captured a timesheet entry between " + new Date(ts.StartDateTime()).format("dd MMM yyyy - HH:mm") + " & " + new Date(ts.EndDateTime()).format("dd MMM yyyy - HH:mm"));
                    break;
                  }
                } 
              }
            }
          }       
        }
      }
  
    function SetEndDates(obj) {

      if (obj.StartDateTime()) {
        if (obj.StartDateTime()) {
          var StartDateTemp = new Date(obj.StartDateTime());
          var TempDate = new Date(obj.EndDateTime());
          var EndDateTemp = new Date(StartDateTemp);
          if (obj.EndDateTime()) {
            EndDateTemp.setHours(TempDate.getHours(), TempDate.getMinutes());
          } 
 
            obj.SInfo.SuspendChanges = true;
            obj.EndDateTime(EndDateTemp);
            obj.SInfo.SuspendChanges = false;
      
        }
        Singular.Validation.CheckRules(obj);
      }
    }

    function SetEmployeCode(obj) {
      var HumanResourceID = obj.HumanResourceID(); 
      if (HumanResourceID) {  
        var hr = ViewModel.ManagerHumanResourceList().Find("HumanResourceID", HumanResourceID);
        if (hr) {
          obj.EmployeeCode(hr.EmployeeCodeIDNo())
        } 
      }
    }

    function SetAmount(SMREntry) {
  
      Singular.ClearMessages();
      if (SMREntry.StartDateTime() && SMREntry.EndDateTime() && SMREntry.HumanResourceID()) {
        var StartDate = new Date(SMREntry.StartDateTime()).getTime();
        var EndDate = new Date(SMREntry.EndDateTime()).getTime();

        if (StartDate < EndDate) {
               SMREntry.IsLoading(true);
        SMREntry.SyncID(SMREntry.SyncID() + 1);

        ViewModel.CallServerMethod('GetReimbursementValues', { HRID: SMREntry.HumanResourceID(), StartDateTime: SMREntry.StartDateTime(), EndDateTime: SMREntry.EndDateTime(), SyncID: SMREntry.SyncID() }, function (result) {

          if (SMREntry.SyncID() == result.Data.SyncID) {
            SMREntry.NoOfMeals(result.Data.NoOfMeals);
            SMREntry.Amount(result.Data.MRAmount);
            SMREntry.IsLoading(false);
          }
        });
      }
      }

    }

    function FilterProductionWeekly (List, NSWTS) {
      var Productions = [];

      var StartDate = new Date(NSWTS.StartDateTime());
      var EndDate = new Date(NSWTS.EndDateTime());
      
      StartDate.setDate(StartDate.getDate() - 7)
      EndDate.setDate(EndDate.getDate() + 2)

      StartDate = StartDate.getTime();
      EndDate = EndDate.getTime();
       
      List.Iterate(function (pr, Index) {
        var StartTest =new Date(pr.PlayStartDateTime).getTime();
        var EndTest = new Date(pr.PlayEndDateTime).getTime();

        if (StartTest >= StartDate && EndTest <= EndDate) {
          Productions.push(pr);
        }
      });
      return Productions; 
    }

    function disableProduction(Object) {
      var CategoryID =  Object.NSWTimesheetCategoryID(); 
      var Cat = ViewModel.NSWTimesheetCategoryList().Find("NSWTimesheetCategoryID", CategoryID);
      if (Cat != null && Cat.ProductionRelatedInd()) {
        return true;
      }else {
        return false;
      } 
    }
    
    function CheckProduction(Value, Rule, RuleArgs) {
      var CategoryID = RuleArgs.Object.NSWTimesheetCategoryID(); 
      if (!RuleArgs.IgnoreResults) {
        var Cat = ViewModel.NSWTimesheetCategoryList().Find("NSWTimesheetCategoryID", CategoryID);
        if (Cat != null && Cat.ProductionRelatedInd()) {
          if (RuleArgs.Object.ProductionID() == 0 || RuleArgs.Object.ProductionID() == null) {
            RuleArgs.AddError("Production is required");
            return;
          }
        } 
      }
    }

    function ButtonsEnabled() {
      for (var i = 0; i < ViewModel.MRSpecialMealReimbursementList().length; i++) {
        var item = ViewModel.MRSpecialMealReimbursementList()[i];
        if (item.IsLoading()) return false;
      }
      return true;
    }

  </script>
</asp:Content>
