﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Site.Master" CodeBehind="AuthoriseReimbursements.aspx.vb" 
   Inherits="NewOBWeb.AuthoriseReimbursements" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
     td.LButtons, th.LButtons
    {
      width: 24px !important;
    } 
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%
    Using h = Helpers
       
      With h.Toolbar
        .Helpers.HTML.Heading2("Reimbursement Authorisation")
        
        .Helpers.Button(Singular.Web.DefinedButtonType.Save).PostBackType = Singular.Web.PostBackType.Ajax 
      End With
      h.MessageHolder().AddClass("HoverMsg")
       
  
      With h.FieldSet(" ")
        
        .Style.Display = Singular.Web.Display.tablerow
        With .Helpers.Div()
          .Style.FloatLeft()
          .Style.Width = 900
          .Style.Display = Singular.Web.Display.inlineblock
           
          With .Helpers.Div() 'StartDate
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("Start Date: ")
              End With
            End With
             
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("90px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "StartDate()")
              End With
            End With
          End With
          
          With .Helpers.Div() 'EndDate 
            .Style.FloatLeft()
            .Style.Width = "40%"
            With .Helpers.Div() ' Label
              .Style.FloatLeft()
              With .Helpers.Div()
                .Helpers.HTML.Heading4("End Date: ")
              End With
            End With
            With .Helpers.Div()
              With .Helpers.HTMLTag("h4")
                .Style.MarginLeft("85px")
                .AddBinding(Singular.Web.KnockoutBindingString.html, "EndDate()")
              End With
            End With
          End With
        End With
      End With
        
      With h.FieldSet(" ")
        .Legend.Style.Width = "900px"
        .Style.Display = Singular.Web.Display.inlineblock 
        
        .Helpers.Control(tblSummary)
         
      End With

          
  
         
      
      With h.TableFor(Of OBLib.NSWTimesheets.NSWReimbursementAuthorisation)(Function(c) c.NSWReimbursementAuthorisationList, False, False)
         
        .FirstRow.AddReadOnlyColumn(Function(c) c.EmployeeCodeIDNo, 90)
        .FirstRow.AddReadOnlyColumn(Function(c) c.FirstName, 120)
        .FirstRow.AddReadOnlyColumn(Function(c) c.Surname, 120) 
        '' .FirstRow.AddReadOnlyColumn(Function(c) c.TimeAuthorisedBy, 120)
        .FirstRow.AddReadOnlyColumn(Function(c) c.AuthorisedNoOfMeals, 50)
        .FirstRow.AddReadOnlyColumn(Function(c) c.SpecialMeals, 50)
        .FirstRow.AddReadOnlyColumn(Function(c) c.ICRMeals, 50)
        .FirstRow.AddReadOnlyColumn(Function(c) c.TotalMeals, 50)
        .FirstRow.AddReadOnlyColumn(Function(c) c.AuthorisedReimbursementAmount, 100) 
  
         
        
        With .FirstRow.AddColumn()
          With CType(.Helpers.EditorFor(Function(c) c.AuthorisedID), Singular.Web.CustomControls.RadioButtonEditor(Of OBLib.NSWTimesheets.NSWReimbursementAuthorisation))
            .Horizontal = True
            ' .Style.Width = 300
            .Style.Width = 230
          End With
        End With
        
        With .FirstRow.AddColumn(Function(c) c.RejectedReason, 300)
           
          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) c.AuthorisedID = 2)
        End With
        
        ' Add ReadOnly Details : Child List
        
        With .AddChildTable(Of OBLib.NSWTimesheets.NSWReimbursementAuthorisationDetail)(Function(c) c.NSWReimbursementAuthorisationDetailList, False, False)
       
          With .FirstRow.AddReadOnlyColumn(Function(c) c.StartDateTime, 80)
            .Style.Style("text-align") = "left"
          End With
          With .FirstRow.AddColumn("Start Time")
            With .Helpers.TimeEditorFor(Function(c) c.StartDateTime)
              .Style.Width = 50
            End With
          End With
          With .FirstRow.AddColumn(Function(c) c.EndDateTime, 90)
            .Style.Style("text-align") = "Center"
          End With
          With .FirstRow.AddColumn("End Time")
            With .Helpers.TimeEditorFor(Function(c) c.EndDateTime)
              .Style.Width = 50
            End With
          End With
          .FirstRow.AddColumn(Function(c) c.MealType, 90)
          .FirstRow.AddColumn(Function(c) c.Production, 200)
          .FirstRow.AddColumn(Function(c) c.NSWTimesheetCategory, 200)
          With .FirstRow.AddColumn(Function(c) c.Hours, 50)
            .Style.Style("text-align") = "right"
          End With
          .FirstRow.AddColumn(Function(c) c.AuthorisedBy, 120)
          With .FirstRow.AddColumn(Function(c) c.AuthorisedDateTime, 90)
            .Style.Style("text-align") = "Center"
          End With
          With .AddRow
            .AddColumn(Function(c) c.Description).ColSpan = 10
          End With

        End With
        
      End With
      
    End Using
    
    %>

  <asp:PlaceHolder ID="tblSummary" runat="server">

    <div data-bind="foreach: GetSummaryList()">
       <table class="Grid" style="display: inline-block; margin-right: 10px">
      <thead><tr><th colspan="2" style="text-align:center" data-bind="text: Type, style: { 'background-color': Color }"></th></tr></thead>
      <tbody>
        <tr><td style="width: 50px;">Employees</td><td style="text-align:right; width: 50px;" data-bind="text: Count"></td></tr>
        <tr><td style="width: 50px;">Meals</td><td style="text-align:right; width: 50px;" data-bind="text: Meals"></td></tr>
        <tr><td>Amount</td><td style="text-align:right; width: 70px"; data-bind="NValue: { Value: Amount, Format: '#,##0.00' }"></td></tr>
      </tbody>
    </table>
    </div>
   

  </asp:PlaceHolder>

  <script type="text/javascript">

    function CheckRejectedReason(Value, Rule, RuleArgs) {
      var AuthorisedID = RuleArgs.Object.AuthorisedID();
      if (!RuleArgs.IgnoreResults) { 
        if (RuleArgs.Object.AuthorisedID() ==2 && RuleArgs.Object.RejectedReason() == '') {
          RuleArgs.AddError("Rejected Reason is required");
          setTimeout(function () { $(RuleArgs.Object.RejectedReason.BoundElements[0]).focus(), 0 });
          return;
        } else if (RuleArgs.Object.AuthorisedID() != 2) {
          RuleArgs.Object.RejectedReason("");
          }

      } 
    }
 
    function GetSummaryList() {
      var list = [];
      list.push({ Type: 'Pending', Color: '#ed9c28', Count: 0, Meals: 0, Amount: 0 });
      list.push({ Type: 'Authorised', Color: '#47a447', Count: 0, Meals: 0, Amount: 0 });
      list.push({ Type: 'Rejected', Color: '#d2322d', Count: 0, Meals: 0, Amount: 0 });

      for (var i = 0; i < ViewModel.NSWReimbursementAuthorisationList().length; i++) {
        var obj = ViewModel.NSWReimbursementAuthorisationList()[i];
      
        list[obj.AuthorisedID()].Count += 1;
        list[obj.AuthorisedID()].Meals += obj.TotalMeals();
          list[obj.AuthorisedID()].Amount += obj.AuthorisedReimbursementAmount();
        
      }
      return list;
  
    }
    
    function Authorise(obj, id) {
      var newid = (obj.AuthorisedID() + 1) % 3

      obj.AuthorisedID(newid);
      obj.AuthorisedByUserID(ViewModel.CurrentUserID());
    }

    //function AuthorisePendingList(List, id) {

    //  for (var i = 0; i < List.length; i++) {
    //    var obj = List[i];
    //    if (obj.AuthorisedID() == 0) {
    //      obj.AuthorisedID(id);
    //      obj.AuthorisedByUserID(ViewModel.CurrentUserID());
    //    }
    //  }
    //}

 
    function AuthorisedCss(obj) {
      switch (obj.AuthorisedID()) {
        case 1:
          return 'btn btn-s btn-success';
          break;
        case 2:
          return 'btn btn-s btn-danger';
          break;
        case 0:
          return 'btn btn-s btn-warning';
          break;
      }
    }

    function AuthorisedHTML(AuthorisedID) {
      switch (AuthorisedID) {
        case 1:
          return '<span class=\"glyphicon glyphicon-ok\"></span> Authorised';
          break;
        case 2:
          return '<span class=\"glyphicon glyphicon-remove\"></span> Rejected';
          break;
        case 0:
          return '<span class=\"glyphicon glyphicon-minus\"></span> Pending';
          break;
      }
    }


  </script>
</asp:Content>