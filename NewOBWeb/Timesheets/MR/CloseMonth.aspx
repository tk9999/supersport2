<%@ Page Title="Close Month" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
  CodeBehind="CloseMonth.aspx.vb" Inherits="NewOBWeb.CloseMonth" %>

<%@ Import Namespace="Singular.Web" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
  <%--<link href="../Bootstrap/override/signin.css" rel="stylesheet" type="text/css" />--%>
    <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../../Styles/FlatDream.css" rel="Stylesheet" />

  <style type="text/css">
    body, .panel, .panel-heading
    {
      background-image: none !important;
      background-color: #ffffff !important;
      background-repeat: repeat-x !important;
    }
    .panel
    {
      border: none;
    }
    .panel-default > .panel-heading
    {
      border: none;
      background-color: #ffffff !important;
    }
    .container
    {
      margin-top: 100px;
    }
    .Msg div, .ValidationPopup
    {
      border-style: solid;
      border-width: 1px;
      border-radius: 4px;
      margin: 6px 0 5px 0;
      background-repeat: no-repeat;
      background-position: 3px 3px;
      padding: 3px 10px 6px 24px;
      font-size: 0.9em;
      color: #000;
    }
    .Msg div strong
    {
      line-height: 1.5em;
    }
    .Msg-Validation, .Msg-Error
    {
      background-color: #FBECEB;
      border-color: #c44;
    }
    .Msg-Validation
    {
      background-image: url('../Images/IconBRule.png');
      overflow: auto;
    }
    .Msg-Validation > ul
    {
      padding-left: 0;
    }
    .Msg-Validation strong, .Msg-Error strong
    {
      color: #c44;
      line-height: 1.2em;
    }
    .Msg-Error
    {
      background-image: url('../Images/IconError.png');
    }
    .Msg-Success
    {
      background-image: url('../Images/IconAuth.png');
      background-color: #F1F8EE;
      border-color: #4a2;
    }
    .Msg-Success strong
    {
      color: #291;
    }
    
    .Msg-Warning
    {
      background-image: url('../Images/IconWarning.png');
      background-color: #FFF7E5;
      border-color: #D9980D;
    }
    .Msg-Warning strong
    {
      color: #D9980D;
    }
    
    .Msg-Information
    {
      background-image: url('../Images/IconInfo.png');
      background-color: #E9F1FB;
      border-color: #225FAA;
    }
    .Msg-Information strong
    {
      color: #225FAA;
    }
    .MsgBoxMessage > ul
    {
      padding-left: 0;
    }

    .Center-Container {
      
    }

    .CenterDiv{
      width: 30%;
      height: 30%;
      margin: auto;
      position: absolute;
      top: 0; left: 0; bottom: 0; right: 0;
      border-radius: 15px 15px 15px 15px;
      padding-right: 10px !important;
      padding-top: 10px !important;
    }

    #Main {
      background-color: #F0F0F0;
    }

    .dd-item-padded {
      padding-right: 10px !important;

    }
  .dd-item{
   text-align:center !important;
  }
  </style>
  <script type="text/javascript">
    $('html').bind('keypress', function (e) {
      if (e.keyCode == 13) {
        return false;
      }
    }); 
  </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  <%
    Using h = Helpers

      '    h.MessageHolder()
        
      'With h.Div()
      '  .AddClass("CenterDiv")
      '  With .Helpers.Div()
      '    .AddClass("CenterDiv")
      '    With .Helpers.Div()
      '      .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.MonthDetailsVisible)
      '      With .Helpers.HTMLTag("h6")
      '        .Helpers.HTML("Meal Reimbursement Financial Month")
      '      End With
      '      With .Helpers.Div()
      '        With .Helpers.Div()
      '          .Style.Display = Singular.Web.Display.inlineblock
      '          With .Helpers.Div()
      '            With .Helpers.ReadOnlyRow("Current month :", Function(c) c.OpenMonthToString)
      '            End With
      '            With .Helpers.BootstrapButton("CloseMonth", "Close Month", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "RunCommand('CloseMonth')")
      '              .Button.Style.Width = 80
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) ViewModel.CanCloseMonthInd)
      '            End With
      '          End With
      '        End With
      '        .Helpers.HTML("<br>")
      '        With .Helpers.Div()
      '          .Style.Display = Singular.Web.Display.inlineblock
      '          With .Helpers.Div()
      '            .Style.FloatLeft()
      '            .Helpers.ReadOnlyRow("Previous month :", Function(c) c.PrevMonthToString)
      '            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.CanReOpenMonthInd)
      '            With .Helpers.BootstrapButton("OpenPrevMonth", "Re-Open Prev Month", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "RunCommand('OpenPrevMonth')")
      '              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.CanReOpenMonthInd)
      '            End With
      '          End With
      '        End With
      '        '
      '      End With
      '    End With
      '    With .Helpers.BootstrapButton("", , "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
      '      With .Button
      '        .AddBinding(Singular.Web.KnockoutBindingString.html, "Cancel")
      '        .AddBinding(Singular.Web.KnockoutBindingString.click, "Done()")
      '        .Style.Width = 80
      '      End With
      '    End With
            
          
      '    'With .Helpers.DivC("content-wrap")
      '    '  .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.MonthCloseSuccessVisible)
            
      '    '  .Helpers.HTML("Month Closed Successfully <br>")
      '    '  With .Helpers.BootstrapButton("Ok", "Ok", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
      '    '    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "Done()")
      '    '  End With
      '    'End With
      '  End With
      'End With
      
      With h.Bootstrap.Row
        ' .AddBinding(KnockoutBindingString.visible, Function(d) Not ViewModel.OptionSelected)
        With .Helpers.Bootstrap.Column(12, 12, 2, 2)
          .AddClass("CenterDiv")
          With .Helpers.Bootstrap.FlatBlock("Meal Reimbursement Financial Month", False, False)
            With .ContentTag
              With .Helpers.Div()
                .AddClass("dd")
                With .Helpers.HTMLTag("ol")
                  .AddClass("dd-list")
                  With .Helpers.HTMLTag("li")
                    .AddClass("dd-item")
                    With .Helpers.ReadOnlyRow("Current month :", Function(c) c.OpenMonthToString)
                    End With
                  End With
                  With .Helpers.HTMLTag("li")
                    .AddClass("dd-item")
                    With .Helpers.BootstrapButton("CloseMonth", "Close Month", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "RunCommand('CloseMonth')")
                      .Button.Style.Width = 120
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) ViewModel.CanCloseMonthInd)
                    End With
                  End With
                  With .Helpers.HTMLTag("li")
                    .AddClass("dd-item")
                    .Helpers.ReadOnlyRow("Previous month :", Function(c) c.PrevMonthToString)
                    .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.CanReOpenMonthInd)
                    With .Helpers.BootstrapButton("OpenPrevMonth", "Re-Open Prev Month", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "RunCommand('OpenPrevMonth')")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.CanReOpenMonthInd)
                      .Button.Style.Width = 120
                    End With
                  End With
                  With .Helpers.HTMLTag("li")
                    .AddClass("dd-item")
                    With .Helpers.BootstrapButton("", , "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
                      With .Button
                        .AddBinding(Singular.Web.KnockoutBindingString.html, "Cancel")
                        .AddBinding(Singular.Web.KnockoutBindingString.click, "Done()")
                        .Style.Width = 120
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.DivC("dd")
            '  With .Helpers.HTMLTag("ol")
            '    .AddClass("dd-list")
            '    With .Helpers.HTMLTag("li")
            '      .AddClass("dd-item")
            '      With .Helpers.DivC("dd-handle")
            '        .Helpers.HTML("Bulk SmS")
            '        .AddBinding(KnockoutBindingString.click, "SetupBulkSmS()")
            '      End With
            '    End With
            '    With .Helpers.HTMLTag("li")
            '      .AddClass("dd-item")
            '      With .Helpers.DivC("dd-handle")
            '        .Helpers.HTML("Generate SmSes")
            '        .AddBinding(KnockoutBindingString.click, "SetupSmSGenerator()")
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
      End With
      
      With h.BootstrapDialog("Success", "MR  Month")
        .ModalDialogDiv.AddClass("modal-sm")
        With .Body
          
          'With .Helpers.HTMLTag("h3")
          .AddBinding(Singular.Web.KnockoutBindingString.html, "feedBack()")
          'End With
          '.Helpers.HTML("Month Closed Successfully <br>")
    
        End With
        With .Footer
          With .Helpers.BootstrapButton("", , "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
            With .Button
              .AddBinding(Singular.Web.KnockoutBindingString.html, Function(c) If(ViewModel.MonthCloseSuccessVisible OrElse Not ViewModel.PrevMonthAutoClosedInd, "Ok", "No"))
              .AddBinding(Singular.Web.KnockoutBindingString.click, "Done()")
              '  .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.MonthCloseSuccessVisible OrElse Not ViewModel.PrevMonthAutoClosedInd)
              
            End With
          End With
          With .Helpers.BootstrapButton("Yes", "Yes", "btn btn-xs btn-primary", , Singular.Web.PostBackType.None)
            .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "RunCommand('OpenAutoClosedMonth')")
            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.PrevMonthAutoClosedInd)
          End With
        End With
        
      End With
        
    End Using
      
  %>
    <script type="text/javascript">

      function RunCommand(Command) {
       
        Singular.SendCommand(Command, {}, function (resposne) {
          if (Command == "OpenAutoClosedMonth") {
            Done();
          } else {
            $("#Success").modal();
          }
        });
      }

      function Done() {
        window.location.href = "../../Default.aspx"
      }

      function feedBack() {
        if (ViewModel.MonthCloseSuccessVisible()  == true) {
          return "Month closed successfully";
        } else if (ViewModel.PrevMonthAutoClosedInd() == true) {
          return "Please note the previous month was Auto Closed. <br >Are sure you want to re-open it?";
        } else if (ViewModel.PrevMonthAutoClosedInd() == false) {
          return "Previous month was Re-opened successfully";
        } else {
          Done();
        }
      }
      </script>
</asp:Content>
