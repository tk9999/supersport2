﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="PaymentRunNew.aspx.vb" Inherits="NewOBWeb.PaymentRunNew" %>

<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.Invoicing.New" %>
<%@ Import Namespace="OBLib.Invoicing.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%--  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../../Styles/FlatDream.css" rel="Stylesheet" />
  <link type="text/css" href="../../Styles/jquery-ui-bootstrap-dialog.css" rel="Stylesheet" />--%>
  <style type="text/css">
    body {
      background-color: #F0F0F0;
    }

    table tr th {
      font-weight: 700;
    }

    table tr td {
      font-weight: 400;
    }

    td.LButtons > .btn-xs {
      padding: 1px 2px !important;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <%Using h = Helpers
      
      'Toolbar----------------------------------------------------------------------------------------------------------------------
      With Helpers.Bootstrap.FlatBlock("Manage Payment Runs", False, True, False)
        .FlatBlockTag.AddClass("reduced-margin-bottom reduced-padding")
        With .ContentTag
          .AddClass("reduced-padding-top")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.With(Of PaymentRun)(Function(d) ViewModel.CurrentPaymentRun)
                With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o",
                                               , Singular.Web.PostBackType.None, "PaymentRunPage.savePaymentRun($data)")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "PaymentRunBO.CanView('SaveButton', $data)")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                End With
              End With
              With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.Primary, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", ,
                                             Singular.Web.PostBackType.None, "PaymentRunPage.findPaymentRun(false)")
              End With
              With .Helpers.Bootstrap.Button(, "New", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", ,
                                             PostBackType.None, "PaymentRunPage.newPaymentRun()")
              End With
              With .Helpers.With(Of PaymentRun)(Function(d) ViewModel.CurrentPaymentRun)
                With .Helpers.Bootstrap.Button(, "Copy Previous Payment Run", BootstrapEnums.Style.Warning, , BootstrapEnums.ButtonSize.Medium, , "fa-copy", ,
                                               PostBackType.None, "PaymentRunPage.findPaymentRun(true)")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "PaymentRunBO.CanView('CopyPaymentRunButton', $data)")
                End With
                With .Helpers.Bootstrap.Button(, "Generate Invoices", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-gears", ,
                                               PostBackType.None, "PaymentRunPage.generatePaymentRunInvoices($data)")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "PaymentRunBO.CanView('GenerateInvoicesButton', $data)")
                End With
                With .Helpers.Bootstrap.Button("Print", "Print", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-print", ,
                               Singular.Web.PostBackType.Full, True)
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Toolbar
                .Helpers.MessageHolder()
              End With
            End With
          End With
        End With
      End With
      '----------------------------------------------------------------------------------------------------------------------Toolbar
      
      'Content----------------------------------------------------------------------------------------------------------------------
      With Helpers.With(Of PaymentRun)(Function(d) ViewModel.CurrentPaymentRun)
          
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 2)
          '.AddClass("col-no-left-padding")
          With .Helpers.Bootstrap.FlatBlock("Summary", False, False, )
            .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, "PaymentRunPage.currentPaymentRunHeading($data)")
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.System)
                    .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.System, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.MonthYearString)
                    .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.MonthYearString, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.StartDate)
                    .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.EndDate)
                    .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 4, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.PaymentRunStatusID)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.PaymentRunStatusID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "PaymentRunBO.CanEdit('PaymentRunStatusID', $data)")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 4, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.TotalInvoices)
                    .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.TotalInvoices, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 4, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.TotalAmount)
                    .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.TotalAmount, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
        End With
        
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 10)
          '.AddClass("col-no-right-padding")
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Top)
            With .AddTab("Alerts", "fa-list-ol", "PaymentRunPage.alerts()", "Alerts", , False)
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                    With .Helpers.FieldSet("Alerts")
                      .Helpers.Control(New NewOBWeb.Controls.ROPaymentRunAlertList(Of NewOBWeb.PaymentRunNewVM)())
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 6, 8, 8)
                    With .Helpers.FieldSet("Alert Details")
                      With .Helpers.With(Of OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroup)("PaymentRunPage.currentAlertGroup()")
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroupDetail)("PaymentRunPage.currentAlertGroup().ROPaymentRunAlertGroupDetailList()",
                                                                                                                     False, False, False, False, True, True, True)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                            With .FirstRow
                              .AddClass("items selectable")
                              .AddReadOnlyColumn(Function(d As OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroupDetail) d.DetailTitle)
                              .AddReadOnlyColumn(Function(d As OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroupDetail) d.DetailDescription)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("loading-custom")
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsFetchingAlerts()")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
                End With
              End With
            End With
            With .AddTab("InvoicePreview", "fa-eye", "PaymentRunPage.previewInvoices()", "Invoice Preview", , False)
              With .TabPane
                With .Helpers.Bootstrap.Row
                  '.AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.OBCityInvoicesVisible()")
                  .Helpers.Control(New NewOBWeb.Controls.ROInvoicePreviewGrid(Of NewOBWeb.PaymentRunNewVM)())
                End With
              End With
            End With
            With .AddTab("Invoices", "fa-cc-mastercard", "PaymentRunPage.actualInvoices()", "Invoices", , False)
              With .TabPane
                'With .Helpers.With(Of PaymentRun)(Function(d) ViewModel.CurrentPaymentRun)
                '  With .Helpers.Bootstrap.Row
                '    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      
                '    End With
                '  End With
                'End With
                With .Helpers.Bootstrap.Row
                  '.AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.ActualInvoicesVisible()")
                  '($data)
                  .Helpers.Control(New NewOBWeb.Controls.ROCreditorInvoiceList(Of NewOBWeb.PaymentRunNewVM)("PaymentRunPage.editROCreditorInvoice($data)", "PaymentRunPage.onROInvoiceSelected($data)", "PaymentRunPage.delayedRefresh"))
                End With
              End With
            End With
          End With
        End With
        
        'With .Helpers.Bootstrap.Row
        '  'With .Helpers.Bootstrap.StateButton("ViewModel.OBCityInvoicesVisible", "Invoice Preview", "Invoice Preview", , , , , "btn-sm")
        '  '  .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "PaymentRunPage.previewInvoices()")
        '  'End With
        '  'With .Helpers.Bootstrap.StateButton("ViewModel.ActualInvoicesVisible", "Invoices", "Invoices", , , , , "btn-sm")
        '  '  .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "PaymentRunPage.actualInvoices()")
        '  'End With
        'End With
        
      End With
      '----------------------------------------------------------------------------------------------------------------------Content
      
      'Dialogs----------------------------------------------------------------------------------------------------------------------
      h.Control(New NewOBWeb.Controls.ROPaymentRunSelector(Of NewOBWeb.PaymentRunNewVM)("SelectPaymentRun",
                                                                                        "PaymentRunPage.onPaymentRunSelected($data)"))
      
      h.Control(New NewOBWeb.Controls.NewPaymentRunModal(Of NewOBWeb.PaymentRunNewVM)("ViewModel.CurrentPaymentRun()",
                                                                                      "NewPaymentRun",
                                                                                      "PaymentRunPage.cancelNewPaymentRun()",
                                                                                      "PaymentRunPage.saveNewPaymentRun()"))
      
      h.Control(New NewOBWeb.Controls.EditCreditorInvoice(Of NewOBWeb.PaymentRunNewVM)("",
                                                                                       "PaymentRunPage.previousInvoice($data)",
                                                                                       "PaymentRunPage.nextInvoice($data)",
                                                                                       "PaymentRunPage.saveAndNextInvoice($data)",
                                                                                       "PaymentRunPage.saveNewInvoice()",
                                                                                       "PaymentRunPage.saveAndNewInvoice()",
                                                                                       "EditCreditorInvoice",
                                                                                       "ViewModel.CurrentInvoice()"))
      '----------------------------------------------------------------------------------------------------------------------Dialogs
      
    End Using%>
  <script type="text/javascript" src="../Scripts/Pages/PaymentRunPage.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Invoicing.js"></script>
</asp:Content>

<%--
'With .Helpers.Bootstrap.FlatBlock(, True)
      '  .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, "PageHeaderDetails()")
      '  With .AboveContentTag
      '    With .Helpers.Bootstrap.Row
      '      .Style.Padding("20px", "0px", "15px", "0px")
      '      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '          With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROCreditorInvoiceListCriteria.Keyword, Singular.Web.BootstrapEnums.InputSize.Small)
      '            .Editor.Attributes("placeholder") = "Search Invoices by Keyword"
      '            .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '            .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: PaymentRunBO.Methods.RefreshCreditorInvoices() }")
      '          End With
      '        End With
      '      End With
      '      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '          If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
      '            With .Helpers.Bootstrap.Button(, "Generate Invoices", BootstrapEnums.Style.Warning, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.GenerateInvoices()")
      '            End With
      '            With .Helpers.Bootstrap.Button(, "Bulk Edit", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.BulkEditInvoiceDetails()")
      '            End With
      '          ElseIf Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then
      '            With .Helpers.Bootstrap.Button(, "New Invoice", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.NewInvoice()")
      '            End With
      '          End If
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .ContentTag
      '    With .Helpers.Bootstrap.Row
      '      'Production Services
      '      'If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
      '      '  With .Helpers.Bootstrap.PagedGridFor(Of ROCreditorInvoice)(Function(vm) ViewModel.ROCreditorInvoiceListPagingManager,
      '      '                                           Function(vm) ViewModel.ROCreditorInvoiceList,
      '      '                                           False, False, True, False, True, True, True, ,
      '      '                                           Singular.Web.BootstrapEnums.PagerPosition.Bottom)
      '      '    .AddClass("no-border hover list")
      '      '    .TableBodyClass = "no-border-y"
      '      '    .Pager.PagerListTag.ListTag.AddClass("pull-right")
      '      '    With .FirstRow
      '      '      .AddClass("items selectable")
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.SystemInvoiceNum)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.SupplierInvoiceNum)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.InvoiceDate)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.HumanResource)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.HumanResourceIDNo)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.StaffNo)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TransactionType)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.InvoiceDate)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.RefNo)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalTxAndTravel)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalSnT)
      '      '    End With
      '      '  End With
                  
      '      '  'Production Content
      '      'Else
      '      'If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then
      '      '  With .Helpers.Bootstrap.PagedGridFor(Of ROCreditorInvoice)(Function(vm) ViewModel.ROCreditorInvoiceListPagingManager,
      '      '                         Function(vm) ViewModel.ROCreditorInvoiceList,
      '      '                         False, False, True, False, True, True, True, ,
      '      '                         Singular.Web.BootstrapEnums.PagerPosition.Bottom,
      '      '                         "PaymentRunPage.EditCreditorInvoice($data)", False)
      '      '    .AddClass("no-border hover list")
      '      '    .TableBodyClass = "no-border-y no-border-x no-border-x-top"
      '      '    .Pager.PagerListTag.ListTag.AddClass("pull-left")
      '      '    With .FirstRow
      '      '      .AddClass("items selectable")
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.SystemInvoiceNum)
      '      '      .AddReadOnlyColumn(Function(c As ROCreditorInvoice) c.SupplierInvoiceNum)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.HumanResource)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.HumanResourceIDNo)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.StaffNo)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.InvoiceDate)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TransactionType)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalAmountExclVAT)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalVATAmount)
      '      '      .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalAmount)
      '      '    End With
      '      '  End With
      '      'End If
      '    End With
      '  End With
      'End With
      
      'With h.Bootstrap.Dialog("EditCreditorInvoice", "Edit Invoice", False, "modal-md", Singular.Web.BootstrapEnums.Style.Info)
      '  With .Body
      '    With .Helpers.With(Of CreditorInvoice)(Function(c) ViewModel.CurrentCreditorInvoice)
      '      With .Helpers.Bootstrap.Row
      '        With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
      '          With .Helpers.FieldSet("Invoice Info")
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Div
      '                .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As CreditorInvoice) c.IsNew)
      '                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As CreditorInvoice) d.HumanResource, "PaymentRunBO.FindHumanResource($element)",
      '                              "Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
      '                  '.AddOnButton.Button.Button.AddBinding(KnockoutBindingString.enable, Function(d As CreditorInvoice) False)
      '                End With
      '              End With
      '              With .Helpers.Div
      '                .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As CreditorInvoice) Not c.IsNew)
      '                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.HumanResource, BootstrapEnums.InputSize.Small)
      '                End With
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.EmployeeCode, BootstrapEnums.InputSize.Small)
      '                .Editor.AddBinding(KnockoutBindingString.enable, Function(d As CreditorInvoice) False)
      '                .Editor.Attributes("placeholder") = "Staff No."
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.InvoiceDate, BootstrapEnums.InputSize.Small)
      '                .Editor.Attributes("placeholder") = "Invoice Date"
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.RefNo, BootstrapEnums.InputSize.Small)
      '                .Editor.Attributes("placeholder") = "Ref Num"
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.SystemInvoiceNum, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.TotalAmountExVat, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.TotalVat, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.Total, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.StateButton(Function(d As CreditorInvoice) d.CompletedInd,
      '                                                  "Completed", "Not Completed", , , , , "btn-sm")
      '                .Button.AddClass("btn-block")
      '              End With
      '            End With
      '          End With
      '        End With
      '        With .Helpers.Bootstrap.Column(12, 12, 6, 9, 10)
      '          With .Helpers.FieldSet("Invoice Details")
      '            With .Helpers.Bootstrap.TableFor(Of CreditorInvoiceDetail)(Function(d) d.CreditorInvoiceDetailList,
      '                                                                       False, True, False, False, True, True, True)
      '              .AddClass("no-border hover list")
      '              .TableBodyClass = "no-border-y no-border-x no-border-x-top"
      '              With .FirstRow
      '                .AddClass("items selectable")
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailTypeID)
      '                .AddColumn(Function(c As CreditorInvoiceDetail) c.CreditorInvoiceDetailDate)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.SupplierInvoiceDetailNum)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.DetailDescription)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.AccountID)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.CostCentreID)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.VatTypeID)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.AmountExclVAT)
      '                .AddColumn(Function(d As CreditorInvoiceDetail) d.VATAmount)
      '                .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.Amount)
      '              End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  '.AddClass("row")
      '  'With .Helpers.Bootstrap.Row
      '  '  With .Helpers.Bootstrap.Column(12, 6, 4, 4)
      '  '    With .Helpers.Div
      '  '      '.Style.FloatRight()
      '  '      .AddBinding(Singular.Web.KnockoutBindingString.html, "Test($data)")
      '  '      .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsValid()")
      '  '      .AddClass("ValidationPopup Msg-Validation")
      '  '      .Style("position") = "relative"
      '  '      .Style("height") = "auto"
      '  '      .Style("visibility") = "visible"
      '  '      .AddClass("HoverMsg")
      '  '    End With
      '  '  End With
      '  'End With
      '  With .Footer
      '    With .Helpers.DivC("pull-right")
      '      ', "Save", "btn-sm btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.None, False, , , "PaymentRunBO.Methods.SaveCreditorInvoiceDetail()"
      '      With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-disk", , Singular.Web.PostBackType.None, "PaymentRunPage.SaveCreditorInvoice()")
      '        '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "PaymentRunBO.CreditorInvoiceIsValid()")
      '      End With
      '    End With
      '  End With
      'End With
      
      'With h.Div
      '  .Attributes("id") = "EditCreditorInvoice"
      '  .AddClass("modal-dg")
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Invoice", False, False)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
                
      '          'With .Helpers.Bootstrap.Column(12, 12, 8, 11)
      '          '  'With .Helpers.Bootstrap.FlatDreamAlert("No Errors Found", BootstrapEnums.FlatDreamAlertColor.Success, "fa-check", "Data Valid!", False)
      '          '  '  .AlertTag.AddBinding(KnockoutBindingString.visible, Function(c) c.IsValid)
      '          '  'End With
      '          '  With .Helpers.Toolbar
      '          '    .Style.FloatRight()
      '          '    With .Helpers.MessageHolder
      '          '      .AddClass("HoverMsg")
      '          '    End With
      '          '  End With
      '          'End With
      '        End With
      '        With .Helpers.Bootstrap.Row
      '          .Style.Padding("20px", "0px", "0px", "20px")
                
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
      
      'With h.Div
      '  .Attributes("id") = "RateOverride"
      '  .AddClass("modal-dg")
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Rate Override", False, True)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 4, 1)
      '            With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.SetDisplayRate()")
      '              .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock(, False, True)
      '      With .ContentTag
      '        'With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '        With .Helpers.With(Of CreditorInvoiceDetail)("PaymentRunBO.SelectedCreditorInvoiceDetail()")
      '          With .Helpers.Bootstrap.Row
      '            .Attributes("id") = "DetailRow"
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              .Helpers.LabelFor(Function(d) d.RateOverride)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d) d.RateOverride, BootstrapEnums.InputSize.Small)
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              .Helpers.LabelFor(Function(d) d.RateOverrideReason)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d) d.RateOverrideReason, BootstrapEnums.InputSize.Small)
      '              End With
      '            End With
      '          End With
      '          'End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
      
      'With h.Div
      '  .Attributes("id") = "AmountOverride"
      '  .AddClass("modal-dg")
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Rate Override", False, True)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 4, 1)
      '            With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.SetDisplayAmount()")
      '              .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Amount Override", False, True)
      '      With .ContentTag
      '        'With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '        With .Helpers.With(Of CreditorInvoiceDetail)("PaymentRunBO.SelectedCreditorInvoiceDetail()")
      '          With .Helpers.Bootstrap.Row
      '            .Attributes("id") = "DetailRow"
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              .Helpers.LabelFor(Function(d) d.AmountOverride)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d) d.AmountOverride, BootstrapEnums.InputSize.Small)
      '              End With
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              .Helpers.LabelFor(Function(d) d.AmountOverrideReason)
      '              With .Helpers.Bootstrap.FormControlFor(Function(d) d.AmountOverrideReason, BootstrapEnums.InputSize.Small)
      '              End With
      '            End With
      '          End With
      '          'End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
      
      'With h.Div
      '  .Attributes("id") = "BulkCreditorInvoiceDetail"
      '  .AddClass("modal-dg")
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Creditor Invoice Details", False, False)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 4, 1)
      '            With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , , , PostBackType.None, "PaymentRunBO.Methods.SaveBulkCreditorInvoiceDetails()")
      '              .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
      '            End With
      '          End With
      '        End With
      '        With .Helpers.Bootstrap.Row
      '          .Style.Padding("20px", "0px", "15px", "0px")
      '          'With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '            With .Helpers.EditorFor(Function(d) ViewModel.BulkCreditorInvoiceDetailListCriteria.Keyword)
      '              .AddClass("form-control input-sm")
      '              .Attributes("placeholder") = "Search by Keyword"
      '              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '              .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: PaymentRunBO.Methods.RefreshBulkCreditorInvoiceDetailList() }")
      '            End With
      '          End With
      '          'End With
      '        End With
      '        With .Helpers.Bootstrap.Row
      '          .Style.Padding("0px", "0px", "5px", "0px")
      '          With .Helpers.Bootstrap.Column(12, 12, 1, 1)
      '            .Helpers.Bootstrap.Button(, "Rate Override", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.ExtraSmall, , , , , "PaymentRunBO.Methods.BulkRateOverride()")
      '          End With
      '          With .Helpers.Bootstrap.Column(12, 12, 1, 1)
      '            .Helpers.Bootstrap.Button(, "Amount Override", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.ExtraSmall, , , , , "PaymentRunBO.Methods.BulkAmountOverride()")
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("", False, False)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 8, 8)
      '            With .Helpers.Bootstrap.PagedGridFor(Of BulkCreditorInvoiceDetail)(Function(vm) ViewModel.BulkCreditorInvoiceDetailListPagingManager,
      '                                                                               Function(vm) ViewModel.BulkCreditorInvoiceDetailList,
      '                                                                               False, False, True, False, True, True, False, ,
      '                                                                               Singular.Web.BootstrapEnums.PagerPosition.Bottom)
      '              .AddClass("no-border hover list")
      '              .TableBodyClass = "no-border-y"
      '              .Pager.PagerListTag.ListTag.AddClass("pull-left")
              
      '              With .FirstRow
      '                .AddClass("items")
      '                With .AddColumn("")
      '                  With .Helpers.BootstrapStateButton("", "PaymentRunBO.Methods.SelectedInd($data)", "PaymentRunBO.Methods.SelectedIndCss($data)", "PaymentRunBO.Methods.SelectedIndHtml($data)", False)
      '                    With .Button
      '                      .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As BulkCreditorInvoiceDetail) Not Singular.Misc.CompareSafe(c.CreditorInvoiceDetailTypeID, CInt(OBLib.CommonData.Enums.CreditorInvoiceDetailType.SnT)))
      '                    End With
      '                  End With
      '                End With
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.HumanResource)
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.SystemInvoiceNum)
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.DetailDescription)
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.VATAmount)
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.AmountExclVAT)
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.DisplayRate)
      '                .AddReadOnlyColumn(Function(c As BulkCreditorInvoiceDetail) c.DisplayAmount)
      '              End With
      '            End With
      '          End With
      '          With .Helpers.Bootstrap.Column(12, 12, 4, 4)
      '            With .Helpers.DivC("table-responsive")
      '              With .Helpers.Bootstrap.TableFor(Of BulkCreditorInvoiceDetail)(Function(d) ViewModel.SelectedBulkCreditorInvoiceDetailList, False, False, , , , , )
      '                With .FirstRow
      '                  .AddReadOnlyColumn(Function(d As BulkCreditorInvoiceDetail) d.HumanResource)
      '                  .AddReadOnlyColumn(Function(d As BulkCreditorInvoiceDetail) d.DetailDescription)
      '                  .AddReadOnlyColumn(Function(d As BulkCreditorInvoiceDetail) d.CreditorInvoiceDetailDate)
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With

      '      End With

      '    End With
      '  End With


      'End With
      
     
      'With h.Div
      '  .Attributes("id") = "BulkRateOverride"
      '  .AddClass("modal-dg")
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Bulk Rate Override", False, True)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 4, 1)
      '            With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.ApplyRatesAndReason()")
      '              .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Bulk Rate Override", False, True)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          .Attributes("id") = "DetailRow"
      '          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '            .Helpers.LabelFor(Function(d) ViewModel.BulkRateOverride)
      '            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.BulkRateOverride, BootstrapEnums.InputSize.Small)
      '            End With
      '          End With
      '          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '            .Helpers.LabelFor(Function(d) ViewModel.BulkRateOverrideReason)
      '            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.BulkRateOverrideReason, BootstrapEnums.InputSize.Small)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
      
      
      'With h.Div
      '  .Attributes("id") = "BulkAmountOverride"
      '  .AddClass("modal-dg")
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("Bulk Amount Override", False, True)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 4, 1)
      '            With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , , , PostBackType.None, "PaymentRunBO.Methods.ApplyAmountsAndReason()")
      '              .Button.AddBinding(KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
      '    With .Helpers.Bootstrap.FlatBlock("", False, True)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          .Attributes("id") = "DetailRow"
      '          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '            .Helpers.LabelFor(Function(d) ViewModel.BulkAmountOverride)
      '            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.BulkAmountOverride, BootstrapEnums.InputSize.Small)
      '            End With
      '          End With
      '          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '            .Helpers.LabelFor(Function(d) ViewModel.BulkAmountOverrideReason)
      '            With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.BulkAmountOverrideReason, BootstrapEnums.InputSize.Small)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
            
      'With h.Div
        'With .Helpers.DivC("modal-dg")
        '  .Attributes("id") = "ROHumanResourceListModal"
        '  With .Helpers.Div
        '    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
        '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '        With .Helpers.EditorFor(Function(d) ViewModel.ROHumanResourceListCriteria.Keyword)
        '          .AddClass("form-control input-sm")
        '          .Attributes("placeholder") = "Search by Keyword"
        '          .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
        '          .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: ROHumanResources.DelayedRefreshList() }")
        '        End With
        '      End With
        '    End With
        '  End With
        '  With .Helpers.Div
        '    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourcePaged)(Function(d) ViewModel.ROHumanResourceListManager,
        '                                                                                    Function(d) ViewModel.ROHumanResourceList,
        '                                                                                    False, False, False, False, True, True, True,
        '                                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
        '                                                                                    "ROHumanResources.OnItemSelected", True)
        '      .AddClass("no-border hover list")
        '      .TableBodyClass = "no-border-y"
        '      .Pager.PagerListTag.ListTag.AddClass("pull-left")
        '      With .FirstRow
        '        .AddClass("items")
        '        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.EmployeeCode)
        '          .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
        '        End With
        '        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.IDNo)
        '          .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
        '        End With
        '        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Firstname)
        '          .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
        '        End With
        '        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.PreferredName)
        '          .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
        '        End With
        '        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Surname)
        '          .AddClass("col-xs-3 col-sm-3 col-md-2 col-lg-2")
        '        End With
        '      End With
        '    End With
        '  End With
        'End With
        
        'With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '  With .Helpers.With(Of CreditorInvoice)(Function(c) ViewModel.CurrentCreditorInvoice)
        '    With .Helpers.Bootstrap.FlatBlock("Invoice Details", False, False)
        '      With .ContentTag
        '        With .Helpers.Bootstrap.Row
        '          .Attributes("id") = "DetailRow"
        '          If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
        '            With .Helpers.DivC("table-responsive")
        '              With .Helpers.Bootstrap.TableFor(Of CreditorInvoiceDetail)(Function(d) d.CreditorInvoiceDetailList, False, False, , , , , )
        '                With .FirstRow
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailType)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailDate)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.DetailDescription)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.Discipline)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.CostCentre)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.Account)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.VatType)
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.HoursForDay, "Hrs for Day")
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.OvertimeHours, "OT Hrs")
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.AmountExclVAT, "Amt Ex. VAT")
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.VATAmount, "VAT Amt")
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.DisplayRate)
        '                  'Rate Override
        '                  With .AddColumn
        '                    With .Helpers.Bootstrap.Button(, "Rate Override", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.ExtraSmall, , , , PostBackType.None, "PaymentRunBO.Methods.RateOverride($data)")
        '                      .Button.AddBinding(KnockoutBindingString.visible, Function(c As CreditorInvoiceDetail) Not Singular.Misc.CompareSafe(c.CreditorInvoiceDetailTypeID, CInt(OBLib.CommonData.Enums.CreditorInvoiceDetailType.SnT)))
        '                    End With
        '                  End With
        '                  .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.DisplayAmount)
        '                  'Amount Override
        '                  With .AddColumn
        '                    With .Helpers.Bootstrap.Button(, "Amount Override", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.ExtraSmall, , , , PostBackType.None, "PaymentRunBO.Methods.AmountOverride($data)")
        '                      .Button.AddBinding(KnockoutBindingString.visible, Function(c As CreditorInvoiceDetail) Not Singular.Misc.CompareSafe(c.CreditorInvoiceDetailTypeID, CInt(OBLib.CommonData.Enums.CreditorInvoiceDetailType.SnT)))
        '                    End With
        '                  End With
        '                End With
        '              End With
        '            End With
                    
        '          ElseIf Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then
        '            With .Helpers.DivC("table-responsive")
        '              With .Helpers.Bootstrap.TableFor(Of CreditorInvoiceDetail)(Function(d) d.CreditorInvoiceDetailList, True, True, , , , , )
        '                With .FirstRow
        '                  With .AddColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailTypeID)
        '                    .Editor.AddBinding(KnockoutBindingString.enable, Function(c) False)
        '                  End With
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailDate)
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.SupplierInvoiceDetailNum)
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.DetailDescription)
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.VatTypeID)
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.AccountID)
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.CostCentreID)
        '                  .AddColumn(Function(d As CreditorInvoiceDetail) d.Amount)
        '                  With .AddColumn(Function(d As CreditorInvoiceDetail) d.AmountExclVAT)
        '                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
        '                  End With
        '                  With .AddColumn(Function(d As CreditorInvoiceDetail) d.VATAmount)
        '                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
        '                  End With
        '                End With
        '              End With
        '            End With
        '          End If
        '        End With
        '      End With
        '    End With
        '  End With
        'End With
        
        'With .Helpers.Bootstrap.Dialog("NewPaymentRun", "New Payment Run")
        '  .ModalDialogDiv.AddClass("modal-lg")
        '  With .Body
        '    .AddClass("row")
        '    With .Helpers.Bootstrap.FlatBlock(, False, True, False)
        '      With .ContentTag
        '        With .Helpers.Bootstrap.Row
        '          With .Helpers.Bootstrap.Column(12, 6, 4, 2)
        '            With .Helpers.With(Of PaymentRun)(Function(c) ViewModel.PaymentRun)
        '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '                With .Helpers.Bootstrap.FormControlFor(Function(p As PaymentRun) p.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
        '                End With
        '              End With
        '            End With
        '          End With
        '        End With
        '      End With
        '    End With
        '  End With
        '  With .Footer
        '    With .Helpers.BootstrapButton(, "Save", "btn-sm btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.None, False, , , "")
        '    End With
        '  End With
        'End With
        
        'With .Helpers.DivC("modal-dg")
          .Attributes("id") = "NewPaymentRun"
          'With .Helpers.Bootstrap.FlatBlock("Payment Run", False, False)
          '  With .ContentTag
          '    With .Helpers.Bootstrap.Button("SavePaymentRunBtn", "save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-save", , PostBackType.None, "SavePaymentRun($data)")
          '      .Button.AddBinding(KnockoutBindingString.click, "PaymentRunBO.Methods.SavePaymentRun($data)")
          '      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "PaymentRunBO.PaymentRunIsValid()")
          '    End With
          '  End With
          '  'With .ContentTag
          '  '  With .Helpers.Bootstrap.Row
          '  '    With .Helpers.Bootstrap.Column(12, 6, 4, 4)
          '  '      With .Helpers.Div
          '  '        '.Style.FloatRight()
          '  '        .AddBinding(Singular.Web.KnockoutBindingString.html, "Test($data)")
          '  '        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsValid()")
          '  '        .AddClass("ValidationPopup Msg-Validation")
          '  '        .Style("position") = "relative"
          '  '        .Style("height") = "auto"
          '  '        .Style("visibility") = "visible"
          '  '        .AddClass("HoverMsg")
          '  '      End With
          '  '    End With
          '  '  End With
          '  'End With
          '  'With .ContentTag
          '  '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '  '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!IsBudgetedProductionValid()")
          '  '    .AddBinding(Singular.Web.KnockoutBindingString.html, "GetPaymentRunBrokenRulesHTML()")
          '  '  End With
          '  'End With
          'End With
          'With .Helpers.Bootstrap.FlatBlock(, False, True, False)
          '  With .ContentTag
          '    With .Helpers.With(Of PaymentRun)(Function(c) ViewModel.PaymentRun)
          '      With .Helpers.Bootstrap.Row
          '        With .Helpers.Bootstrap.Column(12, 6, 4, 4)
          '          With .Helpers.Div
          '            '.Style.FloatRight()
          '            .AddBinding(Singular.Web.KnockoutBindingString.html, "Test($data)")
          '            .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsValid()")
          '            .AddClass("ValidationPopup Msg-Validation")
          '            .Style("position") = "relative"
          '            .Style("height") = "auto"
          '            .Style("visibility") = "visible"
          '            .AddClass("HoverMsg")
          '          End With
          '        End With
          '      End With
          '      With .Helpers.Bootstrap.Row
          '        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
          '          With .Helpers.Bootstrap.Column(12, 4, 4, 4)
          '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '              With .Helpers.Bootstrap.FormControlFor(Function(p As PaymentRun) p.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
          '                .Editor.Attributes("placeholder") = "Start Date"
          '              End With
          '            End With
          '          End With
          '          With .Helpers.Bootstrap.Column(12, 4, 4, 4)
          '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '              With .Helpers.Bootstrap.FormControlFor(Function(p As PaymentRun) p.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
          '                .Editor.Attributes("placeholder") = "End Date"
          '              End With
          '            End With
          '          End With
          '          With .Helpers.Bootstrap.Column(12, 4, 4, 4)
          '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As PaymentRun) p.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
          '              End With
          '            End With
          '          End With
          '        End With
          '      End With
          '      With .Helpers.Bootstrap.Row
          '        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
          '          With .Helpers.Bootstrap.Column(12, 4, 4, 4)
          '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '              With .Helpers.Bootstrap.FormControlFor(Function(p As PaymentRun) p.Month, Singular.Web.BootstrapEnums.InputSize.Small)
          '                .Editor.Attributes("placeholder") = "Month"
          '              End With
          '            End With
          '          End With
          '          With .Helpers.Bootstrap.Column(12, 4, 4, 4)
          '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '              With .Helpers.Bootstrap.FormControlFor(Function(p As PaymentRun) p.Year, Singular.Web.BootstrapEnums.InputSize.Small)
          '                .Editor.Attributes("placeholder") = "Year"
          '              End With
          '            End With
          '          End With
          '          With .Helpers.Bootstrap.Column(12, 4, 4, 4)
          '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '              With .Helpers.Bootstrap.FormControlFor(Function(p As PaymentRun) p.PaymentRunStatusID, Singular.Web.BootstrapEnums.InputSize.Small)
          '                .Editor.Attributes("placeholder") = "Status"
          '              End With
          '            End With
          '          End With
          '        End With
          '      End With
          '    End With
          '  End With
          'End With
        'End With

      'End With--%>
