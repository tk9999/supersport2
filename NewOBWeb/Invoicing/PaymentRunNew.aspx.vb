﻿Imports OBLib.Invoicing.New
Imports Singular
Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports Singular.Misc
Imports System.ComponentModel
Imports OBLib.HR.ReadOnly
Imports Singular.Reporting
Imports Singular.Web.Data
Imports Singular.Web
Imports System

Public Class PaymentRunNew
  Inherits OBPageBase(Of PaymentRunNewVM)

End Class

Public Class PaymentRunNewVM
  Inherits OBViewModel(Of PaymentRunNewVM)
  Implements ControlInterfaces(Of PaymentRunNewVM).IROCreditorInvoices
  Implements ControlInterfaces(Of PaymentRunNewVM).ICurrentInvoice
  Implements ControlInterfaces(Of PaymentRunNewVM).IROInvoicePreview

  'Public Interface IROProductionServicesInvoices
  '  Property ROOBCityHRInvoiceList As OBLib.Invoicing.ReadOnly.ROOBCityHRInvoiceList
  '  Property ROOBCityHRInvoiceListCriteria As OBLib.Invoicing.ReadOnly.ROOBCityHRInvoiceList.Criteria
  '  Property ROOBCityHRInvoiceListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  'End Interface

#Region " Properties "

  Public Property SelectedROPaymentRun As OBLib.Invoicing.ReadOnly.ROPaymentRun
  Public Property CurrentPaymentRun As OBLib.Invoicing.New.PaymentRun
  ' Public Property PreviousInvoice As OBLib.Invoicing.[New].CreditorInvoice Implements ControlInterfaces(Of PaymentRunNewVM).ICurrentInvoice.PreviousInvoice
  Public Property CurrentInvoice As OBLib.Invoicing.[New].CreditorInvoice Implements ControlInterfaces(Of PaymentRunNewVM).ICurrentInvoice.CurrentInvoice
  'Public Property NextInvoice As OBLib.Invoicing.[New].CreditorInvoice Implements ControlInterfaces(Of PaymentRunNewVM).ICurrentInvoice.NextInvoice
  Public Property CurrentInvoiceRowNum As Integer Implements ControlInterfaces(Of PaymentRunNewVM).ICurrentInvoice.CurrentInvoiceRowNum
  Public Property CreditorInvoiceList As OBLib.Invoicing.[New].CreditorInvoiceList
  Public Property IsFetchingInvoice As Boolean
  Private mFromPaymentRunID As Integer?
  Private mToPaymentRunID As Integer?

  'Public Property OBCityInvoicesVisible As Boolean
  'Public Property ActualInvoicesVisible As Boolean

#Region " Paging Lists "

  <InitialDataOnly>
  Public Property ROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourcePagedList = New OBLib.HR.ReadOnly.ROHumanResourcePagedList
  <InitialDataOnly>
  Public Property ROHumanResourceListCriteria As OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria = New OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria
  <InitialDataOnly>
  Public Property ROHumanResourceListManager As Singular.Web.Data.PagedDataManager(Of HumanResourceVM) = New Singular.Web.Data.PagedDataManager(Of HumanResourceVM)(Function(d) Me.ROHumanResourceList,
                                                                                                                                                      Function(d) Me.ROHumanResourceListCriteria,
                                                                                                                                                      "FirstName", 15)
  <InitialDataOnly>
  Public Property ROPaymentRunList As OBLib.Invoicing.ReadOnly.ROPaymentRunList = New OBLib.Invoicing.ReadOnly.ROPaymentRunList
  <InitialDataOnly>
  Public Property ROPaymentRunListCriteria As OBLib.Invoicing.ReadOnly.ROPaymentRunList.Criteria = New OBLib.Invoicing.ReadOnly.ROPaymentRunList.Criteria
  <InitialDataOnly>
  Public Property ROPaymentRunListManager As Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM) = New Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM)(Function(d) Me.ROPaymentRunList,
                                                                                                                                                                       Function(d) Me.ROPaymentRunListCriteria,
                                                                                                                                                                       "StartDate", 10, False)
  <InitialDataOnly>
  Public Property ROCreditorInvoiceList As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList Implements ControlInterfaces(Of PaymentRunNewVM).IROCreditorInvoices.ROCreditorInvoiceList
  <InitialDataOnly>
  Public Property ROCreditorInvoiceListCriteria As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria Implements ControlInterfaces(Of PaymentRunNewVM).IROCreditorInvoices.ROCreditorInvoiceListCriteria
  <InitialDataOnly>
  Public Property ROCreditorInvoiceListManager As Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM) Implements ControlInterfaces(Of PaymentRunNewVM).IROCreditorInvoices.ROCreditorInvoiceListManager

  <InitialDataOnly>
  Public Property ROInvoicePreviewList As OBLib.Invoicing.ReadOnly.ROInvoicePreviewList Implements ControlInterfaces(Of PaymentRunNewVM).IROInvoicePreview.ROInvoicePreviewList
  <InitialDataOnly>
  Public Property ROInvoicePreviewListCriteria As OBLib.Invoicing.ReadOnly.ROInvoicePreviewList.Criteria Implements ControlInterfaces(Of PaymentRunNewVM).IROInvoicePreview.ROInvoicePreviewListCriteria
  <InitialDataOnly>
  Public Property ROInvoicePreviewListManager As Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM) Implements ControlInterfaces(Of PaymentRunNewVM).IROInvoicePreview.ROInvoicePreviewListManager

  <InitialDataOnly>
  Public Property ROPaymentRunAlertGroupList As OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroupList = New OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroupList

  <InitialDataOnly>
  Public Property CurrentAlertGroupID As Integer?

  <InitialDataOnly>
  Public Property IsFetchingAlerts As Boolean

#End Region

  Public ReadOnly Property CurrentSystemID As Integer
    Get
      Return OBLib.Security.Settings.CurrentUser.SystemID
    End Get
  End Property

  Public ReadOnly Property CurrentProductionAreaID As Integer
    Get
      Return OBLib.Security.Settings.CurrentUser.ProductionAreaID
    End Get
  End Property

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    IsFetchingInvoice = False

    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROPaymentRunStatusList", OBLib.CommonData.Lists.ROPaymentRunStatusList, False)
    ClientDataProvider.AddDataSource("ROVATTypeList", OBLib.CommonData.Lists.ROVatTypeList, False)
    ClientDataProvider.AddDataSource("ROCreditorInvoiceDetailTypeList", OBLib.CommonData.Lists.ROCreditorInvoiceDetailTypeList, False)
    ClientDataProvider.AddDataSource("ROAccountList", OBLib.CommonData.Lists.ROAccountList, False)
    ClientDataProvider.AddDataSource("ROCostCentreList", OBLib.CommonData.Lists.ROCostCentreList, False)
    ClientDataProvider.AddDataSource("ROMonthList", OBLib.CommonData.Lists.ROMonthList, False)

    ROPaymentRunListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROPaymentRunListManager.SortAsc = False
    ROPaymentRunListManager.PageSize = 25

    'ROPaymentRunList = New OBLib.Maintenance.Invoicing.ReadOnly.ROPaymentRunList
    'ROPaymentRunListCriteria = New OBLib.Maintenance.Invoicing.ReadOnly.ROPaymentRunList.Criteria
    'ROPaymentRunListPagingManager = New Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM)(Function(d) Me.ROPaymentRunList,
    '                                                                                           Function(d) Me.ROPaymentRunListCriteria,
    '                                                                                           "StartDate", 10, False)

    ROCreditorInvoiceList = New OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList
    ROCreditorInvoiceListCriteria = New OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria
    ROCreditorInvoiceListManager = New Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM)(Function(d) Me.ROCreditorInvoiceList,
                                                                                              Function(d) Me.ROCreditorInvoiceListCriteria,
                                                                                              "HumanResource", 25)

    ROInvoicePreviewList = New OBLib.Invoicing.ReadOnly.ROInvoicePreviewList
    ROInvoicePreviewListCriteria = New OBLib.Invoicing.ReadOnly.ROInvoicePreviewList.Criteria
    ROInvoicePreviewListManager = New Singular.Web.Data.PagedDataManager(Of PaymentRunNewVM)(Function(d) Me.ROInvoicePreviewList,
                                                                                              Function(d) Me.ROInvoicePreviewListCriteria,
                                                                                              "ParticipantName", 25)

    CurrentInvoice = Nothing
    CurrentInvoiceRowNum = 0

    ClientDataProvider.AddDataSource("ROHumanResourceList", OBLib.CommonData.Lists.ROHumanResourceList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "SavePaymentRun"
        SavePaymentRun(CommandArgs)

      Case "EditPaymentRun"
        EditPaymentRun(CommandArgs)

      Case "EditROCreditorInvoice"
        EditROCreditorInvoice(CommandArgs)

      Case "PreviousInvoice"
        PreviousInvoice(CommandArgs)

      Case "NextInvoice"
        NextInvoice(CommandArgs)

      Case "SaveAndNextInvoice"
        SaveAndNextInvoice(CommandArgs)

      Case "SaveNewInvoice"
        Dim sh As Singular.SaveHelper = SaveInvoice(CurrentInvoice)
        If sh.Success Then
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        Else
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If

      Case "SaveAndNewInvoice"
        Dim sh As Singular.SaveHelper = SaveInvoice(CurrentInvoice)
        If sh.Success Then
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        Else
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If

      Case "SaveCurrentInvoice"
        Dim sh As Singular.SaveHelper = SaveInvoice(CurrentInvoice)

      Case "CopyPaymentRun"
        CopyPaymentRun(CommandArgs)

      Case "Print"
        PrintPaymentRun(CommandArgs)

      Case "GenerateInvoices"
        GenerateInvoices(CommandArgs)

      Case "Save"
        Dim prl As New PaymentRunList
        prl.Add(CurrentPaymentRun)
        Dim shr As Singular.SaveHelper = TrySave(prl)
        If Not shr.Success Then
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = shr.ErrorText}
        Else
          CurrentPaymentRun = CType(shr.SavedObject, PaymentRunList).FirstOrDefault
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        End If

        'Case "GenerateInvoices"
        '  If SelectedROPaymentRunID IsNot Nothing Then
        '    Dim cmd As New Singular.CommandProc("CmdProcs.cmdGenerateOBCityCreditorInvoices",
        '               New String() {"@PaymentRunID",
        '                             "@SystemID",
        '                             "@ProductionAreaID",
        '                             "@UserID"
        '                            },
        '               New Object() {SelectedROPaymentRunID,
        '                             OBLib.Security.Settings.CurrentUser.SystemID,
        '                             OBLib.Security.Settings.CurrentUser.ProductionAreaID,
        '                             OBLib.Security.Settings.CurrentUser.UserID
        '                            })
        '    cmd.FetchType = CommandProc.FetchTypes.DataSet
        '    cmd.CommandTimeout = 0
        '    cmd = cmd.Execute()
        '  End If

        'Case "NewInvoice"
        '  CurrentCreditorInvoice = New OBLib.Invoicing.[New].CreditorInvoice
        '  CurrentCreditorInvoice.CheckAllRules()

        'Case "ClearCreditorInvoice"
        '  CurrentCreditorInvoice = Nothing

        'Case "SaveCreditorInvoiceDetail"
        '  If SelectedROPaymentRunID IsNot Nothing Then
        '    CurrentCreditorInvoice.PaymentRunID = SelectedROPaymentRunID
        '    Dim cil As New CreditorInvoiceList
        '    cil.Add(CurrentCreditorInvoice)
        '    Dim shr As Singular.SaveHelper = TrySave(cil)
        '    If Not shr.Success Then
        '      AddMessage(Web.MessageType.Error, "Error During Save", shr.ErrorText)
        '    Else
        '      CurrentCreditorInvoice = CType(shr.SavedObject, CreditorInvoiceList).FirstOrDefault
        '    End If

        '  Else
        '    AddMessage(Web.MessageType.Error, "Error During Save", "No Payment Run Selected")

        '  End If

        'Case "SaveBulkCreditorInvoiceDetails"
        '  Dim shr As Singular.SaveHelper = TrySave(SelectedBulkCreditorInvoiceDetailList)
        '  If Not shr.Success Then
        '    AddMessage(Web.MessageType.Error, "Error During Save", shr.ErrorText)
        '  Else
        '    'CurrentCreditorInvoice = CType(shr.SavedObject, CreditorInvoiceList).FirstOrDefault
        '  End If
        '  SelectedBulkCreditorInvoiceDetailList = New OBLib.Invoicing.[New].BulkCreditorInvoiceDetailList

        'Case "NewPaymentRun"
        '  PaymentRun = New OBLib.Invoicing.[New].PaymentRun

        'Case "ClearPaymentRun"
        '  PaymentRun = Nothing

        'Case "SetROPaymentRun"
        '  ROPaymentRunListCriteria.PaymentRunID = PaymentRun.PaymentRunID

    End Select

  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

    'With AddWebRule(BulkRateOverrideReasonProperty)
    '  .JavascriptRuleCode = "if(CtlError.Object.BulkRateOverride() && CtlError.Object.BulkRateOverrideReason().trim() == '') { CtlError.AddError('Rate Override Reason Required'); }"
    '  .AddTriggerProperty(BulkRateOverrideProperty)
    'End With

    'With AddWebRule(BulkAmountOverrideReasonProperty)
    '  .JavascriptRuleCode = "if(CtlError.Object.BulkAmountOverride() && CtlError.Object.BulkAmountOverrideReason().trim() == '') { CtlError.AddError('Amount Override Reason Required'); }"
    '  .AddTriggerProperty(BulkAmountOverrideProperty)
    'End With

  End Sub

#End Region

#Region " Methods "

  Private Function SaveInvoice(ByRef Invoice As CreditorInvoice) As Singular.SaveHelper
    CreditorInvoiceList = New CreditorInvoiceList()
    CreditorInvoiceList.Add(Invoice)
    Dim sh As Singular.SaveHelper = TrySave(CreditorInvoiceList)
    If sh.Success Then
      CreditorInvoiceList = sh.SavedObject
      Invoice = CreditorInvoiceList(0)
    End If
    Return sh
  End Function

  Private Sub CopyInvoiceMonth(progress As Singular.Web.AjaxProgress)

    Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdInvoicingCopyPaymentRun]",
                                        New String() {
                                                     "@PaymentRunIDFrom",
                                                     "@PaymentRunIDTo",
                                                     "@CurrentUserID"
                                                     },
                                        New Object() {
                                                     mFromPaymentRunID,
                                                     mToPaymentRunID,
                                                     OBLib.Security.Settings.CurrentUserID
                                                     })
    cmd.FetchType = CommandProc.FetchTypes.None
    cmd.CommandTimeout = 0
    cmd = cmd.Execute()
    CurrentPaymentRun = OBLib.Invoicing.[New].PaymentRunList.GetPaymentRunList(mToPaymentRunID, True).FirstOrDefault

  End Sub

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteCreditorInvoices(selectedItems As List(Of SelectedItem)) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim idsXML As String = OBLib.OBMisc.NullableIntegerListToXML(selectedItems.Select(Function(d) d.ID).ToList())
      Dim prdl As OBLib.Invoicing.[New].CreditorInvoiceList = OBLib.Invoicing.[New].CreditorInvoiceList.GetCreditorInvoiceListXML(idsXML)
      prdl.Clear()
      Dim sh As Singular.SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        wr = New Singular.Web.Result(True)
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

#End Region

  Private Sub SavePaymentRun(CommandArgs As Singular.Web.CommandArgs)
    Dim prl As New PaymentRunList
    prl.Add(CurrentPaymentRun)
    Dim shr As Singular.SaveHelper = TrySave(prl)
    If Not shr.Success Then
      CommandArgs.ReturnData = New Singular.Web.Result With {.ErrorText = shr.ErrorText}
    Else
      CurrentPaymentRun = CType(shr.SavedObject, PaymentRunList).FirstOrDefault
      SelectedROPaymentRun = OBLib.Invoicing.ReadOnly.ROPaymentRunList.GetROPaymentRunList(CurrentPaymentRun.PaymentRunID).FirstOrDefault
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    End If
  End Sub

  Private Sub EditPaymentRun(CommandArgs As Singular.Web.CommandArgs)
    CurrentPaymentRun = OBLib.Invoicing.[New].PaymentRunList.GetPaymentRunList(CommandArgs.ClientArgs.PaymentRunID, True).FirstOrDefault
  End Sub

  Private Sub EditROCreditorInvoice(CommandArgs As Singular.Web.CommandArgs)
    CurrentInvoice = OBLib.Invoicing.[New].CreditorInvoiceList.GetCreditorInvoiceList(CommandArgs.ClientArgs.CreditorInvoiceID).FirstOrDefault
    CurrentInvoiceRowNum = CommandArgs.ClientArgs.RowNum
  End Sub

  Private Sub PreviousInvoice(CommandArgs As Singular.Web.CommandArgs)
    Dim NextInvoiceID As Integer? = CommandArgs.ClientArgs.NextInvoiceID
    CurrentInvoice = OBLib.Invoicing.[New].CreditorInvoiceList.GetCreditorInvoiceList(NextInvoiceID).FirstOrDefault
  End Sub

  Private Sub NextInvoice(CommandArgs As Singular.Web.CommandArgs)
    Dim NextInvoiceID As Integer? = CommandArgs.ClientArgs.NextInvoiceID
    CurrentInvoice = OBLib.Invoicing.[New].CreditorInvoiceList.GetCreditorInvoiceList(NextInvoiceID).FirstOrDefault
  End Sub

  Private Sub SaveAndNextInvoice(CommandArgs As Singular.Web.CommandArgs)
    Dim sh As Singular.SaveHelper = SaveInvoice(CurrentInvoice)
    If sh.Success Then
      Dim NextInvoiceID As Integer? = CommandArgs.ClientArgs.NextInvoiceID
      CurrentInvoice = OBLib.Invoicing.[New].CreditorInvoiceList.GetCreditorInvoiceList(NextInvoiceID).FirstOrDefault
    End If
  End Sub

  Private Sub CopyPaymentRun(CommandArgs As Singular.Web.CommandArgs)
    mFromPaymentRunID = CommandArgs.ClientArgs.FromPaymentRunID
    mToPaymentRunID = CommandArgs.ClientArgs.ToPaymentRunID
    CommandArgs.StartLongAction("Copying...", AddressOf CopyInvoiceMonth)
  End Sub

  Private Sub PrintPaymentRun(CommandArgs As Singular.Web.CommandArgs)
    Dim rpt As New OBWebReports.InvoiceReports.PaymentRunCreditorInvoices With {.PaymentRunID = CurrentPaymentRun.PaymentRunID}
    Dim rfi As ReportFileInfo = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.ExcelData)
    If rfi.FileBytes IsNot Nothing Then
      SendFile(rfi.FileName, rfi.FileBytes)
    End If
  End Sub

  Private Sub GenerateInvoices(CommandArgs As Singular.Web.CommandArgs)
    If CurrentPaymentRun IsNot Nothing Then
      Try
        CurrentPaymentRun.GenerateInvoices()
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Catch ex As Exception
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    End If
  End Sub

End Class