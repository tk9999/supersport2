﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="UserProfile.aspx.vb" Inherits="NewOBWeb.UserProfile" %>

<%@ Import Namespace="OBLib.Users" %>

<%@ Import Namespace="OBLib.HR" %>

<%@ Import Namespace="OBLib.Maintenance.Invoicing.ReadOnly" %>

<%@ Import Namespace="OBLib.Timesheets.OBCity" %>

<%@ Import Namespace="OBLib.Maintenance.Timesheets.ReadOnly" %>

<%@ Import Namespace="OBLib.Users.ReadOnly" %>

<%@ Import Namespace="OBLib.HR.ReadOnly" %>

<%@ Import Namespace="NewOBWeb" %>

<%@ Import Namespace="Singular.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link type="text/css" href="../Scripts/vis4.10/vis.min.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/production-system-area-status-styles.css" rel="Stylesheet" />
  <link rel="stylesheet" type="text/css" href="../Scripts/fullcalendar/fullcalendar.min.css" />
  <link rel="stylesheet" type="text/css" href="../Scripts/fullcalendar/fullcalendar.print.css" media="print" />
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/vis4.10/vis.custom.readonly.min.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/BusinessObjects/Users.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/BusinessObjects/Timesheets.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/BusinessObjects/HumanResources.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/Tools/PagingManagers.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/Tools/ROUserBookings.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/fullcalendar/fullcalendar.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/Scripts/Pages/UserProfilePage.js")%>?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <style type="text/css">
    .fc-event {
      border-radius: 0px;
      padding: 5px;
      font-size: 1em;
      cursor: pointer;
    }

    .travel-icon {
      padding: 6px;
      font-size: 1.4em;
      float: right;
      cursor: pointer;
    }

    .travel-flight {
      background-color: #fed16c;
    }

    .travel-rental-car {
      background-color: #ea6153;
    }

    .travel-accommodation {
      background-color: #428bca;
    }

    .vis-label:hover {
      cursor: not-allowed;
    }

    .vis-label.swap:hover {
      cursor: pointer;
    }

    .table > thead > tr > th {
      vertical-align: bottom;
      border-bottom: 2px solid #ddd;
      font-weight: 700;
    }

    .tab-content {
      background: #fff;
      padding: 10px;
      border-bottom: 1px solid #E2E2E2;
      border-left: 1px solid #ECECEC;
      border-radius: 0 3px 3px;
      -webkit-border-radius: 0 3px 3px;
      border-right: 1px solid #ECECEC;
      box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
      margin-bottom: 40px;
    }

    .tab-content {
      background: #fff;
      padding: 10px;
      border-bottom: 1px solid #E2E2E2;
      border-left: 1px solid #ECECEC;
      border-radius: 0 3px 3px;
      -webkit-border-radius: 0 3px 3px;
      border-right: 1px solid #ECECEC;
      box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
      margin-bottom: 40px;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    Using h = Helpers

      If OBLib.Security.Settings.CurrentUser.HumanResourceID Is Nothing Then

      Else
        'Summary Info
        With h.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            .AddClass("animated slideInUp go")
            With .Helpers.DivC("block-flat profile-info")
              With .Helpers.Bootstrap.Row
                With .Helpers.With(Of ROUserProfile)(Function(d) ViewModel.ROUserProfile)
                  With .Helpers.Bootstrap.Column(12, 12, 2, 2, 2)
                    '----------- Avatar -------------
                    .Helpers.Control(New NewOBWeb.Controls.UserProfileAvatar)
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                    With .Helpers.DivC("personal")
                      With .Helpers.HTMLTag("h1")
                        .AddClass("name")
                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As ROUserProfile) d.DisplayName)
                      End With
                      With .Helpers.HTMLTag("p")
                        .AddClass("description")
                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As ROUserProfile) d.ProfileDescription)
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                  End With
                End With
              End With
            End With
          End With
        End With
        With h.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 8)
            .AddClass("animated slideInUp go")
            With .Helpers.Bootstrap.TabControl("tab-container", "nav-tabs", )
              'Detailed Info
              With .AddTab("Info", "", , "My Info", )
                With .TabPane
                  .Helpers.Control(New NewOBWeb.Controls.UserProfileControl())
                End With
              End With
              'Schedule
              With .AddTab("MySchedule", "fa-calendar", "UserProfilePage.CalendarTabSelected()", "My Schedule", )
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of UserProfile)(Function(d) ViewModel.ROUserProfile)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.PullRight
                            With .Helpers.Bootstrap.Button(, "Download Schedule", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-cloud-download", ,
                                                           Singular.Web.PostBackType.None, "UserProfilePage.downloadSchedule()")
                              .Button.AddClass("flash slowest")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of UserProfile)(Function(d) ViewModel.ROUserProfile)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Div
                            .Attributes("id") = "calendar"
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.DivC("loading-custom")
                      .AddBinding(KnockoutBindingString.visible, "ViewModel.FetchingSchedule()")
                      .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
                    End With
                  End With
                End With
              End With
              If ViewModel.ROUserProfile.CanViewOtherPersonnelSchedules Then
                With .AddTab("OtherSchedule", "fa-calendar", "", "Other Schedules", )
                  With .TabPane
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Div
                        .Attributes("id") = "userBookingsContainer"
                      End With
                    End With
                  End With
                End With
              End If
              'Timesheet
              With .AddTab("MyTimesheet", "fa-clock-o", , "My Timesheets", )
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      If ViewModel.ROUserProfile.IsProductionServicesHR Then
                        If OBLib.Security.Settings.CurrentUser.IsFreelancer Then
                          .Helpers.Control(New NewOBWeb.Controls.ServicesFreelancerTimesheetControl(Of UserProfileVM))
                        Else
                          .Helpers.Control(New NewOBWeb.Controls.ServicesPermFTCTimesheetControl(Of UserProfileVM)())
                        End If
                      ElseIf ViewModel.ROUserProfile.IsPlayoutOpsHR Or ViewModel.ROUserProfile.IsICRHR Then
                        .Helpers.Control(New NewOBWeb.Controls.UserProfileShiftsControl())
                      End If
                    End With
                  End With
                End With
              End With

            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 4)
            .AddClass("animated slideInUp go")
            .AddClass("side-right")
            'With .Helpers.Bootstrap.FlatBlock("Pending Leave", False, False)

            'End With
            'With .Helpers.Bootstrap.FlatBlock("Pending Authorisations", False, False)

            'End With
            'With .Helpers.Bootstrap.FlatBlock("Upcoming Events", False, False)

            'End With
            'With .Helpers.Bootstrap.FlatBlock("TBC Events", False, False)

            'End With
            With .Helpers.Bootstrap.FlatBlock("Skills", False, False)
              With .ContentTag
                With .Helpers.DivC("table-responsive")
                  With .Helpers.Bootstrap.TableFor(Of ROHRSkill)(Function(d) ViewModel.ROUserProfile.ROHRSkillList,
                                                                 False, False, False, False, True, True, True)
                    .AddClass("no-border no-border-row-top no-border-row-bottom hover list")
                    .TableBodyClass = "no-border-y no-border-x-top"
                    With .FirstRow
                      .AddReadOnlyColumn(Function(d As ROHRSkill) d.Discipline)
                      With .AddColumn("")
                        With .Helpers.Bootstrap.ROStateButton(Function(d As ROHRSkill) d.PrimaryInd, "Primary", "", , , , "fa-minus", )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As ROHRSkill) d.PrimaryInd)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.FlatBlock("Areas", False, False)
              With .ContentTag
                With .Helpers.DivC("table-responsive")
                  With .Helpers.Bootstrap.TableFor(Of ROHRArea)(Function(d) ViewModel.ROUserProfile.ROHRAreaList,
                                                                False, False, False, False, True, True, True)
                    .AddClass("no-border no-border-row-top no-border-row-bottom hover list")
                    .TableBodyClass = "no-border-y no-border-x-top"
                    With .FirstRow
                      .AddReadOnlyColumn(Function(d As ROHRArea) d.SubDept)
                      .AddReadOnlyColumn(Function(d As ROHRArea) d.Area)
                      With .AddColumn("")
                        With .Helpers.Bootstrap.ROStateButton(Function(d As ROHRArea) d.PrimaryInd, "Primary", "", , , , "fa-minus", )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As ROHRArea) d.PrimaryInd)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.FlatBlock("Swaps", False, False)
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.PagedGridFor(Of ROHumanResourceSwap)("ViewModel.ROHumanResourceSwapListManager",
                                                                                   "ViewModel.ROHumanResourceSwapList()",
                                                                                   False, False, False, False, True, True, False, ,
                                                                                   Singular.Web.BootstrapEnums.PagerPosition.Bottom, "",
                                                                                   True, True)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y no-border-x"
                        With .FirstRow
                          .AddClass("items selectable")
                          With .AddReadOnlyColumn(Function(c As ROHumanResourceSwap) c.SwapDateString)
                            .Style.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(c As ROHumanResourceSwap) c.SwapWithDateString)
                            .Style.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(c As ROHumanResourceSwap) c.SwapWithHumanResource)
                            .Style.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(c As ROHumanResourceSwap) c.Status)
                            .Style.TextAlign = Singular.Web.TextAlign.left
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

        h.Control(New NewOBWeb.Controls.ROPaymentRunSelector(Of UserProfileVM)(, "UserProfilePage.PaymentRunSelected($data)"))
        h.Control(New NewOBWeb.Controls.EditCrewTimesheetControl(Of UserProfileVM)("CurrentCrewTimesheet()", "UserProfilePage.SaveCrewTimesheet($data)"))


        With h.Bootstrap.Dialog("HumanResourceSwapModal", "Swap Request", False, "modal-xs", Singular.Web.BootstrapEnums.Style.Warning, , "fa-exchange", "fa-2x", )
          With .Body
            .AddClass("modal-background-gray")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("My Details", False, False)
                  With .ContentTag
                    With .Helpers.With(Of OBLib.HR.HumanResourceSwap)(Function(d) ViewModel.HumanResourceSwap)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c As HumanResourceSwap) c.HumanResource).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(c As HumanResourceSwap) c.HumanResource, Singular.Web.BootstrapEnums.InputSize.Small, , )
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c As HumanResourceSwap) c.ContractType).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(c As HumanResourceSwap) c.ContractType, Singular.Web.BootstrapEnums.InputSize.Small, , )
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c As HumanResourceSwap) c.SwapDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(c As HumanResourceSwap) c.SwapDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Their Details", False, False)
                  With .ContentTag
                    With .Helpers.With(Of OBLib.HR.HumanResourceSwap)(Function(d) ViewModel.HumanResourceSwap)
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c As HumanResourceSwap) c.SwapWithHumanResource).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(c As HumanResourceSwap) c.SwapWithHumanResource, Singular.Web.BootstrapEnums.InputSize.Small, , )
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c As HumanResourceSwap) c.SwapWithContractType).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(c As HumanResourceSwap) c.SwapWithContractType, Singular.Web.BootstrapEnums.InputSize.Small, , )
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c As HumanResourceSwap) c.SwapWithDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(c As HumanResourceSwap) c.SwapWithDate, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Footer
            With .Helpers.DivC("pull-right")
              With .Helpers.With(Of OBLib.HR.HumanResourceSwap)(Function(d) ViewModel.HumanResourceSwap)
                With .Helpers.Bootstrap.Button(, "Submit Swap", Singular.Web.BootstrapEnums.Style.Primary,
                                      , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                  Singular.Web.PostBackType.Ajax, "UserProfilePage.SubmitSwap()")
                  '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.HumanResourceSwap().IsValid()")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.HumanResourceSwap().IsValid()")
                End With
              End With
            End With
          End With
        End With

        With h.Bootstrap.Dialog("QueryDetailsModal", "Details Query", , "modal-sm")
          .Header.AddClass("modal-primary")
          With .Body
            .AddClass("row")
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(c) ViewModel.QueryDetails)
                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.QueryDetails, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With
          With .Footer
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Send Query ", Singular.Web.BootstrapEnums.Style.Info, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                             "fa-paper-plane", , Singular.Web.PostBackType.None, "UserProfilePage.submitDetailsQuery()")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.IsValid()")
              End With
            End With
          End With
        End With

        With h.Bootstrap.Dialog("QueryShift", "Query Shift", False, "modal-xs", BootstrapEnums.Style.Warning, , "fa-question-circle", "fa-2x", False)
          With .Body
            With .Helpers.With(Of UserProfileShift)("UserProfilePage.currentUserProfileShift()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As UserProfileShift) d.StaffDisputeReason).Style.Width = "100%"
                    .Helpers.Bootstrap.FormControlFor(Function(d As UserProfileShift) d.StaffDisputeReason, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
          With .Footer
            With .Helpers.With(Of UserProfileShift)("UserProfilePage.currentUserProfileShift()")
              With .Helpers.DivC("pull-left")

              End With
              With .Helpers.DivC("pull-right")
                .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", , PostBackType.None, "UserProfilePage.doneQuerying($data)")
              End With
            End With
          End With
        End With

      End If

    End Using
  %>
</asp:Content>
