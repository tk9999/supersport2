﻿Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

Public Class ChangePasswordVM
  Inherits OBViewModel(Of ChangePasswordVM)

  Public Shared OldPasswordProperty As Csla.PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldPassword)
  Public Shared NewPasswordProperty As Csla.PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewPassword)
  Public Shared ConfirmPasswordProperty As Csla.PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ConfirmPassword)

  <Required(ErrorMessage:="Old password is required"),
  System.ComponentModel.PasswordPropertyText()>
  Public Property OldPassword As String
    Get
      Return GetProperty(OldPasswordProperty)
    End Get
    Set(value As String)
      SetProperty(OldPasswordProperty, value)
    End Set
  End Property

  <System.ComponentModel.PasswordPropertyText(),
  StringLength(50, ErrorMessageresourceName:="PasswordMinLength"),
  Required(ErrorMessage:="New password is required")>
  Public Property NewPassword As String
    Get
      Return GetProperty(NewPasswordProperty)
    End Get
    Set(value As String)
      SetProperty(NewPasswordProperty, value)
    End Set
  End Property

  <System.ComponentModel.PasswordPropertyText(),
   Required(ErrorMessage:="New password is required")>
  Public Property ConfirmPassword As String
    Get
      Return GetProperty(ConfirmPasswordProperty)
    End Get
    Set(value As String)
      SetProperty(ConfirmPasswordProperty, value)
    End Set
  End Property


  Public Property PasswordRequirementsInvalid As String = ""

  'Public ReadOnly Property FirstLogin As Boolean
  '  Get
  '    Return OBLib.Security.Settings.CurrentUser.FirstLogin
  '  End Get
  'End Property

  Public Shared PasswordChangedProperty As Csla.PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PasswordChanged, "", False)
  Public ReadOnly Property PasswordChanged As Boolean
    Get
      Return GetProperty(PasswordChangedProperty)
    End Get
  End Property

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    If Command = "ChangePassword" Then
      Dim PasswordError As String = ""
      If OBLib.Security.Settings.CurrentUser.NewPasswordValid(NewPassword, PasswordError) Then
        Dim cProc2 As Singular.CommandProc = Nothing
        Dim cProc As New Singular.CommandProc("CmdProcs.cmdChangePassword", _
                                           New String() {"@UserID", "@OldPassword", "@NewPassword"}, _
                                           New Object() {Singular.Settings.CurrentUserID,
                                                         Singular.Encryption.EncryptString(OldPassword),
                                                         Singular.Encryption.EncryptString(NewPassword)})
        cProc.CommandType = CommandType.StoredProcedure
        cProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cProc2 = cProc.Execute()
        If cProc2.DataRow(0) = 0 Then
          AddMessage(Singular.Web.MessageType.Error, "Change Password", "Please Enter correct Old Password")
        Else
          AddMessage(Singular.Web.MessageType.Success, "Change Password", "Change Password Success")
          SetProperty(PasswordChangedProperty, True)
          'OBLib.Security.Settings.CurrentUser.FirstLogin = False
          OBLib.Security.Settings.CurrentUser.Password = Singular.Encryption.EncryptString(NewPassword)
          'OBLib.CommonData.Refresh("UserList")
        End If
      Else
        PasswordRequirementsInvalid = PasswordError
        'AddMessage(Singular.Web.MessageType.Error, "Change Password", PasswordError)
      End If
    ElseIf Command = "Proceed" Then
      Page.Response.Redirect("~/Default.aspx")
    Else
      AddMessage(Singular.Web.MessageType.Error, "Change Password", "Validation failed")
    End If

  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad
    'PasswordRequirementsInvalid = OBLib.Security.Settings.CurrentUser.CurrentPasswordValid()
  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

    AddWebRule(ConfirmPasswordProperty,
                Function(c) c.NewPassword <> c.ConfirmPassword,
                Function(c) "Confirmed Password does not match new Password")

    AddWebRule(NewPasswordProperty,
                Function(c) c.NewPassword <> "" AndAlso c.NewPassword = c.OldPassword,
                Function(c) "New Password cannot be same as Old Password")

  End Sub

End Class
