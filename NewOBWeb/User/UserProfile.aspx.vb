﻿Imports Singular.Misc
Imports OBLib.HR.ReadOnly
Imports OBLib.Users.ReadOnly
Imports OBLib.Timesheets.OBCity
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports Singular.DataAnnotations
Imports Singular
Imports OBLib.Invoicing.ReadOnly
Imports System.ComponentModel
Imports OBLib.Timesheets
Imports OBLib.NSWTimesheets
Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports Singular.Web
Imports OBLib.HR
Imports System
Imports OBLib.HR.Timesheets.ReadOnly
Imports Singular.Reporting

Public Class UserProfile
  Inherits OBPageBase(Of UserProfileVM)
End Class

Public Class UserProfileVM
  Inherits OBViewModel(Of UserProfileVM)
  Implements ControlInterfaces(Of UserProfileVM).IFreelancerTimesheet
  Implements ControlInterfaces(Of UserProfileVM).IServicesFTCTimesheet
  Implements ControlInterfaces(Of UserProfileVM).ROPaymentRunSelector
  Implements ControlInterfaces(Of UserProfileVM).IROUserProfile
  Implements ControlInterfaces(Of UserProfileVM).IUserProfileShifts

#Region " Properties "

  'User Profile
  <InitialDataOnly>
  Public Property ROUserProfile As ROUserProfile Implements ControlInterfaces(Of UserProfileVM).IROUserProfile.ROUserProfile

  Public Property CurrentCrewTimesheetGuid As String = ""

  'Swaps
  Public Property SwapModalOpen As Boolean
  Public Property HumanResourceSwap As HumanResourceSwap = Nothing
  <InitialDataOnly>
  Public Property HumanResourceSwapList As HumanResourceSwapList = New HumanResourceSwapList

  <InitialDataOnly>
  Public Property ROHumanResourceSwapList As ROHumanResourceSwapList = New ROHumanResourceSwapList
  <InitialDataOnly>
  Public Property ROHumanResourceSwapListCriteria As OBLib.HR.ReadOnly.ROHumanResourceSwapList.Criteria = New OBLib.HR.ReadOnly.ROHumanResourceSwapList.Criteria
  <InitialDataOnly>
  Public Property ROHumanResourceSwapListManager As Singular.Web.Data.PagedDataManager(Of UserProfileVM) = New Singular.Web.Data.PagedDataManager(Of UserProfileVM)(Function(d) Me.ROHumanResourceSwapList,
                                                                                                                                                                    Function(d) Me.ROHumanResourceSwapListCriteria,
                                                                                                                                                                    "SwapDate", 10)

  'HR Details Queries
  Public Property IsQuerySend As Boolean = False
  Public Shared QueryModalOpenProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.QueryModalOpen, "Modal Open", False)
  ''' <summary>
  ''' Gets and sets the Human Resource value
  ''' </summary>
  <Display(Name:="Modal Open")>
  Public Property QueryModalOpen() As Boolean
    Get
      Return GetProperty(QueryModalOpenProperty)
    End Get
    Set(ByVal Value As Boolean)
      SetProperty(QueryModalOpenProperty, Value)
    End Set
  End Property

  Public Shared QueryDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.QueryDetails, "Please enter your query below", "")
  ''' <summary>
  ''' Gets and sets the Human Resource value
  ''' </summary>
  <Display(Name:="Please enter your query below"),
  TextField(True, False, True, 10)>
  Public Property QueryDetails() As String
    Get
      Return GetProperty(QueryDetailsProperty)
    End Get
    Set(ByVal Value As String)
      SetProperty(QueryDetailsProperty, Value)
    End Set
  End Property

  'Schedules
  Public Property ScheduleStartDate As DateTime?
  Public Property ScheduleEndDate As DateTime?
  Public Property FetchingSchedule As Boolean

  'Timesheets

  'Non Freelancer
  Public Property OBCityTimesheet As OBCityTimesheet Implements ControlInterfaces(Of UserProfileVM).IServicesFTCTimesheet.OBCityTimesheet
  Public Property OBCityTimesheetCriteria As OBCityTimesheetList.Criteria Implements ControlInterfaces(Of UserProfileVM).IServicesFTCTimesheet.OBCityTimesheetCriteria
  Public Property CurrentTimesheetMonth As ROTimesheetMonth Implements ControlInterfaces(Of UserProfileVM).IServicesFTCTimesheet.CurrentTimesheetMonth


  'Freelancer 
  Public Property CurrentPaymentRun As ROPaymentRun Implements ControlInterfaces(Of UserProfileVM).IFreelancerTimesheet.CurrentPaymentRun
  Public Property CurrentFreelancerTimesheetList As FreelancerTimesheetList Implements ControlInterfaces(Of UserProfileVM).IFreelancerTimesheet.CurrentFreelancerTimesheetList
  Public Property TotalFreelanceHours As String
  Public Property TotalFreelanceSnT As String

  <InitialDataOnly>
  Public Property ROPaymentRunList As ROPaymentRunList Implements ControlInterfaces(Of UserProfileVM).ROPaymentRunSelector.ROPaymentRunList
  <InitialDataOnly>
  Public Property ROPaymentRunListCriteria As OBLib.Invoicing.ReadOnly.ROPaymentRunList.Criteria Implements ControlInterfaces(Of UserProfileVM).ROPaymentRunSelector.ROPaymentRunListCriteria
  <InitialDataOnly>
  Public Property ROPaymentRunListManager As Singular.Web.Data.PagedDataManager(Of UserProfileVM) Implements ControlInterfaces(Of UserProfileVM).ROPaymentRunSelector.ROPaymentRunListManager


  'Shifts
  <InitialDataOnly>
  Public Property UserProfileShiftListCriteria As OBLib.Users.UserProfileShiftList.Criteria Implements ControlInterfaces(Of UserProfileVM).IUserProfileShifts.UserProfileShiftListCriteria
  <InitialDataOnly>
  Public Property UserProfileShiftList As OBLib.Users.UserProfileShiftList Implements ControlInterfaces(Of UserProfileVM).IUserProfileShifts.UserProfileShiftList
  <InitialDataOnly>
  Public Property CurrentUserProfileShiftGuiD As String Implements ControlInterfaces(Of UserProfileVM).IUserProfileShifts.CurrentUserProfileShiftGuiD

#End Region

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    QueryModalOpen = False
    SwapModalOpen = False

    ROUserProfile = OBLib.Users.ReadOnly.ROUserProfileList.GetROUserProfileList(OBLib.Security.Settings.CurrentUser.UserID, OBLib.Security.Settings.CurrentUser.HumanResourceID).FirstOrDefault

    If OBLib.Security.Settings.CurrentUser.HumanResourceID IsNot Nothing Then
      UserProfileShiftListCriteria = New OBLib.Users.UserProfileShiftList.Criteria
      UserProfileShiftListCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
      UserProfileShiftListCriteria.EndDate = Singular.Misc.Dates.DateMonthEnd(Now)
      UserProfileShiftListCriteria.HumanResourceID = OBLib.Security.Settings.CurrentUser.HumanResourceID
    End If

    ROPaymentRunList = New OBLib.Invoicing.ReadOnly.ROPaymentRunList
    ROPaymentRunListCriteria = New OBLib.Invoicing.ReadOnly.ROPaymentRunList.Criteria
    ROPaymentRunListManager = New Singular.Web.Data.PagedDataManager(Of UserProfileVM)(Function(d) Me.ROPaymentRunList,
                                                                                       Function(d) Me.ROPaymentRunListCriteria,
                                                                                       "StartDate", 10)


    ROHumanResourceSwapListCriteria.ApproverUserID = Nothing
    ROHumanResourceSwapListCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
    ROHumanResourceSwapListCriteria.EndDate = Singular.Dates.DateMonthEnd(Now).AddMonths(2)
    ROHumanResourceSwapListCriteria.ForHumanResourceID = ROUserProfile.HumanResourceID

    OBCityTimesheetCriteria = New OBCityTimesheetList.Criteria
    SetupTimesheets()
    'SetupInitialSchedule()
    ClientDataProvider.AddDataSource("ROUserManagedHumanResourceList", ROUserProfile.ROUserManagedHumanResourceList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)
    Select Case Command

      Case "PaymentRunSelected"
        PaymentRunSelected(CommandArgs)

      Case "SaveCrewTimesheet"
        SaveTimesheet(CommandArgs)

      Case "NewSwap"
        NewSwap(CommandArgs)

      Case "QueryDetails"
        SendQuery(CommandArgs)

    End Select
  End Sub

  Private Sub SetupTimesheets()
    If OBLib.Security.Settings.CurrentUser.IsFreelancer Then
      CurrentPaymentRun = OBLib.Helpers.TimesheetsHelper.GetCurrentPaymentRun(OBLib.Security.Settings.CurrentUser.SystemID)
      If CurrentPaymentRun IsNot Nothing Then
        ROPaymentRunListCriteria.SystemID = ROUserProfile.PrimarySystemID
        GetFreelancerTimesheet(CurrentPaymentRun)
      End If
    Else
      CurrentTimesheetMonth = OBLib.Helpers.TimesheetsHelper.GetCurrentTimesheetMonth()
      OBCityTimesheetCriteria.TimesheetMonthID = CurrentTimesheetMonth.TimesheetMonthID
      OBCityTimesheetCriteria.TimesheetMonth = CurrentTimesheetMonth.MonthYear
      OBCityTimesheetCriteria.HumanResourceID = ROUserProfile.HumanResourceID
      OBCityTimesheetCriteria.HumanResource = ROUserProfile.Firstname
      GetNonFreelancerTimesheet(CurrentTimesheetMonth)
    End If
  End Sub





  Private Sub GetNonFreelancerTimesheet(ROTimesheetMonth As ROTimesheetMonth)

    Dim mOBCityTimesheetList As New OBLib.Timesheets.OBCity.OBCityTimesheetList
    mOBCityTimesheetList = OBLib.Timesheets.OBCity.OBCityTimesheetList.GetOBCityTimesheetList(OBCityTimesheetCriteria.HumanResourceID,
                                                                                              ROTimesheetMonth.TimesheetMonthID,
                                                                                              OBLib.Security.Settings.CurrentUser.SystemID,
                                                                                              OBLib.Security.Settings.CurrentUser.ProductionAreaID, Nothing, Nothing)
    If mOBCityTimesheetList.Count = 1 Then
      OBCityTimesheet = mOBCityTimesheetList(0)
    End If

  End Sub

  Private Sub GetFreelancerTimesheet(ROPaymentRun As ROPaymentRun)
    CurrentFreelancerTimesheetList = OBLib.Timesheets.FreelancerTimesheetList.GetFreelancerTimesheetList(GetXml, ROPaymentRun.StartDate, ROPaymentRun.EndDate,
                                                                                                         OBLib.Security.Settings.CurrentUser.SystemID,
                                                                                                         OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    TotalFreelanceHours = CurrentFreelancerTimesheetList.Sum(Function(d) d.HoursForDay).ToString("#,#.00#;(#,#.00#)")
    TotalFreelanceSnT = CurrentFreelancerTimesheetList.Sum(Function(d) d.SnT).ToString("#,#.00#;(#,#.00#)")
  End Sub

  Private Sub PaymentRunSelected(CommandArgs As Singular.Web.CommandArgs)
    Dim PaymentRunID As Integer? = CommandArgs.ClientArgs.PaymentRunID
    CurrentPaymentRun = OBLib.Invoicing.ReadOnly.ROPaymentRunList.GetROPaymentRunList(PaymentRunID).FirstOrDefault
    If CurrentPaymentRun IsNot Nothing Then
      GetFreelancerTimesheet(CurrentPaymentRun)
    End If
  End Sub

  Private Sub SaveTimesheet(CommandArgs As Singular.Web.CommandArgs)
    Dim mOBCityTimesheetList As New OBCityTimesheetList
    mOBCityTimesheetList.Add(OBCityTimesheet)
    Dim sh As Singular.SaveHelper = TrySave(mOBCityTimesheetList)
    If sh.Success Then
      OBCityTimesheet = CType(sh.SavedObject, OBCityTimesheetList).FirstOrDefault
      OBCityTimesheet.DoCalculations()
    Else
      'TODO: show error
    End If
  End Sub

  Private Sub NewSwap(CommandArgs As Singular.Web.CommandArgs)
    HumanResourceSwap = New HumanResourceSwap
    HumanResourceSwap.HumanResourceID = ROUserProfile.HumanResourceID
    HumanResourceSwap.HumanResource = ROUserProfile.DisplayName
    HumanResourceSwap.ContractType = ROUserProfile.ContractType
    HumanResourceSwap.SwapWithHumanResource = CommandArgs.ClientArgs.HumanResource
    HumanResourceSwap.SwapWithHumanResourceID = CommandArgs.ClientArgs.HumanResourceID
    HumanResourceSwap.SwapWithContractType = CommandArgs.ClientArgs.ContractType
  End Sub



  Public Sub SendQuery(CommandArgs As Singular.Web.CommandArgs)
    Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdQueryDetails]",
                                        New String() {
                                                     "@HumanResourceID",
                                                     "@QueryText"
                                                     },
                                        New Object() {
                                                     OBLib.Security.Settings.CurrentUser.HumanResourceID,
                                                     QueryDetails
                                                     })
    cmd.FetchType = CommandProc.FetchTypes.DataObject
    cmd.CommandTimeout = 0
    cmd = cmd.Execute()

    Dim EmailID = cmd.DataObject

    If Not Singular.Misc.IsNull(EmailID, 0) = 0 Then
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False)
    End If
  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

    With AddWebRule(QueryDetailsProperty)
      .JavascriptRuleFunctionName = "UserProfileBO.QueryDetailsValid"
      .ServerRuleFunction = AddressOf CheckQueryStringValid
      .AddTriggerProperty(QueryModalOpenProperty)
    End With

  End Sub

  Public Function GetXml() As String
    Dim currHRID As Integer = OBLib.Security.Settings.CurrentUser.HumanResourceID
    Dim LID As New List(Of String)
    LID.Add(currHRID.ToString)
    Dim xmlids As String = OBLib.OBMisc.StringArrayToXML(LID.ToArray)
    Return xmlids
  End Function

  Private Shared Function CheckQueryStringValid(UserProfileVM As UserProfileVM) As String
    If UserProfileVM.QueryModalOpen AndAlso UserProfileVM.QueryDetails.Equals("") Then
      Return "Query String is required"
    End If
    Return ""
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetROUserBookingList(StartDate As Date?, EndDate As Date?, PageNo As Integer, PageSize As Integer,
                                                   FirstName As String, Surname As String, PreferredName As String) As Singular.Web.Result
    Try
      If StartDate IsNot Nothing And EndDate IsNot Nothing Then
        Try
          Dim bookingList As OBLib.HR.PersonnelManager.ReadOnly.ROUserResourceList
          bookingList = OBLib.HR.PersonnelManager.ReadOnly.ROUserResourceList.GetROUserResourceList(StartDate, EndDate, Nothing, Nothing, FirstName, Surname, PreferredName, PageNo, PageSize)
          Dim res As New Singular.Web.Result(True) With {
                                                         .Data = New With {
                                                                           .List = bookingList,
                                                                           .PageNo = PageNo,
                                                                           .PageSize = PageSize,
                                                                           .TotalRecords = bookingList.TotalRecords,
                                                                           .TotalPages = bookingList.TotalPages
                                                                          }
                                                        }
          Return res
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function PrintOBCityTimesheet(Timesheet As OBCityTimesheet) As Singular.Web.Result
    Dim rpt As New OBWebReports.HumanResourceReports.HRTimesheet
    rpt.ReportCriteria.HumanResourceID = Timesheet.HumanResourceID
    rpt.ReportCriteria.EndDate = Timesheet.TMEndDate
    Dim rfi As ReportFileInfo = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
    'Save the Document Temporarily for downloading
    Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
    Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function RecalculateOBCityTimesheet(Timesheet As OBCityTimesheet) As Singular.Web.Result
    Timesheet.DoCalculations()
    Return New Singular.Web.Result(True) With {.Data = Timesheet}
  End Function

End Class