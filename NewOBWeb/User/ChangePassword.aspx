﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Login.Master"
  CodeBehind="ChangePassword.aspx.vb" Inherits="NewOBWeb.ChangePassword" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="../Styles/bootstrap/override/signin.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    body, .panel, .panel-heading
    {
      background-image: none !important;
      background-color: #ffffff !important;
      background-repeat: repeat-x !important;
    }
    .panel
    {
      border: none;
    }
    .panel-default > .panel-heading
    {
      border: none;
      background-color: #ffffff !important;
    }
    .container
    {
      margin-top: 100px;
    }
    .Msg div, .ValidationPopup
    {
      border-style: solid;
      border-width: 1px;
      border-radius: 4px;
      margin: 6px 0 5px 0;
      background-repeat: no-repeat;
      background-position: 3px 3px;
      padding: 3px 10px 6px 24px;
      font-size: 0.9em;
      color: #000;
    }
    .Msg div strong
    {
      line-height: 1.5em;
    }
    .Msg-Validation, .Msg-Error
    {
      background-color: #FBECEB;
      border-color: #c44;
    }
    .Msg-Validation
    {
      background-image: url('../Images/IconBRule.png');
      overflow: auto;
    }
    .Msg-Validation > ul
    {
      padding-left: 0;
    }
    .Msg-Validation strong, .Msg-Error strong
    {
      color: #c44;
      line-height: 1.2em;
    }
    .Msg-Error
    {
      background-image: url('../Images/IconError.png');
    }
    .Msg-Success
    {
      background-image: url('../Images/IconAuth.png');
      background-color: #F1F8EE;
      border-color: #4a2;
    }
    .Msg-Success strong
    {
      color: #291;
    }
    
    .Msg-Warning
    {
      background-image: url('../Images/IconWarning.png');
      background-color: #FFF7E5;
      border-color: #D9980D;
    }
    .Msg-Warning strong
    {
      color: #D9980D;
    }
    
    .Msg-Information
    {
      background-image: url('../Images/IconInfo.png');
      background-color: #E9F1FB;
      border-color: #225FAA;
    }
    .Msg-Information strong
    {
      color: #225FAA;
    }
    .MsgBoxMessage > ul
    {
      padding-left: 0;
    }
    .login-wrapper ul
    {
      text-align: left;
    }
  </style>
  <script type="text/javascript">
    $('html').bind('keypress', function (e) {
      if (e.keyCode == 13) {
        return false;
      }
    }); 
  </script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
    
      'h.MessageHolder()
        
      With h.DivC("login-wrapper")
        With .Helpers.DivC("box")
          With .Helpers.DivC("content-wrap")
            With .Helpers.HTMLTag("h6")
              .Helpers.HTML("Change Password")
            End With
            With .Helpers.DivC("alerts-container")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) Not ViewModel.PasswordChanged)
              With .Helpers.DivC("alert alert-info alert-password-requirements")
                With .Helpers.HTMLTag("strong")
                                  .Helpers.HTML("Password Requirements")
                                  
                End With
                With .Helpers.HTMLTag("ul")
                  With .Helpers.HTMLTag("li")
                    .Helpers.HTML("8 Characters long")
                  End With
                  With .Helpers.HTMLTag("li")
                    .Helpers.HTML("1 Uppercase letter")
                  End With
                  With .Helpers.HTMLTag("li")
                    .Helpers.HTML("1 Lowercase letter")
                  End With
                  With .Helpers.HTMLTag("li")
                    .Helpers.HTML("2 Numbers")
                  End With
                End With
              End With
              With .Helpers.DivC("alert alert-danger alert-password-insufficient")
                .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.PasswordRequirementsInvalid <> "")
                With .Helpers.HTMLTag("strong")
                  .Helpers.HTML("Current Password")
                End With
                With .Helpers.HTMLTag("span")
                  .AddBinding(Singular.Web.KnockoutBindingString.html, Function(vm) ViewModel.PasswordRequirementsInvalid)
                End With
              End With
              With .Helpers.DivC("alert alert-danger alert-password-first-login")
                '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.FirstLogin)
                With .Helpers.HTMLTag("strong")
                  .Helpers.HTML("First Login")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Your default password must be changed on your first login")
                End With
              End With
            End With
            With .Helpers.EditorFor(Function(d) ViewModel.OldPassword)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) Not ViewModel.PasswordChanged)
              .AddClass("form-control")
              .Attributes("type") = "text"
              .Attributes("placeholder") = "Old Password"
            End With
            With .Helpers.EditorFor(Function(d) ViewModel.NewPassword)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) Not ViewModel.PasswordChanged)
              .AddClass("form-control")
              .Attributes("type") = "password"
              .Attributes("placeholder") = "New Password"
            End With
            With .Helpers.EditorFor(Function(d) ViewModel.ConfirmPassword)
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) Not ViewModel.PasswordChanged)
              .AddClass("form-control")
              .Attributes("type") = "password"
              .Attributes("placeholder") = "Confirm New Password"
            End With
            With .Helpers.BootstrapButton("ChangePassword", "Change Password", "btn-glow primary login", "", Singular.Web.PostBackType.Ajax, False, , )
              With .Button
                .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) Not ViewModel.PasswordChanged)
              End With
            End With
            With .Helpers.DivC("alert alert-success")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.PasswordChanged)
              With .Helpers.HTMLTag("strong")
                .Helpers.HTML("Change Password")
              End With
              With .Helpers.HTMLTag("span")
               .Helpers.HTML("Password changed successfully")
              End With
            End With
            With .Helpers.BootstrapButton("Proceed", "Continue", "btn-glow success", "", Singular.Web.PostBackType.Full, False, , )
              With .Button
                .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.PasswordChanged)
              End With
            End With
          End With
        End With
      End With
      
    End Using
    
  %>
</asp:Content>
