﻿Public Class AuditTrails
  Inherits OBPageBase(Of AuditTrailsVM)

End Class

Public Class AuditTrailsVM
  Inherits OBViewModel(Of AuditTrailsVM)

  Public Property AuditTrailsTimelineListCriteria As OBLib.AuditTrails.ReadOnly.ROAuditTrailTimelineList.Criteria
  Public Property SelectedAuditTrailTimeline As OBLib.AuditTrails.ReadOnly.ROAuditTrailTimeline

  Protected Overrides Sub Setup()
    MyBase.Setup()
    MessageFadeTime = 2000
    Me.ValidationMode = Singular.Web.ValidationMode.OnLoad
    AuditTrailsTimelineListCriteria = New OBLib.AuditTrails.ReadOnly.ROAuditTrailTimelineList.Criteria
    SelectedAuditTrailTimeline = Nothing
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
    End Select

  End Sub

End Class