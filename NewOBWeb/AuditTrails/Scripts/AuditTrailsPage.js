﻿$(document).ready(function () {
  window.timelineContainer = null;
  window.timeline = null;
  window.timelineOptions = null;
  window.auditTrailTimelines = null;
  window.TempResource = null;
});


Singular.OnPageLoad(function () {

  ViewModel.AuditTrailsTimelineListCriteria().StartDate(new Date("01 December 2014"));
  ViewModel.AuditTrailsTimelineListCriteria().EndDate(new Date("31 January 2015"));
  ViewModel.AuditTrailsTimelineListCriteria().AuditTrailTableID(229);

});

function FetchAuditTrails() {
  Singular.GetDataStateless('OBLib.AuditTrails.ReadOnly.ROAuditTrailTimelineList, OBLib',
    {
      StartDate: ViewModel.AuditTrailsTimelineListCriteria().StartDate(), //'01 January 2014',
      EndDate: ViewModel.AuditTrailsTimelineListCriteria().EndDate(), //'31 December 2015',
      AuditTrailTableID: ViewModel.AuditTrailsTimelineListCriteria().AuditTrailTableID(),
      Fields: ViewModel.AuditTrailsTimelineListCriteria().Fields(),
      PreviousValue: ViewModel.AuditTrailsTimelineListCriteria().PreviousValue(),
      NewValue: ViewModel.AuditTrailsTimelineListCriteria().NewValue()
    },
    function (args) {
      Log(args.Data);
      SetupTimeline(args.Data);
    })
};

function CreateAuditTrail(AuditTrailTimeline) {
  window.TempResource = null;
  window.TempResource = {
    id: AuditTrailTimeline.Guid,
    content: GetATContent(AuditTrailTimeline),
    start: new Date(AuditTrailTimeline.ChangeDateTime),
    className: GetATClassName(AuditTrailTimeline),
    AuditTrailTimeline: AuditTrailTimeline
  }
  window.auditTrailTimelines.add(TempResource);
  window.TempResource = null;
};

function GetATContent(AuditTrailTimeline) {
  switch (AuditTrailTimeline.AuditTrailType) {
    case 1:
      return "<span>" + AuditTrailTimeline.LoginName + " added a record</span>"
    case 2:
      return "<span>" + AuditTrailTimeline.LoginName + " updated " + AuditTrailTimeline.ChangeCount + " field/s " + "</span>"
    case 3:
      return "<span>" + AuditTrailTimeline.LoginName + " deleted a record</span>"
  }
};

function GetATClassName(AuditTrailTimeline) {
  switch (AuditTrailTimeline.AuditTrailType) {
    case 1:
      return "at-insert";
    case 2:
      return "at-update";
    case 3:
      return "at-delete";
  }
};

function SetupTimeline(Data) {

  var sd = new Date(ViewModel.AuditTrailsTimelineListCriteria().StartDate()).setHours(0, 0, 0, 0);
  var ed = new Date(ViewModel.AuditTrailsTimelineListCriteria().EndDate()).setHours(23, 59, 59, 999);

  if (window.timeline) {
    window.timeline.destroy();
  }

  window.timelineOptions = {
    orientation: 'top',
    showCurrentTime: true,
    stack: true,
    min: sd,
    max: ed,
    type: 'point',
    width: '100%'
  };

  window.timelineContainer = document.getElementById("AuditTrailTimeline");
  window.auditTrailTimelines = new vis.DataSet();

  Data.Iterate(function (AuditTrailTimeline, ATTIndex) {
    CreateAuditTrail(AuditTrailTimeline);
  });

  window.timeline = new vis.Timeline(window.timelineContainer, window.auditTrailTimelines, window.timelineOptions);
  window.timeline.fit();

  window.timeline.on('select', itemsSelected);

};

function itemsSelected(idsObj) {
  console.log(idsObj.items[0]);
  var item = window.auditTrailTimelines.get(idsObj.items[0]);
  ViewModel.SelectedAuditTrailTimeline.Set(item.AuditTrailTimeline);
  $("#SelectedAuditTrail").modal();
};