﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AuditTrails.aspx.vb" Inherits="NewOBWeb.AuditTrails" %>

<%@ Import Namespace="OBLib.AuditTrails.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <link type="text/css" href="../Scripts/vis3.12/dist/vis.min.css" rel="Stylesheet" />
  <style type="text/css">
    div.ComboDropDown {
      z-index: 35000;
    }

    body, #CurrentAttendees {
      background-color: #F0F0F0;
    }

    .saturday, .sunday {
      background-color: #F0F0F0;
    }

    .vis.timeline .item .at-insert {
      color: #ffffff;
      border-color: #5cb85c;
      background-color: #449d44;
    }

    .vis.timeline .item .at-update {
      color: #ffffff;
      border-color: #f0ad4e;
      background-color: #ec971f;
    }

    .vis.timeline .item .at-delete {
      color: #ffffff;
      border-color: #d9534f;
      background-color: #c9302c;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%
    Using h = Helpers

      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Audit Trails", True)
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Toolbar
                .Helpers.MessageHolder()
              End With
            End With
          End With
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.AuditTrailsTimelineListCriteria.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.AuditTrailsTimelineListCriteria.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.InputGroupCombo(Function(d) ViewModel.AuditTrailsTimelineListCriteria.AuditTrailTableID, "Table", ,
                                                          Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                          Singular.Web.BootstrapEnums.InputSize.Small, ,
                                                          Singular.Web.BootstrapEnums.InputGroupSize.Small, ,
                                                          Singular.Web.BootstrapEnums.ButtonSize.Small, )
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "FetchAuditTrails()")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.AuditTrailsTimelineListCriteria.Fields, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Fields"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.AuditTrailsTimelineListCriteria.PreviousValue, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Previous Value"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.AuditTrailsTimelineListCriteria.NewValue, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "New Value"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                
              End With
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Results", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Div
                .Attributes("id") = "AuditTrailTimeline"
              End With
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("SelectedAuditTrail", "Selected Audit Trail")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of ROAuditTrailTimeline)(Function(d) ViewModel.SelectedAuditTrailTimeline)
              With .Helpers.Bootstrap.Column(12, 6, 6, 4)
                With .Helpers.Bootstrap.Row
                  .Helpers.LabelFor(Function(d As ROAuditTrailTimeline) d.AuditTrailType).AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                  With .Helpers.Bootstrap.Row
                    .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROAuditTrailTimeline) d.AuditTrailType, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  .Helpers.LabelFor(Function(d As ROAuditTrailTimeline) d.LoginName).AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                  With .Helpers.Bootstrap.Row
                    .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROAuditTrailTimeline) d.LoginName, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  .Helpers.LabelFor(Function(d As ROAuditTrailTimeline) d.ChangeDateTime).AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                  With .Helpers.Bootstrap.Row
                    .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROAuditTrailTimeline) d.ChangeDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  .Helpers.LabelFor(Function(d As ROAuditTrailTimeline) d.Description).AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                  With .Helpers.Bootstrap.Row
                    .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROAuditTrailTimeline) d.Description, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  .Helpers.LabelFor(Function(d As ROAuditTrailTimeline) d.ChangeCount).AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                  With .Helpers.Bootstrap.Row
                    .AddClass("col-xs-6 col-sm-6 col-md-6 col-lg-6")
                    With .Helpers.Bootstrap.FormControlFor(Function(d As ROAuditTrailTimeline) d.ChangeCount, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 6, 6)
                With .Helpers.Bootstrap.TableFor(Of ROAuditTrailTimelineDetail)(Function(d) d.ROAuditTrailTimelineDetailList,
                                                                                False, False, False, False, True, True, False)
                  With .FirstRow
                    With .AddReadOnlyColumn(Function(c As ROAuditTrailTimelineDetail) c.ColumnName)
                      
                    End With
                    With .AddReadOnlyColumn(Function(c As ROAuditTrailTimelineDetail) c.PreviousValue)
                      
                    End With
                    With .AddReadOnlyColumn(Function(c As ROAuditTrailTimelineDetail) c.NewValue)
                      
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
    End Using
  %>
  <script type="text/javascript" src="../Scripts/vis3.12/dist/vis.min.js"></script>
  <script type="text/javascript" src="Scripts/AuditTrailsPage.js"></script>
</asp:Content>
