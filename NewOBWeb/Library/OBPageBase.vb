Public Class OBPageBase(Of VM As Singular.Web.ViewModel(Of VM))
  Inherits Singular.Web.PageBase(Of VM)

  Protected Overrides Sub Setup()
    MyBase.Setup()

    Me.Controls.Add(Helpers.Control(New Controls.UserNotificationsModal(Of VM)()))

  End Sub

End Class
