﻿Imports System
Imports Singular.Web.WebServices

Public Class FileUpload
  Inherits Singular.Web.WebServices.FileUploadHandler

  Public Overrides Function SaveDocument(context As System.Web.HttpContext, FileName As String, FileBytes() As Byte, DocumentGuid As Guid) As Singular.Web.WebServices.FileUploadHandler.ResponseObject

    Dim ro As New FileUploadHandler.ResponseObject

    If context.Request.QueryString("Image") IsNot Nothing Then
      'image
      ro.DocumentID = OBLib.Images.ImgDataHandler.InsertImage(FileName, FileBytes, context.Request.QueryString("ImageTypeID"), 0, context.Request.QueryString("ForHumanResourceID"), context)
      ro.Success = True
    Else
      ''document
      ro = MyBase.SaveDocument(context, FileName, FileBytes, DocumentGuid)
    End If

    Return ro

  End Function


End Class