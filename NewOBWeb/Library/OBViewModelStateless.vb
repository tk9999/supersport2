Imports System.ComponentModel
Imports Singular.Web
Imports Singular.DataAnnotations
Imports OBLib.HR
Imports Singular.Web.Data
Imports OBLib.SatOps
Imports OBLib.Shifts.PlayoutOperations
Imports OBLib.Resources
Imports Singular.Misc
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.RoomScheduling
Imports OBLib.AdHoc
Imports OBLib.Shifts
Imports OBLib.Copying
Imports OBLib.Helpers
Imports OBLib.Slugs
Imports Singular.Reporting
Imports OBLib.Notifications.Email
Imports OBLib.Shifts.Studios
Imports OBLib.Scheduling.Rooms
Imports OBLib.Timesheets
Imports OBLib.Maintenance.SystemManagement
Imports OBLib.Timesheets.ReadOnly
Imports OBLib.OutsideBroadcast
Imports OBLib.Synergy
Imports OBLib.Productions.Base
Imports Singular.Documents
Imports System
Imports OBLib.Timesheets.OBCity

<Serializable()>
Public Class OBViewModelStateless(Of ModelType As Singular.Web.StatelessViewModel(Of ModelType))
  Inherits Singular.Web.StatelessViewModel(Of ModelType)

  Private mSiteInfo As OBLib.Helpers.SiteInfo
  <Browsable(False)>
  Friend ReadOnly Property SiteInfo As OBLib.Helpers.SiteInfo
    Get
      If mSiteInfo Is Nothing Then
        mSiteInfo = New OBLib.Helpers.SiteInfo(Me.Page.Request)
      End If
      Return mSiteInfo
    End Get
  End Property

  Private Shared Property ProductionHROBList As Object

  Protected Overrides Sub WriteInitialiseVariables(JSW As Utilities.JavaScriptWriter)
    MyBase.WriteInitialiseVariables(JSW)
    'Dim sSiteInfo As New OBLib.Helpers.SiteInfo(Me.Page.Request)
    JSW.Write("window.SiteInfo = " & Singular.Web.Data.JSonWriter.SerialiseObject(SiteInfo, , False, OutputType.Javascript) & ";")
  End Sub

  Public Property TempImage As New Singular.Documents.TemporaryDocumentNotRequired

#Region " Properties "

  Public Property IsProcessing As Boolean = False

  <Browsable(False)>
  Public ReadOnly Property HasAuthenticatedUser As Boolean
    Get
      If Not Singular.Security.HasAuthenticatedUser OrElse OBLib.Security.Settings.CurrentUser Is Nothing Then
        Return False
      End If
      Return True
    End Get
  End Property

  Public ReadOnly Property CurrentUserName As String
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.LoginName
      Else
        Return "No Current User"
      End If
    End Get
  End Property

  Public ReadOnly Property CurrentUserID As Integer?
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.UserID
      Else
        Return Nothing
      End If
    End Get
  End Property

  Public ReadOnly Property CurrentUserHumanResourceID As Integer?
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.HumanResourceID
      Else
        Return Nothing
      End If
    End Get
  End Property

#Region " User Notifications "

  Public Property ROUserNotificationList As OBLib.Users.ReadOnly.ROUserNotificationList = New OBLib.Users.ReadOnly.ROUserNotificationList
  Public Property ROUserNotificationListCriteria As OBLib.Users.ReadOnly.ROUserNotificationList.Criteria = New OBLib.Users.ReadOnly.ROUserNotificationList.Criteria
  Public Property ROUserNotificationListManager As PagedDataManager(Of OBViewModelStateless(Of ModelType)) = New PagedDataManager(Of OBViewModelStateless(Of ModelType))(Function(d) d.ROUserNotificationList, Function(d) d.ROUserNotificationListCriteria,
                                                                                                                                                                         "CreatedDateTime", 10, False)

#End Region

#End Region

#Region " ViewModel Methods "

  Protected Overridable Function HasPageAccess() As Boolean
    Return True
  End Function

  Private Sub RedirectToChangePassword()

    If (Not HttpContext.Current.Request.Path.Contains("ChangePassword.aspx")) Then
      HttpContext.Current.Response.Redirect("~/User/ChangePassword.aspx")
    End If

  End Sub

  Public Sub RedirectToLoginPage()
    HttpContext.Current.Response.Redirect("~/Account/Login.aspx")
  End Sub

  Public Sub RedirectToNoAccess()
    HttpContext.Current.Response.Redirect("~/ErrorPage.aspx")
  End Sub

  Public Function CurrentUser() As OBLib.Security.User

    Return OBLib.Security.Settings.CurrentUser

  End Function

  Protected Overrides Sub Setup()
    MyBase.Setup()

    'Add the current users System and SystemArea List
    If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
      ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
      ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    End If

    ROUserNotificationList = New OBLib.Users.ReadOnly.ROUserNotificationList
    ROUserNotificationListCriteria = New OBLib.Users.ReadOnly.ROUserNotificationList.Criteria
    ROUserNotificationListManager = New PagedDataManager(Of OBViewModelStateless(Of ModelType))(Function(d) d.ROUserNotificationList, Function(d) d.ROUserNotificationListCriteria,
                                                                                                "CreatedDateTime", 10, False)
    ROUserNotificationListCriteria.UserID = Me.CurrentUserID
    ROUserNotificationListCriteria.StartDate = Now
    ROUserNotificationListCriteria.EndDate = Now
    ROUserNotificationListCriteria.SortAsc = False
    ROUserNotificationListCriteria.SortColumn = "CreatedDateTime"
    ROUserNotificationListCriteria.PageNo = 1
    ROUserNotificationListCriteria.PageSize = 10

  End Sub

  Protected Sub PreSetup()

    If Not HasAuthenticatedUser Then
      RedirectToLoginPage()
    End If

    'If Not UserPasswordValid Then
    '  RedirectToChangePassword()
    'End If

    If Not HasPageAccess() Then
      RedirectToNoAccess()
    End If

  End Sub

#End Region

#Region " Error Handling "

  <WebCallable(LoggedInOnly:=True)>
  Public Function LogClientError(User As String, Page As String, Method As String, ErrorMessage As String) As Singular.Web.Result

    Try
      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdInsClientError]",
                                    New String() {"@User", "@Page", "@Method", "@Error"},
                                    New Object() {User, Page, Method, ErrorMessage})
      cmd = cmd.Execute
    Catch ex As Exception
      Return New Singular.Web.Result(False)
    End Try
    Return New Singular.Web.Result(True)

  End Function

#End Region

#Region " Resource Bookings "

  <InitialDataOnly>
  Public Property ResourceBookingsCleanList As List(Of OBLib.Resources.ResourceBookingClean) = New List(Of OBLib.Resources.ResourceBookingClean)

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetResourceBookingClashes(ResourceBookings As List(Of OBLib.Resources.ResourceBookingClean)) As Singular.Web.Result
    Try
      Dim data As List(Of OBLib.Resources.ResourceBookingClean) = OBLib.Helpers.ResourceHelpers.GetResourceBookingsCleanClashes(ResourceBookings)
      Return New Singular.Web.Result(True) With {.Data = data}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetSingleBookingClashes(ResourceBookingClean As OBLib.Resources.ResourceBookingClean) As Singular.Web.Result
    Try
      Dim rbl As New List(Of OBLib.Resources.ResourceBookingClean)
      rbl.Add(ResourceBookingClean)
      Dim data As List(Of OBLib.Resources.ResourceBookingClean) = OBLib.Helpers.ResourceHelpers.GetResourceBookingsCleanClashes(rbl)
      Return New Singular.Web.Result(True) With {.Data = data}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetPersonnelManagerBookings(StartDate As DateTime?,
                                              EndDate As DateTime?,
                                              DisciplineID As Integer?,
                                              PositionID As Integer?,
                                              FirstName As String,
                                              Surname As String,
                                              PreferredName As String,
                                              PageNo As Integer?,
                                              PageSize As Integer?,
                                              ResourceSchedulerID As Integer?) As Singular.Web.Result

    Dim wr As Singular.Web.Result
    Try
      Dim RSResources As OBLib.Resources.RSResourceList = OBLib.Resources.RSResourceList.GetRSResourceList(StartDate, EndDate,
                                                                                                           DisciplineID, PositionID,
                                                                                                           FirstName, Surname, PreferredName,
                                                                                                           PageNo, PageSize,
                                                                                                           ResourceSchedulerID)
      wr = New Singular.Web.Result(True)
      wr.Data = New With {
                          .Resources = RSResources,
                          .TotalRecords = RSResources.TotalRecords,
                          .TotalPages = RSResources.TotalPages
                }
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function GetResourceClashes(ResourceBookings As List(Of OBLib.Resources.ResourceBooking)) As Singular.Web.Result

    Try
      Dim clashList As List(Of ClashDetail) = OBLib.Resources.ResourceBookingList.GetResourceClashes(ResourceBookings)
      Return New Singular.Web.Result(True) With {.Data = clashList}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveProductionHROBSimple(ProductionHROBSimple As ProductionHROBSimple) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = ProductionHROBSimple.TrySave(GetType(ProductionHROBSimpleList))
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(sh.Success) With {.Data = sh.SavedObject, .ErrorText = sh.ErrorText}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function CheckEditState(ResourceBookingID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdResourceBookingCheckEditState",
                                              New String() {"@ResourceBookingID", "@CurrentUserID", "@CurrentUser"},
                                              New Object() {NothingDBNull(ResourceBookingID), OBLib.Security.Settings.CurrentUserID, OBLib.Security.Settings.CurrentUser.LoginName})
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
      cmdProc = cmdProc.Execute()

      Dim IsBeingEditedBy As String = ""
      Dim InEditDateTime As DateTime? = Nothing
      Dim IsBeingEditedByUserID As Integer? = Nothing
      Dim IsBeingEditedByName As String = ""
      Dim EditImagePath As String = ""

      If cmdProc.DataRow IsNot Nothing Then
        IsBeingEditedBy = cmdProc.DataRow(0)
        InEditDateTime = OBLib.OBMisc.DBNullNothing(cmdProc.DataRow(1))
        IsBeingEditedByUserID = OBLib.OBMisc.DBNullNothing(cmdProc.DataRow(2))
        IsBeingEditedByName = IIf(IsDBNull(cmdProc.DataRow(3)), "", cmdProc.DataRow(3))
        EditImagePath = IIf(IsDBNull(cmdProc.DataRow(4)), "", cmdProc.DataRow(4))
        Return New Singular.Web.Result(True) With {.Data = New With {.IsBeingEditedBy = IsBeingEditedBy, .InEditDateTime = InEditDateTime,
                                                             .IsBeingEditedByUserID = IsBeingEditedByUserID, .IsBeingEditedByName = IsBeingEditedByName,
                                                             .EditImagePath = EditImagePath}}
      Else
        Return New Singular.Web.Result(True) With {.Data = New With {.IsBeingEditedBy = IsBeingEditedBy, .InEditDateTime = InEditDateTime,
                                                                     .IsBeingEditedByUserID = IsBeingEditedByUserID, .IsBeingEditedByName = IsBeingEditedByName,
                                                                     .EditImagePath = EditImagePath}}
      End If

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

#End Region

#Region " Resource Scheduler "

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function CopyCrewResourceScheduler(FromBooking As RSResourceBooking, ToBooking As RSResourceBooking) As Singular.Web.Result

    Try
      Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerCopyCrew",
                                          New String() {"@FromProductionSystemAreaID", "@ToProductionSystemAreaID", "@CurrentUserID"},
                                          New Object() {FromBooking.ProductionSystemAreaID, ToBooking.ProductionSystemAreaID, OBLib.Security.Settings.CurrentUserID})
      cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      Dim clashesDS As DataSet = cmd.Dataset
      Dim clashesTBL As DataTable = clashesDS.Tables(0)
      Dim FeedbackString As String = ""
      If clashesTBL.Rows.Count > 0 Then
        FeedbackString = "Clashes found for: " & vbCrLf
        For Each tr As DataRow In clashesTBL.Rows
          Dim BookingDescription As String = tr(10)
          Dim ResourceName As String = tr(18)
          FeedbackString &= ResourceName & vbCrLf
        Next
      End If
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) With {.ErrorText = FeedbackString}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return New Singular.Web.Result(False) With {.ErrorText = "Not Implemented"}

  End Function

  'Public Property RoomScheduleChangeList As RoomScheduleChangeList

#End Region

#Region " Synergy "

  <WebCallable(LoggedInOnly:=True)>
  Public Function DoNewSynergyImport(StartDate As DateTime?, EndDate As DateTime?) As Singular.Web.Result
    Try
      OBLib.Synergy.Importer.[New].SynergyImporter.ImportData(StartDate, EndDate, "", "", "", "", "", True, True, True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetSynergyEvent(GenRefNumber As Int64?, StartDateTime As DateTime?, EndDateTime As DateTime?) As Singular.Web.Result
    Try
      Dim sel As OBLib.Synergy.SynergyEventList = OBLib.Synergy.SynergyEventList.GetSynergyEventList(Nothing, Nothing, GenRefNumber, StartDateTime, EndDateTime, True, True, True, False)
      If sel.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = sel(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Could not find event"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SetupRoomAllocatorRoomSchedule(RoomAllocatorRoomSchedule As OBLib.Rooms.RoomAllocatorRoomSchedule) As Singular.Web.Result
    Try
      Dim wr As Singular.Web.Result = RoomAllocatorRoomSchedule.DoSetup()
      Dim channels As OBLib.Rooms.RoomAllocatorRoomScheduleChannelList = OBLib.Rooms.RoomAllocatorRoomScheduleChannelList.GetRoomAllocatorRoomScheduleChannelList(RoomAllocatorRoomSchedule.GenRefNumber, ZeroNothing(RoomAllocatorRoomSchedule.RoomScheduleID), RoomAllocatorRoomSchedule.ScheduleNumber)
      If wr.Success Then
        RoomAllocatorRoomSchedule.SetChannels(channels)
        wr.Data = RoomAllocatorRoomSchedule
      End If
      Return wr
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveRoomAllocatorRoomSchedule(RoomAllocatorRoomSchedule As OBLib.Rooms.RoomAllocatorRoomSchedule) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = RoomAllocatorRoomSchedule.TrySave(GetType(OBLib.Rooms.RoomAllocatorRoomScheduleList))
      If sh.Success Then
        RoomAllocatorRoomSchedule = sh.SavedObject
        'RoomAllocatorRoomSchedule = OBLib.Rooms.RoomAllocatorRoomScheduleList.GetRoomAllocatorRoomScheduleList(New Criter))
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = RoomAllocatorRoomSchedule}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function DeleteRoomAllocatorRoomSchedule(RoomAllocatorRoomSchedule As OBLib.Rooms.RoomAllocatorRoomSchedule) As Singular.Web.Result
    Try
      'Dim rsl As RoomScheduleAreaList = OBLib.Scheduling.Rooms.RoomScheduleAreaList.GetRoomScheduleAreaList(RoomAllocatorRoomSchedule.RoomScheduleID, RoomAllocatorRoomSchedule.ProductionSystemAreaID)
      Dim ids As New List(Of Integer)
      ids.Add(RoomAllocatorRoomSchedule.ProductionSystemAreaID)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Dim sh As Singular.Web.Result = DeleteRoomScheduleAreas(ids, True)
      Return sh
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveRoomScheduleTemplate(RoomScheduleTemplate As OBLib.Scheduling.Rooms.RoomScheduleTemplate) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = RoomScheduleTemplate.TrySave(GetType(OBLib.Scheduling.Rooms.RoomScheduleTemplateList))
      If sh.Success Then
        RoomScheduleTemplate = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = RoomScheduleTemplate}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SetupRoomScheduleTemplate(RoomScheduleTemplate As OBLib.Scheduling.Rooms.RoomScheduleTemplate) As Singular.Web.Result
    Try
      Dim wr As Singular.Web.Result = RoomScheduleTemplate.DoSetup()
      If wr.Success Then
        wr.Data = RoomScheduleTemplate
      End If
      Return wr
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DownloadSynergyChangesWithBookingInfo(SynergyImportID As Integer?) As Singular.Web.Result
    Try
      If SynergyImportID IsNot Nothing Then
        Dim rptChanges As New OBWebReports.SynergyReports.SynergyChangesWithBookingInfo
        rptChanges.ReportCriteria.StartDateTime = Now
        rptChanges.ReportCriteria.EndDateTime = Now
        rptChanges.ReportCriteria.SynergyImportID = SynergyImportID
        Dim stream As System.IO.MemoryStream = rptChanges.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        If stream IsNot Nothing AndAlso stream.Length > 0 Then
          Dim RetInfo As New ReportFileInfo
          RetInfo.FileStream = stream
          RetInfo.FileName = "SynergyChanges.xlsx"
          Dim TempDoc As New Singular.Documents.TemporaryDocument(RetInfo.FileName, RetInfo.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "Document is empty"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Import not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function AcknowledgeScheduleChanges(SchEvtCh As OBLib.Rooms.SchEvtCh) As Singular.Web.Result
    Try
      Dim wr As Singular.SaveHelper = SchEvtCh.TrySave(GetType(OBLib.Rooms.SchEvtChList))
      If wr.Success Then
        Return New Singular.Web.Result(wr.Success) With {.Data = SchEvtCh}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Production "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function GetProductionList(ProductionID As Integer?, IsOBView As Boolean, IsRoomView As Boolean) As Singular.Web.Result
    Try
      Dim list As OBLib.Productions.ProductionList = OBLib.Productions.ProductionList.GetProductionList(ProductionID, IsOBView, IsRoomView)
      Return New Singular.Web.Result(True) With {.Data = list}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveProduction(Production As OBLib.Productions.Production) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.Productions.ProductionList
      prdl.Add(Production)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveManualProduction(ManualProduction As OBLib.Productions.Base.ManualProduction) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.Productions.Base.ManualProductionList
      prdl.Add(ManualProduction)
      ManualProduction.SetCreationTypeID(OBLib.CommonData.Enums.CreationType.Manual)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

#End Region

#Region " AdHoc "

  'Public Property CurrentAdHocBooking As OBLib.AdHoc.AdHocBooking 'Implements ControlInterfaces(Of ModelType).IEditProduction.CurrentProduction

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveCurrentAdHocBooking(adHocBookingToSave As OBLib.AdHoc.Old.AdHocBooking) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.Old.AdHocBookingList
      prdl.Add(adHocBookingToSave)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveRSAdHocBooking(rsAdhocBooking As OBLib.AdHoc.Old.AdHocBooking) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.Old.AdHocBookingList
      prdl.Add(rsAdhocBooking)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  Public Overridable Function SaveAdHocBookingSendCommand(adHocBookingToSave As OBLib.AdHoc.AdHocBooking) As Singular.Web.Result
    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.AdHocBookingList
      prdl.Add(adHocBookingToSave)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr
  End Function

  'Public Overridable Function SaveAdHocBookingAreaSendCommand(adHocBookingAreaToSave As OBLib.AdHoc.PSAAdHoc) As Singular.Web.Result
  '  Dim wr As Singular.Web.Result = Nothing
  '  Try
  '    Dim prdl As New OBLib.AdHoc.PSAAdHocList
  '    prdl.Add(adHocBookingAreaToSave)
  '    Dim sh As SaveHelper = TrySave(prdl)
  '    If sh.Success Then
  '      prdl = sh.SavedObject
  '      WebsiteHelpers.SendResourceBookingUpdateNotifications()
  '      wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
  '    Else
  '      wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '    End If
  '  Catch ex As Exception
  '    wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try

  '  Return wr
  'End Function

  Public Overridable Function EditAdHocBookingSendCommand(ByRef AdHocBooking As AdHocBooking, AdHocBookingID As Integer?, ProductionSystemAreaID As Integer?) As Singular.Web.Result
    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As OBLib.AdHoc.AdHocBookingList = OBLib.AdHoc.AdHocBookingList.GetAdHocBookingList(AdHocBookingID, ProductionSystemAreaID)
      If prdl.Count = 1 Then
        AdHocBooking = prdl(0)
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = "Booking could not be fetched"}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr
  End Function

  'Public Overridable Function EditAdHocBookingAreaSendCommand(ByRef PSAAdHoc As PSAAdHoc, AdHocBookingID As Integer?, ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '  Dim wr As Singular.Web.Result = Nothing
  '  Try
  '    Dim prdl As OBLib.AdHoc.PSAAdHocList = OBLib.AdHoc.PSAAdHocList.GetPSAAdHocList(AdHocBookingID, ProductionSystemAreaID)
  '    If prdl.Count = 1 Then
  '      PSAAdHoc = prdl(0)
  '      wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
  '    Else
  '      wr = New Singular.Web.Result(False) With {.ErrorText = "Area could not be fetched"}
  '    End If
  '  Catch ex As Exception
  '    wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try

  '  Return wr
  'End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveAdHocBooking(AdHocBooking As OBLib.AdHoc.AdHocBooking) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.AdHocBookingList
      prdl.Add(AdHocBooking)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  Public Overridable Function GetAdHocBookingListOld(AdHocBookingID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As Singular.Web.Result
    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim ahbl As OBLib.AdHoc.Old.AdHocBookingList = OBLib.AdHoc.Old.AdHocBookingList.GetAdHocBookingList(AdHocBookingID, False)
      If ahbl.Count = 1 Then
        wr = New Singular.Web.Result(True) With {.Data = ahbl}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = "Booking could not be fetched"}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return wr
  End Function

  Public Overridable Function SaveAdHocBookingListOld(AdHocBooking As OBLib.AdHoc.Old.AdHocBooking) As Singular.Web.Result
    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim ahbl As New OBLib.AdHoc.Old.AdHocBookingList
      ahbl.Add(AdHocBooking)
      Dim sh As Singular.SaveHelper = ahbl.TrySave
      If sh.Success Then
        ahbl = OBLib.AdHoc.Old.AdHocBookingList.GetAdHocBookingList(AdHocBooking.AdHocBookingID, False)
        wr = New Singular.Web.Result(True) With {.Data = ahbl}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return wr
  End Function

#End Region

#Region " Room Scheduling "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteAreaFromRoomSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerDeleteAreaFromRoomSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      'Dim ApplyList As New List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)
      'For Each dr As DataRow In cmdProc.Dataset.Tables(0).Rows
      '  ApplyList.Add(New OBLib.Helpers.ResourceHelpers.ApplyResult(dr))
      'Next

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) 'With {.Data = ApplyList}

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ChangeRoomScheduleStatusID(ResourceBookingID As Integer?,
                                                         NewStatusID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerChangeRoomScheduleStatusID]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@NewStatusID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            Singular.Misc.NothingDBNull(NewStatusID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      'Dim ApplyList As New List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)
      'For Each dr As DataRow In cmdProc.Dataset.Tables(0).Rows
      '  ApplyList.Add(New OBLib.Helpers.ResourceHelpers.ApplyResult(dr))
      'Next

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) 'With {.Data = ApplyList}

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ReInstateRoomScheduleSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerReInstateRoomSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function UnReconcileRoomSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    'Ask for Authorisation
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerUnReconcileRoomSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ApplyRoomScheduleTimeChanges(ResourceBooking As ResourceBookingClean) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerApplyRoomScheduleTimeChanges]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@StartDateTimeBuffer",
                                                            "@StartDateTime",
                                                            "@EndDateTime",
                                                            "@EndDateTimeBuffer",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBooking.ResourceBookingID),
                                                            NothingDBNull(ResourceBooking.StartDateTimeBuffer),
                                                            NothingDBNull(ResourceBooking.StartDateTime),
                                                            NothingDBNull(ResourceBooking.EndDateTime),
                                                            NothingDBNull(ResourceBooking.EndDateTimeBuffer),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function AddAreaToRoomSchedule(ResourceBookingID As Integer?,
                                                    RoomScheduleID As Integer?,
                                                    SystemID As Integer?,
                                                    ProductionAreaID As Integer?) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerAddAreaToRoomSchedule]",
                                              New String() {"@ResourceBookingID", "@RoomScheduleID",
                                                            "@SystemID", "@ProductionAreaID",
                                                            "@ModifiedBy", "@CurrentUsername"},
                                              New Object() {ResourceBookingID, RoomScheduleID,
                                                            SystemID, ProductionAreaID,
                                                            OBLib.Security.Settings.CurrentUser.UserID, OBLib.Security.Settings.CurrentUser.LoginName})
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
      cmdProc = cmdProc.Execute()
      Dim NewResourceBookingID As Integer? = cmdProc.DataRow(0)
      If NewResourceBookingID IsNot Nothing Then
        wr = New Singular.Web.Result(True) With {.Data = New With {.ResourceBookingID = NewResourceBookingID, .RoomScheduleID = cmdProc.DataRow(1), .ProductionSystemAreaID = cmdProc.DataRow(2)}}
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = "An error occured while adding your area to the booking"}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function BeforeHRAddedToRoomScheduleChecks(ResourceID As Integer?, ResourceBookingID As Integer?,
                                                                HumanResourceID As Integer?, RoomScheduleID As Integer?,
                                                                CallTimeStart As DateTime?, OnAirStartTime As DateTime, OnAirEndTime As DateTime, WrapTimeEnd As DateTime,
                                                                SystemID As Integer?, ProductionAreaID As Integer?) As Singular.Web.Result

    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerBeforeHRAddedToRoomScheduleChecks]",
                                              New String() {
                                                            "@ResourceID",
                                                            "@ResourceBookingID",
                                                            "@HumanResourceID",
                                                            "@RoomScheduleID",
                                                            "@CallTimeStart",
                                                            "@OnAirStartTime",
                                                            "@OnAirEndTime",
                                                            "@WrapTimeEnd",
                                                            "@SystemID",
                                                            "@ProductionAreaID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceID),
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            Singular.Misc.NothingDBNull(HumanResourceID),
                                                            Singular.Misc.NothingDBNull(RoomScheduleID),
                                                            Singular.Misc.NothingDBNull(CallTimeStart),
                                                            Singular.Misc.NothingDBNull(OnAirStartTime),
                                                            Singular.Misc.NothingDBNull(OnAirEndTime),
                                                            Singular.Misc.NothingDBNull(WrapTimeEnd),
                                                            Singular.Misc.NothingDBNull(SystemID),
                                                            Singular.Misc.NothingDBNull(ProductionAreaID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute
      Dim BeforeAddHRToRoomScheduleResult As New BeforeAddHRToRoomScheduleResult(cmdProc.Dataset)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) With {.Data = BeforeAddHRToRoomScheduleResult}

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveRoomScheduleArea(RoomScheduleArea As OBLib.Scheduling.Rooms.RoomScheduleArea,
                                                   Production As OBLib.Productions.Production,
                                                   AdHocBooking As OBLib.AdHoc.AdHocBooking) As Singular.Web.Result

    Dim productionSaveResult As Singular.Web.Result = Nothing
    If Production IsNot Nothing Then
      Production.CheckAllRules()
      productionSaveResult = SaveProduction(Production)
      If productionSaveResult.Success Then
        Production = productionSaveResult.Data
        RoomScheduleArea.ProductionID = Production.ProductionID
        RoomScheduleArea.AdHocBookingID = Nothing
      End If
    Else
      productionSaveResult = New Singular.Web.Result(True)
    End If

    Dim adHocSaveResult As Singular.Web.Result = Nothing
    If AdHocBooking IsNot Nothing Then
      AdHocBooking.CheckAllRules()
      adHocSaveResult = SaveAdHocBooking(AdHocBooking)
      If adHocSaveResult.Success Then
        AdHocBooking = adHocSaveResult.Data
        RoomScheduleArea.ProductionID = Nothing
        RoomScheduleArea.AdHocBookingID = AdHocBooking.AdHocBookingID
      End If
    Else
      adHocSaveResult = New Singular.Web.Result(True)
    End If

    Dim roomScheduleSaveResult As Singular.Web.Result
    Dim saveHelper As Singular.Web.SaveHelper = Nothing
    If productionSaveResult.Success And adHocSaveResult.Success Then
      Dim rsl As New RoomScheduleAreaList
      rsl.Add(RoomScheduleArea)
      RoomScheduleArea.CheckAllRules()
      saveHelper = TrySave(rsl)
      If saveHelper.Success Then
        rsl = saveHelper.SavedObject
        rsl = OBLib.Scheduling.Rooms.RoomScheduleAreaList.GetRoomScheduleAreaList(rsl(0).RoomScheduleID, rsl(0).ProductionSystemAreaID)
        roomScheduleSaveResult = New Singular.Web.Result(True) With {.Data = rsl(0)}
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        roomScheduleSaveResult = New Singular.Web.Result(False) With {.ErrorText = saveHelper.ErrorText}
      End If
    Else
      roomScheduleSaveResult = New Singular.Web.Result(False) With {.ErrorText = "Event could not be saved"}
    End If

    Return roomScheduleSaveResult

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteRoomScheduleAreas(ProductionSystemAreaIDs As List(Of Integer), PermamentDelete As Boolean) As Singular.Web.Result
    Try
      Dim rsal As OBLib.Scheduling.Rooms.RoomScheduleAreaList = OBLib.Scheduling.Rooms.RoomScheduleAreaList.GetRoomScheduleAreaList(ProductionSystemAreaIDs)
      For Each rsa As RoomScheduleArea In rsal
        rsa.SetPermanentDelete(PermamentDelete)
      Next
      rsal.Clear()
      Dim saveResult As Singular.SaveHelper = rsal.TrySave()
      If saveResult.Success Then
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = saveResult.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function AddAreaToRoomSchedules(RoomSchedules As List(Of RSResourceBooking)) As Singular.Web.Result
    Try
      For Each bkng As RSResourceBooking In RoomSchedules
        bkng.AddCurrentUserArea()
      Next
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function UnReconcileRoomScheduleArea(RoomScheduleArea As OBLib.Scheduling.Rooms.RoomScheduleArea) As Singular.Web.Result

    Dim roomScheduleSaveResult As Singular.Web.Result
    Dim saveHelper As Singular.SaveHelper = Nothing
    RoomScheduleArea.UnReconcile()
    RoomScheduleArea.CheckAllRules()
    saveHelper = RoomScheduleArea.TrySave(GetType(RoomScheduleAreaList))
    If saveHelper.Success Then
      RoomScheduleArea = saveHelper.SavedObject
      'rsl(0).AfterRoomScheduleAreaSaved()
      RoomScheduleArea = OBLib.Scheduling.Rooms.RoomScheduleAreaList.GetRoomScheduleAreaList(RoomScheduleArea.RoomScheduleID, RoomScheduleArea.ProductionSystemAreaID).FirstOrDefault
      roomScheduleSaveResult = New Singular.Web.Result(True) With {.Data = RoomScheduleArea}
      'WebsiteHelpers.SendResourceBookingUpdateNotifications()
    Else
      roomScheduleSaveResult = New Singular.Web.Result(False) With {.ErrorText = saveHelper.ErrorText}
    End If

    Return roomScheduleSaveResult

  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveRoomSchedule(RoomSchedule As OBLib.Scheduling.Rooms.RoomSchedule) As Singular.Web.Result

    RoomSchedule.CheckAllRules()
    Dim saveHelper As Singular.SaveHelper = RoomSchedule.TrySave(GetType(OBLib.Scheduling.Rooms.RoomScheduleList))
    If saveHelper.Success Then
      RoomSchedule = saveHelper.SavedObject
      Return New Singular.Web.Result(True) With {.Data = RoomSchedule}
      'WebsiteHelpers.SendResourceBookingUpdateNotifications()
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = saveHelper.ErrorText}
    End If

  End Function

#End Region

#Region " Slugs "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetSlugsForGenRef(genRef As Int64?) As Singular.Web.Result
    Try
      If genRef IsNot Nothing Then
        Try
          OBLib.ServiceHelpers.SlugServiceHelpers.RefreshSlugs(genRef)
          Dim fetchedList As SlugItemList = OBLib.Slugs.SlugItemList.GetSlugItemList(genRef)
          Return New Singular.Web.Result(True) With {.Data = fetchedList}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no genref provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Equipment Scheduling "

  '<WebCallable(LoggedInOnly:=True)>
  'Public Function EditEquipmentSchedule(EquipmentScheduleID As Integer?, ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '  Try
  '    Dim list As EquipmentScheduleList = FetchEquipmentSchedule(EquipmentScheduleID, ProductionSystemAreaID)
  '    If list.Count = 1 Then
  '      Return New Singular.Web.Result(True) With {.Data = list(0)}
  '    Else
  '      Return New Singular.Web.Result(False) With {.ErrorText = "Could not find booking"}
  '    End If
  '  Catch ex As Exception
  '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try
  'End Function

  'Private Function FetchEquipmentSchedule(EquipmentScheduleID As Integer?, ProductionSystemAreaID As Integer?) As EquipmentScheduleList
  '  Return OBLib.SatOps.EquipmentScheduleList.GetEquipmentScheduleList(EquipmentScheduleID, ProductionSystemAreaID)
  'End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveEquipmentFeed(EquipmentFeed As EquipmentFeed) As Singular.Web.Result
    Try
      EquipmentFeed.CheckAllRules()
      Dim eqSaveResult As Singular.SaveHelper = EquipmentFeed.TrySave(GetType(OBLib.SatOps.EquipmentFeedList))
      If eqSaveResult.Success Then
        'Dim ReasonForChange = EquipmentSchedule.ChangeDescription
        EquipmentFeed = eqSaveResult.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        EquipmentFeed.SetOriginalStatus()
        EquipmentFeed.IsProcessing = False
        EquipmentFeed.ChangeDescriptionRequired = False
        EquipmentFeed.ChangeDescription = ""
        EquipmentFeed.MarkClean()
        Return New Singular.Web.Result(True) With {.Data = EquipmentFeed}
      Else
        Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = eqSaveResult.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveEquipmentMaintenance(EquipmentMaintenance As EquipmentMaintenance) As Singular.Web.Result
    Try
      EquipmentMaintenance.CheckAllRules()
      Dim eqSaveResult As Singular.SaveHelper = EquipmentMaintenance.TrySave(GetType(OBLib.SatOps.EquipmentMaintenanceList))
      If eqSaveResult.Success Then
        'Dim ReasonForChange = EquipmentSchedule.ChangeDescription
        EquipmentMaintenance = eqSaveResult.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        EquipmentMaintenance.SendChangeNotification()
        EquipmentMaintenance.SetOriginalStatus()
        EquipmentMaintenance.IsProcessing = False
        EquipmentMaintenance.ChangeDescriptionRequired = False
        EquipmentMaintenance.ChangeDescription = ""
        EquipmentMaintenance.MarkClean()
        Return New Singular.Web.Result(True) With {.Data = EquipmentMaintenance}
      Else
        Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = eqSaveResult.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function EquipmentAllocationRequest(allocationRequest As OBLib.Equipment.EquipmentAllocator) As Singular.Web.Result
    Try
      If allocationRequest IsNot Nothing Then
        Try
          Dim res As Singular.Web.Result = allocationRequest.AllocateEquipment()
          res.Data = allocationRequest
          WebsiteHelpers.SendResourceBookingUpdateNotifications()
          WebsiteHelpers.SendUnsentNotifications()
          Return res
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message, .Data = allocationRequest}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received", .Data = allocationRequest}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveEquipmentScheduleBasic(EquipmentScheduleBasic As OBLib.Equipment.EquipmentScheduleBasic) As Singular.Web.Result
    Try
      If EquipmentScheduleBasic IsNot Nothing Then
        Try
          EquipmentScheduleBasic.AllocateEquipment()
          Dim res As Singular.Web.Result = EquipmentScheduleBasic.ImportResult
          res.Data = EquipmentScheduleBasic
          Return res
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message, .Data = EquipmentScheduleBasic}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received", .Data = EquipmentScheduleBasic}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function EquipmentAllocationVenueRequest(allocationRequest As OBLib.Equipment.EquipmentAllocatorVenue) As Singular.Web.Result
    Try
      If allocationRequest IsNot Nothing Then
        Try
          Dim res As Singular.Web.Result = allocationRequest.AllocateEquipment()
          res.Data = allocationRequest
          WebsiteHelpers.SendResourceBookingUpdateNotifications()
          WebsiteHelpers.SendUnsentNotifications()
          Return res
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message, .Data = allocationRequest}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received", .Data = allocationRequest}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ChangeEquipmentScheduleStatusID(ResourceBookingID As Integer?,
                                                         NewStatusID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerChangeEquipmentScheduleStatusID]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@NewStatusID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            Singular.Misc.NothingDBNull(NewStatusID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ReInstateEquipmentSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    'Check Clashes
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerReInstateEquipmentSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function UnReconcileEquipmentSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    'Ask for Authorisation
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerUnReconcileEquipmentSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteEquipmentBookings(ResourceBookingIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If ResourceBookingIDs.Count > 0 Then
        Dim cmdProc As New Singular.CommandProc("[DelProcsWeb].[delEquipmentSchedules]",
                                        New String() {
                                                      "@ResourceBookingIDs",
                                                      "@ModifiedBy"
                                                     },
                                        New Object() {
                                                      OBLib.OBMisc.IntegerListToXML(ResourceBookingIDs),
                                                      OBLib.Security.Settings.CurrentUserID
                                                     })
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmdProc = cmdProc.Execute
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      End If
      Return New Singular.Web.Result(True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function AddFeedGenRef(ByVenueAndDate As Boolean, GenRefNumber As Int64?) As Singular.Web.Result

    'Dim sh As Singular.Web.Result = Nothing
    Try
      Dim GenRefNumbers As New List(Of Int64)
      Dim fpl As New FeedProductionList
      If ByVenueAndDate Then
        'Get Events
        Dim eavl As OBLib.Equipment.EquipmentAllocatorVenueList = OBLib.Equipment.EquipmentAllocatorVenueList.GetEquipmentAllocatorVenueList(Nothing, Nothing, GenRefNumber,
                                                                                                                                             True, True, True,
                                                                                                                                             1, 100, "ScheduleDateTime",
                                                                                                                                             True)
        Dim importedEvents As OBLib.Equipment.EquipmentAllocatorEventList = eavl(0).EquipmentAllocatorEventList
        'Create Missing Productions
        OBLib.Helpers.SynergyHelper.CheckGenRefs(OBLib.Security.Settings.CurrentUserID, importedEvents.Select(Function(d) d.GenRefNumber).ToList)
        'Refresh the Productions
        eavl = OBLib.Equipment.EquipmentAllocatorVenueList.GetEquipmentAllocatorVenueList(Nothing, Nothing, GenRefNumber,
                                                                                          True, True, True,
                                                                                          1, 100, "ScheduleDateTime",
                                                                                          True)
        'Add the Productions
        For Each d As OBLib.Equipment.EquipmentAllocatorEvent In eavl(0).EquipmentAllocatorEventList
          Dim fp As FeedProduction = fpl.AddNew
          fp.SynergyGenRefNo = d.GenRefNumber
          fp.ProductionID = d.ProductionID
          fp.ProductionDescription = d.Title
        Next
      Else
        Dim importedEvents As OBLib.Equipment.EquipmentAllocatorList = OBLib.Equipment.EquipmentAllocatorList.GetEquipmentAllocatorList(Nothing, Nothing, GenRefNumber,
                                                                                                                                        True, True, True,
                                                                                                                                        1, 100, "ScheduleDateTime",
                                                                                                                                        True)
        'Create Missing Productions
        OBLib.Helpers.SynergyHelper.CheckGenRefs(OBLib.Security.Settings.CurrentUserID, importedEvents.Select(Function(d) d.GenRefNumber).ToList)
        'Refresh the Productions
        importedEvents = OBLib.Equipment.EquipmentAllocatorList.GetEquipmentAllocatorList(Nothing, Nothing, GenRefNumber,
                                                                                          True, True, True,
                                                                                          1, 100, "ScheduleDateTime",
                                                                                          True)
        'Add the Productions
        For Each d As OBLib.Equipment.EquipmentAllocator In importedEvents
          Dim fp As FeedProduction = fpl.AddNew
          fp.SynergyGenRefNo = d.GenRefNumber
          fp.ProductionID = d.ProductionID
          fp.ProductionDescription = d.Title
        Next
      End If
      Return New Singular.Web.Result(True) With {.Data = New With {.FeedProductionList = fpl}}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "Unknown Error"}

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ImportEquipmentFeed(EquipmentID As Integer?, GenRefNumbers As List(Of Int64)) As Singular.Web.Result
    Try
      Dim request As New Singular.CommandProc("CmdProcs.cmdImportEquipmentFeed",
                                              New String() {"@EquipmentID", "@GenRefNumbers", "@CurrentUserID"},
                                              New Object() {EquipmentID, OBLib.OBMisc.Int64ListToXML(GenRefNumbers), OBLib.Security.Settings.CurrentUserID})
      request.UseTransaction = True
      request = request.Execute()
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      WebsiteHelpers.SendUnsentNotifications()
      Return New Singular.Web.Result(True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  '<WebCallable(LoggedInOnly:=True)>
  'Public Overridable Function PrintSatOpsBookingReport(EquipmentScheduleID As Integer?, ProductionSystemAreaID As Integer?) As Singular.Web.Result

  '  Dim rpt As New OBWebReports.SatOpsReports.EquipmentBookingReport
  '  rpt.ReportCriteria.EquipmentIDs = New List(Of Integer)
  '  rpt.ReportCriteria.EquipmentScheduleIDs = New List(Of Integer)
  '  rpt.ReportCriteria.EquipmentScheduleIDs.Add(EquipmentScheduleID)
  '  Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
  '  'SendFile(rfi.FileName, rfi.FileBytes)
  '  Dim td As New TemporaryDocument
  '  td.SetDocument(rfi.FileBytes, rfi.FileName, False)
  '  Dim gid As Guid = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(td)
  '  Return New Singular.Web.Result(True) With {.Data = gid.ToString}

  'End Function

#End Region

#Region " Equipment "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveEquipment(Equipment As OBLib.Equipment.Equipment) As Singular.Web.Result
    Try
      Equipment.IsProcessing = False
      Dim sh As Singular.SaveHelper = Equipment.TrySave(GetType(OBLib.Equipment.EquipmentList))
      If sh.Success Then
        Equipment = sh.SavedObject
        Return New Singular.Web.Result(True) With {.Data = Equipment}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteEquipment(EquipmentList As OBLib.Equipment.EquipmentList) As Singular.Web.Result
    Try
      EquipmentList.Clear()
      Dim sh As Singular.SaveHelper = EquipmentList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Copy Bookings "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function CopyResourceBookingToSpecificDates(setting As CopySetting) As Singular.Web.Result
    Try
      If setting IsNot Nothing Then
        Dim l As New List(Of CopyResult)
        l = setting.Submit()
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = l}
      Else
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received", .Data = setting}
      End If
    Catch ex As Exception
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Off Periods "

  Public Shared Function EditOffPeriod(ByVal HumanResourceOffPeriodID As Integer?) As Singular.Web.Result
    Try
      Dim opl As HumanResourceOffPeriodList = OBLib.HR.HumanResourceOffPeriodList.GetHumanResourceOffPeriodList(HumanResourceOffPeriodID)
      If opl.Count = 1 Then
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = opl(0)}
      ElseIf opl.Count > 1 Then
        Return New Singular.Web.Result(False) With {.ErrorText = "Too many results returned"}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Could not retrieve the Off Period"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Public Shared Function SaveOffPeriod(HumanResourceOffPeriod As HumanResourceOffPeriod) As Singular.Web.Result
    Try
      Dim opl As New HumanResourceOffPeriodList
      opl.Add(HumanResourceOffPeriod)
      Dim sh As SaveHelper = opl.TrySave
      If sh.Success Then
        opl = sh.SavedObject
        HumanResourceOffPeriod = opl(0)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = opl(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteHumanResourceOffPeriods(HumanResourceOffPeriodIDs As List(Of Integer)) As Singular.Web.Result
    Try
      Dim DeleteResults As New List(Of Singular.Web.Result)
      If HumanResourceOffPeriodIDs.Count > 0 Then
        Dim IDsXML As String = OBLib.OBMisc.IntegerListToXML(HumanResourceOffPeriodIDs)
        Dim offPeriodList As OBLib.HR.HumanResourceOffPeriodList = OBLib.HR.HumanResourceOffPeriodList.GetHumanResourceOffPeriodList(IDsXML)
        For Each offP As OBLib.HR.HumanResourceOffPeriod In offPeriodList
          If offP.CanAuthoriseLeave Then
            Dim tempList As New OBLib.HR.HumanResourceOffPeriodList
            tempList.Add(offP)
            tempList.Clear()
            Dim sh As SaveHelper = tempList.TrySave
            If sh.Success Then
              DeleteResults.Add(New Singular.Web.Result(True) With {.ErrorText = offP.OffReason & " has been deleted successfully"})
            Else
              DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.OffReason & " could not be deleted " & sh.ErrorText})
            End If
          Else
            DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.OffReason & " could not be deleted: " & " Authorised Leave cannot be deleted"})
          End If
        Next
        offPeriodList.Clear()
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.ErrorText = "Deletions Complete", .Data = DeleteResults}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "Delete not attempted"}
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function UnAuthoriseLeave(HumanResourceOffPeriodID As Integer) As Singular.Web.Result
    Try
      If HumanResourceOffPeriodID > 0 Then
        Dim offPeriodList As OBLib.HR.HumanResourceOffPeriodList = OBLib.HR.HumanResourceOffPeriodList.GetHumanResourceOffPeriodList(HumanResourceOffPeriodID)
        Dim offP As OBLib.HR.HumanResourceOffPeriod = offPeriodList(0)
        If offP.IsAuthorsied Then
          offP.UnAuthorise()
          Dim sh As SaveHelper = offPeriodList.TrySave
          If sh.Success Then
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "OffPeriod is not authorised"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "OffPeriod items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "UnAthorisation not attempted"}
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function GenerateOffPeriod(offPeriod As HumanResourceOffPeriod) As Singular.Web.Result
    Try
      offPeriod.GenerateOffPeriod()
      Return New Singular.Web.Result(True) With {.Data = offPeriod}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Public Shared Function SaveAvailabilityTemplate(HumanResourceUnAvailabilityTemplate As HumanResourceUnAvailabilityTemplate) As Singular.Web.Result
    Try
      Dim sh As SaveHelper = HumanResourceUnAvailabilityTemplate.HumanResourceUnAvailabilityList.TrySave
      If sh.Success Then
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Secondments "

  Friend Sub EditSecondment(ByRef humanResourceSecondment As HumanResourceSecondment, ByRef CommandArgs As CommandArgs)
    Try
      Dim opl As HumanResourceSecondmentList = OBLib.HR.HumanResourceSecondmentList.GetHumanResourceSecondmentList(CommandArgs.ClientArgs.HumanResourceSecondmentID)
      If opl.Count = 1 Then
        humanResourceSecondment = opl(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      ElseIf opl.Count > 1 Then
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Too many results returned"}
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Could not retrieve the secondment"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Friend Sub SaveSecondment(ByRef humanResourceSecondment As HumanResourceSecondment, ByRef CommandArgs As CommandArgs)
    Try
      Dim opl As New HumanResourceSecondmentList
      opl.Add(humanResourceSecondment)
      Dim sh As SaveHelper = TrySave(opl)
      If sh.Success Then
        opl = sh.SavedObject
        humanResourceSecondment = opl(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteSecondments(HumanResourceSecondmentIDs As List(Of Integer)) As Singular.Web.Result
    Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If HumanResourceSecondmentIDs.Count > 0 Then
        Dim IDsXML As String = OBLib.OBMisc.IntegerListToXML(HumanResourceSecondmentIDs)
        Dim secondmentList As OBLib.HR.HumanResourceSecondmentList = OBLib.HR.HumanResourceSecondmentList.GetHumanResourceSecondmentList(IDsXML)
        For Each offP As OBLib.HR.HumanResourceSecondment In secondmentList
          If Not offP.IsAuthorised Then
            Dim tempList As New OBLib.HR.HumanResourceSecondmentList
            tempList.Add(offP)
            tempList.Clear()
            Dim sh As SaveHelper = TrySave(tempList)
            If sh.Success Then
              DeleteResults.Add(New Singular.Web.Result(True) With {.ErrorText = offP.Description & " has been deleted successfully"})
            Else
              DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.Description & " could not be deleted " & sh.ErrorText})
            End If
          Else
            DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.Description & " could not be deleted: " & " Authorised Secondments cannot be deleted"})
          End If
        Next
        secondmentList.Clear()
        Return New Singular.Web.Result(True) With {.ErrorText = "Deletions Complete", .Data = DeleteResults}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - TBC Productions "

  '<Browsable(False)>
  'Public Property ProductionHumanResourceTBCList As New OBLib.HR.ProductionHumanResourcesTBCList

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveProductionHumanResourceTBC(list As OBLib.HR.ProductionHumanResourcesTBCList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If list.Count > 0 Then
        Dim sh As SaveHelper = TrySave(list)
        If sh.Success Then
          Return New Singular.Web.Result(True)
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = Singular.Debug.RecurseExceptionMessage(sh.Error, True)}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Skills "

  Friend Sub EditHumanResourceSkill(ByRef SkillInstance As HumanResourceSkill, CommandArgs As Singular.Web.CommandArgs)
    Try
      Dim tempSkill As HumanResourceSkill = Nothing
      Dim ID As Integer? = CommandArgs.ClientArgs.HumanResourceSKillID
      tempSkill = FetchHumanResourceSkill(ID)
      If tempSkill IsNot Nothing Then
        SkillInstance = tempSkill
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Skill could not be retrieved"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Private Function FetchHumanResourceSkill(HumanResourceSkillID As Integer?) As HumanResourceSkill

    Dim skillList As HumanResourceSkillList = OBLib.HR.HumanResourceSkillList.GetHumanResourceSkillList(Nothing, HumanResourceSkillID)
    If skillList.Count = 1 Then
      Return skillList(0)
    End If
    Return Nothing

  End Function

  Friend Sub SaveHumanResourceSkill(ByRef SkillInstance As HumanResourceSkill, CommandArgs As Singular.Web.CommandArgs)

    Dim skillList As New HumanResourceSkillList
    skillList.Add(SkillInstance)
    skillList.CheckAllRules()
    If skillList.IsValid Then
      Dim sh As SaveHelper = TrySave(skillList)
      If sh.Success Then
        skillList = sh.SavedObject
        SkillInstance = skillList(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Skill is not valid"}
    End If

  End Sub

  <WebCallable(LoggedInOnly:=True)>
  Public Function DeleteHumanResourceSkills(HumanResourceSkillIDs As List(Of Integer)) As Singular.Web.Result
    Try
      Dim DeleteResults As New List(Of Singular.Web.Result)
      If HumanResourceSkillIDs.Count > 0 Then
        Dim IDsXML As String = OBLib.OBMisc.IntegerListToXML(HumanResourceSkillIDs)
        Dim skillList As OBLib.HR.HumanResourceSkillList = OBLib.HR.HumanResourceSkillList.GetHumanResourceSkillList(IDsXML)
        For Each skill As OBLib.HR.HumanResourceSkill In skillList
          If skill.BookingCount = 0 Then
            Dim tempList As New OBLib.HR.HumanResourceSkillList
            tempList.Add(skill)
            tempList.Clear()
            Dim sh As SaveHelper = TrySave(tempList)
            If sh.Success Then
              DeleteResults.Add(New Singular.Web.Result(True) With {.ErrorText = skill.Discipline & " deleted successfully"})  '(skill.Discipline, "Deleted Successfully", "Deleted Successfully", "alert alert-success animated fast slideInRight go", "fa fa-check-square-o"))
            Else
              DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}) '(skill.Discipline, "Deletion Failed", sh.ErrorText, "alert alert-danger animated fast slideInRight go", "fa fa-exclamation-circle"))
            End If
          Else
            DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = skill.Discipline & " has already been booked and cannot be deleted"}) '(skill.Discipline, "Deletion Failed", "This skill has already been booked on events and cannot be deleted", "alert alert-danger animated fast slideInRight go", "fa fa-exclamation-circle"))
          End If
        Next
        skillList.Clear()
        Return New Singular.Web.Result(True) With {.ErrorText = "Deletions Complete", .Data = DeleteResults}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "Delete not attempted"}
  End Function

#End Region

#Region " HR - Documents "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHRDocuments(list As OBLib.HR.HRDocumentList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If list.Count > 0 Then
        Dim sh As SaveHelper = TrySave(list)
        If sh.Success Then
          Return New Singular.Web.Result(True)
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Public Sub SaveHRDocumentsByCommand(DocumentList As OBLib.HR.HRDocumentList, CommandArgs As Singular.Web.CommandArgs)
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If DocumentList.Count > 0 Then
        Dim sh As SaveHelper = TrySave(DocumentList)
        If sh.Success Then
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        Else
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Public Sub DeleteHRDocumentsByCommand(DocumentList As OBLib.HR.HRDocumentList, CommandArgs As Singular.Web.CommandArgs)
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      Dim sh As SaveHelper = TrySave(DocumentList)
      If sh.Success Then
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Public Sub FetchHRDocumentsByCommand(HumanResourceID As Integer?, ByRef DocumentList As OBLib.HR.HRDocumentList, CommandArgs As Singular.Web.CommandArgs)
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If HumanResourceID IsNot Nothing Then
        DocumentList = OBLib.HR.HRDocumentList.GetHRDocumentList(HumanResourceID)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Human Resource not provided"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

#End Region

#Region " HR - Areas "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHRAreas(list As OBLib.HR.HRSystemSelectList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If list.Count > 0 Then
        Dim sh As SaveHelper = TrySave(list)
        If sh.Success Then
          list = sh.SavedObject
          Return New Singular.Web.Result(True) With {.Data = list}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Production Bookings "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ProductionHRRemoveResourceBooking(ProductionHRResourceBookingID As Integer?) As Singular.Web.Result

    If Not IsNullNothing(ProductionHRResourceBookingID) Then
      Try
        Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdProductionHRRemoveResourceBooking]")
        cmdProc.Parameters.AddWithValue("@ProductionHRResourceBookingID", ProductionHRResourceBookingID)
        cmdProc.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmdProc = cmdProc.Execute

        WebsiteHelpers.SendResourceBookingUpdateNotifications()

        Return New Singular.Web.Result(True)

      Catch ex As Exception
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = "Could not identify the booking to be deleted"}
    End If

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ProductionHRAddResourceBooking(applyResult As ApplyResult) As Singular.Web.Result

    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdProductionHRAddResourceBooking]")
      cmdProc.Parameters.AddWithValue("@ResourceID", applyResult.ResourceID)
      cmdProc.Parameters.AddWithValue("@ResourceBookingID", applyResult.ResourceBookingID)
      'cmdProc.Parameters.AddWithValue("@BookingDescription", applyResult.BookingDescription)
      cmdProc.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(applyResult.ProductionSystemAreaID))
      cmdProc.Parameters.AddWithValue("@DisciplineID", applyResult.DisciplineID)
      cmdProc.Parameters.AddWithValue("@HumanResourceID", applyResult.HumanResourceID)
      cmdProc.Parameters.AddWithValue("@AddAnywayReason", applyResult.AddAnywayReason)
      cmdProc.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      'phr
      Dim paramProductionHumanResourceID As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      paramProductionHumanResourceID.Name = "@ProductionHumanResourceID"
      paramProductionHumanResourceID.SqlType = SqlDbType.Int
      paramProductionHumanResourceID.Value = NothingDBNull(applyResult.ProductionHumanResourceID)
      paramProductionHumanResourceID.Direction = ParameterDirection.InputOutput
      cmdProc.Parameters.Add(paramProductionHumanResourceID)

      'ph
      Dim paramProductionHRID As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      paramProductionHRID.Name = "@ProductionHRID"
      paramProductionHRID.SqlType = SqlDbType.Int
      paramProductionHRID.Value = NothingDBNull(Nothing)
      paramProductionHRID.Direction = ParameterDirection.InputOutput
      cmdProc.Parameters.Add(paramProductionHRID)

      'rb
      Dim paramRBID As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      paramRBID.Name = "@ProductionHRResourceBookingID"
      paramRBID.SqlType = SqlDbType.Int
      paramRBID.Value = NothingDBNull(Nothing)
      paramRBID.Direction = ParameterDirection.InputOutput
      cmdProc.Parameters.Add(paramRBID)

      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()

      If paramProductionHRID IsNot Nothing AndAlso paramRBID IsNot Nothing Then
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False)
      End If

    Catch ex As Exception
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

#End Region

#Region " HR - Schedules "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DownloadMonthlyScheduleStateless(HumanResourceID As Integer, StartDate As Date, EndDate As Date, MonthInt As Integer) As Singular.Web.Result
    Try
      If HumanResourceID > 0 Then
        Try
          Dim d As Date = Singular.Dates.DateMonthEnd(StartDate).AddDays(1)
          Dim schedule As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
          schedule.ReportCriteria.HumanResourceIDs.Add(HumanResourceID)
          schedule.ReportCriteria.StartDate = Singular.Dates.DateMonthStart(d)
          schedule.ReportCriteria.EndDate = Singular.Dates.DateMonthEnd(d)
          Dim rfi = schedule.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no document provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function PrintServicesFreelancerTimesheet(HumanResourceID As Integer, EndDate As Date) As Singular.Web.Result
    Try
      If HumanResourceID > 0 Then
        Try
          'Dim d As Date = Singular.Dates.DateMonthEnd(EndDate) '.AddDays(1)
          Dim rpt As New OBWebReports.HumanResourceReports.HRTimesheet
          rpt.ReportCriteria.HumanResourceID = HumanResourceID
          rpt.ReportCriteria.EndDate = EndDate
          Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no document provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Timesheets "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetHumanResourceTimesheet(HumanResourceTimesheetID As Integer?) As Singular.Web.Result
    Try
      If HumanResourceTimesheetID > 0 AndAlso HumanResourceTimesheetID IsNot Nothing Then
        Dim hrtl As OBLib.HR.Timesheets.HumanResourceTimesheetList = OBLib.HR.Timesheets.HumanResourceTimesheetList.GetHumanResourceTimesheetList(HumanResourceTimesheetID)
        If hrtl.Count = 1 Then
          Return New Singular.Web.Result(True) With {.Data = hrtl(0)}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not find timesheet"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no timesheet provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHumanResourceTimesheet(HumanResourceTimesheet As OBLib.HR.Timesheets.HumanResourceTimesheet) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = HumanResourceTimesheet.TrySave(GetType(OBLib.HR.Timesheets.HumanResourceTimesheetList))
      If sh.Success Then
        Return New Singular.Web.Result(False) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Shifts "

#Region " Playout "

  'MCR--------------------
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function EditPlayoutOpsShiftMCR(HumanResourceShiftID As Integer?) As Singular.Web.Result
    Dim result As Singular.Web.Result = FetchPlayoutOpsShiftMCR(HumanResourceShiftID)
    Return result
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function SavePlayoutOpsShiftMCRStateless(PlayoutShift As OBLib.Shifts.PlayoutOperations.MCRShift) As Singular.Web.Result
    Dim res As Singular.Web.Result = SavePlayoutOpsShiftMCR(PlayoutShift)
    Return res
  End Function

  Friend Shared Function SavePlayoutOpsShiftMCR(ByRef ShiftInstance As OBLib.Shifts.PlayoutOperations.MCRShift) As Singular.Web.Result
    Try
      Dim tempShiftList As New MCRShiftList
      tempShiftList.Add(ShiftInstance)
      Dim sh As Singular.SaveHelper = tempShiftList.TrySave
      If sh.Success Then
        tempShiftList = sh.SavedObject
        ShiftInstance = tempShiftList(0)
        'WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = ShiftInstance}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Private Shared Function FetchPlayoutOpsShiftMCR(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim shiftList As OBLib.Shifts.PlayoutOperations.MCRShiftList = OBLib.Shifts.PlayoutOperations.MCRShiftList.GetMCRShiftList(Nothing, Nothing, Nothing, Nothing, Nothing, HumanResourceShiftID)
      If shiftList.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Shift could not be found"}
      End If
      Return Nothing
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeletePlayoutOpsMCRShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim supervisorShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) supervisorShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(supervisorShiftIDs)
        Dim shiftList As OBLib.Shifts.PlayoutOperations.MCRShiftList = OBLib.Shifts.PlayoutOperations.MCRShiftList.GetMCRShiftList(xmlString)
        If shiftList.Count = shifts.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True) 'With {.Data = ROResourceBookingHolderList}
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function SavePlayoutOpsShiftSCCR(ByVal SCCRShift As OBLib.Shifts.PlayoutOperations.SCCRShift) As Singular.Web.Result
    Try
      Dim tempShiftList As New SCCRShiftList
      tempShiftList.Add(SCCRShift)
      Dim sh As Singular.SaveHelper = tempShiftList.TrySave
      If sh.Success Then
        tempShiftList = sh.SavedObject
        SCCRShift = tempShiftList(0)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeletePlayoutOpsSCCRShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim supervisorShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) supervisorShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(supervisorShiftIDs)
        Dim shiftList As OBLib.Shifts.PlayoutOperations.SCCRShiftList = OBLib.Shifts.PlayoutOperations.SCCRShiftList.GetSCCRShiftList(xmlString)
        If shiftList.Count = shifts.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True) 'With {.Data = ROResourceBookingHolderList}
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeletePlayoutOpsShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim supervisorShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) supervisorShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(supervisorShiftIDs)
        Dim shiftList As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShiftList = OBLib.Shifts.PlayoutOperations.PlayoutOperationsShiftList.GetPlayoutOperationsShiftList(xmlString)
        If shiftList.Count = shifts.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True) 'With {.Data = ROResourceBookingHolderList}
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function MergeShifts(HumanResourceShiftID1 As Integer?, HumanResourceShiftID2 As Integer?) As Singular.Web.Result
    Try
      If HumanResourceShiftID1 IsNot Nothing AndAlso HumanResourceShiftID2 IsNot Nothing Then
        Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdMergeShifts]",
                                                New String() {"@HumanResourceShiftID1", "@HumanResourceShiftID2", "@CurrentUserID"},
                                                New Object() {HumanResourceShiftID1, HumanResourceShiftID2, OBLib.Security.Settings.CurrentUserID})
        cmdProc.UseTransaction = True
        cmdProc = cmdProc.Execute
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "2 shifts not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SyncShift(HumanResourceShiftID As Integer?) As Singular.Web.Result
    Try
      If HumanResourceShiftID IsNot Nothing Then
        Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdSyncShift]",
                                                New String() {"@HumanResourceShiftID", "@CurrentUserID"},
                                                New Object() {HumanResourceShiftID, OBLib.Security.Settings.CurrentUserID})
        cmdProc.UseTransaction = True
        cmdProc = cmdProc.Execute
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "shift not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " ICR "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function EditICRShiftServerMethod(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Dim result As Singular.Web.Result = FetchICRShift(HumanResourceShiftID)
    Return result

  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function SaveICRShift(ICRShift As OBLib.Shifts.ICR.ICRShift) As Singular.Web.Result
    Try
      Dim tempShiftList As New OBLib.Shifts.ICR.ICRShiftList
      tempShiftList.Add(ICRShift)
      Dim sh As Singular.SaveHelper = tempShiftList.TrySave
      If sh.Success Then
        tempShiftList = sh.SavedObject
        Dim wr As Singular.Web.Result = FetchICRShift(tempShiftList(0).HumanResourceShiftID)
        ICRShift = wr.Data.Shift
        WebsiteHelpers.SendUpdateHRTimesheetRequirementStats(ICRShift.HumanResourceID, ICRShift.ScheduleDate, ICRShift.ResourceID)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = ICRShift}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Private Shared Function FetchICRShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim shiftList As OBLib.Shifts.ICR.ICRShiftList = OBLib.Shifts.ICR.ICRShiftList.GetICRShiftList(HumanResourceShiftID)
      If shiftList.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Shift could not be found"}
      End If
      Return Nothing
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteICRShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim icrShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) icrShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(icrShiftIDs)
        Dim shiftList As OBLib.Shifts.ICR.ICRShiftList = OBLib.Shifts.ICR.ICRShiftList.GetICRShiftList(xmlString)
        If shiftList.Count = shifts.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            'WebsiteHelpers.SendUpdateHRTimesheetRequirementStats(ICRShift.HumanResourceID, ICRShift.ScheduleDate, ICRShift.ResourceID)
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Studio Supervisors "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function SaveStudioSupervisorShift(StudioSupervisorShift As StudioSupervisorShift) As Singular.Web.Result
    Try
      Dim tempShiftList As New StudioSupervisorShiftList
      tempShiftList.Add(StudioSupervisorShift)
      Dim sh As Singular.SaveHelper = tempShiftList.TrySave
      If sh.Success Then
        tempShiftList = sh.SavedObject
        StudioSupervisorShift = tempShiftList(0)
        WebsiteHelpers.SendUpdateHRTimesheetRequirementStats(StudioSupervisorShift.HumanResourceID, StudioSupervisorShift.ScheduleDate, OBLib.Security.Settings.CurrentUserID)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = StudioSupervisorShift}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteStudioSupervisorShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim supervisorShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) supervisorShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(supervisorShiftIDs)
        Dim supervisorShifts As StudioSupervisorShiftList = OBLib.Shifts.Studios.StudioSupervisorShiftList.GetStudioSupervisorShiftList(xmlString)
        If supervisorShifts.Count = shifts.Count Then
          supervisorShifts.Clear()
          Dim sh As SaveHelper = TrySave(supervisorShifts)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True) 'With {.Data = ROResourceBookingHolderList}
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function EditStudioSupervisorShift(HumanResourceShiftID As Integer?) As Singular.Web.Result
    Dim result As Singular.Web.Result = FetchStudioSupervisorShift(HumanResourceShiftID)
    Return result
  End Function

  Private Shared Function FetchStudioSupervisorShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim shiftList As OBLib.Shifts.Studios.StudioSupervisorShiftList = OBLib.Shifts.Studios.StudioSupervisorShiftList.GetStudioSupervisorShiftList(HumanResourceShiftID)
      If shiftList.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Shift could not be found"}
      End If
      Return Nothing
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

#End Region

#Region " Generic "

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveGenericShift(ByVal GenericShift As OBLib.Shifts.GenericShift) As Singular.Web.Result
    Dim sh As Singular.SaveHelper = GenericShift.TrySave(GetType(OBLib.Shifts.GenericShiftList))
    If sh.Success Then
      GenericShift = sh.SavedObject
      Return New Singular.Web.Result(True) With {.Data = GenericShift}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteGenericShifts(bookings As List(Of OBLib.Resources.ResourceBooking)) As Singular.Web.Result
    Try
      If bookings.Count > 0 Then
        Dim shiftIDs As New List(Of Integer)
        bookings.ForEach(Sub(s) shiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(shiftIDs)
        Dim shiftList As OBLib.Shifts.GenericShiftList = OBLib.Shifts.GenericShiftList.GetGenericShiftList(xmlString)
        If shiftList.Count = bookings.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            'WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function


  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function CreateQuickShift(SystemID As Integer?, ProductionAreaID As Integer?,
                                          ShiftTypeID As Integer?, ShiftType As String,
                                          HumanResourceID As Integer?, ResourceID As Integer?, HumanResource As String,
                                          DisciplineID As Integer?, Discipline As String,
                                          StartDateTime As DateTime?, EndDateTime As DateTime?,
                                          ShiftTitle As String) As Singular.Web.Result

    Dim gs As New GenericShift With {
                                      .SystemID = SystemID, .ProductionAreaID = ProductionAreaID,
                                      .ShiftTypeID = ShiftTypeID, .ShiftType = ShiftType,
                                      .HumanResourceID = HumanResourceID, .ResourceID = ResourceID, .HumanResource = HumanResource,
                                      .DisciplineID = DisciplineID, .Discipline = Discipline,
                                      .StartDateTime = StartDateTime, .EndDateTime = EndDateTime,
                                      .ProductionAreaStatusID = 1, .ProductionAreaStatus = "New",
                                      .ResourceBookingDescription = IIf(ShiftTitle.Length = 0, ShiftType, ShiftTitle),
                                      .ScheduleDate = StartDateTime
    }

    gs.UpdateClashes()
    gs.CheckAllRules()

    If gs.IsValid Then
      Dim sh As Singular.SaveHelper = gs.TrySave(GetType(OBLib.Shifts.GenericShiftList))
      If sh.Success Then
        gs = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = gs}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = gs.GetErrorsAsHTMLString}
    End If

  End Function

#End Region

#End Region

#Region " User Profile "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveUserProfileShiftList(shiftList As OBLib.Users.UserProfileShiftList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If shiftList.Count > 0 Then
        Dim sh As SaveHelper = TrySave(shiftList)
        If sh.Success Then
          shiftList = sh.SavedObject
          Return New Singular.Web.Result(True) With {.Data = shiftList}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetUserSchedule(StartDate As DateTime?, EndDate As DateTime?) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If StartDate IsNot Nothing AndAlso EndDate IsNot Nothing Then
        Dim ROResourceBookingHolderList As OBLib.HR.Schedule.ROResourceBookingHolderList
        ROResourceBookingHolderList = OBLib.HR.Schedule.ROResourceBookingHolderList.GetROResourceBookingHolderList(Nothing,
                                                                                                                   OBLib.Security.Settings.CurrentUser.UserID,
                                                                                                                   StartDate,
                                                                                                                   EndDate)
        Return New Singular.Web.Result(True) With {.Data = ROResourceBookingHolderList}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function PrintOBCityTimesheet(Timesheet As OBCityTimesheet) As Singular.Web.Result
    Dim rpt As New OBWebReports.HumanResourceReports.HRTimesheet
    rpt.ReportCriteria.HumanResourceID = Timesheet.HumanResourceID
    rpt.ReportCriteria.EndDate = Timesheet.TMEndDate
    Dim rfi As ReportFileInfo = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
    'Save the Document Temporarily for downloading
    Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
    Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
  End Function

#End Region

#Region " Documents "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DownloadDocumentStateless(DocumentID As Integer) As Singular.Web.Result
    Try
      If DocumentID > 0 Then
        Try
          Dim FileToSend As Singular.Documents.Document = Singular.Documents.Document.GetDocument(DocumentID)
          Dim TempDoc As New Singular.Documents.TemporaryDocument(FileToSend.DocumentName, FileToSend.Document)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no document provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveProductionDocumentsServerMethod(ProductionDocumentList As OBLib.Productions.Correspondence.ProductionDocumentList) As Singular.Web.Result
    Try
      If ProductionDocumentList.Count > 0 Then
        Dim sh As Singular.SaveHelper = ProductionDocumentList.TrySave
        If sh.Success Then
          Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no documents provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Email "

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function PrepareEmailsForSending(EmailBatchID As Integer?, EmailIDsXml As String) As Singular.Web.Result
    Try
      If EmailBatchID > 0 Or EmailIDsXml.Trim.Length > 0 Then
        Try
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEmailsPrepareForSending",
                                                  New String() {"@EmailBatchID",
                                                                "@EmailIDsXml",
                                                                "@CurrentUserID"},
                                                  New Object() {NothingDBNull(EmailBatchID),
                                                                Singular.Strings.MakeEmptyDBNull(EmailIDsXml),
                                                                OBLib.Security.Settings.CurrentUserID})
          cmdProc.CommandTimeout = 0
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
          cmdProc = cmdProc.Execute
          Dim ItemCount As Integer = cmdProc.DataObject
          Return New Singular.Web.Result(True) With {.ErrorText = ItemCount.ToString}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "batch or emails not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DownloadEmailBatchAttachmentsStateless(EmailBatchID As Integer) As Singular.Web.Result
    Try
      If Not IsNullNothing(EmailBatchID) AndAlso EmailBatchID > 0 Then
        Try
          'Fetch the list
          Dim eml As OBLib.Notifications.Email.BatchEmailList = OBLib.Notifications.Email.BatchEmailList.GetBatchEmailList(EmailBatchID, True)
          'Create the Zip File
          Dim FileBytes As List(Of Byte()) = eml.Select(Function(d) d.EmailAttachment.AttachmentData).ToList
          Dim FileNames As List(Of String) = eml.Select(Function(d) d.EmailAttachment.AttachmentName).ToList
          Dim RetInfo As New ReportFileInfo
          Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes.ToArray, FileNames.ToArray)
          Dim ms As New IO.MemoryStream(CompressedFiles)
          RetInfo.FileStream = ms
          RetInfo.FileName = "Batch_" & EmailBatchID.ToString & "_Attachments.zip"
          'Save the Document Temporarily for downloading
          Dim TempDoc As New Singular.Documents.TemporaryDocument(RetInfo.FileName, RetInfo.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no batch provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DownloadEmailAttachmentsStateless(EmailID As Integer) As Singular.Web.Result
    Try
      If Not IsNullNothing(EmailID) AndAlso EmailID > 0 Then
        Try
          'Fetch the list
          Dim idList As New List(Of Integer)
          idList.Add(EmailID)
          Dim eml As OBLib.Notifications.Email.SoberEmailList = OBLib.Notifications.Email.SoberEmailList.GetSoberEmailList(idList, True)
          Dim attachments As SoberEmailAttachmentList = eml(0).SoberEmailAttachmentList
          'Create the Zip File
          Dim FileBytes As List(Of Byte()) = attachments.Select(Function(d) d.AttachmentData).ToList
          Dim FileNames As List(Of String) = attachments.Select(Function(d) d.AttachmentName).ToList
          Dim RetInfo As New ReportFileInfo
          Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes.ToArray, FileNames.ToArray)
          Dim ms As New IO.MemoryStream(CompressedFiles)
          RetInfo.FileStream = ms
          RetInfo.FileName = "Email" & EmailID.ToString & "_Attachments.zip"
          'Save the Document Temporarily for downloading
          Dim TempDoc As New Singular.Documents.TemporaryDocument(RetInfo.FileName, RetInfo.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no email provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DeleteEmailBatches(EmailBatchIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If EmailBatchIDs.Count > 0 Then
        Try
          Dim eml As BatchEmailList = OBLib.Notifications.Email.BatchEmailList.GetBatchEmailList(EmailBatchIDs, False)
          eml.Clear()
          Dim sh As SaveHelper = TrySave(eml)
          If sh.Success Then
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no batches provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DeleteEmails(EmailIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If EmailIDs.Count > 0 Then
        Try
          Dim eml As SoberEmailList = OBLib.Notifications.Email.SoberEmailList.GetSoberEmailList(EmailIDs, False)
          eml.Clear()
          Dim sh As SaveHelper = TrySave(eml)
          If sh.Success Then
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no emails provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Sms "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveSmsBatchList(SmsBatchList As OBLib.Notifications.SmS.SmsBatchList) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = SmsBatchList.TrySave()
      If sh.Success Then
        SmsBatchList = sh.SavedObject
        If SmsBatchList.Count = 1 Then
          SmsBatchList(0).GenerateSmses()
        End If
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveSmsList(SmsList As OBLib.Notifications.SmS.SmsList) As Singular.Web.Result
    Try
      If SmsList.Count = 1 Then
        Dim FirstSms As OBLib.Notifications.SmS.Sms = SmsList(0)
        If Not IsNullNothing(FirstSms.SmsTemplateTypeID) Then
          SmsList = FirstSms.GenerateSmsListFromTemplate()
        End If
      End If
      Dim sh As Singular.SaveHelper = SmsList.TrySave()
      If sh.Success Then
        SmsList = sh.SavedObject
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Sms Sender.Access"})>
  Public Overridable Function PrepareSmsesForSending(SmsBatchID As Integer?, SmsIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If (Not IsNullNothing(SmsBatchID) AndAlso SmsBatchID > 0) Or (SmsIDs.Count > 0) Then
        Try
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSPrepareForSending",
                                                  New String() {"@SmsBatchID",
                                                                "@SmsIDsXml",
                                                                "@CurrentUserID"},
                                                  New Object() {NothingDBNull(SmsBatchID),
                                                                Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(SmsIDs)),
                                                                OBLib.Security.Settings.CurrentUserID})
          cmdProc.CommandTimeout = 0
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
          cmdProc = cmdProc.Execute
          Dim ItemCount As Integer = cmdProc.DataObject
          Return New Singular.Web.Result(True) With {.ErrorText = ItemCount.ToString}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "batch or emails not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Sms Sender.Access"})>
  Public Overridable Function DeleteSmses(SmsIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If SmsIDs.Count > 0 Then
        Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSDelete",
                                                 New String() {"@SmsIDsXml",
                                                               "@CurrentUserID"},
                                                 New Object() {Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(SmsIDs)),
                                                               OBLib.Security.Settings.CurrentUserID})
        cmdProc.CommandTimeout = 10
        cmdProc.UseTransaction = True
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cmdProc = cmdProc.Execute
        Dim DeletedCount As Integer = cmdProc.DataRow(0)
        Dim CannotDeleteCount As Integer = cmdProc.DataRow(1)
        Return New Singular.Web.Result(True) With {.Data = New With {.DeletedCount = DeletedCount,
                                                                     .CannotDeleteCount = CannotDeleteCount}}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no smses provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveSmsSearchList(SmsSearchList As OBLib.Notifications.SmS.SmsSearchList) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = SmsSearchList.TrySave()
      If sh.Success Then
        SmsSearchList = sh.SavedObject
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function RetrySmsRecipient(SmsSearchRecipient As OBLib.Notifications.SmS.SmsSearchRecipient) As Singular.Web.Result
    Try
      SmsSearchRecipient.Retry()
      Dim sh As Singular.SaveHelper = SmsSearchRecipient.TrySave(GetType(OBLib.Notifications.SmS.SmsSearchRecipientList))
      If sh.Success Then
        SmsSearchRecipient = sh.SavedObject
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Outside Broadcast "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function OBAllocationRequestSynergyEvent(allocationRequest As OBLib.Synergy.SynergyEvent) As Singular.Web.Result
    Try
      If allocationRequest IsNot Nothing Then
        Try
          Dim res As Singular.Web.Result = allocationRequest.AllocateOB()
          res.Data = allocationRequest
          WebsiteHelpers.SendResourceBookingUpdateNotifications()
          WebsiteHelpers.SendUnsentNotifications()
          Return res
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message, .Data = allocationRequest}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received", .Data = allocationRequest}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  '<Singular.Web.WebCallable(LoggedInOnly:=True)>
  'Public Shared Function GetProductionSystemAreaEquipmentTypeList(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '  Try
  '    Dim list As OBLib.ProductionSystemAreas.ProductionSystemAreaEquipmentTypeList = OBLib.ProductionSystemAreas.ProductionSystemAreaEquipmentTypeList.GetProductionSystemAreaEquipmentTypeList(ProductionSystemAreaID)
  '    Return New Singular.Web.Result(True) With {.Data = list}
  '  Catch ex As Exception
  '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try
  'End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionSystemAreaEquipmentTypeList(ProductionSystemAreaEquipmentTypeList As OBLib.ProductionSystemAreas.ProductionSystemAreaEquipmentTypeList) As Singular.Web.Result
  '    Try
  '      Dim sh As Singular.SaveHelper = ProductionSystemAreaEquipmentTypeList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionSystemAreaPositionList(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.ProductionSystemAreas.ProductionSystemAreaPositionList = OBLib.ProductionSystemAreas.ProductionSystemAreaPositionList.GetProductionSystemAreaPositionList(ProductionSystemAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionSystemAreaPositionList(ProductionSystemAreaPositionList As OBLib.ProductionSystemAreas.ProductionSystemAreaPositionList) As Singular.Web.Result
  '    Try
  '      Dim sh As Singular.SaveHelper = ProductionSystemAreaPositionList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionOutsourceServiceList(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.ProductionSystemAreas.ProductionOutsourceServiceList = OBLib.ProductionSystemAreas.ProductionOutsourceServiceList.GetProductionOutsourceServiceList(ProductionSystemAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionOutsourceServiceListServerMethod(ProductionOutsourceServiceList As OBLib.ProductionSystemAreas.ProductionOutsourceServiceList) As Singular.Web.Result
  '    Try
  '      Dim sh As Singular.SaveHelper = ProductionOutsourceServiceList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionSystemAreaCrewTypes(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.ProductionSystemAreaCrewTypeList = OBLib.OutsideBroadcast.ProductionSystemAreaCrewTypeList.GetProductionSystemAreaCrewTypeList(ProductionSystemAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionSystemAreaCrewTypeListServerMethod(ProductionSystemAreaCrewTypeList As OBLib.OutsideBroadcast.ProductionSystemAreaCrewTypeList) As Singular.Web.Result
  '    Try
  '      'If ProductionPettyCashList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = ProductionSystemAreaCrewTypeList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetTimelineListOB(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.OBTimelineList = OBLib.OutsideBroadcast.OBTimelineList.GetOBTimelineList(ProductionSystemAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveOBTimelineListServerMethod(OBTimelineList As OBLib.OutsideBroadcast.OBTimelineList) As Singular.Web.Result
  '    Try
  '      'If ProductionPettyCashList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = OBTimelineList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveBulkTimelineListOB(OBTimelineList As OBLib.OutsideBroadcast.OBTimelineList) As Singular.Web.Result
  '    Try
  '      Dim sh As Singular.Web.Result = SaveOBTimelineListServerMethod(OBTimelineList)
  '      Return sh
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionHumanResourceListOB(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.OBProductionHumanResourceList = OBLib.OutsideBroadcast.OBProductionHumanResourceList.GetOBProductionHumanResourceList(ProductionSystemAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveOBProductionHumanResourceListServerMethod(OBProductionHumanResourceList As OBLib.OutsideBroadcast.OBProductionHumanResourceList) As Singular.Web.Result
  '    Try
  '      'If ProductionPettyCashList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = OBProductionHumanResourceList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        'OBLib.Helpers.ErrorHelpers.LogClientError(
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionHROBList(ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.ProductionHROBList = OBLib.OutsideBroadcast.ProductionHROBList.GetProductionHROBList(ProductionSystemAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionHROBListServerMethod(ProductionHRList As OBLib.OutsideBroadcast.ProductionHROBList) As Singular.Web.Result
  '    Try
  '      'If ProductionPettyCashList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = ProductionHRList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function RemoveProductionHROBServerMethod(ProductionHumanResource As OBLib.OutsideBroadcast.OBProductionHumanResource, ProductionHROB As OBLib.OutsideBroadcast.ProductionHROB) As Singular.Web.Result
  '    Try
  '      If ProductionHumanResource IsNot Nothing AndAlso ProductionHROB IsNot Nothing Then

  '        Dim ProductionHumanResourceList As New OBLib.OutsideBroadcast.OBProductionHumanResourceList
  '        ProductionHumanResourceList.Add(ProductionHumanResource)
  '        Dim phrSaveResult As Singular.SaveHelper = ProductionHumanResourceList.TrySave

  '        Dim ProductionHRList As New OBLib.OutsideBroadcast.ProductionHROBList
  '        ProductionHRList.Add(ProductionHROB)
  '        ProductionHRList.Clear()
  '        Dim phSaveResult As Singular.SaveHelper = ProductionHRList.TrySave


  '        If phrSaveResult.Success AndAlso phSaveResult.Success Then
  '          ProductionHumanResourceList = phrSaveResult.SavedObject
  '          Return New Singular.Web.Result(True) With {.Data = New With {.ProductionHumanResource = ProductionHumanResourceList(0)}}
  '        Else
  '          Return New Singular.Web.Result(False) With {.ErrorText = "Save Failed"}
  '        End If

  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      End If
  '      Return New Singular.Web.Result(True)
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetCrewScheduleDetailOBList(ResourceBookingID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.ResourceBookingOBCSDList = OBLib.OutsideBroadcast.ResourceBookingOBCSDList.GetResourceBookingOBCSDList(ResourceBookingID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionHRUnSelectedTimelineList(ProductionHRID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.ReadOnly.ROTimelineProductionHRList = OBLib.OutsideBroadcast.ReadOnly.ROTimelineProductionHRList.GetROTimelineProductionHRList(ProductionHRID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function AddSelectedProductionHTimelines(ROTimelineProductionHRList As OBLib.OutsideBroadcast.ReadOnly.ROTimelineProductionHRList) As Singular.Web.Result
  '    Try
  '      ROTimelineProductionHRList.AddSelectedTimelines()
  '      Return New Singular.Web.Result(True)
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionHROBServerMethod(ProductionHROB As OBLib.OutsideBroadcast.ProductionHROB) As Singular.Web.Result
  '    Try
  '      Dim list As New OBLib.OutsideBroadcast.ProductionHROBList
  '      list.Add(ProductionHROB)
  '      Dim sh As Singular.SaveHelper = list.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionVehicleList(ProductionID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.OutsideBroadcast.ProductionVehicleList = OBLib.OutsideBroadcast.ProductionVehicleList.GetProductionVehicleList(ProductionID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionVehicleListServerMethod(ProductionVehicleList As OBLib.OutsideBroadcast.ProductionVehicleList) As Singular.Web.Result
  '    Try
  '      'If ProductionPettyCashList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = ProductionVehicleList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function GetProductionPettyCashListServerMethod(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.Productions.Correspondence.ProductionPettyCashList = OBLib.Productions.Correspondence.ProductionPettyCashList.GetProductionPettyCashList(ProductionID, SystemID, ProductionAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionPettyCashServerMethod(ProductionPettyCashList As OBLib.Productions.Correspondence.ProductionPettyCashList) As Singular.Web.Result
  '    Try
  '      'If ProductionPettyCashList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = ProductionPettyCashList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Function GetProductionGeneralCommentListServerMethod(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As Singular.Web.Result
  '    Try
  '      Dim list As OBLib.Productions.Correspondence.ProductionGeneralCommentList = OBLib.Productions.Correspondence.ProductionGeneralCommentList.GetProductionGeneralCommentList(ProductionID, SystemID, ProductionAreaID)
  '      Return New Singular.Web.Result(True) With {.Data = list}
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionGeneralCommentsServerMethod(ProductionGeneralCommentList As OBLib.Productions.Correspondence.ProductionGeneralCommentList) As Singular.Web.Result
  '    Try
  '      'If ProductionGeneralCommentList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = ProductionGeneralCommentList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no comments provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function


  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionCommentSectionListServerMethod(ProductionCommentSectionList As OBLib.OutsideBroadcast.ProductionCommentSectionList) As Singular.Web.Result
  '    Try
  '      'If ProductionGeneralCommentList.Count > 0 Then
  '      Dim sh As Singular.SaveHelper = ProductionCommentSectionList.TrySave
  '      If sh.Success Then
  '        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '      'Else
  '      '  Return New Singular.Web.Result(False) With {.ErrorText = "no comments provided"}
  '      'End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

#End Region

#Region " System Management "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveSystemProductionArea(SystemProductionArea As OBLib.Maintenance.SystemManagement.SystemProductionArea) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = SystemProductionArea.TrySave(GetType(SystemProductionAreaList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#Region " Yearly Requirements "

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function GetSystemYear(SystemYearID As Integer?) As Singular.Web.Result
    Try
      Dim lst As SystemYearList = OBLib.Maintenance.SystemManagement.SystemYearList.GetSystemYearList(SystemYearID)
      If lst.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = lst(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "System Year could not be found"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveSystemYear(SystemYear As SystemYear) As Singular.Web.Result
    Try
      Dim lst As New SystemYearList
      lst.Add(SystemYear)
      Dim sh As Singular.SaveHelper = lst.TrySave
      If sh.Success Then
        lst = sh.SavedObject
        SystemYear = lst(0)
        Return New Singular.Web.Result(True) With {.Data = SystemYear}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Timesheets "

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function GetTimesheetRequirement(TimesheetRequirementID As Integer?) As Singular.Web.Result
    Try
      Dim lst As TimesheetRequirementList = OBLib.Timesheets.TimesheetRequirementList.GetTimesheetRequirementList(TimesheetRequirementID)
      If lst.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = lst(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Timesheet Requirement could not be found"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveTimesheetRequirement(TimesheetRequirement As TimesheetRequirement) As Singular.Web.Result
    Try
      Dim lst As New TimesheetRequirementList
      TimesheetRequirement.MarkDirty()
      lst.Add(TimesheetRequirement)
      Dim sh As Singular.SaveHelper = lst.TrySave
      If sh.Success Then
        TimesheetRequirement.TimesheetRequirementHRList.BulkSave(Sub(proc As SqlClient.SqlCommand)
                                                                   proc.CommandText = "UpdProcsWeb.updTimesheetRequirementHRBulk"
                                                                   TimesheetRequirement.TimesheetRequirementHRList.AddHRTableParameter(proc)
                                                                 End Sub)
        lst = sh.SavedObject
        TimesheetRequirement = lst(0)
        Return New Singular.Web.Result(True) With {.Data = TimesheetRequirement}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function RefreshTimesheetRequirementStats(TimesheetRequirement As TimesheetRequirement) As Singular.Web.Result
    Try
      TimesheetRequirement.RefreshStats()
      Return New Singular.Web.Result(True) With {.Data = TimesheetRequirement}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function ApplyTimesheetRequirement(TimesheetRequirement As TimesheetRequirement) As Singular.Web.Result
    Try
      TimesheetRequirement.ApplyRequirement()
      Return New Singular.Web.Result(True) With {.Data = TimesheetRequirement}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function RunTimesheetRequirement(TimesheetRequirementID As Integer?) As Singular.Web.Result
    'If Singular.Debug.InDebugMode Then
    '  System.Threading.Thread.Sleep(1500)
    'End If
    Try
      Dim tr As ROTimesheetRequirementList = OBLib.Timesheets.ReadOnly.ROTimesheetRequirementList.GetROTimesheetRequirementList(TimesheetRequirementID)
      If tr.Count = 1 Then
        tr(0).RunTimesheets()
      End If
      Return New Singular.Web.Result(True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Spec Requirements "

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function GetProductionSpecRequirement(ProductionSpecRequirementID As Integer?) As Singular.Web.Result
    Try
      Dim lst As ProductionSpecRequirementList = OBLib.Maintenance.SystemManagement.ProductionSpecRequirementList.GetProductionSpecRequirementList(ProductionSpecRequirementID, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
      If lst.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = lst(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Timesheet Requirement could not be found"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  ', Roles:={"System Management.Can Access Timesheet Requirements"}
  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveProductionSpecRequirement(ProductionSpecRequirement As ProductionSpecRequirement) As Singular.Web.Result
    Try
      Dim lst As New ProductionSpecRequirementList
      lst.Add(ProductionSpecRequirement)
      Dim sh As Singular.SaveHelper = lst.TrySave
      If sh.Success Then
        lst = sh.SavedObject
        ProductionSpecRequirement = lst(0)
        Return New Singular.Web.Result(True) With {.Data = ProductionSpecRequirement}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#End Region

#Region " Schedulers "

  ', Roles:=New String() {"Resource Schedulers.Can Edit Resource Scheduler"}
  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function FetchResourceScheduler(ResourceSchedulerID As Integer?, FetchGroups As Boolean, FetchSubGroups As Boolean, FetchSubGroupResources As Boolean) As Singular.Web.Result

    Try
      Dim rsl As OBLib.ResourceSchedulers.[New].ResourceSchedulerList = OBLib.ResourceSchedulers.[New].ResourceSchedulerList.GetResourceSchedulerList(ResourceSchedulerID, OBLib.Security.Settings.CurrentUserID, FetchGroups, FetchSubGroups, FetchSubGroupResources)
      Return New Singular.Web.Result(True) With {.Data = rsl(0)}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveResourceScheduler(Scheduler As OBLib.ResourceSchedulers.[New].ResourceScheduler) As Singular.Web.Result

    Try
      Dim rsgl As New OBLib.ResourceSchedulers.[New].ResourceSchedulerList
      rsgl.Add(Scheduler)
      Dim sh As Singular.SaveHelper = rsgl.TrySave
      If sh.Success Then
        rsgl = sh.SavedObject
        Scheduler = rsgl(0)
        Dim SchedulerList As OBLib.ResourceSchedulers.[New].ResourceSchedulerList = OBLib.ResourceSchedulers.[New].ResourceSchedulerList.GetResourceSchedulerList(Scheduler.ResourceSchedulerID, OBLib.Security.Settings.CurrentUserID, True, True, True)
        'Scheduler
        Return New Singular.Web.Result(True) With {.Data = SchedulerList(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True, Roles:=New String() {"Resource Schedulers.Can Edit Resource Scheduler"})>
  Public Shared Function SaveResourceSchedulerOrdering(groupList As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroupList) As Singular.Web.Result

    Dim res As Singular.Web.Result = groupList.UpdateOrdering()
    Return res

  End Function

  <WebCallable(LoggedInOnly:=True, Roles:=New String() {"Resource Schedulers.Can Edit Resource Scheduler"})>
  Public Shared Function PopulateResourcesForSubGroup(subGroup As OBLib.ResourceSchedulers.[New].ResourceSchedulerSubGroup) As Singular.Web.Result

    'Dim res As Singular.Web.Result = subGroup.PopulateResourcesForSubGroup()
    'Return res

  End Function

#End Region

#Region "Travel"

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function CancelTraveller(Traveller As OBLib.Travel.Travellers.TravelRequisitionTraveller) As Singular.Web.Result
    Traveller.IsCancelled = True
    Traveller.CancelledReason = "Cancelled from travellers tab"
    'Me.CancelledDateTime = Now
    Dim sh As Singular.SaveHelper = Traveller.TrySave(GetType(OBLib.Travel.Travellers.TravelRequisitionTravellerList))
    If sh.Success Then
      Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function RemoveTraveller(Traveller As OBLib.Travel.Travellers.TravelRequisitionTraveller) As Singular.Web.Result
    Dim lst As New OBLib.Travel.Travellers.TravelRequisitionTravellerList
    lst.Add(Traveller)
    lst.Clear()
    'Me.CancelledDateTime = Now
    Dim sh As Singular.SaveHelper = lst.TrySave()
    If sh.Success Then
      Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveTravelRequisitionTravellerList(TravelRequisitionTravellerList As OBLib.Travel.Travellers.TravelRequisitionTravellerList) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = TravelRequisitionTravellerList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveTravelRequisitionTraveller(TravelRequisitionTraveller As OBLib.Travel.Travellers.TravelRequisitionTraveller) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = TravelRequisitionTraveller.TrySave(GetType(OBLib.Travel.Travellers.TravelRequisitionTravellerList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveFlight(Flight As OBLib.Travel.Flights.Flight) As Singular.Web.Result
    Try
      Flight.IsProcessing = False
      Dim sh As Singular.SaveHelper = Flight.TrySave(GetType(OBLib.Travel.Flights.FlightList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteFlights(FlightList As OBLib.Travel.Flights.FlightList) As Singular.Web.Result
    Try
      FlightList.Clear()
      Dim sh As Singular.SaveHelper = FlightList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function UpdateFlightClashes(Flight As OBLib.Travel.Flights.Flight) As Singular.Web.Result
    Try
      Flight.IsProcessing = False
      Dim sh As Singular.Web.Result = Flight.UpdateClashes()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = Flight}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveRentalCar(RentalCar As OBLib.Travel.RentalCars.RentalCar) As Singular.Web.Result
    Try
      RentalCar.IsProcessing = False
      Dim sh As Singular.SaveHelper = RentalCar.TrySave(GetType(OBLib.Travel.RentalCars.RentalCarList))
      If sh.Success Then
        RentalCar = sh.SavedObject
        Return New Singular.Web.Result(True) With {.Data = RentalCar}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteRentalCars(RentalCarList As OBLib.Travel.RentalCars.RentalCarList) As Singular.Web.Result
    Try
      RentalCarList.Clear()
      Dim sh As Singular.SaveHelper = RentalCarList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function UpdateRentalCarClashes(RentalCar As OBLib.Travel.RentalCars.RentalCar) As Singular.Web.Result
    Try
      RentalCar.IsProcessing = False
      Dim sh As Singular.Web.Result = RentalCar.UpdateClashes()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = RentalCar}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveAccommodation(Accommodation As OBLib.Travel.Accommodation.Accommodation) As Singular.Web.Result
    Try
      Accommodation.IsProcessing = False
      Dim sh As Singular.SaveHelper = Accommodation.TrySave(GetType(OBLib.Travel.Accommodation.AccommodationList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteAccommodation(AccommodationList As OBLib.Travel.Accommodation.AccommodationList) As Singular.Web.Result
    Try
      AccommodationList.Clear()
      Dim sh As Singular.SaveHelper = AccommodationList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function UpdateAccommodationClashes(Accommodation As OBLib.Travel.Accommodation.Accommodation) As Singular.Web.Result
    Try
      Accommodation.IsProcessing = False
      Dim sh As Singular.Web.Result = Accommodation.UpdateClashes()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = Accommodation}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveChauffeurDriver(ChauffeurDriver As OBLib.Travel.Chauffeurs.ChauffeurDriver) As Singular.Web.Result
    Try
      ChauffeurDriver.IsProcessing = False
      Dim sh As Singular.SaveHelper = ChauffeurDriver.TrySave(GetType(OBLib.Travel.Chauffeurs.ChauffeurDriverList))
      If sh.Success Then
        ChauffeurDriver = sh.SavedObject
        ChauffeurDriver.UpdateClashes()
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteChauffeurDriver(ChauffeurDriverList As OBLib.Travel.Chauffeurs.ChauffeurDriverList) As Singular.Web.Result
    Try
      ChauffeurDriverList.Clear()
      Dim sh As Singular.SaveHelper = ChauffeurDriverList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function UpdateChauffeurClashes(ChauffeurDriver As OBLib.Travel.Chauffeurs.ChauffeurDriver) As Singular.Web.Result
    Try
      ChauffeurDriver.IsProcessing = False
      Dim sh As Singular.Web.Result = ChauffeurDriver.UpdateClashes()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = ChauffeurDriver}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteTravelAdvances(TravelAdvanceList As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetailList) As Singular.Web.Result
    Try
      TravelAdvanceList.Clear()
      Dim sh As Singular.SaveHelper = TravelAdvanceList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DeleteTravelReqComments(TravelReqCommentList As OBLib.Travel.Comments.TravelReqCommentList) As Singular.Web.Result
    Try
      TravelReqCommentList.Clear()
      Dim sh As Singular.SaveHelper = TravelReqCommentList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveTravelRequisitionTravellerSnTList(TravelRequisitionTravellerSnTList As OBLib.Travel.Travellers.TravellerSnTList) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = TravelRequisitionTravellerSnTList.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function ResetTravellerSnT(TravelRequisitionTravellerSnTList As OBLib.Travel.Travellers.TravellerSnTList) As Singular.Web.Result
    Return TravelRequisitionTravellerSnTList.Reset()
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function ApplySnTTemplate(SnTTemplate As OBLib.Travel.SnT.SnTTemplate) As Singular.Web.Result
    Return SnTTemplate.Apply()
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DownloadTravelRequisitionAdHoc(TravelRequisitionID As Integer) As Singular.Web.Result

    Dim rpt As New OBWebReports.TravelReports.AdHocTravelRec
    rpt.ReportCriteria.TravelRequisitionID = TravelRequisitionID
    rpt.SingleReport = True
    Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
    'Save the Document Temporarily for downloading
    Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
    Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}

  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function DownloadTravelRequisitionProduction(TravelRequisitionID As Integer, SystemID As Integer) As Singular.Web.Result

    Dim rfi As ReportFileInfo = Nothing
    If SystemID = 1 Then
      Dim rpt As New OBWebReports.TravelReports.TravelRecProductionServices
      rpt.TravelRequisitionID = TravelRequisitionID
      rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
    ElseIf SystemID = 2 Then
      Dim rpt As New OBWebReports.TravelReports.TravelRecProductionContent
      rpt.TravelRequisitionID = TravelRequisitionID
      rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
    End If

    'Save the Document Temporarily for downloading
    Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
    Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}

  End Function

#End Region

#Region " Content and Services Integration "

  <WebCallable(LoggedInOnly:=True)>
  Public Function ActionContentServicesDifferences(Differences As ContentServicesDifferenceList) As Singular.Web.Result
    Dim sh As Singular.SaveHelper = Differences.TrySave()
    WebsiteHelpers.SendResourceBookingUpdateNotifications()
    Return New Singular.Web.Result(sh.Success) With {.ErrorText = sh.ErrorText}
  End Function

#End Region

#Region " ProductionSystemAreas "

  <WebCallable(LoggedInOnly:=True)>
  Public Function JoinArea(ProductionSystemAreaID As Integer?) As Singular.Web.Result

    If ProductionSystemAreaID IsNot Nothing Then
      Try
        Dim cmdProc As New Singular.CommandProc("cmdProcs.cmdProductionSystemAreaJoin",
                                        {"@ProductionSystemAreaID", "@CurrentUserID"},
                                        {ProductionSystemAreaID, OBLib.Security.Settings.CurrentUserID})
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cmdProc.UseTransaction = True 'rollbackable lol
        cmdProc = cmdProc.Execute
        Dim resultRow As DataRow = cmdProc.DataRow
        Dim succeeded As Boolean = resultRow(0)
        Dim successMessage As String = resultRow(1)
        Dim failureReason As String = resultRow(2)
        If succeeded Then
          Return New Singular.Web.Result(True) With {.Data = successMessage}
        Else
          Return New Singular.Web.Result(False) With {.Data = failureReason}
        End If
      Catch ex As Exception
        If TypeOf (ex) Is Csla.DataPortalException Then
          Dim exB = CType(ex, Csla.DataPortalException)
          Return New Singular.Web.Result(False) With {.Data = exB.BusinessException.Message}
        Else
          Return New Singular.Web.Result(False) With {.Data = ex.Message}
        End If

      End Try
    Else
      Return New Singular.Web.Result(False) With {.Data = "no area provided"}
    End If

  End Function

#End Region

#Region " Patterns "

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveSAShiftPattern(SAShiftPattern As OBLib.Shifts.Patterns.SAShiftPattern) As Singular.Web.Result

    Try
      Dim saveResult As Singular.SaveHelper = SAShiftPattern.TrySave(GetType(OBLib.Shifts.Patterns.SAShiftPatternList))
      SAShiftPattern = saveResult.SavedObject
      'refetch the pattern as we need to know how many shifts are now linked to the pattern, each day and crew member
      Dim crit As New OBLib.Shifts.Patterns.SAShiftPatternList.Criteria With {
          .SystemAreaShiftPatternID = SAShiftPattern.SystemAreaShiftPatternID,
          .FetchChildren = True
      }
      Dim lst As OBLib.Shifts.Patterns.SAShiftPatternList = OBLib.Shifts.Patterns.SAShiftPatternList.GetSAShiftPatternList(crit)
      SAShiftPattern = lst(0)
      If saveResult.Success Then
        Return New Singular.Web.Result(True) With {.Data = SAShiftPattern}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = saveResult.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.Data = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GenerateSAShiftPatternHRShifts(SAShiftPatternHR As OBLib.Shifts.Patterns.SAShiftPatternHR) As Singular.Web.Result
    Return SAShiftPatternHR.GenerateShifts()
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function UpdateSAShiftPatternHRShifts(SAShiftPatternHR As OBLib.Shifts.Patterns.SAShiftPatternHR) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = SAShiftPatternHR.TrySave(GetType(OBLib.Shifts.Patterns.SAShiftPatternHRList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function


#End Region

End Class
