Imports System.ComponentModel
Imports Singular.Web
Imports Singular.DataAnnotations
Imports OBLib.HR
Imports Singular.Web.Data
Imports OBLib.SatOps
Imports OBLib.Shifts.PlayoutOperations
Imports OBLib.Resources
Imports Singular.Misc
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.RoomScheduling
Imports OBLib.AdHoc
Imports OBLib.Shifts
Imports OBLib.Copying
Imports OBLib.Helpers
Imports OBLib.Slugs
Imports Singular.Reporting
Imports OBLib.Notifications.Email
Imports OBLib.Shifts.Studios
Imports OBLib.Scheduling.Rooms
Imports System
Imports OBLib.OutsideBroadcast

<Serializable()>
Public Class OBViewModel(Of ModelType As Singular.Web.ViewModel(Of ModelType))
  Inherits Singular.Web.ViewModel(Of ModelType)
  Implements ControlInterfaces(Of ModelType).IApplyMessage
  'Implements ControlInterfaces(Of ModelType).IEditProduction

  Private mSiteInfo As OBLib.Helpers.SiteInfo
  <Browsable(False)>
  Friend ReadOnly Property SiteInfo As OBLib.Helpers.SiteInfo
    Get
      If mSiteInfo Is Nothing Then
        mSiteInfo = New OBLib.Helpers.SiteInfo(Me.Page.Request)
      End If
      Return mSiteInfo
    End Get
  End Property

  Protected Overrides Sub WriteInitialiseVariables(JSW As Utilities.JavaScriptWriter)
    MyBase.WriteInitialiseVariables(JSW)
    'Dim sSiteInfo As New OBLib.Helpers.SiteInfo(Me.Page.Request)
    JSW.Write("window.SiteInfo = " & Singular.Web.Data.JSonWriter.SerialiseObject(SiteInfo, , False, OutputType.Javascript) & ";")
  End Sub

  Public Property TempImage As New Singular.Documents.TemporaryDocumentNotRequired

#Region " Properties "

  <InitialDataOnly>
  Public Property ApplyMessageList As List(Of OBLib.Helpers.ApplyMessage) Implements ControlInterfaces(Of ModelType).IApplyMessage.ApplyMessageList
  <InitialDataOnly>
  Public Property IsApplying As Boolean Implements ControlInterfaces(Of ModelType).IApplyMessage.IsApplying

  <Browsable(False)>
  Public ReadOnly Property HasAuthenticatedUser As Boolean
    Get
      If Not Singular.Security.HasAuthenticatedUser OrElse OBLib.Security.Settings.CurrentUser Is Nothing Then
        Return False
      End If
      Return True
    End Get
  End Property

  Public ReadOnly Property CurrentUserName As String
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.LoginName
      Else
        Return "No Current User"
      End If
    End Get
  End Property

  Public ReadOnly Property CurrentUserID As Integer?
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.UserID
      Else
        Return Nothing
      End If
    End Get
  End Property

  Public ReadOnly Property CurrentUserHumanResourceID As Integer?
    Get
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        Return OBLib.Security.Settings.CurrentUser.HumanResourceID
      Else
        Return Nothing
      End If
    End Get
  End Property

#Region " User Notifications "

  <InitialDataOnly>
  Public Property ROUserNotificationList As OBLib.Users.ReadOnly.ROUserNotificationList = New OBLib.Users.ReadOnly.ROUserNotificationList
  <InitialDataOnly>
  Public Property ROUserNotificationListCriteria As OBLib.Users.ReadOnly.ROUserNotificationList.Criteria = New OBLib.Users.ReadOnly.ROUserNotificationList.Criteria
  <InitialDataOnly>
  Public Property ROUserNotificationListManager As PagedDataManager(Of ModelType) = New PagedDataManager(Of ModelType)(Function(d) Me.ROUserNotificationList, Function(d) Me.ROUserNotificationListCriteria,
                                                                                                                                                       "CreatedDateTime", 10, False)

#End Region

#End Region

#Region " ViewModel Methods "

  Protected Overridable Function HasPageAccess() As Boolean
    Return True
  End Function

  Private Sub RedirectToChangePassword()

    If (Not HttpContext.Current.Request.Path.Contains("ChangePassword.aspx")) Then
      HttpContext.Current.Response.Redirect("~/User/ChangePassword.aspx")
    End If

  End Sub

  Public Sub RedirectToLoginPage()
    HttpContext.Current.Response.Redirect("~/Account/Login.aspx")
  End Sub

  Public Sub RedirectToNoAccess()
    HttpContext.Current.Response.Redirect("~/ErrorPage.aspx")
  End Sub

  Public Function CurrentUser() As OBLib.Security.User

    Return OBLib.Security.Settings.CurrentUser

  End Function

  Protected Overrides Sub Setup()
    MyBase.Setup()

    'Add the current users System and SystemArea List
    If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
      ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
      ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    End If

    ROUserNotificationList = New OBLib.Users.ReadOnly.ROUserNotificationList
    ROUserNotificationListCriteria = New OBLib.Users.ReadOnly.ROUserNotificationList.Criteria
    ROUserNotificationListManager = New PagedDataManager(Of ModelType)(Function(d) Me.ROUserNotificationList, Function(d) Me.ROUserNotificationListCriteria,
                                                                                                "CreatedDateTime", 10, False)
    ROUserNotificationListCriteria.UserID = Me.CurrentUserID
    ROUserNotificationListCriteria.StartDate = Now
    ROUserNotificationListCriteria.EndDate = Now
    ROUserNotificationListCriteria.SortAsc = False
    ROUserNotificationListCriteria.SortColumn = "CreatedDateTime"
    ROUserNotificationListCriteria.PageNo = 1
    ROUserNotificationListCriteria.PageSize = 10

  End Sub

  Protected Sub PreSetup()

    If Not HasAuthenticatedUser Then
      RedirectToLoginPage()
    End If

    'If Not UserPasswordValid Then
    '  RedirectToChangePassword()
    'End If

    If Not HasPageAccess() Then
      RedirectToNoAccess()
    End If

  End Sub

#End Region

#Region " Error Handling "

  <WebCallable(LoggedInOnly:=True)>
  Public Function LogClientError(User As String, Page As String, Method As String, ErrorMessage As String) As Singular.Web.Result

    Return OBLib.OBMisc.LogClientError(User, Page, Method, ErrorMessage)

  End Function

#End Region

#Region " Resource Bookings "

  <InitialDataOnly>
  Public Property ResourceBookingsCleanList As List(Of OBLib.Resources.ResourceBookingClean) = New List(Of OBLib.Resources.ResourceBookingClean)

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetResourceBookingClashes(ResourceBookings As List(Of OBLib.Resources.ResourceBookingClean)) As Singular.Web.Result
    Try
      Dim data As List(Of OBLib.Resources.ResourceBookingClean) = OBLib.Helpers.ResourceHelpers.GetResourceBookingsCleanClashes(ResourceBookings)
      Return New Singular.Web.Result(True) With {.Data = data}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetSingleBookingClashes(ResourceBookingClean As OBLib.Resources.ResourceBookingClean) As Singular.Web.Result
    Try
      Dim rbl As New List(Of OBLib.Resources.ResourceBookingClean)
      rbl.Add(ResourceBookingClean)
      Dim data As List(Of OBLib.Resources.ResourceBookingClean) = OBLib.Helpers.ResourceHelpers.GetResourceBookingsCleanClashes(rbl)
      Return New Singular.Web.Result(True) With {.Data = data}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetPersonnelManagerBookings(StartDate As DateTime?,
                                              EndDate As DateTime?,
                                              DisciplineID As Integer?,
                                              PositionID As Integer?,
                                              FirstName As String,
                                              Surname As String,
                                              PreferredName As String,
                                              PageNo As Integer?,
                                              PageSize As Integer?,
                                              ResourceSchedulerID As Integer?) As Singular.Web.Result

    Dim wr As Singular.Web.Result
    Try
      Dim RSResources As OBLib.Resources.RSResourceList = OBLib.Resources.RSResourceList.GetRSResourceList(StartDate, EndDate,
                                                                                                           DisciplineID, PositionID,
                                                                                                           FirstName, Surname, PreferredName,
                                                                                                           PageNo, PageSize,
                                                                                                           ResourceSchedulerID)
      wr = New Singular.Web.Result(True)
      wr.Data = New With {
                          .Resources = RSResources,
                          .TotalRecords = RSResources.TotalRecords,
                          .TotalPages = RSResources.TotalPages
                }
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function GetResourceClashes(ResourceBookings As List(Of OBLib.Resources.ResourceBooking)) As Singular.Web.Result

    Try
      Dim clashList As List(Of ClashDetail) = OBLib.Resources.ResourceBookingList.GetResourceClashes(ResourceBookings)
      Return New Singular.Web.Result(True) With {.Data = clashList}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function SaveProductionHROBSimple(ProductionHROBSimple As ProductionHROBSimple) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = ProductionHROBSimple.TrySave(GetType(ProductionHROBSimpleList))
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(sh.Success) With {.Data = sh.SavedObject, .ErrorText = sh.ErrorText}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function CheckEditState(ResourceBookingID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdResourceBookingCheckEditState",
                                              New String() {"@ResourceBookingID", "@CurrentUserID", "@CurrentUser"},
                                              New Object() {NothingDBNull(ResourceBookingID), OBLib.Security.Settings.CurrentUserID, OBLib.Security.Settings.CurrentUser.LoginName})
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
      cmdProc = cmdProc.Execute()

      Dim IsBeingEditedBy As String = ""
      Dim InEditDateTime As DateTime? = Nothing
      Dim IsBeingEditedByUserID As Integer? = Nothing
      Dim IsBeingEditedByName As String = ""
      Dim EditImagePath As String = ""

      If cmdProc.DataRow IsNot Nothing Then
        IsBeingEditedBy = cmdProc.DataRow(0)
        InEditDateTime = OBLib.OBMisc.DBNullNothing(cmdProc.DataRow(1))
        IsBeingEditedByUserID = ZeroNothing(cmdProc.DataRow(2))
        IsBeingEditedByName = cmdProc.DataRow(3)
        EditImagePath = cmdProc.DataRow(4)
        Return New Singular.Web.Result(True) With {.Data = New With {.IsBeingEditedBy = IsBeingEditedBy, .InEditDateTime = InEditDateTime,
                                                             .IsBeingEditedByUserID = IsBeingEditedByUserID, .IsBeingEditedByName = IsBeingEditedByName,
                                                             .EditImagePath = EditImagePath}}
      Else
        Return New Singular.Web.Result(True) With {.Data = New With {.IsBeingEditedBy = IsBeingEditedBy, .InEditDateTime = InEditDateTime,
                                                                     .IsBeingEditedByUserID = IsBeingEditedByUserID, .IsBeingEditedByName = IsBeingEditedByName,
                                                                     .EditImagePath = EditImagePath}}
      End If

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)
  End Function

#End Region

#Region " Synergy "

  <WebCallable(LoggedInOnly:=True)>
  Public Function DoNewSynergyImport(StartDate As DateTime?, EndDate As DateTime?) As Singular.Web.Result
    Try
      OBLib.Synergy.Importer.[New].SynergyImporter.ImportData(StartDate, EndDate, "", "", "", "", "", True, True, True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(True)

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Function GetSynergyEvent(GenRefNumber As Int64?, StartDateTime As DateTime?, EndDateTime As DateTime?) As Singular.Web.Result
    Try
      Dim sel As OBLib.Synergy.SynergyEventList = OBLib.Synergy.SynergyEventList.GetSynergyEventList(Nothing, Nothing, GenRefNumber, StartDateTime, EndDateTime, True, True, True, False)
      If sel.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = sel(0)}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Could not find event"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DownloadSynergyChangesWithBookingInfo(SynergyImportID As Integer?) As Singular.Web.Result
    Try
      If SynergyImportID IsNot Nothing Then
        Dim rptChanges As New OBWebReports.SynergyReports.SynergyChangesWithBookingInfo
        rptChanges.ReportCriteria.StartDateTime = Now
        rptChanges.ReportCriteria.EndDateTime = Now
        rptChanges.ReportCriteria.SynergyImportID = SynergyImportID
        Dim stream As System.IO.MemoryStream = rptChanges.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        If stream IsNot Nothing AndAlso stream.Length > 0 Then
          Dim RetInfo As New ReportFileInfo
          RetInfo.FileStream = stream
          RetInfo.FileName = "SynergyChanges.xlsx"
          Dim TempDoc As New Singular.Documents.TemporaryDocument(RetInfo.FileName, RetInfo.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "Document is empty"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Import not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Production "

  'Public Property CurrentProduction As OBLib.Productions.Base.Production Implements ControlInterfaces(Of ModelType).IEditProduction.CurrentProduction

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveCurrentProduction(productionToSave As OBLib.Productions.Production) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.Productions.ProductionList
      prdl.Add(productionToSave)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

#End Region

#Region " AdHoc "

  'Public Property CurrentAdHocBooking As OBLib.AdHoc.AdHocBooking 'Implements ControlInterfaces(Of ModelType).IEditProduction.CurrentProduction

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveCurrentAdHocBooking(adHocBookingToSave As OBLib.AdHoc.Old.AdHocBooking) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.Old.AdHocBookingList
      prdl.Add(adHocBookingToSave)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveRSAdHocBooking(rsAdhocBooking As OBLib.AdHoc.Old.AdHocBooking) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.Old.AdHocBookingList
      prdl.Add(rsAdhocBooking)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  Public Overridable Function SaveAdHocBookingSendCommand(adHocBookingToSave As OBLib.AdHoc.AdHocBooking) As Singular.Web.Result
    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim prdl As New OBLib.AdHoc.AdHocBookingList
      prdl.Add(adHocBookingToSave)
      Dim sh As SaveHelper = TrySave(prdl)
      If sh.Success Then
        prdl = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr
  End Function

  'Public Overridable Function SaveAdHocBookingAreaSendCommand(adHocBookingAreaToSave As OBLib.AdHoc.PSAAdHoc) As Singular.Web.Result
  '  Dim wr As Singular.Web.Result = Nothing
  '  Try
  '    Dim prdl As New OBLib.AdHoc.PSAAdHocList
  '    prdl.Add(adHocBookingAreaToSave)
  '    Dim sh As SaveHelper = TrySave(prdl)
  '    If sh.Success Then
  '      prdl = sh.SavedObject
  '      WebsiteHelpers.SendResourceBookingUpdateNotifications()
  '      wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
  '    Else
  '      wr = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '    End If
  '  Catch ex As Exception
  '    wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try

  '  Return wr
  'End Function

  'Public Overridable Function EditAdHocBookingSendCommand(ByRef AdHocBooking As AdHocBooking, AdHocBookingID As Integer?, ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '  Dim wr As Singular.Web.Result = Nothing
  '  Try
  '    Dim prdl As OBLib.AdHoc.AdHocBookingList = OBLib.AdHoc.AdHocBookingList.GetAdHocBookingList(AdHocBookingID, ProductionSystemAreaID)
  '    If prdl.Count = 1 Then
  '      AdHocBooking = prdl(0)
  '      wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
  '    Else
  '      wr = New Singular.Web.Result(False) With {.ErrorText = "Booking could not be fetched"}
  '    End If
  '  Catch ex As Exception
  '    wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try

  '  Return wr
  'End Function

  'Public Overridable Function EditAdHocBookingAreaSendCommand(ByRef PSAAdHoc As PSAAdHoc, AdHocBookingID As Integer?, ProductionSystemAreaID As Integer?) As Singular.Web.Result
  '  Dim wr As Singular.Web.Result = Nothing
  '  Try
  '    Dim prdl As OBLib.AdHoc.PSAAdHocList = OBLib.AdHoc.PSAAdHocList.GetPSAAdHocList(AdHocBookingID, ProductionSystemAreaID)
  '    If prdl.Count = 1 Then
  '      PSAAdHoc = prdl(0)
  '      wr = New Singular.Web.Result(True) With {.Data = prdl(0)}
  '    Else
  '      wr = New Singular.Web.Result(False) With {.ErrorText = "Area could not be fetched"}
  '    End If
  '  Catch ex As Exception
  '    wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try

  '  Return wr
  'End Function

#End Region

#Region " Room Scheduling "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteAreaFromRoomSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerDeleteAreaFromRoomSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      'Dim ApplyList As New List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)
      'For Each dr As DataRow In cmdProc.Dataset.Tables(0).Rows
      '  ApplyList.Add(New OBLib.Helpers.ResourceHelpers.ApplyResult(dr))
      'Next

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) 'With {.Data = ApplyList}

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ChangeRoomScheduleStatusID(ResourceBookingID As Integer?,
                                                         NewStatusID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerChangeRoomScheduleStatusID]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@NewStatusID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            Singular.Misc.NothingDBNull(NewStatusID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      'Dim ApplyList As New List(Of OBLib.Helpers.ResourceHelpers.ApplyResult)
      'For Each dr As DataRow In cmdProc.Dataset.Tables(0).Rows
      '  ApplyList.Add(New OBLib.Helpers.ResourceHelpers.ApplyResult(dr))
      'Next

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) 'With {.Data = ApplyList}

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ReInstateRoomScheduleSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerReInstateRoomSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function UnReconcileRoomSchedule(ResourceBookingID As Integer?) As Singular.Web.Result
    'Ask for Authorisation
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerUnReconcileRoomSchedule]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ApplyRoomScheduleTimeChanges(ResourceBooking As ResourceBookingClean) As Singular.Web.Result
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerApplyRoomScheduleTimeChanges]",
                                              New String() {
                                                            "@ResourceBookingID",
                                                            "@StartDateTimeBuffer",
                                                            "@StartDateTime",
                                                            "@EndDateTime",
                                                            "@EndDateTimeBuffer",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceBooking.ResourceBookingID),
                                                            NothingDBNull(ResourceBooking.StartDateTimeBuffer),
                                                            NothingDBNull(ResourceBooking.StartDateTime),
                                                            NothingDBNull(ResourceBooking.EndDateTime),
                                                            NothingDBNull(ResourceBooking.EndDateTimeBuffer),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function AddAreaToRoomSchedule(ResourceBookingID As Integer?,
                                                    RoomScheduleID As Integer?,
                                                    SystemID As Integer?,
                                                    ProductionAreaID As Integer?) As Singular.Web.Result

    Dim wr As Singular.Web.Result = Nothing
    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerAddAreaToRoomSchedule]",
                                              New String() {"@ResourceBookingID", "@RoomScheduleID",
                                                            "@SystemID", "@ProductionAreaID",
                                                            "@ModifiedBy", "@CurrentUsername"},
                                              New Object() {ResourceBookingID, RoomScheduleID,
                                                            SystemID, ProductionAreaID,
                                                            OBLib.Security.Settings.CurrentUser.UserID, OBLib.Security.Settings.CurrentUser.LoginName})
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
      cmdProc = cmdProc.Execute()
      Dim NewResourceBookingID As Integer? = cmdProc.DataRow(0)
      If NewResourceBookingID IsNot Nothing Then
        wr = New Singular.Web.Result(True) With {.Data = New With {.ResourceBookingID = NewResourceBookingID, .RoomScheduleID = cmdProc.DataRow(1), .ProductionSystemAreaID = cmdProc.DataRow(2)}}
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        wr = New Singular.Web.Result(False) With {.ErrorText = "An error occured while adding your area to the booking"}
      End If
    Catch ex As Exception
      wr = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

    Return wr

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function BeforeHRAddedToRoomScheduleChecks(ResourceID As Integer?, ResourceBookingID As Integer?,
                                                                HumanResourceID As Integer?, RoomScheduleID As Integer?,
                                                                CallTimeStart As DateTime?, OnAirStartTime As DateTime, OnAirEndTime As DateTime, WrapTimeEnd As DateTime,
                                                                SystemID As Integer?, ProductionAreaID As Integer?) As Singular.Web.Result

    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerBeforeHRAddedToRoomScheduleChecks]",
                                              New String() {
                                                            "@ResourceID",
                                                            "@ResourceBookingID",
                                                            "@HumanResourceID",
                                                            "@RoomScheduleID",
                                                            "@CallTimeStart",
                                                            "@OnAirStartTime",
                                                            "@OnAirEndTime",
                                                            "@WrapTimeEnd",
                                                            "@SystemID",
                                                            "@ProductionAreaID",
                                                            "@ModifiedBy"
                                                           },
                                              New Object() {
                                                            Singular.Misc.NothingDBNull(ResourceID),
                                                            Singular.Misc.NothingDBNull(ResourceBookingID),
                                                            Singular.Misc.NothingDBNull(HumanResourceID),
                                                            Singular.Misc.NothingDBNull(RoomScheduleID),
                                                            Singular.Misc.NothingDBNull(CallTimeStart),
                                                            Singular.Misc.NothingDBNull(OnAirStartTime),
                                                            Singular.Misc.NothingDBNull(OnAirEndTime),
                                                            Singular.Misc.NothingDBNull(WrapTimeEnd),
                                                            Singular.Misc.NothingDBNull(SystemID),
                                                            Singular.Misc.NothingDBNull(ProductionAreaID),
                                                            OBLib.Security.Settings.CurrentUserID
                                                           })
      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute
      Dim BeforeAddHRToRoomScheduleResult As New BeforeAddHRToRoomScheduleResult(cmdProc.Dataset)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) With {.Data = BeforeAddHRToRoomScheduleResult}

    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

#End Region

#Region " Slugs "

  ', Roles:={"Slugs.Can Request Slugs"}
  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetSlugsForGenRef(genRef As Int64?) As Singular.Web.Result
    Try
      If genRef IsNot Nothing Then
        Try
          OBLib.ServiceHelpers.SlugServiceHelpers.RefreshSlugs(genRef)
          Dim fetchedList As SlugItemList = OBLib.Slugs.SlugItemList.GetSlugItemList(genRef)
          Return New Singular.Web.Result(True) With {.Data = fetchedList}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no genref provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Copy Bookings "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function CopyResourceBookingToSpecificDates(setting As CopySetting) As Singular.Web.Result
    Try
      If setting IsNot Nothing Then
        Dim l As New List(Of CopyResult)
        l = setting.Submit()
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = l}
      Else
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(False) With {.ErrorText = "blank allocation request received", .Data = setting}
      End If
    Catch ex As Exception
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR "

  <WebCallable(LoggedInOnly:=True)>
  Public Function CheckHRIDNumber(HumanResource As OBLib.HR.HumanResource) As Singular.Web.Result

    Return New Singular.Web.Result(True) With {.Data = HumanResource.IsIDNumberValid(HumanResource)}

  End Function

#End Region

#Region " HR - Off Periods "

  Friend Sub EditOffPeriod(ByRef humanResourceOffPeriod As HumanResourceOffPeriod, ByRef CommandArgs As CommandArgs)
    Try
      Dim HumanResourceOffPeriodID As Integer? = CommandArgs.ClientArgs.HumanResourceOffPeriodID
      Dim opl As HumanResourceOffPeriodList = OBLib.HR.HumanResourceOffPeriodList.GetHumanResourceOffPeriodList(HumanResourceOffPeriodID)
      If opl.Count = 1 Then
        humanResourceOffPeriod = opl(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      ElseIf opl.Count > 1 Then
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Too many results returned"}
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Could not retrieve the Off Period"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Friend Sub SaveOffPeriod(ByRef humanResourceOffPeriod As HumanResourceOffPeriod, ByRef CommandArgs As CommandArgs)
    Try
      Dim opl As New HumanResourceOffPeriodList
      opl.Add(humanResourceOffPeriod)
      Dim sh As SaveHelper = TrySave(opl)
      If sh.Success Then
        opl = sh.SavedObject
        humanResourceOffPeriod = opl(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteHumanResourceOffPeriods(HumanResourceOffPeriodIDs As List(Of Integer)) As Singular.Web.Result
    Try
      Dim DeleteResults As New List(Of Singular.Web.Result)
      If HumanResourceOffPeriodIDs.Count > 0 Then
        Dim IDsXML As String = OBLib.OBMisc.IntegerListToXML(HumanResourceOffPeriodIDs)
        Dim offPeriodList As OBLib.HR.HumanResourceOffPeriodList = OBLib.HR.HumanResourceOffPeriodList.GetHumanResourceOffPeriodList(IDsXML)
        For Each offP As OBLib.HR.HumanResourceOffPeriod In offPeriodList
          'Not offP.IsAuthorsied
          If offP.CanAuthoriseLeave Then
            Dim tempList As New OBLib.HR.HumanResourceOffPeriodList
            tempList.Add(offP)
            tempList.Clear()
            Dim sh As SaveHelper = TrySave(tempList)
            If sh.Success Then
              DeleteResults.Add(New Singular.Web.Result(True) With {.ErrorText = offP.OffReason & " has been deleted successfully"})
            Else
              DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.OffReason & " could not be deleted " & sh.ErrorText})
            End If
          Else
            DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.OffReason & " could not be deleted: " & " Authorised Leave cannot be deleted"})
          End If
        Next
        offPeriodList.Clear()
        Return New Singular.Web.Result(True) With {.ErrorText = "Deletions Complete", .Data = DeleteResults}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "Delete not attempted"}
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function UnAuthoriseLeave(HumanResourceOffPeriodID As Integer) As Singular.Web.Result
    Try
      If HumanResourceOffPeriodID > 0 Then
        Dim offPeriodList As OBLib.HR.HumanResourceOffPeriodList = OBLib.HR.HumanResourceOffPeriodList.GetHumanResourceOffPeriodList(HumanResourceOffPeriodID)
        Dim offP As OBLib.HR.HumanResourceOffPeriod = offPeriodList(0)
        If offP.IsAuthorsied Then
          offP.UnAuthorise()
          Dim sh As SaveHelper = TrySave(offPeriodList)
          If sh.Success Then
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "OffPeriod is not authorised"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "OffPeriod items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "UnAthorisation not attempted"}
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function GenerateOffPeriod(offPeriod As HumanResourceOffPeriod) As Singular.Web.Result
    Try
      offPeriod.GenerateOffPeriod()
      Return New Singular.Web.Result(True) With {.Data = offPeriod}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Public Shared Function SaveHumanResourceUnAvailabilityTemplate(HumanResourceUnAvailabilityTemplate As HumanResourceUnAvailabilityTemplate) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = HumanResourceUnAvailabilityTemplate.HumanResourceUnAvailabilityList.TrySave
      If sh.Success Then
        HumanResourceUnAvailabilityTemplate.ReplaceList(sh.SavedObject)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Public Shared Function DeleteUnAvailability(ROHumanResourceUnAvailabilityList As List(Of ROHumanResourceUnAvailability)) As Singular.Web.Result
    Try
      OBLib.HR.ROHumanResourceUnAvailabilityList.DeleteBulkIndependant(ROHumanResourceUnAvailabilityList)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True)
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Secondments "

  Friend Sub EditSecondment(ByRef humanResourceSecondment As HumanResourceSecondment, ByRef CommandArgs As CommandArgs)
    Try
      Dim opl As HumanResourceSecondmentList = OBLib.HR.HumanResourceSecondmentList.GetHumanResourceSecondmentList(CommandArgs.ClientArgs.HumanResourceSecondmentID)
      If opl.Count = 1 Then
        humanResourceSecondment = opl(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      ElseIf opl.Count > 1 Then
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Too many results returned"}
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Could not retrieve the secondment"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Friend Sub SaveSecondment(ByRef humanResourceSecondment As HumanResourceSecondment, ByRef CommandArgs As CommandArgs)
    Try
      Dim opl As New HumanResourceSecondmentList
      opl.Add(humanResourceSecondment)
      Dim sh As SaveHelper = TrySave(opl)
      If sh.Success Then
        opl = sh.SavedObject
        humanResourceSecondment = opl(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteSecondments(HumanResourceSecondmentIDs As List(Of Integer)) As Singular.Web.Result
    Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If HumanResourceSecondmentIDs.Count > 0 Then
        Dim IDsXML As String = OBLib.OBMisc.IntegerListToXML(HumanResourceSecondmentIDs)
        Dim secondmentList As OBLib.HR.HumanResourceSecondmentList = OBLib.HR.HumanResourceSecondmentList.GetHumanResourceSecondmentList(IDsXML)
        For Each offP As OBLib.HR.HumanResourceSecondment In secondmentList
          If Not offP.IsAuthorised Then
            Dim tempList As New OBLib.HR.HumanResourceSecondmentList
            tempList.Add(offP)
            tempList.Clear()
            Dim sh As SaveHelper = TrySave(tempList)
            If sh.Success Then
              DeleteResults.Add(New Singular.Web.Result(True) With {.ErrorText = offP.Description & " has been deleted successfully"})
            Else
              DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.Description & " could not be deleted " & sh.ErrorText})
            End If
          Else
            DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = offP.Description & " could not be deleted: " & " Authorised Secondments cannot be deleted"})
          End If
        Next
        secondmentList.Clear()
        Return New Singular.Web.Result(True) With {.ErrorText = "Deletions Complete", .Data = DeleteResults}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - TBC Productions "

  '<Browsable(False)>
  'Public Property ProductionHumanResourceTBCList As New OBLib.HR.ProductionHumanResourcesTBCList

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveProductionHumanResourceTBC(list As OBLib.HR.ProductionHumanResourcesTBCList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If list.Count > 0 Then
        Dim sh As SaveHelper = TrySave(list)
        If sh.Success Then
          Return New Singular.Web.Result(True)
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = Singular.Debug.RecurseExceptionMessage(sh.Error, True)}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function


#End Region

#Region " HR - Skills "

  Friend Sub EditHumanResourceSkill(ByRef SkillInstance As HumanResourceSkill, CommandArgs As Singular.Web.CommandArgs)
    Try
      Dim tempSkill As HumanResourceSkill = Nothing
      Dim ID As Integer? = CommandArgs.ClientArgs.HumanResourceSKillID
      tempSkill = FetchHumanResourceSkill(ID)
      If tempSkill IsNot Nothing Then
        SkillInstance = tempSkill
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Skill could not be retrieved"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Private Function FetchHumanResourceSkill(HumanResourceSkillID As Integer?) As HumanResourceSkill

    Dim skillList As HumanResourceSkillList = OBLib.HR.HumanResourceSkillList.GetHumanResourceSkillList(Nothing, HumanResourceSkillID)
    If skillList.Count = 1 Then
      Return skillList(0)
    End If
    Return Nothing

  End Function

  Friend Sub SaveHumanResourceSkill(ByRef SkillInstance As HumanResourceSkill, CommandArgs As Singular.Web.CommandArgs)

    Dim skillList As New HumanResourceSkillList
    skillList.Add(SkillInstance)
    skillList.CheckAllRules()
    If skillList.IsValid Then
      Dim sh As SaveHelper = TrySave(skillList)
      If sh.Success Then
        skillList = sh.SavedObject
        SkillInstance = skillList(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Skill is not valid"}
    End If

  End Sub

  <WebCallable(LoggedInOnly:=True)>
  Public Function DeleteHumanResourceSkills(HumanResourceSkillIDs As List(Of Integer)) As Singular.Web.Result
    Try
      Dim DeleteResults As New List(Of Singular.Web.Result)
      If HumanResourceSkillIDs.Count > 0 Then
        Dim IDsXML As String = OBLib.OBMisc.IntegerListToXML(HumanResourceSkillIDs)
        Dim skillList As OBLib.HR.HumanResourceSkillList = OBLib.HR.HumanResourceSkillList.GetHumanResourceSkillList(IDsXML)
        For Each skill As OBLib.HR.HumanResourceSkill In skillList
          If skill.BookingCount = 0 Then
            Dim tempList As New OBLib.HR.HumanResourceSkillList
            tempList.Add(skill)
            tempList.Clear()
            Dim sh As SaveHelper = TrySave(tempList)
            If sh.Success Then
              DeleteResults.Add(New Singular.Web.Result(True) With {.ErrorText = skill.Discipline & " deleted successfully"})  '(skill.Discipline, "Deleted Successfully", "Deleted Successfully", "alert alert-success animated fast slideInRight go", "fa fa-check-square-o"))
            Else
              DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}) '(skill.Discipline, "Deletion Failed", sh.ErrorText, "alert alert-danger animated fast slideInRight go", "fa fa-exclamation-circle"))
            End If
          Else
            DeleteResults.Add(New Singular.Web.Result(False) With {.ErrorText = skill.Discipline & " has already been booked and cannot be deleted"}) '(skill.Discipline, "Deletion Failed", "This skill has already been booked on events and cannot be deleted", "alert alert-danger animated fast slideInRight go", "fa fa-exclamation-circle"))
          End If
        Next
        skillList.Clear()
        Return New Singular.Web.Result(True) With {.ErrorText = "Deletions Complete", .Data = DeleteResults}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
    Return New Singular.Web.Result(False) With {.ErrorText = "Delete not attempted"}
  End Function

#End Region

#Region " HR - Documents "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHRDocuments(list As OBLib.HR.HRDocumentList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If list.Count > 0 Then
        Dim sh As SaveHelper = TrySave(list)
        If sh.Success Then
          Return New Singular.Web.Result(True)
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Public Sub SaveHRDocumentsByCommand(DocumentList As OBLib.HR.HRDocumentList, CommandArgs As Singular.Web.CommandArgs)
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If DocumentList.Count > 0 Then
        Dim sh As SaveHelper = TrySave(DocumentList)
        If sh.Success Then
          CommandArgs.ReturnData = New Singular.Web.Result(True)
        Else
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Public Sub DeleteHRDocumentsByCommand(DocumentList As OBLib.HR.HRDocumentList, CommandArgs As Singular.Web.CommandArgs)
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      Dim sh As SaveHelper = TrySave(DocumentList)
      If sh.Success Then
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Public Sub FetchHRDocumentsByCommand(HumanResourceID As Integer?, ByRef DocumentList As OBLib.HR.HRDocumentList, CommandArgs As Singular.Web.CommandArgs)
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If HumanResourceID IsNot Nothing Then
        DocumentList = OBLib.HR.HRDocumentList.GetHRDocumentList(HumanResourceID)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Human Resource not provided"}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

#End Region

#Region " HR - Areas "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHRAreas(list As OBLib.HR.HRSystemSelectList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If list.Count > 0 Then
        Dim sh As SaveHelper = TrySave(list)
        If sh.Success Then
          list = sh.SavedObject
          Return New Singular.Web.Result(True) With {.Data = list}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Production Bookings "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ProductionHRRemoveResourceBooking(ProductionHRResourceBookingID As Integer?) As Singular.Web.Result

    If Not IsNullNothing(ProductionHRResourceBookingID) Then
      Try
        Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdProductionHRRemoveResourceBooking]")
        cmdProc.Parameters.AddWithValue("@ProductionHRResourceBookingID", ProductionHRResourceBookingID)
        cmdProc.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmdProc = cmdProc.Execute

        WebsiteHelpers.SendResourceBookingUpdateNotifications()

        Return New Singular.Web.Result(True)

      Catch ex As Exception
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = "Could not identify the booking to be deleted"}
    End If

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function ProductionHRAddResourceBooking(applyResult As ApplyResult) As Singular.Web.Result

    Try
      Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdProductionHRAddResourceBooking]")
      cmdProc.Parameters.AddWithValue("@ResourceID", applyResult.ResourceID)
      cmdProc.Parameters.AddWithValue("@ResourceBookingID", applyResult.ResourceBookingID)
      'cmdProc.Parameters.AddWithValue("@BookingDescription", applyResult.BookingDescription)
      cmdProc.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(applyResult.ProductionSystemAreaID))
      cmdProc.Parameters.AddWithValue("@DisciplineID", applyResult.DisciplineID)
      cmdProc.Parameters.AddWithValue("@HumanResourceID", applyResult.HumanResourceID)
      cmdProc.Parameters.AddWithValue("@AddAnywayReason", applyResult.AddAnywayReason)
      cmdProc.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      'phr
      Dim paramProductionHumanResourceID As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      paramProductionHumanResourceID.Name = "@ProductionHumanResourceID"
      paramProductionHumanResourceID.SqlType = SqlDbType.Int
      paramProductionHumanResourceID.Value = NothingDBNull(applyResult.ProductionHumanResourceID)
      paramProductionHumanResourceID.Direction = ParameterDirection.InputOutput
      cmdProc.Parameters.Add(paramProductionHumanResourceID)

      'ph
      Dim paramProductionHRID As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      paramProductionHRID.Name = "@ProductionHRID"
      paramProductionHRID.SqlType = SqlDbType.Int
      paramProductionHRID.Value = NothingDBNull(Nothing)
      paramProductionHRID.Direction = ParameterDirection.InputOutput
      cmdProc.Parameters.Add(paramProductionHRID)

      'rb
      Dim paramRBID As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      paramRBID.Name = "@ProductionHRResourceBookingID"
      paramRBID.SqlType = SqlDbType.Int
      paramRBID.Value = NothingDBNull(Nothing)
      paramRBID.Direction = ParameterDirection.InputOutput
      cmdProc.Parameters.Add(paramRBID)

      cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute

      WebsiteHelpers.SendResourceBookingUpdateNotifications()

      If paramProductionHRID IsNot Nothing AndAlso paramRBID IsNot Nothing Then
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False)
      End If

    Catch ex As Exception
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

#End Region

#Region " HR - Schedules "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DownloadMonthlyScheduleStateless(HumanResourceID As Integer, StartDate As Date, EndDate As Date) As Singular.Web.Result
    Try
      If HumanResourceID > 0 Then
        Try
          Dim schedule As OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport = New OBWebReports.HumanResourceReports.MonthlyScheduleByIndividualReport
          schedule.ReportCriteria.HumanResourceIDs.Add(HumanResourceID)
          schedule.ReportCriteria.StartDate = Singular.Dates.DateMonthStart(StartDate)
          schedule.ReportCriteria.EndDate = Singular.Dates.DateMonthEnd(EndDate)
          Dim rfi = schedule.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no document provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " HR - Timesheets "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetHumanResourceTimesheet(HumanResourceTimesheetID As Integer?) As Singular.Web.Result
    Try
      If HumanResourceTimesheetID > 0 AndAlso HumanResourceTimesheetID IsNot Nothing Then
        Dim hrtl As OBLib.HR.Timesheets.HumanResourceTimesheetList = OBLib.HR.Timesheets.HumanResourceTimesheetList.GetHumanResourceTimesheetList(HumanResourceTimesheetID)
        If hrtl.Count = 1 Then
          Return New Singular.Web.Result(True) With {.Data = hrtl(0)}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not find timesheet"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no timesheet provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHumanResourceTimesheet(HumanResourceTimesheet As OBLib.HR.Timesheets.HumanResourceTimesheet) As Singular.Web.Result
    Try
      Dim sh As Singular.SaveHelper = HumanResourceTimesheet.TrySave(GetType(OBLib.HR.Timesheets.HumanResourceTimesheetList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteROHumanResourceTimesheets(HumanResourceTimesheets As List(Of OBLib.HR.Timesheets.ReadOnly.ROHumanResourceTimesheetPaged)) As Singular.Web.Result
    Try
      Dim XMLIDs As String = OBLib.OBMisc.IntegerListToXML(HumanResourceTimesheets.Select(Function(d) d.HumanResourceTimesheetID).ToList)
      Dim hrtl As OBLib.HR.Timesheets.HumanResourceTimesheetList = OBLib.HR.Timesheets.HumanResourceTimesheetList.GetHumanResourceTimesheetList(XMLIDs)
      hrtl.Clear()
      Dim sh As Singular.SaveHelper = hrtl.TrySave()
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  '<Singular.Web.WebCallable(LoggedInOnly:=True)>
  'Public Overridable Function GetClashingTimesheets(HumanResourceTimesheet As OBLib.HR.Timesheets.HumanResourceTimesheet) As Singular.Web.Result
  '  Try
  '    Dim ClashCount As Integer = HumanResourceTimesheet.GetClashes()
  '    If sh.Success Then
  '      Return New Singular.Web.Result(False) With {.Data = sh.SavedObject}
  '    Else
  '      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '    End If
  '  Catch ex As Exception
  '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try
  'End Function

#End Region

#Region " Shifts "

#Region " Playout "

  Friend Sub EditPlayoutOpsShift(ByRef ShiftInstance As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift, CommandArgs As Singular.Web.CommandArgs)
    'Dim ID As Integer? = CommandArgs.ClientArgs.HumanResourceShiftID
    Dim res As Singular.Web.Result = FetchPlayoutOpsShift(CommandArgs.ClientArgs.HumanResourceShiftID)
    If res.Success Then
      ShiftInstance = res.Data.Shift
      CommandArgs.ReturnData = New Singular.Web.Result(True)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
    Else
      CommandArgs.ReturnData = res
    End If
  End Sub

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function EditPlayoutOpsShiftServerMethod(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Dim result As Singular.Web.Result = FetchPlayoutOpsShift(HumanResourceShiftID)
    Return result

  End Function

  Friend Sub SavePlayoutOpsShiftCommand(ByRef ShiftInstance As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift, CommandArgs As Singular.Web.CommandArgs)
    Dim res As Singular.Web.Result = SavePlayoutOpsShift(ShiftInstance)
    CommandArgs.ReturnData = res
  End Sub

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function SavePlayoutOpsShiftStateless(PlayoutShift As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) As Singular.Web.Result
    Dim res As Singular.Web.Result = SavePlayoutOpsShift(PlayoutShift)
    Return res
  End Function

  Friend Shared Function SavePlayoutOpsShift(ByRef ShiftInstance As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) As Singular.Web.Result
    Try
      Dim tempShiftList As New PlayoutOperationsShiftList
      tempShiftList.Add(ShiftInstance)
      Dim sh As Singular.SaveHelper = tempShiftList.TrySave
      If sh.Success Then
        tempShiftList = sh.SavedObject
        ShiftInstance = tempShiftList(0)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = ShiftInstance}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Private Shared Function FetchPlayoutOpsShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim shiftList As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShiftList = OBLib.Shifts.PlayoutOperations.PlayoutOperationsShiftList.GetPlayoutOperationsShiftList(HumanResourceShiftID)
      If shiftList.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
      Else
        Return New Singular.Web.Result(True) With {.ErrorText = "Shift could not be found"}
      End If
      Return Nothing
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeletePlayoutOpsShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim supervisorShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) supervisorShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(supervisorShiftIDs)
        Dim shiftList As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShiftList = OBLib.Shifts.PlayoutOperations.PlayoutOperationsShiftList.GetPlayoutOperationsShiftList(xmlString)
        If shiftList.Count = shifts.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True) 'With {.Data = ROResourceBookingHolderList}
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " ICR "

  Friend Sub EditICRShiftCommand(ByRef ShiftInstance As OBLib.Shifts.ICR.ICRShift, CommandArgs As Singular.Web.CommandArgs)
    'Dim ID As Integer? = CommandArgs.ClientArgs.HumanResourceShiftID
    Dim res As Singular.Web.Result = FetchICRShift(CommandArgs.ClientArgs.HumanResourceShiftID)
    If res.Success Then
      ShiftInstance = res.Data.Shift
      CommandArgs.ReturnData = New Singular.Web.Result(True)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
    Else
      CommandArgs.ReturnData = res
    End If
  End Sub

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function EditICRShiftServerMethod(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Dim result As Singular.Web.Result = FetchICRShift(HumanResourceShiftID)
    Return result

  End Function

  Friend Sub SaveICRShiftCommand(ByVal ShiftInstance As OBLib.Shifts.ICR.ICRShift, CommandArgs As Singular.Web.CommandArgs)
    Dim res As Singular.Web.Result = SaveICRShift(ShiftInstance)
    CommandArgs.ReturnData = res
  End Sub

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function SaveICRShiftStateless(ICRShift As OBLib.Shifts.ICR.ICRShift) As Singular.Web.Result
    Dim res As Singular.Web.Result = SaveICRShift(ICRShift)
    Return res
  End Function

  Friend Shared Function SaveICRShift(ByVal ICRShift As OBLib.Shifts.ICR.ICRShift) As Singular.Web.Result
    Try
      Dim tempShiftList As New OBLib.Shifts.ICR.ICRShiftList
      tempShiftList.Add(ICRShift)
      Dim sh As Singular.SaveHelper = tempShiftList.TrySave
      If sh.Success Then
        tempShiftList = sh.SavedObject
        Dim wr As Singular.Web.Result = FetchICRShift(tempShiftList(0).HumanResourceShiftID)
        ICRShift = wr.Data.Shift
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = ICRShift}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  Private Shared Function FetchICRShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim shiftList As OBLib.Shifts.ICR.ICRShiftList = OBLib.Shifts.ICR.ICRShiftList.GetICRShiftList(HumanResourceShiftID)
      If shiftList.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
      Else
        Return New Singular.Web.Result(True) With {.ErrorText = "Shift could not be found"}
      End If
      Return Nothing
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteICRShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim icrShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) icrShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(icrShiftIDs)
        Dim shiftList As OBLib.Shifts.ICR.ICRShiftList = OBLib.Shifts.ICR.ICRShiftList.GetICRShiftList(xmlString)
        If shiftList.Count = shifts.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Studio Supervisors "

  Friend Sub EditStudioSupervisorShift(ByRef ShiftInstance As OBLib.Shifts.Studios.StudioSupervisorShift, CommandArgs As Singular.Web.CommandArgs)
    Dim res As Singular.Web.Result = FetchStudioSupervisorShift(CommandArgs.ClientArgs.HumanResourceShiftID)
    If res.Success Then
      ShiftInstance = res.Data.Shift
      CommandArgs.ReturnData = New Singular.Web.Result(True)
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
    Else
      CommandArgs.ReturnData = res
    End If
    'Try
    '  Dim tempShift As StudioSupervisorShift = Nothing
    '  Dim ID As Integer? = CommandArgs.ClientArgs.HumanResourceShiftID
    '  tempShift = FetchStudioSupervisorShift(ID)
    '  If tempShift IsNot Nothing Then
    '    ShiftInstance = tempShift
    '    CommandArgs.ReturnData = New Singular.Web.Result(True)
    '    WebsiteHelpers.SendResourceBookingUpdateNotifications()
    '  Else
    '    CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "Shift could not be retrieved"}
    '  End If
    'Catch ex As Exception
    '  CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    'End Try
  End Sub

  Friend Sub SaveStudioSupervisorShift(ByRef ShiftInstance As OBLib.Shifts.Studios.StudioSupervisorShift, CommandArgs As Singular.Web.CommandArgs)
    Try
      Dim tempShiftList As New StudioSupervisorShiftList
      tempShiftList.Add(ShiftInstance)
      Dim sh As SaveHelper = TrySave(tempShiftList)
      If sh.Success Then
        tempShiftList = sh.SavedObject
        ShiftInstance = tempShiftList(0)
        CommandArgs.ReturnData = New Singular.Web.Result(True)
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Else
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  'Private Function FetchStudioSupervisorShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

  '  Try
  '    Dim shiftList As OBLib.Shifts.Studios.StudioSupervisorShiftList = OBLib.Shifts.Studios.StudioSupervisorShiftList.GetStudioSupervisorShiftList(HumanResourceShiftID)
  '    If shiftList.Count = 1 Then
  '      Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
  '    Else
  '      Return New Singular.Web.Result(True) With {.ErrorText = "Shift could not be found"}
  '    End If
  '    Return Nothing
  '  Catch ex As Exception
  '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '  End Try
  '  'Dim shiftList As OBLib.Shifts.Studios.StudioSupervisorShiftList = OBLib.Shifts.Studios.StudioSupervisorShiftList.GetStudioSupervisorShiftList(Nothing, HumanResourceShiftID)
  '  'If shiftList.Count = 1 Then
  '  '  Return shiftList(0)
  '  'End If
  '  'Return Nothing

  'End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteStudioSupervisorShifts(shifts As List(Of RSResourceBooking)) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If shifts.Count > 0 Then
        Dim supervisorShiftIDs As New List(Of Integer)
        shifts.ForEach(Sub(s) supervisorShiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(supervisorShiftIDs)
        Dim supervisorShifts As StudioSupervisorShiftList = OBLib.Shifts.Studios.StudioSupervisorShiftList.GetStudioSupervisorShiftList(xmlString)
        If supervisorShifts.Count = shifts.Count Then
          supervisorShifts.Clear()
          Dim sh As SaveHelper = TrySave(supervisorShifts)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True) 'With {.Data = ROResourceBookingHolderList}
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Friend Shared Function EditStudioSupervisorShiftServerMethod(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Dim result As Singular.Web.Result = FetchStudioSupervisorShift(HumanResourceShiftID)
    Return result

  End Function

  Private Shared Function FetchStudioSupervisorShift(HumanResourceShiftID As Integer?) As Singular.Web.Result

    Try
      Dim shiftList As OBLib.Shifts.Studios.StudioSupervisorShiftList = OBLib.Shifts.Studios.StudioSupervisorShiftList.GetStudioSupervisorShiftList(HumanResourceShiftID)
      If shiftList.Count = 1 Then
        Return New Singular.Web.Result(True) With {.Data = New With {.Shift = shiftList(0)}}
      Else
        Return New Singular.Web.Result(True) With {.ErrorText = "Shift could not be found"}
      End If
      Return Nothing
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try

  End Function

#End Region

#Region " Generic "

  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveGenericShift(ByVal GenericShift As OBLib.Shifts.GenericShift) As Singular.Web.Result
    Dim sh As Singular.SaveHelper = GenericShift.TrySave(GetType(OBLib.Shifts.GenericShiftList))
    If sh.Success Then
      GenericShift = sh.SavedObject
      WebsiteHelpers.SendResourceBookingUpdateNotifications()
      Return New Singular.Web.Result(True) With {.Data = GenericShift}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DeleteGenericShifts(bookings As List(Of OBLib.Resources.ResourceBooking)) As Singular.Web.Result
    Try
      If bookings.Count > 0 Then
        Dim shiftIDs As New List(Of Integer)
        bookings.ForEach(Sub(s) shiftIDs.Add(s.HumanResourceShiftID))
        Dim xmlString As String = OBLib.OBMisc.IntegerListToXML(shiftIDs)
        Dim shiftList As OBLib.Shifts.GenericShiftList = OBLib.Shifts.GenericShiftList.GetGenericShiftList(xmlString)
        If shiftList.Count = bookings.Count Then
          shiftList.Clear()
          Dim sh As SaveHelper = TrySave(shiftList)
          If sh.Success Then
            WebsiteHelpers.SendResourceBookingUpdateNotifications()
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = "could not retrieve all shifts"}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function


  <WebCallable(LoggedInOnly:=True)>
  Public Shared Function CreateQuickShift(SystemID As Integer?, ProductionAreaID As Integer?,
                                          ShiftTypeID As Integer?, ShiftType As String,
                                          HumanResourceID As Integer?, ResourceID As Integer?, HumanResource As String,
                                          DisciplineID As Integer?, Discipline As String,
                                          StartDateTime As DateTime?, EndDateTime As DateTime?) As Singular.Web.Result

    Dim gs As New GenericShift With {
                                      .SystemID = SystemID, .ProductionAreaID = ProductionAreaID,
                                      .ShiftTypeID = ShiftTypeID, .ShiftType = ShiftType,
                                      .HumanResourceID = HumanResourceID, .ResourceID = ResourceID, .HumanResource = HumanResource,
                                      .DisciplineID = DisciplineID, .Discipline = Discipline,
                                      .StartDateTime = StartDateTime, .EndDateTime = EndDateTime,
                                      .ProductionAreaStatusID = 1, .ProductionAreaStatus = "New",
                                      .ResourceBookingDescription = ShiftType,
                                      .ScheduleDate = StartDateTime
    }

    gs.UpdateClashes()
    gs.CheckAllRules()

    If gs.IsValid Then
      Dim sh As Singular.SaveHelper = gs.TrySave(GetType(OBLib.Shifts.GenericShiftList))
      If sh.Success Then
        gs = sh.SavedObject
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        Return New Singular.Web.Result(True) With {.Data = gs}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = gs.GetErrorsAsHTMLString}
    End If

  End Function

#End Region

#End Region

#Region " User Profile "

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveUserProfileShiftList(shiftList As OBLib.Users.UserProfileShiftList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If shiftList.Count > 0 Then
        Dim sh As SaveHelper = TrySave(shiftList)
        If sh.Success Then
          shiftList = sh.SavedObject
          Return New Singular.Web.Result(True) With {.Data = shiftList}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function GetUserSchedule(StartDate As DateTime?, EndDate As DateTime?) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    'ResourceID As Integer?, UserID As Integer?, 
    Try
      If StartDate IsNot Nothing AndAlso EndDate IsNot Nothing Then
        Dim ROResourceBookingHolderList As OBLib.HR.Schedule.ROResourceBookingHolderList
        ROResourceBookingHolderList = OBLib.HR.Schedule.ROResourceBookingHolderList.GetROResourceBookingHolderList(Nothing,
                                                                                                                   OBLib.Security.Settings.CurrentUser.UserID,
                                                                                                                   StartDate,
                                                                                                                   EndDate)
        Return New Singular.Web.Result(True) With {.Data = ROResourceBookingHolderList}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <WebCallable(LoggedInOnly:=True)>
  Public Overridable Function SaveHumanResourceSwapList(HumanResourceSwapList As OBLib.HR.HumanResourceSwapList) As Singular.Web.Result
    'Dim DeleteResults As New List(Of Singular.Web.Result)
    Try
      If HumanResourceSwapList.Count > 0 Then
        Dim sh As SaveHelper = TrySave(HumanResourceSwapList)
        If sh.Success Then
          HumanResourceSwapList = sh.SavedObject
          Return New Singular.Web.Result(True) With {.Data = HumanResourceSwapList}
        Else
          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        End If
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "0 items provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Documents "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function DownloadDocumentStateless(DocumentID As Integer) As Singular.Web.Result
    Try
      If DocumentID > 0 Then
        Try
          Dim FileToSend As Singular.Documents.Document = Singular.Documents.Document.GetDocument(DocumentID)
          Dim TempDoc As New Singular.Documents.TemporaryDocument(FileToSend.DocumentName, FileToSend.Document)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no document provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Email "

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function PrepareEmailsForSending(EmailBatchID As Integer?, EmailIDsXml As String) As Singular.Web.Result
    Try
      If EmailBatchID > 0 Or EmailIDsXml.Trim.Length > 0 Then
        Try
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEmailsPrepareForSending",
                                                  New String() {"@EmailBatchID",
                                                                "@EmailIDsXml",
                                                                "@CurrentUserID"},
                                                  New Object() {NothingDBNull(EmailBatchID),
                                                                Singular.Strings.MakeEmptyDBNull(EmailIDsXml),
                                                                OBLib.Security.Settings.CurrentUserID})
          cmdProc.CommandTimeout = 0
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
          cmdProc = cmdProc.Execute
          Dim ItemCount As Integer = cmdProc.DataObject
          Return New Singular.Web.Result(True) With {.ErrorText = ItemCount.ToString}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "batch or emails not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DownloadEmailBatchAttachmentsStateless(EmailBatchID As Integer) As Singular.Web.Result
    Try
      If Not IsNullNothing(EmailBatchID) AndAlso EmailBatchID > 0 Then
        Try
          'Fetch the list
          Dim eml As OBLib.Notifications.Email.BatchEmailList = OBLib.Notifications.Email.BatchEmailList.GetBatchEmailList(EmailBatchID, True)
          'Create the Zip File
          Dim FileBytes As List(Of Byte()) = eml.Select(Function(d) d.EmailAttachment.AttachmentData).ToList
          Dim FileNames As List(Of String) = eml.Select(Function(d) d.EmailAttachment.AttachmentName).ToList
          Dim RetInfo As New ReportFileInfo
          Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes.ToArray, FileNames.ToArray)
          Dim ms As New IO.MemoryStream(CompressedFiles)
          RetInfo.FileStream = ms
          RetInfo.FileName = "Batch_" & EmailBatchID.ToString & "_Attachments.zip"
          'Save the Document Temporarily for downloading
          Dim TempDoc As New Singular.Documents.TemporaryDocument(RetInfo.FileName, RetInfo.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no batch provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DownloadEmailAttachmentsStateless(EmailID As Integer) As Singular.Web.Result
    Try
      If Not IsNullNothing(EmailID) AndAlso EmailID > 0 Then
        Try
          'Fetch the list
          Dim idList As New List(Of Integer)
          idList.Add(EmailID)
          Dim eml As OBLib.Notifications.Email.SoberEmailList = OBLib.Notifications.Email.SoberEmailList.GetSoberEmailList(idList, True)
          Dim attachments As SoberEmailAttachmentList = eml(0).SoberEmailAttachmentList
          'Create the Zip File
          Dim FileBytes As List(Of Byte()) = attachments.Select(Function(d) d.AttachmentData).ToList
          Dim FileNames As List(Of String) = attachments.Select(Function(d) d.AttachmentName).ToList
          Dim RetInfo As New ReportFileInfo
          Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes.ToArray, FileNames.ToArray)
          Dim ms As New IO.MemoryStream(CompressedFiles)
          RetInfo.FileStream = ms
          RetInfo.FileName = "Email" & EmailID.ToString & "_Attachments.zip"
          'Save the Document Temporarily for downloading
          Dim TempDoc As New Singular.Documents.TemporaryDocument(RetInfo.FileName, RetInfo.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no email provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DeleteEmailBatches(EmailBatchIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If EmailBatchIDs.Count > 0 Then
        Try
          Dim eml As BatchEmailList = OBLib.Notifications.Email.BatchEmailList.GetBatchEmailList(EmailBatchIDs, False)
          eml.Clear()
          Dim sh As SaveHelper = TrySave(eml)
          If sh.Success Then
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no batches provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Email Sender.Access"})>
  Public Overridable Function DeleteEmails(EmailIDs As List(Of Integer)) As Singular.Web.Result
    Try
      If EmailIDs.Count > 0 Then
        Try
          Dim eml As SoberEmailList = OBLib.Notifications.Email.SoberEmailList.GetSoberEmailList(EmailIDs, False)
          eml.Clear()
          Dim sh As SaveHelper = TrySave(eml)
          If sh.Success Then
            Return New Singular.Web.Result(True)
          Else
            Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
          End If
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no emails provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

#Region " Sms "

  <Singular.Web.WebCallable(LoggedInOnly:=True, Roles:={"Sms Sender.Access"})>
  Public Overridable Function PrepareSmsesForSending(SmsBatchID As Integer?, SmsIDsXml As String) As Singular.Web.Result
    Try
      If (Not IsNullNothing(SmsBatchID) AndAlso SmsBatchID > 0) Or (SmsIDsXml.Trim.Length > 0) Then
        Try
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSPrepareForSending",
                                                  New String() {"@SmsBatchID",
                                                                "@SmsIDsXml",
                                                                "@CurrentUserID"},
                                                  New Object() {NothingDBNull(SmsBatchID),
                                                                Singular.Strings.MakeEmptyDBNull(SmsIDsXml),
                                                                OBLib.Security.Settings.CurrentUserID})
          cmdProc.CommandTimeout = 0
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
          cmdProc = cmdProc.Execute
          Dim ItemCount As Integer = cmdProc.DataObject
          Return New Singular.Web.Result(True) With {.ErrorText = ItemCount.ToString}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "batch or emails not provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

#End Region

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Overridable Function PrintServicesFreelancerTimesheet(HumanResourceID As Integer, EndDate As Date) As Singular.Web.Result
    Try
      If HumanResourceID > 0 Then
        Try
          'Dim d As Date = Singular.Dates.DateMonthEnd(EndDate) '.AddDays(1)
          Dim rpt As New OBWebReports.HumanResourceReports.HRTimesheet
          rpt.ReportCriteria.HumanResourceID = HumanResourceID
          rpt.ReportCriteria.EndDate = EndDate
          Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          Dim TempDoc As New Singular.Documents.TemporaryDocument(rfi.FileName, rfi.FileBytes)
          Return New Singular.Web.Result(True) With {.Data = Singular.Web.WebServices.FileDownloadHandler.SaveTempFile(TempDoc, 30, )}
        Catch ex As Exception
          Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
        End Try
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "no document provided"}
      End If
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Function

  '#Region " Outside Broadcast "

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionGeneralCommentsServerMethod(ProductionGeneralCommentList As OBLib.Productions.Correspondence.ProductionGeneralCommentList) As Singular.Web.Result
  '    Try
  '      If ProductionGeneralCommentList.Count > 0 Then
  '        Dim sh As Singular.SaveHelper = ProductionGeneralCommentList.TrySave
  '        If sh.Success Then
  '          Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '        Else
  '          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '        End If
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = "no comments provided"}
  '      End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  '  Public Shared Function SaveProductionPettyCashServerMethod(ProductionPettyCashList As OBLib.Productions.Correspondence.ProductionPettyCashList) As Singular.Web.Result
  '    Try
  '      If ProductionPettyCashList.Count > 0 Then
  '        Dim sh As Singular.SaveHelper = ProductionPettyCashList.TrySave
  '        If sh.Success Then
  '          Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
  '        Else
  '          Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '        End If
  '      Else
  '        Return New Singular.Web.Result(False) With {.ErrorText = "no petty cash provided"}
  '      End If
  '    Catch ex As Exception
  '      Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Function

  '#End Region

End Class
