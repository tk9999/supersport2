﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Login.Master"
  CodeBehind="BrowserUpgrade.aspx.vb" Inherits="NewOBWeb.BrowserUpgrade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="Bootstrap/override/signin.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    body, .panel, .panel-heading
    {
      background-image: none !important;
      background-color: #ffffff !important;
      background-repeat: repeat-x !important;
    }
    .panel
    {
      border: none;
    }
    .panel-default > .panel-heading
    {
      border: none;
      background-color: #ffffff !important;
    }
    .container
    {
      margin-top: 100px;
    }
    .Msg div, .ValidationPopup
    {
      border-style: solid;
      border-width: 1px;
      border-radius: 4px;
      margin: 6px 0 5px 0;
      background-repeat: no-repeat;
      background-position: 3px 3px;
      padding: 3px 10px 6px 24px;
      font-size: 0.9em;
      color: #000;
    }
    .Msg div strong
    {
      line-height: 1.5em;
    }
    .Msg-Validation, .Msg-Error
    {
      background-color: #FBECEB;
      border-color: #c44;
    }
    .Msg-Validation
    {
      background-image: url('../Images/IconBRule.png');
      overflow: auto;
    }
    .Msg-Validation > ul
    {
      padding-left: 0;
    }
    .Msg-Validation strong, .Msg-Error strong
    {
      color: #c44;
      line-height: 1.2em;
    }
    .Msg-Error
    {
      background-image: url('../Images/IconError.png');
    }
    .Msg-Success
    {
      background-image: url('../Images/IconAuth.png');
      background-color: #F1F8EE;
      border-color: #4a2;
    }
    .Msg-Success strong
    {
      color: #291;
    }
    
    .Msg-Warning
    {
      background-image: url('../Images/IconWarning.png');
      background-color: #FFF7E5;
      border-color: #D9980D;
    }
    .Msg-Warning strong
    {
      color: #D9980D;
    }
    
    .Msg-Information
    {
      background-image: url('../Images/IconInfo.png');
      background-color: #E9F1FB;
      border-color: #225FAA;
    }
    .Msg-Information strong
    {
      color: #225FAA;
    }
    .MsgBoxMessage > ul
    {
      padding-left: 0;
    }
    .login-wrapper .box
    {
      margin: 0 auto;
      padding: 35px 0 30px;
      float: none;
      width: auto !important;
      box-shadow: 0 0 6px 2px rgba(0, 0, 0, 0.1);
      border-radius: 5px;
      background: rgba(255, 255, 255, 0.65);
    }
    .login-wrapper ul,  .login-wrapper p
    {
      text-align: left;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div class="login-wrapper">
    <div class="box">
      <div class="content-wrap">
        <h6>
          Broswer upgrade required</h6>
        <p style="font-size:16px">
          Please note that the website requires you to use one of the following Web Browsers
          in order to function correctly:</p>
        <ul>
          <li><span><strong>Google Chrome: </strong></span><span><a href="https://www.google.com/intl/en/chrome/browser/">https://www.google.com/intl/en/chrome/browser/</a></span></li>
          <li><span><strong>Firefox: </strong></span><span><a href="http://www.mozilla.org/en-US/firefox/new/">http://www.mozilla.org/en-US/firefox/new/</a></span></li>
          <li><span><strong>Safari: </strong></span><span><a href="https://www.apple.com/za/safari/">https://www.apple.com/za/safari/</a></span></li>
          <li><span><strong>Internet Explorer 11: </strong></span><span><a href="http://windows.microsoft.com/en-ZA/internet-explorer/download-ie">http://windows.microsoft.com/en-ZA/internet-explorer/download-ie</a></span></li>
        </ul>
      </div>
    </div>
  </div>
</asp:Content>
