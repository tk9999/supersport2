﻿Public Class SignalRWindowsReceiver
  Inherits OBPageBase(Of SignalRWindowsReceiverVM)

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    WebsiteHelpers.SendResourceBookingUpdateNotifications()
    If Page.Request.QueryString("A") IsNot Nothing AndAlso Page.Request.QueryString("A") = "Synergy" Then
      Dim ID As String = Page.Request.QueryString("ID")
      WebsiteHelpers.PostSynergyChanges(ID)
    End If
  End Sub

End Class

Public Class SignalRWindowsReceiverVM
  Inherits OBViewModel(Of SignalRWindowsReceiverVM)

End Class