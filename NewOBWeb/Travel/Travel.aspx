﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" ValidateRequest="false"
  CodeBehind="Travel.aspx.vb" Inherits="NewOBWeb.Travel" %>

<%@ Import Namespace="OBLib.Travel.ReadOnly" %>

<%@ Import Namespace="OBLib.Travel.CrewMembers.ReadOnly" %>

<%@ Import Namespace="OBLib.Travel.CrewMembers" %>
<%@ Import Namespace="OBLib.Synergy.Importer.New" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.Synergy.ImportedEvent" %>
<%@ Import Namespace="OBLib.Synergy.ReadOnly" %>
<%@ Import Namespace="OBLib.Maintenance.Resources.ReadOnly" %>
<%@ Import Namespace="OBLib.AdHoc.Travel" %>
<%@ Import Namespace="OBLib.AdHoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    body {
      background-color: #F0F0F0;
    }

    #FindAdHocBooking .modal-content, #NewAdHocBooking .modal-content {
      background-color: #F0F0F0;
    }

    tr.danger td {
      background-color: #e74225 !important;
      color: #fff;
    }

    .CancelledOnAll {
      background-color: #FF6347 !important;
    }

    tr.CancelledOnAll td {
      background-color: #FF6347 !important;
      color: white;
    }

    .CancelledOnFlight {
      background-color: #FF6347 !important;
    }

    tr.CancelledOnFlight td {
      background-color: #FF6347 !important;
      color: white;
    }

    .CancelledOnRentalCar {
      background-color: #FF6347 !important;
    }

    tr.CancelledOnRentalCar td {
      background-color: #FF6347 !important;
      color: white;
    }

    .CancelledOnAccommodation {
      background-color: #FF6347 !important;
    }

    tr.CancelledOnAccommodation td {
      background-color: #FF6347 !important;
      color: white;
    }

    .CancelledOnChauffeur {
      background-color: #FF6347 !important;
    }

    tr.CancelledOnChauffeur td {
      background-color: #FF6347 !important;
      color: white;
    }

    .CancelledOnTravelAdvance {
      background-color: #FF6347 !important;
    }

    tr.CancelledOnTravelAdvance td {
      background-color: #FF6347 !important;
      color: white;
    }

    .btn-custom {
      font-size: 11px;
      font-weight: normal;
      line-height: 1.428571429;
      text-align: center;
      white-space: nowrap;
    }

    .AdHocHumanResourceID {
      float: left;
      width: 160px;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    td.LButtons, th.LButtons {
      width: 24px !important;
    }

    .ValidationPopup {
      min-height: 131px;
      max-height: 650px;
    }

    .snt-column-widths-80 {
      width: 80px;
    }

    .snt-column-widths-100 {
      width: 100px;
    }

    table th {
      font-weight: 700 !important;
    }

    table td span, table td input {
      font-weight: 400 !important;
    }


    div.traveller-cell {
      background-color: #f0f0f0;
    }

      div.traveller-cell > div.type-to-search-row {
        margin-bottom: 8px;
      }

    div.traveller-list {
      height: 60px;
      margin-left: 5px;
      margin-right: 5px;
      background-color: #fff;
      margin-bottom: 8px;
      -webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.75);
      -moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.75);
      box-shadow: 0px 2px 3px 0px #999999;
    }

      div.traveller-list > div.avatar {
        display: inline-block;
        width: 60px;
        height: 100%;
      }

    div.info div.avatar > img {
      width: 40px;
      height: 40px;
      border-radius: 0px;
    }

    div.traveller-list > div.avatar > h5 {
    }

    div.traveller-list > div.info {
      display: inline-block;
      width: 100%;
      vertical-align: top;
      height: 70px;
    }

    div.traveller-list div.progress-bar {
      transition-delay: 1s;
      -webkit-transition-delay: 1s;
    }

    .ComboDropDown tr:hover, .ComboDropDown div.traveller-list:hover {
      color: #000 !important;
      background-color: #cccccc !important;
    }

    .ComboDropDown div.traveller-list.Selected {
      text-decoration: none !important;
      color: #000 !important;
      background-color: #cccccc !important;
    }

    div.traveller-list td {
      border: 0 !important;
    }

    /*div.traveller-list-dropdown > div.type-to-search-row {
      background: #f0f0f0;
      color: #000;
    }

    div.traveller-list-dropdown {
      z-index: 100000;
      background-clip: padding-box;
      border: 1px solid #ccc;
      border: 1px solid rgba(0,0,0,.2);
      border-radius: 6px;
      -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
      box-shadow: 0 5px 10px rgba(0,0,0,.2);
      padding: 10px;
      background: #f0f0f0;
    }*/

    table input:disabled {
      background-color: #eae7e7;
    }

      table input:disabled:hover {
        cursor: not-allowed !important;
      }
  </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%Using h = Helpers

      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Toolbar
            With .Helpers.MessageHolder()
            End With
          End With
        End With
      End With

      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Travel", False)
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.Bootstrap.Button(, "Back to Production", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
                                                 Singular.Web.PostBackType.None, "TravelVM.backToProduction()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('BackToProductionButton', ViewModel.TravelRequisition())")
                  End With
                  With .Helpers.Bootstrap.Button(, "Find Requisition", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", ,
                                             Singular.Web.PostBackType.None, "TravelVM.findRequisition()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('FindRequisitionButton', ViewModel.TravelRequisition())")
                  End With
                  With .Helpers.Bootstrap.Button(, "New Requisition", Singular.Web.BootstrapEnums.Style.Primary, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", ,
                                             Singular.Web.PostBackType.None, "TravelVM.newAdHocBooking()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('AddRequisitionButton', ViewModel.TravelRequisition())")
                  End With
                  With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                    With .Helpers.Bootstrap.Button("", "Save Requisition", Singular.Web.BootstrapEnums.Style.Success, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                                   Singular.Web.PostBackType.None, "TravelVM.saveRequisition()")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.TravelRequisition.IsValid)
                    End With
                    With .Helpers.Bootstrap.Button(, "Edit Booking Details", Singular.Web.BootstrapEnums.Style.Info, ,
                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-edit", ,
                               Singular.Web.PostBackType.None, "TravelVM.editAdHocBooking()")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelVM.canView('EditAdHocBookingButton')")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                    End With
                  End With
                End With
                'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                '  With .Helpers.Toolbar
                '    With .Helpers.MessageHolder()
                '    End With
                '  End With
                'End With
              End With
            End With
          End With
        End With
      End With


      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.TabControl(, "", )
            .ParentDiv.AddClass("animated slideInUp fast go")
            .ParentDiv.AddBinding(Singular.Web.KnockoutBindingString.if, "ViewModel.TravelRequisition()")

            With .AddTab("Travellers", "fa-group", "TravelVM.fetchTravellers()", , , True)
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('TravellersTab', ViewModel.TravelRequisition())")
              With .TabPane
                .AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('TravellersTab', ViewModel.TravelRequisition())")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.DivC("pull-left")
                      With .Helpers.Bootstrap.Button(, "Add Travellers", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus", ,
                                                     Singular.Web.PostBackType.None, "TravelVM.showTravellersModal()", )
                      End With
                    End With
                    With .Helpers.DivC("pull-right")
                      With .Helpers.Bootstrap.Button(, "Cancel Selected", Singular.Web.BootstrapEnums.Style.Warning, , , , "fa-eraser", , , "TravelVM.cancelSelectedTravellers()", )
                      End With
                      With .Helpers.Bootstrap.Button(, "Remove Selected", Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TravelVM.removeSelectedTravellers()", )
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Travellers.TravelRequisitionTraveller)(Function(d As OBLib.Travel.TravelRequisition) d.TravellerList,
                                                                                                              False, False, False, True, True, True, True, "Travellers")
                        .AddClass("hover list")
                        With .FirstRow
                          .AddClass("items selectable")
                          With .AddColumn
                            .Style.Width = 80
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              .Button.AddBinding(KnockoutBindingString.enable, "TravelRequisitionTravellerBO.canEdit($data, 'IsSelected')")
                            End With
                          End With
                          With .AddColumn
                            .Style.Width = 80
                            With .Helpers.Bootstrap.Button(, "View Details", Singular.Web.BootstrapEnums.Style.Warning, "",
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, "", "", ,
                                                           Singular.Web.PostBackType.None, "TravelVM.viewTravellerDetail($data)", )
                              .Button.AddBinding(KnockoutBindingString.enable, "TravelRequisitionTravellerBO.canEdit($data, 'ViewDetails')")
                            End With
                          End With
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravelRequisitionTraveller) c.HumanResource)
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravelRequisitionTraveller) c.IDNo)
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravelRequisitionTraveller) c.CellPhoneNumber)
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravelRequisitionTraveller) c.EmailAddress)
                          With .AddColumn("")
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plane", , Singular.Web.PostBackType.None, , )
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, Function(d As OBLib.Travel.Travellers.TravelRequisitionTraveller) d.FlightCount)
                            End With
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-car", , Singular.Web.PostBackType.None, , )
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, Function(d As OBLib.Travel.Travellers.TravelRequisitionTraveller) d.RentalCarCount)
                            End With
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-building", , Singular.Web.PostBackType.None, , )
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, Function(d As OBLib.Travel.Travellers.TravelRequisitionTraveller) d.AccommodationCount)
                            End With
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-car", , Singular.Web.PostBackType.None, , )
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, Function(d As OBLib.Travel.Travellers.TravelRequisitionTraveller) d.ChauffeurCount)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("TravelAvailability", "fa fa-group", "TravelVM.fetchTravelAvailability()", "Travellers (Availability)", , )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('TravelAvailabilityTab', ViewModel.TravelRequisition())")
              With .TabPane
                .AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('TravelAvailabilityTab', ViewModel.TravelRequisition())")
                With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.CrewMembers.ReadOnly.ROTravelAvailableHumanResource)(Function(c As OBLib.Travel.TravelRequisition) c.ROTravelAvailableHumanResourceList,
                                                                                                                              False, False, False, True, True, True, True, "ROTravelAvailableHumanResourceList")
                          With .FirstRow
                            .AddBinding(Singular.Web.KnockoutBindingString.css, "ROTravelAvailableHumanResourceBO.crewMemberCancelledStateColor($data)")
                            With .AddColumn
                              .Style.Width = 80
                              With .Helpers.Bootstrap.Button(, "View Details", Singular.Web.BootstrapEnums.Style.Warning, "",
                                                             Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, "", "", ,
                                                             Singular.Web.PostBackType.None, "TravelVM.viewCrewMemberDetail($data)", )
                              End With
                            End With
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.HumanResource)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.BaseCityCode)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.CrewType)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.Disciplines)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.PreviousProductionName)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.PreviousProductionCity)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.PreviousProductionCallTimeString)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.NextProductionName)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.NextProductionCity)
                            .AddReadOnlyColumn(Function(c As ROTravelAvailableHumanResource) c.NxtProductionCallTimeString)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Flights", "fa-plane", "TravelVM.fetchFlights()")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                    'With .Helpers.DivC("pull-left")
                    '  With .Helpers.Bootstrap.Button(, "Bulk Add", Singular.Web.BootstrapEnums.Style.Primary, , , , "fa-plus", ,
                    '                                 Singular.Web.PostBackType.None, "TravelVM.showFlightModal()", )
                    '  End With
                    'End With
                    With .Helpers.DivC("pull-right")
                      With .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-trash", , , "TravelVM.removeSelectedFlights()", )
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Flights.Flight)(Function(d As OBLib.Travel.TravelRequisition) d.FlightList,
                                                                                       True, False, False, True, True, True, True, "FlightList")
                        .AddClass("hover list")


                        With .FirstRow
                          .AddClass("items selectable")
                          With .AddColumn
                            .Style.Width = 70
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'IsSelected')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightBO.canEdit($data, 'IsSelected')")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.CrewTypeID, 80)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'CrewTypeID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.FlightNo, 70)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'FlightNo')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.TravelDirectionID, 110)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'TravelDirectionID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.FlightTypeID)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'FlightTypeID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.AirportIDFrom)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'AirportIDFrom')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.DepartureDateTime)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'DepartureDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.DepartureDateTime)
                            .HeaderText = "Time"
                            .Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.TimeEditorFor(Function(d As OBLib.Travel.Flights.Flight) d.DepartureDateTime)
                              .Style.Width = "50px"
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'DepartureDateTime')")
                            End With
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'DepartureDateTime')")
                            .Editor.Style.Width = "50px"
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.AirportIDTo)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'AirportIDTo')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.ArrivalDateTime)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'ArrivalDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.ArrivalDateTime)
                            .HeaderText = "Time"
                            .Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.TimeEditorFor(Function(d As OBLib.Travel.Flights.Flight) d.ArrivalDateTime)
                              .Style.Width = "50px"
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'ArrivalDateTime')")
                            End With
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'ArrivalDateTime')")
                            .Editor.Style.Width = "50px"
                          End With
                          '.AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.PassengerCount)
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                           "fa-user", , Singular.Web.PostBackType.None, "TravelVM.bookFlightPassengers($data)")
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Travel.Flights.Flight) d.PassengerCount)
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "FlightBO.flightPassengerButtonCSS($data)")
                              .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "FlightBO.flightPassengerButtonIcon($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'FlightPassengersButton')")
                              '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightBO.canView($data, 'FlightPassengersButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", "btn-default", "fa-frown-o", "fa-frown-o", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'CancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightBO.canView($data, 'CancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "UnCancel", "UnCancel", "btn-info", "btn-info", "", "", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'UnCancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightBO.canView($data, 'UnCancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(d As OBLib.Travel.Flights.Flight) d.CancelledReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'CancelledReason')")
                          End With
                          With .AddColumn("")
                            .Style.Width = 50
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Success, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , ,
                                                           "FlightBO.saveItem($data, {})", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'SaveButton')")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Rental Cars", "fa-car", "TravelVM.fetchRentalCars()")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                    With .Helpers.DivC("pull-left")
                      With .Helpers.Bootstrap.Button(, "Bulk Add", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus", , Singular.Web.PostBackType.None, "TravelVM.setupBulkRentalCar()", )
                      End With
                    End With
                    With .Helpers.DivC("pull-right")
                      With .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-minus", , , "TravelVM.removeSelectedRentalCars()", )
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.RentalCars.RentalCar)(Function(d As OBLib.Travel.TravelRequisition) d.RentalCarList,
                                                                                             True, False, False, True, True, True, True, "RentalCarList",
                                                                                             "RentalCarBO.afterItemAdded")
                        .AddClass("hover list")
                        With .FirstRow
                          .AddClass("items selectable")
                          With .AddColumn
                            .Style.Width = 80
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'IsSelected')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarBO.canEdit($data, 'IsSelected')")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.CrewTypeID)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CrewTypeID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.RentalCarAgentID)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'RentalCarAgentID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.CarTypeID, 75)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CarTypeID')")
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.MaxSeats, 50)
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CarTypeID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.AgentBranchIDFrom)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'AgentBranchIDFrom')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.PickupDateTime)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'PickupDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.PickupDateTime)
                            .HeaderText = "Time"
                            .Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.TimeEditorFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.PickupDateTime)
                              .Style.Width = "50px"
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'PickupDateTime')")
                            End With
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'PickupDateTime')")
                            .Editor.Style.Width = "50px"
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.AgentBranchIDTo)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'AgentBranchIDTo')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.DropoffDateTime)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'DropoffDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.DropoffDateTime)
                            .HeaderText = "Time"
                            .Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.TimeEditorFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.DropoffDateTime)
                              .Style.Width = "50px"
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'DropoffDateTime')")
                            End With
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'DropoffDateTime')")
                            .Editor.Style.Width = "50px"
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.Driver)
                            .Style.Width = "100px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.EditorFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.DriverHumanResourceID)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As OBLib.Travel.RentalCars.RentalCar) c.IsNew)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'DriverHumanResourceID')")
                            End With
                            With .Helpers.ReadOnlyFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.Driver)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As OBLib.Travel.RentalCars.RentalCar) Not c.IsNew)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'Driver')")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.CoDriver)
                            .Style.Width = "100px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.EditorFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CoDriverHumanResourceID)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As OBLib.Travel.RentalCars.RentalCar) c.IsNew)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CoDriverHumanResourceID')")
                            End With
                            With .Helpers.ReadOnlyFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CoDriver)
                              .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As OBLib.Travel.RentalCars.RentalCar) Not c.IsNew)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CoDriver')")
                            End With
                          End With
                          'With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.DriverHumanResourceID)
                          'End With
                          With .AddReadOnlyColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.RemainingSeats)
                            .Style.Width = "80px"
                          End With
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                           "fa-user", , Singular.Web.PostBackType.None, "TravelVM.bookRentalCarPassengers($data)")
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Travel.RentalCars.RentalCar) d.PassengerCount)
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "RentalCarBO.rentalCarPassengerButtonCSS($data)")
                              .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RentalCarBO.rentalCarPassengerButtonIcon($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'RentalCarPassengersButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", "btn-default", "fa-frown-o", "fa-frown-o", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarBO.canView($data, 'CancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "UnCancel", "UnCancel", "btn-info", "btn-info", "fa-question-circle", "fa-question-circle", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'UnCancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarBO.canView($data, 'UnCancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CancelledReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CancelledReason')")
                          End With
                          With .AddColumn("")
                            .Style.Width = 50
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Success, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , ,
                                                           "RentalCarBO.saveItem($data, {})", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'SaveButton')")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Accommodation", "fa-building-o", "TravelVM.fetchAccommodation()")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                    'With .Helpers.DivC("pull-left")
                    '  With .Helpers.Bootstrap.Button(, "Bulk Add", Singular.Web.BootstrapEnums.Style.Primary, , , , "fa-plus", ,
                    '                                 Singular.Web.PostBackType.Ajax, "TravelVM.showAccommodationModal()", )
                    '  End With
                    'End With
                    With .Helpers.DivC("pull-right")
                      With .Helpers.Bootstrap.Button(, "Remove Selected", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-trash", , , "TravelVM.removeSelectedAccommodation()", )
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Accommodation.Accommodation)(Function(d As OBLib.Travel.TravelRequisition) d.AccommodationList,
                                                                                                    True, False, False, True, True, True, True, "AccommodationList")
                        .AddClass("hover list")


                        With .FirstRow
                          .AddClass("items selectable")
                          With .AddColumn
                            .Style.Width = 80
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'IsSelected')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "AccommodationBO.canEdit($data, 'IsSelected')")
                            End With
                          End With
                          '.AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.CrewTypeID)
                          With .AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.AccommodationProviderID)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'AccommodationProviderID')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.CheckInDate)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CheckInDate')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.CheckOutDate)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CheckOutDate')")
                          End With
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                           "fa-user", , Singular.Web.PostBackType.None, "TravelVM.bookAccommodationGuests($data)")
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Travel.Accommodation.Accommodation) d.GuestCount)
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "AccommodationBO.accommodationGuestButtonCSS($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'AccommodationGuestsButton')")
                              .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "AccommodationBO.accommodationGuestButtonIcon($data)")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.NightsAway)
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", "btn-default", "fa-frown-o", "fa-frown-o", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "AccommodationBO.canView($data, 'CancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "UnCancel", "UnCancel", "btn-info", "btn-info", "fa-question-circle", "fa-question-circle", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'UnCancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "AccommodationBO.canView($data, 'UnCancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CancelledReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CancelledReason')")
                          End With
                          With .AddColumn("")
                            .Style.Width = 50
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Success, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , ,
                                                           "AccommodationBO.saveItem($data, {})", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'SaveButton')")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Chauffeurs", "fa-taxi", "TravelVM.fetchChauffeurs()")
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('ChauffersTab', ViewModel.TravelRequisition())")
              With .TabPane
                .AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('ChauffersTab', ViewModel.TravelRequisition())")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                    'With .Helpers.DivC("pull-left")
                    '  With .Helpers.Bootstrap.Button(, "Bulk Add", Singular.Web.BootstrapEnums.Style.Primary, , , , "fa-plus", ,
                    '                                 Singular.Web.PostBackType.Ajax, "TravelVM.showChauffeurDriveModal()", )
                    '  End With
                    'End With
                    With .Helpers.DivC("pull-right")
                      With .Helpers.Bootstrap.Button(, "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TravelVM.removeSelectedChauffeurDrives()", )
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Chauffeurs.ChauffeurDriver)(Function(d As OBLib.Travel.TravelRequisition) d.ChauffeurDriverList,
                                                                                                   True, False, False, True, True, True, True, "ChauffeurDriverList")
                        .AddClass("hover list")
                        With .FirstRow
                          With .AddColumn("")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'IsSelected')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ChauffeurDriverBO.canEdit($data, 'IsSelected')")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpLocation)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'PickUpLocation')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpDateTime)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'PickUpDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpDateTime)
                            .HeaderText = "Time"
                            .Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.TimeEditorFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.PickUpDateTime)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'PickUpDateTime')")
                            End With
                            .Editor.Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'PickUpDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffLocation)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'DropOffLocation')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffDateTime)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'DropOffDateTime')")
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffDateTime)
                            .HeaderText = "Time"
                            .Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                            With .Helpers.TimeEditorFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.DropOffDateTime)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'DropOffDateTime')")
                            End With
                            .Editor.Style.Width = "50px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'DropOffDateTime')")
                          End With
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                           "fa-user", , Singular.Web.PostBackType.None, "TravelVM.bookChaufferPassengers($data)")
                              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.PassengerCount)
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "ChauffeurDriverBO.chauffeurPassengerButtonCSS($data)")
                              .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "ChauffeurDriverBO.chauffeurPassengerButtonIcon($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'ChauffeurPassengersButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn("")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", "btn-default", "fa-frown-o", "fa-frown-o", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'CancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ChauffeurDriverBO.canView($data, 'CancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "UnCancel", "UnCancel", "btn-info", "btn-info", "fa-undo", "fa-undo", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'UnCancelButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ChauffeurDriverBO.canView($data, 'UnCancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.CancelledReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'CancelledReason')")
                          End With
                          With .AddColumn("")
                            .Style.Width = 50
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Success, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , ,
                                                           "ChauffeurDriverBO.saveItem($data, {})", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'SaveButton')")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("SnT", "fa-spoon", "TravelVM.fetchSnT()", "S&T")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Div
                        .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                      End With
                      With .Helpers.DivC("pull-left")
                        With .Helpers.Bootstrap.Button(, "Bulk Add", Singular.Web.BootstrapEnums.Style.Primary, , , , "fa-plus", ,
                                                       Singular.Web.PostBackType.Ajax, "TravelVM.bulkEditSnT()", )
                        End With
                      End With
                      With .Helpers.DivC("pull-right")
                        With .Helpers.Bootstrap.Button(, "Reset Selected", Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-refresh", , , "TravelVM.resetSnTs($data)", )
                        End With
                      End With
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Travellers.TravellerSnT)(Function(d As OBLib.Travel.TravelRequisition) d.TravellerSnTList,
                                                                                                False, False, True, True, True, True, True, "TravellerSnTList")
                        .AddClass("hover list")


                        '.AddBinding(Singular.Web.KnockoutBindingString.click, "TravelVM.WhatClicked($data)")
                        With .FirstRow
                          .ExandVisibleBinding = "false"
                          'If Singular.Security.HasAccess("Ad Hoc Bookings.Remove Booking") Then
                          '  With .AddColumn("")
                          '    With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                          '       "fa-pencil", , Singular.Web.PostBackType.None, "TravelVM.editHRSnT($data)")
                          '      .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As OBLib.Travel.Travellers.TravellerSnT) ViewModel.IsValid)
                          '      .Button.AddClass("btn-block")
                          '    End With
                          '  End With
                          'End If
                          With .AddColumn("")
                            .Style.Width = "40px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                            End With
                          End With
                          With .AddColumn("")
                            .Style.Width = "120px"
                            With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", ,
                                                           Singular.Web.PostBackType.None, "TravellerSnTBO.saveItem($data)", )
                            End With
                          End With
                          With .AddColumn("")
                            .Style.Width = "15px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.SnTFetched, "View Detail", "View Detail", , , "fa-eye", "fa-eye-slash", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.html, "TravellerSnTBO.expandButtonHTML($data)")
                            End With
                          End With
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.HumanResource).AddClass("bold")
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.CityCode).AddClass("bold")
                          With .AddColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.SnTStartDate)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .Style.Width = "150px"
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.SnTEndDate)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            .Style.Width = "150px"
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.DefaultGroupSnTID)
                            .Style.Width = "150px"
                          End With
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.TotalBreakfastAmount).AddClass("bold")
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.TotalLunchAmount).AddClass("bold")
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.TotalDinnerAmount).AddClass("bold")
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.TotalIncidentalAmount).AddClass("bold")
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.TravellerSnT) c.TotalSnTAmount).AddClass("bold")
                        End With
                        With .AddChildTable(Of OBLib.Travel.SnT.SnTDetail)("$data.SnTDetailList()", False, False, False, True, True, True, True, , "SnTDetailList")

                          With .FirstRow
                            'With .AddColumn(Function(d As OBLib.Travel.SnT.AdHocSnTDetail) d.CalculatedPolicy)
                            'End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.SnTDay)
                              .Style.Width = "80px"
                            End With
                            With .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.GroupSnTID)
                              .Style.Width = "200px"
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'GroupSnTID')")
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.GroupSnTIDCalculated)
                              .Style.Width = "200px"
                              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'GroupSnTID')")
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.CurrencyID)
                              .Style.Width = "150px"
                              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'CurrencyID')")
                            End With
                            With .AddColumn("Breakfast")
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                              With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasBreakfast, , , "btn-success", "btn-default", , "", )
                                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "$data.BreakfastAmount()")
                                .Button.Style.TextAlign = Singular.Web.TextAlign.center
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'HasBreakfast')")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SnTDetailBO.breakfastButtonCss($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.BreakfastPopover()")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                            With .AddColumn("Lunch")
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                              With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasLunch, , , "btn-success", "btn-default", , "", )
                                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "$data.LunchAmount()")
                                .Button.Style.TextAlign = Singular.Web.TextAlign.center
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'HasLunch')")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SnTDetailBO.lunchButtonCss($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.LunchPopover()")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                            With .AddColumn("Dinner")
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                              With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasDinner, , , "btn-success", "btn-default", , "", )
                                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "$data.DinnerAmount()")
                                .Button.Style.TextAlign = Singular.Web.TextAlign.center
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'HasDinner')")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SnTDetailBO.dinnerButtonCss($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.DinnerPopover()")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                            With .AddColumn("Incidental")
                              .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                              With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasIncidental, , , "btn-success", "btn-default", , "", )
                                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "$data.IncidentalAmount()")
                                .Button.Style.TextAlign = Singular.Web.TextAlign.center
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'HasIncidental')")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "SnTDetailBO.incidentalButtonCss($data)")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.IncidentalPopover()")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.TotalAmount)
                              .Style.Width = "120px"
                            End With
                            With .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.ChangedReason)
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'ChangedReason')")
                            End With
                            With .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.Comments)
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "SnTDetailBO.canEdit($data, 'Comments')")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Travel Advances", "fa-credit-card", "TravelVM.fetchAdvances()")
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('TravelAdvancesTab', ViewModel.TravelRequisition())")
              With .TabPane
                .AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelRequisitionBO.canView('TravelAdvancesTab', ViewModel.TravelRequisition())")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Div
                        .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                      End With
                      With .Helpers.DivC("pull-right")
                        If Singular.Security.HasAccess("Ad Hoc Bookings.Remove Booking") Then
                          With .Helpers.Bootstrap.Button(, "Remove Selected", Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TravelVM.removeSelectedTravelAdvances()", )
                          End With
                        End If
                      End With
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail)(Function(c) c.ProductionTravelAdvanceDetailList,
                                                                                                                     True, True, False, True, True, True, True, "ProductionTravelAdvanceDetailList")
                        .AddClass("hover list")


                        With .FirstRow
                          With .AddColumn("")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, "TravelVM.pushAdvances($data)")
                            End With
                          End With
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.HumanResourceID)
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.CurrencyID)
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.Denom)
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.TravelAdvanceTypeID)
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.PayTypeID)
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.Amount)
                          .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.Comments, 400)
                          'With .AddColumn("")
                          '  With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , Singular.Web.PostBackType.None, "TravelVM.removeIndividualAdvance($data)")
                          '    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "TravellerBO.CanEdit($data, 'removeItemVisible')")
                          '  End With
                          'End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Comments", "fa-comments-o", "TravelVM.fetchComments()")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                    With .Helpers.DivC("pull-right")
                      If Singular.Security.HasAccess("Ad Hoc Bookings.Remove Booking") Then
                        With .Helpers.Bootstrap.Button(, "Remove Selected", Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TravelVM.removeSelectedTravelComments()", )
                        End With
                      End If
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Comments.TravelReqComment)(Function(c) c.TravelReqCommentList,
                                                                                                  True, True, False, True, True, True, True, "TravelReqCommentList")
                        .AddClass("hover list")


                        With .FirstRow
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", "btn-success", , , , )
                              '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, "TravelVM.pushComments($data)")
                            End With
                          End With
                          .AddColumn(Function(c As OBLib.Travel.Comments.TravelReqComment) c.CommentTypeID, 200)
                          .AddColumn(Function(c As OBLib.Travel.Comments.TravelReqComment) c.Comment)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            With .AddTab("Reports", "fa-files-o")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Div
                      .AddBinding(Singular.Web.KnockoutBindingString.html, "TravelVM.pageHeaderDetails()")
                    End With
                    With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
                      With .Helpers.Bootstrap.Button(, "Travel Req.", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-print", , Singular.Web.PostBackType.None, "TravelVM.printRequisition($data)")
                        .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "TravelRequisitionBO.travelReqIconCss($data)")
                        ' .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c) ViewModel.TravelRequisition IsNot Nothing)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        'End With
      End With

      'Crew Member Detail
      With h.Bootstrap.Dialog("TravellerSummaryModal", "Traveller Summary", , "modal-md", Singular.Web.BootstrapEnums.Style.Primary, , "fa-user", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of ROTravellerSummary)(Function(c) ViewModel.CurrentROTravellerSummary)

              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                'Crew Member Flights
                With .Helpers.Bootstrap.FlatBlock("Flights", False, False)
                  '.FlatBlockTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROTravellerSummary) c.ROTravellerSummaryFlightList.Count > 0)
                  With .ContentTag
                    With .Helpers.Bootstrap.TableFor(Of ROTravellerSummaryFlight)(Function(c As ROTravellerSummary) c.ROTravellerSummaryFlightList, False, False, False, True, True, True, True)
                      .AddClass("hover list")
                      .FirstRow.AddReadOnlyColumn(Function(c As ROTravellerSummaryFlight) c.FlightDescription)
                    End With
                  End With
                End With
              End With

              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                'Crew Member Rental Cars
                With .Helpers.Bootstrap.FlatBlock("Rental Cars", False, False)
                  '.FlatBlockTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROTravellerSummary) c.ROTravellerSummaryRentalCarList .Count > 0)
                  With .ContentTag
                    With .Helpers.Bootstrap.TableFor(Of ROTravellerSummaryRentalCar)(Function(c As ROTravellerSummary) c.ROTravellerSummaryRentalCarList, False, False, False, True, True, True, True)
                      .AddClass("hover list")
                      .FirstRow.AddReadOnlyColumn(Function(c As ROTravellerSummaryRentalCar) c.RentalCarDescription)
                    End With
                  End With
                End With
              End With

              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                'Crew Member Accommodation
                With .Helpers.Bootstrap.FlatBlock("Accommodation", False, False)
                  '.FlatBlockTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROTravellerSummary) c.ROTravellerSummaryAccommodationList.Count > 0)
                  With .ContentTag
                    With .Helpers.Bootstrap.TableFor(Of ROTravellerSummaryAccommodation)(Function(c As ROTravellerSummary) c.ROTravellerSummaryAccommodationList, False, False, False, True, True, True, True)
                      .AddClass("hover list")
                      .FirstRow.AddReadOnlyColumn(Function(c As ROTravellerSummaryAccommodation) c.AccommodationDescription)
                    End With
                  End With
                End With
              End With

              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                'Crew Member Chauffeur
                With .Helpers.Bootstrap.FlatBlock("Chauffeur Drives", False, False)
                  '.FlatBlockTag.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROTravellerSummary) c.ROTravellerSummaryChauffeurList.Count > 0)
                  With .ContentTag
                    With .Helpers.Bootstrap.TableFor(Of ROTravellerSummaryChauffeur)(Function(c As ROTravellerSummary) c.ROTravellerSummaryChauffeurList, False, False, False, True, True, True, True)
                      .AddClass("hover list")
                      .FirstRow.AddReadOnlyColumn(Function(c As ROTravellerSummaryChauffeur) c.ChauffeurDescription)
                    End With
                  End With
                End With
              End With

            End With
          End With
        End With
      End With


      h.Control(New NewOBWeb.Controls.FindHumanResourceTravelReqModal(Of NewOBWeb.TravelVM)("FindROHumanResourceModal"))
      h.Control(New NewOBWeb.Controls.AdHocBookingModal(Of NewOBWeb.TravelVM)("TravelVM.saveAdHocBooking()"))
      h.Control(New NewOBWeb.Controls.FindTravelRequisition(Of NewOBWeb.TravelVM)("FindAdHocBooking", "TravelVM"))
      h.Control(New NewOBWeb.Controls.FlightPassengerBookingModal(Of NewOBWeb.TravelVM)("TravelVM"))
      h.Control(New NewOBWeb.Controls.RentalCarPassengerBookingModal(Of NewOBWeb.TravelVM)())
      h.Control(New NewOBWeb.Controls.AccommodationGuestBookingModal(Of NewOBWeb.TravelVM)("TravelVM"))
      h.Control(New NewOBWeb.Controls.ChauffeurDrivePassengerBookingModal(Of NewOBWeb.TravelVM)("TravelVM"))
      h.Control(New NewOBWeb.Controls.SnTTemplateModal(Of NewOBWeb.TravelVM)())
      h.Control(New NewOBWeb.Controls.BulkRentalCarModal(Of NewOBWeb.TravelVM)("TravelVM"))

    End Using%>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Travel.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/Pages/TravelVM.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">
    function CheckCancelledReason(Value, Rule, RuleArgs) {
      if (RuleArgs.Object.CancelledReason() == "" && RuleArgs.Object.IsCancelled() == true) {
        RuleArgs.AddError("Cancel Reason is required");
        return;
      }
    };
  </script>
</asp:Content>
