﻿Imports Singular.DataAnnotations
Imports System.ComponentModel
Imports OBLib.Productions.ReadOnly
Imports Singular.Misc
Imports Singular
Imports OBLib.Productions
Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.AdHoc.ReadOnly
Imports OBLib.AdHoc.ReadOnly
Imports OBLib.Travel.CrewMembers
Imports OBLib.Travel
Imports OBLib.Travel.ReadOnly

Public Class Travel
  Inherits OBPageBase(Of TravelVM)

End Class

Public Class TravelVM
  Inherits OBViewModelStateless(Of TravelVM)
  'Implements ControlInterfaces(Of TravelVM).IFindHumanResourceTravelReq

  '#Region "Enumerated Types"

  '  Enum PolicyDetails
  '    South_African_Booking = 1
  '    All_other_countries_International = 5
  '    South_Africa_International_Booking = 11
  '    Other_Countries_Domestic = 12
  '    Default_Group_SnT_Policy = 13
  '    Default_Group_SnT_Policy2 = 17
  '    Currency_Rand = 1
  '    Currency_USDollar = 16
  '  End Enum

  '#End Region

#Region " Properties "

  Public Property TravelRequisition As OBLib.Travel.TravelRequisition
  Public Property AdHocBooking As OBLib.AdHoc.AdHocBooking
  Public Property UserSystemList As OBLib.Security.UserSystemList
  Public Property TravellersToAddList As OBLib.Travel.Travellers.TravelRequisitionTravellerList = New OBLib.Travel.Travellers.TravelRequisitionTravellerList

  Public Property CurrentFlightID As Integer? = Nothing
  Public Property CurrentFlight As OBLib.Travel.Flights.Flight = Nothing
  Public Property CurrentRentalCarID As Integer? = Nothing
  Public Property CurrentRentalCarGuid As String = Nothing
  Public Property CurrentRentalCar As OBLib.Travel.RentalCars.RentalCar = Nothing
  Public Property CurrentAccommodationID As Integer? = Nothing
  Public Property CurrentAccommodation As OBLib.Travel.Accommodation.Accommodation = Nothing
  Public Property CurrentAdHocTravelAdvanceDetailGuid As String = ""
  Public Property CurrentChauffeurID As Integer? = Nothing
  Public Property CurrentChauffeur As OBLib.Travel.Chauffeurs.ChauffeurDriver = Nothing
  Public Property CurrentChauffeurGuid As String = Nothing
  Public Property CurrentROTravellerSummary As OBLib.Travel.ReadOnly.ROTravellerSummary
  Public Property CurrentBulkRentalCar As OBLib.Travel.BulkRentalCars.BulkRentalCar

#Region " Traveller "

  Public Property CurrentTravellerGuid As String = ""
  Public Property CurrentTraveller As OBLib.Travel.Travellers.TravelRequisitionTraveller = Nothing
  Public Property ROCurrentTraveller As OBLib.Travel.Travellers.ReadOnly.ROTravelRequisitionTraveller = Nothing
  Public Property FetchDaysTraveller As OBLib.Travel.Travellers.TravelRequisitionTraveller = Nothing
  'Public Property mStartDateTime As DateTime? = Nothing
  'Public Property mEndDateTime As DateTime? = Nothing
  Public Property Travellers As OBLib.Travel.Travellers.TravelRequisitionTravellerList = OBLib.Travel.Travellers.TravelRequisitionTravellerList.NewTravelRequisitionTravellerList

#End Region

#Region " S&T "

  Public Property SnTTemplate As OBLib.Travel.SnT.SnTTemplate

#End Region

#Region "Paged Lists"

  <InitialDataOnly>
  Public Property ROTravelRequisitionListPaged As ROTravelRequisitionListPaged
  Public Property ROTravelRequisitionListPagedCriteria As ROTravelRequisitionListPaged.Criteria
  Public Property ROTravelRequisitionListPagedPagingManager As Singular.Web.Data.PagedDataManager(Of TravelVM)

  <InitialDataOnly>
  Public Property ROHumanResourceTravelReqList As ROHumanResourceTravelReqList
  <InitialDataOnly>
  Public Property ROHumanResourceTravelReqListCriteria As ROHumanResourceTravelReqList.Criteria
  <InitialDataOnly>
  Public Property ROHumanResourceTravelReqListManager As Singular.Web.Data.PagedDataManager(Of TravelVM)

  <InitialDataOnly>
  Public Property ROTravellerPagedList As OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList
  <InitialDataOnly>
  Public Property ROTravellerPagedListCriteria As OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList.Criteria
  <InitialDataOnly>
  Public Property ROTravellerPagedListManager As Singular.Web.Data.PagedDataManager(Of TravelVM)

  <InitialDataOnly>
  Public Property ROSynergyEventSearchList As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList
  <InitialDataOnly>
  Public Property ROSynergyEventSearchListCriteria As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria
  <InitialDataOnly>
  Public Property ROSynergyEventSearchListManager As Singular.Web.Data.PagedDataManager(Of TravelVM)

#End Region

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

    'UserSystems and Areas
    For Each us As OBLib.Security.UserSystem In UserSystemList
      If CompareSafe(us.SystemID, OBLib.Security.Settings.CurrentUser.SystemID) Then
        us.IsSelected = True
        For Each usa As OBLib.Security.UserSystemArea In us.UserSystemAreaList
          If CompareSafe(usa.ProductionAreaID, OBLib.Security.Settings.CurrentUser.ProductionAreaID) Then
            usa.IsSelected = True
          End If
        Next
      End If
    Next

    ROTravelRequisitionListPaged = New ROTravelRequisitionListPaged
    ROTravelRequisitionListPagedCriteria = New ROTravelRequisitionListPaged.Criteria
    ROTravelRequisitionListPagedPagingManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) Me.ROTravelRequisitionListPaged,
                                                                                                    Function(d) Me.ROTravelRequisitionListPagedCriteria,
                                                                                                    "StartDateTime", 25, False)

    ROTravelRequisitionListPagedCriteria.PageNo = 1
    ROTravelRequisitionListPagedCriteria.PageSize = 25
    ROTravelRequisitionListPagedCriteria.SortAsc = False
    ROTravelRequisitionListPagedCriteria.SortColumn = "StartDateTime"
    ROTravelRequisitionListPagedCriteria.StartDate = Now.AddDays(-14)
    ROTravelRequisitionListPagedCriteria.EndDate = Now.AddDays(14)
    ROTravelRequisitionListPagedCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROTravelRequisitionListPagedCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    'ROTravelRequisitionListPagedCriteria.RefNo = 7129

    'ROHumanResourceList = New ROHumanResourcePagedList
    'ROHumanResourceListCriteria = New ROHumanResourcePagedList.Criteria
    'ROHumanResourceListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROHumanResourceList,
    '                                                                                 Function(d) d.ROHumanResourceListCriteria,
    '                                                                                 "FirstName", 15)
    ROHumanResourceTravelReqList = New ROHumanResourceTravelReqList
    ROHumanResourceTravelReqListCriteria = New ROHumanResourceTravelReqList.Criteria
    ROHumanResourceTravelReqListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROHumanResourceTravelReqList,
                                                                                              Function(d) d.ROHumanResourceTravelReqListCriteria,
                                                                                              "FirstName", 25)

    ROTravellerPagedList = New OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList
    ROTravellerPagedListCriteria = New OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList.Criteria
    ROTravellerPagedListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerPagedList,
                                                                                      Function(d) d.ROTravellerPagedListCriteria,
                                                                                      "FirstName", 25)

    ROSynergyEventSearchList = New OBLib.Synergy.ReadOnly.ROSynergyEventSearchList
    ROSynergyEventSearchListCriteria = New OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria
    ROSynergyEventSearchListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROSynergyEventSearchList,
                                                                                          Function(d) d.ROSynergyEventSearchListCriteria,
                                                                                          "StartDateTime", 25)
    ROSynergyEventSearchListManager.SingleSelect = True

    'DropDownTravellers = New OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList
    'DropDownTravellersCriteria = New OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList.Criteria
    'DropDownTravellersManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerPagedList,
    '                                                                                Function(d) d.ROTravellerPagedListCriteria,
    '                                                                                "FirstName", 25)

    'ROTravellerFlightList = New OBLib.Travel.Flights.ReadOnly.ROTravellerFlightList
    'ROTravellerFlightListCriteria = New OBLib.Travel.Flights.ReadOnly.ROTravellerFlightList.Criteria
    'ROTravellerFlightListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerFlightList,
    '                                                                                       Function(d) d.ROTravellerFlightListCriteria,
    '                                                                                       "DepartureDateTime", 15)

    'ROTravellerRentalCarList = New OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarList
    'ROTravellerRentalCarListCriteria = New OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarList.Criteria
    'ROTravellerRentalCarListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerRentalCarList,
    '                                                                                           Function(d) d.ROTravellerRentalCarListCriteria,
    '                                                                                           "RentalCarID", 15)

    'ROTravellerAccommodationList = New OBLib.Travel.Accommodation.ReadOnly.ROTravellerAccommodationList
    'ROTravellerAccommodationListCriteria = New OBLib.Travel.Accommodation.ReadOnly.ROTravellerAccommodationList.Criteria
    'ROTravellerAccommodationListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerAccommodationList,
    '                                                                                               Function(d) d.ROTravellerAccommodationListCriteria,
    '                                                                                               "AccommodationID", 15)

    'ROTravellerChauffeurList = New OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurList
    'ROTravellerChauffeurListCriteria = New OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurList.Criteria
    'ROTravellerChauffeurListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerChauffeurList,
    '                                                                                           Function(d) d.ROTravellerChauffeurListCriteria,
    '                                                                                           "HumanResourceID", 15)

    'ROTravellerProductionTravelAdvanceList = New OBLib.Travel.TravelAdvances.ReadOnly.ROTravellerProductionAdvanceList
    'ROTravellerProductionTravelAdvanceListCriteria = New OBLib.Travel.TravelAdvances.ReadOnly.ROTravellerProductionAdvanceList.Criteria
    'ROTravellerProductionTravelAdvanceListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerProductionTravelAdvanceList,
    '                                                                                                   Function(d) d.ROTravellerProductionTravelAdvanceListCriteria,
    '                                                                                                   "HumanResourceID", 15)

    'ROTravellerTravelReqCommentList = New OBLib.Travel.Comments.ReadOnly.ROTravellerTravelReqCommentList
    'ROTravellerTravelReqCommentListCriteria = New OBLib.Travel.Comments.ReadOnly.ROTravellerTravelReqCommentList.Criteria
    'ROTravellerTravelReqCommentListManager = New Singular.Web.Data.PagedDataManager(Of TravelVM)(Function(d) d.ROTravellerTravelReqCommentList,
    '                                                                                                  Function(d) d.ROTravellerTravelReqCommentListCriteria,
    '                                                                                                  "TravelRequisitionID", 15)

    'SelectedHRsToRemove = New List(Of Integer)
    'SelectedFlightsToRemove = New List(Of Integer)
    'SelectedRentalCarsToRemove = New List(Of Integer)
    'SelectedAccommodationToRemove = New List(Of Integer)
    'SelectedChauffeurDrivesToRemove = New List(Of Integer)
    'SelectedSnTsToReset = New List(Of Integer)
    'SelectedSnTHRsToReset = New List(Of Integer)
    'SelectedAdvancesToRemove = New List(Of Integer)
    'SelectedCommentsToRemove = New List(Of Integer)
    'TravellersToAddList = New List(Of OBLib.Travel.Travellers.TravelRequisitionTraveller)

    'mROTravelDirectionList = ROTravelDirectionList.GetROTravelDirectionList()
    'Dim mROSystemAreaAdHocBookingTypeList As ROSystemAreaAdHocBookingTypeList
    'mROSystemAreaAdHocBookingTypeList = ROSystemAreaAdHocBookingTypeList.GetROSystemAreaAdHocBookingTypeList(OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID)

    ClientDataProvider.AddDataSource("ROGroupSnTList", OBLib.CommonData.Lists.ROGroupSnTList, False)
    ClientDataProvider.AddDataSource("ROCrewTypeList", OBLib.CommonData.Lists.ROCrewTypeList, False)
    'ClientDataProvider.AddDataSource("ROAirportList", OBLib.CommonData.Lists.ROAirportList, False)
    ClientDataProvider.AddDataSource("ROFlightClassList", OBLib.CommonData.Lists.ROFlightClassList, False)
    'ClientDataProvider.AddDataSource("RORentalCarAgentBranchList", OBLib.CommonData.Lists.RORentalCarAgentBranchList, False)
    'ClientDataProvider.AddDataSource("ROAccommodationProviderList", OBLib.CommonData.Lists.ROAccommodationProviderList, False)
    ClientDataProvider.AddDataSource("ROCityList", OBLib.CommonData.Lists.ROCityList, False)
    ClientDataProvider.AddDataSource("ROCarTypeList", OBLib.CommonData.Lists.ROCarTypeList, False)
    ClientDataProvider.AddDataSource("ROCurrencyList", OBLib.CommonData.Lists.ROCurrencyList, False)
    ClientDataProvider.AddDataSource("ROHumanResourceList", OBLib.CommonData.Lists.ROHumanResourceList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    ClientDataProvider.AddDataSource("ROCountryList", OBLib.CommonData.Lists.ROCountryList, False)
    'ClientDataProvider.AddDataSource("ROSystemAreaAdHocBookingTypeList", mROSystemAreaAdHocBookingTypeList, False)
    ClientDataProvider.AddDataSource("ROTravelDirectionList", OBLib.CommonData.Lists.ROTravelDirectionList, False)
    ClientDataProvider.AddDataSource("ROFlightTypeList", OBLib.CommonData.Lists.ROFlightTypeList, False)
    ClientDataProvider.AddDataSource("ROCostCentreList", OBLib.CommonData.Lists.ROCostCentreList, False)
    ClientDataProvider.AddDataSource("ROTravelAdvanceTypeList", OBLib.CommonData.Lists.ROTravelAdvanceTypeList, False)
    ClientDataProvider.AddDataSource("ROPayTypeList", OBLib.CommonData.Lists.ROPayTypeList, False)
    ClientDataProvider.AddDataSource("ROCommentTypeList", OBLib.CommonData.Lists.ROCommentTypeList, False)
    ClientDataProvider.AddDataSource("RORentalCarAgentList", OBLib.CommonData.Lists.RORentalCarAgentList, False)
    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
    'ClientDataProvider.AddDataSource("ROTravellerList", DropDownTravellers, False)
    ClientDataProvider.AddDataSource("ROAdHocBookingTypeList", OBLib.CommonData.Lists.ROAdHocBookingTypeList, False)
    MessageFadeTime = 5000

    Dim TRIDString As String = Page.Request.QueryString("TRID")
    If Not String.IsNullOrEmpty((TRIDString)) Then
      Dim TRID As Integer? = Singular.Misc.ZeroNothing(CInt(TRIDString))
      If TRID IsNot Nothing Then
        Dim trl As OBLib.Travel.TravelRequisitionList = OBLib.Travel.TravelRequisitionList.GetTravelRequisitionList(TRID)
        If trl.Count = 1 Then
          TravelRequisition = trl(0)
        End If
      End If
    End If

  End Sub

#End Region

#Region " Methods "

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Function SaveNewTravellers(NewTravellers As OBLib.Travel.Travellers.TravelRequisitionTravellerList) As Singular.Web.Result
    Dim sh As Singular.SaveHelper = NewTravellers.TrySave()
    If sh.Success Then
      Return New Singular.Web.Result(True) With {.Data = NewTravellers}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Function SaveTravelRequisition(TravelRequisition As OBLib.Travel.TravelRequisition) As Singular.Web.Result
    Dim sh As Singular.SaveHelper = TravelRequisition.TrySave(GetType(OBLib.Travel.TravelRequisitionList))
    If sh.Success Then
      Return New Singular.Web.Result(True) With {.Data = sh.SavedObject}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

#End Region

End Class

