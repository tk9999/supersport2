﻿Public Class NewSite
  Inherits Singular.Web.MasterBase

  Protected Overrides Sub OnLoad(e As System.EventArgs)
    MyBase.OnLoad(e)

    'languageSelector.Visible = EELib.CommonData.ApplicationSettings.MultiLanguage
    'languageSelector.Style.Margin("10px")

    If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
      'LoginStatus.LoginText = LocalText("Log In")
    End If
    'loginStatus.LogInText = LocalText("Log In")
    'loginStatus.ChangePasswordText = LocalText("Change Password")
    'loginStatus.LogoutText = LocalText("Logout")
    'loginStatus.LoggedInAsText = ""
    SiteMapMain.OnItemRender = AddressOf OnItemRender

    'userImageSmall.Src = "http://localhost/NewOBWebDevelopment/Images/Profile/" & OBLib.Security.Settings.CurrentUserID.ToString & "_2.jpg"
    'userName.InnerText = OBLib.Security.Settings.CurrentUser.FirstName & " " & OBLib.Security.Settings.CurrentUser.Surname
    'userImageLarge.Src = "../Images/Profile/" & OBLib.Security.Settings.CurrentUserID.ToString & "_2.jpg"
    'userImageLarge.Src = "../Images/Profile/" & OBLib.Security.Settings.CurrentUserID.ToString & "_2.jpg"

  End Sub

  Private Sub OnItemRender(ria As Singular.Web.CustomControls.SiteMapDataSource.RenderItemArgs)

    Dim Ident As OBLib.Security.User = OBLib.Security.Settings.CurrentUser

    If Ident IsNot Nothing Then
      Select Case OBLib.Security.Settings.CurrentUser.UserTypeID
        Case OBLib.CommonData.Enums.UserType.Web
          If ria.Item.Title = "Timesheet" _
             Or ria.Item.Description = "Your Account" _
             Or ria.Item.Title = "Logout" _
             Or ria.Item.Title = "Profile" _
             Or ria.Item.Title = "Login" _
             Or ria.Item.Title = "Change Password" _
             Or ria.Item.Title = "User Profile" Then
            ria.ShowItem = True
          Else
            ria.ShowItem = False
          End If
        Case Else
          ria.ShowItem = True
      End Select
      If ria.Item.Description = "Your Account" Then
        ria.Item.Title = OBLib.Security.Settings.CurrentUser.LoginName
      End If
    Else
      ''if there is no user, only show login
      Select Case ria.Item.Title
        Case "Login"
          ria.ShowItem = True
        Case Else
          ria.ShowItem = False
      End Select
    End If

    'Check users timesheet type
    Select Case ria.Item.Title
      Case "Timesheet"
        If Ident IsNot Nothing AndAlso Singular.Security.HasAccess("General Timesheets.Access") Then
          ria.Item.Url = VirtualPathUtility.ToAbsolute("~/Timesheets/GeneralTimesheets.aspx")
        End If
      Case "Ad Hoc Travel"
        If Ident IsNot Nothing Then
          If Not (Singular.Misc.CompareSafe(Ident.UserID, 152) OrElse Singular.Misc.CompareSafe(Ident.UserID, 1)) Then
            ria.ShowItem = False
          End If
        End If
      Case "Quoting"
        If Ident IsNot Nothing Then
          Dim AllowedSuperUserIDs = {1, 152}
          If AllowedSuperUserIDs.Contains(Ident.UserID) Then
            ria.ShowItem = True
          Else
            ria.ShowItem = False
          End If
        Else
          ria.ShowItem = False
        End If
    End Select

  End Sub

End Class