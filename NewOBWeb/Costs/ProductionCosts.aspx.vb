﻿Imports OBLib.VehiclesAndEquipment
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Costs
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports OBLib.Maintenance.Equipment
Imports OBLib.Productions.ReadOnly

Public Class ProductionCosts
  Inherits OBPageBase(Of ProductionCostsVM)

End Class


Public Class ProductionCostsVM
  Inherits OBViewModel(Of ProductionCostsVM)

#Region " Properties "

  Private mROProductionList As ROProductionList
  'Private mProductionCost As ProductionCost
  Public Property ProductionCostList As ProductionCostList
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of FindOBVM)
  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria

  Public ReadOnly Property ROProductionList As ROProductionList
    Get
      Return mROProductionList
    End Get
  End Property

  Public Property CurrentProductionCost As ProductionCost
  'ReadOnly
  '  Get
  '    Return mProductionCost
  '  End Get
  'End Property

#End Region

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    mROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
    CurrentProductionCost = Nothing
    ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
    ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of FindOBVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "TxStartDateTime", 20)
    ROProductionListCriteria.TxDateFrom = Now.Date.AddDays(-7)
    ROProductionListCriteria.TxDateTo = Now.Date.AddMonths(1)
    ROProductionListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROProductionListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    ROProductionListCriteria.PageNo = 1
    ROProductionListCriteria.PageSize = 20
    ROProductionListCriteria.SortAsc = True
    ROProductionListCriteria.SortColumn = "TxStartDateTime"
    If Singular.Debug.InDebugMode Then
      ROProductionListCriteria.TxDateFrom = Now.Date.AddYears(-7)
      ROProductionListCriteria.TxDateTo = Now.Date.AddMonths(1)
      ROProductionListCriteria.ProductionRefNo = "7100"
    End If

    MessageFadeTime = 2000
  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "Fetch"

        ProductionCostList = OBLib.Costs.ProductionCostList.GetProductionCostList(CommandArgs.ClientArgs.ProductionID)

        If ProductionCostList IsNot Nothing OrElse ProductionCostList.Count = 1 Then
          CurrentProductionCost = ProductionCostList(0)
        End If


      Case "FindProduction"


        mROProductionList = OBLib.Productions.[ReadOnly].ROProductionList.GetROProductionList(ROProductionListCriteria.ProductionTypeID, ROProductionListCriteria.EventTypeID, ROProductionListCriteria.ProductionVenueID,
                                                                                              Nothing, Nothing, Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID, _
                                                                                              ROProductionListCriteria.TxDateFrom, ROProductionListCriteria.TxDateTo, ROProductionListCriteria.EventManagerID,
                                                                                              ROProductionListCriteria.ProductionRefNo, Nothing, OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                                              ROProductionListCriteria.PageNo, ROProductionListCriteria.PageSize,
                                                                                              ROProductionListCriteria.SortAsc, ROProductionListCriteria.SortColumn)
      Case "Save"

        If Not CurrentProductionCost.IsValid Then
          AddMessage(Singular.Web.MessageType.Validation, "Valid", CurrentProductionCost.BrokenRulesCollection(0).Description)
          Exit Select
        End If

        Dim mProductionCostList As OBLib.Costs.ProductionCostList = OBLib.Costs.ProductionCostList.NewProductionCostList
        mProductionCostList.Add(CurrentProductionCost)

        With mProductionCostList.TrySave()
          If .Success Then
            mProductionCostList = .SavedObject
            CurrentProductionCost = mProductionCostList(0)
            AddMessage(Singular.Web.MessageType.Success, "Save", "Production Cost Saved Successfully")
          Else
            AddMessage(Singular.Web.MessageType.Error, "Save", "Production Cost Save Error. " & .ErrorText)
          End If
        End With

      Case "UpdatePrices"
        CurrentProductionCost.UpdateAllCosts()

    End Select
  End Sub

End Class

