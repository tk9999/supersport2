﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ProductionCosts.aspx.vb" Inherits="NewOBWeb.ProductionCosts" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>

<%@ Import Namespace="OBLib.Costs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <style type="text/css">
    #EventTypes {
      display: none;
    }

    td.LButtons, th.LButtons {
      width: 24px !important;
    }

    div.ComboDropDown {
      z-index: 35000;
    }

    .ui-dialog-title {
      color: black;
    }

    body {
      background-color: #F0F0F0;
    }

    table tr td, table tr td span {
      color: black;
      font-weight: bold;
    }

    .ROVehicle {
      overflow-y: scroll !important;
      max-height: 500px !important;
    }
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.FlatBlock("Production Costs", False)
          With .ContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Button(, "Find Production", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindProductionCost()")
        
                End With
                With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
               
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Toolbar
                  .Helpers.MessageHolder()
                End With
              End With
            End With
          End With
        End With
      End With
      
      '----Content------------------------------------------------------------------------------------------------------------------
      With h.With(Of ProductionCost)(Function(d) ViewModel.CurrentProductionCost)
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.FlatBlock("Travel Costs", False)
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 2)
                  With .Helpers.DivC("block-flat dark-box ")
                    .Helpers.HTML.Heading4("Total Cost")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        .AddClass("counters pull-right")
                        With .Helpers.HTMLTag("h1")
                          .AddBinding(Singular.Web.KnockoutBindingString.html, "ProductionCostBO.GetTotalCost()")
                        End With
                      End With
                      'With .Helpers.Bootstrap.Column(8, 8, 8, 8)
                      '  '<div id="ticket-chart" style="height: 140px; padding: 0px; position: relative;"><canvas class="flot-base" width="318" height="140" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 318px; height: 140px;"></canvas><canvas class="flot-overlay" width="318" height="140" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 318px; height: 140px;"></canvas></div>
                      'End With
                    End With
                  End With
                  '        <div class="block-flat dark-box visitors">				
                  '    <h4 class="no-margin">Visitors</h4>
                  '  <div class="row">
                  '    <div class="counters col-md-4">
                  '    <h1>477</h1>
                  '    <h1>23</h1>
                  '    </div>							
                  '    <div class="col-md-8">
                  '      <div id="ticket-chart" style="height: 140px; padding: 0px; position: relative;"><canvas class="flot-base" width="318" height="140" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 318px; height: 140px;"></canvas><canvas class="flot-overlay" width="318" height="140" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 318px; height: 140px;"></canvas></div>
                  '    </div>							
                  '  </div>
                  '  <div class="row footer">
                  '    <div class="col-md-6"><p><i class=" fa fa-square"></i> New Visitors</p></div>
                  '    <div class="col-md-6"><p><i class=" return fa fa-square"></i> Returning Visitors</p></div>
                  '  </div>
                  '</div>
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 10)
                  With .Helpers.Bootstrap.TabControl("", "nav-pills", "tablist")
                    .TabHeaderContainer.Attributes("ID") = "PageTabControl"
                    With .AddTab("Flights", "fa-bus", , , )
                      With .TabPane
                        With .Helpers.Bootstrap.TableFor(Of ProductionCostFlight)(Function(d) ViewModel.CurrentProductionCost.ProductionCostFlightList, False, False, False, , True, True, False)
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y"
                          With .FirstRow
                            .AddClass("items")
                            '.AddReadOnlyColumn(Function(d) d.ProductionRefNo)
                            .AddReadOnlyColumn(Function(d) d.TravelDirection)
                            .AddReadOnlyColumn(Function(d) d.FlightNo)
                            .AddReadOnlyColumn(Function(d) d.FlightType)
                            .AddReadOnlyColumn(Function(d) d.AirportFrom)
                            .AddReadOnlyColumn(Function(d) d.DepartureDateTime)
                            .AddReadOnlyColumn(Function(d) d.AirportTo)
                            .AddReadOnlyColumn(Function(d) d.ArrivalDateTime)
                            .AddColumn(Function(d) d.SingleTicketPrice)
                            .AddReadOnlyColumn(Function(d) d.TotalTicketCost)
                            '.AddReadOnlyColumn(Function(d) d.GenRefNum)
                            '.AddReadOnlyColumn(Function(d) d.ProductionDescription)
                          End With
                          With .AddChildTable(Of ProductionCostFlightPassenger)(Function(c) c.ProductionCostFlightPassengerList, False, False, False, True, True, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              .AddClass("items")
                              .AddReadOnlyColumn(Function(c) c.HumanResource)
                              .AddColumn(Function(c) c.TicketPrice)
                              .AddColumn(Function(c) c.ServiceFee)
                              .AddColumn(Function(c) c.CancellationFee)
                              .AddReadOnlyColumn(Function(c) c.CancelledByUser)
                              .AddReadOnlyColumn(Function(c) c.CancelledReason)
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .AddTab("Rental Cars", "fa-bus", , , )
                      With .TabPane
                        With .Helpers.Bootstrap.TableFor(Of ProductionCostCarRental)(Function(c) ViewModel.CurrentProductionCost.ProductionCostCarRentalList, False, False, False, , True, True, False)
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y"
                          With .FirstRow
                            .AddClass("items")
                            '.AddReadOnlyColumn(Function(c) c.ProductionDescription)
                            .AddReadOnlyColumn(Function(c) c.RentalCarRefNo)
                            '.AddReadOnlyColumn(Function(c) c.GenRefNum)
                            .AddReadOnlyColumn(Function(c) c.AgentBranchFrom)
                            .AddReadOnlyColumn(Function(c) c.PickupDateTime)
                            '.AddReadOnlyColumn(Function(c) c.ProductionNo)
                            .AddReadOnlyColumn(Function(c) c.AgentBranchTo)
                            .AddReadOnlyColumn(Function(c) c.DropoffDateTime)
                            '.AddReadOnlyColumn(Function(c) c.AgentBranchTo)
                            .AddReadOnlyColumn(Function(c) c.Driver)
                            .AddColumn(Function(c) c.RentalCost)
                          End With
                          With .AddChildTable(Of ProductionCostCarRentalPassenger)(Function(c) c.ProductionCostCarRentalPassengerList, False, False, False, True, True, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              .AddClass("items")
                              .AddReadOnlyColumn(Function(c) c.HumanResource)
                              .AddReadOnlyColumn(Function(c) c.DriverInd)
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .AddTab("Accommodation", "fa-bus", , , )
                      With .TabPane
                        With .Helpers.Bootstrap.TableFor(Of ProductionCostAccomodation)(Function(c) ViewModel.CurrentProductionCost.ProductionCostAccomodationList, False, False, False, , True, True, False)
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y"
                          With .FirstRow
                            .AddClass("items")
                            .AddReadOnlyColumn(Function(c) c.AccommodationProvider)
                            .AddReadOnlyColumn(Function(c) c.City)
                            '.AddReadOnlyColumn(Function(c) c.ProductionNo)
                            .AddReadOnlyColumn(Function(c) c.CheckInDate)
                            .AddReadOnlyColumn(Function(c) c.CheckOutDate)
                            .AddReadOnlyColumn(Function(c) c.Description)
                            '.AddReadOnlyColumn(Function(c) c.GenRefNum)
                            .AddReadOnlyColumn(Function(c) c.SupplierInd)
                            '.AddReadOnlyColumn(Function(c) c.ProductionDescription)
                            .AddColumn(Function(c) c.RoomCost)
                            .AddReadOnlyColumn(Function(c) c.TotalCost)
                          End With
                          With .AddChildTable(Of ProductionCostAccomodationGuest)(Function(c) c.ProductionCostAccomodationGuestList, False, False, False, True, True, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              .AddClass("items")
                              .AddReadOnlyColumn(Function(c) c.HumanResource)
                              .AddColumn(Function(c) c.RoomCost)
                            End With
                          End With
                          'With .AddChildTable(Of ProductionCostAccomodationSupplierGuest)(Function(c) c.ProductionCostAccomodationSupplierGuestList, False, False, False, True, True, False)     
                          '  .AddClass("no-border hover list")
                          '  .TableBodyClass = "no-border-y"
                          '  With .FirstRow
                          '    .AddClass("items")
                          '    .AddReadOnlyColumn(Function(c) c.SupplierHumanResource)
                          '    .AddColumn(Function(c) c.RoomCost)
                          '  End With
                          'End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("FindProduction", "Find Production")
        .ModalDialogDiv.AddClass("modal-md")
        With .Body
          With .Helpers.Bootstrap.Row
            .AddClass("form-horizontal")
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.ProductionRefNo)
                  .AddClass("form-control input-sm")
                  .Attributes("placeholder") = "Reference Number"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateFrom)
                  .AddClass("form-control input-sm")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateTo)
                  .AddClass("form-control input-sm")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventManagerID, "Event Manager", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                  .InputGroup.AddClass("margin-bottom-override")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionTypeID, "Production Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                  .InputGroup.AddClass("margin-bottom-override")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventTypeID, "Event Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                  .InputGroup.AddClass("margin-bottom-override")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionVenueID, "Production Venue", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
                  .InputGroup.AddClass("margin-bottom-override")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.EditorFor(Function(d) ViewModel.ROProductionListCriteria.Keyword)
                  .AddClass("form-control input-sm")
                  .Attributes("placeholder") = "Search by Keyword"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "Refresh()")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ViewModel.ROProductionListPagingManager,
                                                Function(vm) ViewModel.ROProductionList,
                                                False, False, True, False, True, True, False,
                                                , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y"
              .Pager.PagerListTag.ListTag.AddClass("pull-left")
              With .FirstRow
                .AddClass("items")
                '.AddBinding(Singular.Web.KnockoutBindingString.css, "GetProductionStatColour($data)")
                With .AddColumn("")
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  With .Helpers.Bootstrap.Anchor(, , , , Singular.Web.LinkTargetType.NotSet, "fa-pencil", )
                    .AnchorTag.AddClass("btn btn-sm btn-primary")
                    .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.click, "EditProductionCost($data)")
                  End With
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                  .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionType)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.EventType)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionVenue)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(c) c.EventManager)
                  .AddClass("col-xs-1 col-sm-2 col-md-2 col-lg-1")
                End With
              End With
              With .FooterRow
              End With
            End With
          End With
        End With
      End With
      
      'With h.Bootstrap.Row
      '  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
      '    With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindProductionCost()")
      '    End With
      '  End With
      '  'With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditProductionCost($data)")
      '  'End With
      '  With .Helpers.Bootstrap.Button("Save", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, "SaveProductionCost")
      '    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
      '  End With
      '  'With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
      '  '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
      '  'End With
                
      'End With

    End Using%>
  <script type="text/javascript">

    function Refresh() {
      ViewModel.ROProductionListPagingManager().Refresh();
    };

    function CheckDates(Value, Rule, RuleArgs) {

      if (RuleArgs.Object.IsDirty && RuleArgs.Object.IsDirty()) {

        if (RuleArgs.Object.StartDate() && RuleArgs.Object.EndDate()) {
          var StartDate = new Date(RuleArgs.Object.StartDate()).getTime();
          var EndDate = new Date(RuleArgs.Object.EndDate()).getTime();

          if (!RuleArgs.IgnoreResults) {
            if (EndDate <= StartDate) {
              RuleArgs.AddError("Start date must be less than end date");
              return;
            }
          }
        }
      }
    }

    Singular.OnPageLoad(function () {
      $('#PageTabControl a[href="#Flights"]').tab('show')
    });

    function FindProductionCost() {
      ViewModel.ROProductionListPagingManager().Refresh();
      $("#FindProduction").modal();
    }

    function EditProductionCost(ROProduction) {

      Singular.SendCommand("Fetch",
                           {
                             ProductionID: ROProduction.ProductionID()
                           },
                           function (response) {
                             $("#FindProduction").modal('hide');
                           });
    };

  </script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionCosts.js"></script>
</asp:Content>
