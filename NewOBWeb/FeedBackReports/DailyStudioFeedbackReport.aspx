﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="DailyStudioFeedbackReport.aspx.vb" Inherits="NewOBWeb.DailyStudioFeedbackReport" %>

<%@ Import Namespace="OBLib.Scheduling.Rooms" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.FeedbackReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    body {
      background-color: #F0F0F0;
      position: absolute;
    }

    .modal .popover, .modal .twipsy {
      z-index: 12000;
    }

    .a-feedback-section {
      border-style: solid;
      border-color: lightgray;
      border-width: 0px 0px 0px 0px;
      padding: 5px;
      width: 60%;
      margin: 0px 0px 0px 10px;
      display: inline-block;
    }

      .a-feedback-section:hover {
        background-color: #f6eded;
        box-shadow: inset 0 0 1px rgba(0,0,0,0.075) !important;
        -webkit-transition: border-color ease-in-out .40s,box-shadow ease-in-out .40s !important;
        transition: border-color ease-in-out .40s,box-shadow ease-in-out .40s !important;
      }

    .cursor-hover:hover {
      cursor: pointer;
    }

    .rotate {
      -ms-transform: rotate(-90deg); /* IE 9 */
      -webkit-transform: rotate(-90deg); /* Safari */
      transform: rotate(-90deg);
    }

    .no-padding {
      padding: 0px;
    }

    .no-margin {
      margin: 0px;
    }

    .section {
      border: 1px solid #F0F0F0;
      position: absolute;
      width: 100%;
      /*background: rgba(0,0,0,0.5);
      color: #fff;*/
      -webkit-transform: rotate(270deg) translateX(-100%);
      transform: rotate(270deg) translateX(-100%);
      *-webkit-transform-origin: -10px -10px;
      transform-origin: -10px -10px;
      text-align: right;
      vertical-align: text-top;
      vertical-align: top;
    }

    td.LButtons, th.LButtons {
      width: 24px !important;
    }

    tr.recipient-added td {
      background-color: #7761a7;
      background-image: none;
      color: #fff;
    }

    tr.selectable td {
      cursor: pointer;
    }

    .Flags {
      font-size: 10px !important;
      line-height: 1.101 !important;
      color: #ff392e !important;
      vertical-align: middle !important;
    }

    .GreenFlag {
      font-size: 11px;
      font-weight: 600;
      color: green !important;
    }

    .OrangeFlag {
      font-size: 11px;
      font-weight: 600;
      color: orange !important;
    }

    .RedFlag {
      font-size: 11px;
      font-weight: 600;
      color: red;
    }

    .NoFlags {
    }

    .NoDeleteGrid {
      display: none;
    }

    table.hideButton {
      visibility: hidden;
    }

    table.NoDelete tr td:last-child, table.NoDelete tr th:last-child {
      display: none;
    }

    .HtmlColumn {
      height: 26px;
      color: black;
      outline-style: solid;
      outline-width: 1px;
      background: white;
      outline-color: lightgray;
    }

    .modal.fade .modal-dialog {
      -webkit-transition: all 0.5s linear !important;
      -moz-transition: all 0.5s linear !important;
      -o-transition: all 0.5s linear !important;
      transition: all 0.5s linear !important;
    }

    .booking-time {
      padding: 5px;
      display: inline-block;
      vertical-align: top;
      font-weight: 700;
    }

    .booking-title {
      font-weight: 400;
    }

    .accordion.accordion-color .panel-collapse .panel-body {
      background-color: #fff;
      color: #696969;
      font-weight: 400;
    }

    .roomBookingIcon:hover {
      cursor: pointer;
    }

    .roomBookingIcon.isCompleted {
      color: rgb(152, 204, 70)
    }

    .roomBookingIcon.hasFeedback {
      color: rgb(120, 197, 218)
    }

    .padding-left-10 {
      padding-left: 10px;
    }

    .padding-right-10 {
      padding-right: 10px;
    }

    .isReconciled {
      color: rgb(152, 204, 70)
    }

    .notReconciled {
      color: #d2322d;
    }
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Feedback.js?v=<%= OBLib.OBMisc.VersionNo %>"> </script>
  <script type="text/javascript" src="../Scripts/Pages/FeedbackReport.js?v=<%= OBLib.OBMisc.VersionNo %>"> </script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/RoomScheduling.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionSystemAreas.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/AdHocBookings.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Audit.js?v=<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <% Using h = Helpers

      h.HTML.Heading1("Daily Studio Feedback")

      With h.If("ViewModel.CurrentReport() === null")
        With .Helpers.Bootstrap.Row
          'With .Helpers.Toolbar
          '  With .Helpers.MessageHolder()
          '    .Style.Width = "50%"
          '  End With
          'End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.FlatBlock("Setup Report")
              With .ContentTag
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(vm) vm.ReportDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(vm) vm.ReportDate, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

      With h.With(Of StudioFeedbackReport)("ViewModel.CurrentReport()")
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.FlatBlock("Report Info")
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    .AddClass("hidden-xl hidden-lg")
                    .Style.Margin("10px")
                    .Helpers.Bootstrap.LabelDisplay("Errors/Messages").Style.Width = "100%"
                    With .Helpers.DivC("error-window-column")
                      .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesBootstrap(ViewModel.CurrentReport(), $element)")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                        .Helpers.Bootstrap.LabelFor(Function(rpt) rpt.ReportDate)
                        With .Helpers.Bootstrap.FormControlFor(Function(rpt) rpt.ReportDate, BootstrapEnums.InputSize.Small)
                          .Editor.AddBinding(KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('ReportDate', $data)")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                        .Helpers.Bootstrap.LabelFor(Function(rpt) rpt.SystemID)
                        With .Helpers.Bootstrap.FormControlFor(Function(rpt) rpt.SystemID, BootstrapEnums.InputSize.Small)
                          .Editor.AddBinding(KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('SystemID', $data)")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                        .Helpers.Bootstrap.LabelFor(Function(rpt) rpt.ProductionAreaID)
                        With .Helpers.Bootstrap.FormControlFor(Function(rpt) rpt.ProductionAreaID, BootstrapEnums.InputSize.Small)
                          .Editor.AddBinding(KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('ProductionAreaID', $data)")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                        .Helpers.Bootstrap.LabelDisplay("Completion Status")
                        With .Helpers.Bootstrap.StateButtonNew("IsCompleted", "Completed", "Not Completed", "btn-success", ,,, "btn-md")
                          .Button.AddClass("btn-block")
                          .Button.AddBinding(KnockoutBindingString.html, "FeedbackReportBO.isCompletedHTML($data)")
                          .Button.AddBinding(KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('IsCompleted', $data)")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                        .Helpers.Bootstrap.LabelDisplay("Publication Status")
                        With .Helpers.Bootstrap.StateButtonNew("IsPublished", "Published", "Not Published", "btn-primary", ,,, "btn-md")
                          .Button.AddClass("btn-block")
                          .Button.AddBinding(KnockoutBindingString.html, "FeedbackReportBO.isPublishedHTML($data)")
                          .Button.AddBinding(KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('IsPublished', $data)")
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                    .AddClass("hidden-xs hidden-sm hidden-md")
                    .Style.Margin("10px")
                    .Helpers.Bootstrap.LabelDisplay("Errors/Messages").Style.Width = "100%"
                    With .Helpers.DivC("error-window-column")
                      .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesBootstrap(ViewModel.CurrentReport(), $element)")
                      .AddBinding(KnockoutBindingString.visible, "Singular.Validation.GetBrokenRulesBootstrap(ViewModel.CurrentReport(), $element).length > 0")
                    End With
                  End With
                  'With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                  '  .Helpers.Bootstrap.LabelFor(Function(rpt) rpt.FeedbackReportSettingID)
                  '  With .Helpers.Bootstrap.FormControlFor(Function(rpt) rpt.FeedbackReportSettingID, BootstrapEnums.InputSize.Small)
                  '    .Editor.AddBinding(KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('FeedbackReportSettingID', $data)")
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Row
                  'With .Helpers.Bootstrap.Column(12, 6, 4, 3, 2)
                  '  With .Helpers.Bootstrap.Label
                  '    .AddBinding(KnockoutBindingString.html, "")
                  '  End With
                  'End With
                  With .Helpers.Bootstrap.Column(12, 6, 6, 2, 2)
                    .AddClass("col-lg-offset-8 col-xl-offset-8")
                    With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                        Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-save", ,
                                        Singular.Web.PostBackType.None)
                      With .Button
                        .AddBinding(Singular.Web.KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('SaveButton', $data)")
                        .AddBinding(Singular.Web.KnockoutBindingString.click, "StudiosFeedBackReportPage.SaveCurrentReport($data)")
                        .AddClass("btn-block")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 6, 6, 2, 2)
                    With .Helpers.Bootstrap.Button("Print", "Print", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-print", ,
                                                   Singular.Web.PostBackType.None)
                      With .Button
                        .AddBinding(Singular.Web.KnockoutBindingString.enable, "StudioFeedbackReportBO.canEdit('PrintButton', $data)")
                        .AddBinding(Singular.Web.KnockoutBindingString.click, "StudiosFeedBackReportPage.CreatePrintReport()")
                        .AddClass("btn-block")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        .Helpers.HTML.Heading3("Rooms")
        With .Helpers.ForEach(Of Integer)("ViewModel.CurrentReportRows()")
          With .Helpers.Bootstrap.Row
            With .Helpers.ForEach(Of StudioFeedbackSectionArea)("FeedbackReportBO.GetStudioFeedbackSectionAreaList($data)")
              'Flatblock per room
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                With .Helpers.Bootstrap.FlatBlock(, , , , , True)
                  'With .ActionsDiv
                  '  'Complete all bookings in this room
                  '  With .Helpers.Bootstrap.StateButtonNew("IsCompleted", "Completed", "Complete", "btn-success", , , ,)
                  '  End With
                  'End With
                  'Room title
                  With .HeaderTag
                    .AddBinding(Singular.Web.KnockoutBindingString.text, Function(s As StudioFeedbackSectionArea) s.Room)
                  End With
                  'Bookings in that room
                  With .ContentTag
                    With .Helpers.ForEach(Of StudioFeedbackSection)("$data.StudioFeedbackSectionList")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                          .AddClass("no-padding no-margin")
                          With .Helpers.Div
                            .AddClass("no-padding no-margin")
                            With .Helpers.HTMLTag("span")
                              .AddClass("booking-time")
                              .AddBinding(Singular.Web.KnockoutBindingString.text, Function(s As StudioFeedbackSection) s.RoomScheduleTime)
                            End With
                            With .Helpers.HTMLTag("span")
                              .AddBinding(Singular.Web.KnockoutBindingString.text, Function(s As StudioFeedbackSection) s.RoomSchedule)
                              .AddClass("a-feedback-section cursor-hover booking-title")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                          With .Helpers.Div
                            .Style.Padding("10px", , , )
                            With .Helpers.Div
                              .AddBinding(Singular.Web.KnockoutBindingString.click, "StudioFeedbackSectionBO.editRoomScheduleArea($data)")
                              .AddBinding(Singular.Web.KnockoutBindingString.title, Function(s) "Edit Booking")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "StudioFeedbackSectionBO.getBookingStatusCss($data, 'EditBooking')")
                              .AddClass("fa fa-pencil fa-lg cursor-hover padding-right-10")
                            End With
                            With .Helpers.Div
                              .AddBinding(Singular.Web.KnockoutBindingString.click, "StudioFeedbackSectionBO.editReport($data, 'Comments')")
                              .AddBinding(Singular.Web.KnockoutBindingString.title, Function(s) "Comments")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "StudioFeedbackSectionBO.getSectionCss($data, 'Comments')")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "StudioFeedbackSectionBO.canEdit('SubReport', $data)")
                              .AddClass("fa fa-commenting-o fa-lg cursor-hover padding-right-10")
                            End With
                            With .Helpers.Div
                              .AddBinding(Singular.Web.KnockoutBindingString.click, "StudioFeedbackSectionBO.editReport($data, 'Crew')")
                              '.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportSectionBO.SetSectionCompletedEnabled($data.GetParent())")
                              .AddBinding(Singular.Web.KnockoutBindingString.title, Function(s) "Crew Feedback")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "StudioFeedbackSectionBO.getSectionCss($data, 'Crew')")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "StudioFeedbackSectionBO.canEdit('SubReport', $data)")
                              .AddClass("fa fa-group fa-lg cursor-hover padding-right-10")
                            End With
                            With .Helpers.Div
                              .AddBinding(Singular.Web.KnockoutBindingString.click, "StudioFeedbackSectionBO.editReport($data, 'Technical')")
                              '.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportSectionBO.SetSectionCompletedEnabled($data.GetParent())")
                              .AddBinding(Singular.Web.KnockoutBindingString.title, Function(s) "Technical Problems")
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "StudioFeedbackSectionBO.getSectionCss($data, 'Technical')")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "StudioFeedbackSectionBO.canEdit('SubReport', $data)")
                              .AddClass("fa fa-cogs fa-lg cursor-hover")
                            End With
                          End With
                        End With
                      End With
                    End With
                    'With .Helpers.Bootstrap.Row
                    '  .Style("margin-left") = "1px"
                    '  .Style("margin-right") = "1px"
                    '  .Style("padding-left") = "15px"
                    '  .Style("border-top") = "1px solid #E6E6E6"
                    '  With .Helpers.ReadOnlyRowFor(Function(s As StudioFeedbackSectionArea) s.ReportCompletedByName)
                    '  End With
                    'End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

      'Modals

      With h.Bootstrap.Dialog("SubReport", "Edit Sub Report Details", False, "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , "fa-edit", "fa-2x")
        .Heading.AddBinding(KnockoutBindingString.html, "FeedbackReportBO.modalTitle(ViewModel.CurrentSubReport())")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of FeedbackReport)("ViewModel.CurrentSubReport()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.DivC("panel-group accordion accordion-color")
                  .Attributes("id") = "report-sections"
                  With .Helpers.ForEach(Of FeedbackReportSection)(Function(s) s.FeedbackReportSectionList)
                    With .Helpers.DivC("panel panel-default")
                      .AddBinding(KnockoutBindingString.visible, "StudioFeedbackSectionBO.sectionVisible($data)")
                      With .Helpers.DivC("panel-heading")
                        With .Helpers.DivC("panel-title")
                          With .Helpers.HTMLTag("a")
                            .AddBinding(KnockoutBindingString.attr, "ReportSectionBO.accordionAttributes($data)")
                            .AddClass("collapsed")
                            .Helpers.Bootstrap.FontAwesomeIcon("fa-angle-right")
                            With .Helpers.HTMLTag("span")
                              .AddBinding(KnockoutBindingString.html, "$data.ReportSection()")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.DivC("panel-collapse collapse")
                        .AddBinding(KnockoutBindingString.attr, "ReportSectionBO.accordionBodyContainerAttributes($data)")
                        With .Helpers.DivC("panel-body")
                          With .Helpers.Bootstrap.TableFor(Of FeedbackReportSubSection)(Function(S) S.FeedbackReportSubSectionList, False, False, , , False, , , "FeedbackReportSubSectionList")
                            With .FirstRow
                              With .AddReadOnlyColumn(Function(s As FeedbackReportSubSection) s.ReportSubSection)
                                .HeaderStyle.Width = "200px"
                              End With
                              With .AddColumn("Details")
                                With .Helpers.Bootstrap.TableFor(Of FeedbackReportDetail)("$data.FeedbackReportDetailList()", True, True, , , False,,, "FeedbackReportDetailList")
                                  With .AddNewButton
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "FeedbackReportSubSectionBO.addNewButtonCss($data)")
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.canView('AddNewButton', $data)")
                                  End With
                                  With .RemoveButton
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.canViewDeleteButton($data)")
                                  End With
                                  With .FirstRow
                                    With .AddColumn(Function(S) S.Column1)
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column1Header")
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit('Column1', $data)")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column1', $data)")
                                      .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column1', $data.GetParent())")
                                    End With
                                    With .AddColumn(Function(S) S.Column2)
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit('Column2', $data)")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column2Header")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column2', $data)")
                                      .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column2', $data.GetParent())")
                                    End With
                                    With .AddColumn(Function(S As FeedbackReportDetail) S.Column3)
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit('Column3', $data)")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column3Header")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column3', $data)")
                                      .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column3', $data.GetParent())")
                                    End With
                                    With .AddColumn(Function(S) S.TextAnswer)
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "AnswerHeader")
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit('TextAnswer', $data)")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('TextAnswer', $data)")
                                      .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('TextAnswer', $data.GetParent())")
                                    End With
                                    With .AddColumn(Function(S) S.Comments) 'Comments' 
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit('Comments', $data)")
                                      .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Comments', $data)")
                                      .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Comments',$data.GetParent())")
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("loading-custom")
                  .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of FeedbackReport)("ViewModel.CurrentSubReport()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
                With .Helpers.Bootstrap.StateButtonNew("IsCompleted", "Completed", "Not Completed", "btn-success", ,,, "btn-md")
                  .Button.AddBinding(KnockoutBindingString.html, "FeedbackReportBO.isCompletedHTML($data)")
                  .Button.AddBinding(KnockoutBindingString.enable, "FeedbackReportBO.canEdit('IsCompleted', $data)")
                  .Button.AddClass("btn-block")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 8, 9)
                With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall,
                                               BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o",, Singular.Web.PostBackType.None, "StudiosFeedBackReportPage.SaveCurrentSubReport($data)")
                End With
              End With
            End With
          End With
        End With
      End With




      'With .Helpers.ForEach(Of FeedbackReportSection)(Function(s) s.FeedbackReportSectionList)
      '  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
      '    With .Helpers.Bootstrap.FlatBlock(, , True, , )
      '      With .ContentTag
      '        .Style.Padding("0", "0", "0", "0")
      '        With .Helpers.Bootstrap.Row
      '          With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
      '            .Style.Padding("0", "0", "0", "0")
      '            With .Helpers.HTMLTag("span")
      '              .AddBinding(Singular.Web.KnockoutBindingString.html, Function(s) s.ReportSection)
      '              .AddClass("rotate")
      '              .AddClass("section")
      '            End With
      '          End With
      '          With .Helpers.Bootstrap.Column(12, 12, 12, 10, 10)
      '            With .Helpers.Bootstrap.TableFor(Of FeedbackReportSubSection)(Function(S) S.FeedbackReportSubSectionList, False, False, , , False)
      '              With .FirstRow
      '                With .AddColumn(Function(S) S.ReportSubSection)
      '                End With
      '                With .AddColumn("Flag Details")
      '                  .Style.Width = "70%"
      '                  With .Helpers.Div
      '                    .AddClass("HtmlColumn")
      '                    .AddBinding(Singular.Web.KnockoutBindingString.html, "FeedbackReportSubSectionBO.getFlagDetailsHTML($data,0)")
      '                  End With
      '                End With
      '              End With
      '              With .AddChildTable(Of FeedbackReportDetail)(Function(S) S.FeedbackReportDetailList, True, True, , , False)
      '                With .AddNewButton
      '                  .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "FeedbackReportSubSectionBO.getButtonHidden($data)")
      '                  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.CanAddDetails($data)")
      '                End With
      '                .AddClass("no-border hover list")
      '                With .FirstRow
      '                  With .AddColumn(Function(S) S.Column1)
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column1Header")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column1', $data)")
      '                    .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column1', $data.GetParent())")
      '                  End With
      '                  With .AddColumn(Function(S) S.Column2)
      '                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column2Header")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column2', $data)")
      '                    .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column2', $data.GetParent())")
      '                  End With
      '                  With .AddColumn(Function(S As FeedbackReportDetail) S.Column3)
      '                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column3Header")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column3', $data)")
      '                    .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column3', $data.GetParent())")
      '                  End With
      '                  With .AddColumn(Function(S) S.TextAnswer)
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "AnswerHeader")
      '                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('TextAnswer', $data)")
      '                    .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('TextAnswer', $data.GetParent())")
      '                  End With
      '                  With .AddColumn(Function(S) S.Comments) 'Comments' 
      '                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                    .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Comments', $data)")
      '                    .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Comments',$data.GetParent())")
      '                  End With
      '                End With
      '                With .AddChildTable(Of FeedbackReportAction)(Function(S) S.FeedbackReportActionList, True, True, , , False)
      '                  With .AddNewButton
      '                  End With
      '                  .AddNewButton.Button.HTML = "Action Instruction"
      '                  .AddClass("no-border hover list")
      '                  With .FirstRow
      '                    With .AddColumn(Function(c) c.DelegateAction)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.canEdit($data)")
      '                    End With
      '                    With .AddColumn(Function(c) c.DelegatedBy)
      '                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "false")
      '                    End With
      '                    With .AddColumn(Function(c) c.ActionBy)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableBasedOnDelegateAction($data)")
      '                    End With
      '                    With .AddColumn(Function(c) c.Instruction)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableBasedOnDelegateAction($data)")
      '                    End With
      '                    With .AddColumn(Function(c) c.ActionByDate)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data.GetParent())")
      '                    End With
      '                    With .AddColumn(Function(c) c.ActionComments)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableActionComment($data)")'
      '                    End With
      '                    With .AddColumn(Function(c) c.ActionTakenDate)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableActionTakenDate($data)")
      '                    End With
      '                    With .AddColumn(Function(c) c.Confidential)
      '                      '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data.GetParent())")
      '                    End With
      '                    With .AddReadOnlyColumn(Function(c) c.MarkedConfidentialBy)
      '                    End With
      '                  End With
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With

      'With h.Bootstrap.Dialog("SubReportSection", "Edit Sub Report Section", False, "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-edit", "fa-2x")
      '  With .Body
      '    .AddClass("colour-tone-modal modal-background-gray")
      '    With .Helpers.With(Of FeedbackReportSection)(Function(c) ViewModel.CurrentSubReportSection)
      '      With .Helpers.Bootstrap.Row
      '        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
      '          With .Helpers.Bootstrap.FlatBlock(, , True, , )
      '            With .ContentTag
      '              .Style.Padding("0", "0", "0", "0")
      '              With .Helpers.Bootstrap.Row
      '                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
      '                  With .Helpers.HTMLTag("span")
      '                    With .Helpers.HTMLTag("h3")
      '                      .AddBinding(Singular.Web.KnockoutBindingString.html, Function(s) s.ReportSection)
      '                    End With
      '                    .Helpers.HTML("<br />")
      '                    .Helpers.HTML("<br />")
      '                  End With
      '                  'Sub Sections
      '                  With .Helpers.Bootstrap.TableFor(Of FeedbackReportSubSection)(Function(S) S.FeedbackReportSubSectionList, False, False, , , False)
      '                    With .FirstRow
      '                      With .AddColumn(Function(S) S.ReportSubSection)
      '                      End With
      '                      'Details
      '                      With .AddColumn("Details")
      '                        With .Helpers.Bootstrap.TableFor(Of FeedbackReportDetail)(Function(S) S.FeedbackReportDetailList, False, False, , , False)
      '                          .AddClass("no-border hover list")
      '                          With .FirstRow
      '                            With .AddColumn(Function(S) S.Column1)
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column1Header")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column1', $data)")
      '                              .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column1', $data.GetParent())")
      '                            End With
      '                            With .AddColumn(Function(S) S.Column2)
      '                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column2Header")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column2', $data)")
      '                              .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column2', $data.GetParent())")
      '                            End With
      '                            With .AddColumn(Function(S As FeedbackReportDetail) S.Column3) 'Column3'
      '                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "Column3Header")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column3', $data)")
      '                              .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Column3', $data.GetParent())")
      '                            End With
      '                            'AnswerHeader                    
      '                            With .AddColumn(Function(S) S.TextAnswer) 'TextAnswer'
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.html, "AnswerHeader")
      '                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('TextAnswer', $data)")
      '                              .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('TextAnswer', $data.GetParent())")
      '                            End With
      '                            With .AddColumn(Function(S) S.HumanResourceID)
      '                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "false")
      '                            End With
      '                            With .AddColumn("Booking Date")
      '                              With .Helpers.EditorFor(Function(s) s.StartDateTimeBuffer)
      '                                .Style.Width = "78px"
      '                              End With
      '                            End With
      '                            With .AddColumn("Call Time")
      '                              With .Helpers.TimeEditorFor(Function(s) s.StartDateTimeBuffer)
      '                                .Style.Width = "60px"
      '                              End With
      '                            End With
      '                            With .AddColumn("Start Time")
      '                              With .Helpers.TimeEditorFor(Function(s) s.StartDateTime)
      '                                .Style.Width = "60px"
      '                              End With
      '                            End With
      '                            With .AddColumn("End Time")
      '                              With .Helpers.TimeEditorFor(Function(s) s.EndDateTime)
      '                                .Style.Width = "60px"
      '                              End With
      '                            End With
      '                            With .AddColumn("Wrap Time")
      '                              With .Helpers.TimeEditorFor(Function(s) s.EndDateTimeBuffer)
      '                                .Style.Width = "60px"
      '                              End With
      '                            End With
      '                            With .AddColumn(Function(S) S.Comments) 'Comments' 
      '                              .Style.Width = "400px"
      '                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data)")
      '                              .HeaderBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Comments', $data)")
      '                              .CellBindings.Add(Singular.Web.KnockoutBindingString.visible, "FeedbackReportSubSectionBO.columnVisible('Comments',$data.GetParent())")
      '                            End With
      '                          End With
      '                          'Actions
      '                          With .AddChildTable(Of FeedbackReportAction)(Function(S) S.FeedbackReportActionList, True, True, , , False) 'FeedbackReportAction'''Object.
      '                            With .AddNewButton
      '                            End With
      '                            .AddNewButton.Button.HTML = "Action Instruction"
      '                            .AddClass("no-border hover list")
      '                            With .FirstRow
      '                              With .AddColumn(Function(c) c.DelegateAction)
      '                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.canEdit($data)")
      '                              End With
      '                              With .AddColumn(Function(c) c.DelegatedBy)
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "false")
      '                              End With
      '                              With .AddColumn(Function(c) c.ActionBy)
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableBasedOnDelegateAction($data)")
      '                              End With
      '                              With .AddColumn(Function(c) c.Instruction)
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableBasedOnDelegateAction($data)")
      '                              End With
      '                              With .AddColumn(Function(c) c.ActionByDate)
      '                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data.GetParent())")
      '                              End With
      '                              With .AddColumn(Function(c) c.ActionComments)
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableActionComment($data)")
      '                              End With
      '                              With .AddColumn(Function(c) c.ActionTakenDate)
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportActionBO.SetEditableActionTakenDate($data)")
      '                              End With
      '                              With .AddColumn(Function(c) c.Confidential)
      '                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportDetailBO.canEdit($data.GetParent())")
      '                              End With
      '                              With .AddReadOnlyColumn(Function(c) c.MarkedConfidentialBy)
      '                              End With
      '                            End With
      '                          End With
      '                        End With
      '                      End With
      '                    End With
      '                  End With
      '                  With .Helpers.Div
      '                    .Style.Padding(, , , "15px")
      '                    With .Helpers.ReadOnlyRowFor(Function(s) s.CompletedBy)
      '                    End With
      '                    With .Helpers.ReadOnlyRowFor(Function(s) s.CompletedDate)
      '                    End With
      '                  End With
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  With .Footer
      '    With .Helpers.Bootstrap.Button(, "Complete", Singular.Web.BootstrapEnums.Style.Success, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , ,
      '        Singular.Web.PostBackType.None)
      '      With .Button
      '        .AddBinding(Singular.Web.KnockoutBindingString.click, "FeedbackReportSubSectionBO.SetSubSectionCompletedBy(ViewModel.CurrentSubReportSection())")
      '        .AddBinding(Singular.Web.KnockoutBindingString.enable, "FeedbackReportSubSectionBO.SetSubSectionCompletedEnabled(ViewModel.CurrentSubReportSection())")
      '      End With
      '    End With
      '    With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , ,
      '                                   "fa-floppy-o", , Singular.Web.PostBackType.None, "StudiosFeedBackReportPage.SaveCurrentSubReport()")
      '    End With
      '  End With
      'End With

      '----------------------------------------------------------------------------------------------------
      '---------------------Start------------------------------------------------------------------
      '----------------------------------------------------------------------------------------------------


      '----------------------------------------------------------------------------------------------------
      '---------------------------END ----------------------------------------------------------------
      '----------------------------------------------------------------------------------------------------

    End Using%>

  <script type="text/javascript">

    function GetFeedBackRptHref() {
      return Singular.RootPath + "/FeedBackReports/DailyStudioFeedbackReport.aspx";
    };

  </script>
</asp:Content>


