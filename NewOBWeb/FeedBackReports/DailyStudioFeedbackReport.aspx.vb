﻿Imports Singular.Web
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports Singular.Reporting
Imports OBLib.FeedbackReports
Imports OBLib.FeedbackReports.ReadOnly
Imports System.ComponentModel
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports System

Public Class DailyStudioFeedbackReport
  Inherits OBPageBase(Of DailyStudioFeedbackReportVM)

  Protected Overrides Sub Setup()
    MyBase.Setup()
    Me.Controls.Add(Helpers.Control(New Controls.RoomScheduleControl(Of DailyStudioFeedbackReportVM)()))
  End Sub

End Class

Public Class DailyStudioFeedbackReportVM
  Inherits OBViewModelStateless(Of DailyStudioFeedbackReportVM)

  Public Shared ReportDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ReportDate, RelationshipTypes.None)
  <Display(Name:="Report Date", Description:=""),
   SetExpression("StudiosFeedBackReportPage.ReportDateSet(self)")>
  Public Property ReportDate() As DateTime
    Get
      Return GetProperty(ReportDateProperty)
    End Get
    Set(value As DateTime)
      SetProperty(ReportDateProperty, value)
    End Set
  End Property

  Public Shared CurrentReportProperty As PropertyInfo(Of StudioFeedbackReport) = RegisterSProperty(Of StudioFeedbackReport)(Function(c) c.CurrentReport, Nothing)
  Public Property CurrentReport As StudioFeedbackReport
    Get
      Return GetProperty(CurrentReportProperty)
    End Get
    Set(value As StudioFeedbackReport)
      SetProperty(CurrentReportProperty, value)
    End Set
  End Property
  Public Property CurrentSectionAreaGuid As String
  'Public Property CurrentSectionGuid As StudioFeedbackSection

  Public Property CurrentReportRows As List(Of Integer) = New List(Of Integer)
  Public Property CurrentSubReport As FeedbackReport
  Public Property CurrentSubSectionSettings As ROFeedbackReportSubSectionSetting
  Public Property CurrentReportSettings As ROFeedbackReportSetting

  Public Property CanUserAdd As Boolean
  Public Property isManager As Boolean = False
  Public Property CurrentRoomScheduleArea As OBLib.Scheduling.Rooms.RoomScheduleArea
  Public Property CurrentProduction As OBLib.Productions.Production
  Public Property CurrentAdHocBooking As OBLib.AdHoc.AdHocBooking

  Protected Overrides Sub Setup()
    MyBase.Setup()

    'DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
    MessageFadeTime = 2000

    ValidationMode = Singular.Web.ValidationMode.OnLoad

    ClientDataProvider.AddDataSource("ROFeedbackReportDropDownItemList", OBLib.CommonData.Lists.ROFeedbackReportDropDownItemList, False)
    ClientDataProvider.AddDataSource("ROFeedbackReportSettingList", OBLib.CommonData.Lists.ROFeedbackReportSettingList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    'ClientDataProvider.AddDataSource("ROUserList", OBLib.CommonData.Lists.ROUserList, False)
    ClientDataProvider.AddDataSource("ROFeedbackReportFlagTypeList", OBLib.CommonData.Lists.ROFeedbackReportFlagTypeList, False)
    'ClientDataProvider.AddDataSource("ROHumanResourceList", OBLib.CommonData.Lists.ROHumanResourceList, False)

  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

    With AddWebRule(ReportDateProperty)
      .JavascriptRuleFunctionName = "StudiosFeedBackReportPage.ReportDateValid"
      .AddTriggerProperties({CurrentReportProperty})
    End With

  End Sub

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SetupStudioFeedbackReport(ReportDate As Date?) As Singular.Web.Result
    Try
      Dim StudioFeedbackReport As StudioFeedbackReport = Nothing
      StudioFeedbackReport = StudioFeedbackReportList.GetStudioFeedbackReportList(ReportDate).FirstOrDefault
      Dim CurrentReportRows = StudioFeedbackReport.StudioFeedbackSectionAreaList.Select(Function(x) x.RowNum).Distinct().ToList()
      Return New Singular.Web.Result(True) With {.Data = New With {.Report = StudioFeedbackReport, .Rows = CurrentReportRows}}
    Catch ex As Exception
      Return New Singular.Web.Result(False) With {.Data = Nothing, .ErrorText = ex.Message}
    End Try
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveFeedbackReport(FeedbackReport As FeedbackReport) As Singular.Web.Result
    Dim WasNew As Boolean = FeedbackReport.IsNew
    If FeedbackReport.IsValid Then
      Dim sh As Singular.SaveHelper = FeedbackReport.TrySave(GetType(FeedbackReportList))
      If Not sh.Success Then
        'error 
        Return New Singular.Web.Result(sh.Success) With {.ErrorText = sh.ErrorText}
      Else
        FeedbackReport = DirectCast(sh.SavedObject, FeedbackReport)
        Return New Singular.Web.Result(sh.Success) With {.Data = FeedbackReport}
      End If
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = "Report is not valid"}
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function SaveStudioFeedbackReport(StudioFeedbackReport As StudioFeedbackReport) As Singular.Web.Result
    Dim WasNew As Boolean = StudioFeedbackReport.IsNew
    If StudioFeedbackReport.IsValid Then
      Dim sh As Singular.SaveHelper = StudioFeedbackReport.TrySave(GetType(StudioFeedbackReportList))
      If Not sh.Success Then
        'error 
        Return New Singular.Web.Result(sh.Success) With {.ErrorText = sh.ErrorText}
      Else
        StudioFeedbackReport = DirectCast(sh.SavedObject, StudioFeedbackReport)
        Dim list As StudioFeedbackReportList = OBLib.FeedbackReports.StudioFeedbackReportList.GetStudioFeedbackReportList(StudioFeedbackReport.ReportDate)
        StudioFeedbackReport = list.FirstOrDefault
        Return New Singular.Web.Result(sh.Success) With {.Data = StudioFeedbackReport}
      End If
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = "Report is not valid"}
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Shared Function CreatePrintoutReport(DisplayInd As Boolean, FeedbackReportID As Integer, ReportName As String, ReportDate As Date) As Singular.Documents.IDocument
    Dim RptFeedback = New OBWebReports.FeedbackReports.StudioFeedbackReport()
    RptFeedback.ReportCriteria.ReportDate = ReportDate
    Dim RptFeedbackinfo = RptFeedback.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)

    If DisplayInd Then
      If RptFeedbackinfo.FileStream IsNot Nothing Then
        Return RptFeedbackinfo
      End If


    Else

      Dim Doc As Singular.Documents.TemporaryDocument = New Singular.Documents.TemporaryDocument(ReportName, RptFeedbackinfo.FileBytes)

      Doc.SaveToDatabase()

      ' link add the Document as an Attachment 
      Try
        Dim cmd As New Singular.CommandProc("CmdProcs.[cmdEmailPublishedFeedbackReport]",
                                          New String() {"@DocumentID", "@FeedbackReportID"},
                                          New Object() {Doc.DocumentID, FeedbackReportID})
        ' cmd.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cmd.Execute()
      Catch ex As Exception
        Throw New Exception(ex.Message)
      End Try

    End If

    Return Nothing

  End Function

End Class


'If StudioFeedbackReport.OriginalReportPublishDateTime Is Nothing AndAlso StudioFeedbackReport.ReportPublishedDateTime IsNot Nothing Then
'   CreatePrintoutReport(False, StudioFeedbackReport.FeedbackReportID, StudioFeedbackReport.ReportName, StudioFeedbackReport.ReportDate)
'End If

'<Singular.Web.WebCallable(LoggedInOnly:=True)>
'Public Shared Function SaveCurrentReport(CurrentReport As FeedbackReport) As Singular.Web.SaveResult
'  Dim WasNew As Boolean = CurrentReport.IsNew
'  If CurrentReport.IsValid Then
'    Dim sh As Singular.SaveHelper = CurrentReport.TrySave(GetType(FeedbackReportList))
'    If Not sh.Success Then
'      'error 
'      Return New Singular.Web.SaveResult(sh)
'    Else
'      CurrentReport = DirectCast(sh.SavedObject, FeedbackReport)

'      If CurrentReport.OriginalReportPublishDateTime Is Nothing AndAlso CurrentReport.ReportPublishedDateTime IsNot Nothing Then
'        CreatePrintoutReport(False, CurrentReport.FeedbackReportID, CurrentReport.ReportName, CurrentReport.ReportDate)
'      End If

'      Return New Singular.Web.SaveResult(sh)
'    End If
'  End If
'End Function

'<Singular.Web.WebCallable(LoggedInOnly:=True)>
'Public Sub GetCurrentSubReport()
'  CurrentSubReport = FeedbackReportList.GetFeedbackReportList(CurrentSubReportID).FirstOrDefault
'  For Each child As FeedbackReportSection In CurrentSubReport.FeedbackReportSectionList
'    For Each SubSection In child.FeedbackReportSubSectionList
'      CurrentSection.FeedbackReportSubSectionList.Add(SubSection)
'    Next
'  Next
'End Sub

'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
'  MyBase.HandleCommand(Command, CommandArgs)

'  Select Case Command
'    Case "Save"
'      Dim fblist As New StudioFeedbackReportList
'      Dim WasNew As Boolean = CurrentReport.IsNew
'      fblist.Add(CurrentReport)
'      If fblist.IsValid Then
'        Dim sh As SaveHelper = TrySave(fblist, True)
'        If Not sh.Success Then
'          'error
'          AddMessage(Singular.Web.MessageType.Error, "error during save", sh.ErrorText)
'        Else
'          CurrentReport = DirectCast(sh.SavedObject, StudioFeedbackReportList).FirstOrDefault

'          If CurrentReport.OriginalReportPublishDateTime Is Nothing AndAlso CurrentReport.ReportPublishedDateTime IsNot Nothing Then
'            CreatePrintoutReport(False, CurrentReport.FeedbackReportID, "Studio Daily Feedback Report", CurrentReport.ReportDate)
'          End If
'        End If
'      End If

'    Case "SetCurrentSubReport"
'      GetCurrentSubReport()
'  End Select

'End Sub