﻿Imports Microsoft.AspNet.SignalR
Imports Microsoft.AspNet.SignalR.Client
Imports Singular.Web

Public Class WebsiteHelpers

  Public Shared Sub CreateUpdateChange(ResourceBookingID As Integer?)
    AddChange(ResourceBookingID, OBLib.CommonData.Enums.ResourceBookingChangeType.Update)
  End Sub

  Private Shared Sub AddChange(ResourceBookingID As Integer?, ChangeTypeID As Integer?)
    Dim cmd As New Singular.CommandProc("cmdProcs.cmdAddChangeNotification",
                                        New String() {"ResourceBookingID", "ChangeTypeID"},
                                        New Object() {Singular.Misc.NothingDBNull(ResourceBookingID),
                                                      Singular.Misc.NothingDBNull(ChangeTypeID)})
    cmd = cmd.Execute
  End Sub

  Public Shared Sub SendResourceBookingUpdateNotifications()

    Dim sg = GlobalHost.ConnectionManager.GetHubContext(Of SiteHub)()

    Dim SerialisedChanges1 = OBLib.Resources.ResourceBookingList.GetSerialisedChanges(True, True)
    sg.Clients.All.ReceiveUpdatedBookings(SerialisedChanges1)
    sg.Clients.All.ReceiveChanges(SerialisedChanges1)

  End Sub

  Public Shared Sub SendUpdateHRTimesheetRequirementStats(HumanResourceID As Integer?, BookingDate As Date?, UserID As Integer?)

    OBLib.Helpers.TimesheetsHelper.UpdateHumanResourceTimesheet(BookingDate, HumanResourceID, UserID)
    Dim statList As OBLib.HR.ReadOnly.ROHRTimesheetRequirementStatList = OBLib.HR.ReadOnly.ROHRTimesheetRequirementStatList.GetROHRTimesheetRequirementStatList(HumanResourceID, Nothing, BookingDate)
    Dim wr As Singular.Web.Result = New Singular.Web.Result(True)
    wr.Data = statList
    Dim SerialisedChanges As String = Singular.Web.Data.JSonWriter.SerialiseObject(wr)
    Dim sg = GlobalHost.ConnectionManager.GetHubContext(Of SiteHub)()
    sg.Clients.All.ReceiveUpdatedTimesheetStats(SerialisedChanges)

  End Sub

  Public Shared Sub SendUnsentNotifications()

    'Dim sg As IHubContext(Of SiteHub) = GlobalHost.ConnectionManager.GetHubContext(Of SiteHub)()

  End Sub

  Public Shared Sub PostSynergyChanges(SynergyImportID As String)

    Dim sg = GlobalHost.ConnectionManager.GetHubContext(Of SiteHub)()
    sg.Clients.All.SendSynergyChanges(SynergyImportID)

  End Sub

End Class
