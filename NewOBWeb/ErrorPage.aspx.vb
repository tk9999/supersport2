﻿Public Class ErrorPage
  Inherits OBPageBase(Of Singular.Web.EmptyViewModel)

  Public ReadOnly Property LastErrorDetails As String
    Get
      If Session("LastError") Is Nothing Then
        Return "Actually there was no error.."
      Else
        Return Session("LastError")
      End If

    End Get
  End Property

  Public ReadOnly Property LastError As String
    Get
      If Session("LastErrorLocation") Is Nothing Then
        Return "An unhandled error has occurred."
      Else
        Return "An unhandled error has occurred on page " & Session("LastErrorLocation")
      End If
    End Get
  End Property

End Class