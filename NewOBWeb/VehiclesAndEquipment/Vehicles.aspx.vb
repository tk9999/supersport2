﻿Imports OBLib.VehiclesAndEquipment
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports Singular.DataAnnotations

Imports OBLib.Maintenance.Equipment.ReadOnly
Imports OBLib.Maintenance.Equipment

Public Class Vehicles
  Inherits OBPageBase(Of VehiclesVM)
End Class

Public Class VehiclesVM
  Inherits OBViewModel(Of VehiclesVM)

#Region " Properties "

  Public Property ROVehicleList As ROVehicleOldList
  Public Property ROEquipmentList As ROEquipmentList
  Public Property CurrentVehicle As Vehicle
  Public Property VehicleNameFilter As String

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)> _
  Public Property ROHumanResourceFilter As String = ""
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property FindHRType As Integer = 1 ' 1: Vehicle HumanResource, 2 Temp Human Resource, 
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property CurrentParentGUID As Guid
  Public Property CurrentChildGUID As Guid
  Public Property TabIndex As Integer = 0
  Public Property ROVehicleEquipmentListCriteria As ROVehicleEquipmentList.Criteria
  Public Property ROVehicleEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of VehiclesVM)
  Public Property ROVehicleListCriteria As ROVehicleOldList.Criteria
  Public Property EquipmentFilter As String = ""
  Public Property VehiclesFilter As String = ""
  Public Property ROEquipmentListCriteria As ROEquipmentList.Criteria
  Public Property ROEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)
  Public Property ROHumanResourceFullList As OBLib.HR.ReadOnly.ROHumanResourceFullList

  'Available Equipments Ind
  Public Property AvailableInd As Object
    Get
      Return ROEquipmentListCriteria.AvailableInd
    End Get
    Set(value As Object)
      ROEquipmentListCriteria.AvailableInd = value
    End Set
  End Property


#End Region

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad
    ROVehicleList = OBLib.CommonData.Lists.ROVehicleList

    CurrentVehicle = Nothing
    MessageFadeTime = 2000
    mROVehicleEquipmentList = New ROVehicleEquipmentList
    ROVehicleEquipmentListCriteria = New ROVehicleEquipmentList.Criteria
    ROVehicleEquipmentListPagingManager = New Singular.Web.Data.PagedDataManager(Of VehiclesVM)(Function(d) Me.ROVehicleEquipmentList, Function(d) Me.ROVehicleEquipmentListCriteria, "", 15)

    ROEquipmentList = New ROEquipmentList
    ROEquipmentListCriteria = New ROEquipmentList.Criteria
    ROEquipmentListPagingManager = New Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)(Function(d) Me.ROEquipmentList, Function(d) Me.ROEquipmentListCriteria, "", 25)


    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "AddNewVehicle"
        CurrentVehicle = New Vehicle
      Case "EditVehicle"
        CurrentVehicle = OBLib.VehiclesAndEquipment.VehicleList.GetVehicleList(CommandArgs.ClientArgs.VehicleID).FirstOrDefault
        ROVehicleEquipmentListCriteria.VehicleID = CommandArgs.ClientArgs.VehicleID
        'Dim hrList = OBLib.HR.ReadOnly.ROHumanResourceFullList.GetROHumanResourceFullList()
        'Dim vhrlist = CurrentVehicle.VehicleHumanResourceList.Select(Function(c) c.HumanResourceID).ToList
        'ClientDataProvider.AddDataSource("ROHumanResourceFilterlist", hrList.Where(Function(c) vhrlist.Contains(c.HumanResourceID)).ToList, False)
        CurrentVehicle.CheckAllRules()
      Case "addVehicleEquipment"
        Dim list As List(Of Integer) = Singular.Web.Data.JS.StatelessJSSerialiser.DeserialiseObject(GetType(List(Of Integer)), CommandArgs.ClientArgs.IDs)
        CurrentVehicle.VehicleEquipmentList.AddVehicleEquipment(list)

      Case "DeleteVehicle"
        Try
          Dim VehicleList As New VehicleList()
          VehicleList.Add(CurrentVehicle)
          VehicleList.Remove(CurrentVehicle)
          VehicleList = TrySave(VehicleList).SavedObject
          CurrentVehicle = Nothing
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try
      Case "SaveVehicle"
        CurrentVehicle.CheckAllRules()
        If Not CurrentVehicle.IsValid Then
          AddMessage(Singular.Web.MessageType.Validation, "Valid", CurrentVehicle.GetErrorsAsString)
          Exit Select
        End If
        With CurrentVehicle.TrySave(GetType(VehicleList))
          If .Success Then
            CurrentVehicle = .SavedObject
            AddMessage(Singular.Web.MessageType.Success, "Save", "Vehicle Saved Successfully")
          Else
            AddMessage(Singular.Web.MessageType.Error, "Save", "Vehicle Save Error. " & .ErrorText)
          End If
        End With
      Case "GetROVehicleEquipment"
        SetVehicleEquipmentCriteria()
      Case "DeleteVehicleEquipment"
        Dim list As List(Of Integer) = Singular.Web.Data.JS.StatelessJSSerialiser.DeserialiseObject(GetType(List(Of Integer)), CommandArgs.ClientArgs.IDs)

        CurrentVehicle.VehicleEquipmentList.DeleteVehicleEquipment(list)
      Case "CheckVehicleHumanResourceRules"
        CheckVehicleHumanResourceRules(CommandArgs) '
    End Select
  End Sub

  Private mROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourceList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourceList
    Get
      Return mROHumanResourceList
    End Get
  End Property

  Private mROVehicleEquipmentList As ROVehicleEquipmentList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROVehicleEquipmentList As ROVehicleEquipmentList


  Private Sub SetVehicleEquipmentCriteria()

    If CurrentVehicle IsNot Nothing Then
      ROVehicleEquipmentListCriteria.VehicleID = CurrentVehicle.VehicleID
      ROVehicleEquipmentListCriteria.PageSize = 15
    End If

  End Sub

  Private Sub CheckVehicleHumanResourceRules(CommandArgs As Singular.Web.CommandArgs)
    Dim VehicleHumanResourceID As Integer? = CommandArgs.ClientArgs.VehicleHumanResourceID
    Dim Guid As Guid = Nothing
    Dim VehicleHumanResource As VehicleHumanResource = Nothing
    If VehicleHumanResourceID Is Nothing Then
      Guid = New Guid(CommandArgs.ClientArgs.Guid.ToString)
      VehicleHumanResource = CurrentVehicle.VehicleHumanResourceList.GetItem(VehicleHumanResourceID) ' SelectedPaymentRun.CreditorInvoiceList.GetItemByGuid(Guid)
    Else
      VehicleHumanResource = CurrentVehicle.VehicleHumanResourceList.GetItem(VehicleHumanResourceID)
    End If
    If VehicleHumanResource IsNot Nothing Then
      If VehicleHumanResource.CheckRulesInd Then
        VehicleHumanResource.CheckAllRules()
      End If
    End If
    VehicleHumanResource.CheckAllRules()
  End Sub

  'Private Function IsVehicleHumanResource(VehicleHumanResourceList As List(Of Integer?), HumanResourceID As Integer)
  '  For Each VehicleHumanResource As Integer In VehicleHumanResourceList
  '    If VehicleHumanResource = HumanResourceID Then
  '      Return True
  '    End If
  '  Next
  '  Return False
  'End Function

  'Private Function FilterROHumanResource()
  '  'ROVehicleEquipmentListCriteria.VehicleID = CommandArgs.ClientArgs.VehicleID
  '  Dim hrList = OBLib.HR.ReadOnly.ROHumanResourceFullList.GetROHumanResourceFullList()
  '  Dim vhrlist = CurrentVehicle.VehicleHumanResourceList.Select(Function(c) c.HumanResourceID).ToList
  '  ClientDataProvider.AddDataSource("ROHumanResourceFilterlist", hrList.Where(Function(c) vhrlist.Contains(c.HumanResourceID)).ToList, False)

  '  For Each HumanResource As OBLib.HR.ReadOnly.ROHumanResourceFull In hrList
  '    If IsVehicleHumanResource(vhrlist, HumanResource.HumanResourceID) Then

  '    End If
  '  Next
  'End Function
End Class