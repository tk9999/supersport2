﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Vehicles.aspx.vb" Inherits="NewOBWeb.Vehicles" %>

<%@ Import Namespace="OBLib.Maintenance.Equipment.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.Equipment" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.Vehicles.ReadOnly" %>

<%@ Import Namespace="OBLib.VehiclesAndEquipment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link rel="Stylesheet" href="../Styles/NewStyle.css" />
  <link rel="Stylesheet" href="../Styles/FlatDream.css" />
  <style type="text/css">
    #EventTypes {
      display: none;
    }

    td.LButtons, th.LButtons {
      width: 24px !important;
    }

    div.ComboDropDown {
      z-index: 35000;
    }

    .ui-dialog-title {
      color: white;
    }

    body {
      background-color: #F0F0F0;
    }

    .ROVehicle {
      overflow-y: scroll !important;
      max-height: 500px !important;
    }

    .ValidationPopup {
      min-height: 150px;
    }
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Using h = Helpers

        '----Toolbar------------------------------------------------------------------------------------------------------------------
        With h.Toolbar
          .Helpers.MessageHolder()
        End With
        
        With h.Bootstrap.Row
          With .Helpers.Bootstrap.FlatBlock("Vehicles", True, False)
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindVehicle()")
                End With
                With .Helpers.Bootstrap.Button("AddNewVehicle", "New Vehicle", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                With .Helpers.Bootstrap.Button("SaveVehicle", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                    
                With .Helpers.Bootstrap.Button("DeleteVehicle", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
              End With
            End With
          End With
        End With
        
        '----Current Vehicle------------------------------------------------------------------------------------------------
        ' With Helpers.Bootstrap.Row
        With Helpers.With(Of Vehicle)(Function(d) ViewModel.CurrentVehicle)
          With .Helpers.Bootstrap.FlatBlock(, True)
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.With(Of Vehicle)(Function(d) ViewModel.CurrentVehicle)
                  With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        .Helpers.LabelFor(Function(d) d.VehicleName)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.VehicleName, Singular.Web.BootstrapEnums.InputSize.Small, )
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        .Helpers.LabelFor(Function(d) d.VehicleTypeID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.VehicleTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        .Helpers.LabelFor(Function(d) d.VehicleDescription)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.VehicleDescription, Singular.Web.BootstrapEnums.InputSize.Small)
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        .Helpers.LabelFor(Function(d) d.SupplierID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.SupplierID, Singular.Web.BootstrapEnums.InputSize.Small)
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        .Helpers.LabelFor(Function(d) d.NoOfDrivers)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.NoOfDrivers, Singular.Web.BootstrapEnums.InputSize.Small)
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        .Helpers.LabelFor(Function(d) d.OvernightNoOfDrivers)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.OvernightNoOfDrivers, Singular.Web.BootstrapEnums.InputSize.Small)
                        End With
                      End With
                    End With
                    .Helpers.HTML.Gap()
                    With .Helpers.Bootstrap.Row
                      .Style.Padding(50, , , )
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        '   With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                          
                        .Helpers.LabelFor(Function(d) d.HDCompatibleInd)
                        'With .Helpers.EditorFor(Function(d) d.HDCompatibleInd)
                        '  .Style.FloatRight()
                        'End With
         
                        With .Helpers.BootstrapStateButton("", "VehicleHDCompatibleIndClick($data)", "VehicleHDCompatibleIndCss($data)", "VehicleHDCompatibleIndHtml($data)", False)
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                    With .Helpers.BootstrapTabControl("InnnerTabC", "nav-pills nav-justified", "tablist")
                      With .AddTab("Human Resources", "fa users", True, "")
                        With .TabPane
                          ' With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                          'With .Helpers.DivC("header")
                          '  .Helpers.HTML.Heading3("Human Resources")
                          'End With
                          '   With .Helpers.With(Of Vehicle)(Function(d) ViewModel.CurrentVehicle)
                          With .Helpers.Bootstrap.TableFor(Of VehicleHumanResource)(Function(d) ViewModel.CurrentVehicle.VehicleHumanResourceList, True, True, False, , True, True, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              .AddClass("items")
                              With .AddColumn("Human Resource")
                                .Style.Width = "220px"
                                With .Helpers.Div
                                  .AddClass("btn-group")
                                  With .Helpers.Bootstrap.Button(, "", Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.Small, , , , Singular.Web.PostBackType.None, "FindROHumanResource($data,1)")
                                    With .Button.Helpers.HTMLTag("i")
                                      .AddClass("fa fa-user")
                                    End With
                                    With .Button.Helpers.HTMLTag("span")
                                      .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.HumanResource)
                                    End With
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "GetHRCss($data)")
                                  End With
                                End With
                              End With
                              With .AddColumn(Function(d) d.DisciplineID)
                                '  .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                              End With
                              With .AddColumn(Function(d) d.PreferenceOrder)
                                ' .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                              End With
                            End With
                            '----Child List------------------------------------------------------------------------------------------------
                            With .AddChildTable(Of VehicleHumanResourceTempPosition)(Function(c) c.VehicleHumanResourceTempPositionList, True, True)
          
                              With .FirstRow
                                With .AddColumn("Human Resource")
                                  .Style.Width = "220px"
                                  With .Helpers.Div
                                    .AddClass("btn-group")
                                    With .Helpers.Bootstrap.Button(, "", Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.Small, , , , Singular.Web.PostBackType.None, "FindROHumanResource($data,2)")
                                      With .Button.Helpers.HTMLTag("i")
                                        .AddClass("fa fa-user")
                                      End With
                                      With .Button.Helpers.HTMLTag("span")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.HumanResource)
                                      End With
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "GetHRCss($data)")
                                    End With
                                  End With
                                End With
                                .AddColumn(Function(v) v.StartDate)
                                .AddColumn(Function(v) v.EndDate)
                              End With
                            End With
                          End With
                          ' End With
                        End With
                      End With
                      With .AddTab("Vehicle Equipment", "fa laptop", False, "GetROVehicleEquipmentList()") 'SetRequirementsTabVisible()
                        With .TabPane
                          With .Helpers.Bootstrap.Button("", "Add Equipment", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus", , Singular.Web.PostBackType.None, "FindEquipment()")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                          End With
                          With .Helpers.Bootstrap.Button("DeleteVehicleEquipment", "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                            .Button.AddBinding(KnockoutBindingString.click, "DeleteVehicleEquipment()")
                          End With
                              
                          With .Helpers.Bootstrap.PagedGridFor(Of ROVehicleEquipment)(Function(vm) ViewModel.ROVehicleEquipmentListPagingManager,
                                            Function(vm) ViewModel.ROVehicleEquipmentList,
                                            False, False, True, False, True, True, False,
                                            , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                            .Pager.AddClass("pull-left")
                            ' With .Helpers.Bootstrap.TableFor(Of ROVehicleEquipment)("GetROVehicleEquipmentList()", False, False, False, , True, True, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              With .AddColumn("Select")
                                .Style.Width = 50
                                With .Helpers.BootstrapStateButton("", "VehicleEquipmentSelectIndClick($data)", "VehicleEquipmentSelectIndCss($data)", "VehicleEquipmentSelectIndHtml($data)", False)
                                End With
                              End With
                              .AddReadOnlyColumn(Function(d) d.EquipmentDescription)
                              .AddReadOnlyColumn(Function(d) d.EquipmentType)
                              .AddReadOnlyColumn(Function(d) d.EquipmentSubType)
                            End With
                          End With
                        End With
                      End With
                      With .AddTab("Services", "fa flag", False, "") 'SetRequirementsTabVisible()
                        With .TabPane
                          With .Helpers.Bootstrap.TableFor(Of VehicleService)(Function(d) ViewModel.CurrentVehicle.VehicleServiceList, True, True, False, , True, True, False)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              .AddColumn(Function(d) d.ServiceDescription)
                              .AddColumn(Function(d) d.StartDateTime)
                              .AddColumn(Function(d) d.EndDateTime)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        
        'With h.Bootstrap.Dialog("FindVehicle", "Find Vehicle")
        '  .ModalDialogDiv.AddClass("modal-md")
        '  With .Body
        With h.Div
          .AddClass("modal-dg")
          .Attributes("id") = "FindVehicle"
          With .Helpers.Bootstrap.FlatBlock(, True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .ContentTag
              With .Helpers.Div
                .AddClass("table-responsive")
                .AddClass("ROVehicle")
                With .Helpers.Bootstrap.TableFor(Of ROVehicle)(Function(vm) ViewModel.ROVehicleList, False, False, False, , True, True, False)
                   
                  .AddClass("no-border hover list")
                  '  .TableBodyClass = "no-border-y"
                  With .FirstRow
                    '.AddClass("items")
                    With .AddColumn("")
                      .Style.Width = 50
                      With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditVehicle($data)")
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(d) d.VehicleName)
                      .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d) d.VehicleType)
                      .AddClass("col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left")
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With
                  With .FooterRow
                    
                  End With
                End With
              End With
            End With
          End With
        End With
        
        'With h.Bootstrap.Dialog("FindEquipment", "Find Equipment")
          
        '  .ModalDialogDiv.AddClass("modal-md")
        '  With .Body
        With h.Div
          .AddClass("modal-dg")
          .Attributes("id") = "FindEquipment"
          With .Helpers.Bootstrap.FlatBlock("Find Equipment", True)
            .FlatBlockTag.AddClass("block-flat")
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                  With .Helpers.Div
                    .AddClass("field-box")
                    With .Helpers.EditorFor(Function(p) ViewModel.EquipmentFilter)
                      .Attributes("placeholder") = "Filter..."
                      .AddClass("form-control filter-field")
                      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterEquipment}")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                  With .Helpers.BootstrapStateButton("", "", "availableIndCss()", "availableIndHtml()", True)
                      
                  End With
                  .Helpers.BootstrapButton("", "Clear", "btn-md btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, True, , , "ClearFilter()")
              
                  With .Helpers.Bootstrap.Button(, "Add to Vehicle", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus", , Singular.Web.PostBackType.None, "AllocateVehicleEquipment()")
                    .Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 0)
                  End With
                End With
              End With
            End With
            
            With .ContentTag
              With .Helpers.Div
                .AddClass("table-responsive")
                '  With .Helpers.Bootstrap.TableFor(Of ROEquipment)(Function(vm) ViewModel.ROEquipmentList, False, False, False, , True, True, False)
                With .Helpers.Bootstrap.PagedGridFor(Of ROEquipment)(Function(vm) ViewModel.ROEquipmentListPagingManager,
                 Function(vm) ViewModel.ROEquipmentList,
                 False, False, True, False, True, True, False,
                 , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                  .AddClass("no-border hover list")
                  With .FirstRow
                    With .AddColumn("")
                      .Style.Width = 50
                      With .Helpers.BootstrapStateButton("", "VehicleEquipmentSelectIndClick($data)", "VehicleEquipmentSelectIndCss($data)", "VehicleEquipmentSelectIndHtml($data)", False)
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(d) d.EquipmentDescription)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d) d.EquipmentType)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d) d.Serial)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d) d.AssetTagNumber)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d) d.VehicleName)
                      .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With

                  With .FooterRow
                  End With
                End With
              End With
            End With
          End With
        End With
     
        'With h.BootstrapDialog("SelectROHumanResource", "Select Human Resource")
        '  With .Body
        '    .AddClass("row")
        With h.Div
          .AddClass("modal-dg")
          .Attributes("id") = "SelectROHumanResource"
          With .Helpers.Bootstrap.FlatBlock("Select Human Resource", True)
            .FlatBlockTag.AddClass("block-flat")
            With .AboveContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.Bootstrap.Column(6, 6, 6, 6)
                    With .Helpers.Div
                      .AddClass("field-box")
                      With .Helpers.EditorFor(Function(p) ViewModel.ROHumanResourceFilter)
                        .Attributes("placeholder") = "Filter..."
                        .AddClass("form-control")
                        .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                        .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROHumanResource}")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.BootstrapButton("", "Clear", "btn-md btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, True, , , "ClearFilter()")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(10, 10, 10, 10)
                  With .Helpers.Div
                    .AddClass("row ROHumanResourceList")
                    With .Helpers.BootstrapTableFor(Of OBLib.AdHoc.Travel.ReadOnly.ROAdHocHumanResource)(Function(d) ViewModel.ROHumanResourceList, False, False, "")
                      .AddClass("ROHumanResourceList")
                      With .FirstRow
                        With .AddColumn("")
                          .Style.Width = 80
                          .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetHumanResourceID($data)")
                        End With
                        .AddReadOnlyColumn(Function(hr As OBLib.AdHoc.Travel.ReadOnly.ROAdHocHumanResource) hr.PreferredFirstSurname)
                        .AddReadOnlyColumn(Function(hr As OBLib.AdHoc.Travel.ReadOnly.ROAdHocHumanResource) hr.IDNo)
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

            
      End Using%>
<script type="text/javascript" src="../Scripts/Pages/Vehicles.js"></script>
</asp:Content>
