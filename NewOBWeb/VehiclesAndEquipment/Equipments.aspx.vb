﻿Imports OBLib.VehiclesAndEquipment
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports Singular.DataAnnotations

Imports OBLib.Maintenance.Equipment.ReadOnly
Imports OBLib.Maintenance.Equipment


Public Class Equipments
  Inherits OBPageBase(Of EquipmentsVM)
End Class

Public Class EquipmentsVM
  Inherits OBViewModelStateless(Of EquipmentsVM)

#Region " Properties "

  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)> _
  'Public Property ROHumanResourceFilter As String = ""
  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)> _
  'Public Property EquipmentFilter As String = ""
  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public Property FindHRType As Integer = 1 ' 1: Vehicle HumanResource, 2 Temp Human Resource, 
  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public Property CurrentParentGUID As Guid
  'Public Property CurrentChildGUID As Guid
  'Public Property ROVehicleEquipmentListCriteria As ROVehicleEquipmentList.Criteria
  'Public Property ROVehicleEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)
  'Public Property TabIndex As Integer = 0
  ''Available Equipemt Ind
  'Public Property AvailableInd As Object
  '  Get
  '    Return ROEquipmentListCriteria.AvailableInd
  '  End Get
  '  Set(value As Object)
  '    ROEquipmentListCriteria.AvailableInd = value
  '  End Set
  'End Property

  Public Property CurrentEquipment As OBLib.Equipment.Equipment
  Public Property ROEquipmentList As OBLib.Equipment.ReadOnly.ROEquipmentList
  Public Property ROEquipmentListCriteria As OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria
  Public Property ROEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of EquipmentsVM)

#End Region


  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad
    MessageFadeTime = 2000

    'mROVehicleEquipmentList = New ROVehicleEquipmentList
    'ROVehicleEquipmentListCriteria = New ROVehicleEquipmentList.Criteria
    'ROVehicleEquipmentListPagingManager = New Singular.Web.Data.PagedDataManager(Of EquipmentsVM)(Function(d) Me.ROVehicleEquipmentList,
    '                                                                                                        Function(d) Me.ROVehicleEquipmentListCriteria,
    '                                                                                                        "", 15)
    CurrentEquipment = Nothing
    ROEquipmentList = New OBLib.Equipment.ReadOnly.ROEquipmentList
    ROEquipmentListCriteria = New OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria
    ROEquipmentListPagingManager = New Singular.Web.Data.PagedDataManager(Of EquipmentsVM)(Function(d) Me.ROEquipmentList, Function(d) Me.ROEquipmentListCriteria, "", 25)

    ClientDataProvider.AddDataSource("ROEquipmentTypeList", OBLib.CommonData.Lists.ROEquipmentTypeList, False)
    ClientDataProvider.AddDataSource("ROEquipmentSubTypeList", OBLib.CommonData.Lists.ROEquipmentSubTypeList, False)
    ClientDataProvider.AddDataSource("ROCountryList", OBLib.CommonData.Lists.ROCountryList, False)
    'ClientDataProvider.AddDataSource("ROEquipmentTypeList", OBLib.CommonData.Lists.ROEquipmentTypeList, False)

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    'Select Case Command
    '  Case "AddNewEquipment"
    '    CurrentEquipment = New OBLib.Maintenance.Equipment.Equipment
    '  Case "EditEquipment"
    '    CurrentEquipment = OBLib.Maintenance.Equipment.EquipmentList.GetEquipmentList(CommandArgs.ClientArgs.EquipmentID).FirstOrDefault


    '  Case "DeleteEquipment"
    '    Try
    '      Dim EquipmentList As New EquipmentList()
    '      EquipmentList.Add(CurrentEquipment)
    '      EquipmentList.Remove(CurrentEquipment)
    '      EquipmentList = TrySave(EquipmentList).SavedObject
    '      CurrentEquipment = Nothing
    '    Catch ex As Exception
    '      AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
    '    End Try
    '  Case "SaveEquipment"
    '    CurrentEquipment.CheckAllRules()
    '    If Not CurrentEquipment.IsValid Then
    '      AddMessage(Singular.Web.MessageType.Validation, "EquipmentHolder", "Valid", CurrentEquipment.BrokenRulesCollection(0).Description)
    '      Exit Select
    '    End If
    '    With CurrentEquipment.TrySave(GetType(EquipmentList))
    '      If .Success Then
    '        CurrentEquipment = .SavedObject
    '        AddMessage(Singular.Web.MessageType.Success, "EquipmentHolder", "Save", "Equipment Saved Successfully")
    '      Else
    '        AddMessage(Singular.Web.MessageType.Error, "EquipmentHolder", "Save", "Equipment Save Error. " & .ErrorText)
    '      End If
    '    End With
    'End Select

  End Sub

  'Private mROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourceList
  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public ReadOnly Property ROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourceList
  '  Get
  '    Return mROHumanResourceList
  '  End Get
  'End Property

  'Private mROVehicleEquipmentList As ROVehicleEquipmentList
  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public Property ROVehicleEquipmentList As ROVehicleEquipmentList

End Class