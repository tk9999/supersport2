﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="Equipments.aspx.vb" Inherits="NewOBWeb.Equipments" %>

<%@ Import Namespace="OBLib.Maintenance.Equipment.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.Equipment" %>

<%@ Import Namespace="OBLib.Equipment.ReadOnly" %>

<%@ Import Namespace="OBLib.Equipment" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.VehiclesAndEquipment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

  <%--  <%= Singular.Web.CSSFile.RenderLibraryStyles%>--%>
  <%--  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />--%>
  <style type="text/css">
    #EventTypes {
      display: none;
    }

    td.LButtons, th.LButtons {
      width: 24px !important;
    }

    div.ComboDropDown {
      z-index: 35000;
    }

    .ui-dialog-title {
      color: black;
    }

    body {
      background-color: #F0F0F0;
    }

    .ROVehicle {
      overflow-y: scroll !important;
      max-height: 500px !important;
    }

    .ValidationPopup {
      min-height: 150px;
    }
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js"></script>
  <script type="text/javascript" src="../Scripts/Pages/Equipment.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      '----Toolbar------------------------------------------------------------------------------------------------------------------
      With Helpers.Toolbar
        .Helpers.MessageHolder("EquipmentHolder")
      End With
        
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Equipment", True, False)
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small,
                                                 , "fa-search", , Singular.Web.PostBackType.None, "EquipmentPage.findEquipment()")
                  End With
                  With .Helpers.Bootstrap.Button(, "New Equipment", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , Singular.Web.PostBackType.None, "EquipmentPage.newEquipment()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                  End With
                End With
                'With .Helpers.Bootstrap.Button("SaveEquipment", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                'End With
                'With .Helpers.Bootstrap.Button("DeleteEquipment", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                'End With
              End With
            End With
          End With
        End With
      End With
      
      With h.Bootstrap.Dialog("FindROEquipment", "Find Equipment", False, "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , "fa-search", , True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
              With .Helpers.With(Of OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria)("ViewModel.ROEquipmentListCriteria()")
                With .Helpers.Bootstrap.FlatBlock("Filters", False, False, False, )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria) d.EquipmentName)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Equipment.ReadOnly.ROEquipmentList.Criteria) d.EquipmentName, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-refresh", , Singular.Web.PostBackType.None, "EquipmentPage.refreshROEquipmentList()", )
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
              With .Helpers.Bootstrap.FlatBlock("Equipment List", False, False, False, )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.PagedGridFor(Of ROEquipment)(Function(vm) ViewModel.ROEquipmentListPagingManager, Function(vm) ViewModel.ROEquipmentList,
                                                                           False, False, True, False, True, True, False, , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                        .AddClass("no-border hover list")
                        With .FirstRow
                          With .AddColumn("")
                            .Style.Width = 50
                            With .Helpers.Bootstrap.Button("", "Edit", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                           "fa-pencil", , Singular.Web.PostBackType.None, "EquipmentPage.editROEquipment($data)")
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(d) d.EquipmentDescription)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(d) d.EquipmentType)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(d) d.Serial)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(d) d.AssetTagNumber)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                          End With
                          With .AddReadOnlyColumn(Function(d) d.VehicleName)
                            .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
        
      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.With(Of Equipment)(Function(d) ViewModel.CurrentEquipment)
            With .Helpers.Bootstrap.FlatBlock(, True)
              With .AboveContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.Button(, "Save Changes", Singular.Web.BootstrapEnums.Style.Success, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", , Singular.Web.PostBackType.None, "EquipmentPage.saveEquipment($data)", )
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "EquipmentBO.canEdit($data, 'SaveButton')")
                    End With
                  End With
                End With
              End With
              .HeaderTag.AddBinding(Singular.Web.KnockoutBindingString.html, "ViewModel.CurrentEquipment().EquipmentDescription()")
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.EquipmentDescription)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentDescription, Singular.Web.BootstrapEnums.InputSize.Small, )
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.EquipmentTypeID)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.EquipmentSubTypeID)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentSubTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    '  .Helpers.Bootstrap.LabelFor(Function(d) d.SupplierID)
                    '  With .Helpers.Bootstrap.FormControlFor(Function(d) d.SupplierID, Singular.Web.BootstrapEnums.InputSize.Small)
                    '  End With
                    'End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.Serial)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.Serial, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.AssetTagNumber)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.AssetTagNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.ActiveInd)
                      With .Helpers.Div
                        With .Helpers.Bootstrap.StateButtonNew(Function(d) d.ActiveInd, "Active", "Inactive", , , , , "btn-sm")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                    End With
                    'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    '  .Helpers.Bootstrap.LabelFor(Function(d As Equipment) d.InternationalInd)
                    '  With .Helpers.Div
                    '    With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small, )
                    '      With .Helpers.HTMLTag("span")
                    '        .AddClass("input-group-btn")
                    '        With .Helpers.Bootstrap.StateButton(Function(d As Equipment) d.InternationalInd)
                    '        End With
                    '      End With
                    '      With .Helpers.Bootstrap.FormControlFor(Function(d As Equipment) d.CountryID, BootstrapEnums.InputSize.Small, )
                              
                    '      End With
                    '    End With
                    '  End With
                    'End With                   
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                    '  With .Helpers.BootstrapTabControl("", "nav-pills", "tablist")
                    '    With .AddTab("Child Equipment", "fa users", True, "")
                    '      With .TabPane
                    '        With .Helpers.Bootstrap.Row
                    '          With .Helpers.Bootstrap.TableFor(Of Equipment)(Function(d) ViewModel.CurrentEquipment.ChildEquipmentList, True, True, False, False, True, True, False)
                    '            .AddClass("no-border hover list")
                    '            .TableBodyClass = "no-border-y"
                    '            With .FirstRow
                    '              With .AddColumn("")
                    '                With .Helpers.Bootstrap.Button(, "Find", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-search", , PostBackType.None, "")
                                        
                    '                End With
                    '              End With
                    '              With .AddColumn(Function(d As Equipment) d.EquipmentDescription)
                    '              End With
                    '              With .AddColumn(Function(d As Equipment) d.EquipmentTypeID)
                    '              End With
                    '              With .AddColumn(Function(d As Equipment) d.EquipmentSubTypeID)
                    '              End With
                    '            End With
                    '          End With
                    '        End With
                    '      End With
                    '    End With
                    '    With .AddTab("Services", "fa users", True, "")
                    '      With .TabPane
                    '        With .Helpers.Bootstrap.Row
                    '          With .Helpers.Bootstrap.TableFor(Of EquipmentService)(Function(d) ViewModel.CurrentEquipment.EquipmentServiceList, True, True, False, , True, True, False)
                    '            .AddClass("no-border hover list")
                    '            .TableBodyClass = "no-border-y"
                    '            With .FirstRow
                    '              With .AddColumn(Function(d) d.ServiceDescription)
                    '              End With
                    '              With .AddColumn(Function(d) d.StartDate)
                    '              End With
                    '              With .AddColumn(Function(d) d.EndDate)
                    '              End With
                    '            End With
                    '          End With
                    '        End With
                    '      End With
                    '    End With
                    '  End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
        
      'With h.Bootstrap.Dialog("FindEquipment", "Find Equipment")
          
      '  .ModalDialogDiv.AddClass("modal-md")
      '  With .Body
      

      '  With .Helpers.Bootstrap.TableFor(Of ROEquipment)(Function(vm) ViewModel.ROEquipmentList, False, False, False, , True, True, False)
      '.Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 1)
      'With .Helpers.BootstrapStateButton("", "VehicleEquipmentSelectIndClick($data)", "VehicleEquipmentSelectIndCss($data)", "VehicleEquipmentSelectIndHtml($data)", False)
      '  .Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 0)
      'End With
      'With .AboveContentTag
      '  With .Helpers.Bootstrap.Row
      '    'With .Helpers.Bootstrap.Column(6, 6, 6, 6, 6)
      '    '  With .Helpers.Div
      '    '    .AddClass("field-box")
      '    '    With .Helpers.EditorFor(Function(p) ViewModel.EquipmentFilter)
      '    '      .Attributes("placeholder") = "Filter..."
      '    '      .AddClass("form-control filter-field")
      '    '      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
      '    '      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterEquipment}")
      '    '    End With
      '    '  End With
      '    'End With
      '    'With .Helpers.Bootstrap.Column(6, 6, 6, 6)
      '    '  With .Helpers.BootstrapStateButton("", "", "availableIndCss()", "availableIndHtml()", True)
                      
      '    '  End With
      '    '  .Helpers.BootstrapButton("", "Clear", "btn-md btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, True, , , "ClearFilter()")
              
      '    '  With .Helpers.Bootstrap.Button(, "Add to Vehicle", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus", , Singular.Web.PostBackType.None, "AllocateVehicleEquipment()")
      '    '    .Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 0)
      '    '  End With
      '    'End With
      '  End With
      'End With  
      
    End Using%>
</asp:Content>


