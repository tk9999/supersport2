﻿Imports OBLib.VehiclesAndEquipment
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports Singular.DataAnnotations

Imports OBLib.Maintenance.Equipment.ReadOnly
Imports OBLib.Maintenance.Equipment

Public Class VehiclesAndEquipment
  Inherits OBPageBase(Of VehiclesAndEquipmentVM)

End Class

Public Class VehiclesAndEquipmentVM
  Inherits OBViewModel(Of VehiclesAndEquipmentVM)

#Region " Properties "

  Public Property ROVehicleList As ROVehicleOldList
  Public Property ROEquipmentList As ROEquipmentList
  Public Property CurrentVehicle As Vehicle
  Public Property CurrentEquipment As Equipment
  Public Property VehicleNameFilter As String


  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)> _
  Public Property ROHumanResourceFilter As String = ""
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)> _
  Public Property EquipmentFilter As String = ""
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property FindHRType As Integer = 1 ' 1: Vehicle HumanResource, 2 Temp Human Resource, 
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property CurrentParentGUID As Guid
  Public Property CurrentChildGUID As Guid
  Public Property TabIndex As Integer = 0
  Public Property ROVehicleEquipmentListCriteria As ROVehicleEquipmentList.Criteria
  Public Property ROVehicleEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)

  Public Property ROEquipmentListCriteria As ROEquipmentList.Criteria
  Public Property ROEquipmentListPagingManager As Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)

  'Available Equipemt Ind
  Public Property AvailableInd As Object
    Get
      Return ROEquipmentListCriteria.AvailableInd
    End Get
    Set(value As Object)
      ROEquipmentListCriteria.AvailableInd = value
    End Set
  End Property


#End Region

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad
    ROVehicleList = OBLib.CommonData.Lists.ROVehicleList


    CurrentVehicle = Nothing
    CurrentEquipment = Nothing

    MessageFadeTime = 2000

    mROVehicleEquipmentList = New ROVehicleEquipmentList
    ROVehicleEquipmentListCriteria = New ROVehicleEquipmentList.Criteria
    ROVehicleEquipmentListPagingManager = New Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)(Function(d) Me.ROVehicleEquipmentList, Function(d) Me.ROVehicleEquipmentListCriteria, "", 15)
  
    ROEquipmentList = New ROEquipmentList
    ROEquipmentListCriteria = New ROEquipmentList.Criteria
    ROEquipmentListPagingManager = New Singular.Web.Data.PagedDataManager(Of VehiclesAndEquipmentVM)(Function(d) Me.ROEquipmentList, Function(d) Me.ROEquipmentListCriteria, "", 25)

  

    DirtyWarning = "Changes have not been saved. Are you sure you want to leave this page?"
 

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command
      Case "AddNewVehicle"
        CurrentVehicle = New Vehicle
      Case "AddNewEquipment"
        CurrentEquipment = New Equipment
      Case "EditVehicle"
        CurrentVehicle = OBLib.VehiclesAndEquipment.VehicleList.GetVehicleList(CommandArgs.ClientArgs.VehicleID).FirstOrDefault
        ROVehicleEquipmentListCriteria.VehicleID = CommandArgs.ClientArgs.VehicleID
      Case "EditEquipment"
        CurrentEquipment = OBLib.Maintenance.Equipment.EquipmentList.GetEquipmentList(CommandArgs.ClientArgs.EquipmentID).FirstOrDefault
      Case "addVehicleEquipment"
        Dim list As List(Of Integer) = Singular.Web.Data.JS.StatelessJSSerialiser.DeserialiseObject(GetType(List(Of Integer)), CommandArgs.ClientArgs.IDs)
        CurrentVehicle.VehicleEquipmentList.AddVehicleEquipment(list)
      Case "DeleteVehicle"
        Try
          Dim VehicleList As New VehicleList()
          VehicleList.Add(CurrentVehicle)
          VehicleList.Remove(CurrentVehicle)
          VehicleList = TrySave(VehicleList).SavedObject
          CurrentVehicle = Nothing
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try
      Case "DeleteEquipment"
        Try
          Dim EquipmentList As New EquipmentList()
          EquipmentList.Add(CurrentEquipment)
          EquipmentList.Remove(CurrentEquipment)
          EquipmentList = TrySave(EquipmentList).SavedObject
          CurrentEquipment = Nothing
        Catch ex As Exception
          AddMessage(Singular.Web.MessageType.Error, ex.Source, Singular.Debug.RecurseExceptionMessage(ex))
        End Try

      Case "SaveVehicle"

        If Not CurrentVehicle.IsValid Then
          AddMessage(Singular.Web.MessageType.Validation, "Valid", CurrentVehicle.BrokenRulesCollection(0).Description)
          Exit Select
        End If
        With CurrentVehicle.TrySave(GetType(VehicleList))
          If .Success Then
            CurrentVehicle = .SavedObject
            AddMessage(Singular.Web.MessageType.Success, "Save", "Vehicle Saved Successfully")
          Else
            AddMessage(Singular.Web.MessageType.Error, "Save", "Vehicle Save Error. " & .ErrorText)
          End If
        End With
      Case "SaveEquipment"
        CurrentEquipment.CheckAllRules()
        If Not CurrentEquipment.IsValid Then
          AddMessage(Singular.Web.MessageType.Validation, "EquipmentHolder", "Valid", CurrentEquipment.BrokenRulesCollection(0).Description)
          Exit Select
        End If
        With CurrentEquipment.TrySave(GetType(EquipmentList))
          If .Success Then
            CurrentEquipment = .SavedObject
            AddMessage(Singular.Web.MessageType.Success, "EquipmentHolder", "Save", "Equipment Saved Successfully")
          Else
            AddMessage(Singular.Web.MessageType.Error, "EquipmentHolder", "Save", "Equipment Save Error. " & .ErrorText)
          End If
        End With
      Case "GetROVehicleEquipment"
        SetVehicleEquipmentCriteria()
      Case "DeleteVehicleEquipment"
        Dim list As List(Of Integer) = Singular.Web.Data.JS.StatelessJSSerialiser.DeserialiseObject(GetType(List(Of Integer)), CommandArgs.ClientArgs.IDs)

        CurrentVehicle.VehicleEquipmentList.DeleteVehicleEquipment(list)
    End Select
  End Sub

  Private mROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourceList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourceList
    Get
      Return mROHumanResourceList
    End Get
  End Property

  Private mROVehicleEquipmentList As ROVehicleEquipmentList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROVehicleEquipmentList As ROVehicleEquipmentList


  Private Sub SetVehicleEquipmentCriteria()

    If CurrentVehicle IsNot Nothing Then
      ROVehicleEquipmentListCriteria.VehicleID = CurrentVehicle.VehicleID
      ROVehicleEquipmentListCriteria.PageSize = 15 
    End If

  End Sub
End Class