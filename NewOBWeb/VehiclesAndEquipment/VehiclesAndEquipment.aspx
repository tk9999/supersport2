﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="VehiclesAndEquipment.aspx.vb" Inherits="NewOBWeb.VehiclesAndEquipment" %>

<%@ Import Namespace="OBLib.Maintenance.Equipment.ReadOnly" %>

<%@ Import Namespace="OBLib.Maintenance.Equipment" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.Vehicles.ReadOnly" %>

<%@ Import Namespace="OBLib.VehiclesAndEquipment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <link type="text/css" href="../Styles/NewStyle.css" rel="Stylesheet" />
  <link type="text/css" href="../Styles/FlatDream.css" rel="Stylesheet" />
  <style type="text/css">
    #EventTypes {
      display: none;
    }

    td.LButtons, th.LButtons {
      width: 24px !important;
    }

    div.ComboDropDown {
      z-index: 35000;
    }

    .ui-dialog-title {
      color: black;
    }

    body {
      background-color: #F0F0F0;
    }

    .ROVehicle {
      overflow-y: scroll !important;
      max-height: 500px !important;
    }

    .ValidationPopup {
      min-height: 150px;
    }
  </style>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Equipments.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      
      
      '----Toolbar------------------------------------------------------------------------------------------------------------------
    
      
      With h.Toolbar
        .Helpers.MessageHolder()
      End With
      With h.Bootstrap.TabControl("MainTab", "nav-pills", "tablist")
   
        .TabHeaderContainer.Attributes("ID") = "PageTabControl"

        With .AddTab("Vehicles", "fa-bus", "MainTabChange(0)", , )
          With .TabPane
            'With .Helpers.Toolbar
            '  .Helpers.MessageHolder()
            'End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindVehicle()")
                End With
                With .Helpers.Bootstrap.Button("AddNewVehicle", "New Vehicle", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                With .Helpers.Bootstrap.Button("SaveVehicle", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                    
                With .Helpers.Bootstrap.Button("Delete", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                
              End With
     
              ' End With
              'End With
            End With
         
            '----Current Vehicle------------------------------------------------------------------------------------------------
            ' With Helpers.Bootstrap.Row
            With .Helpers.With(Of Vehicle)(Function(d) ViewModel.CurrentVehicle)
              With .Helpers.Bootstrap.FlatBlock(, True)
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of Vehicle)(Function(d) ViewModel.CurrentVehicle)
                      With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.VehicleName)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.VehicleName, Singular.Web.BootstrapEnums.InputSize.Small, )
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.VehicleTypeID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.VehicleTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.VehicleDescription)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.VehicleDescription, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.SupplierID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.SupplierID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.NoOfDrivers)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.NoOfDrivers, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.OvernightNoOfDrivers)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.OvernightNoOfDrivers, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        .Helpers.HTML.Gap()
                        With .Helpers.Bootstrap.Row
                          .Style.Padding(50, , , )
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            '   With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                          
                            .Helpers.LabelFor(Function(d) d.HDCompatibleInd)
                            With .Helpers.EditorFor(Function(d) d.HDCompatibleInd)
                              .Style.FloatRight()
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                        With .Helpers.BootstrapTabControl("InnnerTabC", "nav-pills nav-justified", "tablist")
                          With .AddTab("Human Resources", "fa users", True, "")
                            With .TabPane
                              ' With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              'With .Helpers.DivC("header")
                              '  .Helpers.HTML.Heading3("Human Resources")
                              'End With
                              '   With .Helpers.With(Of Vehicle)(Function(d) ViewModel.CurrentVehicle)
                              With .Helpers.Bootstrap.TableFor(Of VehicleHumanResource)(Function(d) ViewModel.CurrentVehicle.VehicleHumanResourceList, True, True, False, , True, True, False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y"
                                With .FirstRow
                                  .AddClass("items")
                                  With .AddColumn("Human Resource")
                                    .Style.Width = "220px"
                                    With .Helpers.DivC("btn-group")
                                      With .Helpers.Bootstrap.Button(, "", Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.Small, , , , Singular.Web.PostBackType.None, "FindROHumanResource($data,1)")
                                        With .Button.Helpers.HTMLTag("i")
                                          .AddClass("fa fa-user")
                                        End With
                                        With .Button.Helpers.HTMLTag("span")
                                          .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.HumanResource)
                                        End With
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "GetHRCss($data)")
                                      End With
                                    End With
                                  End With
                                  With .AddColumn(Function(d) d.DisciplineID)
                                    '  .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                                  End With
                                  With .AddColumn(Function(d) d.PreferenceOrder)
                                    ' .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-2 text-left")
                                  End With
                                End With
                                '----Child List------------------------------------------------------------------------------------------------
                                With .AddChildTable(Of VehicleHumanResourceTempPosition)(Function(c) c.VehicleHumanResourceTempPositionList, True, True)
          
                                  With .FirstRow
                                    With .AddColumn("Human Resource")
                                      .Style.Width = "220px"
                                      With .Helpers.DivC("btn-group")
                                        With .Helpers.Bootstrap.Button(, "", Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.Small, , , , Singular.Web.PostBackType.None, "FindROHumanResource($data,2)")
                                          With .Button.Helpers.HTMLTag("i")
                                            .AddClass("fa fa-user")
                                          End With
                                          With .Button.Helpers.HTMLTag("span")
                                            .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.HumanResource)
                                          End With
                                          .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "GetHRCss($data)")
                                        End With
                                      End With
                                    End With
                                    .AddColumn(Function(v) v.StartDate)
                                    .AddColumn(Function(v) v.EndDate)
                                  End With
                                End With
                              End With
                              ' End With
                            End With
                          End With
                          With .AddTab("Vehicle Equipment", "fa laptop", False, "GetROVehicleEquipmentList()") 'SetRequirementsTabVisible()
                            With .TabPane
                              With .Helpers.Bootstrap.Button("", "Add Equipment", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus", , Singular.Web.PostBackType.None, "FindEquipment()")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                              End With
                              With .Helpers.Bootstrap.Button("DeleteVehicleEquipment", "Delete Selected", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                                '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                                .Button.AddBinding(KnockoutBindingString.click, "DeleteVehicleEquipment()")
                              End With
                              
                              With .Helpers.Bootstrap.PagedGridFor(Of ROVehicleEquipment)(Function(vm) ViewModel.ROVehicleEquipmentListPagingManager,
                                                Function(vm) ViewModel.ROVehicleEquipmentList,
                                                False, False, True, False, True, True, False,
                                                , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                                .Pager.AddClass("pull-left")
                                ' With .Helpers.Bootstrap.TableFor(Of ROVehicleEquipment)("GetROVehicleEquipmentList()", False, False, False, , True, True, False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y"
                                With .FirstRow
                                  .AddReadOnlyColumn(Function(d) d.EquipmentDescription)
                                  .AddReadOnlyColumn(Function(d) d.EquipmentType)
                                  .AddReadOnlyColumn(Function(d) d.EquipmentSubType)
                                  With .AddColumn("Select")
                                    .Style.Width = 50
                                    With .Helpers.BootstrapStateButton("", "VehicleEquipmentSelectIndClick($data)", "VehicleEquipmentSelectIndCss($data)", "VehicleEquipmentSelectIndHtml($data)", False)
                                    End With
                                  End With
                                  
                                End With
                              End With
                            End With
                          End With
                          With .AddTab("Services", "fa flag", False, "") 'SetRequirementsTabVisible()
                            With .TabPane
                              With .Helpers.Bootstrap.TableFor(Of VehicleService)(Function(d) ViewModel.CurrentVehicle.VehicleServiceList, True, True, False, , True, True, False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y"
                                With .FirstRow
                                  .AddColumn(Function(d) d.ServiceDescription)
                                  .AddColumn(Function(d) d.StartDateTime)
                                  .AddColumn(Function(d) d.EndDateTime)
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            '  End With
            
            
          End With
      
      
      
       
        End With
        
        With .AddTab("Equipment", "fa-keyboard-o", "MainTabChange(1)", , )
          With .TabPane
            With .Helpers.Toolbar
              .Helpers.MessageHolder("EquipmentHolder")
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Button(, "Find", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-search", , Singular.Web.PostBackType.None, "FindEquipment()")
                End With
                With .Helpers.Bootstrap.Button("AddNewEquipment", "New Equipment", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus-circle", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                With .Helpers.Bootstrap.Button("SaveEquipment", "Save", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
                With .Helpers.Bootstrap.Button("DeleteEquipment", "Delete", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-trash-o", , Singular.Web.PostBackType.Ajax, )
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) ViewModel.IsValid)
                End With
              End With
            End With
         
            '----Current Vehicle------------------------------------------------------------------------------------------------
    
            With .Helpers.With(Of Equipment)(Function(d) ViewModel.CurrentEquipment)
              With .Helpers.Bootstrap.FlatBlock(, True)
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of Equipment)(Function(d) ViewModel.CurrentEquipment)
                      With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                        With .Helpers.Bootstrap.Row
                          .AddClass("top-buffer-10")
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.EquipmentDescription)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentDescription, Singular.Web.BootstrapEnums.InputSize.Small, )
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.EquipmentTypeID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Row
                          .AddClass("top-buffer-10")
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.EquipmentSubTypeID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentSubTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.SupplierID)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.SupplierID, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Row
                          .AddClass("top-buffer-10")
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.Serial)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Serial, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.AssetTagNumber)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.AssetTagNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Row
                         .AddClass("top-buffer-10")
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d) d.ActiveInd)
                            With .Helpers.Div
                              With .Helpers.Bootstrap.StateButton(Function(d) d.ActiveInd)
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            .Helpers.LabelFor(Function(d As Equipment) d.InternationalInd)
                            With .Helpers.Div
                              With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small, )
                                With .Helpers.HTMLTag("span")
                                  .AddClass("input-group-btn")
                                  With .Helpers.Bootstrap.StateButton(Function(d As Equipment) d.InternationalInd)
                                  End With
                                End With
                                With .Helpers.Bootstrap.FormControlFor(Function(d As Equipment) d.CountryID, BootstrapEnums.InputSize.Small, )
                              
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                        With .Helpers.BootstrapTabControl("", "nav-pills", "tablist")
                          With .AddTab("Child Equipment", "fa users", True, "")
                            With .TabPane
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.TableFor(Of Equipment)(Function(d) ViewModel.CurrentEquipment.ChildEquipmentList, True, True, False, False, True, True, False)
                                  .AddClass("no-border hover list")
                                  .TableBodyClass = "no-border-y"
                                  With .FirstRow
                                    With .AddColumn("")
                                      With .Helpers.Bootstrap.Button(, "Find", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-search", , PostBackType.None, "")
                                        
                                      End With
                                    End With
                                    With .AddColumn(Function(d As Equipment) d.EquipmentDescription)
                                    End With
                                    With .AddColumn(Function(d As Equipment) d.EquipmentTypeID)
                                    End With
                                    With .AddColumn(Function(d As Equipment) d.EquipmentSubTypeID)
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                          With .AddTab("Services", "fa users", True, "")
                            With .TabPane
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.TableFor(Of EquipmentService)(Function(d) ViewModel.CurrentEquipment.EquipmentServiceList, True, True, False, , True, True, False)
                                  .AddClass("no-border hover list")
                                  .TableBodyClass = "no-border-y"
                                  With .FirstRow
                                    With .AddColumn(Function(d) d.ServiceDescription)
                                    End With
                                    With .AddColumn(Function(d) d.StartDate)
                                    End With
                                    With .AddColumn(Function(d) d.EndDate)
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
      
      
      
       
        End With
        
        
        ''------------ Dialogs ----------------
        With h.Bootstrap.Dialog("FindVehicle", "Find Vehicle")
          .ModalDialogDiv.AddClass("modal-md")
          With .Body
            With .Helpers.Bootstrap.FlatBlock("Vehicle", True)
              .FlatBlockTag.AddClass("flat-block-paged")
              With .ContentTag
                With .Helpers.DivC("table-responsive")
                  .AddClass("ROVehicle")
                  With .Helpers.Bootstrap.TableFor(Of ROVehicle)(Function(vm) ViewModel.ROVehicleList, False, False, False, , True, True, False)
                   
                    .AddClass("no-border hover list")
                    '  .TableBodyClass = "no-border-y"
                    With .FirstRow
                      '.AddClass("items")
                      With .AddColumn("")
                        .Style.Width = 50
                        With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditVehicle($data)")
                        End With
                      End With
                      With .AddReadOnlyColumn(Function(d) d.VehicleName)
                        ' .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                      With .AddReadOnlyColumn(Function(d) d.VehicleType)
                        ' .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                    End With
                    With .FooterRow
                    
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        
        '-------------- Equipment
        With h.Bootstrap.Dialog("FindEquipment", "Find Equipment")
          
          .ModalDialogDiv.AddClass("modal-md")
          With .Body
            With .Helpers.Bootstrap.Row
              With .Helpers.BootstrapDivColumn(12, 12, 12)
                With .Helpers.BootstrapDivColumn(6, 6, 6)
                  With .Helpers.DivC("field-box")
                    With .Helpers.EditorFor(Function(p) ViewModel.EquipmentFilter)
                      .Attributes("placeholder") = "Filter..."
                      .AddClass("form-control filter-field")
                      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterEquipment}")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.BootstrapStateButton("", "availableIndClick()", "availableIndCss()", "availableIndHtml()", True)
                      
                  End With
                  .Helpers.BootstrapButton("", "Clear", "btn-md btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, True, , , "ClearFilter()")
              
                  With .Helpers.Bootstrap.Button(, "Add to Vehicle", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-plus", , Singular.Web.PostBackType.None, "AllocateVehicleEquipment()")
                    .Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 0)
                  End With
                End With
              End With
              
            End With
            
            With .Helpers.Bootstrap.FlatBlock("Equipment", True)
              .FlatBlockTag.AddClass("flat-block-paged")
              With .ContentTag
                With .Helpers.DivC("table-responsive")
                   
                  '  With .Helpers.Bootstrap.TableFor(Of ROEquipment)(Function(vm) ViewModel.ROEquipmentList, False, False, False, , True, True, False)
                  With .Helpers.Bootstrap.PagedGridFor(Of ROEquipment)(Function(vm) ViewModel.ROEquipmentListPagingManager,
                   Function(vm) ViewModel.ROEquipmentList,
                   False, False, True, False, True, True, False,
                   , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                    .AddClass("no-border hover list")
                    With .FirstRow
                      With .AddColumn("")
                        .Style.Width = 50
                        
                        With .Helpers.Bootstrap.Button("", "Select", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditEquipment($data)")
                          .Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 1)
                        End With
                        With .Helpers.BootstrapStateButton("", "VehicleEquipmentSelectIndClick($data)", "VehicleEquipmentSelectIndCss($data)", "VehicleEquipmentSelectIndHtml($data)", False)
                          .Button.AddBinding(KnockoutBindingString.visible, Function(c) ViewModel.TabIndex = 0)
                        End With
                        
                      End With
                      With .AddReadOnlyColumn(Function(d) d.EquipmentDescription)
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                      With .AddReadOnlyColumn(Function(d) d.EquipmentType)
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                      With .AddReadOnlyColumn(Function(d) d.Serial)
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                      With .AddReadOnlyColumn(Function(d) d.AssetTagNumber)
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                      With .AddReadOnlyColumn(Function(d) d.VehicleName)
                        .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                      End With
                    End With
                    
             
                    
                    With .FooterRow
                   
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        '---------------------- HR 
        
        With h.BootstrapDialog("SelectROHumanResource", "Select Human Resource")
          With .Body
            .AddClass("row")
            With .Helpers.BootstrapDivColumn(8, 8, 8)
              With .Helpers.BootstrapDivColumn(8, 8, 8)
                With .Helpers.DivC("field-box")
                  With .Helpers.EditorFor(Function(p) ViewModel.ROHumanResourceFilter)
                    .Attributes("placeholder") = "Filter..."
                    .AddClass("form-control filter-field")
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROHumanResource}")
                  End With
                End With
              End With
              With .Helpers.BootstrapDivColumn(4, 4, 4)
                .Helpers.BootstrapButton("", "Clear", "btn-sm btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, True, , , "ClearFilter()")
              End With
            End With
            .Helpers.HTML.Gap()
            With .Helpers.BootstrapDivColumn(10, 10, 10)
              With .Helpers.DivC("row ROHumanResourceList")
                With .Helpers.BootstrapTableFor(Of OBLib.AdHoc.Travel.ReadOnly.ROAdHocHumanResource)(Function(d) ViewModel.ROHumanResourceList, False, False, "")
                  .AddClass("ROHumanResourceList")
                  With .FirstRow
                    With .AddColumn("")
                      .Style.Width = 80
                      .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetHumanResourceID($data)")
                    End With
                    .AddReadOnlyColumn(Function(hr As OBLib.AdHoc.Travel.ReadOnly.ROAdHocHumanResource) hr.PreferredFirstSurname)
                    .AddReadOnlyColumn(Function(hr As OBLib.AdHoc.Travel.ReadOnly.ROAdHocHumanResource) hr.IDNo)
                  End With
                End With
              End With
            End With
  
          End With
        End With
      End With
      
      
    End Using%>
  <script type="text/javascript">



    function CheckDates(Value, Rule, RuleArgs) {

      if (RuleArgs.Object.IsDirty && RuleArgs.Object.IsDirty()) {

        if (RuleArgs.Object.StartDate() && RuleArgs.Object.EndDate()) {
          var StartDate = new Date(RuleArgs.Object.StartDate()).getTime();
          var EndDate = new Date(RuleArgs.Object.EndDate()).getTime();

          if (!RuleArgs.IgnoreResults) {
            if (EndDate <= StartDate) {
              RuleArgs.AddError("Start date must be less than end date");
              return;
            }
          }
        }
      }
    }

    //Select Ind
    function VehicleEquipmentSelectIndClick(ROVehicleEquipment) {
      ROVehicleEquipment.SelectInd(!ROVehicleEquipment.SelectInd());
    };

    function VehicleEquipmentSelectIndCss(ROVehicleEquipment) {
      if (ROVehicleEquipment.SelectInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
    };

    function VehicleEquipmentSelectIndHtml(ROVehicleEquipment) {
      if (ROVehicleEquipment.SelectInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
    };

    //ROEquipment Filter Select Ind
    function availableIndClick() {

      if (ViewModel.AvailableInd() == true) {
        ViewModel.AvailableInd(false);
      }
      else if (ViewModel.AvailableInd() == false) {
        ViewModel.AvailableInd(null)
      } else {
        ViewModel.AvailableInd(true)
      }
      GetROEquipmentList();
    };

    function availableIndCss() {
      return 'btn btn-md btn-primary'

    };

    function availableIndHtml() {
      if (ViewModel.AvailableInd() == true) {
        return "Available Only"
      }
      else if (ViewModel.AvailableInd() == false) {
        return "Unvailable Only"
      } else {
        return "All Equipment"
      }
    };

    Singular.OnPageLoad(function () {
      $('#PageTabControl a[href="#Vehicles"]').tab('show')
    });

    function FindVehicle() {
      $("#FindVehicle").modal();
    }

    function FindEquipment() {
      ViewModel.AvailableInd(true);
      ViewModel.EquipmentFilter("");
      ViewModel.ROEquipmentList([]);
      $("#FindEquipment").modal();
      GetROEquipmentList();
    }

    function EditVehicle(ROVehicle) {



      Singular.SendCommand("EditVehicle",
                           {
                             VehicleID: ROVehicle.VehicleID()
                           },
                           function (response) {
                             $("#FindVehicle").modal('hide');
                           });

    };

    function MainTabChange(NewIndex) {
      ViewModel.TabIndex(NewIndex);
    }


    function EditEquipment(Equipment) {

      Singular.SendCommand("EditEquipment",
                           {
                             EquipmentID: Equipment.EquipmentID()
                           },
                           function (response) {
                             $("#FindEquipment").modal('hide');
                           });

    };

    function GetHRCss(obj) {
      if (obj.HumanResourceID()) {
        return "btn-custom btn-primary HumanResourceID";
      } else {
        return "btn-custom btn-default HumanResourceID";
      }
    } //GetHRCss

    function FilterROHumanResource() {
      if (ViewModel.ROHumanResourceFilter().length >= 3) {
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceList, OBLib', {
          FilterName: ViewModel.ROHumanResourceFilter()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.ROHumanResourceList);
            $("#EquipmentButton").html("HTML(mText)");
            Singular.HideLoadingBar();
          }
        });
      }
    }//FilterROHumanResource  

    function FindROHumanResource(DataObject, FindHRType) {
      ViewModel.FindHRType(FindHRType);
      ViewModel.CurrentParentGUID(DataObject.Guid());
      if (FindHRType == 2) {
        ViewModel.CurrentParentGUID(DataObject.GetParent().Guid());
        ViewModel.CurrentChildGUID(DataObject.Guid());
      }
      ViewModel.ROHumanResourceFilter("");
      ViewModel.ROHumanResourceList([]);

      $('#SelectROHumanResource').modal();
    };

    function SetHumanResourceID(ROHumanResource) {
      var currVHR = undefined
      ViewModel.CurrentVehicle().VehicleHumanResourceList().Iterate(function (vhr, Index) {
        if (vhr.Guid() == ViewModel.CurrentParentGUID()) {
          currVHR = vhr
        }
      });

      if (ViewModel.FindHRType() == 1) { //For Vehicle HR
        currVHR.HumanResource(ROHumanResource.PreferredFirstSurname());
        currVHR.HumanResourceID(ROHumanResource.HumanResourceID());
      } else if (ViewModel.FindHRType() == 2) { //Attendee
        currVHR.VehicleHumanResourceTempPositionList().Iterate(function (vhrtp, Index) {
          if (vhrtp.Guid() == ViewModel.CurrentChildGUID()) {
            vhrtp.HumanResource(ROHumanResource.PreferredFirstSurname());
            vhrtp.HumanResourceID(ROHumanResource.HumanResourceID());
          }
        });
      }
      $('#SelectROHumanResource').modal('hide');
    }

    function FilterEquipment() {
      if (ViewModel.EquipmentFilter().length >= 3) {

        var Available = false
        if (ViewModel.TabIndex() == 0) {
          ViewModel.AvailableInd(true)
        }
        GetROEquipmentList();

      }
    }

    function GetROEquipmentList() {
      Singular.ShowLoadingBar();
      Singular.GetDataStateless('OBLib.Maintenance.Equipment.ReadOnly.ROEquipmentList, OBLib', {
        AvailableInd: ViewModel.AvailableInd(),
        PageSize: 20,
        FilterName: ViewModel.EquipmentFilter()
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.ROEquipmentList);
          Singular.HideLoadingBar();
        }
      });

    }

    function GetROVehicleEquipmentList() {
      Singular.ShowLoadingBar();
      ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
      Singular.HideLoadingBar();

    }

    function ClearFilter() {
      ViewModel.EquipmentFilter("");
    }

    function DeleteVehicleEquipment() {

      var tempList = ViewModel.ROVehicleEquipmentList().Filter("SelectInd", true);
      var ret = [];
      for (var i = 0; i < tempList.length; i++) {
        ret.push(tempList[i].VehicleEquipmentID());
      }

      Singular.SendCommand("DeleteVehicleEquipment", { IDs: ret }, function () {
        ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
      });

    }

    function AllocateVehicleEquipment() {

      var tempList = ViewModel.ROEquipmentList().Filter("SelectInd", true);
      var ret = [];
      for (var i = 0; i < tempList.length; i++) {
        ret.push(tempList[i].EquipmentID());
      }

      Singular.SendCommand("addVehicleEquipment", { IDs: ret }, function () {
        ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
      });

      $("#FindEquipment").modal('hide');

    }



  </script>
</asp:Content>
