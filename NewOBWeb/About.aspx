<%@ Page Title="About Us" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
  CodeBehind="About.aspx.vb" Inherits="NewOBWeb.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
    h2
    {
      padding-top: 20px;
      padding-bottom: 10px;
      border-bottom: 1px solid #a0c0f0;
      color: #2B7CE9;
    }
  </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  <h2 id="license">
    Vis.js License</h2>
  <p>
    Copyright (C) 2010-2014 Almende B.V.
  </p>
  <p>
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the License
    at 
  </p>
  <p>
    http://www.apache.org/licenses/LICENSE-2.0
  </p>
  <p>
    Unless required by applicable law or agreed to in writing, software distributed
    under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
    OF ANY KIND, either express or implied. See the License for the specific language
    governing permissions and limitations under the License.
  </p>
</asp:Content>
