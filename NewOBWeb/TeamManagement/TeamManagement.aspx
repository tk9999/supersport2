﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="TeamManagement.aspx.vb" Inherits="NewOBWeb.TeamManagement" %>

<%@ Import Namespace="Singular.Web" %>

<%@ Import Namespace="OBLib.Maintenance.ShiftPatterns.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" src="../Scripts/Pages/TeamManagementPage.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/ShiftPatterns.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/SystemManagement.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js"></script>
  <style type="text/css">
    .ValidationPopup {
      min-height: 131px;
      max-height: 150px;
      max-width: 500px;
    }

    .FadeHide {
      visibility: hidden;
      opacity: 0;
      transition: visibility 1s, opacity 1s;
    }

    .FadeDisplay {
      visibility: visible;
      opacity: 1;
      transition: visibility 1s, opacity 1s linear;
    }

    .add-vertical-scroll tbody td {
      width: 100%;
    }

    .add-vertical-scroll {
      display: block;
      max-height: 500px;
      overflow-y: scroll;
    }

    .CustomModalErrorMessage {
      height:auto;
      width:200px;
      visibility:visible;
      overflow-y:scroll;
      display:block;
      max-height:70px;
      z-index:9999;
    }

    .BookTeamHRButton {
      width:150px;
      text-overflow:ellipsis;
      overflow:hidden;
      white-space:nowrap;
      display:block;
    }

  </style>
  <script type="text/javascript">
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers
      'Toolbar
      'Helpers.Control(New NewOBWeb.Controls.UserSystemActions(Of NewOBWeb.SystemManagementVM)())
      
      With Helpers.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
          With .Helpers.Bootstrap.FlatBlock("Criteria")
            With .ContentTag
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) ViewModel.CurrentSystemID).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSystemID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                End With
              End With
              With .Helpers.With(Of Integer?)(Function(d) ViewModel.CurrentSystemID)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button("", "Manage Teams", , , , , , , , "TeamManagementPage.manageTeams()", )
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Button("", "Manage Shift Patterns", , , , , , , , "TeamManagementPage.manageSystemAreaShiftPatterns()", )
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Button("", "Manage Team Numbers", , , , , , , , "TeamManagementPage.manageTeamNumbers()", )
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 10, 10)
          With .Helpers.Bootstrap.Row
            ' Teams
            With .Helpers.If(Function(vm) vm.TeamManagementSelected)
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .Attributes("id") = "SystemTeamTable"
                .AddClass("FadeHide")
                .Helpers.Control(New NewOBWeb.Controls.SystemManagementSystemTeams(Of NewOBWeb.TeamManagementVM)("ROSystemTeamList", ""))
              End With
            End With
            With .Helpers.If(Function(vm) vm.SystemAreaShiftPatternSelected)
              'Shift Patterns
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .Attributes("id") = "SystemAreaShiftPatternTable"
                .AddClass("FadeHide")
                With .Helpers.Bootstrap.FlatBlock("Shift Patterns", True)
                  With .AboveContentTag
                    With .Helpers.With(Of ROSystemAreaShiftPatternList.Criteria)("ViewModel.ROSystemAreaShiftPatternListCriteria()")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(d As ROSystemAreaShiftPatternList.Criteria) d.SystemID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ROSystemAreaShiftPatternList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                              .Editor.Attributes("placeholder") = "Sub-Dept"
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(vm) ViewModel.ROSystemAreaShiftPatternListCriteria.ProductionAreaID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(vm) ViewModel.ROSystemAreaShiftPatternListCriteria.ProductionAreaID, BootstrapEnums.InputSize.Small)
                              .Editor.Attributes("placeholder") = "Area"
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .Helpers.LabelFor(Function(c As ROSystemAreaShiftPatternList.Criteria) c.KeyWord).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(c As ROSystemAreaShiftPatternList.Criteria) c.KeyWord, BootstrapEnums.InputSize.Small)
                              .Editor.Attributes("placeholder") = "Search for Pattern"
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                          .AddClass("col-xl-offset-3")
                          With .Helpers.Bootstrap.Button("", "Add Shift Pattern", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , , "TeamManagementPage.addSASP()", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "SystemAreaShiftPatternBO.CanEdit($data, 'AddNewSASP')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .ContentTag
                    .Helpers.Control(New NewOBWeb.Controls.SystemManagementShiftPatterns(Of NewOBWeb.TeamManagementVM)("ROSystemAreaShiftPatternList"))
                  End With
                End With
              End With
            End With
            With .Helpers.If(Function(vm) vm.TeamNumbersSelected)
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                .Attributes("id") = "TeamNumbers"
                .AddClass("FadeHide")
                With .Helpers.Bootstrap.FlatBlock("Team Numbers", True)
                  With .ContentTag
                    With .Helpers.DivC("pull-left")
                      With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-save", ,
                                                     PostBackType.None, "TeamManagementPage.saveTeamNumbers($data)", True)
                        .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                      End With
                    End With
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.TeamManagement.SystemTeamNumber)(Function(c) ViewModel.TeamNumberList, False, True, , , , , )
                        .AddClass("no-border hover list")
                        With .FirstRow
                          With .AddColumn(Function(c As OBLib.TeamManagement.SystemTeamNumber) c.SystemID)
                          End With
                          With .AddColumn(Function(c As OBLib.TeamManagement.SystemTeamNumber) c.SystemTeamNumber)
                          End With
                          With .AddColumn(Function(c As OBLib.TeamManagement.SystemTeamNumber) c.SystemTeamNumberName)
                          End With
                        End With
                        With .FooterRow
                          With .AddColumn("")
                            With .Helpers.Bootstrap.Button("", "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus-circle", , , "TeamManagementPage.addTeamNumber()", )
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      
      h.Control(New NewOBWeb.Controls.AddSystemAreaShiftPatternModal(Of NewOBWeb.TeamManagementVM)("AddSystemAreaShiftPatternModal", "ViewModel", "SystemAreaShiftPatternList"))
      h.Control(New NewOBWeb.Controls.EditSystemAreaShiftPatternModal(Of NewOBWeb.TeamManagementVM)("EditSystemAreaShiftPatternModal", "ViewModel", "EditableSystemAreaShiftPatternList"))
      'h.Control(New NewOBWeb.Controls.AddTeamModal(Of NewOBWeb.TeamManagementVM)("AddTeamModal", "ViewModel"))
      h.Control(New NewOBWeb.Controls.EditTeamModal(Of NewOBWeb.TeamManagementVM)("EditTeamModal", "EditTeamModal"))
      h.Control(New NewOBWeb.Controls.SystemTeamHumanResourcesModal(Of NewOBWeb.TeamManagementVM)("SystemTeamHumanResourcesModal",
                                                                                                    "ViewModel.ROSystemTeamSkilledHRList"))
    End Using%>
</asp:Content>
