﻿Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Csla
Imports OBLib.TeamManagement.ReadOnly
Imports OBLib.TeamManagement
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.SystemManagement
Imports System

Public Class TeamManagement
  Inherits OBPageBase(Of TeamManagementVM)
End Class

Public Class TeamManagementVM
  Inherits OBViewModel(Of TeamManagementVM)
  Implements ControlInterfaces(Of TeamManagementVM).ISystemTeams
  Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns
  Implements ControlInterfaces(Of TeamManagementVM).IEditTeam

#Region "Object Properties "

#Region "ViewModel Properties "


  <Display(Name:="Sub-Dept.", Description:="", Order:=1),
   Required(ErrorMessage:="Sub-Dept is required"),
   Singular.DataAnnotations.DropDownWeb(GetType(ROUserSystemList),
                                        UnselectedText:="Sub-Dept.",
                                        ValueMember:="SystemID"),
  SetExpression("TeamManagementPage.SystemIDSet()")>
  Public Property CurrentSystemID As Integer?

  <Display(Name:="Area", Description:="", Order:=2),
 Singular.DataAnnotations.DropDownWeb(GetType(ROUserSystemAreaList),
                                      UnselectedText:="Area")>
  Public Property CurrentProductionAreaID As Integer? 'Initially set to user's current Production Area', ThisFilterMember:="CurrentSystemID"

#End Region

#Region "Team Numbers"

  Public Property TeamNumberList As SystemTeamNumberList

#End Region

#Region "User Systems "

  Public Property ROUserSystemList As OBLib.Maintenance.General.ReadOnly.ROUserSystemList
  Public Property UserSystemList As OBLib.Security.UserSystemList = OBLib.Security.Settings.CurrentUser.UserSystemList

#End Region

#Region "System Teams"

  <InitialDataOnly>
  Public Property ROSystemTeamList As OBLib.TeamManagement.ReadOnly.ROSystemTeamList Implements ControlInterfaces(Of TeamManagementVM).ISystemTeams.ROSystemTeamList
  <InitialDataOnly>
  Public Property ROSystemTeamListCriteria As OBLib.TeamManagement.ReadOnly.ROSystemTeamList.Criteria Implements ControlInterfaces(Of TeamManagementVM).ISystemTeams.ROSystemTeamListCriteria
  <InitialDataOnly>
  Public Property ROSystemTeamListManager As Singular.Web.Data.PagedDataManager(Of TeamManagementVM) Implements ControlInterfaces(Of TeamManagementVM).ISystemTeams.ROSystemTeamListManager

  Public Property EditableSystemTeamShiftPattern As OBLib.TeamManagement.SystemTeamShiftPattern Implements ControlInterfaces(Of TeamManagementVM).IEditTeam.EditableSystemTeamShiftPattern

  Public Property CurrentEditableSystemTeamShiftPatternID As Integer? = Nothing

  Public Property TemplateUpDate As Boolean = False

#End Region

#Region "System Production Areas"

  Public Property ROSystemProductionArea As OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionArea

  Public Property CurrentSystemProductionAreaID As Integer?

#End Region

#Region "System Area Shift Patterns"

  Public Property SystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns.SystemAreaShiftPatternList
  Public Property EditableSystemAreaShiftPattern As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns.EditableSystemAreaShiftPattern

  <InitialDataOnly>
  Public Property ROSystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns.ROSystemAreaShiftPatternList
  <InitialDataOnly>
  Public Property ROSystemAreaShiftPatternListCriteria As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList.Criteria Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns.ROSystemAreaShiftPatternListCriteria
  <InitialDataOnly>
  Public Property ROSystemAreaShiftPatternListManager As Singular.Web.Data.PagedDataManager(Of TeamManagementVM) Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns.ROSystemAreaShiftPatternListManager

  Public Property ROSystemAreaShiftPatternListJustFetched As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList

  Public Property ROSystemAreaShiftPatternTemplateListCriteria As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternTemplateList.Criteria

  Public Property EditableSystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList Implements ControlInterfaces(Of TeamManagementVM).IShiftPatterns.EditableSystemAreaShiftPatternList

#End Region

#Region "Team Discipline Human Resources"

  Public Property ROSystemTeamSkilledHRList As OBLib.TeamManagement.ReadOnly.ROSystemTeamSkilledHRList

  Public Property TeamDisciplineHumanResourceToReplace As OBLib.TeamManagement.TeamDisciplineHumanResource

  Public Property CurrentTeamDisciplineHumanResource As OBLib.TeamManagement.TeamDisciplineHumanResource

  Public Property CurrentTeamDisciplineHumanResourceID As Integer? = Nothing

  Public Property CurrentDisciplineID As Integer? = Nothing

  Public Property ROHumanResourceFilter As String Implements ControlInterfaces(Of TeamManagementVM).IEditTeam.ROHumanResourceFilter

  ' Used in GetDataStateless to show HR selected on a team
  Public Property HRSelectedList As List(Of Integer) = New List(Of Integer)

#End Region

#Region "Team Shift Patterns"

  Public Property TeamShiftPatternList As OBLib.TeamManagement.TeamShiftPatternList Implements ControlInterfaces(Of TeamManagementVM).IEditTeam.TeamShiftPatternList

  Public Property CurrentTeamShiftPatternID As Integer?

#End Region

#Region " Year Requirements "

  'Public Property ROSystemYearList As ROSystemYearList

  Public Property ROSystemYearList As ROSystemYearList
  Public Property ROSystemYearListCriteria As ROSystemYearList.Criteria
  Public Property ROSystemYearListManager As Singular.Web.Data.PagedDataManager(Of TeamManagementVM)

  Public Property CurrentSystemYear As SystemYear

#End Region

#End Region

#Region "UI Properties"

  Public Property TeamManagementSelected As Boolean = False

  Public Property SystemAreaShiftPatternSelected As Boolean = False

  Public Property TeamNumbersSelected As Boolean = False

  Public Property NoTemplatesForSystem As Boolean = False

  Public Property SystemYearRequirementManagementSelected As Boolean = False

#End Region

#Region "User Properties "

  Public Property CurrentUserDepartmentID As Integer
  Public Property CurrentUserSystemID As Integer
  Public Property CurrentUserProductionAreaID As Integer

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    ' System Team List
    ROSystemTeamList = New OBLib.TeamManagement.ReadOnly.ROSystemTeamList
    ROSystemTeamListCriteria = New OBLib.TeamManagement.ReadOnly.ROSystemTeamList.Criteria
    ROSystemTeamListManager = New Singular.Web.Data.PagedDataManager(Of TeamManagementVM)(Function(d) d.ROSystemTeamList,
                                                                                                     Function(d) d.ROSystemTeamListCriteria,
                                                                                                    "ModifiedDateTime", 15, False)
    ROSystemTeamListManager.SingleSelect = True

    ' System Area Shift Pattern List
    SystemAreaShiftPatternList = New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList

    ROSystemAreaShiftPatternList = New OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList
    ROSystemAreaShiftPatternListCriteria = New OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList.Criteria
    ROSystemAreaShiftPatternListManager = New Singular.Web.Data.PagedDataManager(Of TeamManagementVM)(Function(d) d.ROSystemAreaShiftPatternList,
                                                                                                     Function(d) d.ROSystemAreaShiftPatternListCriteria,
                                                                                                    "PatternName", 15, True)
    ROSystemAreaShiftPatternListManager.SingleSelect = True
    ROSystemAreaShiftPatternListCriteria.FetchTemplates = True
    ROSystemAreaShiftPatternListCriteria.SystemID = CurrentUserSystemID

    ROSystemAreaShiftPatternListJustFetched = New OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList

    'User Variables
    CurrentUserDepartmentID = OBLib.Security.Settings.CurrentUser.DepartmentID
    CurrentUserSystemID = OBLib.Security.Settings.CurrentUser.SystemID
    CurrentSystemProductionAreaID = Nothing
    CurrentUserProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    'System Area Shift Patterns
    CurrentProductionAreaID = CurrentUserProductionAreaID
    CurrentEditableSystemTeamShiftPatternID = Nothing

    'User Systems
    ROUserSystemList = OBLib.Maintenance.General.ReadOnly.ROUserSystemList.GetROUserSystemList(OBLib.Security.Settings.CurrentUser.UserID)

    'Team Discipline Human Resources
    CurrentTeamDisciplineHumanResource = Nothing
    HRSelectedList = New List(Of Integer)

    'Team Shift Patterns
    CurrentTeamShiftPatternID = Nothing

    'Years
    ROSystemYearList = New ROSystemYearList
    ROSystemYearListCriteria = New ROSystemYearList.Criteria
    ROSystemYearListManager = New Singular.Web.Data.PagedDataManager(Of TeamManagementVM)(Function(d) d.ROSystemYearList,
                                                                                            Function(d) d.ROSystemYearListCriteria,
                                                                                            "YearStartDate", 10, False)

    'Team Numbers
    TeamNumberList = OBLib.TeamManagement.SystemTeamNumberList.GetSystemTeamNumberList

    'Client Side Lists
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROShiftPatternWeekDayList", OBLib.CommonData.Lists.ROShiftPatternWeekDayList, False)
    ClientDataProvider.AddDataSource("ROShiftDurationList", OBLib.CommonData.Lists.ROShiftDurationList, False)
    ClientDataProvider.AddDataSource("ROTeamNumberList", OBLib.CommonData.Lists.ROTeamNumberList, False)
    ClientDataProvider.AddDataSource("ROSystemAreaShiftTypeList", OBLib.CommonData.Lists.ROSystemAreaShiftTypeList, False)
    ClientDataProvider.AddDataSource("ROSystemAllowedAreaList", OBLib.CommonData.Lists.ROSystemAllowedAreaList, False)
    'ClientDataProvider.AddDataSource("ROProductionAreaAllowedDisciplineList", OBLib.CommonData.Lists.ROProductionAreaAllowedDisciplineList, False)
    ClientDataProvider.AddDataSource("ROSystemAreaShiftPatternList", ROSystemAreaShiftPatternList, False)
    'ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.CommonData.Lists.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaDisciplineList", OBLib.CommonData.Lists.ROSystemProductionAreaDisciplineList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    MessageFadeTime = 5000

    If OBLib.Security.Settings.CurrentUser.ROUserSystemList.Count = 1 Then
      CurrentSystemID = OBLib.Security.Settings.CurrentUser.ROUserSystemList(0).SystemID
      ROSystemTeamListCriteria.SystemID = CurrentSystemID
    Else
      CurrentSystemID = Nothing
      ROSystemTeamListCriteria.SystemID = Nothing
    End If

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "GetTeamShiftPatterns"
        GetTeamShiftPatterns(CommandArgs)

      Case "RemoveSystemAreaShiftPattern"
        RemoveSystemAreaShiftPattern(CommandArgs)

      Case "RemoveTDHR"
        RemoveTDHR(CommandArgs)

      Case "UpdateTeamHRList"
        UpdateTeamHRList(CommandArgs)

      Case "NewSystemTeam"
        CreateNewSystemTeam(CommandArgs)

      Case "RemoveTeam"
        RemoveTeam(CommandArgs)

      Case "CleanupNewteam"
        CleanupNewteam()

      Case "GetSystemProductionArea"
        GetSystemProductionArea(CommandArgs)

      Case "SaveNewTeam"
        SaveNewTeam(CommandArgs)

      Case "SaveSystemAreaShiftPatterns"
        SaveSystemAreaShiftPatterns(CommandArgs)

      Case "RemoveSASPAfterSave"
        RemoveSASPAfterSave()

      Case "RemoveSASPWeekDay"
        RemoveSASPWeekDay(CommandArgs)

        'Case "PopulateHRSchedule"
        '  PopulateHRSchedule(CommandArgs)

      Case "FetchSASPForEdit"
        FetchSASPForEdit(CommandArgs)

      Case "SaveTeamNumbers"
        SaveTeamNumbers(CommandArgs)

      Case "GetTeamNumbers"
        GetTeamNumbers(CommandArgs)

    End Select
  End Sub

#End Region

#Region " Methods "

#Region "Teams"

  Private Sub RemoveTeam(CommandArgs As Singular.Web.CommandArgs)
    Dim SystemTeamID As Integer = CommandArgs.ClientArgs.SystemTeamID
    Dim SystemTeams As OBLib.TeamManagement.SystemTeamList =
                       OBLib.TeamManagement.SystemTeamList.GetSystemTeamList(Nothing, CurrentSystemID)
    If SystemTeams IsNot Nothing Then
      Dim indexOfTeam As Integer = SystemTeams.ToList.FindIndex(Function(c) Singular.Misc.CompareSafe(c.SystemTeamID, SystemTeamID))
      If indexOfTeam > -1 Then
        SystemTeams.RemoveAt(indexOfTeam)
        SaveTeamList(SystemTeams)
      End If
    End If
  End Sub

  Private Sub CreateNewSystemTeam(CommandArgs As Singular.Web.CommandArgs)
    'SASPProductionAreaID = IIf(SASPProductionAreaID Is Nothing, CurrentUserProductionAreaID, SASPProductionAreaID)
    CurrentSystemProductionAreaID = OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList.GetROSystemProductionAreaList(CurrentSystemID, CurrentProductionAreaID)(0).SystemProductionAreaID
    EditableSystemTeamShiftPattern = New OBLib.TeamManagement.SystemTeamShiftPattern
    EditableSystemTeamShiftPattern.SystemID = CurrentUserSystemID
    HRSelectedList.Clear()
    CommandArgs.ReturnData = New Singular.Web.Result(True)
  End Sub

  Private Sub CleanupNewteam()
    If EditableSystemTeamShiftPattern IsNot Nothing Then
      EditableSystemTeamShiftPattern = Nothing
    End If
  End Sub

#End Region

#Region "System Area Shift Patterns"

  Private Sub FetchSASPForEdit(CommandArgs As Singular.Web.CommandArgs)
    Dim SystemAreaShiftPatternID = CommandArgs.ClientArgs.SystemAreaShiftPatternID
    EditableSystemAreaShiftPattern = New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern
    SystemAreaShiftPatternList = OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList.GetSystemAreaShiftPatternList(SystemAreaShiftPatternID, Nothing, Nothing, Nothing)
    EditableSystemAreaShiftPattern = SystemAreaShiftPatternList(0)
    If SystemAreaShiftPatternList IsNot Nothing Then
      CommandArgs.ReturnData = True
    Else
      CommandArgs.ReturnData = False
    End If
  End Sub

  Private Sub RemoveSystemAreaShiftPattern(CommandArgs As Singular.Web.CommandArgs)
    Dim SystemAreaShiftPatternID As Integer = CommandArgs.ClientArgs.SystemAreaShiftPatternID

    Dim SystemAreaShiftPatterns As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList = _
                   OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList.GetSystemAreaShiftPatternList(SystemAreaShiftPatternID, Nothing, Nothing, Nothing)
    If SystemAreaShiftPatterns IsNot Nothing Then
      Dim indexOfSASP As Integer = SystemAreaShiftPatterns.ToList.FindIndex(Function(c) Singular.Misc.CompareSafe(c.SystemAreaShiftPatternID, SystemAreaShiftPatternID))
      SystemAreaShiftPatterns.RemoveAt(indexOfSASP)
      SaveSystemAreaShiftPatterns(CommandArgs, SystemAreaShiftPatterns)
    End If
  End Sub

  Private Sub RemoveSASPAfterSave()
    If SystemAreaShiftPatternList IsNot Nothing Then
      SystemAreaShiftPatternList.Clear()
    End If
    HRSelectedList.Clear()
  End Sub

  Private Sub RemoveSASPWeekDay(CommandArgs As Singular.Web.CommandArgs)
    Dim SystemAreaShiftPatternWeekDayID As Integer = CommandArgs.ClientArgs.SystemAreaShiftPatternWeekDayID
    If EditableSystemAreaShiftPattern IsNot Nothing Then
      Dim indexOfSASPWD As Integer = EditableSystemAreaShiftPattern.SystemAreaShiftPatternWeekDayList.ToList.FindIndex(Function(c) Singular.Misc.CompareSafe(c.SystemAreaShiftPatternWeekDayID, SystemAreaShiftPatternWeekDayID))
      EditableSystemAreaShiftPattern.SystemAreaShiftPatternWeekDayList.RemoveAt(indexOfSASPWD)
    End If
  End Sub

#End Region

#Region "System Production Areas"

  Private Function GetSystemProductionArea(CommandArgs As Singular.Web.CommandArgs) As Singular.Web.Result
    'Create new System Area Shift Pattern
    'Retrieve System Production Area in order to save System Area Shift Pattern
    ROSystemProductionArea = OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList.GetROSystemProductionAreaList(CurrentSystemID, CurrentProductionAreaID)(0)
    If ROSystemProductionArea IsNot Nothing Then
      EditableSystemAreaShiftPattern = New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern
      EditableSystemAreaShiftPattern.SystemID = ROSystemProductionArea.SystemID
      'EditableSystemAreaShiftPattern.ProductionAreaID = ROSystemProductionArea.ProductionAreaID
      'EditableSystemAreaShiftPattern.SystemProductionAreaID = ROSystemProductionArea.SystemProductionAreaID
      ' Make the new System Area Shift Pattern a Template if being added from Manage Shift Patterns section
      EditableSystemAreaShiftPattern.IsTemplate = True
      SystemAreaShiftPatternList.Add(EditableSystemAreaShiftPattern)
      CommandArgs.ReturnData = New Singular.Web.Result(True) With {.Data = ROSystemProductionArea}
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "No System Production Area Found"}
    End If
  End Function

#End Region

#Region " Team Shift Patterns "

  Private Function GetTeamShiftPatterns(CommandArgs As Singular.Web.CommandArgs) As Singular.Web.Result
    Dim SystemTeamID As Integer? = CommandArgs.ClientArgs.SystemTeamID
    EditableSystemTeamShiftPattern = New OBLib.TeamManagement.SystemTeamShiftPattern
    EditableSystemTeamShiftPattern = OBLib.TeamManagement.SystemTeamShiftPatternList.GetSystemTeamShiftPatternList(SystemTeamID, CurrentSystemID).FirstOrDefault
    TeamShiftPatternList = OBLib.TeamManagement.TeamShiftPatternList.GetTeamShiftPatternList(Nothing, Nothing, SystemTeamID)
    HRSelectedList = New List(Of Integer)
    For Each HR In EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList
      HRSelectedList.Add(HR.HumanResourceID)
    Next
    If EditableSystemTeamShiftPattern IsNot Nothing Then
      CurrentEditableSystemTeamShiftPatternID = EditableSystemTeamShiftPattern.SystemAreaShiftPatternID
      CommandArgs.ReturnData = New Singular.Web.Result(True)
    Else
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = "No Team Found"}
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Private Function PopulateHRSchedule(TeamShiftPatternID As Integer, HumanResourceID As Integer?, SystemTeamID As Integer) As Singular.Web.Result
    Dim Rslt As Singular.Web.Result = New Singular.Web.Result(True)
    If EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Count > 0 Then
      Try
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdShiftsPopulate",
            New String() {
                         "@UserID",
                         "@TeamShiftPatternID",
                         "@HumanResourceIDForIndividual"
                         },
            New Object() {
                          OBLib.Security.Settings.CurrentUser.UserID,
                          TeamShiftPatternID,
                          Singular.Misc.NothingDBNull(HumanResourceID)
                         })
        cmd.FetchType = CommandProc.FetchTypes.DataSet
        cmd.CommandTimeout = 0
        cmd = cmd.Execute()
      Catch ex As Exception
        AddMessage(Singular.Web.MessageType.Error, "An Error Occured", ex.Message)
        Rslt.Success = False
        Rslt.ErrorText = ex.Message
        Rslt.Data = Nothing
      End Try
      EditableSystemTeamShiftPattern = OBLib.TeamManagement.SystemTeamShiftPatternList.GetSystemTeamShiftPatternList(SystemTeamID, Nothing).LastOrDefault
      Return Rslt
    Else
      Rslt.Success = False
      Rslt.ErrorText = "No HR to Generate For"
      Return Rslt
    End If
  End Function

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Public Function RemoveShifts(HumanResourceID As Integer?, StartDate As Date?, EndDate As Date?, TeamDisciplineHumanResourceID As Integer?) As Singular.Web.Result
    Dim Rslt As Singular.Web.Result = New Singular.Web.Result(True)
    If HumanResourceID IsNot Nothing AndAlso StartDate IsNot Nothing AndAlso EndDate IsNot Nothing Then
      Try
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdRemoveShifts",
            New String() {
                         "@UserID",
                         "@HumanResourceID",
                         "@StartDate",
                         "@EndDate",
                         "@TeamDisciplineHumanResourceID"
                         },
            New Object() {
                          OBLib.Security.Settings.CurrentUser.UserID,
                          Singular.Misc.NothingDBNull(HumanResourceID),
                          Singular.Misc.NothingDBNull(StartDate),
                          Singular.Misc.NothingDBNull(EndDate),
                          Singular.Misc.NothingDBNull(TeamDisciplineHumanResourceID)
                         })
        cmd.FetchType = CommandProc.FetchTypes.DataSet
        cmd.CommandTimeout = 0
        cmd = cmd.Execute()
      Catch ex As Exception
        AddMessage(Singular.Web.MessageType.Error, "An Error Occured", ex.Message)
        Rslt.Success = False
        Rslt.ErrorText = ex.Message
        Rslt.Data = Nothing
      End Try
    End If
    Return Rslt
  End Function

#End Region

#Region "Team Discipline Human Resources "

  Private Sub RemoveTDHR(CommandArgs As Singular.Web.CommandArgs)
    Dim TeamDisciplineHumanResourceID As Integer = CommandArgs.ClientArgs.TeamDisciplineHumanResourceID
    Try
      Dim TDHRToRemove As OBLib.TeamManagement.TeamDisciplineHumanResource = EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Where _
                                                              (Function(c) Singular.Misc.CompareSafe(c.TeamDisciplineHumanResourceID, TeamDisciplineHumanResourceID))(0)
      EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Remove(TDHRToRemove)
      HRSelectedList.Remove(TDHRToRemove.HumanResourceID)
    Catch ex As Exception
      AddMessage(Singular.Web.MessageType.Error, "An Error Occured", ex.Message)
    End Try
  End Sub

  Public Sub UpdateTeamHRList(CommandArgs As Singular.Web.CommandArgs)

    Dim HumanResourceID As Integer = CommandArgs.ClientArgs.HumanResourceID
    Dim HumanResource As String = CommandArgs.ClientArgs.HumanResource
    Dim DisciplineID As Integer = CommandArgs.ClientArgs.DisciplineID

    ' Used to store variables needed for GetDataStateless after HR saved on team 
    Dim TD As Tuple(Of Integer, Integer, Integer) = Nothing
    Dim TempDisciplineHumanResource As OBLib.TeamManagement.TeamDisciplineHumanResource = New OBLib.TeamManagement.TeamDisciplineHumanResource
    If CurrentTeamDisciplineHumanResource IsNot Nothing Then
      'Replace existing HR in the same modal open session (Remove and add)
      Try
        '  Dim HRToRemove As OBLib.Shifts.ICR.TeamDisciplineHumanResource = EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Where _
        '                                                          (Function(c) Singular.Misc.CompareSafe(c.HumanResourceID, CurrentTeamDisciplineHumanResource.HumanResourceID))(0)
        '  EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Remove(HRToRemove)
        '  HRSelectedList.Remove(HRToRemove.HumanResourceID)
        TempDisciplineHumanResource.HumanResourceID = HumanResourceID
        TempDisciplineHumanResource.HumanResource = HumanResource
        TempDisciplineHumanResource.DisciplineID = DisciplineID
        TempDisciplineHumanResource.StartDate = EditableSystemTeamShiftPattern.StartDate
        TempDisciplineHumanResource.EndDate = EditableSystemTeamShiftPattern.EndDate
        TempDisciplineHumanResource.CheckAllRules()
        EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Add(TempDisciplineHumanResource)
        HRSelectedList.Add(TempDisciplineHumanResource.HumanResourceID)
        CurrentTeamDisciplineHumanResource = TempDisciplineHumanResource
      Catch ex As Exception
        AddMessage(Singular.Web.MessageType.Error, "An Error Occured", ex.Message)
      End Try
    Else
      'Add New HR after modal opened
      Dim NewTDHR As OBLib.TeamManagement.TeamDisciplineHumanResource = EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Where(Function(c) Singular.Misc.CompareSafe(c.HumanResourceID, Nothing))(0)
      If NewTDHR IsNot Nothing Then
        NewTDHR.HumanResourceID = HumanResourceID
        NewTDHR.HumanResource = HumanResource
        NewTDHR.DisciplineID = DisciplineID
        NewTDHR.StartDate = EditableSystemTeamShiftPattern.StartDate
        NewTDHR.EndDate = EditableSystemTeamShiftPattern.EndDate
        NewTDHR.CheckAllRules()
        CurrentTeamDisciplineHumanResource = NewTDHR
        HRSelectedList.Add(CurrentTeamDisciplineHumanResource.HumanResourceID)
      Else
        ' Add new when HR with discipline has already been assigned team (Remove and Add)
        If TeamDisciplineHumanResourceToReplace IsNot Nothing Then
          Try
            Dim HRToRemove As OBLib.TeamManagement.TeamDisciplineHumanResource = EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Where _
                                                                                  (Function(c) Singular.Misc.CompareSafe(c.HumanResourceID, TeamDisciplineHumanResourceToReplace.HumanResourceID))(0)
            If HRToRemove IsNot Nothing Then
              EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Remove(HRToRemove)
              HRSelectedList.Remove(HRToRemove.HumanResourceID)
              TempDisciplineHumanResource.HumanResourceID = HumanResourceID
              TempDisciplineHumanResource.HumanResource = HumanResource
              TempDisciplineHumanResource.DisciplineID = DisciplineID
              TempDisciplineHumanResource.StartDate = EditableSystemTeamShiftPattern.StartDate
              TempDisciplineHumanResource.EndDate = EditableSystemTeamShiftPattern.EndDate
              TempDisciplineHumanResource.CheckAllRules()
              EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Add(TempDisciplineHumanResource)
              HRSelectedList.Add(TempDisciplineHumanResource.HumanResourceID)
            End If
          Catch ex As Exception
            AddMessage(Singular.Web.MessageType.Error, "An Error Occured", ex.Message)
          End Try
        End If
        CurrentTeamDisciplineHumanResource = TempDisciplineHumanResource
      End If
    End If
    TD = New Tuple(Of Integer, Integer, Integer)(DisciplineID, EditableSystemTeamShiftPattern.SystemTeamID, EditableSystemTeamShiftPattern.SystemID)
    CommandArgs.ReturnData = TD

  End Sub

#End Region

#Region "Save Methods"

#Region "New System Team Shift Pattern Object"

  Private Sub SaveNewTeam(CommandArgs As Singular.Web.CommandArgs)
    Dim SaveAndNew As Boolean = CommandArgs.ClientArgs.SaveAndNew
    If EditableSystemTeamShiftPattern.IsValid Then
      Dim mSystemTeamShiftPatternList As OBLib.TeamManagement.SystemTeamShiftPatternList = New OBLib.TeamManagement.SystemTeamShiftPatternList
      Dim mSystemTeamShiftPattern As OBLib.TeamManagement.SystemTeamShiftPattern = New OBLib.TeamManagement.SystemTeamShiftPattern
      Dim TempTeamDiscipllineHRList As OBLib.TeamManagement.TeamDisciplineHumanResourceList = New OBLib.TeamManagement.TeamDisciplineHumanResourceList
      Dim thisTeamID As Integer? = Nothing ' Reference to the System Team currently being saved / updated
      Dim thisTeamName As String = Nothing
      Dim thisStartDate As DateTime? = Nothing
      Dim thisEndDate As DateTime? = Nothing
      ' Store HR on System Team for later saving, need to first insert new system team
      TempTeamDiscipllineHRList.Clear()
      TempTeamDiscipllineHRList.AddRange(EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList)
      EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Clear()

      ' ---------- NEW SYSTEM TEAM INSERTION
      If Singular.Misc.CompareSafe(EditableSystemTeamShiftPattern.SystemTeamID, 0) Then
        'We know this is new / not yet saved
        ' Try save System Team
        mSystemTeamShiftPatternList.Add(EditableSystemTeamShiftPattern)
        Dim sh1 As SaveHelper = TrySave(mSystemTeamShiftPatternList)
        ' Save Attempt (System Team), chain linked (if System Teams fails nothing else will save)
        If Not sh1.Success Then
          'Error
          AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh1.ErrorText)
          CommandArgs.ReturnData = False
        Else
          ' System Team saved successfully
          mSystemTeamShiftPatternList = CType(sh1.SavedObject, OBLib.TeamManagement.SystemTeamShiftPatternList)
          EditableSystemTeamShiftPattern = mSystemTeamShiftPatternList(0)
          ' Create and save new System Area Shift Patterns and Week Days once we have a system team saved
          mSystemTeamShiftPattern = SaveSystemAreaShiftPatternsForCombinedObject()
          ' Recover the System Area Shift Pattern Details just saved for this team
          EditableSystemTeamShiftPattern = OBLib.TeamManagement.SystemTeamShiftPatternList.GetSystemTeamShiftPatternList(Nothing, Nothing).Last
          ' Change the ID from the template selected to the one just saved
          EditableSystemTeamShiftPattern.SystemID = mSystemTeamShiftPattern.SystemID
          EditableSystemTeamShiftPattern.SystemAreaShiftPatternID = mSystemTeamShiftPattern.SystemAreaShiftPatternID
          EditableSystemTeamShiftPattern.StartDate = mSystemTeamShiftPattern.StartDate
          EditableSystemTeamShiftPattern.EndDate = mSystemTeamShiftPattern.EndDate
          EditableSystemTeamShiftPattern.PatternName = mSystemTeamShiftPattern.PatternName
          EditableSystemTeamShiftPattern.ShiftDuration = mSystemTeamShiftPattern.ShiftDuration
          ' Once we know what the System Team ID is, store it as a reference for fetching info on EditableSystemTeamShiftPattern if need be
          thisTeamID = EditableSystemTeamShiftPattern.SystemTeamID
          ' Add System Team Shift Pattern
          mSystemTeamShiftPatternList.Clear()
          EditableSystemTeamShiftPattern.CheckAllRules()
          mSystemTeamShiftPatternList.Add(EditableSystemTeamShiftPattern)
          Dim sh3 As SaveHelper = TrySave(mSystemTeamShiftPatternList)
          ' Create Team Shift Pattern on Update of Custom object, System Area Shift Pattern already saved, the ID of which is sent to the ins proc for
          ' Team Shift Patterns
          If Not sh3.Success Then
            'Error
            AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh3.ErrorText)
            CommandArgs.ReturnData = False
          Else
            ' Created new Team Shift Pattern linking System Team and System Area Shift Pattern
            mSystemTeamShiftPatternList = CType(sh3.SavedObject, OBLib.TeamManagement.SystemTeamShiftPatternList)
            ' Update EditableSystemTeamShiftPattern after save 
            EditableSystemTeamShiftPattern = mSystemTeamShiftPatternList(0)
            EditableSystemTeamShiftPattern.SystemTeamID = thisTeamID
            EditableSystemTeamShiftPattern.SystemID = mSystemTeamShiftPattern.SystemID
            EditableSystemTeamShiftPattern.SystemAreaShiftPatternID = mSystemTeamShiftPattern.SystemAreaShiftPatternID
            EditableSystemTeamShiftPattern.StartDate = mSystemTeamShiftPattern.StartDate
            EditableSystemTeamShiftPattern.EndDate = mSystemTeamShiftPattern.EndDate
            EditableSystemTeamShiftPattern.PatternName = mSystemTeamShiftPattern.PatternName
            EditableSystemTeamShiftPattern.ShiftDuration = mSystemTeamShiftPattern.ShiftDuration
            EditableSystemTeamShiftPattern.SystemID = mSystemTeamShiftPattern.SystemID
            ' Now add Team Discipline Human Resources to save at the end
            EditableSystemTeamShiftPattern.TeamDisciplineHumanResourceList.AddRange(TempTeamDiscipllineHRList)
            mSystemTeamShiftPatternList.Clear()
            mSystemTeamShiftPatternList.Add(EditableSystemTeamShiftPattern)
            ' Try save System Team Discipline Human Resources
            Dim sh2 As SaveHelper = TrySave(mSystemTeamShiftPatternList)
            If Not sh2.Success Then
              'Error
              AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh2.ErrorText)
              CommandArgs.ReturnData = False
            Else
              ' Team Discipline Human Resources saved successfully
              mSystemTeamShiftPatternList = CType(sh2.SavedObject, OBLib.TeamManagement.SystemTeamShiftPatternList)
              ' Fetch an updated version of the custom object
              EditableSystemTeamShiftPattern = OBLib.TeamManagement.SystemTeamShiftPatternList.GetSystemTeamShiftPatternList(thisTeamID, Nothing)(0)
            End If
          End If
        End If
      Else
        '-------------- UPDATE ------------
        thisTeamName = EditableSystemTeamShiftPattern.TeamName
        thisStartDate = EditableSystemTeamShiftPattern.StartDate
        thisEndDate = EditableSystemTeamShiftPattern.EndDate
        ' System Team already exists, so create / update the associated System Area Shift Pattern
        mSystemTeamShiftPattern = SaveSystemAreaShiftPatternsForCombinedObject()
        thisTeamID = mSystemTeamShiftPattern.SystemTeamID
        mSystemTeamShiftPattern.TeamName = thisTeamName
        mSystemTeamShiftPattern.StartDate = thisStartDate
        mSystemTeamShiftPattern.EndDate = thisEndDate
        mSystemTeamShiftPatternList.Add(mSystemTeamShiftPattern)
        Dim sh3 As SaveHelper = TrySave(mSystemTeamShiftPatternList)
        ' Save Attempt (System Team and Team Detail Human Resources)
        If Not sh3.Success Then
          'Error
          AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh3.ErrorText)
          CommandArgs.ReturnData = False
        Else
          mSystemTeamShiftPatternList = CType(sh3.SavedObject, OBLib.TeamManagement.SystemTeamShiftPatternList)
          mSystemTeamShiftPattern = mSystemTeamShiftPatternList(0)
          mSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Clear()
          For Each hr In TempTeamDiscipllineHRList
            If Not hr.IsNew Then
              ' Add and remove to save items that are not New or Dirty
              mSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Add(hr)
              mSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Remove(hr)
              mSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Add(hr)
            Else
              mSystemTeamShiftPattern.TeamDisciplineHumanResourceList.Add(hr)
            End If
          Next
          mSystemTeamShiftPatternList.Clear()
          mSystemTeamShiftPatternList.Add(mSystemTeamShiftPattern)
          Dim sh5 As SaveHelper = TrySave(mSystemTeamShiftPatternList)
          ' Save Attempt (System Team and Team Detail Human Resources)
          If Not sh5.Success Then
            'Error
            AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh5.ErrorText)
            CommandArgs.ReturnData = False
          Else
            mSystemTeamShiftPatternList = CType(sh5.SavedObject, OBLib.TeamManagement.SystemTeamShiftPatternList)
            EditableSystemTeamShiftPattern = OBLib.TeamManagement.SystemTeamShiftPatternList.GetSystemTeamShiftPatternList(thisTeamID, Nothing)(0)
          End If
        End If
      End If
      If SaveAndNew Then
        EditableSystemTeamShiftPattern = Nothing
        CreateNewSystemTeam(CommandArgs)
      End If
      TemplateUpDate = False
      CommandArgs.ReturnData = True
    Else
      CommandArgs.ReturnData = False
    End If
  End Sub
#End Region

#Region "Pattern History and Team Details"

  Private Sub SaveTeamList(TeamList As OBLib.TeamManagement.SystemTeamList)
    If TeamList IsNot Nothing Then
      Dim sh As SaveHelper = TrySave(TeamList)
      If Not sh.Success Then
        'Error
        AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
      Else
        'Success
        TeamList = CType(sh.SavedObject, OBLib.TeamManagement.SystemTeamList)
      End If
    End If
  End Sub

  Private Function SaveSystemAreaShiftPatternsForCombinedObject() As OBLib.TeamManagement.SystemTeamShiftPattern
    'If Not TemplateUpDate Then
    Dim mSystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList = New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList
    ' If a Team Shift Pattern exists and a Template System Area Shift Pattern exists
    Dim tempSystemAreaShiftPattern As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern = Nothing
    If Not Singular.Misc.CompareSafe(EditableSystemTeamShiftPattern.TeamShiftPatternID, 0) _
      AndAlso CurrentEditableSystemTeamShiftPatternID IsNot Nothing Then
      'Team Shift Pattern Already generated, so we know this is an update of an existing System Area Shift Pattern
      tempSystemAreaShiftPattern = OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList.GetSystemAreaShiftPatternList(CurrentEditableSystemTeamShiftPatternID, Nothing, Nothing, Nothing)(0)
      If tempSystemAreaShiftPattern IsNot Nothing Then
        If Not tempSystemAreaShiftPattern.IsTemplate Then 'Make sure we are not going to edit a template
          mSystemAreaShiftPatternList.Add(tempSystemAreaShiftPattern)
        End If
      End If
    Else
      ' Add new System Area Shift Pattern for new Team Shift Pattern
      mSystemAreaShiftPatternList.Add(New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern With {.DisciplineID = Nothing,
                                                                                                       .PatternName = EditableSystemTeamShiftPattern.PatternName,
                                                                                                       .ShiftDuration = EditableSystemTeamShiftPattern.ShiftDuration,
                                                                                                       .SystemID = EditableSystemTeamShiftPattern.SystemID,
                                                                                                       .ProductionAreaID = CurrentUserProductionAreaID})
    End If

    If TemplateUpDate Then
      ' If changing from an ad hoc pattern to a defined template, make sure the week days are cleaned before continuing
      mSystemAreaShiftPatternList(0).SystemAreaShiftPatternWeekDayList.Clear()
      Dim sh7 As SaveHelper = TrySave(mSystemAreaShiftPatternList)
      If Not sh7.Success Then
        'Error
        AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh7.ErrorText)
      Else
        'Success
        mSystemAreaShiftPatternList = CType(sh7.SavedObject, OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList)
      End If
    End If

    If mSystemAreaShiftPatternList(0).SystemAreaShiftPatternWeekDayList.Count > EditableSystemTeamShiftPattern.SystemAreaShiftPatternWeekDayList.Count Then
      'Removing days if editable object has fewer days than object fetched
      For Each SASPWD In mSystemAreaShiftPatternList(0).SystemAreaShiftPatternWeekDayList.ToList
        If EditableSystemTeamShiftPattern.SystemAreaShiftPatternWeekDayList.IndexOf(SASPWD) = -1 Then
          mSystemAreaShiftPatternList(0).SystemAreaShiftPatternWeekDayList.Remove(SASPWD)
        End If
      Next
    Else
      ' Adding Days or staying the same
      Dim Days As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList = New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList
      Days.AddRange(EditableSystemTeamShiftPattern.SystemAreaShiftPatternWeekDayList)
      mSystemAreaShiftPatternList(0).SystemAreaShiftPatternWeekDayList.AddRange(Days)
      EditableSystemTeamShiftPattern.SystemAreaShiftPatternWeekDayList.Clear()
    End If

    ' Save System Area Shift Pattern after updates
    mSystemAreaShiftPatternList(0).PatternName = EditableSystemTeamShiftPattern.PatternName
    mSystemAreaShiftPatternList(0).ShiftDuration = EditableSystemTeamShiftPattern.ShiftDuration

    Dim sh4 As SaveHelper = TrySave(mSystemAreaShiftPatternList)
    If Not sh4.Success Then
      'Error
      AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh4.ErrorText)
    Else
      'Success
      mSystemAreaShiftPatternList = CType(sh4.SavedObject, OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList)
    End If
    If Not Singular.Misc.CompareSafe(EditableSystemTeamShiftPattern.TeamShiftPatternID, 0) Then
      'Get updated System Team Shift Pattern
      EditableSystemTeamShiftPattern = OBLib.TeamManagement.SystemTeamShiftPatternList.GetSystemTeamShiftPatternList(EditableSystemTeamShiftPattern.SystemTeamID, Nothing)(0)
    End If
    ' Not saving the Template ID, so assign to new System Area Shift Pattern ID
    EditableSystemTeamShiftPattern.SystemAreaShiftPatternID = mSystemAreaShiftPatternList(0).SystemAreaShiftPatternID
    CurrentEditableSystemTeamShiftPatternID = EditableSystemTeamShiftPattern.SystemAreaShiftPatternID
    ' End If
    Return EditableSystemTeamShiftPattern
  End Function

#End Region

#Region "Shift Patterns"

  Private Function SaveSystemAreaShiftPatterns(CommandArgs As Singular.Web.CommandArgs, Optional SystemAreaShiftPatterns As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList = Nothing) As Singular.Web.Result
    Dim SaveAndNew As Boolean = CommandArgs.ClientArgs.SaveAndNew
    Dim WebResult As Singular.Web.Result = New Singular.Web.Result
    If SystemAreaShiftPatterns IsNot Nothing Then
      ' Add a single Shift Pattern
      Dim sh As SaveHelper = TrySave(SystemAreaShiftPatterns)
      If sh.Success Then
        SystemAreaShiftPatterns = sh.SavedObject
        ' Clean for new System Area Shift Pattern to be added
        If SaveAndNew Then
          EditableSystemAreaShiftPattern = Nothing
          Return GetSystemProductionArea(CommandArgs)
        Else
          WebResult = New Singular.Web.Result(True) With {.Success = True}
        End If
      Else
        WebResult = New Singular.Web.Result(False) With {.Success = False, .ErrorText = sh.ErrorText}
      End If
    Else
      ' Save the list
      If SystemAreaShiftPatternList.IsValid Then
        Dim sh As SaveHelper = TrySave(SystemAreaShiftPatternList)
        If sh.Success Then
          SystemAreaShiftPatternList = sh.SavedObject
          SystemAreaShiftPatternList = New OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList
          If SaveAndNew Then
            ' Setup for new System Area Shift Pattern
            EditableSystemAreaShiftPattern = Nothing
            Return GetSystemProductionArea(CommandArgs)
          Else
            WebResult = New Singular.Web.Result(True) With {.Success = True}
          End If
        Else
          WebResult = New Singular.Web.Result(False) With {.Success = False, .ErrorText = sh.ErrorText}
        End If
      Else
        ' Invalid System Area Shift Pattern, something went awry
        Return New Singular.Web.Result(False) With {.Success = False, .ErrorText = SystemAreaShiftPatternList.GetErrorsAsHTMLString}
      End If
    End If
    CommandArgs.ReturnData = WebResult
  End Function

#End Region

#Region "Team Numbers"

  Private Sub GetTeamNumbers(CommandArgs As Singular.Web.CommandArgs)
    Try
      TeamNumberList = OBLib.TeamManagement.SystemTeamNumberList.GetSystemTeamNumberList(Me.CurrentSystemID)
    Catch ex As Exception
      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    End Try
  End Sub

  Private Sub SaveTeamNumbers(CommandArgs As Singular.Web.CommandArgs)
    If TeamNumberList IsNot Nothing AndAlso TeamNumberList.IsDirty Then
      Try
        Dim sh As SaveHelper = TrySave(TeamNumberList)
        If Not sh.Success Then
          'Error
          AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
          CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
        Else
          'Success
          CommandArgs.ReturnData = New Singular.Web.Result(True)
          TeamNumberList = CType(sh.SavedObject, OBLib.TeamManagement.SystemTeamNumberList)
        End If
      Catch ex As Exception
        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    End If
  End Sub

#End Region

#End Region

#End Region

  Protected Overrides Sub AddBusinessRules()
    'MyBase.AddBusinessRules()
  End Sub

End Class