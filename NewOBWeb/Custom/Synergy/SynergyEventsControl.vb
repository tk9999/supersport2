﻿Imports Singular.Web
Imports OBLib.Synergy.ReadOnly

Namespace Controls

  Public Class BySeriesModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("BySeriesModal", "Series", , "modal-xl",
                                    BootstrapEnums.Style.Primary, , "fa-series", , )
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Synergy.SeriesItem)("ViewModel.CurrentSeriesItem()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d) d.DefaultEquipmentID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.DefaultEquipmentID, BootstrapEnums.InputSize.Small,, "Circuit")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.DivC("list-group")
                  With .Helpers.ForEach(Of OBLib.Equipment.EquipmentScheduleBasic)("$data.EquipmentScheduleBasicList()")
                    With .Helpers.HTMLTag("a")
                      .AddClass("list-group-item")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.HTMLTag("h5")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.Title()")
                          End With
                          With .Helpers.DivC("series-event-detail")
                            .AddBinding(KnockoutBindingString.html, "$data.GenRefNumber()")
                          End With
                          With .Helpers.DivC("series-event-detail")
                            .AddBinding(KnockoutBindingString.html, "new Date($data.StartTime()).format('ddd dd MMM HH:mm')")
                          End With
                          'With .Helpers.ForEach(Of OBLib.Synergy.ScheduleItem)("$data.ScheduleItemList()")

                          'End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 8, 7)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.Bootstrap.Row
                            .AddClass("event-circuit-container")
                            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 3)
                              .AddClass("col-xl-offset-1 col-lg-offset-8 col-md-offset-6 col-sm-offset-6")
                              .Helpers.Bootstrap.LabelFor(Function(d) d.EquipmentID).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentID, BootstrapEnums.InputSize.Small,, "Circuit")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('EquipmentID', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.CallTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.CallTime, BootstrapEnums.InputSize.Small,, "Start Buffer")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('CallTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.StartTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartTime, BootstrapEnums.InputSize.Small,, "Start Time")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('StartDateTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.EndTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndTime, BootstrapEnums.InputSize.Small,, "End Time")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('EndDateTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.WrapTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.WrapTime, BootstrapEnums.InputSize.Small,, "End Buffer")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('WrapTime', $data)")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 1)
                          .AddClass("padding-top-10")
                          With .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin fa-3x")
                            .IconContainer.AddBinding(KnockoutBindingString.visible, "EquipmentScheduleBasicBO.canView('IsProcessingIcon', $data)")
                          End With
                          With .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-triangle fa-3x fa-red")
                            .IconContainer.AddBinding(KnockoutBindingString.visible, "EquipmentScheduleBasicBO.canView('IsInvalidIcon', $data)")
                            .IconContainer.AddBinding(KnockoutBindingString.title, "$data.Clashes()")
                          End With
                          With .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-circle fa-3x fa-red")
                            .IconContainer.AddBinding(KnockoutBindingString.visible, "EquipmentScheduleBasicBO.canView('ImportFailedIcon', $data)")
                            '.IconContainer.AddBinding(KnockoutBindingString.title, "$data.Clashes()")
                          End With
                          With .Helpers.Bootstrap.Button(, "Create Feed", BootstrapEnums.Style.DefaultStyle,, BootstrapEnums.ButtonSize.Small,
                                                           , "fa-floppy-o",,, "EquipmentScheduleBasicBO.createFeed($data)",)
                            .Button.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('CreateFeedButton', $data)")
                            .Button.AddBinding(KnockoutBindingString.visible, "EquipmentScheduleBasicBO.canView('CreateFeedButton', $data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Synergy.SeriesItem)("ViewModel.CurrentSeriesItem()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Import All", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Medium,
                                             , "fa-floppy-o", , PostBackType.None, "SeriesItemBO.saveAll($data)", )
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class ByEventModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("ByEventModal", "By Event", , "modal-xl", BootstrapEnums.Style.Primary, , "fa-event", , )
        With .Body
          .AddClass("modal-background-gray")
          'Equipment Importing
          With .Helpers.With(Of OBLib.Synergy.ByEventTemplate)("ViewModel.CurrentByEventTemplate()")
            'With .Helpers.Bootstrap.Row
            '  .AddBinding(KnockoutBindingString.if, "$data.Mode() == 'Equipment'")
            '  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            '      .Helpers.Bootstrap.LabelFor(Function(d) d.DefaultEquipmentID)
            '      With .Helpers.Bootstrap.FormControlFor(Function(d) d.DefaultEquipmentID, BootstrapEnums.InputSize.Small, , "Circuit")
            '      End With
            '    End With
            '  End With
            'End With
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.if, "$data.Mode() == 'Equipment'")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.DivC("list-group")
                  With .Helpers.ForEach(Of OBLib.Equipment.EquipmentScheduleBasic)("$data.EquipmentScheduleBasicList()")
                    With .Helpers.HTMLTag("a")
                      .AddClass("list-group-item")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 6, 4, 2, 2)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.GenreDesc()")
                          End With
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.SeriesTitle()")
                          End With
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.Title()")
                          End With
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.GenRefNumber().toString()")
                          End With
                        End With
                        'With .Helpers.Bootstrap.Column(12, 6, 2, 1, 1)
                        '  With .Helpers.DivC("series-event-detail")
                        '    .AddBinding(KnockoutBindingString.html, "RoomScheduleBO.startTimeString($data)")
                        '  End With
                        '  With .Helpers.DivC("series-event-detail")
                        '    .AddBinding(KnockoutBindingString.html, "$data.GenRefNumber()")
                        '  End With
                        'End With
                        With .Helpers.Bootstrap.Column(12, 12, 10, 6, 7)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.Bootstrap.Row
                            .AddClass("event-circuit-container")
                            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.EquipmentID).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.EquipmentID, BootstrapEnums.InputSize.Small,, "Circuit")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('EquipmentID', $data)")
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            .AddClass("event-circuit-container")
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.CallTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.CallTime, BootstrapEnums.InputSize.Small,, "Start Buffer")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('CallTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.StartTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartTime, BootstrapEnums.InputSize.Small,, "Start Time")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('StartTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.EndTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndTime, BootstrapEnums.InputSize.Small,, "End Time")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('EndTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.WrapTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.WrapTime, BootstrapEnums.InputSize.Small,, "End Buffer")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentScheduleBasicBO.canEdit('WrapTime', $data)")
                              End With
                            End With
                          End With
                        End With
                        'With .Helpers.Bootstrap.Column(12, 12, 2, 2, 1)
                        '  .AddClass("padding-top-10")
                        '  With .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin fa-3x")
                        '    .IconContainer.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('IsProcessingIcon', $data)")
                        '  End With
                        '  With .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-triangle fa-3x fa-red")
                        '    .IconContainer.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('IsInvalidIcon', $data)")
                        '    .IconContainer.AddBinding(KnockoutBindingString.title, "$data.Clashes()")
                        '  End With
                        '  With .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-circle fa-3x fa-red")
                        '    .IconContainer.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('ImportFailedIcon', $data)")
                        '    .IconContainer.AddBinding(KnockoutBindingString.title, "$data.Clashes()")
                        '  End With
                        '  With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.DefaultStyle,, BootstrapEnums.ButtonSize.Small,
                        '                                   , "fa-floppy-o",,, "ByEventTemplateBO.saveRoomSchedule($data)",)
                        '    .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('ImportButton', $data)")
                        '    .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('ImportButton', $data)")
                        '  End With
                        'End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          'Room Importing
          With .Helpers.With(Of OBLib.Synergy.ByEventTemplate)("ViewModel.CurrentByEventTemplate()")
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.if, "$data.Mode() == 'Room'")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d) d.DefaultRoomID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.DefaultRoomID, BootstrapEnums.InputSize.Small, , "Circuit")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.if, "$data.Mode() == 'Room'")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.DivC("list-group")
                  With .Helpers.ForEach(Of OBLib.Scheduling.Rooms.RoomSchedule)("$data.RoomScheduleList()")
                    With .Helpers.HTMLTag("a")
                      .AddClass("list-group-item")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 6, 4, 2, 2)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.GenreDesc()")
                          End With
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.SeriesTitle()")
                          End With
                          With .Helpers.HTMLTag("h4")
                            .AddClass("list-group-item-heading")
                            .AddBinding(KnockoutBindingString.html, "$data.ProductionTitle()")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 6, 2, 1, 1)
                          With .Helpers.DivC("series-event-detail")
                            .AddBinding(KnockoutBindingString.html, "RoomScheduleBO.startTimeString($data)")
                          End With
                          With .Helpers.DivC("series-event-detail")
                            .AddBinding(KnockoutBindingString.html, "$data.GenRefNumber()")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 2, 1)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.ForEach(Of OBLib.Synergy.SynergySchedule)("$data.RoomScheduleScheduleNumberList()")
                            With .Helpers.HTMLTag("span")
                              .AddClass("badge badge-info")
                              .AddBinding(KnockoutBindingString.html, "$data.ChannelShortName()")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 10, 6, 7)
                          .AddClass("border-right-darkGray border-bottom-darkGray")
                          With .Helpers.Bootstrap.Row
                            .AddClass("event-circuit-container")
                            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.RoomID).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.RoomID, BootstrapEnums.InputSize.Small,, "Circuit")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('RoomID', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.SystemID).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.SystemID, BootstrapEnums.InputSize.Small,, "Circuit")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('SystemID', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.ProductionAreaID).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionAreaID, BootstrapEnums.InputSize.Small,, "Circuit")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('ProductionAreaID', $data)")
                              End With
                            End With
                          End With
                          With .Helpers.Bootstrap.Row
                            .AddClass("event-circuit-container")
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.CallTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.CallTime, BootstrapEnums.InputSize.Small,, "Start Buffer")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('CallTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.StartTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartTime, BootstrapEnums.InputSize.Small,, "Start Time")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('StartTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.EndTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndTime, BootstrapEnums.InputSize.Small,, "End Time")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('EndTime', $data)")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                              .Helpers.Bootstrap.LabelFor(Function(d) d.WrapTime).Style.Width = "100%"
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.WrapTime, BootstrapEnums.InputSize.Small,, "End Buffer")
                                .Editor.AddClass("eventItem-circuit margin-5")
                                .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('WrapTime', $data)")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 2, 2, 1)
                          .AddClass("padding-top-10")
                          With .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin fa-3x")
                            .IconContainer.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('IsProcessingIcon', $data)")
                          End With
                          With .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-triangle fa-3x fa-red")
                            .IconContainer.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('IsInvalidIcon', $data)")
                            .IconContainer.AddBinding(KnockoutBindingString.title, "$data.Clashes()")
                          End With
                          With .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-circle fa-3x fa-red")
                            .IconContainer.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('ImportFailedIcon', $data)")
                            .IconContainer.AddBinding(KnockoutBindingString.title, "$data.Clashes()")
                          End With
                          With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.DefaultStyle,, BootstrapEnums.ButtonSize.Small,
                                                           , "fa-floppy-o",,, "ByEventTemplateBO.saveRoomSchedule($data)",)
                            .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleBO.canEdit('ImportButton', $data)")
                            .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleBO.canView('ImportButton', $data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Synergy.ByEventTemplate)("ViewModel.CurrentByEventTemplate()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Import All", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Medium,
                                             , "fa-floppy-o", , PostBackType.None, "EventItemBO.saveAll($data)", )
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace