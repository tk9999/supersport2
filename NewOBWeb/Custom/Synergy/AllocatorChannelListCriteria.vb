﻿Imports Singular.Web

Namespace Controls

  Public Class AllocatorChannelListCriteriaControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of OBLib.Rooms.AllocatorChannelList.Criteria)("ViewModel.AllocatorChannelCriteria()")
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.SystemID).Style.Width = "100%"
          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelList.Criteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.StartDate).Style.Width = "100%"
          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelList.Criteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.EndDate).Style.Width = "100%"
          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.AllocatorChannelList.Criteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Genre)
          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Genre, Singular.Web.BootstrapEnums.InputSize.Small, , "Genre")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Series)
          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Series, Singular.Web.BootstrapEnums.InputSize.Small, , "Series")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.GenRefNoString)
          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.GenRefNoString, Singular.Web.BootstrapEnums.InputSize.Small, , "Gen Ref")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Title)
          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Title, Singular.Web.BootstrapEnums.InputSize.Small, , "Title")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelDisplay("Broadcast Status").Style.Width = "100%"
          With .Helpers.DivC("btn-group")
            .Style.Width = "100%"
            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Live, "Live", "Live", , , , , "btn-xs")
            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Delayed, "Delayed", "Delayed", , , , , "btn-xs")
            .Helpers.Bootstrap.StateButton(Function(p As OBLib.Rooms.AllocatorChannelList.Criteria) p.Premier, "Premier", "Premier", , , , , "btn-xs")
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Button(, "Apply Filters", BootstrapEnums.Style.Primary,
                                         , BootstrapEnums.ButtonSize.Small, ,
                                         "fa-download", , PostBackType.Ajax,
                                         "RoomAllocatorPage.refreshActiveTab()")
            .Button.AddClass("btn-block")
            '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
