﻿Imports OBLib.Invoicing.ReadOnly
Imports OBLib.Timesheets
Imports OBLib.Invoicing.New
Imports Singular.Web

Namespace Controls

  Public Class ROPaymentRunSelector(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).ROPaymentRunSelector)

    Private mModalID As String = ""
    Private mOnRowSelected As String = ""

    Public Sub New(Optional ModalID As String = "SelectPaymentRun", Optional OnRowSelect As String = "")
      mModalID = ModalID
      mOnRowSelected = OnRowSelect
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Select Payment Run", ,
                                    "modal-xs", Singular.Web.BootstrapEnums.Style.Primary, , "fa-search", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.FlatBlock(, , True, )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of ROPaymentRun)("ViewModel.ROPaymentRunListManager",
                                                                      "ViewModel.ROPaymentRunList()",
                                                                      False, False, False, False, True, True, True, ,
                                                                      Singular.Web.BootstrapEnums.PagerPosition.Bottom, mOnRowSelected,
                                                                      False, True)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y no-border-x"
                  With .FirstRow
                    .AddClass("items selectable")
                    With .AddReadOnlyColumn(Function(c As ROPaymentRun) c.System)
                      .Style.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(c As ROPaymentRun) c.MonthYearString)
                      .Style.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(c As ROPaymentRun) c.StartDate)
                      .Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                    With .AddReadOnlyColumn(Function(c As ROPaymentRun) c.EndDate)
                      .Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                    With .AddColumn("Status")
                      With .Helpers.Bootstrap.Button(, "Open", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-folder-open-o", , Singular.Web.PostBackType.None, )
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROPaymentRun) c.Status = "Open")
                      End With
                      With .Helpers.Bootstrap.Button(, "Closed", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-folder-o", , Singular.Web.PostBackType.None, )
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROPaymentRun) c.Status = "Closed")
                      End With
                      With .Helpers.Bootstrap.Button(, "Completed", Singular.Web.BootstrapEnums.Style.Success, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-check-square-o", , Singular.Web.PostBackType.None, )
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As ROPaymentRun) c.Status = "Completed")
                      End With
                    End With
                  End With
                  'If mOnRowSelected <> "" Then
                  '    .FirstRow.AddBinding(Singular.Web.KnockoutBindingString.click, mOnRowSelected)
                  'End If
                End With
              End With
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class NewPaymentRunModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = "NewPaymentRun"
    Private mNewPaymentRunBinding As String = "ViewModel.CurrentPaymentRun()"
    Private mCancelCreatePaymentRunFunction As String = "PaymentRunPage.cancelNewPaymentRun()"
    Private mCreatePaymentRunFunction As String = "PaymentRunPage.saveNewPaymentRun()"

    Public Sub New(Optional NewPaymentRunBinding As String = "ViewModel.CurrentPaymentRun()",
                   Optional ModalID As String = "NewPaymentRun",
                   Optional CancelCreatePaymentRunFunction As String = "PaymentRunPage.cancelNewPaymentRun()",
                   Optional CreatePaymentRunFunction As String = "PaymentRunPage.saveNewPaymentRun()")
      mNewPaymentRunBinding = NewPaymentRunBinding
      mModalID = ModalID
      mCancelCreatePaymentRunFunction = CancelCreatePaymentRunFunction
      mCreatePaymentRunFunction = CreatePaymentRunFunction

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "New Payment Run", ,
                                    "modal-xs", Singular.Web.BootstrapEnums.Style.Primary, , "fa-search", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Invoicing.[New].PaymentRun)(mNewPaymentRunBinding)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Details", , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.SystemID)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.SystemID, BootstrapEnums.InputSize.Small, , "Year")
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.Month)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.Month, BootstrapEnums.InputSize.Small, , "Month")
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.New.PaymentRun) d.Year)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.Year, BootstrapEnums.InputSize.Small, , "Year")
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.StartDate)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.StartDate, BootstrapEnums.InputSize.Small, , "Start Date")
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.EndDate)
                        .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.EndDate, BootstrapEnums.InputSize.Small, , "End Date")
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.PaymentRunStatusID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.[New].PaymentRun) d.PaymentRunStatusID, BootstrapEnums.InputSize.Small, , "Status")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "PaymentRunBO.CanEdit('PaymentRunStatusID', $data)")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Messages", , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row

                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of PaymentRun)(mNewPaymentRunBinding)
            With .Helpers.DivC("pull-left")
              With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
                                             PostBackType.None, mCancelCreatePaymentRunFunction, False)
              End With
            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Create", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                             PostBackType.None, mCreatePaymentRunFunction, True)
                .Button.AddBinding(KnockoutBindingString.enable, mNewPaymentRunBinding & ".IsValid()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class ROCreditorInvoiceList(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROCreditorInvoices)

    Private mModalID As String = ""
    Private mOnRowSelected As String = ""
    Private mOnButtonSelect As String = ""
    Private mDelayedRefresh As String = ""

    Public Sub New(Optional OnRowSelect As String = "",
                   Optional OnButtonSelect As String = "",
                   Optional DelayedRefresh As String = "")
      mOnRowSelected = OnRowSelect
      mOnButtonSelect = OnButtonSelect
      mDelayedRefresh = DelayedRefresh
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.With(Of OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria)("ViewModel.ROCreditorInvoiceListCriteria()")
            With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria) d.FirstName,
                                                     Singular.Web.BootstrapEnums.InputSize.Small, , "First Name...")
                If mDelayedRefresh <> "" Then
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(KnockoutBindingString.event, "{ keyup: " & mDelayedRefresh & "}")
                End If
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria) d.PreferredName,
                                                     Singular.Web.BootstrapEnums.InputSize.Small, , "Preferred Name...")
                If mDelayedRefresh <> "" Then
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(KnockoutBindingString.event, "{ keyup: " & mDelayedRefresh & "}")
                End If
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria) d.Surname,
                                                     Singular.Web.BootstrapEnums.InputSize.Small, , "Surname...")
                If mDelayedRefresh <> "" Then
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(KnockoutBindingString.event, "{ keyup: " & mDelayedRefresh & "}")
                End If
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 1)
              .AddClass("col-xl-offset-4 col-lg-offset-4")
              With .Helpers.DivC("pull-right")
                With .Helpers.Bootstrap.Button(, "New Invoice", Singular.Web.BootstrapEnums.Style.Info, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                               Singular.Web.PostBackType.None, "PaymentRunPage.newInvoiceAndOpen()")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 1)
              With .Helpers.DivC("pull-right")
                With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger, ,
                                               BootstrapEnums.ButtonSize.Small, , "fa-trash", ,
                                               PostBackType.None, "PaymentRunPage.deleteSelectedInvoices()", )
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
        End With
      End With
      With Helpers.Bootstrap.Row
        .Style.Margin("5px", , , )
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Invoicing.ReadOnly.ROCreditorInvoice)("ViewModel.ROCreditorInvoiceListManager",
                                                                                              "ViewModel.ROCreditorInvoiceList",
                                                                                              False, False, False, False, True, True, True, ,
                                                                                              Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                              , False, True)
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            With .FirstRow
              .AddClass("items selectable")
              With .AddColumn("")
                .Style.Width = "100px"
                With .Helpers.Bootstrap.StateButton(Function(d As ROCreditorInvoice) d.IsSelected, "Select", "Select", , , , , )
                  .Button.AddClass("btn-select")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.click, mOnButtonSelect)
                End With
                With .Helpers.Bootstrap.Button(, "Edit", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, mOnRowSelected, False)
                End With
              End With
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.HumanResource)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.HumanResourceIDNo)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.StaffNo)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.InvoiceDate)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.SystemInvoiceNum)
              .AddReadOnlyColumn(Function(c As ROCreditorInvoice) c.SupplierInvoiceNum)
              '.AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TransactionType)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalAmountExclVAT)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalVATAmount)
              .AddReadOnlyColumn(Function(d As ROCreditorInvoice) d.TotalAmount)
            End With
            'If mOnRowSelected <> "" Then
            '  .FirstRow.AddBinding(Singular.Web.KnockoutBindingString.click, mOnButtonSelect)
            'End If
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class EditCreditorInvoice(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).ICurrentInvoice)

    Private mModalID As String = ""
    Private mCurrentInvoiceFunction As String = ""
    Private mFindHumanResourceFunction As String = ""
    Private mPreviousInvoiceBinding As String = ""
    Private mNextInvoiceBinding As String = ""
    Private mSaveAndNextInvoiceBinding As String = ""
    Private mSaveNewInvoice As String = ""
    Private mSaveAndNewInvoice As String = ""

    Public Sub New(FindHumanResourceFunction As String,
                   PreviousInvoiceBinding As String,
                   NextInvoiceBinding As String,
                   SaveAndNextInvoiceBinding As String,
                   SaveNewInvoice As String,
                   SaveAndNewInvoice As String,
                   Optional ModalID As String = "EditCreditorInvoice",
                   Optional CurrentInvoiceFunction As String = "ViewModel.CurrentCreditorInvoice()")
      mFindHumanResourceFunction = FindHumanResourceFunction
      mModalID = ModalID
      mCurrentInvoiceFunction = CurrentInvoiceFunction
      mPreviousInvoiceBinding = PreviousInvoiceBinding
      mNextInvoiceBinding = NextInvoiceBinding
      mSaveAndNextInvoiceBinding = SaveAndNextInvoiceBinding
      mSaveNewInvoice = SaveNewInvoice
      mSaveAndNewInvoice = SaveAndNewInvoice
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Edit Invoice",
                                    False, "modal-lg",
                                    Singular.Web.BootstrapEnums.Style.Primary, , "fa-edit", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of CreditorInvoice)(mCurrentInvoiceFunction)
            With .Helpers.Bootstrap.Row
              '.Bindings.AddVisibilityBinding("!ViewModel.IsFetchingInvoice()", VisibleFadeType.Fade, VisibleFadeType.Fade)
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 2)
                With .Helpers.Bootstrap.FlatBlock("Invoice Info", , , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.SystemInvoiceNum)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.SystemInvoiceNum, Singular.Web.BootstrapEnums.InputSize.ExtraSmall)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.InvoiceDate)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.InvoiceDate, BootstrapEnums.InputSize.Small)
                            .Editor.Attributes("placeholder") = "Invoice Date"
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.HumanResource)
                          With .Helpers.If("$data.IsNew()")
                            With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.HumanResourceID, BootstrapEnums.InputSize.Small, , "Human Resource...")
                            End With
                          End With
                          With .Helpers.If("!$data.IsNew()")
                            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.HumanResource, BootstrapEnums.InputSize.Small)
                              .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(c As CreditorInvoice) Not c.IsNew)
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.EmployeeCode)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.EmployeeCode, BootstrapEnums.InputSize.Small, , "Staff No.")
                            .Editor.AddBinding(KnockoutBindingString.enable, Function(d As CreditorInvoice) False)
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        .AddClass("hidden-xs hidden-sm hidden-xl")
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.AmountExclVAT)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.AmountExclVAT, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.VATAmount)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.VATAmount, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Amount)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CreditorInvoice) d.Amount, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6, 12)
                        .AddClass("hidden-xs hidden-sm hidden-xl")
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.RefNo)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As CreditorInvoice) d.RefNo, BootstrapEnums.InputSize.Small, , "Ref Num")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Status")
                          With .Helpers.Bootstrap.StateButton(Function(d As CreditorInvoice) d.CompletedInd,
                                                              "Completed", "Not Completed", , , , , "btn-sm")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 10)
                With .Helpers.Bootstrap.FlatBlock("Line Items", , , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.DivC("pull-left")
                          With .Helpers.Bootstrap.Button(, "Select Dates", BootstrapEnums.Style.Primary, ,
                                                         BootstrapEnums.ButtonSize.Small, , "fa-calendar", ,
                                                         PostBackType.None, "PaymentRunPage.showDetailDateSelector($data, $element)")
                          End With
                        End With
                        With .Helpers.DivC("pull-right")
                          With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger, ,
                                                         BootstrapEnums.ButtonSize.Small, , "fa-trash", ,
                                                         PostBackType.None, "CreditorInvoiceBO.deleteSelected($data)")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of CreditorInvoiceDetail)("$data.CreditorInvoiceDetailList()",
                                                                                     False, False, False, False, True, True, True)
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            With .FirstRow
                              .AddClass("items selectable")
                              With .AddColumn("")
                                .Style.Width = "50px"
                                .Helpers.Bootstrap.StateButton(Function(d As CreditorInvoiceDetail) d.IsSelected, "Select", "Selected", , , , , )
                              End With
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailDate, 100)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.CreditorInvoiceDetailTypeID, 80)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.SupplierInvoiceDetailNum, 80)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.ProductionAreaID, 80)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.DetailDescription)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.AccountID, 80)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.CostCentreID, 80)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.VatTypeID, 80)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.AmountExclVAT, 125)
                              .AddColumn(Function(d As CreditorInvoiceDetail) d.VATAmount, 80)
                              .AddReadOnlyColumn(Function(d As CreditorInvoiceDetail) d.Amount, 125)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("loading")
          '    .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
          '    .Bindings.AddVisibilityBinding("ViewModel.IsFetchingInvoice()", VisibleFadeType.Fade, VisibleFadeType.Fade)
          '  End With
          'End With
        End With
        With .Footer
          With .Helpers.With(Of CreditorInvoice)(mCurrentInvoiceFunction)
            With .Helpers.DivC("pull-left")
              With .Helpers.If("!$data.IsNew()")
                With .Helpers.Bootstrap.Button(, "Previous", BootstrapEnums.Style.Warning, , BootstrapEnums.ButtonSize.Small, , "fa-angle-double-left", ,
                                               PostBackType.None, mPreviousInvoiceBinding, False)
                  '.Button.AddBinding(KnockoutBindingString.html, "<i class='fa fa-arrow-double-left'> Previous")
                End With
              End With
            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.If("$data.IsNew()")
                With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , "fa-angle-double-right", ,
                                               PostBackType.None, mSaveNewInvoice, True)
                End With
                With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-angle-double-right", ,
                                               PostBackType.None, mSaveAndNewInvoice, True)
                End With
              End With
              With .Helpers.If("!$data.IsNew()")
                With .Helpers.Bootstrap.Button(, "Next", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-angle-double-right", ,
                                               PostBackType.None, mNextInvoiceBinding, True)
                End With
                With .Helpers.Bootstrap.Button(, "Save & Next", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-angle-double-right", ,
                                               PostBackType.None, mSaveAndNextInvoiceBinding, True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class ROInvoicePreviewGrid(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROInvoicePreview)

    Private mModalID As String = ""
    Private mRefreshFunction As String = "PaymentRunPage.RefreshCreditorInvoicePreviewList()"

    Public Sub New(Optional RefreshFunction As String = "PaymentRunPage.RefreshCreditorInvoicePreviewList()")
      mRefreshFunction = RefreshFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.DivC("pull-left")

      End With
      With Helpers.DivC("pull-right")
        .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, mRefreshFunction, )
      End With
      With Helpers.DivC("table-responsive")
        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Invoicing.ReadOnly.ROInvoicePreview)("ViewModel.ROInvoicePreviewListManager",
                                                                                           "ViewModel.ROInvoicePreviewList",
                                                                                           False, False, False, False, True, True, True, ,
                                                                                           Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                           "", False, True)
          .AddClass("no-border hover list")
          .TableBodyClass = "no-border-y no-border-x no-border-x-top"
          With .FirstRow
            .AddClass("items selectable")
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.ParticipantName)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.IDNo)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.EmployeeCode)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.ContractType)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.TotalHoursForDay)
            .AddReadOnlyColumn(Function(c As ROInvoicePreview) c.TotalOvertime)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.TotalTaxableAmount)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.TotalSnT)
            .AddReadOnlyColumn(Function(d As ROInvoicePreview) d.EmailAddress)
          End With
          With .AddChildTable(Of OBLib.Invoicing.ReadOnly.ROInvoicePreviewDetail)(Function(d As ROInvoicePreview) d.ROInvoicePreviewDetailList, False, False, True, False, True, True, True)
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            With .FirstRow
              .AddClass("items selectable")
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.DateDisplay)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.ProductionArea)
              .AddReadOnlyColumn(Function(c As ROInvoicePreviewDetail) c.TimesheetCategory)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.StartDateDisplay)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.EndDateDisplay)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.Description)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.Discipline)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.HoursForDay)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.OvertimeHours)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.Rate)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.HourlyRate)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.TaxableTotal)
              .AddReadOnlyColumn(Function(d As ROInvoicePreviewDetail) d.SnT)
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class ROPaymentRunAlertList(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROInvoicePreview)

    Private mListProperty As String = "ViewModel.ROPaymentRunAlertGroupList()"
    Private mOnClick As String = "PaymentRunPage.alertSelected($data)"

    Public Sub New(Optional ListProperty As String = "ViewModel.ROPaymentRunAlertGroupList()",
                   Optional OnClick As String = "PaymentRunPage.alertSelected($data)")
      mListProperty = ListProperty
      mOnClick = OnClick
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'With Helpers.DivC("table-responsive")
      With Helpers.ForEach(Of OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroup)(mListProperty)
        With .Helpers.Div
          .AddBinding(KnockoutBindingString.click, mOnClick)
          .AddBinding(KnockoutBindingString.css, "$data.AlertCategory()")
          .Attributes("role") = "alert"
          With .Helpers.HTMLTag("span")
            With .Helpers.HTMLTag("i")
              .AddBinding(KnockoutBindingString.css, "$data.AlertIcon()")
            End With
          End With
          With .Helpers.HTMLTag("span")
            With .Helpers.HTMLTag("strong")
              .AddBinding(KnockoutBindingString.html, "$data.AlertTitle()")
            End With
          End With
          With .Helpers.HTMLTag("span")
            .AddBinding(KnockoutBindingString.html, "$data.AlertDescription()")
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace