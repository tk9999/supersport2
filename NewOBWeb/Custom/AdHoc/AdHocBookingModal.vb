﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.AdHocBookings

Namespace Controls

  Public Class AdHocBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Private mSaveMethod As String

    Public Sub New(SaveMethod As String)
      mSaveMethod = SaveMethod
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("AdHocBookingModal", "New AdHoc Booking", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , " fa-edit", "fa-2x", )
        .Heading.AddBinding(KnockoutBindingString.html, "AdHocBookingBO.dialogTitle(ViewModel.AdHocBooking())")
        With .ContentDiv
          With .Helpers.With(Of OBLib.AdHoc.AdHocBooking)("ViewModel.AdHocBooking()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.With(Of OBLib.AdHoc.AdHocBooking)("ViewModel.AdHocBooking()")
                With .Helpers.Bootstrap.FlatBlock("Booking Details", , , , , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.SystemID).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.ProductionAreaID).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.AdHocBookingTypeID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.AdHocBookingTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.Title).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.Title, BootstrapEnums.InputSize.Small, , "Title")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.StartDateTime).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.StartDateTime, BootstrapEnums.InputSize.Small, , "Start Time")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTime).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.EndDateTime, BootstrapEnums.InputSize.Small, , "End Time")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.CountryID).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.CountryID, BootstrapEnums.InputSize.Small, , "Country")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.CostCentreID).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.CostCentreID, BootstrapEnums.InputSize.Small, , "Cost Centre")
                        End With
                      End With
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                      '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.ProductionAreaID).Style.Width = "100%"
                      '    .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
                      '  End With
                      'End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.Description).Style.Width = "100%"
                          With .Helpers.TextBoxFor(Function(p As OBLib.AdHoc.AdHocBooking) p.Description)
                            .MultiLine = True
                            .Style.Height = "100px"
                            .AddClass("form-control")
                            '.AddBinding(KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 4, 4, 4)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          '.AddBinding(KnockoutBindingString.visible, "$data.ShowFindSynergyEvent()")
                          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.AdHoc.AdHocBooking) p.GenRefNo).Style.Width = "100%"
                          With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.GenRefNo, BootstrapEnums.InputSize.Small, , "Gen Ref")
                              .Editor.AddBinding(KnockoutBindingString.enable, "AdHocBookingBO.canEdit('GenRefNo', $data)")
                            End With
                            With .Helpers.SpanC("input-group-btn")
                              With .Helpers.Bootstrap.Button(, "Find", , , BootstrapEnums.ButtonSize.Small, , "fa-search", , PostBackType.None, "AdHocBookingBO.findSynergyEvent($data)", )
                                .Button.AddBinding(KnockoutBindingString.enable, "AdHocBookingBO.canEdit('GenRefNoSearch', $data)")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 8, 8, 8)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          '.AddBinding(KnockoutBindingString.visible, "$data.ShowFindSynergyEvent()")
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.UsingDefaultGenRefReason).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.AdHoc.AdHocBooking) p.UsingDefaultGenRefReason, BootstrapEnums.InputSize.Small, , "Gen Ref Reason")
                            .Editor.AddBinding(KnockoutBindingString.enable, "AdHocBookingBO.canEdit('UsingDefaultGenRefReason', $data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.With(Of OBLib.AdHoc.AdHocBooking)("ViewModel.AdHocBooking()")
                With .Helpers.Bootstrap.FlatBlock("Find Event", , , , , )
                  .FlatBlockTag.AddBinding(KnockoutBindingString.visible, "$data.ShowFindSynergyEvent()")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.With(Of OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria)("ViewModel.ROSynergyEventSearchListCriteria()")
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 2)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) d.StartDate).Style.Width = "100%"
                                .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) p.StartDate, BootstrapEnums.InputSize.Small, , )
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 2)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) d.EndDate).Style.Width = "100%"
                                .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) p.EndDate, BootstrapEnums.InputSize.Small, , )
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) d.GenRefNumber).Style.Width = "100%"
                                .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) p.GenRefNumber, BootstrapEnums.InputSize.Small, , )
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) d.Keyword).Style.Width = "100%"
                                .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Synergy.ReadOnly.ROSynergyEventSearchList.Criteria) p.Keyword, BootstrapEnums.InputSize.Small, , )
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
                              .Helpers.Bootstrap.LabelDisplay("Refresh").Style.Width = "100%"
                              With .Helpers.Bootstrap.Button(, "Refresh", , , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , PostBackType.None, "ViewModel.ROSynergyEventSearchListManager().Refresh()", )
                                .Button.AddClass("btn-block")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Synergy.ReadOnly.ROSynergyEventSearch)("ViewModel.ROSynergyEventSearchListManager",
                                                                                                             "ViewModel.ROSynergyEventSearchList()",
                                                                                                             False, False, False, True, True, True, True,
                                                                                                             , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                             "", False, True)
                          .Pager.PagerListTag.ListTag.AddClass("pull-right")
                          With .FirstRow
                            .AddClass("items selectable")
                            With .AddColumn("")
                              .Style.Width = "50px"
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", , , , , )
                                '.Button.AddBinding(KnockoutBindingString.enable, "ROHumanResourceTravelReqBO.canEdit('SelectButton', $data)")
                                '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, "TravelVM.onNewTravellerSelected($data)")
                              End With
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Synergy.ReadOnly.ROSynergyEventSearch) c.GenRefNumber)
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Synergy.ReadOnly.ROSynergyEventSearch) c.GenreSeries)
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Synergy.ReadOnly.ROSynergyEventSearch) c.Title)
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Synergy.ReadOnly.ROSynergyEventSearch) c.StartTimeString)
                            End With
                            With .AddReadOnlyColumn(Function(c As OBLib.Synergy.ReadOnly.ROSynergyEventSearch) c.EndTimeString)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.AdHoc.AdHocBooking)("ViewModel.AdHocBooking()")
            With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
              .AddClass("col-md-offset-6 col-lg-offset-8 col-xl-offset-8")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, ,
                                             BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, mSaveMethod)
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                .Button.AddClass("btn-block")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace