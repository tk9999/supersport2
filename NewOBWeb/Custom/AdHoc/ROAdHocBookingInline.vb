﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling

Namespace Controls

  Public Class ROAdHocBookingInline(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Private mJSControlInstanceName As String = "CurrentRoomScheduleControl"

    Public Sub New(Optional JSControlInstanceName As String = "CurrentRoomScheduleControl")
      mJSControlInstanceName = JSControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking)(mJSControlInstanceName & ".adhocBooking()")
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.AdHocBookingType).Style.Width = "100%"
          With .Helpers.Bootstrap.FormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.AdHocBookingType, Singular.Web.BootstrapEnums.InputSize.Small)
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.SubDeptArea).Style.Width = "100%"
          .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.SubDeptArea, BootstrapEnums.InputSize.Small, , "Sub-Dept (Area)...")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) d.CountryLocation).Style.Width = "100%"
          .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.CountryLocation, BootstrapEnums.InputSize.Small, , "Country (Location)...")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d) d.GenRefNo).Style.Width = "100%"
          .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.GenRefNo, BootstrapEnums.InputSize.Small, , "GenRef...")
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.Description).Style.Width = "100%"
          With .Helpers.TextBoxFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingAdHocBooking) p.Description)
            .MultiLine = True
            .Style.Height = "100px"
            .AddClass("form-control")
            .AddBinding(KnockoutBindingString.enable, Function(d) False)
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace