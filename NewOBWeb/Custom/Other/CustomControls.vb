﻿Imports Singular.Web.CustomControls
Imports Singular.Web
Imports Singular.Web.Controls.HelperControls
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.ReadOnly

Namespace CustomControls

  Public Class MainMenu
    Inherits System.Web.UI.WebControls.WebControl

    Public Property SiteMapDatasourceID As String
    Public Property BannerText As String
    'Public Property DivClass As String = "navbar"

    Protected Overrides Sub Render(writer As System.Web.UI.HtmlTextWriter)

      'NAVBAR--------------------------------------------------------------------------
      writer.WriteBeginTag("header")
      writer.WriteAttribute("class", "navbar navbar-inverse") ' navbar-fixed-top
      writer.WriteAttribute("role", "banner")
      writer.Write(">")

      'HEADER--------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "navbar-header")
      writer.Write(">")

      'button inside header
      writer.WriteBeginTag("button")
      writer.WriteAttribute("type", "button")
      writer.WriteAttribute("class", "navbar-toggle")
      writer.WriteAttribute("data-toggle", "collapse")
      writer.WriteAttribute("data-target", "navbar-ex1-collapse")
      writer.Write(">")

      'spans inside button
      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "sr-only")
      writer.Write(">")
      writer.Write("Toggle navigation")
      writer.WriteEndTag("span")

      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "icon-bar")
      writer.Write(">")
      writer.WriteEndTag("span")

      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "icon-bar")
      writer.Write(">")
      writer.WriteEndTag("span")

      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "icon-bar")
      writer.Write(">")
      writer.WriteEndTag("span")

      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "icon-bar")
      writer.Write(">")
      writer.WriteEndTag("span")

      writer.WriteEndTag("button")

      writer.WriteBeginTag("a")
      writer.WriteAttribute("class", "navbar-brand")
      writer.WriteAttribute("href", "#")
      writer.Write(">")
      writer.Write(BannerText)
      writer.WriteEndTag("a")

      'END HEADER--------------------------------------------------------------------------
      writer.WriteEndTag("div")

      'MENU--------------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "collapse navbar-collapse navbar-ex1-collapse")
      writer.Write(">")


      Dim allLinks As SiteMapDataSource = FindControl(SiteMapDatasourceID)
      'LEFT LINKS--------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "nav navbar-nav pull-left hidden-xs")
      writer.Write(">")
      RenderSubMenu(writer, CType(allLinks, Singular.Web.CustomControls.SiteMapDataSource).GetHierarchicalView.Select, True, "Left")
      writer.WriteEndTag("ul")

      'RIGHT LINKS------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "nav navbar-nav pull-right hidden-xs")
      writer.Write(">")
      RenderSubMenu(writer, CType(allLinks, Singular.Web.CustomControls.SiteMapDataSource).GetHierarchicalView.Select, True, "Right")
      writer.WriteEndTag("ul")

      'END MENU---------------------------------------------------------------------------
      writer.WriteEndTag("div")

      'END NAVBAR-------------------------------------------------------------------------
      writer.WriteEndTag("nav")

    End Sub

    Private Sub CreateListItem(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode)

      'check attributes for the side it must be shown on
      Dim pi As System.Reflection.PropertyInfo = Node.GetType.GetProperty("Attributes", System.Reflection.BindingFlags.Instance + System.Reflection.BindingFlags.NonPublic)
      Dim prop As System.Collections.Specialized.NameValueCollection = Nothing
      If pi IsNot Nothing Then
        prop = pi.GetValue(Node, Nothing)
      End If
      Dim CssClass As String = prop.Item("CssClass")
      Dim GlyphIcon As String = prop.Item("GlyphIcon")
      Dim FAIcon As String = prop.Item("FAIcon")
      Dim NewTab As String = prop.Item("NewTab")
      Dim Avatar As String = prop.Item("Avatar")

      If NewTab Is Nothing Then
        NewTab = False
      End If

      'If Node.Description = "Your Account" Then
      '  'Add Notifications node before "Your Account" node
      '  AddNotificationsNode(writer)
      'End If

      writer.WriteBeginTag("li")

      'If a css class has been specified, add the css classes
      If CssClass IsNot Nothing AndAlso CssClass <> "" Then
        writer.WriteAttribute("class", CssClass)
      End If

      'If has child nodes
      If Node.ChildNodes.Count > 0 Then
        'Populate child nodes
        PopulateAsDropDown(writer, Node, GlyphIcon, FAIcon, Avatar)
      Else
        'Populate self
        PopulateAsSingleItem(writer, Node, GlyphIcon, FAIcon, NewTab)
      End If

      writer.WriteEndTag("li")
      writer.WriteLine()

    End Sub

    Private Sub PopulateAsDropDown(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode, GlyphIcon As String, FAIcon As String, Avatar As String)

      'close begin tag
      writer.WriteAttribute("class", "dropdown")
      writer.Write(">")

      'clickable link
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "#")
      writer.WriteAttribute("class", "dropdown-toggle")
      writer.WriteAttribute("data-toggle", "dropdown")
      writer.Write(">")


      If FAIcon IsNot Nothing AndAlso FAIcon <> "" Then
        writer.WriteBeginTag("span")
        writer.Write(">")
        writer.WriteBeginTag("i")
        writer.WriteAttribute("class", "fa " & FAIcon & " head-nav-dropdown-icon")
        writer.Write(">")
        writer.WriteEndTag("i")
        writer.WriteEndTag("span")
      End If
      ''Icon
      'If Avatar IsNot Nothing Then
      '  writer.WriteBeginTag("img")
      '  writer.WriteAttribute("alt", "Avatar")
      '  writer.WriteAttribute("src", "../Images/avatar6-2.jpg")
      '  writer.Write(">")
      '  writer.WriteEndTag("img")
      'End If

      'Name
      writer.WriteBeginTag("span")
      writer.Write(">")
      writer.Write(Node.Title)
      writer.WriteEndTag("span")

      'the little down arrow thing
      writer.WriteBeginTag("b")
      writer.WriteAttribute("class", "caret")
      writer.Write(">")
      writer.WriteEndTag("b")

      'close clickable link
      writer.WriteEndTag("a")

      'children
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "dropdown-menu")
      writer.Write(">")
      RenderSubMenu(writer, Node.ChildNodes, False, Nothing)
      writer.WriteEndTag("ul")

    End Sub

    Private Sub PopulateAsSingleItem(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode, Optional GlyphIcon As String = "", Optional FAIcon As String = "", Optional NewTab As Boolean = False)

      writer.Write(">")
      writer.WriteBeginTag("a")
      If Node.Url = "" Then
        writer.WriteAttribute("href", "#")
      Else
        'target="_blank"
        writer.WriteAttribute("href", Node.Url)
        If NewTab Then
          writer.WriteAttribute("target", "_blank")
        End If
      End If
      writer.Write(">")
      If GlyphIcon <> "" Then
        writer.WriteBeginTag("span")
        writer.WriteAttribute("class", "glyphicon " & GlyphIcon)
        writer.Write(">")
        writer.WriteEndTag("span")
      ElseIf FAIcon <> "" Then
        writer.WriteBeginTag("i")
        writer.WriteAttribute("class", "fa " & FAIcon)
        writer.Write(">")
        writer.WriteEndTag("i")
      End If
      writer.Write(" " & Node.Title)
      writer.WriteEndTag("a")

    End Sub

    Private Sub RenderSubMenu(writer As System.Web.UI.HtmlTextWriter, RootNode As SiteMapNodeCollection, First As Boolean, Side As String)

      For Each node As SiteMapNode In RootNode
        If Side Is Nothing OrElse Side = GetSide(node) Then
          CreateListItem(writer, node)
          writer.WriteLine()
        End If
      Next
      writer.WriteLine()

    End Sub

    Private Sub AddNotificationsNode(writer As System.Web.UI.HtmlTextWriter)


      'List Item
      writer.WriteBeginTag("li")
      writer.WriteAttribute("id", "inbox")
      writer.WriteAttribute("class", "button dropdown inbox")
      writer.Write(">")

      'Anchor
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "#")
      writer.WriteAttribute("class", "btn-see-through")
      writer.Write(">")

      'Icon
      writer.WriteBeginTag("i")
      writer.WriteAttribute("class", "fa fa-inbox")
      writer.Write(">")
      writer.WriteEndTag("i")

      'Span bubble
      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "bubble")
      writer.Write(">")
      writer.Write("20")
      writer.WriteEndTag("span")

      'Close
      writer.WriteEndTag("a")
      writer.WriteEndTag("li")

      writer.WriteLine()

    End Sub

    Private Function GetSide(Node As SiteMapNode) As String

      'check attributes for the side it must be shown on
      Dim pi As System.Reflection.PropertyInfo = Node.GetType.GetProperty("Attributes", System.Reflection.BindingFlags.Instance + System.Reflection.BindingFlags.NonPublic)
      Dim prop As System.Collections.Specialized.NameValueCollection = Nothing
      If pi IsNot Nothing Then
        prop = pi.GetValue(Node, Nothing)
      End If

      Return prop.Item("Side")

    End Function

  End Class

  Namespace Productions

    Public Class ProductionDashboard
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      'Public Properties
      Public Property ProductionDashboard As OBLib.Productions.Old.Production
      Public Property Sidebar As HTMLDiv(Of OBLib.Productions.Old.Production)
      Public Property Menu As BootstrapUnorderedList(Of OBLib.Productions.Old.Production)
      Public Property Content As HTMLDiv(Of OBLib.Productions.Old.Production)
      Public Property ContentParent As HTMLDiv(Of OBLib.Productions.Old.Production)
      Public Property Messages As HTMLDiv(Of OBLib.Productions.Old.Production)
      Public Property TopMenu As HTMLDiv(Of OBLib.Productions.Old.Production)
      Public Property SaveButtonContainer As HTMLDiv(Of OBLib.Productions.Old.Production)
      Public Property SaveButton As BootstrapButton(Of OBLib.Productions.Old.Production)
      Public Property ReloadButton As BootstrapButton(Of OBLib.Productions.Old.Production)
      Public Property ToolbarContainer As HTMLDiv(Of OBLib.Productions.Old.Production)

      'Private Properties
      Private mLocalHelpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production)
      Private mMenuCssClass As String = ""
      Private mAbs As String = VirtualPathUtility.ToAbsolute("~/Productions")

      Public Sub BuildSideBar(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production))

        Sidebar = Helpers.Div
        Sidebar.Attributes("id") = "sidebar-nav"

        With Sidebar
          Menu = .Helpers.BootstrapUnorderedList("dashboard-menu", "")
        End With

        With Menu
          .AddItem(, , "#", "Main", Singular.Web.LinkTargetType.NotSet, "icon-home", "SetMainTabVisible()") 'mAbs & "/Dashboard.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
          'If ProductionDashboard.IsSelfValid Then
          .AddItem(, , "#", "Requirements", Singular.Web.LinkTargetType._self, "icon-cogs", "SetRequirementsTabVisible()") 'mAbs & "/Requirements/ProductionRequirements.aspx"
          .AddItem(, , "#", "Vehicles", Singular.Web.LinkTargetType.NotSet, "icon-truck", "SetProductionVehiclesTabVisible()") 'mAbs & "/Requirements/ProductionVehicles.aspx"
          .AddItem(, , "#", "Timelines", Singular.Web.LinkTargetType.NotSet, "icon-time", "SetTimelineTabsVisible()") 'mAbs & "/Areas/ProductionSystemArea.aspx"
          .AddItem(, , "#", "Crew", Singular.Web.LinkTargetType.NotSet, "icon-group", "SetProductionCrewTabsVisible()") 'mAbs & "/Crew/ProductionHumanResources.aspx"
          .AddItem(, , "#", "Schedules", Singular.Web.LinkTargetType.NotSet, "icon-list", "SetProductionScheduleTabsVisible()") 'mAbs & "/Crew/Schedules.aspx"
          .AddItem(, , "#", "Services", Singular.Web.LinkTargetType.NotSet, "icon-usd", "SetOutsourceServicesTabVisible()") 'mAbs & "/Requirements/OutsourceServices.aspx"
          .AddItem(, , "#", "Travel", Singular.Web.LinkTargetType.NotSet, "icon-plane", "SetTravelRequisitionTabVisible()") 'mAbs & "/Travel/ProducitonTravelRequisition.aspx?P=" & ProductionDashboard.ProductionID.ToString
          'With .AddSubMenu("icon-cog", "Travel")
          '  .AddItem(, , mAbs & "/Travel/ProducitonTravelRequisition.aspx?P=" & ProductionDashboard.ProductionID.ToString, "Travel Requisition", Singular.Web.LinkTargetType.NotSet, ) '"icon-eye-open"s
          '  '.AddItem(, , mAbs & "/Travel/Overview.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "Overview", Singular.Web.LinkTargetType.NotSet, ) '"icon-eye-open"
          '  '.AddItem(, , mAbs & "/Travel/Flights.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "Flights", Singular.Web.LinkTargetType.NotSet, ) '"icon-plane"
          '  '.AddItem(, , mAbs & "/Travel/RentalCars.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "Rental Cars", Singular.Web.LinkTargetType.NotSet, ) '"icon-road"
          '  '.AddItem(, , mAbs & "/Travel/Accommodation.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "Accommodation", Singular.Web.LinkTargetType.NotSet, ) '"icon-home"
          '  '.AddItem(, , mAbs & "/Travel/SnT.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "S&T", Singular.Web.LinkTargetType.NotSet, ) '"icon-cutlery"
          '  '.AddItem(, , mAbs & "/Travel/TravelReqComments.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "Comments", Singular.Web.LinkTargetType.NotSet, ) '"icon-cutlery"
          'End With
          .AddItem(, , mAbs & "/Requirements/Audio.aspx", "Audio", Singular.Web.LinkTargetType.NotSet, "icon-heaphones")
          '.AddItem(, , mAbs & "/Requirements/PettyCash.aspx", "Petty Cash", Singular.Web.LinkTargetType.NotSet, )
          With .AddSubMenu("icon-comment", "Comments")
            .AddItem(, , "#", "Production Comments", Singular.Web.LinkTargetType.NotSet, , "SetProductionCommentSectionTabVisible()") '.AddItem(, , mAbs & "/Comments/ProductionComments.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString, "Production Comments", Singular.Web.LinkTargetType.NotSet, ) '"icon-eye-open"
            .AddItem(, , "#", "General", Singular.Web.LinkTargetType.NotSet, ) 'mAbs & "/Comments/General.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
          End With
          .AddItem(, , "#", "Correspondence", Singular.Web.LinkTargetType.NotSet, "icon-book", "SetCorrespondenceTabVisible()") '"icon-eye-open" 'mAbs & "/Correspondence.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
          With .AddSubMenu("icon-file", "Reports")
            .AddItem(, , "#", "Schedule", Singular.Web.LinkTargetType.NotSet, ) '"icon-eye-open" mAbs & "/Reports/Schedule.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
            .AddItem(, , "#", "PA Form Sign-In", Singular.Web.LinkTargetType.NotSet, ) '"icon-plane" mAbs & "/Reports/PAFormIn.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
            .AddItem(, , "#", "PA Form Sign-Out", Singular.Web.LinkTargetType.NotSet, ) '"icon-eye-open" mAbs & "/Reports/PAFormOut.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
            .AddItem(, , "#", "Travel Req", Singular.Web.LinkTargetType.NotSet, ) '"icon-plane" mAbs & "/Reports/TravelReq.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
            .AddItem(, , "#", "S&T", Singular.Web.LinkTargetType.NotSet, ) '"icon-eye-open" mAbs & "/Reports/SnT.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
            .AddItem(, , "#", "Costs", Singular.Web.LinkTargetType.NotSet, ) '"icon-plane" mAbs & "/Reports/Cost.aspx?ProductionID=" & ProductionDashboard.ProductionID.ToString
          End With
          'End If
        End With

      End Sub

      Public Sub BuildContent(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production))

        ContentParent = Helpers.DivC("content")
        Content = ContentParent.Helpers.Div
        Content.Attributes("id") = "pad-wrapper"

      End Sub

      Public Sub BuildMessages(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production))

        Dim MainStats As HTMLDiv(Of OBLib.Productions.Old.Production) = Helpers.Div
        MainStats.Attributes("id") = "main-stats"

        With MainStats
          Messages = .Helpers.DivC("row stats-row")
          With Messages
            'Info
            With .Helpers.DivC("col-md-3 col-sm-3 stat stat-info panel panel-info")
              'With .Helpers.DivC("panel panel-info")

              'End With
              With .Helpers.DivC("data")
                With .Helpers.HTMLTag("span")
                  .AddClass("number")
                  .Helpers.HTML("2457")
                End With
              End With
              With .Helpers.HTMLTag("span")
                .AddClass("date")
                .Helpers.HTML("Today")
              End With
            End With
            'Warning
            With .Helpers.DivC("col-md-3 col-sm-3 stat stat-warning panel panel-warning")
              'With .Helpers.DivC("panel panel-warning")

              'End With
              With .Helpers.DivC("data")
                With .Helpers.HTMLTag("span")
                  .AddClass("number")
                  .Helpers.HTML("3240")
                End With
                .Helpers.HTML("users")
              End With
              With .Helpers.HTMLTag("span")
                .AddClass("date")
                .Helpers.HTML("February 2013")
              End With
            End With
            'Errors
            With .Helpers.DivC("col-md-6 col-sm-6 stat-error panel panel-danger last")
              'With .Helpers.HTMLTag("span")
              '  .AddClass("date")
              '  .Helpers.HTML("last 30 days")
              'End With
              '.Helpers.MessageHolder()
              'With .Helpers.DivC("data")
              .Helpers.MessageHolder()
              'With .Helpers.HTMLTag("span")
              '  .AddClass("number")
              '  .Helpers.HTML("$2,340")
              'End With
              '.Helpers.HTML("sales")
              'End With
            End With
          End With
        End With

      End Sub

      Public Sub BuildTopMenuLarge(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production))

        TopMenu = Helpers.BootstrapDivColumn(12, 12, 12) '.DivC("row")

        With TopMenu
          With .Helpers.HTMLTag("ul")
            .AddClass("nav nav-tabs nav-justified hidden-sm")
            .Attributes("role") = "tablist"
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Main")
                End With
                .AddBinding(KnockoutBindingString.click, "SetMainTabVisible()")
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetRequirementsTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Requirements")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetProductionVehiclesTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Vehicles")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetTimelineTabsVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Timelines")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetProductionCrewTabsVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Crew")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetProductionScheduleTabsVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Schedules")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetOutsourceServicesTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Services")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetTravelRequisitionTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Travel")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetProductionCommentSectionTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Comments")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetCorrespondenceTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Correspondence")
                End With
              End With
            End With
            With .Helpers.HTMLTag("li")

              With .Helpers.HTMLTag("a")
                .Attributes("href") = "#"
                .AddBinding(KnockoutBindingString.click, "SetRequirementsTabVisible()")
                With .Helpers.HTMLTag("i")
                  .AddClass("icon-cogs")
                End With
                With .Helpers.HTMLTag("span")
                  .Helpers.HTML("Reports")
                End With
              End With
            End With
          End With
        End With

      End Sub

      Public Sub BuildNavMenu(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production))

        With Helpers.DivC("nav navbar-default")
          .Attributes("id") = "MainNavBar"
          .Attributes("role") = "navigation"

          With .Helpers.DivC("container-fluid")

            '<!-- Brand and toggle get grouped for better mobile display -->
            With .Helpers.DivC("navbar-header")
              With .Helpers.BootstrapButton(, , , , PostBackType.None, , , , )
                With .Button
                  .AddClass("navbar-toggle")
                  .Attributes("data-toggle") = "collapse"
                  .Attributes("data-target") = "#bs-example-navbar-collapse-1"
                  With .Helpers.HTMLTag("span")
                    .AddClass("sr-only")
                    .Helpers.HTML("Toggle")
                  End With
                  .Helpers.HTMLTag("span").AddClass("icon-bar")
                  .Helpers.HTMLTag("span").AddClass("icon-bar")
                  .Helpers.HTMLTag("span").AddClass("icon-bar")
                End With
                With .Helpers.HTMLTag("a")
                  .Attributes("href") = "#"
                  .AddClass("navbar-brand")
                End With
              End With
            End With

            '<!-- Collect the nav links, forms, and other content for toggling -->
            With .Helpers.DivC("collapse navbar-collapse")
              With .Helpers.HTMLTag("ul")
                .AddClass("nav navbar-nav")
                '.Attributes("role") = "tablist"
                '.Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, True, , , )
                With .Helpers.HTMLTag("li")
                  SaveButton = .Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, True, , , )
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "MainTab"
                  .AddClass("MenuTab active")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-home")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Main")
                    End With
                    .AddBinding(KnockoutBindingString.click, "SetMainTabVisible()")
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "RequirementTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetRequirementsTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-cogs")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Requirements")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ProductionVehicleTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetProductionVehiclesTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-truck")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Vehicles")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "TimelineTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetTimelineTabsVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-time")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Timelines")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ProductionCrewTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetProductionCrewTabsVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-group")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Crew")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ProductionScheduleTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetProductionScheduleTabsVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-list")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Schedules")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "OutsourceServiceTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetOutsourceServicesTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-usd")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Services")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "TravelRequisitionTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetTravelRequisitionTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-plane")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Travel")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ProductionCommentTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetProductionCommentSectionTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-heaphones")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Comments")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ProductionAudioTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetProductionAudioTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-heaphones")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Audio")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "CorrespondenceTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetCorrespondenceTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-cogs")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Correspondence")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ReportsTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetReportsTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-file")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Reports")
                    End With
                  End With
                End With
              End With
            End With

          End With

        End With

      End Sub

      Public Sub New(ProductionDashboard As OBLib.Productions.Old.Production, MenuCssClass As String)

        Me.ProductionDashboard = ProductionDashboard
        mMenuCssClass = MenuCssClass
        ', Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Old.Production)
        'mLocalHelpers = Helpers

      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        'Create the Side Bar
        'BuildSideBar(Helpers)

        'Create the Content (messages is built within content)
        'BuildContent(Helpers) 'mLocalHelpers

        'BuildTopMenuLarge(Helpers)
        'BuildNavMenu(Helpers)

        SaveButtonContainer = Helpers.BootstrapDivColumn(12, 12, 12)

        With SaveButtonContainer
          With .Helpers.BootstrapDivColumn(2, 2, 2)
            'With .Helpers.DivC("btn-group")
            SaveButton = .Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, False, , , )
            ReloadButton = .Helpers.BootstrapButton("ReloadProduction", "Reload", "btn-md btn-primary", "glyphicon-refresh", Singular.Web.PostBackType.Full, False, , , )
            'End With
          End With
          ToolbarContainer = .Helpers.BootstrapDivColumn(10, 10, 10)
        End With


        Content = Helpers.BootstrapDivColumn(12, 12, 12) '.DivC("row")

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    'Public Class StudioProductionControl
    '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Studio.StudioProduction)

    '  'Public Properties
    '  Public Property StudioProduction As OBLib.Productions.Studio.StudioProduction

    '  Public Sub New(StudioProduction As OBLib.Productions.Studio.StudioProduction)
    '    Me.StudioProduction = StudioProduction

    '  End Sub

    '  Protected Overrides Sub Setup()
    '    MyBase.Setup()

    '  End Sub

    '  Protected Overrides Sub Render()
    '    MyBase.Render()
    '    RenderChildren()
    '  End Sub

    'End Class

  End Namespace

  Namespace Productions.Travel

    Public Class ProductionTravelDashboard
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Travel.TravelRequisition)

      'Public Properties
      Public Property TravelRequisitionDashboard As OBLib.Travel.TravelRequisition
      Public Property Sidebar As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property Menu As BootstrapUnorderedList(Of OBLib.Travel.TravelRequisition)
      Public Property Content As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property ContentParent As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property Messages As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property SaveButton As BootstrapButton(Of OBLib.Travel.TravelRequisition)
      Public Property BackButton As BootstrapButton(Of OBLib.Travel.TravelRequisition)
      Public Property SaveButtonContainer As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property SaveContainer As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property BackContainer As HTMLDiv(Of OBLib.Travel.TravelRequisition)
      Public Property ToolbarContainer As HTMLDiv(Of OBLib.Travel.TravelRequisition)

      'Private Properties
      Private mLocalHelpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Travel.TravelRequisition)
      Private mMenuCssClass As String = ""
      Private mAbs As String = VirtualPathUtility.ToAbsolute("~/Productions/Travel")
      Private mActiveTab As String = ""
      'Private mProduction As Production = Nothing

      Public Sub BuildSideBar(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Travel.TravelRequisition))

        Sidebar = Helpers.Div
        Sidebar.Attributes("id") = "sidebar-nav"

        With Sidebar
          Menu = .Helpers.BootstrapUnorderedList("dashboard-menu", "")
        End With

        With Menu
          '.AddItem(, , mAbs & "/Dashboard.aspx?ProductionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Main", Singular.Web.LinkTargetType.NotSet, "icon-home")
          'If TravelRequisitionDashboard.IsValid Then
          .AddItem(, , mAbs & "/CrewMembers.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Crew", Singular.Web.LinkTargetType.NotSet, "icon-time")
          .AddItem(, , mAbs & "/Flights.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Flights", Singular.Web.LinkTargetType.NotSet, "icon-user")
          .AddItem(, , mAbs & "/RentalCars.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Rental Cars", Singular.Web.LinkTargetType.NotSet, "icon-list")
          .AddItem(, , mAbs & "/Accommodation.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Accommodation", Singular.Web.LinkTargetType.NotSet, "icon-list")
          .AddItem(, , mAbs & "/SnT.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "S&T", Singular.Web.LinkTargetType.NotSet, "icon-list")
          .AddItem(, , mAbs & "/TravelReqComments.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Comments", Singular.Web.LinkTargetType.NotSet, "icon-list")
          If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
            .AddItem(, , mAbs & "/TravelAdvances.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Travel Advances", Singular.Web.LinkTargetType.NotSet, "icon-list")
          End If
          .AddItem(, , mAbs & "/TravelReports.aspx?TravelRequisitionID=" & TravelRequisitionDashboard.TravelRequisitionID.ToString, "Reports", Singular.Web.LinkTargetType.NotSet, "icon-list")
          'End If
        End With

      End Sub

      Public Sub BuildContent(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Travel.TravelRequisition))

        ContentParent = Helpers.DivC("content")
        Content = ContentParent.Helpers.Div
        Content.Attributes("id") = "pad-wrapper"

      End Sub

      Public Sub BuildMessages(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Travel.TravelRequisition))

        Dim MainStats As HTMLDiv(Of OBLib.Travel.TravelRequisition) = Helpers.Div
        MainStats.Attributes("id") = "main-stats"

        With MainStats
          Messages = .Helpers.DivC("row stats-row")
          With Messages
            'Info
            With .Helpers.DivC("col-md-3 col-sm-3 stat stat-info panel panel-info")
              'With .Helpers.DivC("panel panel-info")

              'End With
              With .Helpers.DivC("data")
                With .Helpers.HTMLTag("span")
                  .AddClass("number")
                  .Helpers.HTML("2457")
                End With
              End With
              With .Helpers.HTMLTag("span")
                .AddClass("date")
                .Helpers.HTML("Today")
              End With
            End With
            'Warning
            With .Helpers.DivC("col-md-3 col-sm-3 stat stat-warning panel panel-warning")
              'With .Helpers.DivC("panel panel-warning")

              'End With
              With .Helpers.DivC("data")
                With .Helpers.HTMLTag("span")
                  .AddClass("number")
                  .Helpers.HTML("3240")
                End With
                .Helpers.HTML("users")
              End With
              With .Helpers.HTMLTag("span")
                .AddClass("date")
                .Helpers.HTML("February 2013")
              End With
            End With
            'Errors
            With .Helpers.DivC("col-md-6 col-sm-6 stat-error panel panel-danger last")
              'With .Helpers.HTMLTag("span")
              '  .AddClass("date")
              '  .Helpers.HTML("last 30 days")
              'End With
              '.Helpers.MessageHolder()
              'With .Helpers.DivC("data")
              .Helpers.MessageHolder()
              'With .Helpers.HTMLTag("span")
              '  .AddClass("number")
              '  .Helpers.HTML("$2,340")
              'End With
              '.Helpers.HTML("sales")
              'End With
            End With
          End With
        End With

      End Sub

      Public Sub BuildNavMenu(Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Travel.TravelRequisition))

        With Helpers.DivC("nav navbar-default")
          .Attributes("id") = "MainNavBar"
          .Attributes("role") = "navigation"

          With .Helpers.DivC("container-fluid")

            '<!-- Brand and toggle get grouped for better mobile display -->
            With .Helpers.DivC("navbar-header")
              With .Helpers.BootstrapButton(, , , , PostBackType.None, , , , )
                With .Button
                  .AddClass("navbar-toggle")
                  .Attributes("data-toggle") = "collapse"
                  .Attributes("data-target") = "#bs-example-navbar-collapse-1"
                  With .Helpers.HTMLTag("span")
                    .AddClass("sr-only")
                    .Helpers.HTML("Toggle")
                  End With
                  .Helpers.HTMLTag("span").AddClass("icon-bar")
                  .Helpers.HTMLTag("span").AddClass("icon-bar")
                  .Helpers.HTMLTag("span").AddClass("icon-bar")
                End With
                With .Helpers.HTMLTag("a")
                  .Attributes("href") = "#"
                  .AddClass("navbar-brand")
                End With
              End With
            End With

            '<!-- Collect the nav links, forms, and other content for toggling -->
            With .Helpers.DivC("collapse navbar-collapse")
              With .Helpers.HTMLTag("ul")
                .AddClass("nav navbar-nav")
                '.Attributes("role") = "tablist"
                '.Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, True, , , )
                With .Helpers.HTMLTag("li")
                  BackButton = .Helpers.BootstrapButton("BackToProduction", "Back", "btn-md btn-danger", "glyphicon-arrow-left", Singular.Web.PostBackType.Full, True, , , ) '"BackToProduction()")
                  With BackButton
                    .Button.Style.MarginRight("10px")
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  SaveButton = .Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, True, , , )
                  With SaveButton
                    .Button.Style.MarginRight("10px")
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "CrewTab"
                  .AddClass("CrewTab active")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-group")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Crew")
                    End With
                    .AddBinding(KnockoutBindingString.click, "SetCrewMembersTabVisible()")
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "FlightsTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetFlightsTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-plane")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Flights")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "RentalCarTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetRentalCarsTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-truck")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Rental Cars")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "AccommodationTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetAccommodationTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-home")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Accommodation")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "SnTTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetSnTTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-food")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("S&T")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "TravelReqCommentsTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetTravelReqCommentsTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-comments")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Comments")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "TravelAdvancesTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetTravelAdvancesTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-money")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Travel Advances")
                    End With
                  End With
                End With
                With .Helpers.HTMLTag("li")
                  .Attributes("id") = "ReportsTab"
                  .AddClass("MenuTab")
                  With .Helpers.HTMLTag("a")
                    .Attributes("href") = "#"
                    .AddBinding(KnockoutBindingString.click, "SetTravelReportsTabVisible()")
                    With .Helpers.HTMLTag("i")
                      .AddClass("icon-money")
                    End With
                    With .Helpers.HTMLTag("span")
                      .Helpers.HTML("Reports")
                    End With
                  End With
                End With


              End With
            End With



          End With

        End With

      End Sub

      Public Sub New(TravelRequisitionDashboard As OBLib.Travel.TravelRequisition, MenuCssClass As String)

        Me.TravelRequisitionDashboard = TravelRequisitionDashboard
        mMenuCssClass = MenuCssClass
        'mProduction = Production
        ', Helpers As Singular.Web.Controls.HelperControls.HelperAccessors(Of OBLib.Productions.Production)
        'mLocalHelpers = Helpers

      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        'Create the Side Bar
        'BuildSideBar(Helpers)

        'Create the Content (messages is built within content)
        'BuildContent(Helpers) 'mLocalHelpers

        'BuildTopMenuLarge(Helpers)
        'BuildNavMenu(Helpers)

        'SaveButtonContainer = Helpers.BootstrapDivColumn(12, 12, 12)

        'With SaveButtonContainer
        '  With .Helpers.BootstrapDivColumn(2, 2, 2)
        '    With BackContainer
        '      With .Helpers.BootstrapDivColumn(1, 1, 1)
        '        BackButton = .Helpers.BootstrapButton("BackToProduction", "Back", "btn-md btn-danger", "glyphicon-arrow-left", Singular.Web.PostBackType.Full, True, , , )
        '      End With
        '    End With
        '    With SaveContainer
        '      With .Helpers.BootstrapDivColumn(1, 1, 1)
        '        SaveButton = .Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, False, , , )
        '      End With
        '    End With
        '  End With
        '  ToolbarContainer = .Helpers.BootstrapDivColumn(10, 10, 10)
        'End With

        SaveButtonContainer = Helpers.BootstrapDivColumn(12, 12, 12)

        BackContainer = SaveButtonContainer.Helpers.BootstrapDivColumn(1, 1, 1)
        SaveContainer = SaveButtonContainer.Helpers.BootstrapDivColumn(1, 1, 1)


        BackButton = BackContainer.Helpers.BootstrapButton("BackToProduction", "Back", "btn-md btn-danger", "glyphicon-arrow-left", Singular.Web.PostBackType.Full, True, , , )
        SaveButton = SaveContainer.Helpers.BootstrapButton("Save", "Save", "btn-md btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.Full, False, , , )

        ToolbarContainer = SaveButtonContainer.Helpers.BootstrapDivColumn(10, 10, 10)

        Content = Helpers.BootstrapDivColumn(12, 12, 12) '.DivC("row")

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

  End Namespace

  Public Class SelectROProductionType(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Property ManagerExpression As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object))
    Public Property ListExpression As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object))
    Public Property CriteriaPropertyExpressions As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object))()

    Public Sub New(PagedGridManager As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object)),
                   ListObject As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object)),
                   CriteriaProperties() As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object)))

      Me.ManagerExpression = PagedGridManager
      Me.ListExpression = ListObject
      Me.CriteriaPropertyExpressions = CriteriaProperties

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Div
        .Attributes("id") = "ROProductionTypesModal"
        With .Helpers.Bootstrap.FlatBlock("Production Types", True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  For Each crit In CriteriaPropertyExpressions
                    With .Helpers.EditorFor(crit)
                      .AddClass("form-control input-sm")
                      .Attributes("placeholder") = "Search by Production Type"
                      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RefreshROProductionTypePagedList() }")
                    End With
                  Next
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                 Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                 Singular.Web.PostBackType.None, "RefreshROProductionTypePagedList()")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)

              End With
            End With
          End With
          With .ContentTag
            With .Helpers.DivC("table-responsive")
              With .Helpers.Bootstrap.PagedGridFor(Of ROProductionTypePaged)(ManagerExpression,
                                                                             ListExpression,
                                                                             False, False, False, False, True, True, False,
                                                                             "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                .Pager.PagerListTag.ListTag.AddClass("pull-left")
                With .FirstRow
                  .AddClass("items")
                  'With .AddColumn("")
                  '  .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                  '  With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , Singular.Web.PostBackType.None, "EditProductionType($data)")
                  '  End With
                  'End With
                  With .AddReadOnlyColumn(Function(d As ROProductionTypePaged) d.ProductionType)
                    .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                  End With
                  'With .AddReadOnlyColumn(Function(d As ROProductionTypePaged) d.ImportedInd)
                  '  .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                  '  .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                  'End With
                End With
                With .FooterRow

                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class SelectROProduction(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Property ManagerExpression As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object))
    Public Property ListExpression As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object))
    Public Property CriteriaPropertyExpressions As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object))()

    Public Sub New(PagedGridManager As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object)),
                   ListObject As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object)),
                   CriteriaProperties() As System.Linq.Expressions.Expression(Of System.Func(Of VMType, Object)))

      Me.ManagerExpression = PagedGridManager
      Me.ListExpression = ListObject
      Me.CriteriaPropertyExpressions = CriteriaProperties

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Div
        .Attributes("id") = "ROProductionsModal"
        With .Helpers.Bootstrap.FlatBlock("Find Production", True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            .AddClass("form-horizontal")
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.ProductionRefNo)
            '      .AddClass("form-control input-sm")
            '      .Attributes("placeholder") = "Reference Number"
            '    End With
            '  End With
            'End With
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateFrom)
            '      .AddClass("form-control input-sm")
            '      .Attributes("placeholder") = "Start Date"
            '    End With
            '  End With
            'End With
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateTo)
            '      .AddClass("form-control input-sm")
            '      .Attributes("placeholder") = "End Date"
            '    End With
            '  End With
            'End With
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventManagerID, "Event Manager", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
            '      .InputGroup.AddClass("margin-bottom-override")
            '      .Attributes("placeholder") = "Event Manager"
            '    End With
            '  End With
            'End With
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionTypeID, "Production Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
            '      .InputGroup.AddClass("margin-bottom-override")
            '      .Attributes("placeholder") = "Production Type"
            '    End With
            '  End With
            'End With
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.EventTypeID, "Event Type", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
            '      .InputGroup.AddClass("margin-bottom-override")
            '      .Attributes("placeholder") = "Event Type"
            '    End With
            '  End With
            'End With
            'With .Helpers.Bootstrap.Column(12, 6, 3, 3)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.Bootstrap.InputGroupCombo(Function(d) d.ROProductionListCriteria.ProductionVenueID, "Production Venue", , Singular.Web.BootstrapEnums.Style.Primary, "fa-caret-down")
            '      .InputGroup.AddClass("margin-bottom-override")
            '      .Attributes("placeholder") = "Production Venue"
            '    End With
            '  End With
            'End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                For Each crit In CriteriaPropertyExpressions
                  With .Helpers.EditorFor(Function(d) crit)
                    .AddClass("form-control input-sm")
                    .Attributes("placeholder") = "Search by Keyword"
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: RefreshROProductionList() }")
                  End With
                Next
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 3, 3)
              With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh",
                                             , Singular.Web.PostBackType.None, "RefreshROProductionList()")
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            '  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            '    With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", , Singular.Web.PostBackType.None, "Refresh()")
            '      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
            '    End With
            '    With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-eraser", , Singular.Web.PostBackType.None, "ClearCriteria()")
            '    End With
            '  End With
            'End With
          End With
          With .ContentTag
            With .Helpers.DivC("table-responsive")
              With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ManagerExpression,
                                                                    Function(vm) ListExpression,
                                                                    False, False, True, False, True, True, False,
                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                .Pager.PagerListTag.ListTag.AddClass("pull-left")
                With .FirstRow
                  .AddClass("items")
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionType)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.EventType)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionVenue)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  'With .AddReadOnlyColumn(Function(c) c.EventManager)
                  '  .AddClass("col-xs-1 col-sm-2 col-md-2 col-lg-1")
                  'End With
                  'With .AddReadOnlyColumn(Function(c) c.SynergyGenRefNo)
                  '  .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  'End With
                End With
                With .FooterRow
                  'With .AddColumn("")
                  '  .ColSpan = 2
                  '  .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                  '  With .Helpers.Bootstrap.Button(, "Add New", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , Singular.Web.PostBackType.None, "AddNewProductionType()")
                  '  End With
                  'End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace


