﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.ResourceSchedulers.New

Namespace Controls

  Public Class EditSchedulerModalNew(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IEditResourceScheduler)

    Private mSchedulerName As String = "window.Scheduler"

    Public Sub New(Optional SchedulerName As String = "window.Scheduler")
      mSchedulerName = SchedulerName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("EditResourceSchedulerModalNew", "Scheduler Settings", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-edit", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.With(Of ResourceScheduler)("ViewModel.ResourceScheduler()")
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , BootstrapEnums.TabAlignment.Left)
                  With .AddTab("GeneralSettings", "fa-cog", , "General", , )
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.ResourceScheduler).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.ResourceScheduler, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.ShowHRStats).Style.Width = "100%"
                            With .Helpers.Bootstrap.StateButton(Function(d As ResourceScheduler) d.ShowHRStats, , , , , , , "btn-sm btn-block")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.ShowResources).Style.Width = "100%"
                            With .Helpers.Bootstrap.StateButton(Function(d As ResourceScheduler) d.ShowResources, , , , , , , "btn-sm btn-block")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.MinViewGroupHeight).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.MinViewGroupHeight, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .AddTab("ResourceSettings", "fa-diamond", , "Resources", , )
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.TableFor(Of ResourceSchedulerGroup)("$data.ResourceSchedulerGroupList().sort(function(c, n) { return (c.GroupOrder() == n.GroupOrder() ? 0 : (c.GroupOrder() < n.GroupOrder() ? -1 : 1)) })",
                                                                                      True, True, False, True, True, True, True, "ResourceSchedulerGroupList", , )
                            With .FirstRow
                              With .AddColumn(Function(d As ResourceSchedulerGroup) d.ResourceSchedulerGroup)

                              End With
                              With .AddColumn(Function(d As ResourceSchedulerGroup) d.ResourceTypeID)

                              End With
                              With .AddColumn(Function(d As ResourceSchedulerGroup) d.SiteID)

                              End With
                              With .AddColumn(Function(d As ResourceSchedulerGroup) d.GroupOrder)

                              End With
                            End With
                            With .AddChildTable(Of ResourceSchedulerSubGroup)("$data.ResourceSchedulerSubGroupList().sort(function(c, n) { return (c.SubGroupOrder() == n.SubGroupOrder() ? 0 : (c.SubGroupOrder() < n.SubGroupOrder() ? -1 : 1)) })",
                                                                              True, True, False, True, True, True, True, , "ResourceSchedulerSubGroupList", , )
                              .Style.Width = "90%"
                              .Style.FloatRight()
                              With .FirstRow
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.ResourceSchedulerSubGroup)
                                End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.DisciplineID)
                                  .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                  .Editor.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 1")
                                End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.HRProductionAreaID)
                                  .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                  .Editor.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 1")
                                End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.SystemTeamNumberID)
                                  .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                  .Editor.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 1")
                                End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.Freelancers)
                                  .Editor.AddBinding(KnockoutBindingString.visible, Function(d) False)
                                  .Editor.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 1")
                                  With .Helpers.Bootstrap.TriStateButton("Freelancers", "Freelance", "Perm/FTC", "All",
                                                                         "btn-danger", "btn-info", "btn-default",
                                                                         , , , )
                                    .Button.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 1")
                                  End With
                                End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.RoomTypeID)
                                  .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 2")
                                  .Editor.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 2")
                                End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.EquipmentTypeID)
                                  .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 5")
                                  .Editor.AddBinding(KnockoutBindingString.enable, "$parent.ResourceTypeID() == 5")
                                End With
                                'With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.VehicleTypeID)
                                'End With
                                With .AddColumn(Function(d As ResourceSchedulerSubGroup) d.SubGroupOrder)
                                End With
                              End With
                              With .AddChildTable(Of ResourceSchedulerSubGroupResource)("$data.ResourceSchedulerSubGroupResourceList().sort(function(c, n) { return (c.ResourceOrder() == n.ResourceOrder() ? 0 : (c.ResourceOrder() < n.ResourceOrder() ? -1 : 1)) })",
                                                                                        False, True, False, True, True, True, True, , "ResourceSchedulerSubGroupResourceList", , )
                                .Style.Width = "90%"
                                .Style.FloatRight()
                                With .FirstRow
                                  With .AddReadOnlyColumn(Function(d As ResourceSchedulerSubGroupResource) d.ResourceName)

                                  End With
                                  With .AddColumn(Function(d As ResourceSchedulerSubGroupResource) d.ResourceOrder)

                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                        'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        '  With .Helpers.With(Of ResourceScheduler)("ViewModel.ResourceScheduler()")
                        '    'With .Helpers.Bootstrap.Button(, "New Group", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                        '    '                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                        '    '                               Singular.Web.PostBackType.None, mSchedulerName & ".addNewGroup($data)", )
                        '    'End With
                        '    'With .Helpers.Bootstrap.Button(, "Auto Calculate Order", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                        '    '                               Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-sort-alpha-asc", ,
                        '    '                               Singular.Web.PostBackType.None, mSchedulerName & ".autoCalculateSchedulerOrdering($data)", )
                        '    'End With
                        '  End With
                        'End With
                        'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        '  With .Helpers.With(Of OBLib.ResourceSchedulers.[New].ResourceScheduler)("ViewModel.ResourceScheduler()")
                        '    With .Helpers.DivC("tree")
                        '      'Groups-----------------------------------------------------------------------------------------------
                        '      '-----------------------------------------------------------------------------------------------------
                        '      With .Helpers.HTMLTag("ul")
                        '        .Attributes("id") = "groupList"
                        '        .AddClass("nav nav-list treeview scheduler-groups")
                        '        With .Helpers.ForEach(Of ResourceSchedulerGroup)("$data.ResourceSchedulerGroupList()")
                        '          With .Helpers.HTMLTag("li")
                        '            .AddClass("orderable-group")
                        '            With .Helpers.HTMLTag("label")
                        '              .AddClass("tree-toggler nav-header hvr-underline-from-center")
                        '              'With .Helpers.DivC("li-group-div")
                        '              'Expand Icon
                        '              With .Helpers.HTMLTag("span")
                        '                '.AddBinding(KnockoutBindingString.click, mSchedulerName & ".liIconClicked($data, $element)")
                        '                .AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
                        '                .AddClass("btn btn-xs btn-default")
                        '                With .Helpers.HTMLTag("i")
                        '                  .AddClass("fa fa-folder-o toggle-icon")
                        '                End With
                        '              End With
                        '              'Group Name
                        '              With .Helpers.HTMLTag("span")
                        '                .Style.Width = "150px"
                        '                .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.ResourceSchedulerGroup, "Group")
                        '              End With
                        '              With .Helpers.HTMLTag("span")
                        '                .Style.Width = "150px"
                        '                .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.ResourceTypeID, "Type")
                        '              End With
                        '              With .Helpers.HTMLTag("span")
                        '                .Style.Width = "150px"
                        '                .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.SiteID, "Site")
                        '              End With
                        '              With .Helpers.HTMLTag("span")
                        '                .Style.Width = "150px"
                        '                With .Helpers.Bootstrap.StateButton(Function(d As ResourceSchedulerGroup) d.SelectedByDefault,
                        '                                                    "Shown on Open", "Shown on Open", "btn-success", , , , )
                        '                End With
                        '              End With
                        '              With .Helpers.HTMLTag("span")
                        '                .Style.Width = "150px"
                        '                'With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                        '                '                              BootstrapEnums.ButtonSize.ExtraSmall, ,
                        '                '                              "fa-trash", , PostBackType.None, mSchedulerName & ".removeSchedulerGroup($data)", )
                        '                'End With
                        '              End With
                        '              With .Helpers.HTMLTag("span")
                        '                .Style.Width = "150px"
                        '                'With .Helpers.Bootstrap.Button(, "New Sub Group", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                        '                '                              Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus-circle", ,
                        '                '                              Singular.Web.PostBackType.None, mSchedulerName & ".addNewSubGroup($data)", )
                        '                'End With
                        '              End With
                        '              If Singular.Debug.InDebugMode Or OBLib.Security.Settings.CurrentUserID = 1 Then
                        '                With .Helpers.HTMLTag("span")
                        '                  .Style.Width = "20px"
                        '                  With .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.GroupOrder, "Order")
                        '                    .Style.Width = "20px"
                        '                  End With
                        '                End With
                        '              End If
                        '            End With
                        '            'SubGroups-----------------------------------------------------------------------------------------------
                        '            '--------------------------------------------------------------------------------------------------------
                        '            With .Helpers.HTMLTag("ul")
                        '              .AddClass("nav nav-list tree scheduler-subgroups")
                        '              With .Helpers.ForEach(Of ResourceSchedulerSubGroup)("$data.ResourceSchedulerSubGroupList()")
                        '                With .Helpers.HTMLTag("li")
                        '                  .AddClass("orderable-subgroup")
                        '                  '.AddBinding(KnockoutBindingString.click, mSchedulerName & ".liIconClicked($data, $element)")
                        '                  With .Helpers.HTMLTag("label")
                        '                    .AddClass("tree-toggler nav-header hvr-underline-from-center")
                        '                    'With .Helpers.DivC("li-subgroup-div")
                        '                    'Expand Icon
                        '                    With .Helpers.HTMLTag("span")
                        '                      '.AddBinding(KnockoutBindingString.click, mSchedulerName & ".liIconClicked($data, $element)")
                        '                      .AddClass("btn btn-xs btn-default")
                        '                      With .Helpers.HTMLTag("i")
                        '                        .AddClass("fa fa-folder-o toggle-icon")
                        '                      End With
                        '                    End With
                        '                    'With .Helpers.HTMLTag("span")
                        '                    '  .Style.Width = "150px"
                        '                    '  With .Helpers.Bootstrap.Button(, "Repopulate", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                        '                    '                                 Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-refresh", ,
                        '                    '                                 Singular.Web.PostBackType.None, mSchedulerName & ".repopulateSubGroup($data)", )
                        '                    '  End With
                        '                    'End With
                        '                    'SubGroup Name
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.ResourceSchedulerSubGroup, "SubGroup")
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.DisciplineID, "Discipline")
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 2")
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.RoomTypeID, "Type")
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.HRProductionAreaID, "Area")
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 5")
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.EquipmentTypeID, "Type")
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                        '                      With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, ,
                        '                                             BootstrapEnums.ButtonSize.ExtraSmall, , "fa-minus", ,
                        '                                             PostBackType.None, "ResourceSchedulerSubGroupBO.FreelancersClicked($data)", )
                        '                        .ButtonText.AddBinding(KnockoutBindingString.html, "ResourceSchedulerSubGroupBO.FreelancersButtonText($data)")
                        '                        .Icon.IconContainer.AddBinding(KnockoutBindingString.css, "ResourceSchedulerSubGroupBO.FreelancersIconCss($data)")
                        '                        '.Button.AddClass("btn-block")
                        '                      End With
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 6")
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.CustomResourceID, "Resource")
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 3")
                        '                      .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.VehicleTypeID, "Type")
                        '                    End With
                        '                    'CustomResourceID
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      With .Helpers.Bootstrap.StateButton(Function(d As ResourceSchedulerSubGroup) d.SelectedByDefault,
                        '                                                          "Shown on Open", "Shown on Open", "btn-success", , , , )
                        '                      End With
                        '                    End With
                        '                    With .Helpers.HTMLTag("span")
                        '                      .Style.Width = "150px"
                        '                      'With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                        '                      '                              BootstrapEnums.ButtonSize.ExtraSmall, ,
                        '                      '                              "fa-trash", , PostBackType.None, mSchedulerName & ".removeSchedulerSubGroup($data)", )
                        '                      'End With
                        '                    End With
                        '                    If Singular.Debug.InDebugMode Or OBLib.Security.Settings.CurrentUserID = 1 Then
                        '                      With .Helpers.HTMLTag("span")
                        '                        .Style.Width = "20px"
                        '                        With .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.SubGroupOrder, "Sub Group")
                        '                          .Style.Width = "20px"
                        '                        End With
                        '                      End With
                        '                    End If
                        '                  End With
                        '                  'SubGroupResources-----------------------------------------------------------------------------------------------
                        '                  '----------------------------------------------------------------------------------------------------------------
                        '                  With .Helpers.HTMLTag("ul")
                        '                    .AddClass("nav nav-list tree scheduler-subgroupresources")
                        '                    With .Helpers.ForEach(Of ResourceSchedulerSubGroupResource)("$data.ResourceSchedulerSubGroupResourceList()")
                        '                      With .Helpers.HTMLTag("li")
                        '                        .AddClass("orderable-subgroupresource")
                        '                        'Expand Icon
                        '                        With .Helpers.HTMLTag("label")
                        '                          .AddClass("subgroupresource hvr-underline-from-center")
                        '                          With .Helpers.Bootstrap.ButtonGroup
                        '                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                        '                                                           Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                        '                                                           "fa-android", , Singular.Web.PostBackType.None, , )
                        '                              .ButtonText.AddBinding(KnockoutBindingString.text, "$data.ResourceName()")
                        '                              .Button.Style.Width = "300px"
                        '                            End With
                        '                            With .Helpers.Bootstrap.StateButton(Function(d As ResourceSchedulerSubGroupResource) d.SelectedByDefault,
                        '                                                                "Shown on Open", "Shown on Open", "btn-success", , , , )
                        '                            End With
                        '                            'With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                        '                            '                              BootstrapEnums.ButtonSize.ExtraSmall, ,
                        '                            '                              "fa-trash", , PostBackType.None, mSchedulerName & ".removeSchedulerResource($data)", )
                        '                            'End With
                        '                            If Singular.Debug.InDebugMode Or OBLib.Security.Settings.CurrentUserID = 1 Then
                        '                              With .Helpers.HTMLTag("span")
                        '                                .Style.Width = "30px"
                        '                                With .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroupResource) d.ResourceOrder, "Order")
                        '                                  .Style.Width = "30px"
                        '                                End With
                        '                              End With
                        '                            End If
                        '                          End With
                        '                        End With
                        '                      End With
                        '                    End With
                        '                  End With
                        '                End With
                        '              End With
                        '            End With
                        '          End With
                        '        End With
                        '      End With
                        '    End With
                        '  End With
                        'End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.PullRight
            With .Helpers.With(Of ResourceScheduler)("ViewModel.ResourceScheduler()")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, ,
                                             BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, "ResourceSchedulerBO.saveModal($data)", )
                'mSchedulerName & ".saveResourceScheduler($data)"
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.ResourceScheduler().IsValid()")
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace