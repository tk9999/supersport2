﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.Copying

Namespace Controls

  Public Class CopyCrewControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mControlName As String

    Public Sub New(Optional ControlName As String = "copyCrewControl")
      mControlName = ControlName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("CopyCrewModal", "Copy Crew", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-copy", , True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.Bootstrap.FlatBlock("From")

            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.Bootstrap.FlatBlock("To")

            End With
          End With
          'With .Helpers.With(Of CopySetting)("ViewModel.CopySetting()")
          '  With .Helpers.Bootstrap.Row
          '    '.AddBinding(KnockoutBindingString.visible, "$data.CopyToDates()")
          '    'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          '    '  With .Helpers.Bootstrap.FlatBlock("Booking")
          '    '    With .ContentTag
          '    '      With .Helpers.Bootstrap.Row
          '    '        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Bootstrap.LabelFor(Function(d As CopySetting) d.EquipmentID)
          '    '            .Helpers.Bootstrap.FormControlFor(Function(d As CopySetting) d.EquipmentID, BootstrapEnums.InputSize.Small)
          '    '          End With
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Bootstrap.LabelFor(Function(d As CopySetting) d.ResourceBookingDescription)
          '    '            .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As CopySetting) d.ResourceBookingDescription, BootstrapEnums.InputSize.Small)
          '    '          End With
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Bootstrap.LabelFor(Function(d As CopySetting) d.StartDateTimeBuffer)
          '    '            .Helpers.Bootstrap.TimeEditorFor(Function(d As CopySetting) d.StartDateTimeBuffer, BootstrapEnums.InputSize.Small)
          '    '          End With
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Bootstrap.LabelFor(Function(d As CopySetting) d.StartDateTime)
          '    '            .Helpers.Bootstrap.TimeEditorFor(Function(d As CopySetting) d.StartDateTime, BootstrapEnums.InputSize.Small)
          '    '          End With
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Bootstrap.LabelFor(Function(d As CopySetting) d.EndDateTime)
          '    '            .Helpers.Bootstrap.TimeEditorFor(Function(d As CopySetting) d.EndDateTime, BootstrapEnums.InputSize.Small)
          '    '          End With
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Bootstrap.LabelFor(Function(d As CopySetting) d.EndDateTimeBuffer)
          '    '            .Helpers.Bootstrap.TimeEditorFor(Function(d As CopySetting) d.EndDateTimeBuffer, BootstrapEnums.InputSize.Small)
          '    '          End With
          '    '        End With
          '    '      End With
          '    '    End With
          '    '  End With
          '    'End With
          '    'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          '    '  With .Helpers.Bootstrap.FlatBlock("Dates")
          '    '    With .ContentTag
          '    '      With .Helpers.Bootstrap.Row
          '    '        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            .Helpers.Div.Attributes("id") = "multiSelectDate" '.Helpers.Bootstrap.FormControlFor(Function(d As CopySetting) d.SelectDate, BootstrapEnums.InputSize.Small, , )
          '    '          End With
          '    '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '    '            With .Helpers.Bootstrap.Button(, "Submit", BootstrapEnums.Style.Primary, ,
          '    '                                           BootstrapEnums.ButtonSize.Small, , "fa-thumbs-up",
          '    '                                           , PostBackType.None, mControlName & ".submitCopy($data)", False)
          '    '              .Button.AddClass("btn-block")
          '    '            End With
          '    '          End With
          '    '        End With
          '    '      End With
          '    '    End With
          '    '  End With
          '    'End With
          '  End With
          '  'With .Helpers.Bootstrap.Row
          '  '  .AddBinding(KnockoutBindingString.visible, "$data.RepeatBooking()")
          '  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '  '    With .Helpers.Bootstrap.LabelDisplay("Repeat Every")
          '  '      .Style.Width = "100%"
          '  '    End With
          '  '    With .Helpers.Div
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Monday, "Monday", "Monday", , , , , )
          '  '      End With
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Tuesday, "Tuesday", "Tuesday", , , , , )
          '  '      End With
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Wednesday, "Wednesday", "Wednesday", , , , , )
          '  '      End With
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Thursday, "Thursday", "Thursday", , , , , )
          '  '      End With
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Friday, "Friday", "Friday", , , , , )
          '  '      End With
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Saturday, "Saturday", "Saturday", , , , , )
          '  '      End With
          '  '      With .Helpers.Bootstrap.StateButton(Function(d As CopySetting) d.Sunday, "Sunday", "Sunday", , , , , )
          '  '      End With
          '  '    End With
          '  '  End With
          '  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '  '    With .Helpers.Bootstrap.LabelDisplay("Repeat Until")
          '  '      .Style.Width = "100%"
          '  '    End With
          '  '    .Helpers.Bootstrap.FormControlFor(Function(d As CopySetting) d.RepeatUntil, BootstrapEnums.InputSize.Small, , )
          '  '  End With
          '  'End With
          '  With .Helpers.DivC("loading-custom")
          '    .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
          '    .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
          '  End With
          'End With
        End With
        'With .Footer
        '  With .Helpers.DivC("pull-right")

        '  End With
        'End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
