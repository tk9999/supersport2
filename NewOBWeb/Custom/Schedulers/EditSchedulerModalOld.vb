﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.ResourceSchedulers.New

Namespace Controls

  Public Class EditSchedulerModalOld(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditResourceScheduler)

    Private mSchedulerName As String = "window.Scheduler"

    Public Sub New(Optional SchedulerName As String = "window.Scheduler")
      mSchedulerName = SchedulerName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("EditResourceSchedulerModal", "Edit Scheduler", False, "modal-xl", BootstrapEnums.Style.Primary, , "fa-edit", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
              With .Helpers.With(Of ResourceScheduler)("ViewModel.EditResourceScheduler()")
                With .Helpers.Bootstrap.FlatBlock("Scheduler", , , , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.ResourceScheduler).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.ResourceScheduler, BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.ShowHRStats).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As ResourceScheduler) d.ShowHRStats, , , , , , , "btn-sm btn-block")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.ShowResources).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As ResourceScheduler) d.ShowResources, , , , , , , "btn-sm btn-block")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.MinViewGroupHeight).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.MinViewGroupHeight, BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                    If OBLib.Security.Settings.CurrentUserID = 1 Or Singular.Debug.InDebugMode Then
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.DefaultDateViewTypeID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.DefaultDateViewTypeID, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.SystemID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.SystemID, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As ResourceScheduler) d.PageName).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As ResourceScheduler) d.PageName, BootstrapEnums.InputSize.Small)
                            End With
                          End With
                        End With
                      End With
                    End If
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 10, 10)
              With .Helpers.Bootstrap.FlatBlock("Groups etc", , , , )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of ResourceScheduler)("ViewModel.EditResourceScheduler()")
                        With .Helpers.Bootstrap.Button(, "New Group", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                                       Singular.Web.PostBackType.None, mSchedulerName & ".addNewGroup($data)", )
                        End With
                        With .Helpers.Bootstrap.Button(, "Auto Calculate Order", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-sort-alpha-asc", ,
                                                       Singular.Web.PostBackType.None, mSchedulerName & ".autoCalculateSchedulerOrdering($data)", )
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of OBLib.ResourceSchedulers.[New].ResourceScheduler)("ViewModel.EditResourceScheduler()")
                        With .Helpers.DivC("tree")
                          'Groups-----------------------------------------------------------------------------------------------
                          '-----------------------------------------------------------------------------------------------------
                          With .Helpers.HTMLTag("ul")
                            .Attributes("id") = "groupList"
                            .AddClass("nav nav-list treeview scheduler-groups")
                            With .Helpers.ForEach(Of ResourceSchedulerGroup)("$data.ResourceSchedulerGroupList()")
                              With .Helpers.HTMLTag("li")
                                .AddClass("orderable-group")
                                With .Helpers.HTMLTag("label")
                                  .AddClass("tree-toggler nav-header hvr-underline-from-center")
                                  'With .Helpers.DivC("li-group-div")
                                  'Expand Icon
                                  With .Helpers.HTMLTag("span")
                                    .AddBinding(KnockoutBindingString.click, mSchedulerName & ".liIconClicked($data, $element)")
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
                                    .AddClass("btn btn-xs btn-default")
                                    With .Helpers.HTMLTag("i")
                                      .AddClass("fa fa-folder-o toggle-icon")
                                    End With
                                  End With
                                  'Group Name
                                  With .Helpers.HTMLTag("span")
                                    .Style.Width = "150px"
                                    .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.ResourceSchedulerGroup, "Group")
                                  End With
                                  With .Helpers.HTMLTag("span")
                                    .Style.Width = "150px"
                                    .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.ResourceTypeID, "Type")
                                  End With
                                  With .Helpers.HTMLTag("span")
                                    .Style.Width = "150px"
                                    .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.SiteID, "Site")
                                  End With
                                  With .Helpers.HTMLTag("span")
                                    .Style.Width = "150px"
                                    With .Helpers.Bootstrap.StateButton(Function(d As ResourceSchedulerGroup) d.SelectedByDefault,
                                                                        "Shown on Open", "Shown on Open", "btn-success", , , , )
                                    End With
                                  End With
                                  With .Helpers.HTMLTag("span")
                                    .Style.Width = "150px"
                                    With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                                                                  BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                                  "fa-trash", , PostBackType.None, mSchedulerName & ".removeSchedulerGroup($data)", )
                                    End With
                                  End With
                                  With .Helpers.HTMLTag("span")
                                    .Style.Width = "150px"
                                    With .Helpers.Bootstrap.Button(, "New Sub Group", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                                  Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus-circle", ,
                                                                  Singular.Web.PostBackType.None, mSchedulerName & ".addNewSubGroup($data)", )
                                    End With
                                  End With
                                  If Singular.Debug.InDebugMode Or OBLib.Security.Settings.CurrentUserID = 1 Then
                                    With .Helpers.HTMLTag("span")
                                      .Style.Width = "20px"
                                      With .Helpers.EditorFor(Function(d As ResourceSchedulerGroup) d.GroupOrder, "Order")
                                        .Style.Width = "20px"
                                      End With
                                    End With
                                  End If
                                  'End With
                                End With
                                'SubGroups-----------------------------------------------------------------------------------------------
                                '--------------------------------------------------------------------------------------------------------
                                With .Helpers.HTMLTag("ul")
                                  .AddClass("nav nav-list tree scheduler-subgroups")
                                  With .Helpers.ForEach(Of ResourceSchedulerSubGroup)("$data.ResourceSchedulerSubGroupList()")
                                    With .Helpers.HTMLTag("li")
                                      .AddClass("orderable-subgroup")
                                      .AddBinding(KnockoutBindingString.click, mSchedulerName & ".liIconClicked($data, $element)")
                                      With .Helpers.HTMLTag("label")
                                        .AddClass("tree-toggler nav-header hvr-underline-from-center")
                                        'With .Helpers.DivC("li-subgroup-div")
                                        'Expand Icon
                                        With .Helpers.HTMLTag("span")
                                          .AddBinding(KnockoutBindingString.click, mSchedulerName & ".liIconClicked($data, $element)")
                                          .AddClass("btn btn-xs btn-default")
                                          With .Helpers.HTMLTag("i")
                                            .AddClass("fa fa-folder-o toggle-icon")
                                          End With
                                        End With
                                        'With .Helpers.HTMLTag("span")
                                        '  .Style.Width = "150px"
                                        '  With .Helpers.Bootstrap.Button(, "Repopulate", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                        '                                 Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-refresh", ,
                                        '                                 Singular.Web.PostBackType.None, mSchedulerName & ".repopulateSubGroup($data)", )
                                        '  End With
                                        'End With
                                        'SubGroup Name
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.ResourceSchedulerSubGroup, "SubGroup")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.DisciplineID, "Discipline")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 2")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.RoomTypeID, "Type")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.HRProductionAreaID, "Area")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.SystemTeamNumberID, "Team")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 5")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.EquipmentTypeID, "Type")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 1")
                                          With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, ,
                                                                 BootstrapEnums.ButtonSize.ExtraSmall, , "fa-minus", ,
                                                                 PostBackType.None, "ResourceSchedulerSubGroupBO.FreelancersClicked($data)", )
                                            .ButtonText.AddBinding(KnockoutBindingString.html, "ResourceSchedulerSubGroupBO.FreelancersButtonText($data)")
                                            .Icon.IconContainer.AddBinding(KnockoutBindingString.css, "ResourceSchedulerSubGroupBO.FreelancersIconCss($data)")
                                            '.Button.AddClass("btn-block")
                                          End With
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 6")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.CustomResourceID, "Resource")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          .AddBinding(KnockoutBindingString.visible, "$parent.ResourceTypeID() == 3")
                                          .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.VehicleTypeID, "Type")
                                        End With
                                        'CustomResourceID
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          With .Helpers.Bootstrap.StateButton(Function(d As ResourceSchedulerSubGroup) d.SelectedByDefault,
                                                                              "Shown on Open", "Shown on Open", "btn-success", , , , )
                                          End With
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Style.Width = "150px"
                                          With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                                                                        BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                                        "fa-trash", , PostBackType.None, mSchedulerName & ".removeSchedulerSubGroup($data)", )
                                          End With
                                        End With
                                        If Singular.Debug.InDebugMode Or OBLib.Security.Settings.CurrentUserID = 1 Then
                                          With .Helpers.HTMLTag("span")
                                            .Style.Width = "20px"
                                            With .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.SubGroupOrder, "Sub Group")
                                              .Style.Width = "20px"
                                            End With
                                          End With
                                        End If
                                        'End With
                                      End With
                                      'SubGroupResources-----------------------------------------------------------------------------------------------
                                      '----------------------------------------------------------------------------------------------------------------
                                      With .Helpers.HTMLTag("ul")
                                        .AddClass("nav nav-list tree scheduler-subgroupresources")
                                        With .Helpers.ForEach(Of ResourceSchedulerSubGroupResource)("$data.ResourceSchedulerSubGroupResourceList()")
                                          With .Helpers.HTMLTag("li")
                                            .AddClass("orderable-subgroupresource")
                                            'Expand Icon
                                            With .Helpers.HTMLTag("label")
                                              .AddClass("subgroupresource hvr-underline-from-center")
                                              With .Helpers.Bootstrap.ButtonGroup
                                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                                               "fa-android", , Singular.Web.PostBackType.None, , )
                                                  .ButtonText.AddBinding(KnockoutBindingString.text, "$data.ResourceName()")
                                                  .Button.Style.Width = "300px"
                                                End With
                                                With .Helpers.Bootstrap.StateButton(Function(d As ResourceSchedulerSubGroupResource) d.SelectedByDefault,
                                                                                    "Shown on Open", "Shown on Open", "btn-success", , , , )
                                                End With
                                                With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                                                                              BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                                              "fa-trash", , PostBackType.None, mSchedulerName & ".removeSchedulerResource($data)", )
                                                End With
                                                If Singular.Debug.InDebugMode Or OBLib.Security.Settings.CurrentUserID = 1 Then
                                                  With .Helpers.HTMLTag("span")
                                                    .Style.Width = "30px"
                                                    With .Helpers.EditorFor(Function(d As ResourceSchedulerSubGroupResource) d.ResourceOrder, "Order")
                                                      .Style.Width = "30px"
                                                    End With
                                                  End With
                                                End If
                                              End With
                                              '.AddClass("btn btn-xs btn-default")
                                              'With .Helpers.HTMLTag("i")
                                              '  .AddClass("fa fa-android")
                                              'End With
                                              ''SubGroup Name
                                              'With .Helpers.HTMLTag("span")
                                              '  .Style.Width = "150px"
                                              '  '.Helpers.EditorFor(Function(d As ResourceSchedulerSubGroup) d.ResourceSchedulerSubGroup, "SubGroup")
                                              '  'With .Helpers.HTMLTag("label")
                                              '  .AddBinding(KnockoutBindingString.text, "$data.ResourceName()")
                                              '  'End With
                                              'End With
                                            End With
                                          End With
                                        End With
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.PullRight
            With .Helpers.With(Of ResourceScheduler)("ViewModel.EditResourceScheduler()")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, ,
                                             BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, mSchedulerName & ".saveResourceScheduler($data)", )
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.EditResourceScheduler().IsValid()")
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  'With Helpers.Bootstrap.Dialog("EditSchedulerModal", "Edit Scheduler", False, "modal-xl", BootstrapEnums.Style.Primary, , "fa-edit", "fa-2x")
  '  '  '.Heading.AddBinding(KnockoutBindingString.html, mSchedulerName & ".getResourceBookingCleanDescription()")
  '  With .Body
  '    With .Helpers.With(Of OBLib.ResourceSchedulers.[New].ResourceScheduler)("ViewModel.EditResourceScheduler()")
  '      With .Helpers.Bootstrap.Row
  '        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 2)
  '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.ResourceSchedulers.[New].ResourceScheduler) d.ResourceScheduler)
  '            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.ResourceSchedulers.[New].ResourceScheduler) d.ResourceScheduler, BootstrapEnums.InputSize.Small)

  '            End With
  '          End With
  '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.ResourceSchedulers.[New].ResourceScheduler) d.DefaultDateViewType)
  '            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.ResourceSchedulers.[New].ResourceScheduler) d.DefaultDateViewType, BootstrapEnums.InputSize.Small)
  '            End With
  '          End With
  '          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '            .Helpers.Bootstrap.LabelDisplay("What Would you like to do?").LabelTag.Style.Width = "100%"
  '            With .Helpers.Bootstrap.ButtonGroup
  '              '.Helpers.Bootstrap.Button(, "Manage Users", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-users", , PostBackType.None, "ResourceSchedulerBO.editUsers($data)")
  '              .Helpers.Bootstrap.Button(, "Manage Groups", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-users", , PostBackType.None, "ResourceSchedulerBO.editGroups($data)")
  '            End With
  '          End With
  '        End With
  '        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
  '          With .Helpers.HTMLTag("ul")
  '            .AddClass("nav nav-list treeview")
  '            .Attributes("id") = "groups"
  '            With .Helpers.ForEach(Of ResourceSchedulerGroup)("$data.ResourceSchedulerGroupList()")
  '              With .Helpers.HTMLTag("li")
  '                .AddBinding(KnockoutBindingString.attr, "{ id: $data.ResourceSchedulerGroupID() }")
  '                With .Helpers.HTMLTag("label")
  '                  .AddClass("tree-toggler nav-header")
  '                  With .Helpers.HTMLTag("i")
  '                    .AddClass("fa fa-folder-o")
  '                  End With
  '                  With .Helpers.HTMLTag("span")
  '                    .AddBinding(KnockoutBindingString.html, "$data.ResourceSchedulerGroup()")
  '                  End With
  '                End With
  '                With .Helpers.HTMLTag("ul")
  '                  .AddClass("nav nav-list tree")
  '                  .Attributes("id") = "subgroups"
  '                  With .Helpers.ForEach(Of ResourceSchedulerSubGroup)("$data.ResourceSchedulerSubGroupList()")
  '                    With .Helpers.HTMLTag("li")
  '                      .AddBinding(KnockoutBindingString.attr, "{ id: $data.ResourceSchedulerSubGroupID() }")
  '                      With .Helpers.HTMLTag("label")
  '                        .AddClass("tree-toggler nav-header")
  '                        With .Helpers.HTMLTag("i")
  '                          .AddClass("fa fa-folder-o")
  '                        End With
  '                        With .Helpers.HTMLTag("span")
  '                          .AddBinding(KnockoutBindingString.html, "$data.ResourceSchedulerSubGroup()")
  '                        End With
  '                      End With
  '                      With .Helpers.HTMLTag("ul")
  '                        .Attributes("id") = "subgroupresources"
  '                        .AddClass("nav nav-list tree")
  '                        With .Helpers.ForEach(Of ResourceSchedulerSubGroupResource)("$data.ResourceSchedulerSubGroupResourceList()")
  '                          With .Helpers.HTMLTag("li")
  '                            .AddClass("sub-group-resource")
  '                            .AddBinding(KnockoutBindingString.attr, "{ id: $data.ResourceSchedulerSubGroupResourceID() }")
  '                            With .Helpers.HTMLTag("a")
  '                              .Attributes("href") = "#"
  '                              .AddBinding(KnockoutBindingString.html, "$data.ResourceName()")
  '                            End With
  '                          End With
  '                        End With
  '                      End With
  '                    End With
  '                  End With
  '                End With
  '              End With
  '            End With
  '          End With
  '        End With
  '        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)

  '        End With
  '      End With
  '    End With
  '  End With
  '  With .Footer
  '  End With
  'End With

  'With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.ResourceSchedulers.ResourceScheduler) d.CanImportFromSynergy).Style.Width = "100%"
  '  With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.ResourceSchedulers.ResourceScheduler) d.CanImportFromSynergy)
  '  End With
  'End With
  'With .Helpers.Div
  '  .AddClass("animated slideInDown go")
  '  .Attributes("id") = "RSGroupList"
  '  .AddBinding(KnockoutBindingString.if, "!ViewModel.SelectedGroup()")
  '  With .Helpers.Div
  '    .Helpers.Bootstrap.Button(, "New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , PostBackType.None, "ResourceSchedulerBO.newGroup($data)", False)
  '  End With
  '  With .Helpers.FieldSet("Groups")
  '    With .Helpers.Bootstrap.TableFor(Of OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup)("ViewModel.EditResourceScheduler().ResourceSchedulerGroupList()", False, False, False, False, True, True, True)
  '      .AddClass("hover list")
  '      With .FirstRow
  '        .AddClass("items")
  '        With .AddColumn("")
  '          .Style.Width = "60px"
  '          With .Helpers.Bootstrap.Button(, "Sub-Groups", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, "ResourceSchedulerBO.editGroup($data, $element)")
  '            .Button.AddBinding(KnockoutBindingString.enable, "!$data.IsNew()")
  '          End With
  '        End With
  '        With .AddColumn(Function(d As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup) d.ResourceSchedulerGroup)

  '        End With
  '        With .AddColumn(Function(d As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup) d.SystemID)

  '        End With
  '        With .AddColumn(Function(d As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup) d.ProductionAreaID)

  '        End With
  '        With .AddColumn(Function(d As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup) d.ResourceTypeID)

  '        End With
  '        With .AddColumn(Function(d As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup) d.GroupOrder)

  '        End With
  '        With .AddColumn("Default?")
  '          .Helpers.Bootstrap.StateButton(Function(d As OBLib.ResourceSchedulers.[New].ResourceSchedulerGroup) d.SelectedByDefault)
  '        End With
  '        With .AddColumn("")
  '          .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash", , PostBackType.None, "ResourceSchedulerBO.deleteGroup($data)", False)
  '          .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , PostBackType.None, "ResourceSchedulerBO.saveGroup($data)", False)
  '        End With
  '      End With
  '    End With
  '  End With
  'End With
  'With .Helpers.With(Of OBLib.ResourceSchedulers.New.ResourceSchedulerGroup)("ViewModel.SelectedGroup()")
  '  With .Helpers.Div
  '    .AddClass("animated slideInRight go")
  '    .Attributes("id") = "RSSubGroupList"
  '    .AddBinding(KnockoutBindingString.if, "!ViewModel.SelectedSubGroup()")
  '    With .Helpers.Div
  '      .Helpers.Bootstrap.Button(, "Back", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Small, , "fa-arrow-left", , PostBackType.None, "ResourceSchedulerBO.backToGroups()", False)
  '      .Helpers.Bootstrap.Button(, "New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , PostBackType.None, "ResourceSchedulerBO.newSubGroup($data)", False)
  '    End With
  '    With .Helpers.FieldSet("Sub Groups")
  '      With .Helpers.Bootstrap.TableFor(Of OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup)("$data.ResourceSchedulerSubGroupList()", False, False, False, False, True, True, True)
  '        .AddClass("hover list")
  '        With .FirstRow
  '          .AddClass("items")
  '          With .AddColumn("")
  '            .Style.Width = "60px"
  '            .Helpers.Bootstrap.Button(, "Resources", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, "ResourceSchedulerBO.editSubGroup($data, $element)")
  '          End With
  '          With .AddColumn(Function(d As OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup) d.ResourceSchedulerSubGroup)

  '          End With
  '          With .AddColumn(Function(d As OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup) d.DisciplineID)

  '          End With
  '          With .AddColumn(Function(d As OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup) d.ContractTypeID)

  '          End With
  '          With .AddColumn(Function(d As OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup) d.RoomTypeID)

  '          End With
  '          With .AddColumn(Function(d As OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup) d.SubGroupOrder)

  '          End With
  '          With .AddColumn("Default?")
  '            .Helpers.Bootstrap.StateButton(Function(d As OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroup) d.SelectedByDefault)
  '          End With
  '          With .AddColumn("")
  '            .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash", , PostBackType.None, "ResourceSchedulerBO.deleteSubGroup($data)", False)
  '            .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-floppy-o", , PostBackType.None, "ResourceSchedulerBO.saveSubGroup($data)", False)
  '          End With
  '        End With
  '      End With
  '    End With
  '  End With
  'End With
  '          With .Helpers.With(Of OBLib.ResourceSchedulers.RSGroup)("ViewModel.SelectedRSSubGroup()")
  '            With .Helpers.Div
  '              .AddClass("animated slideInRight go")
  '              .Attributes("id") = "RSSubGroupResourceList"
  '              .AddBinding(KnockoutBindingString.if, "ViewModel.SelectedRSSubGroup()")
  '              With .Helpers.Div
  '                With .Helpers.Bootstrap.Button(, "Back", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Small, , "fa-arrow-left", , PostBackType.None, "ResourceSchedulerBO.backToSubGroups()", False)
  '                  .Button.AddBinding(KnockoutBindingString.enable, "!$data.IsNew()")
  '                End With
  '              End With
  '              With .Helpers.FieldSet("Sub Group Resources")
  '                With .Helpers.Bootstrap.TableFor(Of OBLib.ResourceSchedulers.RSSubGroupResource)("$data.RSSubGroupResourceList()", False, False, False, False, True, True, True)
  '                  .AddClass("hover list")
  '                  With .FirstRow
  '                    .AddClass("items")
  '                    With .AddColumn("")
  '                      .Style.Width = "60px"
  '                      .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, "")
  '                    End With
  '                    With .AddColumn(Function(d As OBLib.ResourceSchedulers.RSSubGroupResource) d.ResourceID)

  '                    End With
  '                    With .AddColumn(Function(d As OBLib.ResourceSchedulers.RSSubGroupResource) d.ResourceName)

  '                    End With
  '                    With .AddColumn(Function(d As OBLib.ResourceSchedulers.RSSubGroupResource) d.ResourceOrder)

  '                    End With
  '                    With .AddColumn("Default?")
  '                      .Helpers.Bootstrap.StateButton(Function(d As OBLib.ResourceSchedulers.RSSubGroupResource) d.SelectedByDefault)
  '                    End With
  '                  End With
  '                End With
  '              End With
  '            End With
  '          End With
  '        End With

  '  With .Helpers.ForEach(Of ApplyResult)("ViewModel.ApplyResultList()")
  '    With .Helpers.DivC("alert")
  '      .Attributes("role") = "alert"
  '      .AddBinding(KnockoutBindingString.css, mSchedulerName & ".applyResultCss($data)")
  '      With .Helpers.HTMLTag("span")
  '        .AddClass("icon")
  '        With .Helpers.HTMLTag("i")
  '          .AddBinding(KnockoutBindingString.css, mSchedulerName & ".applyResultIconCss($data)")
  '        End With
  '      End With
  '      With .Helpers.DivC("alert-header")
  '        '.AddClass("alert-header")
  '        With .Helpers.HTMLTag("strong")
  '          .AddBinding(KnockoutBindingString.html, mSchedulerName & ".applyResultStrongText($data)")
  '        End With
  '      End With
  '      With .Helpers.DivC("alert-message")
  '        '.AddClass("alert-message")
  '        .AddBinding(KnockoutBindingString.html, mSchedulerName & ".applyResultMessage($data)")
  '      End With
  '      With .Helpers.DivC("apply-result-options-requirements")
  '        With .Helpers.DivC("")
  '          .AddBinding(KnockoutBindingString.visible, "ApplyResultBO.isProductionHRResourceBookingOption($data)")
  '          With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
  '            With .Helpers.HTMLTag("span")
  '              .AddClass("input-group-btn")
  '              .AddBinding(KnockoutBindingString.visible, "ApplyResultBO.areOptionsVisible($data)")
  '              With .Helpers.Bootstrap.StateButton(Function(d As ApplyResult) d.AddAnyway, "Add anyway", "Add anyway?", , , , "fa-minus", )
  '              End With
  '            End With
  '            With .Helpers.Bootstrap.FormControlFor(Function(d As ApplyResult) d.AddAnywayReason,
  '                                                   BootstrapEnums.InputSize.Small, ,
  '                                                   "Reason...")
  '              .Editor.AddBinding(KnockoutBindingString.visible, "ApplyResultBO.areOptionRequirementsVisible($data)")
  '            End With
  '            With .Helpers.Bootstrap.InputGroupAddOnButton(, "Add", BootstrapEnums.Style.Primary, ,
  '                                                          BootstrapEnums.ButtonSize.Small, , "fa-thumbs-o-up", ,
  '                                                          PostBackType.None, "ApplyResultBO.addProductionHRResourceBooking($data)")
  '              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
  '              .AddOn.AddBinding(KnockoutBindingString.visible, "ApplyResultBO.areOptionRequirementsVisible($data)")
  '            End With
  '          End With
  '        End With
  '      End With
  '      With .Helpers.DivC("apply-alert-busy loading-custom")
  '        .AddBinding(KnockoutBindingString.css, "ApplyResultBO.isProcessingCss($data)")
  '        .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
  '      End With
  '    End With
  '  End With

End Namespace