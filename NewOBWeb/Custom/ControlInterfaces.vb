﻿Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Timesheets
Imports OBLib.Timesheets.OBCity
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports OBLib.Rooms
Imports Singular.DataAnnotations
Imports OBLib.Invoicing.ReadOnly
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.RoomScheduling
Imports OBLib.Productions.Base
Imports OBLib.HR
Imports OBLib.SatOps
Imports OBLib.Users.ReadOnly
Imports OBLib.Scheduling.Rooms
Imports OBLib.Maintenance.SystemManagement.ReadOnly

Public Class ControlInterfaces(Of VMType)

  Public Interface ITimesheet
    Property OBCityTimesheet As OBCityTimesheet
  End Interface

  Public Interface IEditCrewTimesheet

  End Interface

  Public Interface IFreelancerTimesheet
    Property CurrentPaymentRun As ROPaymentRun
    Property CurrentFreelancerTimesheetList As FreelancerTimesheetList
  End Interface

  Public Interface IServicesFTCTimesheet
    Property OBCityTimesheet As OBCityTimesheet
    Property CurrentTimesheetMonth As ROTimesheetMonth
    Property OBCityTimesheetCriteria As OBCityTimesheetList.Criteria
  End Interface

  Public Interface ROPaymentRunSelector
    Property ROPaymentRunList As ROPaymentRunList
    Property ROPaymentRunListCriteria As OBLib.Invoicing.ReadOnly.ROPaymentRunList.Criteria
    Property ROPaymentRunListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROTimesheetMonthSelector
    Property ROTimesheetMonthList As ROTimesheetMonthList
    Property ROTimesheetMonthListCriteria As OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetMonthList.Criteria
    Property ROTimesheetMonthPagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface ROHRSelectorBootstrap
    Property ROHRList As OBLib.HR.ReadOnly.ROHumanResourcePagedList
    Property ROHRListCriteria As OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria
    Property ROHRListManager As Singular.Web.Data.PagedDataManager(Of VMType)
    Property SelectedHumanResourceList As OBLib.HR.SelectedHRList
  End Interface

  Public Interface IROAdHocBookingPagedGrid
    Property ROAdHocBookingList As OBLib.AdHoc.ReadOnly.ROAdHocBookingList
    Property ROAdHocBookingListCriteria As OBLib.AdHoc.ReadOnly.ROAdHocBookingList.Criteria
    Property ROAdHocBookingListPagingManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROAdHocBookingListSelector
    Property ROAdHocBookingList As OBLib.AdHoc.ReadOnly.ROAdHocBookingList
    Property ROAdHocBookingListCriteria As OBLib.AdHoc.ReadOnly.ROAdHocBookingList.Criteria
    Property ROAdHocBookingListPagingManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROStatusSelect
    Property ROAllowedStatusList As ROProductionAreaAllowedStatusList
  End Interface

  Public Interface IRODebtorSelectControl
    Property RODebtorList As RODebtorList
    Property RODebtorListCriteria As OBLib.Quoting.ReadOnly.RODebtorList.Criteria
    Property RODebtorListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IRODebtorSelectModal
    Property ModalRODebtorList As RODebtorList
    Property ModalRODebtorListCriteria As OBLib.Quoting.ReadOnly.RODebtorList.Criteria
    Property ModalRODebtorListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROCountrySelect
    Property ROCountryList As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList
    Property ROCountryListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria
    Property ROCountryListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROCitySelect
    Property ROCityList As OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList
    Property ROCityListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList.Criteria
    Property ROCityListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROProductionVenueSelectControl
    Property ROProductionVenueList As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList
    Property ROProductionVenueListCriteria As OBLib.Maintenance.Productions.ReadOnly.ROProductionVenuePagedList.Criteria
    Property ROProductionVenueListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROEventTypeSelectControl
    Property ROEventTypePagedList As ROEventTypePagedList
    Property ROEventTypePagedListCriteria As ROEventTypePagedList.Criteria
    Property ROEventTypePagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROAllowedDisciplineSelectControl
    Property SelectedAllowedDisciplineList As OBLib.Productions.SelectedAllowedDisciplineList
    Property ROAllowedDisciplineSelectList As ROAllowedDisciplineSelectList
    Property ROAllowedDisciplineSelectListCriteria As OBLib.Productions.ReadOnly.ROAllowedDisciplineSelectList.Criteria
    Property ROAllowedDisciplineSelectListPagingManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IRoomScheduleAreaControl '(Of RoomBookingType As OBLib.RoomScheduling.RoomScheduleBase(Of RoomBookingType))
    Property CurrentRoomScheduleArea As RoomScheduleArea
  End Interface

  Public Interface IEditProduction
    Property CurrentProduction As OBLib.Productions.Production
  End Interface

  'Public Interface ISynergyEventsControl
  '  <InitialDataOnly>
  '  Property LastImportDateDescription As String
  '  <InitialDataOnly>
  '  Property ROSynergyEventList As OBLib.Synergy.ReadOnly.ROSynergyEventList
  '  Property ROSynergyEventListCriteria As OBLib.Synergy.ReadOnly.ROSynergyEventList.Criteria
  '  Property ROSynergyEventListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  'End Interface

  'Public Interface ISynergyEventsControlNotPaged
  '  <InitialDataOnly>
  '  Property ImportAsSingleBooking As Boolean
  '  <InitialDataOnly>
  '  Property CanImportAsSingleBooking As Boolean
  '  <InitialDataOnly>
  '  Property SelectedROSynergyEventList As List(Of OBLib.Helpers.SynergyEventChannel)
  '  <InitialDataOnly>
  '  Property TotalSynergyPages As Integer
  '  <InitialDataOnly>
  '  Property LastImportDateDescription As String
  '  <InitialDataOnly>
  '  Property TotalSynergyRecords As Integer
  '  <InitialDataOnly>
  '  Property IsProcessing As Boolean
  '  <InitialDataOnly>
  '  Property ROSynergyEventList As OBLib.Synergy.ReadOnly.ROSynergyEventList
  '  <InitialDataOnly>
  '  Property ROSynergyEventListCriteria As OBLib.Synergy.ReadOnly.ROSynergyEventList.Criteria
  '  <InitialDataOnly>
  '  Property SynergyEventsVisible As Boolean
  '  <InitialDataOnly>
  '  Property SynergyFiltersVisible As Boolean
  'End Interface

  Public Interface IROCreditorInvoices
    Property ROCreditorInvoiceList As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList
    Property ROCreditorInvoiceListCriteria As OBLib.Invoicing.ReadOnly.ROCreditorInvoiceList.Criteria
    Property ROCreditorInvoiceListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROInvoicePreview
    Property ROInvoicePreviewList As OBLib.Invoicing.ReadOnly.ROInvoicePreviewList
    Property ROInvoicePreviewListCriteria As OBLib.Invoicing.ReadOnly.ROInvoicePreviewList.Criteria
    Property ROInvoicePreviewListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface ICurrentInvoice
    '  Property PreviousInvoice As OBLib.Invoicing.[New].CreditorInvoice
    Property CurrentInvoice As OBLib.Invoicing.[New].CreditorInvoice
    ' Property NextInvoice As OBLib.Invoicing.[New].CreditorInvoice
    Property CurrentInvoiceRowNum As Integer
  End Interface

  Public Interface IROAudioSettingListModal
    <InitialDataOnly>
    Property ROAudioSettingList As ROAudioSettingList
    'Property ROAudioSettingListCriteria As OBLib.Maintenance.SatOps.ReadOnly.ROAudioSettingList.Criteria
    'Property ROAudioSettingListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROTurnAroundPointSelection
    Property ROTurnAroundPointList As OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPointList
    Property ROTurnAroundPointListCriteria As OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPointList.Criteria
    Property ROTurnAroundPointListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IFindHumanResource
    Property ROHumanResourceList As OBLib.HR.ReadOnly.ROHumanResourcePagedList
    Property ROHumanResourceListCriteria As OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria
    Property ROHumanResourceListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROHumanResourceTimesheetsPagedGrid
    Property ROHumanResourceTimesheetPagedList As OBLib.HR.Timesheets.ReadOnly.ROHumanResourceTimesheetPagedList
    Property ROHumanResourceTimesheetPagedListCriteria As OBLib.HR.Timesheets.ReadOnly.ROHumanResourceTimesheetPagedList.Criteria
    Property ROHumanResourceTimesheetPagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IEditHRTimesheet
    Property CurrentHumanResourceTimesheet As OBLib.HR.Timesheets.HumanResourceTimesheet
  End Interface

  Public Interface IFindHumanResourceTravelReq
    Property ROHumanResourceTravelReqList As OBLib.HR.ReadOnly.ROHumanResourceTravelReqList
    Property ROHumanResourceTravelReqListCriteria As OBLib.HR.ReadOnly.ROHumanResourceTravelReqList.Criteria
    Property ROHumanResourceTravelReqListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROHumanResourceSecondmentsPagedGrid
    Property ROHumanResourceSecondmentList As OBLib.HR.ReadOnly.ROHumanResourceSecondmentList
    Property ROHumanResourceSecondmentListCriteria As OBLib.HR.ReadOnly.ROHumanResourceSecondmentList.Criteria
    Property ROHumanResourceSecondmentListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IEditHumanResourceSecondment
    Property CurrentHumanResourceSecondment As HumanResourceSecondment
    Property CurrentSecondmentROCountryList As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList
    Property CurrentSecondmentROCountryListCriteria As OBLib.Maintenance.Locations.ReadOnly.ROCountryPagedList.Criteria
    Property CurrentSecondmentROCountryListManager As Singular.Web.Data.PagedDataManager(Of VMType)
    Property CurrentSecondmentRODebtorList As RODebtorList
    Property CurrentSecondmentRODebtorListCriteria As OBLib.Quoting.ReadOnly.RODebtorList.Criteria
    Property CurrentSecondmentRODebtorListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROHumanResourceOffPeriodPagedGrid
    Property ROHumanResourceOffPeriodList As OBLib.HR.ReadOnly.ROHumanResourceOffPeriodList
    Property ROHumanResourceOffPeriodListCriteria As OBLib.HR.ReadOnly.ROHumanResourceOffPeriodList.Criteria
    Property ROHumanResourceOffPeriodListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IEditHumanResourceOffPeriod
    Property CurrentHumanResourceOffPeriod As HumanResourceOffPeriod
  End Interface

  Public Interface IROHumanResourceSkillPagedGrid
    Property ROHumanResourceSkillList As OBLib.HR.ReadOnly.ROHumanResourceSkillList
    Property ROHumanResourceSkillListCriteria As OBLib.HR.ReadOnly.ROHumanResourceSkillList.Criteria
    Property ROHumanResourceSkillListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IProductionHumanResourceTBCGrid
    Property ProductionHumanResourceTBCList As OBLib.HR.ProductionHumanResourcesTBCList
    Property ProductionHumanResourceTBCListIsBusy As Boolean
  End Interface

  Public Interface IPlayoutOpsShift
    Property CurrentPlayoutOpsShift As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift
  End Interface

  Public Interface IPlayoutOpsShiftMCR
    Property CurrentPlayoutOpsShiftMCR As OBLib.Shifts.PlayoutOperations.MCRShift
  End Interface

  Public Interface IPlayoutOpsShiftSCCR
    Property CurrentPlayoutOpsShiftSCCR As OBLib.Shifts.PlayoutOperations.SCCRShift
  End Interface

  Public Interface IEditResourceScheduler
    Property EditResourceScheduler As OBLib.ResourceSchedulers.[New].ResourceScheduler
  End Interface

  Public Interface IEditHRSkill
    Property CurrentHumanResourceSkill As HumanResourceSkill
  End Interface

  'Public Interface IEquipmentScheduling
  '  Property CurrentEquipmentSchedule As EquipmentSchedule
  'End Interface

  Public Interface IROUserProfile
    Property ROUserProfile As ROUserProfile
  End Interface

  Public Interface IUserProfileShifts
    Property UserProfileShiftListCriteria As OBLib.Users.UserProfileShiftList.Criteria
    Property UserProfileShiftList As OBLib.Users.UserProfileShiftList
    Property CurrentUserProfileShiftGuiD As String
  End Interface

  Public Interface IApplyMessage
    <InitialDataOnly>
    Property ApplyMessageList As List(Of OBLib.Helpers.ApplyMessage)
    <InitialDataOnly>
    Property IsApplying As Boolean
  End Interface

  Public Interface ISystemTeams
    Property ROSystemTeamList As OBLib.TeamManagement.ReadOnly.ROSystemTeamList
    Property ROSystemTeamListCriteria As OBLib.TeamManagement.ReadOnly.ROSystemTeamList.Criteria
    Property ROSystemTeamListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IShiftPatterns
    Property SystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList
    Property EditableSystemAreaShiftPattern As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern
    Property EditableSystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList
    Property ROSystemAreaShiftPatternList As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList
    Property ROSystemAreaShiftPatternListCriteria As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList.Criteria
    Property ROSystemAreaShiftPatternListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IEditTeam
    Property EditableSystemTeamShiftPattern As OBLib.TeamManagement.SystemTeamShiftPattern
    Property TeamShiftPatternList As OBLib.TeamManagement.TeamShiftPatternList
    Property ROHumanResourceFilter As String
  End Interface

  Public Interface IAddTeam
    Property EditableSystemTeamShiftPattern As OBLib.TeamManagement.SystemTeamShiftPattern
  End Interface

  Public Interface IROProductionOutsourceServiceList
    Property ProductionOutsourceServiceList As OBLib.ProductionSystemAreas.ReadOnly.ROProductionOutsourceServiceList
    Property ProductionOutsourceServiceListCriteria As OBLib.ProductionSystemAreas.ReadOnly.ROProductionOutsourceServiceList.Criteria
    Property ProductionOutsourceServiceListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

  Public Interface IROProductionSpecRequirements

    'Required Equipment
    Property ROProductionSpecRequirementEquipmentTypeList As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementEquipmentTypeList
    Property ROProductionSpecRequirementEquipmentTypeListCriteria As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementEquipmentTypeList.Criteria
    Property ROProductionSpecRequirementEquipmentTypeListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    'Required Positions
    Property ROProductionSpecRequirementPositionList As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementPositionList
    Property ROProductionSpecRequirementPositionListCriteria As OBLib.ProductionSystemAreas.ReadOnly.ROProductionSpecRequirementPositionList.Criteria
    Property ROProductionSpecRequirementPositionListManager As Singular.Web.Data.PagedDataManager(Of VMType)

  End Interface

  Public Interface IROSystemYears
    Property ROSystemYearList As ROSystemYearList
    Property ROSystemYearListCriteria As ROSystemYearList.Criteria
    Property ROSystemYearListManager As Singular.Web.Data.PagedDataManager(Of VMType)
  End Interface

End Class
