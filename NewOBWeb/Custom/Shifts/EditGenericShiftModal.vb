﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Shifts
Imports OBLib.Resources
Imports OBLib.HR
Imports OBLib.Biometrics

Namespace Controls

  Public Class EditGenericShiftModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("CurrentGenericShiftModal", "Shift", False, "modal-lg", BootstrapEnums.Style.Primary, , "fa-linux", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Shifts.GenericShift)("ViewModel.CurrentGenericShift()")
            '/************************************* Editable View
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
                With .Helpers.Bootstrap.FlatBlock("Shift Details")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
                            .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.ProductionAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.HumanResourceID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.HumanResourceID, BootstrapEnums.InputSize.Small, , "Human Resource")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.DisciplineID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.ShiftTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ShiftTypeID, BootstrapEnums.InputSize.Small, , "Shift Type")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.ProductionAreaStatusID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ProductionAreaStatusID, BootstrapEnums.InputSize.Small, , "Status")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.ResourceBookingDescription)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ResourceBookingDescription, BootstrapEnums.InputSize.Small, , "Title")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        'With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Row
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.StartDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Date")
                              .Editor.Style.TextAlign = TextAlign.center
                              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                            With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.GenericShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Time")
                              .Style.TextAlign = TextAlign.center
                              '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Row
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.EndDateTime).Style.Width = "100%"
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Date")
                                .Editor.Style.TextAlign = TextAlign.center
                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.GenericShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Time")
                                .Style.TextAlign = TextAlign.center
                                '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.ExtraShiftInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.GenericShift) d.ExtraShiftInd, "Yes", "No", "btn-warning", , , , "btn-sm")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.MealReimbursementInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.GenericShift) d.MealReimbursementInd, "Yes", "No", "btn-info", , , , "btn-sm")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.AccessFlagDescription).Style.Width = "100%"
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.AccessFlagDescription, BootstrapEnums.InputSize.Small, , "Biometric Flag")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , BootstrapEnums.TabAlignment.Top)
                  With .AddTab("Authorisation", "fa-gavel", "HumanResourceShiftBaseBO.getBiometricLogs($data)", "Authorisation", , )
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 5, 4)
                          'Auth buttons
                          'Supervisor
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.SupervisorAuthInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d As OBLib.Shifts.GenericShift) d.SupervisorAuthInd, "Authed", "Rejected", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.SupervisorRejectedReason, BootstrapEnums.InputSize.Custom, "", "Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, "$data.SupervisorAuthInd() != null")
                              End With
                            End With
                            With .Helpers.Bootstrap.Row()
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.SupervisorAuthByName, BootstrapEnums.InputSize.Custom, "", "Auth By")
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.SuperAuthDateTime, BootstrapEnums.InputSize.Custom, "", "Auth Time")
                                End With
                              End With
                            End With
                          End With
                          'Manager
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.ManagerAuthInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d As OBLib.Shifts.GenericShift) d.ManagerAuthInd, "Authed", "Rejected", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ManagerRejectedReason, BootstrapEnums.InputSize.Custom, "", "Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, "$data.ManagerAuthInd() != null")
                              End With
                            End With
                            With .Helpers.Bootstrap.Row()
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ManagerAuthByName, BootstrapEnums.InputSize.Custom, "", "Auth By")
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.ManagerAuthDateTime, BootstrapEnums.InputSize.Custom, "", "Auth Time")
                                End With
                              End With
                            End With
                          End With
                          'Staff
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.GenericShift) d.StaffAcknowledgeInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d As OBLib.Shifts.GenericShift) d.StaffAcknowledgeInd, "Acknowledged", "Disputed", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.GenericShift) d.StaffDisputeReason, BootstrapEnums.InputSize.Custom, "", "Dispute Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                              End With
                            End With
                            With .Helpers.Bootstrap.Row()
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.StaffAcknowledgeByName, BootstrapEnums.InputSize.Custom, "", "Ack By")
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.GenericShift) d.StaffAcknowledgeDateTime, BootstrapEnums.InputSize.Custom, "", "Ack Time")
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 7, 8)
                          'Bio logs
                          .Helpers.HTML.Heading3("Access Logs")
                          With .Helpers.DivC("table-responsive")
                            With .Helpers.Bootstrap.TableFor(Of ROAccessTerminalShift)("$data.ROAccessTerminalShiftList()", False, False, False, True, True, True, True, "ROAccessTerminalShiftList")
                              .FirstRow.AddBinding(KnockoutBindingString.css, "HumanResourceShiftBaseBO.startEndCss($data)")
                              With .FirstRow
                                .AddClass("items selectable")
                                With .AddReadOnlyColumn(Function(c) c.AccessTime)
                                  .Style.Width = "125px"
                                End With
                                With .AddReadOnlyColumn(Function(d) d.Terminal)
                                End With
                                With .AddReadOnlyColumn(Function(d) d.StartEndInd)
                                  .Style.Width = "125px"
                                End With
                                With .AddReadOnlyColumn(Function(d) d.TimedInd)
                                  .Style.Width = "125px"
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .AddTab("History", "fa-user-secret", "HumanResourceShiftBaseBO.refreshAudit($data)", "History", , )
                    With .TabPane
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Audit.ROObjectAuditTrail)("$data.ROObjectAuditTrailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailList")
                          .AllowClientSideSorting = True
                          With .FirstRow
                            .AddClass("items selectable")
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.Section)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeDateTime)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeTypeDescription)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.LoginName)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.MachineName)
                            End With
                          End With
                          With .AddChildTable(Of OBLib.Audit.ROObjectAuditTrailDetail)("$data.ROObjectAuditTrailDetailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailDetailList")
                            .AllowClientSideSorting = True
                            .Style.Width = "90%"
                            .Style.FloatRight()
                            With .FirstRow
                              .AddClass("items selectable")
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.ChangeDescription)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.PreviousValueDescription)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.NewValueDescription)
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.With(Of OBLib.Shifts.GenericShift)("ViewModel.CurrentGenericShift()")
              With .Helpers.DivC("loading-custom")
                .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Shifts.GenericShift)("ViewModel.CurrentGenericShift()")
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, "GenericShiftBO.saveShiftModal($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, "GenericShiftBO.canSave($data)")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  'With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
  '  With .Helpers.Bootstrap.FlatBlock("Clashes", False, False, False)
  '    With .ContentTag
  '      With .Helpers.Bootstrap.Row
  '        .AddBinding(KnockoutBindingString.if, "$data.ClashList().length == 0")
  '        With .Helpers.DivC("text-center")
  '          With .Helpers.DivC("i-circle success")
  '            .Helpers.Bootstrap.FontAwesomeIcon("fa-check")
  '          End With
  '          .Helpers.HTML.Heading4("Fantastic!")
  '          With .Helpers.HTMLTag("p")
  '            .Helpers.HTML("There are no clashes!")
  '          End With
  '        End With
  '      End With
  '      With .Helpers.Bootstrap.Row
  '        .AddBinding(KnockoutBindingString.if, "$data.ClashList().length > 0")
  '        With .Helpers.DivC("table-responsive")
  '          With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashList()", False, False, True, False, True, True, True, "ClashList")
  '            .AllowClientSideSorting = True
  '            .AddClass("no-border hover list")
  '            .TableBodyClass = "no-border-y no-border-x"
  '            With .FirstRow
  '              .AddClass("items selectable")
  '              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
  '              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
  '              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
  '            End With
  '          End With
  '        End With
  '      End With
  '    End With
  '  End With
  '  'With .Helpers.Bootstrap.FlatBlock("Clashes")
  '  '  With .ContentTag
  '  '    With .Helpers.Bootstrap.Row
  '  '      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
  '  '        .AddBinding(KnockoutBindingString.visible, "$data.ClashList().length > 0")
  '  '        .AddClass("error-column")
  '  '        With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Large, , "fa-bomb", , PostBackType.None, mControlInstanceName & ".showClashes($data)")
  '  '          .ButtonText.AddBinding(KnockoutBindingString.html, "$data.ClashList().length.toString() + ' clashes found'")
  '  '          .Button.AddBinding(KnockoutBindingString.css, "'slow rubberBand infinite go btn-block'")
  '  '        End With
  '  '        With .Helpers.DivC("table-responsive")
  '  '          With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashList()", False, False, True, False, True, True, True, "ClashList")
  '  '            .AllowClientSideSorting = True
  '  '            .AddClass("no-border hover list")
  '  '            .TableBodyClass = "no-border-y no-border-x"
  '  '            With .FirstRow
  '  '              .AddClass("items selectable")
  '  '              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
  '  '              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
  '  '              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
  '  '            End With
  '  '          End With
  '  '        End With
  '  '      End With
  '  '    End With
  '  '  End With
  '  'End With
  'End With

End Namespace