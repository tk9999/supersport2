﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Shifts.PlayoutOperations
Imports OBLib.Resources
Imports OBLib.HR
Imports OBLib.Biometrics

Namespace Controls

  Public Class EditICRShiftModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = "CurrentICRShiftModal"
    Private mControlInstanceName As String = "CurrentICRShiftControl"

    Public Sub New(Optional ModalID As String = "CurrentICRShiftModal",
                   Optional ControlInstanceName As String = "CurrentICRShiftControl")
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Shift", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-linux", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Shifts.ICR.ICRShift)(mControlInstanceName & ".currentShift()")
            '/************************************* Editable View
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                With .Helpers.Bootstrap.FlatBlock("Shift Details")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
                            .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ProductionAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.DisciplineID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          End With
                          '.Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.DisciplineID)
                          'With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          'End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.HumanResource)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.HumanResource, BootstrapEnums.InputSize.Small, , "Human Resource")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ShiftTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ShiftTypeID, BootstrapEnums.InputSize.Small, , "Shift Type")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ProductionAreaStatusID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ProductionAreaStatusID, BootstrapEnums.InputSize.Small, , "Status")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ResourceBookingDescription)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ResourceBookingDescription, BootstrapEnums.InputSize.Small, , "Title")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StartDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Date")
                                .Editor.Style.TextAlign = TextAlign.center
                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Time")
                                .Style.TextAlign = TextAlign.center
                                '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.EndDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Date")
                                .Editor.Style.TextAlign = TextAlign.center
                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Time")
                                .Style.TextAlign = TextAlign.center
                                '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ExtraShiftInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.ICR.ICRShift) d.ExtraShiftInd, "Yes", "No", "btn-warning", , , , "btn-sm")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.MealReimbursementInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.ICR.ICRShift) d.MealReimbursementInd, "Yes", "No", "btn-info", , , , "btn-sm")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.AccessFlagDescription).Style.Width = "100%"
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.AccessFlagDescription, BootstrapEnums.InputSize.Small, , "Biometric Flag")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.Comments).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.Comments, BootstrapEnums.InputSize.Small)
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            '.Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , )
                  With .AddTab("Authorisation", "fa-gavel", "HumanResourceShiftBaseBO.getBiometricLogs($data)", "Authorisation", , )
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 5, 4)
                          'Auth buttons
                          'Supervisor
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.SupervisorAuthInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d) d.SupervisorAuthInd, "Authed", "Rejected", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.SupervisorRejectedReason, BootstrapEnums.InputSize.Custom, "", "Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, "$data.SupervisorAuthInd() != null")
                              End With
                            End With
                            With .Helpers.DivC("auth-details-row")
                              With .Helpers.HTMLTag("p")
                                .AddClass("auth-details")
                                .AddBinding(KnockoutBindingString.html, "HumanResourceShiftBaseBO.supervisorAuthText($data)")
                              End With
                            End With
                          End With
                          'Manager
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.ManagerAuthInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d) d.ManagerAuthInd, "Authed", "Rejected", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.ManagerRejectedReason, BootstrapEnums.InputSize.Custom, "", "Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, "$data.ManagerAuthInd() != null")
                              End With
                            End With
                            With .Helpers.DivC("auth-details-row")
                              With .Helpers.HTMLTag("p")
                                .AddClass("auth-details")
                                .AddBinding(KnockoutBindingString.html, "HumanResourceShiftBaseBO.managerAuthText($data)")
                              End With
                            End With
                          End With
                          'Staff
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.StaffAcknowledgeInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d) d.StaffAcknowledgeInd, "Acknowledged", "Disputed", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.StaffDisputeReason, BootstrapEnums.InputSize.Custom, "", "Dispute Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                              End With
                            End With
                            With .Helpers.DivC("auth-details-row")
                              With .Helpers.HTMLTag("p")
                                .AddClass("auth-details")
                                .AddBinding(KnockoutBindingString.html, "HumanResourceShiftBaseBO.staffAuthText($data)")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 7, 8)
                          'Bio logs
                          .Helpers.HTML.Heading3("Access Logs")
                          With .Helpers.DivC("table-responsive")
                            With .Helpers.Bootstrap.TableFor(Of ROAccessTerminalShift)("$data.ROAccessTerminalShiftList()", False, False, False, True, True, True, True, "ROAccessTerminalShiftList")
                              .FirstRow.AddBinding(KnockoutBindingString.css, "HumanResourceShiftBaseBO.startEndCss($data)")
                              With .FirstRow
                                .AddClass("items selectable")
                                With .AddReadOnlyColumn(Function(c) c.AccessTime)
                                  .Style.Width = "125px"
                                End With
                                With .AddReadOnlyColumn(Function(d) d.Terminal)
                                End With
                                With .AddReadOnlyColumn(Function(d) d.StartEndInd)
                                  .Style.Width = "125px"
                                End With
                                With .AddReadOnlyColumn(Function(d) d.TimedInd)
                                  .Style.Width = "125px"
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .AddTab("History", "fa-incognito", "HumanResourceShiftBaseBO.refreshAudit($data)", "History", , )
                    With .TabPane
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Audit.ROObjectAuditTrail)("$data.ROObjectAuditTrailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailList")
                          .AllowClientSideSorting = True
                          With .FirstRow
                            .AddClass("items selectable")
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.Section)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeDateTime)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeTypeDescription)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.LoginName)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.MachineName)
                            End With
                          End With
                          With .AddChildTable(Of OBLib.Audit.ROObjectAuditTrailDetail)("$data.ROObjectAuditTrailDetailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailDetailList")
                            .AllowClientSideSorting = True
                            .Style.Width = "90%"
                            .Style.FloatRight()
                            With .FirstRow
                              .AddClass("items selectable")
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.ChangeDescription)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.PreviousValueDescription)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.NewValueDescription)
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.FlatBlock("Clashes", False, False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.ClashList().length == 0")
                      With .Helpers.DivC("text-center")
                        With .Helpers.DivC("i-circle success")
                          .Helpers.Bootstrap.FontAwesomeIcon("fa-check")
                        End With
                        .Helpers.HTML.Heading4("Fantastic!")
                        With .Helpers.HTMLTag("p")
                          .Helpers.HTML("There are no clashes!")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.ClashList().length > 0")
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashList()", False, False, True, False, True, True, True, "ClashList")
                          .AllowClientSideSorting = True
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y no-border-x"
                          With .FirstRow
                            .AddClass("items selectable")
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.With(Of OBLib.Shifts.ICR.ICRShift)(mControlInstanceName & ".currentShift()")
                With .Helpers.DivC("loading-custom")
                  .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Shifts.ICR.ICRShift)(mControlInstanceName & ".currentShift()")
            With .Helpers.DivC("pull-left")
              With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
                                             PostBackType.None, mControlInstanceName & ".cancelShiftModal($data)", False)
                .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".canCancel($data)")
              End With
            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, mControlInstanceName & ".saveShiftStateless($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".canSave($data)")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
  'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
  '  With .Helpers.Bootstrap.FlatBlock("Shift Details", True, True, , )
  '    'With .AboveContentTag
  '    '  .AddBinding(KnockoutBindingString.visible, "$data.ClashList().length > 0")

  '    'End With
  '    With .ContentTag
  '      With .Helpers.Bootstrap.Row

  '      End With
  '    End With
  '  End With
  'End With
  'End With
  'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '    With .Helpers.Bootstrap.Row
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.HumanResource)
  '        With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.HumanResource, BootstrapEnums.InputSize.Small, , "Human Resource")
  '          '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
  '        End With
  '      End With
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '        '.Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.DisciplineID)
  '        'With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
  '        '  '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
  '        'End With
  '        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.Discipline)
  '        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.Discipline, BootstrapEnums.InputSize.Small, , "Discipline")
  '          '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
  '        End With
  '      End With
  '    End With
  '  End With
  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '    With .Helpers.Bootstrap.Row
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.SystemID).Style.Width = "100%"
  '        With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
  '        End With
  '      End With
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ProductionAreaID).Style.Width = "100%"
  '        With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
  '        End With
  '      End With
  '    End With
  '  End With
  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '    With .Helpers.Bootstrap.Row
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ShiftTypeID)
  '        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ShiftTypeID, BootstrapEnums.InputSize.Small, , "Leave Type")
  '          '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
  '        End With
  '      End With
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '        '.Helpers.Bootstrap.LabelFor(Function(d As PlayoutOperationsShift) d.SystemTeamID).Style.Width = "100%"
  '        'With .Helpers.Bootstrap.FormControlFor(Function(d As PlayoutOperationsShift) d.SystemTeamID, BootstrapEnums.InputSize.Small, , "Team")
  '        'End With
  '        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.TeamName).Style.Width = "100%"
  '        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.TeamName, BootstrapEnums.InputSize.Small, , "Team")
  '        End With
  '      End With
  '    End With
  '  End With
  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '    With .Helpers.Bootstrap.Row
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
  '        With .Helpers.Bootstrap.Row
  '          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
  '            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StartDateTime).Style.Width = "100%"
  '          End With
  '          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Date")
  '              .Editor.Style.TextAlign = TextAlign.center
  '              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
  '            End With
  '          End With
  '          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '            With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Time")
  '              .Style.TextAlign = TextAlign.center
  '              '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
  '            End With
  '          End With
  '        End With
  '      End With
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
  '        With .Helpers.Bootstrap.Row
  '          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
  '            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.EndDateTime).Style.Width = "100%"
  '          End With
  '          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Date")
  '              .Editor.Style.TextAlign = TextAlign.center
  '              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
  '            End With
  '          End With
  '          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
  '            With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Time")
  '              .Style.TextAlign = TextAlign.center
  '              '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
  '            End With
  '          End With
  '        End With
  '      End With
  '    End With
  '  End With
  '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '    With .Helpers.Bootstrap.Row
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
  '        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ExtraShiftInd).Style.Width = "100%"
  '          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.ICR.ICRShift) d.ExtraShiftInd, "Yes", "No", "btn-warning", , , , "btn-sm")
  '            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
  '            .Button.AddClass("btn-block")
  '          End With
  '        End With
  '        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.MealReimbursementInd).Style.Width = "100%"
  '          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.ICR.ICRShift) d.MealReimbursementInd, "Yes", "No", "btn-info", , , , "btn-sm")
  '            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
  '            .Button.AddClass("btn-block")
  '          End With
  '        End With
  '      End With
  '      With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
  '        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StaffAcknowledgeInd).Style.Width = "100%"
  '          With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
  '            With .Helpers.HTMLTag("span")
  '              .AddClass("input-group-btn")
  '              With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-minus", , PostBackType.None, "")
  '                .Button.AddBinding(KnockoutBindingString.css, "ICRShiftBO.StaffAckCss($data)")
  '                .ButtonText.AddBinding(KnockoutBindingString.html, "ICRShiftBO.StaffAckText($data)")
  '                .Icon.AddBinding(KnockoutBindingString.css, "ICRShiftBO.StaffAckIcon($data)")
  '                .Button.AddBinding(KnockoutBindingString.enable, Function(d) False)
  '              End With
  '            End With
  '            .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.StaffAcknowledgeDetails, BootstrapEnums.InputSize.Small)
  '          End With
  '        End With
  '        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.SupervisorAuthInd).Style.Width = "100%"
  '          With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
  '            With .Helpers.HTMLTag("span")
  '              .AddClass("input-group-btn")
  '              With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-minus", , PostBackType.None, "")
  '                .Button.AddBinding(KnockoutBindingString.css, "ICRShiftBO.SupervisorAuthCss($data)")
  '                .ButtonText.AddBinding(KnockoutBindingString.html, "ICRShiftBO.SupervisorAuthText($data)")
  '                .Icon.AddBinding(KnockoutBindingString.css, "ICRShiftBO.SupervisorAuthIcon($data)")
  '                .Button.AddBinding(KnockoutBindingString.enable, Function(d) False)
  '              End With
  '            End With
  '            .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.SupervisorAuthDetails, BootstrapEnums.InputSize.Small)
  '          End With
  '        End With
  '        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
  '          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ManagerAuthInd).Style.Width = "100%"
  '          With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
  '            With .Helpers.HTMLTag("span")
  '              .AddClass("input-group-btn")
  '              With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-minus", , PostBackType.None, "")
  '                .Button.AddBinding(KnockoutBindingString.css, "ICRShiftBO.ManagerAuthCss($data)")
  '                .ButtonText.AddBinding(KnockoutBindingString.html, "ICRShiftBO.ManagerAuthText($data)")
  '                .Icon.AddBinding(KnockoutBindingString.css, "ICRShiftBO.ManagerAuthIcon($data)")
  '                .Button.AddBinding(KnockoutBindingString.enable, Function(d) False)
  '              End With
  '            End With
  '            .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.ICR.ICRShift) d.ManagerAuthDetails, BootstrapEnums.InputSize.Small)
  '          End With
  '        End With
  '      End With
  '    End With
  '  End With
  'End With

End Namespace