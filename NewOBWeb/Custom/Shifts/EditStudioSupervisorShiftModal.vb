﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Shifts.PlayoutOperations
Imports OBLib.Resources
Imports OBLib.HR
Imports OBLib.Shifts.Studios
Imports OBLib.Biometrics

Namespace Controls

  Public Class EditStudioSupervisorShiftModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("CurrentStudioSupervisorShiftModal", "Shift", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-pied-piper-alt", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Shifts.Studios.StudioSupervisorShift)("ViewModel.CurrentStudioSupervisorShift()")
            '/************************************* Editable View
            With .Helpers.Bootstrap.Row
              With .Helpers.DivC("pull-right")
                With .Helpers.Toolbar
                  With .Helpers.MessageHolder()
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                With .Helpers.Bootstrap.FlatBlock("Shift Details", False, False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.SupervisorID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.SupervisorID, BootstrapEnums.InputSize.Small, , "Supervisor")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.ShiftTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.ShiftTypeID, BootstrapEnums.InputSize.Small, , "Shift Type")
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.ProductionAreaStatusID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.ProductionAreaStatusID, BootstrapEnums.InputSize.Small, , "Shift Type")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.StartDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Date")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Time")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.EndDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Date")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Time")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.AccessFlagDescription).Style.Width = "100%"
                            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.Studios.StudioSupervisorShift) d.AccessFlagDescription, BootstrapEnums.InputSize.Small, , "Biometric Flag")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , )
                  With .AddTab("Authorisation", "fa-gavel", "HumanResourceShiftBaseBO.getBiometricLogs($data)", "Authorisation", , )
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 6, 5, 4)
                          'Auth buttons
                          'Supervisor
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.SupervisorAuthInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d) d.SupervisorAuthInd, "Authed", "Rejected", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.SupervisorRejectedReason, BootstrapEnums.InputSize.Custom, "", "Reject Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, "!$data.SupervisorAuthInd()")
                              End With
                            End With
                            With .Helpers.DivC("auth-details-row")
                              With .Helpers.HTMLTag("p")
                                .AddClass("auth-details")
                                .AddBinding(KnockoutBindingString.html, "HumanResourceShiftBaseBO.supervisorAuthText($data)")
                              End With
                            End With
                          End With
                          'Manager
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.ManagerAuthInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d) d.ManagerAuthInd, "Authed", "Rejected", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.ManagerRejectedReason, BootstrapEnums.InputSize.Custom, "", "Reject Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, "!$data.ManagerAuthInd()")
                              End With
                            End With
                            With .Helpers.DivC("auth-details-row")
                              With .Helpers.HTMLTag("p")
                                .AddClass("auth-details")
                                .AddBinding(KnockoutBindingString.html, "HumanResourceShiftBaseBO.managerAuthText($data)")
                              End With
                            End With
                          End With
                          'Staff
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.StaffAcknowledgeInd).Style.Width = "100%"
                            With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                              With .Helpers.SpanC("input-group-btn")
                                With .Helpers.Bootstrap.TriStateButton(Function(d) d.StaffAcknowledgeInd, "Acknowledged", "Disputed", "Not Specified", "btn-success", "btn-danger", "btn-default", , "fa-times", , )
                                End With
                              End With
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.StaffDisputeReason, BootstrapEnums.InputSize.Custom, "", "Dispute Reason")
                                .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                              End With
                            End With
                            With .Helpers.DivC("auth-details-row")
                              With .Helpers.HTMLTag("p")
                                .AddClass("auth-details")
                                .AddBinding(KnockoutBindingString.html, "HumanResourceShiftBaseBO.staffAuthText($data)")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 6, 7, 8)
                          'Bio logs
                          .Helpers.HTML.Heading3("Access Logs")
                          With .Helpers.DivC("table-responsive")
                            With .Helpers.Bootstrap.TableFor(Of ROAccessTerminalShift)("$data.ROAccessTerminalShiftList()", False, False, False, True, True, True, True, "ROAccessTerminalShiftList")
                              .FirstRow.AddBinding(KnockoutBindingString.css, "HumanResourceShiftBaseBO.startEndCss($data)")
                              With .FirstRow
                                .AddClass("items selectable")
                                With .AddReadOnlyColumn(Function(c) c.AccessTime)
                                  .Style.Width = "125px"
                                End With
                                With .AddReadOnlyColumn(Function(d) d.Terminal)
                                End With
                                With .AddReadOnlyColumn(Function(d) d.StartEndInd)
                                  .Style.Width = "125px"
                                End With
                                With .AddReadOnlyColumn(Function(d) d.TimedInd)
                                  .Style.Width = "125px"
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .AddTab("History", "fa-incognito", "HumanResourceShiftBaseBO.refreshAudit($data)", "History", , )
                    With .TabPane
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Audit.ROObjectAuditTrail)("$data.ROObjectAuditTrailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailList")
                          .AllowClientSideSorting = True
                          With .FirstRow
                            .AddClass("items selectable")
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.Section)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeDateTime)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeTypeDescription)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.LoginName)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.MachineName)
                            End With
                          End With
                          With .AddChildTable(Of OBLib.Audit.ROObjectAuditTrailDetail)("$data.ROObjectAuditTrailDetailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailDetailList")
                            .AllowClientSideSorting = True
                            .Style.Width = "90%"
                            .Style.FloatRight()
                            With .FirstRow
                              .AddClass("items selectable")
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.ChangeDescription)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.PreviousValueDescription)
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.NewValueDescription)
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.FlatBlock("Clashes", False, False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.ClashList().length == 0")
                      With .Helpers.DivC("text-center")
                        With .Helpers.DivC("i-circle success")
                          .Helpers.Bootstrap.FontAwesomeIcon("fa-check")
                        End With
                        .Helpers.HTML.Heading4("Fantastic!")
                        With .Helpers.HTMLTag("p")
                          .Helpers.HTML("There are no clashes!")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.ClashList().length > 0")
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashList()", False, False, True, False, True, True, True, "ClashList")
                          .AllowClientSideSorting = True
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y no-border-x"
                          With .FirstRow
                            .AddClass("items selectable")
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.With(Of OBLib.Shifts.Studios.StudioSupervisorShift)("ViewModel.CurrentStudioSupervisorShift()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Shifts.Studios.StudioSupervisorShift)("ViewModel.CurrentStudioSupervisorShift()")
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, "StudioSupervisorShiftBO.saveShift($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, "StudioSupervisorShiftBO.canSave($data)")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace