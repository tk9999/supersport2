﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Shifts.PlayoutOperations
Imports OBLib.Resources
Imports OBLib.HR

Namespace Controls

  Public Class EditPlayoutOpsShiftModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IPlayoutOpsShift)

    Private mModalID As String = "CurrentPlayoutOpsShiftModal"
    Private mControlInstanceName As String = "CurrentPlayoutOpsShiftControl"

    Public Sub New(Optional ModalID As String = "CurrentPlayoutOpsShiftModal",
                   Optional ControlInstanceName As String = "CurrentPlayoutOpsShiftControl")
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Shift", False, "modal-sm", BootstrapEnums.Style.Primary, , "fa-linux", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift)(mControlInstanceName & ".currentShift()")
            '/************************************* Editable View
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Shift Details")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
                            .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ProductionAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.Discipline)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.Discipline, BootstrapEnums.InputSize.Small, , "Discipline")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.HumanResource)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.HumanResource, BootstrapEnums.InputSize.Small, , "Human Resource")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ShiftTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ShiftTypeID, BootstrapEnums.InputSize.Small, , "Shift Type")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ProductionAreaStatusID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ProductionAreaStatusID, BootstrapEnums.InputSize.Small, , "Status")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.StartDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Date")
                                .Editor.Style.TextAlign = TextAlign.center
                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Time")
                                .Style.TextAlign = TextAlign.center
                                '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.EndDateTime).Style.Width = "100%"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Date")
                                .Editor.Style.TextAlign = TextAlign.center
                                '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.EndDateTime, BootstrapEnums.InputSize.Small, , "Time")
                                .Style.TextAlign = TextAlign.center
                                '.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ExtraShiftInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.ExtraShiftInd, "Yes", "No", "btn-warning", , , , "btn-sm")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.MealReimbursementInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.MealReimbursementInd, "Yes", "No", "btn-info", , , , "btn-sm")
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.Comments).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift) d.Comments, BootstrapEnums.InputSize.Small, , "Comments")
                            '.Editor.Style.TextAlign = TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Room Bookings", False, False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.Shifts.HumanResourceShiftRoomSchedule)("$data.HumanResourceShiftRoomScheduleList()", False, False, True, False, True, True, True, "HumanResourceShiftRoomScheduleList")
                            .AllowClientSideSorting = True
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            With .FirstRow
                              .AddClass("items selectable")
                              .AddReadOnlyColumn(Function(d As OBLib.Shifts.HumanResourceShiftRoomSchedule) d.Room)
                              .AddReadOnlyColumn(Function(d As OBLib.Shifts.HumanResourceShiftRoomSchedule) d.StartTimeString)
                              .AddReadOnlyColumn(Function(d As OBLib.Shifts.HumanResourceShiftRoomSchedule) d.EndTimeString)
                              .AddReadOnlyColumn(Function(d As OBLib.Shifts.HumanResourceShiftRoomSchedule) d.RoomScheduleTitle)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.FlatBlock("Clashes", False, False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.ClashList().length == 0")
                      With .Helpers.DivC("text-center")
                        With .Helpers.DivC("i-circle success")
                          .Helpers.Bootstrap.FontAwesomeIcon("fa-check")
                        End With
                        .Helpers.HTML.Heading4("Fantastic!")
                        With .Helpers.HTMLTag("p")
                          .Helpers.HTML("There are no clashes!")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.ClashList().length > 0")
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashList()", False, False, True, False, True, True, True, "ClashList")
                          .AllowClientSideSorting = True
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y no-border-x"
                          With .FirstRow
                            .AddClass("items selectable")
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
                            .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.With(Of OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift)(mControlInstanceName & ".currentShift()")
                With .Helpers.DivC("loading-custom")
                  .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Shifts.PlayoutOperations.PlayoutOperationsShift)(mControlInstanceName & ".currentShift()")
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, mControlInstanceName & ".saveShiftStateless($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".canSave($data)")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace