﻿Imports OBLib.Users.ReadOnly
Imports Singular.Web
Imports OBLib.Users

Namespace Controls

  Public Class UserProfileShiftsControl
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of Object)

    Public Sub New()

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of ROUserProfile)("ViewModel.ROUserProfile()")
        '.Helpers.HTML.Heading3("Shifts")
        With .Helpers.With(Of OBLib.Users.UserProfileShiftList.Criteria)("ViewModel.UserProfileShiftListCriteria()")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Users.UserProfileShiftList.Criteria) d.StartDate).Style.Width = "100%"
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Users.UserProfileShiftList.Criteria) d.StartDate, BootstrapEnums.InputSize.Small)
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Users.UserProfileShiftList.Criteria) d.EndDate).Style.Width = "100%"
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Users.UserProfileShiftList.Criteria) d.EndDate, BootstrapEnums.InputSize.Small)
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelDisplay("Refresh Data")
                With .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, ,
                                                 "fa-refresh", , PostBackType.None, "UserProfilePage.fetchShifts()")
                  .Button.AddClass("btn-block")
                  .Button.AddBinding(KnockoutBindingString.enable, "ViewModel.UserProfileShiftListCriteria().IsValid()")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
              .AddClass("col-xl-offset-4 col-lg-offset-2")
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelDisplay("Save Data")
                With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, ,
                                                 "fa-floppy-o", , PostBackType.None, "UserProfilePage.saveShifts()")
                  .Button.AddClass("btn-block")
                  .Button.AddBinding(KnockoutBindingString.enable, "ViewModel.UserProfileShiftListCriteria().IsValid()")
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of UserProfileShift)("ViewModel.UserProfileShiftList()", False, False, False, False, True, True, True)
            .AddClass("no-border")
            .TableBodyClass = "no-border-x no-border-y"
            With .FirstRow
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.ShiftType)

              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.ScheduleDateString)
                .Style.TextAlign = TextAlign.center
              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.StartDateTimeString)
                .Style.TextAlign = TextAlign.center
              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.EndDateTimeString)
                .Style.TextAlign = TextAlign.center
              End With
              With .AddColumn("PH?")
                .Style.Width = "50px"
                With .Helpers.Bootstrap.StateButton(Function(d As UserProfileShift) d.PublicHolidayInd, , , , , , , )
                  .Button.AddBinding(KnockoutBindingString.enable, Function(d) False)
                End With
              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.ShiftDurationString)
                .Style.TextAlign = TextAlign.right
              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.RunningTotalString)
                .Style.TextAlign = TextAlign.right
              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.AuthorisedNoOfMealsString)
                .Style.TextAlign = TextAlign.right
              End With
              With .AddReadOnlyColumn(Function(d As UserProfileShift) d.AllowanceCategory)
                .Style.TextAlign = TextAlign.right
              End With
              With .AddColumn("Status")
                .Style.Width = "70px"
                With .Helpers.Bootstrap.StateButton(Function(d As UserProfileShift) d.StaffAcknowledgeInd, "Acknowledged", "Not Acknowledged", "btn-success", "btn-danger", , "fa-times", )
                End With
              End With
              'With .AddColumn("Dispute Reason")
              '  .Style.Width = "70px"
              '  With .Helpers.Bootstrap.FormControlFor(Function(d As UserProfileShift) d.StaffDisputeReason, BootstrapEnums.InputSize.Small, , "Reason for dispute...")
              '  End With
              'End With
              With .AddColumn("")
                With .Helpers.Bootstrap.Button(, "Query", Singular.Web.BootstrapEnums.Style.Custom,
                                               , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                               "fa-question-circle", , Singular.Web.PostBackType.None,
                                               "UserProfilePage.queryShift($data)")
                  .Button.AddBinding(KnockoutBindingString.css, "UserProfilePage.queryCss($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, "UserProfilePage.queryText($data)")
                  .Button.AddBinding(KnockoutBindingString.visible, "UserProfilePage.canQuery($data)")
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(KnockoutBindingString.visible, "ViewModel.UserProfileShiftListCriteria().IsProcessing()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
        'End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace