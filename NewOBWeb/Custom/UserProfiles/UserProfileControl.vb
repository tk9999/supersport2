﻿Imports OBLib.Users.ReadOnly
Imports Singular.Web

Namespace Controls

  Public Class UserProfileControl
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of Object)

    Private mROUserProfileInstanceName As String = ""

    Public Sub New(Optional ROUserProfileInstanceName = "ViewModel.ROUserProfile()")
      mROUserProfileInstanceName = ROUserProfileInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of ROUserProfile)(mROUserProfileInstanceName)
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
            With .Helpers.Bootstrap.Button(, "Query Details", Singular.Web.BootstrapEnums.Style.Primary, ,
                                           Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                           "fa-question-circle", , Singular.Web.PostBackType.None, "UserProfilePage.QueryDetails()")
            End With
          End With
        End With
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
            With .Helpers.FieldSet("Personal Info")
              '1st Row*****************************************************************************************************************
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Firstname)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Firstname, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.SecondName)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.SecondName, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Surname)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Surname, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              '2nd Row*****************************************************************************************************************
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 12)
                '.AddClass("col-xl-offset-2")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.PreferredName)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.PreferredName, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              '2nd Row*****************************************************************************************************************
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 12)
                '.AddClass("col-xl-offset-2")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.IDNo)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.IDNo, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              '2nd Row*****************************************************************************************************************
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                '.AddClass("col-xl-offset-2")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Race)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Race, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Gender)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Gender, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              '3rd Row*****************************************************************************************************************
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                '.AddClass("col-xl-offset-2")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Nationality)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Nationality, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.City)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.City, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              '6th Row*****************************************************************************************************************
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                '.AddClass("col-xl-offset-2")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.CellPhoneNumber)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.CellPhoneNumber, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.EmailAddress)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.EmailAddress, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
            With .Helpers.FieldSet("Employment Info")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.EmployeeCode)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.EmployeeCode, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.If(mROUserProfileInstanceName & ".ContractDaysApplicable()")
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.ContractType)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.ContractType, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.ContractDays)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.ContractDays, BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
              End With
              With .Helpers.If("!" & mROUserProfileInstanceName & ".ContractDaysApplicable()")
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.ContractType)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.ContractType, Singular.Web.BootstrapEnums.InputSize.Small)
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Supplier)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Supplier, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.LicenceInd)
                  With .Helpers.Bootstrap.InputGroup
                    With .Helpers.Bootstrap.InputGroupAddOnStateButton(Function(d As ROUserProfile) d.LicenceInd, , , "btn-success", "btn-warning", , , "btn-sm")
                      .StateButton.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                    End With
                    .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.LicenceExpiryDateString, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As ROUserProfile) d.Manager1)
                  With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROUserProfile) d.Manager1, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace