﻿
Namespace Controls

  Public Class UserProfileAvatar
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of Object)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.DivC("avatar")
        With .Helpers.Image
          .AddClass("profile-avatar")
          .AddBinding(Singular.Web.KnockoutBindingString.src, "UserProfileBO.AvatarPhotoSrc($data)")
          .AddBinding(Singular.Web.KnockoutBindingString.click, "UserProfileBO.AvatarPhotoClick($data)")
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace