﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling

Namespace Controls

  Public Class ROProductionInline(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Private mProductionControlInstanceName As String = "ProductionEditor"

    Public Sub New(Optional ProductionControlInstanceName As String = "ProductionEditor")
      mProductionControlInstanceName = ProductionControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OBLib.Rooms.ReadOnly.RORoomBookingProduction)(mProductionControlInstanceName & ".production()")
        'Production Type and Event Type-------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.ProductionTypeEventType).Style.Width = "100%"
          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.ProductionTypeEventType, BootstrapEnums.InputSize.Small, , )
          End With
        End With
        'Title--------------------------------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d) d.Title).Style.Width = "100%"
          .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.Title, BootstrapEnums.InputSize.Small, , "Title...")
        End With
        'Teams--------------------------------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.TeamsPlaying).Style.Width = "100%"
          .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.TeamsPlaying, BootstrapEnums.InputSize.Small, "Teams..")
        End With
        'Venue--------------------------------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.ProductionVenue).Style.Width = "100%"
          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.ProductionVenue, BootstrapEnums.InputSize.Small, , )
          End With
        End With
        'Live Start Times---------------------------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.EventStart).Style.Width = "100%"
          .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.EventStart, BootstrapEnums.InputSize.Small, , "Start Date")
        End With
        'Live End Times-----------------------------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.EventEnd).Style.Width = "100%"
          .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.EventEnd, BootstrapEnums.InputSize.Small, , "End Date")
        End With
        'Production Ref No--------------------------------------------------------
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.ProductionRefNo).Style.Width = "100%"
              .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.ProductionRefNo, BootstrapEnums.InputSize.Small, , "Ref Num...")
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.SynergyGenRefNo).Style.Width = "100%"
              .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingProduction) d.SynergyGenRefNo, BootstrapEnums.InputSize.Small, , "Gen Ref...")
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace