﻿Imports Singular.Web
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.SatOps

Namespace Controls

  Public Class FeedProductionAudioSettingListModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))
    '.IROAudioSettingListModal

    Private mModalID As String = "ROAudioSettingListModal"
    Private mFeedProduction As String = "ViewModel.ROAudioSettingList()"
    Private mIsAudioSettingSelectedCssFunction As String
    Private mIsAudioSettingSelectedHtmlFunction As String
    Private mOnAudioSettingSelectedFunction As String

    Public Sub New(ModalID As String,
                   FeedProduction As String,
                   IsAudioSettingSelectedCssFunction As String,
                   IsAudioSettingSelectedHtmlFunction As String,
                   OnAudioSettingSelectedFunction As String)
      mModalID = ModalID
      mFeedProduction = FeedProduction
      mIsAudioSettingSelectedCssFunction = IsAudioSettingSelectedCssFunction
      mIsAudioSettingSelectedHtmlFunction = IsAudioSettingSelectedHtmlFunction
      mOnAudioSettingSelectedFunction = OnAudioSettingSelectedFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Production Audio Settings", False, "modal-xs", BootstrapEnums.Style.Primary, )
        With .Body
          With .Helpers.With(Of FeedProduction)(mFeedProduction)
            With .Helpers.Bootstrap.TableFor(Of FeedProductionAudioSetting)(Function(d) d.FeedProductionAudioSettingList,
                                                                          True, True, False, False, True, True, True)
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y no-border-x no-border-x-top"
              With .FirstRow
                .AddClass("items selectable")
                '.AddBinding(KnockoutBindingString.click, mOnAudioSettingSelectedFunction)
                'With .AddColumn("")
                '  .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                '  With .Helpers.HTMLTag("span")
                '    .AddBinding(KnockoutBindingString.css, mIsAudioSettingSelectedCssFunction)
                '    .AddBinding(KnockoutBindingString.html, mIsAudioSettingSelectedHtmlFunction)
                '  End With
                'End With
                With .AddColumn(Function(c As FeedProductionAudioSetting) c.AudioSettingID)
                End With
              End With
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
