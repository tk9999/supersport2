﻿Imports Singular.Web
Imports OBLib.Maintenance.Productions.ReadOnly

Namespace Controls

  Public Class ROEventTypeSelectControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROEventTypeSelectControl)

    Private mModalID As String
    Private mTableID As String
    Private mItemSelectedCssFunction As String
    Private mItemSelectedHtmlFunction As String
    Private mOnItemSelected As String
    Private mRefreshFunction As String
    Private mDelayedRefreshFunction As String

    Public Sub New(ModalID As String,
                   TableID As String,
                   ItemSelectedCssFunction As String,
                   ItemSelectedHtmlFunction As String,
                   OnItemSelected As String,
                   RefreshFunction As String,
                   DelayedRefreshFunction As String)
      mModalID = ModalID
      mTableID = TableID
      mItemSelectedCssFunction = ItemSelectedCssFunction
      mItemSelectedHtmlFunction = ItemSelectedHtmlFunction
      mOnItemSelected = OnItemSelected
      mRefreshFunction = RefreshFunction
      mDelayedRefreshFunction = DelayedRefreshFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Genre and Series Select", False, "modal-sm", Singular.Web.BootstrapEnums.Style.Primary, "modal-purple")
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROEventTypePagedListCriteria.ProductionType, BootstrapEnums.InputSize.Small, "Genre...")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayedRefreshFunction & " }")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROEventTypePagedListCriteria.EventType, BootstrapEnums.InputSize.Small, "Series...")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayedRefreshFunction & " }")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 4)
              .AddClass("pull-right")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                               Singular.Web.PostBackType.None, mRefreshFunction)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROEventTypePaged)(Function(d) d.ROEventTypePagedListManager,
                                                                          Function(d) d.ROEventTypePagedList,
                                                                          False, False, False, False, True, True, True,
                                                                          "EventTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                          mOnItemSelected)
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                  .Attributes("id") = mTableID
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn("")
                      .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                      With .Helpers.HTMLTag("span")
                        .AddBinding(KnockoutBindingString.css, mItemSelectedCssFunction)
                        .AddBinding(KnockoutBindingString.html, mItemSelectedHtmlFunction)
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.ProductionType)
                      '.HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                    With .AddReadOnlyColumn(Function(d As ROEventTypePaged) d.EventType)
                      '.HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With
                  'If mOnItemSelected <> "" Then
                  '  .FirstRow.AddBinding(Singular.Web.KnockoutBindingString.click, mOnItemSelected)
                  'End If
                End With
              End With
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
