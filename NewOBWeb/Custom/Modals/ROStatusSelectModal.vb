﻿Imports Singular.Web
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

Namespace Controls

  Public Class ROStatusSelectModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROStatusSelect)

    Private mModalID As String
    Private mTableID As String
    Private mItemSelectedCssFunction As String
    Private mItemSelectedHtmlFunction As String
    Private mOnItemSelected As String
    'Private mBackFunction As String
    'Private mDoneFunction As String

    Public Sub New(ModalID As String, TableID As String,
                   ItemSelectedCssFunction As String,
                   ItemSelectedHtmlFunction As String,
                   OnItemSelected As String)
      mModalID = ModalID
      mTableID = TableID
      mItemSelectedCssFunction = ItemSelectedCssFunction
      mItemSelectedHtmlFunction = ItemSelectedHtmlFunction
      mOnItemSelected = OnItemSelected
      'BackFunction As String,
      'DoneFunction As String
      'mBackFunction = BackFunction
      'mDoneFunction = DoneFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Select Status", False, "modal-xs", Singular.Web.BootstrapEnums.Style.Custom, "modal-purple", "fa-question", "fa-2x", True)
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.TableFor(Of ROProductionAreaAllowedStatus)("ViewModel.ROAllowedStatusList()",
                                                                               False, False, False, False, True, True, False)
              .Attributes("id") = mTableID
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y no-border-x no-border-x-top"
              With .FirstRow
                .AddClass("items selectable")
                With .AddColumn("")
                  .Style.Width = "60px"
                  With .Helpers.HTMLTag("span")
                    .AddBinding(KnockoutBindingString.css, mItemSelectedCssFunction)
                    .AddBinding(KnockoutBindingString.html, mItemSelectedHtmlFunction)
                  End With
                End With
                With .AddReadOnlyColumn(Function(c As ROProductionAreaAllowedStatus) c.ProductionAreaStatus)
                End With
                With .AddColumn("")
                  With .Helpers.Div
                    .AddBinding(KnockoutBindingString.css, "$data.CssClass()")
                    .Style.Height = "20px"
                  End With
                End With
              End With
              If mOnItemSelected <> "" Then
                .FirstRow.AddBinding(Singular.Web.KnockoutBindingString.click, mOnItemSelected)
              End If
            End With
          End With
        End With
        'With .Footer
        '  With .Helpers.DivC("pull-left")
        '    With .Helpers.Bootstrap.Button(, "Back", BootstrapEnums.Style.Danger,
        '                                   , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left",
        '                                   , PostBackType.None, mBackFunction)
        '    End With
        '  End With
        '  With .Helpers.DivC("pull-right")
        '    With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Primary,
        '                                   , BootstrapEnums.ButtonSize.Medium, , "fa-check-square-o",
        '                                   , PostBackType.None, mDoneFunction)
        '    End With
        '  End With
        'End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
