﻿Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.ReadOnly

Namespace Controls

  Public Class ROAllowedDisciplineSelectControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROAllowedDisciplineSelectControl)

    ''Private mItemSelectedCssFunction As String
    ''Private mItemSelectedHtmlFunction As String
    ''Private mOnItemSelected As String
    'Private mBackFunction As String
    'Private mDoneFunction As String

    'Public Sub New(BackFunction As String,
    '               DoneFunction As String)
    '  'ItemSelectedCssFunction As String,
    '  'ItemSelectedHtmlFunction As String,
    '  'OnItemSelected As String,
    '  'mItemSelectedCssFunction = ItemSelectedCssFunction
    '  'mItemSelectedHtmlFunction = ItemSelectedHtmlFunction
    '  'mOnItemSelected = OnItemSelected
    '  mBackFunction = BackFunction
    '  mDoneFunction = DoneFunction
    'End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

        'Done--------------------------------------------------------------------
      With Helpers.Bootstrap.Row
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          With .Helpers.ForEach(Of ROAllowedDisciplineSelect)("ViewModel.ROAllowedDisciplineSelectList()")
            With .Helpers.Div
              .AddBinding(KnockoutBindingString.css, "ROAllowedDisciplineSelectBO.GetSelectedCss($data)")
              With .Helpers.HTMLTag("span")
                .AddClass("discipline-position-quantity")
                .AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                With .Helpers.EditorFor(Function(c) c.Quantity)
                  .AddClass("round-editor")
                End With
              End With
              With .Helpers.HTMLTag("span")
                .AddClass("discipline-position-name hide-overflow")
                .AddBinding(KnockoutBindingString.click, "ROAllowedDisciplineSelectBO.OnSelect($data, $element)")
                .AddBinding(KnockoutBindingString.html, "ROAllowedDisciplineSelectBO.GetDisciplinePositionHTML($data)")
                .AddBinding(KnockoutBindingString.title, "$data.DisciplinePosition()")
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
