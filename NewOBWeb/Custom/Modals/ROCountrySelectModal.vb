﻿Imports Singular.Web
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly

Namespace Controls

  Public Class ROCountrySelectModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROCountrySelect)

    Private mModalID As String = "ROCountryListModal"
    Private mTableID As String = "ROCountryList"
    Private mControlInstanceName As String = "ROCountrySelector"
    Private mCountryListCriteria As String = "ViewModel.ROCountryListCriteria()"
    Private mCountryListManager As String = "ViewModel.ROCountryListManager"
    Private mROCountryList As String = "ViewModel.ROCountryList()"
    Private mOnCountrySelected As String
    Private mDelayedRefreshFunction As String

    Public Sub New(ModalID As String,
                   TableID As String,
                   CriteriaProperty As String,
                   CountryListManager As String,
                   ROCountryList As String,
                   ControlInstanceName As String,
                   OnCountrySelected As String,
                   DelayedRefreshFunction As String)
      mModalID = ModalID
      mTableID = TableID
      mControlInstanceName = ControlInstanceName
      mCountryListManager = CountryListManager
      mROCountryList = ROCountryList
      mOnCountrySelected = OnCountrySelected
      mCountryListCriteria = CriteriaProperty
      mDelayedRefreshFunction = DelayedRefreshFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Select Country", False, "modal-xs", Singular.Web.BootstrapEnums.Style.Custom, "modal-purple", "fa-country", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Control(New ROCountrySelectGrid(Of VMType)(mTableID, mCountryListCriteria, mCountryListManager, mROCountryList, mOnCountrySelected, mDelayedRefreshFunction))
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
