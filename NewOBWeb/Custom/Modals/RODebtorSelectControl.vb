﻿Imports Singular.Web
Imports OBLib.Quoting.ReadOnly

Namespace Controls

    Public Class RODebtorSelectControl(Of VMType)
        Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IRODebtorSelectControl)

        Private mTableID As String
        Private mItemSelectedCssFunction As String
        Private mItemSelectedHtmlFunction As String
        Private mOnItemSelected As String
        Private mDelayRefreshFunction As String
        Private mBackFunction As String
        Private mDoneFunction As String

        Public Sub New(TableID As String,
                       ItemSelectedCssFunction As String,
                       ItemSelectedHtmlFunction As String,
                       OnItemSelected As String,
                       DelayRefreshFunction As String,
                       BackFunction As String,
                       DoneFunction As String)
            mTableID = TableID
            mItemSelectedCssFunction = ItemSelectedCssFunction
            mItemSelectedHtmlFunction = ItemSelectedHtmlFunction
            mOnItemSelected = OnItemSelected
            mDelayRefreshFunction = DelayRefreshFunction
            mBackFunction = BackFunction
            mDoneFunction = DoneFunction
        End Sub

        Protected Overrides Sub Setup()
            MyBase.Setup()
            With Helpers.FieldSet("Select Client")
                'Done--------------------------------------------------------------------
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.DivC("pull-left")
                        With .Helpers.Bootstrap.Button(, "Back", BootstrapEnums.Style.Danger,
                                                       , BootstrapEnums.ButtonSize.Small, , "fa-arrow-left",
                                                       , PostBackType.None, mBackFunction)
                        End With
                    End With
                    With .Helpers.DivC("pull-right")
                        With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Primary,
                                                       , BootstrapEnums.ButtonSize.Small, , "fa-check-square-o",
                                                       , PostBackType.None, mDoneFunction)
                        End With
                    End With
                End With
                With .Helpers.With(Of RODebtorList.Criteria)("ViewModel.RODebtorListCriteria()")
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Search...")
                            If mDelayRefreshFunction <> "" Then
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayRefreshFunction & " }")
                            End If
                        End With
                    End With
                End With
                With .Helpers.Bootstrap.PagedGridFor(Of RODebtor)("RODebtorListManager",
                                                                  "ViewModel.RODebtorList",
                                                                   False, False, False, False, True, True, True,
                                                                   "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                   mOnItemSelected)
                    .AddClass("no-border hover list")
                    .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                    .Attributes("id") = mTableID
                    With .FirstRow
                        .AddClass("items selectable")
                        With .AddColumn("")
                            .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                            With .Helpers.HTMLTag("span")
                                .AddBinding(KnockoutBindingString.css, mItemSelectedCssFunction)
                                .AddBinding(KnockoutBindingString.html, mItemSelectedHtmlFunction)
                            End With
                        End With
                        With .AddReadOnlyColumn(Function(c As RODebtor) c.Debtor)
                        End With
                        With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactName)
                        End With
                        With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactNumber)
                        End With
                    End With
                End With
            End With
        End Sub

        Protected Overrides Sub Render()
            MyBase.Render()
            RenderChildren()
        End Sub

    End Class

End Namespace
