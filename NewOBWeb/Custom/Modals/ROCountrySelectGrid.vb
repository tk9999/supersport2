﻿Imports Singular.Web
Imports OBLib.Maintenance.Locations.ReadOnly

Public Class ROCountrySelectGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROCountrySelect)

  Private mTableID As String = "ROCountryList"
  'Private mControlInstanceName As String = "ROCountrySelector"
  Private mCriteriaProperty As String
  Private mDelayRefreshFunction As String
  Private mCountryListManager As String = "ViewModel.ROCountryListManager"
  Private mROCountryList As String = "ViewModel.ROCountryList()"
  Private mOnCountrySelected As String

  Public Sub New(TableID As String,
                 CriteriaProperty As String,
                 CountryListManager As String,
                 ROCountryList As String,
                 OnCountrySelected As String,
                 DelayRefreshFunction As String)
    mTableID = TableID
    mCountryListManager = CountryListManager
    mROCountryList = ROCountryList
    mOnCountrySelected = OnCountrySelected
    mCriteriaProperty = CriteriaProperty
    mDelayRefreshFunction = DelayRefreshFunction
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.FlatBlock(, True, True, False)
      With .AboveContentTag
        With .Helpers.Bootstrap.Row
          With .Helpers.With(Of ROCountryPagedList.Criteria)(mCriteriaProperty)
            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
              .Helpers.Bootstrap.LabelFor(Function(d As ROCountryPagedList.Criteria) d.Country).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As ROCountryPagedList.Criteria) d.Country, Singular.Web.BootstrapEnums.InputSize.Small, , "Country Name...")
                If mDelayRefreshFunction <> "" Then
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayRefreshFunction & " }")
                End If
              End With
            End With
          End With
        End With
      End With
      With .ContentTag
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.PagedGridFor(Of ROCountryPaged)(mCountryListManager,
                                                                   mROCountryList,
                                                                    False, False, False,
                                                                    False, True, True, True,
                                                                    "",
                                                                    Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                    "", False, True)
            .Attributes("id") = mTableID
            .AddClass("no-border hover list")
            With .FirstRow
              .AddBinding(Singular.Web.KnockoutBindingString.click, mOnCountrySelected & "($data)")
              .AddClass("items selectable")
              With .AddColumn("")
                .Style.Width = "60px"
                With .Helpers.Bootstrap.StateButton(Function(d As ROCountryPaged) d.IsSelected, "Selected", "Select", , , , "fa-minus", )
                End With
              End With
              With .AddReadOnlyColumn(Function(d As ROCountryPaged) d.Country)
              End With
            End With
          End With
        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
