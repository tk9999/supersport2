﻿Imports Singular.Web
Imports OBLib.Quoting.ReadOnly

Namespace Controls

  Public Class RODebtorSelectGrid(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IRODebtorSelectControl)

    Private mTableID As String
    Private mOnItemSelected As String
    Private mDelayRefreshFunction As String
    Private mCriteriaProperty As String = "ViewModel.RODebtorListCriteria()"
    Private mManagerProperty As String = "ViewModel.RODebtorListManager"
    Private mListProperty As String = "ViewModel.RODebtorList()"

    Public Sub New(TableID As String,
                   CriteriaProperty As String,
                   ManagerProperty As String,
                   ListProperty As String,
                   OnItemSelected As String,
                   DelayRefreshFunction As String)
      mTableID = TableID
      mOnItemSelected = OnItemSelected
      mDelayRefreshFunction = DelayRefreshFunction
      mCriteriaProperty = CriteriaProperty
      mManagerProperty = ManagerProperty
      mListProperty = ListProperty
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of RODebtorList.Criteria)(mCriteriaProperty)
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Search...")
            If mDelayRefreshFunction <> "" Then
              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayRefreshFunction & " }")
            End If
          End With
        End With
      End With
      With Helpers.Bootstrap.PagedGridFor(Of RODebtor)(mManagerProperty,
                                                        mListProperty,
                                                         False, False, False, False, True, True, True,
                                                         "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                         "", False, True)
        .AddClass("no-border hover list")
        .Attributes("id") = mTableID
        With .FirstRow
          .AddBinding(Singular.Web.KnockoutBindingString.click, mOnItemSelected & "($data)")
          .AddClass("items selectable")
          With .AddColumn("")
            .Style.Width = "60px"
            .Helpers.Bootstrap.StateButton(Function(d As RODebtor) d.IsSelected, "Selected", "Select", , , , )
          End With
          With .AddReadOnlyColumn(Function(c As RODebtor) c.Debtor)
          End With
          With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactName)
          End With
          With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactNumber)
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
