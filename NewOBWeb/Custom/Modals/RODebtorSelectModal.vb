﻿Imports Singular.Web
Imports OBLib.Quoting.ReadOnly

Namespace Controls

  Public Class RODebtorSelectModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IRODebtorSelectModal)

    Private mModalID As String
    Private mTableID As String
    Private mItemSelectedCssFunction As String
    Private mItemSelectedHtmlFunction As String
    Private mOnItemSelected As String
    Private mDelayRefreshFunction As String

    Public Sub New(ModalID As String, TableID As String,
                   ItemSelectedCssFunction As String,
                   ItemSelectedHtmlFunction As String,
                   OnItemSelected As String,
                   DelayRefreshFunction As String)
      mModalID = ModalID
      mTableID = TableID
      mItemSelectedCssFunction = ItemSelectedCssFunction
      mItemSelectedHtmlFunction = ItemSelectedHtmlFunction
      mOnItemSelected = OnItemSelected
      mDelayRefreshFunction = DelayRefreshFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Select Debtor", False, "modal-sm", Singular.Web.BootstrapEnums.Style.Warning, , "fa-credit-card", "fa-2x", True)
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of RODebtorList.Criteria)("ViewModel.ModalRODebtorListCriteria()")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Search...")
                  If mDelayRefreshFunction <> "" Then
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayRefreshFunction & " }")
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.PagedGridFor(Of RODebtor)("ViewModel.ModalRODebtorListManager",
                                                              "ViewModel.ModalRODebtorList()",
                                                               False, False, False, False, True, True, True,
                                                               "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                               mOnItemSelected, False, True)
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y no-border-x no-border-x-top"
              .Attributes("id") = mTableID
              With .FirstRow
                .AddClass("items selectable")
                With .AddColumn("")
                  .Style.Width = "60px"
                  With .Helpers.HTMLTag("span")
                    .AddBinding(KnockoutBindingString.css, mItemSelectedCssFunction)
                    .AddBinding(KnockoutBindingString.html, mItemSelectedHtmlFunction)
                  End With
                End With
                With .AddReadOnlyColumn(Function(c As RODebtor) c.Debtor)
                End With
                With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactName)
                End With
                With .AddReadOnlyColumn(Function(c As RODebtor) c.ContactNumber)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
