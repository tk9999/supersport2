﻿Imports Singular.Web
Imports OBLib.Maintenance.Locations.ReadOnly

Public Class ROCitySelectGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROCitySelect)

  Private mTableID As String = "ROityList"
  Private mControlInstanceName As String = "ROCitySelector"

  Public Sub New(TableID As String,
                 ControlInstanceName As String)
    mTableID = TableID
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.PagedGridFor(Of ROCityPaged)(Function(d) d.ROCityListManager,
                                                            Function(d) d.ROCityList,
                                                            False, False, False,
                                                            False, True, True, True,
                                                            "",
                                                            Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                            "")
      'mControlInstanceName & ".onRowSelected"
      'mControlInstanceName & ".onRowSelected"
      .Attributes("id") = mTableID
      .AddClass("no-border hover list")
      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
      '.Pager.PagerListTag.ListTag.AddClass("pull-left")
      With .FirstRow
        .AddBinding(Singular.Web.KnockoutBindingString.click, mControlInstanceName & ".onRowSelected($data)")
        .AddClass("items selectable")
        With .AddColumn("")
          .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
          With .Helpers.HTMLTag("span")
            .AddBinding(KnockoutBindingString.css, mControlInstanceName & ".isItemSelectedCss($data)")
            .AddBinding(KnockoutBindingString.html, mControlInstanceName & ".isItemSelectedHtml($data)")
          End With
        End With
        With .AddReadOnlyColumn(Function(d As ROCityPaged) d.CountryID)
        End With
        With .AddReadOnlyColumn(Function(d As ROCityPaged) d.City)
        End With
        With .AddReadOnlyColumn(Function(d As ROCityPaged) d.CityCode)
        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
