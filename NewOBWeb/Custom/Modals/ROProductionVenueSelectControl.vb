﻿Imports Singular.Web
Imports OBLib.Maintenance.Productions.ReadOnly

Namespace Controls

  Public Class ROProductionVenueSelectControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROProductionVenueSelectControl)

    Private mModalID As String
    Private mTableID As String
    Private mItemSelectedCssFunction As String
    Private mItemSelectedHtmlFunction As String
    Private mOnItemSelected As String
    Private mRefreshFunction As String
    Private mDelayedRefreshFunction As String

    Public Sub New(ModalID As String,
                   TableID As String,
                   ItemSelectedCssFunction As String,
                   ItemSelectedHtmlFunction As String,
                   OnItemSelected As String,
                   RefreshFunction As String,
                   DelayedRefreshFunction As String)
      mModalID = ModalID
      mTableID = TableID
      mItemSelectedCssFunction = ItemSelectedCssFunction
      mItemSelectedHtmlFunction = ItemSelectedHtmlFunction
      mOnItemSelected = OnItemSelected
      mRefreshFunction = RefreshFunction
      mDelayedRefreshFunction = DelayedRefreshFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Production Venue Select", False, "modal-xs", Singular.Web.BootstrapEnums.Style.Primary, "modal-purple")
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROProductionVenueListCriteria.ProductionVenue, BootstrapEnums.InputSize.Small)
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayedRefreshFunction & " }")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              .AddClass("pull-right")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                               Singular.Web.PostBackType.None, mRefreshFunction)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.DivC("table-responsive")
                With .Helpers.Bootstrap.PagedGridFor(Of ROProductionVenueOld)(Function(d) d.ROProductionVenueListManager,
                                                                               Function(d) d.ROProductionVenueList,
                                                                               False, False, False, False, True, True, True,
                                                                               "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                               mOnItemSelected)
                  .Attributes("id") = mTableID
                  .AddClass("no-border hover list")
                  .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    With .AddColumn("")
                      .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                      With .Helpers.HTMLTag("span")
                        .AddBinding(KnockoutBindingString.css, mItemSelectedCssFunction)
                        .AddBinding(KnockoutBindingString.html, mItemSelectedHtmlFunction)
                      End With
                    End With
                    With .AddReadOnlyColumn(Function(d As ROProductionVenueOld) d.ProductionVenue)
                      '.HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
