﻿Imports Singular.Web
Imports OBLib.Productions
Imports OBLib.Productions.ReadOnly

Namespace Controls

  Public Class ROAllowedDisciplineSelectModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROAllowedDisciplineSelectControl)

    Private mBackFunction As String
    Private mDoneFunction As String
    Private mModalID As String

    Public Sub New(ModalID As String,
                   BackFunction As String,
                   DoneFunction As String)
      mModalID = ModalID
      mBackFunction = BackFunction
      mDoneFunction = DoneFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog(mModalID, "Select Disciplines", False, "modal-sm", BootstrapEnums.Style.Warning, , "da-select", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.FlatBlock(, False, True, False)
              With .ContentTag
                .Helpers.Control(New ROAllowedDisciplineSelectControl(Of VMType))
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.DivC("pull-left")
                With .Helpers.Bootstrap.Button(, "Back", BootstrapEnums.Style.Danger,
                                               , BootstrapEnums.ButtonSize.Small, , "fa-arrow-left",
                                               , PostBackType.None, mBackFunction)
                End With
              End With
              With .Helpers.DivC("pull-right")
                With .Helpers.Bootstrap.Button(, "Done", BootstrapEnums.Style.Primary,
                                               , BootstrapEnums.ButtonSize.Small, , "fa-check-square-o",
                                               , PostBackType.None, mDoneFunction)
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
