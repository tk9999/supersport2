﻿Imports Singular.Web
Imports OBLib.Maintenance.Locations.ReadOnly

Namespace Controls

    Public Class FindROCityModal(Of VMType)
        Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROCitySelect)

        Private mControlInstanceName As String = "ROCitySelector"
        Private mModalID As String = "ROCityListModal"
        Private mTableID As String = "ROCityList"
        Private mROCityListCriteria As String = "ViewModel.ROCityListCriteria()"
        Private mROCityListManager As String = "ViewModel.ROCityListManager"
        Private mROCityList As String = "ViewModel.ROCityList()"
        Private mOnCitySelected As String = mControlInstanceName & ".onCitySelected()"
        Private mDelayedRefreshFunction As String = mControlInstanceName & ".delayedRefresh()"

        Public Sub New(ControlInstanceName As String,
                       ModalID As String,
                       TableID As String,
                       ROCityListCriteria As String,
                       ROCityListManager As String,
                       ROCityList As String,
                       OnCitySelected As String,
                       DelayedRefreshFunction As String)
            mControlInstanceName = ControlInstanceName
            mModalID = ModalID
            mTableID = TableID
            mROCityListManager = ROCityListManager
            mROCityList = ROCityList
            mOnCitySelected = OnCitySelected
            mROCityListCriteria = ROCityListCriteria
            mDelayedRefreshFunction = DelayedRefreshFunction
        End Sub

        Protected Overrides Sub Setup()
            MyBase.Setup()
            With Helpers.Bootstrap.Dialog(mModalID, "Select City", False, "modal-xs", Singular.Web.BootstrapEnums.Style.Primary, "modal-purple", "fa-building-o", "fa-2x", True)
                With .Body
                    With .Helpers.Bootstrap.Row
                        With .Helpers.With(Of OBLib.Maintenance.Locations.ReadOnly.ROCityPagedList.Criteria)(mROCityListCriteria)
                            With .Helpers.Bootstrap.Column(12, 12, 4, 4, 4)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                    .Helpers.Bootstrap.LabelFor(Function(d) d.Country)
                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.Country, BootstrapEnums.InputSize.Small)
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mControlInstanceName & ".delayedRefresh(" & mControlInstanceName & ") }")
                                        .Editor.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".searchByCountryEnabled()")
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 4, 4, 4)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                    .Helpers.Bootstrap.LabelFor(Function(d) d.City)
                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.City, BootstrapEnums.InputSize.Small)
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mControlInstanceName & ".delayedRefresh }")
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 4, 4, 4)
                                With .Helpers.DivC("pull-right")
                                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                        With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                                       Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                                       Singular.Web.PostBackType.None, mControlInstanceName & ".refresh()")
                                        End With
                                    End With
                                End With
                            End With
                        End With
                    End With
                    With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.PagedGridFor(Of ROCityPaged)(mROCityListManager,
                                                                            mROCityList,
                                                                            False, False, False, False, True, True, True,
                                                                            "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                            mOnCitySelected, False, True)
                            .Attributes("id") = mTableID
                            .AddClass("no-border hover list")
                            With .FirstRow
                                '.AddBinding(Singular.Web.KnockoutBindingString.click, mControlInstanceName & ".onRowSelected($data)")
                                .AddClass("items selectable")
                                With .AddColumn("")
                                    .Style.Width = "60px"
                                    With .Helpers.Bootstrap.StateButton(Function(d As ROCityPaged) d.IsSelected, "Selected", "Select", , , , "fa-minus", )
                                    End With
                                End With
                                With .AddReadOnlyColumn(Function(d As ROCityPaged) d.Country)
                                End With
                                With .AddReadOnlyColumn(Function(d As ROCityPaged) d.City)
                                End With
                                With .AddReadOnlyColumn(Function(d As ROCityPaged) d.CityCode)
                                End With
                            End With
                        End With
                    End With
                End With
                With .Footer

                End With
            End With
        End Sub

        Protected Overrides Sub Render()
            MyBase.Render()
            RenderChildren()
        End Sub

    End Class

End Namespace
