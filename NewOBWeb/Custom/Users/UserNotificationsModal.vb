﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers

Namespace Controls

  Public Class UserNotificationsModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("UserNotificationsModal", "My Notifications", False, "modal-sm", BootstrapEnums.Style.Info, , "fa-info-circle", , True)
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.DivC("mail-inbox")
                'With .Helpers.DivC("head")
                '  .Helpers.HTML.Heading3("Notifications")
                'End With
                With .Helpers.DivC("filters")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of OBLib.Users.ReadOnly.ROUserNotificationList.Criteria)("ViewModel.ROUserNotificationListCriteria()")
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Style.MarginAll("0")
                          .AddClass("pull-left")
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Users.ReadOnly.ROUserNotificationList.Criteria) d.StartDate, BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
                            .Editor.Style.Width = "150px"
                            .Editor.Style.Display = Display.inlineblock
                          End With
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Users.ReadOnly.ROUserNotificationList.Criteria) d.EndDate, BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
                            .Editor.Style.Width = "150px"
                            .Editor.Style.Display = Display.inlineblock
                          End With
                        End With
                        With .Helpers.Bootstrap.ButtonGroup
                          .AddClass("pull-right")
                          With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-angle-left", , PostBackType.None, )
                            .Button.AddBinding(KnockoutBindingString.click, "ViewModel.ROUserNotificationListManager().Prev()")
                          End With
                          With .Helpers.HTMLTag("input")
                            .AddClass("form-control")
                            .AddBinding(KnockoutBindingString.value, "ViewModel.ROUserNotificationListManager().PageNo()")
                            .AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
                            .Style.Width = "40px"
                            .Style.Height = "28px"
                            .Style.Display = Display.inlineblock
                            .Style.FloatLeft()
                          End With
                          With .Helpers.Div
                            .Style.Width = "40px"
                            .Style.Height = "28px"
                            .Style.Display = Display.inlineblock
                            .Style.FloatLeft()
                            .Style.TextAlign = TextAlign.center
                            .AddBinding(KnockoutBindingString.html, " 'of ' + ViewModel.ROUserNotificationListManager().Pages().toString()")
                          End With
                          With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-angle-right", , PostBackType.None, )
                            .Button.AddBinding(KnockoutBindingString.click, "ViewModel.ROUserNotificationListManager().Next()")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.ForEachTemplate(Of OBLib.Users.ReadOnly.ROUserNotification)("ViewModel.ROUserNotificationList()")
                  .AddClass("mails")
                  With .Helpers.DivC("item")
                    .AddBinding(KnockoutBindingString.click, "window.siteHubManager.downloadSynergyChangesWithBookingInfo($element, null, $data)")
                    'With .Helpers.Div

                    'End With
                    With .Helpers.Div
                      With .Helpers.SpanC("date pull-right")
                        With .Helpers.Span
                          .AddBinding(KnockoutBindingString.html, "$data.CreatedDateTimeString()")
                        End With
                      End With
                      With .Helpers.Span
                        .AddBinding(KnockoutBindingString.visible, "$data.HasFile()")
                        .AddClass("pull-right file-icon")
                        With .Helpers.HTMLTag("i")
                          .AddBinding(KnockoutBindingString.css, "$data.FileIcon()")
                        End With
                      End With
                      With .Helpers.HTMLTag("h4")
                        .AddClass("from")
                        .AddBinding(KnockoutBindingString.html, "$data.NotificationTitle()")
                      End With
                      With .Helpers.HTMLTag("p")
                        .AddClass("msg")
                        .AddBinding(KnockoutBindingString.html, "$data.NotificationTextShort()")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace