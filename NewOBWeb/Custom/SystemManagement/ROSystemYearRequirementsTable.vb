﻿Imports Singular.Web
Imports OBLib.Shifts.ICR
Imports OBLib.Shifts.PlayoutOperations
Imports OBLib.Maintenance.SystemManagement.ReadOnly

Namespace Controls

  Public Class ROSystemYearRequirementsTable(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROSystemYears)

    Private mROSystemYearRequirementListPropertyNameClean As String
    Private mEditSystemYearRequirementFunction As String
    Private mEditSystemYearRequirementTeamRequirementsFunction As String

    Public Sub New(Optional ROSystemYearRequirementListPropertyNameClean As String = "ROSystemYearRequirementList",
                   Optional editSystemYearRequirementFunction As String = "SystemManagementPage.editSystemYearRequirement($data)",
                   Optional editSystemYearRequirementTeamRequirementsFunction As String = "SystemManagementPage.editSystemYearRequirementTeamRequirements($data)")
      mROSystemYearRequirementListPropertyNameClean = ROSystemYearRequirementListPropertyNameClean
      mEditSystemYearRequirementFunction = editSystemYearRequirementFunction
      mEditSystemYearRequirementTeamRequirementsFunction = editSystemYearRequirementTeamRequirementsFunction
      'mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.PagedGridFor(Of ROSystemYear)("ViewModel.ROSystemYearListManager", "ViewModel.ROSystemYearList()",
                                                          False, False, False, False, True, True, True,
                                                          , BootstrapEnums.PagerPosition.Bottom,
                                                          , , True, )
        .AddClass("no-border hover list")
        .TableBodyClass = "no-border-y no-border-x"
        With .FirstRow
          .AddClass("items selectable")
          With .AddColumn("")
            .Style.Width = "120px"
            .Helpers.Bootstrap.Button(, "Edit Year", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, mEditSystemYearRequirementFunction, False)
            '.Helpers.Bootstrap.Button(, "Edit Team Requirements", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-users", , PostBackType.None, mEditSystemYearRequirementTeamRequirementsFunction, False)
          End With
          With .AddColumn(Function(c As ROSystemYear) c.SubDept)
          End With
          With .AddColumn(Function(c As ROSystemYear) c.YearStartDate)
          End With
          With .AddColumn(Function(c As ROSystemYear) c.YearEndDate)
          End With
          With .AddColumn(Function(c As ROSystemYear) c.CreateDetails)
          End With
          With .AddColumn(Function(c As ROSystemYear) c.ModDetails)
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace
