﻿Imports Singular.Web
Imports OBLib.Maintenance.SystemManagement

Namespace Controls

  Public Class EditYearRequirementModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mSystemYearProperty As String
    Private mSaveFunction As String

    Public Sub New(Optional YearRequirementProperty As String = "ViewModel.CurrentSystemYear()",
                   Optional SaveFunction As String = "SystemManagementPage.saveYearlyRequirment($data)")
      mSystemYearProperty = YearRequirementProperty
      mSaveFunction = SaveFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("EditYearRequirementModal", "Edit Year Requirement", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-edit", )
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.TabControl(, "nav-tabs", , BootstrapEnums.TabAlignment.Top)
                With .AddTab("YearSetup", "fa-calendar", "", "Year Setup", "", False)
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.With(Of SystemYear)(mSystemYearProperty)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
                          With .Helpers.FieldSet("Year")
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              .Helpers.Bootstrap.LabelFor(Function(d As SystemYear) d.YearStartDate).Style.Width = "100%"
                              .Helpers.Bootstrap.FormControlFor(Function(d As SystemYear) d.YearStartDate, BootstrapEnums.InputSize.Small, "Select Start of Year")
                            End With
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                              .Helpers.Bootstrap.LabelFor(Function(d As SystemYear) d.YearEndDate).Style.Width = "100%"
                              .Helpers.Bootstrap.FormControlFor(Function(d As SystemYear) d.YearEndDate, BootstrapEnums.InputSize.Small, , "Select End of Year")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
                          With .Helpers.FieldSet("Month Groups")
                            With .Helpers.DivC("table-responsive")
                              With .Helpers.Bootstrap.TableFor(Of SystemYearMonthGroup)(Function(c As SystemYear) c.SystemYearMonthGroupList, True, True, False, False, True, True, True)
                                .AddClass("no-border hover list")
                                With .FirstRow
                                  .AddClass("items selectable")
                                  With .AddColumn(Function(d As SystemYearMonthGroup) d.GroupName)
                                  End With
                                  With .AddColumn(Function(d As SystemYearMonthGroup) d.MonthGroupStartDate)
                                  End With
                                  With .AddColumn(Function(d As SystemYearMonthGroup) d.MonthGroupEndDate)
                                  End With
                                End With
                                With .AddChildTable(Of SystemYearMonthGroupMonth)(Function(c As SystemYearMonthGroup) c.SystemYearMonthGroupMonthList, True, True, False, False, True, True, True)
                                  .AddClass("no-border hover list")
                                  With .FirstRow
                                    With .AddColumn(Function(d As SystemYearMonthGroupMonth) d.MonthStartDate)
                                    End With
                                    With .AddColumn(Function(d As SystemYearMonthGroupMonth) d.MonthEndDate)
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .AddTab("TeamYearSetup", "fa-users", "", "Team Requirements", "", False)
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        '.AddClass("col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.With(Of SystemYear)(mSystemYearProperty)
                          With .Helpers.Bootstrap.Button(, "Setup", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, ,
                                                        "fa-gears", , PostBackType.None,
                                                        "SystemManagementPage.setupTeamYears($data)", False)
                          End With
                          With .Helpers.Bootstrap.TableFor(Of SystemYearTeamNumber)("$data.SystemYearTeamNumberList()",
                                                                                    False, False, False, False, True, True, True,
                                                                                    "SystemYearTeamNumberList")
                            .AddClass("no-border hover list")
                            With .FirstRow
                              .AddClass("items selectable")
                              'With .AddColumn(Function(d As SystemYearTeamNumber) d.SystemTeamNumber)
                              'End With
                              With .AddReadOnlyColumn(Function(d As SystemYearTeamNumber) d.SystemTeamNumberName)
                              End With
                              With .AddReadOnlyColumn(Function(d As SystemYearTeamNumber) d.RequiredHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As SystemYearTeamNumber) d.TimesheetHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As SystemYearTeamNumber) d.Timesheets)
                              End With
                            End With
                            With .AddChildTable(Of SystemYearTeamNumberMonthGroup)(Function(c As SystemYearTeamNumber) c.SystemYearTeamNumberMonthGroupList,
                                                                                   True, True, False, False, True, True, True)
                              .AddClass("no-border hover list")
                              With .FirstRow
                                .AddClass("items selectable")
                                With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroup) d.MonthGroupStartDate)
                                End With
                                With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroup) d.MonthGroupEndDate)
                                End With
                                With .AddColumn(Function(d As SystemYearTeamNumberMonthGroup) d.RequiredHoursGroup)
                                End With
                                With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroup) d.RequiredHoursHRTotal)
                                End With
                                With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroup) d.TimesheetHours)
                                End With
                                With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroup) d.Timesheets)
                                End With
                              End With
                              With .AddChildTable(Of SystemYearTeamNumberMonthGroupMonth)(Function(c As SystemYearTeamNumberMonthGroup) c.SystemYearTeamNumberMonthGroupMonthList, True, True, False, False, True, True, True)
                                .AddClass("no-border hover list")
                                With .FirstRow
                                  .AddClass("items selectable")
                                  With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.MonthStartDate)
                                  End With
                                  With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.MonthEndDate)
                                  End With
                                  With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.SystemTimesheetMonth)
                                  End With
                                  With .AddColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.RequiredHours)
                                  End With
                                  With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.RequiredHoursHRTotal)
                                  End With
                                  With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.TimesheetHours)
                                  End With
                                  With .AddReadOnlyColumn(Function(d As SystemYearTeamNumberMonthGroupMonth) d.Timesheets)
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.With(Of SystemYear)(mSystemYearProperty)
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of SystemYear)(mSystemYearProperty)
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", , PostBackType.None, mSaveFunction, False)
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  ''"$data.SystemMonthGroupRequirementList()"
  'With .AddChildTable(Of SystemMonthGroupRequirement)(Function(c As SystemMonthGroup) c.SystemMonthGroupRequirementList, True, True, False, False, True, True, True)
  '  .AddClass("no-border hover list")
  '  With .FirstRow
  '    .AddClass("items selectable")
  '    With .AddColumn(Function(d As SystemMonthGroupRequirement) d.SystemTeamNumberID)
  '    End With
  '    'With .AddColumn(Function(d As SystemMonthGroupRequirement) d.SystemMonthGroupID)
  '    'End With
  '    'With .AddColumn(Function(d As SystemMonthGroupRequirement) d.DisciplineID)
  '    'End With
  '    With .AddColumn(Function(d As SystemMonthGroupRequirement) d.RequiredHours)
  '    End With
  '  End With
  '  '"$data.SystemMonthGroupRequirementMonthList()"
  '  'With .AddChildTable(Of SystemMonthGroupRequirementMonth)(Function(c As SystemMonthGroupRequirement) c.SystemMonthGroupRequirementMonthList, True, True, False, False, True, True, True)
  '  '  .AddClass("no-border hover list")
  '  '  With .FirstRow
  '  '    .AddClass("items selectable")
  '  '    With .AddColumn(Function(d As SystemMonthGroupRequirementMonth) d.SystemMonthGroupMonthID)
  '  '    End With
  '  '    With .AddColumn(Function(d As SystemMonthGroupRequirementMonth) d.RequiredHours)
  '  '    End With
  '  '  End With
  '  'End With
  'End With

End Namespace