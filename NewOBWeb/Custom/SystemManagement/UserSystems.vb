﻿Imports Singular.Web

Namespace Controls

  Public Class UserSystems(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.FlatBlock("Sub Depts.")
        With .ContentTag
          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList")
            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected, "", "", , , , , )
              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.SystemName)
              .Button.AddClass("btn-block buttontext")
            End With
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                  .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "SystemManagementPage.showActions($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
