﻿Imports Singular.Web

Namespace Controls

  Public Class UserSystemActions(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

#Region "System Teams"
#End Region

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.FlatBlock("Sub-Dept Actions")
        .FlatBlockTag.AddClass("FadeHide")
        .FlatBlockTag.Attributes("id") = "Actions"
        With .ContentTag
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
              With .Helpers.Bootstrap.Button("", "Manage Year Requirements", , , , , , , , "SystemManagementPage.manageSystemYearRequirements()", )
                .Button.AddClass("btn-block buttontext")
              End With
            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Toolbar
                With .Helpers.MessageHolder
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
