﻿Imports Singular.Web

Namespace Controls

  Public Class AddSystemAreaShiftPatternModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IShiftPatterns)

    Private mModalID As String
    Private mControlInstanceName As String
    Private mTableID As String

    Public Sub New(ModalID As String,
                   ControlInstanceName As String,
                   TableID As String)
      mModalID = ModalID
      mTableID = TableID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("AddSystemAreaShiftPatternModal", "Add Shift Pattern", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , " fa-plus", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.DivC("pull-right ValidationPopup Msg-Validation CustomModalErrorMessage HoverMsg")
              .AddBinding(Singular.Web.KnockoutBindingString.html, "TeamManagementPage.getEditableShiftPatternBrokenRulesHTML()")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "!TeamManagementPage.checkEditableShiftPatternValidity()")
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern)("ViewModel.EditableSystemAreaShiftPattern")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.Column(12, 10, 3, 3, 3)
                    With .Helpers.Bootstrap.FlatBlock("Pattern Details")
                      With .ContentTag
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c) c.SystemID)
                          With .Helpers.Bootstrap.FormControlFor(Function(c) c.SystemID, BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c) c.ProductionAreaID)
                          With .Helpers.Bootstrap.FormControlFor(Function(c) c.ProductionAreaID, BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c) c.PatternName)
                          With .Helpers.Bootstrap.FormControlFor(Function(c) c.PatternName, BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(c) c.ShiftDuration)
                          With .Helpers.Bootstrap.FormControlFor(Function(c) c.ShiftDuration, BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 10, 9, 9, 9)
                    With .Helpers.Bootstrap.FlatBlock("Shift Days")
                      With .ContentTag
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay)("ViewModel.EditableSystemAreaShiftPattern().SystemAreaShiftPatternWeekDayList", False, False, False, False, True, True, True)
                            .AddClass("no-border hover list add-vertical-scroll")
                            .Attributes("id") = mTableID
                            'System Area Shift Pattern Week Day
                            .TableBodyClass = "no-border-y"
                            With .FirstRow
                              .AddBinding(KnockoutBindingString.enableChildren, "SystemAreaShiftPatternBO.CanEdit($data, 'DeleteSASPWD')")
                              With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.WeekNo, 50)
                              End With
                              With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.WeekDay, 100)
                              End With
                              With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.ShiftTypeID, 200)
                              End With
                              With .AddColumn("Start Time")
                                .Style.Width = "100px"
                                With .Helpers.Bootstrap.FormControlFor(Function(c) c.StartTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Time")
                                  .Editor.Style.Width = "100px"
                                End With
                              End With
                              With .AddColumn("End Time")
                                .Style.Width = "100px"
                                With .Helpers.Bootstrap.FormControlFor(Function(c) c.EndTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Time")
                                  .Editor.Style.Width = "100px"
                                End With
                              End With
                              With .AddColumn("")
                                With .Helpers.Bootstrap.Button("", "", BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TeamManagementPage.removeSASPWeekDay($data)", )
                                End With
                              End With
                              With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.CreateDetails, 250)
                              End With
                              With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.ModifiedDetails, 250)
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Button("", "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , , "TeamManagementPage.addSASPWeekDay()", )
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-save", ,
                                           PostBackType.None, "TeamManagementPage.saveSASPChanges()", True)
              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
            End With
            With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-save", ,
                                           PostBackType.None, "TeamManagementPage.saveSASPChanges(true)", True)
              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
