﻿Imports Singular.Web

Namespace Controls

  Public Class EditSystemAreaShiftPatternModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IShiftPatterns)

    Private mModalID As String
    Private mControlInstanceName As String
    Private mTableID As String

    Public Sub New(ModalID As String,
                   ControlInstanceName As String,
                   TableID As String)
      mModalID = ModalID
      mTableID = TableID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("EditSystemAreaShiftPatternModal", "Edit Shift Pattern", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , " fa-edit", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.DivC("pull-right ValidationPopup Msg-Validation CustomModalErrorMessage HoverMsg")
              .AddBinding(Singular.Web.KnockoutBindingString.html, "TeamManagementPage.getEditableShiftPatternBrokenRulesHTML()")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "!TeamManagementPage.checkEditableShiftPatternValidity()")
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern)("ViewModel.EditableSystemAreaShiftPattern")
              With .Helpers.Bootstrap.Column(12, 10, 8, 4, 4)
                With .Helpers.Bootstrap.FlatBlock("")
                  .HeaderTag.AddBinding(KnockoutBindingString.html, "TeamManagementPage.getPatternInfo($data)")
                  With .ContentTag
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.PatternName)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.PatternName, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.ShiftDuration)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.ShiftDuration, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(c) c.ProductionAreaID)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.ProductionAreaID, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.HRUsingPatternInd()")
                      With .Helpers.DivC("text-center tada animated infinite go")
                        With .Helpers.DivC("i-circle warning")
                          .Helpers.Bootstrap.FontAwesomeIcon("fa-warning")
                        End With
                        .Helpers.HTML.Heading4("Warning!")
                        With .Helpers.HTMLTag("p")
                          .Helpers.HTML("Shifts Already Generated from Pattern!")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 10, 9, 8, 8)
                With .Helpers.Bootstrap.FlatBlock("Shift Days")
                  With .ContentTag
                    With .Helpers.DivC("table-responsive")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay)("ViewModel.EditableSystemAreaShiftPattern().SystemAreaShiftPatternWeekDayList", False, False, False, False, True, True, True)
                        .AddClass("no-border hover list add-vertical-scroll")
                        .Attributes("id") = mTableID
                        'System Area Shift Pattern Week Day
                        .TableBodyClass = "no-border-y"
                        With .FirstRow
                          .AddBinding(KnockoutBindingString.enableChildren, "SystemAreaShiftPatternBO.CanEdit($data, 'DeleteSASPWD')")
                          With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.WeekNo)
                          End With
                          With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.WeekDay)
                          End With
                          With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.ShiftTypeID)
                          End With
                          With .AddColumn("Start Time")
                            .Style.Width = "100px"
                            With .Helpers.Bootstrap.FormControlFor(Function(c) c.StartTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Time")
                              .Editor.Style.Width = "100px"
                            End With
                          End With
                          With .AddColumn("End Time")
                            .Style.Width = "100px"
                            With .Helpers.Bootstrap.FormControlFor(Function(c) c.EndTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Time")
                              .Editor.Style.Width = "100px"
                            End With
                          End With
                          With .AddColumn("")
                            With .Helpers.Bootstrap.Button("", "", BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TeamManagementPage.removeSASPWeekDay($data)", )
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.CreateDetails)
                          End With
                          'With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.ModifiedDetails, 250)
                          'End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Button("", "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , , "TeamManagementPage.addSASPWeekDay()", )
                      .Button.AddBinding(KnockoutBindingString.enable, "SystemAreaShiftPatternBO.CanEdit($data, 'AddSASPWD')")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-save", ,
                                           PostBackType.None, "TeamManagementPage.saveSASPChanges()", True)
              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace