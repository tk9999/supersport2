﻿Imports Singular.Web
Imports OBLib.TeamManagement
Imports OBLib.Shifts.ICR
Imports OBLib.Shifts.PlayoutOperations

Namespace Controls

  Public Class AddTeamModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IAddTeam)

    Private mModalID As String
    Private mControlInstanceName As String

    Public Sub New(ModalID As String,
                   ControlInstanceName As String)
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "New Team", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-plus", "fa-2x", False)
        ' Toolbar
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.DivC("pull-right ValidationPopup Msg-Validation HoverMsg CustomModalErrorMessage")
              .AddBinding(Singular.Web.KnockoutBindingString.html, "TeamManagementPage.getTeamDetailsBrokenRulesHTML()")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "!TeamManagementPage.checkTeamDetailsValidity()")
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Column(12, 10, 8, 4, 4)
                With .Helpers.Bootstrap.FlatBlock("Team Details", False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.With(Of OBLib.TeamManagement.SystemTeamShiftPattern)("ViewModel.EditableSystemTeamShiftPattern")
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.SystemID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.ProductionAreaID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.TeamName)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.TeamName, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                ' Teams Discipline Human Resources
                With .Helpers.Bootstrap.FlatBlock("Team Members")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.With(Of OBLib.TeamManagement.SystemTeamShiftPattern)("ViewModel.EditableSystemTeamShiftPattern")
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of TeamDisciplineHumanResource)(Function(c) c.TeamDisciplineHumanResourceList, True, False, False, False, True, True, True)
                            .AddClass("no-border hover list")
                            ' add-vertical-scroll
                            With .FirstRow
                              With .AddColumn(Function(c As TeamDisciplineHumanResource) c.DisciplineID, 150)
                                .Editor.AddBinding(KnockoutBindingString.enable, "TeamDisciplineHumanResourceBO.CanEdit($data, 'DeleteTDHR')")
                              End With
                              With .AddColumn("Human Resource")
                                .Style.Width = "150px"
                                With .Helpers.Bootstrap.Button("", "", BootstrapEnums.Style.Primary, , , , "fa-user", , , "TeamManagementPage.bookTeamHR($data)", )
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.HumanResource)
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "TeamManagementPage.getHRCss($data)")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamDisciplineHumanResourceBO.CanEdit($data, 'HasDiscipline')")
                                  .Button.AddClass("btn-block buttontext BookTeamHRButton")
                                End With
                              End With
                              With .AddColumn("")
                                .Style.Width = "100px"
                                With .Helpers.Bootstrap.Button(, "Generate Shifts", Singular.Web.BootstrapEnums.Style.Primary, , , , "fa-gear", , Singular.Web.PostBackType.None, "TeamManagementPage.generateHRShiftsForIndividual($data)")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamDisciplineHumanResourceBO.CanEdit($data, 'HasSchedule')")
                                End With
                              End With
                              With .AddColumn("")
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Warning, , , , "fa-refresh", , Singular.Web.PostBackType.None, "TeamDisciplineHumanResourceBO.removeShifts($data)")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamDisciplineHumanResourceBO.CanEdit($data, 'RemoveShifts')")
                                End With
                              End With
                              With .AddColumn("")
                                .Style.Width = "50px"
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , Singular.Web.PostBackType.None, "TeamManagementPage.removeTDHR($data)")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamDisciplineHumanResourceBO.CanEdit($data, 'DeleteTDHR')")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 10, 8, 8, 8)
                With .Helpers.Bootstrap.FlatBlock("Team Pattern")
                  With .ContentTag
                    With .Helpers.With(Of OBLib.TeamManagement.SystemTeamShiftPattern)("ViewModel.EditableSystemTeamShiftPattern")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Button("", "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , " fa-refresh", , , "TeamManagementPage.refreshSASP()")
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'ResetDisable')")
                          .Button.Style.Width = "78px"
                        End With
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.TeamManagement.SystemTeamShiftPattern)("ViewModel.EditableSystemTeamShiftPattern", False, False, False, False, True, True, True)
                            .AddClass("no-border hover list add-vertical-scroll")
                            With .FirstRow
                              .ExpandButtonColumnWidth = "50px"
                              With .AddColumn(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.StartDate, 100)
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'DeleteShifts')")
                              End With
                              With .AddColumn(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.EndDate, 100)
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'DeleteShifts')")
                              End With
                              With .AddColumn(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.PatternName, 100)
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'PatternName')")
                              End With
                              With .AddColumn(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.SystemAreaShiftPatternID, 100, "Template")
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'AlreadyGenerated')")
                              End With
                              With .AddColumn(Function(d As OBLib.TeamManagement.SystemTeamShiftPattern) d.ShiftDuration, 100)
                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'ShiftDuration')")
                              End With
                              With .AddColumn("")
                                .Style.Width = "100px"
                                With .Helpers.Bootstrap.Button(, "Generate Shifts", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-gear", , PostBackType.None, "TeamManagementPage.generateHRShifts($data)")
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data, 'GenerateShifts')")
                                End With
                              End With
                            End With
                            With .AddChildTable(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay)(Function(d) d.SystemAreaShiftPatternWeekDayList, False, False, False, False, True, True, False)
                              .AddClass("no-border hover list")
                              .TableBodyClass = "no-border-y"
                              With .FirstRow
                                With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.WeekNo, 20)
                                  .Editor.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'TableDisable')")
                                End With
                                With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.WeekDay, 80)
                                  .Editor.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'TableDisable')")
                                End With
                                With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.ShiftTypeID, 200)
                                  .Editor.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'TableDisable')")
                                End With
                                With .AddColumn("Start Time")
                                  .Style.Width = "100px"
                                  With .Helpers.Bootstrap.FormControlFor(Function(c) c.StartTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Time")
                                    .Editor.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'TableDisable')")
                                    .Editor.Style.Width = "100px"
                                  End With
                                End With
                                With .AddColumn("End Time")
                                  .Style.Width = "100px"
                                  With .Helpers.Bootstrap.FormControlFor(Function(c) c.EndTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Time")
                                    .Editor.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'TableDisable')")
                                    .Editor.Style.Width = "100px"
                                  End With
                                End With
                                'With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.CreateDetails, 250)
                                'End With
                                'With .AddColumn(Function(c As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay) c.ModifiedDetails, 250)
                                'End With
                                With .AddColumn("")
                                  .Style.Width = "50px"
                                  With .Helpers.Bootstrap.Button("", "", BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TeamManagementPage.removeSASPWeekDayNonTemplate($data)", )
                                    .Button.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'TableDisable')")
                                  End With
                                End With
                              End With
                              With .FooterRow
                                With .AddColumn("")
                                  .Style.Width = "50px"
                                  With .Helpers.Bootstrap.Button("", "Add", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-plus-circle", , , "TeamManagementPage.addSASPWeekDayNonTemplate()", )
                                    .Button.AddBinding(KnockoutBindingString.enable, "TeamShiftPatternBO.CanEdit($data,'AlreadyGenerated')")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-save", ,
                                           PostBackType.None, "TeamManagementPage.saveNewTeam($data)", True)
              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
            End With
            With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-save", ,
                                           PostBackType.None, "TeamManagementPage.saveNewTeam($data, true)", True)
              .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace
