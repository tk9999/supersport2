﻿Imports Singular.Web
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly

Namespace Controls

  Public Class PatternModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mOnItemSelected As String
    Private mPagingManager As String
    Private mListName As String
    Private mCriteriaName As String
    Private mRowCssClass As String

    Public Sub New(ModalID As String,
                   Optional OnItemSelected As String = "OnItemSelected",
                   Optional PagingManager As String = "ViewModel.ROSystemAreaShiftPatternListManager",
                   Optional ListName As String = "ViewModel.ROSystemAreaShiftPatternList()",
                   Optional mCriteriaName As String = "ViewModel.ROSystemAreaShiftPatternListCriteria()",
                   Optional RowCssClass As String = ""
                   )
      mModalID = ModalID
      mOnItemSelected = OnItemSelected
      mPagingManager = PagingManager
      mListName = ListName
      mRowCssClass = RowCssClass
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Select Pattern", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , "fa-th-list", "fa-2x", True)
        .AddClass("modal-background-gray")
        With .Body
          .AddClass("colour-tone-modal")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
              With .Helpers.With(Of OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList.Criteria)("ViewModel.ROSystemAreaShiftPatternListCriteria()")
                With .Helpers.Bootstrap.FormControlFor(Function(c) c.KeyWord, BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Search for Pattern"
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: TeamManagementPage.DelayedRefreshList(ViewModel.ROSystemAreaShiftPatternListManager()) }")
                End With
              End With
            End With
          End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern)(mPagingManager,
                                                                                              mListName,
                                                                                              False, False, False, False, True, True, True, "",
                                                                                              Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                              "", False, True, mRowCssClass)
                .Attributes("id") = mModalID
                .AddClass("no-border hover list add-vertical-scroll")
                With .FirstRow
                  With .AddColumn("")
                    .Style.Width = "100px"
                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected, "", "", "btn-success", "btn-primary", , , )
                      .Button.AddClass("btn-block")
                    .Button.AddBinding(KnockoutBindingString.click, "TeamManagementPage.addPattern($data)")
                    End With
                  End With
                  With .AddColumn(Function(d As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern) d.PatternName)
                  End With
                End With
              End With
            End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace