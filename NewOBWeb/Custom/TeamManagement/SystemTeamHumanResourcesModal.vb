﻿Imports Singular.Web
Imports OBLib.TeamManagement.ReadOnly
Imports OBLib.TeamManagement

Namespace Controls

  Public Class SystemTeamHumanResourcesModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditTeam)

    Private mModalID As String
    Private mList As String

    Public Sub New(ModalID As String,
                   ListRef As String)
      mModalID = ModalID
      mList = ListRef
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Human Resource for Discipline", , , Singular.Web.BootstrapEnums.Style.Primary, , " fa-user-plus", "fa-2x", True)
        .ModalDialogDiv.AddClass("modal-xs")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("", True)
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.TableFor(Of ROSystemTeamSkilledHR)(mList, False, False, , , , , True)
                        .AddClass("add-vertical-scroll")
                        With .FirstRow
                          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Visible()")
                          With .AddColumn("")
                            .Style.Width = "30px"
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButton(Function(c) c.OnTeamInd, "", "", "btn-success", "btn-primary", , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamManagementPage.canSelectHR($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "TeamManagementPage.onTeam($data)")
                            End With
                          End With
                          .AddColumn(Function(c As ROSystemTeamSkilledHR) c.HumanResource)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
