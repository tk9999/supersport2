﻿Imports Singular.Web

Namespace Controls

  Public Class TeamActions(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.FlatBlock("Actions")
        .FlatBlockTag.Attributes("id") = "TeamActions"
        With .ContentTag
          'With .Helpers.Bootstrap.Row
          'With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
          With .Helpers.Bootstrap.Button("", "Manage Teams", , , , , , , , "TeamManagementPage.manageTeams()", )
            .Button.AddClass("btn-block buttontext")
          End With
          'End With
          'With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
          With .Helpers.Bootstrap.Button("", "Manage Shift Patterns", , , , , , , , "TeamManagementPage.manageSystemAreaShiftPatterns()", )
            .Button.AddClass("btn-block buttontext")
          End With
          'End With
          'With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
          With .Helpers.Bootstrap.Button("", "Manage Team Numbers", , , , , , , , "TeamManagementPage.manageTeamNumbers()", )
            .Button.AddClass("btn-block buttontext")
          End With
          'End With
          'With .Helpers.DivC("pull-right")
          '  With .Helpers.Toolbar
          '    With .Helpers.MessageHolder
          '    End With
          '  End With
          'End With
          'End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
