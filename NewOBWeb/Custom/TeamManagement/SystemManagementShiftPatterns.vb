﻿Imports Singular.Web
Namespace Controls
  Public Class SystemManagementShiftPatterns(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IShiftPatterns)

    Private mModalID As String
    Private mOnItemSelected As String
    Private mPagingManager As String
    Private mListName As String
    Private mRowCssClass As String

    Public Sub New(ModalID As String,
                   Optional OnItemSelected As String = "OnItemSelected",
                   Optional PagingManager As String = "ViewModel.ROSystemAreaShiftPatternListManager",
                   Optional ListName As String = "ViewModel.ROSystemAreaShiftPatternList()",
                   Optional RowCssClass As String = ""
                   )
      mModalID = ModalID
      mOnItemSelected = OnItemSelected
      mPagingManager = PagingManager
      mListName = ListName
      mRowCssClass = RowCssClass
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.DivC("table-responsive")
        .Attributes("id") = "ShiftPatternDiv"
        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern)(mPagingManager,
                                                                                        mListName,
                                                                                        False, False, False, False, True, True, True, "",
                                                                                        Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                        "", False, True, mRowCssClass)
          .Attributes("id") = mModalID
          .AddClass("no-border hover list")
          With .FirstRow
            With .AddColumn("")
              .Style.Width = "100px"
              With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected, "Edit", "Edit", "btn-success", "btn-primary", , , )
                .Button.AddClass("btn-block")
                .Button.AddBinding(KnockoutBindingString.click, "TeamManagementPage.editPattern($data)")
              End With
            End With
            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern) d.PatternName)
            End With
            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern) d.SystemName)
            End With
            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern) d.ProductionAreaName)
            End With
            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern) d.CreateDetails)
            End With
            With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPattern) d.ModifiedDetails)
            End With
            With .AddColumn("")
              .Style.Width = "50px"
              With .Helpers.Bootstrap.Button("", "", BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TeamManagementPage.removeSASP($data)", )
                .Button.AddBinding(KnockoutBindingString.enable, "SystemAreaShiftPatternBO.CanEdit($data, 'DeleteSASP')")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace
