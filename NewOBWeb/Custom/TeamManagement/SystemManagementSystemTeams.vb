﻿Imports OBLib.TeamManagement.ReadOnly

Namespace Controls

  Public Class SystemManagementSystemTeams(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).ISystemTeams)

#Region "System Teams"
    Private mTableID As String
    Private mOnItemSelected As String
    Private mPagingManager As String
    Private mListName As String
    Private mRowCssClass As String
#End Region

    Public Sub New(TableID As String,
                 Optional OnItemSelected As String = "OnItemSelected",
                 Optional PagingManager As String = "ViewModel.ROSystemTeamListManager",
                 Optional ListName As String = "ViewModel.ROSystemTeamList()",
                 Optional RowCssClass As String = "")
      mTableID = TableID
      mOnItemSelected = OnItemSelected
      mPagingManager = PagingManager
      mListName = ListName
      mRowCssClass = RowCssClass
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.FlatBlock("Teams", True)
        With .ContentTag
          'With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Button("", "Add", Singular.Web.BootstrapEnums.Style.Primary, ,
                                        Singular.Web.BootstrapEnums.ButtonSize.Small, , " fa-plus-circle", ,
                                        Singular.Web.PostBackType.None, "TeamManagementPage.addNewTeam()")
            .Button.Style.Width = "78px"
          End With
          With .Helpers.Bootstrap.PagedGridFor(Of ROSystemTeam)(mPagingManager, mListName,
                                                                False, False, False, False, True, True, True, "",
                                                                Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                "", False, True, mRowCssClass)
            .Attributes("id") = mTableID
            .AddClass("no-border hover list")
            With .FirstRow
              With .AddColumn("")
                .AddClass("col-xs-2 col-sm-1 col-md-1 col-lg-1 text-left")
                With .Helpers.Bootstrap.Button("", "Edit", Singular.Web.BootstrapEnums.Style.Primary, , , , "fa-edit", , , "TeamManagementPage.fetchEditableShiftPatterns($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.TeamName)
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.PatternName)
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.HROnTeam, 50)
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.StartDateString, 50)
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.EndDateString, 50)
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.ShiftCount, 50)
              End With
              With .AddColumn("")
                .Style.Width = "50px"
                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, , , , "fa-trash", , , "TeamManagementPage.removeTeam($data)")
                  '.Button.AddClass("btn-block")
                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TeamBO.CanEdit($data, 'DeleteTeam')")
                End With
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.CreateDetails)
              End With
              With .AddReadOnlyColumn(Function(c As ROSystemTeam) c.ModifiedDetails)
              End With
            End With
          End With
          'End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
