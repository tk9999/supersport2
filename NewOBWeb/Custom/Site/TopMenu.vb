﻿
Namespace CustomControls

  Public Class TopMenu
    Inherits System.Web.UI.WebControls.WebControl

    Public Property SiteMapDatasourceID As String
    Public Property BannerText As String

    Protected Overrides Sub Render(writer As System.Web.UI.HtmlTextWriter)

      Dim mSiteInfo As OBLib.Helpers.SiteInfo = New OBLib.Helpers.SiteInfo(Me.Page.Request)

      'TopBar----------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("id", "head-nav")
      writer.WriteAttribute("class", "navbar navbar-default")
      writer.Write(">")

      'Container-Fluid----------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "container-fluid")
      writer.Write(">")

      'Navbar-Collapse----------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "navbar-collapse")
      writer.Write(">")

      'UserNav------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "nav navbar-nav navbar-right user-nav")
      writer.Write(">")

      'ProfileMenu------------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.WriteAttribute("class", "dropdown profile_menu")
      writer.Write(">")

      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "#")
      writer.WriteAttribute("class", "dropdown-toggle")
      writer.WriteAttribute("data-toggle", "dropdown")
      writer.Write(">")

      'TopBarUserAvatarImage-------------------------------------------------------------
      writer.WriteBeginTag("img")
      writer.WriteAttribute("class", "avatar")
      writer.WriteAttribute("id", "userImageSmall")
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        writer.WriteAttribute("src", mSiteInfo.SitePath & "/Images/Profile/" & OBLib.Security.Settings.CurrentUserID.ToString & "_4.jpg")
      End If
      'userImageSmall
      writer.WriteAttribute("alt", "Avatar")
      writer.Write(">")
      writer.WriteEndTag("img") '-----------------------------------TopBarUserAvatarImage

      'UserName--------------------------------------------------------------
      writer.WriteBeginTag("span")
      writer.Write(">")
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        writer.Write(OBLib.Security.Settings.CurrentUser.FirstNameSurname)
      End If
      writer.WriteEndTag("span") '-----------------------------------UserName

      'UserName-----------------------------------------------------------
      writer.WriteBeginTag("b")
      writer.WriteAttribute("class", "caret")
      writer.Write(">")
      writer.WriteEndTag("b") '-----------------------------------UserName

      writer.WriteEndTag("a")

      'DropDownMenu------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "dropdown-menu")
      writer.Write(">")

      'ProfileLink-----------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", mSiteInfo.SitePath & "/User/UserProfile.aspx")
      writer.Write(">")
      writer.Write("Profile")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li") '-----------------------------------------------ProfileLink

      'MessagesLink-----------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "#")
      writer.Write(">")
      writer.Write("Messages")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li") '-----------------------------------------------MessagesLink

      'ChangePasswordLink-----------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", mSiteInfo.SitePath & "/User/ChangePassword.aspx")
      writer.Write(">")
      writer.Write("Change Password")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li") '-----------------------------------------------ChangePasswordLink

      'Divider------------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.WriteAttribute("class", "divider")
      writer.Write(">")
      writer.WriteEndTag("li") '-----------------------------------------------Divider

      'SignOutLink-----------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", mSiteInfo.SitePath & "/Account/Login.aspx?SCmd=Logout")
      writer.Write(">")
      writer.Write("Sign Out")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li") '-----------------------------------------------SignOutLink

      writer.WriteEndTag("ul") '----------------------------------------------DropDownMenu

      writer.WriteEndTag("li") '-----------------------------------------------ProfileMenu

      writer.WriteEndTag("ul") '-----------------------------------------------UserNav

      'NotNav------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "nav navbar-nav not-nav")
      writer.Write(">")

      'ButtonDropDown1-----------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.WriteAttribute("id", "userMessages")
      writer.WriteAttribute("class", "button dropdown")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "javascript:;")
      writer.WriteAttribute("class", "dropdown-toggle")
      writer.WriteAttribute("data-toggle", "dropdown")
      writer.Write(">")

      writer.WriteBeginTag("i")
      writer.WriteAttribute("class", "fa fa-inbox")
      writer.Write(">")
      writer.WriteEndTag("i")

      writer.WriteEndTag("a")

      'dropdown-menu messages------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "dropdown-menu wide messages")
      writer.Write(">")

      writer.WriteBeginTag("li")
      writer.Write(">")

      'NScroller-------------------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "nano nscroller")
      writer.Write(">")

      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "content")
      writer.Write(">")
      writer.WriteEndTag("div")

      writer.WriteEndTag("div") '-----------------------------------------------------------NScroller

      'Foot-------------------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "foot")
      writer.Write(">")
      writer.WriteBeginTag("li")
      writer.Write(">")
      '<a href="#">View all messages </a>
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "#")
      writer.Write(">")
      writer.Write("View all messages")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li")
      writer.WriteEndTag("ul") '------------------------------------------------------------Foot

      writer.WriteEndTag("li")

      writer.WriteEndTag("ul") '-----------------------------------------------dropdown-menu messages

      writer.WriteEndTag("li") '-----------------------------------------------ButtonDropDown1

      'ButtonDropDown2-----------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.WriteAttribute("id", "userNotifications")
      writer.WriteAttribute("class", "button dropdown")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "javascript:;")
      writer.WriteAttribute("class", "dropdown-toggle")
      writer.WriteAttribute("data-toggle", "dropdown")
      writer.Write(">")

      writer.WriteBeginTag("i")
      writer.WriteAttribute("class", "fa fa-globe")
      writer.Write(">")
      writer.WriteEndTag("i")

      writer.WriteBeginTag("span")
      writer.WriteAttribute("class", "bubble")
      writer.Write(">")
      writer.Write("0")
      writer.WriteEndTag("span")

      writer.WriteEndTag("a")

      'dropdown-menu messages------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "dropdown-menu wide")
      writer.Write(">")

      writer.WriteBeginTag("li")
      writer.Write(">")

      'NScroller-------------------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "nano nscroller has-scrollbar")
      writer.Write(">")

      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "content")
      writer.Write(">")

      'Container for Notifications
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("id", "userNotificationsUL")
      writer.Write(">")
      writer.WriteEndTag("ul")

      writer.WriteEndTag("div")

      writer.WriteEndTag("div") '-----------------------------------------------------------NScroller

      'Foot-------------------------------------------------------------------------------------
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "foot")
      writer.Write(">")
      writer.WriteBeginTag("li")
      writer.Write(">")
      '<a href="#">View all messages </a>
      writer.WriteBeginTag("a")
      writer.WriteAttribute("onclick", "window.siteHubManager.showMyNotificationsModal()")
      writer.WriteAttribute("href", "#")
      writer.Write(">")
      writer.Write("View all notifications")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li")
      writer.WriteEndTag("ul") '------------------------------------------------------------Foot

      writer.WriteEndTag("li")

      writer.WriteEndTag("ul") '-----------------------------------------------dropdown-menu messages

      writer.WriteEndTag("li") '-----------------------------------------------ButtonDropDown2


      'ButtonDropDown3------------------------------------------------------------------------
      writer.WriteBeginTag("li")
      writer.WriteAttribute("class", "button")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("class", "toggle-menu menu-right push-body")
      writer.WriteAttribute("href", "javascript:;")
      writer.Write(">")
      writer.WriteBeginTag("i")
      writer.WriteAttribute("class", "fa fa-comments")
      writer.Write(">")
      writer.WriteEndTag("i")
      writer.WriteEndTag("a")
      writer.WriteEndTag("li") '-----------------------------------------------ButtonDropDown3

      writer.WriteEndTag("ul") '-----------------------------------------------NotNav

      writer.WriteEndTag("div") '--------------------------------------------------Navbar-Collapse

      writer.WriteEndTag("div") '--------------------------------------------------Container-Fluid

      writer.WriteEndTag("div") '-------------------------------------------TopBar


      ''NavBlock---------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "cl-navblock")
      'writer.Write(">")

      ''MenuSpace--------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "menu-space")
      'writer.Write(">")

      ''MenuContent------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "content")
      'writer.Write(">")

      ''SideBarLogo------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "sidebar-logo")
      'writer.Write(">")

      ''Logo-------------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "logo")
      'writer.Write(">")
      'writer.WriteBeginTag("a")
      'writer.WriteAttribute("href", "index2.html")
      'writer.Write(">")
      'writer.WriteEndTag("a")
      'writer.WriteEndTag("div") '-----------------------------------------------------Logo

      'writer.WriteEndTag("div") '----------------------------------------------SideBarLogo

      ''SideBarUser------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "side-user")
      'writer.Write(">")

      ''SideBarUserAvatar------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "avatar")
      'writer.Write(">")

      ''SideBarUserAvatarImage-------------------------------------------------------------
      'writer.WriteBeginTag("img")
      'writer.WriteAttribute("class", "avatar")
      'If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
      '  writer.WriteAttribute("src", mSiteInfo.SitePath & "/Images/Profile/" & OBLib.Security.Settings.CurrentUserID.ToString & "_4.jpg")
      'End If
      ''userImageSmall
      'writer.WriteAttribute("alt", "Avatar")
      'writer.Write(">")
      'writer.WriteEndTag("img") '-----------------------------------SideBarUserAvatarImage

      'writer.WriteEndTag("div") '----------------------------------------SideBarUserAvatar

      ''SideBarUserInfo--------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "info")
      'writer.Write(">")
      'writer.WriteEndTag("div") '------------------------------------------SideBarUserInfo

      'writer.WriteEndTag("div") '----------------------------------------------SideBarUser

      ' ''Template------------------------------------------------------------------------
      ''writer.WriteBeginTag("div")
      ''writer.WriteAttribute("class", "content")
      ''writer.Write(">")
      ''writer.WriteEndTag("div") '----------------------------------------------Template

      ''VNavigation------------------------------------------------------------------------
      ''Dim allLinks As SiteMapDataSource = FindControl(SiteMapDatasourceID)
      'writer.WriteBeginTag("ul")
      'writer.WriteAttribute("class", "cl-vnavigation")
      'writer.Write(">")
      ''RenderMenuItems(writer, CType(allLinks, Singular.Web.CustomControls.SiteMapDataSource).GetHierarchicalView.Select, True)
      'writer.WriteEndTag("ul") '-----------------------------------------------VNavigation


      'writer.WriteEndTag("div") '----------------------------------------------MenuContent

      'writer.WriteEndTag("div") '------------------------------------------------MenuSpace

      ''CollapseButton---------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "text-right collapse-button")
      'writer.WriteAttribute("style", "padding:7px 9px;")
      'writer.Write(">")
      'writer.WriteBeginTag("input")
      'writer.WriteAttribute("class", "form-control search")
      'writer.WriteAttribute("placeholder", "Search...")
      'writer.Write(">")
      'writer.WriteEndTag("input")
      'writer.WriteBeginTag("button")
      'writer.WriteAttribute("id", "sidebar-collapse")
      'writer.WriteAttribute("class", "btn btn-default")
      'writer.WriteAttribute("style", "")
      'writer.WriteAttribute("type", "button")
      'writer.Write(">")
      'writer.WriteBeginTag("i")
      'writer.WriteAttribute("class", "fa fa-angle-left")
      'writer.WriteAttribute("style", "color:#fff;")
      'writer.Write(">")
      'writer.WriteEndTag("i")
      'writer.WriteEndTag("button")
      'writer.WriteEndTag("div") '-------------------------------------------CollapseButton

      'writer.WriteEndTag("div") '-------------------------------------------NavBlock

    End Sub

    'Private Sub RenderMenuItems(writer As System.Web.UI.HtmlTextWriter, RootNode As SiteMapNodeCollection, First As Boolean)

    '  For Each node As SiteMapNode In RootNode
    '    CreateListItem(writer, node)
    '    writer.WriteLine()
    '  Next
    '  writer.WriteLine()

    'End Sub

    'Private Sub CreateListItem(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode)

    '  'check attributes for the side it must be shown on
    '  Dim pi As System.Reflection.PropertyInfo = Node.GetType.GetProperty("Attributes", System.Reflection.BindingFlags.Instance + System.Reflection.BindingFlags.NonPublic)
    '  Dim prop As System.Collections.Specialized.NameValueCollection = Nothing
    '  If pi IsNot Nothing Then
    '    prop = pi.GetValue(Node, Nothing)
    '  End If
    '  Dim CssClass As String = prop.Item("CssClass")
    '  Dim GlyphIcon As String = prop.Item("GlyphIcon")
    '  Dim FAIcon As String = prop.Item("FAIcon")
    '  Dim NewTab As String = prop.Item("NewTab")
    '  Dim Avatar As String = prop.Item("Avatar")

    '  If NewTab Is Nothing Then
    '    NewTab = False
    '  End If

    '  writer.WriteBeginTag("li")
    '  'If a css class has been specified, add the css classes
    '  If CssClass IsNot Nothing AndAlso CssClass <> "" Then
    '    writer.WriteAttribute("class", CssClass)
    '  End If
    '  writer.Write(">")

    '  PopulateNode(writer, Node, FAIcon, NewTab)

    '  writer.WriteEndTag("li")
    '  writer.WriteLine()

    'End Sub

    'Private Sub PopulateNode(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode, Optional FAIcon As String = "", Optional NewTab As Boolean = False)

    '  'Create the opening tag for the anchor
    '  writer.WriteBeginTag("a")
    '  If Node.Url = "" Then
    '    writer.WriteAttribute("href", "#")
    '  Else
    '    writer.WriteAttribute("href", Node.Url)
    '    If NewTab Then
    '      writer.WriteAttribute("target", "_blank")
    '    End If
    '  End If
    '  writer.Write(">")

    '  'Add the icon
    '  If FAIcon <> "" Then
    '    writer.WriteBeginTag("i")
    '    writer.WriteAttribute("class", "fa " & FAIcon)
    '    writer.Write(">")
    '    writer.WriteEndTag("i")
    '  End If

    '  'Add the title
    '  writer.WriteFullBeginTag("span")
    '  writer.Write(Node.Title)
    '  writer.WriteEndTag("span")

    '  'Close the anchor tag
    '  writer.WriteEndTag("a")

    '  If Node.ChildNodes.Count > 0 Then
    '    writer.WriteBeginTag("ul")
    '    writer.WriteAttribute("class", "sub-menu")
    '    writer.Write(">")
    '    For Each childNode As SiteMapNode In Node.ChildNodes
    '      CreateListItem(writer, childNode)
    '      writer.WriteLine()
    '    Next
    '    writer.WriteLine()
    '    writer.WriteEndTag("ul")
    '  End If

    'End Sub

  End Class

End Namespace
