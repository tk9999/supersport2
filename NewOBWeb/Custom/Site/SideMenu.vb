﻿
Namespace CustomControls

  Public Class SideMenu
    Inherits System.Web.UI.WebControls.WebControl

    Public Property SiteMapDatasourceID As String
    Public Property BannerText As String

    Protected Overrides Sub Render(writer As System.Web.UI.HtmlTextWriter)

      Dim mSiteInfo As OBLib.Helpers.SiteInfo = New OBLib.Helpers.SiteInfo(Me.Page.Request)

      'SideBar----------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "cl-sidebar")
      writer.Write(">")

      'Toggler----------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "cl-toggle")
      writer.Write(">")
      writer.WriteBeginTag("i")
      writer.WriteAttribute("class", "fa fa-bars")
      writer.Write(">")
      writer.WriteEndTag("i")
      writer.WriteEndTag("div") '--------------------------------------------------Toggler

      'NavBlock---------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "cl-navblock")
      writer.Write(">")

      'MenuSpace--------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "menu-space")
      writer.Write(">")

      'MenuContent------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "content")
      writer.Write(">")

      'SideBarLogo------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "sidebar-logo")
      writer.Write(">")

      'Logo-------------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "logo")
      writer.Write(">")
      writer.WriteBeginTag("a")
      writer.WriteAttribute("href", "index2.html")
      writer.Write(">")
      writer.WriteEndTag("a")
      writer.WriteEndTag("div") '-----------------------------------------------------Logo

      writer.WriteEndTag("div") '----------------------------------------------SideBarLogo

      'SideBarUser------------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "side-user")
      writer.Write(">")

      'SideBarUserAvatar------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "avatar")
      writer.Write(">")

      'SideBarUserAvatarImage-------------------------------------------------------------
      writer.WriteBeginTag("img")
      writer.WriteAttribute("class", "avatar")
      writer.WriteAttribute("id", "userImageMedium")
      If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
        writer.WriteAttribute("src", mSiteInfo.SitePath & "/Images/Profile/" & OBLib.Security.Settings.CurrentUserID.ToString & "_4.jpg")
      End If
      'userImageSmall
      writer.WriteAttribute("alt", "Avatar")
      writer.Write(">")
      writer.WriteEndTag("img") '-----------------------------------SideBarUserAvatarImage

      writer.WriteEndTag("div") '----------------------------------------SideBarUserAvatar

      'SideBarUserInfo--------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "info")
      writer.Write(">")
      writer.WriteEndTag("div") '------------------------------------------SideBarUserInfo

      writer.WriteEndTag("div") '----------------------------------------------SideBarUser

      ''Template------------------------------------------------------------------------
      'writer.WriteBeginTag("div")
      'writer.WriteAttribute("class", "content")
      'writer.Write(">")
      'writer.WriteEndTag("div") '----------------------------------------------Template

      'VNavigation------------------------------------------------------------------------
      Dim allLinks As SiteMapDataSource = FindControl(SiteMapDatasourceID)
      writer.WriteBeginTag("ul")
      writer.WriteAttribute("class", "cl-vnavigation")
      writer.Write(">")
      RenderMenuItems(writer, CType(allLinks, Singular.Web.CustomControls.SiteMapDataSource).GetHierarchicalView.Select, True)
      writer.WriteEndTag("ul") '-----------------------------------------------VNavigation


      writer.WriteEndTag("div") '----------------------------------------------MenuContent

      writer.WriteEndTag("div") '------------------------------------------------MenuSpace

      'CollapseButton---------------------------------------------------------------------
      writer.WriteBeginTag("div")
      writer.WriteAttribute("class", "text-right collapse-button")
      writer.WriteAttribute("style", "padding:7px 9px;")
      writer.Write(">")
      writer.WriteBeginTag("input")
      writer.WriteAttribute("class", "form-control search")
      writer.WriteAttribute("placeholder", "Search...")
      writer.Write(">")
      writer.WriteEndTag("input")
      writer.WriteBeginTag("button")
      writer.WriteAttribute("id", "sidebar-collapse")
      writer.WriteAttribute("class", "btn btn-default")
      writer.WriteAttribute("style", "")
      writer.WriteAttribute("type", "button")
      writer.Write(">")
      writer.WriteBeginTag("i")
      writer.WriteAttribute("class", "fa fa-angle-left")
      writer.WriteAttribute("style", "color:#fff;")
      writer.Write(">")
      writer.WriteEndTag("i")
      writer.WriteEndTag("button")
      writer.WriteEndTag("div") '-------------------------------------------CollapseButton

      writer.WriteEndTag("div") '-------------------------------------------NavBlock

      writer.WriteEndTag("div") '-------------------------------------------SideBar

    End Sub

    Private Sub RenderMenuItems(writer As System.Web.UI.HtmlTextWriter, RootNode As SiteMapNodeCollection, First As Boolean)

      Dim mSiteInfo As OBLib.Helpers.SiteInfo = New OBLib.Helpers.SiteInfo(Me.Page.Request)

      For Each node As SiteMapNode In RootNode
        If node.Title = "Schedulers" Then

          'Parent Node
          writer.WriteBeginTag("li")
          writer.Write(">")
          writer.WriteBeginTag("a")
          writer.WriteAttribute("href", "#")
          writer.Write(">")

          'Add the icon
          writer.WriteBeginTag("i")
          writer.WriteAttribute("class", "fa fa-calendar")
          writer.Write(">")
          writer.WriteEndTag("i")

          'Add the title
          writer.WriteFullBeginTag("span")
          writer.Write(" Schedulers")
          writer.WriteEndTag("span")

          writer.WriteEndTag("a")

          writer.WriteBeginTag("ul")
          writer.WriteAttribute("class", "sub-menu")
          writer.Write(">")
          For Each usrScheduler As OBLib.Security.ReadOnly.ROUserResourceScheduler In OBLib.Security.Settings.CurrentUser.ROUserResourceSchedulerList
            'Add the list item----------------------------------
            writer.WriteFullBeginTag("li")
            writer.WriteBeginTag("a")
            writer.WriteAttribute("href", mSiteInfo.SitePath & "/Schedulers/" & usrScheduler.PageName & ".aspx")
            writer.Write(">")
            'Add the icon---------------------------------------
            writer.WriteBeginTag("i")
            writer.WriteAttribute("class", "fa fa-calendar")
            writer.Write(">")
            writer.WriteEndTag("i")
            'Add the title--------------------------------------
            writer.WriteFullBeginTag("span")
            writer.Write(" " & usrScheduler.ResourceScheduler)
            writer.WriteEndTag("span")
            'Close the anchor tag-------------------------------
            writer.WriteEndTag("a")
            'Close the list item--------------------------------
            writer.WriteEndTag("li")
            writer.WriteLine()
          Next
          writer.WriteLine()
          writer.WriteEndTag("ul")

          writer.WriteEndTag("li")
          writer.WriteLine()
        Else
          CreateListItem(writer, node)
        End If
        writer.WriteLine()
      Next
      writer.WriteLine()

    End Sub

    Private Sub CreateListItem(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode)

      'check attributes for the side it must be shown on
      Dim pi As System.Reflection.PropertyInfo = Node.GetType.GetProperty("Attributes", System.Reflection.BindingFlags.Instance + System.Reflection.BindingFlags.NonPublic)
      Dim prop As System.Collections.Specialized.NameValueCollection = Nothing
      If pi IsNot Nothing Then
        prop = pi.GetValue(Node, Nothing)
      End If
      Dim CssClass As String = prop.Item("CssClass")
      Dim GlyphIcon As String = prop.Item("GlyphIcon")
      Dim FAIcon As String = prop.Item("FAIcon")
      Dim NewTab As String = prop.Item("NewTab")
      Dim Avatar As String = prop.Item("Avatar")

      If NewTab Is Nothing Then
        NewTab = False
      End If

      writer.WriteBeginTag("li")
      'If a css class has been specified, add the css classes
      If CssClass IsNot Nothing AndAlso CssClass <> "" Then
        writer.WriteAttribute("class", CssClass)
      End If
      writer.Write(">")

      PopulateNode(writer, Node, FAIcon, NewTab)

      writer.WriteEndTag("li")
      writer.WriteLine()

    End Sub

    Private Sub PopulateNode(writer As System.Web.UI.HtmlTextWriter, Node As SiteMapNode, Optional FAIcon As String = "", Optional NewTab As Boolean = False)

      'Create the opening tag for the anchor
      writer.WriteBeginTag("a")
      If Node.Url = "" Then
        writer.WriteAttribute("href", "#")
      Else
        writer.WriteAttribute("href", Node.Url)
        If NewTab Then
          writer.WriteAttribute("target", "_blank")
        End If
      End If
      writer.Write(">")

      'Add the icon
      If FAIcon <> "" Then
        writer.WriteBeginTag("i")
        writer.WriteAttribute("class", "fa " & FAIcon)
        writer.Write(">")
        writer.WriteEndTag("i")
      End If

      'Add the title
      writer.WriteFullBeginTag("span")
      writer.Write(" " & Node.Title)
      writer.WriteEndTag("span")

      'Close the anchor tag
      writer.WriteEndTag("a")

      If Node.ChildNodes.Count > 0 Then
        writer.WriteBeginTag("ul")
        writer.WriteAttribute("class", "sub-menu")
        writer.Write(">")
        For Each childNode As SiteMapNode In Node.ChildNodes
          CreateListItem(writer, childNode)
          writer.WriteLine()
        Next
        writer.WriteLine()
        writer.WriteEndTag("ul")
      End If

    End Sub

  End Class

End Namespace
