﻿Imports Singular.Web
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.SatOps
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Costs

Namespace Controls

  Public Class EquipmentMaintenanceModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("EquipmentMaintenaceModal", "Equipment Booking", False, "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , "fa-signal", "fa-2x", True)
        .ModalDialogDiv.AddBinding(KnockoutBindingString.css, "EquipmentMaintenanceBO.modalCss(ViewModel.CurrentEquipmentMaintenance())")
        .Heading.AddBinding(KnockoutBindingString.html, "EquipmentMaintenanceBO.modalHeading(ViewModel.CurrentEquipmentMaintenance())")
        With .ContentDiv
          With .Helpers.With(Of EquipmentMaintenance)("ViewModel.CurrentEquipmentMaintenance()")
            With .Helpers.DivC("room-schedule-busy loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            '  .AddBinding(KnockoutBindingString.visible, "EquipmentMaintenanceBO.canView(ViewModel.CurrentEquipmentMaintenance(), 'IsNewLayout')")
            With .Helpers.With(Of EquipmentMaintenance)("ViewModel.CurrentEquipmentMaintenance()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FlatBlock("New Booking Details", , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.SystemID)
                          With .Helpers.Bootstrap.FormControlFor(Function(c As EquipmentMaintenance) c.SystemID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'SystemID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.ProductionAreaID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionAreaID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'ProductionAreaID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.EquipmentID)
                          With .Helpers.Bootstrap.FormControlFor(Function(c As EquipmentMaintenance) c.EquipmentID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'EquipmentID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.Title)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentMaintenance) d.Title, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'EquipmentMaintenanceTitle')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.ProductionAreaStatusID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentMaintenance) d.ProductionAreaStatusID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'ProductionAreaStatusID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.StartDateTime)
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentMaintenance) d.StartDateTime,
                                                                     BootstrapEnums.InputSize.Custom, "input-xs", "Start Date")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'StartDateTime')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentMaintenance) d.StartDateTime,
                                                                    BootstrapEnums.InputSize.Small, , "Start Time")
                                .AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'StartDateTime')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.EndDateTime)
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentMaintenance) d.EndDateTime,
                                                                     BootstrapEnums.InputSize.Custom, "input-xs", "End Date")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'EndDateTime')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentMaintenance) d.EndDateTime,
                                                                    BootstrapEnums.InputSize.Small, , "End Time")
                                .AddBinding(KnockoutBindingString.enable, "EquipmentMaintenanceBO.canEdit($data, 'EndDateTime')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentMaintenance) d.Comments)
                          With .Helpers.TextBoxFor(Function(d) d.Comments)
                            .AddClass("black")
                            .MultiLine = True
                            .Style.Height = "250px"
                            .AddClass("form-control equipment-schedule-comments")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, ,
                                                       BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                                       PostBackType.None, "EquipmentMaintenanceBO.saveModal($data)")
                          .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
