﻿Imports Singular.Web
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.SatOps
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Costs
Imports OBLib.Scheduling.Equipment

Namespace Controls

  Public Class EquipmentFeedModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    'Private mJSControlInstanceName As String

    'Public Sub New(JSControlInstanceName As String)
    '  mJSControlInstanceName = JSControlInstanceName
    'End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("EquipmentFeedModal", "Equipment Booking", False, "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , "fa-signal", "fa-2x", True)
        .ModalDialogDiv.AddBinding(KnockoutBindingString.css, "EquipmentFeedBO.modalCss(ViewModel.CurrentEquipmentFeed())")
        .Heading.AddBinding(KnockoutBindingString.html, "EquipmentFeedBO.modalHeading(ViewModel.CurrentEquipmentFeed())")
        With .ContentDiv
          With .Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
            With .Helpers.DivC("room-schedule-busy loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            .AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'IsNewLayout')")
            With .Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FlatBlock("New Booking Details", , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.SystemID)
                          With .Helpers.Bootstrap.FormControlFor(Function(c As EquipmentFeed) c.SystemID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'SystemID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.ProductionAreaID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionAreaID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'ProductionAreaID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EquipmentID)
                          With .Helpers.Bootstrap.FormControlFor(Function(c As EquipmentFeed) c.EquipmentID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EquipmentID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.FeedTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.FeedTypeID, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Attributes("placeholder") = "Feed Type"
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'FeedTypeID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EquipmentFeedTitle)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.EquipmentFeedTitle, BootstrapEnums.InputSize.Custom, "input-xs")
                            .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EquipmentFeedTitle')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.StartDateTime)
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.StartDateTime,
                                                                     BootstrapEnums.InputSize.Custom, "input-xs", "Start Date")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'StartDateTime')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentFeed) d.StartDateTime,
                                                                    BootstrapEnums.InputSize.Small, , "Start Time")
                                .AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'StartDateTime')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EndDateTime)
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.EndDateTime,
                                                                     BootstrapEnums.InputSize.Custom, "input-xs", "End Date")
                                .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EndDateTime')")
                              End With
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentFeed) d.EndDateTime,
                                                                    BootstrapEnums.InputSize.Small, , "End Time")
                                .AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EndDateTime')")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, ,
                                                       BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                                       PostBackType.None, "EquipmentFeedBO.saveModal($data)")
                          .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            .AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'IsOldLayout')")
            'LEFT COLUMN--------------------------------------
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 3)
              .Helpers.Control(New EquipmentBookingDetails(Of VMType)())
            End With
            ''RIGHT COLUMN--------------------------------------
            With .Helpers.Bootstrap.Column(12, 12, 8, 9, 9)
              With .Helpers.Toolbar
                .Helpers.MessageHolder()
              End With
              With .Helpers.Bootstrap.TabControl(, "nav-tabs", )
                With .AddTab("FeedSetup", "fa-gears", , "Feed Setup", )
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
                        With .Helpers.FieldSet("Current Audio Settings")
                          .Helpers.Control(New FeedAudioSettings(Of VMType)())
                        End With
                      End With
                    End With
                  End With
                End With
                With .AddTab("Turnaround", "fa-globe", , "Turnaround Points", , )
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New FeedTurnAroundPoints(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("IngestInstructions", "fa-cloud-download", , "Ingest Instructions", )
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New FeedIngestInstructions(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("Productions", "fa-camera", , "Productions", )
                  .TabLink.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'ProductionsTab')")
                  .TabPane.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'ProductionsTab')")
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New FeedProductions(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("Destinations", "fa-house", , "Destinations", )
                  .TabLink.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'DestinationsTab')")
                  .TabPane.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'DestinationsTab')")
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New FeedPaths(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("GeneralComments", "fa-question-circle", , "Comments", )
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
                          With .Helpers.TextBoxFor(Function(d) d.EquipmentFeedComments)
                            .AddClass("black")
                            .MultiLine = True
                            .Style.Height = "250px"
                            .AddClass("form-control equipment-schedule-comments")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .AddTab("Crew", "fa-users", "EquipmentFeedBO.refreshProductionCrew(ViewModel.CurrentEquipmentFeed())", "Crew", )
                  .TabLink.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'CrewTab')")
                  .TabPane.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'CrewTab')")
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New FeedProductionCrew(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("Vehicles", "fa-truck", "EquipmentFeedBO.refreshProductionVehicles(ViewModel.CurrentEquipmentFeed())", "Vehicles", )
                  .TabLink.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'VehiclesTab')")
                  .TabPane.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'VehiclesTab')")
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New FeedProductionVehicles(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("Costs", "fa-dollar", "EquipmentFeedBO.refreshEquipmentCosts(ViewModel.CurrentEquipmentFeed())", "Costs", )
                  .TabLink.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'CostsTab')")
                  .TabPane.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'CostsTab')")
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New EquipmentFeedCosts(Of VMType)())
                      End With
                    End With
                  End With
                End With
                With .AddTab("Incidents", "fa-exclamation-circle", "EquipmentFeedBO.refreshIncidents(ViewModel.CurrentEquipmentFeed())", "Incidents", )
                  .TabLink.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'IncidentsTab')")
                  .TabPane.AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'IncidentsTab')")
                  With .TabPane
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        .Helpers.Control(New EquipmentFeedIncidents(Of VMType)())
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class EquipmentBookingDetails(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.FlatBlock("Details", , )
        With .ContentTag
          With .Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.SatOps.EquipmentFeed) d.ChangeDescription).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.SatOps.EquipmentFeed) d.ChangeDescription, BootstrapEnums.InputSize.Small)
                    .Editor.Style.Width = "100%"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, ,
                                               BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                               PostBackType.None, "EquipmentFeedBO.saveModal($data)")
                  .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                  .Button.AddClass("btn-block")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EquipmentID)
                  With .Helpers.Bootstrap.FormControlFor(Function(c As EquipmentFeed) c.EquipmentID, BootstrapEnums.InputSize.Custom, "input-xs")
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EquipmentID')")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.FeedTypeID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.FeedTypeID, BootstrapEnums.InputSize.Custom, "input-xs")
                    .Attributes("placeholder") = "Feed Type"
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'FeedTypeID')")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EquipmentFeedTitle)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.EquipmentFeedTitle, BootstrapEnums.InputSize.Custom, "input-xs")
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EquipmentFeedTitle')")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.ProductionAreaStatusID).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.ProductionAreaStatusID, BootstrapEnums.InputSize.Small, , "Status")
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'ProductionAreaStatusID')")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.StartDateTimeBuffer)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.StartDateTimeBuffer,
                                                             BootstrapEnums.InputSize.Custom, "input-xs", "Start Date")
                        .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'StartDateTime')")
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentFeed) d.StartDateTimeBuffer,
                                                            BootstrapEnums.InputSize.Small, , "Start Time")
                        .AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'StartDateTime')")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.StartDateTime)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.StartDateTime,
                                                             BootstrapEnums.InputSize.Custom, "input-xs", "Start Date")
                        .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'StartDateTime')")
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentFeed) d.StartDateTime,
                                                            BootstrapEnums.InputSize.Small, , "Start Time")
                        .AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'StartDateTime')")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EndDateTime)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.EndDateTime,
                                                             BootstrapEnums.InputSize.Custom, "input-xs", "End Date")
                        .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EndDateTime')")
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentFeed) d.EndDateTime,
                                                            BootstrapEnums.InputSize.Small, , "End Time")
                        .AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EndDateTime')")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.EndDateTimeBuffer)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.EndDateTimeBuffer,
                                                             BootstrapEnums.InputSize.Custom, "input-xs", "End Date")
                        .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EndDateTime')")
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.TimeEditorFor(Function(d As EquipmentFeed) d.EndDateTimeBuffer,
                                                            BootstrapEnums.InputSize.Small, , "End Time")
                        .AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'EndDateTime')")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.DebtorID).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.DebtorID, BootstrapEnums.InputSize.Small, , "Client")
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'DebtorID')")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.CostCentreID)
                  With .Helpers.Bootstrap.FormControlFor(Function(c As EquipmentFeed) c.CostCentreID, BootstrapEnums.InputSize.Custom, "input-xs")
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'CostCentreID')")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.SlugName)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.SlugName, BootstrapEnums.InputSize.Custom, "input-xs")
                    .Attributes("placeholder") = "Feed Type"
                    .Editor.AddBinding(KnockoutBindingString.enable, "EquipmentFeedBO.canEdit($data, 'SlugName')")
                  End With
                End With
              End With
            End With
          End With
        End With

      End With
      'End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedProductions(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        .Helpers.Bootstrap.LabelFor(Function(d As EquipmentFeed) d.AddSynergyGenRefNo).Style.Width = "100%"
        With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
          With .Helpers.DivC("input-group-btn")
            With .Helpers.Bootstrap.StateButton(Function(d As EquipmentFeed) d.ImportAllEventsSameDayAndVenue, "Same Venue Same Day", "Same Venue Same Day", , , , , )
            End With
          End With
          With .Helpers.Bootstrap.FormControlFor(Function(d As EquipmentFeed) d.AddSynergyGenRefNo, BootstrapEnums.InputSize.Small)
          End With
          With .Helpers.DivC("input-group-btn")
            With .Helpers.Bootstrap.Button(, "Import", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.Small,
                                           , "fa-cloud-download", , PostBackType.None, "EquipmentFeedBO.importGenRefNumber($data)")
            End With
          End With
        End With
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of FeedProduction)(Function(f As EquipmentFeed) f.FeedProductionList, False, True, False, False, True, True, False, "FeedProductionList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            With .FirstRow
              With .AddReadOnlyColumn(Function(d As FeedProduction) d.SynergyGenRefNo, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedProduction) d.ProductionDescription, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedProduction) d.ProductionVenue, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedProduction) d.KickOffTimeString, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedProduction) d.IsSamrandFeed, , , , , )
                .FieldDisplay.AddBinding(KnockoutBindingString.visible, Function(d) False)
                With .Helpers.Bootstrap.StateButtonNew("IsSamrandFeed",,,,,,,)
                  .Button.AddBinding(KnockoutBindingString.enable, Function(d) False)
                End With
              End With
              With .AddColumn("")
                With .Helpers.Bootstrap.Button(, "Audio", Singular.Web.BootstrapEnums.Style.Primary, ,
                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-file-audio-o", ,
                                               Singular.Web.PostBackType.None, "EquipmentFeedBO.selectFeedProductionAudioSettings($data)")
                End With
              End With

            End With
            With .AddChildTable(Of FeedProductionAudioSetting)(Function(d As FeedProduction) d.FeedProductionAudioSettingList,
                                                               True, True, False, False, True, True, False, , "FeedProductionAudioSettingList")
              .AddClass("no-border hover list")
              .TableBodyClass = "no-border-y no-border-x no-border-x-top"
              With .FirstRow
                .AddClass("items")
                With .AddColumn(Function(d As FeedProductionAudioSetting) d.ChannelNumber)
                  .Style.Width = "100px"
                End With
                With .AddColumn(Function(d As FeedProductionAudioSetting) d.AudioSettingID)
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedPaths(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of FeedPath)(Function(f As EquipmentFeed) f.FeedPathList, False, True, False, False, True, True, False, "FeedPathList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x"
            With .FirstRow
              With .AddReadOnlyColumn(Function(d As FeedPath) d.Room, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedPath) d.ProductionTitle, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedPath) d.RoomScheduleTitle, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedPath) d.PathComments, , , , , )

              End With
              With .AddReadOnlyColumn(Function(d As FeedPath) d.CreationDetails, , , , , )

              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedTurnAroundPoints(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of FeedTurnAroundPoint)(Function(d) d.FeedTurnAroundPointList,
                                                                   True, True, False, False, True, True, False,
                                                                   "FeedTurnAroundPointList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            With .FirstRow
              .AddClass("items")
              With .AddColumn("")
                .Style.Width = "60px"
                With .Helpers.Bootstrap.Label("Source", BootstrapEnums.Style.Custom, "label-info", "fa-wifi", )
                  .LabelTag.AddBinding(KnockoutBindingString.visible, Function(d As FeedTurnAroundPoint) d.TurnAroundPointOrder = 1)
                End With
              End With
              With .AddColumn(Function(d As FeedTurnAroundPoint) d.TurnAroundPointID)
              End With
              With .AddColumn(Function(d As FeedTurnAroundPoint) d.TurnAroundPointOrder)
              End With
            End With
            With .AddChildTable(Of FeedTurnAroundPointContact)(Function(c As FeedTurnAroundPoint) c.FeedTurnAroundPointContactList, True, True, False, False, True, True, True, , "FeedTurnAroundPointContactList")
              .TableBodyClass = "no-border-y no-border-x no-border-x-top"
              .AddClass("no-border hover list")
              .Style.Width = "85%"
              .AddClass("pull-right")
              With .FirstRow
                .AddClass("items")
                With .AddColumn(Function(d As FeedTurnAroundPointContact) d.TurnAroundPointContactID)
                End With
                With .AddColumn(Function(d As FeedTurnAroundPointContact) d.ContactName)
                  .Editor.AddBinding(KnockoutBindingString.enable, "FeedTurnAroundPointContactBO.canEdit($data, 'ContactName')")
                End With
                With .AddColumn(Function(d As FeedTurnAroundPointContact) d.ContactNumber)
                  .Editor.AddBinding(KnockoutBindingString.enable, "FeedTurnAroundPointContactBO.CanEdit($data, 'ContactNumber')")
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedAudioSettings(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.Bootstrap.Row
          .AddBinding(KnockoutBindingString.visible, "EquipmentFeedBO.canView(ViewModel.CurrentEquipmentFeed(), 'ProductionsTab')")
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.Bootstrap.Button(, "Apply to All Productions", BootstrapEnums.Style.Info, ,
                                           BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                           PostBackType.None, "EquipmentFeedBO.applyAllAudioSettings($data)")
            End With
          End With
        End With
      End With

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of FeedDefaultAudioSetting)(Function(d) d.FeedDefaultAudioSettingList,
                                                                       False, False, False, False, True, True, False,
                                                                       "FeedDefaultAudioSettingList")
            .AddClass("no-border hover list table-input-xs")
            .TableBodyClass = "no-border-y no-border-x"
            With .FirstRow
              .AddClass("items")
              With .AddColumn(Function(d As FeedDefaultAudioSetting) d.ChannelNumber)
                .Style.Width = "100px"
              End With
              With .AddColumn(Function(d As FeedDefaultAudioSetting) d.AudioSettingID)
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedIngestInstructions(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of FeedIngestInstruction)(Function(d) d.FeedIngestInstructionList,
                                                                     True, True, False, False, True, True, False,
                                                                     "FeedIngestInstructionList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            With .FirstRow
              .AddClass("items")
              With .AddColumn(Function(d As FeedIngestInstruction) d.IngestInstruction)
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedProductionCrew(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("pull-left")
          With .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.DefaultStyle, ,
                                         BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                         PostBackType.None, "EquipmentFeedBO.refreshProductionCrew($data)")
          End With
        End With
      End With

      With Helpers.With(Of OBLib.SatOps.EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProduction)("$data.ROCrewDetailByProductionList()",
                                                        False, False, False, False, True, True, False, "ROCrewDetailByProductionList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x"
            With .FirstRow
              .AddClass("items")
              With .AddReadOnlyColumn(Function(d As OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProduction) d.ProductionName)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProduction) d.Discipline)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProduction) d.Position)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProduction) d.HumanResource)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProduction) d.CellPhoneNumber)
              End With
            End With
          End With
        End With
      End With

      With Helpers.With(Of OBLib.SatOps.EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("loading-custom")
          .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class FeedProductionVehicles(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OBLib.SatOps.EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("pull-left")
          With .Helpers.Bootstrap.Button(, "Refresh", BootstrapEnums.Style.DefaultStyle, ,
                                         BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                         PostBackType.None, "EquipmentFeedBO.refreshProductionVehicles($data)")
          End With
        End With
      End With

      With Helpers.With(Of OBLib.SatOps.EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of OBLib.Vehicles.ReadOnly.ROVehicleDetailByProduction)("$data.ROVehicleDetailByProductionList()",
                                                                                                   False, False, False, False, True, True, False,
                                                                                                   "ROVehicleDetailByProductionList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x"
            With .FirstRow
              .AddClass("items")
              With .AddReadOnlyColumn(Function(d As OBLib.Vehicles.ReadOnly.ROVehicleDetailByProduction) d.ProductionName)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Vehicles.ReadOnly.ROVehicleDetailByProduction) d.VehicleName)
              End With
            End With
          End With
        End With
      End With

      With Helpers.With(Of OBLib.SatOps.EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("loading-custom")
          .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class EquipmentFeedCosts(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of EquipmentFeedCost)(Function(d) d.EquipmentFeedCostList,
                                                                 True, True, False, False, True, True, False,
                                                                "EquipmentFeedCostList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            With .FirstRow
              .AddClass("items")
              With .AddColumn(Function(d As EquipmentFeedCost) d.CostTypeID)
              End With
              With .AddColumn(Function(d As EquipmentFeedCost) d.CurrencyID)
              End With
              With .AddColumn(Function(d As EquipmentFeedCost) d.Amount)
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class EquipmentFeedIncidents(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of IncidentItem)(Function(d) d.IncidentItemList,
                                                            True, True, False, False, True, True, False,
                                                            "IncidentItemList")
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            .Style.Width = "100%"
            With .FirstRow
              .AddClass("items")
              With .AddReadOnlyColumn(Function(d As IncidentItem) d.Incident)
                .AddClass("wrappable-cell")
                .Style.Width = "70%"
                .FieldDisplay.AddBinding(KnockoutBindingString.visible, Function(d) False)
                '.FieldDisplay.AddBinding(KnockoutBindingString.title, "$data.Incident()")
                With .Helpers.TextBoxFor(Function(d As IncidentItem) d.Incident)
                  .Style.Width = "100%"
                  .AddBinding(KnockoutBindingString.enable, Function(d) False)
                End With
              End With
              With .AddReadOnlyColumn(Function(d As IncidentItem) d.CreatorDetails)
                .AddClass("wrappable-cell")
                .Style.Width = "15%"
              End With
              With .AddReadOnlyColumn(Function(d As IncidentItem) d.Created)
                .AddClass("wrappable-cell")
                .Style.Width = "15%"
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  'Public Class FeedProductionAudioSettingsModal(Of VMType)
  '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

  '  Private mModalID As String
  '  
  '  Private mJSControlInstanceName As String

  '  

  '  Public Sub New(ModalID As String,
  '                 EquipmentFeedBinding As String,
  '                 JSControlInstanceName As String)
  '    mModalID = ModalID
  '    "ViewModel.CurrentEquipmentFeed()" = EquipmentFeedBinding
  '    mJSControlInstanceName = JSControlInstanceName
  '  End Sub

  '  Protected Overrides Sub Setup()
  '    MyBase.Setup()

  '    With Helpers.With(Of FeedProduction)(mJSControlInstanceName & ".currentFeedProduction()")
  '      With .Helpers.DivC("selection-panel")
  '        ' animated slideInDown fast
  '        .Attributes("id") = "feedProductionAudioSelectionPanel"
  '        With .Helpers.DivC("selection-panel-header")
  '          With .Helpers.HTMLTag("span")
  '            .AddClass("selection-panel-header-icon")
  '            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-volume-up")
  '          End With
  '          With .Helpers.HTMLTag("span")
  '            .AddClass("selection-panel-header-text")
  '            .Helpers.HTML("Manage Production Audio Settings")
  '          End With
  '          With .Helpers.HTMLTag("span")
  '            .AddClass("selection-panel-header-close")
  '            .AddBinding(KnockoutBindingString.click, mJSControlInstanceName & ".closeFeedProductionAudioSelection($data, $element)")
  '            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-close")
  '          End With
  '        End With
  '        With .Helpers.DivC("selection-panel-body")
  '          With .Helpers.DivC("table-responsive")

  '          End With
  '        End With
  '      End With
  '    End With

  '  End Sub

  '  Protected Overrides Sub Render()
  '    MyBase.Render()
  '    RenderChildren()
  '  End Sub

  'End Class

  'Public Class FeedAudioSelection(Of VMType)
  '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType) '(Of ControlInterfaces(Of VMType).IROStatusSelectControl)

  '  
  '  Private mJSControlInstanceName As String

  '  

  '  Public Sub New(EquipmentFeedBinding As String,
  '                 JSControlInstanceName As String)
  '    "ViewModel.CurrentEquipmentFeed()" = EquipmentFeedBinding
  '    mJSControlInstanceName = JSControlInstanceName
  '  End Sub

  '  Protected Overrides Sub Setup()
  '    MyBase.Setup()

  '    With Helpers.With(Of OBLib.SatOps.EquipmentFeed)("ViewModel.CurrentEquipmentFeed()")
  '      With .Helpers.DivC("selection-panel")
  '        ' animated slideInDown fast
  '        .Attributes("id") = "feedAudioSelectionPanel"
  '        With .Helpers.DivC("selection-panel-header")
  '          With .Helpers.HTMLTag("span")
  '            .AddClass("selection-panel-header-icon")
  '            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-volume-up")
  '          End With
  '          With .Helpers.HTMLTag("span")
  '            .AddClass("selection-panel-header-text")
  '            .Helpers.HTML("Selection Audio Settings")
  '          End With
  '          With .Helpers.HTMLTag("span")
  '            .AddClass("selection-panel-header-close")
  '            .AddBinding(KnockoutBindingString.click, mJSControlInstanceName & ".closeAudioSelection($data, $element)")
  '            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-close")
  '          End With
  '        End With
  '        With .Helpers.DivC("selection-panel-body")
  '          With .Helpers.DivC("table-responsive")
  '            With .Helpers.Bootstrap.TableFor(Of FeedDefaultAudioSettingSelect)(Function(d) d.FeedDefaultAudioSettingSelectList,
  '                                                                                 False, False, False, False, True, True, False,
  '                                                                                 "FeedDefaultAudioSettingSelectList")
  '              .AddClass("no-border hover list table-input-xs")
  '              .TableBodyClass = "no-border-y no-border-x"
  '              With .FirstRow
  '                .AddClass("items")
  '                With .AddColumn("")
  '                  .Helpers.Bootstrap.StateButton("IsSelected", "Selected", "Select", , , , , )
  '                End With
  '                With .AddColumn(Function(d As FeedDefaultAudioSettingSelect) d.AudioSetting)
  '                End With
  '                'With .AddColumn(Function(d As FeedDefaultAudioSetting) d.GroupNumber)
  '                'End With
  '                'With .AddColumn(Function(d As FeedDefaultAudioSetting) d.PairNumber)
  '                'End With
  '                'With .AddColumn(Function(d As FeedDefaultAudioSettingSelect) d.ChannelNumber)
  '                'End With
  '              End With
  '            End With
  '          End With
  '        End With
  '        With .Helpers.DivC("selection-panel-footer")

  '        End With
  '      End With
  '    End With

  '  End Sub

  '  Protected Overrides Sub Render()
  '    MyBase.Render()
  '    RenderChildren()
  '  End Sub

  'End Class

End Namespace
