﻿
Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.RoomScheduling
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.Scheduling.Rooms
Imports OBLib.Notifications.Sms
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.HR.ReadOnly


Namespace SmsNotifications

  Public Class SmsBatchGeneric(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SmsBatch)("ViewModel.CurrentSmsBatch()")
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.SystemID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.ProductionAreaID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.ContractTypeID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.DisciplineID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.StartDate).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.EndDate).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.LabelFor(Function(d As SmsBatch) d.Cancellations).Style.Width = "100%"
              With .Helpers.Bootstrap.TriStateButton("Cancellations", "Only Cancellations", "Only Bookings", "Bookings & Cancellations", , , , , , , "btn-sm")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
          .Helpers.LabelFor(Function(d As SmsBatch) d.BatchSendDateTime).Style.Width = "100%"
          With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.BatchSendDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.LabelFor(Function(d As SmsBatch) d.SelectRecipientsManually).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              With .Helpers.Bootstrap.StateButton(Function(d As SmsBatch) d.SelectRecipientsManually, "Yes", "Yes", , , , , "btn-sm")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
              With .Helpers.Bootstrap.StateButton(Function(d As SmsBatch) d.SelectRecipientsManually, "No", "No", "btn-default", "btn-primary", "fa-minus", "fa-check-square-o", "btn-sm")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
          End With
        End With
        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) d.SelectRecipientsManually)
        '  With .Helpers.Bootstrap.Row
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
        '      With .Helpers.Bootstrap.Button(, "Select Recipients", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-users", , Singular.Web.PostBackType.None, "SmsPage.SelectSmsBatchRecipientsManually()")
        '        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
        '        .Button.AddClass("btn-block")
        '      End With
        '    End With
        '  End With
        'End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class ScheduleProductionServices(Of VMType)
    Inherits SmsBatchGeneric(Of VMType)

  End Class

  Public Class SchedulePlayoutOperations(Of VMType)
    Inherits SmsBatchGeneric(Of VMType)

  End Class

  Public Class ScheduleICR(Of VMType)
    Inherits SmsBatchGeneric(Of VMType)

  End Class

  Public Class ScheduleOB(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SmsBatch)("ViewModel.CurrentSmsBatch()")
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
              '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  .Helpers.LabelFor(Function(d As SmsBatch) d.SystemID).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  .Helpers.LabelFor(Function(d As SmsBatch) d.ProductionAreaID).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
              '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.LabelFor(Function(d As SmsBatch) d.StartDate)
                    .Style.Width = "100%"
                    .LabelText = "Event Start Date" 'override default label (without having to use a binding)
                  End With
                  With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.LabelFor(Function(d As SmsBatch) d.EndDate)
                    .Style.Width = "100%"
                    .LabelText = "Event End Date" 'override default label (without having to use a binding)
                  End With
                  With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
              '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
              .Helpers.LabelFor(Function(d As SmsBatch) d.ProductionSystemAreaID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.ProductionSystemAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
              '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As SmsBatch) Not d.SelectRecipientsManually)
              .Helpers.LabelFor(Function(d As SmsBatch) d.BatchSendDateTime).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsBatch) d.BatchSendDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsNew()")
              End With
            End With
          End With
        End With

      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class SelectRecipientsManually(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of SmsBatch)("ViewModel.CurrentSmsBatch()")
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.FieldSet("Filter Crew")
              .AddClass("filter-crew")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                      With .Helpers.Bootstrap.FlatBlock("Areas", , , , )
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .ContentTag
                          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                            With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "SmsPage.OnSubDeptSelect($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                              .Button.AddClass("btn-block buttontext")
                            End With
                            With .Helpers.Bootstrap.Row
                              .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                              '.AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                                With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                                  .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                    .Button.AddBinding(KnockoutBindingString.click, "SmsPage.OnAreaSelect($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                    .Button.AddClass("btn-block buttontext")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                      With .Helpers.Bootstrap.FlatBlock("Contract Types")
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .ContentTag
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Row
                              With .Helpers.ForEachTemplate(Of ROContractType)("ViewModel.SelectableContractTypes()")
                                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , "", )
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.OnContractTypeSelect($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                                    .Button.AddClass("btn-block")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FlatBlock("Disciplines")
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .ContentTag
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "SmsPage.SelectAllDisciplines()")
                            End With
                            With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-eraser", , Singular.Web.PostBackType.None, "SmsPage.ClearDisciplines()")
                            End With
                          End With
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .AddClass("max-height-400")
                            With .Helpers.Bootstrap.Row
                              With .Helpers.ForEachTemplate(Of RODisciplineSelect)("ViewModel.SelectableDisciplines()")
                                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                  With .Helpers.Bootstrap.StateDiv(Function(c) c.IsSelected(), "", "", , , , "", )
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.OnDisciplineSelect($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                                    .Button.AddClass("btn-block")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FlatBlock("Human Resources", True)
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .AboveContentTag
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                             Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                             "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "SmsPage.SelectAllHumanResources()")
                                .Button.Style.Width = "100px"
                              End With
                              With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                         Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                         "fa-eraser", , Singular.Web.PostBackType.None, "SmsPage.ClearRecipients()")
                                .Button.Style.Width = "100px"
                              End With
                              With .Helpers.Bootstrap.Button(, "Add Selected To SMS", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                             Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                             "fa-plus-circle", , Singular.Web.PostBackType.None, "SmsPage.addSelectedToSMS()")
                                .Button.Style.Width = "200px"
                              End With
                            End With
                          End With
                        End With
                        With .ContentTag
                          With .Helpers.ForEachTemplate(Of ROHumanResourceContact)("ViewModel.SelectableHumanResources()")
                            .AddClass("row")
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Div
                                .Style.Width = "100%"
                                .Style.TextAlign = Singular.Web.TextAlign.left
                                .AddBinding(Singular.Web.KnockoutBindingString.enable, "SmsPage.canSelectContact($data)")
                                .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.onContactClicked($data)")
                                .AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.ROHumanResourceFindCSS($data)")
                                .AddBinding(KnockoutBindingString.html, "SmsPage.ROHumanResourceFindHTML($data)")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class SelectRecipients(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of SmsBatch)("ViewModel.CurrentSms()")
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.FieldSet("Select Recipients")
              .AddClass("filter-crew")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                      With .Helpers.Bootstrap.FlatBlock("Areas", , , , )
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .ContentTag
                          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "SmsPage.OnSubDeptSelect($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                              .Button.AddClass("btn-block buttontext")
                            End With
                            With .Helpers.Bootstrap.Row
                              .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                              '.AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                                With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                                  .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                    .Button.AddBinding(KnockoutBindingString.click, "SmsPage.OnAreaSelect($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                    .Button.AddClass("btn-block buttontext")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                      With .Helpers.Bootstrap.FlatBlock("Contract Types")
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .ContentTag
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Row
                              With .Helpers.ForEachTemplate(Of ROContractType)("ViewModel.SelectableContractTypes()")
                                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.OnContractTypeSelect($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                                    .Button.AddClass("btn-block")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FlatBlock("Disciplines")
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .ContentTag
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "SmsPage.SelectAllDisciplines()")
                            End With
                            With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                           Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-eraser", , Singular.Web.PostBackType.None, "SmsPage.ClearDisciplines()")
                            End With
                          End With
                          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                            .AddClass("max-height-400")
                            With .Helpers.Bootstrap.Row
                              With .Helpers.ForEachTemplate(Of RODisciplineSelect)("ViewModel.SelectableDisciplines()")
                                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.OnDisciplineSelect($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                                    .Button.AddClass("btn-block")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FlatBlock("Human Resources", True)
                        .FlatBlockTag.AddClass("block-flat-small")
                        With .AboveContentTag
                          With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                             Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                             "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "SmsPage.SelectAllHumanResources()")
                                .Button.Style.Width = "100px"
                              End With
                              With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                         Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                         "fa-eraser", , Singular.Web.PostBackType.None, "SmsPage.ClearRecipients()")
                                .Button.Style.Width = "100px"
                              End With
                              With .Helpers.Bootstrap.Button(, "Add Selected To SMS", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                             Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                             "fa-plus-circle", , Singular.Web.PostBackType.None, "SmsPage.AddSelectedToSMS()")
                                .Button.Style.Width = "200px"
                              End With
                            End With
                          End With
                        End With
                        With .ContentTag
                          With .Helpers.ForEachTemplate(Of ROHumanResourceContact)("ViewModel.SelectableHumanResources()")
                            .AddClass("row")
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Div
                                .Style.Width = "100%"
                                .Style.TextAlign = Singular.Web.TextAlign.left
                                .AddBinding(Singular.Web.KnockoutBindingString.enable, "SmsPage.canSelectContact($data)")
                                .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.onContactClicked($data)")
                                .AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.ROHumanResourceFindCSS($data)")
                                .AddBinding(KnockoutBindingString.html, "SmsPage.ROHumanResourceFindHTML($data)")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

  Public Class SelectRecipientsModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("SelectRecipients", "Select Recipients", , "modal-lg", BootstrapEnums.Style.Primary, , "fa-users", , )
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                  With .Helpers.Bootstrap.FlatBlock("Areas", , , , )
                    .FlatBlockTag.AddClass("block-flat-small")
                    With .ContentTag
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "SmsPage.OnSubDeptSelect($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                          .Button.AddClass("btn-block buttontext")
                        End With
                        With .Helpers.Bootstrap.Row
                          .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                          '.AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                            With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                              .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                .Button.AddBinding(KnockoutBindingString.click, "SmsPage.OnAreaSelect($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                .Button.AddClass("btn-block buttontext")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                  With .Helpers.Bootstrap.FlatBlock("Contract Types")
                    .FlatBlockTag.AddClass("block-flat-small")
                    With .ContentTag
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Row
                          With .Helpers.ForEachTemplate(Of ROContractType)("ViewModel.SelectableContractTypes()")
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.OnContractTypeSelect($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                                .Button.AddClass("btn-block")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Disciplines")
                    .FlatBlockTag.AddClass("block-flat-small")
                    With .ContentTag
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "SmsPage.SelectAllDisciplines()")
                        End With
                        With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-eraser", , Singular.Web.PostBackType.None, "SmsPage.ClearDisciplines()")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .AddClass("max-height-400")
                        With .Helpers.Bootstrap.Row
                          With .Helpers.ForEachTemplate(Of RODisciplineSelect)("ViewModel.SelectableDisciplines()")
                            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 6)
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , "", )
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.OnDisciplineSelect($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                                .Button.AddClass("btn-block")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Human Resources", True)
                    .FlatBlockTag.AddClass("block-flat-small")
                    With .AboveContentTag
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.Button(, "Select All", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-sort-amount-asc", , Singular.Web.PostBackType.None, "SmsPage.SelectAllHumanResources()")
                            .Button.Style.Width = "100px"
                          End With
                          With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, ,
                                     Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                     "fa-eraser", , Singular.Web.PostBackType.None, "SmsPage.ClearRecipients()")
                            .Button.Style.Width = "100px"
                          End With
                          With .Helpers.Bootstrap.Button(, "Add Selected To SMS", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                         "fa-plus-circle", , Singular.Web.PostBackType.None, "SmsPage.addSelectedToSMS()")
                            .Button.Style.Width = "200px"
                          End With
                        End With
                      End With
                    End With
                    With .ContentTag
                      With .Helpers.ForEachTemplate(Of ROHumanResourceContact)("ViewModel.SelectableHumanResources()")
                        .AddClass("row")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Div
                            .Style.Width = "100%"
                            .Style.TextAlign = Singular.Web.TextAlign.left
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "SmsPage.canSelectContact($data)")
                            .AddBinding(Singular.Web.KnockoutBindingString.click, "SmsPage.onContactClicked($data)")
                            .AddBinding(Singular.Web.KnockoutBindingString.css, "SmsPage.ROHumanResourceFindCSS($data)")
                            .AddBinding(KnockoutBindingString.html, "SmsPage.ROHumanResourceFindHTML($data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With


      'With Helpers.With(Of SmsBatch)("ViewModel.CurrentSms()")
      '  With .Helpers.Bootstrap.Row
      '    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
      '      .AddClass("filter-crew")


      '    End With
      '  End With
      'End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace


