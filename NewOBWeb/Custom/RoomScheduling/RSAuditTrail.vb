﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSAuditTrail(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of OBLib.Audit.ROObjectAuditTrail)("$data.ROObjectAuditTrailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailList")
            .AllowClientSideSorting = True
            With .FirstRow
              .AddClass("items selectable")
              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.Section)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeDateTime)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.ChangeTypeDescription)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.LoginName)
              End With
              With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrail) d.MachineName)
              End With
            End With
            With .AddChildTable(Of OBLib.Audit.ROObjectAuditTrailDetail)("$data.ROObjectAuditTrailDetailList()", False, False, False, True, True, True, True, "ROObjectAuditTrailDetailList")
              .AllowClientSideSorting = True
              .Style.Width = "90%"
              .Style.FloatRight()
              With .FirstRow
                .AddClass("items selectable")
                With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.ChangeDescription)
                End With
                With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.PreviousValueDescription)
                End With
                With .AddReadOnlyColumn(Function(d As OBLib.Audit.ROObjectAuditTrailDetail) d.NewValueDescription)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace