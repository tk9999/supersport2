﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSEditBookingPanel(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
        With .Helpers.FieldSet("Event Details")
          With .Helpers.If("$data.RoomScheduleTypeID() == 1")
            .Helpers.Control(New RSProduction(Of VMType)())
          End With
          With .Helpers.If("$data.RoomScheduleTypeID() == 4")
            .Helpers.Control(New RSAdHocBooking(Of VMType)())
          End With
        End With
        With Helpers.DivC("loading-custom")
          .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-5x")
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace