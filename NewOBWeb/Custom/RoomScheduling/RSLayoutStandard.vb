﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSLayoutStandard(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'Booking Details------------------------------------------------------------------------------------
      With Helpers.Bootstrap.Column(12, 12, 5, 4, 3)
        With .Helpers.Bootstrap.FlatBlock("Booking Details")
          .FlatBlockTag.AddClass("block-flat-small")
          With .ContentTag
            .Helpers.Control(New RSLeftPanel(Of VMType)())
          End With
        End With
      End With
      '------------------------------------------------------------------------------------Booking Details

      'Messages-------------------------------------------------------------------------------------------
      With Helpers.Bootstrap.Column(12, 12, 7, 8, 9)
        .Attributes("id") = "RoomScheduleMessages"
        'With .Helpers.Toolbar
        '  .Helpers.MessageHolder()
        'End With
        With .Helpers.DivC("error-window-column")
          .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesBootstrap($data, $element)")
        End With
      End With
      '-------------------------------------------------------------------------------------------Messages

      'Area-----------------------------------------------------------------------------------------------
      With Helpers.Bootstrap.Column(12, 12, 7, 8, 9)
        With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", , BootstrapEnums.TabAlignment.Top)
            With .AddTab("BookingTimes", "fa-clock-o", "", "Booking Info", , False)
              With .TabPane
                With .Helpers.Bootstrap.Row
                  .Helpers.Control(New RSDetails(Of VMType)())
                End With
              End With
            End With
            With .AddTab("EventInfo", "fa-info-circle", "RoomScheduleAreaBO.refreshAndEditEvent($data)", "Event Info", , False)
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    .Helpers.Control(New RSEditBookingPanel(Of VMType)())
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    With .Helpers.FieldSet("Other Areas on event")

                    End With
                  End With
                End With
              End With
            End With
            With .AddTab("Crew", "fa-users", "", "Crew", , False)
              .TabLink.AddBinding(KnockoutBindingString.visible, "!$data.IsNew()")
              With .TabPane
                .Helpers.Control(New RSArea(Of VMType)())
              End With
            End With
            If OBLib.Security.Settings.CurrentUserID = 1 Or Singular.Debug.InDebugMode Then
              With .AddTab("Bookings", "fa-users", "", "Bookings", , False)
                .TabLink.AddBinding(KnockoutBindingString.visible, "!$data.IsNew()")
                With .TabPane
                  .Helpers.Control(New RSHRBookings(Of VMType)())
                End With
              End With
            End If
            With .AddTab("Channels", "fa-television", "", "Channels", , False)
              .TabLink.AddBinding(KnockoutBindingString.visible, "!$data.IsNew()")
              With .TabPane
                .Helpers.Control(New RSChannels(Of VMType)())
              End With
            End With
            With .AddTab("History", "fa-user-secret", "RoomScheduleAreaBO.refreshAudit($data)", "History", , False)
              .TabLink.AddBinding(KnockoutBindingString.visible, "!$data.IsNew()")
              With .TabPane
                .Helpers.Control(New RSAuditTrail(Of VMType)())
              End With
            End With
          End With
        End With
      End With
      '-----------------------------------------------------------------------------------------------Area
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace