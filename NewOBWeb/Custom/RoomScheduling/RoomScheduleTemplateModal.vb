﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers

Namespace Controls

  Public Class RoomScheduleTemplateModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("RoomScheduleTemplateModal", "Room Booking", , "modal-xs", Singular.Web.BootstrapEnums.Style.Primary, , "fa-home", , )
        '.Heading.AddBinding(Singular.Web.KnockoutBindingString.html, "ViewModel.CurrentRoomScheduleTemplate().Title()")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Booking Info", , , , )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of OBLib.Scheduling.Rooms.RoomScheduleTemplate)("ViewModel.CurrentRoomScheduleTemplate()")
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.SystemID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small, , "Sub-Dept")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.ProductionAreaID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small, , "Area")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.Title).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.Title, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.RoomID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.RoomID, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.CallTime)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OnAirTimeStart)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OnAirTimeStart, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OnAirTimeStart, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OnAirTimeEnd)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OnAirTimeEnd, Singular.Web.BootstrapEnums.InputSize.Small, , "End Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OnAirTimeEnd, Singular.Web.BootstrapEnums.InputSize.Small, , "End Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.WrapTime)
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Wrap Date")
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Wrap Time")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.DebtorID).Style.Width = "100%"
                            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.DebtorID, Singular.Web.BootstrapEnums.InputSize.Small, , "Debtor")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OwnerComments).Style.Width = "100%"
                            With .Helpers.Div
                              With .Helpers.TextBoxFor(Function(d As OBLib.Scheduling.Rooms.RoomScheduleTemplate) d.OwnerComments)
                                .MultiLine = True
                                .Style.Height = "240px"
                                .AddClass("form-control room-schedule-comments")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Scheduling.Rooms.RoomScheduleTemplate)("ViewModel.CurrentRoomScheduleTemplate()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Create Booking", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                             "fa-cloud-upload", , Singular.Web.PostBackType.None, "RoomScheduleTemplateBO.saveFromResourceScheduler(ViewModel.CurrentRoomScheduleTemplate)", False)
                .Button.AddClass("btn-block")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace