﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSLeftPanel(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Label("Pending Delete", BootstrapEnums.Style.Danger, , "fa-trash", , )
            .LabelTag.AddBinding(KnockoutBindingString.title, "RoomScheduleAreaBO.pendingDeleteTitleText($data)")
            .LabelTag.AddBinding(KnockoutBindingString.visible, "$data.PendingDelete()")
          End With
          With .Helpers.Bootstrap.Label("Parent Mismatch", BootstrapEnums.Style.Warning, , "fa-exclamation-triangle", , )
            .LabelTag.AddBinding(KnockoutBindingString.title, "RoomScheduleAreaBO.parentMismatchTitleText($data)")
            .LabelTag.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.doesNotMatchParent($data)")
          End With
        End With
        With .Helpers.DivC("differences-from-owner")
          .AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'DifferencesFromOwner')")
          .AddBinding(KnockoutBindingString.html, "RoomScheduleAreaBO.differencesFromOwnerHTML($data)")
        End With
        With .Helpers.If("$data.IsOwner()")
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d) d.Title)
            With .Helpers.Bootstrap.ReadOnlyComboFor(Function(p As RoomScheduleArea) p.Title, "RoomScheduleAreaBO.editEvent($data)", "Event...",
                                                     , BootstrapEnums.Style.DefaultStyle, "fa-edit", True, , ,
                                                     BootstrapEnums.InputGroupSize.Small, BootstrapEnums.InputSize.Small, False)
              .AddOnButton.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'TitleButton')")
              .AddOnButton.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'TitleButton')")
            End With
          End With
        End With
        With .Helpers.If("!$data.IsOwner()")
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d) d.Title)
            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As RoomScheduleArea) p.Title, BootstrapEnums.InputSize.Small, , "Event Name...")
            End With
          End With
        End With
        '---Booking Times
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .Helpers.Bootstrap.LabelFor(Function(e As RoomScheduleArea) e.BookingTimes)
          With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
            With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.BookingTimes, Singular.Web.BootstrapEnums.InputSize.Small, , "Booking Times")
              .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
            End With
            With .Helpers.Bootstrap.InputGroupAddOnButton(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                          Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-edit", ,
                                                          Singular.Web.PostBackType.None, "RoomScheduleAreaBO.editTimes($data)")
            End With
          End With
        End With
        '---Status
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'ProductionAreaStatusID')")
          .Helpers.Bootstrap.LabelFor(Function(p As RoomScheduleArea) p.ProductionAreaStatusID).Style.Width = "100%"
          With .Helpers.Bootstrap.FormControlFor(Function(p As RoomScheduleArea) p.ProductionAreaStatusID, BootstrapEnums.InputSize.Small, , "Status")
            .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'ProductionAreaStatusID')")
          End With
        End With
        '---Status
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          .AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'UnReconcileButton')")
          .Helpers.Bootstrap.LabelDisplay("UnReconcile").Style.Width = "100%"
          With .Helpers.Bootstrap.Button(, "UnReconcile", BootstrapEnums.Style.Warning, ,
                                         BootstrapEnums.ButtonSize.Small, , "fa-eraser", , , "RoomScheduleAreaBO.unreconcile($data)", )
            .Button.AddClass("btn-block")
          End With
        End With
        '--Debtor
        With .Helpers.If("$data.IsOwner()")
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.DebtorID)
            With .Helpers.Bootstrap.FormControlFor(Function(p As RoomScheduleArea) p.DebtorID, BootstrapEnums.InputSize.Small, , "Client")
              .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'DebtorID')")
            End With
          End With
        End With
        '---Comments
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Div
            .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.AreaComments)
          End With
          With .Helpers.If("$data.IsOwner()")
            With .Helpers.Div
              With .Helpers.TextBoxFor(Function(d As RoomScheduleArea) d.OwnerComments)
                .MultiLine = True
                .Style.Height = "240px"
                .AddClass("form-control room-schedule-comments")
                .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'OwnerComments')")
              End With
            End With
          End With
          With .Helpers.If("!$data.IsOwner()")
            With .Helpers.Bootstrap.TabControl(, "nav-tabs", )
              .TabContentContainer.AddClass("tab-content-no-padding tab-content-no-margin-bottom")
              With .AddTab("OwnerComments", "fa-wechat", , "Owner", , )
                With .TabPane
                  With .Helpers.TextBoxFor(Function(d As RoomScheduleArea) d.OwnerComments)
                    .AddBinding(KnockoutBindingString.enable, "$data.IsOwner()")
                    .MultiLine = True
                    .Style.Height = "240px"
                    .AddClass("form-control room-schedule-comments")
                  End With
                End With
              End With
              With .AddTab("MyAreaComments", " fa-comment-o", , "My Area", , )
                With .TabPane
                  With .Helpers.TextBoxFor(Function(d As RoomScheduleArea) d.AreaComments)
                    .MultiLine = True
                    .Style.Height = "240px"
                    .AddClass("form-control room-schedule-comments")
                    .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'AreaComments')")
                  End With
                End With
              End With
            End With
          End With
        End With
        '---Channels
        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Div
            .Helpers.Bootstrap.LabelDisplay("Channels")
          End With
          With .Helpers.ForEach(Of RoomScheduleChannel)("$data.RoomScheduleChannelList()")
            With .Helpers.SpanC("label label-primary")
              .AddBinding(KnockoutBindingString.html, "$data.ChannelShortName()")
              .AddBinding(KnockoutBindingString.visible, "RoomScheduleChannelBO.canDisplayInBookingDetails($data, $parent)")
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace