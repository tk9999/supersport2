﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers

Namespace Controls

  Public Class ApplyResultsModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mSchedulerName As String = "window.Scheduler"

    Public Sub New(SchedulerName As String)
      mSchedulerName = SchedulerName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("ApplyResultsModal", "Apply Results", False, "modal-sm", BootstrapEnums.Style.Primary, , , )
        '.Heading.AddBinding(KnockoutBindingString.html, mSchedulerName & ".getResourceBookingCleanDescription()")
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.ForEach(Of ApplyResult)("ViewModel.ApplyResultList()")
              With .Helpers.DivC("alert")
                .Attributes("role") = "alert"
                .AddBinding(KnockoutBindingString.css, mSchedulerName & ".applyResultCss($data)")
                With .Helpers.HTMLTag("span")
                  .AddClass("icon")
                  With .Helpers.HTMLTag("i")
                    .AddBinding(KnockoutBindingString.css, mSchedulerName & ".applyResultIconCss($data)")
                  End With
                End With
                With .Helpers.DivC("alert-header")
                  '.AddClass("alert-header")
                  With .Helpers.HTMLTag("strong")
                    .AddBinding(KnockoutBindingString.html, mSchedulerName & ".applyResultStrongText($data)")
                  End With
                End With
                With .Helpers.DivC("alert-message")
                  '.AddClass("alert-message")
                  .AddBinding(KnockoutBindingString.html, mSchedulerName & ".applyResultMessage($data)")
                End With
                With .Helpers.DivC("apply-result-options-requirements")
                  With .Helpers.DivC("")
                    .AddBinding(KnockoutBindingString.visible, "ApplyResultBO.isProductionHRResourceBookingOption($data)")
                    With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                      With .Helpers.HTMLTag("span")
                        .AddClass("input-group-btn")
                        .AddBinding(KnockoutBindingString.visible, "ApplyResultBO.areOptionsVisible($data)")
                        With .Helpers.Bootstrap.StateButton(Function(d As ApplyResult) d.AddAnyway, "Add anyway", "Add anyway?", , , , "fa-minus", )
                        End With
                      End With
                      With .Helpers.Bootstrap.FormControlFor(Function(d As ApplyResult) d.AddAnywayReason,
                                                             BootstrapEnums.InputSize.Small, ,
                                                             "Reason...")
                        .Editor.AddBinding(KnockoutBindingString.visible, "ApplyResultBO.areOptionRequirementsVisible($data)")
                      End With
                      With .Helpers.Bootstrap.InputGroupAddOnButton(, "Add", BootstrapEnums.Style.Primary, ,
                                                                    BootstrapEnums.ButtonSize.Small, , "fa-thumbs-o-up", ,
                                                                    PostBackType.None, "ApplyResultBO.addProductionHRResourceBooking($data)")
                        .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
                        .AddOn.AddBinding(KnockoutBindingString.visible, "ApplyResultBO.areOptionRequirementsVisible($data)")
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("apply-alert-busy loading-custom")
                  .AddBinding(KnockoutBindingString.css, "ApplyResultBO.isProcessingCss($data)")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin")
                End With
              End With
            End With
          End With
        End With
        With .Footer
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace