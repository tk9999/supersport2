﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class EditProductionHumanResourceControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("PersonnelManagerModal", "Personnel Manager", False, "modal-sm", Singular.Web.BootstrapEnums.Style.Custom, "modal-purple", "fa-street-view", "fa-2x", True)
        With .ContentDiv
          With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
            With .Helpers.DivC("room-schedule-busy loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.Bootstrap.FlatBlock("Booking Details")
                  With .ContentTag
                    'ProductionHumanResource----------------------------------------------------------------------------------------------------------------
                    With .Helpers.With(Of RoomScheduleAreaProductionHumanResource)("RoomScheduleAreaBO.currentProductionHumanResource($data, $element)")
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHumanResource) d.DisciplineID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHumanResource) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          '.Editor.AddBinding(KnockoutBindingString.enable, RoomScheduleAreaProductionHumanResourceBO & ".CanEdit($data, 'DisciplinePosition')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHumanResource) d.PositionTypeID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHumanResource) d.PositionTypeID, BootstrapEnums.InputSize.Small, , "Position Type")
                          '.Editor.AddBinding(KnockoutBindingString.enable, RoomScheduleAreaProductionHumanResourceBO & ".CanEdit($data, 'Position')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHumanResource) d.PositionID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHumanResource) d.PositionID, BootstrapEnums.InputSize.Small, , "Position")
                          '.Editor.AddBinding(KnockoutBindingString.enable, RoomScheduleAreaProductionHumanResourceBO & ".CanEdit($data, 'Position')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHumanResource) d.HumanResource)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHumanResource) d.HumanResourceID, BootstrapEnums.InputSize.Small, , "Human Resource")
                          '.Editor.AddBinding(KnockoutBindingString.enable, RoomScheduleAreaProductionHumanResourceBO & ".CanEdit($data, 'HumanResource')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.TBCInd, "TBC", "TBC", "btn-warning", , , "fa-minus", "btn-sm")
                        End With
                        With .Helpers.Bootstrap.StateButton(Function(c) c.TrainingInd, "Training", "Training", "btn-info", , , "fa-minus", "btn-sm")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(v As RoomScheduleAreaProductionHumanResource) v.Comments)
                        With .Helpers.TextBoxFor(Function(v As RoomScheduleAreaProductionHumanResource) v.Comments)
                          .MultiLine = True
                          .Style.Height = "80px"
                          .AddClass("form-control")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                With .Helpers.With(Of RoomScheduleAreaProductionHRBooking)("RoomScheduleAreaBO.currentProductionHR($data)")
                  With .Helpers.Bootstrap.FlatBlock("Booking Times")
                    With .ContentTag
                      With .Helpers.Bootstrap.Row
                        'ProductionHR----------------------------------------------------------------------------------------------------------------
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTimeBuffer).Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTimeBuffer, BootstrapEnums.InputSize.Small, , )
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTimeBuffer, BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTime).Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTime, BootstrapEnums.InputSize.Small, , )
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTime, BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTime).Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTime, BootstrapEnums.InputSize.Small, , )
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTime, BootstrapEnums.InputSize.Small, , )
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTimeBuffer).Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                            .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTimeBuffer, BootstrapEnums.InputSize.Small, , )
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                            .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTimeBuffer, BootstrapEnums.InputSize.Small, , )
                          End With
                          '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.GetParent().GetParent().IsProcessing()")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.With(Of RoomScheduleAreaProductionHRBooking)("RoomScheduleAreaBO.currentProductionHR($data)")
                  With .Helpers.Bootstrap.FlatBlock("Clashes", False, False, False)
                    With .ContentTag
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.ClashDetailList().length == 0")
                        With .Helpers.DivC("text-center")
                          With .Helpers.DivC("i-circle success")
                            .Helpers.Bootstrap.FontAwesomeIcon("fa-check")
                          End With
                          .Helpers.HTML.Heading4("Fantastic!")
                          With .Helpers.HTMLTag("p")
                            .Helpers.HTML("There are no clashes!")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.ClashDetailList().length > 0")
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.Helpers.ResourceHelpers.ClashDetail)("$data.ClashDetailList()", False, False, True, False, True, True, True, "ClashDetailList")
                            .AllowClientSideSorting = True
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            With .FirstRow
                              .AddClass("items selectable")
                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.ResourceBookingDescription)
                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.StartDateTimeDisplay)
                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.ResourceHelpers.ClashDetail) d.EndDateTimeDisplay)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .AddBinding(KnockoutBindingString.visible, "false")
                With .Helpers.Bootstrap.FlatBlock("Personnel Manager")
                  With .ContentTag
                    With .Helpers.Div
                      .Attributes("id") = "ROPersonnelManager"
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace