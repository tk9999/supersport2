﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RoomScheduleControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IRoomScheduleAreaControl)
    '(Of RoomBookingType)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("CurrentRoomScheduleModal", "Room Booking", False, "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-fort-awesome", "fa-2x")
        .Heading.AddBinding(KnockoutBindingString.html, "RoomScheduleAreaBO.currentTitle()")
        With .ContentDiv
          With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
            With .Helpers.DivC("cancellation-modal")
              With .Helpers.DivC("cancellation-modal-content")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    .AddClass("col-lg-offset-3 col-xl-offset-3")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.CancellationDate)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.CancellationDate, BootstrapEnums.InputSize.Small,, "Cancellation Date")
                        .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'CancelControl')")
                        .AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'CancelControl')")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    .AddClass("col-lg-offset-3 col-xl-offset-3")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.CancellationReason)
                      With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.CancellationReason, BootstrapEnums.InputSize.Small,, "Cancellation Reason")
                        .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'CancelControl')")
                        .AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'CancelControl')")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    .AddClass("col-lg-offset-3 col-xl-offset-3")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.PullLeft
                        With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small,, "fa-thumbs-down",,, "RoomScheduleAreaBO.cancelCancellation($data)",)
                          .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'CancelControl')")
                          .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'CancelControl')")
                        End With
                      End With
                      With .Helpers.Bootstrap.PullRight
                        With .Helpers.Bootstrap.Button(, "Proceed", BootstrapEnums.Style.Warning, , BootstrapEnums.ButtonSize.Small,, "fa-thumbs-up",,, "RoomScheduleAreaBO.commitCancellation($data)",)
                          .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.canProceedWithCancelCrew($data)")
                          .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'CancelControl')")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.if, "$data.IsNew()")
              .Helpers.Control(New RSLayoutNew(Of VMType)())
            End With
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.if, "!$data.IsNew()")
              .Helpers.Control(New RSLayoutStandard(Of VMType)())
            End With
          End With
        End With
        With .Footer
          .AddClass("modal-footer-small")
          With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
            With .Helpers.DivC("pull-left")

            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success,
                                             , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o",
                                             , PostBackType.None, "RoomScheduleAreaBO.saveModal($data)")
                .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.canSave($data)")
                .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'SaveButton')")
              End With
            End With
          End With
        End With
      End With

      Helpers.Control(New EditProductionHumanResourceControl(Of VMType)())

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace