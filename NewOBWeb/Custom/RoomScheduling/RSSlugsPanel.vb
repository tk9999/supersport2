﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.RoomScheduling
Imports OBLib.Slugs

Namespace Controls

  Public Class RSSlugsPanel(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Private mRoomScheduleControlName As String = "CurrentRoomSchedule"

    Public Sub New(RoomScheduleControlName As String)
      mRoomScheduleControlName = RoomScheduleControlName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'With Helpers.With(Of RoomSchedule)(mRoomScheduleControlName & ".roomSchedule()")
      '  With .Helpers.Bootstrap.FlatBlock("Slugs", , , , )
      '    .FlatBlockTag.AddClass("block-flat-small")
      '    With .ContentTag
      '      With .Helpers.DivC("table-responsive")
      '        With .Helpers.Bootstrap.TableFor(Of SlugItem)("$data.SlugItemList()", False, False, False, False, True, True, True, "SlugItemList")
      '          .AllowClientSideSorting = True
      '          .AddClass("no-border hover list")
      '          .TableBodyClass = "no-border-y no-border-x"
      '          With .FirstRow
      '            .AddClass("items selectable")
      '            With .AddColumn(Function(d As SlugItem) d.SlugDuration, 70)
      '            End With
      '            With .AddReadOnlyColumn(Function(d As SlugItem) d.SlugName)
      '            End With
      '            'With .AddReadOnlyColumn(Function(d As SlugItem) d.TypeCode)
      '            'End With
      '            With .AddReadOnlyColumn(Function(d As SlugItem) d.SlugType)
      '            End With
      '            With .AddColumn(Function(d As SlugItem) d.ProductionHumanResourceID, 140)
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      '  'With .Helpers.Bootstrap.FlatBlock("Slug Editors", , , , )
      '  '  .FlatBlockTag.AddClass("block-flat-small")
      '  '  With .ContentTag
      '  '    With .Helpers.DivC("table-responsive")
      '  '      With .Helpers.Bootstrap.TableFor(Of SlugEditor)("$data.SlugEditorList()", False, False, False, False, True, True, True, "SlugEditorList")
      '  '        .AllowClientSideSorting = True
      '  '        .AddClass("no-border hover list")
      '  '        .TableBodyClass = "no-border-y no-border-x"
      '  '        With .FirstRow
      '  '          .AddClass("items selectable")
      '  '          'With .AddColumn(Function(d As SlugEditor) d.SlugDuration, 70)
      '  '          'End With
      '  '          With .AddReadOnlyColumn(Function(d As SlugEditor) d.SlugName)
      '  '          End With
      '  '          'With .AddReadOnlyColumn(Function(d As SlugItem) d.TypeCode)
      '  '          'End With
      '  '          With .AddReadOnlyColumn(Function(d As SlugEditor) d.HumanResourceName)
      '  '          End With
      '  '        End With
      '  '      End With
      '  '    End With
      '  '  End With
      '  'End With
      'End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace