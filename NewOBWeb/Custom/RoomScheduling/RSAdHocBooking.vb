﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling

Namespace Controls

  Public Class RSAdHocBooking(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of AdHocBooking)("ViewModel.CurrentAdHocBooking()")

        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.SystemID).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.ProductionAreaID).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small)
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.AdHocBookingTypeID).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.AdHocBookingTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.Title).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.Title, Singular.Web.BootstrapEnums.InputSize.Small)
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          'Live Start Times---------------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.StartDateTimeBuffer).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.AdHoc.AdHocBooking) d.StartDateTimeBuffer, BootstrapEnums.InputSize.Small, , "Call Date")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.AdHoc.AdHocBooking) d.StartDateTimeBuffer, BootstrapEnums.InputSize.Small, , "Call Time")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          'Live Start Times---------------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.StartDateTime).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.AdHoc.AdHocBooking) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Start Date")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.AdHoc.AdHocBooking) d.StartDateTime, BootstrapEnums.InputSize.Small, , "Start Time")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          'Live Start Times---------------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTime).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTime, BootstrapEnums.InputSize.Small, , "End Date")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTime, BootstrapEnums.InputSize.Small, , "End Time")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          'Live Start Times---------------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTimeBuffer).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTimeBuffer, BootstrapEnums.InputSize.Small, , "Wrap Date")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.AdHoc.AdHocBooking) d.EndDateTimeBuffer, BootstrapEnums.InputSize.Small, , "Wrap Time")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d) d.GenRefNo).Style.Width = "100%"
            .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As AdHocBooking) p.GenRefNo, BootstrapEnums.InputSize.Small, , "GenRef...")
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.Description).Style.Width = "100%"
            With .Helpers.TextBoxFor(Function(p As AdHocBooking) p.Description)
              .MultiLine = True
              .Style.Height = "100px"
              .AddClass("form-control")
            End With
          End With
        End With

        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.SystemArea).Style.Width = "100%"
        '  .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As AdHocBooking) p.SystemArea, BootstrapEnums.InputSize.Small, , "Sub-Dept (Area)...")
        'End With
        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(d) d.CountryLocation).Style.Width = "100%"
        '  .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As AdHocBooking) p.CountryLocation, BootstrapEnums.InputSize.Small, , "Country (Location)...")
        'End With


        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.StartDateTimeBuffer).Style.Width = "100%"
        '  With .Helpers.Bootstrap.Row
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.StartDateTimeBuffer, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.TimeEditorFor(Function(p As AdHocBooking) p.StartDateTimeBuffer, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '  End With
        'End With
        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.StartDateTime).Style.Width = "100%"
        '  With .Helpers.Bootstrap.Row
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.TimeEditorFor(Function(p As AdHocBooking) p.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '  End With
        'End With
        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.EndDateTime).Style.Width = "100%"
        '  With .Helpers.Bootstrap.Row
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.TimeEditorFor(Function(p As AdHocBooking) p.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '  End With
        'End With
        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.EndDateTimeBuffer).Style.Width = "100%"
        '  With .Helpers.Bootstrap.Row
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.EndDateTimeBuffer, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '      With .Helpers.Bootstrap.TimeEditorFor(Function(p As AdHocBooking) p.EndDateTimeBuffer, Singular.Web.BootstrapEnums.InputSize.Small)
        '      End With
        '    End With
        '  End With
        'End With

        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.Description).Style.Width = "100%"
        '  With .Helpers.TextBoxFor(Function(p As AdHocBooking) p.Description)
        '    .MultiLine = True
        '    .Style.Height = "100px"
        '    .AddClass("form-control")
        '  End With
        'End With

        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(d As AdHocBooking) d.GenRefNo).Style.Width = "100%"
        '  With .Helpers.Div
        '    With .Helpers.Bootstrap.InputGroup
        '      With .Helpers.Bootstrap.FormControlFor(Function(d As AdHocBooking) d.GenRefNo, BootstrapEnums.InputSize.Small)
        '        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) d.AdHocBookingTypeID = 6)
        '      End With
        '      With .Helpers.Bootstrap.InputGroupAddOnButton("", "Production Details", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-soccer-ball-o", , , "")
        '        .Button.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) d.GenRefNo > 0 AndAlso d.GenRefNo <> d.DefaultGenRefNo AndAlso d.IsValid)
        '        .Button.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) d.GenRefNo > 0 AndAlso d.GenRefNo <> d.DefaultGenRefNo AndAlso d.IsValid)
        '      End With
        '    End With
        '  End With
        'End With

        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  '.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) (d.UsingDefaultForProduction AndAlso d.GenRefNo IsNot Nothing) OrElse (d.UsingDefaultGenRefReason IsNot Nothing AndAlso d.UsingDefaultGenRefReason <> ""))
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.UsingDefaultGenRefReason).Style.Width = "100%"
        '  With .Helpers.Bootstrap.FormControlFor(Function(p As AdHocBooking) p.UsingDefaultGenRefReason, Singular.Web.BootstrapEnums.InputSize.Small)
        '  End With
        'End With
        'With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
        '  .Helpers.Bootstrap.LabelFor(Function(p As AdHocBooking) p.RequiresTravel).Style.Width = "100%"
        '  With .Helpers.Bootstrap.StateButton(Function(d As AdHocBooking) d.RequiresTravel, , , , , , "fa-minus", )
        '  End With
        'End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace