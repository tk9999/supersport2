﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSLayoutNew(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      'Messages-------------------------------------------------------------------------------------------
      With Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
        .Attributes("id") = "RoomScheduleMessages"
        With .Helpers.Toolbar
          .Helpers.MessageHolder()
        End With
      End With

      With Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
        'Booking Details------------------------------------------------------------------------------------
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          With .Helpers.Bootstrap.FlatBlock(, , True, , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                .Helpers.Control(New RSEditBookingPanel(Of VMType)())
              End With
            End With
          End With
        End With
        'Area-----------------------------------------------------------------------------------------------
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          With .Helpers.Bootstrap.FlatBlock("Room Details", , , , )
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.SystemID).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
                    '.Editor.AddClass("date-editor")
                    '.Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('OnAirStartDate', $data)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.ProductionAreaID).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
                    '.Editor.AddClass("date-editor")
                    '.Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('OnAirStartDate', $data)")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.Title).Style.Width = "100%"
                .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.Title, BootstrapEnums.InputSize.Small, , "Title...")
              End With
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.RoomID).Style.Width = "100%"
                .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.RoomID, BootstrapEnums.InputSize.Small, , "Room...")
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.CallTime).Style.Width = "100%"
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.CallTime, BootstrapEnums.InputSize.Small, , "Start Date")
                    .Editor.AddClass("date-editor")
                    .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('CallTimeDate', $data)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.CallTime, BootstrapEnums.InputSize.Small, , "Start Time")
                    .AddClass("time-editor")
                    .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('CallTime', $data)")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.OnAirTimeStart).Style.Width = "100%"
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.OnAirTimeStart, BootstrapEnums.InputSize.Small, , "Start Date")
                    .Editor.AddClass("date-editor")
                    .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('OnAirStartDate', $data)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.OnAirTimeStart, BootstrapEnums.InputSize.Small, , "Start Time")
                    .AddClass("time-editor")
                    .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('OnAirStartTime', $data)")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.OnAirTimeEnd).Style.Width = "100%"
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.OnAirTimeEnd, BootstrapEnums.InputSize.Small, , "Start Date")
                    .Editor.AddClass("date-editor")
                    .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('OnAirEndDate', $data)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.OnAirTimeEnd, BootstrapEnums.InputSize.Small, , "Start Time")
                    .AddClass("time-editor")
                    .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit('OnAirEndTime', $data)")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.WrapTime).Style.Width = "100%"
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.WrapTime, BootstrapEnums.InputSize.Small, , "Start Date")
                    .Editor.AddClass("date-editor")
                    '.Editor.AddBinding(KnockoutBindingString.enable, "RSTimelineBO.CanEdit('Timeline Date', $data)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.WrapTime, BootstrapEnums.InputSize.Small, , "Start Time")
                    .AddClass("time-editor")
                    '.AddBinding(KnockoutBindingString.enable, "RSTimelineBO.CanEdit('Timeline Start', $data)")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      '-----------------------------------------------------------------------------------------------Area
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace