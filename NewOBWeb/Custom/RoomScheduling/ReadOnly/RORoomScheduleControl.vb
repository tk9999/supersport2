﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.Rooms.ReadOnly

Namespace Controls

  Public Class RORoomScheduleControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Rooms.ReadOnly.RORoomBooking) '(Of ControlInterfaces(Of VMType).IRoomScheduleControl)
    '(Of RoomBookingType)

    'Private mRoomScheduleControlName As String = "CurrentRORoomScheduleControl"

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("CurrentRORoomScheduleModal", "Room Booking", False, "modal-md", Singular.Web.BootstrapEnums.Style.Primary, , "fa-fort-awesome", "fa-2x", True)
        '  .Heading.AddBinding(KnockoutBindingString.html, mRoomScheduleControlName & ".currentTitle()")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of OBLib.Rooms.ReadOnly.RORoomBooking)("ViewModel.CurrentRORoomBooking()")
            With .Helpers.Bootstrap.Row
              'Booking Details------------------------------------------------------------------------------------
              With .Helpers.Bootstrap.Column(12, 12, 5, 4, 3)
                With .Helpers.Bootstrap.FlatBlock("Booking Details")
                  With .ContentTag
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBooking) d.Title)
                      With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBooking) p.Title, BootstrapEnums.InputSize.Small, , "Event Name...")
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Rooms.ReadOnly.RORoomBookingArea)("$data.RORoomBookingAreaList()[0]")
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingArea) d.BookingTimes)
                        With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingArea) p.BookingTimes, BootstrapEnums.InputSize.Small, , "Event Name...")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingArea) d.ProductionAreaStatus)
                        With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingArea) p.ProductionAreaStatus, BootstrapEnums.InputSize.Small, , "Event Name...")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBooking) d.Debtor)
                      With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBooking) p.Debtor, BootstrapEnums.InputSize.Small, , "Event Name...")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBooking) d.OwnedBy)
                      With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBooking) p.OwnedBy, BootstrapEnums.InputSize.Small, , "Event Name...")
                      End With
                    End With
                    With .Helpers.With(Of OBLib.Rooms.ReadOnly.RORoomBookingArea)("$data.RORoomBookingAreaList()[0]")
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingArea) d.AreaComments)
                        With .Helpers.TextBoxFor(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingArea) d.AreaComments)
                          .MultiLine = True
                          .Style.Height = "240px"
                          .AddClass("form-control room-schedule-comments")
                          .AddBinding(KnockoutBindingString.enable, Function(d) False)
                        End With
                        'With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(p As OBLib.Rooms.ReadOnly.RORoomBookingArea) p.AreaComments, BootstrapEnums.InputSize.Small, , "Event Name...")
                        'End With
                      End With
                    End With
                  End With
                End With
              End With
              '------------------------------------------------------------------------------------Booking Details
              'Area-----------------------------------------------------------------------------------------------
              With .Helpers.Bootstrap.Column(12, 12, 7, 8, 9)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , BootstrapEnums.TabAlignment.Top)
                  'With .AddTab("BookingTimes", "fa-clock-o", "", "Booking Info", , False)

                  '  With .TabPane
                  '    With .Helpers.Bootstrap.Row
                  '      '.Helpers.Control(New EditRoomScheduleBookingTimesControl(Of VMType)(mRoomScheduleControlName))
                  '    End With
                  '  End With
                  'End With
                  'With .AddTab("EventInfo", "fa-info-circle", "", "Event Info", , False)
                  '  With .TabPane
                  '    With .Helpers.Bootstrap.Row
                  '      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  '        With .Helpers.FieldSet("Event Details")
                  '          With .Helpers.If("$data.RoomScheduleTypeID() == 1")
                  '            .Helpers.Control(New ROProductionInline(Of VMType)(mRoomScheduleControlName))
                  '          End With
                  '          With .Helpers.If("$data.RoomScheduleTypeID() == 4")
                  '            .Helpers.Control(New ROAdHocBookingInline(Of VMType)(mRoomScheduleControlName))
                  '          End With
                  '        End With
                  '      End With
                  '      'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  '      '  With .Helpers.FieldSet("Other Areas on event")

                  '      '  End With
                  '      'End With
                  '    End With
                  '  End With
                  'End With
                  With .AddTab("Crew", "fa-users", "", "Crew", , False)
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.With(Of OBLib.Rooms.ReadOnly.RORoomBookingArea)("$data.RORoomBookingAreaList()[0]")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.Rooms.ReadOnly.RORoomBookingAreaPHR)("$data.RORoomBookingAreaPHRList()", False, False, False, False, True, True, True)
                            .AddClass("no-border hover no-border-x list")
                            With .FirstRow
                              .AddClass("items")
                              .AddReadOnlyColumn(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingAreaPHR) d.DisciplinePosition)
                              .AddReadOnlyColumn(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingAreaPHR) d.HRName)
                              .AddReadOnlyColumn(Function(d As OBLib.Rooms.ReadOnly.RORoomBookingAreaPHR) d.BookingTimes)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              '-----------------------------------------------------------------------------------------------Area
            End With
            'With .Helpers.Bootstrap.Row
            '  With .Helpers.DivC("loading-custom")
            '    .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
            '    .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            '  End With
            'End With
          End With
        End With
      End With


    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace