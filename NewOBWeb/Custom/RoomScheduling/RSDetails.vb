﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.RoomScheduling
Imports OBLib.Helpers.ResourceHelpers
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSDetails(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Column(12, 12, 12, 6, 5)
        With .Helpers.FieldSet("Booking Info")
          With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
            With .Helpers.If("$data.IsOwner()")
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.Title).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.Title, BootstrapEnums.InputSize.Small, , "Title...")
                  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'Title')")
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.RoomID).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.RoomID, BootstrapEnums.InputSize.Small, , "Room...")
                  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'RoomID')")
                End With
              End With
            End With
            With .Helpers.If("!$data.IsOwner()")
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.Title).Style.Width = "100%"
                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As RoomScheduleArea) d.Title, BootstrapEnums.InputSize.Small, , "Title...")

                End With
              End With
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.Room).Style.Width = "100%"
                With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As RoomScheduleArea) d.Room, BootstrapEnums.InputSize.Small, , "Room...")

                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.CallTime).Style.Width = "100%"
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.CallTime, BootstrapEnums.InputSize.Small, , "Start Date")
                  .Editor.AddClass("date-editor")
                  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'CallTime')")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.CallTime, BootstrapEnums.InputSize.Small, , "Start Time")
                  .AddClass("time-editor")
                  .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'CallTime')")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.OnAirTimeStart).Style.Width = "100%"
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.OnAirTimeStart, BootstrapEnums.InputSize.Small, , "Start Date")
                  .Editor.AddClass("date-editor")
                  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'OnAirTimeStart')")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.OnAirTimeStart, BootstrapEnums.InputSize.Small, , "Start Time")
                  .AddClass("time-editor")
                  .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'OnAirTimeStart')")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.OnAirTimeEnd).Style.Width = "100%"
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.OnAirTimeEnd, BootstrapEnums.InputSize.Small, , "Start Date")
                  .Editor.AddClass("date-editor")
                  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'OnAirTimeEnd')")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.OnAirTimeEnd, BootstrapEnums.InputSize.Small, , "Start Time")
                  .AddClass("time-editor")
                  .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'OnAirTimeEnd')")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                .Helpers.Bootstrap.LabelFor(Function(d As RoomScheduleArea) d.WrapTime).Style.Width = "100%"
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FormControlFor(Function(d As RoomScheduleArea) d.WrapTime, BootstrapEnums.InputSize.Small, , "Start Date")
                  .Editor.AddClass("date-editor")
                  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'WrapTime')")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.TimeEditorFor(Function(d As RoomScheduleArea) d.WrapTime, BootstrapEnums.InputSize.Small, , "Start Time")
                  .AddClass("time-editor")
                  .AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.CanEdit($data, 'WrapTime')")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.Button(, "Updated Crew Times", BootstrapEnums.Style.Info, ,
                                               BootstrapEnums.ButtonSize.Small, , "fa-clock-o", , PostBackType.None,
                                               "RoomScheduleAreaBO.updateCrewTimesToBookingTimes($data)", )
                  .Button.AddClass("btn-block")
                  .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'UpdateCrewTimes')")
                End With
              End With
            End With
            With .Helpers.If("!$data.IsOwner()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.Button(, "Match with Parent", BootstrapEnums.Style.Warning, ,
                                                 BootstrapEnums.ButtonSize.Small, , "fa-asl-interpreting", , PostBackType.None,
                                                 "RoomScheduleAreaBO.matchWithParent($data)", )
                    .Button.AddClass("btn-block")
                    .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'MatchWithParent')")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      With Helpers.Bootstrap.Column(12, 12, 12, 6, 7)
        With .Helpers.FieldSet("Clashes")
          With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
            With .Helpers.ForEach(Of ClashDetail)(Function(d As RoomScheduleArea) d.ClashDetailList)
              With .Helpers.DivC("alert alert-danger fast slideInUp go")
                .Attributes("role") = "alert"
                With .Helpers.HTMLTag("span")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa-exclamation-circle")
                End With
                With .Helpers.HTMLTag("span")
                  .AddBinding(KnockoutBindingString.html, Function(d As ClashDetail) d.ResourceBookingDescription)
                End With
                'With .Helpers.HTMLTag("span")
                '  .AddBinding(KnockoutBindingString.html, Function(d As ClashDetail) d.ClashTimes)
                'End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace