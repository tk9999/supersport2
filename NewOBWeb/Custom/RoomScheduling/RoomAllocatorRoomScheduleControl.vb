﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RoomAllocatorRoomScheduleControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))
    '(Of RoomBookingType)

    Private Property IsOldScreen As Boolean

    Public Sub New(IsOldScreen As Boolean)
      Me.IsOldScreen = IsOldScreen
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.Bootstrap.Dialog("RoomAllocatorRoomScheduleModal", "Room Booking", , "modal-xxs", Singular.Web.BootstrapEnums.Style.Primary, , "fa-home", , True)
        .Heading.AddBinding(Singular.Web.KnockoutBindingString.html, "RoomScheduleAreaBO.currentTitle()")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Booking Info", , , , )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.With(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("ViewModel.CurrentRoomAllocatorRoomSchedule()")
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.Title).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.Title, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.ProductionAreaID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.ProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.RoomID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.RoomID, Singular.Web.BootstrapEnums.InputSize.Small, , "Room")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.CallTime)
                            .Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Row
                            .Style("margin-top") = "0px"
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Date")
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.CallTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Call Time")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.StartDateTime)
                            .Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Row
                            .Style("margin-top") = "0px"
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Date")
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Time")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.EndDateTime)
                            .Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Row
                            .Style("margin-top") = "0px"
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Date")
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small, , "End Time")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.WrapTime)
                            .Style.Width = "100%"
                          End With
                          With .Helpers.Bootstrap.Row
                            .Style("margin-top") = "0px"
                            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                              .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Wrap Date")
                            End With
                            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                              .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.WrapTime, Singular.Web.BootstrapEnums.InputSize.Small, , "Wrap Time")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.OwnerComments).Style.Width = "100%"
                          With .Helpers.Div
                            With .Helpers.TextBoxFor(Function(d As OBLib.Rooms.RoomAllocatorRoomSchedule) d.OwnerComments)
                              .MultiLine = True
                              .Style.Height = "240px"
                              .AddClass("form-control room-schedule-comments")
                            End With
                          End With
                        End With
                      End With
                      If IsOldScreen Then
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.Button(, "Create Booking", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-cloud-upload", , Singular.Web.PostBackType.None, "RoomAllocatorPage.saveCurrentRoomAllocatorRoomSchedule($data)", False)
                              .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorRoomScheduleBO.iconCss($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomAllocatorRoomScheduleBO.canSave($data)")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                        End With
                      Else
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.Button(, "Create Booking", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-cloud-upload", , Singular.Web.PostBackType.None, "window.allocator.saveNewRoomSchedule($data)", False)
                              .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorRoomScheduleBO.iconCss($data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomAllocatorRoomScheduleBO.canSave($data)")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                        End With
                      End If
                    End With
                    'End With
                  End With
                End With
              End With
            End With
            'If OBLib.Security.Settings.CurrentUser.SystemID = 2 Then
            '  With .Helpers.Bootstrap.Column(12, 12, 9, 9, 9)
            '    With .Helpers.With(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("ViewModel.CurrentRoomAllocatorRoomSchedule()")
            '      With .Helpers.Bootstrap.FlatBlock("Channels", , , , )
            '        With .ContentTag
            '          With .Helpers.Bootstrap.Row
            '            With .Helpers.Bootstrap.Column(12, 12, 21, 12, 12)
            '              With .Helpers.Bootstrap.TableFor(Of OBLib.Rooms.RoomAllocatorRoomScheduleChannel)("$data.RoomAllocatorRoomScheduleChannelList()",
            '                                                                                                False, False, False, False, True, True, True,
            '                                                                                                "RoomAllocatorRoomScheduleChannelList")
            '                .AllowClientSideSorting = True
            '                .AddClass("no-border hover list")
            '                .TableBodyClass = "no-border-y no-border-x"
            '                With .FirstRow
            '                  '.AddBinding(KnockoutBindingString.visible, "$data.IsOtherRoomSchedule()")
            '                  .AddClass("items selectable")
            '                  With .AddColumn("")
            '                    .Style.Width = "50px"
            '                    With .Helpers.Bootstrap.StateButton(Function(d) d.IsSelected, "Selected", "Select", , , , , )
            '                      .Button.AddBinding(KnockoutBindingString.enable, "$data.CanEdit()")
            '                    End With
            '                  End With
            '                  With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.ScheduleNumber)
            '                  End With
            '                  With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.ChannelShortName)
            '                  End With
            '                  With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.StatusCode)
            '                  End With
            '                  With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.ScheduledTimes)
            '                  End With
            '                  With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.OtherRoom)
            '                  End With
            '                  'With .AddReadOnlyColumn(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.OtherRoomScheduleTitle)
            '                  'End With
            '                  With .AddColumn("Removed From Synergy?")
            '                    With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.Rooms.RoomAllocatorRoomScheduleChannel) d.RemovedFromSynergy, , , "btn-danger", , "fa-frown-o", "fa-smile-o", )
            '                    End With
            '                  End With
            '                End With
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End If
          End With
        End With
        'With .Footer
        '  With .Helpers.With(Of OBLib.Rooms.RoomAllocatorRoomSchedule)("ViewModel.CurrentRoomAllocatorRoomSchedule()")
        '    With .Helpers.Bootstrap.PullRight
        '      With .Helpers.Bootstrap.Button(, "Create Booking", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
        '                                     "fa-cloud-upload", , Singular.Web.PostBackType.None, "RoomAllocatorPage.saveCurrentRoomAllocatorRoomSchedule($data)", False)
        '        .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "RoomAllocatorRoomScheduleBO.iconCss($data)")
        '        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RoomAllocatorRoomScheduleBO.canSave($data)")
        '        .Button.AddClass("btn-block")
        '      End With
        '    End With
        '  End With
        'End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace