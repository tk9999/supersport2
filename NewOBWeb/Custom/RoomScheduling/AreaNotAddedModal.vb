﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly

Namespace Controls

  Public Class AreaNotAddedModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))
    'IAreaNotAddedControl

    Private mSchedulerName As String = "window.Scheduler"

    Public Sub New(SchedulerName As String)
      mSchedulerName = SchedulerName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("AreaNotAddedModal", "Your area is not linked to this Booking", False, "modal-sm", BootstrapEnums.Style.Warning, , , )
        '.Heading.AddBinding(KnockoutBindingString.html, mSchedulerName & ".getResourceBookingCleanDescription()")
        With .Body
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of ResourceBookingClean)("ViewModel.ResourceBookingClean()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.HTMLTag("div")
                  .AddClass("text-center")
                  With .Helpers.HTMLTag("h1")
                    .Helpers.HTML("Your area is not linked to this Booking")
                  End With
                  With .Helpers.Bootstrap.FontAwesomeIcon("fa-frown-o", "fa-5x")
                  End With
                End With
                With .Helpers.HTMLTag("div")
                  .AddClass("text-center")
                  With .Helpers.HTMLTag("h3")
                    .Helpers.HTML("Would you like to add your area?")
                  End With
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              '  With .Helpers.FieldSet("Booking Details")
              '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
              '      .Helpers.Bootstrap.LabelFor(Function(d As ResourceBookingClean) d.ResourceBookingDescription)
              '      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ResourceBookingClean) d.ResourceBookingDescription, BootstrapEnums.InputSize.Small)
              '    End With
              '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
              '      .Helpers.Bootstrap.LabelFor(Function(d As ResourceBookingClean) d.ResourceName)
              '      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ResourceBookingClean) d.ResourceName, BootstrapEnums.InputSize.Small)
              '    End With
              '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
              '      .Helpers.Bootstrap.LabelFor(Function(d As ResourceBookingClean) d.StartDateTimeBuffer)
              '      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ResourceBookingClean) d.StartDateTimeBuffer, BootstrapEnums.InputSize.Small)
              '    End With
              '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
              '      .Helpers.Bootstrap.LabelFor(Function(d As ResourceBookingClean) d.StartDateTime)
              '      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ResourceBookingClean) d.StartDateTime, BootstrapEnums.InputSize.Small)
              '    End With
              '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
              '      .Helpers.Bootstrap.LabelFor(Function(d As ResourceBookingClean) d.EndDateTime)
              '      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ResourceBookingClean) d.EndDateTime, BootstrapEnums.InputSize.Small)
              '    End With
              '    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small, )
              '      .Helpers.Bootstrap.LabelFor(Function(d As ResourceBookingClean) d.EndDateTimeBuffer)
              '      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ResourceBookingClean) d.EndDateTimeBuffer, BootstrapEnums.InputSize.Small)
              '    End With
              '  End With
              'End With
              'With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              '  With .Helpers.FieldSet("Active areas")
              '    With .Helpers.Bootstrap.TableFor(Of RoomBookingArea)("$data.RoomBookingAreaList()", False, False, False, False, True, True, True)
              '      .AddClass("no-border hover list")
              '      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
              '      With .FirstRow
              '        .AddClass("items")
              '        .AddReadOnlyColumn(Function(d As RoomBookingArea) d.System)
              '        .AddReadOnlyColumn(Function(d As RoomBookingArea) d.ProductionArea)
              '        .AddReadOnlyColumn(Function(d As RoomBookingArea) d.ProductionAreaStatus)
              '      End With
              '    End With
              '  End With
              'End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-left")
            With .Helpers.Bootstrap.Button(, "No", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-down", ,
                                           PostBackType.None, mSchedulerName & ".cancelAddAreaToRoomSchedule()")
            End With
          End With
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Yes", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                           PostBackType.None, mSchedulerName & ".commitAddAreaToRoomSchedule()")
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace