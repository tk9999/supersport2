﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSArea(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.DivC("production-system-area-container")
        With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
          With .Helpers.FieldSet("Crew")
            'Toolbar
            With .Helpers.DivC("production-system-area-options-container")
              With .Helpers.DivC("pull-left")

              End With
              With .Helpers.DivC("pull-right")
                With .Helpers.Bootstrap.Button(, "Updated Crew Times", BootstrapEnums.Style.Info, ,
                                               BootstrapEnums.ButtonSize.Small, , "fa-clock-o", , PostBackType.None,
                                               "RoomScheduleAreaBO.updateCrewTimesToBookingTimes($data)", )
                  .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'UpdateCrewTimes')")
                End With
                With .Helpers.Bootstrap.Button(, "Add More Crew", BootstrapEnums.Style.Primary,
                                               , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle",
                                               , PostBackType.None, "RoomScheduleAreaBO.addCrew($data)")
                  .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'AddCrew')")
                End With
                With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger,
                                               , BootstrapEnums.ButtonSize.Small, , "fa-trash",
                                               , PostBackType.None, "RoomScheduleAreaBO.deletedSelectedPHR($data)")
                  .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.canDeleteCrew($data)")
                  .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'DeleteSelected')")
                End With
                With .Helpers.Bootstrap.Button(, "Cancel Selected", BootstrapEnums.Style.Warning,
                                               , BootstrapEnums.ButtonSize.Small, , "fa-hand-scissors-o",
                                               , PostBackType.None, "RoomScheduleAreaBO.cancelSelectedPHR($data)")
                  .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaBO.canCancelCrew($data)")
                  .Button.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaBO.canView($data, 'CancelSelected')")
                End With
              End With
            End With
            'Crew List
            With .Helpers.DivC("table-responsive")
              .Style.Width = "100%"
              With .Helpers.Bootstrap.TableFor(Of RoomScheduleAreaProductionHumanResource)("$data.RoomScheduleAreaProductionHumanResourceList()",
                                                                                           False, False, False, True, True, True, True,
                                                                                           "RoomScheduleAreaProductionHumanResourceList")
                .AllowClientSideSorting = True
                With .FirstRow
                  .AddClass("items selectable")
                  With .AddColumn("")
                    .Style.Width = "90px"
                    With .Helpers.Bootstrap.Label(, BootstrapEnums.Style.Danger, , "fa-exclamation-circle", , )
                      .LabelTag.AddBinding(KnockoutBindingString.title, "RoomScheduleAreaProductionHumanResourceBO.isValidTitleText($data)")
                      .LabelTag.AddBinding(KnockoutBindingString.visible, "!RoomScheduleAreaProductionHumanResourceBO.isValid($data)")
                    End With
                    With .Helpers.Bootstrap.Label(, BootstrapEnums.Style.Warning, , "fa-exclamation-triangle", , )
                      .LabelTag.AddBinding(KnockoutBindingString.title, "RoomScheduleAreaProductionHumanResourceBO.timeMismatchTitleText($data)")
                      .LabelTag.AddBinding(KnockoutBindingString.visible, "RoomScheduleAreaProductionHumanResourceBO.hasTimeMismatch($data)")
                    End With
                  End With
                  With .AddColumn("")
                    .Style.Width = "90px"
                    With .Helpers.DivC("btn-group")
                      'With .Helpers.HTMLTag("span")
                      '  .Helpers.Bootstrap.Button(, "Edit", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, ,
                      '                            "fa-edit", , PostBackType.None, , )
                      'End With
                      With .Helpers.HTMLTag("span")
                        With .Helpers.Bootstrap.StateButton(Function(d As RoomScheduleAreaProductionHumanResource) d.IsSelected, "Selected", "Select", , , , , )
                          .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('IsSelected', $data)")
                        End With
                      End With
                    End With
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.DisciplineID)
                    .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('DisciplineID', $data)")
                  End With
                  'With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.PositionTypeID)
                  'End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.PositionID)
                    .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('PositionID', $data)")
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.HumanResourceID)
                    .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('HumanResourceID', $data)")
                  End With
                  'With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.CancelledDate)
                  '  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('CancelledDate', $data)")
                  'End With
                  'With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.CancelledReason)
                  '  .Editor.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('CancelledReason', $data)")
                  'End With
                  With .AddColumn("Booking Times")
                    With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, ,
                                                   BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                   "fa-clock-o", , PostBackType.None, "RoomScheduleAreaBO.editProductionHumanResource($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, "RoomScheduleAreaProductionHumanResourceBO.getBookingTimes($data, $element)")
                      .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('BookingTimes', $data)")
                    End With
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHumanResource) d.IsCancelled)
                    .Editor.AddBinding(KnockoutBindingString.visible, Function(d) False)
                    With .Helpers.Bootstrap.StateButton("IsCancelled", ,, "btn-warning",,,,)
                      .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canEdit('IsCancelled', $data)")
                      .Button.AddBinding(KnockoutBindingString.title, "RoomScheduleAreaProductionHumanResourceBO.cancelledDescription($data)")
                    End With
                  End With
                  With .AddColumn("")
                    .Style.Width = "50px"
                    With .Helpers.DivC("btn-group")
                      With .Helpers.HTMLTag("span")
                        With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Warning, , BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                  "fa-eraser", , PostBackType.None, "RoomScheduleAreaBO.removeHumanResource($data, $element)", )
                          .Button.AddBinding(KnockoutBindingString.enable, "RoomScheduleAreaProductionHumanResourceBO.canRemoveHumanResource($data)")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace