﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSHRBookings(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.DivC("production-system-area-container")
        With .Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
          With .Helpers.FieldSet("Bookings")
            'Crew List
            With .Helpers.DivC("table-responsive")
              .Style.Width = "100%"
              With .Helpers.Bootstrap.TableFor(Of RoomScheduleAreaProductionHRBooking)("$data.RoomScheduleAreaProductionHRBookingList()",
                                                                                       False, False, False, True, True, True, True,
                                                                                       "RoomScheduleAreaProductionHRBookingList")
                .AllowClientSideSorting = True
                With .FirstRow
                  .AddClass("items selectable")
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.ResourceID)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.HumanResourceID)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTimeBuffer)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.StartDateTime)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTime)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.EndDateTimeBuffer)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.IsCancelled)
                  End With
                  With .AddColumn(Function(d As RoomScheduleAreaProductionHRBooking) d.IgnoreClashesReason)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace