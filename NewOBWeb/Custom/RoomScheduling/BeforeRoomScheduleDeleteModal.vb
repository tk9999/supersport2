﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly

Namespace Controls

  Public Class BeforeRoomScheduleDeleteModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mRoomScheduleControlName As String = "CurrentRoomScheduleControl"

    Public Sub New(RoomScheduleControlName As String)
      mRoomScheduleControlName = RoomScheduleControlName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("BeforeRoomScheduleDeleteModal", "Title", False, "modal-sm", BootstrapEnums.Style.Danger)
        .Heading.AddBinding(KnockoutBindingString.html, mRoomScheduleControlName & ".getRORoomScheduleInfoDescription()")
        With .Body
          With .Helpers.Bootstrap.Row
            'With .Helpers.With(Of RORoomScheduleInfo)("ViewModel.RORoomScheduleInfo()")
            '  With .Helpers.Bootstrap.Column(12, 12, 4, 3, 3)
            '    With .Helpers.HTMLTag("p")
            '      With .Helpers.HTMLTag("h1")
            '        .AddBinding(KnockoutBindingString.html, "$data.RORoomScheduleInfoDetailList().length")
            '      End With
            '    End With
            '    With .Helpers.HTMLTag("p")
            '      .Helpers.HTML("area/s are working on this booking")
            '    End With
            '  End With
            '  With .Helpers.Bootstrap.Column(12, 12, 8, 9, 9)
            '    With .Helpers.FieldSet("Area Details")
            '      With .Helpers.Bootstrap.TableFor(Of RORoomScheduleInfoDetail)("$data.RORoomScheduleInfoDetailList()", False, False, False, False, True, True, True)
            '        .AddClass("no-border hover list")
            '        .TableBodyClass = "no-border-y no-border-x no-border-x-top"
            '        With .FirstRow
            '          .AddClass("items")
            '          .AddReadOnlyColumn(Function(d As RORoomScheduleInfoDetail) d.System)
            '          .AddReadOnlyColumn(Function(d As RORoomScheduleInfoDetail) d.ProductionArea)
            '          .AddReadOnlyColumn(Function(d As RORoomScheduleInfoDetail) d.ProductionAreaStatus)
            '          .AddReadOnlyColumn(Function(d As RORoomScheduleInfoDetail) d.PersonnelCount)
            '          .AddReadOnlyColumn(Function(d As RORoomScheduleInfoDetail) d.StartTime)
            '          .AddReadOnlyColumn(Function(d As RORoomScheduleInfoDetail) d.EndTime)
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-left")
            With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
                                           PostBackType.None, mRoomScheduleControlName & ".cancelRoomScheduleDelete()")
            End With
          End With
          With .Helpers.DivC("pull-right")
            With .Helpers.Bootstrap.Button(, "Commit Delete", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-trash", ,
                                           PostBackType.None, mRoomScheduleControlName & ".commitRoomScheduleDelete()")
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace