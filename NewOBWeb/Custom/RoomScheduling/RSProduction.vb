﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.AdHoc
Imports OBLib.RoomScheduling

Namespace Controls

  Public Class RSProduction(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '.IRoomScheduleControl(Of RoomBookingType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OBLib.Productions.Production)("ViewModel.CurrentProduction()")
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.ProductionRefNo).Style.Width = "100%"
            .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Productions.Production) d.ProductionRefNo, BootstrapEnums.InputSize.Small, , "Ref Num...")
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          'Production Type----------------------------------------------------
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.ProductionTypeID).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.ProductionTypeID, BootstrapEnums.InputSize.Small, , )
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          'Event Type---------------------------------------------------------
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.EventTypeID).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.EventTypeID, BootstrapEnums.InputSize.Small, , )
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          'Title--------------------------------------------------------------
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d) d.Title).Style.Width = "100%"
            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.Title, BootstrapEnums.InputSize.Small, , "Title...")
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
          'Teams--------------------------------------------------------------
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.TeamsPlaying).Style.Width = "100%"
            .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.TeamsPlaying, BootstrapEnums.InputSize.Small, "Teams..")
          End With
        End With
        'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '  'Venue--------------------------------------------------------------
        '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
        '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.ProductionVenueID).Style.Width = "100%"
        '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.ProductionVenueID, BootstrapEnums.InputSize.Small, , )
        '    End With
        '  End With
        'End With
        'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '  'Venue--------------------------------------------------------------
        '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
        '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.VenueConfirmedDate).Style.Width = "100%"
        '    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.VenueConfirmedDate, BootstrapEnums.InputSize.Small, , )
        '    End With
        '  End With
        'End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          'Live Start Times---------------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.PlayStartDateTime).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.PlayStartDateTime, BootstrapEnums.InputSize.Small, , "Start Date")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Productions.Production) d.PlayStartDateTime, BootstrapEnums.InputSize.Small, , "Start Time")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          'Live End Times-----------------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.PlayEndDateTime).Style.Width = "100%"
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.PlayEndDateTime, BootstrapEnums.InputSize.Small, , "End Date")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Productions.Production) d.PlayEndDateTime, BootstrapEnums.InputSize.Small, , "End Time")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.SynergyGenRefNo).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Productions.Production) d.SynergyGenRefNo, BootstrapEnums.InputSize.Small, , "Gen Ref...")
              .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
            End With
          End With
        End With
        'With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
        '  'Placeholder-----------------------------------------------------------
        '  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
        '    .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Productions.Production) d.PlaceholderInd).Style.Width = "100%"
        '    .Helpers.Bootstrap.StateButton(Function(d As OBLib.Productions.Production) d.PlaceholderInd, "Placeholder", "Placeholder",
        '                                   , , , , "btn-sm")
        '  End With
        'End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)

        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace