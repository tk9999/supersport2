﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.Rooms.ReadOnly
Imports OBLib.Helpers.ResourceHelpers

Namespace Controls

  Public Class ContentServicesDifferencesModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()

    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("ContentServicesDifferencesModal", "Booking Differences", False, "modal-md", BootstrapEnums.Style.Success, , "fa-frown-o", )
        With .Body
          With .Helpers.With(Of OBLib.Scheduling.Rooms.ContentServicesDifferenceList.Criteria)("ViewModel.ContentServicesDifferenceListCriteria()")
            With .Helpers.Bootstrap.Row
              'With .Helpers.Bootstrap.Column(12, 12, 3, 2, 1)
              '  With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-angle-left", ,
              '                                 PostBackType.None, "ContentServicesDifferenceListCriteriaBO.previousDay($data)")
              '    .Button.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
              '    .Button.AddClass("btn-block")
              '  End With
              'End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifferenceList.Criteria) d.StartDate, BootstrapEnums.InputSize.Small)
                  .Editor.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifferenceList.Criteria) d.EndDate, BootstrapEnums.InputSize.Small)
                  .Editor.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 3, 2, 1)
              '  With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-angle-right", ,
              '                                 PostBackType.None, "ContentServicesDifferenceListCriteriaBO.nextDay($data)")
              '    .Button.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
              '    .Button.AddClass("btn-block")
              '  End With
              'End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.TableFor(Of OBLib.Scheduling.Rooms.ContentServicesDifference)("ViewModel.ContentServicesDifferenceList()", False, False, , , , , , "ContentServicesDifferenceList")
                With .FirstRow
                  With .AddColumn("")
                    .Style.Width = "50px"
                    .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.IsSelected, "Selected", "Select", , , , , )
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.ResourceNameOwnr)
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.Title)
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.CallTimeString)
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.CallTimeStart)
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.WrapTimeEnd)
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Scheduling.Rooms.ContentServicesDifference) d.CrewCount)
                  End With
                  With .AddColumn("Differences")
                    With .Helpers.SpanC("btn btn-xs btn-danger")
                      .AddBinding(KnockoutBindingString.html, "'Removed'")
                      .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('RemovedDiff', $data)")
                    End With
                    With .Helpers.SpanC("btn btn-xs btn-success")
                      .AddBinding(KnockoutBindingString.html, "'New'")
                      .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('NotAddedDiff', $data)")
                    End With
                    With .Helpers.SpanC("btn btn-xs btn-info")
                      .AddBinding(KnockoutBindingString.html, "$data.RoomChangeDescription()")
                      .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('RoomDiff', $data)")
                    End With
                    With .Helpers.SpanC("btn btn-xs btn-info")
                      .AddBinding(KnockoutBindingString.html, "$data.StartTimeChangeDescription()")
                      .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('StartTimeDiff', $data)")
                    End With
                    With .Helpers.SpanC("btn btn-xs btn-info")
                      .AddBinding(KnockoutBindingString.html, "$data.EndTimeChangeDescription()")
                      .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('EndTimeDiff', $data)")
                    End With
                  End With
                  'With .AddColumn("Action/s")
                  '  With .Helpers.SpanC("btn btn-xs btn-danger")
                  '    .AddBinding(KnockoutBindingString.html, "'Remove'")
                  '    .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('PendingDeleteButton', $data)")
                  '  End With
                  '  With .Helpers.SpanC("btn btn-xs btn-success")
                  '    .AddBinding(KnockoutBindingString.html, "'Create'")
                  '    .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('NotAddedButton', $data)")
                  '  End With
                  '  With .Helpers.SpanC("btn btn-xs btn-info")
                  '    .AddBinding(KnockoutBindingString.html, "'Update'")
                  '    .AddBinding(KnockoutBindingString.visible, "ContentServicesDifferenceBO.canView('UpdateButton', $data)")
                  '  End With
                  'End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.Bootstrap.PullRight()
            With .Helpers.With(Of OBLib.Scheduling.Rooms.ContentServicesDifferenceList.Criteria)("ViewModel.ContentServicesDifferenceListCriteria()")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium,
                               , "fa-floppy-o", , PostBackType.None, "ContentServicesDifferenceModal.save()")
                .Button.AddBinding(KnockoutBindingString.enable, "!$data.IsProcessing()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace