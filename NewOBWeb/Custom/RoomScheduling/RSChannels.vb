﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.RoomScheduling
Imports OBLib.Scheduling.Rooms

Namespace Controls

  Public Class RSChannels(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of VMType)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of RoomScheduleArea)("RoomScheduleAreaBO.currentRoomScheduleArea()")
        With .Helpers.FieldSet("Channels")
          With .Helpers.DivC("table-responsive")
            With .Helpers.If("$data.IsOwner()")
              With .Helpers.Bootstrap.TableFor(Of RoomScheduleChannel)("$data.RoomScheduleChannelList()", False, False, False, True, True, True, True, "RoomScheduleChannelList")
                .AllowClientSideSorting = True
                With .FirstRow
                  '.AddBinding(KnockoutBindingString.visible, "$data.IsOtherRoomSchedule()")
                  .AddClass("items selectable")
                  With .AddColumn("")
                    .Style.Width = "50px"
                    With .Helpers.Bootstrap.StateButtonNew(Function(d) d.IsSelected, "Selected", "Select", , , , , )
                      .Button.AddBinding(KnockoutBindingString.enable, "$data.CanEdit()")
                    End With
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.ScheduleNumber)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.ChannelShortName)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.StatusCode)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.ScheduledTimes)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.OtherRoom)
                  End With
                  'With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.OtherRoomScheduleTitle)
                  'End With
                  With .AddColumn("Removed From Synergy?")
                    With .Helpers.Bootstrap.ROStateButton(Function(d As RoomScheduleChannel) d.RemovedFromSynergy, , , "btn-danger", , "fa-frown-o", "fa-smile-o", )
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.If("!$data.IsOwner()")
              With .Helpers.Bootstrap.TableFor(Of RoomScheduleChannel)("$data.RoomScheduleChannelList()", False, False, False, False, True, True, True, "RoomScheduleChannelList")
                .AllowClientSideSorting = True
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y no-border-x"
                With .FirstRow
                  '.AddBinding(KnockoutBindingString.visible, "$data.IsOtherRoomSchedule()")
                  .AddClass("items selectable")
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.ScheduleNumber)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.ChannelShortName)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.StatusCode)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.ScheduledTimes)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.OtherRoom)
                  End With
                  With .AddReadOnlyColumn(Function(d As RoomScheduleChannel) d.OtherRoomScheduleTitle)
                  End With
                  With .AddColumn("Removed From Synergy?")
                    With .Helpers.Bootstrap.ROStateButton(Function(d As RoomScheduleChannel) d.RemovedFromSynergy, , , "btn-danger", , "fa-frown-o", "fa-smile-o", )
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace