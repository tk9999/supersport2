﻿Imports OBLib.Invoicing.ReadOnly
Imports OBLib.Timesheets
Imports Singular.Web

Namespace Controls

  Public Class ServicesFreelancerTimesheetControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IFreelancerTimesheet)

    Private mPaymentRun As String = "ViewModel.CurrentPaymentRun()"
    Private mCurrentFreelancerTimesheetList As String = "ViewModel.CurrentFreelancerTimesheetList()"

    Public Sub New(Optional PaymentRun As String = "ViewModel.CurrentPaymentRun()",
                   Optional CurrentFreelancerTimesheetList As String = "ViewModel.CurrentFreelancerTimesheetList()")
      mPaymentRun = PaymentRun
      mCurrentFreelancerTimesheetList = CurrentFreelancerTimesheetList
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'Freelancer Timesheet (Freelancer, ISP)***************************************************************************************************************
      With Helpers.With(Of ROPaymentRun)(mPaymentRun)
        With .Helpers.Div
          With .Helpers.Bootstrap.PullLeft
            With .Helpers.Bootstrap.Button(, "Print", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                           Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                           "fa-print", , Singular.Web.PostBackType.None, "UserProfilePage.printFreelancerTimesheet($data)")

            End With
          End With
          With .Helpers.Bootstrap.PullRight
            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Primary, ,
                                           Singular.Web.BootstrapEnums.ButtonSize.Medium, ,
                                           "fa-search", , Singular.Web.PostBackType.None,
                                           "UserProfilePage.SelectPaymentRun()")
              .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "ViewModel.CurrentPaymentRun().MonthYearString()")
            End With
          End With
        End With
      End With
      With Helpers.With(Of OBLib.Timesheets.FreelancerTimesheetList)(mCurrentFreelancerTimesheetList)
        With .Helpers.DivC("table-responsive")
          With .Helpers.Bootstrap.TableFor(Of OBLib.Timesheets.FreelancerTimesheet)(mCurrentFreelancerTimesheetList,
                                                                                    False, False, False, False, True, True, True)
            .AddClass("no-border hover list")
            .TableBodyClass = "no-border-y no-border-x"
            With .FirstRow
              .AddClass("items")
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.AccountingDate)
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.DayOfWeek).Style.TextAlign = Singular.Web.TextAlign.center
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.[Description])
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.StartTime).Style.TextAlign = Singular.Web.TextAlign.center
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.EndTime).Style.TextAlign = Singular.Web.TextAlign.center
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.HoursForDay)
              .AddReadOnlyColumn(Function(d As OBLib.Timesheets.FreelancerTimesheet) d.SnT)
            End With
            With .FooterRow
              With .AddColumn
                .ColSpan = 5
                .Helpers.HTML("Totals")
              End With
              With .AddColumn()
                With .Helpers.DivC("pull-right")
                  .Helpers.HTMLTag("label").AddBinding(Singular.Web.KnockoutBindingString.html, "ViewModel.CurrentFreelancerTimesheetList().Sum('HoursForDay').toString()")
                End With
              End With
              With .AddColumn()
                With .Helpers.DivC("pull-right")
                  .Helpers.HTMLTag("label").AddBinding(Singular.Web.KnockoutBindingString.html, "ViewModel.CurrentFreelancerTimesheetList().Sum('SnT').toString()")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace