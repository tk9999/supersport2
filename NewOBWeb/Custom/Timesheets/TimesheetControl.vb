﻿Imports OBLib.Timesheets.OBCity
Imports Singular.Web

Namespace Controls

    Public Class TimesheetControl(Of VMType)
        Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).ITimesheet)

        Private mEditTimesheetFunction As String = ""
        Public Sub New(EditTimesheetFunction As String)
            mEditTimesheetFunction = EditTimesheetFunction
        End Sub

        Protected Overrides Sub Setup()
            MyBase.Setup()

            'Toolbar
            With Helpers.Bootstrap.Row
                With .Helpers.With(Of OBCityTimesheet)(Function(d) d.OBCityTimesheet)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Button(, "Add Adjustment", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-warning",
                                                       , Singular.Web.PostBackType.None,
                                                       "TimesheetManagementPage.AddAdjustment($data)")
                        End With
                    End With
                End With
            End With

            'Non Freelancer Timesheet (Perm/FTC)******************************************************************************************************************
            With Helpers.Bootstrap.Row
                With .Helpers.With(Of OBCityTimesheet)(Function(d) d.OBCityTimesheet)
                    With .Helpers.Bootstrap.TableFor(Of OBLib.Timesheets.OBCity.CrewTimesheet)(Function(d) d.CrewTimesheetList,
                                                                                                 False, False, False, False, True, True, False)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y no-border-x no-border-row-top no-border-row-bottom no-border-x-top"
                        With .FirstRow
                            .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.LastDayPreviousMonthInd()")
                            .AddClass("items")
                            With .AddColumn("")
                                .Attributes("width") = "50px"
                                With .Helpers.If(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) Not d.GetParent.TMClosedInd)
                                    With .Helpers.Bootstrap.Button(, "Query", Singular.Web.BootstrapEnums.Style.Primary,
                                       , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                       "fa-question-circle", , Singular.Web.PostBackType.None,
                                       mEditTimesheetFunction)
                                        .Button.AddBinding(KnockoutBindingString.visible, "CrewTimesheetBO.CanViewField('QueryButton', $data, $element)")
                                    End With
                                    With .Helpers.Bootstrap.Button(, "Queried", Singular.Web.BootstrapEnums.Style.Warning,
                                                                   , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                                   "fa-comments-o", , Singular.Web.PostBackType.None,
                                                                   mEditTimesheetFunction)
                                        .Button.AddBinding(KnockoutBindingString.visible, "CrewTimesheetBO.CanViewField('QueriedButton', $data, $element)")
                                    End With
                                End With
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.AccountingDate, 0, , "center")
                                .Attributes("width") = "120px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.DayOfWeek, 0, , "center")
                                .Attributes("width") = "50px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            End With
                            With .AddColumn("PH?")
                                .Attributes("width") = "25px"
                                With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.PublicHolidayInd,
                                                                      "Yes", "No", , , "", "", )
                                End With
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.ShortDescription, 0, , "left")
                                .Attributes("width") = "250px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.Description)
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.StartDate, 0, , "center")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.StartTime, 0, , "center")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.EndTime, 0, , "center")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.HoursForDay, 0, , "right")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.HoursForMonth, 0, , "right")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.css, "CrewTimesheetBO.GetHoursForMonthCss($data)")
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.ShortfallHoursString, 0, , "right")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.css, "CrewTimesheetBO.GetShortfallCss($data)")
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.SnTForDayString, 0, , "right")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                            End With
                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.MealsString, 0, , "right")
                                .Attributes("width") = "100px"
                                .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                            End With
                        End With
                        With .FooterRow
                            With .AddColumn
                                .ColSpan = 13
                            End With
                        End With
                    End With
                End With
            End With

            'Summary
            With Helpers.Bootstrap.Row
                With .Helpers.With(Of OBCityTimesheet)(Function(d) d.OBCityTimesheet)
                    With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                            With .Helpers.Bootstrap.Row
                                'Adjustment and Final Headings
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Overall Summary")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Unadjusted")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Adjustment")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Final")
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Hours
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Hours")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedHours)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.NormalHoursAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalHours)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Planning Hours
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Planning Hours")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedPlanningHours)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.PlanningHoursAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalPlanningHours)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Total
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Total")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedTotal)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.TotalAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalTotal)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Required Hours
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Required Hours")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedRequiredHours)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.RequiredHoursAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalRequiredHours)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Public Holidays within Required
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Public Holidays within Required (x1)")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .Helpers.HTML("0")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .Helpers.HTML("0")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.PublicHolidaysUnderLimit)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Public Holidays above Required
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Public Holidays above Required (x2)")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .Helpers.HTML("0")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .Helpers.HTML("0")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.PublicHolidaysOverLimit)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Public Holidays Total
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Public Holidays Total")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedPublicHolidayHours)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.PublicHolidayHoursAdjustedAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalPublicHolidayHours)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Overtime
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Overtime (x1.5)")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedOvertime)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.OvertimeAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalOvertime)
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Row
                                'Shortfall
                                With .Helpers.Bootstrap.Column(6, 6, 6, 4, 4)
                                    .AddClass("col-lg-offset-2 col-xl-offset-2")
                                    .AddClass("negative-cell")
                                    With .Helpers.HTMLTag("label")
                                        .Helpers.HTML("Shortfall")
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.UnadjustedShortfall)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.ShortfallAdjustment)
                                    End With
                                End With
                                With .Helpers.Bootstrap.Column(2, 2, 2, 2)
                                    .AddClass("text-right")
                                    With .Helpers.HTMLTag("label")
                                        .AddClass("summary-label")
                                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.FinalShortfall)
                                    End With
                                End With
                            End With
                        End With
                    End With
                End With
            End With
            
        End Sub

        Protected Overrides Sub Render()
            MyBase.Render()
            RenderChildren()
        End Sub

    End Class


End Namespace