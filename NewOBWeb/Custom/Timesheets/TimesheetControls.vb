﻿
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports OBLib.Timesheets
Imports Singular.Web

Namespace Controls

  Public Class TimesheetControls

    Public Class ROTimesheetMonthSelectorControl(Of VMType)
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROTimesheetMonthSelector)

      Private mOnRowSelected As String = ""
      Private mDelayedRefreshFunction As String = ""
      Private mCriteriaProperty As String = ""
      Private mManagerProperty As String = ""
      Private mListProperty As String = ""


      Public Sub New(Optional OnRowSelect As String = "",
                     Optional DelayedRefreshFunction As String = "",
                     Optional CriteriaProperty As String = "ViewModel.ROTimesheetMonthListCriteria()",
                     Optional ManagerProperty As String = "ViewModel.ROTimesheetMonthPagedListManager",
                     Optional ListProperty As String = "ViewModel.ROTimesheetMonthList()")
        mOnRowSelected = OnRowSelect
        mDelayedRefreshFunction = DelayedRefreshFunction
        mCriteriaProperty = CriteriaProperty
        mManagerProperty = ManagerProperty
        mListProperty = ListProperty
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()
        'Timesheet Select***************************************************************************************************************
        With Helpers.Bootstrap.Dialog("SelectTimesheetMonth", "Select Month", , "modal-xs", BootstrapEnums.Style.Primary, , "", "fa-2x")
          With .Body
            With .Helpers.Bootstrap.Row
              With .Helpers.With(Of OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetMonthList.Criteria)(mCriteriaProperty)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month Name..."
                    If mDelayedRefreshFunction <> "" Then
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayedRefreshFunction & " }")
                    End If
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.PagedGridFor(Of ROTimesheetMonth)(mManagerProperty,
                                                                        mListProperty,
                                                                        False, False, False, False, True, True, True, "",
                                                                        Singular.Web.BootstrapEnums.PagerPosition.Bottom, , , True)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y no-border-x"
                With .FirstRow
                  .AddClass("selectable")
                  .AddReadOnlyColumn(Function(d As ROTimesheetMonth) d.MonthYear)
                  .AddReadOnlyColumn(Function(d As ROTimesheetMonth) d.StartDate)
                  .AddReadOnlyColumn(Function(d As ROTimesheetMonth) d.EndDate)
                  With .AddColumn("Status")
                    .Style.Width = "50px"
                    With .Helpers.Bootstrap.ROStateButton(Function(d As ROTimesheetMonth) d.ClosedInd,
                                                          "Closed", "Open", "btn-danger",
                                                          "btn-primary", "fa-folder", "fa-folder-open-o", )
                    End With
                  End With
                End With
                If mOnRowSelected <> "" Then
                  .FirstRow.AddBinding(Singular.Web.KnockoutBindingString.click, mOnRowSelected)
                End If
              End With
            End With
          End With
        End With
      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

  End Class

End Namespace


