﻿Imports OBLib.Timesheets.OBCity
Imports Singular.Web

Namespace Controls

    Public Class TimesheetAdjustmentsControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IServicesFTCTimesheet)

        Private mSaveFunction As String = ""
        Private mCurrentAdjustmentFunction As String = ""
        Private mAddNewAdjustmentFunction As String = ""
        Private mAddNewAdjustmentToListFunction As String = ""

        Public Sub New(SaveFunction As String, AddNewAdjustmentFunction As String, CurrentAdjustmentFunction As String, AddNewAdjustmentToListFunction As String)
            mSaveFunction = SaveFunction
            mAddNewAdjustmentFunction = AddNewAdjustmentFunction
            mCurrentAdjustmentFunction = CurrentAdjustmentFunction
            mAddNewAdjustmentToListFunction = AddNewAdjustmentToListFunction
        End Sub

        Protected Overrides Sub Setup()
            MyBase.Setup()
            With Helpers.Bootstrap.Dialog("CrewTimesheetAdjustmentsModal", "Adjustments", False, "modal-md", BootstrapEnums.Style.Warning)
                With .Body
                    .AddClass("row")
                    With .Helpers.With(Of OBCityTimesheet)(Function(d) d.OBCityTimesheet)
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                            With .Helpers.FieldSet("New Adjustment")
                                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                    With .Helpers.Bootstrap.Button(, "Add", Singular.Web.BootstrapEnums.Style.Primary,
                                                                   , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                                                   Singular.Web.PostBackType.None,
                                                                   mAddNewAdjustmentFunction)
                                        .Button.AddClass("btn-block")
                                        '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                                    End With
                                End With
                                With .Helpers.With(Of TimesheetAdjustment)(mCurrentAdjustmentFunction)
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        .Helpers.Bootstrap.LabelFor(Function(d As TimesheetAdjustment) d.TimesheetHourTypeID)
                                        With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetAdjustment) d.TimesheetHourTypeID, BootstrapEnums.InputSize.Small)

                                        End With
                                    End With
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        .Helpers.Bootstrap.LabelFor(Function(d As TimesheetAdjustment) d.Adjustment)
                                        With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetAdjustment) d.Adjustment, BootstrapEnums.InputSize.Small)

                                        End With
                                    End With
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        .Helpers.Bootstrap.LabelFor(Function(d As TimesheetAdjustment) d.AdjustmentReason)
                                        With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetAdjustment) d.AdjustmentReason, BootstrapEnums.InputSize.Small)

                                        End With
                                    End With
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        .Helpers.Bootstrap.LabelFor(Function(d As TimesheetAdjustment) d.AdjustedTimesheetMonthID)
                                        With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetAdjustment) d.AdjustedTimesheetMonthID, BootstrapEnums.InputSize.Small)

                                        End With
                                    End With
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        .Helpers.Bootstrap.LabelFor(Function(d As TimesheetAdjustment) d.ReflectInTimesheetMonthID)
                                        With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetAdjustment) d.ReflectInTimesheetMonthID, BootstrapEnums.InputSize.Small)

                                        End With
                                    End With
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        .Helpers.Bootstrap.LabelFor(Function(d As TimesheetAdjustment) d.Authorised)
                                        With .Helpers.Bootstrap.StateButton(Function(d As TimesheetAdjustment) d.Authorised,
                                                                            "Authorised", "Not Authorised", , , , "fa-minus", "btn-sm")
                                            .Button.AddClass("btn-block")
                                        End With
                                    End With
                                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                        With .Helpers.Bootstrap.Button(, "Done", Singular.Web.BootstrapEnums.Style.Primary,
                                                                       , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-check-square-o", ,
                                                                       Singular.Web.PostBackType.None,
                                                                       mAddNewAdjustmentToListFunction)
                                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, mCurrentAdjustmentFunction & ".IsValid()")
                                            .Button.AddClass("btn-block")
                                        End With
                                    End With
                                End With
                            End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 9, 9, 9)
                            With .Helpers.FieldSet("All Adjustments")
                                With .Helpers.DivC("table-responsive")
                                    With .Helpers.Bootstrap.TableFor(Of OBLib.Timesheets.OBCity.TimesheetAdjustment)(Function(d) d.TimesheetAdjustmentList,
                                                                                                                     False, False, False, False, True, True, False)
                                        .AddClass("no-border hover list")
                                        .TableBodyClass = "no-border-y no-border-x no-border-row-top no-border-row-bottom no-border-x-top"
                                        With .FirstRow
                                            .AddClass("items")
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.TimesheetHourTypeID)

                                            End With
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.Adjustment)

                                            End With
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.AdjustmentReason)

                                            End With
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.AdjustedTimesheetMonthID)

                                            End With
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.ReflectInTimesheetMonthID)

                                            End With
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.AuthorisedByUserID)

                                            End With
                                            With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.AuthorisedDateTime)

                                            End With
                                            If Singular.Debug.InDebugMode Then
                                                With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.TimesheetAdjustment) d.HumanResourceID)

                                                End With
                                            End If
                                        End With
                                    End With
                                End With
                            End With
                        End With
                    End With
                End With
                With .Footer
                    With .Helpers.With(Of OBCityTimesheet)(Function(d) d.OBCityTimesheet)
                        With .Helpers.DivC("pull-right")
                            With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success,
                                                           , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                                           Singular.Web.PostBackType.None,
                                                           mSaveFunction)
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
                            End With
                        End With
                    End With
                End With
            End With

        End Sub

        Protected Overrides Sub Render()
            MyBase.Render()
            RenderChildren()
        End Sub

    End Class

End Namespace


