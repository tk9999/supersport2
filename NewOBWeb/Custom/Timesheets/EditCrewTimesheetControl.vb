﻿Imports OBLib.Timesheets.OBCity
Imports Singular.Web

Namespace Controls

  Public Class EditCrewTimesheetControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditCrewTimesheet)

    Private mCurrentCrewTimesheetFunction As String = ""
    Private mSaveCurrentTimesheetFunction As String = ""

    Public Sub New(CurrentCrewTimesheetFunction As String, SaveCurrentTimesheetFunction As String)
      mCurrentCrewTimesheetFunction = CurrentCrewTimesheetFunction
      mSaveCurrentTimesheetFunction = SaveCurrentTimesheetFunction
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("EditCrewTimesheet", "Query Timesheet", False, "modal-xs", BootstrapEnums.Style.Warning, , "fa-question-circle", "fa-2x")
        With .Body
          With .Helpers.With(Of OBLib.Timesheets.OBCity.CrewTimesheet)(mCurrentCrewTimesheetFunction)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.AccountingDate).Style.Width = "100%"
                  .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.AccountingDate, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.TimesheetCategoryID).Style.Width = "100%"
                  .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.TimesheetCategory.TimesheetCategory, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CalculatedStartDateTime).Style.Width = "100%"
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CalcStartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      .FieldDisplay.Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CalcStartTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      .FieldDisplay.Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CalculatedEndDateTime).Style.Width = "100%"
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CalcEndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      .FieldDisplay.Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                    With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CalcEndTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      .FieldDisplay.Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewStartDateTime).Style.Width = "100%"
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                    With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewEndDateTime).Style.Width = "100%"
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                    With .Helpers.Bootstrap.TimeEditorFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Style.TextAlign = Singular.Web.TextAlign.center
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewChangeDetails).Style.Width = "100%"
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.CrewChangeDetails, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Timesheets.OBCity.CrewTimesheet)(mCurrentCrewTimesheetFunction)
            With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success,
                                           , Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                           Singular.Web.PostBackType.None,
                                           mSaveCurrentTimesheetFunction)
              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.IsValid()")
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace