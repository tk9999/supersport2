﻿Imports OBLib.Timesheets.OBCity
Imports Singular.Web

Namespace Controls

  Public Class ServicesPermFTCTimesheetControl(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IServicesFTCTimesheet)

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'Non Freelancer Timesheet (Perm/FTC)******************************************************************************************************************

      With Helpers.Bootstrap.Row
        With .Helpers.With(Of OBCityTimesheetList.Criteria)("ViewModel.OBCityTimesheetCriteria()")
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
            .AddClass("col-xl-offset-3 col-lg-offset-3")
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.Bootstrap.LabelFor(Function(d As OBCityTimesheetList.Criteria) d.HumanResourceID)
                .Style.Width = "100%"
              End With
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBCityTimesheetList.Criteria) d.HumanResourceID, BootstrapEnums.InputSize.Small, , "Staff Member")
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.Bootstrap.LabelDisplay("Month")
                .Style.Width = "100%"
              End With
              With .Helpers.Bootstrap.FormControlFor(Function(d) d.TimesheetMonthID, BootstrapEnums.InputSize.Small,, "Month")
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.Bootstrap.LabelDisplay("Recalculate")
                .Style.Width = "100%"
              End With
              With .Helpers.With(Of OBCityTimesheet)("ViewModel.OBCityTimesheet()")
                With .Helpers.Bootstrap.Button(, "Recalculate", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                   "fa-calculator", , Singular.Web.PostBackType.None, "OBCityTimesheetBO.recalculateTimesheet($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 2, 2)
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.With(Of OBCityTimesheet)("ViewModel.OBCityTimesheet()")
                With .Helpers.Bootstrap.LabelDisplay("Print Timesheet")
                  .Style.Width = "100%"
                End With
                With .Helpers.Bootstrap.Button(, "Print", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                  "fa-print", , Singular.Web.PostBackType.None, "OBCityTimesheetBO.printTimesheet($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
        End With
      End With

      With Helpers.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.With(Of OBCityTimesheet)("ViewModel.OBCityTimesheet()")
            With .Helpers.DivC("table-responsive-forced")
              With .Helpers.Bootstrap.TableFor(Of OBLib.Timesheets.OBCity.CrewTimesheet)("$data.CrewTimesheetList()",
                                                                                         False, False, False, False, True, True, False)
                .AddClass("no-border hover list")
                'no-border-row-top no-border-row-bottom no-border-x-top
                .TableBodyClass = "no-border-y no-border-x"
                With .FirstRow
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.LastDayPreviousMonthInd()")
                  .AddClass("items")
                  With .AddColumn("")
                    .Style.Width = "60px"
                    With .Helpers.If(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) Not d.GetParent.TMClosedInd)
                      With .Helpers.Bootstrap.Button(, "Query", Singular.Web.BootstrapEnums.Style.Primary,
                           , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                         "fa-question-circle", , Singular.Web.PostBackType.None,
                         "UserProfilePage.EditCrewTimesheet($data)")
                        .Button.AddBinding(KnockoutBindingString.visible, "CrewTimesheetBO.CanViewField('QueryButton', $data, $element)")
                      End With
                      With .Helpers.Bootstrap.Button(, "Queried", Singular.Web.BootstrapEnums.Style.Warning,
                                                       , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                     "fa-comments-o", , Singular.Web.PostBackType.None,
                                                     "UserProfilePage.EditCrewTimesheet($data)")
                        .Button.AddBinding(KnockoutBindingString.visible, "CrewTimesheetBO.CanViewField('QueriedButton', $data, $element)")
                      End With
                    End With
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.AccountingDate, 0, , "center")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.DayOfWeek, 0, , "center")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddColumn("PH?")
                    .Style.Width = "50px"
                    .HeaderStyle.Width = 50
                    With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.PublicHolidayInd,
                                                          "Yes", "No", , , "", "", )
                    End With
                  End With
                  With .AddColumn("Description", , , , , "left")
                    .Attributes("width") = "320px"
                    With .Helpers.DivC("flex-div")
                      With .Helpers.DivC("overflow-text-div")
                        .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.[Description])
                        .AddBinding(Singular.Web.KnockoutBindingString.title, Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.Description)
                      End With
                    End With
                    .HeaderStyle.Width = 320
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                  End With
                  'With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.StartDate, 0, , "center")
                  '  .Style.Width = "80px"
                  '  .HeaderStyle.Width = 80
                  '  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                  'End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.StartTime, 0, , "center")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                  End With
                  'With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.EndDate, 0, , "center")
                  '  .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                  'End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.EndTime, 0, , "center")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.HoursForDay, 0, , "right")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.HoursForMonth, 0, , "right")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                    .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.css, "CrewTimesheetBO.GetHoursForMonthCss($data)")
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.ShortfallHoursString, 0, , "right")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                    .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.css, "CrewTimesheetBO.GetShortfallCss($data)")
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.SnTForDayString, 0, , "right")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                  End With
                  With .AddReadOnlyColumn(Function(d As OBLib.Timesheets.OBCity.CrewTimesheet) d.MealsString, 0, , "right")
                    .Style.Width = "80px"
                    .HeaderStyle.Width = 80
                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.right
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace