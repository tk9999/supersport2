﻿Imports Singular.Web
Imports OBLib.HR
Imports OBLib.Helpers

Namespace Controls

  Public Class ApplyResultModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = "ApplyResultModal"
    Private mTableID As String = "ApplyMessageList"
    Private mControlInstanceName As String = "ApplyModalControl"

    Public Sub New(Optional ModalID As String = "ApplyResultModal",
                   Optional TableID As String = "ApplyMessageList",
                   Optional ControlInstanceName As String = "ApplyModalControl")
      mModalID = ModalID
      mTableID = TableID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Apply", False, "modal-sm", Singular.Web.BootstrapEnums.Style.Primary, , "fa-hourglass-start", "fa-2x")
        With .Body
          With .Helpers.Bootstrap.Row
            .AddBinding(KnockoutBindingString.visible, "!$data.IsApplying()")
            With .Helpers.ForEach(Of ApplyMessage)("ViewModel.ApplyMessageList()")
              With .Helpers.Div
                .AddBinding(KnockoutBindingString.css, "$data.AlertCss()")
                With .Helpers.HTMLTag("span")
                  With .Helpers.HTMLTag("i")
                    .AddBinding(KnockoutBindingString.css, "$data.IconCssClass()")
                  End With
                End With
                With .Helpers.HTMLTag("span")
                  With .Helpers.HTMLTag("strong")
                    .AddBinding(KnockoutBindingString.html, "$data.Title()")
                  End With
                End With
                With .Helpers.HTMLTag("span")
                  .AddBinding(KnockoutBindingString.html, "$data.LongDescription()")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsApplying()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
