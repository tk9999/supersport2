﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceSecondmentsTab(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mTableID As String = "ROHumanResourceSecondmentList"
  Private mEditSecondment As String = "onSecondmentSelected"
  Private mPagingManager As String = "ViewModel.ROHumanResourceSecondmentListManager"
  Private mListName As String = "ViewModel.ROHumanResourceSecondmentList()"
  Private mRowCssClass As String = ""

  Public Sub New(TableID As String,
                 EditSecondment As String,
                 Optional PagingManager As String = "ViewModel.ROHumanResourceSecondmentListManager",
                 Optional ListName As String = "ViewModel.ROHumanResourceSecondmentList()",
                 Optional RowCssClass As String = "")
    mTableID = TableID
    mEditSecondment = EditSecondment
    mPagingManager = PagingManager
    mListName = ListName
    mRowCssClass = RowCssClass
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.Row
      .AddClass("no-margin-horizontal")
      With .Helpers.DivC("pull-left")
        With .Helpers.Bootstrap.Button(, "New", BootstrapEnums.Style.Primary, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                      PostBackType.None, "HumanResourcesPage.newSecondment($data)", )
        End With
      End With
      With .Helpers.DivC("pull-right")
        With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-trash", ,
                                      PostBackType.None, "HumanResourcesPage.deleteSelectedSecondments($data)", )
        End With
      End With
    End With
    With Helpers.Bootstrap.Row
      .AddClass("no-margin-horizontal")
      .Helpers.Control(New ROHumanResourceSecondmentsPagedGrid(Of VMType)(mTableID, mEditSecondment, mPagingManager, mListName, mRowCssClass))
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class

'      With .AddTab("Secondments", "fa-refresh", "", "Secondments", )
'        With .TabPane
'          .AddClass("cont")
'          With .Helpers.Bootstrap.Row
'            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
'              With .Helpers.DivC("table-responsive")
'                With .Helpers.Bootstrap.TableFor(Of HumanResourceSecondment)(Function(d) ViewModel.CurrentHumanResource.HumanResourceSecondmentList,
'                                                                           True, False, False, False, True, True, True)
'                  .AddClass("HumanResourceSecondmentList")
'                  .AddClass("no-border hover list")
'                  .TableBodyClass = "no-border-y"
'                  With .FirstRow.AddColumn("Start Date")
'                    With .Helpers.EditorFor(Function(c) c.FromDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Time")
'                    With .Helpers.TimeEditorFor(Function(c) c.FromDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("End Date")
'                    With .Helpers.EditorFor(Function(c) c.ToDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Time")
'                    With .Helpers.TimeEditorFor(Function(c) c.ToDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Description")
'                    With .Helpers.EditorFor(Function(c) c.Description)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn(Function(c As HumanResourceSecondment) c.CountryID)
'                  End With
'                  With .FirstRow.AddColumn(Function(c As HumanResourceSecondment) c.DebtorID)
'                  End With
'                  With .FirstRow.AddColumn(Function(c As HumanResourceSecondment) c.CostCentreID)
'                  End With
'                  With .FirstRow.AddColumn("Confirmed")
'                    .Style.TextAlign = Singular.Web.TextAlign.center
'                    With .Helpers.Bootstrap.StateButton(Function(c) c.ConfirmedInd, "Yes", "No", "btn-success", "btn-danger", , , )
'                      .Button.Style.VerticalAlign = Singular.Web.VerticalAlign.middle
'                      .Button.AddBinding(KnockoutBindingString.click, "HumanResourceJS.ConfirmIndClick($data)")
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Authorised")
'                    .Style.TextAlign = Singular.Web.TextAlign.center
'                    With .Helpers.Bootstrap.StateButton(Function(c) c.AuthorisedInd, "Yes", "No", "btn-success", "btn-danger", , , )
'                      .Button.Style.VerticalAlign = Singular.Web.VerticalAlign.middle
'                      .Button.AddBinding(KnockoutBindingString.click, "HumanResourceJS.AuthorisedIndClick($data)")
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("")
'                    .Style.TextAlign = Singular.Web.TextAlign.center
'                    With .Helpers.Bootstrap.Button("", "", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash-o", , , "HumanResourceJS.RemoveSecondment($data)")
'                      .Button.Style.VerticalAlign = Singular.Web.VerticalAlign.middle
'                    End With
'                  End With
'                End With
'              End With
'            End With
'          End With
'        End With
'      End With