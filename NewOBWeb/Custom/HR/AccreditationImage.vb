﻿
Namespace Controls


  Public Class AccreditationImage
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of Object)

    Public Property ImageType As Integer

    Public Sub New(ImageType As OBLib.CommonData.Enums.ImageType)
      Me.ImageType = ImageType
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.DivC("fileinput fileinput-new")
        .Attributes("data-provides") = "fileinput"
        With .Helpers.Div
          .Style.Width = "100%"
          With .Helpers.DivC("fileinput-preview thumbnail")
            .Attributes("data-trigger") = "fileinput"
            .AddBinding(Singular.Web.KnockoutBindingString.style, " { backgroundImage: HumanResourceBO.AccreditationPhotoStyle($data), width: '320px', height: '240px', backgroundRepeat: 'no-repeat', backgroundSize: 'contain' }")
            With .Helpers.DivC("loading-custom")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsLoadingImage()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", )
            End With
          End With
        End With
        With .Helpers.Div
          .Style.Width = "100%"
          With .Helpers.Bootstrap.Button(, "Select Image", Singular.Web.BootstrapEnums.Style.Primary, ,
                                         Singular.Web.BootstrapEnums.ButtonSize.Small, , , ,
                                         Singular.Web.PostBackType.None, "ChooseImage($data," & ImageType.ToString & ")")
            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsNew()")
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace