﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceTBCGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IProductionHumanResourceTBCGrid)

  Private mTableID As String = "ProductionHumanResourceTBCList"
  Private mControlInstanceName As String = "PHRTBC"

  Public Sub New(TableID As String,
                 ControlInstanceName As String)
    mTableID = TableID
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.Button(, "Save TBC Productions", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, ,
                                  "fa-floppy-o", , PostBackType.None, mControlInstanceName & ".saveInHRScreen()")
      .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".isListValid()")
      .Button.AddClass("shiny")
    End With
    With Helpers.DivC("table-responsive")
      With .Helpers.Bootstrap.TableFor(Of ProductionHumanResourcesTBC)("ViewModel.ProductionHumanResourceTBCList()",
                                                                      False, False, False,
                                                                      False, True, True, True)
        .Attributes("id") = mTableID
        .AddClass("no-border hover list")
        .TableBodyClass = "no-border-y no-border-x"
        With .FirstRow
          .AddReadOnlyColumn(Function(c As ProductionHumanResourcesTBC) c.ProductionDescription)
          With .AddColumn("TBC?")
            .Helpers.Bootstrap.ROStateButton(Function(c As ProductionHumanResourcesTBC) c.TBCInd, "Yes", "No", "btn-warning", , , "fa-minus", )
          End With
          With .AddColumn("Is Available?")
            With .Helpers.Bootstrap.ButtonGroup
              With .Helpers.Bootstrap.Button(, , , , , , "fa-minus", , Singular.Web.PostBackType.None, "ProductionHumanResourceTBCBO.SetAvailable($data)")
                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "ProductionHumanResourceTBCBO.IsAvailableText($data)")
                .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "ProductionHumanResourceTBCBO.IsAvailableIcon($data)")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "ProductionHumanResourceTBCBO.IsAvailableButtonCss($data)")
              End With
              With .Helpers.Bootstrap.Button(, , , , , , "fa-minus", , Singular.Web.PostBackType.None, "ProductionHumanResourceTBCBO.SetNotAvailable($data)")
                .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "ProductionHumanResourceTBCBO.IsNotAvailableText($data)")
                .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "ProductionHumanResourceTBCBO.IsNotAvailableIcon($data)")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "ProductionHumanResourceTBCBO.IsNotAvailableButtonCss($data)")
              End With
            End With
          End With
        End With
      End With
      With Helpers.DivC("loading-custom")
        .AddBinding(KnockoutBindingString.visible, mControlInstanceName & ".isBusy()")
        .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-2x")
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
