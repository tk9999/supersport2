﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.HR

Namespace Controls

  Public Class EditHRSkillModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditHRSkill)

    Private mModalID As String
    Private mControlInstanceName As String

    Public Sub New(ModalID As String,
                   ControlInstanceName As String)
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Skill", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-video-camera", "fa-2x", False)
        .Heading.AddBinding(KnockoutBindingString.html, mControlInstanceName & ".headingText()")
        '.ModalDialogDiv.AddBinding(KnockoutBindingString.css, mControlInstanceName & ".modalCss()")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of HumanResourceSkill)("ViewModel.CurrentHumanResourceSkill()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4, 3)
                With .Helpers.Bootstrap.FlatBlock("Skill")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSkill) d.DisciplineID).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkill) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSkill) d.PrimaryInd).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSkill) d.PrimaryInd, , , , , , , "btn-sm")
                            .Button.AddClass("btn-block")
                            .Button.AddBinding(KnockoutBindingString.enable, "$data.CanChangePrimaryIndicator()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSkill) d.StartDate).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkill) d.StartDate, BootstrapEnums.InputSize.Small, , "Start Date")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSkill) d.EndDate).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkill) d.EndDate, BootstrapEnums.InputSize.Small, , "End Date")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 8, 8, 9)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , BootstrapEnums.TabAlignment.Left)
                  With .AddTab("PositionsTab", "fa-", , "Positions", )
                    With .TabPane
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.FieldSet("Positions")
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "New Position", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , PostBackType.None, mControlInstanceName & ".newPosition($data)", )
                            End With
                          End With
                          With .Helpers.DivC("table-responsive")
                            With .Helpers.Bootstrap.TableFor(Of HumanResourceSkillPosition)("$data.HumanResourceSkillPositionList()", False, False, False, False, True, True, False)
                              With .FirstRow
                                With .AddColumn(Function(d As HumanResourceSkillPosition) d.StartDate)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillPosition) d.ProductionAreaID)
                                End With
                                'With .AddColumn(Function(d As HumanResourceSkillPosition) d.PositionTypeID)
                                'End With
                                With .AddColumn(Function(d As HumanResourceSkillPosition) d.PositionID)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillPosition) d.ProductionTypeID)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillPosition) d.RoomTypeID)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillPosition) d.RoomID)
                                End With
                                With .AddColumn("")
                                  .Style.Width = "50px"
                                  .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                                                            BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                            "fa-trash", , PostBackType.None, "HumanResourceSkillPositionBO.deleteSkillPosition($data)")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .AddTab("RatesTab", "fa-dollar", , "Rates", )
                    .TabHeader.AddBinding(KnockoutBindingString.if, "$data.CurrentUserIsHRManager()")
                    With .TabPane
                      .AddBinding(KnockoutBindingString.if, "$data.CurrentUserIsHRManager()")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.FieldSet("Rates")
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.Button(, "New Rate", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, ,
                                                           "fa-plus-circle", , PostBackType.None,
                                                           mControlInstanceName & ".newRate($data)", )
                            End With
                          End With
                          With .Helpers.DivC("table-responsive")
                            With .Helpers.Bootstrap.TableFor(Of HumanResourceSkillRate)("$data.HumanResourceSkillRateList()", False, False, False, False, True, True, False)
                              With .FirstRow
                                With .AddColumn(Function(d As HumanResourceSkillRate) d.RateDate, 100)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillRate) d.RatePerDay, 100)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillRate) d.ProductionAreaID, 100)
                                End With
                                With .AddColumn(Function(d As HumanResourceSkillRate) d.Comments)
                                End With
                                With .AddColumn("")
                                  .Style.Width = "50px"
                                  .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, ,
                                                            BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                            "fa-trash", , PostBackType.None, "HumanResourceSkillRateBO.deleteRate($data)")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of HumanResourceSkill)("ViewModel.CurrentHumanResourceSkill()")
            'With .Helpers.DivC("pull-left")
            '  With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
            '                                 PostBackType.None, mControlInstanceName & ".cancelSkillModal($data)", False)
            '  End With
            'End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                             PostBackType.None, mControlInstanceName & ".save($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
              'With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
              '                               PostBackType.None, mControlInstanceName & ".saveAndNew($data)", True)
              '  .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              'End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace