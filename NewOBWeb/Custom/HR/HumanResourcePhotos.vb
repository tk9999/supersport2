﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourcePhotos(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(ControlInstanceName As String)
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.With(Of OBLib.HR.HumanResource)(mControlInstanceName & ".currentHumanResource()")
      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
        With .Helpers.FieldSet("Accreditation Photo")
          .Helpers.Control(New Controls.AccreditationImage(OBLib.CommonData.Enums.ImageType.AccreditationPhoto))
        End With
      End With
      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
        With .Helpers.FieldSet("Internal Staff Photo")
          With .Helpers.DivC("fileinput fileinput-new")
            .Attributes("data-provides") = "fileinput"
            With .Helpers.Div
              .Style.Width = "100%"
              With .Helpers.DivC("fileinput-preview thumbnail")
                .Attributes("data-trigger") = "fileinput"
                .AddBinding(Singular.Web.KnockoutBindingString.style, " { backgroundImage: HumanResourceBO.internalStaffPhotoStyle($data), width: '130px', height: '130px', backgroundRepeat: 'no-repeat', backgroundSize: 'contain' }")
                With .Helpers.DivC("loading-custom")
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsLoadingImage()")
                  .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", )
                End With
              End With
            End With
            With .Helpers.Div
              .Style.Width = "100%"
              With .Helpers.Bootstrap.Button(, "Select Image", Singular.Web.BootstrapEnums.Style.Primary, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Small, , , ,
                                             Singular.Web.PostBackType.None, "HumanResourceBO.chooseInternalStaffPhoto($data)")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsNew()")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsNew()")
              End With
            End With
          End With
        End With
      End With
    End With

  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
