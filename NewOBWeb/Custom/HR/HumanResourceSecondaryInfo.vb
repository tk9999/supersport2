﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceSecondaryInfo(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(ControlInstanceName As String)
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.With(Of OBLib.HR.HumanResource)(mControlInstanceName & ".currentHumanResource()")
      With .Helpers.Bootstrap.FlatBlock("Secondary Info")
        .FlatBlockTag.AddClass("animated slideInUp fast go")
        With .ContentTag
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.InvoiceSupplierInd).Style.Width = "100%"
                With .Helpers.Bootstrap.StateButton(Function(d As OBLib.HR.HumanResource) d.InvoiceSupplierInd,
                                                    "Yes", "No", "btn-primary", "btn-danger", , , "btn-sm")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.SupplierID).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.SupplierID, BootstrapEnums.InputSize.Small, , "Supplier...")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.PayHalfMonthInd).Style.Width = "100%"
                With .Helpers.Bootstrap.StateButton(Function(d As OBLib.HR.HumanResource) d.PayHalfMonthInd,
                                                        "Yes", "No", "btn-primary", "btn-danger", , , "btn-sm")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.LicenceInd).Style.Width = "100%"
                With .Helpers.Bootstrap.InputGroup
                  .Helpers.Bootstrap.InputGroupAddOnStateButton(Function(d As OBLib.HR.HumanResource) d.LicenceInd, , , "btn-success", "btn-warning", , , "btn-sm")
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.LicenceExpiryDate, BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ActiveInd).Style.Width = "100%"
                With .Helpers.Bootstrap.InputGroup
                  .Helpers.Bootstrap.InputGroupAddOnStateButton(Function(d As OBLib.HR.HumanResource) d.ActiveInd, , , "btn-success", "btn-warning", , , "btn-sm")
                  .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.InactiveReason, BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.VendorNumber).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.VendorNumber, BootstrapEnums.InputSize.Small, , "Vendor Number")
                End With
              End With
            End With
          End With
        End With
      End With
    End With

  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class