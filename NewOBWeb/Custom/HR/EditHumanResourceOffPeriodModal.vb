﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.HR
Imports OBLib.HR.ReadOnly

Namespace Controls

  Public Class EditHumanResourceOffPeriodModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditHumanResourceOffPeriod)

    Private mModalID As String
    Private mControlInstanceName As String

    Public Sub New(ModalID As String,
                   ControlInstanceName As String)
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Leave Application", False, "modal-md", BootstrapEnums.Style.Primary, , "fa-coffee", "fa-2x", False)
        .ModalDialogDiv.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.modalCss(" & mControlInstanceName & ".currentOffPeriod()" & ")")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of HumanResourceOffPeriod)(mControlInstanceName & ".currentOffPeriod()")
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.visible, "!$data.SetupCompleted()")
              With .Helpers.Bootstrap.FlatBlock(, , True, )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.OffReasonID).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.OffReasonID, BootstrapEnums.InputSize.Small, , "Leave Type")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, BootstrapEnums.InputSize.Small, , "Date")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, BootstrapEnums.InputSize.Small, , "Date")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            '/************************************* Editable View
            With .Helpers.Bootstrap.Row
              .AddBinding(KnockoutBindingString.visible, "$data.SetupCompleted()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , )
                  With .AddTab("OffPeriod", "", , "Leave Application", , True)
                    With .TabPane
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.OffReasonID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.OffReasonID, BootstrapEnums.InputSize.Small, , "Leave Type")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'OffReasonID')")
                        End With
                      End With

                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .AddBinding(KnockoutBindingString.visible, "HumanResourcesPage.isPlayoutsUser()")
                        .Helpers.Bootstrap.LabelFor(Function(d) d.IsForNegativeHours).Style.Width = "100%"
                        With .Helpers.Bootstrap.StateButton(Function(d) d.IsForNegativeHours, "Yes", "No", "btn-warning", , , , "btn-sm")
                          .Button.AddClass("btn-block")
                        End With
                      End With

                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, BootstrapEnums.InputSize.Small, , "Date")
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                            With .Helpers.Bootstrap.TimeEditorFor(Function(d) d.StartDate, BootstrapEnums.InputSize.Small, , "Time")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'StartDate')")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 7, 7, 7)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, BootstrapEnums.InputSize.Small, , "Date")
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 5, 5, 5)
                            With .Helpers.Bootstrap.TimeEditorFor(Function(d) d.EndDate, BootstrapEnums.InputSize.Small, , "Time")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'EndDate')")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.Detail)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.Detail, BootstrapEnums.InputSize.Small, , "Detail")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceOffPeriodBO.CanEditField($data, 'Detail')")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d) d.AuthorisedID)
                        With .Helpers.Bootstrap.InputGroup(BootstrapEnums.InputGroupSize.Small)
                          With .Helpers.Bootstrap.Button(, "Pending", BootstrapEnums.Style.Custom, "", BootstrapEnums.ButtonSize.Small, , "fa-minus", , PostBackType.None, "HumanResourceOffPeriodBO.pending($data)")
                            .Icon.IconContainer.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.pendingCss($data)")
                            .Button.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.pendingButtonCss($data)")
                          End With
                          With .Helpers.Bootstrap.Button(, "Authorised", BootstrapEnums.Style.Custom, "", BootstrapEnums.ButtonSize.Small, , "fa-minus", , PostBackType.None, "HumanResourceOffPeriodBO.authorise($data)")
                            .Icon.IconContainer.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.authoriseCss($data)")
                            .Button.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.authoriseButtonCss($data)")
                            .Button.AddBinding(KnockoutBindingString.enable, "$data.CanAuthoriseLeave()")
                          End With
                          With .Helpers.Bootstrap.Button(, "Rejected", BootstrapEnums.Style.Custom, "", BootstrapEnums.ButtonSize.Small, , "fa-minus", , PostBackType.None, "HumanResourceOffPeriodBO.rejected($data)")
                            .Icon.IconContainer.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.rejectedCss($data)")
                            .Button.AddBinding(KnockoutBindingString.css, "HumanResourceOffPeriodBO.rejectedButtonCss($data)")
                            .Button.AddBinding(KnockoutBindingString.enable, "$data.CanAuthoriseLeave()")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                            .Helpers.Bootstrap.LabelFor(Function(d) d.AuthorisedByName)
                            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d) d.AuthorisedByName, BootstrapEnums.InputSize.Small)
                              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceJS.isLeaveAuthorised()")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                            .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResourceOffPeriod) d.AuthorisedDateString)
                            With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.HR.HumanResourceOffPeriod) d.AuthorisedDateString, BootstrapEnums.InputSize.Small)
                              '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "HumanResourceJS.isLeaveAuthorised()")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .AddTab("OffPeriodDays", "", , "Leave Days", , )
                    With .TabPane
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.HumanResourceOffPeriodDay)("$data.HumanResourceOffPeriodDayList()", False, False, False, False, True, True, True, "HumanResourceOffPeriodDayList")
                          .AllowClientSideSorting = True
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y no-border-x"
                          .Attributes("id") = "HumanResourceOffPeriodDayList"
                          With .FirstRow
                            .AddClass("items selectable")
                            With .AddReadOnlyColumn(Function(d As OBLib.HumanResourceOffPeriodDay) d.TimesheetDate)
                              .Style.TextAlign = TextAlign.center
                            End With
                            With .AddColumn(Function(d As OBLib.HumanResourceOffPeriodDay) d.StartDateTime)
                              .Style.TextAlign = TextAlign.center
                              .Editor.Style.TextAlign = TextAlign.center
                            End With
                            With .AddColumn(Function(d As OBLib.HumanResourceOffPeriodDay) d.EndDateTime)
                              .Style.TextAlign = TextAlign.center
                              .Editor.Style.TextAlign = TextAlign.center
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                'With .Helpers.Bootstrap.FlatBlock(, , True, )
                '  With .ContentTag
                '    With .Helpers.Bootstrap.Row
                '      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)

                '      End With
                '      '.AddBinding(Singular.Web.KnockoutBindingString.visible, mControlInstanceName & ".isUnAuthorisedLeave()")

                '    End With
                '  End With
                'End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                'With .Helpers.Bootstrap.FlatBlock("Leave Days", , , )
                '  With .ContentTag
                '    With .Helpers.Bootstrap.Row
                '      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)

                '      End With
                '    End With
                '  End With
                'End With
                With .Helpers.Bootstrap.FlatBlock("Others On Leave", , , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave)("$data.ROOtherDisciplinesOnLeaveList()", False, False, False, False, True, True, True, "ROOtherDisciplinesOnLeaveList")
                            .AllowClientSideSorting = True
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            .Attributes("id") = "ROOtherDisciplinesOnLeaveList"
                            With .FirstRow
                              .AddClass("items selectable")
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave) d.Discipline)
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave) d.HumanResource)
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave) d.OffReason)
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave) d.LeaveDetail)
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave) d.StartDate)
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeave) d.EndDate)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.FlatBlock("Clash Warnings", True, , )
                  '.HeaderTag.AddBinding(KnockoutBindingString.html, "HumanResourceOffPeriodBO.clashHeaderHtml($data)")
                  With .AboveContentTag
                    With .Helpers.HTMLTag("div")
                      .AddClass("alert alert-danger animated pulse infinite go")
                      .AddBinding(KnockoutBindingString.visible, "$data.ROOffPeriodClashList().length > 0")
                      .AddBinding(KnockoutBindingString.html, "HumanResourceOffPeriodBO.clashHtml($data)")
                    End With
                    With .Helpers.HTMLTag("div")
                      .AddClass("alert alert-success")
                      .AddBinding(KnockoutBindingString.visible, "$data.ROOffPeriodClashList().length == 0")
                      .AddBinding(KnockoutBindingString.html, "HumanResourceOffPeriodBO.clashHtml($data)")
                    End With
                  End With
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of OBLib.HR.ReadOnly.ROOffPeriodClash)("$data.ROOffPeriodClashList()", False, False, False, False, True, True, True, "ROOffPeriodClashList")
                            .AllowClientSideSorting = True
                            .AddClass("no-border hover list")
                            .TableBodyClass = "no-border-y no-border-x"
                            .Attributes("id") = "ROOffPeriodClashList"
                            With .FirstRow
                              .AddClass("items selectable")
                              .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOffPeriodClash) d.ResourceBookingDescription)
                              With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOffPeriodClash) d.StartDateTimeBuffer)
                                .HeaderText = "Call Time"
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOffPeriodClash) d.StartDateTime)
                                .HeaderText = "Start Time"
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOffPeriodClash) d.EndDateTime)
                                .HeaderText = "End Time"
                              End With
                              With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROOffPeriodClash) d.EndDateTimeBuffer)
                                .HeaderText = "Wrap Time"
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.With(Of HumanResourceOffPeriod)(mControlInstanceName & ".currentOffPeriod()")
              With .Helpers.DivC("loading-custom")
                .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
                .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
              End With
            End With
          End With

          '/************************************* ReadOnly View
          With .Helpers.With(Of ROHumanResourceOffPeriod)(mControlInstanceName & ".currentROOffPeriod()")
            With .Helpers.Bootstrap.FlatBlock(, , True, )
              '.FlatBlockTag.AddBinding(KnockoutBindingString.visible, "$data.SetupCompleted()")
              With .ContentTag
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    '.AddBinding(Singular.Web.KnockoutBindingString.visible, mControlInstanceName & ".isUnAuthorisedLeave()")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.OffReason)
                      With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d) d.OffReason, BootstrapEnums.InputSize.Small, , "Leave Type")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROHumanResourceOffPeriod) d.StartDateDisplay, BootstrapEnums.InputSize.Small, , "")
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROHumanResourceOffPeriod) d.EndDateDisplay, BootstrapEnums.InputSize.Small, , "")
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.Detail)
                      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROHumanResourceOffPeriod) d.Detail, BootstrapEnums.InputSize.Small, , "")
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.AuthStatus)
                      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROHumanResourceOffPeriod) d.AuthStatus, BootstrapEnums.InputSize.Small, , "")
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.AuthDetails)
                      .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As ROHumanResourceOffPeriod) d.AuthDetails, BootstrapEnums.InputSize.Small, , "")
                    End With
                  End With
                End With
              End With
            End With
          End With

        End With
        With .Footer
          With .Helpers.With(Of HumanResourceOffPeriod)(mControlInstanceName & ".currentOffPeriod()")
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                             PostBackType.None, mControlInstanceName & ".save($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
              With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Medium, , "fa-thumbs-o-up", ,
                                             PostBackType.None, mControlInstanceName & ".saveAndNew($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace