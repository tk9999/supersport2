﻿Imports Singular.Web
Imports OBLib.HR

Namespace Controls

  Public Class HRDuplicatesModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = "ROHRDuplicatesModal"
    Private mControlInstanceName As String = "CurrentHumanResourceControl"

    Public Sub New(Optional ModalID As String = "ROHRDuplicatesModal",
                   Optional ControlInstanceName As String = "CurrentHumanResourceControl")
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Detected Duplicates", False, "modal-md", Singular.Web.BootstrapEnums.Style.Danger, , "fa-bomb", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock(, , True, )
                With .ContentTag
                  With .Helpers.DivC("table-responsive")
                    With .Helpers.With(Of HumanResource)(mControlInstanceName & ".currentHumanResource()")
                      With .Helpers.Bootstrap.TableFor(Of ROHRDuplicate)("$data.ROHRDuplicateList()",
                                                                         False, False, False,
                                                                         False, True, True, False)
                        .AddClass("no-border hover list")
                        .TableBodyClass = "no-border-y no-border-x"
                        With .FirstRow
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.FirstName)
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.Surname)
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.EmployeeCode)
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.System)
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.ProductionArea)
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.MatchType)
                          .AddReadOnlyColumn(Function(c As ROHRDuplicate) c.BookingCount)
                          With .AddColumn(Function(c As ROHRDuplicate) c.SystemID)
                            .Editor.AddBinding(KnockoutBindingString.enable, "$data.Link()")
                          End With
                          With .AddColumn(Function(c As ROHRDuplicate) c.ProductionAreaID)
                            .Editor.AddBinding(KnockoutBindingString.enable, "$data.Link()")
                          End With
                          With .AddColumn("Actions")
                            .Style.Width = "500px"
                            With .Helpers.Bootstrap.StateButton(Function(d As ROHRDuplicate) d.Link, "Link", "Link", , , "fa-chain", "fa-chain", )
                              .Button.AddBinding(KnockoutBindingString.enable, "!$data.IsDuplicate() && !$data.IsNotSameHR()")
                              .Button.Style.Width = "75px"
                            End With
                            With .Helpers.Bootstrap.StateButton(Function(d As ROHRDuplicate) d.IsDuplicate, "Flag for Removal", "Flag for Removal", , , "fa-flag-checkered", "fa-flag-checkered", )
                              .Button.AddBinding(KnockoutBindingString.enable, "!$data.Link() && !$data.IsNotSameHR()")
                              .Button.Style.Width = "150px"
                            End With
                            With .Helpers.Bootstrap.StateButton(Function(d As ROHRDuplicate) d.IsNotSameHR, "Invalid Clash", "Invalid Clash", , , "fa-minus", "fa-minus", )
                              .Button.AddBinding(KnockoutBindingString.enable, "!$data.Link() && !$data.IsDuplicate()")
                              .Button.Style.Width = "150px"
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.DivC("pull-right")
            With .Helpers.With(Of HumanResource)(mControlInstanceName & ".currentHumanResource()")
              With .Helpers.Bootstrap.Button(, "Submit", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-gears", , PostBackType.None, mControlInstanceName & ".submitDuplicates($data)")
                .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".allDuplicatesValid($data)")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace
