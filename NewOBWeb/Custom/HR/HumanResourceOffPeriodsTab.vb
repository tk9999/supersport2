﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceOffPeriodsTab(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mTableID As String = "ROHumanResourceOffPeriodList"
  Private mOnOffPeriodSelected As String = "onOffPeriodSelected"
  Private mPagingManager As String = "ViewModel.ROHumanResourceOffPeriodListManager"
  Private mListName As String = "ViewModel.ROHumanResourceOffPeriodList()"
  Private mRowCssClass As String = ""
  Private mOnOffPeriodUnAuthorised As String

  Public Sub New(TableID As String,
                 OffPeriodSelected As String,
                 Optional PagingManager As String = "ViewModel.ROHumanResourceOffPeriodListManager",
                 Optional ListName As String = "ViewModel.ROHumanResourceOffPeriodList()",
                 Optional RowCssClass As String = "",
                 Optional OnOffPeriodUnAuthorised As String = "HumanResourcesPage.onOffPeriodUnAuthorised")
    mTableID = TableID
    mOnOffPeriodSelected = OffPeriodSelected
    mPagingManager = PagingManager
    mListName = ListName
    mRowCssClass = RowCssClass
    mOnOffPeriodUnAuthorised = OnOffPeriodUnAuthorised
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.Row
      .AddClass("no-margin-horizontal")
      With .Helpers.DivC("pull-left")
        With .Helpers.Bootstrap.Button(, "New Off Period", BootstrapEnums.Style.Primary, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                      PostBackType.None, "HumanResourcesPage.newOffPeriod($data)", )
          .AddClass("shiny")
        End With
        With .Helpers.Bootstrap.Button(, "Specify UnAvailability", BootstrapEnums.Style.Warning, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-hand-stop-o", ,
                                      PostBackType.None, "HumanResourcesPage.specifyUnAvailability($data)", )
          .AddClass("shiny")
        End With
      End With
      With .Helpers.DivC("pull-right")
        With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-trash", ,
                                      PostBackType.None, "HumanResourcesPage.deleteSelectedOffPeriods($data)", )
          '.Button.AddBinding(KnockoutBindingString.visible, "ViewModel.CurrentHumanResource().CurrentUserIsHRManager()")
        End With
      End With
    End With
    With Helpers.Bootstrap.Row
      .AddClass("no-margin-horizontal")
      With Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourceOffPeriod)(mPagingManager,
                                                                                       mListName,
                                                                                       False, False, False, False, True, True, True,
                                                                                       "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                       , False, True, mRowCssClass)
        .Attributes("id") = mTableID
        .AddClass("no-border hover list")
        .TableBodyClass = "no-border-y no-border-x"
        With .FirstRow
          .AddClass("items")
          With .AddColumn("")
            .Style.Width = "90px"
            With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.IsSelected, "Selected", "Select", , , , , )
              .Button.AddBinding(KnockoutBindingString.visible, "$data.CanEditOffPeriod()")
            End With
            If mOnOffPeriodSelected <> "" Then
              With .Helpers.Bootstrap.Button(, "Edit", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, mOnOffPeriodSelected & "($data)", )
                .Button.AddBinding(KnockoutBindingString.visible, "$data.CanEditOffPeriod()")
              End With
            End If
            If mOnOffPeriodUnAuthorised <> "" Then
              With .Helpers.Bootstrap.Button(, "UnAuthorise", BootstrapEnums.Style.Warning, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-bomb", , PostBackType.None, mOnOffPeriodUnAuthorised & "($data)", )
                .Button.AddBinding(KnockoutBindingString.visible, "($data.CanAuthLeave() && $data.AuthorisedID() == 2)")
                '.Button.AddBinding(KnockoutBindingString.title, "UnAuthorise the leave application")
              End With
            End If
          End With
          With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.StartDateDisplay)

          End With
          With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.EndDateDisplay)

          End With
          With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.OffReason)

          End With
          With .AddColumn("")
            With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.IsAuthorised, "Authorised", "Not Authorised", "btn-success", , , "fa-minus")
              .Button.AddBinding(KnockoutBindingString.visible, "$data.IsAuthorised()")
            End With
            With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.IsPending, "Pending", "Not Pending", "btn-default", , "fa-pause", "fa-minus")
              .Button.AddBinding(KnockoutBindingString.visible, "$data.IsPending()")
            End With
            With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.IsRejected, "Rejected", "Not Rejected", "btn-danger", , "fa-times-circle", "fa-minus")
              .Button.AddBinding(KnockoutBindingString.visible, "$data.IsRejected()")
            End With
          End With
          'With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.AuthStatus)

          'End With
          With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.AuthDetails)

          End With
          'With .AddColumn("")
          '  .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.IsAuthorised, "Authorised", "Not Authorised", "btn-success", , , "fa-minus")
          'End With
          'With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.AuthorisedDetails)

          'End With
          'With .AddColumn("")
          '  .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.IsConfirmed, "Confirmed", "Not Confirmed", "btn-success", , , "fa-minus")
          'End With
          'With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.CreatedByName)

          'End With
          With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceOffPeriod) d.CreateDetails)

          End With
        End With
      End With
      '.Helpers.Control(New ROHumanResourceOffPeriodsPagedGrid(Of VMType)(mTableID, "", mPagingManager, mListName, mRowCssClass, mOnOffPeriodSelected, mOnOffPeriodUnAuthorised))
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class

'      With .AddTab("Secondments", "fa-refresh", "", "Secondments", )
'        With .TabPane
'          .AddClass("cont")
'          With .Helpers.Bootstrap.Row
'            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
'              With .Helpers.DivC("table-responsive")
'                With .Helpers.Bootstrap.TableFor(Of HumanResourceSecondment)(Function(d) ViewModel.CurrentHumanResource.HumanResourceSecondmentList,
'                                                                           True, False, False, False, True, True, True)
'                  .AddClass("HumanResourceSecondmentList")
'                  .AddClass("no-border hover list")
'                  .TableBodyClass = "no-border-y"
'                  With .FirstRow.AddColumn("Start Date")
'                    With .Helpers.EditorFor(Function(c) c.FromDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Time")
'                    With .Helpers.TimeEditorFor(Function(c) c.FromDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("End Date")
'                    With .Helpers.EditorFor(Function(c) c.ToDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Time")
'                    With .Helpers.TimeEditorFor(Function(c) c.ToDate)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Description")
'                    With .Helpers.EditorFor(Function(c) c.Description)
'                    End With
'                  End With
'                  With .FirstRow.AddColumn(Function(c As HumanResourceSecondment) c.CountryID)
'                  End With
'                  With .FirstRow.AddColumn(Function(c As HumanResourceSecondment) c.DebtorID)
'                  End With
'                  With .FirstRow.AddColumn(Function(c As HumanResourceSecondment) c.CostCentreID)
'                  End With
'                  With .FirstRow.AddColumn("Confirmed")
'                    .Style.TextAlign = Singular.Web.TextAlign.center
'                    With .Helpers.Bootstrap.StateButton(Function(c) c.ConfirmedInd, "Yes", "No", "btn-success", "btn-danger", , , )
'                      .Button.Style.VerticalAlign = Singular.Web.VerticalAlign.middle
'                      .Button.AddBinding(KnockoutBindingString.click, "HumanResourceJS.ConfirmIndClick($data)")
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("Authorised")
'                    .Style.TextAlign = Singular.Web.TextAlign.center
'                    With .Helpers.Bootstrap.StateButton(Function(c) c.AuthorisedInd, "Yes", "No", "btn-success", "btn-danger", , , )
'                      .Button.Style.VerticalAlign = Singular.Web.VerticalAlign.middle
'                      .Button.AddBinding(KnockoutBindingString.click, "HumanResourceJS.AuthorisedIndClick($data)")
'                    End With
'                  End With
'                  With .FirstRow.AddColumn("")
'                    .Style.TextAlign = Singular.Web.TextAlign.center
'                    With .Helpers.Bootstrap.Button("", "", Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash-o", , , "HumanResourceJS.RemoveSecondment($data)")
'                      .Button.Style.VerticalAlign = Singular.Web.VerticalAlign.middle
'                    End With
'                  End With
'                End With
'              End With
'            End With
'          End With
'        End With
'      End With