﻿Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Timesheets
Imports Singular.Web

Namespace Controls

    Public Class ROHRSelectorBootstrap(Of VMType)
        Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).ROHRSelectorBootstrap)
        Private mOnRowSelectFunctionName As String = ""
        Private mOnCommitClicked As String = ""
        Private mDelayedRefreshMethod As String = ""
        Private mModalID As String = ""
        Public Sub New(ModalID As String,
                       Optional OnRowSelectFunctionName As String = "",
                       Optional OnCommitClicked As String = "",
                       Optional DelayedRefreshMethod As String = "")
            mModalID = ModalID
            mOnRowSelectFunctionName = OnRowSelectFunctionName
            mOnCommitClicked = OnCommitClicked
            mDelayedRefreshMethod = DelayedRefreshMethod
        End Sub

        Protected Overrides Sub Setup()
            MyBase.Setup()
            With Helpers.Bootstrap.Dialog(mModalID, "Select Human Resources", ,
                                          "modal-lg", Singular.Web.BootstrapEnums.Style.Info)
                With .Body
                    .AddClass("row modal-bg-gray")
                    'With .Helpers.Bootstrap.Row

                    '    'With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                    '    '    'With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROHRListCriteria.Surname, Singular.Web.BootstrapEnums.InputSize.Small, , "Surname...")
                    '    '    '    .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    '    '    '    .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: TimesheetManagementPage.DelayedROHRRefreshList() }")
                    '    '    'End With
                    '    'End With
                    '    'With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                    '    '    'With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROHRListCriteria.PreferredName, Singular.Web.BootstrapEnums.InputSize.Small, , "Surname...")
                    '    '    '    .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    '    '    '    .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: TimesheetManagementPage.DelayedROHRRefreshList() }")
                    '    '    'End With
                    '    'End With
                    '    'With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
                    '    '    With .Helpers.Bootstrap.StateButton("ROHRListCriteria().ShowMyAreaOnly()",
                    '    '                                        "My Area Only", "All HR", , ,
                    '    '                                        "fa-group", "fa-filter", "btn-sm")
                    '    '    End With
                    '    'End With
                    'End With
                    With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                        .AddClass("col-white")
                        With .Helpers.FieldSet("Find")
                            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ROHRListCriteria.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Keyword...")
                                    If mDelayedRefreshMethod <> "" Then
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: " & mDelayedRefreshMethod & " }")
                                    End If
                                End With
                            End With
                            With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourcePaged)(Function(d) d.ROHRListManager,
                                                                   Function(d) d.ROHRList,
                                                                   False, False, False, False, True, True, True, ,
                                                                   Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                   mOnRowSelectFunctionName, False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                                With .FirstRow
                                    .AddClass("selectable items")
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.Firstname)
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.PreferredName)
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.Surname)
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.Firstname)
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.ContractType)
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.IDNo)
                                    .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResourcePaged) hr.EmailAddress)
                                End With
                            End With
                        End With
                    End With

                    With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                        .AddClass("col-white")
                        With .Helpers.FieldSet("Selected")
                            With .Helpers.Bootstrap.TableFor(Of OBLib.HR.SelectedHR)(Function(d) d.SelectedHumanResourceList,
                                                                                     False, True,
                                                                                     False, False, True, True, False)
                                .AddClass("no-border hover list")
                                .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                                With .FirstRow.AddReadOnlyColumn(Function(c As OBLib.HR.SelectedHR) c.HumanResource)
                                End With
                            End With
                        End With
                    End With

                End With
                With .Footer
                    With .Helpers.DivC("pull-right")
                        With .Helpers.Bootstrap.Button(, "Add Selected", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                       Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-thumbs-o-up", , Singular.Web.PostBackType.None, mOnCommitClicked)
                        End With
                    End With
                End With
            End With
        End Sub

        Protected Overrides Sub Render()
            MyBase.Render()
            RenderChildren()
        End Sub

    End Class

End Namespace
