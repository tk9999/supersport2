﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceDocumentGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mTableID As String = "HumanResourceDocumentList"
  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(TableID As String,
                 ControlInstanceName As String)
    mTableID = TableID
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
      With .Helpers.DivC("pull-left")
        With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", , PostBackType.None, mControlInstanceName & ".saveDocuments()", )
          .Button.AddClass("shiny")
        End With
        With .Helpers.Bootstrap.Button(, "New Document", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , PostBackType.None, mControlInstanceName & ".addNewDocument()", )
          .Button.AddClass("shiny")
        End With
      End With
      With .Helpers.DivC("pull-right")
        With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Small, , "fa-trash", , PostBackType.None, mControlInstanceName & ".deleteSelectedDocuments()", )
          .Button.AddClass("shiny")
        End With
      End With
    End With
    With Helpers.DivC("table-responsive")
      With .Helpers.With(Of HumanResource)(mControlInstanceName & ".currentHumanResource()")
        With .Helpers.Bootstrap.TableFor(Of HRDocument)("ViewModel.HRDocumentList()",
                                                        False, False, False,
                                                        False, True, True, True)
          .Attributes("id") = mTableID
          .AddClass("no-border hover list")
          With .FirstRow
            With .AddColumn("")
              .Helpers.Bootstrap.StateButton(Function(c As HRDocument) c.IsSelected, "Selected", "Select", , , , , )
            End With
            With .AddColumn(Function(c As HRDocument) c.HRDocumentTypeID)
            End With
            With .AddColumn("")
              .Helpers.DocumentManager()
            End With
          End With
        End With
      End With
      With Helpers.DivC("loading-custom")
        .AddBinding(KnockoutBindingString.visible, "ViewModel.CurrentHumanResource().IsProcessing()")
        .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-2x")
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
