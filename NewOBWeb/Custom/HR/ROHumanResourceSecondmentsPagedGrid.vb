﻿Imports Singular.Web
Imports OBLib.HR
Imports OBLib.HR.ReadOnly

Public Class ROHumanResourceSecondmentsPagedGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROHumanResourceSecondmentsPagedGrid)

  Private mTableID As String = "ROHumanResourceSecondmentList"
  Private mEditSecondmentFunction As String = "onOffPeriodSelected"
  Private mPagingManager As String = "ViewModel.ROHumanResourceSecondmentListManager"
  Private mListName As String = "ViewModel.ROHumanResourceSecondmentList()"
  Private mRowCssClass As String = ""

  Public Sub New(TableID As String,
                 EditSecondmentFunction As String,
                 Optional PagingManager As String = "ViewModel.ROHumanResourceSecondmentListManager",
                 Optional ListName As String = "ViewModel.ROHumanResourceSecondmentList()",
                 Optional RowCssClass As String = "")
    mTableID = TableID
    mEditSecondmentFunction = EditSecondmentFunction
    mPagingManager = PagingManager
    mListName = ListName
    mRowCssClass = RowCssClass
  End Sub


  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourceSecondment)(mPagingManager,
                                                                                        mListName,
                                                                                        False, False, False, False, True, True, True,
                                                                                        "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                        "", False, True, mRowCssClass)
      .Attributes("id") = mTableID
      .AddClass("no-border hover list")
      .TableBodyClass = "no-border-y no-border-x"
      With .FirstRow
        .AddClass("items")
        With .AddColumn("")
          .Style.Width = "60px"
          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.IsSelected, "Selected", "Select", , , , "fa-minus", )
          End With
        End With
        With .AddColumn("")
          .Style.Width = "60px"
          With .Helpers.Bootstrap.Button(, "Edit", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, mEditSecondmentFunction & "($data)", )
            '.Button.AddBinding(KnockoutBindingString.visible, "$data.CanAuthLeave()")
          End With
        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.FromDateDisplay)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.ToDateDisplay)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.Country)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.Description)

        End With
        With .AddColumn("")
          .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.IsAuthorised, "Authorised", "Not Authorised", "btn-success", , , "fa-minus")
        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.AuthorisedDetails)

        End With
        With .AddColumn("")
          .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.IsConfirmed, "Confirmed", "Not Confirmed", "btn-success", , , "fa-minus")
        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.ConfirmedDetails)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.CreatedByName)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSecondment) d.CreatedDateDisplay)

        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class