﻿Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Timesheets
Imports Singular.Web
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Security.User
Imports Singular.Rules
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly

Namespace Controls

  Public Class FindHumanResourceModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = ""
    Private mJSControlName As String = ""

    Public Sub New(ModalID As String,
                   JSControlName As String)
      mModalID = ModalID
      mJSControlName = JSControlName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Find Human Resource", False, "modal-xl", BootstrapEnums.Style.Primary, , "fa-user-secret", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
              With .Helpers.Bootstrap.FlatBlock("Criteria")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourcePagedList.Criteria)("ViewModel.ROHumanResourceListCriteria()")
                      With .Helpers.Bootstrap.Column(12, 12, 4, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Keyword)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, BootstrapEnums.InputSize.Small, , "Keyword")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 4, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.ContractTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 4, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Firstname)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Firstname, BootstrapEnums.InputSize.Small, , "Firstname")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 4, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.PreferredName)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 4, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Surname)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 4, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.DisciplineID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.FieldSet("Sub-Depts and Areas")
                            With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                  With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                    .Button.AddBinding(KnockoutBindingString.click, mJSControlName & ".AddSystem($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                                    .Button.AddClass("btn-block")
                                  End With
                                  With .Helpers.Bootstrap.Row
                                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                                        With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                          .Button.AddBinding(KnockoutBindingString.click, mJSControlName & ".AddProductionArea($data)")
                                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                          .Button.AddClass("btn-block")
                                        End With
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With

                    End With
                  End With
                  With .Helpers.DivC("loading-custom")
                    .AddBinding(KnockoutBindingString.visible, "ViewModel.ROHumanResourceListManager().IsLoading()")
                    .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
              With .Helpers.Bootstrap.FlatBlock("Human Resources")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourcePaged)("ViewModel.ROHumanResourceListManager",
                                                                                                  "ViewModel.ROHumanResourceList()",
                                                                                                  False, False, False, False, True, True, True,
                                                                                                  , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                  "", False, True)
                      .AddClass("no-border hover list")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      .Pager.PagerListTag.ListTag.AddClass("pull-right")
                      With .FirstRow
                        .AddClass("items selectable")
                        .AddBinding(Singular.Web.KnockoutBindingString.click, mJSControlName & ".onRowSelected($data)")
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.EmployeeCode)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.PreferredFirstSurname)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.IDNo)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.ContractType)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.EmailAddress)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.CellPhoneNumber)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.PrimaryArea)
                        End With
                        With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourcePaged) c.Skills)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace
