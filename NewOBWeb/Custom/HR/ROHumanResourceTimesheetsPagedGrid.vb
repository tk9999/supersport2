﻿Imports Singular.Web
Imports OBLib.HR
Imports OBLib.HR.Timesheets.ReadOnly

Public Class ROHumanResourceTimesheetsPagedGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IROHumanResourceTimesheetsPagedGrid)

  Private mTableID As String = "HumanResourceTimesheetsPagedList"
  Private mControlName As String = ""

  Public Sub New(TableID As String, ControlName As String)
    mTableID = TableID
    mControlName = ControlName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.PagedGridFor(Of ROHumanResourceTimesheetPaged)("ViewModel.ROHumanResourceTimesheetPagedListManager",
                                                                          "ViewModel.ROHumanResourceTimesheetPagedList",
                                                                          False, False, False, False, True, True, True,
                                                                          "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                          , False, True, "")
      .Attributes("id") = mTableID
      .AddClass("no-border hover list")
      .TableBodyClass = "no-border-y no-border-x"
      With .FirstRow
        With .AddColumn("")
          .Style.Width = "50px"
          With .Helpers.Bootstrap.StateButton(Function(d) d.IsSelected, "Selected", "Select", , , , , )
          End With
        End With
        With .AddColumn("")
          .Style.Width = "50px"
          With .Helpers.Bootstrap.Button(, "Edit", BootstrapEnums.Style.DefaultStyle, ,
                                        BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", ,
                                        PostBackType.None, mControlName & ".editROHumanResourceTimesheetPaged($data)", )
          End With
        End With
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.StartDate)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.EndDate)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.StartingHours)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.RequiredHours)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.TotalHours)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.TotalPublicHolidayHours)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.TotalShortfall)
        .AddReadOnlyColumn(Function(c As ROHumanResourceTimesheetPaged) c.TotalOvertime)
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
