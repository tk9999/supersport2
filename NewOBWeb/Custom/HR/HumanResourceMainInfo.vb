﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceMainInfo(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(ControlInstanceName As String)
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.With(Of OBLib.HR.HumanResource)(mControlInstanceName & ".currentHumanResource()")
      With .Helpers.Bootstrap.FlatBlock("Main Info", False, False, False)
        .FlatBlockTag.AddClass("animated slideInUp fast go")
        With .ContentTag

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Button(, , BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Large, , "fa-bomb", , PostBackType.None, mControlInstanceName & ".showDuplicates($data)")
                .ButtonText.AddBinding(KnockoutBindingString.html, "$data.DuplicateCount().toString() + ' duplicates found'")
                .Button.AddBinding(KnockoutBindingString.css, "'slow rubberBand infinite go btn-block'")
                .Button.AddBinding(KnockoutBindingString.visible, "$data.DuplicateCount() > 0")
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.Firstname)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.Firstname, BootstrapEnums.InputSize.Small, , "First Name")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.Surname)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.SecondName)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.SecondName, BootstrapEnums.InputSize.Small, , "Second Name")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.PreferredName)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PreferredName, BootstrapEnums.InputSize.Small, , "Preferred Name")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.IDNo)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.IDNo, BootstrapEnums.InputSize.Small, , "ID Num")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.NationalityCountyID).Style.Width = "100%"
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.NationalityCountyID, BootstrapEnums.InputSize.Small, , "Nationality...")
                'With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As OBLib.HR.HumanResource) d.Country, "FindCountryControl.show()", "Country", , , "fa-search", True)
                '  .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "FindCountryControl.show()")
                '  .ClearButton.AddOn.AddBinding(KnockoutBindingString.visible, Function(d) False)
                'End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.CityID).Style.Width = "100%"
                .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.CityID, BootstrapEnums.InputSize.Small, , "City...")
                '.Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.City)
                'With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As OBLib.HR.HumanResource) d.City, "FindHomeCityModal.show()", "Select City", , , "fa-search", True)
                '  .AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "FindHomeCityModal.show()")
                '  .ClearButton.AddOn.AddBinding(KnockoutBindingString.visible, Function(d) False)
                'End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.RaceID)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.RaceID, BootstrapEnums.InputSize.Small, ,
                                                      "Race...")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.GenderID)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.GenderID, BootstrapEnums.InputSize.Small, ,
                                                      "Gender...")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.EmailAddress).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.EmailAddress, Singular.Web.BootstrapEnums.InputSize.Small, , "Email")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.AlternativeEmailAddress).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AlternativeEmailAddress, Singular.Web.BootstrapEnums.InputSize.Small, , "Email")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.CellPhoneNumber).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.CellPhoneNumber, Singular.Web.BootstrapEnums.InputSize.Small, , "Cellphone Number")
                  .Editor.AddClass("txtCellPhoneNumber")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.AlternativeContactNumber).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AlternativeContactNumber, Singular.Web.BootstrapEnums.InputSize.Small, , "Alt. Cellphone Number")
                  .Editor.AddClass("txtCellPhoneNumber")
                End With
              End With
            End With
          End With

        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
