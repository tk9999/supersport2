﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceAddToBooking(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

  Public Sub New()
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()

    With Helpers.If("ViewModel.HumanResourceAddToBooking() === null")
      With .Helpers.Bootstrap.Button(, "New Booking", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", , PostBackType.None, "HumanResourceAddToBookingBO.createNew()")
        .Button.AddClass("btn-block")
      End With
    End With

    With Helpers.With(Of HumanResourceAddToBooking)("ViewModel.HumanResourceAddToBooking()")
      With .Helpers.Bootstrap.HorizontalForm(True)
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceAddToBooking) d.HumanResourceName)
              With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As HumanResourceAddToBooking) d.HumanResourceName, BootstrapEnums.InputSize.Small,, "Human Resource")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceAddToBooking) d.HumanResourceSystemID)
              With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceAddToBooking) d.HumanResourceSystemID, BootstrapEnums.InputSize.Small, , "Working Area")
                .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('HumanResourceSystemID', $data)")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceAddToBooking) d.StartDate)
              With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceAddToBooking) d.StartDate, BootstrapEnums.InputSize.Small, , "Start Date")
                .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('StartDate', $data)")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceAddToBooking) d.EndDate)
              With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceAddToBooking) d.EndDate, BootstrapEnums.InputSize.Small, , "End Date")
                .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('EndDate', $data)")
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceAddToBooking) d.ProductionSystemAreaID)
              With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceAddToBooking) d.ProductionSystemAreaID, BootstrapEnums.InputSize.Small,, "Event/Booking")
                .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('ProductionSystemAreaID', $data)")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceAddToBooking) d.ProductionHumanResourceID)
              With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceAddToBooking) d.ProductionHumanResourceID, BootstrapEnums.InputSize.Small,, "Discipline")
                .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('ProductionHumanResourceID', $data)")
              End With
            End With
          End With
        End With
        With .Helpers.With(Of OBLib.OutsideBroadcast.ProductionHROBSimple)("$data.OBBooking()")
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 6, 4, 5, 5)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.StartDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.StartDateTime, BootstrapEnums.InputSize.Small,, "Start Time")
                  .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('StartDateTime', $data)")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 4, 5, 5)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.EndDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.OutsideBroadcast.ProductionHROBSimple) d.EndDateTime, BootstrapEnums.InputSize.Small,, "End Time")
                  .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceAddToBookingBO.canEdit('EndDateTime', $data)")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
                .Helpers.Bootstrap.LabelDisplay("Booking Details")
                With .Helpers.Bootstrap.Button(, "Detail", BootstrapEnums.Style.Info, , BootstrapEnums.ButtonSize.Small, , "fa-list-ul", , PostBackType.None, "HumanResourceAddToBookingBO.showModal()")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", , PostBackType.None, )
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
            End With
          End With
        End With
      End With
    End With

  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
