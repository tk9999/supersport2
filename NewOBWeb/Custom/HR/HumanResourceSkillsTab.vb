﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceSkillsTab(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mTableID As String = "ROHumanResourceSkillList"
  Private mOnSkillSelected As String = "onSkillSelected"
  Private mPagingManager As String = "ViewModel.ROHumanResourceSkillListManager"
  Private mListName As String = "ViewModel.ROHumanResourceSkillList()"
  Private mRowCssClass As String = ""

  Public Sub New(TableID As String,
                 SkillSelected As String,
                 Optional PagingManager As String = "ViewModel.ROHumanResourceSkillListManager",
                 Optional ListName As String = "ViewModel.ROHumanResourceSkillList()",
                 Optional RowCssClass As String = "")
    mTableID = TableID
    mOnSkillSelected = SkillSelected
    mPagingManager = PagingManager
    mListName = ListName
    mRowCssClass = RowCssClass
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.Row
      .AddClass("no-margin-horizontal")
      With .Helpers.DivC("pull-left")
        With .Helpers.Bootstrap.Button(, "Add New Skill", BootstrapEnums.Style.Primary, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                      PostBackType.None, "HumanResourcesPage.newSkill($data)", )
          .Button.AddClass("shiny")
        End With
      End With
      With .Helpers.DivC("pull-right")
        With .Helpers.Bootstrap.Button(, "Delete Selected", BootstrapEnums.Style.Danger, ,
                                      BootstrapEnums.ButtonSize.Small, , "fa-trash", ,
                                      PostBackType.None, "HumanResourcesPage.deleteSelectedSkills($data)", )
        End With
      End With
    End With
    With Helpers.Bootstrap.Row
      .AddClass("no-margin-horizontal")
      .Helpers.Control(New ROHumanResourceSkillsPagedGrid(Of VMType)(mTableID, "", mPagingManager, mListName, mRowCssClass, mOnSkillSelected))
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class