﻿Imports Singular.Web
Imports OBLib.Rooms
Imports OBLib.Productions.Base
Imports OBLib.Resources
Imports OBLib.HR
Imports OBLib.HR.Timesheets

Namespace Controls

  Public Class EditHRTimesheetModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditHRTimesheet)

    Private mModalID As String
    Private mControlInstanceName As String

    Public Sub New(ModalID As String,
                   ControlInstanceName As String)
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Timesheet", False, "modal-xl", BootstrapEnums.Style.Primary, , "fa-clock-o", "fa-2x", False)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of HumanResourceTimesheet)("ViewModel.CurrentHumanResourceTimesheet()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 2, 2)
                With .Helpers.Bootstrap.FlatBlock("Summary")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.TimesheetRequirementID).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.TimesheetRequirementID, BootstrapEnums.InputSize.Small, , "Requirement")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.StartDate).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.StartDate, BootstrapEnums.InputSize.Small, , "Start Date")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.EndDate).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.EndDate, BootstrapEnums.InputSize.Small, , "End Date")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.StartingHours).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.StartingHours, BootstrapEnums.InputSize.Small, , "Balance/Carry Over")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.RequiredHours).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.RequiredHours, BootstrapEnums.InputSize.Small, , "Required")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.TotalHours).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.TotalHours, BootstrapEnums.InputSize.Small, , "Hours")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.TotalOvertime).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.TotalOvertime, BootstrapEnums.InputSize.Small, , "Overtime")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceTimesheet) d.TotalShortfall).Style.Width = "100%"
                          .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceTimesheet) d.TotalShortfall, BootstrapEnums.InputSize.Small, , "Shortfall")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 9, 10, 10)
                With .Helpers.Bootstrap.FlatBlock("Days")
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.DivC("table-responsive")
                          With .Helpers.Bootstrap.TableFor(Of HumanResourceTimesheetDay)("$data.HumanResourceTimesheetDayList()", False, False, False, False, True, True, False, "HumanResourceTimesheetDayList")
                            With .FirstRow
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.TimesheetDay)
                                .Style.TextAlign = TextAlign.left
                                '.FieldDisplay.Style.TextAlign = TextAlign.left
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.HumanResourceTimesheetDayCategory)
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.TimesheetDayDescription)
                              End With
                              'With .AddColumn(Function(d As HumanResourceSkillPosition) d.PositionTypeID)
                              'End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.StartTimeString)
                                .Style.TextAlign = TextAlign.center
                                '.FieldDisplay.Style.TextAlign = TextAlign.center
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.EndTimeString)
                                .Style.TextAlign = TextAlign.center
                                '.FieldDisplay.Style.TextAlign = TextAlign.center
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.NormalHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.WeekdayHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.WeekendHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.OvertimeHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.PublicHolidayHours)
                              End With
                              With .AddReadOnlyColumn(Function(d As HumanResourceTimesheetDay) d.ShortfallHours)
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of HumanResourceTimesheet)("ViewModel.CurrentHumanResourceTimesheet()")
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, "HumanResourcesPage.saveHumanResourceTimesheet($data)", True)
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace