﻿Imports Singular.Web
Imports OBLib.HR

Public Class ROHumanResourceSkillsPagedGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mTableID As String = "ROHumanResourceSkillList"
  Private mOnSkillSelected As String = "SkillControl"
  Private mPagingManager As String = "ViewModel.ROHumanResourceSkillListManager"
  Private mListName As String = "ViewModel.ROHumanResourceSkillList()"
  Private mRowCssClass As String = ""
  Private mEditSkillFunction As String = ""

  Public Sub New(TableID As String,
                 SkillSelected As String,
                 Optional PagingManager As String = "ViewModel.ROHumanResourceSkillListManager",
                 Optional ListName As String = "ViewModel.ROHumanResourceSkillList()",
                 Optional RowCssClass As String = "",
                 Optional EditSkillFunction As String = "")
    mTableID = TableID
    mOnSkillSelected = SkillSelected
    mPagingManager = PagingManager
    mListName = ListName
    mRowCssClass = RowCssClass
    mEditSkillFunction = EditSkillFunction
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourceSkill)(mPagingManager,
                                                                                   mListName,
                                                                                   False, False, False, False, True, True, True,
                                                                                   "", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                   mOnSkillSelected, False, True, mRowCssClass)
      .Attributes("id") = mTableID
      .AddClass("no-border hover list")
      .TableBodyClass = "no-border-y no-border-x"
      With .FirstRow
        .AddClass("items")
        With .AddColumn("")
          .Style.Width = "60px"
          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.IsSelected, "Selected", "Select", , , , "fa-minus", )
          End With
        End With
        If mEditSkillFunction <> "" Then
          With .AddColumn("")
            .Style.Width = "60px"
            With .Helpers.Bootstrap.Button(, "Edit", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-edit", , PostBackType.None, mEditSkillFunction & "($data)", )
            End With
          End With
        End If
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.Discipline)

        End With
        With .AddColumn("")
          .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.PrimaryInd, "Primary", "Primary", , , , , )
        End With
        'With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.SkillLevel)

        'End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.StartDateDisplay)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.EndDateDisplay)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.BookingCount)

        End With
        With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceSkill) d.CreateDetails)

        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class

'      '#region Human Resource Skills
'      With .AddTab("Skills", " fa-briefcase", "", "Skills", )
'        With .TabPane
'          .AddClass("active")
'          With .Helpers.Bootstrap.Row
'            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
'              With .Helpers.DivC("table-responsive")
'                With .Helpers.Bootstrap.TableFor(Of HumanResourceSkill)(Function(d) ViewModel.CurrentHumanResource.HumanResourceSkillList, True, True, False, False, True, True, False)
'                  .AddClass("no-border hover list")
'                  .TableBodyClass = "no-border-y"


'                  '#region Human Resource Skill Positions
'                  With .AddChildTable(Of HumanResourceSkillPosition)(Function(d) d.HumanResourceSkillPositionList, True, True, False, False, True, True, False)
'                    .AddClass("HumanResourceSkillPositionList")
'                    .AddClass("no-border hover list")
'                    .TableBodyClass = "no-border-y"
'                    .Style.Width = "90%"
'                    .AddClass("pull-right")
'                    With .FirstRow.AddColumn(Function(e As HumanResourceSkillPosition) e.ProductionAreaID)
'                    End With
'                    With .FirstRow.AddColumn(Function(e As HumanResourceSkillPosition) e.PositionID)
'                    End With
'                    With .FirstRow.AddColumn(Function(e As HumanResourceSkillPosition) e.PositionTypeID)
'                    End With
'                    With .FirstRow.AddColumn("Production Type")
'                      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(e As HumanResourceSkillPosition) e.ProductionType, "SelectProdType($data)", "Production Type", , , "fa-search", True, , "100")
'                        '.AddOnButton.Button.Button.AddBinding(KnockoutBindingString.click, "SelectProdType($data)")
'                      End With
'                    End With
'                    With .FirstRow.AddColumn(Function(e As HumanResourceSkillPosition) e.RoomTypeID)
'                    End With
'                    With .FirstRow.AddColumn(Function(e As HumanResourceSkillPosition) e.RoomID)
'                    End With
'                    With .FirstRow.AddColumn(Function(e As HumanResourceSkillPosition) e.StartDate)
'                    End With
'                  End With
'                  '#endregion Human Resource Skill Positions

'                  '#region Human Resource Skill Rates
'                  With .AddChildTable(Of HumanResourceSkillRate)(Function(d) d.HumanResourceSkillRateList, True, True, False, False, True, True, False)
'                    .AddBinding(Singular.Web.KnockoutBindingString.if, Function(d) ViewModel.CurrentHumanResource.CanViewSkillRates)
'                    .AddClass("HumanResourceSkillRatelist")
'                    .AddClass("no-border hover list")
'                    .TableBodyClass = "no-border-y"
'                    .Style.Width = "90%"
'                    .AddClass("pull-right")
'                    With .FirstRow.AddColumn(Function(e) e.RateDate)
'                    End With
'                    With .FirstRow.AddColumn(Function(e) e.RatePerDay)
'                    End With
'                  End With
'                  '#endregion Human Resource Skill Rates
'                End With
'              End With
'            End With
'          End With
'        End With
'      End With
'      '#endregion Human Resource Skills
