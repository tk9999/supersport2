﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceSystemGrid(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mTableID As String = "HumanResourceSystemList"
  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(TableID As String,
                 ControlInstanceName As String)
    mTableID = TableID
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.DivC("table-responsive")
      With .Helpers.With(Of HumanResource)(mControlInstanceName & ".currentHumanResource()")
        With .Helpers.Bootstrap.TableFor(Of HRSystemSelect)("$data.HRSystemSelectList()",
                                                                 False, False, False,
                                                                 False, True, True, True)
          .Attributes("id") = mTableID
          .AddClass("no-border hover list")
          .TableBodyClass = "no-border-y no-border-x"
          With .FirstRow
            With .AddReadOnlyColumn(Function(c As HRSystemSelect) c.Department)
            End With
            With .AddReadOnlyColumn(Function(c As HRSystemSelect) c.SubDept)
            End With
            With .AddReadOnlyColumn(Function(c As HRSystemSelect) c.Area)
            End With
            With .AddColumn("Used by Area?")
              With .Helpers.Bootstrap.StateButton(Function(c As HRSystemSelect) c.IsAdded, "Yes", "No", , , , , )
                .Button.AddBinding(KnockoutBindingString.enable, "HRSystemSelectBO.CanEdit('IsAdded', $data)")
              End With
            End With
            With .AddColumn(Function(c As HRSystemSelect) c.SystemTeamNumberID)
            End With
            With .AddColumn("Primary?")
              With .Helpers.Bootstrap.StateButton(Function(c As HRSystemSelect) c.IsPrimary, "Yes", "No", , , , , )
                .Button.AddBinding(KnockoutBindingString.enable, "HRSystemSelectBO.CanEdit('IsPrimary', $data)")
              End With
            End With
            With .AddReadOnlyColumn(Function(d As HRSystemSelect) d.CreateDetails)

            End With
          End With
        End With
      End With
    End With
    With Helpers.DivC("loading-custom")
      .AddBinding(KnockoutBindingString.visible, "ViewModel.CurrentHumanResource().IsProcessing()")
      .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-2x")
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class