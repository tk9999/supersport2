﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceOtherInfo(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(ControlInstanceName As String)
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.With(Of OBLib.HR.HumanResource)(mControlInstanceName & ".currentHumanResource()")
      With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
        With .Helpers.FieldSet("Accreditation Photo")
          .Helpers.Control(New Controls.AccreditationImage(OBLib.CommonData.Enums.ImageType.AccreditationPhoto))
        End With
      End With
      With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
        With .Helpers.FieldSet("Residential Address")
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressR1)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressR1, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressR2)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressR2, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressR3)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressR3, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressR4)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressR4, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.PostalCodeR)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PostalCodeR, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
        End With
      End With
      With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
        With .Helpers.FieldSet("Postal Address")
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressP1)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressP1, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressP2)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressP2, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressP3)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressP3, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.AddressP4)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.AddressP4, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
            With .Helpers.Div
              .Helpers.LabelFor(Function(d As OBLib.HR.HumanResource) d.PostalCodeP)
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.PostalCodeP, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
