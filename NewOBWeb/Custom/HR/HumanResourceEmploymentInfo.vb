﻿Imports Singular.Web
Imports OBLib.HR

Public Class HumanResourceEmploymentInfo(Of VMType)
  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType)) '(Of ControlInterfaces(Of VMType).IROHumanResourceSkillsGrid)

  Private mControlInstanceName As String = "CurrentHumanResourceControl"

  Public Sub New(ControlInstanceName As String)
    mControlInstanceName = ControlInstanceName
  End Sub

  Protected Overrides Sub Setup()
    MyBase.Setup()
    With Helpers.With(Of OBLib.HR.HumanResource)(mControlInstanceName & ".currentHumanResource()")
      With .Helpers.Bootstrap.FlatBlock("Employment Info", False, False, False)
        .FlatBlockTag.AddClass("animated slideInUp fast go")
        With .ContentTag

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.EmployeeCode)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.EmployeeCode, BootstrapEnums.InputSize.Small, , "Employee Code")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ContractTypeID)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type...")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ContractStartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.ContractStartDate, BootstrapEnums.InputSize.Small, , "Contract Start Date...")
                  .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourcesPage.enableContractType($data)")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ContractEndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.ContractEndDate, BootstrapEnums.InputSize.Small, , "Contract End Date...")
                  .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourcesPage.enableContractType($data)")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ContractDays)
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.ContractDays, BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.IsOfficeWorker).Style.Width = "100%"
                With .Helpers.Bootstrap.StateButton(Function(d As OBLib.HR.HumanResource) d.IsOfficeWorker,
                                                    "Yes", "No", "btn-primary", "btn-danger", , , "btn-sm")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.IsIntern).Style.Width = "100%"
                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsIntern, , , "btn-success", "btn-danger", , , "btn-sm")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.IsManager).Style.Width = "100%"
                With .Helpers.Bootstrap.StateButton(Function(c) c.IsManager, , , "btn-success", "btn-danger", , , "btn-sm")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With

          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ManagerHumanResourceID).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.ManagerHumanResourceID, BootstrapEnums.InputSize.Small, , "Primary Manager...")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d As OBLib.HR.HumanResource) d.ManagerHumanResourceID2).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResource) d.ManagerHumanResourceID2, BootstrapEnums.InputSize.Small, , "Select Secondary Manager...")
                  .Editor.AddBinding(KnockoutBindingString.enable, "HumanResourceBO.canEdit('ManagerHumanResourceID2', $data)")
                End With
              End With
            End With
          End With

        End With
      End With
    End With
  End Sub

  Protected Overrides Sub Render()
    MyBase.Render()
    RenderChildren()
  End Sub

End Class
