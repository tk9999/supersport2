﻿Imports Singular.Web
Imports OBLib.HR

Namespace Controls

  Public Class EditHumanResourceSecondmentModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IEditHumanResourceSecondment)

    Private mModalID As String = "HumanResourceSecondmentList"
    Private mControlInstanceName As String = "CurrentHumanResourceControl"

    Public Sub New(ModalID As String,
                   ControlInstanceName As String)
      mModalID = ModalID
      mControlInstanceName = ControlInstanceName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Edit Secondment", False, "modal-xs", BootstrapEnums.Style.Primary, , "fa-qq", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.DivC("pull-right")
              With .Helpers.Toolbar
                .Helpers.MessageHolder()
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of HumanResourceSecondment)(mControlInstanceName & ".currentSecondment()")
              With .Helpers.Bootstrap.FlatBlock("Secondment Details", , , )
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.FromDate)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSecondment) d.FromDate, BootstrapEnums.InputSize.Small)

                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.ToDate)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSecondment) d.ToDate, BootstrapEnums.InputSize.Small)

                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.Description)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSecondment) d.Description, BootstrapEnums.InputSize.Small)

                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.CountryID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSecondment) d.CountryID, BootstrapEnums.InputSize.Small, , "Country")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.DebtorID)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.HR.HumanResourceSecondment) d.DebtorID, BootstrapEnums.InputSize.Small, , "Debtor")
                        End With
                      End With
                      'With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      '  .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.CostCentreID)
                      '  With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSecondment) d.CostCentreID, BootstrapEnums.InputSize.Small)
                      '  End With
                      'End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(6, 6, 6, 6, 6)
                            .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.IsAuthorised)
                            With .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSecondment) d.IsAuthorised, "Authorised", "Not Authorised", , , , "fa-minus", "btn-sm")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(6, 6, 6, 6, 6)
                            .Helpers.Bootstrap.LabelFor(Function(d As HumanResourceSecondment) d.IsConfirmed)
                            With .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSecondment) d.IsConfirmed, "Confirmed", "Not Confirmed", , , , "fa-minus", "btn-sm")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(6, 6, 6, 6, 6)
            '  With .Helpers.FieldSet("Select Country")
            '    .AddClass("animated slideInUp go")
            '    .Helpers.Control(New ROCountrySelectGrid(Of VMType)("SecondmentROCountryList",
            '                                                        "ViewModel.CurrentSecondmentROCountryListCriteria()",
            '                                                        "ViewModel.CurrentSecondmentROCountryListManager",
            '                                                        "ViewModel.CurrentSecondmentROCountryList()",
            '                                                        mControlInstanceName & ".onSecondmentCountrySelected",
            '                                                        mControlInstanceName & ".secondmentDelayedCountryRefresh"))
            '  End With
            '  With .Helpers.FieldSet("Select Debtor")
            '    .AddClass("animated slideInUp go")
            '    .Helpers.Control(New RODebtorSelectGrid(Of VMType)("SecondmentRODebtorList",
            '                                                       "ViewModel.CurrentSecondmentRODebtorListCriteria()",
            '                                                       "ViewModel.CurrentSecondmentRODebtorListManager",
            '                                                       "ViewModel.CurrentSecondmentRODebtorList()",
            '                                                       mControlInstanceName & ".onSecondmentDebtorSelected",
            '                                                       mControlInstanceName & ".secondmentDelayedDebtorRefresh"))
            '  End With
            'End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of HumanResourceSecondment)(mControlInstanceName & ".currentSecondment()")
            With .Helpers.DivC("pull-left")
              With .Helpers.Bootstrap.Button(, "Cancel", BootstrapEnums.Style.Danger, , BootstrapEnums.ButtonSize.Medium, , "fa-arrow-left", ,
                                             PostBackType.None, mControlInstanceName & ".cancelSecondment()", False)
              End With
            End With
            With .Helpers.DivC("pull-right")
              With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, mControlInstanceName & ".save()", True)
                .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".currentSecondment().IsValid()")
              End With
              With .Helpers.Bootstrap.Button(, "Save & New", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-floppy-o", ,
                                             PostBackType.None, mControlInstanceName & ".saveAndNew()", True)
                .Button.AddBinding(KnockoutBindingString.enable, mControlInstanceName & ".currentSecondment().IsValid()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class

End Namespace

