﻿Imports Singular.Web

Namespace Controls

  Public Class ChauffeurDriveBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String
    Private mBOJavaScript As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String,
                   BOJavaScript As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
      mBOJavaScript = BOJavaScript
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Chauffeur Drives", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-taxi", "fa-2x", True)
        ' Toolbar
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Add Chauffeur Drives")
                    With .ContentTag
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Chauffeurs.ChauffeurDriver)(Function(c) c.ChauffeurDriverList, True, True, False, False, True, True, False, "ChauffeurDriverList")
                          .AddClass("no-border hover list")
                          .RemoveClass("table")
                          With .FirstRow
                            .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpLocation)
                            With .AddColumn("PickUp Date")
                              With .Helpers.EditorFor(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpDateTime)
                              End With
                            End With
                            With .AddColumn("PickUp Time")
                              With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpDateTime, BootstrapEnums.InputSize.Small)
                                .Style.Height = "27px"
                              End With
                            End With
                            .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffLocation)
                            With .AddColumn("DropOff Date")
                              With .Helpers.EditorFor(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffDateTime)
                              End With
                            End With
                            With .AddColumn("DropOff Time")
                              With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffDateTime, BootstrapEnums.InputSize.Small)
                                .Style.Height = "27px"
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


