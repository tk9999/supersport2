﻿Namespace Controls

  Public Class TravelCommentsModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Travel Comments", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-comments-o", "fa-2x", True)
        ' Toolbar
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Add Travel Comments")
                    With .ContentTag
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Comments.TravelReqComment)(Function(c) c.TravelReqCommentList, True, True, False, False, True, True, False, "TravelReqCommentList")
                          .AddClass("no-border hover list")
                          .RemoveClass("table")
                          .TableBodyClass = "no-border-y"
                          With .FirstRow
                            .AddColumn(Function(c As OBLib.Travel.Comments.TravelReqComment) c.CommentTypeID, 200)
                            .AddColumn(Function(c As OBLib.Travel.Comments.TravelReqComment) c.Comment)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace



