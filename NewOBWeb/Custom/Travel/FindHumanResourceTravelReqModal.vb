﻿Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Timesheets
Imports Singular.Web
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Security.User
Imports Singular.Rules
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly

Namespace Controls

  Public Class FindHumanResourceTravelReqModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType).IFindHumanResourceTravelReq)

    Private mModalID As String = ""

    Public Sub New(ModalID As String)
      mModalID = ModalID
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Find Human Resource", False, "modal-xl", BootstrapEnums.Style.Primary, , "fa-user-secret", "fa-2x", True)
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
              With .Helpers.Bootstrap.FlatBlock("Criteria")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceTravelReqList.Criteria)("ViewModel.ROHumanResourceTravelReqListCriteria()")
                      With .Helpers.Bootstrap.Column(12, 12, 6, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.KeyWord)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, BootstrapEnums.InputSize.Small, , "Search")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Firstname)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Firstname, BootstrapEnums.InputSize.Small, , "Firstname")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.PreferredName)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Surname)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.DisciplineID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.ContractTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.FieldSet("Sub-Depts and Areas")
                            With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                    .Button.AddBinding(KnockoutBindingString.click, "TravelVM.selectSystem($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                                    .Button.AddClass("btn-block")
                                  End With
                                  With .Helpers.Bootstrap.Row
                                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                                        With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                          .Button.AddBinding(KnockoutBindingString.click, "TravelVM.selectProductionArea($data)")
                                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                          .Button.AddClass("btn-block")
                                        End With
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.DivC("loading-custom")
                    .AddBinding(KnockoutBindingString.visible, "ViewModel.ROHumanResourceTravelReqListManager().IsLoading()")
                    .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 8, 6, 7)
              With .Helpers.Bootstrap.FlatBlock("Human Resources")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourceTravelReq)("ViewModel.ROHumanResourceTravelReqListManager",
                                                                                                          "ViewModel.ROHumanResourceTravelReqList()",
                                                                                                          False, False, False, True, True, True, True,
                                                                                                          , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                                          "", False, True)
                        .AddClass("hover list")
                        .Pager.PagerListTag.ListTag.AddClass("pull-right")
                        With .FirstRow
                          .AddClass("items selectable")
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Selected", "Select", , , , , )
                              .Button.AddBinding(KnockoutBindingString.enable, "ROHumanResourceTravelReqBO.canEdit('SelectButton', $data)")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "TravelVM.onNewTravellerSelected($data)")
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) c.PreferredFirstSurname)
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) c.EmployeeCode)
                          End With
                          With .AddReadOnlyColumn(Function(c As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) c.IDNo)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 3)
              'With .Helpers.Bootstrap.FlatBlock("Travellers Booked")
              '  With .ContentTag
              '    With .Helpers.Bootstrap.Row
              '      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              '        With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged)("ViewModel.ROTravellerPagedListManager", "ViewModel.ROTravellerPagedList()",
              '                                                                              False, False, False, False, True, True, True,
              '                                                                             , Singular.Web.BootstrapEnums.PagerPosition.Bottom,
              '                                                                             "", True, True)
              '          .AddClass("hover list")
              '          .RemoveClass("table")
              '          
              '          With .FirstRow
              '            .AddClass("items selectable")
              '            With .AddReadOnlyColumn(Function(c As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) c.HumanResource)
              '            End With
              '          End With
              '        End With
              '      End With
              '    End With
              '  End With
              'End With
              With .Helpers.Bootstrap.FlatBlock("Travellers Selected")
                .AddBinding(KnockoutBindingString.if, "ViewModel.TravellersToAddList().length > 0")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Travellers.TravelRequisitionTraveller)("ViewModel.TravellersToAddList()", False, False, False, , True, True, False)
                        .RemoveClass("table")
                        With .FirstRow
                          .AddReadOnlyColumn(Function(d) d.HumanResource)
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer

        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace

