﻿Imports Singular.Web

Namespace Controls

  Public Class FindHumanResourceBasic(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = ""
    Private mJSControlName As String = ""
    Private mJSMethodsPage As String = ""

    Public Sub New(ModalID As String,
                   JSMethodsPage As String,
                   JSControlName As String)
      mModalID = ModalID
      mJSControlName = JSControlName
      mJSMethodsPage = JSMethodsPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("SelectROHumanResource", "Search for Travellers", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , "fa-user-plus", "fa-2x", True)
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          With .Helpers.Bootstrap.FlatBlock("Travellers", True)
            With .AboveContentTag
              With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceTravelReqList.Criteria)("ViewModel.ROHumanResourceTravelReqListCriteria()")
                With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, BootstrapEnums.InputSize.Small, , "Search...")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.Button("", "Clear", BootstrapEnums.Style.Danger, , , , "fa-trash-o", , , mJSMethodsPage + ".ClearFilter()", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                End With
              End With
            End With
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.PagedGridFor(Of OBLib.HR.ReadOnly.ROHumanResourceTravelReq)("ROHumanResourceTravelReqListManager",
                                                                                       "ROHumanResourceTravelReqList()",
                                                                                       False, False, False, False, True, True, False,
                                                                                       "ProductionTypesNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom,
                                                                                       "", False)
                  .AddClass("no-border hover list")
                  .RemoveClass("table")
                  .TableBodyClass = "no-border-y"
                  .Pager.PagerListTag.ListTag.AddClass("pull-left")
                  With .FirstRow
                    .AddClass("items")
                    .AddBinding(Singular.Web.KnockoutBindingString.click, mJSControlName & ".onRowSelected($data)")
                    With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) d.Firstname)
                    End With
                    With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) d.SecondName)
                    End With
                    With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) d.Surname)
                    End With
                    With .AddReadOnlyColumn(Function(d As OBLib.HR.ReadOnly.ROHumanResourceTravelReq) d.EmployeeCode)
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace


