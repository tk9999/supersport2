﻿Imports Singular.Web

Namespace Controls

  Public Class RentalCarBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String
    Private mBOJavaScript As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String,
                   BOJavaScript As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
      mBOJavaScript = BOJavaScript
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Rental Cars", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-car", "fa-2x", True)
        ' Toolbar
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Add Rental Cars")
                    With .ContentTag
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.RentalCars.RentalCar)(Function(c) c.RentalCarList, True, True, False, False, True, True, False, "RentalCarList")
                          .AddClass("no-border hover list")
                          .TableBodyClass = "no-border-y"
                          .RemoveClass("table")
                          With .FirstRow
                            .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.RentalCarAgentID, 80)
                            .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.CarTypeID, 100)
                            .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.AgentBranchIDFrom, 120)
                            With .AddColumn("PickUp Date")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.EditorFor(Function(c As OBLib.Travel.RentalCars.RentalCar) c.PickupDateTime)
                              End With
                            End With
                            With .AddColumn("PickUp Time")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.RentalCars.RentalCar) c.PickupDateTime, BootstrapEnums.InputSize.Small)
                                .Style.Height = "27px"
                              End With
                            End With
                            .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCar) c.AgentBranchIDTo, 120)
                            With .AddColumn("DropOff Date")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.EditorFor(Function(c As OBLib.Travel.RentalCars.RentalCar) c.DropoffDateTime)
                              End With
                            End With
                            With .AddColumn("DropOff Time")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.RentalCars.RentalCar) c.DropoffDateTime, BootstrapEnums.InputSize.Small)
                                .Style.Height = "27px"
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace

