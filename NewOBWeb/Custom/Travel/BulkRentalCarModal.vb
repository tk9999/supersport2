﻿Imports Singular.Web

Namespace Controls

  Public Class BulkRentalCarModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mJSMethodPage As String

    Public Sub New(JSMethodPage As String)
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("BulkRentalCarModal", "Bulk Rental Cars", , "modal-xs", Singular.Web.BootstrapEnums.Style.Danger, , " fa-car", "fa-2x", )
        ' Toolbar
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FlatBlock("Rental Car Details", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.BulkRentalCars.BulkRentalCar)(mJSMethodPage & ".currentBulkRentalCar()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.CrewTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.CrewTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'CrewTypeID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.RentalCarAgentID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.RentalCarAgentID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'RentalCarAgentID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.CarTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.CarTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'CarTypeID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.AgentBranchIDFrom)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.AgentBranchIDFrom, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'AgentBranchIDFrom')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.PickupDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.PickupDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'PickupDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Pickup Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCar) c.PickupDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'PickupDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.AgentBranchIDTo)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.AgentBranchIDTo, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'AgentBranchIDTo')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.DropoffDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.DropoffDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'DropoffDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Dropoff Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCar) c.DropoffDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'DropoffDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.Quantity)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.BulkRentalCars.BulkRentalCar) d.Quantity, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "BulkRentalCarBO.canEdit($data, 'Quantity')")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
            '  With .Helpers.Bootstrap.FlatBlock("Passengers", False, False)
            '    With .ContentTag
            '      With .Helpers.With(Of OBLib.Travel.BulkRentalCars.BulkRentalCar)(mJSMethodPage & ".currentRentalCar()")
            '        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResource)("$data.RentalCarHumanResourceList()",
            '                                                                                            False, False, False, True, True, True, False,
            '                                                                                            "RentalCarHumanResourceList")
            '          .AddClass("no-border hover list")
            '          .RemoveClass("table")
            '          .TableBodyClass = "no-border-y"
            '          With .FirstRow
            '            With .AddColumn("")
            '              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.OnRentalCarInd, "On Car", "Select", "btn-success", "btn-default", , , )
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'OnCarButton')")
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarPassengerBO.canView($data, 'OnCarButton')")
            '                '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".OnRentalCar($data)")
            '                .Button.AddClass("btn-block")
            '              End With
            '              With .Helpers.Bootstrap.Button(, "Clashes", BootstrapEnums.Style.Danger, , , , "fa-bomb", , , , )
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarPassengerBO.canView($data, 'OtherRentalCarsButton')")
            '                .Button.AddBinding(KnockoutBindingString.title, "$data.OtherRentalCars()")
            '                .Button.AddClass("btn-block")
            '              End With
            '            End With
            '            .AddReadOnlyColumn(Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResource) c.HumanResourceName)
            '            With .AddColumn("Licence")
            '              .Style.Width = "80px"
            '              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.DriverLicenseInd, "Yes", "No", "btn-info", , "fa-drivers-license-o", , )
            '                .Button.Style.Width = "80px"
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
            '                .Button.AddClass("btn-block")
            '              End With
            '            End With
            '            .AddReadOnlyColumn(Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResource) c.CityCode)
            '            With .AddReadOnlyColumn(Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResource) c.OtherRentalCarsShort)
            '              .FieldDisplay.AddBinding(KnockoutBindingString.title, "$data.OtherRentalCars()")
            '            End With
            '            With .AddColumn("Driver")
            '              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.DriverInd, "Yes", "No", "btn-info", "btn-default", "fa-car", , )
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'DriverInd')")
            '                '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mJSMethodPage + ".PassengerCountValid($data) && $data.OnRentalCarInd()")
            '                '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".OnDriverInd($data)")
            '                .Button.AddClass("btn-block")
            '              End With
            '            End With
            '            With .AddColumn("Co Driver")
            '              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.CoDriverInd, "Yes", "No", "btn-info", "btn-default", "fa-car", , )
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'CoDriverInd')")
            '                '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mJSMethodPage + ".PassengerCountValid($data) && $data.OnRentalCarInd()")
            '                .Button.AddClass("btn-block")
            '              End With
            '            End With
            '            With .AddColumn("Cancel?")
            '              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
            '                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'CancelButton')")
            '                '.Button.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResource) c.OnRentalCarInd AndAlso c.RentalCarHumanResourceID <> 0)
            '                '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".CancelledRCPassenger($data)")
            '                .Button.AddClass("btn-block")
            '              End With
            '            End With
            '            With .AddColumn(Function(c As OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResource) c.CancelledReason)
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'CancelledReason')")
            '            End With
            '            With .AddColumn("Re-Add")
            '              With .Helpers.LinkFor(, , , , Singular.Web.LinkTargetType.NotSet)
            '                '.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(c As OBLib.Travel.Flights.FlightHumanResource) c.CancelledInd)
            '                .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'ReAddButton')")
            '                '.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".ReAddFlightPassenger($data)")
            '                .AddClass("btn btn-xs btn-warning")
            '                .Helpers.HTML("Re-Add")
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Travel.BulkRentalCars.BulkRentalCar)(mJSMethodPage & ".currentBulkRentalCar()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Generate", BootstrapEnums.Style.Success, , BootstrapEnums.ButtonSize.Medium, , "fa-gears", , PostBackType.None, "BulkRentalCarBO.generateCars(ViewModel.TravelRequisition(), $data)")
                .Button.AddBinding(KnockoutBindingString.enable, "$data.IsValid()")
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


