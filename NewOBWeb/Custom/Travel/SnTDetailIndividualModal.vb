﻿Imports Singular.Web

Namespace Controls

  Public Class SnTDetailIndividualModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String
    Private mBOJavaScript As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String,
                   BOJavaScript As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
      mBOJavaScript = BOJavaScript
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Edit S&T", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , " fa-edit", "fa-2x", True)
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'Error Messages Start -------------------------------------------------------------------------------------
          With .Helpers.Bootstrap.Row
            'With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
            '  .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
            '  .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
            'End With
            With .Helpers.With(Of OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged)("ViewModel.CurrentSnTHumanResource")
              With .Helpers.Bootstrap.Column(12, 12, 12, 9, 3)
                With .Helpers.Bootstrap.FlatBlock("Traveller S&T Summary", False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.HumanResource)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.HumanResource, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.CityCode)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.CityCode, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalBreakfastAmount)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalBreakfastAmount, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalLunchAmount)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalLunchAmount, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalDinnerAmount)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalDinnerAmount, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalIncidentalAmount)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged) d.TotalIncidentalAmount, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 9, 9)
                With .Helpers.Bootstrap.FlatBlock("S&T Details", False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.SnT.SnTDetail)("ViewModel.BulkSnTDetailList()", False, False, , , , , )
                        .RemoveClass("table")
                        With .FirstRow
                          'With .AddColumn(Function(d As OBLib.Travel.SnT.AdHocSnTDetail) d.CalculatedPolicy)
                          'End With
                          With .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.GroupSnTID)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.SnTDay)
                            .AddClass("snt-column-widths-100")
                          End With
                          With .AddColumn("Breakfast")
                            .AddClass("snt-column-widths-80")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasBreakfast, "Yes", "No", "btn-success", "btn-default", "", "", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, mBOJavaScript + ".CanEdit($data, 'BreakfastSnTEnabled')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".SetOnBreakfast($data)")
                              .Button.AddClass("btn-block")
                            End With
                            'With .Helpers.Bootstrap.Button("", "View Detail", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, "")
                            '  .Button.AddClass("Popovers btn-block")
                            '  .Button.Attributes("data-html") = "true"
                            '  .Button.Attributes("data-title") = "<strong>S&T Already Provided</strong>"
                            '  .Button.Attributes("data-placement") = "top"
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.attr, "{'data-content': " + mJSMethodPage + ".GetDetails($data.BreakfastSystemID(), $data.BreakfastProductionAreaID(), $data.OtherBooking()) }")
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mBOJavaScript + ".CanEdit($data, 'BreakfastSnTEnabled')")
                            'End With
                          End With 'Breakfast

                          With .AddColumn("Lunch")
                            .AddClass("snt-column-widths-80")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasLunch, "Yes", "No", "btn-success", "btn-default", "", "", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, mBOJavaScript + ".CanEdit($data, 'LunchSnTEnabled')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".SetOnLunch($data)")
                              .Button.AddClass("btn-block")
                            End With
                            'With .Helpers.Bootstrap.Button("", "View Detail", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, "")
                            '  .Button.AddClass("Popovers btn-block")
                            '  .Button.Attributes("data-html") = "true"
                            '  .Button.Attributes("data-title") = "<strong>S&T Already Provided</strong>"
                            '  .Button.Attributes("data-placement") = "top"
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.attr, "{'data-content': " + mJSMethodPage + ".GetDetails($data.LunchSystemID(), $data.LunchProductionAreaID(), $data.OtherBooking()) }")
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mBOJavaScript + ".CanEdit($data, 'LunchSnTEnabled')")
                            'End With
                          End With

                          With .AddColumn("Dinner")
                            .AddClass("snt-column-widths-80")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasDinner, "Yes", "No", "btn-success", "btn-default", "", "", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, mBOJavaScript + ".CanEdit($data, 'DinnerSnTEnabled')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".SetOnDinner($data)")
                              .Button.AddClass("btn-block")
                            End With
                            'With .Helpers.Bootstrap.Button("", "View Detail", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, "")
                            '  .Button.AddClass("Popovers btn-block")
                            '  .Button.Attributes("data-html") = "true"
                            '  .Button.Attributes("data-title") = "<strong>S&T Already Provided</strong>"
                            '  .Button.Attributes("data-placement") = "top"
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.attr, "{'data-content': " + mJSMethodPage + ".GetDetails($data.DinnerSystemID(), $data.DinnerProductionAreaID(), $data.OtherBooking()) }")
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mBOJavaScript + ".CanEdit($data, 'DinnerSnTEnabled')")
                            'End With
                          End With

                          With .AddColumn("Incidental")
                            .AddClass("snt-column-widths-80")
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Travel.SnT.SnTDetail) c.HasIncidental, "Yes", "No", "btn-success", "btn-default", "", "", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, mBOJavaScript + ".CanEdit($data, 'IncidentalSnTEnabled')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".SetOnIncidental($data)")
                              .Button.AddClass("btn-block")
                            End With
                            'With .Helpers.Bootstrap.Button("", "View Detail", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , , , Singular.Web.PostBackType.None, "")
                            '  .Button.AddClass("Popovers btn-block")
                            '  .Button.Attributes("data-html") = "true"
                            '  .Button.Attributes("data-title") = "<strong>S&T Already Provided</strong>"
                            '  .Button.Attributes("data-placement") = "top"
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.attr, "{'data-content': " + mJSMethodPage + ".GetDetails($data.IncidentalSystemID(), $data.IncidentalProductionAreaID(), $data.OtherBooking()) }")
                            '  .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mBOJavaScript + ".CanEdit($data, 'IncidentalSnTEnabled')")
                            'End With
                          End With
                          With .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.TotalAmount, 100)
                            .AddClass("snt-column-widths-80")
                          End With
                          With .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.ChangedReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c As OBLib.Travel.SnT.SnTDetail) Not c.IsValid OrElse c.ChangedReason <> "")
                          End With
                          .AddColumn(Function(d As OBLib.Travel.SnT.SnTDetail) d.Comments)
                        End With
                        With .FooterRow
                          With .AddColumn("")
                            With .Helpers.Bootstrap.Button(, "Fetch Days", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus", , , mJSMethodPage + ".openFetchDaysModal($data)")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      Helpers.Control(New FetchDaysModal(Of VMType)("FetchDays", "TravelVM"))
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


