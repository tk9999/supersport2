﻿Namespace Controls

  Public Class AccommodationBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String
    Private mBOJavaScript As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String,
                   BOJavaScript As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
      mBOJavaScript = BOJavaScript
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Accommodation", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-building-o", "fa-2x", True)
        ' Toolbar
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Add Accommodation")
                    With .ContentTag
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Accommodation.Accommodation)(Function(c) c.AccommodationList, True, True, False, False, True, True, False, "AccommodationList")
                          .AddClass("no-border hover list")
                          .RemoveClass("table")
                          .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                          With .FirstRow
                            .AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.AccommodationProviderID)
                            .AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.CheckInDate)
                            .AddColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.CheckOutDate)
                            .AddReadOnlyColumn(Function(c As OBLib.Travel.Accommodation.Accommodation) c.NightsAway)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace

