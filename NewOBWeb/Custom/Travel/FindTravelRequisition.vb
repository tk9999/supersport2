﻿Imports Singular.Web

Namespace Controls

  Public Class FindTravelRequisition(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mJSControlName As String = ""

    Public Sub New(ModalID As String,
                   JSControlName As String)
      mJSControlName = JSControlName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("FindTravelRequisition", "Find Requisition", False, "modal-xl", BootstrapEnums.Style.Primary, , "fa-search", "fa-2x")
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
              With .Helpers.Bootstrap.FlatBlock("Criteria")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.With(Of OBLib.Travel.ReadOnly.ROTravelRequisitionListPaged.Criteria)("ViewModel.ROTravelRequisitionListPagedCriteria()")
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.Title).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Title, BootstrapEnums.InputSize.Small, , "Title")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, BootstrapEnums.InputSize.Small, , "Start Date")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, BootstrapEnums.InputSize.Small, , "End Date")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.CreatedBy).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.CreatedBy, BootstrapEnums.InputSize.Small, , "Created By")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.RefNo).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.RefNo, BootstrapEnums.InputSize.Small, , "Ref No.")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d) d.HumanResourceID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResourceID, BootstrapEnums.InputSize.Small, , "Traveller Name")
                          End With
                          'With .Helpers.Bootstrap.ButtonGroup
                          '  With .Helpers.Bootstrap.Button(, "", Singular.Web.BootstrapEnums.Style.Custom, "", Singular.Web.BootstrapEnums.ButtonSize.Small, , , , Singular.Web.PostBackType.None, mJSControlName + ".showROHumanResourceList()")
                          '    With .Button.Helpers.HTMLTag("i")
                          '      .AddClass("fa fa-user")
                          '    End With
                          '    With .Button.Helpers.HTMLTag("span")
                          '      .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d) d.HumanResource)
                          '    End With
                          '    .Button.AddBinding(Singular.Web.KnockoutBindingString.css, mJSControlName + ".GetHRCss(ViewModel.ROTravelRequisitionListPagedCriteria())")
                          '  End With
                          'End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.FieldSet("Sub-Depts")
                            With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                  .Button.AddBinding(KnockoutBindingString.click, "TravelVM.addSystemCriteria($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                                  .Button.AddClass("btn-block buttontext")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.Div
                            With .Helpers.FieldSet("Areas")
                              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
                                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                      .Button.AddBinding(KnockoutBindingString.click, "TravelVM.addProductionAreaCriteria($data)")
                                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                                      .Button.AddClass("btn-block buttontext")
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", , Singular.Web.PostBackType.None, "ROTravelRequisitionListPagedCriteriaBO.refresh(ViewModel.ROTravelRequisitionListPagedPagingManager())")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.IsValid()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-eraser", , Singular.Web.PostBackType.None, "ROTravelRequisitionListPagedCriteriaBO.clearCriteria($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ViewModel.IsValid()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .Helpers.DivC("loading-custom")
                          .AddBinding(KnockoutBindingString.visible, "ViewModel.ROTravelRequisitionListPagedPagingManager().IsLoading()")
                          .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
              With .Helpers.Bootstrap.FlatBlock("Bookings")
                With .ContentTag
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.PagedGridFor(Of OBLib.Travel.ReadOnly.ROTravelRequisitionPaged)("ViewModel.ROTravelRequisitionListPagedPagingManager",
                                                                                                            "ViewModel.ROTravelRequisitionListPaged()",
                                                                                                            False, False, False, False, True, True, True,
                                                                                                            "ROTravelRequisitionPagedsNav", Singular.Web.BootstrapEnums.PagerPosition.Bottom, , , True)

                      .AddClass("no-border hover list")
                      .RemoveClass("table")
                      .TableBodyClass = "no-border-y no-border-x no-border-x-top"
                      .Pager.PagerListTag.ListTag.AddClass("pull-left")
                      With .FirstRow
                        .AddClass("items selectable")
                        With .AddColumn
                          .Style.Width = 50
                          .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                    Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-pencil", , ,
                                                    mJSControlName + ".editROTravelRequisitionPaged($data)", )
                        End With
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.RequisitionNumber)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.Title)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.SubTitle)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.SubDept)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.Area)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.StartDateTime)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.EndDateTime)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.ReadOnly.ROTravelRequisitionPaged) c.CreatedByName)
                        If Singular.Security.HasAccess("Ad Hoc Bookings.Remove Booking") Then
                          With .AddColumn
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-trash-o", , Singular.Web.PostBackType.None, mJSControlName + ".removeAdHocBooking($data)")
                              .Button.AddClass("btn-custom")
                            End With
                          End With
                        End If
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace

