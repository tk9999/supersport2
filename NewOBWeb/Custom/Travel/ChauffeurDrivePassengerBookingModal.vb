﻿Imports Singular.Web

Namespace Controls

  Public Class ChauffeurDrivePassengerBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mJSMethodPage As String

    Public Sub New(JSMethodPage As String)
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("AddChauffeurDrivePassengerModal", "Chauffeur Passengers", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-taxi", "fa-2x", True)
        With .ContentDiv
          With .Helpers.With(Of OBLib.Travel.RentalCars.RentalCar)("TravelVM.currentChauffeur()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.DivC("error-list")
            .AddBinding(KnockoutBindingString.visible, "Singular.Validation.GetBrokenRulesHTML($data) != '<ul></ul>'")
            .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesHTML($data)")
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.Chauffeurs.ChauffeurDriver)("TravelVM.currentChauffeur()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 9, 3)
                With .Helpers.Bootstrap.FlatBlock("Chauffeur Details", False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      '  .AddBinding(KnockoutBindingString.visible, "ChauffeurDriverBO.canView($data, 'ErrorBox')")
                      '  With .Helpers.DivC("modal-validation-popup")
                      '    .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesHTML($data)")
                      '  End With
                      'End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.PickUpDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.PickUpDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'PickUpDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Pickup Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.PickUpDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'PickUpDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.DropOffDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.DropOffDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'DropOffDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Dropoff Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriver) c.DropOffDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'DropOffDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                          .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'CancelButton')")
                          '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".IsChauffeurCancelled($data)")
                          .Button.AddClass("btn-block")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.CancelledReason)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.CancelledReason, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'CancelledReason')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.MainPassenger)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Travel.Chauffeurs.ChauffeurDriver) d.MainPassenger, Singular.Web.BootstrapEnums.InputSize.Small)
                            '.Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurDriverBO.canEdit($data, 'CancelledReason')")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 9, 9)
              With .Helpers.Bootstrap.FlatBlock("Chauffeur Passengers", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.Chauffeurs.ChauffeurDriver)("TravelVM.currentChauffeur()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResource)("$data.ChauffeurDriverHumanResourceList()",
                                                                                                                False, False, False, True, True, True, False,
                                                                                                                "ChauffeurDriverHumanResourceList")
                        .AddClass("no-border hover list")
                        .RemoveClass("table")
                        .TableBodyClass = "no-border-y"
                        With .FirstRow
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResource) c.OnCarInd, "On Car", "Select", "btn-success", "btn-default", , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurPassengerBO.canEdit($data, 'OnCarButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ChauffeurPassengerBO.canView($data, 'OnCarButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.Button(, "Clashes", BootstrapEnums.Style.Danger, , , , "fa-bomb", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "ChauffeurPassengerBO.canView($data, 'OtherChauffeursButton')")
                              .Button.AddBinding(KnockoutBindingString.title, "$data.OtherChauffeurs()")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResource) c.HumanResource)
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResource) c.CityCode)
                          With .AddColumn("Main Passenger")
                            '.Style.Width = "80px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.MainPassengerInd, "Yes", "No", "btn-success", "btn-default", , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurPassengerBO.canEdit($data, 'MainPassengerInd')")
                              '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.OnChauffeurCarInd()")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn("Cancel?")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurPassengerBO.canEdit($data, 'CancelButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResource) c.CancelledReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "ChauffeurPassengerBO.canEdit($data, 'CancelledReason')")
                            .Editor.AddClass("input-md")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
      'Helpers.Control(New ChauffeurPassengerClashModal(Of VMType)("ChauffeurPassengerClashDetails", mJSMethodPage))
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace



