﻿Imports Singular.Web

Namespace Controls

  Public Class SnTTemplateModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Public Sub New()
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("SnTTemplateModal", "S&T Template", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , " fa-edit", "fa-2x", )
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.SnT.SnTTemplate)("ViewModel.SnTTemplate()")
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                With .Helpers.Bootstrap.FlatBlock("Policy Details", False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d As OBLib.Travel.SnT.SnTTemplate) d.GroupSnTID)
                          .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.SnT.SnTTemplate) d.GroupSnTID, Singular.Web.BootstrapEnums.InputSize.Small, , "Policy")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d As OBLib.Travel.SnT.SnTTemplate) d.CurrencyID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.SnT.SnTTemplate) d.CurrencyID, Singular.Web.BootstrapEnums.InputSize.Small, , "Policy")
                            .Editor.AddBinding(KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Travel.SnT.SnTTemplate) d.BreakfastInd, "Breakfast", "Breakfast", "btn-success", , , "", "btn-sm")
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "SnTTemplateBO.breakfastAmount($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Travel.SnT.SnTTemplate) d.LunchInd, "Lunch", "Lunch", "btn-success", , , "", "btn-sm")
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "SnTTemplateBO.lunchAmount($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Travel.SnT.SnTTemplate) d.DinnerInd, "Dinner", "Dinner", "btn-success", , , "", "btn-sm")
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "SnTTemplateBO.dinnerAmount($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Travel.SnT.SnTTemplate) d.IncidentalInd, "Incidental", "Incidental", "btn-success", , , "", "btn-sm")
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "SnTTemplateBO.incidentalAmount($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.LabelFor(Function(d As OBLib.Travel.SnT.SnTTemplate) d.ChangedReason)
                          .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.SnT.SnTTemplate) d.ChangedReason, Singular.Web.BootstrapEnums.InputSize.Small, , "Reason")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                With .Helpers.Bootstrap.FlatBlock("Travellers", False, False)
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.Button(, "Select All", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-angle-double-down", , PostBackType.None, "SnTTemplateBO.selectAll($data)")
                        End With
                        With .Helpers.Bootstrap.Button(, "Select All", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, ,
                                                       "fa-angle-double-up", , PostBackType.None, "SnTTemplateBO.deselectAll($data)")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.SnT.SnTTemplateTraveller)(Function(d As OBLib.Travel.SnT.SnTTemplate) d.SnTTemplateTravellerList, False, False, , , , , )
                          .AddClass("hover list")
                          With .FirstRow
                            With .AddColumn("")
                              .Style.Width = "50px"
                              With .Helpers.Bootstrap.StateButtonNew(Function(d As OBLib.Travel.SnT.SnTTemplateTraveller) d.IsSelected, "Selected", "Select", "btn-success", , , , )
                                '.ButtonText.AddBinding(Singular.Web.KnockoutBindingString.text, "SnTTemplateBO.breakfastAmount($data)")
                                .Button.AddClass("btn-block")
                              End With
                            End With
                            .AddReadOnlyColumn(Function(c As OBLib.Travel.SnT.SnTTemplateTraveller) c.HumanResource)
                            .AddReadOnlyColumn(Function(c As OBLib.Travel.SnT.SnTTemplateTraveller) c.Discipline)
                            .AddColumn(Function(c As OBLib.Travel.SnT.SnTTemplateTraveller) c.SnTStartDate)
                            .AddColumn(Function(c As OBLib.Travel.SnT.SnTTemplateTraveller) c.SnTEndDate)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of OBLib.Travel.SnT.SnTTemplate)("ViewModel.SnTTemplate()")
            With .Helpers.Bootstrap.PullRight()
              With .Helpers.Bootstrap.Button(, "Apply", BootstrapEnums.Style.Success, ,
                                             BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                                             PostBackType.None, "TravelVM.applyBulkGroupPolicy($data)", )
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace

