﻿Imports Singular.Web

Namespace Controls

  Public Class ChauffeurPassengerClashModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'With Helpers.Bootstrap.Dialog(mModalID, "Clash Details", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , " fa-bomb", "fa-2x", True)
      '  With .Body
      '    .AddClass("colour-tone-modal modal-background-gray")
      '    With .Helpers.Bootstrap.FlatBlock("Chauffeur Passenger Clash Details", False, False)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          .AddBinding(KnockoutBindingString.if, "ViewModel.IsAdHoc() == true")
      '          With .Helpers.With(Of OBLib.AdHoc.Travel.AdHocBulkChauffeurDriverPassenger)(mJSMethodPage + ".CurrentBulkChauffeurDriverPassenger()")
      '            With .Helpers.HTMLTag("h3")
      '              .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".HRClashDetails($data)")
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.TableFor(Of OBLib.AdHoc.Travel.ROAdHocBulkChauffeurDriverPassengerClash)(Function(c) c.ROAdHocBulkChauffeurDriverPassengerClashList, False, False, , , , , )
      '                .AddClass("no-border hover list")
      '                .RemoveClass("table")
      '                .TableBodyClass = "no-border-y"
      '                With .FirstRow
      '                  .AddReadOnlyColumn(Function(d As OBLib.AdHoc.Travel.ROAdHocBulkChauffeurDriverPassengerClash) d.Description)
      '                  .AddReadOnlyColumn(Function(d As OBLib.AdHoc.Travel.ROAdHocBulkChauffeurDriverPassengerClash) d.PickUpDateTime)
      '                  .AddReadOnlyColumn(Function(d As OBLib.AdHoc.Travel.ROAdHocBulkChauffeurDriverPassengerClash) d.DropOffDateTime)
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With
      '        With .Helpers.Bootstrap.Row
      '          .AddBinding(KnockoutBindingString.if, "ViewModel.IsAdHoc() == false")
      '          With .Helpers.With(Of OBLib.Productions.OBBulkChauffeurPassenger)(mJSMethodPage + ".CurrentBulkChauffeurDriverPassenger()")
      '            With .Helpers.HTMLTag("h3")
      '              .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".HRClashDetails($data)")
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.TableFor(Of OBLib.Productions.ROOBBulkChauffeurPassengerClash)(Function(c) c.ROOBBulkChauffeurPassengerClashList, False, False, , , , , )
      '                .AddClass("no-border hover list")
      '                .RemoveClass("table")
      '                .TableBodyClass = "no-border-y"
      '                With .FirstRow
      '                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ROOBBulkChauffeurPassengerClash) d.ProductionDescription)
      '                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ROOBBulkChauffeurPassengerClash) d.PickUp)
      '                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ROOBBulkChauffeurPassengerClash) d.DropOff)
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


