﻿Imports Singular.Web

Namespace Controls

  Public Class FetchDaysModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String = ""
    Private mJSControlName As String = ""

    Public Sub New(ModalID As String,
                   JSControlName As String)
      mModalID = ModalID
      mJSControlName = JSControlName
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Fetch Days", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , " fa-calendar-plus-o", "fa-2x", True)
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          With .Helpers.Bootstrap.FlatBlock("Fetch Days", True)
            .FlatBlockTag.AddClass("flat-block-paged")
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Travellers.TravelRequisitionTraveller)("ViewModel.FetchDaysTraveller", False, False, False, , True, True, False)
                  .RemoveClass("table")
                  With .FirstRow
                    ' .AddBinding(Singular.Web.KnockoutBindingString.visible, "TravelVM.ShowThisTraveller($data)")
                    With .AddColumn("Start Date")
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.SnTStartDate, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    With .AddColumn("End Date")
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.SnTEndDate, BootstrapEnums.InputSize.Small)
                      End With
                    End With
                    .AddReadOnlyColumn(Function(d) d.HumanResource)
                    'With .AddColumn("Group Policy")
                    '  With .Helpers.Bootstrap.FormControlFor(Function(d) d.GroupPolicyID, BootstrapEnums.InputSize.Small)
                    '  End With
                    'End With
                    With .AddColumn("")
                      With .Helpers.Bootstrap.Button("", "Fetch Days", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-gear", , PostBackType.None, mJSControlName + ".fetchDays($data)")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, mJSControlName + ".checkForDateCompliance($data)")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class


End Namespace

