﻿Imports Singular.Web

Namespace Controls

  Public Class AccommodationGuestClashModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String
    Public Sub New(ModalID As String,
                   JSMethodPage As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      'With Helpers.Bootstrap.Dialog(mModalID, "Clash Details", , "modal-med", Singular.Web.BootstrapEnums.Style.Primary, , " fa-bomb", "fa-2x", True)
      '  ' Toolbar
      '  With .Body
      '    .AddClass("colour-tone-modal modal-background-gray")
      '    With .Helpers.Bootstrap.FlatBlock("Accommodation Clash Details", False, False)
      '      With .ContentTag
      '        With .Helpers.Bootstrap.Row
      '          .AddBinding(KnockoutBindingString.if, "ViewModel.IsAdHoc() == true")
      '          With .Helpers.With(Of OBLib.AdHoc.Travel.AdHocBulkAccommodationGuest)(mJSMethodPage + ".CurrentBulkAccommodationGuest()")
      '            With .Helpers.HTMLTag("h3")
      '              .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".HRClashDetails($data)")
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.TableFor(Of OBLib.AdHoc.Travel.ROAdHocBulkAccommodationGuestClash)(Function(c) c.ROAdHocBulkAccommodationGuestClashList, False, False, , , , , )
      '                .RemoveClass("table")
      '                With .FirstRow
      '                  .AddReadOnlyColumn(Function(d As OBLib.AdHoc.Travel.ROAdHocBulkAccommodationGuestClash) d.Description)
      '                  .AddReadOnlyColumn(Function(d As OBLib.AdHoc.Travel.ROAdHocBulkAccommodationGuestClash) d.AccommodationCheckInDate)
      '                  .AddReadOnlyColumn(Function(d As OBLib.AdHoc.Travel.ROAdHocBulkAccommodationGuestClash) d.AccommodationCheckOutDate)
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With
      '        With .Helpers.Bootstrap.Row
      '          .AddBinding(KnockoutBindingString.if, "ViewModel.IsAdHoc() == false")
      '          With .Helpers.With(Of OBLib.Productions.OBBulkAccommodationGuest)(mJSMethodPage + ".CurrentBulkAccommodationGuest()")
      '            With .Helpers.HTMLTag("h3")
      '              .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".HRClashDetails($data)")
      '            End With
      '            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '              With .Helpers.Bootstrap.TableFor(Of OBLib.Productions.ROOBBulkAccommodationGuestClash)(Function(c) c.ROOBBulkAccommodationGuestClashList, False, False, , , , , )
      '                .RemoveClass("table")
      '                With .FirstRow
      '                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ROOBBulkAccommodationGuestClash) d.ProductionRefNo)
      '                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ROOBBulkAccommodationGuestClash) d.AccommodationCheckInDate)
      '                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ROOBBulkAccommodationGuestClash) d.AccommodationCheckOutDate)
      '                End With
      '              End With
      '            End With
      '          End With
      '        End With
      '      End With
      '    End With
      '  End With
      'End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


