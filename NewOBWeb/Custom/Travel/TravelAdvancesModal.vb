﻿Namespace Controls

  Public Class TravelAdvancesModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Travel Advances", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-credit-card", "fa-2x", True)
        ' Toolbar
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Add Travel Advances")
                    With .ContentTag
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail)(Function(c) c.ProductionTravelAdvanceDetailList, True, True, False, False, True, True, False, "ProductionTravelAdvanceDetailList")
                          .AddClass("no-border hover list")
                          .RemoveClass("table")
                          .TableBodyClass = "no-border-y"
                          With .FirstRow
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.HumanResourceID)
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.CurrencyID)
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.Denom)
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.TravelAdvanceTypeID)
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.PayTypeID)
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.Amount)
                            .AddColumn(Function(c As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetail) c.Comments, 400)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


