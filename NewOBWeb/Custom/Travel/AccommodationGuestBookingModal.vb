﻿Imports Singular.Web

Namespace Controls

  Public Class AccommodationGuestBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mJSMethodPage As String

    Public Sub New(JSMethodPage As String)
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("AddAccommodationGuestModal", "Accommodation Guests", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-building-o", "fa-2x", True)
        ' Toolbar
        With .ContentDiv
          With .Helpers.With(Of OBLib.Travel.Flights.Flight)("TravelVM.currentAccommodation()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FlatBlock("Accommodation Details", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.Accommodation.Accommodation)("TravelVM.currentAccommodation()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.AccommodationProviderID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.AccommodationProviderID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'AccommodationProviderID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CheckInDate)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CheckInDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CheckInDate')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CheckOutDate)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CheckOutDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CheckOutDate')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.NightsAway)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.NightsAway, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CancelButton')")
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".IsAccommodationCancelled($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CancelledReason)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Accommodation.Accommodation) d.CancelledReason, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationBO.canEdit($data, 'CancelledReason')")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
              With .Helpers.Bootstrap.FlatBlock("Guests", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.Accommodation.Accommodation)("TravelVM.currentAccommodation()")
                    With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Accommodation.AccommodationHumanResource)("$data.AccommodationHumanResourceList()",
                                                                                                               False, False, True, True, True, True, False,
                                                                                                               "AccommodationHumanResourceList")
                      .AddClass("no-border hover list")
                      .RemoveClass("table")
                      .TableBodyClass = "no-border-y"
                      With .FirstRow
                        With .AddColumn("")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected, "Booked", "Select", "btn-success", "btn-default", , "", )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationGuestBO.canEdit($data, 'OnAccommodationButton')")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "AccommodationGuestBO.canView($data, 'OnAccommodationButton')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn("Clashes")
                          With .Helpers.Bootstrap.Button(, "Clashes", BootstrapEnums.Style.Danger, , , , "fa-bomb", , , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "AccommodationGuestBO.canView($data, 'ClashesButton')")
                            .Button.AddBinding(KnockoutBindingString.title, "$data.OtherAccommodation()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.Accommodation.AccommodationHumanResource) c.HumanResourceName)
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.Accommodation.AccommodationHumanResource) c.CityCode)
                        With .AddColumn(Function(c As OBLib.Travel.Accommodation.AccommodationHumanResource) c.BookingReason)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationGuestBO.canEdit($data, 'BookingReason')")
                        End With
                        'With .AddReadOnlyColumn(Function(c As OBLib.Travel.Accommodation.AccommodationHumanResource) c.OtherAccommodationShort)
                        '  .FieldDisplay.AddBinding(KnockoutBindingString.title, "$data.OtherAccommodation()")
                        'End With
                        With .AddColumn("Cancel?")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationGuestBO.canEdit($data, 'CancelButton')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn(Function(c As OBLib.Travel.Accommodation.AccommodationHumanResource) c.CancelledReason)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationGuestBO.canEdit($data, 'CancelledReason')")
                        End With
                        With .AddColumn("Re-Add")
                          With .Helpers.LinkFor(, , , , Singular.Web.LinkTargetType.NotSet)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "AccommodationGuestBO.canEdit($data, 'ReAddButton')")
                            .AddBinding(Singular.Web.KnockoutBindingString.click, "AccommodationGuestBO.readdToBooking($data)")
                            .AddClass("btn btn-xs btn-warning btn-block")
                            .Helpers.HTML("Re-Add")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace



