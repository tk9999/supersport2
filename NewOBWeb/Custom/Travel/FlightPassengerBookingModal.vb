﻿Imports Singular.Web

Namespace Controls

  Public Class FlightPassengerBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mJSMethodPage As String

    Public Sub New(JSMethodPage As String)
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("AddFlightPassengerModal", "Flight Passengers", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-plane", "fa-2x", True)
        With .ContentDiv
          With .Helpers.With(Of OBLib.Travel.Flights.Flight)("TravelVM.currentFlight()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.DivC("error-list")
            .AddBinding(KnockoutBindingString.visible, "Singular.Validation.GetBrokenRulesHTML($data) != '<ul></ul>'")
            .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesHTML($data)")
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FlatBlock("Flight Details", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.Flights.Flight)("TravelVM.currentFlight()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.CrewTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.CrewTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'CrewTypeID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.FlightNo)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.FlightNo, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'FlightNo')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.FlightTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.FlightTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'FlightTypeID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.TravelDirectionID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.TravelDirectionID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'TravelDirectionID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.AirportIDFrom)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.AirportIDFrom, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'AirportIDFrom')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.DepartureDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.DepartureDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'DepartureDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Departure Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Flights.Flight) c.DepartureDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'DepartureDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.AirportIDTo)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.AirportIDTo, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'AirportIDTo')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.ArrivalDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.ArrivalDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'ArrivalDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Arrival Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Flights.Flight) c.ArrivalDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'ArrivalDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , "btn-sm")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'CancelButton')")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightBO.canView($data, 'CancelButton')")
                            .Button.AddClass("btn-block")
                          End With
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "UnCancel", "UnCancel", "btn-info", "btn-info", "fa-undo", "fa-undo", "btn-sm")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'UnCancelButton')")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightBO.canView($data, 'UnCancelButton')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.Flights.Flight) d.CancelledReason)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.Flights.Flight) d.CancelledReason, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightBO.canEdit($data, 'CancelledReason')")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
              With .Helpers.Bootstrap.FlatBlock("Flight Passengers", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.Flights.Flight)("TravelVM.currentFlight()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Flights.FlightHumanResource)("$data.FlightHumanResourceList()",
                                                                                                    False, False, False, True, True, True, False,
                                                                                                    "FlightHumanResourceList")
                        .AddClass("no-border hover list")
                        .RemoveClass("table")
                        .TableBodyClass = "no-border-y"
                        With .FirstRow
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsOnFlight, "On Flight", "Select", "btn-success", "btn-default", , "", )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightPassengerBO.canView($data, 'OnFlightButton')")
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightPassengerBO.canEdit($data, 'OnFlightButton')")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn("")
                            .Style.Width = "50px"
                            With .Helpers.Bootstrap.Button(, "Clashes", BootstrapEnums.Style.Danger, , , , "fa-bomb", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "FlightPassengerBO.canView($data, 'OtherFlightsButton')")
                              .Button.AddBinding(KnockoutBindingString.title, "$data.OtherFlights()")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.FlightHumanResource) c.FlightClassID)
                            .Style.Width = "100px"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightPassengerBO.canEdit($data, 'FlightClassID')")
                          End With
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Flights.FlightHumanResource) c.HumanResourceName)
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Flights.FlightHumanResource) c.CityCode)
                          .AddReadOnlyColumn(Function(c As OBLib.Travel.Flights.FlightHumanResource) c.Disciplines)
                          'With .AddReadOnlyColumn(Function(c As OBLib.Travel.Flights.FlightHumanResource) c.OtherFlightsShort)
                          '  .FieldDisplay.AddBinding(KnockoutBindingString.title, "$data.OtherFlights()")
                          'End With
                          With .AddColumn("Cancel?")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                              .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightPassengerBO.canEdit($data, 'CancelButton')")
                              '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".CancelledFlightPassenger($data)")
                              .Button.AddClass("btn-block")
                            End With
                          End With
                          With .AddColumn(Function(c As OBLib.Travel.Flights.FlightHumanResource) c.CancelledReason)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightPassengerBO.canEdit($data, 'CancelledReason')")
                            .Editor.AddClass("input-md")
                          End With
                          With .AddColumn("Re-Add")
                            With .Helpers.LinkFor(, , , , Singular.Web.LinkTargetType.NotSet)
                              '.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(c As OBLib.Travel.Flights.FlightHumanResource) c.CancelledInd)
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, "FlightPassengerBO.canEdit($data, 'ReAddButton')")
                              .AddBinding(Singular.Web.KnockoutBindingString.click, "FlightPassengerBO.readdToBooking($data)")
                              .AddClass("btn btn-xs btn-warning")
                              .Helpers.HTML("Re-Add")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace

