﻿Imports Singular.Web

Namespace Controls

  Public Class RentalCarPassengerBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog("AddRentalCarPassengerModal", "Rental Car Passengers", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-car", "fa-2x", True)
        With .ContentDiv
          With .Helpers.With(Of OBLib.Travel.RentalCars.RentalCar)("TravelVM.currentRentalCar()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh").IconContainer.AddClass("fa-spin fa-5x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.DivC("error-list")
            .AddBinding(KnockoutBindingString.visible, "Singular.Validation.GetBrokenRulesHTML($data) != '<ul></ul>'")
            .AddBinding(KnockoutBindingString.html, "Singular.Validation.GetBrokenRulesHTML($data)")
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FlatBlock("Rental Car Details", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.RentalCars.RentalCar)("TravelVM.currentRentalCar()")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.RentalCarAgentID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.RentalCarAgentID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'RentalCarAgentID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CarTypeID)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CarTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CarTypeID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.MaxSeats)
                          With .Helpers.Bootstrap.ReadOnlyFormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.MaxSeats, Singular.Web.BootstrapEnums.InputSize.Small)
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.AgentBranchIDFrom)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.AgentBranchIDFrom, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'AgentBranchIDFrom')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.PickupDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.PickupDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'PickupDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Pickup Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.RentalCars.RentalCar) c.PickupDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'PickupDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.AgentBranchIDTo)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.AgentBranchIDTo, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'AgentBranchIDTo')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 7, 7)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.DropoffDateTime)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.DropoffDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'DropoffDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 5, 5)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelDisplay("Dropoff Time")
                          With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.RentalCars.RentalCar) c.DropoffDateTime, BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'DropoffDateTime')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CancelButton')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CancelledReason)
                          With .Helpers.Bootstrap.FormControlFor(Function(d As OBLib.Travel.RentalCars.RentalCar) d.CancelledReason, Singular.Web.BootstrapEnums.InputSize.Small)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarBO.canEdit($data, 'CancelledReason')")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 9)
              With .Helpers.Bootstrap.FlatBlock("Passengers", False, False)
                With .ContentTag
                  With .Helpers.With(Of OBLib.Travel.RentalCars.RentalCar)("TravelVM.currentRentalCar()")
                    With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.RentalCars.RentalCarHumanResource)("$data.RentalCarHumanResourceList()",
                                                                                                        False, False, False, True, True, True, False,
                                                                                                        "RentalCarHumanResourceList")
                      .AddClass("no-border hover list")
                      .RemoveClass("table")
                      .TableBodyClass = "no-border-y"
                      With .FirstRow
                        With .AddColumn("")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.OnRentalCarInd, "On Car", "Select", "btn-success", "btn-default", , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'OnCarButton')")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarPassengerBO.canView($data, 'OnCarButton')")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn("")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.Button(, "Clashes", BootstrapEnums.Style.Danger, , , , "fa-bomb", , , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "RentalCarPassengerBO.canView($data, 'OtherRentalCarsButton')")
                            .Button.AddBinding(KnockoutBindingString.title, "$data.OtherRentalCars()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.RentalCars.RentalCarHumanResource) c.HumanResourceName)
                        With .AddColumn("Licence")
                          .Style.Width = "80px"
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.DriverLicenseInd, "Yes", "No", "btn-info", , "fa-drivers-license-o", , )
                            .Button.Style.Width = "80px"
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        .AddReadOnlyColumn(Function(c As OBLib.Travel.RentalCars.RentalCarHumanResource) c.CityCode)
                        'With .AddReadOnlyColumn(Function(c As OBLib.Travel.RentalCars.RentalCarHumanResource) c.OtherRentalCarsShort)
                        '  .FieldDisplay.AddBinding(KnockoutBindingString.title, "$data.OtherRentalCars()")
                        'End With
                        With .AddColumn("Driver")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.DriverInd, "Yes", "No", "btn-info", "btn-default", "fa-car", , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'DriverInd')")
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mJSMethodPage + ".PassengerCountValid($data) && $data.OnRentalCarInd()")
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".OnDriverInd($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn("Co Driver")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.CoDriverInd, "Yes", "No", "btn-info", "btn-default", "fa-car", , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'CoDriverInd')")
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, mJSMethodPage + ".PassengerCountValid($data) && $data.OnRentalCarInd()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn("Cancel?")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsCancelled, "Cancelled", "Cancel", "btn-danger", , , , )
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'CancelButton')")
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(c As OBLib.Travel.RentalCars.RentalCarHumanResource) c.OnRentalCarInd AndAlso c.RentalCarHumanResourceID <> 0)
                            '.Button.AddBinding(Singular.Web.KnockoutBindingString.click, mJSMethodPage + ".CancelledRCPassenger($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn(Function(c As OBLib.Travel.RentalCars.RentalCarHumanResource) c.CancelledReason)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'CancelledReason')")
                        End With
                        With .AddColumn("Re-Add")
                          With .Helpers.LinkFor(, , , , Singular.Web.LinkTargetType.NotSet)
                            '.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(c As OBLib.Travel.Flights.FlightHumanResource) c.CancelledInd)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, "RentalCarPassengerBO.canEdit($data, 'ReAddButton')")
                            .AddBinding(Singular.Web.KnockoutBindingString.click, "RentalCarPassengerBO.readdToBooking($data)")
                            .AddClass("btn btn-xs btn-warning")
                            .Helpers.HTML("Re-Add")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace


