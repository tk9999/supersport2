﻿Imports Singular.Web

Namespace Controls

  Public Class FlightBookingModal(Of VMType)
    Inherits Singular.Web.Controls.HelperControls.HelperBase(Of ControlInterfaces(Of VMType))

    Private mModalID As String
    Private mJSMethodPage As String

    Public Sub New(ModalID As String,
                   JSMethodPage As String)
      mModalID = ModalID
      mJSMethodPage = JSMethodPage
    End Sub

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.Bootstrap.Dialog(mModalID, "Flights", , "modal-xl", Singular.Web.BootstrapEnums.Style.Primary, , " fa-plane", "fa-2x", True)
        ' Toolbar
        With .Body
          .AddClass("colour-tone-modal modal-background-gray")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.DivC("pull-right ValidationPopup Msg-Validation error-validation-styling HoverMsg")
          '    .AddBinding(Singular.Web.KnockoutBindingString.html, mJSMethodPage + ".getVMBrokenRulesHTML()")
          '    .AddBinding(Singular.Web.KnockoutBindingString.visible, "!" + mJSMethodPage + ".checkVMValidity()")
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.With(Of OBLib.Travel.TravelRequisition)("ViewModel.TravelRequisition()")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.FlatBlock("Add Flights")
                    With .ContentTag
                      With .Helpers.DivC("table-responsive")
                        With .Helpers.Bootstrap.TableFor(Of OBLib.Travel.Flights.Flight)(Function(c) c.FlightList, True, True, False, False, True, True, False, "FlightList")
                          .AddClass("no-border hover list")
                          .RemoveClass("table")
                          .TableBodyClass = "no-border-y"
                          With .FirstRow
                            .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.FlightNo)
                            .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.TravelDirectionID, 130)
                            .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.FlightTypeID, 120)
                            .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.AirportIDFrom)
                            With .AddColumn("Dept. Date")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.EditorFor(Function(c As OBLib.Travel.Flights.Flight) c.DepartureDateTime)
                              End With
                            End With
                            With .AddColumn("Dept. Time")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Flights.Flight) c.DepartureDateTime, BootstrapEnums.InputSize.Small)
                                .Style.Height = "27px"
                              End With
                            End With
                            .AddColumn(Function(c As OBLib.Travel.Flights.Flight) c.AirportIDTo)
                            With .AddColumn("Arr. Date")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.EditorFor(Function(c As OBLib.Travel.Flights.Flight) c.ArrivalDateTime)
                              End With
                            End With
                            With .AddColumn("Arr. Time")
                              .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                              With .Helpers.Bootstrap.TimeEditorFor(Function(c As OBLib.Travel.Flights.Flight) c.ArrivalDateTime, BootstrapEnums.InputSize.Small)
                                .Style.Height = "27px"
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub

  End Class
End Namespace