﻿OBMisc = {
  keypressTimeout: null,
  currentDropDown: null,
  Dates: {
    SameDay: function (Date1, Date2) {
      LDate1 = new Date(Date1);
      LDate2 = new Date(Date2);
      var sameDay = (LDate1.getDate() == LDate2.getDate() && LDate1.getMonth() == LDate2.getMonth() && LDate1.getFullYear() == LDate2.getFullYear());
      return sameDay;
    },
    DateIsBetween: function (DateToCheck, LeftDate, RightDate) {
      return (DateToCheck >= LeftDate && DateToCheck <= RightDate) || (DateToCheck <= LeftDate && DateToCheck >= RightDate);
    },
    DateIsBetweenExclusive: function (DateToCheck, LeftDate, RightDate) {
      return (DateToCheck > LeftDate && DateToCheck < RightDate) || (DateToCheck < LeftDate && DateToCheck > RightDate);
    },
    DatesOverlapExclusive: function (StartDate1, EndDate1, StartDate2, EndDate2) {
      var sd1 = new Date(StartDate1);
      var ed1 = new Date(EndDate1);
      var sd2 = new Date(StartDate2);
      var ed2 = new Date(EndDate2);
      if (OBMisc.Dates.DateIsBetweenExclusive(sd1, sd2, ed2)
        || OBMisc.Dates.DateIsBetweenExclusive(ed1, sd2, ed2)
        || OBMisc.Dates.DateIsBetweenExclusive(sd2, sd1, ed1)
        || OBMisc.Dates.DateIsBetweenExclusive(ed2, sd1, ed1)) {
        return true;
      };
      return false;
    },
    DatesOverlap: function (StartDate1, EndDate1, StartDate2, EndDate2) {
      var sd1 = new Date(StartDate1);
      var ed1 = new Date(EndDate1);
      var sd2 = new Date(StartDate2);
      var ed2 = new Date(EndDate2);
      if (OBMisc.Dates.DateIsBetween(sd1, sd2, ed2) || OBMisc.Dates.DateIsBetween(ed1, sd2, ed2)) {
        return true;
      };
      return false;
    },
    AddDays: function (Day, DaysToAdd) {
      var d2 = new Date(Day);
      d2.setDate(d2.getDate() + DaysToAdd);
      return d2;
    },
    AddMinutes: function (Day, Minutes) {
      return new Date(Day.getTime() + Minutes * 60000);
    },
    StartOfDay: function (Day) {
      var curr = new Date(Day);
      var d = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate())
      d.setHours(0, 0, 0, 0)
      return d;
    },
    EndOfDay: function (Day) {
      var curr = new Date(Day);
      var d = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate())
      d.setHours(23, 59, 59, 999)
      return d;
    },
    StartTimeBeforeEndtime: function () {

    },
    DateOnly: function (Date1) {
      var d = Date1.getDate();
      var m = Date1.getMonth();
      var yr = Date1.getFullYear();
      //var mmt = moment({ year: yr, month: m, day: d });
      var sd = new Date(yr, m, d); //mmt._d;
      return sd;
    },
    IsAfter: function (StartDate, EndDate) {
      var sd = new Date(StartDate);
      var ed = new Date(EndDate);
      if (moment(sd).isAfter(ed, 'minute')) {
        return true;
      }
      return false;
    },
    IsSameDay: function (Date1, Date2) {
      var sd = new Date(Date1);
      var ed = new Date(Date2);
      return moment(sd).isSame(ed, 'day');
    },
    IsSameTimeHour: function (Date1, Date2) {
      var sd = new Date(Date1);
      var ed = new Date(Date2);
      return moment(sd).isSame(ed, 'hour');
    },
    IsSameTimeMinute: function (Date1, Date2) {
      var sd = new Date(Date1);
      var ed = new Date(Date2);
      return moment(sd).isSame(ed, 'minute');
    },
    DifferenceInHours: function (Date1, Date2) {
      var sd = moment(new Date(Date1));
      var ed = moment(new Date(Date2));
      return sd.diff(ed, 'hours');
    },
    DifferenceInMinutes: function (StartDate, EndDate) {
      var sd = moment(new Date(StartDate));
      var ed = moment(new Date(EndDate));
      return sd.diff(ed, 'minutes');
    }
  },
  Errors: {
    LogClientError: function (usr, pge, meth, err) {
      ViewModel.CallServerMethod("LogClientError", {
        User: usr,
        Page: pge,
        Method: meth,
        ErrorMessage: err
      }, function (response) {

      });
    }
  },
  Modals: {
    CustomModal: function (ID, Type, Title, Message, SubMessage, Size, Icon, buttonOptions, HeaderIcon) {
      var headerStyleClass = '';
      switch (Type) {
        case "primary":
          headerStyleClass = 'modal-primary'
          break;
        case "success":
          headerStyleClass = 'modal-success'
          break;
        case "info":
          headerStyleClass = 'modal-info'
          break;
        case "warning":
          headerStyleClass = 'modal-warning'
          break;
        case "error":
          headerStyleClass = 'modal-error'
          break;
      };

      if (!Size) {
        Size = 'modal-xs';
      };

      var defaultButtonOptions = {
        onShown: null,
        onHidden: null,
        confirmButton: {
          callback: null,
          text: "Confirm",
          cssClass: 'btn-primary'
        },
        cancelButton: {
          callback: null,
          text: "Cancel",
          cssClass: 'btn-warning'
        }
      };

      var options = $.extend(defaultButtonOptions, buttonOptions);

      var customModal = $('<div class="custom-modal modal fade" tabindex="-1" role="dialog" aria-hidden="true"' + (ID ? 'id=' + ID + '"' : '') + '>' +
        '<div class="modal-dialog ' + Size + '">' +
        '<div class="modal-content">' +
        '<div class="modal-header ' + headerStyleClass + '">' +
        (HeaderIcon ? '<i class="fa ' + HeaderIcon + ' fa-2x"></i>'
          : '') +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
        '<h3 class="modal-title">' + Title + '</h3>' +
        '</div>' +
        '<div class="modal-body row">' +
        (Icon ? '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">' +
          '<i class="fa ' + Icon + ' fa-5x"></i>' +
          '</div>'
          : '') +
        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">' +
        '<h3>' + Message + '</h3>' +
        '<p>' + SubMessage + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '</div>' +
        '</div>' + //content
        '</div>' + //dialog
        '</div>'); //modal

      $('body').append(customModal);

      var me = $('div.custom-modal');
      var footer = $(me).find("div.modal-footer");

      //add confirm
      if (options.cancelButton.callback) {
        var cancelButton = $('<button class="btn btn-md ' + options.cancelButton.cssClass + '">' + options.cancelButton.text + '</button>');
        cancelButton.on('click', function (evt) {
          options.cancelButton.callback(me, evt);
        });
        footer.append(cancelButton);
      };

      //add cancel
      if (options.confirmButton.callback) {
        var confirmButton = $('<button class="btn btn-md ' + options.confirmButton.cssClass + '">' + options.confirmButton.text + '</button>');
        confirmButton.on('click', function (evt) {
          options.confirmButton.callback(me, evt);
        });
        footer.append(confirmButton);
      };

      $(me).off('shown.bs.modal')
      $(me).off('hidden.bs.modal')

      $(me).on('shown.bs.modal', function (e) {
        if (options.onShown) {
          options.onShown(me);
        }
      });

      $(me).on('hidden.bs.modal', function (e) {
        $(me).remove();
        if (options.onHidden) {
          options.onHidden(me);
        }
      });

      $(me).modal();
    },
    ShowMessage: function (ID, Type, Title, Message, SubMessage, Size, Icon) {

      var headerStyleClass = '';
      switch (Type) {
        case "primary":
          headerStyleClass = 'modal-primary'
          break;
        case "success":
          headerStyleClass = 'modal-success'
          break;
        case "info":
          headerStyleClass = 'modal-info'
          break;
        case "warning":
          headerStyleClass = 'modal-warning'
          break;
        case "error":
          headerStyleClass = 'modal-error'
          break;
      };

      if (!Size) {
        Size = 'modal-xs';
      }

      var customModal = $('<div class="custom-modal modal fade" tabindex="-1" role="dialog" aria-hidden="true"' + (ID ? 'id=#' + ID + '"' : '') + '>' +
        '<div class="modal-dialog ' + Size + '">' +
        '<div class="modal-content">' +
        '<div class="modal-header ' + headerStyleClass + '">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
        '<h3 class="modal-title">' + Title + '</h3>' +
        '</div>' +
        '<div class="modal-body row">' +
        (Icon ? '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
          '<i class="fa ' + Icon + '"></i>' +
          '</div>'
          : '') +
        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
        '<h3>' + Message + '</h3>' +
        '<p>' + SubMessage + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button class="btn btn-md btn-default" data-dismiss="modal">Close</button>' +
        '</div>' +
        '</div>' + //content
        '</div>' + //dialog
        '</div>'); //modal
      $('body').append(customModal);
      //$(this).find($('h3')).clone().appendTo('.custom-modal .modal-header');
      //$(this).find('.device-product, .device-details').clone().appendTo('.custom-modal .modal-body');
      //$('.custom-modal .hide').show();

      $('.custom-modal').on('hidden', function () {
        $('.custom-modal').remove();
      });

      $('.custom-modal').modal();

    },
    NotAuthorised: function (Title, Message, SubMessage, Size) {
      this.CustomModal('NotAuthorisedModal', "error", Title, Message, SubMessage, (Size ? Size : "modal-xs"), "fa-hand-stop-o");
    },
    CannotContinue: function (Title, Message, SubMessage, Size) {
      this.CustomModal('CannotContinueModal', "error", Title, Message, SubMessage, (Size ? Size : "modal-xs"), "fa-hand");
    },
    Warning: function (Title, Message, SubMessage, buttonOptions, HeaderIcon, Size) {
      this.CustomModal('WarningModal', "warning", Title, Message, SubMessage, (Size ? Size : "modal-xs"), "fa-exclamation-triangle", buttonOptions, HeaderIcon);
    },
    Error: function (Title, Message, SubMessage, buttonOptions, Size) {
      this.CustomModal('ErrorModal', "error", Title, Message, SubMessage, (Size ? Size : "modal-xs"), "fa-exclamation", buttonOptions);
    },
    custom: function (options) {
      var me = this;
      var defaultOptions = {
        id: "customModal",
        modalClass: "modal-primary",
        title: "",
        message: "",
        subMessage: "",
        size: "modal-xs",
        icon: "",
        onShown: null,
        onHidden: null,
        headerIcon: "",
        buttonOptions: {
          confirmButton: {
            callback: null,
            text: "Confirm",
            cssClass: 'btn-primary'
          },
          cancelButton: {
            callback: null,
            text: "Cancel",
            cssClass: 'btn-warning'
          }
        }
      }
      me.options = $.extend({}, defaultOptions, options)
      var customModal = $('<div class="custom-modal modal fade" tabindex="-1" role="dialog" aria-hidden="true"' + (me.options.id ? 'id=' + me.options.id + '"' : '') + '>' +
        '<div class="modal-dialog ' + me.options.size + '">' +
        '<div class="modal-content">' +
        '<div class="modal-header ' + me.options.modalClass + '">' +
        (me.options.headerIcon ? '<i class="fa ' + me.options.headerIcon + ' fa-2x"></i>' : '') +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
        '<h3 class="modal-title">' + me.options.title + '</h3>' +
        '</div>' +
        '<div class="modal-body row">' +
        (me.options.icon ? '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">' +
          '<i class="fa ' + me.options.icon + ' fa-5x"></i>' +
          '</div>'
          : '') +
        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">' +
        '<h3>' + me.options.message + '</h3>' +
        '<p>' + me.options.subMessage + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '</div>' +
        '</div>' + //content
        '</div>' + //dialog
        '</div>'); //modal

      $('body').append(customModal);

      var modal = $('div.custom-modal');
      var footer = $(modal).find("div.modal-footer");

      //add cancel
      if (me.options.buttonOptions.cancelButton.callback) {
        var cancelButton = $('<button class="btn btn-md ' + me.options.buttonOptions.cancelButton.cssClass + '">' + me.options.buttonOptions.cancelButton.text + '</button>');
        cancelButton.off('click')
        cancelButton.on('click', function (evt) {
          me.options.buttonOptions.cancelButton.callback.call(modal, evt);
        });
        footer.append(cancelButton);
      };

      //add confirm
      if (me.options.buttonOptions.confirmButton.callback) {
        var confirmButton = $('<button class="btn btn-md ' + me.options.buttonOptions.confirmButton.cssClass + '">' + me.options.buttonOptions.confirmButton.text + '</button>');
        confirmButton.off('click')
        confirmButton.on('click', function (evt) {
          me.options.buttonOptions.confirmButton.callback.call(modal, evt);
        });
        footer.append(confirmButton);
      };

      $(modal).off('shown.bs.modal')
      $(modal).off('hidden.bs.modal')

      $(modal).on('shown.bs.modal', function (e) {
        if (me.options.onShown) {
          me.options.onShown.call(me, e);
        }
      });

      $(modal).on('hidden.bs.modal', function (e) {
        $(modal).remove();
        if (me.options.onHidden) {
          me.options.onHidden.call(me, e);
        }
      });

      $(modal).modal();
    }
  },
  Resources: {
    getResourceBookingClean: function (someResourceBooking) {
      var kof = new ResourceBookingCleanObject();
      kof.Guid(someResourceBooking.Guid());
      kof.ResourceBookingID(someResourceBooking.ResourceBookingID());
      kof.ResourceID(someResourceBooking.ResourceID());
      kof.ResourceBookingTypeID(someResourceBooking.ResourceBookingTypeID());
      kof.ResourceBookingDescription(someResourceBooking.ResourceBookingDescription());
      kof.StartDateTimeBuffer(someResourceBooking.StartDateTimeBuffer());
      kof.StartDateTime(someResourceBooking.StartDateTime());
      kof.EndDateTime(someResourceBooking.EndDateTime());
      kof.EndDateTimeBuffer(someResourceBooking.EndDateTimeBuffer());
      kof.IsBeingEditedBy(someResourceBooking.IsBeingEditedBy());
      kof.InEditDateTime(someResourceBooking.InEditDateTime());
      kof.HumanResourceID(someResourceBooking.HumanResourceID());
      kof.RoomID(someResourceBooking.RoomID());
      kof.VehicleID(someResourceBooking.VehicleID());
      kof.EquipmentID(someResourceBooking.EquipmentID());
      kof.ChannelID(someResourceBooking.ChannelID());
      kof.HumanResourceShiftID(someResourceBooking.HumanResourceShiftID());
      kof.IsCancelled(someResourceBooking.IsCancelled());
      kof.HumanResourceOffPeriodID(someResourceBooking.HumanResourceOffPeriodID());
      kof.HumanResourceSecondmentID(someResourceBooking.HumanResourceSecondmentID());
      kof.ProductionHRID(someResourceBooking.ProductionHRID());
      kof.StatusCssClass(someResourceBooking.StatusCssClass());
      kof.IgnoreClashes(someResourceBooking.IgnoreClashes());
      kof.IgnoreClashesReason(someResourceBooking.IgnoreClashesReason());
      kof.RoomScheduleID(someResourceBooking.RoomScheduleID());
      kof.IsLocked(someResourceBooking.IsLocked());
      kof.IsLockedReason(someResourceBooking.IsLockedReason());
      kof.IsTBC(someResourceBooking.IsTBC());
      return kof;
    },
    getResourceBookingCleanCustom: function (Guid, ResourceBookingID, ResourceID, ResourceBookingTypeID,
      StartDateTimeBuffer, StartDateTime, EndDateTime, EndDateTimeBuffer,
      HumanResourceShiftID, HumanResourceOffPeriodID, HumanResourceSecondmentID,
      ProductionHRID, RoomScheduleID, ProductionSystemAreaID) {
      var kof = new ResourceBookingCleanObject();
      kof.Guid(Guid);
      kof.ResourceBookingID(ResourceBookingID);
      kof.ResourceID(ResourceID);
      kof.ResourceBookingTypeID(ResourceBookingTypeID);
      //kof.ResourceBookingDescription(someResourceBooking.ResourceBookingDescription());
      kof.StartDateTimeBuffer(StartDateTimeBuffer);
      kof.StartDateTime(StartDateTime);
      kof.EndDateTime(EndDateTime);
      kof.EndDateTimeBuffer(EndDateTimeBuffer);
      //kof.IsBeingEditedBy(someResourceBooking.IsBeingEditedBy());
      //kof.InEditDateTime(someResourceBooking.InEditDateTime());
      //kof.HumanResourceID(someResourceBooking.HumanResourceID());
      //kof.RoomID(someResourceBooking.RoomID());
      //kof.VehicleID(someResourceBooking.VehicleID());
      //kof.EquipmentID(someResourceBooking.EquipmentID());
      //kof.ChannelID(someResourceBooking.ChannelID());
      kof.HumanResourceShiftID(HumanResourceShiftID);
      //kof.IsCancelled(someResourceBooking.IsCancelled());
      kof.HumanResourceOffPeriodID(HumanResourceOffPeriodID);
      kof.HumanResourceSecondmentID(HumanResourceSecondmentID);
      kof.ProductionHRID(ProductionHRID);
      //kof.StatusCssClass(someResourceBooking.StatusCssClass());
      //kof.IgnoreClashes(someResourceBooking.IgnoreClashes());
      //kof.IgnoreClashesReason(someResourceBooking.IgnoreClashesReason());
      kof.RoomScheduleID(RoomScheduleID);
      kof.ProductionSystemAreaID(ProductionSystemAreaID);
      //kof.IsLocked(someResourceBooking.IsLocked());
      //kof.IsLockedReason(someResourceBooking.IsLockedReason());
      //kof.IsTBC(someResourceBooking.IsTBC());
      return kof;
    },
    getResourceBookings: function (startDate, endDate, resourceID, resourceIDsXML, resourceBookingID, resourceBookingIDsXML, afterFetch, afterFail) {
      Singular.GetDataStateless("OBLib.Resources.RSResourceList", {
        ResourceID: resourceID,
        ResourceIDs: resourceIDsXML,
        ResourceBookingID: resourceBookingID,
        ResourceBookingIDs: resourceBookingIDsXML,
        StartDate: startDate,
        EndDate: endDate,
        ResourceSchedulerID: null
      }, function (response) {
        if (response.Success) {
          if (afterFetch) { afterFetch(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Error Fetching Bookings", response.ErrorText, 3000)
          if (afterFail) { afterFail(response) }
        }
      })
    },
    IsAvailable: function (ResourceID, ResourceBookingID, StartDate, EndDate) {

    },
    getROResourceBookings: function (startDate, endDate, resourceID, resourceIDsXML, resourceBookingID, resourceBookingIDsXML, excludeResourceBookingID, excludeResourceBookingIDsXML, afterFetch, afterFail) {
      Singular.GetDataStateless("OBLib.Resources.ReadOnly.ROResourceBookingList", {
        ResourceID: resourceID,
        ResourceIDs: resourceIDsXML,
        ResourceBookingID: resourceBookingID,
        ResourceBookingIDs: resourceBookingIDsXML,
        ExcludeResourceBookingID: excludeResourceBookingID,
        ExcludeResourceBookingIDs: excludeResourceBookingIDsXML,
        StartDate: startDate,
        EndDate: endDate,
        PageNo: null,
        PageSize: null,
        SortColumn: "",
        SortAsc: true,
        PageNo: 1,
        PageSize: 1000,
        SortAsc: true,
        SortColumn: 'StartDateTime'
      }, function (response) {
        if (response.Success) {
          if (afterFetch) { afterFetch(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Error Fetching Bookings", response.ErrorText, 3000)
          if (afterFail) { afterFail(response) }
        }
      })
    }
  },
  Notifications: {
    GritterSuccess: function (Title, Text, Duration, Position) {
      Position ? Position : 'top-right';
      //success message
      $.gritter.add({
        title: Title,
        text: Text,
        class_name: 'success',
        time: Duration,
        position: Position
      })
    },
    GritterError: function (Title, Text, Duration, Position) {
      Position ? Position : 'top-right';
      //success message
      $.gritter.add({
        title: Title,
        text: Text,
        class_name: 'danger',
        time: Duration,
        position: Position
      })
    },
    GritterInfo: function (Title, Text, Duration, Position) {
      Position ? Position : 'top-right';
      //info message
      $.gritter.add({
        title: Title,
        text: Text,
        class_name: 'info',
        time: Duration,
        position: Position
      })
    },
    GritterWarning: function (Title, Text, Duration, Position) {
      Position ? Position : 'top-right';
      //info message
      $.gritter.add({
        title: Title,
        text: Text,
        class_name: 'warning',
        time: Duration,
        position: Position
      })
    },
    IsValidPhoneNumber: function (cellNum) {
      var re = new RegExp("^((\+\d{2})(\-| )?\d{3}(\-| )?\d{3}(\-| )?\d{3})|(0\d{2}(\-| )?\d{3}(\-| )?\d{4})$");
      return re.test(cellNum);
    }
  },
  Xml: {
    getXmlIDs: function (list, idProp) {
      var xmlIDs = ""
      list.Iterate(function (itm, indx) {
        if (idProp) {
          xmlIDs += "<Table ID='" + itm[idProp].toString() + "' />"
        } else {
          xmlIDs += "<Table ID='" + itm.toString() + "' />"
        }
      })
      if (xmlIDs.trim().length > 0) {
        xmlIDs = '<DataSet>' + xmlIDs + '</DataSet>'
      } else {
        xmlIDs = ""
      }
      return xmlIDs
    }
  },
  Slugs: {
    getSlugs: function (GenRefNumber, onSuccess, onFail) {
      ViewModel.CallServerMethod("GetSlugsForGenRef", {
        genRef: GenRefNumber
      }, function (response) {
        if (response.Success) {
          onSuccess(response)
        }
        else {
          if (onFail) { onFail(response) } else {
            OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000)
          }
        }
      })
    }
  },
  UI: {
    hideTab: function (tabID) {
      $('a[href="#' + tabID + '"]').tab('hide')
    },
    activateTab: function (tabID) {
      $('a[href="#' + tabID + '"]').tab('show')
    },
    getAbsoluteLeft: function (elem) {
      return elem.getBoundingClientRect().left;
    },
    getAbsoluteTop: function (elem) {
      return elem.getBoundingClientRect().top;
    },
    getTarget: function (event) {
      // code from http://www.quirksmode.org/js/events_properties.html
      if (!event) {
        event = window.event;
      }
      var target;
      if (event.target) {
        target = event.target;
      } else if (event.srcElement) {
        target = event.srcElement;
      }
      if (target.nodeType != undefined && target.nodeType == 3) {
        // defeat Safari bug
        target = target.parentNode;
      }
      return target;
    },
    getContextProperties: function (event) {
      var element = sUtil.getTarget(event);
      var tr = element.parentNode.parentNode;
      var businessObject = ko.dataFor(tr);
      return {
        event: event,
        element: element
      };
    },
    getdraggedEventProperties: function (event) {
      var clientX = event.center ? event.center.x : event.clientX;
      var clientY = event.center ? event.center.y : event.clientY;
      var element = sUtil.getTarget(event);
      //var businessObject = ko.dataFor(element);
      var timelineGroup = element['timeline-group'];
      var group = null;
      if (timelineGroup) {
        group = window.Scheduler.getGroupByGUID(timelineGroup.groupId);
      };
      return {
        event: event,
        timelineGroup: timelineGroup,
        group: group,
        pageX: event.srcEvent ? event.srcEvent.pageX : event.pageX,
        pageY: event.srcEvent ? event.srcEvent.pageY : event.pageY
      };
    },
    randomUUID: function () {
      var S4 = function () {
        return Math.floor(
          Math.random() * 0x10000 /* 65536 */
        ).toString(16);
      };
      return (
        S4() + S4() + '-' +
        S4() + '-' +
        S4() + '-' +
        S4() + '-' +
        S4() + S4() + S4()
      );
    }
  },
  Shifts: {
    getStudioSupervisorShift: function (HumanResourceShiftID, onFetchSuccess, onFetchFailed) {
      ViewModel.CallServerMethod("EditStudioSupervisorShiftServerMethod",
        {
          HumanResourceShiftID: HumanResourceShiftID
        },
        function (response) {
          if (response.Success) { onFetchSuccess(response) }
          else { onFetchFailed(response) }
        })
    },
    getStageHandShift: function (HumanResourceShiftID, onFetchSuccess, onFetchFailed) {
      ViewModel.CallServerMethod("EditStageHandShiftServerMethod",
        {
          HumanResourceShiftID: HumanResourceShiftID
        },
        function (response) {
          if (response.Success) { onFetchSuccess(response) }
          else { onFetchFailed(response) }
        })
    },
    getICRShift: function (HumanResourceShiftID, onFetchSuccess, onFetchFailed) {
      ViewModel.CallServerMethod("EditICRShiftServerMethod",
        {
          HumanResourceShiftID: HumanResourceShiftID
        },
        function (response) {
          if (response.Success) { onFetchSuccess(response) }
          else { onFetchFailed(response) }
        })
    },
    getPlayoutShift: function (HumanResourceShiftID, onFetchSuccess, onFetchFailed) {
      ViewModel.CallServerMethod("EditPlayoutOpsShiftServerMethod",
        {
          HumanResourceShiftID: HumanResourceShiftID
        },
        function (response) {
          if (response.Success) { onFetchSuccess(response) }
          else { onFetchFailed(response) }
        })
    },
    getProgrammingShift: function (HumanResourceShiftID, onFetchSuccess, onFetchFailed) {
      ViewModel.CallServerMethod("EditProgrammingShiftServerMethod",
        {
          HumanResourceShiftID: HumanResourceShiftID
        },
        function (response) {
          if (response.Success) { onFetchSuccess(response) }
          else { onFetchFailed(response) }
        })
    }
  },
  Knockout: {
    updateObservable: function (observable, json) {
      ko.mapping.fromJS(json, {
        'key': function (item) { return item.Guid() },
        'ignore': ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"],
        'include': ['_Severity', 'IsValid'],
        'copy': ['IsValid']
      },
        observable)
    },
    updateObservableFromJSON: function (observable, json, propsToIgnore) {
      return ko.mapping.fromJS(json, { 'key': function (item) { return item.Guid() }, 'ignore': propsToIgnore }, observable)
    },
    updateFromJSON: function (observable, json, mapping) {
      return ko.mapping.fromJS(json, mapping, observable)
    }
  },
  Tools: {
    enableNumericKeysOnly: function (options) {
      var me = this,
        selector = "",
        keyCodes = [8, 9, 12, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
      var defaultOptions = {
        id: "",
        cssClass: "",
      }
      me.options = $.extend({}, defaultOptions, options)
      if (me.options.cssClass != "") {
        selector = "." + me.options.cssClass
      }
      else {
        selector = "#" + me.options.id
      }
      $(selector).keydown(function (e) {
        if ($.inArray(e.keyCode, keyCodes) == -1 && !($.inArray(e.which, [89, 90]) > -1 && e.ctrlKey) && (event.keyCode < 48 || event.keyCode > 57)) {
          event.preventDefault();
        }
      })
    },
    getUrlParameter(name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
      var results = regex.exec(location.search);
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    },
    sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }
  }
};

String.prototype.endsWith = function (suffix) {
  return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

Array.prototype.FindNew = function (Property, Value) {
  for (var i = 0; i < this.length; i++) {
    if (this[i][Property] == Value)
      return this[i];
  }
};

$.fn.isOnScreen = function (partial) {

  //let's be sure we're checking only one element (in case function is called on set)
  var t = $(this).first();

  //we're using getBoundingClientRect to get position of element relative to viewport
  //so we dont need to care about scroll position
  var box = t[0].getBoundingClientRect();

  //let's save window size
  var win = {
    h: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
    w: Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
  };

  //now we check against edges of element

  //firstly we check one axis
  //for example we check if left edge of element is between left and right edge of scree (still might be above/below)
  var topEdgeInRange = box.top >= 0 && box.top <= win.h;
  var bottomEdgeInRange = box.bottom >= 0 && box.bottom <= win.h;

  var leftEdgeInRange = box.left >= 0 && box.left <= win.w;
  var rightEdgeInRange = box.right >= 0 && box.right <= win.w;


  //here we check if element is bigger then window and 'covers' the screen in given axis
  var coverScreenHorizontally = box.left <= 0 && box.right >= win.w;
  var coverScreenVertically = box.top <= 0 && box.bottom >= win.h;

  //now we check 2nd axis
  var topEdgeInScreen = topEdgeInRange && (leftEdgeInRange || rightEdgeInRange || coverScreenHorizontally);
  var bottomEdgeInScreen = bottomEdgeInRange && (leftEdgeInRange || rightEdgeInRange || coverScreenHorizontally);

  var leftEdgeInScreen = leftEdgeInRange && (topEdgeInRange || bottomEdgeInRange || coverScreenVertically);
  var rightEdgeInScreen = rightEdgeInRange && (topEdgeInRange || bottomEdgeInRange || coverScreenVertically);

  //now knowing presence of each edge on screen, we check if element is partially or entirely present on screen
  var isPartiallyOnScreen = topEdgeInScreen || bottomEdgeInScreen || leftEdgeInScreen || rightEdgeInScreen;
  var isEntirelyOnScreen = topEdgeInScreen && bottomEdgeInScreen && leftEdgeInScreen && rightEdgeInScreen;

  return partial ? isPartiallyOnScreen : isEntirelyOnScreen;

};

$.expr.filters.onscreen = function (elem) {
  return $(elem).isOnScreen(true);
};

$.expr.filters.entireonscreen = function (elem) {
  return $(elem).isOnScreen(true);
};