﻿PaymentRunBO = {
  PaymentRun_DatesOverlap: function (Value, Rule, CtlError) {
    var pr = CtlError.Object;
    if (pr.StartDate() && pr.EndDate() && pr.IsNew() && pr.IsDirty()) {
      Singular.ShowLoadingBar();
      //if (CtlError.BeginAsync('Checking Dates...')) {
      var f = new KOFormatterObject();
      ViewModel.CallServerMethod('CheckPaymentRunDatesOverlap', { PaymentRun: f.Serialise(pr) }, function (WebResult) {
        var DatesOverlap = '';
        if (WebResult.Data) {
          DatesOverlap = WebResult.Data;
          if (DatesOverlap.length > 0) {
            CtlError.AddError(DatesOverlap);
          }
        } else {
          DatesOverlap = '';
        }
        Singular.HideLoadingBar();
        //CtlError.EndAsync();
      });
      //}
    }
  }
}

CreditorInvoiceBO = {
  CalculateTotals: function (self) {
    var TotalAmtExVat = 0;
    var TotalVat = 0;
    var Total = 0
    for (i = 0; i < self.CreditorInvoiceDetailList().length; i++) {
      var CurrentDetail = self.CreditorInvoiceDetailList()[i];
      TotalAmtExVat += CurrentDetail.AmountExclVAT();
      TotalVat += CurrentDetail.VATAmount();
      Total += CurrentDetail.TotalAmount();
    };
    self.AmountExclVAT(TotalAmtExVat);
    self.VATAmount(TotalVat);
    self.TotalAmount(Total);
  },
  SetStaffNo: function (self) {
    if (self.HumanResourceID()) {
      var ROHR = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID());
      self.StaffNo(ROHR.EmployeeCode);

    } else if (self.CreditorID()) {
      var ROCr = ClientData.ROCreditorList.Find('CreditorID', self.CreditorID());
      self.StaffNo(ROCr.EmployeeCode);

    } else {
      self.StaffNo('');

    }
  },
  StaffNoValid: function (Value, Rule, CtlError) {
    var CreditorInvoiceObject = CtlError.Object;
    if (CreditorInvoiceObject.HumanResourceID() || CreditorInvoiceObject.CreditorID()) {
      var mystring = CreditorInvoiceObject.StaffNo();
      mystring = mystring.replace(/0/g, '');
      mystring = mystring.trim();
      if (mystring.length == 0) {
        CtlError.AddError('Invalid Staff No.');
      }
    }
  },
  CreditorInvoiceToString: function (self) {
    if (self.IsNew()) {
      return 'New Invoice';
    } else {
      return self.SystemInvoiceNum();
    }
  },
  TriggerPreviousInvoices: function (Value, Rule, CtlError) {
    CtlError.Object.CheckPreviousInvoices(!CtlError.Object.CheckPreviousInvoices());
  },
  CreditorInvoice_CheckPreviousInvoices: function (Value, Rule, CtlError) {
    //  //window.ctle = CtlError;
    //  //if (CtlError.BeginAsync('Checking Previous Invoices...')) {
    //  var f = new KOFormatterObject();
    //  ViewModel.CallServerMethod('CheckPreviousInvoices', { CreditorInvoice: f.Serialise(self) }, function (WebResult) {
    //    if (WebResult.Data) {
    //      self.PreviousInvoices(WebResult.Data);
    //    } else {
    //      self.PreviousInvoices("");
    //    }
    //    //CtlError.EndAsync();
    //  });
    //  //};
    //  //if ((self.GetParent().HumanResourceID() || self.GetParent().CreditorID()) && self.GetParent().InvoiceDate()) {
    //  //Singular.IsPopulatingModel = false;
    //  //Singular.IsPopulatingModel = true;
    //  //};
  },
  InvoiceDateValid: function (Value, Rule, CtlError) {
    var ci = CtlError.Object;
    var pr = ci.GetParent();
    var sd = new Date(pr.StartDate());
    var ed = new Date(pr.EndDate());
    var id = new Date(ci.InvoiceDate());
    var res = (id.getTime() >= sd.getTime() && id.getTime() <= ed.getTime());
    if (res) {
      ci.InvoiceDateInvalid("");
    } else {
      //    console.log({ obj: 'Invoice',
      //      id: ci.CreditorInvoiceID(),
      //      inv: ci.InvoiceDate(),
      //      sd: new Date(pr.StartDate()),
      //      ed: new Date(pr.EndDate()),
      //      res: (ci.InvoiceDate() >= sd && ci.InvoiceDate() <= ed)
      //    });
      CtlError.AddError('Invoice date is not with payment run dates');
      //ci.InvoiceDateInvalid("Invoice date is not with payment run dates");
    }
  }
};

CreditorInvoiceDetailBO = {
  CalculateAmounts: function (self) {
    if (self.TotalAmount() >= 0
        && self.VATAmount() >= 0
        && self.AmountExclVAT() >= 0
        && self.VatTypeID()) {
      var CreditorInvoice = self.GetParent();
      var VATSetting = ClientData.ROVATTypeList.Find('VatTypeID', self.VatTypeID())
      var VAT = self.TotalAmount() * VATSetting.VatPercentage;
      var AmountExclVAT = self.TotalAmount() - VAT;
      var TotalAmount = self.TotalAmount();
      self.VATAmount(VAT);
      self.AmountExclVAT(AmountExclVAT);
      var TotalAmtExVat = 0;
      var TotalVat = 0;
      var Total = 0
      for (i = 0; i < CreditorInvoice.CreditorInvoiceDetailList().length; i++) {
        var CurrentDetail = CreditorInvoice.CreditorInvoiceDetailList()[i];
        TotalAmtExVat += CurrentDetail.AmountExclVAT();
        TotalVat += CurrentDetail.VATAmount();
        Total += CurrentDetail.TotalAmount();
      };
      CreditorInvoice.AmountExclVAT(TotalAmtExVat);
      CreditorInvoice.VATAmount(TotalVat);
      CreditorInvoice.TotalAmount(Total);
    }
  },
  CreditorInvoiceDetail_CheckPreviousInvoices: function (Value, Rule, CtlError) {
    var ci = CtlError.Object.GetParent();
    ci.CheckPreviousInvoices(!ci.CheckPreviousInvoices());
    //    if (CtlError.BeginAsync('Checking Previous Invoices...')) {
    //      var ci = self.GetParent();
    //      var f = new KOFormatterObject();
    //      ViewModel.CallServerMethod('CheckPreviousInvoices', { CreditorInvoice: f.Serialise(ci) }, function (WebResult) {
    //        if (WebResult.Data) {
    //          ci.PreviousInvoices(WebResult.Data);
    //        } else {
    //          ci.PreviousInvoices("");
    //        }
    //        CtlError.EndAsync();
    //      });
    //    };
    //if (self.CreditorInvoiceDetailDate() && ((self.GetParent().HumanResourceID() || self.GetParent().CreditorID()) && self.GetParent().InvoiceDate())) {
    //};
  },
  InvoiceDetailDateValid: function (Value, Rule, CtlError) {
    var cid = CtlError.Object;
    var ci = CtlError.Object.GetParent();
    var pr = ci.GetParent();
    var sd = new Date(pr.StartDate());
    var ed = new Date(pr.EndDate());
    var id = new Date(cid.CreditorInvoiceDetailDate());
    var res = (id.getTime() >= sd.getTime() && id.getTime() <= ed.getTime());
    if (res) {
      cid.InvoiceDateInvalid("");
    } else {
      //    console.log({ obj: 'Detail',
      //      id: cid.CreditorInvoiceDetailID(),
      //      invd: cid.CreditorInvoiceDetailDate(),
      //      sd: new Date(pr.StartDate()),
      //      ed: new Date(pr.EndDate()),
      //      res: (cid.CreditorInvoiceDetailDate() >= sd && cid.CreditorInvoiceDetailDate() <= ed)
      //    });
      CtlError.AddError("Invoice detail date is not with payment run dates");
      //cid.InvoiceDateInvalid("Invoice detail date is not with payment run dates");
    }
  }
};

//CreditorInvoiceDetailBO = {
//  CalculateAmounts: function (self) {
//    if (self.TotalAmount() >= 0
//        && self.VATAmount() >= 0
//        && self.AmountExclVAT() >= 0
//        && self.VatTypeID()) {
//      var CreditorInvoice = self.GetParent();
//      var VATSetting = ClientData.ROVATTypeList.Find('VatTypeID', self.VatTypeID())
//      var VAT = self.TotalAmount() * VATSetting.VatPercentage;
//      var AmountExclVAT = self.TotalAmount() - VAT;
//      var TotalAmount = self.TotalAmount();
//      self.VATAmount(VAT);
//      self.AmountExclVAT(AmountExclVAT);
//      var TotalAmtExVat = 0;
//      var TotalVat = 0;
//      var Total = 0
//      for (i = 0; i < CreditorInvoice.CreditorInvoiceDetailList().length; i++) {
//        var CurrentDetail = CreditorInvoice.CreditorInvoiceDetailList()[i];
//        TotalAmtExVat += CurrentDetail.AmountExclVAT();
//        TotalVat += CurrentDetail.VATAmount();
//        Total += CurrentDetail.TotalAmount();
//      };
//      CreditorInvoice.AmountExclVAT(TotalAmtExVat);
//      CreditorInvoice.VATAmount(TotalVat);
//      CreditorInvoice.TotalAmount(Total);
//    }
//  },
//  CheckPreviousInvoices: function (Value, Rule, CtlError) {
//    var ci = CtlError.Object.GetParent();
//    ci.CheckPreviousInvoices(!ci.CheckPreviousInvoices());
//    //    if (CtlError.BeginAsync('Checking Previous Invoices...')) {
//    //      var ci = self.GetParent();
//    //      var f = new KOFormatterObject();
//    //      ViewModel.CallServerMethod('CheckPreviousInvoices', { CreditorInvoice: f.Serialise(ci) }, function (WebResult) {
//    //        if (WebResult.Data) {
//    //          ci.PreviousInvoices(WebResult.Data);
//    //        } else {
//    //          ci.PreviousInvoices("");
//    //        }
//    //        CtlError.EndAsync();
//    //      });
//    //    };
//    //if (self.CreditorInvoiceDetailDate() && ((self.GetParent().HumanResourceID() || self.GetParent().CreditorID()) && self.GetParent().InvoiceDate())) {
//    //};
//  },
//  InvoiceDetailDateValid: function (Value, Rule, CtlError) {
//    var cid = CtlError.Object;
//    var ci = CtlError.Object.GetParent();
//    var pr = ci.GetParent();


//    //var sd = new Date(pr.StartDate());
//    //var ed = new Date(pr.EndDate());
//    //var id = new Date(cid.CreditorInvoiceDetailDate());
//    //var res = (id.getTime() >= sd.getTime() && id.getTime() <= ed.getTime());
//    //if (res) {
//    //  cid.InvoiceDateInvalid("");
//    //} else {
//    //  //    console.log({ obj: 'Detail',
//    //  //      id: cid.CreditorInvoiceDetailID(),
//    //  //      invd: cid.CreditorInvoiceDetailDate(),
//    //  //      sd: new Date(pr.StartDate()),
//    //  //      ed: new Date(pr.EndDate()),
//    //  //      res: (cid.CreditorInvoiceDetailDate() >= sd && cid.CreditorInvoiceDetailDate() <= ed)
//    //  //    });
//    //  CtlError.AddError("Invoice detail date is not with payment run dates");
//    //  //cid.InvoiceDateInvalid("Invoice detail date is not with payment run dates");
//    //}
//  },
//  DatesExists: function (Value, Rule, CtlError) {
//    var cid = CtlError.Object;
//    if (cid.CreditorInvoiceDetailDate() && cid.IsDirty()) {
//      if (cid.GetParent().CheckRulesInd()) {
//        //cid.CheckRulesInd(true);
//        Singular.SendCommand("CheckCreditorInvoiceRules",
//                 {
//                   CreditorInvoiceID: cid.GetParent().CreditorInvoiceID()
//                 }, function (response) {

//                 });
//      }
//    }
//  },
//  DuplicateSupplierInvoiceNumber: function (Value, Rule, CtlError) {

//    var cid = CtlError.Object;
//    if (cid.SupplierInvoiceDetailNum() && cid.IsDirty()) {
//      if (cid.GetParent().CheckRulesInd()) {
//        Singular.SendCommand("CheckCreditorInvoiceRules",
//                 {
//                   CreditorInvoiceID: cid.GetParent().CreditorInvoiceID()
//                 }, function (response) {

//                 });
//      }
//    }
//  },
//  CreditorInvoiceDetailDateSet: function (CreditorInvoiceDetail) {
//    if (!CreditorInvoiceDetail.GetParent().CheckRulesInd()) {
//      CreditorInvoiceDetail.GetParent().CheckRulesInd(true);
//    }
//  },
//  SupplierInvoiceDetailNumSet: function (CreditorInvoiceDetail) {
//    if (!CreditorInvoiceDetail.GetParent().CheckRulesInd()) {
//      CreditorInvoiceDetail.GetParent().CheckRulesInd(true);
//    }
//  }
//};

//CreditorInvoiceBO = {
//  CalculateTotals: function (self) {
//    var TotalAmtExVat = 0;
//    var TotalVat = 0;
//    var Total = 0
//    for (i = 0; i < self.CreditorInvoiceDetailList().length; i++) {
//      var CurrentDetail = self.CreditorInvoiceDetailList()[i];
//      TotalAmtExVat += CurrentDetail.AmountExclVAT();
//      TotalVat += CurrentDetail.VATAmount();
//      Total += CurrentDetail.TotalAmount();
//    };
//    self.AmountExclVAT(TotalAmtExVat);
//    self.VATAmount(TotalVat);
//    self.TotalAmount(Total);
//  },
//  SetStaffNo: function (self) {
//    if (self.HumanResourceID()) {
//      var ROHR = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID());
//      self.StaffNo(ROHR.EmployeeCode);

//    } else if (self.CreditorID()) {
//      var ROCr = ClientData.ROCreditorList.Find('CreditorID', self.CreditorID());
//      self.StaffNo(ROCr.EmployeeCode);

//    } else {
//      self.StaffNo('');

//    }
//  },
//  StaffNoValid: function (Value, Rule, CtlError) {
//    var CreditorInvoiceObject = CtlError.Object;
//    if (CreditorInvoiceObject.HumanResourceID() || CreditorInvoiceObject.CreditorID()) {
//      var mystring = CreditorInvoiceObject.StaffNo();
//      mystring = mystring.replace(/0/g, '');
//      mystring = mystring.trim();
//      if (mystring.length == 0) {
//        CtlError.AddError('Invalid Staff No.');
//      }
//    }
//  },
//  CreditorInvoiceToString: function (self) {
//    if (self.IsNew()) {
//      return 'New Invoice';
//    } else {
//      return self.SystemInvoiceNum();
//    }
//  },
//  TriggerPreviousInvoices: function (Value, Rule, CtlError) {
//    CtlError.Object.CheckPreviousInvoices(!CtlError.Object.CheckPreviousInvoices());
//  },
//  CheckPreviousInvoices: function (Value, Rule, CtlError) {
//    //  //window.ctle = CtlError;
//    //  //if (CtlError.BeginAsync('Checking Previous Invoices...')) {
//    //  var f = new KOFormatterObject();
//    //  ViewModel.CallServerMethod('CheckPreviousInvoices', { CreditorInvoice: f.Serialise(self) }, function (WebResult) {
//    //    if (WebResult.Data) {
//    //      self.PreviousInvoices(WebResult.Data);
//    //    } else {
//    //      self.PreviousInvoices("");
//    //    }
//    //    //CtlError.EndAsync();
//    //  });
//    //  //};
//    //  //if ((self.GetParent().HumanResourceID() || self.GetParent().CreditorID()) && self.GetParent().InvoiceDate()) {
//    //  //Singular.IsPopulatingModel = false;
//    //  //Singular.IsPopulatingModel = true;
//    //  //};
//  },
//  InvoiceDateValid: function (Value, Rule, CtlError) {
//    //var ci = CtlError.Object;
//    //var pr = ci.GetParent();
//    //var sd = new Date(pr.StartDate());
//    //var ed = new Date(pr.EndDate());
//    //var id = new Date(ci.InvoiceDate());
//    //var res = (id.getTime() >= sd.getTime() && id.getTime() <= ed.getTime());
//    //if (res) {
//    //  ci.InvoiceDateInvalid("");
//    //} else {
//    //  //    console.log({ obj: 'Invoice',
//    //  //      id: ci.CreditorInvoiceID(),
//    //  //      inv: ci.InvoiceDate(),
//    //  //      sd: new Date(pr.StartDate()),
//    //  //      ed: new Date(pr.EndDate()),
//    //  //      res: (ci.InvoiceDate() >= sd && ci.InvoiceDate() <= ed)
//    //  //    });
//    //  CtlError.AddError('Invoice date is not with payment run dates');
//    //  //ci.InvoiceDateInvalid("Invoice date is not with payment run dates");
//    //}
//  },
//  CheckRulesIndSet: function (CreditorInvoice) {
//    if (CreditorInvoice.CheckRulesInd()) {
//      Singular.SendCommand("CheckCreditorInvoiceRules",
//                     {
//                       CreditorInvoiceID: CreditorInvoice.CreditorInvoiceID()
//                     }, function (response) {
//                       CreditorInvoice.CheckRulesInd(false);
//                     });
//    }
//  },
//  SupplierInvoiceNumSet: function (CreditorInvoice) {
//    if (!CreditorInvoice.CheckRulesInd()) {
//      CreditorInvoice.CheckRulesInd(true);
//    }
//  },
//  DuplicateSupplierInvoiceNumber: function (Value, Rule, CtlError) {

//    var ci = CtlError.Object;
//    if (ci.SupplierInvoiceNum() && ci.IsDirty()) {
//      if (ci.CheckRulesInd()) {
//        Singular.SendCommand("CheckCreditorInvoiceRules",
//                 {
//                   CreditorInvoiceID: ci.CreditorInvoiceID()
//                 }, function (response) {

//                 });
//      }
//    }
//  }
//}