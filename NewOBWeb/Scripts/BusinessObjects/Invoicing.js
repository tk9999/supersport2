﻿PaymentRunBO = {
  DatesOverlap: function (Value, Rule, CtlError) {
    var pr = CtlError.Object;
    if (pr.StartDate() && pr.EndDate() && pr.IsNew() && pr.IsDirty()) {
      Singular.ShowLoadingBar();
      //if (CtlError.BeginAsync('Checking Dates...')) {
      var f = new KOFormatterObject();
      ViewModel.CallServerMethod('CheckPaymentRunDatesOverlap', { PaymentRun: f.Serialise(pr) }, function (WebResult) {
        var DatesOverlap = '';
        if (WebResult.Data) {
          DatesOverlap = WebResult.Data;
          if (DatesOverlap.length > 0) {
            CtlError.AddError(DatesOverlap);
          }
        } else {
          DatesOverlap = '';
        }
        Singular.HideLoadingBar();
        //CtlError.EndAsync();
      });
      //}
    }
  },
  PaymentRunToString: function (PaymentRun) {
    return PaymentRun.MonthYearString();
  },
  CanView: function (itemName, paymentRun) {
    switch (itemName) {
      case 'CopyPaymentRunButton':
        return (paymentRun.SystemID() == 2 && !paymentRun.IsNew());
        break;
      case 'GenerateInvoicesButton':
        if (paymentRun.SystemID() == 1) {
          if (paymentRun.IsNew()) {
            return false
          } else if ((!paymentRun.IsDirty() && paymentRun.PaymentRunStatusID() == 3)) {
            return false
          } else {
            return true
          }
        }
        else {
          return false
        }
        break;
      case 'SaveButton':
        if (!paymentRun.IsDirty() && paymentRun.PaymentRunStatusID() == 3) {
          return false
        }
        else {
          return paymentRun.IsValid()
        }
        break;
      default:
        return true
        break;
    }
  },
  CanEdit: function (itemName, paymentRun) {
    switch (itemName) {
      case 'PaymentRunStatusID':
        if (paymentRun.SystemID() == 1) {
          if ((!paymentRun.IsDirty() && paymentRun.PaymentRunStatusID() == 3)) {
            return false
          }
          else {
            return true
          }
        }
        else {
          return true
        }
        break;
      default:
        return true
        break;
    }
  },
  ToString: function (PaymentRun) {
    return "Payment Run";
  }
};

ROPaymentRunBO = {
}

CreditorInvoiceBO = {
  CalculateTotals: function (self) {
    var TotalAmtExVat = 0;
    var TotalVat = 0;
    var Total = 0
    for (i = 0; i < self.CreditorInvoiceDetailList().length; i++) {
      var CurrentDetail = self.CreditorInvoiceDetailList()[i];
      TotalAmtExVat += CurrentDetail.AmountExclVAT();
      TotalVat += CurrentDetail.VATAmount();
      Total += CurrentDetail.Amount();
    };
    self.AmountExclVAT(TotalAmtExVat);
    self.VATAmount(TotalVat);
    self.Amount(Total);
  },
  SetStaffNo: function (self) {
    if (self.HumanResourceID()) {
      var ROHR = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID());
      self.StaffNo(ROHR.EmployeeCode);

    } else if (self.CreditorID()) {
      var ROCr = ClientData.ROCreditorList.Find('CreditorID', self.CreditorID());
      self.StaffNo(ROCr.EmployeeCode);

    } else {
      self.StaffNo('');

    }
  },
  StaffNoValid: function (Value, Rule, CtlError) {
    var CreditorInvoiceObject = CtlError.Object;
    if (CreditorInvoiceObject.HumanResourceID() || CreditorInvoiceObject.CreditorID()) {
      var mystring = CreditorInvoiceObject.StaffNo();
      mystring = mystring.replace(/0/g, '');
      mystring = mystring.trim();
      if (mystring.length == 0) {
        CtlError.AddError('Invalid Staff No.');
      }
    }
  },
  CreditorInvoiceToString: function (self) {
    if (self.IsNew()) {
      return 'New Invoice';
    } else {
      return self.SystemInvoiceNum();
    }
  },
  InvoiceDateValid: function (Value, Rule, CtlError) {
    var ci = CtlError.Object;
    if (ci.DateClashes().trim().length > 0) {
      CtlError.AddError(ci.DateClashes())
    }
  },
  SupplierInvoiceNumValid: function (Value, Rule, CtlError) {
    var ci = CtlError.Object;
    if (ci.SuppInvNumClashes().trim().length > 0) {
      CtlError.AddError(ci.SuppInvNumClashes())
    }
  },
  CheckRulesIndSet: function (CreditorInvoice) {
    if (CreditorInvoice.CheckRulesInd()) {
      Singular.SendCommand("CheckCreditorInvoiceRules",
                     {
                       Guid: CreditorInvoice.Guid(),
                       CreditorInvoiceID: CreditorInvoice.CreditorInvoiceID()
                     }, function (response) {
                       CreditorInvoice.CheckRulesInd(false);
                       Singular.Validation.CheckRules(CreditorInvoice);
                     });
    }
  },
  InvoiceDateSet: function (CreditorInvoice) {
    if (!CreditorInvoice.CheckRulesInd()) {
      CreditorInvoice.CheckRulesInd(true);
    }
  },
  SupplierInvoiceNumSet: function (CreditorInvoice) {
    if (!CreditorInvoice.CheckRulesInd()) {
      CreditorInvoice.CheckRulesInd(true);
    }
  },
  SelectDatesVisibleSet: function (CreditorInvoice) {

  },
  CreditorInvoiceToString: function (self) {
    if (self.HumanResourceID()) {
      return self.HumanResource()
    }
    else {
      return 'Blank Creditor Invoice'
    }
  },
  HumanResourceIDSet: function (self) {
    if (self.HumanResourceID()) {
      var ROHR = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID())
      self.EmployeeCode(ROHR.EmployeeCode)
      self.HumanResource(ROHR.PreferredFirstSurname)
    }
    else {
      self.EmployeeCode('')
      self.HumanResource('')
    }
  },
  deleteSelected: function (creditorInvoice) {
    var toRemove = []
    creditorInvoice.CreditorInvoiceDetailList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        toRemove.push(itm)
      }
    })
    toRemove.Iterate(function (itm, indx) {
      creditorInvoice.CreditorInvoiceDetailList.RemoveNoCheck(itm)
    })
  }
};

CreditorInvoiceDetailBO = {
  CalculateAmounts: function (columnName, self) {
    if (self.AmountExclVAT() >= 0 && self.VatTypeID()) {
      var CreditorInvoice = self.GetParent();
      var VATSetting = ClientData.ROVATTypeList.Find('VatTypeID', self.VatTypeID())
      var VATAmount = parseFloat(self.AmountExclVAT() * VATSetting.VatPercentage) //.toFixed(2)
      var TotalAmount = parseFloat(self.AmountExclVAT() + VATAmount) //.toFixed(2);
      //var TotalAmount = self.TotalAmount();
      self.VATAmount(VATAmount)
      self.Amount(TotalAmount)
      CreditorInvoiceBO.CalculateTotals(CreditorInvoice)
      //self.AmountExclVAT(AmountExclVAT);
      //var invTotalAmtExVat = 0;
      //var invTotalVat = 0;
      //var invTotal = 0
      //for (i = 0; i < CreditorInvoice.CreditorInvoiceDetailList().length; i++) {
      //  var CurrentDetail = CreditorInvoice.CreditorInvoiceDetailList()[i];
      //  invTotalAmtExVat += CurrentDetail.AmountExclVAT();
      //  invTotalVat += CurrentDetail.VATAmount();
      //  invTotal += CurrentDetail.Amount();
      //};
      //CreditorInvoice.AmountExclVAT(invTotalAmtExVat);
      //CreditorInvoice.VATAmount(invTotalVat);
      //CreditorInvoice.Amount(invTotal);
    }
  },
  InvoiceDetailDateValid: function (Value, Rule, CtlError) {
    var ci = CtlError.Object;
    if (ci.DateClashes().trim().length > 0) {
      CtlError.AddError(ci.DateClashes())
    }
  },
  SupplierInvoiceNumValid: function (Value, Rule, CtlError) {
    var ci = CtlError.Object;
    if (ci.SuppInvNumClashes().trim().length > 0) {
      CtlError.AddError(ci.SuppInvNumClashes())
    }
  },
  CheckDuplicateSupplierInvoiceNumber: function (Value, Rule, CtlError) {
    //var cid = CtlError.Object;
    //if (cid.SupplierInvoiceDetailNum() && cid.IsDirty()) {
    //  if (!cid.GetParent().CheckRulesInd()) { cid.GetParent().CheckRulesInd(true) }
    //}
  },
  CreditorInvoiceDetailDateSet: function (CreditorInvoiceDetail) {
    if (!CreditorInvoiceDetail.GetParent().CheckRulesInd()) {
      CreditorInvoiceDetail.GetParent().CheckRulesInd(true);
    }
  },
  SupplierInvoiceDetailNumSet: function (CreditorInvoiceDetail) {
    if (!CreditorInvoiceDetail.GetParent().CheckRulesInd()) {
      CreditorInvoiceDetail.GetParent().CheckRulesInd(true);
    }
  },
  CreditorInvoiceDetailToString: function (CreditorInvoiceDetail) {
    return "";
  }
};