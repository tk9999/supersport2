﻿ProductionSpecRequirementEquipmentTypeBO = {
  ProductionEquipmentTypeToString: function () {
    var ROEquipmentType = ClientData.ROEquipmentTypeList.Find('EquipmentTypeID', self.EquipmentTypeID());
    if (ROEquipmentType) {
      return ROEquipmentType.EquipmentType
    } else {
      return 'Equipment Type';
    }
  },
  EquipmentTypeMeetsSpecRequirement: function (self) {
    //var ParentListname = self.GetParent().SInfo.ParentList.PropertyName;
    if (!self.ProductionSpecRequirementID()) {
      if (ViewModel.CurrentProduction) {
        ProductionHelper.Spec.Rules.PositionMeetsSpecRequirement(self)
      }
    } else if (self.ProductionSpecRequirementID()) {
      if (self.IsValid() && self.Quantity != 0 && self.IsDirty()) {

      }
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var self = CtlError.Object;
    if (self.ProductionSpecRequirementID()) {
      if (self.ProductionAreaID() == 2 && !self.RoomID()) {
        CtlError.AddError('Room is required');
      }
    }
  }
}

ProductionSpecRequirementBO = {
  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    var Spec = CtlError.Object;
    if (Spec.ProductionAreaID() == 1) {
      if (!Spec.ProductionTypeID()) { CtlError.AddError('Production Type is required') }
    }
  },
  EventTypeIDValid: function (Value, Rule, CtlError) {
    var Spec = CtlError.Object;
    if (Spec.ProductionAreaID() == 1) {
      if (!Spec.EventTypeID()) { CtlError.AddError('Event Type is required') }
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var Spec = CtlError.Object;
    if (Spec.ProductionAreaID() == 2) {
      if (!Spec.RoomID()) { CtlError.AddError('Room is required') }
    }
  }
}

ProductionSpecRequirementPositionBO = {
  ProductionSpecPositionToString: function () {
    var RODiscipline = ClientData.RODisciplineList.Find('DisciplineID', self.DisciplineID());
    var ROPosition = ClientData.ROPositionList.Find('PositionID', self.PositionID());
    var str = "";
    if (RODiscipline) {
      str += RODiscipline.Discipline
    }
    if (ROPosition) {
      str += ROPosition.Position
    }
    if (str == "") { return "Spec Position" } else { return str }
  },
  PositionMeetsSpecRequirement: function (self) {
    if (!self.ProductionSpecRequirementID()) {
      if (ViewModel.CurrentProduction) {
        ProductionHelper.Spec.Rules.PositionMeetsSpecRequirement(self)
      }
    } else if (self.ProductionSpecRequirementID()) {
      if (self.IsValid() && self.Quantity != 0 && self.IsDirty()) {

      }
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var self = CtlError.Object;
    if (self.ProductionSpecRequirementID()) {
      if (self.ProductionAreaID() == 2 && !self.RoomID()) {
        CtlError.AddError('Room is required');
      }
    }
  }
}

ProductionBudgetedBO = {
  CanEdit: function (ProductionBudgeted, FieldName) {
    switch (FieldName) {
      case 'BudgetedInd':
        if (!ProductionBudgeted.RingFencedInd())
          return true;
        break;
      case 'RingFencedInd':
        if (!ProductionBudgeted.BudgetedInd())
          return true;
        break;
      case 'RecoupedInd':
        if (!ProductionBudgeted.BudgetedInd())
          return true;
        break;
      default:
        return true;
        break;
    }
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.PlayStartDateTime();
    var leti = CtlError.Object.PlayEndDateTime();

    if (!lst) {
      CtlError.AddError("Live Start Time is required");
    } else if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live Start Time must be before Live End Time");
      }
    }
  },
  DebtorNameRequired: function (Value, Rule, CtlError) {
    if (!CtlError.Object.BudgetedInd() && CtlError.Object.RecoupedInd() && CtlError.Object.DebtorID() == 0) {
      CtlError.AddError("Debtor name is required");
    }
  },
  AmountRecoupedRequired: function (Value, Rule, CtlError) {
    if (!CtlError.Object.BudgetedInd() && CtlError.Object.RecoupedInd() && CtlError.Object.TotalRecoupedAmount() == 0) {
      CtlError.AddError("Amount Recouped is required");
    }
  },
  NoRecoupReasonRequired: function (Value, Rule, CtlError) {
    if (!CtlError.Object.BudgetedInd() && !CtlError.Object.RecoupedInd()) {
      if (CtlError.Object.NoRecoupReason().trim().length == 0) {
        CtlError.AddError("Reason Required for no recoupment");
      }
    }
  },
  TotalCostRequired: function (Value, Rule, CtlError) {
    if ((!CtlError.Object.BudgetedInd() || CtlError.Object.RingFencedInd()) && CtlError.Object.RealCostAmount() == 0) {
      CtlError.AddError("Total Cost of Production Required");
    }
  },
  ProductionBudgetedBOToString: function (self) {
    return 'ProductionID: ' + self.ProductionID() + ' - ' +
           'Production Description: ' + self.ProductionDescription();
  }
}

ManualProductionBO = {
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionVenueID() && (CtlError.Object.PlaceHolderInd || CtlError.Object.OBCityInd || CtlError.Object.OBContentInd)) {
      CtlError.AddError("Production Venue is required");
    }
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.PlayStartDateTime();
    var leti = CtlError.Object.PlayEndDateTime();
    //var isPlaceholder = CtlError.Object.PlaceHolderInd();
    if ((CtlError.Object.PlaceHolderInd || CtlError.Object.OBCityInd || CtlError.Object.OBContentInd)) {
      if (!lst) {
        CtlError.AddError("Live Start Time is required");
      } else if (lst && leti) {
        if (OBMisc.Dates.IsAfter(lst, leti)) {
          CtlError.AddError("Live Start Time must be before Live End Time");
        }
      }
    }
  },
  PlayEndDateTimeValid: function (Value, Rule, CtlError) {
    var leti = CtlError.Object.PlayEndDateTime();
    var isPlaceholder = CtlError.Object.PlaceHolderInd();
    if (!leti) {
      CtlError.AddError("Live End Time is required");
    }
  }
}

ProductionBO = {

  //scenario: 'ManualProduction',
  ProductionToString: function (self) { return self.Title() },

  //set
  ProductionTypeIDSet: function () { },
  onProductionTypeIDSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {
      businessObject.ProductionTypeID(null)
      businessObject.ProductionType("")
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
    else {
      businessObject.ProductionTypeID(selectedItem.ProductionTypeID)
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
  },
  triggerProductionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionTypeIDCriteriaBeforeRefresh: function (args) {
  },
  afterProductionTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  EventTypeIDSet: function () { },
  onEventTypeIDSelected: function (selectedItem, businessObject) {
    businessObject.EventTypeID(selectedItem.EventTypeID)
  },
  triggerEventTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setEventTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterEventTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  ProductionVenueIDSet: function (self) { },
  onProductionVenueSelected: function (selectedItem, businessObject) {
    businessObject.ProductionVenueID(selectedItem.ProductionVenueID)
  },
  triggerProductionVenueAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setProductionVenueCriteriaBeforeRefresh: function (args) {
    //args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    //args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterProductionVenueRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  TeamsPlayingSet: function (self) {
    this.SetDefaultProductionDescription(self)
    //if (this.scenario == "NewRoomSchedule") {
    //  ViewModel.CurrentRoomScheduleArea().Title(self.Title())
    //  ViewModel.CurrentRoomScheduleArea().ResourceBookingDescription(self.Title() + (self.TeamsPlaying().trim().length == 0 ? '' : ' - ' + self.TeamsPlaying()))
    //}
  },
  PlayStartDateTimeSet: function (self) {

  },
  PlayEndDateTimeSet: function (self) {

  },
  TitleSet: function (self) {
    this.SetDefaultProductionDescription(self)
    //if (this.scenario == "NewRoomSchedule") {
    //  ViewModel.CurrentRoomScheduleArea().Title(self.Title())
    //  ViewModel.CurrentRoomScheduleArea().ResourceBookingDescription(self.Title() + (self.TeamsPlaying().trim().length == 0 ? '' : ' - ' + self.TeamsPlaying()))
    //}
  },

  TeamsPlayingValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.IsOBView() && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  },
  GenRefNoValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.CreationTypeID() == 1 && !CtlError.Object.SynergyGenRefNo()) {
      CtlError.AddError("Gen Ref is required");
    }
  },
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
  },
  PlayEndDateTimeValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.IsOBView() && !self.PlayEndDateTime) {
      //CtlError.AddError("Live End Time is required")
    }
  },

  //other
  SetDefaultProductionDescription: function (self) {
    self.ProductionDescription((self.Title().trim().length == 0 ? '' : self.Title()) + (self.TeamsPlaying().trim().length == 0 ? '' : ' - ' + self.TeamsPlaying()))
  },
  ProductionTypeIDChecks: function (self) {
    //if (!self.ProductionTypeID()) {
    //  self.EventTypeID(null);
    //} else {
    //  var ROProductionType = ClientData.ROProductionTypeList.Find('ProductionTypeID', self.ProductionTypeID())
    //  var ROEventType = null;
    //  if (ROProductionType) {
    //    ROEventType = ClientData.ROEventTypeList.Filter('EventTypeID', self.EventTypeID())
    //  }
    //  if (ROEventType.length == 0) {
    //    self.EventTypeID(null);
    //  }
    //}
  },
  CheckCanReconcile: function (self) {
    //ProductionHelper.Production.Rules.CheckCanReconcile(self);
  },
  SetupAutoServices: function (self) {
    //ProductionHelper.Production.Events.ProductionVenueIDSet(self);
  },
  SetVenueConfirmedDate: function (self) {
    //ProductionHelper.Production.Methods.SetVenueConfirmedDate(self)
  },

  //saving
  save: function (production, onSaveSuccess, onSaveFailed) {
    var me = this
    if (!production.IsProcessing()) {
      production.IsProcessing(true)
      ViewModel.CallServerMethod("SaveProduction", {
        Production: production.Serialise()
      },
      function (response) {
        if (response.Success) {
          onSaveSuccess(response)
          OBMisc.Notifications.GritterSuccess('Production Saved', "Production Saved Successfully", 1000)
        }
        else {
          onSaveFailed(response)
          OBMisc.Notifications.GritterError('Error Saving Production', response.ErrorText, 3000)
        }
        production.IsProcessing(false)
      })
    }
  },

  getProduction: function (productionID, isOBView, isRoomView, onFetchSuccess, onFetchFailed) {
    ViewModel.CallServerMethod("GetProductionList",
    {
      ProductionID: productionID,
      IsOBView: isOBView,
      IsRoomView: isRoomView
    },
    function (response) {
      if (response.Success) {
        onFetchSuccess(response)
      }
      else {
        onFetchFailed(response)
        OBMisc.Notifications.GritterError('Error Fetching Production', response.ErrorText, 3000)
      }
    })
  },

  //cans
  canSave: function (production) {
    return production.IsValid()
  }

}

ProductionDetailBO = {

  saveManualProduction: function (manualProduction, afterSuccess, afterFail) {
    ViewModel.CallServerMethod("SaveManualProduction",
                              {
                                ManualProduction: manualProduction.Serialise()
                              },
                              function (response) {
                                if (response.Success) {
                                  if (afterSuccess) { afterSuccess(response) }
                                }
                                else {
                                  OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
                                  if (afterFail) { afterFail(response) }
                                }
                              })
  },

  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionTypeID()) {
      CtlError.AddError("Production Type is required");
    }
  },
  EventTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.EventTypeID()) {
      CtlError.AddError("Event Type is required");
    }
  },
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  },
  TeamsValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  },
  GenRefNoValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.CreationTypeID() == 1 && !CtlError.Object.SynergyGenRefNo()) {
      CtlError.AddError("Gen Ref is required");
    }
  },
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    //if (!CtlError.Object.ProductionVenueID()) {
    //  CtlError.AddError("Production Venue is required");
    //}
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    //var lst = CtlError.Object.PlayStartDateTime();
    //var leti = CtlError.Object.PlayEndDateTime();
    //if (!lst) {
    //  CtlError.AddError("Live Start Time is required");
    //} else if (lst && leti) {
    //  if (OBMisc.Dates.IsAfter(lst, leti)) {
    //    CtlError.AddError("Live Start Time must be before Live End Time");
    //  }
    //}
  },
  PlayEndDateTimeValid: function (Value, Rule, CtlError) {
    //var leti = CtlError.Object.PlayEndDateTime();
    //var isPlaceholder = CtlError.Object.PlaceHolderInd();
    //if (!leti) {
    //  CtlError.AddError("Live End Time is required");
    //}
  },

  ProductionTypeIDSet: function () { },
  onProductionTypeIDSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {
      businessObject.ProductionTypeID(null)
      businessObject.ProductionType("")
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
    else {
      businessObject.ProductionTypeID(selectedItem.ProductionTypeID)
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
  },
  triggerProductionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionTypeIDCriteriaBeforeRefresh: function (args) {
  },
  afterProductionTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  EventTypeIDSet: function () { },
  onEventTypeIDSelected: function (selectedItem, businessObject) {
    businessObject.EventTypeID(selectedItem.EventTypeID)
  },
  triggerEventTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setEventTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterEventTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  ProductionVenueIDSet: function () { },
  onProductionVenueIDSelected: function (selectedItem, businessObject) {
    //businessObject.EventTypeID(selectedItem.EventTypeID)
  },
  triggerProductionVenueIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setProductionVenueIDCriteriaBeforeRefresh: function (args) {
    //args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterProductionVenueIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  }
}