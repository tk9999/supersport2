﻿ApplyResultBO = {
  AddAnywayReasonValid: function (Value, Rule, CtlError) {
    var ar = CtlError.Object;
    if ([1, 2].indexOf(ar.ActionTypeID()) >= 0
        && ar.AddAnyway()) {
      if (ar.AddAnywayReason().trim().length == 0) {
        CtlError.AddError("Reason for adding is required");
      }
    };
  },
  isProductionHRResourceBookingOption: function (applyResult) {
    return ([1, 2].indexOf(applyResult.ActionTypeID()) >= 0);
  },
  addProductionHRResourceBooking: function (applyResult) {
    var serialised = applyResult.Serialise(true);
    applyResult.IsProcessing(true);
    ViewModel.CallServerMethod("ProductionHRAddResourceBooking", {
      applyResult: serialised
    }, function (response) {
      applyResult.HasProcessed(true);
      if (response.Success) {
        applyResult.ProcessedSuccessfully(true);
        applyResult.ProcessedSuccessfullyMessage(applyResult.ResourceName() + ' has been added to ' + applyResult.BookingDescription());
        applyResult.MessageTypeID(1);
      } else {
        applyResult.ProcessedSuccessfully(false);
        applyResult.ProcessingFailedMessage(applyResult.ResourceName() + ' could not be added to ' + applyResult.BookingDescription());
        applyResult.MessageTypeID(3);
      }
      applyResult.IsProcessing(false);
    });
    serialised = null; //cleanup
  },
  areOptionsVisible: function (applyResult) {
    if (applyResult.HasProcessed()) {
      return false
    } else {
      if (!applyResult.HasProcessed() && !applyResult.AddAnyway()) {
        return true;
      } else {
        return applyResult.AddAnyway();
      }
    }
  },
  areOptionRequirementsVisible: function (applyResult) {
    if (applyResult.HasProcessed()) {
      return false
    } else {
      return applyResult.AddAnyway();
    }
  },
  isProcessingCss: function (applyResult) {
    var base = 'apply-alert-busy loading-custom';
    if (applyResult.IsProcessing()) {
      base += ' go'
    }
    return base;
  },
  optionRequirementCss: function (applyResult) {
    var base = 'apply-result-options-requirements slideInDown';
    if (applyResult.AddAnyway()) {
      base += ' go'
    }
    return base;
  }
}

ResourceSchedulerBO = {
  ResourceSchedulerBOToString: function (self) {
    return ""
  },
  getScheduler: function (id, onSuccess, onFail) {
    ViewModel.CallServerMethod("FetchResourceScheduler",
                               {
                                 ResourceSchedulerID: id,
                                 FetchGroups: true,
                                 FetchSubGroups: true,
                                 FetchSubGroupResources: true
                               },
                               function (response) {
                                 if (response.Success) {
                                   onSuccess(response)
                                 }
                                 else {
                                   OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 2000)
                                   onFail(response)
                                 }
                               })
  }
  //getScheduler: function (id) {
  //  Singular.GetDataStateless("OBLib.ResourceSchedulers.New.ResourceSchedulerList", {
  //    ResourceSchedulerID: id,
  //    FetchGroups: true,
  //    FetchSubGroups: true,
  //    FetchSubGroupResources: true
  //  },
  //  function (response) {
  //    if (response.Success) {
  //      ViewModel.EditResourceScheduler.Set(response.Data[0])

  //      //$('#EditSchedulerModal label.tree-toggler').click(function () {
  //      //  var icon = $(this).children(".fa");
  //      //  if (icon.hasClass("fa-folder-o")) {
  //      //    icon.removeClass("fa-folder-o").addClass("fa-folder-open-o");
  //      //  } else {
  //      //    icon.removeClass("fa-folder-open-o").addClass("fa-folder-o");
  //      //  }
  //      //  $(this).parent().children('ul.tree').toggle(300, function () {
  //      //    $(this).parent().toggleClass("open");
  //      //    $(".tree .nscroller").nanoScroller({ preventPageScrolling: true });
  //      //  })
  //      //})

  //      $("#groupList").sortable({
  //        delay: 100,
  //        placeholder: "ui-state-highlight",
  //        update: function (event, ui) {
  //          console.log(event, ui)
  //          var values = $("#groupList").sortable('toArray')
  //          var id = ui.item[0].id;
  //          var numItemsAbove = $(ui.item[0]).parents('li').length
  //          var grp = ko.dataFor(ui.item[0])
  //          console.log(id, numItemsAbove, grp)
  //        }
  //      })

  //      $("#subgroups").sortable({
  //        delay: 100,
  //        placeholder: "ui-state-highlight",
  //        update: function (event, ui) {
  //          console.log(event, ui)
  //          var values = $("#subgroups").sortable('toArray')
  //          var id = ui.item[0].id;
  //          var numItemsAbove = $(ui.item[0]).parents('li').length
  //          var subGrp = ko.dataFor(ui.item[0])
  //          console.log(id, numItemsAbove, subGrp)
  //        }
  //      })

  //      $("#subgroupresources").sortable({
  //        delay: 100,
  //        placeholder: "ui-state-highlight",
  //        update: function (event, ui) {
  //          console.log(event, ui)
  //          var values = $("#subgroupresources").sortable('toArray')
  //          var id = ui.item[0].id;
  //          var numItemsAbove = $(ui.item[0]).parents('li').length
  //          var res = ko.dataFor(ui.item[0])
  //          console.log(id, numItemsAbove, res)
  //        }
  //      })

  //    } else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to retrieve the scheduler', response.ErrorText, null)
  //    }
  //  })
  //},
  //editUsers: function (resourceScheduler) {
  //  //Singular.GetDataStateless("OBLib.ResourceSchedulers.RSGroupList", {
  //  //  ResourceSchedulerID: resourceScheduler.ResourceSchedulerID()
  //  //},
  //  //function (Response) {

  //  //})
  //},
  //editGroups: function (resourceScheduler) {
  //  Singular.GetDataStateless("OBLib.ResourceSchedulers.New.ResourceSchedulerGroupList", {
  //    ResourceSchedulerID: resourceScheduler.ResourceSchedulerID()
  //  },
  //  function (response) {
  //    if (response.Success) {
  //      ViewModel.EditResourceScheduler().ResourceSchedulerGroupList.Set(response.Data)
  //      //$("#RSGroupList").addClass("go")
  //      //$("#RSSubGroupList").removeClass("go")
  //      //$("#RSSubGroupResourceList").removeClass("go")
  //    }
  //    else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to retrieve the groups', response.ErrorText, null)
  //    }
  //  })
  //},
  //editGroup: function (group, element) {
  //  Singular.GetDataStateless("OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroupList", {
  //    ResourceSchedulerID: group.GetParent().ResourceSchedulerID(),
  //    ResourceSchedulerGroupID: group.ResourceSchedulerGroupID()
  //  },
  //  function (response) {
  //    if (response.Success) {
  //      ViewModel.SelectedGroup(group)
  //      ViewModel.SelectedGroup().ResourceSchedulerSubGroupList.Set(response.Data)
  //      //$("#RSGroupList").removeClass("go")
  //      //$("#RSSubGroupList").addClass("go")
  //      //$("#RSSubGroupResourceList").removeClass("go")
  //    }
  //    else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to retrieve the sub groups', response.ErrorText, null)
  //    }
  //  })
  //},
  //newGroup: function (scheduler) {
  //  var ng = scheduler.ResourceSchedulerGroupList.AddNew()
  //  ng.ResourceSchedulerID(scheduler.ResourceSchedulerID())
  //},
  //saveGroup: function (group) {
  //  var grp = group.Serialise(true)
  //  ViewModel.CallServerMethod("SaveGroup", {
  //    group: grp
  //  }, function (response) {
  //    if (response.Success) {
  //      KOFormatterFull.Deserialise(response.Data, group)
  //      //group.Set(response.Data)
  //    } else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to save the group', response.ErrorText, null)
  //    }
  //  })
  //},
  //deleteGroup: function (group) {
  //  var lst = group.GetParent()
  //  var grp = group.Serialise(true)
  //  ViewModel.CallServerMethod("DeleteGroup", {
  //    group: grp
  //  }, function (response) {
  //    if (response.Success) {
  //      lst.ResourceSchedulerGroupList.RemoveNoCheck(group)
  //    } else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to save the sub-group', response.ErrorText, null)
  //    }
  //  })
  //},
  //editSubGroup: function (subGroup, element) {
  //  Singular.GetDataStateless("OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroupResourceList", {
  //    ResourceSchedulerSubGroupID: subGroup.ResourceSchedulerSubGroupID()
  //  },
  //  function (response) {
  //    if (response.Success) {
  //      ViewModel.SelectedSubGroup(subGroup)
  //      ViewModel.SelectedSubGroup().ResourceSchedulerSubGroupResourceList.Set(response.Data)
  //      //$("#RSGroupList").removeClass("go")
  //      //$("#RSSubGroupList").removeClass("go")
  //      //$("#RSSubGroupResourceList").addClass("go")
  //      //group.RSSubGroupResourceList.Set(response.Data)
  //    }
  //    else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to retrieve the groups', response.ErrorText, null)
  //    }
  //  })
  //},
  //newSubGroup: function (group) {
  //  var nsg = group.ResourceSchedulerSubGroupResourceList.AddNew()
  //  nsg.RSGroupID(group.RSGroupID())
  //},
  //saveSubGroup: function (subGroup) {
  //  var sgrp = subGroup.Serialise(true)
  //  ViewModel.CallServerMethod("SaveSubGroup", {
  //    subGroup: sgrp
  //  }, function (response) {
  //    if (response.Success) {
  //      KOFormatterFull.Deserialise(response.Data, subGroup)
  //      //subGroup.Set(response.Data)
  //    } else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to save the sub-group', response.ErrorText, null)
  //    }
  //  })
  //},
  //deleteSubGroup: function (subGroup) {
  //  var lst = subGroup.GetParent()
  //  var sgrp = subGroup.Serialise(true)
  //  ViewModel.CallServerMethod("DeleteSubGroup", {
  //    subGroup: sgrp
  //  }, function (response) {
  //    if (response.Success) {
  //      lst.ResourceSchedulerSubGroupList.RemoveNoCheck(subGroup)
  //    } else {
  //      OBMisc.Modals.Error('Error Processing', 'An error occured while trying to save the sub-group', response.ErrorText, null)
  //    }
  //  })
  //},
  //backToGroups: function () {
  //  ViewModel.SelectedGroup(null)
  //},
  //backToSubGroups: function () {
  //  ViewModel.SelectedResourceSchedulerSubGroup(null)
  //},
}

ResourceSchedulerGroupBO = {
  ResourceSchedulerGroupBOToString: function (self) { return "" },

  //Set
  SelectedByDefaultSet: function (self) {
    self.ResourceSchedulerSubGroupList().Iterate(function (sGroup, sGroupIndx) {
      sGroup.SelectedByDefault(self.SelectedByDefault())
    })
  }
}

ResourceSchedulerSubGroupBO = {
  ResourceSchedulerSubGroupBOToString: function (self) { return "" },
  FreelancersClicked: function (self) {
    if (self.Freelancers() == null) {
      self.Freelancers(true)
    } else if (self.Freelancers()) {
      self.Freelancers(false)
    } else if (self.Freelancers() == false) {
      self.Freelancers(null)
    }
  },
  FreelancersButtonText: function (self) {
    if (self.Freelancers() == null) {
      return "All Contract Types"
    } else if (self.Freelancers()) {
      return "Freelancers/ISP"
    } else if (self.Freelancers() == false) {
      return "Perm/Contract"
    }
  },
  FreelancersIconCss: function (self) {
    if (self.Freelancers() == null) {
      return "fa fa-minus"
    } else if (self.Freelancers()) {
      return "fa fa-fire"
    } else if (self.Freelancers() == false) {
      return "fa fa-drupal"
    }
  },

  //Discipline
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.HRProductionAreaID()
  },
  afterDisciplineIDRefreshAjax: function (args) { },
  onDisciplineIDSelected: function (item, businessObject) { },

  //Vehicle
  triggerVehicleTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setVehicleTypeIDCriteriaBeforeRefresh: function (args) {

  },
  afterVehicleTypeIDRefreshAjax: function (args) { },
  onVehicleTypeIDSelected: function (item, businessObject) { },

  //Team Number
  triggerSystemTeamNumberIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setSystemTeamNumberIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
  },
  afterSystemTeamNumberIDRefreshAjax: function (args) { },
  onSystemTeamNumberIDSelected: function (item, businessObject) { },

  //Set
  DisciplineIDSet: function (self) { },
  SelectedByDefaultSet: function (self) {
    self.ResourceSchedulerSubGroupResourceList().Iterate(function (sGroupRes, sGroupResIndx) {
      sGroupRes.SelectedByDefault(self.SelectedByDefault())
    })
  },
  VehicleTypeIDSet: function (self) { },
  SystemTeamNumberIDSet: function (self) { }
}

ResourceSchedulerSubGroupResourceBO = {
  ResourceSchedulerSubGroupResourceBOToString: function (self) { return "" }
}