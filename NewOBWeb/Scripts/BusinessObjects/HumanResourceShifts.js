﻿HumanResourceShiftBaseBO = {
  //set expression
  ShiftStartDateSet: function (self) {
    if (!self.IsProcessing()) {
      self.ScheduleDate(self.StartDateTime())
      if (self.ResourceBookingList) {
        var resourceBooking = self.ResourceBookingList()[0] //.Find('HumanResourceShiftID', self.HumanResourceShiftID())
        if (resourceBooking) {
          resourceBooking.StartDateTime(self.StartDateTime())
        }
      }
      HumanResourceShiftBaseBO.checkClashes(self)
    }
  },
  ShiftEndDateSet: function (self) {
    if (!self.IsProcessing()) {
      if (self.ResourceBookingList) {
        var resourceBooking = self.ResourceBookingList()[0] //.Find('HumanResourceShiftID', self.HumanResourceShiftID())
        if (resourceBooking) {
          resourceBooking.EndDateTime(self.EndDateTime())
        }
      }
      HumanResourceShiftBaseBO.checkClashes(self)
    }
  },
  ShiftTypeIDSet: function (self) {
    if (self.ShiftTypeID()) {
      var st = ClientData.ROSystemAreaShiftTypeList.Find('ShiftTypeID', self.ShiftTypeID())
      //update Shift
      if (st) {
        if (st.StartTime && st.EndTime) {
          //update times
          var sd = moment(new Date(self.StartDateTime())).startOf('day').toDate(),
            ed = moment(new Date(self.StartDateTime())).startOf('day').toDate();
          var sTimes = st.StartTime.split(':')
          var sdHr = parseInt(sTimes[0])
          var sdMin = parseInt(sTimes[1])
          var eTimes = st.EndTime.split(':')
          var edHr = parseInt(eTimes[0])
          var edMin = parseInt(eTimes[1])
          var newSD = moment(sd).set({ 'hour': sdHr, 'minute': sdMin }).toDate()
          var newED = moment(ed).set({ 'hour': edHr, 'minute': edMin }).toDate()
          var endDateIsNextDay = moment(newSD).isAfter(moment(newED))
          if (endDateIsNextDay) {
            var correctedEd = moment(newED).add(1, 'days')
            newED = correctedEd.toDate()
          }
          self.StartDateTime(newSD)
          self.EndDateTime(newED)
        }
      }
      //update ResourceBooking
      if (self.ResourceBookingList) {
        if (self.ResourceBookingList().length == 1) {
          var resourceBooking = self.ResourceBookingList()[0]

          //update resource booking times
          resourceBooking.StartDateTimeBuffer(null)
          resourceBooking.StartDateTime(self.StartDateTime())
          resourceBooking.EndDateTime(self.EndDateTime())
          resourceBooking.EndDateTimeBuffer(null)

          //update status
          resourceBooking.StatusCssClass(st.StatusCssClass)

          //update description
          resourceBooking.ResourceBookingDescription(st.ShiftType)

          //check clashes
          HumanResourceShiftBaseBO.checkClashes(self)
        }
      }
    }
  },
  HumanResourceIDSet: function (self) { },
  SupervisorAuthIndSet: function (self) {
    if (self.SupervisorAuthInd()) {
      self.SupervisorAuthBy(ViewModel.CurrentUserID())
      self.SupervisorAuthByName(ViewModel.CurrentUserName())
      self.SuperAuthDateTime(new Date())
    } else if (self.SupervisorAuthInd() == false) {
      self.SupervisorAuthBy(ViewModel.CurrentUserID())
      self.SupervisorAuthByName(ViewModel.CurrentUserName())
      self.SuperAuthDateTime(new Date())
    } else {
      self.SupervisorAuthBy(null)
      self.SupervisorAuthByName("")
      self.SuperAuthDateTime(null)
    }
  },
  ManagerAuthIndSet: function (self) {
    if (self.ManagerAuthInd()) {
      self.ManagerAuthBy(ViewModel.CurrentUserID())
      self.ManagerAuthByName(ViewModel.CurrentUserName())
      self.ManagerAuthDateTime(new Date())
    } else if (self.ManagerAuthInd() == false) {
      self.ManagerAuthBy(ViewModel.CurrentUserID())
      self.ManagerAuthByName(ViewModel.CurrentUserName())
      self.ManagerAuthDateTime(new Date())
    } else {
      self.ManagerAuthBy(null)
      self.ManagerAuthByName("")
      self.ManagerAuthDateTime(null)
    }
  },
  StaffAcknowledgeIndSet: function (self) {
    if (self.StaffAcknowledgeInd()) {
      self.StaffAcknowledgeBy(ViewModel.CurrentUserID())
      self.StaffAcknowledgeByName(ViewModel.CurrentUserName())
      self.StaffAcknowledgeDateTime(new Date())
    } else if (self.StaffAcknowledgeInd() == false) {
      self.StaffAcknowledgeBy(ViewModel.CurrentUserID())
      self.StaffAcknowledgeByName(ViewModel.CurrentUserName())
      self.StaffAcknowledgeDateTime(new Date())
    } else {
      self.StaffAcknowledgeBy(null)
      self.StaffAcknowledgeByName("")
      self.StaffAcknowledgeDateTime(null)
    }
  },

  //dropdowns
  setShiftTypeIDCriteriaBeforeRefresh: function (args) {
    args.Object.IsProcessing(true)
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  triggerShiftTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  afterShiftTypeIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onShiftTypeIDSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ShiftType(selectedItem.ShiftType);
      businessObject.MealReimbursementInd(selectedItem.MealReimbursement);
    }
    else {
      businessObject.ShiftTypeID(null)
      businessObject.ShiftType("")
    }
  },

  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Object.IsProcessing(true)
    args.Data.HumanResourceID = args.Object.HumanResourceID()
    //args.SystemID = self.SystemID()
    //args.ProductionAreaID = self.ProductionAreaID()
    //args.DisciplineID = self.DisciplineID()
    //args.ShiftDate = new Date(self.ScheduleDate()).format("dd MMM yyyy")
  },
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  afterDisciplineIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onDisciplineIDSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.Discipline(selectedItem.Discipline)
    }
    else {
      businessObject.DisciplineID(null)
      businessObject.Discipline("")
    }
  },

  setDisciplineCriteriaBeforeRefresh: function (args) {
    args.Object.IsProcessing(true)
    args.Data.HumanResourceID = args.Object.HumanResourceID()
    //args.SystemID = self.SystemID()
    //args.ProductionAreaID = self.ProductionAreaID()
    //args.DisciplineID = self.DisciplineID()
    //args.ShiftDate = new Date(self.ScheduleDate()).format("dd MMM yyyy")
  },
  triggerDisciplineAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  afterDisciplineRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onDisciplineSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.Discipline(selectedItem.Discipline)
    }
    else {
      businessObject.DisciplineID(null)
      businessObject.Discipline("")
    }
  },

  setProductionAreaStatusIDCriteriaBeforeRefresh: function (args) {
    args.Object.IsProcessing(true)
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  triggerProductionAreaStatusIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  afterProductionAreaStatusIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onProductionAreaStatusIDSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaStatus(selectedItem.ProductionAreaStatus)
    }
    else {
      businessObject.ProductionAreaStatusID(null)
      businessObject.ProductionAreaStatus("")
    }
  },

  triggerHumanResourceIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setHumanResourceIDCriteriaBeforeRefresh: function (args) {
    //if (args.Object.StartDateTime() && args.Object.EndDateTime() && args.Object.DisciplineID()
    //    && args.Object.SystemID() && args.Object.ProductionAreaID()) {
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.SystemID = args.Object.SystemID()
    args.Data.CallTime = null
    args.Data.WrapTime = null
    //}
  },
  afterHumanResourceIDRefreshAjax: function (args) {

  },
  onHumanResourceIDSelected: function (item, businessObject) {
    if (item) {
      businessObject.HumanResourceID(item.HumanResourceID)
      businessObject.HumanResource(item.HRName)
      businessObject.ResourceID(item.ResourceID)
    } else {
      businessObject.HumanResource("")
      businessObject.HumanResourceID(null)
      businessObject.ResourceID(null)
    }
    //the value of 'this', in the context of this method is the dropdown control and not the RoomScheduleAreaProductionHumanResourceBO js object
    //RoomScheduleAreaProductionHumanResourceBO.checkHumanResource(businessObject)
  },
  onCellCreate: function (element, index, columnData, businessObject) {
    element.parentElement.style.width = "520px"
    element.parentNode.className = "hr-availability-list"
    element.className = 'hr-availability';
    element.innerHTML = '<div class="avatar">' +
      '<img src="' + window.SiteInfo.InternalPhotosPath + businessObject.ImageFileName + '" alt="Avatar">' +
      '</div>' +
      '<div class="info">' +
      '<table class="table table-condensed no-border">' +
      '<tbody>' +
      //header
      '<tr>' +
      '<td style="text-align:left;"><h4>' + businessObject.HRName + '</h4></td>' +
      '<td style="text-align:center; vertical-align:middle;"><span class="' + businessObject.ContractTypeCss + '">' + businessObject.ContractType + '</span></td>' +
      '<td style="text-align:right;"><h5>' + businessObject.TotalHours.toString() + ' / ' + businessObject.RequiredHours.toString() + ' Hours</h5></td>' +
      '</tr>' +
      //progress bar
      '<tr>' +
      '<td colspan="3">' +
      '<div class="progress small">' +
      '<div class="' + businessObject.ProgressBarCss + '" style="width:' + businessObject.HrsPercString + '"></div>' +
      '</div>' +
      '</td>' +
      '</tr>' +
      //other
      '<tr>' +
      '<td colspan="3"><span title="' + businessObject.ClashesTitle + '" class="' + businessObject.ClashesCss + '" style="width:100%;">' + businessObject.IsAvailableString + '</span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PublicHolidaysString + '" class="label label-info">' + businessObject.PublicHolidayCount.toString() + ' Public Holidays </span></td>' +
      '<td><span title="' + businessObject.OffWeekendsString + '" class="label label-info">' + businessObject.OffWeekendCount.toString() + ' Off Weekend/s </span></td>' +
      '<td><span title="' + businessObject.ConsecutiveDaysString + '" class="' + businessObject.ConsecutiveDaysCss + '">' + businessObject.ConsecutiveDays.toString() + ' Consecutive Days </span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PrevShiftEndString + '" class="' + businessObject.ShortfallPrevCss + '">' + businessObject.ShortfallPrevString + ' Shortfall </span></td>' +
      '<td></td>' +
      '<td><span title="' + businessObject.NextShiftStartString + '" class="' + businessObject.ShortfallNextCss + '">' + businessObject.ShortfallNextString + ' Shortfall </span></td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</div>'
  },

  //validation
  SupervisorAuthIndValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.SupervisorAuthInd() === false && obj.SupervisorRejectedReason().trim() === "") {
      CtlError.AddError("Sup Reject Reason is required")
    }
    if (obj.BiometricChecksEnabled()) { 
      if (obj.SupervisorAuthInd() === true && obj.AccessFlagID() && obj.AccessFlagID() > 0 && obj.SupervisorRejectedReason().trim() === "") {
        CtlError.AddError("Authorising a flagged Shift, Reason is required")
      }
    }
  },
  ManagerAuthIndValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.ManagerAuthInd() === false && obj.ManagerRejectedReason().trim() === "") {
      CtlError.AddError("Man Reject Reason is required")
    }
    if (obj.BiometricChecksEnabled()) {
      if (obj.ManagerAuthInd() === true && obj.AccessFlagID() && obj.AccessFlagID() > 0 && obj.ManagerRejectedReason().trim() === "") {
        CtlError.AddError("Authorising a flagged Shift, Reason is required")
      }
    }
  },
  StaffAckIndValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.StaffAcknowledgeInd() === false && obj.StaffDisputeReason().trim() === "") {
      CtlError.AddError("Dispute Reason is required")
    }
  },

  //auth
  supervisorAuthText: function (shift) {
    if (shift.SupervisorAuthInd() || shift.SupervisorAuthInd() == false) {
      return shift.SupervisorAuthByName() + ' ' + new Date(shift.SuperAuthDateTime()).format('ddd dd MMM yy HH:mm')
    }
    else {
      return ""
    }
  },
  managerAuthText: function (shift) {
    if (shift.ManagerAuthInd() || shift.ManagerAuthInd() == false) {
      return shift.ManagerAuthByName() + ' ' + new Date(shift.ManagerAuthDateTime()).format('ddd dd MMM yy HH:mm')
    }
    else {
      return ""
    }
  },
  staffAuthText: function (shift) {
    if (shift.StaffAcknowledgeInd() || shift.StaffAcknowledgeInd() == false) {
      return shift.StaffAcknowledgeByName() + ' ' + new Date(shift.StaffAcknowledgeDateTime()).format('ddd dd MMM yy HH:mm')
    }
    else {
      return ""
    }
  },
  //biometrics
  startEndCss: function (log) {
    if (log.StartEndInd()) {
      return 'SelectRow'
    }
    return 'UnSelectRow'
  },
  getBiometricLogs: function (shift) {
    AccessTerminalShiftBO.get({
      criteria: {
        HumanResourceShiftID: shift.HumanResourceShiftID(),
        PageNo: 1,
        PageSize: 100,
        SortAsc: true,
        SortColumn: "AccessDateTime"
      },
      onSuccess: function (response) {
        shift.ROAccessTerminalShiftList.Set(response.Data)
        OBMisc.Notifications.GritterSuccess("Biometrics fetched", "", 250)
      },
      onFail: function (response) {
        OBMisc.Notifications.GritterError("Fetch biometrics failed", response.ErrorText, 1000)
      }
    })
  },
  //audit
  refreshAudit: function (shift) {
    shift.IsProcessing(true)
    ROObjectAuditTrailBO.get({
      criteria: {
        ForScenario: "Shift",
        HumanResourceShiftID: shift.HumanResourceShiftID()
      },
      onSuccess: function (response) {
        shift.ROObjectAuditTrailList.Set(response.Data)
        shift.IsProcessing(false)
      },
      onFail: function (response) {
        OBMisc.Notifications.GritterError("Failed to fecth audit", response.ErrorText, 1000)
        shift.IsProcessing(false)
      }
    })
  },
  //other
  StartBeforeEnd: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    if (Shift.EndDateTime() != undefined && Shift.StartDateTime() != undefined) {
      var start = new Date(Shift.StartDateTime());
      var end = new Date(Shift.EndDateTime());
      var millisBetween = end.getTime() - start.getTime();
      if (millisBetween < 0) {
        CtlError.AddError("Start date must be before end date");
      }
    }
  },
  GetFilteredShiftTypes: function (List, self) {
    var filteredList = [];
    List.Iterate(function (shiftType, indx) {
      if (shiftType.SystemID == self.SystemID()
        && shiftType.ProductionAreaID == self.ProductionAreaID()) {
        filteredList.push(shiftType)
      }
    })
    return filteredList
  },
  checkClashes: function (self) {
    if (self.ResourceID() && self.StartDateTime() && self.EndDateTime()) {
      self.IsProcessing(true)
      ResourceBookingBO.getClashes([{
        Guid: self.Guid(), ResourceBookingID: self.ResourceBookingID(), ResourceID: self.ResourceID(),
        StartDateTime: self.StartDateTime(), EndDateTime: self.EndDateTime()
      }],
        {
          onSuccess: function (data) {
            self.ClashList([])
            data.forEach((itm, indx) => {
              let ncd = self.ClashList.AddNew()
              ncd.ResourceBookingGuid(itm.ResourceBookingGuid)
              ncd.ResourceID(itm.ResourceID)
              ncd.ResourceBookingID(itm.ResourceBookingID)
              ncd.StartDateTimeBuffer(itm.StartDateTimeBuffer)
              ncd.StartDateTime(itm.StartDateTime)
              ncd.EndDateTime(itm.EndDateTime)
              ncd.EndDateTimeBuffer(itm.EndDateTimeBuffer)
              ncd.ResourceBookingTypeID(itm.ResourceBookingTypeID)
              ncd.ResourceBookingDescription(itm.ResourceBookingDescription)
              ncd.ResourceBookingType(itm.ResourceBookingType)
              ncd.StartDateTimeDisplay(itm.StartDateTimeDisplay)
              ncd.EndDateTimeDisplay(itm.EndDateTimeDisplay)
            })
            self.IsProcessing(false)
          },
          onFail: function (errorText) {
            self.IsProcessing(false)
          }
        })
    }
  },
  FilterStatusList: function (List, shift) {
    var results = []
    List.Iterate(function (itm, indx) {
      if (itm.SystemID == shift.SystemID() && itm.ProductionAreaID == shift.ProductionAreaID()) {
        results.push(itm)
      }
    })
    return results
  },
  refreshRoomScheduleSelection: function (humanResourceShift) {
    ViewModel.RORoomScheduleListSelectShiftListCriteria().HumanResourceShiftID(humanResourceShift.HumanResourceShiftID())
    ViewModel.RORoomScheduleListSelectShiftListCriteria().StartDateTime(humanResourceShift.StartDateTime())
    ViewModel.RORoomScheduleListSelectShiftListCriteria().EndDateTime(humanResourceShift.EndDateTime())
    ViewModel.RORoomScheduleListSelectShiftListCriteria().Keyword("")
    ViewModel.RORoomScheduleListSelectShiftListCriteria().SystemID(humanResourceShift.SystemID())
    ViewModel.RORoomScheduleListSelectShiftListCriteria().ProductionAreaID(humanResourceShift.ProductionAreaID())
    ViewModel.RORoomScheduleListSelectShiftListManager().Refresh()
  },
  manualRefreshRoomScheduleSelection: function (humanResourceShift) {
    ViewModel.RORoomScheduleListSelectShiftListManager().Refresh()
  }
}

ICRShiftBO = {
  ShiftValid: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    if (Shift) {
      if (Shift.ClashList().length > 0) {
        CtlError.AddError("Shift Clashes Exist")
      }
    }
  },
  StaffAckCss: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  StaffAckText: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "Authorised"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "Pending"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "Rejected"
    }
  },
  StaffAckIcon: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "fa fa-check-square-o"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "fa fa-minus"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "fa fa-times"
    }
  },
  SupervisorAuthCss: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.SupervisorAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.SupervisorAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  SupervisorAuthText: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "Authorised"
    } else if (shift.SupervisorAuthInd() == null) {
      return "Pending"
    } else if (shift.SupervisorAuthInd() == false) {
      return "Rejected"
    }
  },
  SupervisorAuthIcon: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.SupervisorAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.SupervisorAuthInd() == false) {
      return "fa fa-times"
    }
  },
  ManagerAuthCss: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.ManagerAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.ManagerAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  ManagerAuthText: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "Authorised"
    } else if (shift.ManagerAuthInd() == null) {
      return "Pending"
    } else if (shift.ManagerAuthInd() == false) {
      return "Rejected"
    }
  },
  ManagerAuthIcon: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.ManagerAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.ManagerAuthInd() == false) {
      return "fa fa-times"
    }
  },

  newShiftResourceScheduler(args, defaults = { SystemID: null, ProductionAreaID: null, ShiftTypeID: null }) {
    let me = this;
    ViewModel.CurrentICRShift(null)
    let newShift = new ICRShiftObject()
    newShift.SystemID(defaults.SystemID)
    newShift.ProductionAreaID(defaults.ProductionAreaID)
    newShift.DisciplineID(null)
    newShift.ShiftTypeID(null)
    let bookingStartTime = args.data.origEvt.time; //.format('dd MMM yyyy HH:mm')
    let bookingEndTime = moment(args.data.origEvt.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
    newShift.StartDateTime(bookingStartTime)
    newShift.EndDateTime(bookingEndTime)
    newShift.ProductionAreaStatusID(1)
    newShift.HumanResourceID(args.data.group.Resource.HumanResourceID)
    newShift.ResourceID(args.data.group.Resource.ResourceID)
    newShift.HumanResource(args.data.group.Resource.ResourceName)
    ViewModel.CurrentICRShift(newShift)
    this.showShiftModal()
  },
  showShiftModal() {
    var me = this;
    $("#CurrentICRShiftModal").off('hidden.bs.modal')
    $("#CurrentICRShiftModal").off('shown.bs.modal')

    $("#CurrentICRShiftModal").on('hidden.bs.modal', function () {
      window.siteHubManager.bookingOutOfEdit(ViewModel.CurrentICRShift().ResourceBookingID())
      ViewModel.CurrentICRShift(null)
    })

    $("#CurrentICRShiftModal").on('shown.bs.modal', function () {
      if (!ViewModel.CurrentICRShift().IsNew()) {
        window.siteHubManager.bookingIntoEdit(ViewModel.CurrentICRShift().ResourceBookingID())
      }
      $("#CurrentICRShiftModal").draggable({ handle: ".modal-header" })
      if (ViewModel.CurrentICRShift()) { Singular.Validation.CheckRules(ViewModel.CurrentICRShift()) }
    })

    $("#CurrentICRShiftModal").modal()
  },
  hideShiftModal() {
    $("#CurrentICRShiftModal").modal('hide')
  },
  saveShiftModal(humanResourceShift) {
    humanResourceShift.IsProcessing(true)
    ViewModel.CallServerMethodPromise("SaveICRShift", {
      ICRShift: humanResourceShift.Serialise()
    }).then((response) => {
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      humanResourceShift.IsProcessing(false)
      this.hideShiftModal()
    }),
      errorText => {
        OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
      };
  }
};

PlayoutOperationsShiftBO = {
  ShiftValid: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    if (Shift) {
      if (Shift.ClashList().length > 0) {
        CtlError.AddError("Shift Clashes Exist")
      }
    }
  },
  StaffAckCss: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  StaffAckText: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "Authorised"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "Pending"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "Rejected"
    }
  },
  StaffAckIcon: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "fa fa-check-square-o"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "fa fa-minus"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "fa fa-times"
    }
  },
  SupervisorAuthCss: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.SupervisorAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.SupervisorAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  SupervisorAuthText: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "Authorised"
    } else if (shift.SupervisorAuthInd() == null) {
      return "Pending"
    } else if (shift.SupervisorAuthInd() == false) {
      return "Rejected"
    }
  },
  SupervisorAuthIcon: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.SupervisorAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.SupervisorAuthInd() == false) {
      return "fa fa-times"
    }
  },
  ManagerAuthCss: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.ManagerAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.ManagerAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  ManagerAuthText: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "Authorised"
    } else if (shift.ManagerAuthInd() == null) {
      return "Pending"
    } else if (shift.ManagerAuthInd() == false) {
      return "Rejected"
    }
  },
  ManagerAuthIcon: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.ManagerAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.ManagerAuthInd() == false) {
      return "fa fa-times"
    }
  },

  //data access
  get: function (humanResourceShiftID, onSuccess, onFail) {
    ViewModel.CallServerMethod("EditPlayoutOpsShift", {
      HumanResourceShiftID: humanResourceShiftID
    }, function (response) {
      if (response.Success) {
        onSuccess(response)
      }
      else {
        OBMisc.Notifications.GritterError('Error Fetching Shift', response.ErrorText, 1000)
        onFail(response)
      }
    })
  },
  save: function (shift) {
    ViewModel.CallServerMethod("SavePlayoutOpsShift", {
      Shift: shift.Serialise()
    }, function (response, onSuccess, onFail) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 500)
        onSuccess(response)
      } else {
        OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
        onFail(response)
      }
    })
  }
}

MCRShiftBO = {
  //other
  RoomIDSet: function () {
  },
  StaffAckCss: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  StaffAckText: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "Authorised"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "Pending"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "Rejected"
    }
  },
  StaffAckIcon: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "fa fa-check-square-o"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "fa fa-minus"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "fa fa-times"
    }
  },
  SupervisorAuthCss: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.SupervisorAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.SupervisorAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  SupervisorAuthText: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "Authorised"
    } else if (shift.SupervisorAuthInd() == null) {
      return "Pending"
    } else if (shift.SupervisorAuthInd() == false) {
      return "Rejected"
    }
  },
  SupervisorAuthIcon: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.SupervisorAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.SupervisorAuthInd() == false) {
      return "fa fa-times"
    }
  },
  ManagerAuthCss: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.ManagerAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.ManagerAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  ManagerAuthText: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "Authorised"
    } else if (shift.ManagerAuthInd() == null) {
      return "Pending"
    } else if (shift.ManagerAuthInd() == false) {
      return "Rejected"
    }
  },
  ManagerAuthIcon: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.ManagerAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.ManagerAuthInd() == false) {
      return "fa fa-times"
    }
  },
  refreshROChannelList: function (shift) {
    //shift.IsProcessing(true)
    //if (shift.RoomID()) {
    //  shift.RORoomChannelList.Set([])
    //  shift.IsProcessing(false)
    //} else {
    //  Singular.GetDataStateless("OBLib.Maintenance.Rooms.ReadOnly.RORoomChannelList",
    //    {
    //      RoomID: shift.RoomID,
    //      StartDate: shift.StartDateTime(),
    //      EndDate: shift.EndDateTime()
    //    },
    //    function (response) {
    //      if (response.Success) {
    //        shift.RORoomChannelList.Set(response.Data)
    //      }
    //      else {
    //        OBMisc.Notifications.GritterError("Error Refreshing Channels", response.ErrorText, 1000)
    //      }
    //      shift.IsProcessing(false)
    //    })
    //}
  },

  beforeRoomIDDropDownShown: function () {

  },
  onRoomSelected: function (selectedItem, businessObject) {
    if (selectedItem && (selectedItem.IsGap || selectedItem.IsSufficientTime)) {
      businessObject.RoomClashCount(0)
    }
    else {
      businessObject.RoomClashCount(businessObject.RoomClashCount() + 1)
    }
    MCRShiftBO.refreshROChannelList(businessObject)
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomRefreshAjax: function (args, self) {
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.EndDateTime()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {

  },

  //rules
  ShiftValid: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    //if (Shift) {
    //  if (Shift.ClashList().length > 0) {
    //    CtlError.AddError("Shift Clashes Exist")
    //  }
    //}
  },

  //data access
  get: function (humanResourceShiftID, onSuccess, onFail) {
    ViewModel.CallServerMethod("EditPlayoutOpsShiftMCR", {
      HumanResourceShiftID: humanResourceShiftID
    }, function (response) {
      if (response.Success) {
        onSuccess(response)
      }
      else {
        OBMisc.Notifications.GritterError('Error Fetching Shift', response.ErrorText, 1000)
        onFail(response)
      }
    })
  },
  save: function (shift) {
    ViewModel.CallServerMethod("SavePlayoutOpsShift", {
      Shift: shift.Serialise()
    }, function (response, onSuccess, onFail) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 500)
        if (onSuccess) { onSuccess(response) }
      } else {
        OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
        if (onFail) { onFail(response) }
      }
    })
  }
}

SCCRShiftBO = {
  //other
  StaffAckCss: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  StaffAckText: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "Authorised"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "Pending"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "Rejected"
    }
  },
  StaffAckIcon: function (shift) {
    if (shift.StaffAcknowledgeInd()) {
      return "fa fa-check-square-o"
    } else if (shift.StaffAcknowledgeInd() == null) {
      return "fa fa-minus"
    } else if (shift.StaffAcknowledgeInd() == false) {
      return "fa fa-times"
    }
  },
  SupervisorAuthCss: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.SupervisorAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.SupervisorAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  SupervisorAuthText: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "Authorised"
    } else if (shift.SupervisorAuthInd() == null) {
      return "Pending"
    } else if (shift.SupervisorAuthInd() == false) {
      return "Rejected"
    }
  },
  SupervisorAuthIcon: function (shift) {
    if (shift.SupervisorAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.SupervisorAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.SupervisorAuthInd() == false) {
      return "fa fa-times"
    }
  },
  ManagerAuthCss: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "btn btn-sm btn-success"
    } else if (shift.ManagerAuthInd() == null) {
      return "btn btn-sm btn-default"
    } else if (shift.ManagerAuthInd() == false) {
      return "btn btn-sm btn-danger"
    }
  },
  ManagerAuthText: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "Authorised"
    } else if (shift.ManagerAuthInd() == null) {
      return "Pending"
    } else if (shift.ManagerAuthInd() == false) {
      return "Rejected"
    }
  },
  ManagerAuthIcon: function (shift) {
    if (shift.ManagerAuthInd()) {
      return "fa fa-check-square-o"
    } else if (shift.ManagerAuthInd() == null) {
      return "fa fa-minus"
    } else if (shift.ManagerAuthInd() == false) {
      return "fa fa-times"
    }
  },
  CanEdit: function (shift, fieldName) {
    switch (fieldName) {
      case "ShiftTypeID":
        return !(shift.ShiftTypeID() == 30)
        break;
      case "StartDate":
      case "StartTime":
      case "EndDate":
      case "EndTime":
        return !(shift.ShiftTypeID() == 30)
        break;
      default:
        return true
        break;
    }
  },

  //rules
  ShiftValid: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    if (Shift) {
      if (Shift.ClashList().length > 0) {
        CtlError.AddError("Shift Clashes Exist")
      }
    }
  },

  //data access
  get: function (humanResourceShiftID, options) {
    Singular.GetDataStateless("OBLib.Shifts.PlayoutOperations.SCCRShiftList, OBLib", {
      HumanResourceShiftID: humanResourceShiftID
    }, function (response) {
      if (response.Success) {
        if (options.onSuccess) { options.onSuccess(response) }
      }
      else {
        OBMisc.Notifications.GritterError('Error Fetching Shift', response.ErrorText, 1000)
        if (options.onFail) { options.onFail(response) }
      }
    })
  },
  save: function (shift) {
    ViewModel.CallServerMethod("SavePlayoutOpsShiftSCCR", {
      SCCRShift: shift.Serialise()
    }, function (response, onSuccess, onFail) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
        onSuccess(response)
      } else {
        OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
        onFail(response)
      }
    })
  },
  editShiftSchedulerScreen: function (item, options) {
    var me = this
    me.get(item.Booking.HumanResourceShiftID, options)
  },
  saveShiftModal: function (shift) {
    this.save(shift, {
      onSuccess: function (response) {
        me.hideModal()
      }
    })
  },

  //modals
  showModal: function () {
    $("#CurrentPlayoutOpsShiftControlSCCR").off('shown.bs.modal')
    $("#CurrentPlayoutOpsShiftControlSCCR").on('shown.bs.modal', function () {
      $("#CurrentPlayoutOpsShiftControlSCCR").draggable({ handle: ".modal-header" })
      Singular.Validation.CheckRules(ViewModel.CurrentPlayoutOpsShiftSCCR())
    })
    $("#CurrentPlayoutOpsShiftControlSCCR").modal()
  },
  hideModal: function () {
    $("#CurrentPlayoutOpsShiftControlSCCR").modal('hide')
  }
}

GenericShiftBO = {
  beforeHumanResourceRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.DisciplineID = self.DisciplineID()
    args.ShiftDate = new Date(self.ScheduleDate()).format("dd MMM yyyy")
  },
  afterHumanResourceRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  DisciplineIDSet: function (self) {

  },
  HumanResourceIDSet: function (self) {

  },
  onHumanResourceSelected: function (selectedItem, businessObject) {
    businessObject.HumanResourceID(selectedItem.HumanResourceID)
    businessObject.HumanResource(selectedItem.HRName)
    businessObject.ResourceID(selectedItem.ResourceID)
  },
  ShiftTypeSelected(selectedItem, businessObject) {
    //default description to shifttype if programming
    if (businessObject.SystemID() == 8 && selectedItem) {
      businessObject.ResourceBookingDescription(selectedItem.ShiftType);
    }
  },

  ShiftValid: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    if (Shift) {
      if (Shift.ClashList().length > 0) {
        CtlError.AddError("Shift Clashes Exist")
      }
    }
  },

  canEdit: function (shift, fieldName) {

  },
  canSave: function (shift) {
    return shift.IsValid()
  },

  get(options) {
    let promise = Singular.GetDataStatelessPromise("OBLib.Shifts.GenericShiftList, OBLib", options.criteria)
    promise.then(function (data) {
      if (options.onSuccess) { options.onSuccess(data) }
    },
      //error
      function (errorText) {
        OBMisc.Notifications.GritterError("Fetch shift failed", errorText, 1000)
      })
  },
  save: function (shift, options) {
    ViewModel.CallServerMethod("SaveGenericShift", {
      GenericShift: shift.Serialise()
    },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, shift)
          OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
          humanResourceShift.IsProcessing(false)
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  deleteShiftsResourceScheduler: function (bookings, options) {
    ViewModel.CallServerMethod("DeleteGenericShifts", {
      shifts: bookings
    },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError('Error Removing Shifts', response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  createQuickShift(params = {
    SystemID: null, ProductionAreaID: null,
    ShiftTypeID: null, ShiftType: "",
    HumanResourceID: null, ResourceID: null, HumanResource: "",
    DisciplineID: null, Discipline: "",
    StartDateTime: null, EndDateTime: null,
    ShiftTitle: ""
  }) {
    return ViewModel.CallServerMethodPromise("CreateQuickShift", {
      SystemID: params.SystemID, ProductionAreaID: params.ProductionAreaID,
      ShiftTypeID: params.ShiftTypeID, ShiftType: params.ShiftType,
      HumanResourceID: params.HumanResourceID, ResourceID: params.ResourceID,
      HumanResource: params.HumanResource,
      DisciplineID: params.DisciplineID, Discipline: params.Discipline,
      StartDateTime: params.StartDateTime, EndDateTime: params.EndDateTime,
      ShiftTitle: params.ShiftTitle
    });
  },

  newShiftResourceScheduler: function (args, defaults = {
    SystemID: null, ProductionAreaID: null,
    ShiftTypeID: null, ShiftType: "",
    HumanResourceID: null, ResourceID: null, HumanResource: "",
    DisciplineID: null, Discipline: ""
  }) {
    ViewModel.CurrentGenericShift(null)
    let newShift = new GenericShiftObject()
    newShift.SystemID(defaults.SystemID)
    newShift.ProductionAreaID(defaults.ProductionAreaID)
    newShift.Discipline(defaults.Discipline)
    newShift.DisciplineID(defaults.DisciplineID)
    newShift.Discipline(defaults.Discipline)
    newShift.ShiftType(defaults.ShiftType)
    newShift.ShiftTypeID(defaults.ShiftTypeID)
    newShift.ShiftType(defaults.ShiftType)
    let bookingStartTime = args.data.origEvt.time; //.format('dd MMM yyyy HH:mm')
    let bookingEndTime = moment(bookingStartTime).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
    newShift.StartDateTime(bookingStartTime)
    newShift.EndDateTime(bookingEndTime)
    newShift.ProductionAreaStatus("New")
    newShift.ProductionAreaStatusID(1)
    newShift.ProductionAreaStatus("New")
    newShift.HumanResource(defaults.HumanResource)
    newShift.HumanResourceID(defaults.HumanResourceID)
    newShift.ResourceID(defaults.ResourceID)
    newShift.HumanResource(defaults.HumanResource)
    ViewModel.CurrentGenericShift(newShift)
    this.showShiftModal()
  },
  editShiftScheduler(item, group, onSuccess = (data) => { }, onFail = (errorText) => { }) {
    this.get({
      criteria: {
        HumanResourceShiftID: (item.businessObject ? item.businessObject.HumanResourceShiftID : item.Booking.HumanResourceShiftID)
      },
      onSuccess: (data) => {
        ViewModel.CurrentGenericShift.Set(data[0])
        this.showShiftModal()
        onSuccess(data);
      },
      onFail: (errorText) => {
        onFail(errorText);
      }
    })
  },
  showShiftModal: function () {
    var me = this;
    $("#CurrentGenericShiftModal").off('hidden.bs.modal')
    $("#CurrentGenericShiftModal").off('shown.bs.modal')

    $("#CurrentGenericShiftModal").on('hidden.bs.modal', function () {
      window.siteHubManager.bookingOutOfEdit(ViewModel.CurrentGenericShift().ResourceBookingID())
      ViewModel.CurrentGenericShift(null)
    })

    $("#CurrentGenericShiftModal").on('shown.bs.modal', function () {
      if (!ViewModel.CurrentGenericShift().IsNew()) {
        window.siteHubManager.bookingIntoEdit(ViewModel.CurrentGenericShift().ResourceBookingID())
      }
      $("#CurrentGenericShiftModal").draggable({ handle: ".modal-header" })
      if (ViewModel.CurrentGenericShift()) { Singular.Validation.CheckRules(ViewModel.CurrentGenericShift()) }
    })

    $("#CurrentGenericShiftModal").modal()
  },
  hideShiftModal: function () {
    $("#CurrentGenericShiftModal").modal('hide')
  },
  saveShiftModal: function (humanResourceShift) {
    var me = this;
    humanResourceShift.IsProcessing(true)
    ViewModel.CallServerMethod("SaveGenericShift", {
      GenericShift: humanResourceShift.Serialise()
    }, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
        humanResourceShift.IsProcessing(false);
        window.SiteHub.server.changeMade();
        me.hideShiftModal();
      } else {
        OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
      }
    });
  },
  removeShifts(bookingsData, onSuccess = (data) => { }, onFail = (errorText) => { }) {
    ViewModel.CallServerMethodPromise("DeleteGenericShifts", { bookings: bookingsData })
      .then((data) => {
        onSuccess(data);
        window.SiteHub.server.changeMade();
        OBMisc.Notifications.GritterSuccess("Shifts Removed", "", 250);
      },
      (errorText) => {
        onFail(errorText);
        OBMisc.Notifications.GritterError('Error Removing Shifts', errorText, 1000);
      })
  }
}

ShiftAuthHumanResourceShiftBO = {
  SetScheduleDate: function (shiftAuthHumanResourceShift) {

  },
  PreAuthChanged: function (authEntry, args) {
    authEntry._OldManagerAuthInd = args.OldValue;
  },
  AuthChanged: function (authEntry) {

    var WasAuthorised = authEntry._OldManagerAuthInd == true,
      IsAuthorised = authEntry.ManagerAuthInd() == true,
      ParentRecord = authEntry.GetParent();

    if (authEntry.ManagerAuthInd() != false) {
      authEntry.ManagerRejectedReason("");
    } else {
      //    setTimeout(function () {
      //      var ctl = $(authEntry.ManagerRejectedReason.BoundElements[0]);
      //      ctl.focus();
      //      ctl.select();
      //    }, 0);
    }

    Singular.ClearMessages();
    if (WasAuthorised != IsAuthorised) {

      var List = [];
      ParentRecord.ShiftAuthHumanResourceShiftList().Iterate(function (item) {
        var authoriseInt;
        switch (item.ManagerAuthInd()) {
          case true:
            authoriseInt = 1;
            break
          case false:
            authoriseInt = 2;
            break
          case null:
            authoriseInt = 0;
            break
        }

        List.push({ HumanResourceShiftID: item.HumanResourceShiftID(), ManagerAuthInd: authoriseInt });
      });
      ParentRecord.IsLoading(true);
      ParentRecord.SyncID(ParentRecord.SyncID() + 1);

      ViewModel.CallServerMethod('GetReimbursementValues', { HRID: ParentRecord.HumanResourceID(), JSon: JSON.stringify(List), SyncID: ParentRecord.SyncID(), SystemID: authEntry.SystemID() }, function (result) {

        if (ParentRecord.SyncID() == result.Data.SyncID) {
          ParentRecord.AuthorisedNoOfMeals(result.Data.NoOfMeals);
          ParentRecord.AuthorisedReimbursementAmount(result.Data.MRAmount);
          ParentRecord.IsLoading(false);
        }
      });

    }
  },
  Save: function (ParentRecord) {
    ViewModel.CallServerMethod('SaveHRAuthList', { HRAuth: ParentRecord.Serialise() },
      function (response, onSuccess, onFail) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 500)
          ParentRecord.Set(response.Data);
          //onSuccess(response)
        } else {
          OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
          //onFail(response)
        }
      })
  }
}

StudioSupervisorShiftBO = {
  SupervisorIDSet: function (self) {
    if (self.SupervisorID()) {
      var hr = ClientData.ROStudioSupervisorList.Find("HumanResourceID", self.SupervisorID())
      self.HumanResourceID(self.SupervisorID())
      self.ResourceID(hr.ResourceID)
      HumanResourceShiftBaseBO.checkClashes(self)
    }
    else {
      self.HumanResourceID(null)
      self.ResourceID(null)
      self.ClashList([])
    }
  },
  ShiftValid: function (Value, Rule, CtlError) {
    var Shift = CtlError.Object;
    if (Shift) {
      if (Shift.ClashList().length > 0) {
        CtlError.AddError("Shift Clash Exists")
      }
    }
  },
  ProductionAreaStatusIDSet: function (supervisorShift) {
    //console.log(supervisorShift)
  },
  saveShift(shift) {
    ViewModel.CallServerMethodPromise("SaveStudioSupervisorShift", {
      StudioSupervisorShift: shift.Serialise()
    }).then(data => {
      KOFormatter.Deserialise(data, shift);
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250);
      if (options.onSuccess) { options.onSuccess(response) };
    }),
      errorText => {
        OBMisc.Notifications.GritterError('Error During Save', errorText, 1000);
        if (options.onFail) { options.onFail(errorText) };
      };
  },
  canSave(shift) {
    return shift.IsValid();
  }
};

ContentShiftRoomScheduleBO = {
  IsSelectedSet: function (self) {

  }
}

AccessTerminalShiftBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Biometrics.ROAccessTerminalShiftList",
      options.criteria,
      function (response) {
        if (options.onSuccess) {
          options.onSuccess(response)
        } else {
          options.onFail(response)
        }
      })
  }
}

SAShiftPatternCriteriaBO = {
  SystemIDSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },
  ProductionAreaIDSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },
  StartDateSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },
  EndDateSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },

  //methods
  async refresh(criteriaObject) {
    ViewModel.IsProcessing(true)
    try {
      let criteriaTemplate = {
        SystemID: criteriaObject.SystemID(),
        ProductionAreaID: criteriaObject.ProductionAreaID(),
        StartDate: criteriaObject.StartDate(),
        EndDate: criteriaObject.EndDate()
      };
      let result = await Singular.GetDataStatelessPromise("OBLib.Shifts.Patterns.SAShiftPatternList", criteriaTemplate);
      ViewModel.CurrentSAShiftPatternList.Set(result);
    } catch (err) {

    } finally {
      ViewModel.IsProcessing(false)
    }
  }
}

SAShiftPatternBO = {
  PatternNameSet(self) {
  },
  SystemIDSet(self) {
  },
  ProductionAreaIDSet(self) {
  },
  NumWeeksSet(self) {
    self.SAShiftPatternDayList([]);
    if (self.NumWeeks()) {
      let currentWeekNum = 1;
      while (currentWeekNum <= self.NumWeeks()) {
        let currentDay = 1;
        while (currentDay <= 7) {
          let newWeekDay = self.SAShiftPatternDayList.AddNew();
          newWeekDay.WeekNo(currentWeekNum);
          newWeekDay.WeekDay(currentDay);
          //newWeekDay.WeekdayName();
          currentDay += 1;
        };
        currentWeekNum += 1;
      };
    };
    Singular.Validation.CheckRules(self);
  },
  IsTemplateSet(self) {
  },
  PatternNameSet(self) {
  },

  canEdit(fieldName, pattern) {
    switch (fieldName) {
      case "PatternName":
        return true;
        break;
      case "SystemID":
        return (pattern.PatternName().length > 0);
        break;
      case "ProductionAreaID":
        return ((pattern.PatternName().length > 0) && pattern.SystemID());
        break;
      case "NumWeeks":
        return ((pattern.PatternName().length > 0) && pattern.SystemID() && pattern.ProductionAreaID());
        break;
      case "Crew":
        let dirtyDays = pattern.SAShiftPatternDayList().filter(day => { return day.IsSelfDirty() });
        let basicInfoIsDirty = pattern.IsSelfDirty();
        return !(dirtyDays.length > 0 || basicInfoIsDirty);
        break;
      default:
        return true
        break;
    }
  },

  savePromise(pattern) {
    return ViewModel.CallServerMethodPromise("SaveSAShiftPattern", { SAShiftPattern: pattern.Serialise() });
  },
  getPromise(criteria) {
    return Singular.GetDataStatelessPromise("OBLib.Shifts.Patterns.SAShiftPatternList", criteria);
  },

  async editModal(pattern) {
    try {
      pattern.IsProcessing(true);
      let result = await this.getPromise({
        SystemAreaShiftPatternID: pattern.SystemAreaShiftPatternID(),
        FetchChildren: true
      });
      ViewModel.CurrentSAShiftPattern.Set(result[0]);
      this.showModal();
    } catch (err) {
      OBMisc.Notifications.GritterError("Fetch failed", err, 1000);
    } finally {
      pattern.IsProcessing(false);
    }
  },
  newPatternModal() {
    ViewModel.CurrentSAShiftPattern(new SAShiftPatternObject());
    this.showModal();
  },
  showModal() {
    Singular.Validation.CheckRules(ViewModel.CurrentSAShiftPattern());
    let modalElement = $("#SAShiftPatternModal").find("#NewSAShiftPatternModal");
    modalBase.positionFromParent(modalElement[0], null, null);
  },
  findModal() {

  },
  async saveModal(pattern) {
    try {
      pattern.IsProcessing(true);
      let result = await this.savePromise(pattern);
      KOFormatter.Deserialise(result, pattern);
      OBMisc.Notifications.GritterSuccess("Save succeeded", "", 250);
      if (ViewModel.CurrentSAShiftPatternListManager) {
        ViewModel.CurrentSAShiftPatternListManager().Refresh();
      };
    } catch (err) {
      OBMisc.Notifications.GritterError("Save failed", err, 1000);
    } finally {
      pattern.IsProcessing(false);
    }
  }
}

SAShiftPatternDayBO = {
  //set
  WeekNoSet(self) {
  },
  WeekDaySet(self) {
  },
  StartTimeSet(self) {
  },
  EndTimeSet(self) {
  },
  ShiftTypeIDSet(self) {
  },

  canEdit(fieldName, patternDay) {
    switch (fieldName) {
      case "WeekNo":
        return false;
        break;
      case "WeekDay":
        return false; //patternDay.WeekNo();
        break;
      case "ShiftTypeID":
        return ((patternDay.IsNew() || patternDay.ShiftCount() == 0) && (patternDay.WeekNo() && patternDay.WeekDay()));
        break;
      case "StartTime":
        return ((patternDay.IsNew() || patternDay.ShiftCount() == 0) && (patternDay.WeekNo() && patternDay.WeekDay() && patternDay.ShiftTypeID()));
        break;
      case "EndTime":
        return ((patternDay.IsNew() || patternDay.ShiftCount() == 0) && (patternDay.WeekNo() && patternDay.WeekDay() && patternDay.ShiftTypeID() && patternDay.StartTime()));
        break;
      default:
        return true
        break;
    }
  },

  //dropdowns
  setShiftTypeIDCriteriaBeforeRefresh(args) {
    args.Object.IsProcessing(true);
    args.Object.GetParent().IsProcessing(true);
    args.Data.SystemID = args.Object.GetParent().SystemID();
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID();
  },
  triggerShiftTypeIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterShiftTypeIDRefreshAjax(args) {
    args.Object.IsProcessing(false);
    args.Object.GetParent().IsProcessing(false);
  },
  onShiftTypeIDSelected(selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.StartTime(selectedItem.StartTime);
      businessObject.EndTime(selectedItem.EndTime);
    }
    else {
      businessObject.StartTime(null);
      businessObject.EndTime(null);
    }
  }
};

SAShiftPatternHRBO = {

  //dropdowns
  setDisciplineIDCriteriaBeforeRefresh(args) {
    //args.Object.IsProcessing(true);
    args.Data.SystemID = args.Object.GetParent().SystemID();
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID();
    args.Data.HumanResourceID = args.Object.HumanResourceID();
    args.Data.DisciplineID = args.Object.DisciplineID();
  },
  triggerDisciplineIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterDisciplineIDRefreshAjax(args) {
    //args.Object.IsProcessing(false);
  },
  onDisciplineIDSelected(selectedItem, businessObject) {
  },

  triggerHumanResourceIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  setHumanResourceIDCriteriaBeforeRefresh(args) {
    args.Data.DisciplineIDs = (args.Object.DisciplineID() ? [args.Object.DisciplineID()] : []);
    args.Data.SystemIDs = (args.Object.GetParent().SystemID() ? [args.Object.GetParent().SystemID()] : []);
    args.Data.ProductionAreaIDs = (args.Object.GetParent().ProductionAreaID() ? [args.Object.GetParent().ProductionAreaID()] : args.Object.GetParent().ProductionAreaID());
  },
  afterHumanResourceIDRefreshAjax(args) {
  },
  onHumanResourceIDSelected(item, businessObject) {
  },
  HumanResourceIDSet() { },

  canEdit(fieldName, patternHR) {
    let dirtyDays = patternHR.GetParent().SAShiftPatternDayList().filter(day => { return day.IsSelfDirty() });
    let basicInfoIsDirty = patternHR.GetParent().IsSelfDirty();
    if (patternHR.IsProcessing() || dirtyDays.length > 0 || basicInfoIsDirty) {
      return false;
    } else {
      switch (fieldName) {
        case "DisciplineID":
          return (patternHR.IsNew() || patternHR.ShiftCount() == 0);
          break;
        case "HumanResourceID":
          return ((patternHR.IsNew() || patternHR.ShiftCount() == 0) && patternHR.DisciplineID());
          break;
        case "StartDate":
          return ((patternHR.IsNew() || patternHR.ShiftCount() == 0) && patternHR.DisciplineID() && patternHR.HumanResourceID());
          break;
        case "EndDate":
          if ((patternHR.IsNew() || patternHR.ShiftCount() == 0)) {
            return (patternHR.DisciplineID() && patternHR.HumanResourceID() && patternHR.StartDate());
          } else {
            return true;
          }
          break;
        case "ShiftCount":
          return false;
          break;
        case "GenerateButton":
          return (patternHR.ShiftCount() == 0);
          break;
        case "AddNewButton":
          return true;
          break;
        case "RemoveButton":
          //return (patternHR.ShiftCount() == 0);
          return true;
          break;
        case "UpdateButton":
          let sameDay = moment(patternHR.StartDate()).isSame(moment(patternHR.EndDate()), 'day');
          return (patternHR.ShiftCount() > 0 && !patternHR.IsProcessing() && !sameDay);
          break;
        default:
          return true
          break;
      };
    };
  },
  canView(fieldName, patternHR) {
    let dirtyDays = patternHR.GetParent().SAShiftPatternDayList().filter(day => { return day.IsSelfDirty() });
    let basicInfoIsDirty = patternHR.GetParent().IsSelfDirty();
    if (patternHR.IsProcessing() || dirtyDays.length > 0 || basicInfoIsDirty) {
      return false;
    } else {
      switch (fieldName) {
        case "DisciplineID":
          return (patternHR.IsNew() || patternHR.ShiftCount() == 0);
          break;
        case "HumanResourceID":
          return ((patternHR.IsNew() || patternHR.ShiftCount() == 0) && patternHR.DisciplineID());
          break;
        case "StartDate":
          return ((patternHR.IsNew() || patternHR.ShiftCount() == 0) && patternHR.DisciplineID() && patternHR.HumanResourceID());
          break;
        case "EndDate":
          if ((patternHR.IsNew() || patternHR.ShiftCount() == 0)) {
            return (patternHR.DisciplineID() && patternHR.HumanResourceID() && patternHR.StartDate());
          } else {
            return true;
          }
          break;
        case "ShiftCount":
          return false;
          break;
        case "GenerateButton":
          return (patternHR.ShiftCount() == 0 && !patternHR.IsProcessing());
          break;
        case "GeneratingButton":
          return (patternHR.ShiftCount() == 0 && patternHR.IsProcessing());
          break;
        case "UpdateButton":
          return (patternHR.ShiftCount() > 0 && !patternHR.IsProcessing());
          break;
        case "UpdatingButton":
          return (patternHR.ShiftCount() > 0 && patternHR.IsProcessing());
          break;
        case "AddNewButton":
          return true;
          break;
        case "RemoveButton":
          return (patternHR.ShiftCount() == 0);
          break;
        default:
          return true
          break;
      };
    };
  },

  canAdd(pattern) {
    let dirtyDays = pattern.SAShiftPatternDayList().filter(day => { return day.IsSelfDirty() });
    if (pattern.IsProcessing() || dirtyDays.length > 0 || pattern.IsSelfDirty()) {
      return false;
    } else {
      return true;
    }
  },

  //rules
  EndDateValid(Value, Rule, CtlError) {
    let patternHR = CtlError.Object;
    if (moment(patternHR.StartDate()).isSame(moment(patternHR.EndDate()), 'day')) {
      CtlError.AddError("Start Date and End Date cannot be the same");
    };
  },

  async generateShifts(patternHR) {
    return new Promise(async (resolve, reject) => {
      try {
        patternHR.SystemAreaShiftPatternID(patternHR.GetParent().SystemAreaShiftPatternID());
        patternHR.IsProcessing(true);
        let result = await ViewModel.CallServerMethodPromise("GenerateSAShiftPatternHRShifts", { SAShiftPatternHR: patternHR.Serialise() });
        KOFormatter.Deserialise(result.BusinessObject, patternHR);
        if (result.ClashCount > 0) {
          OBMisc.Notifications.GritterWarning(patternHR.HRName() + " - Shift generation", result.ClashCount.toString() + " clashes were detected", 1000);
        };
        patternHR.IsProcessing(false);
        resolve(result)
      } catch (err) {
        reject(err);
      }
    });
  },
  async updateShifts(patternHR) {
    return new Promise(async (resolve, reject) => {
      try {
        patternHR.SystemAreaShiftPatternID(patternHR.GetParent().SystemAreaShiftPatternID());
        patternHR.IsProcessing(true);
        let result = await ViewModel.CallServerMethodPromise("UpdateSAShiftPatternHRShifts", { SAShiftPatternHR: patternHR.Serialise() });
        KOFormatter.Deserialise(result.BusinessObject, patternHR);
        //if (result.ClashCount > 0) {
        //  OBMisc.Notifications.GritterWarning(patternHR.HRName() + " - Shift generation", result.ClashCount.toString() + " clashes were detected", 1000);
        //};
        patternHR.IsProcessing(false);
        resolve(result)
      } catch (err) {
        reject(err);
      }
    });
  }

};

ROSAShiftPatternCriteriaBO = {
  SystemIDSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },
  ProductionAreaIDSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },
  StartDateSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },
  EndDateSet(self) {
    if (ViewModel.CurrentSAShiftPatternList) {
      this.refresh(self);
    }
  },

  //methods
  async refresh(criteriaObject) {
    if (ViewModel) {
      ViewModel.CurrentSAShiftPatternListManager().Refresh();
    };
    //ViewModel.IsProcessing(true)
    //try {
    //  let criteriaTemplate = {
    //    SystemID: criteriaObject.SystemID(),
    //    ProductionAreaID: criteriaObject.ProductionAreaID(),
    //    StartDate: criteriaObject.StartDate(),
    //    EndDate: criteriaObject.EndDate()
    //  };
    //  let result = await Singular.GetDataStatelessPromise("OBLib.Shifts.Patterns.ReadOnly.ROSAShiftPatternList", criteriaTemplate);
    //  ViewModel.CurrentSAShiftPatternList.Set(result);
    //} catch (err) {

    //} finally {
    //  ViewModel.IsProcessing(false)
    //}
  }
};