﻿ProductionSpecRequirementBO = {
  //other
  ProductionSpecRequirementBOToString: function (self) {
    return self.ProductionSpecRequirementName()
  },
  get: function (productionSpecRequirementID, afterSuccess, afterFail) {
    ViewModel.CallServerMethod("GetProductionSpecRequirement",
                              {
                                ProductionSpecRequirementID: productionSpecRequirementID
                              },
                              function (response) {
                                if (response.Success) {
                                  if (afterSuccess) { afterSuccess(response) }
                                }
                                else {
                                  OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 2000)
                                  if (afterFail) { afterFail(response) }
                                }
                              })
  },
  save: function (productionSpecRequirement, afterSuccess, afterFail) {
    ViewModel.CallServerMethod("SaveProductionSpecRequirement",
                              {
                                ProductionSpecRequirement: productionSpecRequirement.Serialise()
                              },
                              function (response) {
                                if (response.Success) {
                                  if (afterSuccess) { afterSuccess(response) }
                                }
                                else {
                                  OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
                                  if (afterFail) { afterFail(response) }
                                }
                              })
  },

  //set
  SystemIDSet: function (self) { },
  ProductionAreaIDSet: function (self) {
    if (self.ProductionAreaID() == 2) {
      self.ProductionType("")
      self.EventType("")
      self.ProductionTypeID(null)
      self.EventTypeID(null)
    }
    else if (self.ProductionAreaID() == 1) {
      self.Room("")
      self.RoomID(null)
    }
  },
  RoomIDSet: function (self) { },

  ProductionTypeIDSet: function () { },
  onProductionTypeIDSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {
      businessObject.ProductionTypeID(null)
      businessObject.ProductionType("")
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
    else {
      businessObject.ProductionTypeID(selectedItem.ProductionTypeID)
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
  },
  triggerProductionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionTypeIDCriteriaBeforeRefresh: function (args) {
  },
  afterProductionTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  EventTypeIDSet: function () { },
  onEventTypeIDSelected: function (selectedItem, businessObject) {
    businessObject.EventTypeID(selectedItem.EventTypeID)
  },
  triggerEventTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setEventTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterEventTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  //rules
  RoomIDValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.ProductionAreaID() == 2) {
      if (!CtlError.Object.RoomID()) {
        CtlError.AddError("Room must be specified for a Studio area")
      }
      if ((CtlError.Object.ProductionTypeID() || CtlError.Object.EventTypeID()) ? true : false) {
        CtlError.AddError("Production Type and Event Type must not be specified for a Studio area")
      }
    } else if (CtlError.Object.ProductionAreaID() == 1) {
      if (CtlError.Object.RoomID()) {
        CtlError.AddError("Room must not be specified for a Studio area")
      }
      if ((!CtlError.Object.ProductionTypeID() || !CtlError.Object.EventTypeID()) ? true : false) {
        CtlError.AddError("Production Type and Event Type must be specified for an OB area")
      }
    }
  },
  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    var Spec = CtlError.Object;
    if (Spec.ProductionAreaID() == 1) {
      if (!Spec.ProductionTypeID()) { CtlError.AddError('Production Type is required') }
    }
  },
  EventTypeIDValid: function (Value, Rule, CtlError) {
    var Spec = CtlError.Object;
    if (Spec.ProductionAreaID() == 1) {
      if (!Spec.EventTypeID()) { CtlError.AddError('Event Type is required') }
    }
  },
}

ProductionSpecRequirementPositionBO = {
  //other
  ProductionSpecRequirementPositionBOToString: function (self) {
    return ""
  },

  PositionMeetsSpecRequirement: function (self) {
  },

  //set
  //discipline
  DisciplineIDSet: function (self) { },
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
  },
  afterDisciplineIDRefreshAjax: function (args) { },
  onDisciplineIDSelected: function (item, businessObject) { },

  //position type
  PositionIDSet: function (self) { },
  triggerPositionIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
  },
  afterPositionIDRefreshAjax: function (args) { },
  onPositionIDSelected: function (item, businessObject) {
    if (item) {
      //businessObject.PositionTypeID(item.PositionTypeID)
      businessObject.PositionID(item.PositionID)
      businessObject.Position(item.Position)
    } else {
      //businessObject.PositionTypeID(null)
      businessObject.PositionID(null)
      businessObject.Position("")
    }
  },

  //rules

}

ROProductionSpecRequirementPagedListCriteriaBO = {
  SystemIDSet: function (self) {
    ProductionSpecRequirementsPage.refresh()
  },
  ProductionAreaIDSet: function (self) {
    ProductionSpecRequirementsPage.refresh()
  },
  ProductionSpecRequirementNameSet: function (self) {
    ProductionSpecRequirementsPage.refresh()
  }
}