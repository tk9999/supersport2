﻿RentalCarAgentBranchBO = {

   OldDateValid: function(Value, Rule, CtlError) {
    if (CtlError.Object.OldInd() && !CtlError.Object.OldDate()) {
      CtlError.AddError("Old Date is required");
    }
  },

   OldReasonValid: function(Value, Rule, CtlError) {
    if (CtlError.Object.OldInd() && CtlError.Object.OldReason().trim().length == 0) {
      CtlError.AddError("Old Reason is required");
    }
  },

}