﻿RentalCarAgentBO = {

   OldDateValid: function(Value, Rule, CtlError) {
    if (CtlError.Object.OldInd() && !CtlError.Object.OldDate()) {
      CtlError.AddError("Old Date is required");
    }
  },

   OldReasonValid: function(Value, Rule, CtlError) {
    if (CtlError.Object.OldInd() && CtlError.Object.OldReason().trim().length == 0) {
      CtlError.AddError("Old Reason is required");
    }
  },

  OldIndSet: function(self) {
    self.RentalCarAgentBranchList().Iterate(function (RCAB, Ind) {
      RCAB.OldInd(self.OldInd());
    });
  },

   OldDateSet: function(self) {
    self.RentalCarAgentBranchList().Iterate(function (RCAB, Ind) {
      RCAB.OldDate(self.OldDate());
    });
  },

   OldReasonSet: function(self) {
    self.RentalCarAgentBranchList().Iterate(function (RCAB, Ind) {
      RCAB.OldReason(self.OldReason());
    });
  },
}