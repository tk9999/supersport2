﻿SystemAreaDisciplineShiftRateBO = {
  FilterSystemAllowedAreas: function () {
    var Allowed = [];
    ClientData.ROSystemAllowedAreaList.Iterate(function (Item, Index) {
      if (Item.SystemID == ViewModel.CurrentSystemID()) {
        Allowed.push({ ProductionAreaID: Item.ProductionAreaID, ProductionArea: Item.ProductionArea });
      }
    });
    return Allowed;
  },

  SystemAreaDisciplineShiftRateToString: function () {
    return "Shift Rates";
  },
  ToString: function (self) {
    var System = ClientData.ROSystemList.Find('SystemID', self.SystemID());
    var ProductionArea = ClientData.ROProductionAreaList.Find('ProductionAreaID', self.ProductionAreaID());
    var Sys = (System == undefined) ? '' : ' - System: ' + System.System;
    var ProdArea = (ProductionArea == undefined) ? '' : ' - Production Area: ' + ProductionArea.ProductionArea;
    var Weekend = (self.WeekendInd() == undefined) ? '' : ' - Weekend: ' + (self.WeekendInd() == true) ? 'Yes' : 'No';
    return 'Discipline Shift Rate' + Sys + ProdArea + Weekend;
  },

  //DisciplineIDSet: function (DisciplineID) {
  //  ViewModel.ROHumanResourceListCriteria().DisciplineID(DisciplineID);
  //  }
  CanEdit: function (ShiftRate, FieldName) {
    switch (FieldName) {
      case 'SelectRate':
        if (ViewModel.SelectedShiftRate().IsValid()) {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  }
}

DisciplineShiftRateTemplateBO = {
  CanEdit: function (ShiftRate, FieldName) {
    switch (FieldName) {
      case 'AssignHR':
        if (ViewModel.SelectedShiftRate().DisciplineID() != 0 && ViewModel.SelectedShiftRate().DisciplineID() != undefined) {
          return true;
        }
        break;
      case 'ApplyToRates':
        if (ViewModel.SelectedShiftRate().DisciplineID() != null &&
            ViewModel.SelectedShiftRate().EffectiveStartDate() != null &&
            ViewModel.SelectedShiftRate().StartTime() != null &&
            ViewModel.SelectedShiftRate().EndTime() != null &&
            ViewModel.SelectedShiftRate().Rate() != null) {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  },
  DisciplineShiftRateTemplateBOToString: function (self) {
    return "Discipline Shift Rate Template";
  },
  TimesValid: function (Value, Rule, CtlError) {
    var Rate = CtlError.Object;
    if (Rate.StartTime() > Rate.EndTime()) {
      CtlError.AddError("Start Time cannot be before End Time");
    }
  }
}