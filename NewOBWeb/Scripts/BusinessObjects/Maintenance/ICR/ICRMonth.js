﻿ICRMonthBO = {

   SetMonthYear: function(self) {
    if (self.EndDate()) {
      var d = new Date(self.EndDate());
      self.Month((d.getMonth()) + 1);
      self.Year(d.getFullYear());
    }
    else {
      self.Month(null);
    }
  },

   StartEndDateDifference: function(Value, Rule, CtlError) {
     var ICRMonth = CtlError.Object;
    var StartDate = (new Date(ICRMonth.StartDate())).getTime();
    var EndDate = (new Date(ICRMonth.EndDate())).getTime();
    var one_day = 1000 * 60 * 60 * 24;

    var difference = EndDate - StartDate;

    if (Math.round(difference / one_day) > 31) {
      CtlError.AddWarning("Start and End Date Greater than 1 month");
    }
  },
}