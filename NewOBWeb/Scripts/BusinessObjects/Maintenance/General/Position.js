﻿PositionBO = {

   PositionCodeValid: function(Value, Rule, CtlError) {
     var obj = CtlError.Object;
    if (obj.GetParent().DisciplineID() == 4 && obj.PositionCode().trim() == '') {
      CtlError.AddError("Position Code is required for discipline Camera Operator");
    }
  },

}