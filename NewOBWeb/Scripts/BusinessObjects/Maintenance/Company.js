﻿CompanyMonthBO = {
  SetMonthYear: function (self) {
    if (self.EndDate()) {
      var d = new Date(self.EndDate());
      self.Month((d.getMonth()) + 1);
      self.Year(d.getFullYear());
    }
    else {
      self.Month(null);
    }
  },
  StartEndDateDifference: function (Value, Rule, CtlError) {
    var ICRMonth = CtlError.Object;
    var StartDate = (new Date(ICRMonth.StartDate())).getTime();
    var EndDate = (new Date(ICRMonth.EndDate())).getTime();
    var one_day = 1000 * 60 * 60 * 24;

    var difference = EndDate - StartDate;

    if (Math.round(difference / one_day) > 31) {
      CtlError.AddWarning("Start and End Date Greater than 1 month");
    }
  }
}
SystemEmailRecipientBO = {
  UserIDAndEmailAddressValid: function (Value, Rule, CtlError) {
    var HasUser = (CtlError.Object.UserID() ? true : false);
    if (CtlError.Object.EmailAddress() != undefined) {
      var HasEmailAddress = ($.trim(CtlError.Object.EmailAddress()).length > 0 ? true : false);
      if (HasUser && HasEmailAddress) {
        CtlError.AddError('Either User or Email Address must be specified, not both');
      } else if (!HasUser && !HasEmailAddress) {
        CtlError.AddError('Please specify either User or Email Address');
      }
    }
  }
}