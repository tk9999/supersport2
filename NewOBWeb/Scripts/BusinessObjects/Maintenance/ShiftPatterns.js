﻿SystemAreaShiftPatternBO = {
  UpdateNoWeekDays: function (self) {
    self.SystemAreaShiftPatternWeekDayList().Clear();
    for (var i = 0; i < self.ShiftDuration() ; i++) {
      for (var k = 0; k < 7; k++) {
        var NewSPWD = self.SystemAreaShiftPatternWeekDayList.AddNew();
        NewSPWD.WeekNo(i + 1);
        NewSPWD.WeekDay(k + 1);
      }
    }
  },
  CanEdit: function (ShiftPattern, FieldName) {
    switch (FieldName) {
      case 'GenHRBtn':
        if (ViewModel.EditableShiftPattern().IsValid() && 
            (ViewModel.EditableTeam() != null && ViewModel.EditableTeam().IsValid()) &&
             ViewModel.EditableTeam().TeamDisciplineHumanResourceList().length > 0) {
          return true;
        }
        break;
      case 'ShiftPanelPattern':
        if (ViewModel.EditableShiftPattern() != null/* && ViewModel.RemovingTeam() == false*/) {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  },
  SystemAreaShiftPatternBOToString: function (self) {
    var PatternName = (self.PatternName() == undefined || self.PatternName().trim().toString() == '') ? '' : ' - Pattern Name: ' + self.PatternName();
    //var StartDate = (self.StartDate() == undefined) ? '' : ' - Start Date: ' + self.StartDate().format('dd-MMM-yy');
    //var EndDate = (self.EndDate() == undefined) ? '' : ' - End Date: ' + self.EndDate().format('dd-MMM-yy');
    return 'System Area Shift Pattern: ' + PatternName;// + StartDate + EndDate;
  }
}

SystemAreaShiftPatternWeekDayBO = {
  SetStartEndTime: function (self) {
    if (self.OffDay() == true) {
      var sd = new Date();
      var ed = new Date();
      sd.setHours(0, 1, 0);
      ed.setHours(23, 59, 0);
      self.StartTime(sd.format('HH:mm'));
      self.EndTime(ed.format('HH:mm'));
    }
  },
  SetStartEndTimeFromShiftType: function (self) {
    if (self.ShiftTypeID()) {
      if (ViewModel.SASPProductionAreaID() != 2) {
        var st = ClientData.ROSystemAreaShiftTypeList.Find('ShiftTypeID', self.ShiftTypeID());
        if (st != undefined) {
          var mStartTime = moment.duration(st.StartTime);
          var sd = moment({ years: 2000, months: 1, date: 1, hours: mStartTime.hours(), minutes: mStartTime.minutes(), seconds: 0, milliseconds: 0 }).format("HH:mm");
          var mEndTime = moment.duration(st.EndTime);
          var ed = moment({ years: 2000, months: 1, date: 1, hours: mEndTime.hours(), minutes: mEndTime.minutes(), seconds: 0, milliseconds: 0 }).format("HH:mm");
          //self.StartTime(sd._d);
          //self.EndTime(ed._d);
          self.StartTime(sd);
          self.EndTime(ed);
          if (st.ShiftTypeID == 3) {
            self.OffDay(true);
          }
          else {
            self.OffDay(false);
          }
        }
      }
      else {
        self.StartTime(null);
        self.EndTime(null);
      }
    }
  },
  SystemAreaShiftPatternWeekDayBOToString: function (self) {
    var WeekNo = (self.WeekNo() == undefined || self.WeekNo() == 0) ? '' : ' - Week Number: ' + self.WeekNo();
    var WeekDay = (self.WeekDay() == undefined || self.WeekDay() == 0) ? '' : ' - Week Day: ' + self.WeekDay();
    return 'System Area Shift Pattern Week Day: ' + WeekNo + WeekDay;
  }
}

