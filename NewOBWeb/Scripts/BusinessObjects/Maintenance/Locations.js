﻿CountryBO = {
  DefaultDailySnTRateValid: function (Value, Rule, CtlError) {
    var DefaultDailySnTRate = CtlError.Object.DefaultDailySnTRate();
    if (DefaultDailySnTRate < 0) {
      CtlError.AddError("Default Daily S&T Rate cannot be less then 0");
    }
  }
}
CityBO = {
  CheckCityIsNew: function (Value, Rule, CtlError) {
    if (CtlError.Object.IsNew()) {
      CtlError.AddWarning("Please save the new city before adding city to city records");
    }
  },
  CityBOToString: function (self) {
    return self.City();
  }
}
CityToCityBO = {
  CheckParentCityID: function (Value, Rule, CtlError) {
    var ctc = CtlError.Object;
    var ParentCityID = ctc.GetParent().CityID();
    var CityID2 = ctc.CityID2();
    if (ParentCityID == CityID2) {
      CtlError.AddError("Start and end cities cannot be the same");
    }
  }
}