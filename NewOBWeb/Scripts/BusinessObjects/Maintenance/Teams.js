﻿TeamBO = {
  TeamNameValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.TeamName().trim() == "")
        CtlError.AddError("Team Name Required");
    }
  },
  SystemValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.SystemID() == undefined)
        CtlError.AddError("Sub-Dept. Required");
    }
  },
  ProductionAreaValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.SystemID() == undefined || Team.SystemID() == null)
        CtlError.AddError("Production Area Required");
    }
  },
  PatternValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.SystemAreaShiftPatternID() == undefined)
        CtlError.AddError("System Area Shift Pattern Required");
    }
  },
  TeamNumberValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.TeamNo() == undefined || Team.TeamNo() <= 0)
        CtlError.AddError("Team Number Required");
    }
  },
  CanEdit: function (Team, FieldName) {
    switch (FieldName) {
      case 'TeamHR':
        if (ViewModel.EditableSystemTeamShiftPattern() != null/* && ViewModel.RemovingTeam() == false*/) {
          return true;
        }
        break;
      case 'AddBtn':
        if (ViewModel.EditableSystemTeamShiftPattern() != null) {
          return true;
        }
        break;
      case 'HasDiscipline':
        if (Team.DisciplineID() != undefined) {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  },
  ToString: function (self) {
    var Name = (self.TeamName() == undefined || self.TeamName().trim() == "") ? '' : ' - Name: ' + self.TeamName();
    var SystemS = ClientData.ROSystemList.Find('SystemID', self.SystemID());
    var System = (SystemS == undefined) ? '' : ' - Sub-Dept: ' + SystemS.System;
    var ProductionAreaS = ClientData.ROSystemAllowedAreaList.Find('ProductionAreaID', self.ProductionAreaID());
    var ProductionArea = (ProductionAreaS == undefined) ? '' : ' - Area: ' + ProductionAreaS.ProductionArea;
    var ShiftPatternS = ClientData.ROSystemAreaShiftPatternList.Find('SystemAreaShiftPatternID', self.SystemAreaShiftPatternID());
    var ShiftPattern = (ShiftPatternS == undefined) ? '' : ' - Shift Pattern: ' + ShiftPatternS.PatternName;
    return 'Team ' + Name + System + ProductionArea + ShiftPattern;
  }
}
TeamRequirementDetailBO = {
  GetScheduledHoursForPeriod: function (self) {
    //var ThisYearGroupPeriod = ClientData.ROYearGroupPeriodList.Find('YearGroupPeriodID', self.YearGroupPeriodID());
    var EmployeeHours = [];
    var Hours = 0;
    var TotalHours = 0;
    var HRID = 0;
    var CurrentHR = ViewModel.CurrentTeamRequirementDetailHumanresource();
    var CurrentHRList = ViewModel.TeamRequirementDetailHumanresourceList();
    if (self.YearGroupPeriodID()) {
      //if (ThisYearGroupPeriod) {
      Singular.GetDataStateless('OBLib.Shifts.ICR.ReadOnly.ROHumanResourceGroupShiftList, OBLib', {
        HumanResourceID: null,
        StartDate: self.StartDate(),
        EndDate: self.EndDate(),
        SystemID: ViewModel.CurrentSystemID(),
        ProductionAreaID: ViewModel.CurrentProductionAreaID(),
        TeamID: self.GetParent().GetParent().TeamID()
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.HumanResourceGroupShiftList);
          Singular.HideLoadingBar();
          for (var i = 0; i < ViewModel.HumanResourceGroupShiftList().length; i++) {
            for (var n = 0; n < ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList().length; n++) {
              HRID = ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].HumanResourceID();
              var sd = new Date(ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].StartDateTime());
              var ed = new Date(ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].EndDateTime());
              var diff = ed - sd;
              var hh = Math.floor(diff / 1000 / 60 / 60);
              Hours += hh;
            }
            CurrentHR.HumanResourceID(HRID);
            CurrentHR.HoursWorked(Hours);
            CurrentHRList.push(CurrentHR);
            EmployeeHours.push([HRID, Hours]);
            TotalHours += Hours;
            Hours = 0;
          }
          self.TotalScheduledHoursWorked(TotalHours);
          for (var p = 0; p < EmployeeHours.length; p++) {
            ViewModel.genericlist.push(EmployeeHours[p]);
          }
          //ViewModel.genericlist(EmployeeHours);
        }
      });
    }
    else {
      self.TotalScheduledHoursWorked(0);
    }
    TotalHours = 0;
  }
}
TeamShiftPatternBO = {
  CheckforDuplicates: function (Value, Rule, CtlError) {
    var TeamShiftPattern = CtlError.Object;
    if (self) {
      var dups = [];
      if (ViewModel.TeamShiftPatternList().length > 1) {
        for (var i = 0; i < ViewModel.TeamShiftPatternList().length; i++) {
          if (Value == ViewModel.TeamShiftPatternList()[i].SystemTeamID())
            dups.push(ViewModel.TeamShiftPatternList()[i]);
          if (dups.length > 1) {//We Have a Duplicate Here
            var ts = $.grep(dups, function (h) {
              return h.SystemTeamID() == Value;
            }); //Array of Duplicate Teams Found
            if (ts.length > 0) {
              for (var p = 0; p < dups.length - 1; p++) {
                var anchor = dups[p];
                for (var j = 0; j < dups.length; j++) {
                  var slider = dups[j];
                  if (anchor.Guid() !== slider.Guid()) {
                    var startdate1 = new Date(anchor.StartDate());
                    var enddate1 = new Date(anchor.EndDate());
                    var startdate2 = new Date(slider.StartDate());
                    var enddate2 = new Date(slider.EndDate());
                    if ((startdate1 <= enddate2) && (enddate1 >= startdate2)) {
                      CtlError.AddError('This Team has already been selected within this date range');
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  StartBeforeEnd: function (Value, Rule, CtlError) {
    var TeamShiftPattern = CtlError.Object;
    if (TeamShiftPattern.EndDate() != undefined && TeamShiftPattern.StartDate() != undefined) {
      var d1 = new Date(TeamShiftPattern.StartDate());
      var d2 = new Date(TeamShiftPattern.EndDate());
      if (d2.getTime() < d1.getTime()) {
        CtlError.AddError("Start date must be before end date");
      }
    }
  },
  HasPatternName: function (Value, Rule, CtlError) {
  },
  CanEdit: function (TeamShiftPattern, FieldName) {
    switch (FieldName) {
      case 'GenerateShifts':
        if (TeamShiftPattern.IsValid() && !TeamShiftPattern.IsNew()) {
          return true;
        }
        else
          return false;
        break;
      default:
        return true;
        break;
    }
  },
  FilterPatterns: function (List, obj) {
    var results = []
    List.Iterate(function (item, indx) {
      if (ViewModel.CurrentSystemID() == item.SystemID) {
        results.push(item)
      }
    })
    return results
  }
}