﻿SmsBO = {
  SmsTemplateTypeIDSet: function (sms) {
    if (sms.SmsTemplateTypeID()) {
      var selectedItem = ClientData.ROSmsTemplateTypeList.Find(sms.SmsTemplateTypeID())
      sms.Message(selectedItem.MessageTemplate)
    } else {
      sms.Message("")
    }
  },
  MessageSet: function (sms) {
    console.log(Math.round((sms.Message().length / 153)))
    var rounded = Math.round((sms.Message().length / 153))
    sms.SmsesRequired((rounded === 0 ? 1 : rounded))
  },

  getNewRecipientPrimary: function (sms, ROHumanResourceContact) {
    var smsRecip = sms.SmsRecipientList.AddNew()
    smsRecip.CellNo(ROHumanResourceContact.CellPhoneNumber())
    smsRecip.RecipientName(ROHumanResourceContact.PreferredFirstSurname())
    smsRecip.HumanResourceID(ROHumanResourceContact.HumanResourceID())
    return smsRecip;
  },
  getNewRecipientSecondary: function (sms, ROHumanResourceContact) {
    var smsRecip = sms.SmsRecipientList.AddNew()
    smsRecip.CellNo(ROHumanResourceContact.AlternativeContactNumber())
    smsRecip.RecipientName(ROHumanResourceContact.PreferredFirstSurname())
    smsRecip.HumanResourceID(ROHumanResourceContact.HumanResourceID())
    return smsRecip;
  },

  saveSms: function (sms, options) {
    ViewModel.CallServerMethod("SaveSmsList", {
      SmsList: [sms.Serialise()]
    },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Sms Saved", "", 250)
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Sms Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onSuccess(onFail) }
        }
      })
  },
  saveSmsList: function (smsList, options) {
    ViewModel.CallServerMethod("SaveSmsList", {
      SmsList: smsList.Serialise()
    },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Smses Saved", "", 250)
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Smses Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onSuccess(onFail) }
        }
      })
  }
}

SmsRecipientBO = {
  smsRecipientHTML: function (smsRecipient) {
    var path = window.SiteInfo.SitePath.toString() + "/Images/InternalPhotos/" + (!smsRecipient.HumanResourceID() ? "" : smsRecipient.HumanResourceID().toString()) + "_5.jpg";
    return '<img class="sms-recipient-avatar" src="' + path + '" alt="Avatar"> ' +
      '<div class="sms-recipient-name">' + smsRecipient.RecipientName() + '</div>' +
      '<div class="sms-recipient-cellno">' + smsRecipient.CellNo() + '</div>'
  }
};

SmsBatchBO = {
  SmsBatchTemplateIDSet: function (SmsBatch) {
    if (SmsBatch.SmsBatchTemplateID() == 1) {
      SmsBatch.SystemID(1)
      SmsBatch.ProductionAreaID(2)
    }
    else if (SmsBatch.SmsBatchTemplateID() == 2) {
      SmsBatch.SystemID(5)
    }
    else if (SmsBatch.SmsBatchTemplateID() == 3) {
      SmsBatch.SystemID(1)
      SmsBatch.ProductionAreaID(1)
    }
    else if (SmsBatch.SmsBatchTemplateID() == 4) {
      SmsBatch.SystemID(4)
    }
  },
  AllDaysSet: function (SmsBatch) {
    SmsBatch.SmsBatchDateList().Iterate(function (smsB, smsBIndx) {
      smsB.IsSelected(SmsBatch.AllDays())
    })
  },
  CancellationsSet: function (SmsBatch) { },

  SmsBatchTemplateIDValid: function (Value, Rule, CtlError) {
    var batch = CtlError.Object
    if (batch.SmsBatchTemplateID()) {
      if (batch.SmsBatchTemplateID() == 1) {
        //Schedule - Production Services
        if (!batch.StartDate()) { CtlError.AddError("Start Date is required") }
        if (!batch.EndDate()) { CtlError.AddError("End Date is required") }
        if (!batch.SystemID()) { CtlError.AddError("Sub-Dept is required") }

      } else if (batch.SmsBatchTemplateID() == 2) {
        //Schedule - Playout Operations
        if (!batch.StartDate()) { CtlError.AddError("Start Date is required") }
        if (!batch.EndDate()) { CtlError.AddError("End Date is required") }
        if (!batch.SystemID()) { CtlError.AddError("Sub-Dept is required") }

      } else if (batch.SmsBatchTemplateID() == 3) {
        //Schedule - OB
        if (!batch.StartDate()) { CtlError.AddError("Start Date is required") }
        if (!batch.EndDate()) { CtlError.AddError("End Date is required") }
        if (!batch.SystemID()) { CtlError.AddError("Sub-Dept is required") }
        if (!batch.ProductionSystemAreaID()) { CtlError.AddError("Event is required") }
        //if (batch.SmsBatchDateList().length == 0) { CtlError.AddError("At least one 'Event Day' must be selected") }

      } else if (batch.SmsBatchTemplateID() == 4) {
        //Schedule - ICR
        if (!batch.StartDate()) { CtlError.AddError("Start Date is required") }
        if (!batch.EndDate()) { CtlError.AddError("End Date is required") }
        if (!batch.SystemID()) { CtlError.AddError("Sub-Dept is required") }
      }
    } else {
      CtlError.AddError("Template is required")
    }
  },

  FilteredDisciplineList: function (ROProductionAreaAllowedDisciplineList, SmsBatch) {
    var results = []
    ROProductionAreaAllowedDisciplineList.Iterate(function (disc, discInd) {
      if (disc.SystemID == SmsBatch.SystemID()) {
        if (SmsBatch.ProductionAreaID()) {
          if (SmsBatch.ProductionAreaID() == disc.ProductionAreaID) {
            results.push(disc)
          }
        } else {
          results.push(disc)
        }
      }
    })
    return results
  },
  GetNewRecipientPrimary: function (smsBatch, ROHumanResourceContact) {
    var smsRecip = smsBatch.SmsBatchRecipientList.AddNew();
    smsRecip.CellNo(ROHumanResourceContact.CellPhoneNumber());
    smsRecip.RecipientName(ROHumanResourceContact.PreferredFirstSurname());
    smsRecip.HumanResourceID(ROHumanResourceContact.HumanResourceID());
    return smsRecip;
  },
  GetNewRecipientSecondary: function (smsBatch, ROHumanResourceContact) {
    var smsRecip = smsBatch.SmsBatchRecipientList.AddNew();
    smsRecip.CellNo(ROHumanResourceContact.AlternativeContactNumber());
    smsRecip.RecipientName(ROHumanResourceContact.PreferredFirstSurname());
    smsRecip.HumanResourceID(ROHumanResourceContact.HumanResourceID());
    return smsRecip;
  },

  triggerProductionSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDate = args.Object.StartDate()
    args.Data.EndDate = args.Object.EndDate()
    args.Data.Scenario = "Sms"
  },
  afterProductionSystemAreaRefreshAjax: function (args) {

  },
  ProductionSystemAreaIDSet: function (self) {
    ////days
    //var daysToRemove = []
    //self.SmsBatchDateList().Iterate(function (itm, indx) {
    //  daysToRemove.push(itm)
    //});
    //daysToRemove.Iterate(function (itm, indx) {
    //  self.SmsBatchDateList.RemoveNoCheck(itm)
    //})
    ////crewtypes
    //var ctToRemove = []
    //self.SmsBatchCrewTypeList().Iterate(function (itm, indx) {
    //  ctToRemove.push(itm)
    //});
    //ctToRemove.Iterate(function (itm, indx) {
    //  self.SmsBatchCrewTypeList.RemoveNoCheck(itm)
    //})
  },
  onProductionSystemAreaIDSelected: function (item, businessObject) {

    ROPSACrewGroupBO.get({
      criteria: {
        ProductionSystemAreaID: item.ProductionSystemAreaID
      },
      onSuccess: function (response) {
        response.Data.Iterate(function (cg, cgInd) {
          //crew
          var smsBatchCrewGroup = businessObject.SmsBatchCrewGroupList.AddNew()
          smsBatchCrewGroup.CrewTypeID(cg.CrewTypeID)
          smsBatchCrewGroup.GroupName(cg.CrewType)
          //hr
          cg.ROPSACrewGroupHRList.Iterate(function (cgh, cghInd) {
            var smsBatchCrewGroupHR = smsBatchCrewGroup.SmsBatchCrewGroupHumanResourceList.AddNew()
            smsBatchCrewGroupHR.HumanResourceID(cgh.HumanResourceID)
            smsBatchCrewGroupHR.HRName(cgh.HRName)
            smsBatchCrewGroupHR.ImageFileName(cgh.ImageFileName)
          })
        })
      },
      onFail: function (response) { }
    })

    ////days
    //businessObject.SmsBatchDateList([])
    //item.ROPSADayList.Iterate(function (tl, tlInd) {
    //  var smsBatchDate = businessObject.SmsBatchDateList.AddNew();
    //  smsBatchDate.BatchDate(tl.TimelineDate)
    //})
    ////crewtypes
    //businessObject.SmsBatchCrewTypeList([])
    //item.ROPSACrewTypeList.Iterate(function (ct, ctInd) {
    //  var smsBatchCrewType = businessObject.SmsBatchCrewTypeList.AddNew()
    //  smsBatchCrewType.CrewTypeID(ct.CrewTypeID)
    //  smsBatchCrewType.CrewType(ct.CrewType)
    //})
  },

  save: function (serialisedList, options) {
    ViewModel.CallServerMethod("SaveSmsBatchList",
      { SmsBatchList: serialisedList },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
        }
      })
  },
  saveItem: function (smsBatch, options) {
    var lst = []
    lst.push(smsBatch.Serialise())
    this.save(lst, options)
  }
}

BatchSmsBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Notifications.SmS.BatchSmsList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Batch Smses Failed", response.ErrorText, 1000)
        }
      })
  },
  save: function (serialisedList, options) {
    //ViewModel.CallServerMethod("SaveTravelRequisitionTravellerSnTList",
    //                           { TravelRequisitionTravellerSnTList: (list ? list.Serialise() : serialisedList) },
    //                           function (response) {
    //                             if (response.Success) {
    //                               OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
    //                               if (options.onSuccess) { options.onSuccess(response) }
    //                             } else {
    //                               OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
    //                             }
    //                           })
  },
  saveItem: function (travellerSnT, options) {
    //let x = async 
    //travellerSnT.IsProcessing(true)
    //var list = []
    //list.push(travellerSnT.Serialise())
    //this.save(null, list, {
    //  onSuccess: function (response) {
    //    TravellerSnTBO.get({
    //      criteria: { TravelRequisitionID: travellerSnT.GetParent().TravelRequisitionID(), HumanResourceID: travellerSnT.HumanResourceID(), FetchSnTDetails: false },
    //      onSuccess: function (response) {
    //        KOFormatter.Deserialise(response.Data[0], travellerSnT)
    //        travellerSnT.IsExpanded(false)
    //      }
    //    })
    //  }
    //})
  }
}

ROSmsSearchBO = {
  itemCss: function (roSmsSearch) {
    if (roSmsSearch.IsSelected()) {
      return 'item sms-selected'
    } else {
      return 'item'
    }
  }
}

ROSmsSearchListCriteriaBO = {

  //set expressions
  CreatedStartDateSet: function (self) {

  },
  CreatedEndDateSet: function (self) {

  },

  //validation
  CreatedStartDateValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() === 3 && obj.CreatedStartDate() == null) { CtlError.AddError("Start Date is required") }
  },
  CreatedEndDateValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 3 && obj.CreatedEndDate() == null) { CtlError.AddError("End Date is required") }
  },
  SmsBatchIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 3 && obj.SmsBatchID() == null) { CtlError.AddError("Batch is required") }
  },
  CrewTypeIDValid: function (Value, Rule, CtlError) {
    //nothing required
  },
  ProductionSystemAreaStartDateValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 2 && obj.ProductionSystemAreaStartDate() == null) { CtlError.AddError("Event Start Day is required") }
  },
  ProductionSystemAreaEndDateValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 2 && obj.ProductionSystemAreaEndDate() == null) { CtlError.AddError("Event End Day is required") }
  },
  ProductionSystemAreaIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 2 && obj.ProductionSystemAreaID() == null) { CtlError.AddError("Event is required") }
  },
  StartDayValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 1 && obj.StartDay() == null) { CtlError.AddError("Start Day is required") }
  },
  EndDayValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.Scenario() == 1 && obj.EndDay() == null) { CtlError.AddError("End Day is required") }
  },

  //dropdowns
  //psa
  triggerProductionSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.SystemIDs = args.Object.SystemIDs()
    args.Data.ProductionAreaIDs = args.Object.ProductionAreaIDs()
    args.Data.StartDate = args.Object.ProductionSystemAreaStartDate()
    args.Data.EndDate = args.Object.ProductionSystemAreaEndDate()
  },
  afterProductionSystemAreaRefreshAjax: function (args) {

  },
  ProductionSystemAreaIDSet: function (self) {

  },
  onProductionSystemAreaIDSelected: function (item, businessObject) {

  },

  //user
  triggerCreatedByUserIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setCreatedByUserIDCriteriaBeforeRefresh: function (args) {

  },
  afterCreatedByUserIDRefreshAjax: function (args) {

  },
  CreatedByUserIDSet: function (self) {

  },
  onUserIDSelected: function (item, businessObject) {

  },

  //batch
  triggerSmsBatchIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setSmsBatchIDCriteriaBeforeRefresh: function (args) {
    args.Data.CreatedBy = args.Object.CreatedByUserID()
    args.Data.CreatedStartDate = args.Object.CreatedStartDate()
    args.Data.CreatedEndDate = args.Object.CreatedEndDate()
  },
  afterSmsBatchIDRefreshAjax: function (args) {

  },
  SmsBatchIDSet: function (self) {

  },
  onSmsBatchIDSelected: function (item, businessObject) {

  },

  triggerSmsBatchCrewTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setSmsBatchCrewTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SmsBatchID = args.Object.SmsBatchID()
  },
  afterSmsBatchCrewTypeIDRefreshAjax: function (args) {

  },
  SmsBatchCrewTypeIDSet: function (self) {

  },
  onSmsBatchCrewTypeIDSelected: function (item, businessObject) {

  },

  //recipient
  triggerRecipientHumanResourceIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setRecipientHumanResourceIDCriteriaBeforeRefresh: function (args) {
    args.Data.PageNo = 1
    args.Data.PageSize = 1000
    args.Data.SortColumn = "FirstName"
    args.Data.SortAsc = true
  },
  afterRecipientHumanResourceIDRefreshAjax: function (args) {

  },
  RecipientHumanResourceIDSet: function (self) {

  },
  onRecipientHumanResourceIDSelected: function (item, businessObject) {

  }
}

SmsSearchBO = {
  itemCss: function (SmsSearch) {
    if (SmsSearch.IsSelected()) {
      return 'item sms-selected'
    } else {
      return 'item'
    }
  },
  readonlyStateCss: function (SmsSearch) {
    if (SmsSearch.InEditMode()) {
      return 'animated fadeOut fast'
    } else {
      return 'animated fadeIn fast'
    }
  },
  editStateCss: function (SmsSearch) {
    if (SmsSearch.InEditMode()) {
      return 'animated fadeIn fast'
    } else {
      return 'animated fadeOut fast'
    }
  },
  isSelectedClick: function (SmsSearch) {
    SmsSearch.IsSelected(!SmsSearch.IsSelected())
  },

  readOnlyStateClick: function (SmsSearch) {
    SmsSearch.InEditMode(!SmsSearch.InEditMode())
  },
  editStateClick: function (SmsSearch) {
    SmsSearch.InEditMode(!SmsSearch.InEditMode())
  },

  progressBarStyle: function (SmsSearch) {
    return { width: SmsSearch.SentPercentageString() }
  },
  progressBarHtml: function (SmsSearch) {
    return SmsSearch.SentPercentageString()
  },

  saveItem: function (SmsSearch, options) {
    ViewModel.CallServerMethod("SaveSmsSearchList", {
      SmsSearchList: [SmsSearch.Serialise()]
    },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Sms saved successfully")
          if (options.onSuccess) { options.onSuccess.call(SmsSearchBO, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Failed to save Sms")
          if (options.onFail) { options.onFail.call(SmsSearchBO, response) }
        }
      })
  },

  canEdit: function (what, smsSearch) {
    switch (what) {
      case 'FullMessage':
        return (smsSearch.InTransitCount() > 0 || smsSearch.FirstSentTime() ? false : true)
        break;
      case 'SaveButton':
        return (smsSearch.InTransitCount() > 0 || smsSearch.FirstSentTime() ? false : true)
        break;
      default:
        return true;
        break;
    }
  }
}

SmsSearchRecipientBO = {
  imgSource: function (smsSearchRecipient) {
    return window.SiteInfo.SitePath + '/Images/InternalPhotos/' + smsSearchRecipient.ImageFileName().toString()
  },
  canEdit: function (what, smsSearch) {
    switch (what) {
      case 'FullMessage':
        return (smsSearch.InTransitCount() > 0 || smsSearch.FirstSentTime() ? false : true)
        break;
      case 'SaveButton':
        return (smsSearch.InTransitCount() > 0 || smsSearch.FirstSentTime() ? false : true)
        break;
      default:
        return true;
        break;
    }
  },
  canDelete: function (smsSearchRecipient) {
    return (smsSearch.StatusCode() != '' || smsSearchRecipient.SentDate() ? false : true)
  },
  async retrySmsRecipient(smsSearchRecipient) {
    smsSearchRecipient.IsProcessing(true);
    try {
      let retryResult = await ViewModel.CallServerMethodPromise("RetrySmsRecipient", { SmsSearchRecipient: smsSearchRecipient.Serialise() });
      smsSearchRecipient.IsProcessing(false);
      OBMisc.Notifications.GritterSuccess("Retry Succeeded", "The sms will be resent in approx 1 minute", 250);
    }
    catch (err) {
      OBMisc.Notifications.GritterError("Retry Failed", err.message, 1000);
      smsSearchRecipient.IsProcessing(false);
    };
  }
}

SmsBatchCrewGroupHumanResourceBO = {
  src: function (obj) {
    return window.SiteInfo.SitePath + "/Images/InternalPhotos/" + obj.ImageFileName().toString()
  }
}

EmailBatchBO = {
  EmailBatchTemplateIDSet: function (EmailBatch) {
    switch (EmailBatch.EmailBatchTemplateID()) {
      case 1:
        //studio
        EmailBatch.SystemID(1);
        //EmailBatch.ProductionAreaID(1);
        EmailBatch.StartDate(null);
        EmailBatch.EndDate(null);
        break;
    }
  },
  CanEdit: function (FieldName, EmailBatch) {
    switch (FieldName) {
      default:
        if (EmailBatch.IsNew()) {
          return true;
        } else {
          return false;
        }
        break;
    }
  },
  CanView: function (ItemName, EmailBatch) {
    switch (ItemName) {
      case 'GenerateButton':
        if (EmailBatch.IsNew()) {
          return true;
        } else {
          return false;
        }
      default:
        return true;
        break;
    }
  },
  ProductionServicesIndSet: function (EmailBatch) {
    EmailsPage.GetFilteredDisciplines();
  },
  OutsideBroadcastIndSet: function (EmailBatch) {
    EmailsPage.GetFilteredDisciplines();
  },
  StudiosIndSet: function (EmailBatch) {
    EmailsPage.GetFilteredDisciplines();
  },
  SelectedEmailIndSet: function (EmailBatch) {
    EmailsPage.GetFilteredHumanResources();
  },
  GetNewRecipientPrimary: function (EmailBatch, obj) {
    var emailRecip = EmailBatch.EmailBatchRecipientList.AddNew()
    emailRecip.EmailAddress(obj.EmailAddress())
    emailRecip.HumanResourceID(obj.HumanResourceID())
    return emailRecip;
  },
  GetNewRecipientSecondary: function (EmailBatch, obj) {
    var emailRecip = EmailBatch.EmailBatchRecipientList.AddNew()
    emailRecip.EmailAddress(obj.AlternativeEmailAddress())
    emailRecip.HumanResourceID(obj.HumanResourceID())
    return emailRecip;
  },
  SystemIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.SelectRecipientsManually() && !CtlError.Object.SystemID()) {
      CtlError.AddError("Sub-Dept is required")
    }
  },
  FilteredDisciplineList: function (ROProductionAreaAllowedDisciplineList, EmailBatch) {
    var results = []
    ROProductionAreaAllowedDisciplineList.Iterate(function (disc, discInd) {
      if (disc.SystemID == EmailBatch.SystemID()) {
        if (EmailBatch.ProductionAreaID()) {
          if (EmailBatch.ProductionAreaID() == disc.ProductionAreaID) {
            results.push(disc)
          }
        } else {
          results.push(disc)
        }
      }
    })
    return results
  }
}

OBEmailBO = {
  CanEdit: function (FieldName, OBEmail) {
    switch (FieldName) {
      default:
        return true;
        break;
    }
  },
  CanView: function (ItemName, OBEmail) {
    switch (ItemName) {
      case 'SendButton':
        if (!OBEmail.IsNew() && OBEmail.SendStatus() != "Sent") {
          return true;
        } else {
          return false;
        }
      case 'ResendButton':
        if (!OBEmail.IsNew() && (OBEmail.SendStatus() == "Sent" || OBEmail.SendStatus() == "Failed to Send")) {
          return true;
        } else {
          return false;
        }
      default:
        return false;
        break;
    }
  }
}

ROBatchEmailBO = {
  CanEdit: function (FieldName, ROBatchEmail) {
    switch (FieldName) {
      default:
        return true;
        break;
    }
  },
  CanView: function (ItemName, ROBatchEmail) {
    switch (ItemName) {
      case 'SendButton':
        if (!ROBatchEmail.IsNew() && ROBatchEmail.SendStatus() != "Sent") {
          return true;
        } else {
          return false;
        }
      case 'ResendButton':
        if (!ROBatchEmail.IsNew() && (ROBatchEmail.SendStatus() == "Sent" || ROBatchEmail.SendStatus() == "Failed to Send")) {
          return true;
        } else {
          return false;
        }
      default:
        return false;
        break;
    }
  },
  AttachmentClicked: function (ROBatchEmail) {
    ViewModel.CallServerMethod("DownloadDocumentStateless", {
      DocumentID: ROBatchEmail.DocumentID()
    }, function (response) {
      if (response.Success) {
        Singular.DownloadFile(null, response.Data);
      } else {
        OBMisc.Notifications.GritterError('Error Downloading', response.ErrorText, null)
      }
    })
  }
}

SoberEmailBO = {
  EmailTemplateTypeIDSet: function (SoberEmail) {
    var Template = ClientData.ROEmailTemplateTypeList.Find("EmailTemplateTypeID", SoberEmail.EmailTemplateTypeID());
    SoberEmail.Body(Template.MessageTemplate);
    SoberEmail.HTMLBody(Template.MessageTemplate);
  },
  GetNewRecipientPrimary: function (SoberEmail, obj) {
    var emailRecip = SoberEmail.SoberEmailRecipientList.AddNew()
    emailRecip.EmailAddress(obj.EmailAddress())
    emailRecip.HumanResourceID(obj.HumanResourceID())
    return emailRecip;
  },
  GetNewRecipientSecondary: function (SoberEmail, obj) {
    var emailRecip = SoberEmail.SoberEmailRecipientList.AddNew()
    emailRecip.EmailAddress(obj.AlternativeEmailAddress())
    emailRecip.HumanResourceID(obj.HumanResourceID())
    return emailRecip;
  },
  getToString: function (SoberEmail) {
    if (SoberEmail.Subject().trim().length == 0) {
      return "Current Email"
    } else {
      return SoberEmail.Subject()
    }
  }
};

ROSoberEmailBO = {
  AttachmentCountDescription: function (ROSoberEmail) {
    if (ROSoberEmail.AttachmentCount() == 1) {
      return "Attachment";
    } else {
      return "Attachments";
    }
  },
  AttachmentsClicked: function (ROSoberEmail) {
    alert("")
    //ViewModel.CallServerMethod("DownloadDocumentStateless", {
    //  DocumentID: ROBatchEmail.DocumentID()
    //}, function (response) {
    //  if (response.Success) {
    //    Singular.DownloadFile(null, response.Data);
    //  } else {
    //    OBMisc.Notifications.GritterError('Error Downloading', response.ErrorText, null)
    //  }
    //})
  }
};

ROEmailSearchBO = {
  AttachmentCountDescription: function (ROEmailSearch) {
    if (ROEmailSearch.AttachmentCount() == 1) {
      return "Attachment";
    } else {
      return "Attachments";
    }
  },
  AttachmentsClicked: function (ROEmailSearch) {
    ViewModel.ROEmailSearchListManager().IsLoading(true)
    if (ROEmailSearch.IsBatch()) {
      ViewModel.CallServerMethod("DownloadEmailBatchAttachmentsStateless", {
        EmailBatchID: ROEmailSearch.EmailBatchID()
      }, function (response) {
        if (response.Success) {
          Singular.DownloadFile(null, response.Data)
          OBMisc.Notifications.GritterSuccess('Success', "attachments have been downloaded", 1000)
        } else {
          OBMisc.Notifications.GritterError('Error Downloading', response.ErrorText, 3000)
        }
        ViewModel.ROEmailSearchListManager().IsLoading(false)
      })
    } else {
      ViewModel.CallServerMethod("DownloadEmailAttachmentsStateless", {
        EmailID: ROEmailSearch.EmailID()
      }, function (response) {
        if (response.Success) {
          Singular.DownloadFile(null, response.Data);
          OBMisc.Notifications.GritterSuccess('Success', "attachments have been downloaded", 1000)
        } else {
          OBMisc.Notifications.GritterError('Error Downloading', response.ErrorText, 3000)
        }
        ViewModel.ROEmailSearchListManager().IsLoading(false)
      })
    }
  }
};

EmailRecipientBO = {
  RemoveFromEmail: function (EmailRecipient) {
    var email = EmailRecipient.GetParent();
    email.EmailRecipientList.Remove(EmailRecipient);
  },
  GetCssClass: function (EmailRecipient) {
    var baseCss = 'tags-search-choice';
    var statusCss = 'sms-status-group-pending';
    //switch (EmailRecipient.StatusGroup()) {
    //  case "":
    //    statusCss = 'sms-status-group-pending';
    //    break;
    //  case "Error":
    //    statusCss = 'sms-status-group-error';
    //    break;
    //  case "Pending":
    //    statusCss = 'sms-status-group-pending';
    //    break;
    //  case "In Transit":
    //    statusCss = 'sms-status-group-in-transit';
    //    break;
    //  case "Delivered":
    //    statusCss = 'sms-status-group-delivered';
    //    break;
    //  case "Cancelled":
    //    statusCss = 'sms-status-group-cancelled';
    //    break;
    //  case "Unknown":
    //    statusCss = 'sms-status-group-unknown';
    //    break;
    //  default:
    //    statusCss = 'sms-status-group-unknown';
    //    break;
    //}
    return baseCss + ' ' + statusCss;
  },
  CanDeleteClient: function (EmailRecipient) {
    if (EmailRecipient.IsNew()) {
      return true;
    }
    else {
      return false;
      //switch (SmsRecipient.StatusGroup()) {
      //  case "":
      //    return true;
      //    break;
      //  case "Error":
      //    return true;
      //    break;
      //  case "Pending":
      //    return true;
      //    break;
      //  case "In Transit":
      //    return false;
      //    break;
      //  case "Delivered":
      //    return false;
      //    break;
      //  case "Cancelled":
      //    return true;
      //    break;
      //  case "Unknown":
      //    return true;
      //    break;
      //  default:
      //    return true;
      //    break;
      //}
    }
  }
};

SoberEmailRecipientBO = {
  RemoveFromEmail: function (EmailRecipient) {
    var email = EmailRecipient.GetParent();
    email.SoberEmailRecipientList.Remove(EmailRecipient);
  },
  GetCssClass: function (EmailRecipient) {
    var baseCss = 'tags-search-choice';
    var statusCss = 'sms-status-group-pending';
    //switch (EmailRecipient.StatusGroup()) {
    //  case "":
    //    statusCss = 'sms-status-group-pending';
    //    break;
    //  case "Error":
    //    statusCss = 'sms-status-group-error';
    //    break;
    //  case "Pending":
    //    statusCss = 'sms-status-group-pending';
    //    break;
    //  case "In Transit":
    //    statusCss = 'sms-status-group-in-transit';
    //    break;
    //  case "Delivered":
    //    statusCss = 'sms-status-group-delivered';
    //    break;
    //  case "Cancelled":
    //    statusCss = 'sms-status-group-cancelled';
    //    break;
    //  case "Unknown":
    //    statusCss = 'sms-status-group-unknown';
    //    break;
    //  default:
    //    statusCss = 'sms-status-group-unknown';
    //    break;
    //}
    return baseCss + ' ' + statusCss;
  },
  CanDeleteClient: function (EmailRecipient) {
    if (EmailRecipient.IsNew()) {
      return true;
    }
    else {
      return false;
      //switch (SmsRecipient.StatusGroup()) {
      //  case "":
      //    return true;
      //    break;
      //  case "Error":
      //    return true;
      //    break;
      //  case "Pending":
      //    return true;
      //    break;
      //  case "In Transit":
      //    return false;
      //    break;
      //  case "Delivered":
      //    return false;
      //    break;
      //  case "Cancelled":
      //    return true;
      //    break;
      //  case "Unknown":
      //    return true;
      //    break;
      //  default:
      //    return true;
      //    break;
      //}
    }
  }
};

ROEmailSearchListCriteriaBO = {
  CreatedStartDateSet: function (self) {

  },
  CreatedEndDateSet: function (self) {

  },
  CreatedByMeSet: function (self) {

  },
  CreatedByOtherSet: function (self) {

  }
};