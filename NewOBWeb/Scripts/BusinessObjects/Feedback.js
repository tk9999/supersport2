﻿FeedbackReportBO = {
  IsCompletedSet: function (self) {
    if (self.IsCompleted()) {
      self.ReportCompletedBy(ViewModel.CurrentUserID())
      self.ReportCompletedByName(ViewModel.CurrentUserName())
      self.ReportCompletedDateTime(new Date())
    } else {
      self.ReportCompletedBy(null)
      self.ReportCompletedByName("")
      self.ReportCompletedDateTime(null)
    }
  },
  IsPublishedSet: function (self) {
    if (self.IsPublished()) {
      self.ReportPublishedBy(ViewModel.CurrentUserID())
      self.ReportPublishedByName(ViewModel.CurrentUserName())
      self.ReportPublishedDateTime(new Date())
    } else {
      self.ReportPublishedBy(null)
      self.ReportPublishedByName("")
      self.ReportPublishedDateTime(null)
    }
  },

  canEdit: function (fieldName, report) {
    switch (fieldName) {
      case "IsCompleted":
        if (!report.IsNew()) {
          if (report.IsSubReport()) {
            return !report.IsCompletedParent();
          } else {
            return true;
          }
        } else {
          return false
        }
        return !report.IsNew(); //&& !report.IsCompletedParent();
        break;
      case "IsPublished":
        return !report.IsNew();
        break;
    }
  },
  canView: function (fieldName, inst) { },

  isCompletedHTML: function (feedbackReport) {
    if (feedbackReport.IsCompleted()) {
      return '<i class="fa fa-check-square-o"></i> Completed by ' + feedbackReport.ReportCompletedByName() + ', ' + new Date(feedbackReport.ReportCompletedDateTime()).format('ddd dd MMM yy HH:mm')
    } else {
      return '<i class="fa fa-minus"></i> Not Completed '
    }
  },
  isPublishedHTML: function (feedbackReport) {
    if (feedbackReport.IsPublished()) {
      return '<i class="fa fa-check-square-o"></i> Published by ' + feedbackReport.ReportPublishedByName() + ', ' + new Date(feedbackReport.ReportPublishedDateTime()).format('ddd dd MMM yy HH:mm')
    } else {
      return '<i class="fa fa-minus"></i> Not Published '
    }
  },

  FilterAdHocSettings: function (FullList, ReportSetting) {
    var RSList = [];
    FullList.Iterate(function (Item, index) {
      if (Item.FeedbackReportAdHocTypeID != null) {
        RSList.push(Item)
      }
    });
    return RSList;
  },
  GetStudioFeedbackSectionAreaList: function (rowNum) {
    return ViewModel.CurrentReport().StudioFeedbackSectionAreaList().Filter("RowNum", rowNum);
  },

  get: function (options) {
    Singular.GetDataStatelessPromise('OBLib.FeedbackReports.FeedbackReportList', options.criteria)
      .then(data => {
        if (options.onSuccess) { options.onSuccess(data) }
      }),
      errorText => {
        OBMisc.Notifications.GritterError("Fetch Failed", errorText, 1000)
      }
    //{
    //  FeedbackReportID: ViewModel.CurrentSubReportID(),
    //    ProductionID: productionID,
    //      ProductionSystemAreaID: productionSystemAreaID
    //},
    //function (result) {

    //  if (result.Success && result.Data.length == 1) {

    //    //Set Current Report 
    //    ViewModel.CurrentSubReport.Set(result.Data[0]);
    //    if (defaultSection)
    //      ViewModel.CurrentSubReportSection.Set(ViewModel.CurrentSubReport().FeedbackReportSectionList().Find('ReportSection', defaultSection));
    //    else
    //      ViewModel.CurrentSubReportSection.Set(null);

    //  } else {

    //  }
    //}
  },
  save: function (feedbackReport, options) {
    feedbackReport.IsProcessing(true)
    ViewModel.CallServerMethodPromise("SaveFeedbackReport", { FeedbackReport: feedbackReport.Serialise() })
      .then(data => {
        KOFormatterFull.Deserialise(data, feedbackReport)
        feedbackReport.IsProcessing(false)
        if (options.onSuccess) { options.onSuccess(data) }
      }),
      errorText => {
        feedbackReport.IsProcessing(false)
        OBMisc.Notifications.GritterError("Save Failed", errorText, 1000)
        if (options.onFail) { options.onFail(errorText) }
      }
  },
  modalTitle: function (report) {
    if (!report || !report.ReportDate()) {
      return "Unknown Date"
    }
    return new Date(report.ReportDate()).format('ddd dd MMM yyyy')
  }
};

StudioFeedbackReportBO = {
  IsCompletedSet: function (self) {
    if (self.IsCompleted()) {
      self.ReportCompletedBy(ViewModel.CurrentUserID())
      self.ReportCompletedByName(ViewModel.CurrentUserName())
      self.ReportCompletedDateTime(new Date())
    } else {
      self.ReportCompletedBy(null)
      self.ReportCompletedByName("")
      self.ReportCompletedDateTime(null)
    }
  },
  IsPublishedSet: function (self) {
    if (self.IsPublished()) {
      self.ReportPublishedBy(ViewModel.CurrentUserID())
      self.ReportCompletedByName(ViewModel.CurrentUserName())
      self.ReportPublishedDateTime(new Date())
    } else {
      self.ReportPublishedBy(null)
      self.ReportCompletedByName("")
      self.ReportPublishedDateTime(null)
    }
  },

  ReconciledCountValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.ReportCount() != CtlError.Object.ReconciledCount()) {
      CtlError.AddError("All bookings need to be reconciled")
    }
  },

  canEdit: function (fieldName, report) {
    switch (fieldName) {
      case "ReportDate":
        return report.IsNew();
        break;
      case "SystemID":
        return false;
        break;
      case "ProductionAreaID":
        return false;
        break;
      case "FeedbackReportSettingID":
        return false;
        break;
      case "IsCompleted":
        return (!report.IsNew() && !report.IsPublished() && (report.ReportCount() == report.ReconciledCount()));
        break;
      case "IsPublished":
        return (!report.IsNew() && report.IsCompleted());
        break;
      case "PrintButton":
        return !report.IsDirty();
        break;
      case "SaveButton":
        return (report.IsDirty() && report.IsValid());
        break;
      default:
        return true;
        break;
    }
  },
  canView: function (fieldName, inst) {
  },
  updateReconciledCount: function (report) {
    let reconCount = 0;
    report.StudioFeedbackSectionAreaList().forEach(area => {
      area.StudioFeedbackSectionList().forEach(section => {
        if (section.IsReconciled()) {
          reconCount += 1
        }
      })
    })
    report.ReconciledCount(reconCount)
  },

  isCompletedHTML: function (feedbackReport) {
    if (feedbackReport.IsCompleted()) {
      return '<i class="fa fa-check-square-o"></i> Completed ' + feedbackReport.ReportCompletedByName() + ', ' + new Date(feedbackReport.ReportCompletedDateTime()).format('ddd dd MMM yy HH:mm')
    } else {
      return '<i class="fa fa-minus"></i> Not Completed '
    }
  },
  isPublishedHTML: function (feedbackReport) {
    if (feedbackReport.IsPublished()) {
      return '<i class="fa fa-check-square-o"></i> Published ' + feedbackReport.ReportPublishedByName() + ', ' + new Date(feedbackReport.ReportPublishedDateTime()).format('ddd dd MMM yy HH:mm')
    }
    else {
      return '<i class="fa fa-minus"></i> Not Published '
    }
  },

  save: function (feedbackReport, options) {
    feedbackReport.IsProcessing(true)
    ViewModel.CallServerMethodPromise("SaveStudioFeedbackReport", { StudioFeedbackReport: feedbackReport.Serialise() })
      .then(data => {
        KOFormatter.Deserialise(data, feedbackReport)
        feedbackReport.IsProcessing(false)
        if (options.onSuccess) { options.onSuccess(data) }
      }),
      errorText => {
        feedbackReport.IsProcessing(false)
        OBMisc.Notifications.GritterError("Save Failed", errorText, 1000)
        if (options.onFail) { options.onFail(errorText) }
      }
  }
};

StudioFeedbackSectionBO = {
  SetSectionCompletedBy: function (section) {
    if (section.IsCompleted()) {
      section.ReportCompletedBy(ViewModel.CurrentUserID())
      section.ReportCompletedDateTime(new Date())
      Section.ReportCompletedByName(ViewModel.CurrentUserName())
    }
    else {
      section.ReportCompletedBy(null)
      section.ReportCompletedDateTime(null)
      section.ReportCompletedByName("")
    }
  },
  IsReconciledSet: function (self) {
    //upate the parent
    let parentReport = self.GetParent().GetParent()
    StudioFeedbackReportBO.updateReconciledCount(parentReport)
  },

  editRoomScheduleArea: function (section) {
    RoomScheduleAreaBO.afterSave = function (data) {
      //update reconciled state of section based on the roomschedule booking
      section.IsReconciled(data.IsReconciled)
    }
    RoomScheduleAreaBO.getRoomScheduleArea({
      resourceBookingID: section.ResourceBookingID(),
      criteria: {
        roomScheduleID: section.RoomScheduleID(),
        productionSystemAreaID: section.ProductionSystemAreaID(),
        resourceBookingID: section.ResourceBookingID()
      },
      onSuccess: function (response) {
        ViewModel.CurrentProduction(null)
        ViewModel.CurrentAdHocBooking(null)
        ViewModel.CurrentRoomScheduleArea.Set(response[0])
        RoomScheduleAreaBO.showModal(response[0])
      },
      onFail: function (response) {
      }
    })
  },
  editReport: function (childReport, selectedSection) {
    FeedbackReportBO.get({
      criteria: {
        FeedbackReportID: childReport.FeedbackReportID(),
        ProductionID: childReport.ProductionID(),
        ProductionSystemAreaID: childReport.ProductionSystemAreaID()
      },
      onSuccess: function (data) {
        ViewModel.CurrentSectionAreaGuid(childReport.GetParent().Guid())
        ViewModel.CurrentSubReport.Set(data[0])
        StudioFeedbackSectionBO.showSubReportModal()
        $('[data-toggle="popover"]').popover()
      }
    })
  },
  getSectionCss: function (StudioFeedbackSection, sectionName) {
    let baseCss = "roomBookingIcon";
    if (StudioFeedbackSection.IsCompleted()) {
      return baseCss + " isCompleted";
    } else if (StudioFeedbackSection.HasFeedback()) {
      return baseCss + " hasFeedback";
    }
    else {
      return baseCss;
    }
  },
  getBookingStatusCss: function (StudioFeedbackSection, sectionName) {
    let baseCss = "fa fa-lg cursor-hover padding-left-10 padding-right-10";
    if (StudioFeedbackSection.IsReconciled()) {
      return baseCss + " isReconciled";
    } else {
      return baseCss + " notReconciled";
    }
    return baseCss;
  },
  canEdit: function (fieldName, report) {
    let parentReport = report.GetParent().GetParent()
    switch (fieldName) {
      default:
        return true;
        break;
    }
  },

  showSubReportModal: function () {
    $("#SubReport").off('shown.bs.modal')
    $("#SubReport").off('hidden.bs.modal')
    $("#SubReport").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentSubReport())
    })
    $("#SubReport").on('hidden.bs.modal', function () {
      const feedbackReportID = ViewModel.CurrentSubReport().FeedbackReportID()
      let studioFeedbackSection = ViewModel.CurrentReport().StudioFeedbackSectionAreaList().Find("Guid", ViewModel.CurrentSectionAreaGuid()).StudioFeedbackSectionList().Find("FeedbackReportID", feedbackReportID)
      if (studioFeedbackSection) {
        studioFeedbackSection.IsCompleted(ViewModel.CurrentSubReport().IsCompleted())
        studioFeedbackSection.HasFeedback(ViewModel.CurrentSubReport().HasFeedback())
      }
      ViewModel.CurrentSubReport(null)
      ViewModel.CurrentSectionAreaGuid("")
    })
    $("#SubReport").modal()
  },
  hideSubReportModal: function () {
    $("#SubReport").modal('hide')
  },

  sectionVisible: function (section) {
    const allowedSections = ["Crew", "Technical", "General"]
    return (allowedSections.indexOf(section.ReportSection()) >= 0)
  }
};

ReportSectionBO = {
  accordionAttributes: function (section) {
    return { 'data-toggle': 'collapse', 'data-parent': '#report-sections', href: "#collapseSection" + section.FeedbackReportSectionID().toString(), 'aria-expanded': false };
  },
  accordionBodyContainerAttributes: function (section) {
    return { style: 'height: 0px;', id: "collapseSection" + section.FeedbackReportSectionID().toString(), 'aria-expanded': false };
  }
}

FeedbackReportSubSectionBO = {
  addNewButtonCss: function (SubSection) {
    if (!SubSection.UserCanAdd()) {
      return "hideButton"
    }
    return ""
  },
  canView: function (fieldName, self) {
    let subReport = self.GetParent().GetParent()
    switch (fieldName) {
      case "AddNewButton":
        if (!subReport.IsCompleted()) {
          return self.UserCanAdd()
        }
        else {
          return false
        }
        break;
      default:
        return true;
        break;
    }
  },
  canViewDeleteButton: function (self) {
    let subReport = self.GetParent().GetParent().GetParent()
    if (!subReport.IsCompleted()) {
      if (self.GetParent().ReportSubSection() == "Crew Feedback/Performance") {
        return false
      } else {
        return self.GetParent().UserCanAdd()
      }
    }
    else {
      return false
    }
  },

  isParentCompleted: function (subSection) {
    return subSection.GetParent().GetParent().IsCompleted()
  },
  CanAddDetails: function (Subsection) {
    if (!FeedbackReportSubSectionBO.isParentCompleted(Subsection)) {
      if (Subsection.UserCanAdd()) {
        return true
      }
    }
    return false
  },
  columnVisible: function (columnName, self) {
    var parentSection = self;

    switch (columnName) {
      case "Column1":
        if (self.Column1Header() && self.Column1Header().length > 0) {
          return true;
        } else {
          return false
        }
        break;
      case "Column2":
        if (self.Column2Header() && self.Column2Header().length > 0) {
          return true;
        } else {
          return false
        }
        break;
      case "Column3":
        if (self.Column3Header() && self.Column3Header().length > 0) {
          return true;
        } else {
          return false
        }
        break;
      case "TextAnswer":
        if (self.isTypeFreeText() && !self.AutoPopulated()) {
          return true;
        } else {
          return false
        }
        break;
      case "DropDown1":
        if (self.RequiresDropDown()) {
          return true;
        } else {
          return false
        }
        break;
      case "DropDown2":
        if (self.RequiresDropDown2()) {
          return true;
        } else {
          return false
        }
        break;
      case "Comments":
        if (self.CommentsAllowed()) {
          return true;
        } else {
          return false
        }
        break;
      default:
        return true
        break;
    }
  },
  getFlagDetailsHTML: function (SubSectionORDetails, IsDetails) {
    var GreenFlag = 0;
    var OrangeFlag = 0;
    var RedFlag = 0;
    var subSection = SubSectionORDetails;

    if (IsDetails == 1) {
      subSection = SubSectionORDetails.GetParent();
    }
    var Total = subSection.FeedbackReportDetailList().length;
    subSection.FeedbackReportDetailList().Iterate(function (Item, index) {
      var FlagTypeID = Item.RaisedFeedbackReportFlagTypeID();
      if (FlagTypeID == 1) {
        GreenFlag = GreenFlag + 1;
      }
      else if (FlagTypeID == 2) {
        OrangeFlag = OrangeFlag + 1;
      }
      else if (FlagTypeID == 3) {
        RedFlag = RedFlag + 1;
      }
    });
    return "<p><span style='color: green;'>" + GreenFlag + " Green Flag</span>, <span style='color: Orange;'>" + OrangeFlag + " Orange flag</span>, <span style='color: Red;'>" + RedFlag + " Red flag</span> out of " + Total + "</p>"
  }
};

FeedbackReportDetailBO = {
  canEdit: function (fieldName, self) {
    if (FeedbackReportSubSectionBO.isParentCompleted(self.GetParent())) {
      return false;
    }
    else if (self.SystemInd() && !self.GetParent().UserCanEdit()) {
      return false;
    }
    else {
      switch (fieldName) {
        case "Column1":
          if (self.GetParent().Column1Header() == "Discipline") { return false } else { return true }
          break;
        case "Column2":
          if (self.GetParent().Column2Header() == "Name") { return false } else { return true }
          return true;
          break;
        case "Column3":
          return true;
          break;
        case "TextAnswer":
          return true;
          break;
        case "Comments":
          return true;
          break;
      }
    }
    return true;
  },

  FilterDropDown1: function (FullList, ReportDetail) {
    var ddList = [];
    FullList.Iterate(function (Item, index) {
      if (Item.FeedbackReportDropDownID == ReportDetail.GetParent().FeedbackReportDropDownID()) {
        ddList.push(Item)
      }
    })
    return ddList;
  },
  FilterDropDown2: function (FullList, ReportDetail) {
    var IList = [];
    var dditem = FullList.Find("FeedbackReportDropDownItemID", ReportDetail.Answer1FeedbackReportDropDownItemID())
    if (dditem) {
      ClientData.ROFeedbackReportDropDownItemList.Iterate(function (Item, index) {
        if (Item.FeedbackReportDropDownID == dditem.DropDown2FeedbackReportDropDownID) {
          IList.push(Item)
        }
      });
    }
    return IList
  },
  SetAnswer1DropDownFlag: function (FRDetail) {
    var cr = FRDetail.GetParent().SubSectionSettings().ROFeedbackReportFlagCriteriaList().Find("FlagRaisedIfAnswer1IsFeedbackReportDropDownItemID", FRDetail.Answer1FeedbackReportDropDownItemID())
    if (cr) {
      if (cr.FlagRaisedIfAnswer2IsFeedbackReportDropDownItemID() == null) {
        FRDetail.RaisedFeedbackReportFlagTypeID(cr.FeedbackReportFlagTypeID())
      }
    }
    else { FRDetail.RaisedFeedbackReportFlagTypeID(null); }
  },
  SetAnswer2DropDownFlag: function (FRDetail) {
    var crdda = FRDetail.GetParent().SubSectionSettings().ROFeedbackReportFlagCriteriaList().Find("FlagRaisedIfAnswer2IsFeedbackReportDropDownItemID", FRDetail.Answer2FeedbackReportDropDownItemID())
    if (crdda) {
      if (!crdda.FlagRaisedWhenAnyAnswerGiven()) {
        if (crdda.FlagRaisedIfAnswer2IsFeedbackReportDropDownItemID() == FRDetail.Answer2FeedbackReportDropDownItemID()) {
          FRDetail.RaisedFeedbackReportFlagTypeID(crdda.FeedbackReportFlagTypeID())
        }
        else { FRDetail.RaisedFeedbackReportFlagTypeID(null) };
      }
      else { FRDetail.RaisedFeedbackReportFlagTypeID(crdda.FeedbackReportFlagTypeID()) };
    }
  },
  SetTextAnswerFlag: function (FRDetail) {
    FRDetail.GetParent().SubSectionSettings().ROFeedbackReportFlagCriteriaList().Iterate(function (Item, index) {
      var FlagTypeID = Item.FeedbackReportFlagTypeID();
      if (!Item.FlagRaisedWhenAnyAnswerGiven()) {
        if (Item.FlagRaisedIfAnswerContainsText() != "") {
          var ContainsText = Item.FlagRaisedIfAnswerContainsText();
          if (FRDetail.TextAnswer().indexOf(ContainsText) >= 0) {
            FRDetail.RaisedFeedbackReportFlagTypeID(FlagTypeID)
          }
          else { FRDetail.RaisedFeedbackReportFlagTypeID(2) }
        }
        else { };
      }
      else { FRDetail.RaisedFeedbackReportFlagTypeID(FlagTypeID); };
    });
  },
  SetCommentFlag: function (FRDetail) {
    FRDetail.GetParent().SubSectionSettings().ROFeedbackReportFlagCriteriaList().Iterate(function (Item, index) {
      var FlagTypeID = Item.FeedbackReportFlagTypeID();
      if (!Item.FlagRaisedWhenAnyAnswerGiven()) {
        if (Item.FlagRaisedIfAnswerContainsText() != "") {
          var ContainsText = Item.FlagRaisedIfAnswerContainsText();
          if (FRDetail.Comments().indexOf(ContainsText) >= 0) {
            FRDetail.RaisedFeedbackReportFlagTypeID(FlagTypeID)
          }
          else { FRDetail.RaisedFeedbackReportFlagTypeID(2) }
        }
        else { };
      }
      else { FRDetail.RaisedFeedbackReportFlagTypeID(FlagTypeID); };
    });
  },
  SetMarkedConfidentialBy: function (FRDetail) {
    if (FRDetail.Confidential() == true) {
      FRDetail.MarkedConfidentialBy(ViewModel.CurrentUserID())
    }
    else {
      FRDetail.MarkedConfidentialBy(null)
    };
  },
  SetMarkedConfidentialByML: function (FRDetailML) {
    if (FRDetailML.Confidential() == true) {
      FRDetailML.MarkedConfidentialBy(ViewModel.CurrentUserID())
    }
    else {
      FRDetailML.MarkedConfidentialBy(null)
    };
  },
  flagColor: function (FRDetail) {
    var GreenFlag = 1;
    var OrangeFlag = 2;
    var RedFlag = 3;
    if (FRDetail.RaisedFeedbackReportFlagTypeID() != 0) {
      if (FRDetail.RaisedFeedbackReportFlagTypeID() == GreenFlag) {
        return "GreenFlag";
      }
      else if (FRDetail.RaisedFeedbackReportFlagTypeID() == OrangeFlag) {
        return "OrangeFlag";
      }
      else if (FRDetail.RaisedFeedbackReportFlagTypeID() == RedFlag) {
        return "RedFlag";
      }
      else {
        return "noflags";
      };
    }
    else {
      return "noflags";
    };
  }
};

FeedbackReportActionBO = {
  canEdit: function (self) {
    if (FeedbackReportSubSectionBO.isParentCompleted(self.GetParent().GetParent())) {
      return false;
    }
    return true;
  },
  SetActionTakenDate: function (FRDetailA) {
    var date = new Date();
    var options = {
      weekday: "long", year: "numeric", month: "short",
      day: "numeric", hour: "2-digit", minute: "2-digit"
    };
    if (FRDetailA.ActionComments().length > 0) {
      FRDetailA.ActionBy(ViewModel.CurrentUserID());
    } else {
      FRDetailA.ActionTakenDate(null)
      FRDetailA.ActionBy(null)
    };
  },
  SetDelegatedBy: function (FRDetailA) {
    var date = new Date();
    //Get Tomorrow's Date...24 hours 60 minutes 60 seconds 1000 milliseconds
    var nextDate = new Date();
    if (FRDetailA.DelegateAction()) {
      FRDetailA.ActionByDate(nextDate);
      FRDetailA.DelegatedBy(ViewModel.CurrentUserID())
      FRDetailA.ActionBy(null)
      FRDetailA.ActionTakenDate(null)
    }
    else if (!FRDetailA.DelegateAction()) {
      FRDetailA.ActionByDate(date);
      FRDetailA.ActionTakenDate(date)
      FRDetailA.ActionBy(ViewModel.CurrentUserID())
      FRDetailA.DelegatedBy(null)
      FRDetailA.ActionBy(ViewModel.CurrentUserID())
    };
  },
  SetEditable: function (FRDetailA) {
    if (FRDetailA.DelegateAction() == true && FRDetailA.ActionBy() == ViewModel.CurrentUserID()) {
      return true;
    }
    else {
      return false;
    };
  },
  SetEditableActionTakenDate: function (FRDetailA) {
    if (FRDetailA.DelegateAction() && FRDetailA.ActionBy() == ViewModel.CurrentUserID() && !FeedbackReportSubSectionBO.isParentCompleted(FRDetailA.GetParent().GetParent())) {
      return true;
    }
    else if (FRDetailA.DelegateAction() && FRDetailA.ActionBy() != ViewModel.CurrentUserID()) {
      return false;
    }
    else if (!FRDetailA.DelegateAction() && !FeedbackReportSubSectionBO.isParentCompleted(FRDetailA.GetParent().GetParent())) {
      return true;
    }
  },
  SetEditableBasedOnDelegateAction: function (FRDetailA) {
    if (FRDetailA.DelegateAction() && FRDetailA.ActionBy() == ViewModel.CurrentUserID()) {
      return false;
    }
    else if (FRDetailA.DelegateAction() && FRDetailA.ActionBy() != ViewModel.CurrentUserID() && FeedbackReportActionBO.canEdit(FRDetailA)) {
      return true;
    };
  },
  SetDate: function (FRDetailA) {
    var date = new Date();
    if (FRDetailA.DelegateAction() == false) {
      FRDetailA.ActionByDate() = date;
    }
    else {
      FRDetailA.ActionByDate() = date + 1;
    };
  },
  SetEditableActionComment: function (FRDetailA) {

    if (FRDetailA.DelegateAction() && FRDetailA.ActionBy() == ViewModel.CurrentUserID()) {
      if (!FeedbackReportSubSectionBO.isParentCompleted(FRDetailA.GetParent().GetParent())) {
        return true;
      }
    }

    if (!FRDetailA.DelegateAction() && !FeedbackReportSubSectionBO.isParentCompleted(FRDetailA.GetParent().GetParent())) {
      return true;
    }
    else { return false; }
  }
};




