﻿EquipmentBO = {

  ChildEquipmentValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  },
  CountryIDValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.InternationalInd() && !eq.CountryID()) {
    //  CtlError.AddError("Country is required");
    //}
    //if (eq.InternationalInd() && eq.CountryID() && eq.CountryID() == 1) {
    //  CtlError.AddError("Country cannot be South Africa for international equipment");
    //}
  },

  //cans
  canEdit: function (Equipment, FieldName) {
    switch (FieldName) {
      case 'SaveButton':
        return (Equipment.IsValid() && Equipment.IsDirty() && !Equipment.IsProcessing())
        break;
      default:
        return true
        break;
    }
  },

  get: function (options) {
    Singular.GetDataStateless("OBLib.Equipment.EquipmentList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Equipment Failed", response.ErrorText, 1000)
        }
      })
  },
  save: function (equipment, options) {
    ViewModel.CallServerMethod("SaveEquipment",
      { Equipment: equipment.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Equipment Failed", response.ErrorText, 1000)
        }
      })
  }
}

EquipmentScheduleBaseBO = {

  EquipmentIDSet: function (self) {

  },

  StartDateTimeBufferSet: function (self) {
    //var booking = EquipmentScheduleBaseBO.ResourceBooking(self)
    //if (booking) {
    //  booking.StartDateTimeBuffer(self.StartDateTimeBuffer())
    //}
  },
  StartDateTimeSet: function (self) {
    //var booking = EquipmentScheduleBaseBO.ResourceBooking(self)
    //if (booking) {
    //  booking.StartDateTime(self.StartDateTime())
    //  EquipmentScheduleBaseBO.getClashes(self)
    //}
  },
  EndDateTimeSet: function (self) {
    //var booking = EquipmentScheduleBaseBO.ResourceBooking(self)
    //if (booking) {
    //  booking.EndDateTime(self.EndDateTime())
    //  EquipmentScheduleBaseBO.getClashes(self)
    //}
  },
  EndDateTimeBufferSet: function (self) {
    //var booking = EquipmentScheduleBaseBO.ResourceBooking(self)
    //if (booking) {
    //  booking.EndDateTimeBuffer(self.EndDateTimeBuffer())
    //}
  },
  EquipmentScheduleTypeIDSet: function (self) {
    //if (self.IsNew()) {
    //  if (self.EquipmentScheduleTypeID() == 1) {
    //    if (self.ObjType() == 'OBLib.SatOps.EquipmentSchedule, OBLib') {
    //      var nf = self.FeedList.AddNew();
    //      var np = nf.ProductionDetailBaseList.AddNew();
    //    }
    //    Singular.Validation.CheckRules(self);
    //  } else if (self.EquipmentScheduleTypeID() == 2) {
    //    if (self.FeedList().length > 0) {
    //      self.FeedList.Remove(self.FeedList()[0]);
    //    }
    //  }
    //}
  },

  ProductionAreaStatusIDSet: function (self) {
    //var booking = EquipmentScheduleBaseBO.ResourceBooking(self)
    //if (booking) {

    //  var filteredStatusBySystem = ClientData.ROSystemProductionAreaStatusSelectList.Filter("SystemID", 2)
    //  var filteredStatusByArea = filteredStatusBySystem.Filter("ProductionAreaID", 8) //Content
    //  var status = filteredStatusByArea.Filter("ProductionAreaStatusID", self.ProductionAreaStatusID()) //SatOps
    //  var stat = status[0]
    //  if (stat.length == 1) {
    //    stat = stat[0]
    //    booking.StatusCssClass(stat.CssClass)
    //  }
    //  if (stat.ProductionAreaStatusID == 4) {
    //    booking.IsCancelled(false)
    //    booking.IsLocked(true)
    //    booking.IsLockedReason("Reconciled")
    //  } else if (stat.ProductionAreaStatusID == 5) {
    //    booking.IsCancelled(true)
    //    booking.IsLocked(false)
    //    booking.IsLockedReason("")
    //  }
    //}
  },

  BufferStartTimeValid: function (Value, Rule, CtlError) {
    var ct = CtlError.Object.StartDateTimeBuffer();
    var st = CtlError.Object.StartDateTime();
    if (!CtlError.Object.StartDateTimeBuffer()) {
      CtlError.AddError("Buffer Start Time is required");
    } else {
      if (ct && st) {
        if (OBMisc.Dates.IsAfter(ct, st)) {
          CtlError.AddError("Buffer Start Time must be before Start Time");
        }
      }
    }
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.StartDateTime();
    var oaet = CtlError.Object.EndDateTime();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("Start Time must be before End Time");
      } else if (!CtlError.Object.StartDateTime()) {
        CtlError.AddError("Start Time is required");
      }
    }
  },
  EndDateTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.StartDateTime();
    var oaet = CtlError.Object.EndDateTime();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("Start Time must be before End Time");
      } else if (!CtlError.Object.EndDateTime()) {
        CtlError.AddError("End Time is required");
      }
    }
  },
  BufferEndTimeValid: function (Value, Rule, CtlError) {
    var oaet = CtlError.Object.EndDateTime();
    var wrt = CtlError.Object.EndDateTimeBuffer();
    if (!CtlError.Object.EndDateTimeBuffer()) {
      CtlError.AddError("Buffer End Time is required");
    } else {
      if (oaet && wrt) {
        if (OBMisc.Dates.IsAfter(oaet, wrt)) {
          CtlError.AddError("End Time must be before Buffer End Time");
        }
      }
    }
  },


  EquipmentScheduleToString: function (EquipmentSchedule) {
    if (EquipmentSchedule.EquipmentScheduleTitle().trim().length == 0) {
      return "Blank Equipment Booking"
    }
    else {
      return EquipmentSchedule.EquipmentScheduleTitle()
    }
  },
  getClashes: function (EquipmentSchedule) {
    if (!EquipmentSchedule.IsProcessing()) {
      //EquipmentSchedule.IsProcessing(true)
      //ViewModel.ResourceBookingsCleanList([])
      //var booking = EquipmentScheduleBaseBO.ResourceBooking(EquipmentSchedule)
      //var _allBookings = []
      //_allBookings.push(booking)
      //ViewModel.ResourceBookingsCleanList(_allBookings)
      //var data = KOFormatter.Serialise(ViewModel.ResourceBookingsCleanList())
      //ViewModel.CallServerMethod("GetResourceBookingClashes", {
      //  ResourceBookings: data
      //},
      //function (response) {
      //  if (response.Success) {
      //    //EquipmentSchedule.UpdateClashes(RoomSchedule, response.Data)
      //  }
      //  else {
      //  }
      //  EquipmentSchedule.IsProcessing(false)
      //})
    }
  },
  FilterStatusList: function (List, self) {
    var results = []
    List.Iterate(function (itm, indx) {
      if (itm.SystemID == 2 && itm.ProductionAreaID == 8) {
        results.push(itm)
      }
    })
    return results
  }
}

EquipmentScheduleBO = {
  EquipmentIDValid: function (Value, Rule, CtlError) {
    var eq = CtlError.Object;
    if (!eq.EquipmentID()) {
      CtlError.AddError("Equipment is required");
    }
  },
  EquipmentScheduleIDValid: function (Value, Rule, CtlError) {

  },
  HasClashesValid: function (Value, Rule, CtlError) {
    var eq = CtlError.Object;
    if (eq.Clashes().trim().length > 0) {
      CtlError.AddError(eq.Clashes());
    }
  },
  ChangeDescriptionValid: function (Value, Rule, CtlError) {
    var eq = CtlError.Object;
    if (eq.ChangeDescriptionRequired() && (eq.ChangeDescription().trim().length == 0)) {
      CtlError.AddError("Change Reason is required")
    }
  },

  CanEdit: function (self, FieldName) {
    switch (FieldName) {
      case "QuickAddEquipmentSchedulesButton":
        if (self.EquipmentScheduleID() && !self.IsNew()) {
          return true;
        } else {
          return false;
        }
        break;
      case "RoomScheduleTypeID":
        return self.IsNew();
        break;
      case "EquipmentID", "EquipmentName":
        return self.IsNew();
        break;
      case "ProductionAreaStatusID", "ProductionAreaStatusName":
        return true;
        break;
      case "DebtorID", "DebtorName":
        return true;
        break;
    }
    return true;
  },
  GetEquipmentScheduleErrors: function (self) {
    //console.log(self);
    //var br = GetBrokenRules(self);
    //console.log(br);
    //return "Test";
  },
  Save: function () {
    Singular.SendCommand("SaveEquipmentSchedule", {}, function (response) {

    });
  },
  SelectTurnAroundPoints: function () {
    ROTurnAroundPoints.ShowModal();
  },
  AddSelectedTurnAroundPoints: function () {
    ViewModel.ROTurnAroundPointPagedListManager().SelectedItems().Iterate(function (TP, TPInd) {
      var NewItem = ViewModel.CurrentEquipmentSchedule().FeedList()[0].FeedTurnAroundPointList.AddNew();
      NewItem.TurnAroundPointID(TP.ID());
      NewItem.TurnAroundPointDisplay(TP.Description());
    });
    ROTurnAroundPoints.HideModal();
  },
  SelectFeedPaths: function () {
    ROPagedRooms.ShowModal();
  },
  AddSelectedFeedPaths: function () {
    ViewModel.RORoomListPagedManager().SelectedItems().Iterate(function (RM, RMInd) {
      var NewItem = ViewModel.CurrentEquipmentSchedule().FeedList()[0].FeedPathList.AddNew();
      NewItem.RoomID(RM.ID());
      NewItem.Room(RM.Description());
    });
    ROPagedRooms.HideModal();
  },
  SelectDebtor: function () {
    ViewModel.RODebtorListManager().SetOptions({
      AfterRowSelected: function (RODebtor) {
        ViewModel.CurrentEquipmentSchedule().DebtorName(RODebtor.Debtor());
        ViewModel.CurrentEquipmentSchedule().DebtorID(RODebtor.DebtorID());
        ROPagedDebtors.HideModal();
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.CurrentEquipmentSchedule().DebtorName("");
        ViewModel.CurrentEquipmentSchedule().DebtorID(null);
        ROPagedDebtors.HideModal();
      }
    });
    ROPagedDebtors.ShowModal();
  },
  SelectStatus: function () {
    ViewModel.ROPSAStatusesListManager().MultiSelect(false);
    ViewModel.ROPSAStatusesListManager().SingleSelect(true);
    ViewModel.ROPSAStatusesListManager().SetOptions({
      AfterRowSelected: function (ROStatus) {
        ViewModel.CurrentEquipmentSchedule().ProductionAreaStatusName(ROStatus.ProductionAreaStatus());
        ViewModel.CurrentEquipmentSchedule().ProductionAreaStatusID(ROStatus.ProductionAreaStatusID());
        ROPSAStatuses.HideModal();
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.CurrentEquipmentSchedule().ProductionAreaStatusName("");
        ViewModel.CurrentEquipmentSchedule().ProductionAreaStatusID(null);
        ROPSAStatuses.HideModal();
      }
    });
    ROPSAStatuses.ShowModal()
  },
  UpdateProductionAudioSettings: function () {
    Singular.SendCommand("UpdateProductionAudioSettings", {}, function (response) {

    });
  },
  triggerDebtorAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDebtorCriteriaBeforeRefresh: function (args) {

  },
  afterDebtorRefreshAjax: function (args) {

  },
  onDebtorSelected: function (selectedItem, businessObject) {
    businessObject.DebtorName(selectedItem.Debtor)
  },
  DebtorIDSet: function (self) {

  },

  //dropdowns
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.EquipmentID(selectedItem.EquipmentID)
      businessObject.ResourceID(selectedItem.ResourceID)
    }
    else {
      businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
    }
  },

  //other
  get: function (EquipmentScheduleID, ProductionSystemAreaID, onSuccess, onFail) {
    ViewModel.CallServerMethod("EditEquipmentSchedule",
      {
        EquipmentScheduleID: EquipmentScheduleID,
        ProductionSystemAreaID: ProductionSystemAreaID
      },
      function (response) {
        if (response.Success) {
          onSuccess(response)
        } else {
          OBMisc.Modals.Error('Error Fetching Feed', 'An error occured while fetching the Feed', response.ErrorText, 1500)
          onFail(response)
        }
      })
  },
  save: function (equipmentSchedule, onSuccess, onFail) {
    ViewModel.CallServerMethod("SaveEquipmentSchedule",
      {
        EquipmentSchedule: equipmentSchedule.Serialise()
      },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess('Booking Saved', equipmentSchedule.EquipmentScheduleTitle() + ' saved successfully', 500)
          onSuccess(response)
        }
        else {
          OBMisc.Notifications.GritterError('Error Saving', response.ErrorText, 1000)
          onFail(response)
        }
      })
  },
  addFeedGenRef: function (equipmentSchedule) {
    ViewModel.CallServerMethod("AddFeedGenRefs",
      { GenRefNumber: 0 },
      function (response) {

      })
  }
}

EquipmentScheduleFeedBO = {
  EquipmentIDValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  },
  EndDateTimeValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  }
}

EquipmentScheduleServiceBO = {
  EquipmentIDValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  },
  EndDateTimeValid: function (Value, Rule, CtlError) {
    //var eq = CtlError.Object;
    //if (eq.EquipmentTypeID()) {
    //  if (eq.EquipmentTypeID() == 19 && eq.ChildEquipmentList().length == 0) {
    //    CtlError.AddError("At least one child equipment must be selected");
    //  }
    //}
  }
}

FeedTurnAroundPointBO = {
  TurnAroundPointIDSet: function (self) { },
  TurnAroundPointOrderValid: function (Value, Rule, CtlError) {
    var ftap = CtlError.Object;
    if (ftap.TurnAroundPointOrder() == 0) {
      CtlError.AddError("Order cannot be 0")
      return
    } else if (ftap.GetParent()) {
      ftap.GetParent().FeedTurnAroundPointList().Iterate(function (ft, ftInd) {
        if (ft.Guid() != ftap.Guid()) {
          if (ft.TurnAroundPointOrder() == ftap.TurnAroundPointOrder()) {
            CtlError.AddError("Order must be unique")
            return
          }
        }
      });
    }
  },
  triggerTurnAroundPointAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setTurnAroundPointCriteriaBeforeRefresh: function (args) {
  },
  afterTurnAroundPointRefreshAjax: function (args) {
  },
  onTurnAroundPointSelected: function (selectedItem, businessObject) {
    businessObject.TurnAroundPoint(selectedItem.TurnAroundPoint)
  },
  FeedTurnAroundPointBOToString: function (self) {
    return (self.TurnAroundPoint().length > 0 ? self.TurnAroundPoint() : "TurnAround Point")
  },
  CanEdit: function (feedTurnAroundPoint, FieldName) {
    switch (FieldName) {
      default:
        return true
        break;
    }
  }
}

FeedTurnAroundPointContactBO = {
  TurnAroundPointContactIDSet: function (self) { },
  triggerTurnAroundPointContactAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setTurnAroundPointContactCriteriaBeforeRefresh: function (args) {
    args.Data.TurnAroundPointID = args.Object.GetParent().TurnAroundPointID()
  },
  afterTurnAroundPointContactRefreshAjax: function (args) {
  },
  onTurnAroundPointContactSelected: function (selectedItem, businessObject) {
    businessObject.ContactName(selectedItem.ContactName)
    businessObject.ContactNumber(selectedItem.ContactNumber)
  },
  FeedTurnAroundPointContactBOToString: function (self) {
    return (self.GetParent().TurnAroundPoint().length > 0 ? self.GetParent().TurnAroundPoint() : "TurnAround Point") + (self.ContactName().length > 0 ? " - " + self.ContactName() : "")
  },
  CanEdit: function (feedTurnAroundPointContact, FieldName) {
    switch (FieldName) {
      case "ContactName":
      case "ContactNumber":
        if (feedTurnAroundPointContact.TurnAroundPointContactID()) {
          return false
        } else {
          return true
        }
      default:
        return true
        break;
    }
  }
}

AudioPairBO = {
  AudioPairTypeIDSet: function (AudioPair) {
    if (AudioPair.AudioPairTypeID()) {
      Singular.SendCommand("AudioPairTypeChanged",
        {
          AudioPairID: AudioPair.AudioPairID()
        },
        function (response) {

        });
      //var ROAudioPairType = ClientData.ROAudioPairTypeList.Find("AudioPairTypeID", AudioPair.AudioPairTypeID());
      //var maxSettings = ROAudioPairType.MaxSettings;
      //AudioPair.AudioPairAudioSettingList.removeAll();
      //for (i = 1; i <= maxSettings; i++) {
      //  var ap = AudioPair.AudioPairAudioSettingList.AddNew();
      //  ap.ChannelNumber(i);
      //  ap.AudioSettingID(null);
      //}
    }
  }
}

FeedBO = {
  SetupAudioSettings: function () {
    Singular.SendCommand("SetupAudioSettings", {}, function (response) { });
  },
  IngestIndValid: function (Value, Rule, CtlError) {
    //var Feed = CtlError.Object;
    //var RORoom = null;
    //if (Feed.IngestInd()) {
    //  if (!Feed.IngestSourceTypeID()) {
    //    CtlError.AddError("Ingest Source Type is required");
    //  } else {
    //    switch (Feed.IngestSourceTypeID()) {
    //      case 1:
    //        if (Feed.IngestSourceDescription().trim().length == 0) {
    //          //CtlError.AddError("Ingest Source Description is required");
    //        }
    //        break;
    //      case 2:
    //        if (!Feed.IngestSourceRoomID()) {
    //          CtlError.AddError("Room to ingest is required");
    //        } else {
    //          RORoom = ClientData.RORoomList.Find("RoomID", Feed.IngestSourceRoomID())
    //          if (RORoom) {
    //            if (RORoom.RoomTypeID != 3) {
    //              CtlError.AddError("Room to ingest must be a gallery");
    //            }
    //          }
    //        }
    //        break;
    //      case 3:
    //        if (!Feed.IngestSourceRoomID()) {
    //          CtlError.AddError("Room to ingest is required");
    //        } else {
    //          RORoom = ClientData.RORoomList.Find("RoomID", Feed.IngestSourceRoomID())
    //          if (RORoom) {
    //            if (RORoom.RoomTypeID != 2) {
    //              CtlError.AddError("Room to ingest must be a studio");
    //            }
    //          }
    //        }
    //        break;
    //      case 4:
    //        if (Feed.IngestSourceDescription().trim().length == 0) {
    //          CtlError.AddError("Ingest Source Description is required");
    //        }
    //        break;
    //    }
    //  }
    //}
  },
  async importGenRefNumber(Feed) {
    if (Feed.AddSynergyGenRefNo().toString().length >= 6) {
      Feed.GetParent().IsProcessing(true);
      try {
        let result = await ViewModel.CallServerMethodPromise("AddFeedGenRef", { ByVenueAndDate: Feed.ImportAllEventsSameDayAndVenue(), GenRefNumber: Feed.AddSynergyGenRefNo() });
        result.FeedProductionList.forEach((fp) => {
          let existing = Feed.FeedProductionList().Find("GenRefNumber", fp.SynergyGenRefNo);
          if (!existing) {
            let newFP = Feed.FeedProductionList.AddNew();
            newFP.ProductionID(fp.ProductionID);
            newFP.SynergyGenRefNo(fp.SynergyGenRefNo);
            newFP.ProductionDescription(fp.ProductionDescription);
            newFP.IsSamrandFeed(fp.IsSamrandFeed);
          }
        });
        Feed.GetParent().IsProcessing(false);
      }
      catch (err) {
        OBMisc.Notifications.GritterError("Error Adding Events", err.message, 1000);
        Feed.GetParent().IsProcessing(false);
      };
    };
  },
  GetOBPersonnel: function (Feed) {
    Feed.IsProcessing(true)
    Feed.GetParent().IsProcessing(true)
    var xmlProductionIDs = "";
    var xmlDisciplineIDs = "";

    Feed.FeedProductionList().Iterate(function (fp, fpInd) {
      xmlProductionIDs += "<Table ID='" + fp.ProductionID().toString() + "' />"
    })
    xmlProductionIDs = '<DataSet>' + xmlProductionIDs + '</DataSet>'

    var xmlDisciplineIDs = "<DataSet>";
    xmlDisciplineIDs += "<Table ID='" + '6' + "' />"
    xmlDisciplineIDs += "<Table ID='" + '7' + "' />"
    xmlDisciplineIDs += "<Table ID='" + '11' + "' />"
    xmlDisciplineIDs += "</DataSet>"

    Singular.GetDataStateless("OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProductionList",
      {
        ProductionIDs: xmlProductionIDs,
        DisciplineIDs: xmlDisciplineIDs
      },
      function (response) {
        if (response.Success) {
          Feed.ROCrewDetailByProductionList.Set(response.Data)
        }
        else {
          OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the Crew', response.ErrorText, null)
        }
        Feed.IsProcessing(false)
        Feed.GetParent().IsProcessing(false)
      })

  },
  GetOBVehicles: function (Feed) {
    Feed.IsProcessing(true)
    Feed.GetParent().IsProcessing(true)
    var xmlProductionIDs = "";

    Feed.FeedProductionList().Iterate(function (fp, fpInd) {
      xmlProductionIDs += "<Table ID='" + fp.ProductionID().toString() + "' />"
    })
    xmlProductionIDs = '<DataSet>' + xmlProductionIDs + '</DataSet>'

    Singular.GetDataStateless("OBLib.Vehicles.ReadOnly.ROVehicleDetailByProductionList",
      {
        ProductionIDs: xmlProductionIDs
      },
      function (response) {
        if (response.Success) {
          Feed.ROVehicleDetailByProductionList.Set(response.Data)
        }
        else {
          OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the Vehicles', response.ErrorText, null)
        }
        Feed.IsProcessing(false)
        Feed.GetParent().IsProcessing(false)
      })

  }
}

FeedPathBO = {
  Edit: function () {

  }
}

FeedProductionBO = {
  SynergyGenRefNoSet: function (FeedProduction) {
    //if (FeedProduction.SynergyGenRefNo()) {
    //  Singular.SendCommand("FeedProductionGenRefSet", { FeedProductionGuid: FeedProduction.Guid().toString() }, function (response) { });
    //} else {
    //  Singular.SendCommand("FeedProductionGenRefCleared", { FeedProductionGuid: FeedProduction.Guid().toString() }, function (response) { });
    //}
  }
}

FeedDefaultAudioSettingBO = {
  GroupNumberValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    if (fdas.GroupNumber() <= 0) {
      CtlError.AddError("Group Number must be above 0");
    }
  },
  PairNumberValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    if (fdas.PairNumber() <= 0) {
      CtlError.AddError("Pair Number must be above 0");
    }
  },
  ChannelNumberValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    if (fdas.ChannelNumber() <= 0) {
      CtlError.AddError("Channel Number must be above 0");
    }
  },
  FeedAudioRowValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    var duplicateCount = 0;
    var Feed = fdas.GetParent();
    Feed.FeedDefaultAudioSettingList().Iterate(function (DefaultAudioSetting, indx) {
      if (fdas.Guid() != DefaultAudioSetting.Guid()) {
        if (DefaultAudioSetting.GroupNumber() == fdas.GroupNumber()
          && DefaultAudioSetting.PairNumber() == fdas.PairNumber()
          && DefaultAudioSetting.ChannelNumber() == fdas.ChannelNumber()) {
          duplicateCount += 1;
        }
      }
    });
    if (duplicateCount > 0) {
      CtlError.AddError("This comibination of group, pair and channel numbers is already being used");
    }
  }
}

FeedProductionAudioSettingBO = {
  GroupNumberValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    if (fdas.GroupNumber() <= 0) {
      CtlError.AddError("Group Number must be above 0");
    }
  },
  PairNumberValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    if (fdas.PairNumber() <= 0) {
      CtlError.AddError("Pair Number must be above 0");
    }
  },
  ChannelNumberValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    if (fdas.ChannelNumber() <= 0) {
      CtlError.AddError("Channel Number must be above 0");
    }
  },
  FeedAudioRowValid: function (Value, Rule, CtlError) {
    var fdas = CtlError.Object;
    var duplicateCount = 0;
    var prd = fdas.GetParent();
    prd.FeedProductionAudioSettingList().Iterate(function (DefaultAudioSetting, indx) {
      if (fdas.Guid() != DefaultAudioSetting.Guid()) {
        if (DefaultAudioSetting.GroupNumber() == fdas.GroupNumber()
          && DefaultAudioSetting.PairNumber() == fdas.PairNumber()
          && DefaultAudioSetting.ChannelNumber() == fdas.ChannelNumber()) {
          duplicateCount += 1;
        }
      }
    });
    if (duplicateCount > 0) {
      CtlError.AddError("This comibination of group, pair and channel numbers is already being used");
    }
  }
}

ROAudioSettingBO = {
  SelectDefaultAudio: function (Feed) {
    $("#FeedDefaultAudioSettingList").dialog({
      title: "Select Audio Settings",
      closeText: "Close",
      height: 600,
      width: 630,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
      },
      beforeClose: function (event, ui) {
      }
    });
  },
  DefaultAudioSettingRowClicked: function (ROAudioSetting) {
    var nas = ViewModel.CurrentEquipmentSchedule().Feed().FeedDefaultAudioSettingList().Find("AudioSettingID", ROAudioSetting.AudioSettingID());
    if (!nas) {
      nas = ViewModel.CurrentEquipmentSchedule().Feed().FeedDefaultAudioSettingList.AddNew();
      nas.AudioSettingID(ROAudioSetting.AudioSettingID());
    } else {
      //say already added?
    }
  },
  SelectFeedProductionAudio: function (FeedProduction) {
    window.CurrentFP = FeedProduction
    $("#FeedProductionAudioSettingList").dialog({
      title: "Select Audio Settings",
      closeText: "Close",
      height: 600,
      width: 630,
      modal: true,
      resizeable: true,
      open: function (event, ui) {

      },
      beforeClose: function (event, ui) {
        window.CurrentFP = null;
      }
    });
  },
  FeedProductionAudioSettingRowClicked: function (ROAudioSetting) {
    console.log(ROAudioSetting);
    if (window.CurrentFP) {
      var nas = window.CurrentFP.FeedProductionAudioSettingList().Find("AudioSettingID", ROAudioSetting.AudioSettingID());
      if (!nas) {
        nas = window.CurrentFP.FeedProductionAudioSettingList.AddNew();
        nas.AudioSettingID(ROAudioSetting.AudioSettingID());
      } else {
        //say already added?
      }
    }
    //var nas = ViewModel.CurrentEquipmentSchedule().Feed().FeedDefaultAudioSettingList().Find("AudioSettingID", ROAudioSetting.AudioSettingID());
    //if (!nas) {
    //  nas = ViewModel.CurrentEquipmentSchedule().Feed().FeedDefaultAudioSettingList.AddNew();
    //  nas.AudioSettingID(ROAudioSetting.AudioSettingID());
    //} else {
    //  //say already added?
    //}
  }
}

EquipmentScheduleBasicBO = {

  EquipmentIDSet: function (self) {
  },
  StartTimeSet: function (self) {
    EquipmentScheduleBasicBO.getClashes(self)
  },
  EndTimeSet: function (self) {
    EquipmentScheduleBasicBO.getClashes(self)
  },

  EquipmentIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
  },
  CallTimeValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object,
      ct = moment(CtlError.Object.CallTime()),
      st = moment(CtlError.Object.StartTime()),
      et = moment(CtlError.Object.EndTime()),
      wt = moment(CtlError.Object.WrapTime());
    if (!ct.isBefore(st)) { CtlError.AddError("Start Time Buffer must be before Start Time") }
    if (!st.isBefore(et)) { CtlError.AddError("Start Time must be before End Time") }
    if (!et.isBefore(wt)) { CtlError.AddError("End Time Buffer must be before End Time Buffer") }
  },
  ClashesValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.Clashes().trim().length > 0) {
      CtlError.AddError(obj.Clashes())
    }
  },

  //cans
  canEdit: function (fieldName, equipmentSchedule) {
    switch (fieldName) {
      case "EquipmentID":
      case "CallTime":
      case "StartDateTime":
      case "EndDateTime":
      case "WrapTime":
        return (!equipmentSchedule.IsProcessing())
        break;
      default:
        return true;
        break;
    }
  },
  canView: function (fieldName, equipmentSchedule) {
    switch (fieldName) {
      case "CreateFeedButton":
        return (!equipmentSchedule.IsProcessing() && equipmentSchedule.IsValid() && equipmentSchedule.ImportResult() === null)
        break;
      case "IsInvalidIcon":
        return (!equipmentSchedule.IsValid() || (equipmentSchedule.ImportResult() && !equipmentSchedule.ImportResult().Success()))
        break;
      case "ImportFailedIcon":
        return (equipmentSchedule.ImportResult() && !equipmentSchedule.ImportResult().Success())
        break;
      case "IsProcessingIcon":
        return equipmentSchedule.IsProcessing()
        break;
      default:
        return true;
        break;
    }
  },

  //dropdowns
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.StartTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.EquipmentID(selectedItem.EquipmentID)
      businessObject.ResourceID(selectedItem.ResourceID)
      EquipmentScheduleBasicBO.getClashes(businessObject)
    } else {
      businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
      businessObject.Clashes("")
    }
  },

  //other
  getClashes: function (self) {
    if (self.ResourceID() && self.StartTime() && self.EndTime() && !self.IsProcessing()) {
      self.IsProcessing(true)
      ResourceBookingBO.getClashes([{
        Guid: self.Guid(),
        ResourceBookingID: self.ResourceBookingID(),
        ResourceID: self.ResourceID(),
        ResourceBookingTypeID: 10,
        StartDateTime: self.StartTime(),
        EndDateTime: self.EndTime(),
        EquipmentScheduleID: self.EquipmentScheduleID()
      }],
        {
          onSuccess: function (data) {
            let clashes = ""
            data.forEach((clash, index) => {
              if (index > 0) { clashes += "\n"; }
              clashes += clash.ResourceBookingDescription + ': ' + clash.ClashTimes;
            })
            self.Clashes(clashes)
            self.IsProcessing(false)
          },
          onFail: function (errorText) {
            OBMisc.Notifications.GritterError("Clash Check Failed", errorText, 1000)
            self.IsProcessing(false)
          }
        })
    }
  },
  createFeed: function (obj, options = { onSuccess: (data) => { }, onFail: (errorText) => { } }) {
    EquipmentScheduleBasicBO.getCreateFeedPromise(obj)
      .then(function (data) {
        window.SiteHub.server.sendNotifications()
        window.siteHubManager.changeMade()
        OBMisc.Notifications.GritterSuccess("Allocated Successfully", "", 250)
        if (options.onSuccess) { options.onSuccess(data) }
        obj.IsProcessing(false)
      }),
      (errorText) => {
        OBMisc.Notifications.GritterSuccess("Allocation Failed", errorText, 1000)
        if (options.onFail) { options.onFail(errorText) }
      }
  },
  getCreateFeedPromise(equipmentScheduleBasic) {
    equipmentScheduleBasic.IsProcessing(true)
    return ViewModel.CallServerMethodPromise("SaveEquipmentScheduleBasic", { EquipmentScheduleBasic: equipmentScheduleBasic.Serialise() })
  },
  GetToString: function (self) {
    return self.GenreSeries() + ' - ' + self.Title()
  }
}

EquipmentAllocatorBO = {
  FilterByOwner: function (list, instance) {
    var results = []
    list.Iterate(function (itm, indx) {
      if (itm.OwningSystemID == ViewModel.EquipmentAllocatorCriteria().SystemID()
        && itm.OwningProductionAreaID == ViewModel.EquipmentAllocatorCriteria().ProductionAreaID()) {
        results.push(itm)
      }
    })
    return results
  },
  sendRequest: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("EquipmentAllocationRequest", {
      allocationRequest: ser
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        //success message
        $.gritter.add({
          title: 'Allocated Successfully',
          text: EquipmentAllocatorBO.GetToString(obj),
          class_name: 'success',
          time: 1000
        })
      }
      else {
        //error message
        $.gritter.add({
          title: 'Allocation Failed',
          text: '<p>' + EquipmentAllocatorBO.GetToString(obj) + '</p>' +
          '<p>' + response.ErrorText + '</p>',
          class_name: 'danger',
          time: 2000
        })
      }
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  editTimes: function (object, element) {

  },
  EquipmentIDSet: function (EquipmentAllocator) {
    if (EquipmentAllocator.EquipmentID()) {

    }
    else {

    }
  },
  EquipmentIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.EquipmentID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.OnAirStartDateTime())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.OnAirStartDateTime(), obj.OnAirEndDateTime())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.OnAirEndDateTime(), obj.WrapTime())
      obj.TimesValid(true)
      if (ctAfterStart) {
        CtlError.AddError('Call Time must be before On Air Start Time')
        obj.TimesValid(false)
      }
      if (onAirStartAfterEnd) {
        CtlError.AddError('On Air Start must be before On Air End Time')
        obj.TimesValid(false)
      }
      if (onAirEndAfterWrap) {
        CtlError.AddError('Wrap Time must be before On Air End Time')
        obj.TimesValid(false)
      }
      if (!obj.FeedTypeID()) {
        CtlError.AddError("Feed Type is required")
      }
      //if (obj.CircuitClashCount() > 0) {
      //  CtlError.AddError("Clashes Detected")
      //}
    }
  },
  GetToString: function (self) {
    return self.GenreSeries() + ' - ' + self.Title()
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  canUpload: function (obj) {
    if (!ViewModel.DisableUpload() && obj.IsValid()) {
      if (obj.EquipmentID() && obj.FeedTypeID() && obj.CallTime() && obj.WrapTime() && obj.IsValid() && !obj.IsProcessing()) {
        return true
      }
    }
    return false
  },
  bookingTimesButtonCSS: function (obj) {
    if (obj.TimesValid()) {
      return "btn btn-xs btn-default btn-block"
    } else {
      return "btn btn-xs btn-danger btn-block"
    }
  },
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.OnAirStartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.OnAirEndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    businessObject.EquipmentID(selectedItem.EquipmentID)
    businessObject.ResourceID(selectedItem.ResourceID)
  }
}

EquipmentAllocatorVenueBO = {
  FilterByOwner: function (list, instance) {
    var results = []
    list.Iterate(function (itm, indx) {
      if (itm.OwningSystemID == ViewModel.EquipmentAllocatorCriteria().SystemID()
        && itm.OwningProductionAreaID == ViewModel.EquipmentAllocatorCriteria().ProductionAreaID()) {
        results.push(itm)
      }
    })
    return results
  },
  sendRequest: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("EquipmentAllocationVenueRequest", {
      allocationRequest: ser
    },
      function (response) {
        if (response.Success) {
          window.SiteHub.server.sendNotifications()
          //success message
          $.gritter.add({
            title: 'Allocated Successfully',
            text: EquipmentAllocatorVenueBO.GetToString(obj),
            class_name: 'success',
            time: 1000
          })
        }
        else {
          //error message
          $.gritter.add({
            title: 'Allocation Failed',
            text: '<p>' + EquipmentAllocatorVenueBO.GetToString(obj) + '</p>' +
            '<p>' + response.ErrorText + '</p>',
            class_name: 'danger',
            time: 2000
          })
        }
        obj.IsProcessing(false)
        ViewModel.DisableUpload(false)
      })
  },
  editTimes: function (object, element) {

  },
  EquipmentIDSet: function (EquipmentAllocator) {
    if (EquipmentAllocator.EquipmentID()) {

    }
    else {

    }
  },
  EquipmentIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.EquipmentID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.StartBuffer(), obj.FirstEventStart())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.FirstEventStart(), obj.LastEventEnd())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.LastEventEnd(), obj.EndBuffer())
      if (ctAfterStart) {
        CtlError.AddError('Call Time must be before On Air Start Time')
      }
      if (onAirStartAfterEnd) {
        CtlError.AddError('On Air Start must be before On Air End Time')
      }
      if (onAirEndAfterWrap) {
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }
      if (!obj.FeedTypeID()) {
        CtlError.AddError("Feed Type is required")
      }
      if (obj.FeedName().trim().length == 0) {
        CtlError.AddError("Feed Name is required")
      }
    }
  },
  GetToString: function (self) {
    return self.VenueDayString() + ' - ' + self.Venue()
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  canUpload: function (obj) {
    if (!ViewModel.DisableUpload() && obj.IsValid()) {
      if (obj.EquipmentID() && obj.FeedTypeID() && obj.StartBuffer() && obj.EndBuffer() && obj.IsValid() && !obj.IsProcessing()) {
        return true
      }
    }
    return false
  }
}

EquipmentAllocatorListCriteriaBO = {
  SystemIDSet: function (self) { },
  ProductionAreaIDSet: function (self) { },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  LiveSet: function (self) { },
  DelayedSet: function (self) { },
  PremierSet: function (self) { },
  GenRefNoSet: function (self) { },
  SelectedChannelIDsXMLSet: function (self) { },
  GenreSet: function (self) { },
  SeriesSet: function (self) { },
  TitleSet: function (self) { }
}

FeedDefaultAudioSettingSelectBO = {
  IsSelectedSet: function (self) {
    self.IsSelectedClient(self.IsSelected())
    var feed = self.GetParent()
    var setting = feed.FeedDefaultAudioSettingList().Find("AudioSettingID", self.AudioSettingID())
    if (self.IsSelected()) {
      //if the setting is not already added
      if (!setting) {
        var nextSlot = null
        var availableSlots = feed.FeedDefaultAudioSettingList().Filter("AudioSettingID", null)
        if (availableSlots.length > 0) {
          nextSlot = availableSlots[0]
        } else {

        }
        nextSlot.AudioSettingID(self.AudioSettingID())
        //var defA = feed.FeedDefaultAudioSettingList.AddNew()
        //defA.AudioSettingID(self.AudioSettingID())
      }
    }
    else {
      //if the setting is already added
      if (setting) {
        setting.AudioSettingID(null)
      }
    }
  }
}

EquipmentFeedBO = {
  toString: function (equipmentFeed) {
    if (equipmentFeed.EquipmentFeedTitle().trim().length == 0) {
      return "Blank Equipment Booking"
    }
    else {
      return equipmentFeed.EquipmentFeedTitle()
    }
  },

  //set
  EquipmentIDSet: function (self) {

  },
  EquipmentFeedTitleSet: function (self) {
  },
  DebtorIDSet: function (self) {

  },
  ProductionAreaStatusIDSet: function (area) {

  },
  StartDateTimeBufferSet: function (self) {
  },
  StartDateTimeSet: function (self) {
    if (self.IsNew()) { self.StartDateTimeBuffer(self.StartDateTime()) }
  },
  EndDateTimeSet: function (self) {
    if (self.IsNew()) { self.EndDateTimeBuffer(self.EndDateTime()) }
  },
  EndDateTimeBufferSet: function (self) {
  },

  ChangeDescriptionValid: function (Value, Rule, CtlError) {
    var eq = CtlError.Object;
    if (eq.ChangeDescriptionRequired() && (eq.ChangeDescription().trim().length == 0)) {
      CtlError.AddError("Change Reason is required")
    }
  },

  canEdit: function (self, FieldName) {
    switch (FieldName) {
      case "EquipmentID", "EquipmentName":
        return self.SystemID() && self.ProductionAreaID() && self.IsOwner()
        break;
      case "ProductionAreaStatusID":
        return true
        break;
      case "DebtorID":
        return true
        break;
      default:
        return self.IsOwner()
        break;
    }
  },
  canView: function (self, FieldName) {
    switch (FieldName) {
      case 'FeedSetupTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'TurnaroundPointsTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'IngestInstructionsTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'CommentsTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'ProductionsTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'DestinationsTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'CrewTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'VehiclesTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'IncidentsTab':
        return (self && self.IsOwner() && self.SystemID() == 2)
        break;
      case 'IsNewLayout':
        return (self && self.IsOwner() && self.IsNew())
        break;
      case 'IsOldLayout':
        return (self && !self.IsNew())
        break;
      default:
        return true;
        break;
    }
    return true;
  },
  importGenRefNumber: function (equipmentFeed) {
    if (equipmentFeed.AddSynergyGenRefNo().toString().length >= 6) {
      equipmentFeed.IsProcessing(true)
      ViewModel.CallServerMethod("AddFeedGenRef", {
        ByVenueAndDate: equipmentFeed.ImportAllEventsSameDayAndVenue(),
        GenRefNumber: equipmentFeed.AddSynergyGenRefNo()
      },
        function (response) {
          if (response.Success) {
            response.Data.FeedProductionList.Iterate(function (fp, fpIndx) {
              var existing = equipmentFeed.FeedProductionList().Find("GenRefNumber", fp.SynergyGenRefNo)
              if (!existing) {
                var newFP = equipmentFeed.FeedProductionList.AddNew()
                newFP.ProductionID(fp.ProductionID)
                newFP.SynergyGenRefNo(fp.SynergyGenRefNo)
                newFP.ProductionDescription(fp.ProductionDescription)
              }
            })
            if (response.Data.FeedProductionList.length > 1) {
              let ft = ClientData.ROFeedTypeList.Find("FeedTypeID", equipmentFeed.FeedTypeID());
              let descript = ((ft ? ft.FeedType : "") + " - " + response.Data[0].GenreSeries());
              equipmentFeed.EquipmentFeedTitle(descript);
            }
          } else {
            OBMisc.Notifications.GritterError("Error Adding Events", response.ErrorText, 1000)
          }
          equipmentFeed.IsProcessing(false)
        })
    }
  },

  //dropdowns
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.EquipmentName(selectedItem.EquipmentName)
      businessObject.EquipmentID(selectedItem.EquipmentID)
      businessObject.ResourceID(selectedItem.ResourceID)
    }
    else {
      businessObject.EquipmentName("")
      businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
    }
  },

  triggerDebtorAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDebtorCriteriaBeforeRefresh: function (args) {

  },
  afterDebtorRefreshAjax: function (args) {

  },
  onDebtorSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.DebtorName(selectedItem.Debtor)
    } else {
      businessObject.DebtorName("")
    }
  },

  triggerStatusAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setStatusCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterStatusRefreshAjax: function (args) {

  },
  onStatusSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaStatusID(selectedItem.ProductionAreaStatusID)
      businessObject.ProductionAreaStatusName(selectedItem.ProductionAreaStatus)
      businessObject.StatusCssClass(selectedItem.CssClass)
    }
    else {
      businessObject.ProductionAreaStatusID(null)
      businessObject.ProductionAreaStatusName("")
      businessObject.StatusCssClass("")
    }
  },

  //other
  get: function (options) {
    Singular.GetDataStateless("OBLib.SatOps.EquipmentFeedList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Booking Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  save: function (equipmentFeed, options) {
    ViewModel.CallServerMethod("SaveEquipmentFeed",
      { EquipmentFeed: equipmentFeed.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Feed Failed", response.ErrorText, 1000)
        }
      })
  },
  saveModal: function (equipmentFeed) {
    if (equipmentFeed.IsDirty() && (equipmentFeed.ProductionAreaStatusIDOriginal() == 9)) {
      equipmentFeed.ChangeDescriptionRequired(true)
      Singular.Validation.CheckRules(equipmentFeed)
    }
    //else {
    Singular.Validation.CheckRules(equipmentFeed)
    if (equipmentFeed.IsValid()) {
      Singular.Validation.CheckRules(equipmentFeed)
      if (!equipmentFeed.IsProcessing() && equipmentFeed.IsValid()) {
        equipmentFeed.IsProcessing(true)
        EquipmentFeedBO.save(equipmentFeed, {
          onSuccess: function (response) {
            KOFormatter.Deserialise(response.Data, equipmentFeed)
            //equipmentFeed.ChangeDescriptionRequired(false)
            //equipmentFeed.ChangeDescription("")
            //equipmentFeed.IsProcessing(false)
            Singular.Validation.CheckRules(equipmentFeed)
          },
          onFail: function (response) {

          }
        })
      }
    }
    //}
  },
  refreshProductionCrew: function (equipmentFeed) {
    var me = this
    equipmentFeed.IsProcessing(true)
    var xmlProductionIDs = "";
    var xmlDisciplineIDs = "";

    equipmentFeed.FeedProductionList().Iterate(function (fp, fpInd) {
      xmlProductionIDs += "<Table ID='" + fp.ProductionID().toString() + "' />"
    })
    xmlProductionIDs = '<DataSet>' + xmlProductionIDs + '</DataSet>'

    var xmlDisciplineIDs = "<DataSet>";
    xmlDisciplineIDs += "<Table ID='" + '6' + "' />"
    xmlDisciplineIDs += "<Table ID='" + '7' + "' />"
    xmlDisciplineIDs += "<Table ID='" + '11' + "' />"
    xmlDisciplineIDs += "</DataSet>"

    Singular.GetDataStateless("OBLib.Productions.Crew.ReadOnly.ROCrewDetailByProductionList",
      {
        ProductionIDs: xmlProductionIDs,
        DisciplineIDs: xmlDisciplineIDs
      },
      function (response) {
        if (response.Success) {
          equipmentFeed.ROCrewDetailByProductionList.Set(response.Data)
        }
        else {
          OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the Crew', response.ErrorText, null)
        }
        equipmentFeed.IsProcessing(false)
      })

  },
  refreshProductionVehicles: function (equipmentFeed) {
    var me = this
    equipmentFeed.IsProcessing(true)
    var xmlProductionIDs = "";

    equipmentFeed.FeedProductionList().Iterate(function (fp, fpInd) {
      xmlProductionIDs += "<Table ID='" + fp.ProductionID().toString() + "' />"
    })
    xmlProductionIDs = '<DataSet>' + xmlProductionIDs + '</DataSet>'

    Singular.GetDataStateless("OBLib.Vehicles.ReadOnly.ROVehicleDetailByProductionList",
      {
        ProductionIDs: xmlProductionIDs
      },
      function (response) {
        if (response.Success) {
          equipmentFeed.ROVehicleDetailByProductionList.Set(response.Data)
        }
        else {
          OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the Vehicles', response.ErrorText, null)
        }
        equipmentFeed.IsProcessing(false)
      })
  },
  refreshEquipmentCosts: function (equipmentFeed) {
    var me = this
    equipmentFeed.IsProcessing(true)
    Singular.GetDataStateless("OBLib.Costs.EquipmentFeedCostList, OBLib",
      {
        ProductionSystemAreaID: equipmentFeed.ProductionSystemAreaID()
      },
      function (response) {
        if (response.Success) {
          equipmentFeed.EquipmentFeedCostList.Set(response.Data)
        }
        else {
          OBMisc.Notifications.GritterError('Error Fetching Costs', response.ErrorText, 1000)
        }
        equipmentFeed.IsProcessing(false)
      })
  },
  refreshIncidents: function (equipmentFeed) {
    var me = this
    equipmentFeed.IsProcessing(true)
    IncidentItemBO.get({
      criteria: { FeedID: equipmentFeed.FeedID() },
      onSuccess: function (response) {
        equipmentFeed.IncidentItemList.Set(response.Data)
        equipmentFeed.IsProcessing(false)
      },
      onFail: function (response) {
        equipmentFeed.IsProcessing(false)
      }
    })
    //Singular.GetDataStateless("OBLib.Costs.EquipmentFeedCostList, OBLib",
    //{
    //  ProductionSystemAreaID: equipmentFeed.ProductionSystemAreaID()
    //},
    //function (response) {
    //  if (response.Success) {
    //    equipmentFeed.EquipmentFeedCostList.Set(response.Data)
    //  }
    //  else {
    //    OBMisc.Notifications.GritterError('Error Fetching Costs', response.ErrorText, 1000)
    //  }
    //  equipmentFeed.IsProcessing(false)
    //})
  },
  selectFeedProductionAudioSettings: function (equipmentFeed) {
    equipmentFeed.IsExpanded(!equipmentFeed.IsExpanded())
  },
  applyAllAudioSettings: function (equipmentFeed) {
    equipmentFeed.GetParent().IsProcessing(true)
    var defaultAudioSettingList = equipmentFeed.FeedDefaultAudioSettingList()
    equipmentFeed.FeedProductionList().Iterate(function (fPrd, fpIndx) {
      var items = []
      fPrd.FeedProductionAudioSettingList().Iterate(function (itm, indx) {
        items.push(itm)
      })
      items.Iterate(function (item, indx) {
        fPrd.FeedProductionAudioSettingList.RemoveNoCheck(item)
      })
      defaultAudioSettingList.Iterate(function (aSet, aSetIndx) {
        if (aSet.AudioSettingID()) {
          var newItem = fPrd.FeedProductionAudioSettingList.AddNew()
          newItem.AudioSettingID(aSet.AudioSettingID())
          newItem.ChannelNumber(aSet.ChannelNumber())
        }
      })
    })
    equipmentFeed.GetParent().IsProcessing(false)
  },
  modalCss: function (equipmentFeed) {
    if (equipmentFeed && equipmentFeed.IsNew()) { return 'modal-dialog modal-sm' } else { return 'modal-dialog modal-lg' }
  },
  modalHeading: function (equipmentFeed) {
    if (!equipmentFeed) {
      return "Equipment Booking"
    } else {
      return equipmentFeed.EquipmentFeedTitle()
    }
  },
  showModal: function () {
    var me = this
    //modal events
    var efModal = $("#EquipmentFeedModal")
    efModal.off('shown.bs.modal')
    efModal.off('hidden.bs.modal')
    efModal.on('shown.bs.modal', function () {
      efModal.draggable({ handle: ".modal-header" })
      if (ViewModel.CurrentEquipmentFeed().IsOwner()) {
        OBMisc.UI.activateTab('FeedSetup')
      } else {
        OBMisc.UI.activateTab('Costs')
        me.refreshEquipmentCosts(ViewModel.CurrentEquipmentFeed())
      }
    })
    efModal.on('hide.bs.modal', function (e) {
      if (ViewModel.CurrentEquipmentFeed().IsDirty()) {
        e.preventDefault()
        e.stopImmediatePropagation()
        alert('There are unsaved changes in your booking')
        return false
      }
    })
    efModal.on('hidden.bs.modal', function (e) {
      ViewModel.CurrentEquipmentFeed(null)
    })
    efModal.modal()
  },
  joinFeedFromScheduler: function (resourceBooking, options) {
    ViewModel.CallServerMethod("JoinArea", {
      ProductionSystemAreaID: resourceBooking.ProductionSystemAreaID
    }, function (response) {
      if (response.Success) {
        //window.siteHubManager.changeMade()
        OBMisc.Notifications.GritterSuccess("Joined Succeeded", response.Data, 250)
        if (options.onSuccess) { options.onSuccess(response) }
      } else {
        OBMisc.Notifications.GritterError("Join Failed", response.Data, 1000)
      }
    })
  },
  importFeed(params) { 
    return ViewModel.CallServerMethodPromise("ImportEquipmentFeed", params);
  }
}

EquipmentFeedCostBO = {
  toString: function (self) {
    return "Feed: " + self.GetParent().EquipmentFeedTitle()
  }
}

IncidentItemBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Scheduling.Equipment.IncidentItemList", options.criteria, function (response) {
      if (response.Success) {
        if (options.onSuccess) {
          options.onSuccess(response)
        }
      }
      else {
        OBMisc.Notifications.GritterError("Error Fetching Incidents", response.ErrorText, 1000)
        if (options.onFail) {
          options.onFail(response)
        }
      }
    })
  }
}

EquipmentMaintenanceBO = {
  toString: function (equipmentMaintenance) {
    if (equipmentMaintenance.Title().trim().length == 0) {
      return "Blank Equipment Booking"
    }
    else {
      return equipmentMaintenance.Title()
    }
  },

  //set
  EquipmentIDSet: function (self) {

  },
  TitleSet: function (self) {
  },
  DebtorIDSet: function (self) {

  },
  ProductionAreaStatusIDSet: function (area) {

  },
  StartDateTimeBufferSet: function (self) {
  },
  StartDateTimeSet: function (self) {
    self.StartDateTimeBuffer(self.StartDateTime())
  },
  EndDateTimeSet: function (self) {
    self.EndDateTimeBuffer(self.EndDateTime())
  },
  EndDateTimeBufferSet: function (self) {
  },

  ChangeDescriptionValid: function (Value, Rule, CtlError) {
    var eq = CtlError.Object;
    if (eq.IsDirty() && (eq.ProductionAreaStatusIDOriginal() == 9) && (eq.ChangeDescription().trim().length == 0)) {
      CtlError.AddError("Change Reason is required")
    }
  },

  canEdit: function (self, FieldName) {
    switch (FieldName) {
      case "EquipmentID", "EquipmentName":
        return self.SystemID() && self.ProductionAreaID() && self.IsOwner()
        break;
      default:
        return self.IsOwner()
        break;
    }
  },
  canView: function (self, FieldName) {
    switch (FieldName) {
      case 'ProductionsTab':
      case 'DestinationsTab':
      case 'CrewTab':
      case 'VehiclesTab':
        return (self && self.SystemID() == 2)
        break;
      case 'IsNewLayout':
        return (self && self.IsNew())
        break;
      case 'IsOldLayout':
        return (self && !self.IsNew())
        break;
      //case "QuickAddEquipmentSchedulesButton":
      //  if (self.EquipmentScheduleID() && !self.IsNew()) {
      //    return true;
      //  } else {
      //    return false;
      //  }
      //  break;
      //case "RoomScheduleTypeID":
      //  return self.IsNew();
      //  break;
      //case "EquipmentID", "EquipmentName":
      //  return self.IsNew();
      //  break;
      //case "ProductionAreaStatusID", "ProductionAreaStatusName":
      //  return true;
      //  break;
      //case "DebtorID", "DebtorName":
      //  return true;
      //  break;
      default:
        return true;
        break;
    }
    return true;
  },

  //dropdowns
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      //businessObject.EquipmentName(selectedItem.EquipmentName)
      //businessObject.EquipmentID(selectedItem.EquipmentID)
      businessObject.ResourceID(selectedItem.ResourceID)
    }
    else {
      //businessObject.EquipmentName("")
      //businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
    }
  },

  triggerDebtorAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDebtorCriteriaBeforeRefresh: function (args) {

  },
  afterDebtorRefreshAjax: function (args) {

  },
  onDebtorSelected: function (selectedItem, businessObject) {
    businessObject.DebtorName(selectedItem.Debtor)
  },

  triggerStatusAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setStatusCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterStatusRefreshAjax: function (args) {

  },
  onStatusSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaStatusID(selectedItem.ProductionAreaStatusID)
      businessObject.ProductionAreaStatusName(selectedItem.ProductionAreaStatus)
      businessObject.StatusCssClass(selectedItem.CssClass)
    }
    else {
      businessObject.ProductionAreaStatusID(null)
      businessObject.ProductionAreaStatusName("")
      businessObject.StatusCssClass("")
    }
  },

  //other
  get: function (options) {
    Singular.GetDataStateless("OBLib.SatOps.EquipmentMaintenanceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Booking Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  save: function (equipmentMaintenance, options) {
    ViewModel.CallServerMethod("SaveEquipmentMaintenance",
      { EquipmentMaintenance: equipmentMaintenance.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Feed Failed", response.ErrorText, 1000)
        }
      })
  },
  saveModal: function (equipmentMaintenance) {
    if (equipmentMaintenance.IsDirty() && (equipmentMaintenance.ProductionAreaStatusIDOriginal() == 9) && (equipmentMaintenance.ChangeDescription().trim().length == 0)) {
      equipmentMaintenance.ChangeDescriptionRequired(true)
      Singular.Validation.CheckRules(equipmentMaintenance)
    }
    else {
      equipmentMaintenance.ChangeDescriptionRequired(false)
      Singular.Validation.CheckRules(equipmentMaintenance)
      if (!equipmentMaintenance.IsProcessing() && equipmentMaintenance.IsValid()) {
        equipmentMaintenance.IsProcessing(true)
        EquipmentMaintenanceBO.save(equipmentMaintenance, {
          onSuccess: function (response) {
            KOFormatter.Deserialise(response.Data, equipmentMaintenance)
            equipmentMaintenance.ChangeDescriptionRequired(false)
            equipmentMaintenance.ChangeDescription("")
            equipmentMaintenance.IsProcessing(false)
            Singular.Validation.CheckRules(equipmentMaintenance)
            EquipmentMaintenanceBO.hideModal()
          },
          onFail: function (response) {

          }
        })
      }
    }
  },
  refreshEquipmentCosts: function (equipmentMaintenance) {
    var me = this
    equipmentMaintenance.IsProcessing(true)
    Singular.GetDataStateless("OBLib.Costs.EquipmentMaintenanceCostList, OBLib",
      {
        ProductionSystemAreaID: equipmentMaintenance.ProductionSystemAreaID()
      },
      function (response) {
        if (response.Success) {
          equipmentMaintenance.EquipmentFeedCostList.Set(response.Data)
        }
        else {
          OBMisc.Notifications.GritterError('Error Fetching Costs', response.ErrorText, 1000)
        }
        equipmentMaintenance.IsProcessing(false)
      })
  },
  modalCss: function (equipmentMaintenance) {
    if (equipmentMaintenance && equipmentMaintenance.IsNew()) { return 'modal-dialog modal-xs' } else { return 'modal-dialog modal-xs' }
  },
  modalHeading: function (equipmentMaintenance) {
    if (!equipmentMaintenance) {
      return "Equipment Booking"
    } else {
      return equipmentMaintenance.Title()
    }
  },
  showModal: function () {
    $("#EquipmentMaintenaceModal").off('shown.bs.modal')
    $("#EquipmentMaintenaceModal").off('hidden.bs.modal')
    $("#EquipmentMaintenaceModal").on('shown.bs.modal', function () {
      $("#EquipmentMaintenaceModal").draggable({ handle: ".modal-header" })
    })
    $("#EquipmentMaintenaceModal").on('hide.bs.modal', function (e) {
      if (ViewModel.CurrentEquipmentMaintenance().IsDirty()) {
        e.preventDefault()
        e.stopImmediatePropagation()
        alert('There are unsaved changes in your booking')
        return false
      }
    })
    $("#EquipmentMaintenaceModal").on('hidden.bs.modal', function (e) {
    })
    $("#EquipmentMaintenaceModal").modal()
  },
  hideModal: function () {
    $("#EquipmentMaintenaceModal").modal('hide')
  }
}