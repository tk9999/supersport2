﻿//#region Editable
HumanResourceBO = {


  setCriteriaBeforeRefresh: function () {
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  afterHRRefreshAjax: function () {
  },
  onHRSelected: function () {
  },
  HRIDSet: function () {
  },

  //cans
  canSave(humanResource) {
    if (humanResource.IsValid() && !humanResource.IsProcessing()) {
      return true;
    } else {
      return false;
    }
  },
  canView(what, humanResource) {
    switch (what) {
      case 'NewHumanResourceSection':
        return (humanResource.IsNew() || humanResource.HasMissingBasics() || !humanResource.PrimarySkillDisciplineID() || !humanResource.PrimarySystemID() || !humanResource.PrimaryProductionAreaID());
        break;
      case 'OldHumanResourceSection':
        return (!humanResource.IsNew() && !humanResource.HasMissingBasics() && humanResource.PrimarySkillDisciplineID() && humanResource.PrimarySystemID() && humanResource.PrimaryProductionAreaID());
        break;
      case 'TabsSection':
        return (!humanResource.HasMissingBasics());
        break;
    }
  },
  canEdit(what, humanResource) {
    switch (what) {
      case 'ManagerHumanResourceID2':
        return (humanResource.ManagerHumanResourceID() != null);
        break;
    }
  },

  //validation
  EmailAddressValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(HumanResource.EmailAddress()) == false) {
      CtlError.AddError("Invalid Email Address");
    }
  },
  AltEmailAddressValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    if (HumanResource.AlternativeEmailAddress() != "") {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(HumanResource.AlternativeEmailAddress()) == false) {
        CtlError.AddError("Invalid Email Address");
      }
    }
  },
  InActiveReasonRequired(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    if (HumanResource.ActiveInd() == false && HumanResource.InactiveReason().trim() == "") {
      CtlError.AddError("Inactive Reason required");
    }
  },
  LicenceExpDateValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    if (HumanResource.LicenceInd() && HumanResource.LicenceExpiryDate() == null) {
      CtlError.AddError("Licence Expiry Date Required");
    }
  },
  ContractTypeIDValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    //Supplier & Contract Type ID validation
    var Supplier = ((HumanResource.SupplierID() != null || HumanResource.SupplierID() != undefined) ? true : false); //HumanResource.SupplierID();
    var ContractType = ((HumanResource.ContractTypeID() != null || HumanResource.ContractTypeID() != undefined) ? true : false); //HumanResource.ContractTypeID();

    if (Supplier && ContractType && !HumanResource.InvoiceSupplierInd()) {
      CtlError.AddError("Supplier Human Resources cannot have a contract type");
    }
    else if (!Supplier && !ContractType) {
      CtlError.AddError("Contract Type is required");
    }
  },
  CityIDValid(Value, Rule, CtlError) {
    if (!CtlError.Object.CityID()) {
      CtlError.AddError("Home City is required");
    }
  },
  SupplierIDValid(Value, Rule, CtlError) {
    if (CtlError.Object.InvoiceSupplierInd() && !CtlError.Object.SupplierID()) {
      CtlError.AddError("Supplier is required")
    }
  },
  NationalityCountryIDValid(Value, Rule, CtlError) {
    if (!CtlError.Object.NationalityCountyID()) {
      CtlError.AddError("Nationality is required");
    }
  },
  DuplicateCountValid(Value, Rule, CtlError) {
    var hr = CtlError.Object;
    if (hr.ROHRDuplicateList().length > 0) {
      CtlError.AddError("Duplicates Exist!")
    }
  },
  PrimarySystemAndAreaValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    if (!HumanResource.PrimarySystemID() && !HumanResource.PrimaryProductionAreaID()) {
      CtlError.AddError("Both Primary Sub-Dept and Area are required")
    }
  },
  ContractStartDateValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    if (HumanResource.ContractTypeID() == 8 && HumanResource.ContractStartDate() == undefined) {
      CtlError.AddError("Contract Start Date is Required");
    }
  },
  ContractEndDateValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    if (HumanResource.ContractTypeID() == 8 && HumanResource.ContractEndDate() == undefined) {
      CtlError.AddError("Contract End Date is Required");
    }
  },
  ContractEndNotBeforeStartValid(Value, Rule, CtlError) {
    var HumanResource = CtlError.Object;
    var sd = new Date(HumanResource.ContractStartDate());
    var ed = new Date(HumanResource.ContractEndDate());

    if (ed.getTime() < sd.getTime()) {
      CtlError.AddError("Contract End Date must be after Contract Start Date");
    }
  },
  PrimarySkillIDValid(Value, Rule, CtlError) {
    var humanResource = CtlError.Object;
    if (!CtlError.Object.PrimarySkillDisciplineID()) {
      CtlError.AddError("Primary Skill is required")
    }
    var roDisc = ClientData.RODisciplineList.Find("DisciplineID", humanResource.PrimarySkillDisciplineID())
    if (roDisc) {
      if (roDisc.PositionRequiredInd && !humanResource.PrimarySkillPositionID()) {
        CtlError.AddError("Primary Skill Position is required")
      }
    }
  },
  MissingRateCountValid(Value, Rule, CtlError) {
    if (!CtlError.Object.IsNew() && CtlError.Object.ContractTypeID()) { //if has ContractType
      var ct = ClientData.ROContractTypeList.Find("ContractTypeID", CtlError.Object.ContractTypeID())
      if (ct && ct.FreelancerInd) { //if ct is not blank AndAlso ct is a freelance contract type
        if (!CtlError.Object.IsNew() && CtlError.Object.SkillCount() > 0) {
          if ((CtlError.Object.CurrentUserIsHRManager() || CtlError.Object.IsManager()) && CtlError.Object.MissingRateCount() > 0) {
            CtlError.AddError("Some skills have missing rates")
          }
        }
      }
    }
  },

  //set expressions
  FirstNameSet: function (self) {
    if (!self.IsCheckingDuplicates()) {
      HumanResourceBO.GetHRHDuplicates(self)
    }
  },
  SurnameSet: function (self) {
    if (!self.IsCheckingDuplicates()) {
      HumanResourceBO.GetHRHDuplicates(self)
    }
  },
  IDNoSet: function (self) {
    if (!self.IsCheckingDuplicates()) {
      HumanResourceBO.GetHRHDuplicates(self)
    }
  },
  EmployeeCodeSet: function (self) {
    if (!self.IsCheckingDuplicates()) {
      HumanResourceBO.GetHRHDuplicates(self)
    }
  },
  NationalityCountyIDSet: function (self) {
    if (!self.IsCheckingDuplicates()) {
      HumanResourceBO.GetHRHDuplicates(self)
    }
  },
  ContractTypeIDSet: function (self) {
    if (self.ContractTypeID()) {
      //set PayHalfMonth
      //&& self.PrimaryProductionAreaID() == 1
      //if (self.PrimarySystemID() == 2) {
      if (self.ContractTypeID() == 2 || self.ContractTypeID() == 3) {
        self.PayHalfMonthInd(true);
      }
      else if (self.ContractTypeID() == 6 || self.ContractTypeID() == 7 || self.ContractTypeID() == 8) {
        self.PayHalfMonthInd(false);
      }
      //}
      //else {
      self.PayHalfMonthInd(false)
      //}
      //set IsFreelancer
      var roCT = ClientData.ROContractTypeList.Find("ContractTypeID", self.ContractTypeID());
      if (roCT) {
        self.IsFreelancer(roCT.FreelancerInd)
      }
    }
  },
  CellPhoneNumberSet: function (self) {
    self.CellPhoneNumber(self.CellPhoneNumber().replace(/[^0-9]/g, ""));
  },
  AlternativeContactNumberSet: function (self) {
    self.AlternativeContactNumber(self.AlternativeContactNumber().replace(/[^0-9]/g, ""));
  },

  HumanResourceBOToString: function (self) {
    if (self.IsNew() && self.Firstname().length == 0 && self.Surname().length == 0) {
      return "New Human Resource"
    }
    else {
      return self.Firstname() + ' ' + self.Surname()
    }
  },

  GetHRHDuplicates: function (self, afterFetch) {
    if (self.Firstname().trim().length > 0 || self.Surname().trim().length > 0 || self.IDNo().trim().length > 0 || self.EmployeeCode().trim().length > 0) {
      self.IsProcessing(true)
      self.IsCheckingDuplicates(true)
      self.ROHRDuplicateList([])
      Singular.GetDataStateless('OBLib.HR.ROHRDuplicateList, OBLib', {
        HumanResourceID: ((ViewModel.CurrentHumanResource().HumanResourceID() == 0) ? null : ViewModel.CurrentHumanResource().HumanResourceID()),
        MatchTypeID: null,
        Firstname: ViewModel.CurrentHumanResource().Firstname(),
        Surname: ViewModel.CurrentHumanResource().Surname(),
        IDNo: ViewModel.CurrentHumanResource().IDNo(),
        EmployeeCode: ViewModel.CurrentHumanResource().EmployeeCode()
      },
        function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, self.ROHRDuplicateList);
            self.DuplicateCount(self.ROHRDuplicateList().length)
            if (self.ROHRDuplicateList().length > 0) {
              HumanResourceBO.OnDuplicatesDetected(self)
            }
            if (afterFetch) { afterFetch(args) }
          }
          self.IsProcessing(false)
          self.IsCheckingDuplicates(false)
        })
    }
  },
  AccreditationPhotoStyle: function (HumanResource) {
    if (HumanResource.AccreditationImageID()) {
      var basicPath = window.SiteInfo.SitePath  //+ '/Images/Profile/' + ViewModel.CurrentUserID().toString()
      return "url('" + basicPath + '/Api/GetImage?ImageID=' + HumanResource.AccreditationImageID().toString() + "')";
    }
    else {
      return '';
    }
  },
  AccreditationPhotoWidth: function (HumanResource, element) {
    console.log(element.naturalWidth)
    //var imgType = ClientData.ROImageTypeList.Find("ImageTypeID", 1);
    //element.style.width = imgType.Width.toString() + 'px';
    //return imgType.Width.toString() + 'px';
  },
  AccreditationPhotoHeight: function (HumanResource, element) {
    console.log(element.naturalHeight)
    //var imgType = ClientData.ROImageTypeList.Find("ImageTypeID", 1);
    //element.style.height = imgType.Height.toString() + 'px';
    //return imgType.Height.toString() + 'px';
  },
  AccreditationPhotoSrc: function (HumanResource) {
    //; width: 320px; height: 240px;
    if (HumanResource.AccreditationImageID()) {
      var basicPath = window.SiteInfo.SitePath
      return basicPath + '/Api/GetImage?ImageID=' + HumanResource.AccreditationImageID().toString()
    } else {
      return "";
    }
  },

  FilteredPrimarySkillPositionList: function (ROPositionList, humanResource) {
    var results = []
    if (humanResource.PrimarySkillDisciplineID()) {
      ROPositionList.Iterate(function (itm, indx) {
        if (itm.DisciplineID == humanResource.PrimarySkillDisciplineID()) {
          results.push(itm)
        }
      })
    }
    return results
  },
  FilteredPrimaryAreaList: function (ROSystemProductionAreaList, humanResource) {
    var results = []
    if (humanResource.PrimarySystemID()) {
      ROSystemProductionAreaList.Iterate(function (itm, indx) {
        if (itm.SystemID == humanResource.PrimarySystemID()) {
          results.push(itm)
        }
      })
    }
    return results
  },
  FilteredDuplicatePrimaryAreaList: function (ROSystemProductionAreaList, dupe) {
    var results = []
    var humanResource = dupe.GetParent()
    if (humanResource.PrimarySystemID()) {
      ROSystemProductionAreaList.Iterate(function (itm, indx) {
        if (itm.SystemID == humanResource.PrimarySystemID()) {
          results.push(itm)
        }
      })
    }
    return results
  },
  OnDuplicatesDetected: function (humanResource) {

  },

  deleteUnAvailability: function (deletionList, options) {
    var serialisedList = []
    deletionList.Iterate(function (itm, indx) {
      serialisedList.push(itm.Serialise())
    })
    ViewModel.CallServerMethod("DeleteUnAvailability",
      {
        ROHumanResourceUnAvailabilityList: serialisedList
      },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },

  internalStaffPhotoStyle: function (HumanResource) {
    if (HumanResource.HumanResourceImageID()) {
      var basicPath = window.SiteInfo.SitePath  //+ '/Images/Profile/' + ViewModel.CurrentUserID().toString()
      return "url('" + basicPath + '/Api/GetImage?ImageID=' + HumanResource.HumanResourceImageID().toString() + "')";
    }
    else {
      return '';
    }
  },
  chooseInternalStaffPhoto: function (obj) {
    var imageType = ClientData.ROImageTypeList.Find('ImageTypeID', 5);
    Singular.ShowFileDialog(function (e) {
      var sizeInBytes = e.target.files[0].size;
      var sizeInKB = sizeInBytes / 1024;
      if (sizeInKB > imageType.FileSizeLimit) {
        alert('Image exceeds the file size limit of ' + (imageType.FileSizeLimit / 1024).toString() + 'MB');
      }
      else {
        obj.IsLoadingImage(true)
        Singular.UploadFile(e.target.files[0], ViewModel.TempImage(), 'png,jpg,jpeg', function (Result) {
          if (Result.Success) {
            obj.HumanResourceImageID(Result.DocumentID);
          } else {
            alert(Result.Response);
          }
          obj.IsLoadingImage(false)
        },
          'Image=true&ImageTypeID=' + "5" + '&Stateless=true&ForHumanResourceID=' + obj.HumanResourceID());
      }
    });
  },

  //manager
  setManagerIDCriteriaBeforeRefresh() {
  },
  triggerManagerIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterManagerIDRefreshAjax() {
  },
  onManagerSelected() {
  },
  ManagerHumanResourceIDSet() {
  },

  //manager2
  setSecondaryManagerIDCriteriaBeforeRefresh(args) {
    args.Data.ExcludeManagerHumanResourceIDs = [args.Object.ManagerHumanResourceID()];
  },
  triggerSecondaryManagerIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterSecondaryManagerIDRefreshAjax() {
  },
  onSecondaryManagerSelected() {
  },
  SecondaryManagerHumanResourceIDSet() {
    //  
  }

};

HumanResourceSkillBO = {
  PrimaryDisciplineSet(skill) {
    if (ViewModel.CurrentHumanResource()) {
      if (skill.PrimaryInd()) {
        ViewModel.CurrentHumanResource().PrimarySkillDisciplineID(skill.DisciplineID());
      } else {
        ViewModel.CurrentHumanResource().PrimarySkillDisciplineID(null);
      }
      Singular.Validation.CheckRules(ViewModel.CurrentHumanResource());
    }
  },
  PrimaryDisciplineValid(Value, Rule, CtlError) {
    var CurrentHumanResourceSkill = CtlError.Object;
    var HRSkillList = CurrentHumanResourceSkill.GetParent().HumanResourceSkillList();
    var length = CurrentHumanResourceSkill.GetParent().HumanResourceSkillList().length;
    var count = 0; //Count for all primary skills
    if (length > 0) {
      for (var i = 0; i < length; i++) {
        if (HRSkillList[i].PrimaryInd() == true) {
          count++;
        }
      }
    }
    if (count != 1) {
      CtlError.AddError("At Least 1 Primary Skill is Required");
    }
  },
  CheckDiscipline(Value, Rule, CtlError) {
    var HumanResourceSkill = CtlError.Object;
    var d = ClientData.RODisciplineList.Find('DisciplineID', HumanResourceSkill.DisciplineID());
    if (d != undefined) {
      if (d.PositionRequiredInd == true) {
        var SkillPosCount = HumanResourceSkill.HumanResourceSkillPositionList().length;
        if (SkillPosCount == 0) {
          CtlError.AddError("At least one Skill Position required.");
        }
      }
    }
  },
  CheckSkillLevel(Value, Rule, CtlError) {
    //****Rule No Longer Required****
    //  var HumanResourceSkill = CtlError.Object; 
    //  var d = ClientData.RODisciplineList.Find('DisciplineID', HumanResourceSkill.DisciplineID());
    //  if (d != undefined) {
    //    if (d.SkillLevelRequiredInd == true && HumanResourceSkill.SkillLevelID() == null) {
    //      CtlError.AddError("Skill Level required for selected Discipline.");
    //    }
    //    else if (d.SkillLevelRequiredInd == false && HumanResourceSkill.SkillLevelID() != null) {
    //      CtlError.AddError("Skill Level not required for selected Discipline.");
    //    }
    //  }
  },
  HRSkillPositionListUnique(Value, Rule, CtlError) {
    //  var HumanResourceSkill = CtlError.Object;
    //  var hrspList = HumanResourceSkill.HumanResourceSkillPositionList();
    //  var len = hrspList.length;

    //  for (var i = 0; i < len; i++) {
    //    var ptID1 = hrspList[i].ProductionTypeID();
    //    var pID1 = hrspList[i].PositionID();
    //    for (var j = i + 1; j < len; j++) {
    //      var ptID2 = hrspList[j].ProductionTypeID();
    //      var pID2 = hrspList[j].PositionID();

    //      if (ptID1 == ptID2 && pID1 == pID2) {
    //        CtlError.AddError("Skills must be unique by Production Type and Position");
    //      }
    //    }
    //  }  
  },
  HRSkillRateListUnique(Value, Rule, CtlError) {
    //  var HumanResourceSkill = CtlError.Object;
    //  var hrsrList = HumanResourceSkill.HumanResourceSkillRateList();
    //  var len = hrsrList.length;

    //  for (var i = 0; i < len; i++) {
    //    var RateDate1 = new Date(hrsrList[i].RateDate());
    //    //var RateDate1 = ;

    //    for (var j = i + 1; j < len; j++) {
    //      var RateDate2 = new Date(hrsrList[j].RateDate());

    //      if (RateDate1.getTime() == RateDate2.getTime()) {
    //        CtlError.AddError("Skill Rates must be unique by Rate Date");
    //      }
    //    }
    //  }
  },
  HumanResourceSkillBOToString(self) {
    var Discipline = ClientData.RODisciplineList.Find('DisciplineID', self.DisciplineID());
    var ds = (Discipline == undefined) ? 'Skill' : Discipline.Discipline;
    //var SkillLevel = ClientData.ROSkillLevelList.Find('SkillLevelID', self.SkillLevelID());
    //var ds = (Discipline == undefined) ? '' : ' - Discipline: ' + Discipline.Discipline;
    //var sk = (SkillLevel == undefined) ? '' : ' - Skill Level: ' + SkillLevel.SkillLevel;
    //var StartDate = (self.StartDate() == undefined) ? '' : ' - Start Date: ' + self.StartDate().format('dd-MMM-yy');
    //var EndDate = (self.EndDate() == undefined) ? '' : ' - End Date:' + self.EndDate().format('dd-MMM-yy');
    return ds;// + StartDate + EndDate;
  }
};

HumanResourceSkillPositionBO = {
  PositionIDSet: function (self) {
    if (self.PositionID()) {
      var pos = ClientData.ROPositionList.Find("PositionID", self.PositionID())
      if (pos) {
        self.PositionTypeID(pos.PositionTypeID)
      }
    } else {
      self.PositionTypeID(null)
    }
  },
  PositionIDValid: function (Value, Rule, CtlError) {
    var skillPos = CtlError.Object;

    if (!skillPos.PositionID()) {
      CtlError.AddError("Position is required")
    }

    //if (skillPos.ProductionAreaID() && skillPos.GetParent().DisciplineID()) {
    //  var settingList = ClientData.ROSkillsDatabaseSettingList.Filter("DisciplineID", skillPos.GetParent().DisciplineID())
    //  settingList = settingList.Filter("ProductionAreaID", skillPos.ProductionAreaID())
    //  if (settingList.length == 1) {
    //    var setting = settingList[0]
    //    if (setting.PositionTypeRequired && !skillPos.PositionTypeID()) {
    //      CtlError.AddError("Position Type is required")
    //    }
    //    if (setting.ProductionTypeRequired && !skillPos.ProductionTypeID()) {
    //      CtlError.AddError("Production Type is required")
    //    }
    //    if (setting.RoomTypeRequired && !skillPos.RoomTypeID()) {
    //      CtlError.AddError("Room Type is required")
    //    }
    //    if (setting.RoomRequired && !skillPos.RoomID()) {
    //      CtlError.AddError("Room is required")
    //    }
    //  } else {
    //    CtlError.AddError("setting could not be found")
    //  }
    //} else {
    //  CtlError.AddError("Discipline and Area must be specified")
    //}

  },
  HRSkillPositionListUnique: function (Value, Rule, CtlError) {
    //var HumanResourceSkillPosition = CtlError.Object;
    //var hrspList = HumanResourceSkillPosition.GetParent().HumanResourceSkillPositionList();  //HumanResourceSkill.HumanResourceSkillPositionList();
    //var len = hrspList.length;

    //for (var i = 0; i < len; i++) {
    //  if (HumanResourceSkillPosition != hrspList[i]) {
    //    var ptID1 = hrspList[i].ProductionTypeID();
    //    var pID1 = hrspList[i].PositionID();
    //    var ptID2 = HumanResourceSkillPosition.ProductionTypeID();
    //    var pID2 = HumanResourceSkillPosition.PositionID();
    //    if (ptID1 == ptID2 && pID1 == pID2) {
    //      //CtlError.AddError("Skills must be unique by Production Type and Position");
    //    }
    //  }
    //}
  },
  ToString: function (self) {
    return 'Human Resource Skill Position';
  },
  FilterPositionsByDiscipline: function (List, self) {
    var skill = self.GetParent()
    if (skill.DisciplineID()) {
      var newList = ClientData.ROPositionList.Filter("DisciplineID", skill.DisciplineID())
      return newList
    } else {
      return List
    }
  },
  deleteSkillPosition: function (pos) {
    var prnt = pos.GetParent()
    prnt.HumanResourceSkillPositionList.Remove(pos)
  }
};

HumanResourceSkillRateBO = {
  HRSkillRateListUnique: function (Value, Rule, CtlError) {
    //HumanResourceHelper.HumanResourceSkillRate.Rules.HRSkillRateListUnique(Value, Rule, CtlError);
    var HumanResourceSkillRate = CtlError.Object;
    var hrsrList = HumanResourceSkillRate.GetParent().HumanResourceSkillRateList(); //HumanResourceSkill.HumanResourceSkillRateList(); 
    var len = hrsrList.length;
    for (var i = 0; i < len; i++) {
      if (HumanResourceSkillRate != hrsrList[i]) {
        var RateDate1 = new Date(hrsrList[i].RateDate());
        var RateDate2 = new Date(HumanResourceSkillRate.RateDate());
        if (RateDate1.getTime() == RateDate2.getTime()) {
          CtlError.AddError("Skill Rates must be unique by Rate Date");
        }
      }
    }
  },
  RatePerDayValid: function (Value, Rule, CtlError) {
    var HumanResourceSkillRate = CtlError.Object;
    if (HumanResourceSkillRate.RatePerDay() < 0) {
      CtlError.AddError("Rate per day cannot be less than zero");
    }
  },
  HumanResourceSkillRateBOToString: function (self) {
    var RateDate = self.RateDate();
    var rt = (RateDate == undefined) ? '' : ' - Rate Date: ' + RateDate.format('dd-MMM-yy');
    return 'Human Resource Skill Rate' + rt;
  },
  deleteRate: function (rate) {
    var prnt = rate.GetParent()
    prnt.HumanResourceSkillRateList.Remove(rate)
  }
};

HumanResourceSystemBO = {
  SystemIsValid: function (Value, Rule, CtlError) {
    var hrs = CtlError.Object;
    var hrsList = hrs.GetParent().HumanResourceSystemList();
    var len = hrsList.length;
    for (var i = 0; i < len; i++) {
      if (hrs != hrsList[i]) {
        if (hrs.SystemID() == hrsList[i].SystemID() && hrs.ProductionAreaID() == hrsList[i].ProductionAreaID()) {
          CtlError.AddError("Unique Sub-Dept Required");
        }
      }
    }
  },
  HumanResourceSystemBOToString: function (self) {
    var System = ClientData.ROSystemList.Find('SystemID', self.SystemID());
    var s = (System == undefined) ? '' : ' - System: ' + System.System;
    var ProductionArea = ClientData.ROProductionAreaList.Find('ProductionAreaID', self.ProductionAreaID());
    var pa = (ProductionArea == undefined) ? '' : ' - Production Area: ' + ProductionArea.ProductionArea;
    var Primary = (self.PrimaryInd() == undefined) ? '' : ' - Primary: ' + self.PrimaryInd();
    return 'Human Resource System' + s + pa + Primary;
  }
};

HumanResourceSecondmentBO = {
  CountryIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.CountryID()) {
      CtlError.AddError("Country is required");
    }
  },
  DebtorIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.DebtorID()) {
      CtlError.AddError("Debtor is required");
    }
  },
  IsAuthorisedSet: function (HRSec) {
    if (HRSec.IsAuthorised()) {
      HRSec.AuthorisedByUserID(ViewModel.CurrentUserID());
      HRSec.AuthorisedDateTime(new Date());
      HRSec.AuthorisedByName(ViewModel.CurrentUserName())
    }
    else {
      HRSec.AuthorisedByUserID(null);
      HRSec.AuthorisedDateTime(null);
      HRSec.AuthorisedByName(null)
    }
  },
  IsConfirmedSet: function (HRSec) {
    if (HRSec.IsConfirmed()) {
      HRSec.ConfirmedByUserID(ViewModel.CurrentUserID());
      HRSec.ConfirmedDateTime(new Date())
      HRSec.ConfirmedByName(ViewModel.CurrentUserName())
    }
    else {
      HRSec.ConfirmedByUserID(null);
      HRSec.ConfirmedDateTime(null);
      HRSec.ConfirmedByName(null)
    }
  }
};

HRSystemSelectBO = {
  CanEdit: function (PropertyName, HRSystemSelect) {
    switch (PropertyName) {
      case "IsPrimary":
        var hr = HRSystemSelect.GetParent()
        var list = hr.HRSystemSelectList()
        var primaryItems = list.Filter("IsPrimary", true)
        if (primaryItems.length > 1) {
          return true
        }
        else if (primaryItems.length == 1) {
          if (primaryItems[0].Guid() == HRSystemSelect.Guid()) {
            return true
          }
          else {
            return false
          }
        }
        else {
          return true
        }
        break;
      case "IsAdded":
        return true
        break;
      default:
        return false
        break;
    }
  },
  IsAddedSet: function (self) {

  },
  IsPrimarySet: function (self) {
    if (self.IsPrimary() && !self.IsAdded()) { self.IsAdded(true) }
    var hr = self.GetParent()
    var primCount = 0
    hr.HRSystemSelectList().Iterate(function (itm, indx) {
      if (itm.IsPrimary()) {
        primCount += 1
      }
    })
    hr.PrimarySystemAreaCount(primCount)
  }
}

HumanResourceOffPeriodBO = {

  //Cans
  CanEditField: function (self, FieldName) {
    switch (FieldName) {
      //case "AuthorisedID":
      //  return self.CanAuthoriseLeave();
      //  break;
      //case 'ResourceSchedulerSaveButton':
      //  return self.IsValid();
      default:
        return true;
        break;
    };
  },

  //Set
  StartDateSet: function (self) {
    this.CalculateDefaultLeaveEndTime(self)
    this.updateBooking(self)
    this.getOtherDisciplinesOnLeave(self)
    this.getOffPeriodClashes(self)
  },
  EndDateSet: function (self) {
    this.updateBooking(self)
    this.getOtherDisciplinesOnLeave(self)
    this.getOffPeriodClashes(self)
  },
  OffReasonIDSet: function (self) {
    this.CalculateDefaultLeaveEndTime(self)
    this.updateBooking(self)
  },
  AuthorisedIDSet: function (self) {
    this.updateBooking(self)
  },

  //Validation
  OffReasonDetailRequired: function (Value, Rule, CtlError) {
    var hrop = CtlError.Object;
    var or = ClientData.ROOffReasonList.Find('OffReasonID', hrop.OffReasonID());
    if (or != undefined) {
      if (or.DetailRequiredInd == true && hrop.Detail().trim() == "") {
        CtlError.AddError("Detail required for selected Off Reason");
      }
    }
  },
  OffReasonIDValid: function (Value, Rule, CtlError) {
    var hrop = CtlError.Object;

    if (hrop.OffReasonID() != null) {
      if (ClientData.ROOffReasonList != undefined) {
        var offReason = ClientData.ROOffReasonList.Find('OffReasonID', hrop.OffReasonID());
        var IsFreelancer = hrop.IsFreelancer();
        if (hrop.IsNew()) {
          if (IsFreelancer && (offReason.FreelancerInd == false)) {
            CtlError.AddError("Selected Off Reason only applicable to Human Resources that are not Freelancers");
          }
          else if (!IsFreelancer && offReason.FreelancerInd) {
            CtlError.AddError("Selected Off Reason only applicable to Human Resources that are Freelancers");
          }
        }
      }
    }

    ////Check Leave Duration
    //if (ViewModel.CurrentSystemID() == 1 && hrop.OffReasonID() != null && hrop.StartDate() != '' && hrop.EndDate() != '' && hrop.StartDate() != null && hrop.EndDate() != null) {
    //  //if half day leave
    //  if (hrop.OffReasonID() == 12) {
    //    var roSystem = ClientData.ROSystemList.Find('SystemID', hrop.SystemID());
    //    var hoursDuration = roSystem.DefaultLeaveDuration;
    //    var sd = moment(hrop.StartDate());
    //    var ed = moment(hrop.EndDate());
    //    var MinuteDiff = 0;
    //    var dr = moment.range(sd, ed);
    //    MinuteDiff = dr.diff('minutes');
    //    if (MinuteDiff > (hoursDuration / 2)) {
    //      CtlError.AddError("Half Day Leave Duration cannot be more than " + (hoursDuration / 2).toString() + " hours");
    //    }
    //  }
    //}

  },

  //dropdowns
  onOffReasonIDSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {

    }
    else {
      if (businessObject.StartDate()) {
        var currentSDate = new Date(businessObject.StartDate())
        var newSD = new Date(new Date(currentSDate.getFullYear(), currentSDate.getMonth(), currentSDate.getDate(), 0, 0, 0, 0).setMinutes(selectedItem.DefaultStartTimeMinutes))
        businessObject.StartDate(newSD)
      }
      if (businessObject.EndDate()) {
        var currentEDate = new Date(businessObject.EndDate())
        var newED = new Date(new Date(currentEDate.getFullYear(), currentEDate.getMonth(), currentEDate.getDate(), 0, 0, 0, 0).setMinutes(selectedItem.DefaultEndTimeMinutes))
        businessObject.EndDate(newED)
      }
    }
  },
  triggerOffReasonIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setOffReasonIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterOffReasonIDRefreshAjax: function (args) {

  },

  //methods
  CalculateDefaultLeaveEndTime: function (self) {
    //var sysID = self.SystemID()
    //if (sysID == 1 || sysID == 2) {

    //} else if (sysID == 4) {

    //} else {

    //}
    //if (self.IsNew() && self.OffReasonID() && self.StartDate()) {
    //  //var roSystem = ClientData.ROSystemList.Find('SystemID', self.SystemID());
    //  //var roSystemProductionArea;
    //  //ClientData.ROSystemProductionAreaList.Iterate(function (itm, ind) {
    //  //  if (itm.SystemID == self.SystemID() && itm.ProductionAreaID == self.ProductionAreaID()) {
    //  //    roSystemProductionArea = itm;
    //  //  }
    //  //});
    //  //if (roSystemProductionArea) {
    //  //  var ed = new moment(self.StartDate());
    //  //  var hoursToAdd = roSystemProductionArea.DefaultLeaveDuration;
    //  //  //if Half-Day Leave
    //  //  if (self.OffReasonID() == 12) {
    //  //    hoursToAdd = (hoursToAdd / 2);
    //  //  };
    //  //  var newED = ed.add(hoursToAdd, 'hours').toDate();
    //  //  self.EndDate(newED);
    //  //};
    //}
  },
  SetAuthorisedDetails: function (self) {
    var ad = new Date();
    if (self.AuthorisedID() == 1) {
      self.AuthorisedByName("")
      self.AuthorisedBy(null)
      self.AuthorisedDateString("")

    } else if (self.AuthorisedID() == 2) {
      self.AuthorisedBy(ViewModel.CurrentUserID())
      self.AuthorisedByName(ViewModel.CurrentUserName())
      self.AuthorisedDateString(ad.format('ddd dd MMM yyyy HH:mm'))

    } else if (self.AuthorisedID() == 3) {
      self.AuthorisedBy(ViewModel.CurrentUserID())
      self.AuthorisedByName(ViewModel.CurrentUserName())
      self.AuthorisedDateString(ad.format('ddd dd MMM yyyy HH:mm'))
    }
  },
  HumanResourceOffPeriodBOToString: function (self) {
    var OffReason = ClientData.ROOffReasonList.Find('OffReasonID', self.OffReasonID());
    var Detail = self.Detail();
    var StartDate = (self.StartDate() == undefined) ? '' : ' - Start Date: ' + self.StartDate().format('dd-MMM-yy');
    var EndDate = (self.EndDate() == undefined) ? '' : ' - End Date: ' + self.EndDate().format('dd-MMM-yy');
    var ofr = (OffReason == undefined) ? '' : ' - Off Reason: ' + OffReason.OffReason
    var d = (Detail == undefined) ? '' : '- Detail: ' + Detail;
    return ofr + d + StartDate;
  },
  pending: function (offP) {
    offP.AuthorisedID(1)
    this.SetAuthorisedDetails(offP)
  },
  pendingCss: function (offP) { return (offP.AuthorisedID() == 1 ? 'fa fa-check-square-o' : 'fa fa-minus') },
  pendingButtonCss: function (offP) { return (offP.AuthorisedID() == 1 ? 'btn btn-sm btn-warning' : 'btn btn-sm btn-default') },
  authorise: function (offP) {
    offP.AuthorisedID(2)
    this.SetAuthorisedDetails(offP)
  },
  authoriseCss: function (offP) { return (offP.AuthorisedID() == 2 ? 'fa fa-check-square-o' : 'fa fa-minus') },
  authoriseButtonCss: function (offP) { return (offP.AuthorisedID() == 2 ? 'btn btn-sm btn-success' : 'btn btn-sm btn-default') },
  rejected: function (offP) {
    offP.AuthorisedID(3)
    this.SetAuthorisedDetails(offP)
  },
  rejectedCss: function (offP) { return (offP.AuthorisedID() == 3 ? 'fa fa-check-square-o' : 'fa fa-minus') },
  rejectedButtonCss: function (offP) { return (offP.AuthorisedID() == 3 ? 'btn btn-sm btn-danger' : 'btn btn-sm btn-default') },
  updateBooking: function (offP) {
    if ((offP.IsNew() || offP.IsDirty())
      && (offP.StartDate() != null)
      && (offP.EndDate() != null)
      && (offP.OffReasonID() != null)
      && !offP.IsProcessing()) {
      offP.IsProcessing(true)
      //update the booking
      this.updateOffPeriodBooking(offP)
      offP.IsProcessing(false)
    }
  },
  updateOffPeriodBooking: function (offP) {
    ViewModel.CallServerMethod("GenerateOffPeriod",
      { offPeriod: offP.Serialise() },
      function (response) {
        if (response.Success) {
          if (!offP.SetupCompleted()) {
            offP.StartDate(response.Data.StartDate)
            offP.EndDate(response.Data.EndDate)
          }

          while (offP.HumanResourceOffPeriodDayList().length > 0) {
            offP.HumanResourceOffPeriodDayList.DeleteAll()
          }

          response.Data.HumanResourceOffPeriodDayList.Iterate(function (itm, indx) {
            var newDay = offP.HumanResourceOffPeriodDayList.AddNew()
            newDay.TimesheetDate(itm.TimesheetDate)
            newDay.StartDateTime(itm.StartDateTime)
            newDay.EndDateTime(itm.EndDateTime)
          })

          offP.SetupCompleted(true)
          offP.IsProcessing(false)
        }
        else {
          OBMisc.Notifications.GritterError("Error Generating Days", response.ErrorText, 1000)
          offP.IsProcessing(false)
        }
      })
  },
  clashHtml: function (data) {
    if (data.ROOffPeriodClashList().length > 0) {
      return "<fa class='fa fa-exclamation-circle'></fa> <strong>" + data.ROOffPeriodClashList().length.toString() + " clashes </strong>"
    }
    else {
      return "<fa class='fa fa-check-square-o'></fa> <strong>" + data.ROOffPeriodClashList().length.toString() + " clashes </strong>"
    }
  },

  getOtherDisciplinesOnLeave: function (offP) {
    if (offP.HumanResourceID() && offP.StartDate() && offP.EndDate()) {
      var s = new Date(offP.StartDate()).format('dd MMM yyyy')
      var e = new Date(offP.EndDate()).format('dd MMM yyyy')
      Singular.GetDataStateless("OBLib.HR.ReadOnly.ROOtherDisciplinesOnLeaveList", {
        HumanResourceID: offP.HumanResourceID(),
        StartDate: s,
        EndDate: e
      }, function (response) {
        if (response.Success) {
          //OBMisc.Notifications.GritterSuccess("Saved", 'duplicates have been updated successfully', 1000)
          KOFormatter.Deserialise(response.Data, offP.ROOtherDisciplinesOnLeaveList)
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 2000)
        }
      })
    }
    else {
      offP.ROOtherDisciplinesOnLeaveList([])
    }
  },
  getOffPeriodClashes: function (offP) {
    if (offP.HumanResourceID() && offP.StartDate() && offP.EndDate()) {
      var s = new Date(offP.StartDate()).format('dd MMM yyyy HH:mm')
      var e = new Date(offP.EndDate()).format('dd MMM yyyy HH:mm')
      Singular.GetDataStateless("OBLib.HR.ReadOnly.ROOffPeriodClashList", {
        HumanResourceID: offP.HumanResourceID(),
        HumanResourceOffPeriodID: offP.HumanResourceOffPeriodID(),
        StartDateTime: s,
        EndDateTime: e
      }, function (response) {
        if (response.Success) {
          //OBMisc.Notifications.GritterSuccess("Saved", 'duplicates have been updated successfully', 1000)
          KOFormatter.Deserialise(response.Data, offP.ROOffPeriodClashList)
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 2000)
        }
      })
    }
    else {
      offP.ROOffPeriodClashList([])
    }
  },
  StartBeforeEndValid: function (Value, Rule, CtlError) {
    var Booking = CtlError.Object;
    if (Booking.EndDate() != undefined && Booking.StartDate() != undefined) {
      var d1 = new Date(Booking.StartDate());
      var d2 = new Date(Booking.EndDate());
      if (d2.getTime() < d1.getTime()) {
        CtlError.AddError("Start date must be before end date");
      }
    }
  },
  modalCss: function (self) {
    if (self && self.SetupCompleted()) {
      return "modal-dialog modal-md"
    }
    else {
      return "modal-dialog modal-xs"
    }
  }

};

HumanResourceOffPeriodDayBO = {
  TimesheetDateTimeSet: function (day) {

  },
  StartDateTimeSet: function (day) {
    day.TimesheetDate(day.StartDateTime())
  },
  EndDateTimeSet: function (day) {

  }
}

ProductionHumanResourceTBCBO = {
  TBCSet: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.TBCInd()) {
      ProductionHumanResourceTBC.IsAvailable(null);
    }
  },
  IsAvailableSet: function (ProductionHumanResourceTBC) {
  },

  SetAvailable: function (ProductionHumanResourceTBC) {
    ProductionHumanResourceTBC.TBCInd(false);
    ProductionHumanResourceTBC.IsAvailable(true);
  },
  IsAvailableText: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.IsAvailable()) {
      return "Yes"
    } else if (ProductionHumanResourceTBC.IsAvailable() == false) {
      return "Yes"
    } else if (ProductionHumanResourceTBC.IsAvailable() == null) {
      return "Yes"
    }
  },
  IsAvailableIcon: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.IsAvailable()) {
      return "fa fa-check-square-o"
    } else if (ProductionHumanResourceTBC.IsAvailable() == false) {
      return "fa fa-minus"
    } else if (ProductionHumanResourceTBC.IsAvailable() == null) {
      return "fa fa-minus"
    }
  },
  IsAvailableButtonCss: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.IsAvailable()) {
      return "btn btn-xs btn-success"
    } else if (ProductionHumanResourceTBC.IsAvailable() == false) {
      return "btn btn-xs btn-default"
    } else if (ProductionHumanResourceTBC.IsAvailable() == null) {
      return "btn btn-xs btn-default"
    }
  },

  SetNotAvailable: function (ProductionHumanResourceTBC) {
    ProductionHumanResourceTBC.TBCInd(false);
    ProductionHumanResourceTBC.IsAvailable(false);
  },
  IsNotAvailableText: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.IsAvailable()) {
      return "No"
    } else if (ProductionHumanResourceTBC.IsAvailable() == false) {
      return "No"
    } else if (ProductionHumanResourceTBC.IsAvailable() == null) {
      return "No"
    }
  },
  IsNotAvailableIcon: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.IsAvailable()) {
      return "fa fa-minus"
    } else if (ProductionHumanResourceTBC.IsAvailable() == false) {
      return "fa fa-check-square-o"
    } else if (ProductionHumanResourceTBC.IsAvailable() == null) {
      return "fa fa-minus"
    }
  },
  IsNotAvailableButtonCss: function (ProductionHumanResourceTBC) {
    if (ProductionHumanResourceTBC.IsAvailable()) {
      return "btn btn-xs btn-default"
    } else if (ProductionHumanResourceTBC.IsAvailable() == false) {
      return "btn btn-xs btn-danger"
    } else if (ProductionHumanResourceTBC.IsAvailable() == null) {
      return "btn btn-xs btn-default"
    }
  }
}

ROHRDuplicateBO = {
  LinkValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.Link()) {
      if ((!CtlError.Object.SystemID() || !CtlError.Object.ProductionAreaID()) && !CtlError.Object.IsDuplicate()) {
        CtlError.AddError("Sub-Dept and Area area are required for linking")
      }
    }
  },
  IgnoreDuplicateValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.OptionSelected()) {
      CtlError.AddError("Select an action")
    }
  },
  LinkSet: function (ROHRDuplicate) {
    if (ROHRDuplicate.Link() || ROHRDuplicate.IsDuplicate() || ROHRDuplicate.IsNotSameHR()) {
      ROHRDuplicate.OptionSelected(true)
    }
  },
  IsDuplicateSet: function (ROHRDuplicate) {
    if (ROHRDuplicate.Link() || ROHRDuplicate.IsDuplicate() || ROHRDuplicate.IsNotSameHR()) {
      ROHRDuplicate.OptionSelected(true)
    }
  },
  OptionSelectedSet: function (ROHRDuplicate) {
    //ROHRDuplicate.Link(!ROHRDuplicate.IsDuplicate())
  },
  IsNotSameHRSet: function (ROHRDuplicate) {
    if (ROHRDuplicate.Link() || ROHRDuplicate.IsDuplicate() || ROHRDuplicate.IsNotSameHR()) {
      ROHRDuplicate.OptionSelected(true)
    }
  }
}

SkillRateUpdateBO = {
  CanEdit: function (Object, FieldName) {
    switch (FieldName) {
      case "EnableAuthoriseButton":
        if (ViewModel.OTPString()) {
          if (ViewModel.OTPString().trim().length > 0) {
            return true;
          } else {
            return false;
          }
        }
        break;
      case "EnableBulkEdit":
        var oneDirty = false;
        ViewModel.SkillRateUpdateList().Iterate(function (itm, ind) {
          if (itm.IsDirty()) {
            oneDirty = true;
            return
          }
        });
        if (ViewModel.IsValid() && !oneDirty && ViewModel.SkillRateUpdateList().length > 0) {
          return true;
        } else {
          return false;
        }
        break;
      case "EnableSave":
        if (ViewModel.IsValid() && ViewModel.SkillRateUpdateList().length > 0) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        break;
    }
  },
  SkillRateUpdateBOToString: function (self) {
    var HumanResource = self.HumanResource();
    return 'Skill Rate: ' + HumanResource;
  }
}

HumanResourceUnAvailabilityTemplateBO = {
  HumanResourceUnAvailabilityTemplateBOToString: function (self) { return "" },
  StartDateSet: function (self) {
    self.HumanResourceUnAvailabilityList().Iterate(function (itm, indx) {
      itm.StartDate(self.StartDate())
    })
  },
  EndDateSet: function (self) {
    self.HumanResourceUnAvailabilityList().Iterate(function (itm, indx) {
      itm.EndDate(self.EndDate())
    })
  },
  DetailSet: function (self) {
    self.HumanResourceUnAvailabilityList().Iterate(function (itm, indx) {
      itm.Detail(self.Detail())
    })
  },
  saveItem: function (item, options) {
    ViewModel.CallServerMethod("SaveHumanResourceUnAvailabilityTemplate",
      { HumanResourceUnAvailabilityTemplate: item.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
          OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
        }
        else {
          if (options.onFail) { options.onFail(response) }
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
        }
      })
  }
}

HumanResourceUnAvailabilityBO = {
  HumanResourceUnAvailabilityBOToString: function (self) { return "" },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  DetailSet: function (self) { },
  remove: function (parent, obj) {
    parent.HumanResourceUnAvailabilityList.RemoveNoCheck(obj)
  }
}

HumanResourceTimesheetBO = {
  //other
  HumanResourceTimesheetBOToString: function (self) {
    if (self.IsNew()) {
      return 'new timesheet'
    } else {
      return 'timesheet'
    }
  },
  getTimesheet: function (HumanResourceTimesheetID, onSuccess, onFail) {
    ViewModel.CallServerMethod("GetHumanResourceTimesheet", {
      HumanResourceTimesheetID: HumanResourceTimesheetID
    },
      function (response) {
        if (response.Success) {
          onSuccess(response)
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 1000)
          onFail(response)
        }
      })
  },
  save: function (timesheet, options) {
    ViewModel.CallServerMethod("SaveHumanResourceTimesheet",
      { HumanResourceTimesheet: timesheet.Serialise() },
      function (response) {
        if (response.Success) {
          OBMisc.Knockout.updateObservable(timesheet, response.Data)
          OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 250)
          if (options.onSaveSuccess) { options.onSaveSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Error Saving", response.ErrorText, 1000)
          if (options.onSaveFailed) { options.onSaveFailed(response) }
        }
      })
  },
  getClashes: function (self) {
    if (!self.IsProcessing() && self.StartDate() && self.EndDate()) {
      self.IsProcessing(true)
      Singular.GetDataStateless("OBLib.HR.Timesheets.ReadOnly.ROHumanResourceTimesheetDayList, OBLib", {
        HumanResourceID: self.HumanResourceID(),
        StartDate: new Date(self.StartDate()).format('dd MMM yyyy'),
        EndDate: new Date(self.EndDate()).format('dd MMM yyyy'),
        ClashHumanResourceTimesheetID: self.HumanResourceTimesheetID(),
        ClashCheck: true
      }, function (response) {
        if (response.Success) {
          self.ClashingDaysCount(response.Data.length)
        }
        else {
          OBMisc.Notifications.GritterError("Error Checking Clashes", response.ErrorText, 1000)
        }
        self.IsProcessing(false)
      })
    }
  },
  getDays: function (self) {

  },

  //set expressions
  StartDateSet: function (self) {
    HumanResourceTimesheetBO.getClashes(self)
  },
  EndDateSet: function (self) {
    HumanResourceTimesheetBO.getClashes(self)
  },

  //Validation Rules
  ClashingDaysCountValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.ClashingDaysCount() > 0) {
      CtlError.AddError('This timesheet has days which overlap with another timesheet')
    }
  },

  //dropdowns
  TimesheetRequirementIDSet: function (self) {
  },
  triggerTimesheetRequirementIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setTimesheetRequirementIDCriteriaBeforeRefresh: function (args) {
    args.Data.IsClosed = false
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterTimesheetRequirementIDRefreshAjax: function (args) {

  },
  onTimesheetRequirementIDSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.RequiredHours(selectedItem.RequiredHours)
      businessObject.RequiredShifts(selectedItem.RequiredShifts)
      businessObject.StartDate(selectedItem.StartDate)
      businessObject.EndDate(selectedItem.EndDate)
      businessObject.TimesheetRequirement(selectedItem.TimesheetRequirement)
    }
    else {
      businessObject.RequiredHours(0)
      businessObject.RequiredShifts(0)
      businessObject.StartDate(null)
      businessObject.EndDate(null)
      businessObject.TimesheetRequirement("")
    }
  }
}

CrewScheduleDetailBO = {
  StartDateTimeSet: function (self) { },
  EndDateTimeSet: function (self) { }
}

HumanResourceAddToBookingBO = {
  //cans
  canEdit: function (fieldName, self) {
    switch (fieldName) {
      case 'HumanResourceSystemID':
        return self.HumanResourceID()
        break;
      case 'StartDate':
        return self.HumanResourceSystemID()
        break;
      case 'EndDate':
        return (self.HumanResourceSystemID() && self.StartDate())
        break;
      case 'ProductionSystemAreaID':
        return (self.HumanResourceSystemID() && self.StartDate() && self.EndDate())
        break;
      case 'ProductionHumanResourceID':
        return (self.HumanResourceSystemID() && self.StartDate() && self.EndDate() && self.ProductionSystemAreaID())
        break;
    }
  },

  //dropdowns
  //psa
  triggerProductionSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDate = args.Object.StartDate()
    args.Data.EndDate = args.Object.EndDate()
    args.Data.Scenario = "AssignOB"
  },
  afterProductionSystemAreaRefreshAjax: function (args) {

  },
  onProductionSystemAreaIDSelected: function (item, businessObject) {
  },

  //phr
  triggerProductionHumanResourceIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionHumanResourceIDCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionSystemAreaID = args.Object.ProductionSystemAreaID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterProductionHumanResourceIDRefreshAjax: function (args) {

  },
  onProductionHumanResourceIDSelected: function (item, businessObject) {

  },

  //hr
  triggerHumanResourceSystemIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setHumanResourceSystemIDCriteriaBeforeRefresh: function (args) {
    args.Data.HumanResourceID = args.Object.HumanResourceID()
    args.Data.ProductionAreaIDs = [1]
  },
  afterHumanResourceSystemIDRefreshAjax: function (args) {

  },
  onHumanResourceSystemIDSelected: function (item, businessObject) {
    if (item) {
      businessObject.SystemID(item.SystemID)
      businessObject.ProductionAreaID(item.ProductionAreaID)
    } else {
      businessObject.SystemID(null)
      businessObject.ProductionAreaID(null)
    }
  },

  //set expressions
  HumanResourceIDSet: function (self) {
    //HumanResourceAddToBookingBO.generateTempBooking(self)
    self.OBBooking.Set(null)
  },
  SystemIDSet: function (self) {
    //HumanResourceAddToBookingBO.generateTempBooking(self)
    self.OBBooking.Set(null)
  },
  ProductionAreaIDSet: function (self) {
    //HumanResourceAddToBookingBO.generateTempBooking(self)
    self.OBBooking.Set(null)
  },
  DisciplineIDSet: function (self) {
    //HumanResourceAddToBookingBO.generateTempBooking(self)
    self.OBBooking.Set(null)
  },
  HumanResourceSystemIDSet: function (self) {
    //HumanResourceAddToBookingBO.generateTempBooking(self)
    self.OBBooking.Set(null)
  },
  ProductionSystemAreaIDSet: function (self) {
    self.OBBooking.Set(null)
  },
  ProductionHumanResourceIDSet: function (self) {
    if (self.ProductionHumanResourceID()) {
      if (self.OBBooking()) {
        self.OBBooking().NewProductionHumanResourceID(self.ProductionHumanResourceID())
      }
    }
    else {
      if (self.OBBooking()) {
        self.OBBooking().NewProductionHumanResourceID(null)
      }
    }
    HumanResourceAddToBookingBO.generateTempBooking(self)
  },
  StartDateSet: function (self) {
  },
  EndDateSet: function (self) {
  },

  //other
  generateTempBooking: function (self) {
    if (self.HumanResourceID() && self.SystemID() && self.ProductionAreaID()
      && self.ProductionSystemAreaID() && self.ProductionHumanResourceID()) {
      if (self.ProductionAreaID() == 1) {
        this.generateOBBooking(self)
      }
    }
  },
  generateOBBooking: function (self) {
    Singular.GetDataStatelessPromise("OBLib.OutsideBroadcast.ProductionHROBSimpleList, OBLib",
      {
        HumanResourceID: self.HumanResourceID(),
        ProductionSystemAreaID: self.ProductionSystemAreaID(),
        ProductionHumanResourceID: self.ProductionHumanResourceID()
      })
      .then(data => {
        self.OBBooking.Set(data[0])
        self.OBBooking().NewProductionHumanResourceID(self.ProductionHumanResourceID())
        ProductionHROBSimpleBO.getClashes(self.OBBooking())
      })
  },
  showModal: function () {
    $("#ProductionHROBSimpleModal").off("shown.bs.modal")
    $("#ProductionHROBSimpleModal").on("shown.bs.modal", function () {
      Singular.Validation.CheckRules(ViewModel.HumanResourceAddToBooking())
    })
    $("#ProductionHROBSimpleModal").modal()
  },
  hideModal: function () {
    $("#ProductionHROBSimpleModal").modal('hide')
    ViewModel.HumanResourceAddToBooking(null)
  },
  createNew: function () {
    ViewModel.HumanResourceAddToBooking(null)
    ViewModel.HumanResourceAddToBooking(new HumanResourceAddToBookingObject())
    ViewModel.HumanResourceAddToBooking().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.HumanResourceAddToBooking().HumanResourceName(ViewModel.CurrentHumanResource().Firstname())
    Singular.Validation.CheckRules(ViewModel.HumanResourceAddToBooking())
  }
}

ProductionHROBSimpleBO = {
  //set
  StartDateTimeSet: function (self) {
    ProductionHROBSimpleBO.getClashes(self)
  },
  EndDateTimeSet: function (self) {
    ProductionHROBSimpleBO.getClashes(self)
  },

  //rules
  ClashListValid: function (Valid, Rule, CtlError) {
    if (CtlError.Object.ClashList().length > 0) {
      CtlError.AddError("There are clashes with this booking")
    }
  },

  //other
  getClashes: function (self) {
    let sd = moment(self.StartDateTime()).subtract(0, 'days'),
      ed = moment(self.EndDateTime())
    ResourceBookingBO.getClashes([{
      Guid: self.Guid(),
      ResourceBookingID: self.ResourceBookingID(),
      ResourceID: self.ResourceID(),
      ResourceBookingTypeID: 1,
      StartDateTime: sd.toDate().format('dd MMM yyyy HH:mm'),
      EndDateTime: ed.toDate().format('dd MMM yyyy HH:mm'),
      ProductionHRID: self.ProductionHRID(),
      ProductionSystemAreaID: self.ProductionSystemAreaID()
    }],
      {
        onSuccess: function (data) {
          self.ClashList.Set(data)
          Singular.Validation.CheckRules(self)
        },
        onFail: function (errorText) {

        }
      })
  },
  saveItem: function (self) {
    let serialised = self.Serialise()
    ViewModel.CallServerMethodPromise("SaveProductionHROBSimple", { ProductionHROBSimple: serialised })
      .then(data => {
        KOFormatter.Deserialise(data, self)
        OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
        HumanResourceAddToBookingBO.hideModal()
      }),
      errorText => { OBMisc.Notifications.GritterError("Save Failed", errorText, 1000) }
  }
}

ProductionHROBSimpleDetailBO = {
  StartDateTimeSet: function (self) {
    let parent = self.GetParent()
    let startTimes = parent.ProductionHROBSimpleDetailList().map(item => { return new Date(item.StartDateTime()).getTime() })
    let endTimes = parent.ProductionHROBSimpleDetailList().map(item => { return new Date(item.EndDateTime()).getTime() })
    let minStartTime = Math.min.apply(null, startTimes)
    let maxEndTime = Math.max.apply(null, endTimes)
    minStartTime = new Date(minStartTime)
    maxEndTime = new Date(maxEndTime)
    parent.StartDateTime(minStartTime)
    parent.EndDateTime(maxEndTime)
  },
  EndDateTimeSet: function (self) {
    let parent = self.GetParent()
    let startTimes = parent.ProductionHROBSimpleDetailList().map(item => { return new Date(item.StartDateTime()).getTime() })
    let endTimes = parent.ProductionHROBSimpleDetailList().map(item => { return new Date(item.EndDateTime()).getTime() })
    let minStartTime = Math.min.apply(null, startTimes)
    let maxEndTime = Math.max.apply(null, endTimes)
    minStartTime = new Date(minStartTime)
    maxEndTime = new Date(maxEndTime)
    parent.StartDateTime(minStartTime)
    parent.EndDateTime(maxEndTime)
  },
  onDetailRemoved: function (self) {
    let parent = self.GetParent()
    let startTimes = parent.ProductionHROBSimpleDetailList().map(item => { return new Date(item.StartDateTime()).getTime() })
    let endTimes = parent.ProductionHROBSimpleDetailList().map(item => { return new Date(item.EndDateTime()).getTime() })
    let minStartTime = Math.min.apply(null, startTimes)
    let maxEndTime = Math.max.apply(null, endTimes)
    minStartTime = new Date(minStartTime)
    maxEndTime = new Date(maxEndTime)
    parent.StartDateTime(minStartTime)
    parent.EndDateTime(maxEndTime)
    ProductionHROBSimpleBO.getClashes(parent)
  }
}

//#endregion Editable

//#region ReadOnly
ROHumanResourceBO = {
  ContractTypeHtml: function (ROHumanResource) {
    var html = '';
    switch (ROHumanResource.ContractTypeID()) {
      case 2:
      case 3:
        html = '<label class="label label-success">' + ROHumanResource.ContractType() + '</label>';
        break;
      case 7:
        html = '<label class="label label-primary">' + ROHumanResource.ContractType() + '</label>';
        break;
      case 6:
      case 8:
        html = '<label class="label label-danger">' + ROHumanResource.ContractType() + '</label>';
        break;
    }
    return html;
  }
};

ROHumanResourceDetailBO = {
  //data access
  getItem: function (options) {
    Singular.GetDataStateless("OBLib.HR.ReadOnly.ROHumanResourceDetailList", options.criteria,
      function (response) {
        if (response.Success) {
          if (response.Data.length == 0) {
            OBMisc.Notifications.GritterError('HR details could not be retrieved', 'Please view their profile in the Human Resource Screen for any errors.', response.ErrorText, 1000)
          }
          else if (response.Data.length == 1) {
            if (options.onSuccess) { options.onSuccess.call(response.Data[0], response) }
          }
          else {
            OBMisc.Notifications.GritterError('HR details could not be retrieved', 'Please view their profile in the Human Resource Screen for any errors.', 1000)
          }
        }
        else {
          OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        }
      })
  }
};

ROHumanResourceUtilisationBO = {
  GetHoursWorkedCss: function (ROHumanResourceUtilisation) {
    var css = "progress-bar ";
    var PercWorked = ROHumanResourceUtilisation.TimesheetUtilPerc();
    if (PercWorked < 80) {
      css += "progress-bar-success";
    } else if (PercWorked < 90) {
      css += "progress-bar-info";
    } else if (PercWorked < 100) {
      css += "progress-bar-warning";
    } else {
      css += "progress-bar-danger";
    }
    return css;
  },
  GetHoursWorkedStyle: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.TimesheetUtilPerc();
    if (PercWorked > 100) {
      PercWorked = 100;
    }
    return { width: PercWorked.toString() + '%' };
  },
  GetHoursWorkedHtml: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.TimesheetUtilPerc();
    return PercWorked.toString() + '%';
  },
  GetDaysWorkedCss: function (ROHumanResourceUtilisation) {
    var css = "progress-bar ";
    var PercWorked = ROHumanResourceUtilisation.CSDUtilPerc();
    if (PercWorked < 80) {
      css += "progress-bar-success";
    } else if (PercWorked < 90) {
      css += "progress-bar-info";
    } else if (PercWorked < 100) {
      css += "progress-bar-warning";
    } else {
      css += "progress-bar-danger";
    }
    return css;
  },
  GetDaysWorkedStyle: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.CSDUtilPerc();
    if (PercWorked > 100) {
      PercWorked = 100;
    }
    return { width: PercWorked.toString() + '%' };
  },
  GetDaysWorkedHtml: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.CSDUtilPerc();
    return PercWorked.toString() + '%';
    return "";
  },
  GetShiftsWorkedCss: function (ROHumanResourceUtilisation) {
    var css = "progress-bar ";
    var PercWorked = ROHumanResourceUtilisation.ShiftUtilPerc();
    if (PercWorked < 80) {
      css += "progress-bar-success";
    } else if (PercWorked < 90) {
      css += "progress-bar-info";
    } else if (PercWorked < 100) {
      css += "progress-bar-warning";
    } else {
      css += "progress-bar-danger";
    }
    return css;
  },
  GetShiftsWorkedStyle: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.ShiftUtilPerc();
    if (PercWorked > 100) {
      PercWorked = 100;
    }
    return { width: PercWorked.toString() + '%' };
  },
  GetShiftsWorkedHtml: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.ShiftUtilPerc();
    return PercWorked.toString() + '%';
    return "";
  },
  GetContractDaysWorkedCss: function (ROHumanResourceUtilisation) {
    var css = "progress-bar ";
    var PercWorked = ROHumanResourceUtilisation.ContractUtilPerc();
    if (PercWorked < 80) {
      css += "progress-bar-success";
    } else if (PercWorked < 90) {
      css += "progress-bar-info";
    } else if (PercWorked < 100) {
      css += "progress-bar-warning";
    } else {
      css += "progress-bar-danger";
    }
    return css;
  },
  GetContractDaysWorkedStyle: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.ContractUtilPerc();
    if (PercWorked > 100) {
      PercWorked = 100;
    }
    return { width: PercWorked.toString() + '%' };
  },
  GetContractDaysWorkedHtml: function (ROHumanResourceUtilisation) {
    var PercWorked = ROHumanResourceUtilisation.ContractUtilPerc();
    return PercWorked.toString() + '%';
    return "";
  }
};

ROHumanResourcePagedListBO = {
  Criteria: {
    DisciplineIDSet: function (critInstance) {
      console.log(critInstance);
    },
    KeywordUserProfileSet: function (critInstance) {
      console.log(critInstance);
    },
    KeywordSet: function (critInstance) {
      console.log(critInstance);
    }
  }
};

ROHumanResourceFindBO = {
  SelectedIndSet: function (self) {

  }
}

ROHumanResourcePagedListCriteriaBO = {
  KeywordSet: function (self) { },
  ContractTypeIDSet: function (self) { },
  DisciplineIDSet: function (self) { },
  FirstnameSet: function (self) { },
  SecondNameSet: function (self) { },
  PreferredNameSet: function (self) { },
  SurnameSet: function (self) { },
  SystemIDsXmlSet: function () { },
  ProductionAreaIDsXmlSet: function () { }
}

ROHumanResourceTimesheetPagedBO = {

}
//#endregion