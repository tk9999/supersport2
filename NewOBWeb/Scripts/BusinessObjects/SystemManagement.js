﻿TeamBO = {
  TeamNameValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.TeamName().trim() == "")
        CtlError.AddError("Team Name Required");
    }
  },
  SystemValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.SystemID() == undefined)
        CtlError.AddError("Sub-Dept. Required");
    }
  },
  ProductionAreaValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.SystemID() == undefined || Team.SystemID() == null)
        CtlError.AddError("Production Area Required");
    }
  },
  PatternValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.SystemAreaShiftPatternID() == undefined)
        CtlError.AddError("System Area Shift Pattern Required");
    }
  },
  TeamNumberValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.TeamNo() == undefined || Team.TeamNo() <= 0)
        CtlError.AddError("Team Number Required");
    }
  },
  DatesValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team.StartDate() != undefined && Team.EndDate() != undefined) {
      var sd = new Date(Team.StartDate());
      var ed = new Date(Team.EndDate());
      if (ed.getTime() < sd.getTime()) {
        CtlError.AddError("Start Date must be before End Date");
      }
    }
  },
  ShiftDurationValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.ShiftDuration() === 0) {
        CtlError.AddError("Shift Duration Required");
      }
    }
  },
  CanEdit: function (Team, FieldName) {
    switch (FieldName) {
      case 'TeamHR':
        if (ViewModel.EditableSystemTeamShift() != null/* && ViewModel.RemovingTeam() == false*/) {
          return true;
        }
        break;
      case 'AddBtn':
        if (ViewModel.EditableSystemTeamShift() != null) {
          return true;
        }
        break;
      case 'HasDiscipline':
        if (Team.DisciplineID() != undefined) {
          return true;
        }
        break;
      case 'DeleteTeam':
        if (Team.HROnTeam() == 0 && !Team.ShedulesGeneratedForTeam()) {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  },
  ToString: function (self) {
    var Name = (self.TeamName() == undefined || self.TeamName().trim() == "") ? '' : ' - Name: ' + self.TeamName();
    var SystemS = ClientData.ROSystemList.Find('SystemID', self.SystemID());
    var System = (SystemS == undefined) ? '' : ' - Sub-Dept: ' + SystemS.System;
    return 'Team ' + Name + System;
  }
}

TeamRequirementDetailBO = {
  GetScheduledHoursForPeriod: function (self) {
    var EmployeeHours = [];
    var Hours = 0;
    var TotalHours = 0;
    var HRID = 0;
    var CurrentHR = ViewModel.CurrentTeamRequirementDetailHumanresource();
    var CurrentHRList = ViewModel.TeamRequirementDetailHumanresourceList();
    if (self.YearGroupPeriodID()) {
      Singular.GetDataStateless('OBLib.Shifts.ICR.ReadOnly.ROHumanResourceGroupShiftList, OBLib', {
        HumanResourceID: null,
        StartDate: self.StartDate(),
        EndDate: self.EndDate(),
        SystemID: ViewModel.CurrentSystemID(),
        ProductionAreaID: ViewModel.CurrentProductionAreaID(),
        TeamID: self.GetParent().GetParent().SystemTeamID()
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.HumanResourceGroupShiftList);
          Singular.HideLoadingBar();
          for (var i = 0; i < ViewModel.HumanResourceGroupShiftList().length; i++) {
            for (var n = 0; n < ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList().length; n++) {
              HRID = ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].HumanResourceID();
              var sd = new Date(ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].StartDateTime());
              var ed = new Date(ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].EndDateTime());
              var diff = ed - sd;
              var hh = Math.floor(diff / 1000 / 60 / 60);
              Hours += hh;
            }
            CurrentHR.HumanResourceID(HRID);
            CurrentHR.HoursWorked(Hours);
            CurrentHRList.push(CurrentHR);
            EmployeeHours.push([HRID, Hours]);
            TotalHours += Hours;
            Hours = 0;
          }
          self.TotalScheduledHoursWorked(TotalHours);
          for (var p = 0; p < EmployeeHours.length; p++) {
            ViewModel.genericlist.push(EmployeeHours[p]);
          }
        }
      });
    }
    else {
      self.TotalScheduledHoursWorked(0);
    }
    TotalHours = 0;
  }
}

TeamShiftPatternBO = {
  //CheckforDuplicates: function (Value, Rule, CtlError) {
  //  var TeamShiftPattern = CtlError.Object;
  //  if (self) {
  //    var dups = [];
  //    if (ViewModel.TeamShiftPatternList().length > 1) {
  //      for (var i = 0; i < ViewModel.TeamShiftPatternList().length; i++) {
  //        if (Value == ViewModel.TeamShiftPatternList()[i].SystemTeamID())
  //          dups.push(ViewModel.TeamShiftPatternList()[i]);
  //        if (dups.length > 1) {//We Have a Duplicate Here
  //          var ts = $.grep(dups, function (h) {
  //            return h.SystemTeamID() == Value;
  //          }); //Array of Duplicate Teams Found
  //          if (ts.length > 0) {
  //            for (var p = 0; p < dups.length - 1; p++) {
  //              var anchor = dups[p];
  //              for (var j = 0; j < dups.length; j++) {
  //                var slider = dups[j];
  //                if (anchor.Guid() !== slider.Guid()) {
  //                  var startdate1 = new Date(anchor.StartDate());
  //                  var enddate1 = new Date(anchor.EndDate());
  //                  var startdate2 = new Date(slider.StartDate());
  //                  var enddate2 = new Date(slider.EndDate());
  //                  if ((startdate1 <= enddate2) && (enddate1 >= startdate2)) {
  //                    CtlError.AddError('This Team has already been selected within this date range');
  //                  }
  //                }
  //              }
  //            }
  //          }
  //        }
  //      }
  //    }
  //  }
  //},
  StartBeforeEnd: function (Value, Rule, CtlError) {
    var TeamShiftPattern = CtlError.Object;
    if (TeamShiftPattern.EndDate() != undefined && TeamShiftPattern.StartDate() != undefined) {
      var d1 = new Date(TeamShiftPattern.StartDate());
      var d2 = new Date(TeamShiftPattern.EndDate());
      if (d2.getTime() < d1.getTime()) {
        CtlError.AddError("Start date must be before end date");
      }
    }
  },
  HasPatternName: function (Value, Rule, CtlError) {
  },
  CanEdit: function (TeamShiftPattern, FieldName) {
    switch (FieldName) {
      case 'GenerateShifts':
        if (TeamShiftPattern.IsValid() /*&& !TeamShiftPattern.IsDirty()*/
          && ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().length > 0
          && TeamShiftPattern.SchedulesGenerated() == 0 && TeamShiftPattern.SystemAreaShiftPatternID() != undefined
          && !TeamShiftPattern.TeamDisciplineHumanResourceList.IsDirty()) {
          return true;
        } else
          return false;
        break;
      case 'SaveShifts':
        if (ViewModel.IsValid()) {
          return true;
        } else
          return false;
        break;
      case 'DeleteShifts':
        if (TeamShiftPattern.SchedulesGenerated() == 0 || TeamShiftPattern.IsNew()) {
          return true;
        } else
          return false;
        break;
      case 'AlreadyGenerated':
        if (TeamShiftPattern.SchedulesGenerated() == 0) {
          return true;
        } else {
          return false;
        }
        break;
      case 'ShiftDuration':
        if (TeamShiftPattern.SchedulesGenerated() != 0) {
          return false;
        } else {
          return true;
        }
        break;
      case 'PatternName':
        if (TeamShiftPattern.SchedulesGenerated() == 0) {
          return true;
        } else
          return false;
        break;
      case 'TableDisable':
        if (TeamShiftPattern.GetParent().SchedulesGenerated() != 0) {
          return false;
        } else {
          return true;
        }
        break;
      case 'ResetDisable':
        if (TeamShiftPattern.SchedulesGenerated() == 0 || TeamShiftPattern.IsNew()) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return true;
        break;
    }
  },
  FilterPatterns: function (List, obj) {
    var results = []
    List.Iterate(function (item, indx) {
      if (ViewModel.CurrentSystemID() == item.SystemID) {
        results.push(item)
      }
    })
    return results
  }
}

SystemAreaShiftPatternBO = {
  UpdateNoWeekDays: function (self) {
    self.SystemAreaShiftPatternWeekDayList.Set([]);
    for (var i = 0; i < self.ShiftDuration(); i++) {
      for (var k = 0; k < 7; k++) {
        var NewSPWD = self.SystemAreaShiftPatternWeekDayList.AddNew();
        NewSPWD.WeekNo(i + 1);
        NewSPWD.WeekDay(k + 1);
      }
    }
  },
  CanEdit: function (ShiftPattern, FieldName) {
    switch (FieldName) {
      case 'SaveShiftPatterns':
        if (ViewModel.IsValid() && ViewModel.SystemAreaShiftPatternList().length > 0) {
          return true;
        } else {
          return false;
        }
      case 'GenHRBtn':
        if (ViewModel.EditableSystemTeamShiftPattern().IsValid() &&
          (ViewModel.EditableSystemTeamShiftPattern() != null && ViewModel.EditableSystemTeamShiftPattern().IsValid()) &&
          ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().length > 0) {
          return true;
        }
        break;
      case 'ShiftPanelPattern':
        if (ViewModel.EditableShiftPattern() != null) {
          return true;
        }
        break;
      case 'AddNewSASP':
        if ((ShiftPattern.SystemID() != undefined || ShiftPattern.SystemID() != 0)
          || (ShiftPattern.ProductionAreaID() != undefined || ShiftPattern.ProductionAreaID() != 0)) {
          return true;
        }
        else
          return false;
      case 'DeleteSASP':
        if (!ShiftPattern.HRUsingPatternInd()) {
          return true;
        }
        else
          return false;
        break;
      case 'DeleteSASPWD':
        if (!ShiftPattern.GetParent().HRUsingPatternInd()) {
          return true;
        }
        else
          return false;
        break;
      case 'AddSASPWD':
        if (!ShiftPattern.HRUsingPatternInd()) {
          return true;
        }
        else
          return false;
        break;
      case 'ProductionAreaID':
        if (ViewModel.IsValid()) {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  },
  ShiftDurationValid: function (Value, Rule, CtlError) {
    var SystemAreaShiftPattern = CtlError.Object;
    if (SystemAreaShiftPattern.ShiftDuration() == undefined || SystemAreaShiftPattern.ShiftDuration() == 0) {
      CtlError.AddError("Shift Duration Required");
    }
  },
  SystemAreaShiftPatternBOToString: function (self) {
    var PatternName = (self.PatternName() == undefined || self.PatternName().trim().toString() == '') ? '' : ' - Pattern Name: ' + self.PatternName();
    return 'System Area Shift Pattern: ' + PatternName;
  },
  AllowedShiftDurations: function (List, pattern) {
    var Allowed = [];
    if (List) {
      List.Iterate(function (itm, ind) {
        Allowed.push({ ShiftDuration: itm.ShiftDuration, ShiftDurationName: itm.ShiftDurationName });
      });
    }
    return Allowed;
  },
  beforeRoomRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  triggerAutoPopulate: function () {
    args.AutoPopulate = true
  },
  afterPatternRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onPatternSelected: function (selectedItem, businessObject) {

  }
}

SystemAreaShiftPatternWeekDayBO = {
  SetStartEndTime: function (self) {
    if (self.OffDay() == true) {
      var sd = new Date();
      var ed = new Date();
      sd.setHours(0, 1, 0);
      ed.setHours(23, 59, 0);
      self.StartTime(sd.format('HH:mm'));
      self.EndTime(ed.format('HH:mm'));
    }
  },
  SetStartEndTimeFromShiftType: function (self) {
    if (self.ShiftTypeID()) {
      if (ViewModel.CurrentProductionAreaID() != 2) {
        var types = ClientData.ROSystemAreaShiftTypeList.Filter('ShiftTypeID', self.ShiftTypeID());
        var st = types.Find('ProductionAreaID', self.GetParent().ProductionAreaID());
        if (st != undefined) {
          var mStartTime = moment.duration(st.StartTime);
          var sd = moment({ years: 2000, months: 1, date: 1, hours: mStartTime.hours(), minutes: mStartTime.minutes(), seconds: 0, milliseconds: 0 }).format("HH:mm");
          var mEndTime = moment.duration(st.EndTime);
          var ed = moment({ years: 2000, months: 1, date: 1, hours: mEndTime.hours(), minutes: mEndTime.minutes(), seconds: 0, milliseconds: 0 }).format("HH:mm");
          //self.StartTime(sd._d);
          //self.EndTime(ed._d);
          self.StartTime(sd);
          self.EndTime(ed);
          if (st.ShiftTypeID == 3) {
            self.OffDay(true);
          }
          else {
            self.OffDay(false);
          }
        }
      }
      else {
        self.StartTime(null);
        self.EndTime(null);
      }
    }
  },
  SystemAreaShiftPatternWeekDayBOToString: function (self) {
    var WeekNo = (self.WeekNo() == undefined || self.WeekNo() == 0) ? '' : ' - Week Number: ' + self.WeekNo();
    var WeekDay = (self.WeekDay() == undefined || self.WeekDay() == 0) ? '' : ' - Week Day: ' + self.WeekDay();
    return 'System Area Shift Pattern Week Day: ' + WeekNo + WeekDay;
  },
  GetAllowedSystemAreaShiftType: function (List, weekday) {
    var allowedShiftTypes = []
    var pattern = weekday.GetParent()
    List.Iterate(function (roShiftType, Index) {
      if (roShiftType.SystemID == pattern.SystemID()
        && roShiftType.ProductionAreaID == pattern.ProductionAreaID()) {
        allowedShiftTypes.push(roShiftType);
      }
    })
    return allowedShiftTypes
  },
  AllowedWeekDays: function (List, weekday) {
    var Allowed = []
    List.Iterate(function (itm, ind) {
      Allowed.push({ WeekDay: itm.WeekDay, WeekDayName: itm.WeekDayName })
    })
    return Allowed
  }
}

TeamDisciplineHumanResourceBO = {
  CanEdit: function (TeamDisciplineHumanResource, FieldName) {
    switch (FieldName) {
      case 'RemoveShifts':
        //var today = new Date()
        if (!TeamDisciplineHumanResource.IsNew()
          && (new Date(TeamDisciplineHumanResource.EndDate()) < new Date(TeamDisciplineHumanResource.GetParent().EndDate()))) {
          return true
        } else {
          return false
        }
        break;
      case 'HasDiscipline':
        if (TeamDisciplineHumanResource.DisciplineID() && !TeamDisciplineHumanResource.HasSchedule()) {
          return true;
        } else {
          return false;
        }
        break;
        break;
      case 'DeleteTDHR':
        if ((ViewModel.IsValid() || TeamDisciplineHumanResource.IsNew()) && !TeamDisciplineHumanResource.HasSchedule()) {
          return true;
        } else {
          return false;
        }
        break;
      case 'HasSchedule':
        if (!TeamDisciplineHumanResource.HasSchedule() && !TeamDisciplineHumanResource.IsNew() && ViewModel.IsValid()) {
          return true;
        } else
          return false;
        break;
      default:
        return true;
        break;
    }
  },
  GetAllowedDisciplines: function (List, self) {
    var results = []
    var parentShiftPattern = self.GetParent()
    List.Iterate(function (disc, discIndx) {
      if (disc.SystemID == parentShiftPattern.SystemID()
        && (parentShiftPattern.ProductionAreaID() == null || (disc.ProductionAreaID == parentShiftPattern.ProductionAreaID()))) {
        results.push(disc)
      }
    })
    return results
  },

  //Defaults
  SetStartDate: function (self) {
    return self.GetParent().StartDate()
  },
  SetEndDate: function (self) {
    return self.GetParent().EndDate()
  },

  //methods
  removeShifts: function (tdhr) {
    if (!tdhr.IsNew()) {
      var r = confirm("Are you sure you want to remove shifts for " + tdhr.HumanResource() + " from " + tdhr.EndDate() + ' onwards');
      if (r == true) {
        Singular.ShowLoadingBar()
        ViewModel.CallServerMethod("RemoveShifts", {
          HumanResourceID: tdhr.HumanResourceID(),
          StartDate: tdhr.StartDate(),
          EndDate: tdhr.EndDate(),
          TeamDisciplineHumanResourceID: tdhr.TeamDisciplineHumanResourceID()
        },
          function (response) {
            if (response.Success) {
              var ed = new Date(tdhr.EndDate())
              OBMisc.Notifications.GritterSuccess("Shifts Removed", "Shifts from " + ed.format("dd-MMM-yy") + ' onwards have been removed', 500)
              Singular.Validation.CheckRules(tdhr)
            } else {
              OBMisc.Notifications.GritterError("Error Removing Shifts", response.ErrorText, 1000)
            }
            Singular.HideLoadingBar()
          })
      }
    }
  }
}

SystemTeamShiftPatternBO = {
  GetSystemAreaDropDown: function (self) {
    if (!ViewModel.NoTemplatesForSystem() && ViewModel.ROSystemAreaShiftPatternList().length == 0) {
      Singular.GetDataStateless('OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList, OBLib', {
        PageSize: 1000,
        SystemID: self.SystemID(),
        FetchTemplates: true
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.ROSystemAreaShiftPatternList);
          if (ViewModel.ROSystemAreaShiftPatternList().length == 0) {
            ViewModel.NoTemplatesForSystem(true);
          }
        }
        Singular.HideLoadingBar();
      });
    }
    return ViewModel.ROSystemAreaShiftPatternList;
  },
  UpdateNoWeekDays: function (self) {
    if (self.SystemAreaShiftPatternID() == undefined) {
      // If not a template then create empty Week Days as per shift duaration
      self.SystemAreaShiftPatternWeekDayList.Set([]);
      for (var i = 0; i < self.ShiftDuration(); i++) {
        for (var k = 0; k < 7; k++) {
          var NewSPWD = self.SystemAreaShiftPatternWeekDayList.AddNew();
          NewSPWD.WeekNo(i + 1);
          NewSPWD.WeekDay(k + 1);
        }
        self.IsExpanded(true);
      }
    }
  },
  InitDate: function (obj) {
    var d = new Date();
    var first_day = new Date(d.getFullYear(), d.getMonth(), 1);
    return first_day;
  },
  DatesValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team.StartDate() != undefined && Team.EndDate() != undefined) {
      var sd = new Date(Team.StartDate());
      var ed = new Date(Team.EndDate());
      if (ed.getTime() < sd.getTime()) {
        CtlError.AddError("Start Date must be before End Date");
      }
    }
  },
  ShiftDurationValid: function (Value, Rule, CtlError) {
    var Team = CtlError.Object;
    if (Team) {
      if (Team.ShiftDuration() === 0) {
        CtlError.AddError("Shift Duration Required");
      }
    }
  },
  SystemAreaShiftPatternWeekDaysSet: function (self) {
    if (self) {
      ViewModel.ROSystemAreaShiftPatternListJustFetched.Set([]);
      if (self.SystemAreaShiftPatternID() != null) { // Fetch Week Days for this shift pattern
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList, OBLib', {
          PageSize: 1,
          SystemAreaShiftPatternID: self.SystemAreaShiftPatternID(),
          FetchTemplates: true,
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.ROSystemAreaShiftPatternListJustFetched);
            var obj = ViewModel.ROSystemAreaShiftPatternListJustFetched()[0];
            if (obj) {
              self.SystemAreaShiftPatternWeekDayList.Set([]);
              self.ShiftDuration(obj.ShiftDuration());
              self.PatternName(obj.PatternName());
              //self.SystemProductionAreaID(obj.SystemProductionAreaID());
              //self.SystemID(obj.SystemID());
              //self.ProductionAreaID(obj.ProductionAreaID());
              ViewModel.TemplateUpDate(true);
              obj.ROSystemAreaShiftPatternWeekDayList().Iterate(function (itm, ind) {
                var NewSPWD = self.SystemAreaShiftPatternWeekDayList.AddNew();
                NewSPWD.WeekNo(itm.WeekNo());
                NewSPWD.WeekDay(itm.WeekDay());
                NewSPWD.ShiftTypeID(itm.ShiftTypeID());
                NewSPWD.OffDay(itm.OffDay());
                NewSPWD.StartTime(itm.StartTime());
                NewSPWD.EndTime(itm.EndTime());
              });
              Singular.Validation.CheckRules(self);
              self.IsExpanded(true);
            }
            Singular.HideLoadingBar();
          }
        });
      }
    }
  },
  AllowedShiftDurations: function (List, pattern) {
    var Allowed = [];
    if (List) {
      List.Iterate(function (itm, ind) {
        Allowed.push({ ShiftDuration: itm.ShiftDuration, ShiftDurationName: itm.ShiftDurationName });
      });
    }
    return Allowed;
  },
  AllowedSystemAreaShiftPatterns: function (List, pattern) {
    var allowedPatterns = []
    List().Iterate(function (systemAreaShiftPattern, Index) {
      if (systemAreaShiftPattern.IsTemplate()) {
        allowedPatterns.push(systemAreaShiftPattern);
      }
    })
    return allowedPatterns;
  }
}

SystemMonthGroupRequirementMonthBO = {
  getSystemMonthGroupMonthDropDown: function (List, self) {
    //var monthGroupRequirement = self.GetParent()
    var monthGroup = self.GetParent().GetParent()
    var results = []
    monthGroup.SystemMonthGroupMonthList().Iterate(function (mnth, mnthIndx) {
      var sds = new Date(mnth.MonthStartDate()).format('dd MMM yy')
      var eds = new Date(mnth.MonthEndDate()).format('dd MMM yy')
      var str = sds + ' - ' + eds
      results.push({ SystemMonthGroupMonthID: mnth.SystemMonthGroupMonthID(), MonthName: str })
    })
    return results
  }
}

SystemTeamNumberBO = {
  SystemTeamNumberValid: function (Value, Rule, CtlError) {
    var TeamNumber = CtlError.Object;
    if (TeamNumber) {
      if (TeamNumber.SystemTeamNumber() <= 0) {
        CtlError.AddError("Team Number Required");
      }
    } else {
      CtlError.AddError("Team Number Required");
    }
  }
}

ROSystemYearBO = {
  ROSystemYearBOToString: function (self) {
    if (self.YearStartDate()) {
      return new Date(self.YearStartDate()).format('yyyy')
    }
  }
}

SystemYearBO = {
  SystemYearBOToString: function (self) {
    if (self.YearStartDate()) {
      return new Date(self.YearStartDate()).format('yyyy')
    }
  }
}

SystemYearMonthGroupBO = {
  SystemYearMonthGroupBOToString: function (self) {
    if (self.MonthGroupStartDate()) {
      return new Date(self.MonthGroupStartDate()).format('yyyy')
    }
  }
}

SystemYearMonthGroupMonthBO = {
  SystemYearMonthGroupMonthBOToString: function (self) {
    if (self.MonthStartDate()) {
      return new Date(self.MonthStartDate()).format('yyyy')
    }
  }
}

SystemYearTeamNumberBO = {
  SystemYearTeamNumberBOToString: function (self) { return "SystemYearTeamNumberBO" }
}

SystemYearTeamNumberMonthGroupBO = {
  SystemYearTeamNumberMonthGroupBOToString: function (self) {
    var team = self.GetParent()
    return team.SystemTeamNumberName() + ': ' + new Date(self.MonthGroupStartDate()).format('dd MMM yy') + ' - ' + new Date(self.MonthGroupEndDate()).format('dd MMM yy')
  },
  RequiredHoursGroupValid: function (Value, Rule, CtlError) {
    if (!(CtlError.Object.RequiredHoursGroup() > 0)) {
      CtlError.AddError("Required Hours must be greater than 0")
    }
  }
}

SystemYearTeamNumberMonthGroupMonthBO = {
  SystemYearTeamNumberMonthGroupMonthBOToString: function (self) {
    var team = self.GetParent().GetParent()
    return team.SystemTeamNumberName() + ': ' + new Date(self.MonthStartDate()).format('dd MMM yy')
  },
  RequiredHoursValid: function (Value, Rule, CtlError) {
    if (!(CtlError.Object.RequiredHours() > 0)) {
      CtlError.AddError("Required Hours must be greater than 0")
    }
  }
}

ROSystemYearMonthGroupBO = {
  ROSystemYearMonthGroupBOToString: function (self) {
    if (self.MonthGroupStartDate()) {
      return new Date(self.MonthGroupStartDate()).format('yyyy')
    }
  }
}

ROSystemYearMonthGroupMonthBO = {
  ROSystemYearMonthGroupMonthBOToString: function (self) {
    if (self.MonthStartDate()) {
      return new Date(self.MonthStartDate()).format('yyyy')
    }
  }
}

ROSystemProductionAreaStatusSelectBO = {
  SystemAreaShiftPatternBOToString: function (self) {
    return 'ROSystemProductionAreaStatusSelect';
  }
}

SystemTimesheetMonthBO = {
  allAreasAndContractTypes: function (systemTimesheetMonth) {
    ClientData.ROSystemProductionAreaList.Iterate(function (area, areaIndex) {
      if (area.SystemID == systemTimesheetMonth.SystemID()) {
        ClientData.ROContractTypeList.Iterate(function (contractType, ctIndx) {
          var newMonthSetting = systemTimesheetMonth.SystemTimesheetMonthSettingList.AddNew()
          newMonthSetting.ProductionAreaID(area.ProductionAreaID)
          newMonthSetting.ContractTypeID(contractType.ContractTypeID)
        })
      }
    })
  },
  save: function (systemTimesheetMonth, onSaveSuccess, onSaveFailed) {
    ViewModel.CallServerMethod("SaveSystemTimesheetMonthStateless",
      {
        SystemTimesheetMonth: systemTimesheetMonth.Serialise()
      },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 1000)
          if (onSaveSuccess) { onSaveSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 3000)
          if (onSaveFailed) { onSaveFailed(response) }
        }
      })
  }
}

ROSystemTimesheetMonthBO = {
  editMonth: function (roSystemTimesheetMonth, onSuccess, onFail) {
    ViewModel.CallServerMethod("GetSystemTimesheetMonth",
      {
        SystemTimesheetMonthID: roSystemTimesheetMonth.SystemTimesheetMonthID()
      },
      function (response) {
        if (response.Success) {
          if (onSuccess) { onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 3000)
          if (onFail) { onFail(response) }
        }
      })
  }
}

SystemTimesheetMonthSettingBO = {
  filteredAreas: function (List, monthSetting) {
    var results = []
    var parent = monthSetting.GetParent()
    List.Iterate(function (area, areaIndex) {
      if (area.SystemID == parent.SystemID()) {
        results.push(area)
      }
    })
    return results
  }
}

TimesheetRequirementBO = {
  //other
  TimesheetRequirementBOToString: function (self) { return self.TimesheetRequirement() },
  saveTimesheetRequirement: function (timesheetRequirement, afterSuccess, afterFail) {
    ViewModel.CallServerMethod("SaveTimesheetRequirement",
      {
        TimesheetRequirement: timesheetRequirement.Serialise()
      },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Requirement Saved", "", 1000)
          if (afterSuccess) { afterSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Requirement Failed to Save", response.ErrorText, 3000)
          if (afterFail) { afterFail(response) }
        }
      })
  },
  refreshStats: function (timesheetRequirement, options) {
    var me = this
    if ((timesheetRequirement.StartDate() == null) || (timesheetRequirement.EndDate() == null)) {
      //do nothing
    } else {
      timesheetRequirement.IsProcessing(true)
      ViewModel.CallServerMethod("RefreshTimesheetRequirementStats",
        { TimesheetRequirement: timesheetRequirement.Serialise() },
        function (response) {
          timesheetRequirement.IsProcessing(false)
          if (response.Success) {
            timesheetRequirement.TimesheetRequirementHRList.Set(response.Data.TimesheetRequirementHRList)
            //if (options && options.afterStatsRefreshSuccess) { options.afterStatsRefreshSuccess(timesheetRequirement, response) }
          } else {
            //if (options && options.afterStatsRefreshFailed) { options.afterStatsRefreshFailed(timesheetRequirement, response) }
          }
        })
    }

  },
  //afterStatsRefreshSuccess: function (self, response) {
  //  self.TotalHumanResources(response.Data.TotalHumanResources)
  //  self.TotalHumanResourceTimesheets(response.Data.TotalHumanResourceTimesheets)
  //  self.ConflictingHumanResourceTimesheets(response.Data.ConflictingHumanResourceTimesheets)
  //},
  //afterStatsRefreshFailed: function (self, response) {

  //},
  HRCountHtml: function (self) {
    return "HR: <small>" + self.TotalHumanResources().toString() + "</small>"
  },
  HRTimesheetCountHtml: function (self) {
    return "Timesheets: <small>" + self.TotalHumanResourceTimesheets().toString() + "</small>"
  },
  HRClashCountHtml: function (self) {
    return "Conflicts: <small>" + self.ConflictingHumanResourceTimesheets().toString() + "</small>"
  },
  applyTimesheetRequirement: function (timesheetRequirement) {
    var me = this
    timesheetRequirement.IsProcessing(true)
    ViewModel.CallServerMethod("ApplyTimesheetRequirement",
      {
        TimesheetRequirement: timesheetRequirement.Serialise()
      },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Requirement Applied Successfully", "", 250)
          me.refreshStats(timesheetRequirement)
        } else {
          OBMisc.Notifications.GritterError("Requirement Failed to Apply", response.ErrorText, 1000)
        }
        timesheetRequirement.IsProcessing(false)
      })
  },
  //afterApplySuccess: function (self, response) {

  //},
  //afterApplyFailed: function (self, response) {

  //},
  getTimesheetRequirement: function (timesheetRequirementID, afterSuccess, afterFail) {
    var me = this
    //timesheetRequirement.IsProcessing(true)
    ViewModel.CallServerMethod("GetTimesheetRequirement",
      {
        TimesheetRequirementID: timesheetRequirementID
      },
      function (response) {
        if (response.Success) {
          //OBMisc.Notifications.GritterSuccess("Requirement Saved", "", 1000)
          if (afterSuccess) { afterSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Requirement Failed to Fetch", response.ErrorText, 3000)
          if (afterFail) { afterFail(response) }
        }
        //timesheetRequirement.IsProcessing(false)
      })
  },
  addAll: function (self) {
    var me = this;
    self.IsProcessing(true)
    self.TimesheetRequirementSettingList().Clear()
    var allAreas = ClientData.ROSystemProductionAreaList
    allAreas.Iterate(function (itm, indx) {
      if (itm.SystemID == self.SystemID()) {
        ClientData.ROContractTypeList.Iterate(function (ct, ctIndx) {
          var newItem = self.TimesheetRequirementSettingList.AddNew()
          newItem.ProductionAreaID(itm.ProductionAreaID)
          newItem.ContractTypeID(ct.ContractTypeID)
        })
      }
    })
    self.IsProcessing(false)
    me.refreshStats(self)
  },
  removeSetting: function (timesheetRequirement, timesheetRequirementSetting) {
    var me = this;
    timesheetRequirement.TimesheetRequirementSettingList.RemoveNoCheck(timesheetRequirementSetting)
    me.refreshStats(timesheetRequirement)
  },

  //set
  StartDateSet: function (self) {
    this.refreshStats(self)
  },
  EndDateSet: function (self) {
    this.refreshStats(self)
  },
  SystemIDSet: function (self) {
    this.refreshStats(self)
  },
  ProductionAreaIDSet: function (self) {
    this.refreshStats(self)
  },
  SystemTeamNumberIDSet: function (self) {
    this.refreshStats(self)
  },

  //rules
  conflictingHumanResourceTimesheetsValid: function (Value, Rule, CtlError) {
    var timesheetRequirement = CtlError.Object;
    if (timesheetRequirement.ConflictingHumanResourceTimesheets() > 0) {
      CtlError.AddWarning("There are timesheets which conflict with this setting")
    }
  },
  timesheetCountsValid: function (Value, Rule, CtlError) {
    var timesheetRequirement = CtlError.Object;
    if (timesheetRequirement.TotalHumanResourceTimesheets() < timesheetRequirement.TotalHumanResources()) {
      CtlError.AddWarning("There are timesheets which conflict with this setting")
    } else if (timesheetRequirement.TotalHumanResourceTimesheets() > timesheetRequirement.TotalHumanResources()) {
      CtlError.AddWarning("Some Human Resources should not have this requirement")
    }
  },

  //cans
  CanEdit: function (inst, columnName) {
    switch (columnName) {
      case 'SystemTeamNumberID':
        return (inst.IsTeamBasedRequirment())
        break;
      case 'SaveButton':
        return (inst.IsValid() && !inst.IsProcessing() && inst.IsDirty())
        break;
      case 'ApplyRequirementButton':
        return (inst.IsValid() && !inst.IsProcessing() && !inst.IsNew())
        break;
      default:
        return true;
        break;
    }
  },
  CanView: function (inst, itemName) {
    switch (itemName) {
      case 'SubRequirementsPanel':
        return (inst.SystemTeamNumberID() == null || inst.SystemTeamNumberID() == undefined)
        break;
    }
  }
}

TimesheetRequirementSettingBO = {
  //other
  TimesheetRequirementSettingBOToString: function (self) { return self.TimesheetRequirement() },
  ProductionAreaIDDropDownList: function (list, self) {
    var results = []
    list.Iterate(function (itm, indx) {
      if (itm.SystemID == self.GetParent().SystemID()) {
        results.push(itm)
      }
    })
    return results
  },

  //set
  ProductionAreaIDSet: function (self) {
    if (!self.GetParent().IsProcessing()) {
      TimesheetRequirementBO.refreshStats(self.GetParent())
    }
  },
  ContractTypeIDSet: function (self) {
    if (!self.GetParent().IsProcessing()) {
      TimesheetRequirementBO.refreshStats(self.GetParent())
    }
  },
  DisciplineIDSet: function (self) {
    if (!self.GetParent().IsProcessing()) {
      TimesheetRequirementBO.refreshStats(self.GetParent())
    }
  },

  //rules

  //cans
  CanEdit: function (inst, columnName) {
    switch (columnName) {
      case 'SaveButton':
        return (inst.IsValid() && !inst.IsProcessing() && inst.IsDirty())
        break;
      case 'ApplyRequirementButton':
        return (inst.IsValid() && !inst.IsProcessing() && !inst.IsNew())
        break;
      default:
        return true;
        break;
    }
  }
}

TimesheetRequirementHRBO = {
  TimesheetRequirementHRBOToString: function (timesheetRequirementHR) { return timesheetRequirementHR.HRName() },
  statusButtonCss: function (timesheetRequirementHR) {
    if (timesheetRequirementHR.HasTimesheetRequirement()) {
      if (timesheetRequirementHR.IsRequired()) {
        return "btn btn-xs btn-default"
      } else {
        return "btn btn-xs btn-danger"
      }
    }
    else {
      if (timesheetRequirementHR.IsRequired()) {
        return "btn btn-xs btn-warning"
      } else {
        return "btn btn-xs btn-danger"
      }
    }
  },
  statusButtonText: function (timesheetRequirementHR) {
    if (timesheetRequirementHR.HasTimesheetRequirement()) {
      if (timesheetRequirementHR.IsRequired()) {
        return "Already Added"
      } else {
        return "No Longer Required"
      }
    }
    else {
      if (timesheetRequirementHR.IsRequired()) {
        return "Needs to be Added"
      } else {
        return "Not Required"
      }
    }
    //if (timesheetRequirementHR.IsClosed()) { return "Closed: " + timesheetRequirementHR.IsClosedDateTimeString() } else { return "Open" }
  },
  statusIconCss: function (timesheetRequirementHR) {
    if (timesheetRequirementHR.HasTimesheetRequirement()) {
      if (timesheetRequirementHR.IsRequired()) {
        return "fa fa-check-square-o"
      } else {
        return "fa fa-times"
      }
    }
    else {
      if (timesheetRequirementHR.IsRequired()) {
        return "fa fa-plus-circle"
      } else {
        return "fa fa-times"
      }
    }
    //if (timesheetRequirementHR.IsClosed()) { return "fa fa-folder-o" } else { return "fa fa-folder-open-o" }
  }
}

ROTimesheetRequirementBO = {
  ROTimesheetRequirementBOToString: function (self) {
    return ""
  }
}

ROTimesheetRequirementPagedBO = {
  ROTimesheetRequirementPagedBOToString: function (self) {
    return ""
  },
  statusButtonCss: function (self) {
    if (self.IsClosed()) {
      return "btn btn-xs btn-danger btn-block"
    } else {
      return "btn btn-xs btn-success btn-block"
    }
  },
  runTimesheetRequirement: function (self) {
    self.IsProcessing(true)
    ViewModel.CallServerMethod("RunTimesheetRequirement",
      {
        TimesheetRequirementID: self.TimesheetRequirementID()
      },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess('Timesheets Run', "Timesheets Run Successfully", 500)
        }
        else {
          OBMisc.Notifications.GritterError('Failed to Run', response.ErrorText, 1500)
        }
        self.IsProcessing(false)
      })
  },
  runButtonCss: function (self) {
    if (self.IsProcessing()) {
      return "fa fa-gear fa-spin"
    } else {
      return "fa fa-gears"
    }
  },
  editButtonCss: function (self) {
    if (self.IsProcessing()) {
      return "fa fa-gear fa-spin"
    } else {
      return "fa fa-edit"
    }
  }
}

SystemProductionAreaBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Maintenance.SystemManagement.SystemProductionAreaList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Sub-Dept Failed", response.ErrorText, 1000)
        }
      })
  },
  //
  save: function (options) {
    ViewModel.CallServerMethod("SaveSystemProductionArea",
      { SystemProductionArea: options.data.Serialise() },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Save Successful", "", 250)
          //  if (options.onSuccess) { options.onSuccess(response) }
          options.onSuccess(response)
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  firstPage: function () {
    ViewModel.SystemProductionArea.DisciplineRatePageNo(1)
    this.updateDisciplineShiftRateList()
  },
  previousPage: function () {
    var CurrentPageNo = ViewModel.SystemProductionArea.DisciplineRatePageNo()
    ViewModel.SystemProductionArea.DisciplineRatePageNo(CurrentPageNo - 1)
    this.updateDisciplineShiftRateList()
  },
  nextPage: function () {
    var CurrentPageNo = ViewModel.SystemProductionArea.DisciplineRatePageNo()
    this.updateDisciplineShiftRateList()
  },
  lastPage: function () {
    this.updateDisciplineShiftRateList()
  },
  updateDisciplineShiftRateList: function () {
    Singular.GetDataStateless("OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRateList, OBLib", {
      DisciplineRatePageNo: ViewModel.SystemProductionArea.DisciplineRatePageNo(),
      DisciplineRatePageSize: ViewModel.SystemProductionArea.DisciplineRatePageSize(),
      DisciplineRateSortColumn: ViewModel.SystemProductionArea.DisciplineRateSortColumn(),
      DisciplineRateSortAsc: ViewModel.SystemProductionArea.DisciplineRateSortAsc(),
      SystemID: ViewModel.SystemProductionArea.SystemID(),
      ProductionAreaID: ViewModel.SystemProductionArea.ProductionAreaID()
    },
      function (response) {
        if (response.Success) {
          ViewModel.SystemProductionArea.SystemAreaDisciplineShiftRateList.Set(response.Data)
        }
        else {
          OBMisc.Notifications.GritterError("Fetch List Failed", response.ErrorText, 1000)
        }
      })

  },

  orderedSystemAreaDisciplineShiftRateList(spa) {
    return spa.SystemAreaDisciplineShiftRateList().sort((item1, item2) => { return item1.SortOrder() - item2.SortOrder() })
  },
  newSystemAreaDisciplineShiftRate(spa) {
    let newRate = spa.SystemAreaDisciplineShiftRateList.AddNew();
    newRate.SystemID(spa.SystemID());
    newRate.ProductionAreaID(spa.ProductionAreaID());
    return newRate;
  }
}

SystemAreaDisciplineShiftRateBO = {
  afterItemAdded: function (item) {
    item.SystemID(item.GetParent().SystemID())
    item.ProductionAreaID(item.GetParent().ProductionAreaID())
    item.SystemProductionAreaID(item.GetParent().SystemProdctionAreaID())
  },

  //AutoDropDown for Human Resources
  onHRSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.FullName(selectedItem.FullName)
    } else {
      businessObject.HumanResourceID(null)
    }
  },
  triggerHRAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setHRCriteriaBeforeRefresh: function (args) {
    args.Data.SystemProductionAreaID = args.Object.GetParent().SystemProductionAreaID()
  },
  afterHRRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //AutoDropDown for Disciplines
  onDisciplineSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.Discipline(selectedItem.Discipline)
    } else {
      businessObject.DisciplineID(null)
    }
  },
  triggerDisciplineAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setDisciplineCriteriaBeforeRefresh: function (args) {
    args.Data.SystemProductionAreaID = args.Object.GetParent().SystemProductionAreaID()
  },
  afterDisciplineRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

SystemProductionAreaFreelancerRateBO = {
  //AutoDropDown for Disciplines
  onDisciplineSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.Discipline(selectedItem.Discipline)
    } else {
      businessObject.SystemProductionAreaDisciplineID(null)
    }
  },
  triggerDisciplineAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setDisciplineCriteriaBeforeRefresh: function (args) {
    args.Data.SystemProductionAreaID = args.Object.GetParent().SystemProductionAreaID()
  },
  afterDisciplineRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

SystemProductionAreaDisciplinePositionTypeBO = {
  //AutoDropDown for Position Types
  onPositionTypeSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.PositionType(selectedItem.PositionType)
    } else {
      businessObject.PositionTypeID(null)
    }
  },
  triggerPositionTypeAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setPositionTypeCriteriaBeforeRefresh: function (args) {
    //not filtering for a specific area / system
    //args.Data.SystemID = args.Object.GetParent().SystemID()
  },
  afterPositionTypeRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //AutoDropDown for Positions
  onPositionSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.Position(selectedItem.Position)
    } else {
      businessObject.PositionID(null)
    }
  },
  triggerPositionAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setPositionCriteriaBeforeRefresh: function (args) {
    //not filtering for a specific area / system
    //args.Data.SystemID = args.Object.GetParent().SystemID()
  },
  afterPositionRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

SystemProductionAreaStatusBO = {
  //AutoDropDown for ProductionAreaStatusID
  onStatusSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaStatus(selectedItem.ProductionAreaStatus)
    } else {
      businessObject.ProductionAreaStatusID(null)
    }
  },
  triggerStatusAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setStatusCriteriaBeforeRefresh: function (args) {
    //not filtering for a specific area / system
    //args.Data.SystemID = args.Object.GetParent().SystemID()
  },
  afterStatusRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

SystemProductionAreaCallTimeSettingBO = {
  //AutoDropDown for Rooms
  onRoomSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.Room(selectedItem.Room)
    } else {
      businessObject.RoomID(null)
    }
  },
  triggerRoomAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    //not filtering for a specific area / system
    //args.Data.SystemID = args.Object.GetParent().SystemID()
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

SystemProductionAreaChannelDefaultBO = {
  //AutoDropDown for Channels
  onChannelSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ChannelName(selectedItem.ChannelName)
    } else {
      businessObject.ChannelID(null)
    }
  },
  triggerChannelAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setChannelCriteriaBeforeRefresh: function (args) {
    //not filtering for a specific area / system
    //args.Data.SystemID = args.Object.GetParent().SystemID()
  },
  afterChannelRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

SystemProductionAreaTimelineTypeBO = {
  //AutoDropDown for Production Timelines
  onTimelineSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionTimelineType(selectedItem.ProductionTimelineType)
    } else {
      businessObject.ProductionTimelineTypeID(null)
    }
  },
  triggerTimelineAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setTimelineCriteriaBeforeRefresh: function (args) {
    //not filtering for a specific area / system
    //args.Data.SystemID = args.Object.GetParent().SystemID()
  },
  afterTimelineRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  }
}

AutomaticImportBO = {
  //series
  triggerSeriesNumberAutoPopulate: function (args) {
    args.AutoPopulate = true;
    args.Object.IsProcessing(true);
  },
  setSeriesNumberCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID();
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID();
  },
  afterSeriesNumberRefreshAjax: function (args) {
    args.Object.IsProcessing(false);
  },
  onSeriesNumberSelected: function (selectedItem, businessObject) {

  },
  //room
  triggerRoomAutoPopulate: function (args) {
    args.AutoPopulate = true;
    args.Object.IsProcessing(true);
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID();
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID();
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm");
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm");
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false);
  },
  onRoomSelected: function (selectedItem, businessObject) {

  }
}

AutomaticImportCrewBO = {
  //Other
  AutomaticImportCrewBOToString: function (self) {
    return "";
  },

  //Cans
  canEdit: function (columnName, productionHumanResource) {
    let area = productionHumanResource.GetParent()
    switch (columnName) {
      case 'DisciplineID':
      case 'PositionID':
      case 'HumanResourceID':
      case "IsSelected":
        return true
        break;
      default:
        return true;
        break;
    }
  },

  //Set

  //dropdowns-------

  //discipline
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().GetParent().ProductionAreaID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterDisciplineIDRefreshAjax: function (args) { },
  onDisciplineIDSelected: function (item, businessObject) { },

  //position type
  triggerPositionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionTypeIDRefreshAjax: function (args) { },
  onPositionTypeIDSelected: function (item, businessObject) {
    //if (item) {
    //  businessObject.PositionTypeID(item.PositionTypeID)
    //  businessObject.PositionID(item.PositionID)
    //  businessObject.Position(item.Position)
    //} else {
    //  businessObject.PositionTypeID(null)
    //  businessObject.PositionID(null)
    //  businessObject.Position("")
    //}
  },

  //position
  triggerPositionIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionIDRefreshAjax: function (args) { },
  onPositionTypeIDSelected: function (item, businessObject) {
    if (item) {
      businessObject.PositionTypeID(item.PositionTypeID)
      businessObject.PositionID(item.PositionID)
      businessObject.Position(item.Position)
    } else {
      businessObject.PositionTypeID(null)
      businessObject.PositionID(null)
      businessObject.Position("")
    }
  },

  //hr
  triggerHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setHumanResourceCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.PositionID = args.Object.PositionID()
    args.Data.PositionTypeID = args.Object.PositionTypeID()
    args.Data.ProductionAreaID = args.Object.GetParent().GetParent().ProductionAreaID()
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
    args.Data.CallTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.WrapTime = new Date().format("dd MMM yyyy HH:mm")
  },
  afterHumanResourceRefreshAjax: function (args) {

  },
  onHumanResourceSelected: function (item, businessObject) {
  },
  onCellCreate: function (element, index, columnData, businessObject) {
    element.parentElement.style.width = "520px"
    element.parentNode.className = "hr-availability-list"
    element.className = 'hr-availability';
    element.innerHTML = '<div class="avatar">' +
      '<img src="' + window.SiteInfo.InternalPhotosPath + businessObject.ImageFileName + '" alt="Avatar">' +
      '</div>' +
      '<div class="info">' +
      '<table class="table table-condensed no-border">' +
      '<tbody>' +
      //header
      '<tr>' +
      '<td style="text-align:left;"><h4>' + businessObject.HRName + '</h4></td>' +
      '<td style="text-align:center; vertical-align:middle;"><span class="' + businessObject.ContractTypeCss + '">' + businessObject.ContractType + '</span></td>' +
      '<td style="text-align:right;"><h5>' + businessObject.TotalHours.toString() + ' / ' + businessObject.RequiredHours.toString() + ' Hours</h5></td>' +
      '</tr>' +
      //progress bar
      '<tr>' +
      '<td colspan="3">' +
      '<div class="progress small">' +
      '<div class="' + businessObject.ProgressBarCss + '" style="width:' + businessObject.HrsPercString + '"></div>' +
      '</div>' +
      '</td>' +
      '</tr>' +
      //other
      '<tr>' +
      '<td colspan="3"><span title="' + businessObject.ClashesTitle + '" class="' + businessObject.ClashesCss + '" style="width:100%;">' + businessObject.IsAvailableString + '</span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PublicHolidaysString + '" class="label label-info">' + businessObject.PublicHolidayCount.toString() + ' Public Holidays </span></td>' +
      '<td><span title="' + businessObject.OffWeekendsString + '" class="label label-info">' + businessObject.OffWeekendCount.toString() + ' Off Weekend/s </span></td>' +
      '<td><span title="' + businessObject.ConsecutiveDaysString + '" class="' + businessObject.ConsecutiveDaysCss + '">' + businessObject.ConsecutiveDays.toString() + ' Consecutive Days </span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PrevShiftEndString + '" class="' + businessObject.ShortfallPrevCss + '">' + businessObject.ShortfallPrevString + ' Shortfall </span></td>' +
      '<td></td>' +
      '<td><span title="' + businessObject.NextShiftStartString + '" class="' + businessObject.ShortfallNextCss + '">' + businessObject.ShortfallNextString + ' Shortfall </span></td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</div>'
  }

  //Rules
};