﻿//#region OB

CrewScheduleDetailOBBO = {
  TimesheetDateValid: function (Value, Rule, CtlError) {
    //CtlError.Object.SuspendChanges = true;
    CrewScheduleDetailBaseBO.CSDClashWithNextTimeline(CtlError);
    if (!OBMisc.Dates.SameDay(CtlError.Object.TimesheetDate(), CtlError.Object.StartDateTime())) {
      CtlError.AddError("Timesheet Date must be the same as Start Date");
    }
    //CtlError.Object.SuspendChanges = false;
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    CtlError.Object.SuspendChanges = true;
    //if not same day as TimesheetDate
    if (!OBMisc.Dates.SameDay(CtlError.Object.StartDateTime(), CtlError.Object.TimesheetDate())) {
      CtlError.AddError('Start Date must be the same as Timesheet Date');
    }
    //if clashes with next timeline
    CrewScheduleDetailBaseBO.CSDClashWithNextTimeline(CtlError);
    //Schedule duration cannot be negative
    if (OBMisc.Dates.DifferenceInMinutes(CtlError.Object.EndDateTime(), CtlError.Object.StartDateTime()) < 0) {
      CtlError.AddError('Start Time must be before End Time');
    };
    CtlError.Object.SuspendChanges = false;
  },
  EndDateTimeValid: function (Value, Rule, CtlError) {

  },
  CSDClashWithNextTimeline: function (CtlError) {
    //switch (CtlError.Object.ProductionTimelineTypeID()) {
    //  case 51:
    //    var OnAirTime = CtlError.Object.GetParent().CrewScheduleDetailList().Find('ProductionTimelineTypeID', 52);
    //    if (OnAirTime) {
    //      if (OBMisc.Dates.IsAfter(CtlError.Object.EndDateTime(), OnAirTime.StartDateTime())) {
    //        CtlError.AddError('Call Time must be before On Air Time');
    //      }
    //    }
    //    break;
    //  case 52:
    //    var WrapTime = CtlError.Object.GetParent().CrewScheduleDetailList().Find('ProductionTimelineTypeID', 54);
    //    if (WrapTime) {
    //      if (OBMisc.Dates.IsAfter(CtlError.Object.EndDateTime(), WrapTime.StartDateTime())) {
    //        CtlError.AddError('Wrap Time must be after On Air Time');
    //      }
    //    }
    //    break;
    //  case 54:
    //    break;
    //}
  },
  TimesheetDateTimeSet: function (CrewScheduleDetail) {

  },
  StartDateTimeSet: function (CrewScheduleDetail) {
    CrewScheduleDetail.TimesheetDate(CrewScheduleDetail.StartDateTime())
  },
  EndDateTimeSet: function (CrewScheduleDetail) {

  }
};

ProductionTimelineOBBO = {
  ProductionTimelineTypeIDSet: function (ProductionTimelineOB) {

  },
  TimelineDateSet: function (ProductionTimelineOB) {
    if (ProductionTimelineOB.IsNew() && !ProductionTimelineOB.StartDateTime() && !ProductionTimelineOB.EndDateTime()) {
      ProductionTimelineOB.Frozen(true);
      ProductionTimelineOB.StartDateTime(ProductionTimelineOB.TimelineDate());
      ProductionTimelineOB.EndDateTime(ProductionTimelineOB.TimelineDate());
      ProductionTimelineOB.Frozen(false);
    } else {
      console.log(ProductionTimelineOB.TimelineDate(), ProductionTimelineOB.StartDateTime(), ProductionTimelineOB.EndDateTime())
      ProductionTimelineOB.Frozen(true);
      //get the new date-------------------------------------------------------------------------
      var newDate = new Date(ProductionTimelineOB.TimelineDate());
      newDate.setHours(0, 0, 0, 0);
      //Current StartTime and EndTime Values-----------------------------------------------------
      var currentStartTimeHours = new Date(ProductionTimelineOB.StartDateTime()).getHours();
      var currentStartTimeMinutes = new Date(ProductionTimelineOB.StartDateTime()).getMinutes();
      console.log(currentStartTimeHours, currentStartTimeMinutes);
      var currentEndTimeHours = new Date(ProductionTimelineOB.EndDateTime()).getHours();
      var currentEndTimeMinutes = new Date(ProductionTimelineOB.EndDateTime()).getMinutes();
      console.log(currentEndTimeHours, currentEndTimeMinutes);
      //Calculate the new StartTime and EndTime Values-------------------------------------------
      var newStartDateTime = new Date(newDate);
      newStartDateTime.setHours(currentStartTimeHours, currentStartTimeMinutes);
      var newEndDateTime = new Date(newDate);
      newEndDateTime.setHours(currentEndTimeHours, currentEndTimeMinutes);
      //Set the new values-----------------------------------------------------------------------
      ProductionTimelineOB.StartDateTime(newStartDateTime);
      ProductionTimelineOB.EndDateTime(newEndDateTime);
      ProductionTimelineOB.Frozen(false);
      console.log(ProductionTimelineOB.TimelineDate(), ProductionTimelineOB.StartDateTime(), ProductionTimelineOB.EndDateTime());
      if (ProductionTimelineOB.LinkedProductionHR() > 0) {
        Singular.SendCommand("ProductionTimelineFullUpdate", {
          Guid: ProductionTimelineOB.Guid()
        }, function (response) {

        });
      }
    };
  },
  StartDateTimeSet: function (ProductionTimelineOB) {
    if (!ProductionTimelineOB.Frozen()) {
      if (ProductionTimelineOB.LinkedProductionHR() > 0) {
        //Send Command to Update
        Singular.SendCommand("ProductionTimelineStartDateUpdated", {
          Guid: ProductionTimelineOB.Guid()
        }, function (response) {

        });
      }
    }
  },
  EndDateTimeSet: function (ProductionTimelineOB) {
    if (!ProductionTimelineOB.Frozen()) {
      if (ProductionTimelineOB.LinkedProductionHR() > 0) {
        //Send Command to Update
        Singular.SendCommand("ProductionTimelineEndDateUpdated", {
          Guid: ProductionTimelineOB.Guid()
        }, function (response) {

        });
      }
    }
  },
  VehicleIDSet: function (ProductionTimelineOB) {

  },
  ProductionTimelineTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionTimelineTypeID()) {
      CtlError.AddError("Timeline Type is required");
    }
  },
  TimelineDateValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.TimelineDate()) {
      CtlError.AddError("Timeline Date is required");
    }
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    if (OBMisc.Dates.IsAfter(CtlError.Object.StartDateTime(), CtlError.Object.EndDateTime())) {
      CtlError.AddError("Start Time must be befoe End Time");
    }
  },
  EndDateTimeValid: function (Value, Rule, CtlError) {
    if (OBMisc.Dates.IsAfter(CtlError.Object.StartDateTime(), CtlError.Object.EndDateTime())) {
      CtlError.AddError("Start Time must be befoe End Time");
    }
  },
  TimelineClashesValid: function (Value, Rule, CtlError) {
    var Timeline = CtlError.Object;
    var TotalClashes = 0;
    Timeline.ProductionTimelineClashList().Iterate(function (itm, indx) {
      TotalClashes += itm.ClashList().length;
    });
    if (TotalClashes > 0) {
      CtlError.AddError(Timeline.ProductionTimelineType & " clashes with other timelines");
    }
  },
  ToString: function (ProductionTimelineOB) {
    var ptt = ""
    var sd = ""
    if (ProductionTimelineOB.ProductionTimelineTypeID()) {
      ptt = ClientData.ROProductionTimelineTypeList.Find("ProductionTimelineTypeID", ProductionTimelineOB.ProductionTimelineTypeID()).ProductionTimelineType;
    }
    if (ProductionTimelineOB.StartDateTime()) {
      sd = new Date(ProductionTimelineOB.StartDateTime()).format("ddd dd MMM yy");
    }
    return ptt & " - " & sd;
  }
};

PSAOutsideBroadcastBO = {
  HasClashesValid: function (Value, Rule, CtlError) {
    var psa = CtlError.Object;
    psa.ROProductionHROBList().Iterate(function (ph, phInd) {
      ph.ClashList().Iterate(function (cls, clsInd) {
        CtlError.AddError(cls);
      });
    });
  },

  //Spec Requirements
  triggerProductionSpecRequirementAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSpecRequirementCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterProductionSpecRequirementRefreshAjax: function (args) {

  },
  ProductionSpecRequirementIDSet: function (self) { },
  onProductionSpecRequirementSelected: function (item, businessObject) { },

  applySpecChange: function (psa) {

  }

};

OBTimelineBO = {
  //set
  ProductionTimelineTypeIDSet: function (timeline) {
  },
  TimelineDateSet: function (timeline) {
  },
  StartDateTimeSet: function (timeline) {
  },
  EndDateTimeSet: function (timeline) {
  },

  //rules
  TimelineDateValid: function (Value, Rule, CtlError) { },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    var st = new Date(CtlError.Object.StartDateTime());
    var et = new Date(CtlError.Object.EndDateTime());
    if (!st) {
      CtlError.AddError("Start Time is required");
    }
    if (!et) {
      CtlError.AddError("End Time is required");
    }
    if (st && et) {
      if (OBMisc.Dates.IsAfter(st, et)) {
        CtlError.AddError("Start Time must be before End Time");
      }
    }
  },
  EndDateTimeValid: function (Value, Rule, CtlError) { },

  //other
  crewButtonCss: function (timeline) {
    if (timeline.HRCount() == 0) {
      return "btn btn-xs btn-default"
    } else {
      return "btn btn-xs btn-primary"
    }
  },
  crewButtonHtml: function (timeline) {
    return timeline.HRCount().toString()
  },

  //cans
  canEdit: function (timeline, columnName) {
    switch (columnName) {
      case "CrewButton":
        return !timeline.IsNew()
        break;
      default:
        return true
        break;
    }
  },

  //vehicle
  triggerTimelineVehicleAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setTimelineVehicleCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionSystemAreaID = args.Object.ProductionSystemAreaID()
  },
  afterTimelineVehicleRefreshAjax: function (args) {

  }
};

OBProductionHumanResourceBO = {
  //set
  DisciplineIDSet: function (ProductionHumanResource) {

  },
  disciplineSelected: function (item, businessObject) {

  },
  PositionIDSet: function (ProductionHumanResource) {

  },
  positionSelected: function (item, businessObject) {
    businessObject.PositionTypeID(item.PositionTypeID)
  },
  IsCancelledSet: function (ProductionHumanResource) {
    var psa = ProductionHumanResource.GetParent();
    var ph = psa.ProductionHRList().Find("HumanResourceID", ProductionHumanResource.HumanResourceID());
    if (ph) {
      ph.ProductionHRBookingList().Iterate(function (bkng, bkngInd) {
        bkng.IsCancelled(ProductionHumanResource.IsCancelled());
        bkng.IgnoreClashes(bkng.IsCancelled());
        bkng.IgnoreClashesReason(ProductionHumanResource.CancelledReason());
      });
    }
  },

  //set - HR
  triggerHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setHumanResourceCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.PositionID = args.Object.PositionID()
    args.Data.PositionTypeID = args.Object.PositionTypeID()
    args.Data.ProductionAreaID = 1//args.Object.ProductionAreaID()
  },
  afterHumanResourceRefreshAjax: function (args) {

  },
  HumanResourceIDBeforeSet: function (self) {
    self.PreviousHumanResourceID(self.HumanResourceID())
    self.PreviousHumanResource(self.HumanResource())
    self.PreviousResourceID(self.ResourceID())
  },
  HumanResourceIDSet: function (self) {
    var me = this;
    var currentHRID = self.HumanResourceID(),
      previousHRID = self.PreviousHumanResourceID();
    //if previously null
    if (previousHRID == null) {
      var newPHRCount = self.GetParent().ProductionHumanResourceList().Filter("HumanResourceID", currentHRID).length
      if (newPHRCount == 0) {
        //if not on the production then add the ProductionHR record
        console.log({ action: "Generate Temp Schedule" })
        me.addProductionHR(self)
      } else {
        //he is already on the production, so do nothing
        console.log({ action: "Do Nothing: already on schedule" })
      }
    }
    else if (previousHRID != null) {
      //if the phr record previously had someone booked
      if (currentHRID == null) {
        //if this record is now null, then the person has been removed
        //but we must check if that previous hrid is still on the production
        var phrCount = self.GetParent().ProductionHumanResourceList().Filter("HumanResourceID", previousHRID).length
        if (phrCount == 0) {
          //if not on the production anymore, then remove the ProductionHR record
          me.removeProductionHR(self)
          console.log({ action: "Remove From Production" })
        } else {
          //he is still on the production, so do nothing
          console.log({ action: "Do Nothing: still on the production" })
        }
      }
      else if (previousHRID != currentHRID) {
        //if this record has now been switched to another person
        //then check if the previous person is still on the production
        var prevPHRCount = self.GetParent().ProductionHumanResourceList().Filter("HumanResourceID", previousHRID).length
        var newPHRCount = self.GetParent().ProductionHumanResourceList().Filter("HumanResourceID", currentHRID).length

        if (prevPHRCount == 0 && newPHRCount >= 1) {
          console.log({ action: "Replace Old with New: update old ProductionHR with new HRID and ResourceID" })
          me.switchProductionHR(self)
        }
        else if (prevPHRCount == 1 && newPHRCount == 1) {
          console.log({ action: "Do Nothing: both still on production?" })
        }
        else {
          if (prevPHRCount == 0) {
            //if not on the production anymore, then remove the ProductionHR record
            me.removeProductionHR(self)
            console.log({ action: "Remove From Production" })
          } else {
            //he is still on the production, so do nothing
            console.log({ action: "Do Nothing: still on the production" })
          }
          //after the above, check if the currentHRID is already on the production    
          if (newPHRCount == 0) {
            //if not on the production then add the ProductionHR record
            console.log({ action: "Generate Temp Schedule" })
            me.addProductionHR(self)
          } else {
            //he is already on the production, so do nothing
            console.log({ action: "Do Nothing: still on the production" })
          }
        }
      }
    }
  },
  onHumanResourceSelected: function (item, businessObject) {
    var x = null
  },
  removeProductionHR: function (phr, onRemoveSuccess, onRemoveFailed) {
    ViewModel.IsProcessing(true)
    var ph = phr.GetParent().ProductionHRList().Find("HumanResourceID", phr.PreviousHumanResourceID())
    if (ph) {
      //remove from list
      ViewModel.CallServerMethod("RemoveProductionHROBServerMethod",
        {
          ProductionHumanResource: phr.Serialise(),
          ProductionHROB: ph.Serialise()
        },
        function (response) {
          if (response.Success) {
            phr.GetParent().ProductionHRList.RemoveNoCheck(ph)
            phr.GetParent().ProductionHRList.DeletedList([])
            var newPHR = new OBProductionHumanResourceObject()
            KOFormatterFull.Deserialise(response.Data.ProductionHumanResource, newPHR)
            phr.GetParent().ProductionHumanResourceList.replace(phr, newPHR)
            if (onRemoveSuccess) { onRemoveSuccess(response) }
          } else {
            OBMisc.Notifications.GritterError("Failed to Remove", response.ErrorText, 3000)
            if (onRemoveFailed) { onSaveSuccess(response) }
          }
          ViewModel.IsProcessing(false)
        })
    }
  },
  addProductionHR: function (phr) {

  },
  switchProductionHR: function (phr) {

  },

  //set - Pref HR
  triggerPrefHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPrefHumanResourceCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.PositionID = args.Object.PositionID()
    args.Data.PositionTypeID = args.Object.PositionTypeID()
    args.Data.ProductionAreaID = 1//args.Object.ProductionAreaID()
  },
  afterPrefHumanResourceRefreshAjax: function (args) {

  },
  PrefHumanResourceIDSet: function (self) { },
  onPrefHumanResourceSelected: function (item, businessObject) { },

  //vehicle
  triggerProductionHumanResourceVehicleAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionHumanResourceVehicleCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionSystemAreaID = args.Object.ProductionSystemAreaID()
  },
  afterProductionHumanResourceVehicleRefreshAjax: function (args) {

  },

  //rules
  ProductionHumanResourceIDValid: function (Value, Rule, CtlError) {
    //var phr = CtlError.Object;
    //if (phr.HumanResourceID()) {
    //  var psa = phr.GetParent();
    //  var productionHR = psa.ProductionHRList().Find("HumanResourceID", phr.HumanResourceID());
    //  if (productionHR) {
    //    if (productionHR.ProductionHRBookingList().length == 1) {
    //      if (productionHR.ProductionHRBookingList()[0].ClashDetailList().length > 0) {
    //        CtlError.AddError(phr.HumanResource() + " has clashes on his schedule");
    //        productionHR.ProductionHRBookingList()[0].ClashDetailList().Iterate(function (cls, clsIndx) {
    //          var sdString = new Date(cls.StartDateTime()).format('ddd dd MMM yy HH:mm')
    //          CtlError.AddError("clash: " + cls.ResourceBookingDescription() + ': ' + sdString);
    //        });
    //      }
    //    }
    //  }
    //};
  },
  CancelledDateValid: function (Value, Rule, CtlError) {
    var phr = CtlError.Object;
    if (phr.IsCancelled()) {
      if (!phr.CancelledDate() || phr.CancelledReason().trim().length == 0) {
        CtlError.AddError("Cancelled Date and Cancelled Reason are required");
      }
    }
  },

  //other


  //cans
  canEdit: function (phr, columnName) {
    switch (columnName) {
      case 'IsCancelled':
        return (phr.HumanResourceID() ? true : false);
        break;
      case 'CancelledReason':
      case 'CancelledDate':
        return !phr.IsNew();
        break;
      case 'DisciplinePosition':
        return false;
        break;
      case 'HumanResourceID':
      case 'HumanResource':
        return !phr.IsNew();
        break;
      default:
        return true;
        break;
    }
    return false;
  }
}

ResourceBookingOBCSDBO = {
  TimesheetDateValid: function (Value, Rule, CtlError) {
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {

  },
  EndDateTimeValid: function (Value, Rule, CtlError) {
  },
  CSDClashWithNextTimeline: function (CtlError) {

  },
  TimesheetDateTimeSet: function (CrewScheduleDetail) {
  },
  BeforeStartDateTimeSet: function (CrewScheduleDetail, args) {
  },
  BeforeEndDateTimeSet: function (CrewScheduleDetail, args) {
  },
  StartDateTimeSet: function (CrewScheduleDetail, args) {
  },
  EndDateTimeSet: function (CrewScheduleDetail, args) {
  },
  canEdit: function (CrewScheduleDetail, FieldName) {
    var ProductionHR = CrewScheduleDetail.GetParent();
    switch (FieldName) {
      case "ExcludeReason":
        if (ProductionHR.IsProcessing() || !CrewScheduleDetail.ExcludeInd()) {
          return false
        }
        break;
      default:
        return !ProductionHR.IsProcessing();
    }
  },
  ScheduledTimesChanged: function (CrewScheduleDetail) {
  },
  StartTimeCss: function (CrewScheduleDetail) { },
  EndTimeCss: function (CrewScheduleDetail) { }
}

ProductionHROBBO = {
  //other
  flightButtonCss: function (productionHR) {
    if (productionHR.FlightCount() > 0) { return "btn btn-xs btn-info" } else { return "btn btn-xs btn-default" }
  },
  rentalCarButtonCss: function (productionHR) {
    if (productionHR.RentalCarCount() > 0) { return "btn btn-xs btn-danger" } else { return "btn btn-xs btn-default" }
  },
  accommodationButtonCss: function (productionHR) {
    if (productionHR.AccommodationCount() > 0) { return "btn btn-xs btn-warning" } else { return "btn btn-xs btn-default" }
  },
  expandButtonVisible: function (productionHR) {
    return false
  },

  //cans
  canEdit: function (productionHR, columnName) {
    switch (columnName) {
      case "SaveProductionHRButton":
        return productionHR.IsValid()
        break;
      case "EditButton":
        return !productionHR.ScheduleFetched()
        break;
      //case "AddTimelinesButton":
      //  return productionHR.IsValid()
      //  break;
      default:
        return true;
        break;
    }
  }
}

ProductionVehicleBO = {
  //VehicleID
  triggerVehicleAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setVehicleCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.CurrentProductionVenueID = ViewModel.CurrentProduction().ProductionVenueID()
  },
  afterVehicleRefreshAjax: function (args) {

  },
  VehicleIDBeforeSet: function (self) {
    //self.PreviousVehicleID(self.VehicleID())
  },
  VehicleIDSet: function (self) {
    var me = this;
  },
  onVehicleSelected: function (item, businessObject) {
    var x = null
  },

  //other
  canEdit: function (productionVehicle, columnName) {
    switch (columnName) {
      case "VehicleID":
        return productionVehicle.IsNew()
        break;
      case "PopulateVehicleCrewButton":
        return !productionVehicle.IsNew()
        break;
      default:
        return true;
        break;
    }
  }
}

ProductionCommentHeaderBO = {
  triggerProductionCommentHeaderAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionCommentHeaderCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionCommentHeader = args.Object.ProductionCommentHeader()
  },
  afterProductionCommentHeaderRefreshAjax: function (args) {

  },
  onProductionCommentHeaderSelected: function (item, businessObject) {
    var x = null
  }
}

ProductionCommentBO = {
  triggerProductionCommentAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionCommentCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionComment = args.Object.ProductionComment()
  },
  afterProductionCommentRefreshAjax: function (args) {

  },
  onProductionCommentSelected: function (item, businessObject) {
    var x = null
  }
}

ProductionSystemAreaCrewTypeBO = {
  IsFinalisedSet: function (self) {
    if (self.IsFinalised()) {
      self.IsFinalisedDateTime(new Date())
      self.IsFinalisedBy(ViewModel.CurrentUserID())
      self.IsFinalisedByName(ViewModel.CurrentUserName())
    } else {
      self.IsFinalisedDateTime(null)
      self.IsFinalisedBy(null)
      self.IsFinalisedByName("")
    }
  },

  //other
  crewButtonHtml: function (crewType) {
    return crewType.HRCount().toString()
  },
  crewButtonCss: function (crewType) {
    if (crewType.HRCount() == 0) {
      return "btn btn-xs btn-default"
    } else {
      return "btn btn-xs btn-warning"
    }
  },
  timelineButtonHtml: function (crewType) {
    return crewType.TimelineCount().toString()
  },
  timelineButtonCss: function (crewType) {
    if (crewType.TimelineCount() == 0) {
      return "btn btn-xs btn-default"
    } else {
      return "btn btn-xs btn-primary"
    }
  }
}

//#endregion

//#region RoomSchedule

RoomScheduleAreaBO = {
  //Other
  RoomScheduleAreaBOToString: function (self) { return self.Room() + " - " + self.Title() },
  removeHumanResource: function (productionHumanResource, element) {
    var me = this,
      rsa = productionHumanResource.GetParent();
    if (productionHumanResource.HumanResourceID()) {
      me.removeHRFromBookingBase(productionHumanResource)
    }
    RoomScheduleAreaBO.syncProductionHR(rsa)
  },
  removeHRFromBookingBase: function (productionHumanResource) {
    var area = productionHumanResource.GetParent()
    var _existingPH = area.RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", productionHumanResource.HumanResourceID());

    if (_existingPH) {
      productionHumanResource.HumanResourceID(null)
      productionHumanResource.HumanResource("")
      productionHumanResource.ResourceID(null)
      area.RoomScheduleAreaProductionHRBookingList.RemoveNoCheck(_existingPH)
    }

    if (area.SlugLayoutInScheduler()) {
      var slugs = area.SlugItemList()
      slugs.Iterate(function (slg, slgInd) {
        if (slg.ProductionHumanResourceID() == productionHumanResource.ProductionHumanResourceID()) {
          slg.ProductionHumanResourceID(null)
        }
      })
    }
    RoomScheduleAreaBO.updatePopverContent(area)
  },
  updatePopverContent: function (area) {
    //update the Popover Content
    if (area.ProductionAreaID() == 9) {
      //if Media Operations
      var editors = me.getNamesForDiscipline(roomSchedule, 50)
      var prdAssistants = me.getNamesForDiscipline(roomSchedule, 40)
      var popover = editors + "<br />" + prdAssistants
      area.PopoverContent(popover)
    } else {
      area.PopoverContent("")
    }
  },
  updateBookingTimes: function (area) {
    if (area.CallTime() && area.WrapTime()) {
      var timesDescript = new Date(area.CallTime()).format('ddd dd MMM yy HH:mm') + ' - ' + new Date(area.WrapTime()).format('HH:mm')
      area.BookingTimes(timesDescript)
    }
    //if (area.IsNew() && area.OnAirTimeStart() && area.OnAirTimeEnd()) {
    //  if (area.RoomScheduleTypeID() == 1) {
    //    ViewModel.CurrentProduction().PlayStartDateTime(area.OnAirTimeStart())
    //  }
    //  else if (area.RoomScheduleTypeID() == 4) {
    //    ViewModel.CurrentAdHocBooking().PlayEndDateTime(area.OnAirTimeEnd())
    //  }
    //}
  },
  updateBookingDescription: function (area) {
    area.ResourceBookingDescription(area.Title())
    //if (area.IsNew()) {
    //  if (area.RoomScheduleTypeID() == 1) {
    //    ViewModel.CurrentProduction().Title(area.Title())
    //  }
    //  else if (area.RoomScheduleTypeID() == 4) {
    //    ViewModel.CurrentAdHocBooking().Title(area.Title())
    //  }
    //}
  },
  updateBookingStatus: function (area) {
    //Update RoomBooking status
    //if reconciled
    if (area.ProductionAreaStatusID() == 4) {
      area.IsCancelled(false)
    }
    //if cancelled
    else if (area.ProductionAreaStatusID() == 5) {
      area.IsCancelled(true)
    }
    //if all others
    else {
      area.IsCancelled(false)
    }
  },
  updateCrew: function (area, statuses, titles, times) {
    area.RoomScheduleAreaProductionHRBookingList().Iterate(function (ph, indx) {
      //Title
      if (titles) {
        var allPositions = ""
        var parent = ph.GetParent()
        if (ClientData.ROSystemProductionAreaDisciplinePositionTypeListFlat) {
          var psaSettings = ClientData.ROSystemProductionAreaDisciplinePositionTypeListFlat.Filter("SystemID", parent.SystemID()).Filter("ProductionAreaID", parent.ProductionAreaID())
          if (psaSettings.length > 0) {
            var phrl = parent.RoomScheduleAreaProductionHumanResourceList().Filter("HumanResourceID", ph.HumanResourceID())
            phrl.Iterate(function (phr, phrIndx) {
              var disciplineSettings = psaSettings.Filter("DisciplineID", phr.DisciplineID())
              if (disciplineSettings.length > 0 && phr.PositionID()) {
                var positionTypeSettings = disciplineSettings.Filter("PositionID", phr.PositionID())
                if (positionTypeSettings.length == 1) {
                  var setting = positionTypeSettings[0]
                  if (setting.IncludeInBookingDescription) {
                    if (allPositions.length > 0) {
                      allPositions += ", " + setting.PositionShortName
                    }
                    else {
                      allPositions = setting.PositionShortName
                    }
                  }
                }
              }
            })
          }
        }
        //do the update
        if (allPositions == "") {
          ph.ResourceBookingDescription(area.ResourceNamePSA() + " - " + area.ResourceBookingDescription())
        }
        else {
          ph.ResourceBookingDescription(allPositions + ": " + area.ResourceNamePSA() + " - " + area.ResourceBookingDescription())
        }
      }
      //Status
      var phrList = area.RoomScheduleAreaProductionHumanResourceList().Filter("HumanResourceID", ph.HumanResourceID())
      var cancellationCount = phrList.Filter("IsCancelled", true).length
      if (statuses) {
        ph.StatusCssClass(area.StatusCssClass())
        //if reconciled
        if (area.ProductionAreaStatusID() == 4 && cancellationCount == 0) {
          ph.IsCancelled(false)
        }
        //if cancelled
        else if (area.ProductionAreaStatusID() == 5) {
          ph.IsCancelled(true)
        }
        //if all others
        else {
          if (cancellationCount == 0) {
            ph.IsCancelled(false)
          }
          else {
            ph.IsCancelled(true)
          }
        }
      }
      //Times
      if (times) {
        ph.StartDateTimeBuffer(area.CallTime())
        ph.StartDateTime(area.OnAirTimeStart())
        ph.EndDateTime(area.OnAirTimeEnd())
        ph.EndDateTimeBuffer(area.WrapTime())
      }
    })
  },
  updateAll: function (area) {
    if (!area.IsProcessing()) {
      RoomScheduleAreaBO.updateBookingDescription(area)
      RoomScheduleAreaBO.updateBookingTimes(area)
      RoomScheduleAreaBO.updateBookingStatus(area)
      RoomScheduleAreaBO.updateCrew(area, true, true, false)
      RoomScheduleAreaBO.checkClashes(area, null, null)
    }
  },
  checkClashes: function (area, afterSuccess, afterFail) {
    var me = this
    if (area.CallTime() && area.OnAirTimeStart() && area.OnAirTimeEnd() && area.WrapTime() && area.RoomID()) {
      if (!area.IsProcessing()) {
        var _allBookings = []
        area.IsProcessing(true)
        //cleanup
        ViewModel.ResourceBookingsCleanList([])
        //only if owner then check room clashes
        if (area.IsOwner()) {
          var _roomBookings = []
          _roomBooking = me.getRoomResourceBookingCleanList(area)
          _roomBookings.push(_roomBooking)
          _allBookings = _allBookings.concat(_roomBookings)
        }
        //add hr bookings
        var _hrBookings = []
        _hrBookings = me.getHRResourceBookingCleanList(area)
        _allBookings = _allBookings.concat(_hrBookings)
        ViewModel.ResourceBookingsCleanList(_allBookings)
        var data = KOFormatter.Serialise(ViewModel.ResourceBookingsCleanList())
        ViewModel.CallServerMethod("GetResourceBookingClashes", {
          ResourceBookings: data
        },
          function (response) {
            area.IsProcessing(false)
            if (response.Success) {
              me.updateClashes(area, response.Data)
              if (afterSuccess) { afterSuccess(response) }
            }
            else {
              OBMisc.Notifications.GritterError("Error Checking Clashes", response.ErrorText, 3000)
              if (afterFail) { afterFail(response) }
            }
          })
      }
    }
  },
  getRoomResourceBookingCleanList: function (area) {
    return OBMisc.Resources.getResourceBookingCleanCustom(area.Guid(), area.ResourceBookingID(), area.ResourceID(), area.ResourceBookingTypeID(),
      area.CallTime(), area.OnAirTimeStart(), area.OnAirTimeEnd(), area.WrapTime(),
      null, null, null,
      null, area.RoomScheduleID(), area.ProductionSystemAreaID())
  },
  getHRResourceBookingCleanList: function (area) {
    var lst = []
    area.RoomScheduleAreaProductionHRBookingList().Iterate(function (hr, indx) {
      lst.push(OBMisc.Resources.getResourceBookingCleanCustom(hr.Guid(), hr.ResourceBookingID(), hr.ResourceID(), hr.ResourceBookingTypeID(),
        hr.StartDateTimeBuffer(), hr.StartDateTime(), hr.EndDateTime(), hr.EndDateTimeBuffer(),
        null, null, null,
        hr.ProductionHRID(), hr.RoomScheduleID(), area.ProductionSystemAreaID()))
    })
    return lst
  },
  updateClashes: function (area, returnedBookings) {
    //Room Schedule
    if (area.IsOwner()) {
      area.IsProcessing(true)
      var _clashData = returnedBookings.Find("Guid", area.Guid())
      area.ClashDetailList([])
      _clashData.ClashDetailList.Iterate(function (cls, clsInd) {
        var nI = new ResourceHelpers_ClashDetailObject()
        KOFormatterFull.Deserialise(cls, nI)
        area.ClashDetailList.Add(nI)
      })
    }
    //Production HR
    area.RoomScheduleAreaProductionHRBookingList().Iterate(function (ph, phIndx) {
      ph.ClashDetailList([])
      var _hrClashes = returnedBookings.Find("Guid", ph.Guid())
      if (_hrClashes) {
        _hrClashes.ClashDetailList.Iterate(function (cls, clsInd) {
          var nbI = new ResourceHelpers_ClashDetailObject()
          KOFormatterFull.Deserialise(cls, nbI)
          ph.ClashDetailList.Add(nbI)
        })
      }
    })
    Singular.Validation.CheckRules(area)
    area.IsProcessing(false)
  },
  pendingDeleteTitleText: function (area) {
    return "This booking has been flagged for deletion by its owner."
  },
  updateMismatch: function (self) {
    var ct = new Date(self.CallTime()).getTime()
    var oas = new Date(self.OnAirTimeStart()).getTime()
    var oae = new Date(self.OnAirTimeEnd()).getTime()
    var wt = new Date(self.WrapTime()).getTime()
    var cth = new Date(self.RSCallTime()).getTime()
    var oash = new Date(self.RSOnAirTimeStart()).getTime()
    var oaeh = new Date(self.RSOnAirTimeEnd()).getTime()
    var wth = new Date(self.RSWrapTime()).getTime()
    var rmOwn = self.RoomResourceIDOwner()
    var rmPSA = self.ResourceIDPSA()
    if (ct != cth) { self.CallTimeMatch(false) } else { self.CallTimeMatch(true) }
    if (oas != oash) { self.OnAirStartMatch(false) } else { self.OnAirStartMatch(true) }
    if (oae != oaeh) { self.OnAirEndMatch(false) } else { self.OnAirEndMatch(true) }
    if (wt != wth) { self.WrapTimeMatch(false) } else { self.WrapTimeMatch(true) }
    if (rmOwn != rmPSA) { self.RoomMatch(false) } else { self.RoomMatch(true) }
  },
  doesNotMatchParent: function (area) {
    return !(area.CallTimeMatch() & area.OnAirStartMatch() & area.OnAirEndMatch() & area.WrapTimeMatch() & area.RoomMatch())
  },
  parentMismatchTitleText: function (area) {
    return "This booking does not match the owners details"
  },
  updateCrewTimesToBookingTimes: function (area) {
    //var rs = area.GetParent()
    area.RoomScheduleAreaProductionHRBookingList().Iterate(function (ph, phIndx) {
      ph.StartDateTimeBuffer(area.CallTime())
      ph.StartDateTime(area.OnAirTimeStart())
      ph.EndDateTime(area.OnAirTimeEnd())
      ph.EndDateTimeBuffer(area.WrapTime())
    })
    this.checkClashes(area, null, null)
  },
  matchWithParent: function (area) {
    area.CallTime(area.RSCallTime())
    area.OnAirTimeStart(area.RSOnAirTimeStart())
    area.OnAirTimeEnd(area.RSOnAirTimeEnd())
    area.WrapTime(area.RSWrapTime())
    area.ResourceIDPSA(area.RoomResourceIDOwner())
    area.ResourceNamePSA(area.ResourceNamePSA())
    this.updateMismatch(area)
    this.updateCrew(area, true, true, false)
    this.updateAll(area)
  },
  showModal: function (response) {
    var me = this
    var modalID = "#CurrentRoomScheduleModal"

    $(modalID).off('shown.bs.modal')
    $(modalID).off('hidden.bs.modal')

    if (!ViewModel.CurrentRoomScheduleArea().IsNew()) {
      ViewModel.CurrentProduction(null)
      ViewModel.CurrentAdHocBooking(null)
    }
    $(modalID).on('shown.bs.modal', function () {
      $(modalID).draggable({ handle: ".modal-header" })
      OBMisc.UI.activateTab("MyAreaComments")
      OBMisc.UI.activateTab("Crew")
    })
    $(modalID).on('hidden.bs.modal', function () {
      window.siteHubManager.bookingOutOfEdit(ViewModel.CurrentRoomScheduleArea().ResourceBookingID())
      ViewModel.CurrentProduction(null)
      ViewModel.CurrentAdHocBooking(null)
    })

    $(modalID).modal()
  },
  currentTitle: function () {
    if (ViewModel.CurrentRoomScheduleArea()) {
      return ViewModel.CurrentRoomScheduleArea().Title();
    };
    return "Room Booking";
  },
  currentRoomScheduleArea: function () {
    if (ViewModel.CurrentRoomScheduleArea) {
      return ViewModel.CurrentRoomScheduleArea()
    }
    return null
  },
  editEvent: function (roomScheduleArea) {
    var me = this
    OBMisc.UI.activateTab("EventInfo")
    roomScheduleArea.IsProcessing(true)
    if (roomScheduleArea.RoomScheduleTypeID() == 1) {
      me.editProduction(roomScheduleArea)
    } else if (roomScheduleArea.RoomScheduleTypeID() == 4) {
      me.editAdHocBooking(roomScheduleArea)
    }
  },
  editProduction: function (roomScheduleArea) {
    var me = this
    ProductionBO.getProduction(roomScheduleArea.ProductionID(), false, true,
      function (response) {
        ViewModel.CurrentProduction.Set(response.Data[0])
        roomScheduleArea.EventFetched(true)
        roomScheduleArea.IsProcessing(false)
      },
      function (response) {
        ViewModel.CurrentProduction.Set(null)
        roomScheduleArea.EventFetched(false)
        roomScheduleArea.IsProcessing(false)
      })
  },
  editAdHocBooking: function (roomScheduleArea) {
    var me = this
    AdHocBookingBO.getAdHocBooking(roomScheduleArea.AdHocBookingID(),
      function (response) {
        ViewModel.CurrentAdHocBooking.Set(response.Data[0])
        roomScheduleArea.EventFetched(true)
        roomScheduleArea.IsProcessing(false)
      },
      function (response) {
        ViewModel.CurrentAdHocBooking.Set(null)
        roomScheduleArea.EventFetched(false)
        roomScheduleArea.IsProcessing(false)
      })
  },
  addCrew: function (psa) {
    var phr = psa.RoomScheduleAreaProductionHumanResourceList.AddNew()
    phr.ProductionSystemAreaID(psa.ProductionSystemAreaID())
  },
  deletedSelectedPHR: function (roomScheduleArea) {
    var me = this;
    var toRemove = [];
    roomScheduleArea.RoomScheduleAreaProductionHumanResourceList().Iterate(function (phr, phrIndx) {
      if (phr.IsSelected()) {
        toRemove.push(phr)
      }
    })
    toRemove.Iterate(function (phr, phrIndx) {
      var prnt = phr.GetParent()
      prnt.RoomScheduleAreaProductionHumanResourceList.RemoveNoCheck(phr)
    })
    RoomScheduleAreaBO.syncProductionHR(roomScheduleArea)
  },
  editProductionHumanResource: function (productionHumanResource) {
    var me = this;
    var parent = productionHumanResource.GetParent()
    parent.SelectedProductionHumanResourceGuid(productionHumanResource.Guid())
    me.showPersonnelManager()
  },
  refreshAndEditEvent: function (roomScheduleArea) {
    var me = this;
    me.editEvent(roomScheduleArea)
  },
  currentProductionHumanResource: function (roomScheduleArea) {
    var selectedGuid = roomScheduleArea.SelectedProductionHumanResourceGuid();
    if (selectedGuid.trim().length > 0) {
      return roomScheduleArea.RoomScheduleAreaProductionHumanResourceList().Find("Guid", selectedGuid);
    }
    return null;
  },
  currentProductionHR: function (roomScheduleArea) {
    var me = this;
    if (roomScheduleArea.SelectedProductionHumanResourceGuid() != null) {
      var phr = me.currentProductionHumanResource(roomScheduleArea)
      if (phr && phr.HumanResourceID()) {
        var productionHR = roomScheduleArea.RoomScheduleAreaProductionHRBookingList().Find('HumanResourceID', phr.HumanResourceID());
        return productionHR
      }
    }
    return null
  },
  editTimes: function (roomSchedule) {
    $('a[href="#BookingTimes"]').tab('show')
  },
  showPersonnelManager: function () {
    var me = this;
    var selector = '#PersonnelManagerModal'
    $(selector).off('shown.bs.modal')
    $(selector).off('hidden.bs.modal')
    $(selector).on('shown.bs.modal', function () {
      $(selector).draggable({
        handle: ".modal-header"
      })
    })
    $(selector).on('hidden.bs.modal', function () {
    })
    $(selector).modal()
  },
  refreshAudit: function (roomScheduleArea) {
    roomScheduleArea.IsProcessing(true)
    ROObjectAuditTrailBO.get({
      criteria: {
        ForScenario: "RoomScheduleArea",
        RoomScheduleID: roomScheduleArea.RoomScheduleID(),
        ProductionSystemAreaID: roomScheduleArea.ProductionSystemAreaID()
      },
      onSuccess: function (response) {
        roomScheduleArea.ROObjectAuditTrailList.Set(response.Data)
        roomScheduleArea.IsProcessing(false)
      },
      onFail: function (response) {
        OBMisc.Notifications.GritterError("Failed to fecth audit", response.ErrorText, 1000)
        roomScheduleArea.IsProcessing(false)
      }
    })
  },
  syncProductionHR: function (rsa) {
    //set
    rsa.RoomScheduleAreaProductionHRBookingList().Iterate(function (ph) {
      ph.Found = false
    })
    //check
    var currentHRIDs = rsa.RoomScheduleAreaProductionHumanResourceList().map(function (phr) { return phr.HumanResourceID() })
    currentHRIDs.Iterate(function (id) {
      var ph = rsa.RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", id)
      if (ph) {
        ph.Found = true
      }
    })
    //remove
    var toRemove = rsa.RoomScheduleAreaProductionHRBookingList().filter(function (item) { return ((item.Found ? item.Found : false) == false) })
    while (toRemove.length > 0) {
      rsa.RoomScheduleAreaProductionHRBookingList.RemoveNoCheck(toRemove[0])
      toRemove.pop(toRemove[0])
    }
  },
  cancelSelectedPHR: function (roomScheduleArea) {
    roomScheduleArea.CancellationSelected(true)
    var cancellationModal = document.querySelector('div.cancellation-modal')
    var cancellationModalContent = document.querySelector('div.cancellation-modal-content')
    if (cancellationModal) {
      cancellationModal.style.display = 'block'
    }
    if (cancellationModalContent) {
      var parentDimensions = cancellationModal.getBoundingClientRect()
      var myDimensions = cancellationModalContent.getBoundingClientRect()
      var calculatedWidth = (parentDimensions.width / 2)
      cancellationModalContent.style.width = calculatedWidth.toString() + 'px'
      cancellationModalContent.style.marginLeft = ((parentDimensions.width - calculatedWidth) / 2).toString() + 'px'
      cancellationModalContent.style.marginTop = ((parentDimensions.height - myDimensions.height) / 2).toString() + 'px'
    }
  },
  commitCancellation: function (roomScheduleArea) {
    var me = this;
    var toRemove = [];
    roomScheduleArea.RoomScheduleAreaProductionHumanResourceList().Iterate(function (phr, phrIndx) {
      if (phr.IsSelected()) {
        phr.CancelledReason(roomScheduleArea.CancellationReason())
        phr.CancelledDate(roomScheduleArea.CancellationDate())
        phr.IsCancelled(true)
      }
    })
    RoomScheduleAreaBO.cancelCancellation(roomScheduleArea)
    RoomScheduleAreaBO.updateAll(roomScheduleArea)
  },
  cancelCancellation: function (roomScheduleArea) {
    roomScheduleArea.CancellationSelected(false)
    roomScheduleArea.CancellationDate(null)
    roomScheduleArea.CancellationReason("")
    var cancellationModal = document.querySelector('div.cancellation-modal')
    if (cancellationModal) {
      cancellationModal.style.display = 'none'
    }
  },
  unreconcile: function (roomScheduleArea) {
    ViewModel.CallServerMethodPromise("UnReconcileRoomScheduleArea", { RoomScheduleArea: roomScheduleArea.Serialise() })
      .then(data => {
        ViewModel.CurrentRoomScheduleArea.Set(data)
        if (RoomScheduleAreaBO.afterSave) { RoomScheduleAreaBO.afterSave(data) }
      }),
      errorText => {
        OBMisc.Notifications.GritterError("UnReconcile failed", errorText, 1000)
      }
  },
  differencesFromOwnerHTML(roomScheduleArea) {
    const oCT = moment(roomScheduleArea.RSCallTime());
    const oST = moment(roomScheduleArea.RSOnAirTimeStart());
    const oET = moment(roomScheduleArea.RSOnAirTimeEnd());
    const oWT = moment(roomScheduleArea.RSWrapTime());

    const myCT = moment(roomScheduleArea.CallTime());
    const myST = moment(roomScheduleArea.OnAirTimeStart());
    const myET = moment(roomScheduleArea.OnAirTimeEnd());
    const myWT = moment(roomScheduleArea.WrapTime());

    const header = "<label>Differences from owner:</label>" + " <br>";
    const roomDifferent = ((roomScheduleArea.ResourceIDPSA() != roomScheduleArea.RoomResourceIDOwner()) ? ("<label>Room:</label> " + roomScheduleArea.ResourceNamePSA() + " vs " + roomScheduleArea.ResourceNameOwner() + " <br>") : "");
    const calltimeDifferent = (!myCT.isSame(oCT, 'minute') ? ("<label>Call Time:</label> <br>" + myCT.format("ddd DD MMM HH:mm") + " vs " + oCT.format("ddd DD MMM HH:mm") + " <br>") : "");
    const onAirTimeStartDifferent = (!myST.isSame(oST, 'minute') ? ("<label>Start Time:</label> <br>" + myST.format("ddd DD MMM HH:mm") + " vs " + oST.format("ddd DD MMM HH:mm") + " <br>") : "");
    const onAirTimeEndDifferent = (!myET.isSame(oET, 'minute') ? ("<label>End Time:</label> <br>" + myET.format("ddd DD MMM HH:mm") + " vs " + oET.format("ddd DD MMM HH:mm") + " <br>") : "");
    const wrapTimeDifferent = (!myWT.isSame(oWT, 'minute') ? ("<label>Wrap Time:</label> <br>" + myWT.format("ddd DD MMM HH:mm") + " vs " + oWT.format("ddd DD MMM HH:mm") + " <br>") : "");
    const commentsDifferent = ((roomScheduleArea.AreaComments() != roomScheduleArea.OwnerComments()) ? ("<label>Comments are different</label>" + " <br>") : "");

    return header + roomDifferent + calltimeDifferent + onAirTimeStartDifferent + onAirTimeEndDifferent + wrapTimeDifferent + commentsDifferent;
  },

  //server methods
  save: function (area, production, adhocbooking, afterSuccess, afterFail, otherParams) {
    var me = this;
    if (!area.IsProcessing()) {
      area.IsProcessing(true);
      RoomScheduleAreaBO.updateCrew(area, false, true, false);
      ViewModel.CallServerMethod("SaveRoomScheduleArea", {
        RoomScheduleArea: area.Serialise(),
        Production: (production ? production.Serialise() : null),
        AdHocBooking: (adhocbooking ? adhocbooking.Serialise() : null)
      },
        function (response) {
          area.IsProcessing(false)
          if (response.Success) {
            OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 250);
            window.siteHubManager.changeMade();
            afterSuccess.call(otherParams, response);
          }
          else {
            OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000);
            afterFail.call(otherParams, response);
          }
        });
    }
  },
  saveGeneric: function (area, production, adhocBooking, options) {
    var me = this;
    if (!area.IsProcessing()) {
      area.IsProcessing(true);
      RoomScheduleAreaBO.updateCrew(area, false, true, false);
      ViewModel.CallServerMethod("SaveRoomScheduleArea", {
        RoomScheduleArea: area.Serialise(),
        Production: (production ? production.Serialise() : null),
        AdHocBooking: (adhocBooking ? adhocBooking.Serialise() : null)
      },
        function (response) {
          if (response.Success) {
            KOFormatterFull.Deserialise(response.Data, area)
            OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 150);
            window.siteHubManager.changeMade();
            if (options.onSuccess) { options.onSuccess.call(this, response) }
          }
          else {
            OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
            if (options.onFail) { options.onFail.call(this, response) }
          }
          area.IsProcessing(false)
        });
    }
  },
  saveModal: function (roomScheduleArea) {
    var me = this;
    RoomScheduleAreaBO.checkClashes(roomScheduleArea,
      function (response) {
        RoomScheduleAreaBO.saveGeneric(roomScheduleArea,
          ViewModel.CurrentProduction(), ViewModel.CurrentAdHocBooking(),
          { onSuccess: RoomScheduleAreaBO.afterSave })
      },
      function (response) {
        //check clashes failed
      })
  },
  async getRoomScheduleArea(options) {
    //standard stuff to happen after every RoomScheduleArea has been fetched
    window.siteHubManager.bookingIntoEdit(options.criteria.resourceBookingID);
    try {
      let data = await Singular.GetDataStatelessPromise("OBLib.Scheduling.Rooms.RoomScheduleAreaList", {
        RoomScheduleID: options.criteria.roomScheduleID,
        ProductionSystemAreaID: options.criteria.productionSystemAreaID
      });
      if (options.onSuccess) { options.onSuccess(data) }
    }
    catch (err) {
      OBMisc.Notifications.GritterError("Error Opening", err.message, 1000)
    };
  },
  get: function (options) {
    Singular.GetDataStateless("OBLib.Scheduling.Rooms.RoomScheduleAreaList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          ViewModel.CurrentProduction(null)
          ViewModel.CurrentAdHocBooking(null)
          ViewModel.CurrentRoomScheduleArea.Set(response.Data[0])
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  getPromise(criteria) {
    return Singular.GetDataStatelessPromise("OBLib.Scheduling.Rooms.RoomScheduleAreaList, OBLib", criteria);
  },
  deleteAreas: function (options) {
    ViewModel.CallServerMethod("DeleteRoomScheduleAreas",
      options.criteria,
      function (response) {
        if (response.Success) { if (options.onSuccess) { options.onSuccess(response) } }
        else { if (options.onFail) { options.onFail(response) } }
      })
  },
  deleteRoomSchedulesPromise(criteria) {
    return ViewModel.CallServerMethodPromise("DeleteRoomScheduleAreas", criteria);
  },
  afterSave: null,
  saveRoomAllocator: function (roomScheduleArea, options) {
    RoomScheduleAreaBO.updateCrew(roomScheduleArea, false, true, false)
    if (!roomScheduleArea.IsProcessing()) {
      roomScheduleArea.IsProcessing(true);
      ViewModel.CallServerMethod("SaveRoomScheduleArea", {
        RoomScheduleArea: roomScheduleArea.Serialise(),
        Production: null,
        AdHocBooking: null
      },
        function (response) {
          roomScheduleArea.IsProcessing(false)
          if (response.Success) {
            OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 250);
            window.siteHubManager.changeMade();
            options.onSuccess(response);
          }
          else {
            OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000);
            options.onFail(response);
          }
        });
    }
  },

  //Cans
  CanEdit: function (psa, columnName) {
    switch (columnName) {
      case "CancelControl":
      case "TitleButton":
      case "ProductionAreaStatusID":
      case "DebtorID":
      case "OwnerComments":
      case "AreaComments":
      case "Title":
      case "RoomID":
      case "CallTime":
      case "OnAirTimeStart":
      case "OnAirTimeEnd":
      case "WrapTime":
      case "RoomID":
        return !psa.IsReconciled()
      default:
        return true
        break;
    }
  },
  canView: function (psa, columnName) {
    switch (columnName) {
      case "SaveButton":
        return (!psa.IsReconciled() && !psa.IsProcessing())
      case "CancelControl":
      case "TitleButton":
      case "DebtorID":
      case "OwnerComments":
      case "AreaComments":
      case "UpdateCrewTimes":
      case "MatchWithParent":
      case "UpdateCrewTimes":
      case "AddCrew":
      case "DeleteSelected":
      case "CancelSelected":
        return !psa.IsReconciled()
        break;
      case "ProductionAreaStatusID":
        return !psa.IsReconciled()
        break;
      case "UnReconcileButton":
        return psa.IsReconciled()
        break;
      case "DifferencesFromOwner":
        return !psa.IsOwner()
        break;
      default:
        return true
        break;
    }
  },
  canAddCrew: function (psa) {
    if (psa.IsValid()) {
      return true;
    }
    return false;
  },
  canDeleteCrew: function (psa) {
    return true;
  },
  canSave: function (area) {
    return (area.IsValid() && !area.IsReconciled())
  },
  canCancelCrew: function (roomScheduleArea) {
    var count = roomScheduleArea.RoomScheduleAreaProductionHumanResourceList().Filter("IsSelected", true).length
    return (count > 0)
  },
  canProceedWithCancelCrew: function (area) {
    return (!area.IsReconciled() && area.CancellationDate() && area.CancellationReason().trim().length > 0)
  },

  //Set
  CallTimeStartSet: function (area) {
    this.updateMismatch(area)
    RoomScheduleAreaBO.updateAll(area)
  },
  StartDateTimeSet: function (area) {
    this.updateMismatch(area)
    RoomScheduleAreaBO.updateAll(area)
  },
  EndDateTimeSet: function (area) {
    this.updateMismatch(area)
    RoomScheduleAreaBO.updateAll(area)
  },
  WrapTimeEndSet: function (area) {
    this.updateMismatch(area)
    RoomScheduleAreaBO.updateAll(area)
  },

  RoomIDSet: function (area) {
    //RoomScheduleAreaBO.updateAll(area)
  },
  triggerRoomAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.OnAirTimeStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.OnAirTimeEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    var me = this;
    if (businessObject.IsOwner()) {
      businessObject.Room(selectedItem.Room)
      businessObject.RoomID(selectedItem.RoomID)
      businessObject.ResourceID(selectedItem.ResourceID)
      businessObject.RoomResourceIDOwner(selectedItem.ResourceID)
      RoomScheduleAreaBO.updateAll(businessObject)
    }
    //if (!selectedItem.IsAvailable) {
    //  businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    //} else {
    //  businessObject.RoomClashCount(0)
    //}
  },

  DebtorIDSet: function (self) {

  },
  triggerDebtorAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDebtorCriteriaBeforeRefresh: function (args) {

  },
  afterDebtorRefreshAjax: function (args) {

  },
  onDebtorSelected: function (selectedItem, businessObject) {
    businessObject.Debtor(selectedItem.Debtor)
  },

  ProductionAreaStatusIDSet: function (area) {
    RoomScheduleAreaBO.setStatusClass(self)
    RoomScheduleAreaBO.updateAll(area)
  },
  triggerStatusAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setStatusCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterStatusRefreshAjax: function (args) {

  },
  onStatusSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaStatus(selectedItem.ProductionAreaStatus)
      businessObject.StatusCssClass(selectedItem.CssClass)
    }
    else {
      businessObject.ProductionAreaStatus("")
      businessObject.StatusCssClass("")
    }
    RoomScheduleAreaBO.updateBookingStatus(businessObject)
    RoomScheduleAreaBO.updateCrew(businessObject, true, false, false)
  },

  //Set
  SystemIDSet: function (self) {
    if (self.IsNew()) {
      RoomScheduleAreaBO.setStatusClass(self)
    }
  },
  ProductionAreaIDSet: function (self) {
    if (self.IsNew()) {
      RoomScheduleAreaBO.setStatusClass(self)
    }
  },
  setStatusClass: function (self) {
    try {
      if (self.SystemID() && self.ProductionAreaID() && self.ProductionAreaStatusID()) {
        var lst = ClientData.ROSystemProductionAreaStatusSelectList.Filter("SystemID", self.SystemID())
        lst = lst.Filter("ProductionAreaID", self.ProductionAreaID())
        lst = lst.Filter("ProductionAreaStatusID", self.ProductionAreaStatusID())
        if (lst.length == 1) {
          self.StatusCssClass(lst[0].CssClass)
        }
      }
    }
    catch (ex) {

    }
  },
  TitleSet: function (self) {
    self.ResourceBookingDescription(self.Title())
  },

  //Rules
  RoomScheduleIDValid: function (Value, Rule, CtlError) {
    var RoomSchedule = CtlError.Object;
    if (RoomSchedule.ClashDetailList().length > 0) {
      CtlError.AddError("This booking is clashing with other bookings");
    }
  },
  CallTimeValid: function (Value, Rule, CtlError) {
    var ct = CtlError.Object.CallTime();
    var st = CtlError.Object.OnAirTimeStart();
    var et = CtlError.Object.OnAirTimeEnd();
    var wt = CtlError.Object.WrapTime();
    if (!CtlError.Object.CallTime()) {
      CtlError.AddError("Call Time is required");
    } else {
      if (ct && st) {
        if (OBMisc.Dates.IsAfter(ct, st)) {
          CtlError.AddError("Call Time must be before On Air Start Time");
        }
      }
      if (ct && et) {
        if (OBMisc.Dates.IsAfter(ct, et)) {
          CtlError.AddError("Call Time must be before On Air End Time");
        }
      }
      if (ct && wt) {
        if (OBMisc.Dates.IsAfter(ct, wt)) {
          CtlError.AddError("Call Time must be before Wrap Time");
        }
      }
    }
  },
  OnAirTimeStartValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.OnAirTimeStart();
    var oaet = CtlError.Object.OnAirTimeEnd();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      } else if (!CtlError.Object.OnAirTimeStart()) {
        CtlError.AddError("On Air Start Time is required");
      }
    }
  },
  OnAirEndTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.OnAirTimeStart();
    var oaet = CtlError.Object.OnAirTimeEnd();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      } else if (!CtlError.Object.OnAirTimeEnd()) {
        CtlError.AddError("On Air End Time is required");
      }
    }
  },
  WrapTimeValid: function (Value, Rule, CtlError) {
    var oaet = CtlError.Object.OnAirTimeEnd();
    var wrt = CtlError.Object.WrapTime();
    if (oaet && wrt) {
      if (OBMisc.Dates.IsAfter(oaet, wrt)) {
        CtlError.AddError("On Air End Time must be before Wrap End Time");
      } else if (!CtlError.Object.WrapTime()) {
        CtlError.AddError("Wrap Time is required");
      }
    }
  },
  CancellationSelectedValid: function (Value, Rule, CtlError) {
    var oaet = CtlError.Object.CancellationSelected();
    var wrt = CtlError.Object.WrapTime();
    if (CtlError.Object.CancellationSelected() && (!CtlError.Object.CancellationDate() || CtlError.Object.CancellationReason().trim().length == 0)) {
      CtlError.AddError("Cancellation details are required")
    }
  }

}

RoomScheduleAreaProductionHumanResourceBO = {
  //Other
  RoomScheduleAreaProductionHumanResourceBOToString: function (self) {
    if (self.HumanResource().trim().length > 0) {
      return self.HumanResource()
    } else {
      return self.Discipline() + (self.Position().trim().length > 0 ? ' (' + self.Position() + ')' : '')
    }
  },
  GetPositionList: function (ProductionHumanResource) {
    var lst = [];
    var area = ProductionHumanResource.GetParent()
    ClientData.ROSystemProductionAreaDisciplineList.Iterate(function (disc, discIndx) {
      if (disc.SystemID == area.SystemID()
        && disc.ProductionAreaID == area.ProductionAreaID()
        && disc.DisciplineID == ProductionHumanResource.DisciplineID()) {
        ClientData.ROSystemProductionAreaDisciplinePositionTypeList.Iterate(function (pos, indx) {
          if (pos.SystemProductionAreaDisciplineID == disc.SystemProductionAreaDisciplineID) {
            lst.push({ PositionID: pos.PositionID, Position: pos.Position })
          }
        })
      }
    })
    return lst;
  },
  getBookingTimes: function (productionHumanResource) {
    if (productionHumanResource.HumanResourceID()) {
      var ph = productionHumanResource.GetParent().RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", productionHumanResource.HumanResourceID());
      if (ph) {
        if (ph.StartDateTimeBuffer() && ph.EndDateTimeBuffer()) {
          var sd = new Date(ph.StartDateTimeBuffer()).format('dd MMM HH:mm');
          var ed = new Date(ph.EndDateTimeBuffer()).format('HH:mm');
          return sd + ' - ' + ed;
        }
      }
    };
    return "";
  },
  checkHumanResource: function (self) {
    try {
      var me = this
      var currentHRID = self.HumanResourceID(),
        previousHRID = self.PreviousHumanResourceID();
      //if previously null
      if (previousHRID == null) {
        var newPHRCount = self.GetParent().RoomScheduleAreaProductionHumanResourceList().Filter("HumanResourceID", currentHRID).length
        var phCount = self.GetParent().RoomScheduleAreaProductionHRBookingList().Filter("HumanResourceID", currentHRID).length
        //if not on the production then add the ProductionHR record
        if ((newPHRCount == 0 || newPHRCount == 1) && phCount == 0) { me.addProductionHR(self) }
      }
      else if (previousHRID != null) {
        //if the phr record previously had someone booked
        if (currentHRID == null) {
          //if this record is now null, then the person has been removed, but we must check if that previous hrid is still on the production
          var phrCount = self.GetParent().RoomScheduleAreaProductionHumanResourceList().Filter("HumanResourceID", previousHRID).length
          //if not on the production anymore, then remove the ProductionHR record
          if (phrCount == 0) { me.removeProductionHR(self, self.PreviousHumanResourceID()) }
        }
        else if (previousHRID != currentHRID) {
          //if this record has now been switched to another person, then check if the previous person is still on the production
          var prevPHRCount = self.GetParent().RoomScheduleAreaProductionHumanResourceList().Filter("HumanResourceID", previousHRID).length
          var newPHRCount = self.GetParent().RoomScheduleAreaProductionHumanResourceList().Filter("HumanResourceID", currentHRID).length

          //Replace Old with New: update old ProductionHR with new HRID and ResourceID
          if (prevPHRCount == 0 && newPHRCount >= 1) { me.switchProductionHR(self) }
          else if (prevPHRCount == 1 && newPHRCount == 1) {
            //Do Nothing: both still on production?
          }
          else {
            //if not on the production anymore, then remove the ProductionHR record
            if (prevPHRCount == 0) { me.removeProductionHR(self) }
            else {
              //after the above, check if the currentHRID is already on the production    
              //      if (newPHRCount == 0) {
              //        //if not on the production then add the ProductionHR record
              //        console.log({ action: "Generate Temp Schedule" })
              //        me.addProductionHR(self)
              //      } else {
              //        //he is already on the production, so do nothing
              //        console.log({ action: "Do Nothing: still on the production" })
            }
          }
        }
      }
    }
    catch (err) {
      OBMisc.Notifications.GritterError(err);
    }
    finally {
      RoomScheduleAreaBO.checkClashes(self.GetParent())
    }
  },
  addProductionHR: function (productionHumanResource) {
    var me = this
    var roomBooking = productionHumanResource.GetParent()
    var newPH = roomBooking.RoomScheduleAreaProductionHRBookingList.AddNew()
    newPH.ProductionSystemAreaID(roomBooking.ProductionSystemAreaID())
    newPH.HumanResourceID(productionHumanResource.HumanResourceID())
    newPH.ResourceID(productionHumanResource.ResourceID())
    newPH.ResourceBookingTypeID(3)
    newPH.RoomScheduleID(roomBooking.RoomScheduleID())
    newPH.StartDateTimeBuffer(roomBooking.CallTime())
    newPH.FreezeSetExpressions(true)
    newPH.StartDateTime(roomBooking.OnAirTimeStart())
    newPH.EndDateTime(roomBooking.OnAirTimeEnd())
    newPH.FreezeSetExpressions(false)
    newPH.EndDateTimeBuffer(roomBooking.WrapTime())
    newPH.IsCancelled(roomBooking.IsCancelled()) //calc
    newPH.IsTBC(false)
    newPH.ResourceBookingDescription(roomBooking.ResourceBookingDescription())
    newPH.StatusCssClass(roomBooking.StatusCssClass())
    newPH.CallTimeTimelineID(roomBooking.CallTimeTimelineID())
    newPH.OnAirTimeTimelineID(roomBooking.OnAirTimeTimelineID())
    newPH.WrapTimeTimelineID(roomBooking.WrapTimeTimelineID())
    RoomScheduleAreaBO.checkClashes(roomBooking, function (response) { }, function (response) { })
  },
  removeProductionHR: function (productionHumanResource, humanResourceID) {
    var me = this
    var roomBooking = productionHumanResource.GetParent()
    var ph = roomBooking.RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", humanResourceID)
    roomBooking.RoomScheduleAreaProductionHRBookingList.RemoveNoCheck(ph)
  },
  switchProductionHR: function (productionHumanResource) {
    var me = this;
    me.removeProductionHR(productionHumanResource, productionHumanResource.PreviousHumanResourceID())
    me.addProductionHR(productionHumanResource)
  },
  isValid: function (phr) {
    var phValid = false;
    if (phr.HumanResourceID()) {
      var ph = phr.GetParent().RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", phr.HumanResourceID())
      if (ph) {
        phValid = ph.IsValid()
      } else {
        phValid = false
      }
    } else {
      phValid = true;
    }
    return (phr.IsValid() && phValid)
  },
  isValidTitleText: function (phr) {
    return "This crew member has errors."
  },
  hasTimeMismatch: function (phr) {
    var rs = phr.GetParent()
    if (phr.HumanResourceID()) {
      var ph = rs.RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", phr.HumanResourceID())
      if (ph) {
        return !(ph.CallTimeMatch() & ph.OnAirStartMatch() & ph.OnAirEndMatch() & ph.WrapTimeMatch())
      }
    }
    return false
  },
  timeMismatchTitleText: function (phr) {
    return "This crew member has times which do not match that of the booking."
  },
  cancelledDescription: function (phr) {
    if (phr.IsCancelled()) {
      return new Date(phr.CancelledDate()).format('dd MMM yyyy HH:mm') + ': ' + phr.CancelledReason
    }
    return ""
  },

  //Cans
  canRemoveHumanResource: function (productionHumanResource) {
    let area = productionHumanResource.GetParent()
    if (area.IsReconciled()) {
      return false
    }
    else {
      return (productionHumanResource.HumanResourceID() ? true : false)
    }
  },
  canEdit: function (columnName, productionHumanResource) {
    let area = productionHumanResource.GetParent()
    switch (columnName) {
      case 'DisciplineID':
      case 'PositionID':
      case 'HumanResourceID':
      case "BookingTimes":
      case "IsCancelled":
      case "IsSelected":
        if (area.IsReconciled()) {
          return false
        }
        else if (productionHumanResource.GetParent().SystemID() == 5) {
          return productionHumanResource.IsNew()
        }
        else {
          return true
        }
        break;
      default:
        return true;
        break;
    }
  },

  //Set
  DisciplineIDBeforeSet: function (ProductionHumanResource) {
  },
  DisciplineIDSet: function (ProductionHumanResource) {
  },
  PositionTypeIDBeforeSet: function (ProductionHumanResource) {
    console.log(ProductionHumanResource)
  },
  PositionTypeIDSet: function (ProductionHumanResource) {
    console.log(ProductionHumanResource)
  },
  PositionIDBeforeSet: function (ProductionHumanResource) {
    console.log(ProductionHumanResource)
  },
  PositionIDSet: function (ProductionHumanResource) {
    console.log(ProductionHumanResource)
  },
  HumanResourceIDBeforeSet: function (ProductionHumanResource) {
    ProductionHumanResource.PreviousHumanResourceID(ProductionHumanResource.HumanResourceID())
    ProductionHumanResource.PreviousHumanResource(ProductionHumanResource.HumanResource())
    ProductionHumanResource.PreviousResourceID(ProductionHumanResource.ResourceID())
  },
  HumanResourceIDSet: function (ProductionHumanResource) {
    var me = this;
    //this fires too quickly
  },
  IsCancelledSet: function (ProductionHumanResource) {
    var roomBooking = ProductionHumanResource.GetParent()
    if (ProductionHumanResource.IsCancelled()) {
      ProductionHumanResource.CancelledDate(new Date())
      //ProductionHumanResource.CancelledBy(ViewModel.CurrentUserID())
    }
    else {
      ProductionHumanResource.CancelledDate(null)
      //ProductionHumanResource.CancelledBy(null)
      ProductionHumanResource.CancelledReason("")
    }
    if (ProductionHumanResource.HumanResourceID()) {
      var ph = roomBooking.RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", ProductionHumanResource.HumanResourceID())
      if (ph) {
        ph.IsCancelled(ProductionHumanResource.IsCancelled())
        ph.IgnoreClashesReason(ProductionHumanResource.CancelledReason())
      }
    }
  },

  //dropdowns-------

  //discipline
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterDisciplineIDRefreshAjax: function (args) { },
  onDisciplineIDSelected: function (item, businessObject) { },

  //position type
  triggerPositionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionTypeIDRefreshAjax: function (args) { },
  onPositionTypeIDSelected: function (item, businessObject) {
    //if (item) {
    //  businessObject.PositionTypeID(item.PositionTypeID)
    //  businessObject.PositionID(item.PositionID)
    //  businessObject.Position(item.Position)
    //} else {
    //  businessObject.PositionTypeID(null)
    //  businessObject.PositionID(null)
    //  businessObject.Position("")
    //}
  },

  //position
  triggerPositionIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionIDRefreshAjax: function (args) { },
  onPositionTypeIDSelected: function (item, businessObject) {
    if (item) {
      businessObject.PositionTypeID(item.PositionTypeID)
      businessObject.PositionID(item.PositionID)
      businessObject.Position(item.Position)
    } else {
      businessObject.PositionTypeID(null)
      businessObject.PositionID(null)
      businessObject.Position("")
    }
  },

  //hr
  triggerHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setHumanResourceCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.GetParent().OnAirTimeStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.GetParent().OnAirTimeEnd()).format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.PositionID = args.Object.PositionID()
    args.Data.PositionTypeID = args.Object.PositionTypeID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.CallTime = new Date(args.Object.GetParent().CallTime()).format("dd MMM yyyy HH:mm")
    args.Data.WrapTime = new Date(args.Object.GetParent().WrapTime()).format("dd MMM yyyy HH:mm")
  },
  afterHumanResourceRefreshAjax: function (args) {

  },
  onHumanResourceSelected: function (item, businessObject) {
    if (item) {
      businessObject.ResourceID(item.ResourceID)
    } else {
      businessObject.ResourceID(null)
    }
    //the value of 'this', in the context of this method is the dropdown control and not the RoomScheduleAreaProductionHumanResourceBO js object
    RoomScheduleAreaProductionHumanResourceBO.checkHumanResource(businessObject)
  },
  onCellCreate: function (element, index, columnData, businessObject) {
    let contentShiftCount = '<td style="text-align:right;"><h5>' + businessObject.ShiftCount.toString() + ' / 20 Shifts</h5></td>';
    element.parentElement.style.width = "600px"
    element.parentNode.className = "hr-availability-list"
    element.className = 'hr-availability';
    element.innerHTML = '<div class="avatar">' +
      '<img src="' + window.SiteInfo.InternalPhotosPath + businessObject.ImageFileName + '" alt="Avatar">' +
      '</div>' +
      '<div class="info">' +
      '<table class="table table-condensed no-border">' +
      '<tbody>' +
      //header
      '<tr>' +
      '<td style="text-align:left;"><h4>' + businessObject.HRName + '</h4></td>' +
      '<td style="text-align:center; vertical-align:middle;"><span class="' + businessObject.ContractTypeCss + '">' + businessObject.ContractType + '</span></td>' +
      '<td style="text-align:right;"><h5>' + businessObject.TotalHours.toString() + ' / ' + businessObject.RequiredHours.toString() + ' Hours</h5></td>' +
      (businessObject.SystemID == 2 ? contentShiftCount : '<td style="text-align:right;"></td>') +
      '</tr>' +
      //progress bar
      '<tr>' +
      '<td colspan="3">' +
      '<div class="progress small">' +
      '<div class="' + businessObject.ProgressBarCss + '" style="width:' + businessObject.HrsPercString + '"></div>' +
      '</div>' +
      '</td>' +
      '</tr>' +
      //other
      '<tr>' +
      '<td colspan="3"><span title="' + businessObject.ClashesTitle + '" class="' + businessObject.ClashesCss + '" style="width:100%;">' + businessObject.IsAvailableString + '</span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PublicHolidaysString + '" class="label label-info">' + businessObject.PublicHolidayCount.toString() + ' Public Holidays </span></td>' +
      '<td><span title="' + businessObject.OffWeekendsString + '" class="label label-info">' + businessObject.OffWeekendCount.toString() + ' Off Weekend/s </span></td>' +
      '<td><span title="' + businessObject.ConsecutiveDaysString + '" class="' + businessObject.ConsecutiveDaysCss + '">' + businessObject.ConsecutiveDays.toString() + ' Consecutive Days </span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PrevShiftEndString + '" class="' + businessObject.ShortfallPrevCss + '">' + businessObject.ShortfallPrevString + ' Shortfall </span></td>' +
      '<td></td>' +
      '<td><span title="' + businessObject.NextShiftStartString + '" class="' + businessObject.ShortfallNextCss + '">' + businessObject.ShortfallNextString + ' Shortfall </span></td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</div>'
  },

  //Rules
  ProductionHumanResourceIDValid: function (Value, Rule, CtlError) {
    var phr = CtlError.Object;
    if (phr.HumanResourceID()) {
      var psa = phr.GetParent();
      var productionHR = psa.RoomScheduleAreaProductionHRBookingList().Find("HumanResourceID", phr.HumanResourceID());
      if (productionHR) {
        if (productionHR.ClashDetailList().length > 0) {
          CtlError.AddError(phr.HumanResource() + " has clashes on his schedule");
          productionHR.ClashDetailList().Iterate(function (cls, clsIndx) {
            var sdString = new Date(cls.StartDateTime()).format('ddd dd MMM yy HH:mm')
            CtlError.AddError("clash: " + cls.ResourceBookingDescription() + ': ' + sdString);
          })
        }
      }
    }
  },
  CancelledDateValid: function (Value, Rule, CtlError) {
    var phr = CtlError.Object;
    if (phr.CancelledDate() && phr.CancelledReason().trim().length == 0) {
      CtlError.AddError("Cancelled Date and Cancelled Reason are required");
    }
  }
};

RoomScheduleAreaProductionHRBookingBO = {
  //other
  RoomScheduleAreaProductionHRBookingBOToString: function () {
    return "HR Booking"
  },
  updateMismatch: function (self) {
    var rs = self.GetParent()
    var ct = new Date(rs.CallTime()).getTime()
    var oas = new Date(rs.OnAirTimeStart()).getTime()
    var oae = new Date(rs.OnAirTimeEnd()).getTime()
    var wt = new Date(rs.WrapTime()).getTime()
    var cth = new Date(self.StartDateTimeBuffer()).getTime()
    var oash = new Date(self.StartDateTime()).getTime()
    var oaeh = new Date(self.EndDateTime()).getTime()
    var wth = new Date(self.EndDateTimeBuffer()).getTime()
    if (ct != cth) { self.CallTimeMatch(false) } else { self.CallTimeMatch(true) }
    if (oas != oash) { self.OnAirStartMatch(false) } else { self.OnAirStartMatch(true) }
    if (oae != oaeh) { self.OnAirEndMatch(false) } else { self.OnAirEndMatch(true) }
    if (wt != wth) { self.WrapTimeMatch(false) } else { self.WrapTimeMatch(true) }
  },

  //Set
  StartDateTimeBufferSet: function (hrBooking) {
    RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
  },
  StartDateTimeSet: function (hrBooking) {
    RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
    if (!hrBooking.FreezeSetExpressions()) {
      RoomScheduleAreaBO.checkClashes(hrBooking.GetParent(),
        function (response) {
          //do something
        },
        function (response) {
          //do something
        })
    }
  },
  EndDateTimeSet: function (hrBooking) {
    RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
    if (!hrBooking.FreezeSetExpressions()) {
      RoomScheduleAreaBO.checkClashes(hrBooking.GetParent(),
        function (response) {

        },
        function (response) {

        })
    }
  },
  EndDateTimeBufferSet: function (hrBooking) {
    RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
  },

  //Rules
  CallTimeValid: function (Value, Rule, CtlError) {
    var ct = CtlError.Object.StartDateTimeBuffer();
    var st = CtlError.Object.StartDateTime();
    var et = CtlError.Object.EndDateTime();
    var wt = CtlError.Object.EndDateTimeBuffer();
    if (ct && st) {
      if (OBMisc.Dates.IsAfter(ct, st)) {
        CtlError.AddError("Call Time must be before On Air Start Time");
      }
    }
    else if (!ct) {
      CtlError.AddError("Call Time is required");
    }
  },
  StartTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.StartDateTime();
    var oaet = CtlError.Object.EndDateTime();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      }
      else if (!CtlError.Object.StartDateTime()) {
        CtlError.AddError("On Air Start Time is required");
      }
    }
  },
  EndTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.StartDateTime();
    var oaet = CtlError.Object.EndDateTime();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      }
      else if (!CtlError.Object.EndDateTime()) {
        CtlError.AddError("On Air End Time is required");
      }
    }
  },
  WrapTimeValid: function (Value, Rule, CtlError) {
    var oaet = CtlError.Object.EndDateTime();
    var wrt = CtlError.Object.EndDateTimeBuffer();
    if (oaet && wrt) {
      if (OBMisc.Dates.IsAfter(oaet, wrt)) {
        CtlError.AddError("On Air End Time must be before Wrap End Time");
      }
      else if (!CtlError.Object.EndDateTimeBuffer()) {
        CtlError.AddError("Wrap Time is required");
      }
    }
  }
};

RoomScheduleChannelBO = {
  RoomScheduleChannelBOToString: function (self) {
    return self.ChannelShortName()
  },
  canDisplayInBookingDetails: function (channel, roomScheduleArea) {
    return ((channel.RoomScheduleID() == roomScheduleArea.RoomScheduleID()) && channel.RoomScheduleChannelID())
  }
}

ContentServicesDifferenceBO = {
  ContentServicesDifferenceBOToString: function (self) { return self.Title() },
  //Cans
  canView: function (FieldName, self) {
    switch (FieldName) {
      case "RemovedDiff":
        return self.PendingDelete()
        break;
      case "NotAddedDiff":
        return (!self.PendingDelete() && self.NotAdded())
        break;
      case "RoomDiff":
        return (!self.PendingDelete() && !self.NotAdded() && self.RoomDifferent())
        break;
      case "StartTimeDiff":
        return (!self.PendingDelete() && !self.NotAdded() && self.StartTimeDifferent())
        break;
      case "EndTimeDiff":
        return (!self.PendingDelete() && !self.NotAdded() && self.EndTimeDifferent())
        break;
      case "PendingDeleteButton":
        return self.PendingDelete()
        break;
      case "NotAddedButton":
        return (!self.PendingDelete() && self.NotAdded())
        break;
      case "UpdateButton":
        return (!self.PendingDelete() && !self.NotAdded() && (self.RoomDifferent() || self.StartTimeDifferent() || self.EndTimeDifferent()))
        break;
    }
  },
  //DataAccess
  get: function (options) {
    Singular.GetDataStateless("OBLib.Scheduling.Rooms.ContentServicesDifferenceList, OBLib", options.criteria, function (response) {
      if (response.Success) {
        if (options.onSuccess) { options.onSuccess.call(response.Data[0], response) }
      } else {
        if (options.onFail) { options.onFail(response) }
      }
    })
  },
  save: function (contentServicesDifferenceList, options) {
    var me = this;
    ViewModel.CallServerMethod("SaveContentServicesDifferenceList", {
      ContentServicesDifferenceList: contentServicesDifferenceList.Serialise()
    },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 150)
          if (options.onSuccess) { options.onSuccess.call(this, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onFail.call(this, response) }
        }
      });
  }
}

ContentServicesDifferenceListCriteriaBO = {
  StartDateSet: function (self) {
    self.IsProcessing(true)
    ContentServicesDifferenceBO.get({
      criteria: {
        StartDate: self.StartDate(), //we can get away with this kind of sin right here as this object is only used in a special case,
        EndDate: self.EndDate()
      },
      onSuccess: function (response) {
        ViewModel.ContentServicesDifferenceList.Set([])
        ViewModel.ContentServicesDifferenceList.Set(response.Data)
        self.IsProcessing(false)
        $("#ContentServicesDifferencesModal").modal()
      },
      onFail: function (response) {
        self.IsProcessing(false)
        OBMisc.Notifications.GritterError("Error", response.ErrorText, 500)
      }
    })
  },
  EndDateSet: function (self) {
    self.IsProcessing(true)
    ContentServicesDifferenceBO.get({
      criteria: {
        StartDate: self.StartDate(), //we can get away with this kind of sin right here as this object is only used in a special case,
        EndDate: self.EndDate()
      },
      onSuccess: function (response) {
        ViewModel.ContentServicesDifferenceList.Set([])
        ViewModel.ContentServicesDifferenceList.Set(response.Data)
        self.IsProcessing(false)
        $("#ContentServicesDifferencesModal").modal()
      },
      onFail: function (response) {
        self.IsProcessing(false)
        OBMisc.Notifications.GritterError("Error", response.ErrorText, 500)
      }
    })
  },
  previousDay: function (self) {
    if (!self.IsProcessing()) {
      var pd = moment(self.StartDate()).add(-1, 'days')
      self.StartDate(pd.toDate())
    }
  },
  nextDay: function (self) {
    if (!self.IsProcessing()) {
      var nd = moment(self.EndDate()).add(1, 'days')
      self.EndDate(nd.toDate())
    }
  }
}

RoomScheduleTemplateBO = {
  //Other
  RoomScheduleTemplateBOToString: function (self) { return self.Room() + " - " + self.Title() },

  updateAll: function (templat) {

  },
  checkClashes: function (template, afterSuccess, afterFail) {

  },

  //server methods
  saveFromResourceScheduler: function (template) {
    this.save(template, {
      onSuccess: function (response) {
        $("#RoomScheduleTemplateModal").modal('hide')
      },
      onFail: function (response) {

      }
    })
  },
  save: function (template, options) {
    var me = this;
    if (!template().IsProcessing()) {
      template().IsProcessing(true)
      ViewModel.CallServerMethod("SaveRoomScheduleTemplate", {
        RoomScheduleTemplate: template.Serialise()
      },
        function (response) {
          template().IsProcessing(false)
          if (response.Success) {
            template.Set(response.Data)
            OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 250)
            if (window.siteHubManager) {
              window.siteHubManager().roomBookingChanged()
              window.SiteHub.server.sendNotifications()
            }
            if (options.onSuccess) { options.onSuccess(response) }
          }
          else {
            OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
            if (options.onFail) { options.onFail(response) }
          }
        })
    }
  },
  get: function (options) {
    //Singular.GetDataStateless("OBLib.Scheduling.Rooms.RoomScheduleAreaList, OBLib",
    //  options.criteria,
    //  function (response) {
    //    if (response.Success) {
    //      if (options.onSuccess) { options.onSuccess(response) }
    //    } else {
    //      if (options.onFail) { options.onFail(response) }
    //    }
    //  })
  },
  doSetup(self, options) {
    return ViewModel.CallServerMethodPromise("SetupRoomScheduleTemplate", { RoomScheduleTemplate: self.Serialise() });
  },
  doSetupOld: function (self, options) {
    ViewModel.CallServerMethod("SetupRoomScheduleTemplate", {
      RoomScheduleTemplate: self.Serialise()
    },
      function (response) {
        if (response.Success) {
          self.Set(response.Data)
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else { if (options.onFail) { options.onFail(response) } }
      })
  },

  //Cans
  CanEdit: function (psa, columnName) {
    switch (columnName) {
      default:
        return true
        break;
    }
  },
  canSave: function (area) {
    return area.IsValid()
  },

  //Set
  CallTimeStartSet: function (area) {
  },
  StartDateTimeSet: function (area) {
  },
  EndDateTimeSet: function (area) {
  },
  WrapTimeEndSet: function (area) {
  },

  RoomIDSet: function (area) {
    //RoomScheduleAreaBO.updateAll(area)
  },
  triggerRoomAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.OnAirTimeStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.OnAirTimeEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    var me = this;
    if (selectedItem) {
      businessObject.Room(selectedItem.Room)
      businessObject.RoomID(selectedItem.RoomID)
      businessObject.ResourceID(selectedItem.ResourceID)

    } else {
      businessObject.Room("")
      businessObject.RoomID(null)
      businessObject.ResourceID(null)
    }
  },

  DebtorIDSet: function (self) {

  },
  triggerDebtorAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDebtorCriteriaBeforeRefresh: function (args) {

  },
  afterDebtorRefreshAjax: function (args) {

  },
  onDebtorSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.DebtorID(selectedItem.DebtorID)
      businessObject.Debtor(selectedItem.Debtor)
    } else {
      businessObject.DebtorID(null)
      businessObject.Debtor("")
    }
  },

  //Set
  SystemIDSet: function (self) {

  },
  ProductionAreaIDSet: function (self) {

  },
  TitleSet: function (self) {

  },

  //Rules
  RoomScheduleIDValid: function (Value, Rule, CtlError) {
    //var RoomSchedule = CtlError.Object;
    //if (RoomSchedule.ClashDetailList().length > 0) {
    //  CtlError.AddError("This booking is clashing with other bookings");
    //}
  },
  CallTimeValid: function (Value, Rule, CtlError) {
    var ct = CtlError.Object.CallTime();
    var st = CtlError.Object.OnAirTimeStart();
    var et = CtlError.Object.OnAirTimeEnd();
    var wt = CtlError.Object.WrapTime();
    if (!CtlError.Object.CallTime()) {
      CtlError.AddError("Call Time is required");
    } else {
      if (ct && st) {
        if (OBMisc.Dates.IsAfter(ct, st)) {
          CtlError.AddError("Call Time must be before On Air Start Time");
        }
      }
      if (ct && et) {
        if (OBMisc.Dates.IsAfter(ct, et)) {
          CtlError.AddError("Call Time must be before On Air End Time");
        }
      }
      if (ct && wt) {
        if (OBMisc.Dates.IsAfter(ct, wt)) {
          CtlError.AddError("Call Time must be before Wrap Time");
        }
      }
    }
  },
  OnAirTimeStartValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.OnAirTimeStart();
    var oaet = CtlError.Object.OnAirTimeEnd();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      } else if (!CtlError.Object.OnAirTimeStart()) {
        CtlError.AddError("On Air Start Time is required");
      }
    }
  },
  OnAirEndTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.OnAirTimeStart();
    var oaet = CtlError.Object.OnAirTimeEnd();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      } else if (!CtlError.Object.OnAirTimeEnd()) {
        CtlError.AddError("On Air End Time is required");
      }
    }
  },
  WrapTimeValid: function (Value, Rule, CtlError) {
    var oaet = CtlError.Object.OnAirTimeEnd();
    var wrt = CtlError.Object.WrapTime();
    if (oaet && wrt) {
      if (OBMisc.Dates.IsAfter(oaet, wrt)) {
        CtlError.AddError("On Air End Time must be before Wrap End Time");
      } else if (!CtlError.Object.WrapTime()) {
        CtlError.AddError("Wrap Time is required");
      }
    }
  },

  //methods
  async newRoomScheduleTemplate(resource, roomScheduleTemplateID, event) {
    ViewModel.CurrentRoomScheduleTemplate(new RoomScheduleTemplateObject());
    ViewModel.CurrentRoomScheduleTemplate().ResourceID(resource.ResourceID);
    ViewModel.CurrentRoomScheduleTemplate().Room(resource.ResourceName);
    ViewModel.CurrentRoomScheduleTemplate().RoomID(resource.RoomID);
    ViewModel.CurrentRoomScheduleTemplate().OnAirTimeStart(event.data.eventProps.time.format('dd MMM yyyy HH:mm'));
    ViewModel.CurrentRoomScheduleTemplate().OnAirTimeEnd(event.data.eventProps.time.format('dd MMM yyyy HH:mm'));
    ViewModel.CurrentRoomScheduleTemplate().TemplateID(roomScheduleTemplateID);
    try {
      let data = await RoomScheduleTemplateBO.doSetup(ViewModel.CurrentRoomScheduleTemplate());
      ViewModel.CurrentRoomScheduleTemplate.Set(data);
      this.showModal();
      OBMisc.Notifications.GritterSuccess("Setup Completed", "", 250);
    }
    catch (errorText) {
      OBMisc.Notifications.GritterError("Error During Setup", errorText, 1000);
    }
  },
  showModal() {
    $("#RoomScheduleTemplateModal").off('shown.bs.modal');
    $("#RoomScheduleTemplateModal").on('shown.bs.modal', function () {

    });
    $("#RoomScheduleTemplateModal").on('hidden.bs.modal', function () {
      ViewModel.CurrentRoomScheduleTemplate(null);
    });
    $("#RoomScheduleTemplateModal").modal();
  },
  hideModal() {
    $("#RoomScheduleTemplateModal").modal("hide")
  }
};

RORoomScheduleAreaBO = {
  get: (options) => {
    Singular.GetDataStateless("OBLib.Rooms.ReadOnly.RORoomBookingList",
      {
        RoomScheduleID: options.criteria.roomScheduleID,
        ProductionSystemAreaID: options.criteria.productionSystemAreaID
      },
      (response) => {
        if (response.Success) {
          ViewModel.CurrentRORoomBooking.Set(response.Data[0])
          RORoomScheduleAreaBO.showModal()
          if (options.onSuccess) { options.onSuccess(response) }
        }
      })
  },
  showModal: function () {
    $("#CurrentRORoomScheduleModal").off('shown.bs.modal')
    $("#CurrentRORoomScheduleModal").on('shown.bs.modal', function () {
      OBMisc.UI.activateTab('Crew')
    })
    $("#CurrentRORoomScheduleModal").modal()
  },
  hideModal: function () {
    $("#CurrentRORoomScheduleModal").modal("hide")
  }
}

//#endregion

//#region Reports

ROPSAReportBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.ProductionSystemAreas.ReadOnly.ROPSAReportList",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          if (options.onFail) { options.onFail(response) }
        }
      })
  }
}

//#endregion

//#region Generic

ROPSACrewGroupBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Scheduling.Generic.ReadOnly.ROPSACrewGroupList", options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Error Fetching Crew Groups", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  }
}

//#endregion