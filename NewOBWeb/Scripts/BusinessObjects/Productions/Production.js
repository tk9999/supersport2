﻿ProductionBO = {

   Production_SetDefaultProductionDescription: function(self) {
    ProductionHelper.Production.Methods.SetDefaultProductionDescription(self);
  },

   ProductionToString: function(self) { return self.Title() },

   ProductionTypeIDChecks: function(self) {
    if (!self.ProductionTypeID()) {
      self.EventTypeID(null);
    } else {
      var ROProductionType = ClientData.ROProductionTypeList.Find('ProductionTypeID', self.ProductionTypeID())
      var ROEventType = null;
      if (ROProductionType) {
        ROEventType = ClientData.ROEventTypeList.Filter('EventTypeID', self.EventTypeID())
      }
      if (ROEventType.length == 0) {
        self.EventTypeID(null);
      }
    }
  },

   CheckCanReconcile: function(self) {
    ProductionHelper.Production.Rules.CheckCanReconcile(self);
  },

   SetupAutoServices: function(self) {
    ProductionHelper.Production.Events.ProductionVenueIDSet(self);
  },

   SetVenueConfirmedDate: function(self) { ProductionHelper.Production.Methods.SetVenueConfirmedDate(self) },

   PlayStartSet: function(self) { ProductionHelper.Production.Methods.UpdateTx(self) },

   PlayEndSet: function(self) { ProductionHelper.Production.Methods.UpdateTx(self) },

}

AccessShiftBO = {
  CheckTimeChangedReason: function (Value, Rule, RuleArgs) {
    if (RuleArgs.Object.MustValidateAccessShift()) {
      if (RuleArgs.Object.PlannedTimeChangedReason() == "" && RuleArgs.Object.AccessFlagID() > 0) {
        // RuleArgs.AddError("Reason Required");
        RuleArgs.AddWarning("Reason Required");
        return;
      }
    }
  },
  CheckShiftType: function (Value, Rule, RuleArgs) {
    return;
  },
  CheckDiscipline: function (Value, Rule, RuleArgs) {
    var AccessShiftOptionInd = RuleArgs.Object.AccessShiftOptionInd();
    if (!RuleArgs.IgnoreResults) {
      if (RuleArgs.Object.AccessShiftOptionInd() == true && (RuleArgs.Object.DisciplineID() == null || RuleArgs.Object.DisciplineID() == 0)) {
        RuleArgs.AddError("Discipline is required");
        return;
      }
    }
  }
}