﻿ManualOBProductionBO = {

  TitleValid: function (Value, Rule, CtlError) {
    ManualOBProduction.TitleValid(Value, Rule, CtlError)
  },

  TeamsPlayingValid: function (Value, Rule, CtlError) {
    ManualOBProduction.TeamsPlayingValid(Value, Rule, CtlError)
  },

  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    ManualOBProduction.ProductionTypeIDValid(Value, Rule, CtlError)
  },

  ProductionTypeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.ProductionTypeValid(Value, Rule, CtlError)
  },

  EventTypeIDValid: function (Value, Rule, CtlError) {
    ManualOBProduction.EventTypeIDValid(Value, Rule, CtlError)
  },

  EventTypeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.EventTypeValid(Value, Rule, CtlError)
  },

  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    ManualOBProduction.ProductionVenueIDValid(Value, Rule, CtlError)
  },

  ProductionVenueValid: function (Value, Rule, CtlError) {
    ManualOBProduction.ProductionVenueValid(Value, Rule, CtlError)
  },

  LiveStartTimeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.LiveStartTimeValid(Value, Rule, CtlError)
  },

  LiveEndTimeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.LiveEndTimeValid(Value, Rule, CtlError)
  },

  CallTimeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.CallTimeValid(Value, Rule, CtlError)
  },

  OnAirStartTimeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.OnAirStartTimeValid(Value, Rule, CtlError)
  },

  OnAirEndTimeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.OnAirEndTimeValid(Value, Rule, CtlError)
  },

  WrapTimeValid: function (Value, Rule, CtlError) {
    ManualOBProduction.WrapTimeValid(Value, Rule, CtlError)
  },

  RoomIDValid: function (Value, Rule, CtlError) {
    ManualOBProduction.RoomIDValid(Value, Rule, CtlError)
  },

  RoomIDSet: function (self) {
    ManualOBProduction.RoomIDSet(self)
  },

  SpecValid: function (Value, Rule, CtlError) {
    ManualOBProduction.SpecValid(Value, Rule, CtlError)
  },

}

ManualStudioProductionBO = {

  TitleValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.TitleValid(Value, Rule, CtlError)
  },

  TeamsPlayingValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.TeamsPlayingValid(Value, Rule, CtlError)
  },

  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.ProductionTypeIDValid(Value, Rule, CtlError)
  },

  EventTypeIDValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.EventTypeIDValid(Value, Rule, CtlError)
  },

  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.ProductionVenueIDValid(Value, Rule, CtlError)
  },

  LiveStartTimeValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.LiveStartTimeValid(Value, Rule, CtlError)
  },

  LiveEndTimeValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.LiveEndTimeValid(Value, Rule, CtlError)
  },

  CallTimeValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.CallTimeValid(Value, Rule, CtlError)
  },

  OnAirStartTimeValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.OnAirStartTimeValid(Value, Rule, CtlError)
  },

  OnAirEndTimeValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.OnAirEndTimeValid(Value, Rule, CtlError)
  },

  WrapTimeValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.WrapTimeValid(Value, Rule, CtlError)
  },

  RoomIDValid: function (Value, Rule, CtlError) {
    ManualStudioProduction.RoomIDValid(Value, Rule, CtlError)
  },

  RoomIDSet: function (self) {
    ManualStudioProduction.RoomIDSet(self)
  },

  OnAirStartTimeSet: function (self) {
    ManualStudioProduction.OnAirStartTimeSet(self)
  },

  OnAirEndTimeSet: function (self) {
    ManualStudioProduction.OnAirEndTimeSet(self)
  },

}
