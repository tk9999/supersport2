﻿ROProductionAreaAllowedDisciplineBO = new function () {
  this.SelectedIndSet = function (ROProductionAreaAllowedDiscipline) {
    console.log(ROProductionAreaAllowedDiscipline);
  }
};

ROContractTypeBO = new function () {
  this.SelectedIndSet = function (ROContractType) {
    console.log(ROContractType);
  }
};

ROAllowedDisciplineSelectBO = new function () {
  this.OnSelect = function (ROAllowedDisciplineSelect, element) {

    ROAllowedDisciplineSelect.IsSelected(!ROAllowedDisciplineSelect.IsSelected());

    if (ROAllowedDisciplineSelect.IsSelected() && ROAllowedDisciplineSelect.Quantity() == 0) {
      ROAllowedDisciplineSelect.Quantity(1);
    };

    if (!ROAllowedDisciplineSelect.IsSelected()) {
      ROAllowedDisciplineSelect.Quantity(0);
    };

    var elem = $(element).parent('div.discipline-position');
    var quantControl = elem.find('input.round-editor');
    if (quantControl.length > 0) {
      $(quantControl).focus();
    };

  };
  this.IsSelectedSet = function (ROAllowedDisciplineSelect) {
    console.log(ROAllowedDisciplineSelect);
  };
  this.GetSelectedCss = function (ROAllowedDisciplineSelect) {
    if (ROAllowedDisciplineSelect.IsSelected()) {
      return "discipline-position selected"
    } else {
      return "discipline-position"
    }
  };
  this.GetDisciplinePositionHTML = function (ROAllowedDisciplineSelect) {
    if (ROAllowedDisciplineSelect.IsSelected()) {
      return '<i class="fa fa-check-square-o"></i> ' + ROAllowedDisciplineSelect.DisciplinePosition();
    } else {
      return ROAllowedDisciplineSelect.DisciplinePosition();
    };
  };
};

ROEventTypePagedListCriteriaBO = {
  ProductionTypeSet: function (self) { },
  EventTypeSet: function (self) { }
}

ROSystem = {
  IsSelectedSet: function (ROSystem) {

  }
}

ROChannelBO = {
  IsSelectedSet: function (self) {
    console.log(self)
  }
}

RORoomTypeReportBO = {
  RORoomTypeReportBOToString: function (self) { return self.RoomType() },
  get: function (options) {
    Singular.GetDataStateless("OBLib.Maintenance.Rooms.ReadOnly.RORoomTypeReportList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {

        }
        else {
          OBMisc.Notifications.GritterError("Error Fetching Room Types", response.ErrorText, 1000)
        }
      })
  }
}

RORoomTypeReportRoomBO = {
  RORoomTypeReportRoomBOToString: function (self) { return self.Room() },
  get: function (options) {
    Singular.GetDataStateless("OBLib.Maintenance.Rooms.ReadOnly.RORoomTypeReportRoomList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Error Fetching Room", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  }
}

RODisciplineReportBO = {
  RODisciplineReportBOToString: function (self) {
    return self.Discipline()
  },
  get: function (options) {
    Singular.GetDataStateless("OBLib.Maintenance.General.ReadOnly.RODisciplineReportList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Error Fetching Discipline", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  }
}