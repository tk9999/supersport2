﻿CrewScheduleDetail = {
  TimesheetDateValid: function (Value, Rule, CtlError) {
    var csd = CtlError.Object;
    if (!OBMisc.Dates.SameDay(csd.TimesheetDate(), csd.StartDateTime())) {
      CtlError.AddError("Schedule Date must be the same as Start Date");
    }
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    var csd = CtlError.Object;
    if (OBMisc.Dates.DifferenceInMinutes(csd.EndDateTime(), csd.StartDateTime()) < 0) {
      CtlError.AddError('Start Time must be before End Time');
    };
    var lc = CrewScheduleDetail.GetLocalClashes(csd);
    lc.Iterate(function (Err, Ind) {
      CtlError.AddError(Err);
    });
  },
  HasClashesValid: function (Value, Rule, CtlError) {
    var csd = CtlError.Object;
    var Errors = ""
    var Clashes = CrewScheduleDetail.GetLocalClashes(csd);
    if (Clashes.length > 0) {
      Clashes.Iterate(function (Clash, Index) {
        //CtlError.AddError(Clash)
        Errors += ((Errors.length == 0) ? "" : ", ") + Clash
      });
    }
    if (Clashes.length > 0) {
      csd.HasClashesInd(true)
    }
    Errors = null;
    csd = null;
  },
  GetLocalClashes: function (csd) {
    var Errors = [];
    var ProductionHR = csd.GetParent();
    ProductionHR.CrewScheduleDetailBaseList().Iterate(function (iCSD, Index) {
      if (csd.Guid() != iCSD.Guid()) {
        var Overlap = OBMisc.Dates.DatesOverlapExclusive(csd.StartDateTime(), csd.EndDateTime(), iCSD.StartDateTime(), iCSD.EndDateTime());
        if (Overlap) {
          Errors.push('Clash with ' + iCSD.ProductionTimelineType());
        }
      }
    })
    ProductionHR = null;
    return Errors;
  },
  GetCrewScheduleDetailClash: function () {

  }
}