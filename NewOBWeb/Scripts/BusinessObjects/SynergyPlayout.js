﻿SynergySchedulePlayoutBO = {
  canUpdate: function (obj) {
    //if (!obj.HumanResourceShiftScheduleID()) {
    return true
    //} else {
    //  return false
    //}
  },
  canUpload: function (obj) {
    //var hasChannel = obj.IsSelected()
    //if (obj.RoomID() && obj.CallTime() && obj.WrapTime() && !obj.IsProcessing() && obj.RoomClashCount() == 0 && hasChannel) {
    return true
    //}
    //return false
  },
  sendRequest: function (obj) {
    //obj.IsProcessing(true)
    //ViewModel.DisableUpload(true)
    //ViewModel.CallServerMethod("RoomAllocationRequestSynergyEventPlayout", {
    //  allocationRequest: obj.Serialise()
    //}, function (response) {
    //  if (response.Success) {
    //    window.SiteHub.server.sendNotifications()
    //    OBMisc.Notifications.GritterSuccess('Allocated Successfully', SynergySchedulePlayoutBO.GetToString(obj), 350, 'bottom-right')
    //  }
    //  else {
    //    OBMisc.Notifications.GritterError('Allocation Failed', '<p>' + SynergySchedulePlayoutBO.GetToString(obj) + '</p>' +
    //                                                           '<p>' + response.ErrorText + '</p>',
    //                                      1000, 'bottom-right')
    //  }
    //  OBMisc.Knockout.updateObservableFromJSON(obj, response.Data, ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"])
    //  obj.IsProcessing(false)
    //  ViewModel.DisableUpload(false)
    //})
  },
  canClear: function (obj) {
    return (obj.RoomScheduleChannelID() ? true : false)
  },
  clearIconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-eraser"
    }
  },
  clear: function (obj) {
    //obj.IsProcessing(true)
    //ViewModel.DisableUpload(true)
    //ViewModel.CallServerMethod("RoomAllocationRequestPlayoutClear", {
    //  allocationRequest: obj.Serialise()
    //}, function (response) {
    //  if (response.Success) {
    //    window.SiteHub.server.sendNotifications()
    //    OBMisc.Notifications.GritterSuccess('Cleared Successfully', SynergySchedulePlayoutBO.GetToString(obj), 350, 'bottom-right')
    //  }
    //  else {
    //    OBMisc.Notifications.GritterError('Clearing Failed', '<p>' + SynergySchedulePlayoutBO.GetToString(obj) + '</p>' +
    //                                                           '<p>' + response.ErrorText + '</p>',
    //                                      1000, 'bottom-right')
    //  }
    //  OBMisc.Knockout.updateObservableFromJSON(obj, response.Data, ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"])
    //  obj.IsProcessing(false)
    //  ViewModel.DisableUpload(false)
    //})
  },

  //other
  GetToString: function (self) {
    return self.Title()
  },
  SynergyEventBOToString: function (self) {
    return self.Title()
  },
  FilterByOwner: function (list, instance) {
    var results = []
    return list
  },
  ChannelShortNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    var css = "";
    if (self.PrimaryChannelInd()) {
      css = "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-info bold";
    } else {
      css = "btn btn-xs btn-default bold";
    }
    //if (ViewModel.BigTextMode()) {
    //  css += " black-text big-text-mode"
    //}
    return css;
  },
  ChannelShortNameHtml: function (self) {
    return self.ChannelShortName();
  },
  ChannelNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    if (self.PrimaryChannelInd()) {
      return "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-info bold";
    } else {
      return "btn btn-xs btn-default bold";
    }
  },
  ChannelNameHtml: function (self) {
    return self.ChannelName();
  },
  updateBookingTimesText: function (obj) {
    var s = ""
    if (obj.CallTime()) {
      s = new Date(obj.CallTime()).format("dd MMM HH:mm")
    } else {
      s = new Date(obj.FirstStart()).format("dd MMM HH:mm")
    }
    if (obj.CallTime()) {
      s += " - " + new Date(obj.WrapTime()).format("dd MMM HH:mm")
    } else {
      s += " - " + new Date(obj.LastEnd()).format("dd MMM HH:mm")
    }
    obj.BookingTimes(s)
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  bookingTimesButtonCSS: function (obj) {
    //&& obj.RoomClashCount() == 0
    if (obj.TimesValid()) {
      return "btn btn-xs btn-default btn-block"
    } else {
      return "btn btn-xs btn-danger btn-block animated rubberBand infinite go"
    }
  },
  SCCROperatorCss: function (obj) {
    if (obj.SCCROperatorClashCount() > 0) {
      return "border-error"
    }
  },
  MCRControllerCss: function (obj) {
    if (obj.MCRControllerClashCount() > 0) {
      return "border-error"
    }
  },
  buttonHTML: function (obj) {
    return obj.BookingCount().toString()
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  getEventStatusCssClass: function (self) {
    if (self.EventStatus() == "L") {
      return 'btn btn-xs btn-block event-live'
    } else if (self.EventStatus() == "D") {
      return 'btn btn-xs btn-block event-delayed'
    } else if (self.EventStatus() == "P") {
      return 'btn btn-xs btn-block event-premier'
    } else {
      return 'btn btn-xs btn-block event-default'
    }
  },
  getEventStatusFull: function (self) {
    if (self.EventStatus() == "L") {
      return 'Live'
    } else if (self.EventStatus() == "D") {
      return 'Delayed'
    } else if (self.EventStatus() == "P") {
      return 'Premier'
    } else {
      return ''
    }
  },
  sponsorshipButtonCss: function (self) {
    if (self.SponsorshipCount() > 0) {
      return "btn btn-xs btn-danger"
    } else {
      return "btn btn-xs btn-default"
    }
  },

  //cans
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case 'ResourceIDMCRController':
      case 'ResourceIDSCCROperator':
        return (instance.SystemID() == 5)
        break
      default:
        return !instance.IsProcessing()
        break;
    }
  },

  //set
  SystemIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  ProductionAreaIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  RoomIDSet: function (SynergyEvent) {
    if (SynergyEvent.RoomID()) {
      var callTimeRequired = false,
          wrapTimeRequired = false;

      //get area settings
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', SynergyEvent.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', SynergyEvent.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', SynergyEvent.RoomID())

      var roomSetting = null;
      if (roomSettings.length == 1) {
        roomSetting = roomSettings[0]
        callTimeRequired = roomSetting.RoomCallTimeRequired
        wrapTimeRequired = roomSetting.RoomWrapTimeRequired
      }
      else if (roomSettings.length > 1) {
        //check dates
        roomSettings.Iterate(function (rm, rmIndx) {
          var effectiveDate = new moment(rm.EffectiveDate)
          var effectiveEndDate = (rm.EffectiveEndDate ? new moment(rm.EffectiveEndDate) : new moment(SynergyEvent.LastEnd()))
          var dateRange = moment().range(effectiveDate, effectiveEndDate)
          var startDate = moment(SynergyEvent.FirstStart())
          if (startDate.isBetween(effectiveDate, effectiveEndDate)) {
            roomSetting = rm
            return //exit loop
          }
        })
      } else {
        callTimeRequired = false;
        wrapTimeRequired = false;
        roomSetting = null;
      }

      //call and wrap time variables
      var ctMinutes = 0,
          wtMinutes = 0;

      //check call time
      if (callTimeRequired) {
        if (roomSetting) {
          var ct = moment(new Date(ViewModel.CurrentSynergyEventPlayout().FirstStart())).add(-roomSetting.CallTimeMinutes, 'minutes').toDate()
          SynergyEvent.CallTime(ct)
        }
        else {
          SynergyEvent.CallTime(ViewModel.CurrentSynergyEventPlayout().FirstStart())
        }
      }
      else {
        SynergyEvent.CallTime(ViewModel.CurrentSynergyEventPlayout().FirstStart())
      }

      //check wrap time
      if (wrapTimeRequired) {
        if (roomSetting) {
          var wt = moment(new Date(ViewModel.CurrentSynergyEventPlayout().LastEnd())).add(roomSetting.WrapTimeMinutes, 'minutes').toDate()
          SynergyEvent.WrapTime(wt)
        }
        else {
          SynergyEvent.WrapTime(ViewModel.CurrentSynergyEventPlayout().LastEnd())
        }
      }
      else {
        SynergyEvent.WrapTime(ViewModel.CurrentSynergyEventPlayout().LastEnd())
      }

    }
    else {
      //SynergyEvent.RoomClashCount(0)
      SynergyEvent.CallTime(null)
      SynergyEvent.WrapTime(null)
    }
    SynergyEvent.OnAirTimeStart(SynergyEvent.ScheduleDateTime())
    SynergyEvent.OnAirTimeEnd(SynergyEvent.ScheduleEndDate())
    Singular.Validation.CheckRules(SynergyEvent)
  },
  CallTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  FirstStartSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             //SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      //SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    SynergyEvent.IsProcessing(false)
  },
  LastEndSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             //SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      //SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             //SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      //SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             //SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      //SynergyEvent.SCCROperatorClashCount(0)
    }
    if (SynergyEvent.LastEnd()) {
      var lastEnd = new Date(SynergyEvent.LastEnd())
      var newWrapTime = new Date(lastEnd.getTime() + (1 * 36e5)) //Default Wrap Time to 1 hr after end time
      SynergyEvent.WrapTime(newWrapTime)
    }
    SynergyEvent.IsProcessing(false)
  },
  WrapTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  ResourceIDControllerSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDSCCROperator(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.SCCROperatorClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },
  ResourceIDInternSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDMCRController(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.MCRControllerClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },

  //Rules
  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = false
      var onAirStartAfterEnd = false
      if (obj.StartDateTime()) {
        ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.StartDateTime())
        onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.StartDateTime(), obj.EndDateTime())
      }
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.EndDateTime(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      ////clash
      //if (obj.RoomClashCount() > 0) {
      //  CtlError.AddError('Room not available')
      //}

      if (invalidTimeCount > 0) {
        obj.TimesValid(false)
      } else { obj.TimesValid(true) }

    }
  },
  HoursValid: function (Value, Rule, CtlError) {
    //var obj = CtlError.Object;
    //if (obj.CallTime() != undefined && obj.LastEnd() != undefined) {
    //  var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
    //  areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
    //  var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
    //  var callTime = new Date(obj.CallTime())
    //  var endTime = new Date(obj.LastEnd())
    //  var hours = Math.abs(endTime - callTime) / 36e5 // 36e5 = 60*60*1000(Convert ms to hrs)
    //  if (roomSettings.length > 0) {
    //    if (hours > roomSettings[0].MaxHoursPerShift) {
    //      CtlError.AddError('Shift is more than ' + roomSettings[0].MaxHoursPerShift + ' Hours long')
    //    }
    //  }
    //}
  },
  ResourceIDControllerValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    //if (obj.ResourceIDSCCROperator() && obj.SCCROperatorClashCount() > 0) {
    //  //var lst = ClientData.ROSCCROperatorList.Criteria().SCCROperator(obj.SCCROperator())
    //  var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
    //  var hrName = (hr ? hr.HRName : "SCCR Operator")
    //  CtlError.AddError(hrName + ' has clashes')
    //}
    //else if (obj.ResourceIDSCCROperator()) {
    //  var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
    //  if (hr) {
    //    var hrName = (hr ? hr.HRName : "SCCR Operator")
    //    var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
    //    areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
    //    var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
    //    if (roomSettings.length > 0) {
    //      if (hr.TimeToNextShift() > roomSettings[0].MinShortfallBetweenShifts) {
    //        CtlError.AddError(hrName + ' has less than ' + roomSettings[0].MinShortfallBetweenShifts + ' hours before next shift')
    //      }
    //    }
    //  }
    //}
  },
  ResourceIDInternValid: function (Value, Rule, CtlError) {
    //var obj = CtlError.Object;
    //var hr = ClientData.ROMCRControllerList.Find("ResourceID", obj.ResourceIDMCRController())
    //if (hr) {
    //  var hrName = (hr ? hr.HRName : "MCR Controller")
    //  if (obj.ResourceIDMCRController() && obj.MCRControllerClashCount() > 0) {
    //    CtlError.AddError(hrName + ' has clashes')
    //  }
    //  //else if (hr.TimeToNextShift() > 10) {
    //  //  CtlError.AddError(hrName + ' has less than 10 hours before this shift')
    //  //}
    //}
  },
  ControllerBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDController()) {
      if (obj.ControllerDurationWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Shift Duration too long)')
      }
      if (obj.ControllerShortfallWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Rest Time not adequate)')
      }
      if (obj.ControllerTimesheetWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Max Timesheet Hours exceeded)')
      }
    }
  },
  InternBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDIntern()) {
      if (obj.ControllerDurationWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Shift Duration too long)')
      }
      if (obj.ControllerShortfallWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Rest Time not adequate)')
      }
      if (obj.ControllerTimesheetWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Max Timesheet Hours exceeded)')
      }
    }
  },

  //RoomID
  beforeRoomIDDropDownShown: function () {
    console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    //if (selectedItem && (selectedItem.IsGap || selectedItem.IsSufficientTime)) {
    //  businessObject.RoomClashCount(0)
    //} else {
    //  businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    //}
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.FirstStart()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //ControllerID
  setResourceIDControllerCriteriaBeforeRefresh: function (args) {
    var sd = (args.Object.StartDateTime() ? args.Object.StartDateTime() : args.Object.ScheduleDateTime())
    var ed = (args.Object.EndDateTime() ? args.Object.EndDateTime() : args.Object.ScheduleEndDate())
    args.Data.StartDateTime = new Date(sd).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(ed).format("dd MMM yyyy HH:mm")
  },
  triggerResourceIDControllerAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  afterResourceIDControllerRefresh: function (args) {
    //args.Object.IsProcessing(false)
  },
  onControllerSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ControllerShortfallWarning(selectedItem.ShortfallWarning)
      businessObject.ControllerDurationWarning(selectedItem.ShiftHoursWarning)
      businessObject.ControllerTimesheetWarning(selectedItem.TimesheetHoursWarning)
    } else {
      businessObject.ControllerShortfallWarning(false)
      businessObject.ControllerDurationWarning(false)
      businessObject.ControllerTimesheetWarning(false)
      businessObject.ControllerClashCount(0)
    }
  },
  onResourceIDControllerCellCreate: function (cellElement, index, ColumnData, RowData) {
    switch (ColumnData[0]) {
      case "ShiftHoursWarning":
        cellElement.innerHTML = ""
        var c = document.createElement('span')
        if (RowData.ShiftHoursWarning) {
          c.className = "btn btn-xs btn-warning"
          c.innerHTML = "<i class='fa fa-warning'></i>"
        }
        cellElement.appendChild(c)
        break;
      case "ShortfallWarning":
        cellElement.innerHTML = ""
        var d = document.createElement('span')
        if (RowData.ShortfallWarning) {
          d.className = "btn btn-xs btn-warning"
          d.innerHTML = "<i class='fa fa-warning'></i>"
        }
        cellElement.appendChild(d)
        break;
      case "TimesheetHoursWarning":
        cellElement.innerHTML = ""
        var e = document.createElement('span')
        if (RowData.TimesheetHoursWarning) {
          e.className = "btn btn-xs btn-warning"
          e.innerHTML = "<i class='fa fa-warning'></i>"
        }
        cellElement.appendChild(e)
        break;
    }
    //console.log({ ce: cellElement, i: index, cn: ColumnData, rd: RowData })
  },

}

SynergySchedulePlayoutRoomBookingBO = {
  canUpdate: function (obj) {
    //if (!obj.HumanResourceShiftScheduleID()) {
    return true
    //} else {
    //  return false
    //}
  },
  canUpload: function (obj) {
    //var hasChannel = obj.IsSelected()
    //if (obj.RoomID() && obj.CallTime() && obj.WrapTime() && !obj.IsProcessing() && obj.RoomClashCount() == 0 && hasChannel) {
    return true
    //}
    //return false
  },
  sendRequest: function (obj) {
    //obj.IsProcessing(true)
    //ViewModel.DisableUpload(true)
    //ViewModel.CallServerMethod("RoomAllocationRequestSynergyEventPlayout", {
    //  allocationRequest: obj.Serialise()
    //}, function (response) {
    //  if (response.Success) {
    //    window.SiteHub.server.sendNotifications()
    //    OBMisc.Notifications.GritterSuccess('Allocated Successfully', SynergySchedulePlayoutBO.GetToString(obj), 350, 'bottom-right')
    //  }
    //  else {
    //    OBMisc.Notifications.GritterError('Allocation Failed', '<p>' + SynergySchedulePlayoutBO.GetToString(obj) + '</p>' +
    //                                                           '<p>' + response.ErrorText + '</p>',
    //                                      1000, 'bottom-right')
    //  }
    //  OBMisc.Knockout.updateObservableFromJSON(obj, response.Data, ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"])
    //  obj.IsProcessing(false)
    //  ViewModel.DisableUpload(false)
    //})
  },
  canClear: function (obj) {
    return (obj.RoomScheduleChannelID() ? true : false)
  },
  clearIconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-eraser"
    }
  },
  clear: function (obj) {
    //obj.IsProcessing(true)
    //ViewModel.DisableUpload(true)
    //ViewModel.CallServerMethod("RoomAllocationRequestPlayoutClear", {
    //  allocationRequest: obj.Serialise()
    //}, function (response) {
    //  if (response.Success) {
    //    window.SiteHub.server.sendNotifications()
    //    OBMisc.Notifications.GritterSuccess('Cleared Successfully', SynergySchedulePlayoutBO.GetToString(obj), 350, 'bottom-right')
    //  }
    //  else {
    //    OBMisc.Notifications.GritterError('Clearing Failed', '<p>' + SynergySchedulePlayoutBO.GetToString(obj) + '</p>' +
    //                                                           '<p>' + response.ErrorText + '</p>',
    //                                      1000, 'bottom-right')
    //  }
    //  OBMisc.Knockout.updateObservableFromJSON(obj, response.Data, ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"])
    //  obj.IsProcessing(false)
    //  ViewModel.DisableUpload(false)
    //})
  },

  //other
  GetToString: function (self) {
    return self.Title()
  },
  SynergyEventBOToString: function (self) {
    return self.Title()
  },
  FilterByOwner: function (list, instance) {
    var results = []
    return list
  },
  ChannelShortNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    var css = "";
    if (self.PrimaryChannelInd()) {
      css = "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-info bold";
    } else {
      css = "btn btn-xs btn-default bold";
    }
    //if (ViewModel.BigTextMode()) {
    //  css += " black-text big-text-mode"
    //}
    return css;
  },
  ChannelShortNameHtml: function (self) {
    return self.ChannelShortName();
  },
  ChannelNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    if (self.PrimaryChannelInd()) {
      return "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-info bold";
    } else {
      return "btn btn-xs btn-default bold";
    }
  },
  ChannelNameHtml: function (self) {
    return self.ChannelName();
  },
  updateBookingTimesText: function (obj) {
    var s = ""
    if (obj.CallTime()) {
      s = new Date(obj.CallTime()).format("dd MMM HH:mm")
    } else {
      s = new Date(obj.FirstStart()).format("dd MMM HH:mm")
    }
    if (obj.CallTime()) {
      s += " - " + new Date(obj.WrapTime()).format("dd MMM HH:mm")
    } else {
      s += " - " + new Date(obj.LastEnd()).format("dd MMM HH:mm")
    }
    obj.BookingTimes(s)
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  bookingTimesButtonCSS: function (obj) {
    //&& obj.RoomClashCount() == 0
    if (obj.TimesValid()) {
      return "btn btn-xs btn-default btn-block"
    } else {
      return "btn btn-xs btn-danger btn-block animated rubberBand infinite go"
    }
  },
  SCCROperatorCss: function (obj) {
    if (obj.SCCROperatorClashCount() > 0) {
      return "border-error"
    }
  },
  MCRControllerCss: function (obj) {
    if (obj.MCRControllerClashCount() > 0) {
      return "border-error"
    }
  },
  buttonHTML: function (obj) {
    return obj.BookingCount().toString()
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  getEventStatusCssClass: function (self) {
    if (self.EventStatus() == "L") {
      return 'btn btn-xs btn-block event-live'
    } else if (self.EventStatus() == "D") {
      return 'btn btn-xs btn-block event-delayed'
    } else if (self.EventStatus() == "P") {
      return 'btn btn-xs btn-block event-premier'
    } else {
      return 'btn btn-xs btn-block event-default'
    }
  },
  sponsorshipButtonCss: function (self) {
    if (self.SponsorshipCount() > 0) {
      return "btn btn-xs btn-danger"
    } else {
      return "btn btn-xs btn-default"
    }
  },

  //cans
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case 'ResourceIDMCRController':
      case 'ResourceIDSCCROperator':
        return (instance.SystemID() == 5)
        break
      default:
        return !instance.IsProcessing()
        break;
    }
  },

  //set
  SystemIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  ProductionAreaIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  RoomIDSet: function (SynergyEvent) {
    if (SynergyEvent.RoomID()) {
      var callTimeRequired = false,
          wrapTimeRequired = false;

      //get area settings
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', SynergyEvent.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', SynergyEvent.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', SynergyEvent.RoomID())

      var roomSetting = null;
      if (roomSettings.length == 1) {
        roomSetting = roomSettings[0]
        callTimeRequired = roomSetting.RoomCallTimeRequired
        wrapTimeRequired = roomSetting.RoomWrapTimeRequired
      }
      else if (roomSettings.length > 1) {
        //check dates
        roomSettings.Iterate(function (rm, rmIndx) {
          var effectiveDate = new moment(rm.EffectiveDate)
          var effectiveEndDate = (rm.EffectiveEndDate ? new moment(rm.EffectiveEndDate) : new moment(SynergyEvent.LastEnd()))
          var dateRange = moment().range(effectiveDate, effectiveEndDate)
          var startDate = moment(SynergyEvent.FirstStart())
          if (startDate.isBetween(effectiveDate, effectiveEndDate)) {
            roomSetting = rm
            return //exit loop
          }
        })
      } else {
        callTimeRequired = false;
        wrapTimeRequired = false;
        roomSetting = null;
      }

      //call and wrap time variables
      var ctMinutes = 0,
          wtMinutes = 0;

      //check call time
      if (callTimeRequired) {
        if (roomSetting) {
          var ct = moment(new Date(ViewModel.CurrentSynergyEventPlayout().FirstStart())).add(-roomSetting.CallTimeMinutes, 'minutes').toDate()
          SynergyEvent.CallTime(ct)
        }
        else {
          SynergyEvent.CallTime(ViewModel.CurrentSynergyEventPlayout().FirstStart())
        }
      }
      else {
        SynergyEvent.CallTime(ViewModel.CurrentSynergyEventPlayout().FirstStart())
      }

      //check wrap time
      if (wrapTimeRequired) {
        if (roomSetting) {
          var wt = moment(new Date(ViewModel.CurrentSynergyEventPlayout().LastEnd())).add(roomSetting.WrapTimeMinutes, 'minutes').toDate()
          SynergyEvent.WrapTime(wt)
        }
        else {
          SynergyEvent.WrapTime(ViewModel.CurrentSynergyEventPlayout().LastEnd())
        }
      }
      else {
        SynergyEvent.WrapTime(ViewModel.CurrentSynergyEventPlayout().LastEnd())
      }

    }
    else {
      //SynergyEvent.RoomClashCount(0)
      SynergyEvent.CallTime(null)
      SynergyEvent.WrapTime(null)
    }
    SynergyEvent.OnAirTimeStart(SynergyEvent.ScheduleDateTime())
    SynergyEvent.OnAirTimeEnd(SynergyEvent.ScheduleEndDate())
    Singular.Validation.CheckRules(SynergyEvent)
  },
  CallTimeSet: function (SynergyEvent) {
    //this.updateBookingTimesText(SynergyEvent)
  },
  FirstStartSet: function (SynergyEvent) {
    //this.updateBookingTimesText(SynergyEvent)
    //SynergyEvent.IsProcessing(true)
    ////Check Room availability
    //if (SynergyEvent.RoomID()) {
    //  var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
    //  OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         //SynergyEvent.RoomClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                       })
    //}
    //else {
    //  //SynergyEvent.RoomClashCount(0)
    //}
    ////Check Controller availability
    //if (SynergyEvent.ResourceIDMCRController()) {
    //  OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         SynergyEvent.MCRControllerClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                       })
    //}
    //else {
    //  SynergyEvent.MCRControllerClashCount(0)
    //}
    ////Check Operator availability
    //if (SynergyEvent.ResourceIDSCCROperator()) {
    //  OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         SynergyEvent.SCCROperatorClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                       })
    //}
    //else {
    //  SynergyEvent.SCCROperatorClashCount(0)
    //}
    //SynergyEvent.IsProcessing(false)
  },
  LastEndSet: function (SynergyEvent) {
    //this.updateBookingTimesText(SynergyEvent)
    //SynergyEvent.IsProcessing(true)
    ////Check Room availability
    //if (SynergyEvent.RoomID()) {
    //  var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
    //  OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         //SynergyEvent.RoomClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                       })
    //}
    //else {
    //  //SynergyEvent.RoomClashCount(0)
    //}
    ////Check Controller availability
    //if (SynergyEvent.ResourceIDMCRController()) {
    //  OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         //SynergyEvent.MCRControllerClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                       })
    //}
    //else {
    //  //SynergyEvent.MCRControllerClashCount(0)
    //}
    ////Check Operator availability
    //if (SynergyEvent.ResourceIDSCCROperator()) {
    //  OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         //SynergyEvent.SCCROperatorClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         SynergyEvent.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                       })
    //}
    //else {
    //  //SynergyEvent.SCCROperatorClashCount(0)
    //}
    //if (SynergyEvent.LastEnd()) {
    //  var lastEnd = new Date(SynergyEvent.LastEnd())
    //  var newWrapTime = new Date(lastEnd.getTime() + (1 * 36e5)) //Default Wrap Time to 1 hr after end time
    //  SynergyEvent.WrapTime(newWrapTime)
    //}
    //SynergyEvent.IsProcessing(false)
  },
  WrapTimeSet: function (SynergyEvent) {
    //this.updateBookingTimesText(SynergyEvent)
  },
  ResourceIDControllerSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDSCCROperator(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.SCCROperatorClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },
  ResourceIDInternSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDMCRController(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.MCRControllerClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },

  //Rules
  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = false
      var onAirStartAfterEnd = false
      if (obj.StartDateTime()) {
        ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.StartDateTime())
        onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.StartDateTime(), obj.EndDateTime())
      }
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.EndDateTime(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      ////clash
      //if (obj.RoomClashCount() > 0) {
      //  CtlError.AddError('Room not available')
      //}

      if (invalidTimeCount > 0) {
        obj.TimesValid(false)
      } else { obj.TimesValid(true) }

    }
  },
  HoursValid: function (Value, Rule, CtlError) {
    //var obj = CtlError.Object;
    //if (obj.CallTime() != undefined && obj.LastEnd() != undefined) {
    //  var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
    //  areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
    //  var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
    //  var callTime = new Date(obj.CallTime())
    //  var endTime = new Date(obj.LastEnd())
    //  var hours = Math.abs(endTime - callTime) / 36e5 // 36e5 = 60*60*1000(Convert ms to hrs)
    //  if (roomSettings.length > 0) {
    //    if (hours > roomSettings[0].MaxHoursPerShift) {
    //      CtlError.AddError('Shift is more than ' + roomSettings[0].MaxHoursPerShift + ' Hours long')
    //    }
    //  }
    //}
  },
  ResourceIDControllerValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    //if (obj.ResourceIDSCCROperator() && obj.SCCROperatorClashCount() > 0) {
    //  //var lst = ClientData.ROSCCROperatorList.Criteria().SCCROperator(obj.SCCROperator())
    //  var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
    //  var hrName = (hr ? hr.HRName : "SCCR Operator")
    //  CtlError.AddError(hrName + ' has clashes')
    //}
    //else if (obj.ResourceIDSCCROperator()) {
    //  var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
    //  if (hr) {
    //    var hrName = (hr ? hr.HRName : "SCCR Operator")
    //    var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
    //    areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
    //    var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
    //    if (roomSettings.length > 0) {
    //      if (hr.TimeToNextShift() > roomSettings[0].MinShortfallBetweenShifts) {
    //        CtlError.AddError(hrName + ' has less than ' + roomSettings[0].MinShortfallBetweenShifts + ' hours before next shift')
    //      }
    //    }
    //  }
    //}
  },
  ResourceIDInternValid: function (Value, Rule, CtlError) {
    //var obj = CtlError.Object;
    //var hr = ClientData.ROMCRControllerList.Find("ResourceID", obj.ResourceIDMCRController())
    //if (hr) {
    //  var hrName = (hr ? hr.HRName : "MCR Controller")
    //  if (obj.ResourceIDMCRController() && obj.MCRControllerClashCount() > 0) {
    //    CtlError.AddError(hrName + ' has clashes')
    //  }
    //  //else if (hr.TimeToNextShift() > 10) {
    //  //  CtlError.AddError(hrName + ' has less than 10 hours before this shift')
    //  //}
    //}
  },
  ControllerBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDController()) {
      if (obj.ControllerDurationWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Shift Duration too long)')
      }
      if (obj.ControllerShortfallWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Rest Time not adequate)')
      }
      if (obj.ControllerTimesheetWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Max Timesheet Hours exceeded)')
      }
    }
  },
  InternBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDIntern()) {
      if (obj.ControllerDurationWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Shift Duration too long)')
      }
      if (obj.ControllerShortfallWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Rest Time not adequate)')
      }
      if (obj.ControllerTimesheetWarning() && obj.ControllerBookedReason().trim().length == 0) {
        CtlError.AddError('Booking Reason is required (Max Timesheet Hours exceeded)')
      }
    }
  },

  //RoomID
  beforeRoomIDDropDownShown: function () {
    console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    //if (selectedItem && (selectedItem.IsGap || selectedItem.IsSufficientTime)) {
    //  businessObject.RoomClashCount(0)
    //} else {
    //  businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    //}
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.FirstStart()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //ControllerID
  setResourceIDControllerCriteriaBeforeRefresh: function (args) {
    var sd = (args.Object.StartDateTime() ? args.Object.StartDateTime() : args.Object.ScheduleDateTime())
    var ed = (args.Object.EndDateTime() ? args.Object.EndDateTime() : args.Object.ScheduleEndDate())
    args.Data.StartDateTime = new Date(sd).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(ed).format("dd MMM yyyy HH:mm")
  },
  triggerResourceIDControllerAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  afterResourceIDControllerRefresh: function (args) {
    //args.Object.IsProcessing(false)
  },
  onControllerSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ControllerShortfallWarning(selectedItem.ShortfallWarning)
      businessObject.ControllerDurationWarning(selectedItem.ShiftHoursWarning)
      businessObject.ControllerTimesheetWarning(selectedItem.TimesheetHoursWarning)
    } else {
      businessObject.ControllerShortfallWarning(false)
      businessObject.ControllerDurationWarning(false)
      businessObject.ControllerTimesheetWarning(false)
      businessObject.ControllerClashCount(0)
    }
  },
  onResourceIDControllerCellCreate: function (cellElement, index, ColumnData, RowData) {
    switch (ColumnData[0]) {
      case "ShiftHoursWarning":
        cellElement.innerHTML = ""
        var c = document.createElement('span')
        if (RowData.ShiftHoursWarning) {
          c.className = "btn btn-xs btn-warning"
          c.innerHTML = "<i class='fa fa-warning'></i>"
        }
        cellElement.appendChild(c)
        break;
      case "ShortfallWarning":
        cellElement.innerHTML = ""
        var d = document.createElement('span')
        if (RowData.ShortfallWarning) {
          d.className = "btn btn-xs btn-warning"
          d.innerHTML = "<i class='fa fa-warning'></i>"
        }
        cellElement.appendChild(d)
        break;
      case "TimesheetHoursWarning":
        cellElement.innerHTML = ""
        var e = document.createElement('span')
        if (RowData.TimesheetHoursWarning) {
          e.className = "btn btn-xs btn-warning"
          e.innerHTML = "<i class='fa fa-warning'></i>"
        }
        cellElement.appendChild(e)
        break;
    }
    //console.log({ ce: cellElement, i: index, cn: ColumnData, rd: RowData })
  },

}

SynergyEventPlayoutBO = {
  canUpdate: function (obj) {
    if (!obj.HumanResourceShiftScheduleID()) {
      return true
    } else {
      return false
    }
  },
  canUpload: function (obj) {
    var hasChannel = obj.IsSelected()
    if (obj.RoomID() && obj.CallTime() && obj.WrapTime() && !obj.IsProcessing() && obj.RoomClashCount() == 0 && hasChannel) {
      return true
    }
    return false
  },
  sendRequest: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    ViewModel.CallServerMethod("RoomAllocationRequestSynergyEventPlayout", {
      allocationRequest: obj.Serialise()
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        OBMisc.Notifications.GritterSuccess('Allocated Successfully', SynergyEventPlayoutBO.GetToString(obj), 350, 'bottom-right')
      }
      else {
        OBMisc.Notifications.GritterError('Allocation Failed', '<p>' + SynergyEventPlayoutBO.GetToString(obj) + '</p>' +
                                                               '<p>' + response.ErrorText + '</p>',
                                          1000, 'bottom-right')
      }
      OBMisc.Knockout.updateObservableFromJSON(obj, response.Data, ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"])
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  canClear: function (obj) {
    return (obj.RoomScheduleChannelID() ? true : false)
  },
  clearIconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-eraser"
    }
  },
  clear: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    ViewModel.CallServerMethod("RoomAllocationRequestPlayoutClear", {
      allocationRequest: obj.Serialise()
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        OBMisc.Notifications.GritterSuccess('Cleared Successfully', SynergyEventPlayoutBO.GetToString(obj), 350, 'bottom-right')
      }
      else {
        OBMisc.Notifications.GritterError('Clearing Failed', '<p>' + SynergyEventPlayoutBO.GetToString(obj) + '</p>' +
                                                               '<p>' + response.ErrorText + '</p>',
                                          1000, 'bottom-right')
      }
      OBMisc.Knockout.updateObservableFromJSON(obj, response.Data, ["IsDirty", "IsSelfDirty", "IsNew", "IsSelfNew", "IsValid", "IsSelfValid", "IsProcessing"])
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  //other
  GetToString: function (self) {
    return self.Title()
  },
  SynergyEventBOToString: function (self) {
    return self.Title()
  },
  FilterByOwner: function (list, instance) {
    var results = []
    return list
  },
  ChannelShortNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    var css = "";
    if (self.PrimaryChannelInd()) {
      css = "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-info bold";
    } else {
      css = "btn btn-xs btn-default bold";
    }
    //if (ViewModel.BigTextMode()) {
    //  css += " black-text big-text-mode"
    //}
    return css;
  },
  ChannelShortNameHtml: function (self) {
    return self.ChannelShortName();
  },
  ChannelNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    if (self.PrimaryChannelInd()) {
      return "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-info bold";
    } else {
      return "btn btn-xs btn-default bold";
    }
  },
  ChannelNameHtml: function (self) {
    return self.ChannelName();
  },
  updateBookingTimesText: function (obj) {
    var s = ""
    if (obj.CallTime()) {
      s = new Date(obj.CallTime()).format("dd MMM HH:mm")
    } else {
      s = new Date(obj.FirstStart()).format("dd MMM HH:mm")
    }
    if (obj.CallTime()) {
      s += " - " + new Date(obj.WrapTime()).format("dd MMM HH:mm")
    } else {
      s += " - " + new Date(obj.LastEnd()).format("dd MMM HH:mm")
    }
    obj.BookingTimes(s)
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  bookingTimesButtonCSS: function (obj) {
    if (obj.TimesValid() && obj.RoomClashCount() == 0) {
      return "btn btn-xs btn-default btn-block"
    } else {
      return "btn btn-xs btn-danger btn-block animated rubberBand infinite go"
    }
  },
  SCCROperatorCss: function (obj) {
    if (obj.SCCROperatorClashCount() > 0) {
      return "border-error"
    }
  },
  MCRControllerCss: function (obj) {
    if (obj.MCRControllerClashCount() > 0) {
      return "border-error"
    }
  },
  buttonHTML: function (obj) {
    return obj.BookingCount().toString()
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },

  //cans
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case 'ResourceIDMCRController':
      case 'ResourceIDSCCROperator':
        return (instance.SystemID() == 5)
        break
      default:
        return !instance.IsProcessing()
        break;
    }
  },

  //set
  SystemIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  ProductionAreaIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  RoomIDSet: function (SynergyEvent) {
    if (SynergyEvent.RoomID()) {
      var callTimeRequired = false,
          wrapTimeRequired = false;

      //get area settings
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', SynergyEvent.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', SynergyEvent.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', SynergyEvent.RoomID())

      var roomSetting = null;
      if (roomSettings.length == 1) {
        roomSetting = roomSettings[0]
        callTimeRequired = roomSetting.RoomCallTimeRequired
        wrapTimeRequired = roomSetting.RoomWrapTimeRequired
      }
      else if (roomSettings.length > 1) {
        //check dates
        roomSettings.Iterate(function (rm, rmIndx) {
          var effectiveDate = new moment(rm.EffectiveDate)
          var effectiveEndDate = (rm.EffectiveEndDate ? new moment(rm.EffectiveEndDate) : new moment(SynergyEvent.LastEnd()))
          var dateRange = moment().range(effectiveDate, effectiveEndDate)
          var startDate = moment(SynergyEvent.FirstStart())
          if (startDate.isBetween(effectiveDate, effectiveEndDate)) {
            roomSetting = rm
            return //exit loop
          }
        })
      } else {
        callTimeRequired = false;
        wrapTimeRequired = false;
        roomSetting = null;
      }

      //call and wrap time variables
      var ctMinutes = 0,
          wtMinutes = 0;

      //check call time
      if (callTimeRequired) {
        if (roomSetting) {
          var ct = moment(new Date(ViewModel.CurrentSynergyEventPlayout().FirstStart())).add(-roomSetting.CallTimeMinutes, 'minutes').toDate()
          SynergyEvent.CallTime(ct)
        }
        else {
          SynergyEvent.CallTime(ViewModel.CurrentSynergyEventPlayout().FirstStart())
        }
      }
      else {
        SynergyEvent.CallTime(ViewModel.CurrentSynergyEventPlayout().FirstStart())
      }

      //check wrap time
      if (wrapTimeRequired) {
        if (roomSetting) {
          var wt = moment(new Date(ViewModel.CurrentSynergyEventPlayout().LastEnd())).add(roomSetting.WrapTimeMinutes, 'minutes').toDate()
          SynergyEvent.WrapTime(wt)
        }
        else {
          SynergyEvent.WrapTime(ViewModel.CurrentSynergyEventPlayout().LastEnd())
        }
      }
      else {
        SynergyEvent.WrapTime(ViewModel.CurrentSynergyEventPlayout().LastEnd())
      }

    }
    else {
      SynergyEvent.RoomClashCount(0)
      SynergyEvent.CallTime(null)
      SynergyEvent.WrapTime(null)
    }
    SynergyEvent.OnAirTimeStart(SynergyEvent.ScheduleDateTime())
    SynergyEvent.OnAirTimeEnd(SynergyEvent.ScheduleEndDate())
    Singular.Validation.CheckRules(SynergyEvent)
  },
  CallTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  FirstStartSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    SynergyEvent.IsProcessing(false)
  },
  LastEndSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    if (SynergyEvent.LastEnd()) {
      var lastEnd = new Date(SynergyEvent.LastEnd())
      var newWrapTime = new Date(lastEnd.getTime() + (1 * 36e5)) //Default Wrap Time to 1 hr after end time
      SynergyEvent.WrapTime(newWrapTime)
    }
    SynergyEvent.IsProcessing(false)
  },
  WrapTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  ResourceIDSCCROperatorSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDSCCROperator(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.SCCROperatorClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },
  ResourceIDMCRControllerSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDMCRController(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.MCRControllerClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },

  //Rules
  LiveDateValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.LiveDate();
    var leti = CtlError.Object.LiveEndDateTime();
    if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live Start Time must be before Live End Time");
      }
    }
  },
  LiveEndDateTimeValid: function (Value, Rule, CtlError) {
    var leti = CtlError.Object.LiveEndDateTime();
    if (!leti) {
      CtlError.AddError("Live End Time is required");
    }
  },
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.OBInd() && CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  },
  TeamsValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.OBInd() && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  },
  TeamsPlayingValid: function (Value, Rule, CtlError) {
    if ((CtlError.Object.ProductionAreaID() == 1) && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams Playing is required");
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = false
      var onAirStartAfterEnd = false
      if (obj.FirstStart()) {
        ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.FirstStart())
        onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.FirstStart(), obj.LastEnd())
      }
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.LastEnd(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      //clash
      if (obj.RoomClashCount() > 0) {
        CtlError.AddError('Room not available')
      }

      if (invalidTimeCount > 0) {
        obj.TimesValid(false)
      } else { obj.TimesValid(true) }

    }
  },
  HoursValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.CallTime() != undefined && obj.LastEnd() != undefined) {
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
      var callTime = new Date(obj.CallTime())
      var endTime = new Date(obj.LastEnd())
      var hours = Math.abs(endTime - callTime) / 36e5 // 36e5 = 60*60*1000(Convert ms to hrs)
      if (roomSettings.length > 0) {
        if (hours > roomSettings[0].MaxHoursPerShift) {
          CtlError.AddError('Shift is more than ' + roomSettings[0].MaxHoursPerShift + ' Hours long')
        }
      }
    }
  },
  ResourceIDSCCROperatorValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDSCCROperator() && obj.SCCROperatorClashCount() > 0) {
      //var lst = ClientData.ROSCCROperatorList.Criteria().SCCROperator(obj.SCCROperator())
      var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
      var hrName = (hr ? hr.HRName : "SCCR Operator")
      CtlError.AddError(hrName + ' has clashes')
    }
    else if (obj.ResourceIDSCCROperator()) {
      var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
      if (hr) {
        var hrName = (hr ? hr.HRName : "SCCR Operator")
        var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
        areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
        var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
        if (roomSettings.length > 0) {
          if (hr.TimeToNextShift() > roomSettings[0].MinShortfallBetweenShifts) {
            CtlError.AddError(hrName + ' has less than ' + roomSettings[0].MinShortfallBetweenShifts + ' hours before next shift')
          }
        }
      }
    }
  },
  ResourceIDMCRControllerValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    var hr = ClientData.ROMCRControllerList.Find("ResourceID", obj.ResourceIDMCRController())
    if (hr) {
      var hrName = (hr ? hr.HRName : "MCR Controller")
      if (obj.ResourceIDMCRController() && obj.MCRControllerClashCount() > 0) {
        CtlError.AddError(hrName + ' has clashes')
      }
      //else if (hr.TimeToNextShift() > 10) {
      //  CtlError.AddError(hrName + ' has less than 10 hours before this shift')
      //}
    }
  },
  MCRControllerBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDMCRController() && obj.HumanResourceShiftIDMCRController() && obj.MCRControllerShiftDuration() > 14) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('MCR Controller booking reason is required')
      }
    }
    if (obj.ResourceIDMCRController() && obj.HumanResourceShiftIDMCRController() && obj.MCRControllerRestHours() < 10) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('MCR Controller booking reason is required')
      }
    }
  },
  SCCRControllerBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDSCCROperator() && obj.HumanResourceShiftIDSCCROperator() && obj.SCCRControllerShiftDuration() > 14) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('SCCR Controller booking reason is required')
      }
    }
    if (obj.ResourceIDSCCROperator() && obj.HumanResourceShiftIDSCCROperator() && obj.SCCRControllerRestHours() < 10) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('SCCR Controller booking reason is required')
      }
    }
  },

  //RoomID
  beforeRoomIDDropDownShown: function () {
    console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    if (selectedItem && (selectedItem.IsGap || selectedItem.IsSufficientTime)) {
      businessObject.RoomClashCount(0)
    } else {
      businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    }
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.FirstStart()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //Controller
  onMCRControllerSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      if (!selectedItem.IsAvailable) {
        businessObject.MCRControllerClashCount(businessObject.MCRControllerClashCount() + 1)
      } else {
        businessObject.MCRControllerClashCount(0)
        businessObject.HumanResourceShiftIDMCRController(selectedItem.HumanResourceShiftID)
        businessObject.MCRControllerShiftDuration(selectedItem.ExpectedShiftDuration)
        businessObject.MCRControllerRestHours(selectedItem.TimeToNextShift)
      }
    }
    else {
      businessObject.MCRControllerClashCount(0)
      businessObject.HumanResourceShiftIDMCRController(null)
      businessObject.MCRControllerShiftDuration(0)
      businessObject.MCRControllerRestHours(0)
    }
  },
  triggerMCRControllerAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setMCRControllerCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterMCRControllerRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  //Intern
  onSCCROperatorSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      if (!selectedItem.IsAvailable) {
        businessObject.SCCROperatorClashCount(businessObject.SCCROperatorClashCount() + 1)
      } else {
        businessObject.SCCROperatorClashCount(0)
        businessObject.HumanResourceShiftIDSCCROperator(selectedItem.HumanResourceShiftID)
        businessObject.SCCRControllerShiftDuration(selectedItem.ExpectedShiftDuration)
        businessObject.SCCRControllerRestHours(selectedItem.TimeToNextShift)
      }
    }
    else {
      businessObject.SCCROperatorClashCount(0)
      businessObject.HumanResourceShiftIDSCCROperator(null)
      businessObject.SCCRControllerShiftDuration(0)
      businessObject.SCCRControllerRestHours(0)
    }
  },
  triggerSCCROperatorAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setSCCROperatorCriteriaBeforeRefresh: function (args) {
    if (args.Object.FirstStart()) {
      args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    } else {
      args.Data.StartDateTime = new Date(args.Object.CallTime()).format("dd MMM yyyy HH:mm")
    }
    if (args.Object.LastEnd()) {
      args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
    } else {
      args.Data.StartDateTime = new Date(args.Object.WrapTime()).format("dd MMM yyyy HH:mm")
    }
  },
  afterSCCROperatorRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  }

}

SynergyEventBO = {
  //other
  GetToString: function (self) {
    return self.Title()
  },
  SynergyEventBOToString: function (self) {
    return self.Title()
  },
  FilterByOwner: function (list, instance) {
    var results = []
    return list
  },
  sendRequest: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("RoomAllocationRequestSynergyEventPlayout", {
      allocationRequest: ser
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        //success message
        obj.BookingCount(response.Data.BookingCount)
        obj.SynergyEventChannelList().Iterate(function (itm, indx) {
          response.Data.SynergyEventChannelList.Iterate(function (ch, chIndx) {
            if (itm.ImportedEventChannelID() == ch.ImportedEventChannelID) {
              itm.RoomScheduleCount(ch.RoomScheduleCount)
            }
          })
        })
        OBMisc.Notifications.GritterSuccess('Allocated Successfully', RoomAllocatorBO.GetToString(obj), 1500)
      }
      else {
        //error message
        OBMisc.Notifications.GritterError('Allocation Failed', response.ErrorText, 3000)
      }
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  sendRequestOB: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("OBAllocationRequestSynergyEvent", {
      allocationRequest: ser
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        obj.ProductionID(response.Data.ProductionID)
        obj.ProductionRefNo(response.Data.ProductionRefNo)
        OBMisc.Notifications.GritterSuccess('Allocated Successfully', SynergyEventBO.GetToString(obj), 500)
      }
      else {
        //error message
        OBMisc.Notifications.GritterError('Allocation Failed', response.ErrorText, 2000)
      }
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  ChannelShortNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    var css = "";
    if (self.PrimaryChannelInd()) {
      css = "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-info bold";
    } else {
      css = "btn btn-xs btn-default bold";
    }
    //if (ViewModel.BigTextMode()) {
    //  css += " black-text big-text-mode"
    //}
    return css;
  },
  ChannelShortNameHtml: function (self) {
    return self.ChannelShortName();
  },
  ChannelNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    if (self.PrimaryChannelInd()) {
      return "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-info bold";
    } else {
      return "btn btn-xs btn-default bold";
    }
  },
  ChannelNameHtml: function (self) {
    return self.ChannelName();
  },
  updateBookingTimesText: function (obj) {
    var s = ""
    if (obj.CallTime()) {
      s = new Date(obj.CallTime()).format("dd MMM HH:mm")
    } else {
      s = new Date(obj.FirstStart()).format("dd MMM HH:mm")
    }
    if (obj.CallTime()) {
      s += " - " + new Date(obj.WrapTime()).format("dd MMM HH:mm")
    } else {
      s += " - " + new Date(obj.LastEnd()).format("dd MMM HH:mm")
    }
    obj.BookingTimes(s)
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  bookingTimesButtonCSS: function (obj) {
    if (obj.TimesValid() && obj.RoomClashCount() == 0) {
      return "btn btn-xs btn-default btn-block"
    } else {
      return "btn btn-xs btn-danger btn-block animated rubberBand infinite go"
    }
  },
  SCCROperatorCss: function (obj) {
    if (obj.SCCROperatorClashCount() > 0) {
      return "border-error"
    }
  },
  MCRControllerCss: function (obj) {
    if (obj.MCRControllerClashCount() > 0) {
      return "border-error"
    }
  },
  buttonHTML: function (obj) {
    return obj.BookingCount().toString()
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },

  //cans
  channelLockDown: function (obj) {

  },
  canUpload: function (obj) {
    //if (!obj.IsProcessing()) {
    //&& obj.IsValid() 
    var hasChannel = false
    obj.SynergyEventChannelList().Iterate(function (itm, ind) {
      if (itm.IsSelected()) {
        hasChannel = true
      }
    })
    if (obj.RoomID() && obj.CallTime() && obj.WrapTime() && !obj.IsProcessing() && obj.RoomClashCount() == 0 && hasChannel/*&& ViewModel.CurrentSynergyEventPlayout().IsValid()*/) {
      return true
    }
    //}
    return false
  },
  canUploadOB: function (obj) {
    return (obj.IsValid() && !obj.IsProcessing() && (obj.ProductionID() == null))
  },
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case 'ResourceIDMCRController':
      case 'ResourceIDSCCROperator':
        return (instance.SystemID() == 5)
        break
      default:
        return !instance.IsProcessing()
        break;
    }
  },

  //set
  SystemIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  ProductionAreaIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  RoomIDSet: function (SynergyEvent) {
    if (SynergyEvent.RoomID()) {
      var callTimeRequired = false,
          wrapTimeRequired = false;

      //get area settings
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', SynergyEvent.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', SynergyEvent.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', SynergyEvent.RoomID())

      var roomSetting = null;
      if (roomSettings.length == 1) {
        roomSetting = roomSettings[0]
        callTimeRequired = roomSetting.RoomCallTimeRequired
        wrapTimeRequired = roomSetting.RoomWrapTimeRequired
      }
      else if (roomSettings.length > 1) {
        //check dates
        roomSettings.Iterate(function (rm, rmIndx) {
          var effectiveDate = new moment(rm.EffectiveDate)
          var effectiveEndDate = (rm.EffectiveEndDate ? new moment(rm.EffectiveEndDate) : new moment(SynergyEvent.LastEnd()))
          var dateRange = moment().range(effectiveDate, effectiveEndDate)
          var startDate = moment(SynergyEvent.FirstStart())
          if (startDate.isBetween(effectiveDate, effectiveEndDate)) {
            roomSetting = rm
            return //exit loop
          }
        })
      } else {
        callTimeRequired = false;
        wrapTimeRequired = false;
        roomSetting = null;
      }

      //call and wrap time variables
      var ctMinutes = 0,
          wtMinutes = 0;

      //check call time
      if (callTimeRequired) {
        if (roomSetting) {
          var ct = moment(new Date(SynergyEvent.FirstStart())).add(-roomSetting.CallTimeMinutes, 'minutes').toDate()
          SynergyEvent.CallTime(ct)
        }
        else {
          SynergyEvent.CallTime(SynergyEvent.FirstStart())
        }
      }
      else {
        SynergyEvent.CallTime(SynergyEvent.FirstStart())
      }

      //check wrap time
      if (wrapTimeRequired) {
        if (roomSetting) {
          var wt = moment(new Date(SynergyEvent.LastEnd())).add(roomSetting.WrapTimeMinutes, 'minutes').toDate()
          SynergyEvent.WrapTime(wt)
        }
        else {
          SynergyEvent.WrapTime(SynergyEvent.LastEnd())
        }
      }
      else {
        SynergyEvent.WrapTime(SynergyEvent.LastEnd())
      }

    }
    else {
      SynergyEvent.RoomClashCount(0)
      SynergyEvent.CallTime(null)
      SynergyEvent.WrapTime(null)
    }
  },
  CallTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  FirstStartSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    SynergyEvent.IsProcessing(false)
  },
  LastEndSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    if (SynergyEvent.LastEnd()) {
      var lastEnd = new Date(SynergyEvent.LastEnd())
      var newWrapTime = new Date(lastEnd.getTime() + (1 * 36e5)) //Default Wrap Time to 1 hr after end time
      SynergyEvent.WrapTime(newWrapTime)
    }
    SynergyEvent.IsProcessing(false)
  },
  WrapTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  ResourceIDSCCROperatorSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDSCCROperator(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.SCCROperatorClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },
  ResourceIDMCRControllerSet: function (self) {
    //self.IsProcessing(true)
    //OBMisc.Resources.getROResourceBookings(self.FirstStart(), self.LastEnd(), self.ResourceIDMCRController(), null, null, null, null, null,
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         self.MCRControllerClashCount(response.Data.length)
    //                                       },
    //                                       function (response) {
    //                                         self.IsProcessing(false)
    //                                         OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
    //                                         //self.MCRControllerClashCount(response.Data.length)
    //                                       })
  },

  //Rules
  LiveDateValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.LiveDate();
    var leti = CtlError.Object.LiveEndDateTime();
    if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live Start Time must be before Live End Time");
      }
    }
  },
  LiveEndDateTimeValid: function (Value, Rule, CtlError) {
    var leti = CtlError.Object.LiveEndDateTime();
    if (!leti) {
      CtlError.AddError("Live End Time is required");
    }
  },
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.OBInd() && CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  },
  TeamsValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.OBInd() && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  },
  TeamsPlayingValid: function (Value, Rule, CtlError) {
    if ((CtlError.Object.ProductionAreaID() == 1) && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams Playing is required");
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.FirstStart())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.FirstStart(), obj.LastEnd())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.LastEnd(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      //clash
      if (obj.RoomClashCount() > 0) {
        CtlError.AddError('Room not available')
      }

      if (invalidTimeCount > 0) {
        obj.TimesValid(false)
      } else { obj.TimesValid(true) }

    }
  },
  HoursValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.CallTime() != undefined && obj.LastEnd() != undefined) {
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
      var callTime = new Date(obj.CallTime())
      var endTime = new Date(obj.LastEnd())
      var hours = Math.abs(endTime - callTime) / 36e5 // 36e5 = 60*60*1000(Convert ms to hrs)
      if (roomSettings.length > 0) {
        if (hours > roomSettings[0].MaxHoursPerShift) {
          CtlError.AddError('Shift is more than ' + roomSettings[0].MaxHoursPerShift + ' Hours long')
        }
      }
    }
  },
  ResourceIDSCCROperatorValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDSCCROperator() && obj.SCCROperatorClashCount() > 0) {
      //var lst = ClientData.ROSCCROperatorList.Criteria().SCCROperator(obj.SCCROperator())
      var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
      var hrName = (hr ? hr.HRName : "SCCR Operator")
      CtlError.AddError(hrName + ' has clashes')
    }
    else if (obj.ResourceIDSCCROperator()) {
      var hr = ClientData.ROSCCROperatorList.Find("ResourceID", obj.ResourceIDSCCROperator())
      if (hr) {
        var hrName = (hr ? hr.HRName : "SCCR Operator")
        var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', obj.SystemID())
        areaSettings = areaSettings.Filter('ProductionAreaID', obj.ProductionAreaID())
        var roomSettings = areaSettings.Filter('RoomID', obj.RoomID())
        if (roomSettings.length > 0) {
          if (hr.TimeToNextShift() > roomSettings[0].MinShortfallBetweenShifts) {
            CtlError.AddError(hrName + ' has less than ' + roomSettings[0].MinShortfallBetweenShifts + ' hours before next shift')
          }
        }
      }
    }
  },
  ResourceIDMCRControllerValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    var hr = ClientData.ROMCRControllerList.Find("ResourceID", obj.ResourceIDMCRController())
    if (hr) {
      var hrName = (hr ? hr.HRName : "MCR Controller")
      if (obj.ResourceIDMCRController() && obj.MCRControllerClashCount() > 0) {
        CtlError.AddError(hrName + ' has clashes')
      }
      //else if (hr.TimeToNextShift() > 10) {
      //  CtlError.AddError(hrName + ' has less than 10 hours before this shift')
      //}
    }
  },
  MCRControllerBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDMCRController() && obj.HumanResourceShiftIDMCRController() && obj.MCRControllerShiftDuration() > 14) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('MCR Controller booking reason is required')
      }
    }
    if (obj.ResourceIDMCRController() && obj.HumanResourceShiftIDMCRController() && obj.MCRControllerRestHours() < 10) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('MCR Controller booking reason is required')
      }
    }
  },
  SCCRControllerBookedReasonValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.ResourceIDSCCROperator() && obj.HumanResourceShiftIDSCCROperator() && obj.SCCRControllerShiftDuration() > 14) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('SCCR Controller booking reason is required')
      }
    }
    if (obj.ResourceIDSCCROperator() && obj.HumanResourceShiftIDSCCROperator() && obj.SCCRControllerRestHours() < 10) {
      if (obj.MCRControllerBookedReason().trim().length == 0) {
        CtlError.AddError('SCCR Controller booking reason is required')
      }
    }
  },

  //RoomID
  beforeRoomIDDropDownShown: function () {
    console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    if (selectedItem && (selectedItem.IsGap || selectedItem.IsSufficientTime)) {
      businessObject.RoomClashCount(0)
    } else {
      businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    }
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.FirstStart()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //MCRControllerID
  onMCRControllerSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      if (!selectedItem.IsAvailable) {
        businessObject.MCRControllerClashCount(businessObject.MCRControllerClashCount() + 1)
      } else {
        businessObject.MCRControllerClashCount(0)
        businessObject.HumanResourceShiftIDMCRController(selectedItem.HumanResourceShiftID)
        businessObject.MCRControllerShiftDuration(selectedItem.ExpectedShiftDuration)
        businessObject.MCRControllerRestHours(selectedItem.TimeToNextShift)
      }
    }
    else {
      businessObject.MCRControllerClashCount(0)
      businessObject.HumanResourceShiftIDMCRController(null)
      businessObject.MCRControllerShiftDuration(0)
      businessObject.MCRControllerRestHours(0)
    }
  },
  triggerMCRControllerAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setMCRControllerCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterMCRControllerRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  //SCCROperatorID
  onSCCROperatorSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      if (!selectedItem.IsAvailable) {
        businessObject.SCCROperatorClashCount(businessObject.SCCROperatorClashCount() + 1)
      } else {
        businessObject.SCCROperatorClashCount(0)
        businessObject.HumanResourceShiftIDSCCROperator(selectedItem.HumanResourceShiftID)
        businessObject.SCCRControllerShiftDuration(selectedItem.ExpectedShiftDuration)
        businessObject.SCCRControllerRestHours(selectedItem.TimeToNextShift)
      }
    }
    else {
      businessObject.SCCROperatorClashCount(0)
      businessObject.HumanResourceShiftIDSCCROperator(null)
      businessObject.SCCRControllerShiftDuration(0)
      businessObject.SCCRControllerRestHours(0)
    }
  },
  triggerSCCROperatorAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setSCCROperatorCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterSCCROperatorRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCellCreate: function (cellElement, index, ColumnData, RowData) {
    //console.log({ ce: cellElement, i: index, cn: ColumnName, rd: RowData })
  },

  //VehicleID
  triggerVehicleAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setVehicleCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.CurrentProductionVenueID = args.Object.ProductionVenueID()
  },
  afterVehicleRefreshAjax: function (args) {

  },
  VehicleIDBeforeSet: function (self) {
    //self.PreviousVehicleID(self.VehicleID())
  },
  VehicleIDSet: function (self) {
    var me = this;
  },
  onVehicleSelected: function (item, businessObject) {
    var x = null
  },

  //ProductionVenueID
  ProductionVenueIDSet: function (self) { },
  onProductionVenueSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionVenueID(selectedItem.ProductionVenueID)
    }
    else {
      businessObject.ProductionVenueID(null)
    }
  },
  triggerProductionVenueAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setProductionVenueCriteriaBeforeRefresh: function (args) {
    //args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    //args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterProductionVenueRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  //ProductionSpecRequirementID
  triggerProductionSpecRequirementAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSpecRequirementCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterProductionSpecRequirementRefreshAjax: function (args) {

  },
  ProductionSpecRequirementIDSet: function (self) { },
  onProductionSpecRequirementSelected: function (item, businessObject) { }

}

AllocatorChannelListCriteriaBO = {
  SystemIDSet: function (self) { },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  LiveSet: function (self) { },
  DelayedSet: function (self) { },
  PremierSet: function (self) { },
  GenRefNoSet: function (self) { },
  GenRefNoStringSet: function (self) { },
  PrimaryChannelsOnlySet: function (self) { },
  AllChannelsSet: function (self) { },
  GenreSet: function (self) { },
  SeriesSet: function (self) { },
  TitleSet: function (self) { },
  KeywordSet: function (self) { }
}

ROSynergyChangePagedListCriteriaBO = {
  GenRefNumberSet: function (self) { },
  ScheduleNumberSet: function (self) { },
  ChangeStartDateTimeSet: function (self) { },
  ChangeEndDateTimeSet: function (self) { },
  SynergyChangeTypeIDSet: function (self) { },
  ColumnNameSet: function (self) { }
}