﻿SlugItemBO = {
  ProductionHumanResourceIDSet: function (self) {
    //var roomSchedule = self.GetParent()
    //var previousEditorSlug = roomSchedule.SlugEditorList().Find("SlugID", self.SlugID())
    //if (self.ProductionHumanResourceID()) {
    //  //var phr = roomSchedule.ProductionSystemAreaList()[0].ProductionHumanResourceList().Find("ProductionHumanResourceID", self.ProductionHumanResourceID())
    //  //if (previousEditorSlug) {
    //  //  //Update existing Slug Editor
    //  //  previousEditorSlug.HumanResourceID(phr.HumanResourceID())
    //  //  previousEditorSlug.HumanResourceName(phr.HumanResource())
    //  //}
    //  //else {
    //  //  //Add New Slug Editor
    //  //  previousEditorSlug = roomSchedule.SlugEditorList.AddNew()
    //  //  previousEditorSlug.SlugID(self.SlugID())
    //  //  previousEditorSlug.SlugName(self.SlugName())
    //  //  previousEditorSlug.SlugType(self.SlugType())
    //  //  previousEditorSlug.SlugDuration(self.SlugDuration())
    //  //  previousEditorSlug.HumanResourceID(phr.HumanResourceID())
    //  //  previousEditorSlug.HumanResourceName(phr.HumanResource())
    //  //  previousEditorSlug.HasPendingDelete(self.HasPendingDelete())
    //  //}
    //}
    //else {
    //  roomSchedule.SlugEditorList.RemoveNoCheck(previousEditorSlug)
    //}
  },
  getEditorList: function (slugItem) {
    var roomSchedule = slugItem.GetParent()
    var editors = []
    if (roomSchedule.ProductionSystemAreaList().length == 1) {
      var psa = roomSchedule.ProductionSystemAreaList()[0]
      if (psa) {
        psa.ProductionHumanResourceList().Iterate(function (item, indx) {
          if (item.DisciplineID() == 50 && item.HumanResourceID()) {
            editors.push({ ProductionHumanResourceID: item.ProductionHumanResourceID(), HumanResourceName: item.HumanResource() })
          }
        })
      }
    }
    return editors
  }
}