﻿ROObjectAuditTrailBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Audit.ROObjectAuditTrailList, OBLib",
                              options.criteria,
                              function (response) {
                                if (response.Success) {
                                  if (options.onSuccess) { options.onSuccess(response) }
                                } else {
                                  if (options.onFail) { options.onFail(response) }
                                }
                              })
  }
}