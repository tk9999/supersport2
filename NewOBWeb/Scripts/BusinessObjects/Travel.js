﻿TravelRequisitionBO = {
  toString: function (self) {
    return self.EventTitle()
  },
  canView: function (ItemName, travelReq) {
    switch (ItemName) {
      case "BackToProductionButton":
        if (!travelReq) {
          return false;
        } else {
          return (travelReq && travelReq.ProductionID() != null);
        };
        break;
      case "FindRequisitionButton":
        if (!travelReq) {
          return true;
        } else {
          return (travelReq && travelReq.AdHocBookingID() != null);
        };
        break;
      case "AddRequisitionButton":
        if (!travelReq) {
          return true;
        } else {
          return (travelReq && travelReq.AdHocBookingID() != null);
        };
        break;
      case "ChauffersTab":
        return (travelReq && travelReq.CanBookChauffers());
        break;
      case "TravelAvailabilityTab":
        return (!travelReq.AdHocBookingID());
        break;
      case "TravellersTab":
        return (travelReq.ProductionAreaID() != 1 || travelReq.AdHocBookingID() != null || travelReq.SystemID() == 2);
        break;
      case "TravelAdvancesTab":
        return (travelReq.ProductionAreaID() != 1 || travelReq.AdHocBookingID() != null || travelReq.SystemID() == 2);
        break;
      default:
        return true
        break;
    }
  },
  travelReqIconCss: function (travelRequisition) {
    if (travelRequisition.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    return "fa fa-print"
  },

  //methods
  fetchSnT: function (requisition) {
    TravellerSnTBO.get({
      criteria: { TravelRequisitionID: requisition.TravelRequisitionID(), HumanResourceID: null, FetchSnTDetails: false },
      onSuccess: function (response) {
        requisition.TravellerSnTList.Set(response.Data)
        requisition.TravellerSnTFetched(true)
      }
    })
  },
  resetSnT: function (requisition) {
    var list = []
    requisition.TravellerSnTList().Iterate(function (trt, trtIndx) {
      if (trt.IsSelected()) {
        list.push(trt.Serialise())
      }
    })
    ViewModel.CallServerMethod("ResetTravellerSnT",
      { TravelRequisitionTravellerSnTList: list },
      function (response) {
        if (response.Success) {
          TravellerSnTBO.get({
            criteria: { TravelRequisitionID: requisition.TravelRequisitionID(), HumanResourceID: null, FetchSnTDetails: false },
            onSuccess: function (response) {
              requisition.TravellerSnTList.Set(response.Data);
              requisition.TravellerSnTFetched(true);
              OBMisc.Notifications.GritterSuccess("Reset S&T Succeeded", "", 250);
            }
          })
        } else {
          OBMisc.Notifications.GritterError("Reset S&T Failed", response.ErrorText, 1000);
        }
      })
  },

  removeSelectedFlights: function (requisition) {
    //populate the items to be removed
    var itemsToRemove = []
    requisition.FlightList().Iterate(function (flight, flightIndex) {
      if (flight.IsSelected() && !flight.IsCancelled()) {
        itemsToRemove.push(flight)
      }
    })
    //create a serialised list of the flights to be removed
    var serialisedFlights = []
    itemsToRemove.Iterate(function (flight, flightIndex) {
      serialisedFlights.push(flight.Serialise())
    })
    //delete the flights
    FlightBO.deleteFlights(serialisedFlights, {
      onSuccess: function (response) {
        //remove the items for the FlightList on the TravelRequisition
        itemsToRemove.Iterate(function (flightToDelete, flightToDeleteIndex) {
          requisition.FlightList.RemoveNoCheck(flightToDelete)
        })
      }
    })
  },
  removeSelectedRentalCars: function (requisition) {
    //populate the items to be removed
    var itemsToRemove = []
    requisition.RentalCarList().Iterate(function (rentalCar, rentalCarIndex) {
      if (rentalCar.IsSelected() && !rentalCar.IsCancelled()) {
        itemsToRemove.push(rentalCar)
      }
    })
    //create a serialised list of the rentalCars to be removed
    var serialisedRentalCars = []
    itemsToRemove.Iterate(function (rentalCar, rentalCarIndex) {
      serialisedRentalCars.push(rentalCar.Serialise())
    })
    //delete the rentalCars
    RentalCarBO.deleteRentalCars(serialisedRentalCars, {
      onSuccess: function (response) {
        //remove the items for the RentalCarList on the TravelRequisition
        itemsToRemove.Iterate(function (rentalCarToDelete, rentalCarToDeleteIndex) {
          requisition.RentalCarList.RemoveNoCheck(rentalCarToDelete)
        })
      }
    })
  },
  removeSelectedAccommodation: function (requisition) {
    //populate the items to be removed
    var itemsToRemove = []
    requisition.AccommodationList().Iterate(function (accommodation, accommodationIndex) {
      if (accommodation.IsSelected() && !accommodation.IsCancelled()) {
        itemsToRemove.push(accommodation)
      }
    })
    //create a serialised list of the accommodation to be removed
    var serialisedAccommodation = []
    itemsToRemove.Iterate(function (accommodation, accommodationIndex) {
      serialisedAccommodation.push(accommodation.Serialise())
    })
    //delete the accommodation
    AccommodationBO.deleteAccommodation(serialisedAccommodation, {
      onSuccess: function (response) {
        //remove the items for the AccommodationList on the TravelRequisition
        itemsToRemove.Iterate(function (accommodationToDelete, accommodationToDeleteIndex) {
          requisition.AccommodationList.RemoveNoCheck(accommodationToDelete)
        })
      }
    })
  },
  removeSelectedChauffeurDrives: function (requisition) {
    //populate the items to be removed
    var itemsToRemove = []
    requisition.ChauffeurDriverList().Iterate(function (chauffeur, chauffeurIndex) {
      if (chauffeur.IsSelected() && !chauffeur.IsCancelled()) {
        itemsToRemove.push(chauffeur)
      }
    })
    //create a serialised list of the chauffeur to be removed
    var serialisedChauffeur = []
    itemsToRemove.Iterate(function (chauffeur, chauffeurIndex) {
      serialisedChauffeur.push(chauffeur.Serialise())
    })
    //delete the chauffeur
    ChauffeurDriverBO.deleteChauffeur(serialisedChauffeur, {
      onSuccess: function (response) {
        //remove the items for the ChauffeurList on the TravelRequisition
        itemsToRemove.Iterate(function (chauffeurToDelete, chauffeurToDeleteIndex) {
          requisition.ChauffeurDriverList.RemoveNoCheck(chauffeurToDelete)
        })
      }
    })
  },
  removeSelectedTravelAdvances: function (requisition) {
    //populate the items to be removed
    var itemsToRemove = []
    requisition.ProductionTravelAdvanceDetailList().Iterate(function (travelAdvance, travelAdvanceIndex) {
      if (travelAdvance.IsSelected()) {
        itemsToRemove.push(travelAdvance)
      }
    })
    //create a serialised list of the chauffeur to be removed
    var serialisedTravelAdvances = []
    itemsToRemove.Iterate(function (travelAdvance, travelAdvanceIndex) {
      serialisedTravelAdvances.push(travelAdvance.Serialise())
    })
    //delete the chauffeur
    TravelAdvanceDetailBO.deleteTravelAdvances(serialisedTravelAdvances, {
      onSuccess: function (response) {
        //remove the items for the ChauffeurList on the TravelRequisition
        itemsToRemove.Iterate(function (travelAdvanceToDelete, travelAdvanceToDeleteIndex) {
          requisition.ProductionTravelAdvanceDetailList.RemoveNoCheck(travelAdvanceToDelete)
        })
      }
    })
  },
  removeSelectedComments: function (requisition) {
    //populate the items to be removed
    var itemsToRemove = []
    requisition.TravelReqCommentList().Iterate(function (travelComment, travelCommentIndex) {
      if (travelComment.IsSelected()) {
        itemsToRemove.push(travelComment)
      }
    })
    //create a serialised list of the chauffeur to be removed
    var serialisedTravelComments = []
    itemsToRemove.Iterate(function (travelComment, travelCommentIndex) {
      serialisedTravelComments.push(travelComment.Serialise())
    })
    //delete the chauffeur
    TravelReqCommentBO.deleteTravelComments(serialisedTravelComments, {
      onSuccess: function (response) {
        //remove the items for the ChauffeurList on the TravelRequisition
        itemsToRemove.Iterate(function (travelCommentToDelete, travelCommentToDeleteIndex) {
          requisition.TravelReqCommentList.RemoveNoCheck(travelCommentToDelete)
        })
      }
    })
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.TravelRequisitionList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Requisition Failed", response.ErrorText, 1000)
        }
      })
  },
  save: function (requisition, options) {
    ViewModel.CallServerMethod("SaveTravelRequisition",
      { TravelRequisition: requisition.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Requisition Failed", response.ErrorText, 1000)
        }
      })
  },
  savePromise(requisition) {
    return ViewModel.CallServerMethodPromise("SaveTravelRequisition", { TravelRequisition: requisition.Serialise() });
  },
  printRequisition: function (requisition, options) {
    requisition.IsProcessing(true);
    OBMisc.Notifications.GritterInfo("Preparing Report", "", 1000);
    if (requisition.AdHocBookingID()) {
      ViewModel.CallServerMethod("DownloadTravelRequisitionAdHoc", {
        TravelRequisitionID: requisition.TravelRequisitionID()
      }, function (response) {
        if (response.Success) {
          Singular.DownloadFile(null, response.Data);
          OBMisc.Notifications.GritterSuccess("Downloading Report", "", 1000)
          requisition.IsProcessing(false)
        } else {
          OBMisc.Notifications.GritterError("Error Downloading", response.ErrorText, 1000)
          requisition.IsProcessing(false)
        }
      })
    } else if (requisition.ProductionID()) {
      ViewModel.CallServerMethod("DownloadTravelRequisitionProduction", {
        TravelRequisitionID: requisition.TravelRequisitionID(),
        SystemID: requisition.SystemID()
      }, function (response) {
        if (response.Success) {
          Singular.DownloadFile(null, response.Data);
          OBMisc.Notifications.GritterSuccess("Downloading Report", "", 1000)
          requisition.IsProcessing(false)
        } else {
          OBMisc.Notifications.GritterError("Error Downloading", response.ErrorText, 1000)
          requisition.IsProcessing(false)
        }
      })
    };
  }
}

TravelRequisitionTravellerBO = {
  toString: function (self) {
    return self.HumanResource()
  },
  InitDate: function (date) {
    return date;
  },
  CheckSnTDates: function (Value, Rule, CtlError) {
    var SnT = CtlError.Object;
    var StartDateTime = new Date(SnT.SnTStartDate());
    var EndDateTime = new Date(SnT.SnTEndDate());
    if (EndDateTime < StartDateTime) {
      CtlError.AddError('Start Date must be before End Date');
    }
  },
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Travellers.TravelRequisitionTravellerList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Travellers Failed", response.ErrorText, 1000)
        }
      })
  },
  saveList: function (travellerList, options) {
    var me = this
    ViewModel.CallServerMethod("SaveTravelRequisitionTravellerList",
      { TravelRequisitionTravellerList: travellerList.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  save: function (traveller, options) {
    var me = this
    ViewModel.CallServerMethod("SaveTravelRequisitionTraveller",
      { TravelRequisitionTraveller: traveller.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  cancelTraveller(traveller) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await ViewModel.CallServerMethodPromise("CancelTraveller", { Traveller: traveller.Serialise() });
        resolve(result);
      }
      catch (error) {
        reject(error);
      }
    });
  },
  removeTraveller(traveller) {
    return new Promise(async (resolve, reject) => {
      if (traveller.FlightCount() > 0 || traveller.RentalCarCount() > 0 || traveller.AccommodationCount() > 0 || traveller.ChauffeurCount() > 0) {
        reject(traveller.HumanResource() + " is still on some bookings");
      } else {
        try {
          let result = await ViewModel.CallServerMethodPromise("RemoveTraveller", { Traveller: traveller.Serialise() });
          resolve(result);
        }
        catch (error) {
          reject(error);
        }
      };
    });
  },

  canEdit(inst, fieldName) {
    switch (fieldName) {
      case "IsSelected":
        return !inst.IsProcessing();
        break;
      default:
        return !inst.IsProcessing();
        break;
    };
  }
}

FlightBO = {
  toString: function (self) {
    var base = 'Flight',
      apFrom = '',
      deptTime = ''
    if (self.AirportFrom().trim().length > 0) {
      apFrom = ' from ' + self.AirportFrom()
    }
    if (self.DepartureDateTime()) {
      deptTime = new Date(self.DepartureDateTime()).format('dd MMM yy HH:mm')
    }
    return base + (self.FlightNo().trim().length > 0 ? ' (' + self.FlightNo() + ')' : ' ')
      + (apFrom.trim().length > 0 ? apFrom : ' ')
      + (deptTime.trim().length > 0 ? ' on ' + deptTime : ' ')
  },

  //dropdowns
  triggerAirportIDFromAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAirportIDFromCriteriaBeforeRefresh: function (args) {
  },
  afterAirportIDFromRefreshAjax: function (args) {
  },
  onAirportIDFromSelected: function (item, businessObject) {
    if (item) {
      businessObject.CountryIDFrom(item.CountryID)
    }
    else {
      businessObject.CountryIDFrom(null)
    }
    if (item && businessObject.IsNew() && businessObject.DepartureDateTime()
      && businessObject.AirportIDFrom() && businessObject.AirportIDTo()
      && businessObject.CountryIDFrom() == 1 && businessObject.CountryIDTo() == 1) {
      businessObject.ArrivalDateTime(businessObject.DepartureDateTime())
    }
  },

  triggerAirportIDToAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAirportIDToCriteriaBeforeRefresh: function (args) {
  },
  afterAirportIDToRefreshAjax: function (args) {
  },
  onAirportIDToSelected: function (item, businessObject) {
    if (item) {
      businessObject.CountryIDTo(item.CountryID)
    }
    else {
      businessObject.CountryIDTo(null)
    }
    if (item && businessObject.IsNew() && businessObject.DepartureDateTime()
      && businessObject.AirportIDFrom() && businessObject.AirportIDTo()
      && businessObject.CountryIDFrom() == 1 && businessObject.CountryIDTo() == 1) {
      businessObject.ArrivalDateTime(businessObject.DepartureDateTime())
    }
  },

  //validation
  TravelDatesValid: function (Value, Rule, CtlError) {
    var Flight = CtlError.Object;
    var DepartureDateTime = new Date(Flight.DepartureDateTime());
    var ArrivalDateTime = new Date(Flight.ArrivalDateTime());

    if (ArrivalDateTime <= DepartureDateTime) {
      CtlError.AddError("Departure Date must be before Arrival Date");
    };
  },
  AirportsValid: function (Value, Rule, CtlError) {
    var Flight = CtlError.Object;
    if (Flight.AirportIDFrom() != 0 && Flight.AirportIDTo() != 0) {
      if (Flight.AirportIDFrom() == Flight.AirportIDTo()) {
        CtlError.AddError("Airport From and To cannot be the same");
      }
    }
  },
  CancelledReasonValid: function (Value, Rule, CtlError) {
    var Flight = CtlError.Object;
    if (Flight.IsCancelled() && Flight.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required");
    }
  },
  HRClashCountValid: function (Value, Rule, CtlError) {
    var rc = CtlError.Object;
    if (rc.HRClashCount() > 0) {
      CtlError.AddError("There are passenger clashes on this flight")
    }
  },
  CrewTypeIDValid(Value, Rule, CtlError) {
    let busObj = CtlError.Object;
    if (busObj.GetParent() && busObj.CrewTypeID() == null && (busObj.GetParent().SystemID() == 1)) {
      CtlError.AddError("Crew Type is required");
    }
  },

  //set
  CancelledReasonSet: function (self) {
    if (self) {
      if (self.CancelledReason().trim() != "") {
        self.FlightHumanResourceList().Iterate(function (itm, ind) {
          if (itm.IsOnFlight()) {
            itm.CancelledReason(self.CancelledReason())
          }
        })
      } else {
        self.FlightHumanResourceList().Iterate(function (itm, ind) {
          if (itm.IsOnFlight()) {
            itm.CancelledReason('')
          }
        })
      }
    }
  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    } else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
    }
    self.FlightHumanResourceList().Iterate(function (itm, ind) {
      if (itm.IsOnFlight()) {
        itm.IsCancelled(self.IsCancelled())
        itm.CancelledByUserID(self.CancelledByUserID())
        itm.CancelledDateTime(self.CancelledDateTime())
        itm.CancelledReason(self.CancelledReason())
      }
    })
  },
  DepartureDateTimeSet: function (self) {
    FlightBO.updateClashes(self)
  },
  ArrivalDateTimeSet: function (self) {
    FlightBO.updateClashes(self)
  },

  //cans
  canEdit: function (Flight, FieldName) {
    //if not cancelled or only recently cancelled, but not saved
    const isEditableBase = (!Flight.IsCancelled() || (Flight.IsCancelled() && Flight.IsDirty()));
    if (Flight.IsProcessing()) {
      return false;
    } else {
      switch (FieldName) {
        case "CrewTypeID":
          return isEditableBase;
          break;
        case "TravelDirectionID":
          return isEditableBase;
          break;
        case "FlightTypeID":
          return (isEditableBase && Flight.TravelDirectionID());
          break;
        case "AirportIDFrom":
          return (isEditableBase && Flight.TravelDirectionID() && Flight.FlightTypeID());
          break;
        case "DepartureDateTime":
          return (isEditableBase && Flight.TravelDirectionID() && Flight.FlightTypeID() && Flight.AirportIDFrom());
          break;
        case "AirportIDTo":
          return (isEditableBase && Flight.TravelDirectionID() && Flight.FlightTypeID() && Flight.AirportIDFrom() && Flight.DepartureDateTime());
          break;
        case "ArrivalDateTime":
          return (isEditableBase && Flight.TravelDirectionID() && Flight.FlightTypeID() && Flight.AirportIDFrom() && Flight.DepartureDateTime() && Flight.AirportIDTo());
          break;
        case "FlightPassengersButton":
          return (isEditableBase && Flight.TravelDirectionID() && Flight.FlightTypeID() && Flight.AirportIDFrom() && Flight.DepartureDateTime() && Flight.AirportIDTo() && Flight.ArrivalDateTime());
          break;
        case 'CancelButton':
          return isEditableBase;
          break;
        case 'CancelledReason':
          return isEditableBase;
          break;
        case 'UnCancelButton':
          return (Flight.IsCancelled())
          break;
        case 'SaveButton':
          return (Flight.IsValid() && Flight.IsDirty() && !Flight.IsProcessing())
          break;
        default:
          return isEditableBase;
          break;
      }
    }
  },
  canView: function (Flight, FieldName) {
    switch (Flight, FieldName) {
      case 'FlightPassengersButton':
      case 'IsSelected':
        return (!Flight.IsCancelled())
        break;
      case 'CancelButton':
        return (!Flight.IsCancelled())
        break;
      case 'UnCancelButton':
        return (Flight.IsCancelled())
        break;
      default:
        return true
        break;
    }
  },

  //other
  InitDate: function (obj) {
    var d = new Date();
    var tr = obj.GetParent()
    if (tr.StartDate() || tr.EndDate()) {
      var sd = KOFormatterFull.Serialise(tr.StartDate());
      var ad = KOFormatterFull.Serialise(tr.EndDate());
      if (sd !== null) {
        d = new Date(tr.StartDate());
      }
      else if (ad !== null) {
        d = new Date(tr.EndDate());
      }
    }
    var first_day = new Date(d.getFullYear(), d.getMonth(), 1);
    return first_day;
  },
  filterROCrewTypes: function (List, flight) {
    var Allowed = [];
    var travelReq = flight.GetParent()
    ClientData.ROCrewTypeList.Iterate(function (crewType, Index) {
      if (travelReq.SystemID() == crewType.SystemID) {
        Allowed.push({ CrewTypeID: crewType.CrewTypeID, TravelScreenDisplayName: crewType.TravelScreenDisplayName });
      }
    });
    return Allowed;
  },
  async updateClashes(flt) {
    if (!flt.IsProcessing()) {
      flt.IsProcessing(true);
      await OBMisc.Tools.sleep(250);
      try {
        let data = await ViewModel.CallServerMethodPromise("UpdateFlightClashes", { Flight: flt.Serialise() });
        KOFormatter.Deserialise(data, flt);
        Singular.Validation.CheckRules(flt);
        flt.IsProcessing(false);
      } catch (errorText) {
        OBMisc.Notifications.GritterError("Failed to update clashes", errorText, 1000);
        flt.IsProcessing(false);
      };
    };
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Flights.FlightList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Flights Failed", response.ErrorText, 1000)
        }
      })
  },
  getPassengers: function (flight, options) {
    FlightPassengerBO.get({
      criteria: { TravelRequisitionID: flight.TravelRequisitionID(), FlightID: flight.FlightID() },
      onSuccess: function (response) {
        flight.FlightHumanResourceList.Set(response.Data);
        FlightBO.updateClashes(flight);
        if (options.onSuccess) { options.onSuccess(response) }
      }
    })
  },
  deleteFlights: function (serialisedFlightList, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteFlights",
      { FlightList: serialisedFlightList },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  },
  saveItem: function (Flight, options) {
    var me = this
    Flight.IsProcessing(true)
    Flight.TravelRequisitionID(Flight.GetParent().TravelRequisitionID())
    ViewModel.CallServerMethod("SaveFlight",
      { Flight: Flight.Serialise() },
      function (response) {
        if (response.Success) {
          Flight.IsProcessing(false)
          KOFormatter.Deserialise(response.Data, Flight)
          if (options.onSuccess) { options.onSuccess.call(me, response) } else { OBMisc.Notifications.GritterSuccess("Flight Saved", "", 250) }
        }
        else {
          if (options.onFail) { options.onFail.call(me, response) };
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  save: function (serialisedFlightList, options) {
    //var me = this
    //ViewModel.CallServerMethod("SaveFlights",
    //                           { FlightList: serialisedFlightList },
    //                           function (response) {
    //                             if (response.Success) {
    //                               //KOFormatter.Deserialise(response.Data, Flight)
    //                               if (options.onSuccess) { options.onSuccess.call(me, response) }
    //                             }
    //                             else {
    //                               OBMisc.Notifications.GritterError("Save Failed", "", 1000)
    //                             }
    //                           })
  },

  //ui stuff
  flightPassengerButtonCSS: function (Flight) {
    if (Flight.IsProcessing()) {
      return "btn btn-xs btn-info btn-block"
    }
    else if (Flight.PassengerCount() > 0 || Flight.IsCancelled()) {
      return "btn btn-xs btn-primary btn-block"
    }
    else if (Flight.PassengerCount() == 0) {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
    else {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
  },
  flightPassengerButtonIcon: function (Flight) {
    if (Flight.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-user"
    }
  }
}

FlightPassengerBO = {
  toString: function (self) {
    return self.HumanResourceName()
  },
  canEdit: function (self, FieldName) {
    if (self.HasClashes() && !self.IsOnFlight()) {
      return false
    } else {
      switch (FieldName) {
        case 'OnFlightButton':
          if ((self.IsCancelled() && !self.IsDirty()) || self.ReAddedToFlight()) {
            return false
          } else {
            return true
          }
          break;
        case 'FlightClassID':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelButton':
          if ((self.IsCancelled() && !self.IsDirty()) || self.ReAddedToFlight()) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelledReason':
          if ((self.IsCancelled() && !self.IsDirty()) || self.ReAddedToFlight()) {
            return false
          } else {
            return true
          }
          break;
        case 'ReAddButton':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return true
          } else {
            return false
          }
          break;
      }
    }
  },
  canView: function (self, FieldName) {
    switch (FieldName) {
      case 'OnFlightButton':
        if (self.IsOnFlight()) {
          return true;
        } else {
          return !self.HasClashes();
        }
        break;
      case 'OtherFlightsButton':
        return self.HasClashes()
        break;
    }
  },
  readdToBooking: function (self) {
    var newItem = self.GetParent().FlightHumanResourceList.AddNew()
    newItem.IsOnFlight(true)
    newItem.FlightID(self.GetParent().FlightID())
    newItem.HumanResourceID(self.HumanResourceID())
    newItem.FlightClassID(self.FlightClassID())
    newItem.BookingReason(self.BookingReason())
    newItem.HumanResourceName(self.HumanResourceName())
    newItem.CityCode(self.CityCode())
    newItem.Disciplines(self.Disciplines())
    newItem.ReAddedToFlight(true)
    //check clashes
    FlightBO.updateClashes(self.GetParent())
  },

  FlightClassRequired: function (Value, Rule, CtlError) {
    var CurrentBulkFlightPassenger = CtlError.Object;
    if (CurrentBulkFlightPassenger.FlightClassID() == null) {
      CtlError.AddError("Flight Class Required");
    }
  },
  CancelledReasonRequired: function (Value, Rule, CtlError) {
    var currentFlightPassenger = CtlError.Object;
    if (currentFlightPassenger.IsCancelled() && currentFlightPassenger.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + currentFlightPassenger.HumanResourceName());
    }
  },
  IsOnFlightValid(Value, Rule, CtlError) {
    let self = CtlError.Object;
    if (self.HasClashes() && self.IsOnFlight()) {
      CtlError.AddError(self.HumanResourceName() + " has flight clashes");
    };
  },

  FlightClassSet: function (self) {
    //if (self.IsOnFlight() == true) {
    //  var FlightClass = ClientData.ROFlightClassList.Find('FlightClassID', 1);
    //  self.FlightClassID(FlightClass.FlightClassID);
    //}
    //else {
    //  self.FlightClassID(1);
    //}
  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    } else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
      self.CancelledReason("")
    }
  },
  CancelledReasonSet: function (self) {
    //var bfp = self;
    //self.FlightHumanResourceList().Iterate(function (itm, ind) {
    //  if (itm.OnFlightInd()) {
    //    itm.CancelledReason(self.CancelledReason())
    //  }
    //})
    //var fhr = ViewModel.CurrentFlight().FlightHumanResourceList().Find("FlightHumanResourceID", bfp.FlightHumanResourceID());
    //if (fhr != undefined) fhr.CancelledReason(bfp.CancelledReason());
  },
  IsOnFlightSet(self) {
    if (!self.IsOnFlight()) {
      self.OtherFlights("");
      self.OtherFlightsShort("");
      self.HasClashes(false);
    };
    let flight = self.GetParent();
    FlightBO.updateClashes(flight);
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Flights.FlightHumanResourceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Passengers Failed", response.ErrorText, 1000)
        }
      })
  }
}

RentalCarBO = {
  toString: function (self) {
    var deptTime = ''
    if (self.PickupDateTime()) {
      deptTime = ' - ' + new Date(self.PickupDateTime()).format('dd MMM yy HH:mm')
    }
    return 'Car: ' + self.BranchFrom() + deptTime
  },

  setRentalCarAgentCriteriaBeforeRefresh(args) {
    args.Data.BookingDate = (args.Object.PickupDateTime() ? args.Object.PickupDateTime() : args.Object.GetParent().StartDate());
  },
  triggerRentalCarAgentAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterRentalCarAgentRefreshAjax(args) { },
  onRentalCarAgentSelected(item, businessObject) { },

  //dropdowns
  triggerAgentBranchIDFromAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAgentBranchIDFromCriteriaBeforeRefresh: function (args) {
    args.Data.RentalCarAgentID = args.Object.RentalCarAgentID()
  },
  afterAgentBranchIDFromRefreshAjax: function (args) {
  },
  onAgentBranchIDFromSelected: function (item, businessObject) {
  },

  triggerAgentBranchIDToAutoPopulate: function (args) {
    args.AutoPopulate = true;
  },
  setAgentBranchIDToCriteriaBeforeRefresh: function (args) {
    args.Data.RentalCarAgentID = args.Object.RentalCarAgentID()
  },
  afterAgentBranchIDToRefreshAjax: function (args) {
  },
  onAgentBranchIDToSelected: function (item, businessObject) {

  },

  triggerRentalCarAgentIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setRentalCarAgentIDCriteriaBeforeRefresh: function (args) {
    args.Data.IncludeOld = false
  },
  afterRentalCarAgentIDRefreshAjax: function (args) {
  },
  onRentalCarAgentIDSelected: function (item, businessObject) {

  },

  triggerDriverAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDriverCriteriaBeforeRefresh: function (args) {
    args.Data.TravelRequisitionID = args.Object.GetParent().TravelRequisitionID()
    args.Data.RentalCarID = args.Object.RentalCarID()
    args.Data.HumanResourceID = null //args.Object.GetParent().TravelRequisitionID()
    args.Data.PickupDateTime = args.Object.PickupDateTime()
    args.Data.DropoffDateTime = args.Object.DropoffDateTime()
  },
  afterDriverRefreshAjax: function (args) {
  },
  onDriverSelected: function (item, businessObject) {
    if (item && item.OtherRentalCars.length == 0) {
      if (businessObject.IsNew()) {
        var tReq = businessObject.GetParent()
        tReq.RentalCarsBusy(true)

        var driver = businessObject.RentalCarHumanResourceList.AddNew()
        driver.IsSelected(true)
        driver.HumanResourceID(item.HumanResourceID)
        driver.HumanResourceName(item.HumanResourceName)
        driver.DriverInd(true)
        driver.OtherRentalCars(item.OtherRentalCars)
        driver.OtherRentalCarsShort(item.OtherRentalCarsShort)
        driver.DriverLicenseInd(item.LicenceInd)
        driver.OnRentalCarInd(true)
        driver.HasClashes((item.OtherRentalCarsShort.length > 0 ? true : false))

        RentalCarBO.setRemainingSeats(businessObject)

        RentalCarBO.saveItem(businessObject, {
          onSuccess: function (response) {
            OBMisc.Notifications.GritterSuccess("Rental Car Saved", "", 250)
            tReq.RentalCarsBusy(false)
          },
          onFail: function (response) {
            tReq.RentalCarsBusy(false)
          }
        })

      }
      else {
        alert('business object not new')
      }
    }
    else {
      businessObject.DriverHumanResourceID(null)
      businessObject.Driver("")
    }
  },
  drawDriverCellContent: function (element, index, columnData, businessObject) {
    element.parentElement.style.width = "350px"
    element.parentNode.className = "traveller-cell"
    element.className = 'traveller-list';
    element.innerHTML = '<div class="info">' +
      '<table>' +
      '<tbody>' +
      //header
      '<tr>' +
      '<td style="width:60px;height:60px;">' +
      '<div class="avatar" style="width:50px;height:50px;">' +
      '<img src="' + window.SiteInfo.SitePath + '/Images/InternalPhotos/' + businessObject.ImageFileName + '" alt="Avatar">' +
      '</div>' +
      '</td>' +
      '<td><p style="text-align: left;">' + businessObject.HumanResourceName + '</p></td>' +
      '<td><p style="color:red;" title="' + businessObject.OtherRentalCars + '">' + businessObject.OtherRentalCarsShort + '</p></td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</div>'
  },

  triggerCoDriverAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setCoDriverCriteriaBeforeRefresh: function (args) {
    args.Data.TravelRequisitionID = args.Object.GetParent().TravelRequisitionID()
    args.Data.RentalCarID = args.Object.RentalCarID()
    args.Data.HumanResourceID = null //args.Object.GetParent().TravelRequisitionID()
    args.Data.PickupDateTime = args.Object.PickupDateTime()
    args.Data.DropoffDateTime = args.Object.DropoffDateTime()
  },
  afterCoDriverRefreshAjax: function (args) {
  },
  onCoDriverSelected: function (item, businessObject) {
    if (item && item.OtherRentalCars.length == 0) {
      if (businessObject.IsNew()) {
        var tReq = businessObject.GetParent()
        tReq.RentalCarsBusy(true)

        var driver = businessObject.RentalCarHumanResourceList.AddNew()
        driver.IsSelected(true)
        driver.HumanResourceID(item.HumanResourceID)
        driver.HumanResourceName(item.HumanResourceName)
        driver.DriverInd(true)
        driver.OtherRentalCars(item.OtherRentalCars)
        driver.OtherRentalCarsShort(item.OtherRentalCarsShort)
        driver.DriverLicenseInd(item.LicenceInd)
        driver.OnRentalCarInd(true)
        driver.HasClashes((item.OtherRentalCarsShort.length > 0 ? true : false))

        RentalCarBO.setRemainingSeats(businessObject)

        RentalCarBO.saveItem(businessObject, {
          onSuccess: function (response) {
            OBMisc.Notifications.GritterSuccess("Rental Car Saved", "", 250)
            tReq.RentalCarsBusy(false)
          },
          onFail: function (response) {
            tReq.RentalCarsBusy(false)
          }
        })

      }
      else {
        alert('business object not new')
      }
    }
    else {
      businessObject.DriverHumanResourceID(null)
      businessObject.Driver("")
    }
  },
  drawCoDriverCellContent: function (element, index, columnData, businessObject) {
    element.parentElement.style.width = "350px"
    element.parentNode.className = "traveller-cell"
    element.className = 'traveller-list';
    element.innerHTML = '<div class="info">' +
      '<table>' +
      '<tbody>' +
      //header
      '<tr>' +
      '<td style="width:60px;height:60px;">' +
      '<div class="avatar" style="width:50px;height:50px;">' +
      '<img src="' + window.SiteInfo.SitePath + '/Images/InternalPhotos/' + businessObject.ImageFileName + '" alt="Avatar">' +
      '</div>' +
      '</td>' +
      '<td><p style="text-align: left;">' + businessObject.HumanResourceName + '</p></td>' +
      '<td><p style="color:red;" title="' + businessObject.OtherRentalCars + '">' + businessObject.OtherRentalCarsShort + '</p></td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</div>'
  },

  //validation
  DriverValid: function (Value, Rule, CtlError) {
    var passenger = CtlError.Object;
    var checkOnce = false;
    var bulklist = self.RentalCarHumanResourceList();
    var AtLeastOneOnCar = false;
    var DriverCount = 0;
    var DriverCancelled = false;
    for (var n = 0; n < bulklist.length; n++) {
      if (bulklist[n].OnRentalCarInd()) {
        if (!bulklist[n].IsCancelled()) {
          AtLeastOneOnCar = true;
          return;
        }
      }
      if (bulklist[n].DriverInd()) {
        DriverCount++;
        if (bulklist[n].IsCancelled()) {
          DriverCancelled = true;
          DriverCount--;
          return;
        }
      }
    }
    if (AtLeastOneOnCar) {
      if (!checkOnce) {
        if (DriverCount == 0) {
          checkOnce = true;
          //CtlError.AddError("Driver Required");
        }
        else if (DriverCount > 1) {
          checkOnce = true;
          //CtlError.AddError("Only One Driver Required");
        }
      }
    }
  },
  RentalCarAgentValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.RentalCarAgentID()) {
      var RORentalCarAgent = ClientData.RORentalCarAgentList.Find('RentalCarAgentID', RentalCar.RentalCarAgentID())
      //if (RORentalCarAgent.OldInd) {
      //  if (RentalCar.PickupDateTime()) {
      //    if (OBMisc.Dates.IsAfter(RentalCar.PickupDateTime(), RORentalCarAgent.OldDate)) {
      //      CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgent.OldReason);
      //    }
      //  } else {
      //    CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgent.OldReason);
      //  }
      //}
    } else {
      CtlError.AddError("Rental Car Agent is required");
    }
  },
  AgentBranchIDFromValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.AgentBranchIDFrom()) {
      //var RORentalCarAgentBranch = ClientData.RORentalCarAgentBranchList.Find('RentalCarAgentBranchID', RentalCar.AgentBranchIDFrom());
      //if (RORentalCarAgentBranch.OldInd) {
      //  if (RentalCar.PickupDateTime()) {
      //    if (OBMisc.Dates.IsAfter(RentalCar.PickupDateTime(), RORentalCarAgentBranch.OldDate)) {
      //      CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //    }
      //  } else {
      //    CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //  }
      //}
    } else {
      CtlError.AddError("Branch From is required");
    }
  },
  AgentBranchIDToValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.AgentBranchIDTo()) {
      //var RORentalCarAgentBranch = ClientData.RORentalCarAgentBranchList.Find('RentalCarAgentBranchID', RentalCar.AgentBranchIDTo());
      //if (RORentalCarAgentBranch.OldInd) {
      //  if (RentalCar.PickupDateTime()) {
      //    if (OBMisc.Dates.IsAfter(RentalCar.PickupDateTime(), RORentalCarAgentBranch.OldDate)) {
      //      CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //    }
      //  } else {
      //    CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //  }
      //}
    } else {
      CtlError.AddError("Branch To is required");
    }
  },
  CancelledReasonRequired: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.IsCancelled() && RentalCar.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required");
    }
  },
  DriverRequired: function (Value, Rule, CtlError) {
  },
  TravelDatesValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    var PickupDateTime = new Date(RentalCar.PickupDateTime());
    var DropoffDateTime = new Date(RentalCar.DropoffDateTime());
    if (DropoffDateTime <= PickupDateTime) {
      CtlError.AddError("Pickup Date must be before Dropoff Date");
    }
  },
  HRClashCountValid: function (Value, Rule, CtlError) {
    var rc = CtlError.Object;
    if (rc.HRClashCount() > 0) {
      CtlError.AddError("There are passenger clashes on this rental car")
    }
  },
  CrewTypeIDValid(Value, Rule, CtlError) {
    let busObj = CtlError.Object;
    if (busObj.GetParent() && busObj.CrewTypeID() == null && (busObj.GetParent().SystemID() == 1)) {
      CtlError.AddError("Crew Type is required");
    }
  },

  //set
  RentalCarAgentSet: function (self) {
    self.AgentBranchIDFrom(null)
    self.AgentBranchIDTo(null)
  },
  AgentBranchIDSet: function (self) {
    self.AgentBranchIDFrom(null)
    self.AgentBranchIDTo(null)
  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    }
    else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
    }
    self.RentalCarHumanResourceList().Iterate(function (itm, ind) {
      if (itm.OnRentalCarInd()) {
        itm.IsCancelled(self.IsCancelled())
        itm.CancelledByUserID(self.CancelledByUserID())
        itm.CancelledDateTime(self.CancelledDateTime())
      }
    })
  },
  CancelledReasonSet: function (self) {
    self.RentalCarHumanResourceList().Iterate(function (itm, ind) {
      if (itm.OnRentalCarInd()) {
        itm.CancelledReason(self.CancelledReason())
      }
    })
  },
  setRemainingSeats: function (self) {
    var CarType = ClientData.ROCarTypeList.Find('CarTypeID', self.CarTypeID());
    if (CarType != undefined) {
      var MaxNoOfOccupants = CarType.MaxNoOfOccupants;
      var len = self.RentalCarHumanResourceList().length;
      var rchrList = self.RentalCarHumanResourceList();
      var PassengerCount = 0;
      for (var i = 0; i < len; i++) {
        if (rchrList[i].IsCancelled() == false) {
          PassengerCount++;
        }
      }
      var RemainingSeats = '(' + (MaxNoOfOccupants - PassengerCount) + ' seats left)';
      self.MaxSeats(CarType.MaxNoOfOccupants);
      self.RemainingSeats(RemainingSeats);
    }
  },
  CarTypeIDSet: function (self) {
    this.setRemainingSeats(self)
  },
  PickupDateTimeSet: function (self) {
    RentalCarBO.updateClashes(self)
  },
  DropoffDateTimeSet: function (self) {
    RentalCarBO.updateClashes(self)
  },

  //cans
  canEdit: function (RentalCar, FieldName) {
    const isEditableBase = (!RentalCar.IsCancelled() || (RentalCar.IsCancelled() && RentalCar.IsDirty()));
    const allRequiredFieldsSpecified = (RentalCar.RentalCarAgentID() && RentalCar.CarTypeID() && RentalCar.AgentBranchIDFrom() && RentalCar.PickupDateTime() && RentalCar.AgentBranchIDTo());
    if (RentalCar.GetParent().RentalCarsBusy() || RentalCar.IsProcessing()) {
      return false
    }
    else {
      switch (FieldName) {
        case 'CrewTypeID':
          return isEditableBase;
          break;
        case 'RentalCarAgentID':
          return isEditableBase;
          break;
        case 'CarTypeID':
          return (isEditableBase && RentalCar.RentalCarAgentID());
          break
        case 'AgentBranchIDFrom':
          return (isEditableBase && RentalCar.RentalCarAgentID() && RentalCar.CarTypeID());
          break;
        case 'PickupDateTime':
          return (isEditableBase && RentalCar.RentalCarAgentID() && RentalCar.CarTypeID() && RentalCar.AgentBranchIDFrom());
          break;
        case 'AgentBranchIDTo':
          return (isEditableBase && RentalCar.RentalCarAgentID() && RentalCar.CarTypeID() && RentalCar.AgentBranchIDFrom() && RentalCar.PickupDateTime());
          break;
        case 'DropoffDateTime':
          return (isEditableBase && RentalCar.RentalCarAgentID() && RentalCar.CarTypeID() && RentalCar.AgentBranchIDFrom() && RentalCar.PickupDateTime() && RentalCar.AgentBranchIDTo());
          break;
        case 'Driver':
        case 'DriverHumanResourceID':
          return (RentalCar.IsNew() && isEditableBase && allRequiredFieldsSpecified);
          break;
        case 'CoDriver':
        case 'CoDriverHumanResourceID':
          return (RentalCar.IsNew() && isEditableBase && allRequiredFieldsSpecified);
          break;
        case 'CancelButton':
          return isEditableBase;
          break;
        case 'CancelledReason':
          return isEditableBase;
          break;
        case 'UnCancelButton':
          return (RentalCar.IsCancelled())
          break;
        case 'SaveButton':
          return (RentalCar.IsValid() && RentalCar.IsDirty() && !RentalCar.IsProcessing())
          break;
        default:
          return isEditableBase;
          break;
      }
    }
  },
  canView: function (RentalCar, FieldName) {
    switch (RentalCar, FieldName) {
      case 'RentalCarPassengersButton':
      case 'IsSelected':
        return (!RentalCar.IsCancelled())
        break;
      case 'CancelButton':
        return (!RentalCar.IsCancelled())
        break;
      case 'UnCancelButton':
        return (RentalCar.IsCancelled())
        break;
      default:
        return true
        break;
    }
  },

  //other
  InitDate: function (obj) {
    var d = new Date();
    var tr = obj.GetParent()
    if (tr.StartDate() || tr.EndDate()) {
      var sd = KOFormatterFull.Serialise(tr.StartDate());
      var ad = KOFormatterFull.Serialise(tr.EndDate());
      if (sd !== null) {
        d = new Date(tr.StartDate());
      }
      else if (ad !== null) {
        d = new Date(tr.EndDate());
      }
    }
    var first_day = new Date(d.getFullYear(), d.getMonth(), 1);
    return first_day;
  },
  filterROCrewTypes: function (List, rentalCar) {
    var Allowed = [];
    var travelReq = rentalCar.GetParent()
    ClientData.ROCrewTypeList.Iterate(function (crewType, Index) {
      if (travelReq.SystemID() == crewType.SystemID) {
        Allowed.push({ CrewTypeID: crewType.CrewTypeID, TravelScreenDisplayName: crewType.TravelScreenDisplayName });
      }
    });
    return Allowed;
  },
  determineDriver: function (self) {
    self.Driver("")
    self.RentalCarHumanResourceList().Iterate(function (p, pIndx) {
      if (p.DriverInd()) {
        self.Driver(p.HumanResourceName())
      }
    })
  },
  afterItemAdded: function (newItem) {
    newItem.TravelRequisitionID(newItem.GetParent().TravelRequisitionID())
  },
  async updateClashes(rcar) {
    if (rcar.PickupDateTime() && rcar.DropoffDateTime() && !rcar.IsProcessing()) {
      rcar.IsProcessing(true);
      await OBMisc.Tools.sleep(250);
      try {
        let data = await ViewModel.CallServerMethodPromise("UpdateRentalCarClashes", { RentalCar: rcar.Serialise() });
        KOFormatter.Deserialise(data, rcar);
        Singular.Validation.CheckRules(rcar);
        rcar.IsProcessing(false);
      } catch (errorText) {
        OBMisc.Notifications.GritterError("Failed to update clashes", errorText, 1000);
        rcar.IsProcessing(false);
      };
    }
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.RentalCars.RentalCarList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch RentalCars Failed", response.ErrorText, 1000)
        }
      })
  },
  getPassengers: function (rentalCar, options) {
    RentalCarPassengerBO.get({
      criteria: { TravelRequisitionID: rentalCar.TravelRequisitionID(), RentalCarID: rentalCar.RentalCarID() },
      onSuccess: function (response) {
        rentalCar.RentalCarHumanResourceList.Set(response.Data);
        RentalCarBO.updateClashes(rentalCar);
        if (options.onSuccess) { options.onSuccess(response) }
      }
    })
  },
  saveItem: function (RentalCar, options) {
    var me = this
    RentalCar.IsProcessing(true)
    RentalCar.TravelRequisitionID(RentalCar.GetParent().TravelRequisitionID())
    ViewModel.CallServerMethod("SaveRentalCar",
      { RentalCar: RentalCar.Serialise() },
      function (response) {
        if (response.Success) {
          RentalCar.IsProcessing(false)
          KOFormatter.Deserialise(response.Data, RentalCar);
          RentalCarBO.updateClashes(RentalCar);
          if (options.onSuccess) { options.onSuccess.call(me, response) } else { OBMisc.Notifications.GritterSuccess("Rental Car Saved", "", 250) }
        }
        else {
          if (options.onFail) { options.onFail.call(me, response) } else { OBMisc.Notifications.GritterError("Save Failed", "", 1000) }
        }
      })
  },
  save: function (RentalCar, options) {
    var me = this
    ViewModel.CallServerMethod("SaveRentalCar",
      { RentalCar: RentalCar.Serialise() },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, RentalCar);
          RentalCarBO.updateClashes(RentalCar);
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  deleteRentalCars: function (serialisedRentalCarList, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteRentalCars",
      { RentalCarList: serialisedRentalCarList },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  },

  //ui stuff
  rentalCarPassengerButtonCSS: function (RentalCar) {
    if (RentalCar.IsProcessing()) {
      return "btn btn-xs btn-info btn-block"
    }
    else if (RentalCar.PassengerCount() > 0 || RentalCar.IsCancelled()) {
      return "btn btn-xs btn-primary btn-block"
    }
    else if (RentalCar.PassengerCount() == 0) {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
    else {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
  },
  rentalCarPassengerButtonIcon: function (RentalCar) {
    if (RentalCar.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-user"
    }
  }
}

RentalCarPassengerBO = {
  toString: function (self) {
    return self.HumanResourceName()
  },

  canEdit: function (self, FieldName) {
    if (self.HasClashes() && !self.OnRentalCarInd()) {
      return false
    } else {
      switch (FieldName) {
        case 'OnCarButton':
          if ((self.IsCancelled() && !self.IsDirty()) || self.ReAddedToRentalCar()) {
            return false
          } else {
            return true
          }
          break;
        case 'DriverInd':
          if ((self.IsCancelled() && !self.IsDirty()) || self.CoDriverInd() || (self.GetParent().Driver() != "" && self.GetParent().Driver() != self.HumanResourceName())) {
            return false
          } else {
            return true
          }
          break;
        case 'CoDriverInd':
          if ((self.IsCancelled() && !self.IsDirty()) || self.DriverInd()) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelButton':
          if ((self.IsCancelled() && !self.IsDirty()) || self.ReAddedToRentalCar()) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelledReason':
          if ((self.IsCancelled() && !self.IsDirty()) || self.ReAddedToRentalCar()) {
            return false
          } else {
            return true
          }
          break;
        case 'ReAddButton':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return true
          } else {
            return false
          }
          break;
      }
    }
  },
  canView: function (self, FieldName) {
    switch (FieldName) {
      case 'OnCarButton':
        if (self.OnRentalCarInd()) {
          return true;
        } else {
          return !self.HasClashes();
        }
        break;
      case 'OtherRentalCarsButton':
        return self.HasClashes()
        break;
    }
  },
  readdToBooking: function (self) {
    var newItem = self.GetParent().RentalCarHumanResourceList.AddNew()
    newItem.OnRentalCarInd(true)
    newItem.RentalCarID(self.GetParent().RentalCarID())
    newItem.HumanResourceID(self.HumanResourceID())
    newItem.DriverInd(false)
    newItem.CoDriverInd(false)
    newItem.BookingReason(self.BookingReason())
    newItem.HumanResourceName(self.HumanResourceName())
    newItem.CityCode(self.CityCode())
    newItem.DriverLicenseInd(self.DriverLicenseInd())
    newItem.ReAddedToRentalCar(true)
    //check clashes
    RentalCarBO.updateClashes(self.GetParent())
  },

  TimesValid: function (Value, Rule, CtlError) {
    var ChauffeurDrive = CtlError.Object;
    var PickupDateTime = new Date(ChauffeurDrive.PickupDateTime());
    var DropoffDateTime = new Date(ChauffeurDrive.DropoffDateTime());
    if (DropoffDateTime <= PickupDateTime) {
      CtlError.AddError("Pickup Time must be before Dropoff Time");
    }
  },
  CancelledReasonValid: function (Value, Rule, CtlError) {
    var CurrentBulkRentalCarPassenger = CtlError.Object;
    if (CurrentBulkRentalCarPassenger.IsCancelled() && CurrentBulkRentalCarPassenger.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkRentalCarPassenger.HumanResourceName());
    }
  },
  OnRentalCarIndValid(Value, Rule, CtlError) {
    let self = CtlError.Object;
    if (self.HasClashes() && self.OnRentalCarInd()) {
      CtlError.AddError(self.HumanResourceName() + " has rental car clashes");
    };
  },

  DriverIndSet: function (self) {
    RentalCarBO.determineDriver(self.GetParent())
  },
  CoDriverIndSet: function (self) {
    //RentalCarBO.determineDriver(self.GetParent())
  },
  CancelledReasonSet: function (self) {

  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.DriverInd(false)
      self.CoDriverInd(false)
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    } else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
      self.CancelledReason("")
    }
  },
  OnRentalCarIndSet: function (self) {
    if (!self.OnRentalCarInd()) {
      self.DriverInd(false);
      self.CoDriverInd(false);
      self.IsCancelled(false);
      self.CancelledReason("");
    }
    let rcar = self.GetParent();
    RentalCarBO.updateClashes(rcar);
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.RentalCars.RentalCarHumanResourceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Passengers Failed", response.ErrorText, 1000)
        }
      })
  }
}

AccommodationBO = {
  toString: function (self) {
    var deptTime = ''
    if (self.CheckInDate()) {
      deptTime = ' - ' + new Date(self.CheckInDate()).format('dd MMM yy')
    }
    return 'Accommodation: ' + self.AccommodationProvider() + deptTime
  },

  //dropdowns
  triggerAccommodationProviderIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAccommodationProviderIDCriteriaBeforeRefresh: function (args) {
  },
  afterAccommodationProviderIDRefreshAjax: function (args) {
  },
  onAccommodationProviderSelected: function (item, businessObject) {
    businessObject.AccomProviderCityID(item.CityID)

  },

  //set
  CheckInDateSet: function (self) {
    this.calculateNightsAway(self);
    this.updateClashes(self);
  },
  CheckOutDateSet: function (self) {
    this.calculateNightsAway(self);
    this.updateClashes(self);
  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    }
    else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
      self.CancelledReason("")
    }
    self.AccommodationHumanResourceList().Iterate(function (itm, ind) {
      if (itm.OnAccommodationInd()) {
        itm.IsCancelled(self.IsCancelled())
        itm.CancelledByUserID(self.CancelledByUserID())
        itm.CancelledDateTime(self.CancelledDateTime())
        itm.CancelledReason(self.CancelledReason())
      }
    })
  },
  CancelledReasonSet: function (self) {
    //if (self.IsCancelled()) {
    //  self.CancelledByUserID(ViewModel.CurrentUserID())
    //  self.CancelledDateTime(new Date())
    //}
    //else {
    //  self.CancelledByUserID(null)
    //  self.CancelledDateTime(null)
    //  self.CancelledReason("")
    //}
    self.AccommodationHumanResourceList().Iterate(function (itm, ind) {
      // && itm.IsCancelled()
      if (itm.OnAccommodationInd()) {
        //itm.IsCancelled(self.IsCancelled())
        //itm.CancelledByUserID(self.CancelledByUserID())
        //itm.CancelledDateTime(self.CancelledDateTime())
        itm.CancelledReason(self.CancelledReason())
      }
    })
  },

  //validation
  CancelledReasonValid: function (Value, Rule, CtlError) {
    var Accommodation = CtlError.Object;
    if (Accommodation.IsCancelled() && Accommodation.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required");
    }
  },
  AccommodationDatesValid: function (Value, Rule, CtlError) {
    var Accommodation = CtlError.Object;
    var CheckInDate = new Date(Accommodation.CheckInDate());
    var CheckOutDate = new Date(Accommodation.CheckOutDate());
    if (CheckOutDate < CheckInDate) {
      CtlError.AddError("Check In Date must be before Check Out Date");
    }
  },
  HRClashCountValid: function (Value, Rule, CtlError) {
    var rc = CtlError.Object;
    if (rc.HRClashCount() > 0) {
      CtlError.AddError("There are guest clashes on this accommodation");
    }
  },
  async updateClashes(rcar) {
    if (rcar.CheckInDate() && rcar.CheckOutDate()) {
      rcar.IsProcessing(true);
      await OBMisc.Tools.sleep(250);
      try {
        let data = await ViewModel.CallServerMethodPromise("UpdateAccommodationClashes", { Accommodation: rcar.Serialise() });
        KOFormatter.Deserialise(data, rcar);
        Singular.Validation.CheckRules(rcar);
        rcar.IsProcessing(false);
      } catch (errorText) {
        OBMisc.Notifications.GritterError("Failed to update clashes", errorText, 1000);
        rcar.IsProcessing(false);
      };
    }
  },

  InitDate: function (obj) {
    var d = new Date();
    var tr = obj.GetParent()
    if (tr.StartDate() || tr.EndDate()) {
      var sd = KOFormatterFull.Serialise(tr.StartDate());
      var ad = KOFormatterFull.Serialise(tr.EndDate());
      if (sd !== null) {
        d = new Date(tr.StartDate());
      }
      else if (ad !== null) {
        d = new Date(tr.EndDate());
      }
    }
    var first_day = new Date(d.getFullYear(), d.getMonth(), 1);
    return first_day;
  },
  calculateNightsAway: function (self) {
    if (self.CheckInDate() != null && self.CheckOutDate() != null) {
      //Get 1 day in milliseconds
      var one_day = 1000 * 60 * 60 * 24;
      // Convert both dates to milliseconds
      var date1_ms = (new Date(self.CheckInDate())).getTime();
      var date2_ms = (new Date(self.CheckOutDate())).getTime();
      // Calculate the difference in milliseconds
      var difference_ms = date2_ms - date1_ms;
      // Convert back to days and return
      self.NightsAway(Math.round(difference_ms / one_day));
    }
    else {
      self.NightsAway(0);
    }
  },

  //cans
  canEdit: function (Accommodation, FieldName) {
    const isEditableBase = (!Accommodation.IsCancelled() || (Accommodation.IsCancelled() && Accommodation.IsDirty()));
    switch (FieldName) {
      case 'AccommodationProviderID':
        return isEditableBase;
        break;
      case 'CheckInDate':
        return (isEditableBase && Accommodation.AccommodationProviderID());
        break;
      case 'CheckOutDate':
        return (isEditableBase && Accommodation.AccommodationProviderID() && Accommodation.CheckInDate());
        break;
      case 'AccommodationGuestsButton':
        return (isEditableBase && Accommodation.AccommodationProviderID() && Accommodation.CheckInDate() && Accommodation.CheckOutDate());
        break;
      case 'CancelButton':
        return isEditableBase;
        break;
      case 'CancelledReason':
        return isEditableBase;
        break;
      case 'UnCancelButton':
        return (Accommodation.IsCancelled())
        break;
      case 'SaveButton':
        return (Accommodation.IsValid() && Accommodation.IsDirty() && !Accommodation.IsProcessing())
        break;
      default:
        return isEditableBase;
        break;
    }
  },
  canView: function (Accommodation, FieldName) {
    switch (Accommodation, FieldName) {
      case 'AccommodationGuestsButton':
      case 'IsSelected':
        return (!Accommodation.IsCancelled())
        break;
      case 'CancelButton':
        return (!Accommodation.IsCancelled())
        break;
      case 'UnCancelButton':
        return (Accommodation.IsCancelled())
        break;
      default:
        return true
        break;
    }
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Accommodation.AccommodationList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Accommodation Failed", response.ErrorText, 1000)
        }
      })
  },
  getGuests: function (accommodation, options) {
    AccommodationGuestBO.get({
      criteria: { TravelRequisitionID: accommodation.TravelRequisitionID(), AccommodationID: accommodation.AccommodationID() },
      onSuccess: function (response) {
        accommodation.AccommodationHumanResourceList.Set(response.Data)
        if (options.onSuccess) { options.onSuccess(response) }
      }
    })
  },
  saveItem: function (Accommodation, options) {
    var me = this
    Accommodation.IsProcessing(true)
    Accommodation.TravelRequisitionID(Accommodation.GetParent().TravelRequisitionID())
    ViewModel.CallServerMethod("SaveAccommodation",
      { Accommodation: Accommodation.Serialise() },
      function (response) {
        if (response.Success) {
          Accommodation.IsProcessing(false)
          KOFormatter.Deserialise(response.Data, Accommodation)
          if (options.onSuccess) { options.onSuccess.call(me, response) } else { OBMisc.Notifications.GritterSuccess("Accommodation Saved", "", 250) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  save: function (Accommodation, options) {
    var me = this
    ViewModel.CallServerMethod("SaveAccommodation",
      { Accommodation: Accommodation.Serialise() },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, Accommodation)
          //, "IsValid", "IsSelfValid"
          //var updatedAccommodation = OBMisc.Knockout.updateObservableFromJSON(Accommodation, response.Data, ["IsProcessing"])
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  deleteAccommodation: function (serialisedAccommodationList, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteAccommodation",
      { AccommodationList: serialisedAccommodationList },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  },

  //ui stuff
  accommodationGuestButtonCSS: function (Accommodation) {
    if (Accommodation.IsProcessing()) {
      return "btn btn-xs btn-info btn-block"
    }
    else if (Accommodation.GuestCount() > 0 || Accommodation.IsCancelled()) {
      return "btn btn-xs btn-primary btn-block"
    }
    else if (Accommodation.GuestCount() == 0) {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
    else {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
  },
  accommodationGuestButtonIcon: function (Accommodation) {
    if (Accommodation.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-user"
    }
  }
}

AccommodationGuestBO = {
  toString: function (self) {
    return self.HumanResourceName()
  },

  IsSelectedSet: function (self) {
    self.OnAccommodationInd(self.IsSelected());
  },
  OnAccommodationIndSet: function (self) {
    self.IsCancelled(false);
    self.CancelledReason("");
    AccommodationBO.updateClashes(self.GetParent());
  },
  CancelledReasonSet: function (self) {
    //var bag = self;
    //var ahr = self.AccommodationHumanResourceList().Find("AccommodationHumanResourceID", bag.AccommodationHumanResourceID());
    //ahr.CancelledReason(bag.CancelledReason());
  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    } else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
      self.CancelledReason("")
    }
  },

  IsOnAccommodationValid(Value, Rule, CtlError) {
    let self = CtlError.Object;
    if (self.HasClashes() && self.OnAccommodationInd()) {
      CtlError.AddError(self.HumanResourceName() + " has accommodation clashes");
    };
  },

  canEdit: function (self, FieldName) {
    if (self.HasClashes() && FieldName != 'OnAccommodationButton') {
      return false
    } else {
      switch (FieldName) {
        case 'OnAccommodationButton':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            if (!self.OnAccommodationInd() && !self.AccommodationHumanResourceID() && self.OtherAccommodation().length > 0) {
              return false;
            }
            return true;
          }
          break;
        case 'CancelButton':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelledReason':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            return true
          }
          break;
        case 'ReAddButton':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return true
          } else {
            return false
          }
          break;
      }
    }
  },
  canView: function (self, FieldName) {
    switch (FieldName) {
      case 'OnAccommodationButton':
        return true;
        break;
      case 'ClashesButton':
        return self.HasClashes()
        break;
      case 'OtherAccommodationButton':
        return self.HasClashes()
        break;
    }
  },
  readdToBooking: function (self) {
    //var newItem = self.GetParent().RentalCarHumanResourceList.AddNew()
    //newItem.OnRentalCarInd(true)
    //newItem.RentalCarID(self.GetParent().RentalCarID())
    //newItem.HumanResourceID(self.HumanResourceID())
    //newItem.DriverInd(false)
    //newItem.CoDriverInd(false)
    //newItem.BookingReason(self.BookingReason())
    //newItem.HumanResourceName(self.HumanResourceName())
    //newItem.CityCode(self.CityCode())
    //newItem.DriverLicenseInd(self.DriverLicenseInd())
    //newItem.ReAddedToRentalCar(true)
    ////check clashes
    //RentalCarBO.updateClashes(self.GetParent())
  },

  BookingReasonValid: function (Value, Rule, CtlError) {
    var CurrentBulkAccommodationGuest = CtlError.Object;
    var CurrentAccommodation = CurrentBulkAccommodationGuest.GetParent();
    if (CurrentAccommodation) {
      //var AccommInBaseCityInd = CurrentBulkAccommodationGuest.AccommInBaseCityInd();
      var bookingReason = CurrentBulkAccommodationGuest.BookingReason();
      if (CurrentBulkAccommodationGuest.OnAccommodationInd()
        && (CurrentBulkAccommodationGuest.BaseCityID() == CurrentBulkAccommodationGuest.GetParent().AccomProviderCityID())
        && bookingReason.trim() == "") {
        var error = "Booking Reason Required: " + CurrentBulkAccommodationGuest.HumanResourceName() + "'s Base City is the same as Accommodation Provider's City";
        CtlError.AddError(error);
      }
    }
  },
  CancelledReasonValid: function (Value, Rule, CtlError) {
    var CurrentBulkAccommodationGuest = CtlError.Object;
    if (CurrentBulkAccommodationGuest.IsCancelled() && CurrentBulkAccommodationGuest.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkAccommodationGuest.HumanResourceName());
    }
  },

  UpdateRoomCost: function (self) {
    var bag = self;
    var ahr = ViewModel.CurrentAccommodation().AccommodationHumanResourceList().Find("AccommodationHumanResourceID", bag.AccommodationHumanResourceID());
    if (ahr == undefined) {// if Guest is added to accommodation but not yet saved, there will be no bag.AccommodationHumanResourceID, so need to match on HumanResourceID, N.B. Look out for cancelled guests
      var len = ViewModel.CurrentAccommodation().AccommodationHumanResourceList().length;
      var ahrList = ViewModel.CurrentAccommodation().AccommodationHumanResourceList();
      for (var i = 0; i < len; i++) {
        if (ahrList[i].IsCancelled() == false && ahrList[i].HumanResourceID() == bag.HumanResourceID()) {
          ahr = ahrList[i];
        } //if
      } //for
    } //if
    if (ahr != undefined) {
      ahr.RoomCost(bag.RoomCost());
    }
  },
  SetRoomCost: function (self) {
    if (self.OnAccommodationInd() == true) {
      var CurrentAccommodation = ViewModel.CurrentAccommodation();
      if (CurrentAccommodation) {
        self.RoomCost(CurrentAccommodation.PerPersonPerNightCost());
      }
    }
    else {
      self.RoomCost(0.0);
    }
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Accommodation.AccommodationHumanResourceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Guests Failed", response.ErrorText, 1000)
        }
      })
  }
}

AccommodationSupplierGuestBO = {
  UpdateRoomCost: function (self) {
    var bag = self;
    var ahr = ViewModel.CurrentAccommodation().AccommodationSupplierHumanResourceList().Find("AccommodationSupplierHumanResourceID", bag.AccommodationSupplierHumanResourceID());
    if (ahr == undefined) {// if Guest is added to accommodation but not yet saved, there will be no bag.AccommodationSupplierHumanResourceID, so need to match on SupplierHumanResourceID, N.B. Look out for cancelled guests
      var len = ViewModel.CurrentAccommodation().AccommodationSupplierHumanResourceList().length;
      var ahrList = ViewModel.CurrentAccommodation().AccommodationSupplierHumanResourceList();
      for (var i = 0; i < len; i++) {
        if (ahrList[i].IsCancelled() == false && ahrList[i].SupplierHumanResourceID() == bag.SupplierHumanResourceID()) {
          ahr = ahrList[i];
        } //if
      } //for
    } //if
    if (ahr != undefined) ahr.RoomCost(bag.RoomCost());
  },
  BulkAccommodationSupplierGuestBOToString: function (self) {
    var SupplierHumanResource = (self.SupplierHumanResource() == undefined) ? '' : ' - Supplier Human Resource: ' + self.SupplierHumanResource();
    return 'Bulk Accommodation Supplier Guest' + SupplierHumanResource;
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Accommodation.AccommodationSupplierHumanResourceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Guests Failed", response.ErrorText, 1000)
        }
      })
  }
}

ChauffeurDriverBO = {
  toString: function (self) {
    var deptTime = ''
    if (self.PickUpDateTime()) {
      deptTime = ' - ' + new Date(self.PickUpDateTime()).format('dd MMM yy')
    }
    return 'Chauffeur: ' + self.PickUpLocation() + deptTime
  },

  CancelledReasonValid: function (Value, Rule, CtlError) {
    var ChauffeurDriver = CtlError.Object;
    if (ChauffeurDriver.IsCancelled() == true && ChauffeurDriver.CancelledReason() == '') {
      CtlError.AddError('A Reason for cancellation is required');
    }
  },
  TimesValid: function (Value, Rule, CtlError) {
    var ChauffeurDrive = CtlError.Object;
    var PickUpDateTime = new Date(ChauffeurDrive.PickUpDateTime());
    var DropOffDateTime = new Date(ChauffeurDrive.DropOffDateTime());
    if (DropOffDateTime <= PickUpDateTime) {
      CtlError.AddError("Pickup Time must be before Dropoff Time");
    }
  },
  MainPassengerValid: function (Value, Rule, CtlError) {
    var rc = CtlError.Object
    if (!rc.IsCancelled() && rc.MainPassenger().trim().length == 0) {
      //CtlError.AddError("Main Passenger is required")
    }
  },

  InitDate: function (obj) {
    var d = new Date();
    var tr = obj.GetParent()
    if (tr.StartDate() || tr.EndDate()) {
      var sd = KOFormatterFull.Serialise(tr.StartDate());
      var ad = KOFormatterFull.Serialise(tr.EndDate());
      if (sd !== null) {
        d = new Date(tr.StartDate());
      }
      else if (ad !== null) {
        d = new Date(tr.EndDate());
      }
    }
    var first_day = new Date(d.getFullYear(), d.getMonth(), 1);
    return first_day;
  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID())
      self.CancelledDateTime(new Date())
    }
    else {
      self.CancelledByUserID(null)
      self.CancelledDateTime(null)
      self.CancelledReason("")
    }
    self.ChauffeurDriverHumanResourceList().Iterate(function (itm, ind) {
      if (itm.OnCarInd()) {
        itm.IsCancelled(self.IsCancelled())
        itm.CancelledByUserID(self.CancelledByUserID())
        itm.CancelledDateTime(self.CancelledDateTime())
        itm.CancelledReason(self.CancelledReason())
      }
    })
  },
  CancelledReasonSet: function (self) {
    self.ChauffeurDriverHumanResourceList().Iterate(function (itm, ind) {
      if (itm.OnCarInd()) {
        itm.CancelledReason(self.CancelledReason())
      }
    })
  },
  PickUpDateTimeSet: function (self) {
    ChauffeurDriverBO.updateClashes(self)
  },
  DropOffDateTimeSet: function (self) {
    ChauffeurDriverBO.updateClashes(self)
  },

  PickUpCheck: function (Value, Rule, CtlError) {
    var ChauffeurDriver = CtlError.Object;
    if (ChauffeurDriver.PickUpLocation() != '' && ChauffeurDriver.PickUpLocation().localeCompare(ChauffeurDriver.DropOffLocation()) == 0) {
      CtlError.AddWarning('Pick Up location is the same as the Drop Off location');
    }
  },
  DropOffCheck: function (Value, Rule, CtlError) {
    var ChauffeurDriver = CtlError.Object;
    if (ChauffeurDriver.DropOffLocation() != '' && ChauffeurDriver.PickUpLocation().localeCompare(ChauffeurDriver.DropOffLocation()) == 0) {
      CtlError.AddWarning('');
    }
  },
  HRClashCountValid: function (Value, Rule, CtlError) {
    var rc = CtlError.Object;
    if (rc.HRClashCount() > 0) {
      CtlError.AddError("There are passenger clashes on this chauffeur drive")
    }
  },

  //cans
  canEdit: function (ChauffeurDriver, FieldName) {
    const isEditableBase = (!ChauffeurDriver.IsCancelled() || (ChauffeurDriver.IsCancelled() && ChauffeurDriver.IsDirty()));
    const allRequiredFieldsSpecified = ((ChauffeurDriver.PickUpLocation().trim().length > 0) && ChauffeurDriver.PickUpDateTime() && (ChauffeurDriver.DropOffLocation().trim().length > 0) && ChauffeurDriver.DropOffDateTime());
    switch (FieldName) {
      case 'PickUpLocation':
        return (isEditableBase);
        break;
      case 'PickUpDateTime':
        return (isEditableBase && (ChauffeurDriver.PickUpLocation().trim().length > 0));
        break;
      case 'DropOffLocation':
        return (isEditableBase && (ChauffeurDriver.PickUpLocation().trim().length > 0) && ChauffeurDriver.PickUpDateTime());
        break;
      case 'DropOffDateTime':
        return (isEditableBase && (ChauffeurDriver.PickUpLocation().trim().length > 0) && ChauffeurDriver.PickUpDateTime() && (ChauffeurDriver.DropOffLocation().trim().length > 0));
        break;
      case 'ChauffeurPassengersButton':
        return (isEditableBase && allRequiredFieldsSpecified);
        break;
      case 'CancelButton':
        return (!ChauffeurDriver.IsCancelled() || (ChauffeurDriver.IsCancelled() && ChauffeurDriver.IsDirty()))
        break;
      case 'CancelledReason':
        return (!ChauffeurDriver.IsCancelled() || (ChauffeurDriver.IsCancelled() && ChauffeurDriver.IsDirty()))
        break;
      case 'UnCancelButton':
        return (ChauffeurDriver.IsCancelled())
        break;
      case 'SaveButton':
        return (ChauffeurDriver.IsValid() && ChauffeurDriver.IsDirty() && !ChauffeurDriver.IsProcessing())
        break;
      default:
        return (!ChauffeurDriver.IsCancelled() || (ChauffeurDriver.IsCancelled() && ChauffeurDriver.IsDirty()))
        break;
    }
  },
  canView: function (ChauffeurDriver, FieldName) {
    switch (ChauffeurDriver, FieldName) {
      case 'ChauffeurPassengersButton':
      case 'IsSelected':
        return (!ChauffeurDriver.IsCancelled() || (ChauffeurDriver.IsCancelled() && ChauffeurDriver.IsDirty()))
        break;
      case 'CancelButton':
        return (!ChauffeurDriver.IsCancelled())
        break;
      case 'UnCancelButton':
        return (ChauffeurDriver.IsCancelled())
        break;
      default:
        return true
        break;
    }
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Chauffeurs.ChauffeurDriverList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Chauffeurs Failed", response.ErrorText, 1000)
        }
      })
  },
  getPassengers: function (chaufferDriver, options) {
    ChauffeurPassengerBO.get({
      criteria: { TravelRequisitionID: chaufferDriver.TravelRequisitionID(), ChauffeurDriverID: chaufferDriver.ChauffeurDriverID() },
      onSuccess: function (response) {
        chaufferDriver.ChauffeurDriverHumanResourceList.Set(response.Data)
        if (options.onSuccess) { options.onSuccess(response) }
      }
    })
  },
  saveItem: function (ChauffeurDriver, options) {
    var me = this
    ChauffeurDriver.IsProcessing(true)
    ChauffeurDriver.TravelRequisitionID(ChauffeurDriver.GetParent().TravelRequisitionID())
    ViewModel.CallServerMethod("SaveChauffeurDriver",
      { ChauffeurDriver: ChauffeurDriver.Serialise() },
      function (response) {
        if (response.Success) {
          ChauffeurDriver.IsProcessing(false)
          KOFormatter.Deserialise(response.Data, ChauffeurDriver)
          if (options.onSuccess) { options.onSuccess.call(me, response) } else { OBMisc.Notifications.GritterSuccess("Chauffeur Saved", "", 250) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  save: function (ChauffeurDriver, options) {
    var me = this
    ViewModel.CallServerMethod("SaveChauffeurDriver",
      { ChauffeurDriver: ChauffeurDriver.Serialise() },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, ChauffeurDriver)
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  deleteChauffeur: function (serialisedChauffeurList, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteChauffeurDriver",
      { ChauffeurDriverList: serialisedChauffeurList },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  },

  //ui stuff
  chauffeurPassengerButtonCSS: function (ChauffeurDriver) {
    if (ChauffeurDriver.IsProcessing()) {
      return "btn btn-xs btn-info btn-block"
    }
    else if (ChauffeurDriver.PassengerCount() > 0 || ChauffeurDriver.IsCancelled()) {
      return "btn btn-xs btn-primary btn-block"
    }
    else if (ChauffeurDriver.PassengerCount() == 0) {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
    else {
      return "btn btn-xs btn-danger btn-block animated tada infinite go"
    }
  },
  chauffeurPassengerButtonIcon: function (ChauffeurDriver) {
    if (ChauffeurDriver.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-user"
    }
  },

  //other
  determineMainPassenger: function (self) {
    self.MainPassenger("")
    self.ChauffeurDriverHumanResourceList().Iterate(function (p, pIndx) {
      if (p.MainPassengerInd()) {
        self.MainPassenger(p.HumanResource())
      }
    })
  },
  async updateClashes(rcar) {
    if (rcar.PickUpDateTime() && rcar.DropOffDateTime() && !rcar.IsProcessing()) {
      rcar.IsProcessing(true);
      await OBMisc.Tools.sleep(250);
      try {
        let data = await ViewModel.CallServerMethodPromise("UpdateChauffeurClashes", { ChauffeurDriver: rcar.Serialise() });
        KOFormatter.Deserialise(data, rcar);
        Singular.Validation.CheckRules(rcar);
        rcar.IsProcessing(false);
      } catch (errorText) {
        OBMisc.Notifications.GritterError("Failed to update clashes", errorText, 1000);
        rcar.IsProcessing(false);
      };
    }
  }
}

ChauffeurPassengerBO = {
  toString: function (self) {
    return self.HumanResource()
  },

  CancelledReasonValid: function (Value, Rule, CtlError) {
    var CurrentBulkChauffeurPassenger = CtlError.Object;
    if (CurrentBulkChauffeurPassenger.IsCancelled() & CurrentBulkChauffeurPassenger.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkChauffeurPassenger.HumanResource());
    }
  },
  OnCarIndValid(Value, Rule, CtlError) {
    let self = CtlError.Object;
    if (self.OnCarInd() && self.HasClashes()) {
      CtlError.AddError(self.HumanResource() + " has rental car clashes");
    };
    if (!self.OnCarInd() && self.MainPassengerInd()) {
      CtlError.AddError(self.HumanResource() + " cannot be the main passenger");
    };
  },

  canEdit: function (self, FieldName) {
    if (self.HasClashes() && !self.OnCarInd()) {
      return false
    } else {
      switch (FieldName) {
        case 'OnCarButton':
          if ((self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            return true
          }
          break;
        case 'MainPassengerInd':
          if (!self.IsSelected() || (self.IsCancelled() && !self.IsDirty()) || (self.GetParent().MainPassenger() != "" && self.GetParent().MainPassenger() != self.HumanResource())) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelButton':
          if (!self.IsSelected() || (self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            return true
          }
          break;
        case 'CancelledReason':
          if (!self.IsSelected() || (self.IsCancelled() && !self.IsDirty())) {
            return false
          } else {
            return true
          }
          break;
        case 'ReAddButton':
          if (!self.IsSelected() || (self.IsCancelled() && !self.IsDirty())) {
            return true
          } else {
            return false
          }
          break;
      }
    }
  },
  canView: function (self, FieldName) {
    switch (FieldName) {
      case 'OnCarButton':
        if (self.OnCarInd()) {
          return true;
        } else {
          return !self.HasClashes();
        }
        break;
      case 'OtherChauffeursButton':
        return self.HasClashes()
        break;
    }
  },

  OnCarIndSet: function (self) {
    self.IsSelected(self.OnCarInd());
    if (!self.OnCarInd()) {
      self.MainPassengerInd(false);
      self.IsCancelled(false);
      self.CancelledReason("");
    };
    let rcar = self.GetParent();
    ChauffeurDriverBO.updateClashes(rcar);
  },
  MainPassengerIndSet: function (self) {
    ChauffeurDriverBO.determineMainPassenger(self.GetParent())
  },
  CancelledReasonSet: function (self) {

  },
  IsCancelledSet: function (self) {
    if (self.IsCancelled()) {
      self.CancelledByUserID(ViewModel.CurrentUserID());
      self.CancelledDateTime(new Date());
      self.MainPassengerInd(false);
    } else {
      self.CancelledByUserID(null);
      self.CancelledDateTime(null);
      self.CancelledReason("");
    }
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResourceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Passengers Failed", response.ErrorText, 1000)
        }
      })
  }
}

TravelAdvanceDetailBO = {
  toString: function (self) {
    return 'Travel Advance: ' + self.HumanResource()
  },

  //dropdowns
  setHumanResourceCriteriaBeforeRefresh(args) {
    args.Data.TravelRequisitionID = args.Object.GetParent().TravelRequisitionID();
  },
  triggerHumanResourceAutoPopulate(args) {
    args.AutoPopulate = true
  },
  afterHumanResourceRefreshAjax(self) {

  },
  onHumanResourceSelected(selectedItem, businessObject) {

  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetailList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Advances Failed", response.ErrorText, 1000)
        }
      })
  },
  deleteTravelAdvances: function (serialisedTravelAdvances, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteTravelAdvances",
      { TravelAdvanceList: serialisedTravelAdvances },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  }
}

TravelReqCommentBO = {
  toString: function (self) {
    return 'Comment: '
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Comments.TravelReqCommentList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Comments Failed", response.ErrorText, 1000)
        }
      })
  },
  deleteTravelComments: function (serialisedComments, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteTravelReqComments",
      { TravelAdvanceList: serialisedComments },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  }
}

TravellerSnTBO = {
  IsExpandedSet: function (TravellerSnT) {
  },
  SnTFetchedSet: function (TravellerSnT) {
    if (TravellerSnT.SnTFetched()) {
      this.fetchDays(TravellerSnT);
      Singular.Validation.CheckRules(TravellerSnT);
    } else {
      TravellerSnT.SnTDetailList([]);
      TravellerSnT.IsExpanded(false);
      Singular.Validation.CheckRules(TravellerSnT);
    }
  },
  expandButtonHTML: function (TravellerSnT) {
    if (TravellerSnT.IsProcessing()) {
      return "<i class='fa fa-refresh fa-spin' /> Fetching Days..."
    } else {
      if (TravellerSnT.IsExpanded()) {
        return "<i class='fa fa-eye-slash' /> Hide Detail"
      } else {
        return "<i class='fa fa-eye' /> View Detail"
      }
    }
  },

  //set
  DefaultGroupSnTIDSet: function (TravellerSnT) {
    if (!TravellerSnT.SnTFetched()) {
      this.fetchDays(TravellerSnT, function () {
        TravellerSnT.SnTDetailList().Iterate(function (sntDay, sntDayIndex) {
          if (sntDay.CanEditRecord()) {
            sntDay.GroupSnTID(TravellerSnT.DefaultGroupSnTID())
          }
        })
      })
    } else {
      TravellerSnT.SnTDetailList().Iterate(function (sntDay, sntDayIndex) {
        if (sntDay.CanEditRecord()) {
          sntDay.GroupSnTID(TravellerSnT.DefaultGroupSnTID())
        }
      })
    }
  },

  //data access
  fetchDays: function (TravellerSnT, afterFetch) {
    var tReq = TravellerSnT.GetParent()
    TravellerSnT.IsProcessing(true)
    TravellerSnT.IsExpanded(false)
    this.get({
      criteria: {
        TravelRequisitionID: tReq.TravelRequisitionID(), HumanResourceID: TravellerSnT.HumanResourceID(), FetchSnTDetails: true,
        SnTStartDateOverride: TravellerSnT.SnTStartDate(), SnTEndDateOverride: TravellerSnT.SnTEndDate()
      },
      onSuccess: function (response) {
        if (response.Success) {
          TravellerSnT.SnTDetailList.Set(response.Data[0].SnTDetailList)
          TravellerSnT.IsExpanded(true)
          if (afterFetch) { afterFetch() }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Days Failed", response.Data.ErrorText, 1000)
        }
        TravellerSnT.IsProcessing(false)
      }
    })
  },
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.Travellers.TravellerSnTList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch S&T Failed", response.ErrorText, 1000)
        }
      })
  },
  save: function (list, serialisedList, options) {
    ViewModel.CallServerMethod("SaveTravelRequisitionTravellerSnTList",
      { TravelRequisitionTravellerSnTList: (list ? list.Serialise() : serialisedList) },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
        }
      })
  },
  saveItem: function (travellerSnT, options) {
    travellerSnT.IsProcessing(true)
    var list = []
    list.push(travellerSnT.Serialise())
    this.save(null, list, {
      onSuccess: function (response) {
        TravellerSnTBO.get({
          criteria: { TravelRequisitionID: travellerSnT.GetParent().TravelRequisitionID(), HumanResourceID: travellerSnT.HumanResourceID(), FetchSnTDetails: false },
          onSuccess: function (response) {
            KOFormatter.Deserialise(response.Data[0], travellerSnT)
            travellerSnT.IsExpanded(false)
          }
        })
      }
    })
  }
}

SnTDetailBO = {
  breakfastButtonCss: function (SnTDetail) {
    var base = 'btn btn-xs btn-block';
    if (SnTDetail.IsBreakfastCurrentEvent()) {
      if (SnTDetail.BreakfastAmount() > 0) {
        base += ' btn-success'
      } else {
        base += ' btn-default'
      }
    } else {
      if (SnTDetail.BreakfastAmount() > 0) {
        base += ' btn-warning'
      } else {
        base += ' btn-default'
      }
    }
    return base
  },
  lunchButtonCss: function (SnTDetail) {
    var base = 'btn btn-xs btn-block';
    if (SnTDetail.IsLunchCurrentEvent()) {
      if (SnTDetail.LunchAmount() > 0) {
        base += ' btn-success'
      } else {
        base += ' btn-default'
      }
    } else {
      if (SnTDetail.LunchAmount() > 0) {
        base += ' btn-warning'
      } else {
        base += ' btn-default'
      }
    }
    return base
  },
  dinnerButtonCss: function (SnTDetail) {
    var base = 'btn btn-xs btn-block';
    if (SnTDetail.IsDinnerCurrentEvent()) {
      if (SnTDetail.DinnerAmount() > 0) {
        base += ' btn-success'
      } else {
        base += ' btn-default'
      }
    } else {
      if (SnTDetail.DinnerAmount() > 0) {
        base += ' btn-warning'
      } else {
        base += ' btn-default'
      }
    }
    return base
  },
  incidentalButtonCss: function (SnTDetail) {
    var base = 'btn btn-xs btn-block';
    if (SnTDetail.IsIncidentalCurrentEvent()) {
      if (SnTDetail.IncidentalAmount() > 0) {
        base += ' btn-success'
      } else {
        base += ' btn-default'
      }
    } else {
      if (SnTDetail.IncidentalAmount() > 0) {
        base += ' btn-warning'
      } else {
        base += ' btn-default'
      }
    }
    return base
  },
  canEdit: function (SnTDetail, FieldName) {
    if (SnTDetail.CanEditRecord()) {
      switch (FieldName) {
        case 'ChangedReason':
          return (SnTDetail.IsValid() || SnTDetail.ChangedReason().trim().length == 0)
          break;
        case 'BreakfastAmount':
        case 'HasBreakfast':
          return (SnTDetail.IsBreakfastCurrentEvent() || SnTDetail.BreakfastAmount() == 0)
          break;
        case 'LunchAmount':
        case 'HasLunch':
          return (SnTDetail.IsLunchCurrentEvent() || SnTDetail.LunchAmount() == 0)
          break;
        case 'DinnerAmount':
        case 'HasDinner':
          return (SnTDetail.IsDinnerCurrentEvent() || SnTDetail.DinnerAmount() == 0)
          break;
        case 'IncidentalAmount':
        case 'HasIncidental':
          return (SnTDetail.IsIncidentalCurrentEvent() || SnTDetail.IncidentalAmount() == 0)
          break;
        default:
          return true;
          break;
      }
    } else {
      return false
    }
  },
  updateTotalAmount: function (SnTDetail) {
    SnTDetail.TotalAmount(SnTDetail.BreakfastAmount() + SnTDetail.LunchAmount() + SnTDetail.DinnerAmount() + SnTDetail.IncidentalAmount())
    this.updateParentAmount(SnTDetail)
  },
  updateParentAmount: function (SnTDetail) {
    var parent = SnTDetail.GetParent()
    var totalSnT = 0, totalBreakfast = 0, totalLunch = 0, totalDinner = 0, totalIncidental = 0;
    parent.SnTDetailList().Iterate(function (sntD, indx) {
      totalBreakfast += (sntD.IsBreakfastCurrentEvent() ? sntD.BreakfastAmount() : 0)
      totalLunch += (sntD.IsLunchCurrentEvent() ? sntD.LunchAmount() : 0)
      totalDinner += (sntD.IsDinnerCurrentEvent() ? sntD.DinnerAmount() : 0)
      totalIncidental += (sntD.IsIncidentalCurrentEvent() ? sntD.IncidentalAmount() : 0)
      //totalSnT += (totalBreakfast + totalLunch + totalDinner + totalIncidental)
    })
    parent.TotalBreakfastAmount(totalBreakfast)
    parent.TotalLunchAmount(totalLunch)
    parent.TotalDinnerAmount(totalDinner)
    parent.TotalIncidentalAmount(totalIncidental)
    parent.TotalSnTAmount(parent.TotalBreakfastAmount() + parent.TotalLunchAmount() + parent.TotalDinnerAmount() + parent.TotalIncidentalAmount())
  },

  //set
  GroupSnTIDSet: function (SnTDetail) {
    if (SnTDetail.GroupSnTID()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", SnTDetail.GroupSnTID())
      SnTDetail.CurrencyID(setting.CurrencyID)
      if (SnTDetail.IsBreakfastCurrentEvent()) { SnTDetail.BreakfastAmount(setting.BreakfastAmount) }
      if (SnTDetail.IsLunchCurrentEvent()) { SnTDetail.LunchAmount(setting.LunchAmount) }
      if (SnTDetail.IsDinnerCurrentEvent()) { SnTDetail.DinnerAmount(setting.DinnerAmount) }
      if (SnTDetail.IsIncidentalCurrentEvent()) { SnTDetail.IncidentalAmount(setting.IncidentalAmount) }
    } else {
      if (SnTDetail.IsBreakfastCurrentEvent()) { SnTDetail.BreakfastAmount(0) }
      if (SnTDetail.IsLunchCurrentEvent()) { SnTDetail.LunchAmount(0) }
      if (SnTDetail.IsDinnerCurrentEvent()) { SnTDetail.DinnerAmount(0) }
      if (SnTDetail.IsIncidentalCurrentEvent()) { SnTDetail.IncidentalAmount(0) }
    }
    SnTDetailBO.updateTotalAmount(SnTDetail)
  },
  HasBreakfastSet: function (SnTDetail) {
    var tReq = SnTDetail.GetParent().GetParent()
    SnTDetail.IsBreakfastCurrentEvent(SnTDetail.HasBreakfast())
    if (SnTDetail.HasBreakfast()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", SnTDetail.GroupSnTID())
      SnTDetail.BreakfastAmount(setting.BreakfastAmount)
      SnTDetail.BreakfastProductionID(tReq.ProductionID())
      SnTDetail.BreakfastAdHocBookingID(tReq.AdHocBookingID())
      SnTDetail.BreakfastSystemID(tReq.SystemID())
      SnTDetail.BreakfastProductionAreaID(tReq.ProductionAreaID())
    } else {
      SnTDetail.BreakfastAmount(0)
      SnTDetail.BreakfastProductionID(null)
      SnTDetail.BreakfastAdHocBookingID(null)
      SnTDetail.BreakfastSystemID(null)
      SnTDetail.BreakfastProductionAreaID(null)
    }
    SnTDetailBO.updateTotalAmount(SnTDetail)
  },
  HasLunchSet: function (SnTDetail) {
    var tReq = SnTDetail.GetParent().GetParent()
    SnTDetail.IsLunchCurrentEvent(SnTDetail.HasLunch())
    if (SnTDetail.HasLunch()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", SnTDetail.GroupSnTID())
      SnTDetail.LunchAmount(setting.LunchAmount)
      SnTDetail.LunchProductionID(tReq.ProductionID())
      SnTDetail.LunchAdHocBookingID(tReq.AdHocBookingID())
      SnTDetail.LunchSystemID(tReq.SystemID())
      SnTDetail.LunchProductionAreaID(tReq.ProductionAreaID())
    } else {
      SnTDetail.LunchAmount(0)
      SnTDetail.LunchProductionID(null)
      SnTDetail.LunchAdHocBookingID(null)
      SnTDetail.LunchSystemID(null)
      SnTDetail.LunchProductionAreaID(null)
    }
    SnTDetailBO.updateTotalAmount(SnTDetail)
  },
  HasDinnerSet: function (SnTDetail) {
    var tReq = SnTDetail.GetParent().GetParent()
    SnTDetail.IsDinnerCurrentEvent(SnTDetail.HasDinner())
    if (SnTDetail.HasDinner()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", SnTDetail.GroupSnTID())
      SnTDetail.DinnerAmount(setting.DinnerAmount)
      SnTDetail.DinnerProductionID(tReq.ProductionID())
      SnTDetail.DinnerAdHocBookingID(tReq.AdHocBookingID())
      SnTDetail.DinnerSystemID(tReq.SystemID())
      SnTDetail.DinnerProductionAreaID(tReq.ProductionAreaID())
    } else {
      SnTDetail.DinnerAmount(0)
      SnTDetail.DinnerProductionID(null)
      SnTDetail.DinnerAdHocBookingID(null)
      SnTDetail.DinnerSystemID(null)
      SnTDetail.DinnerProductionAreaID(null)
    }
    SnTDetailBO.updateTotalAmount(SnTDetail)
  },
  HasIncidentalSet: function (SnTDetail) {
    var tReq = SnTDetail.GetParent().GetParent()
    SnTDetail.IsIncidentalCurrentEvent(SnTDetail.HasIncidental())
    if (SnTDetail.HasIncidental()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", SnTDetail.GroupSnTID())
      SnTDetail.IncidentalAmount(setting.IncidentalAmount)
      SnTDetail.IncidentalProductionID(tReq.ProductionID())
      SnTDetail.IncidentalAdHocBookingID(tReq.AdHocBookingID())
      SnTDetail.IncidentalSystemID(tReq.SystemID())
      SnTDetail.IncidentalProductionAreaID(tReq.ProductionAreaID())
    } else {
      SnTDetail.IncidentalAmount(0)
      SnTDetail.IncidentalProductionID(null)
      SnTDetail.IncidentalAdHocBookingID(null)
      SnTDetail.IncidentalSystemID(null)
      SnTDetail.IncidentalProductionAreaID(null)
    }
    SnTDetailBO.updateTotalAmount(SnTDetail)
  },
  GroupSnTIDCalculatedSet: function (SnTDetail) { },


  PolicyDetails: PolicyDetails = {
    Other_Countries_Domestic: 12,
    Currency_USDollar: 16,
    All_other_countries_International: 5,
    South_African_Booking: 1
  },
  TotalAmountValid: function (Value, Rule, CtlError) {
    var ahsd = CtlError.Object;
    if (ahsd.GroupSnTID()) {
      var ROGroupSnT;
      ROGroupSnT = ClientData.ROGroupSnTList.Find("GroupSnTID", ahsd.GroupSnTID());
      if (ROGroupSnT.InternationalInd == false) {
        var Total = ahsd.BreakfastAmount() + ahsd.LunchAmount() + ahsd.DinnerAmount() + ahsd.IncidentalAmount();
        if (Total > ROGroupSnT.TotalAmount && ahsd.ChangedReason().trim() == "") {
          CtlError.AddError("Total S&T is greater than the allowed maximum, please specify a reason why");
        }
      }
    }
  },
  UpdateFields: function (self) {
    if (self.GroupSnTID()) {
      var BookingCountryID = ViewModel.TravelRequisition().CountryID(); // Country where the booking is to take place
      var ROGroupSnT;
      ROGroupSnT = ClientData.ROGroupSnTList.Find("GroupSnTID", self.GroupSnTID());
      if (self.GroupSnTID() == 13 || self.GroupSnTID() == 17) {
        var d1 = new Date(self.SnTDay());
        var d2 = new Date(ROGroupSnT.EffectiveDate);
        if (d1 >= d2) {
          self.GroupSnTID(17);
        } else {
          self.GroupSnTID(13);
        }
      } else {
        self.GroupSnTID(ROGroupSnT.GroupSnTID);
      }
      self.InternationalInd(ROGroupSnT.InternationalInd);
      self.CurrencyID(ROGroupSnT.CurrencyID);
      if (ROGroupSnT.DestinationCountryID == 0) {
        self.DestinationCountryID(BookingCountryID);
      }
    }
  },

  setupSnTDetails: function (self) {
    if (self.GroupSnTID()) {
      var obj = ClientData.ROGroupSnTList.Find("GroupSnTID", self.GroupSnTID());
      if (obj) {
        self.CountryID(obj.DestinationCountryID);
        self.CurrencyID(obj.CurrencyID);
      }
    }
    else {
      self.CountryID(null);
      self.CurrencyID(null);
    }
    var child = FindProductionHumanResourceSnTDetail(self);
    if (child != undefined) {
      child.GroupSnTID(self.GroupSnTID());
    }
  },
  UpdateChangeReason: function (self) {
    var child = FindProductionHumanResourceSnTDetail(self);
    child.ChangedReason(self.ChangedReason());
  },
  UpdateComments: function (self) {
    var child = FindProductionHumanResourceSnTDetail(self);
    child.Comments(self.Comments());
  },
  SnTDetailBOToString: function (self) {
    var HumanResource = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID());
    var HR = (HumanResource == undefined) ? '' : ' - Human Resource: ' + HumanResource;
    var SnTDay = (self.SnTDay() == undefined) ? '' : ' - S&T Day: ' + self.SnTDay();
    var Breakfast = (self.BreakfastProductionID() == undefined) ? 'No' : 'Yes';
    var Lunch = (self.LunchProductionID() == undefined) ? 'No' : 'Yes';
    var Dinner = (self.DinnerProductionID() == undefined) ? 'No' : 'Yes';
    var Incidental = (self.IncidentalProductionID() == undefined) ? 'No' : 'Yes';
    return 'S&T Detail' + HR + SnTDay + ' - ' +
      'Breakfast: ' + Breakfast + ' - ' +
      'Lunch: ' + Lunch + ' - ' +
      'Dinner: ' + Dinner + ' - ' +
      'Incidental: ' + Incidental;
  }
}

SnTTemplateBO = {
  GroupSnTIDSet: function (self) {
    if (self.GroupSnTID() != null) {
      var ROGroupSnT = ClientData.ROGroupSnTList.Find("GroupSnTID", self.GroupSnTID())
      if (ROGroupSnT) {
        self.CurrencyID(ROGroupSnT.CurrencyID)
      }
    }
  },
  TotalAmountValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.GroupSnTID() != null) {
      var ROGroupSnT = ClientData.ROGroupSnTList.Find("GroupSnTID", obj.GroupSnTID());
      //if (ROGroupSnT.InternationalInd == false) {
      var Total = 0; //PHRSnTDetail.BreakfastAmount() + PHRSnTDetail.LunchAmount() + PHRSnTDetail.DinnerAmount() + PHRSnTDetail.IncidentalAmount();
      if (obj.BreakfastInd()) {
        Total += ROGroupSnT.BreakfastAmount;
      }
      if (obj.LunchInd()) {
        Total += ROGroupSnT.LunchAmount;
      }
      if (obj.DinnerInd()) {
        Total += ROGroupSnT.DinnerAmount;
      }
      if (obj.IncidentalInd()) {
        Total += ROGroupSnT.IncidentalAmount;
      }

      if (Total > ROGroupSnT.TotalAmount && obj.ChangedReason().trim() == "") {
        CtlError.AddError("Total S&T is greater than the standard allowed amount, please specify a reason why");
      }
      //}
    }
  },
  SnTTemplateBOToString: function (self) {
    var Breakfast = (self.BreakfastInd() == undefined) ? 'No' : 'Yes';
    var Lunch = (self.LunchInd() == undefined) ? 'No' : 'Yes';
    var Dinner = (self.DinnerInd() == undefined) ? 'No' : 'Yes';
    var Incidental = (self.IncidentalInd() == undefined) ? 'No' : 'Yes';
    return 'Breakfast: ' + Breakfast + ' - ' +
      'Lunch: ' + Lunch + ' - ' +
      'Dinner: ' + Dinner + ' - ' +
      'Incidental: ' + Incidental;
  },
  applyTemplate: function (template, options) {
    ViewModel.CallServerMethod("ApplySnTTemplate",
      { SnTTemplate: template.Serialise() },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Applied Successfully", "", 250)
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Apply Failed", response.ErrorText, 1000)
        }
      })
  },
  breakfastAmount: function (template) {
    if (template.GroupSnTID()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", template.GroupSnTID())
      if (setting) { return 'Breakfast: ' + setting.BreakfastAmount.toString() }
    }
    return "0"
  },
  lunchAmount: function (template) {
    if (template.GroupSnTID()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", template.GroupSnTID())
      if (setting) { return 'Lunch: ' + setting.LunchAmount.toString() }
    }
    return "0"
  },
  dinnerAmount: function (template) {
    if (template.GroupSnTID()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", template.GroupSnTID())
      if (setting) { return 'Dinner: ' + setting.DinnerAmount.toString() }
    }
    return "0"
  },
  incidentalAmount: function (template) {
    if (template.GroupSnTID()) {
      var setting = ClientData.ROGroupSnTList.Find("GroupSnTID", template.GroupSnTID())
      if (setting) { return 'Incidental: ' + setting.IncidentalAmount.toString() }
    }
    return "0"
  },
  selectAll: function (template) {
    template.SnTTemplateTravellerList().Iterate(function (trv) {
      trv.IsSelected(true)
    })
  },
  deselectAll: function (template) {
    template.SnTTemplateTravellerList().Iterate(function (trv) {
      trv.IsSelected(false)
    })
  }
}

SnTTemplateTravellerBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.SnT.SnTTemplateTravellerList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch S&T Template Travellers Failed", response.ErrorText, 1000)
        }
      })
  }
}

ROTravelRequisitionListCriteriaBO = {
  TitleSet: function (self) { },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  CreatedBySet: function (self) { },
  RefNoSet: function (self) { },
  AdHocBookingTypeIDSet: function (self) { },
  SystemIDSet: function (self) { },
  ProductionAreaIDSet: function (self) { },
  HumanResourceIDSet: function (self) { }
}

ROTravelRequisitionListPagedCriteriaBO = {
  TitleSet(self) { },
  StartDateSet(self) { },
  EndDateSet(self) { },
  CreatedBySet(self) { },
  RefNoSet(self) { },
  AdHocBookingTypeIDSet(self) { },
  SystemIDSet(self) { },
  ProductionAreaIDSet(self) { },
  HumanResourceIDSet(self) { },
  HumanResourceSet(self) { },
  refresh(pagingManager) { pagingManager.Refresh() },
  clearCriteria(criteria) { },
  setHumanResourceIDCriteriaBeforeRefresh(args) {
    args.Data.SystemIDs = [args.Object.SystemID()];
    args.Data.ProductionAreaIDs = [args.Object.ProductionAreaID()];
  },
  triggerHumanResourceIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterHumanResourceIDRefreshAjax(args) {

  },
  onHumanResourceIDSelected(selectedItem, businessObject) {

  }
}

ROTravelAvailableHumanResourceBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.CrewMembers.ReadOnly.ROTravelAvailableHumanResourceList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Travel Availability Failed", response.ErrorText, 1000)
        }
      })
  },
  crewMemberCancelledStateColor: function (availability) {
    var defaultStyle = "";
    if (availability.Disciplines().trim().length == 0) { defaultStyle = "danger" }
    return defaultStyle;
  }
}

ROHumanResourceTravelReqBO = {
  canEdit: function (fieldName, inst) {
    switch (fieldName) {
      case "SelectButton":
        return (!inst.TravelRequisitionTravellerID())
        break;
      default:
        return false;
        break;
    }
  }
}

ROTravellerSummaryBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.ReadOnly.ROTravellerSummaryList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Summary Failed", response.ErrorText, 1000)
        }
      })
  }
}

BulkRentalCarBO = {
  toString: function (self) {
    var deptTime = ''
    if (self.PickupDateTime()) {
      deptTime = ' - ' + new Date(self.PickupDateTime()).format('dd MMM yy HH:mm')
    }
    return 'Car: ' + self.BranchFrom() + deptTime
  },

  //dropdowns
  triggerAgentBranchIDFromAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAgentBranchIDFromCriteriaBeforeRefresh: function (args) {
    args.Data.RentalCarAgentID = args.Object.RentalCarAgentID()
  },
  afterAgentBranchIDFromRefreshAjax: function (args) {
  },
  onAgentBranchIDFromSelected: function (item, businessObject) {
  },

  triggerAgentBranchIDToAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAgentBranchIDToCriteriaBeforeRefresh: function (args) {
    args.Data.RentalCarAgentID = args.Object.RentalCarAgentID()
  },
  afterAgentBranchIDToRefreshAjax: function (args) {
  },
  onAgentBranchIDToSelected: function (item, businessObject) {

  },

  triggerRentalCarAgentIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setRentalCarAgentIDCriteriaBeforeRefresh: function (args) {
    args.Data.IncludeOld = false
  },
  afterRentalCarAgentIDRefreshAjax: function (args) {
  },
  onRentalCarAgentIDSelected: function (item, businessObject) {

  },

  //validation
  DriverValid: function (Value, Rule, CtlError) {
    var passenger = CtlError.Object;
    var checkOnce = false;
    //var bulklist = self.RentalCarHumanResourceList();
    var AtLeastOneOnCar = false;
    var DriverCount = 0;
    var DriverCancelled = false;
    //for (var n = 0; n < bulklist.length; n++) {
    //  if (bulklist[n].OnRentalCarInd()) {
    //    if (!bulklist[n].IsCancelled()) {
    //      AtLeastOneOnCar = true;
    //      return;
    //    }
    //  }
    //  if (bulklist[n].DriverInd()) {
    //    DriverCount++;
    //    if (bulklist[n].IsCancelled()) {
    //      DriverCancelled = true;
    //      DriverCount--;
    //      return;
    //    }
    //  }
    //}
    //if (AtLeastOneOnCar) {
    //  if (!checkOnce) {
    //    if (DriverCount == 0) {
    //      checkOnce = true;
    //      //CtlError.AddError("Driver Required");
    //    }
    //    else if (DriverCount > 1) {
    //      checkOnce = true;
    //      //CtlError.AddError("Only One Driver Required");
    //    }
    //  }
    //}
  },
  RentalCarAgentValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.RentalCarAgentID()) {
      var RORentalCarAgent = ClientData.RORentalCarAgentList.Find('RentalCarAgentID', RentalCar.RentalCarAgentID())
      //if (RORentalCarAgent.OldInd) {
      //  if (RentalCar.PickupDateTime()) {
      //    if (OBMisc.Dates.IsAfter(RentalCar.PickupDateTime(), RORentalCarAgent.OldDate)) {
      //      CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgent.OldReason);
      //    }
      //  } else {
      //    CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgent.OldReason);
      //  }
      //}
    } else {
      CtlError.AddError("Rental Car Agent is required");
    }
  },
  AgentBranchIDFromValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.AgentBranchIDFrom()) {
      //var RORentalCarAgentBranch = ClientData.RORentalCarAgentBranchList.Find('RentalCarAgentBranchID', RentalCar.AgentBranchIDFrom());
      //if (RORentalCarAgentBranch.OldInd) {
      //  if (RentalCar.PickupDateTime()) {
      //    if (OBMisc.Dates.IsAfter(RentalCar.PickupDateTime(), RORentalCarAgentBranch.OldDate)) {
      //      CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //    }
      //  } else {
      //    CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //  }
      //}
    } else {
      CtlError.AddError("Branch From is required");
    }
  },
  AgentBranchIDToValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    if (RentalCar.AgentBranchIDTo()) {
      //var RORentalCarAgentBranch = ClientData.RORentalCarAgentBranchList.Find('RentalCarAgentBranchID', RentalCar.AgentBranchIDTo());
      //if (RORentalCarAgentBranch.OldInd) {
      //  if (RentalCar.PickupDateTime()) {
      //    if (OBMisc.Dates.IsAfter(RentalCar.PickupDateTime(), RORentalCarAgentBranch.OldDate)) {
      //      CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //    }
      //  } else {
      //    CtlError.AddError("This branch can no longer be selected: " + RORentalCarAgentBranch.OldReason);
      //  }
      //}
    } else {
      CtlError.AddError("Branch To is required");
    }
  },
  DriverRequired: function (Value, Rule, CtlError) {
  },
  TravelDatesValid: function (Value, Rule, CtlError) {
    var RentalCar = CtlError.Object;
    var PickupDateTime = new Date(RentalCar.PickupDateTime());
    var DropoffDateTime = new Date(RentalCar.DropoffDateTime());
    if (DropoffDateTime <= PickupDateTime) {
      CtlError.AddError("Pickup Date must be before Dropoff Date");
    }
  },
  HRClashCountValid: function (Value, Rule, CtlError) {
    var rc = CtlError.Object;
    if (rc.HRClashCount() > 0) {
      CtlError.AddError("There are passenger clashes on this rental car")
    }
  },

  //set
  AgentBranchIDSet: function (self) {
    self.AgentBranchIDFrom(null)
    self.AgentBranchIDTo(null)
  },
  CarTypeIDSet: function (self) {
  },
  PickupDateTimeSet: function (self) {
    RentalCarBO.updateClashes(self)
  },
  DropoffDateTimeSet: function (self) {
    RentalCarBO.updateClashes(self)
  },

  //cans
  canEdit: function (RentalCar, FieldName) {
    switch (FieldName) {
      case 'SaveButton':
        return (RentalCar.IsValid() && RentalCar.IsDirty() && !RentalCar.IsProcessing())
        break;
      default:
        return true
        break;
    }
  },
  canView: function (RentalCar, FieldName) {
    switch (RentalCar, FieldName) {
      default:
        return true
        break;
    }
  },

  //other
  InitDate: function (obj) {
    var tr = ViewModel.TravelRequisition()
    var d = new Date(tr.StartDate())
    var first_day = new Date(d.getFullYear(), d.getMonth(), 1);
    return first_day;
  },
  filterROCrewTypes: function (List, rentalCar) {
    var Allowed = [];
    var travelReq = ViewModel.TravelRequisition()
    ClientData.ROCrewTypeList.Iterate(function (crewType, Index) {
      if (travelReq.SystemID() == crewType.SystemID) {
        Allowed.push({ CrewTypeID: crewType.CrewTypeID, TravelScreenDisplayName: crewType.TravelScreenDisplayName });
      }
    });
    return Allowed;
  },
  determineDriver: function (self) {
    self.Driver("")
    self.RentalCarHumanResourceList().Iterate(function (p, pIndx) {
      if (p.DriverInd()) {
        self.Driver(p.HumanResourceName())
      }
    })
  },
  afterItemAdded: function (newItem) {
    newItem.TravelRequisitionID(newItem.GetParent().TravelRequisitionID())
  },
  updateClashes: function (rcar) {
    rcar.IsProcessing(true)
    ViewModel.CallServerMethod("UpdateRentalCarClashes", {
      RentalCar: rcar.Serialise()
    },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, rcar)
          rcar.IsProcessing(false)
        }
        else {
          OBMisc.Notifications.GritterError("Failed to update clashes", response.ErrorText, 1000)
          rcar.IsProcessing(false)
        }
      })
  },

  //data access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Travel.RentalCars.RentalCarList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch RentalCars Failed", response.ErrorText, 1000)
        }
      })
  },
  getPassengers: function (rentalCar, options) {
    RentalCarPassengerBO.get({
      criteria: { TravelRequisitionID: rentalCar.TravelRequisitionID(), RentalCarID: rentalCar.RentalCarID() },
      onSuccess: function (response) {
        rentalCar.RentalCarHumanResourceList.Set(response.Data)
        if (options.onSuccess) { options.onSuccess(response) }
      }
    })
  },
  saveItem: function (RentalCar, options) {
    var me = this
    RentalCar.IsProcessing(true)
    RentalCar.TravelRequisitionID(RentalCar.GetParent().TravelRequisitionID())
    ViewModel.CallServerMethod("SaveRentalCar",
      { RentalCar: RentalCar.Serialise() },
      function (response) {
        if (response.Success) {
          RentalCar.IsProcessing(false)
          KOFormatter.Deserialise(response.Data, RentalCar)
          if (options.onSuccess) { options.onSuccess.call(me, response) } else { OBMisc.Notifications.GritterSuccess("Rental Car Saved", "", 250) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  save: function (RentalCar, options) {
    var me = this
    ViewModel.CallServerMethod("SaveRentalCar",
      { RentalCar: RentalCar.Serialise() },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, RentalCar)
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", "", 1000)
        }
      })
  },
  deleteRentalCars: function (serialisedRentalCarList, options) {
    var me = this
    ViewModel.CallServerMethod("DeleteRentalCars",
      { RentalCarList: serialisedRentalCarList },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess.call(me, response) }
        }
        else {
          OBMisc.Notifications.GritterError("Delete Failed", "", 1000)
        }
      })
  },
  generateCars: function (travelReq, bulkRentalCar) {

    var quant = bulkRentalCar.Quantity()
    while (quant > 0) {
      var newRC = travelReq.RentalCarList.AddNew()
      newRC.TravelRequisitionID(bulkRentalCar.TravelRequisitionID())
      newRC.RentalCarAgentID(bulkRentalCar.RentalCarAgentID())
      newRC.CrewTypeID(bulkRentalCar.CrewTypeID())
      newRC.CarTypeID(bulkRentalCar.CarTypeID())
      //newRC.RentalCarRefNo(bulkRentalCar.RentalCarRefNo())
      newRC.AgentBranchIDFrom(bulkRentalCar.AgentBranchIDFrom())
      newRC.BranchFrom(bulkRentalCar.BranchFrom())
      newRC.PickupDateTime(bulkRentalCar.PickupDateTime())
      newRC.AgentBranchIDTo(bulkRentalCar.AgentBranchIDTo())
      newRC.BranchTo(bulkRentalCar.BranchTo())
      newRC.DropoffDateTime(bulkRentalCar.DropoffDateTime())
      newRC.DriverRentalCarHumanResourceID(null)
      quant -= 1
    }

    $("#BulkRentalCarModal").modal('hide')

  }
}