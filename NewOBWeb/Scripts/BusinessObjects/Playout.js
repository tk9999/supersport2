﻿LiveEventHRCardBO = {
  //data access
  getList: function (options) {
    Singular.GetDataStateless("OBLib.Playout.LiveEventHRCardList, OBLib",
                              options.criteria,
                              function (response) {
                                if (response.Success) {
                                  if (options.onSuccess) { options.onSuccess(response) }
                                }
                                else {
                                  OBMisc.Notifications.GritterError("Fetch Failed", response.ErrorText, 1000)
                                  if (options.onFail) { options.onFail(response) }
                                }
                              })
  }
}

LiveEventHRCardCriteriaBO = {
  StartDateSet: function () { },
  EndDateSet: function () { },

  RoomIDSet: function (area) {
    //RoomScheduleAreaBO.updateAll(area)
  },
  triggerRoomAutoPopulate: function (args) {
    args.AutoPopulate = true
    //  args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    //args.Data.Room = args.Object.Room()
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.IgnoreAreas = true
  },
  afterRoomRefreshAjax: function (args) {
    // args.Object.IsProcessing(false)
  },
  onRoomSelected: function (selectedItem, businessObject) {

  }
}