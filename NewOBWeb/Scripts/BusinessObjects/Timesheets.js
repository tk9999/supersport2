﻿CrewTimesheetBO = {
  CanViewField: function (FieldName, CrewTimesheet, Element) {
    switch (FieldName) {
      case 'CrewStartDateTime':
      case 'CrewEndDateTime':
        return (CrewTimesheet.TimesheetCategoryID() != 11);
        break;
      case 'HoursAboveOTLimit':
        return (CrewTimesheet.HoursAboveOTLimit() > 0);
        break;
      case 'ShortfallForMonth':
        return (CrewTimesheet.ShortfallForMonth() > 0);
        break;
      case 'HoursForDay':
      case 'PublicHolidayHours':
      case 'SnTForDay':
        return (CrewTimesheet.SnTForDay() > 0);
        break;
      case 'QueryButton':
        if (CrewTimesheet.TimesheetCategoryID() != 11) {
          if (CrewTimesheet.CrewChangeDetails().trim().length == 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
        break;
      case 'QueriedButton':
        if (CrewTimesheet.TimesheetCategoryID() != 11) {
          if (CrewTimesheet.CrewChangeDetails().trim().length > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
        break;
    }
    return true;
  },
  GetHoursForMonthCss: function (CrewTimesheet) {
    if (CrewTimesheet.HoursForMonth() > CrewTimesheet.GetParent().TMHoursBeforeOvertime()) {
      return 'into-overtime';
    } else {
      return '';
    }
  },
  GetShortfallCss: function (CrewTimesheet) {
    if (CrewTimesheet.ShortfallHours() > 0) {
      return 'has-shortfall';
    } else {
      return '';
    }
  },
  CrewChangeDetailsValid: function (Value, Rule, CtlError) {
    var CrewTimesheet = CtlError.Object
    var s = moment(CrewTimesheet.CrewStartDateTime())
    var e = moment(CrewTimesheet.CrewEndDateTime())
    var s1 = moment(CrewTimesheet.CalculatedStartDateTime())
    var e1 = moment(CrewTimesheet.CalculatedEndDateTime())
    if ((!s.isSame(s1) || !e.isSame(e1)) && CrewTimesheet.CrewChangeDetails().length == 0) {
      CtlError.AddError("Query Reason is required")
    }
  },
  ManagerChangeDetailsValid: function (Value, Rule, CtlError) {
    //var CrewTimesheet = CtlError.Object
    //var s = moment(CrewTimesheet.CrewStartDateTime())
    //var e = moment(CrewTimesheet.CrewEndDateTime())
    //var s1 = moment(CrewTimesheet.CalculatedStartDateTime())
    //var e1 = moment(CrewTimesheet.CalculatedEndDateTime())
    //if (!s.isSame(s1) || !e.isSame(e1)) {
    //  CtlError.AddError("Query Reason is required")
    //}
  }
}

HumanResourceSwapBO = {
  ROHumanResourceRowClicked: function (HumanResource) {
    if (HumanResource) {
      ViewModel.HumanResourceSwap().SwapWithHumanResource(HumanResource.Firstname() + ' ' + HumanResource.Surname());
      ViewModel.HumanResourceSwap().SwapWithHumanResourceID(HumanResource.HumanResourceID())
    }
  }
}

AdhocCrewTimesheetBO = {
  TimesValid: function (Value, Rule, CtlError) {
    var ahct = CtlError.Object;
    if (OBMisc.Dates.IsAfter(ahct.StartDateTime(), ahct.EndDateTime())) {
      CtlError.AddError("Start Time must be before End Time");
    }
  },
  StartDateTimeSet: function (self) {
    self.TimesheetDate(self.StartDateTime());
    self.AdhocCrewTimesheetHumanResourceList().Iterate(function (item, ind) {
      item.CrewStartDateTime(self.StartDateTime());
    });
    Singular.Validation.CheckRules(self);
  },
  EndDateTimeSet: function (self) {
    self.AdhocCrewTimesheetHumanResourceList().Iterate(function (item, ind) {
      item.CrewEndDateTime(self.EndDateTime());
    });
    Singular.Validation.CheckRules(self);
  }
}

AdhocCrewTimesheetHumanResourceBO = {
  TimesValid: function (Value, Rule, CtlError) {
    var ahct = CtlError.Object;
    if (OBMisc.Dates.IsAfter(ahct.CrewStartDateTime(), ahct.CrewEndDateTime())) {
      CtlError.AddError("Start Time must be before End Time");
    }
  }
}

TimesheetAdjustmentBO = {
  AuthorisedSet: function (self) {
    if (self.Authorised()) {
      self.AuthorisedByUserID(ViewModel.CurrentUserID());
      self.AuthorisedDateTime(new Date())
    } else {
      self.AuthorisedByUserID(null);
      self.AuthorisedDateTime(null);
    }
  }
}

OBCityTimesheetBO = {
  setUserCriteriaBeforeRefresh(args) {
    args.Data.UserID = args.Object.UserID()
  },
  triggerAutoPopulate(args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  afterUserRefreshAjax() {
    //args.Object.IsProcessing(false)
  },
  onUserSelected(selectedItem, businessObject) {
    OBCityTimesheetBO.refreshTimesheet(businessObject);
  },
  CriteriaHumanResourceIDSet(self) {
    console.log(self);
  },

  setTimesheetMonthCriteriaBeforeRefresh(args) {
    args.Data.PageNo = 1;
    args.Data.PageSize = 1000;
    args.Data.SortColumn = "Year";
    args.Data.SortAsc = false;
  },
  triggerTimesheetMonthAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterTimesheetMonthRefreshAjax() {
  },
  onCriteriaTimesheetMonthSelected(selectedItem, businessObject) {
    OBCityTimesheetBO.refreshTimesheet(businessObject);
  },

  async refreshTimesheet(self) {
    if (self.HumanResourceID() && self.TimesheetMonthID()) {
      try {
        let result = await Singular.GetDataStatelessPromise("OBLib.Timesheets.OBCity.OBCityTimesheetList, OBLib", {
          HumanResourceID: self.HumanResourceID(),
          TimesheetMonthID: self.TimesheetMonthID(),
          SystemID: self.SystemID(),
          ProductionAreaID: self.ProductionAreaID()
        });
        ViewModel.OBCityTimesheet.Set(result[0]);
        OBMisc.Notifications.GritterInfo("Timesheet changed", "", 250);
      }
      catch (err) {
        OBMisc.Notifications.GritterError("Error fetching timesheet", err, 1000);
      }
    };
  },
  async printTimesheet(self) {
    try {
      let result = await ViewModel.CallServerMethodPromise("PrintOBCityTimesheet", {
        Timesheet: self.Serialise()
      });
      Singular.DownloadFile(null, result);
    } catch (err) {
      OBMisc.Notifications.GritterError('Error Downloading', err, null)
    }
  },
  async recalculateTimesheet(self) {
    try {
      let result = await ViewModel.CallServerMethodPromise("RecalculateOBCityTimesheet", {
        Timesheet: self.Serialise()
      });
      KOFormatter.Deserialise(result, self);
    } catch (err) {
      OBMisc.Notifications.GritterError('Error Calculating', err, null)
    }
  }
}