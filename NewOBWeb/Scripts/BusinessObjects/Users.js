﻿UserProfileBO = {
  QueryDetailsValid: function (Value, Rule, CtlError) {
    if (ViewModel.QueryModalOpen() && ViewModel.QueryDetails() == "") {
      CtlError.AddError("Query Details are required");
    }
  },
  AvatarPhotoClick: function (ROUserProfile) {
    Singular.ShowFileDialog(function (e) {
      Singular.UploadFile(e.target.files[0], ViewModel.TempImage(), 'png,jpg,jpeg', function (Result) {
        if (Result.Success) {
          console.log(Result);
          ROUserProfile.UserProfileLargeImageID(Result.DocumentID);
        } else {
          alert(Result.Response);
        }
      }, 'Image=true&ImageTypeID=1&Stateless=true&ForUserID=' + ViewModel.CurrentUserID().toString());
    });
  },
  AvatarPhotoSrc: function (ROUserProfile) {
    var ImageID = ROUserProfile.UserProfileLargeImageID();
    if (ImageID) {
      var basicPath = window.SiteInfo.SitePath + '/Images/Profile/' + ViewModel.CurrentUserID().toString()
      var smallPath = basicPath + '_2.jpg' + '?I=' + ImageID.toString()
      var mediumPath = basicPath + '_4.jpg' + '?I=' + ImageID.toString()
      var largePath = basicPath + '_1.jpg' + '?I=' + ImageID.toString()
      $("#userImageSmall").attr('src', smallPath)
      $("#userImageMedium").attr('src', mediumPath)
      return largePath
    }
    else {
      return '../Images/avatar-placeholder.png';
    }
  }
};

UserProfileShiftBO = {
  AcknowledgeSet: function (self) {
    if (self.StaffAcknowledgeInd()) {
      self.StaffAcknowledgeBy(ViewModel.CurrentUserID())
      self.StaffAcknowledgeDateTime(new Date())
    }
    else {
      self.StaffAcknowledgeBy(null)
      self.StaffAcknowledgeDateTime(null)
    }
  },
  StaffDisputeReasonValid: function (Value, Rule, CtlError) {
    var shift = CtlError.Object
    if (shift.IsDisputing() && shift.StaffDisputeReason().trim().length == 0) {
      CtlError.AddError("Dispute Reason is required")
    }
  }
};

ROUserListCriteriaBO = {
  FilterNameSet: function (self) {
  }
};

UserBO = {
  setHumanResourceCriteriaBeforeRefresh: function (args) {
    //args.PageNo = 1
    //args.PageSize = 50
    //args.SortAsc = true
    //args.SortColumn = "HRName"
  },
  triggerHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  afterHumanResourceRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onHumanResourceSelected: function (selectedItem, businessObject) {
    businessObject.HumanResourceID(selectedItem.HumanResourceID)
  },
  sendLoginDetails: function (user) {
    ViewModel.CallServerMethod("SendLoginDetails",
                               {
                                 User: user.Serialise()
                               },
                               function (response) {
                                 if (response.Success) {
                                   OBMisc.Notifications.GritterSuccess("Login Details Sent", "", 1000)
                                 } else {
                                   OBMisc.Notifications.GritterSuccess("Login Details Failed to Send", response.ErrorText, 3000)
                                 }
                               })
  },
  resetPassword: function (user) {
    ViewModel.CallServerMethod("ResetPassword",
                               {
                                 User: user.Serialise()
                               },
                               function (response) {
                                 if (response.Success) {
                                   OBMisc.Notifications.GritterSuccess("Password Reset", "", 1000)
                                 } else {
                                   OBMisc.Notifications.GritterSuccess("Password Failed to Reset", response.ErrorText, 3000)
                                 }
                               })
  },
  //Validation Rules
  EmailAddressValid: function (Value, Rule, ErrorControl) {
    var currentUser = ErrorControl.Object
    if (currentUser.EmailAddress().trim().length == 0) {
      ErrorControl.AddError("Email address is required")
    }
  },
  NewPasswordValid: function (Value, Rule, ErrorControl) {
    var currentUser = ErrorControl.Object
    if (currentUser.NewPassword().trim().length == 0 && currentUser.IsNew()) {
      ErrorControl.AddError("Password is required")
    }
  }
};

UserResourceSchedulerBO = {
  UserResourceSchedulerBOToString: function (self) {
    return "Resource Scheduler"
  }
};

UserSystemAreaBO = {
  AreaOptions: function (List, self) {
    var results = []
    List.Iterate(function (ar, arIndx) {
      if (ar.SystemID == self.GetParent().SystemID()) {
        results.push(ar)
      }
    })
    return results
  }
};

UserSecurityBO = {
  CanEdit: function (Template, FieldName) {
    switch (FieldName) {
      case 'TemplateButtonEnable':
        if (ViewModel.TemplateUserTimesheetVisibleDisciplineList().length > 0) {
          if (ViewModel.TemplateUserTimesheetVisibleDisciplineList()[0].DisciplineID() != undefined &&
              ViewModel.TemplateUserTimesheetVisibleDisciplineList()[0].SystemID() != undefined &&
              ViewModel.TemplateUserTimesheetVisibleDisciplineList()[0].ProductionAreaID() != undefined &&
              ViewModel.UsersToAddList().length > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return true;
        }
        break;
      default:
        return true;
        break;
    }
  },
};

ROUserFindListPagedBO = {
  SystemIDSet: function (self) {
    ViewModel.ROUserFindListPagedListManager().Refresh();
    this.afterRefresh();
  },
  ProductionAreaIDSet: function (self) {
    ViewModel.ROUserFindListPagedListManager().Refresh();
    this.afterRefresh();
  },
  DisciplineIDSet: function (self) {
    ViewModel.ROUserFindListPagedListManager().Refresh();
    this.afterRefresh();
  },
  KeywordSet: function (self) {
    ViewModel.ROUserFindListPagedListManager().Refresh();
    this.afterRefresh();
  },
  AllowedDisciplines: function (List, Item) {
    var Allowed = [];
    if (List) {
      ClientData.ROSystemProductionAreaDisciplineList.Iterate(function (itm, ind) {
        if (itm.SystemID == Item.SystemID() && itm.ProductionAreaID == Item.ProductionAreaID()) {
          Allowed.push(itm);
        }
      });
    }
    return Allowed;
  },
  afterRefresh: function () {
    ViewModel.ROUserFindListPagedListManager().afterRefresh = function () {
      ViewModel.UsersToAddList().Iterate(function (itm, ind) {
        ViewModel.ROUserFindListPagedList().Iterate(function (itm2, ind2) {
          if (itm.UserID() == itm2.UserID()) {
            itm2.IsSelected(true);
          }
        });
      });
    };
  }
};

HumanResourceSwapBO2 = {
  saveItem: function (item, options) {
    ViewModel.CallServerMethod("SaveHumanResourceSwapList",
                               { HumanResourceSwapList: [item.Serialise()] },
                               function (response) {
                                 if (response.Success) {
                                   KOFormatter.Deserialise(response.Data[0], item)
                                   OBMisc.Notifications.GritterSuccess("Swap Saved", "", 250)
                                   if (options.onSuccess) { options.onSuccess(response) }
                                 }
                                 else {
                                   OBMisc.Notifications.GritterError("Save Swap Failed", response.ErrorText, 1000)
                                   if (options.onSuccess) { options.onSuccess(response) }
                                 }
                               })
  }
}

ROUserNotificationBO = {
  toString: function (self) { return self.NotificationTitle() },
  //DataAccess
  get: function (options) {
    Singular.GetDataStateless("OBLib.Users.ReadOnly.ROUserNotificationList, OBLib", options.criteria, function (response) {
      if (response.Success) {
        if (options.onSuccess) { options.onSuccess.call(response.Data[0], response) }
      } else {
        if (options.onFail) { options.onFail(response) }
      }
    })
  }
};

ROUserNotificationListCriteriaBO = {
  StartDateSet: function (self) {
    this.refresh(self)
  },
  EndDateSet: function (self) {
    this.refresh(self)
  },
  refresh: function (self) {
    if (!self.IsProcessing()) {
      self.IsProcessing(true)
      ROUserNotificationBO.get({
        criteria: {
          UserID: ViewModel.CurrentUserID(), //we can get away with this kind of sin right here as this object is only used in a special case,
          StartDate: self.StartDate(), //we can get away with this kind of sin right here as this object is only used in a special case,
          EndDate: self.EndDate() //we can get away with this kind of sin right here as this object is only used in a special case
        },
        onSuccess: function (response) {
          ViewModel.ROUserNotificationList.Set(response.Data)
          self.IsProcessing(false)
        },
        onFail: function (response) {
          self.IsProcessing(false)
          OBMisc.Notifications.GritterError("Error", response.ErrorText, 500)
        }
      })
    }
  }
};

ROUserBO = {
  AddContractType: function (self) {
    //accessing a propert in knockout - call as a method i.e ROContractType.IsSelected() will give you the value of the IsSelected property on ROContractType (self)
    self.IsSelected(!self.IsSelected()); //changing the value of a knockout property, you simple pass in the newvalue to the function
    //self.ContractType("blah")
    if (self.IsSelected()) {  //If selected
      if (ViewModel.UserListCriteria().ContractTypeIDs().indexOf(self.ContractTypeID()) == -1) { //If item is not in list, add it to the list (JS always returns -1 when an item is not found in an array)
        ViewModel.UserListCriteria().ContractTypeIDs().push(self.ContractTypeID());  //If item is not found in the list then push item through to ContractTypeIDs (BusinessObject)
      }
    }
    else {
      var index = ViewModel.UserListCriteria().ContractTypeIDs().indexOf(self.ContractTypeID()); //Does the opposite of previous IF statement.(Removes item from list if already selected)
      if (index != -1) {
        ViewModel.UserListCriteria().ContractTypeIDs().splice(index, 1);
      }
    }
    ViewModel.UserPagingInfo().Refresh()  //    
  },

  AddSystem: function (self) {
    //obj.IsSelected(!obj.IsSelected())
    //this.SetXMLCriteria()
    //HumanResourceScheduleExcelReport.GetAllowedHumanResources()
    //HumanResourceScheduleExcelReport.RefreshDisciplines()

    self.IsSelected(!self.IsSelected());
    if (self.IsSelected()) {
      if (ViewModel.UserListCriteria().SystemIDs().indexOf(self.SystemID()) == -1) {
        ViewModel.UserListCriteria().SystemIDs().push(self.SystemID());
      }
    }
    else {
      var index = ViewModel.UserListCriteria().SystemIDs().indexOf(self.SystemID());
      if (index != -1) {
        ViewModel.UserListCriteria().SystemIDs().splice(index, 1);
      }
    }
    ViewModel.UserPagingInfo().Refresh()

  },

  AddProductionArea: function (self) {
    self.IsSelected(!self.IsSelected());
    if (self.IsSelected()) {
      if (ViewModel.UserListCriteria().ProductionAreaIDs().indexOf(self.ProductionAreaID()) == -1) {
        ViewModel.UserListCriteria().ProductionAreaIDs().push(self.ProductionAreaID());
      }
    }
    else {
      var index = ViewModel.UserListCriteria().ProductionAreaIDs().indexOf(self.ProductionAreaID());
      if (index != -1) {
        ViewModel.UserListCriteria().ProductionAreaIDs().splice(index, 1);
      }
    }
    ViewModel.UserPagingInfo().Refresh()
  }
};