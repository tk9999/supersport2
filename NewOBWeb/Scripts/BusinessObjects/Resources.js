﻿CopySettingBO = {
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    //args.Data.SystemID = args.Object.SystemID()
    //args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.EquipmentID(selectedItem.EquipmentID)
      businessObject.ResourceID(selectedItem.ResourceID)
    } else {
      businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
    }
    //if (!selectedItem.IsAvailable) {
    //  businessObject.CircuitClashCount(1)
    //  //businessObject.CircuitClashCount() + 
    //} else {
    //  businessObject.CircuitClashCount(0)
    //}
  },
  EquipmentIDSet: function (EquipmentAllocator) {

  },
  DatesValid: function (Value, Rule, CtlError) {
    var ct = CtlError.Object.StartDateTimeBuffer();
    var st = CtlError.Object.StartDateTime();
    var et = CtlError.Object.EndDateTime();
    var wt = CtlError.Object.EndDateTimeBuffer();
    if (ct && st && et && wt) {
      if (OBMisc.Dates.IsAfter(ct, st) || OBMisc.Dates.IsAfter(st, et) || OBMisc.Dates.IsAfter(et, wt)) {
        CtlError.AddError("Times overlap");
      }
    } else {
      CtlError.AddError("All times are required");
    }
  }
}

ResourceBookingBO = {
  getClashes: function (bookingObjects, options) {
    let me = this,
      bookingsArray = [];

    bookingtemplate = {
      Guid: null,
      ResourceBookingID: null,
      ResourceID: null,
      ResourceBookingTypeID: null,
      StartDateTime: null,
      EndDateTime: null,
      HumanResourceShiftID: null,
      IsCancelled: null,
      HumanResourceOffPeriodID: null,
      HumanResourceSecondmentID: null,
      ProductionHRID: null,
      IgnoreClashes: null,
      RoomScheduleID: null,
      IsTBC: null,
      EquipmentScheduleID: null,
      ProductionSystemAreaID: null,
      ParentResourceBookingID: null
    }

    let bt = null
    bookingObjects.forEach((bookingObject) => {
      //create a copy
      let bookingItem = $.extend({}, bookingtemplate)
      //copy the bookingtemplate properties from the  bookingObject
      Object.keys(bookingItem).map((propertyName) => {
        if (propertyName in bookingObject) {
          bookingItem[propertyName] = bookingObject[propertyName]
        }
      })
      //add this to the list
      bookingsArray.push(bookingItem)
    }, me)

    let promise = ViewModel.CallServerMethodPromise("GetResourceClashes", { ResourceBookings: bookingsArray })
    promise.then(function (data) {
      if (options.onSuccess) { options.onSuccess(data) };
    }),
      function (errorText) {
        if (options.onFail) { options.onFail(errorText) };
      }

  },
  checkEditState: function (resourceBookingID, options) {
    ViewModel.CallServerMethodPromise("CheckEditState", { ResourceBookingID: resourceBookingID })
      .then(data => {
        if (options.onSuccess) { options.onSuccess(data) }
      }),
      errorText => {
        if (options.onFail) { options.onFail(errorText) }
      }
  }
};

ResourceSchedulerBO = {
  ResourceSchedulerBOToString: function (self) {
    return ""
  },
  getPromise: function (options) {
    return Singular.GetDataStatelessPromise("OBLib.ResourceSchedulers.New.ResourceSchedulerList, OBLib", options.criteria)
  },
  saveModal: function (rs) {
    ViewModel.CallServerMethodPromise("SaveResourceScheduler",
      { Scheduler: rs.Serialise() }).then(function (data) {
        ViewModel.ResourceScheduler.Set(data)
        OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
        $("#EditResourceSchedulerModal").modal('hide')
      },
      function (errorText) {
        OBMisc.Notifications.GritterError("Save Failed", errorText, 1000)
      })
  }
};

ResourceSchedulerGroupBO = {
  ResourceSchedulerGroupBOToString: function (self) { return "" },
  SelectedByDefaultSet: function (self) {
    self.ResourceSchedulerSubGroupList().Iterate(function (sGroup, sGroupIndx) {
      sGroup.SelectedByDefault(self.SelectedByDefault())
    })
  }
};

ResourceSchedulerSubGroupBO = {
  ResourceSchedulerSubGroupBOToString: function (self) { return "" },
  FreelancersClicked: function (self) {
    if (self.Freelancers() == null) {
      self.Freelancers(true)
    } else if (self.Freelancers()) {
      self.Freelancers(false)
    } else if (self.Freelancers() == false) {
      self.Freelancers(null)
    }
  },
  FreelancersButtonText: function (self) {
    if (self.Freelancers() == null) {
      return "All Contract Types"
    } else if (self.Freelancers()) {
      return "Freelancers/ISP"
    } else if (self.Freelancers() == false) {
      return "Perm/Contract"
    }
  },
  FreelancersIconCss: function (self) {
    if (self.Freelancers() == null) {
      return "fa fa-minus"
    } else if (self.Freelancers()) {
      return "fa fa-fire"
    } else if (self.Freelancers() == false) {
      return "fa fa-drupal"
    }
  },

  //Discipline
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.HRProductionAreaID()
  },
  afterDisciplineIDRefreshAjax: function (args) { },
  onDisciplineIDSelected: function (item, businessObject) { },

  //Vehicle
  triggerVehicleTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setVehicleTypeIDCriteriaBeforeRefresh: function (args) {

  },
  afterVehicleTypeIDRefreshAjax: function (args) { },
  onVehicleTypeIDSelected: function (item, businessObject) { },

  //Team Number
  triggerSystemTeamNumberIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setSystemTeamNumberIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().GetParent().SystemID()
  },
  afterSystemTeamNumberIDRefreshAjax: function (args) { },
  onSystemTeamNumberIDSelected: function (item, businessObject) { },

  //Set
  DisciplineIDSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  FreelancersSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  RoomTypeIDSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  HRProductionAreaIDSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  EquipmentTypeIDSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  VehicleTypeIDSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  SystemTeamNumberIDSet(self) {
    ResourceSchedulerSubGroupBO.updateSubGroupResources(self);
  },
  async updateSubGroupResources(subGroup) { 
    ResourceSchedulerSubGroupResourceBO.get({
      criteria: {
        ResourceTypeID: subGroup.GetParent().ResourceTypeID(),
        SiteID: subGroup.GetParent().SiteID(),
        DisciplineID: subGroup.DisciplineID(),
        RoomTypeID: subGroup.RoomTypeID(),
        HRProductionAreaID: subGroup.HRProductionAreaID(),
        EquipmentTypeID: subGroup.EquipmentTypeID(),
        VehicleTypeID: subGroup.VehicleTypeID(),
        SystemTeamNumberID: subGroup.SystemTeamNumberID(),
        Freelancers: subGroup.Freelancers(),
        SystemID: subGroup.GetParent().GetParent().SystemID()
      },
      onSuccess: function (data) {
        subGroup.ResourceSchedulerSubGroupResourceList.Set(data)
      }
    })
  },
  SelectedByDefaultSet(self) {
    self.ResourceSchedulerSubGroupResourceList().Iterate(function (sGroupRes, sGroupResIndx) {
      sGroupRes.SelectedByDefault(self.SelectedByDefault())
    })
  }
}

ResourceSchedulerSubGroupResourceBO = {
  get: function (options) {
    Singular.GetDataStatelessPromise("OBLib.ResourceSchedulers.New.ResourceSchedulerSubGroupResourceList, OBLib", options.criteria)
      .then(function (data) {
        if (options.onSuccess) { options.onSuccess(data) }
      },
      function (errorText) {
        OBMisc.Notifications.GritterError("Fetch shift failed", errorText, 1000)
      })

  },
  ResourceSchedulerSubGroupResourceBOToString: function (self) { return "" }
}