﻿function GetEventErrors(Value, Rule, Args) {
  var ImportedEvent = CtlError.Object;
  var AllErrors = '';
  if (ImportedEvent.ErrorInd()) {
    AllErrors = ImportedEvent.ErrorDescription() + '<br/>';
  }
  for (i = 0; i < ImportedEvent.ImportedEventChannelList() ; i++) {
    var chan = ImportedEvent.ImportedEventChannelList()[i];
    if (chan.ExceptionInd()) {
      AllErrors += chan.ExceptionDescription() + '<br/>';
    }
  }
  CtlError.AddError(AllErrors);
};

function ImportedEventToString(Value, Rule, Args) {
  var CurrentImportedEvent = self;
  return CurrentImportedEvent.SeriesTitle() + ' - ' + CurrentImportedEvent.Title();
}

function ProductionVenueIDValid(Value, Rule, Args) {
  var ImportedEvent = CtlError.Object;
  if (!ImportedEvent.ProductionVenueID() && ImportedEvent.Selected()) {
    CtlError.AddError('Venue/Stadium is required');
  }
}

function DefaultRoomIDValid(Value, Rule, CtlError) {
  SynergyImporter.DefaultRoomIDValid(Value, Rule, CtlError);
}

function ProductionSpecRequirementIDValid(Value, Rule, CtlError) {
  SynergyImporter.ProductionSpecRequirementIDValid(Value, Rule, CtlError);
}

function LiveDateValid() {
  var ImportedEvent = CtlError.Object;
  //if we have both dates
  if (ImportedEvent.Selected() && ImportedEvent.UserProductionAreaID() == 1) {
    if (ImportedEvent.LiveDate() && ImportedEvent.LiveEndDateTime()) {
      var sd = new Date(ImportedEvent.LiveDate());
      var ed = new Date(ImportedEvent.LiveEndDateTime());

      if (moment(sd).isSame(ed, 'minute')) {
        CtlError.AddError('Start Time and End Time cannot be the same');
      }

      //start date time before end date time
      if (moment(sd).isAfter(ed, 'minute')) {
        CtlError.AddError('Start Date Time must be before End Date Time');
      }

      //day difference
      var diff = moment(ed).diff(sd, 'day');
      if (diff > 0) {
        CtlError.AddError('Start Date Time and End Date Time cannot be more than a day apart');
      }

    } else if (!ImportedEvent.LiveDate()) {
      CtlError.AddError("Live Start Date and Time is required");
    }
  }
}

function LiveEndDateTimeValid() {
  var ImportedEvent = CtlError.Object;
  //if we have both dates
  if (ImportedEvent.Selected() && ImportedEvent.UserProductionAreaID() == 1) {
    if (ImportedEvent.LiveDate() && ImportedEvent.LiveEndDateTime()) {
      var sd = new Date(ImportedEvent.LiveDate());
      var ed = new Date(ImportedEvent.LiveEndDateTime());

      if (moment(sd).isSame(ed, 'minute')) {
        CtlError.AddError('Start Time and End Time cannot be the same');
      }

      //start date time before end date time
      if (moment(sd).isAfter(ed, 'minute')) {
        CtlError.AddError('Start Date Time must be before End Date Time');
      }

      //day difference
      var diff = moment(ed).diff(sd, 'day');
      if (diff > 0) {
        CtlError.AddError('Start Date Time and End Date Time cannot be more than a day apart');
      }

    } else if (!ImportedEvent.LiveEndDateTime()) {
      CtlError.AddError("Live End Date and Time is required");
    }
  }
}

function VehicleIDValid(Value, Rule, Args) {
  var ImportedEvent = CtlError.Object;
  if (ViewModel.ProductionAreaID() == 1 && ViewModel.SystemID() == 1 && ImportedEvent.Selected()) {
    if (ImportedEvent.VehicleID()) {
      ImportedEvent.IsBusyClient(true);
      var f = new KOFormatterObject();
      ViewModel.CallServerMethod('CheckVehicleAvailable', { ImportedEvent: f.Serialise(ImportedEvent) }, function (WebResult) {
        CtlError.AddError(WebResult.Data);
        ImportedEvent.IsBusyClient(false);
      });
    }
  }
}

function PrimaryChannelValid(Value, Rule, CtlError) {
  SynergyImporter.PrimaryChannelValid(Value, Rule, CtlError);
}

function CallTimeValid(Value, Rule, CtlError) {
  SynergyImporter.CallTimeValid(Value, Rule, CtlError);
}

function OnAirStartDateTimeValid(Value, Rule, CtlError) {
  SynergyImporter.OnAirStartDateTimeValid(Value, Rule, CtlError);
}

function OnAirEndDateTimeValid(Value, Rule, CtlError) {
  SynergyImporter.OnAirEndDateTimeValid(Value, Rule, CtlError);
}

function WrapEndDateTimeValid(Value, Rule, CtlError) {
  SynergyImporter.WrapEndDateTimeValid(Value, Rule, CtlError);
}

function DefaultRoomIDSet(self) {
  SynergyImporter.DefaultRoomIDSet(self);
}

function CallTimeValid(Value, Rule, CtlError) {
  SynergyImporter.CallTimeValid(Value, Rule, CtlError);
}

function OnAirStartTimeValid(Value, Rule, CtlError) {
  SynergyImporter.OnAirStartTimeValid(Value, Rule, CtlError);
}

function OnAirEndTimeValid(Value, Rule, CtlError) {
  SynergyImporter.OnAirEndTimeValid(Value, Rule, CtlError);
}

function WrapTimeValid(Value, Rule, CtlError) {
  SynergyImporter.WrapTimeValid(Value, Rule, CtlError);
}

function AllTimesValid(Value, Rule, CtlError) {
  SynergyImporter.AllTimesValid(Value, Rule, CtlError);
}

function TeamsPlayingValid(Value, Rule, CtlError) {
  SynergyImporter.TeamsPlayingValid(Value, Rule, CtlError);
};

function SelectedSet(self) {
  SynergyImporter.SelectedSet(self);
};

function FilterSet(self) {
  SynergyImporter.FilterSet(self);
}

function PrimaryChannelSet(self) {
  SynergyImporter.PrimaryChannelSet(self);
}

function KeywordSet(self) {
  SynergyImporter.Search(1);
}