﻿SDHRPosBO = {
  PositionLevelChanged: function (self) {
    SkillsDatabase.PositionLevelChanged(self, this);
  }
}

OBHRSkillPosBO = {
  OBHRPosLevelBeforeSet: function (self) {
    SkillsDatabase.OBHRPosLevelBeforeSet(self);
  },
  OBHRPosLevelAfterSet: function (self) {
    SkillsDatabase.OBHRPosLevelAfterSet(self);
  },
  OBHRSkillPosToString: function (self) {
    return SkillsDatabase.OBHRSkillPosToString(self);
  }
}