﻿ProductionCostBO = {
  AccommodationGuestRoomCostSet: function (AccommodationGuest) {
    ProductionCostBO.SetTotalAccommodationCost(AccommodationGuest.GetParent());
  },
  FlightSingleTicketPriceSet: function (Flight) {
    Flight.ProductionCostFlightPassengerList().Iterate(function (FlightPassenger, Index) {
      FlightPassenger.TicketPrice(Flight.SingleTicketPrice());
    });
  },
  FlightPassengerTicketPriceSet: function (FlightPassenger) {
    ProductionCostBO.SetTotalFlightCost(FlightPassenger.GetParent());
  },
  FlightPassengerServiceFeeSet: function (FlightPassenger) {
    ProductionCostBO.SetTotalFlightCost(FlightPassenger.GetParent());
  },
  FlightPassengerCancellationFeeSet: function (FlightPassenger) {
    ProductionCostBO.SetTotalFlightCost(FlightPassenger.GetParent());
  },
  RentalCostSet: function (self) {
    ProductionCostBO.SetTotalCost();
  },
  RoomCostSet: function (self) {
    self.ProductionCostAccomodationGuestList().Iterate(function (AccommodationGuest, Index) {
      AccommodationGuest.RoomCost(self.RoomCost());
    });
    self.ProductionCostAccomodationSupplierGuestList().Iterate(function (AccommodationSupplierGuest, Index) {
      AccommodationSupplierGuest.RoomCost(self.RoomCost());
    });
  },
  SetTotalAccommodationCost: function (Accommodation) {
    var Total = 0;
    Accommodation.ProductionCostAccomodationGuestList().Iterate(function (AG, AGInd) {
      Total += (AG.RoomCost());
    });
    Accommodation.ProductionCostAccomodationSupplierGuestList().Iterate(function (AG, AGInd) {
      Total += (AG.RoomCost());
    });
    Accommodation.TotalCost(Total);
    ProductionCostBO.SetTotalCost();
  },
  SetTotalFlightCost: function (ProductionCostFlight) {
    var Total = 0;
    ProductionCostFlight.ProductionCostFlightPassengerList().Iterate(function (FP, FPInd) {
      Total += (FP.TicketPrice() + FP.ServiceFee() + FP.CancellationFee());
    });
    ProductionCostFlight.TotalTicketCost(Total);
    ProductionCostBO.SetTotalCost();
  },
  SetTotalCost: function () {
    var Total = 0;
    ViewModel.CurrentProductionCost().ProductionCostAccomodationList().Iterate(function (Accommodation, AccInd) {
      Accommodation.ProductionCostAccomodationGuestList().Iterate(function (AG, AGInd) {
        Total += (AG.RoomCost());
      });
      Accommodation.ProductionCostAccomodationSupplierGuestList().Iterate(function (AG, AGInd) {
        Total += (AG.RoomCost());
      });
    });
    ViewModel.CurrentProductionCost().ProductionCostFlightList().Iterate(function (ProductionCostFlight, AccInd) {
      ProductionCostFlight.ProductionCostFlightPassengerList().Iterate(function (FP, FPInd) {
        Total += (FP.TicketPrice() + FP.ServiceFee() + FP.CancellationFee());
      });
    });
    ViewModel.CurrentProductionCost().ProductionCostCarRentalList().Iterate(function (CR, FPInd) {
      Total += (CR.RentalCost());
    });
    ViewModel.CurrentProductionCost().TotalCost(Total);
  },
  GetTotalCost: function () {
    if (ViewModel.CurrentProductionCost()) {
      return "R " + ViewModel.CurrentProductionCost().TotalCost();
    }
    return "R 0.00";
  }
}

ProductionCostAccomodationGuestBO = {
  RoomCostSet: function (self) {
    ProductionCostBO.AccommodationGuestRoomCostSet(self);
  }
}

ProductionCostCarRentalBO = {
  RentalCostSet: function (self) {
    ProductionCostBO.RentalCostSet(self);
  }
}

ProductionCostFlightPassengerBO = {
  TicketPriceSet: function (self) {
    ProductionCostBO.FlightPassengerTicketPriceSet(self);
  },
  ServiceFeeSet: function (self) {
    ProductionCostBO.FlightPassengerServiceFeeSet(self);
  },
  CancellationFeeSet: function (self) {
    ProductionCostBO.FlightPassengerCancellationFeeSet(self);
  }
}