﻿var ProductionBOPrototype = function () {
  var me = this;

  //Validation Rules
  this.ProductionTypeIDValid = function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionTypeID()) {
      CtlError.AddError("Production Type is required");
    }
  }
  this.EventTypeIDValid = function (Value, Rule, CtlError) {
    if (!CtlError.Object.EventTypeID()) {
      CtlError.AddError("Event Type is required");
    }
  }
  this.TitleValid = function (Value, Rule, CtlError) {
    if (CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  }
  this.TeamsValid = function (Value, Rule, CtlError) {
    if (CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  }
  this.GenRefNoValid = function (Value, Rule, CtlError) {
    if (CtlError.Object.CreationTypeID() == 1 && !CtlError.Object.SynergyGenRefNo()) {
      CtlError.AddError("Gen Ref is required");
    }
  }
  this.ProductionVenueIDValid = function (Value, Rule, CtlError) {
    //if (!CtlError.Object.ProductionVenueID()) {
    //  CtlError.AddError("Production Venue is required");
    //}
  }
  this.PlayStartDateTimeValid = function (Value, Rule, CtlError) {
    //var lst = CtlError.Object.PlayStartDateTime();
    //var leti = CtlError.Object.PlayEndDateTime();
    //if (!lst) {
    //  CtlError.AddError("Live Start Time is required");
    //} else if (lst && leti) {
    //  if (OBMisc.Dates.IsAfter(lst, leti)) {
    //    CtlError.AddError("Live Start Time must be before Live End Time");
    //  }
    //}
  }
  this.PlayEndDateTimeValid = function (Value, Rule, CtlError) {
    //var leti = CtlError.Object.PlayEndDateTime();
    //var isPlaceholder = CtlError.Object.PlaceHolderInd();
    //if (!leti) {
    //  CtlError.AddError("Live End Time is required");
    //}
  }

  //Set Expressions

  //Cans
  this.CanEdit = function (ProductionDetailBase, Field) {
    switch (Field) {
      case 'SynergyGenRefNo':
        if (CtlError.Object.CreationTypeID() == 2) { return false } else { return true }
        break;
      default:
        return true;
        break;
    }
  }

  //Methods
  this.GetToString = function (self) {
    var descript = ''
    if (self.ProductionTypeEventType().trim().length > 0) {
      descript = self.ProductionTypeEventType()
      if (self.Title().trim().length > 0) {
        descript += ' (' + self.Title() + ')'
      }
    }
    else {
      descript = self.ProductionTypeEventType()
    }
    return descript
  }

};

ProductionBudgetedBO = {
  CanEdit: function (ProductionBudgeted, FieldName) {
    switch (FieldName) {
      case 'BudgetedInd':
        if (!ProductionBudgeted.RingFencedInd())
          return true;
        break;
      case 'RingFencedInd':
        if (!ProductionBudgeted.BudgetedInd())
          return true;
        break;
      case 'RecoupedInd':
        if (!ProductionBudgeted.BudgetedInd())
          return true;
        break;
      default:
        return true;
        break;
    }
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.PlayStartDateTime();
    var leti = CtlError.Object.PlayEndDateTime();

    if (!lst) {
      CtlError.AddError("Live Start Time is required");
    } else if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live Start Time must be before Live End Time");
      }
    }
  },
  DebtorNameRequired: function (Value, Rule, CtlError) {
    if (!CtlError.Object.BudgetedInd() && CtlError.Object.RecoupedInd() && CtlError.Object.DebtorID() == 0) {
      CtlError.AddError("Debtor name is required");
    }
  },
  AmountRecoupedRequired: function (Value, Rule, CtlError) {
    if (!CtlError.Object.BudgetedInd() && CtlError.Object.RecoupedInd() && CtlError.Object.TotalRecoupedAmount() == 0) {
      CtlError.AddError("Amount Recouped is required");
    }
  },
  NoRecoupReasonRequired: function (Value, Rule, CtlError) {
    if (!CtlError.Object.BudgetedInd() && !CtlError.Object.RecoupedInd()) {
      if (CtlError.Object.NoRecoupReason().trim().length == 0) {
        CtlError.AddError("Reason Required for no recoupment");
      }
    }
  },
  TotalCostRequired: function (Value, Rule, CtlError) {
    if ((!CtlError.Object.BudgetedInd() || CtlError.Object.RingFencedInd()) && CtlError.Object.RealCostAmount() == 0) {
      CtlError.AddError("Total Cost of Production Required");
    }
  },
  ProductionBudgetedBOToString: function (self) {
    return 'ProductionID: ' + self.ProductionID() + ' - ' +
      'Production Description: ' + self.ProductionDescription();
  }
};

ProductionDetailBaseBO = {
  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionTypeID()) {
      CtlError.AddError("Production Type is required");
    }
  },
  EventTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.EventTypeID()) {
      CtlError.AddError("Event Type is required");
    }
  },
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  },
  TeamsValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.TeamsPlaying().trim().length == 0 && (CtlError.Object.PlaceHolderInd() || CtlError.Object.OBCityInd() || CtlError.Object.OBContentInd())) {
      CtlError.AddError("Teams are required");
    }
  },
  GenRefNoValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.CreationTypeID() == 1 && !CtlError.Object.SynergyGenRefNo()) {
      CtlError.AddError("Gen Ref is required");
    }
  },
  CanEdit: function (ProductionDetailBase, Field) {
    switch (Field) {
      case 'SynergyGenRefNo':
        if (CtlError.Object.CreationTypeID() == 2) { return false } else { return true }
        break;
      default:
        return true;
        break;
    }
  },
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    //if (!CtlError.Object.ProductionVenueID()) {
    //  CtlError.AddError("Production Venue is required");
    //}
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    //var lst = CtlError.Object.PlayStartDateTime();
    //var leti = CtlError.Object.PlayEndDateTime();
    ////var isPlaceholder = CtlError.Object.PlaceHolderInd();
    //if ((CtlError.Object.PlaceHolderInd || CtlError.Object.OBCityInd || CtlError.Object.OBContentInd)) {
    //  if (!lst) {
    //    CtlError.AddError("Live Start Time is required");
    //  } else if (lst && leti) {
    //    if (OBMisc.Dates.IsAfter(lst, leti)) {
    //      CtlError.AddError("Live Start Time must be before Live End Time");
    //    }
    //  }
    //}
  },
  PlayEndDateTimeValid: function (Value, Rule, CtlError) {
    //var leti = CtlError.Object.PlayEndDateTime();
    //var isPlaceholder = CtlError.Object.PlaceHolderInd();
    //if (!leti) {
    //  CtlError.AddError("Live End Time is required");
    //}
  },
  SelectStudioGenreSeries: function (ProductionDetailBase) {
    ROEventTypes.Manager().SelectedItems([]); //.ClearSelection();
    ROEventTypes.Manager().PageNo(1);
    ROEventTypes.Manager().SingleSelect(true);
    ROEventTypes.Manager().MultiSelect(false);
    ROEventTypes.SetOptions({
      AfterRowSelected: function (SelectedItem) {
        ProductionDetailBase.ProductionTypeID(SelectedItem.ProductionTypeID());
        ProductionDetailBase.EventTypeID(SelectedItem.EventTypeID());
        var Title = SelectedItem.ProductionType() + ' (' + SelectedItem.EventType() + ')' + ((ProductionDetailBase.TeamsPlaying().trim().length > 0) ? ProductionDetailBase.TeamsPlaying() : '');
        ProductionDetailBase.GetParent().Title(Title);
        //BookingDescription
        ProductionDetailBase.ProductionTypeEventType(SelectedItem.ProductionType() + ' (' + SelectedItem.EventType() + ')');
        $("#RoomScheduleEventTypeSelect").dialog("close");
      },
      AfterRowDeSelected: function () {
        ProductionDetailBase.ProductionTypeID(null);
        ProductionDetailBase.EventTypeID(null);
        ProductionDetailBase.ProductionTypeEventType("");
      }
    });
    $("#RoomScheduleEventTypeSelect").dialog({
      title: "Select Genre and Series",
      closeText: "Close",
      height: 520,
      width: 800,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        ROEventTypes.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })
  },
  ClearGenreSeries: function (ProductionDetailBase) {
    ProductionDetailBase.ProductionTypeEventType("");
    ProductionDetailBase.ProductionTypeID(null);
    ProductionDetailBase.EventTypeID(null);
  },
  SelectProductionVenue: function (ProductionDetailBase) {

  },
  ClearProductionVenue: function (ProductionDetailBase) {

  }
};

ManualProductionBO = {
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionVenueID() && (CtlError.Object.PlaceHolderInd || CtlError.Object.OBCityInd || CtlError.Object.OBContentInd)) {
      CtlError.AddError("Production Venue is required");
    }
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.PlayStartDateTime();
    var leti = CtlError.Object.PlayEndDateTime();
    //var isPlaceholder = CtlError.Object.PlaceHolderInd();
    if ((CtlError.Object.PlaceHolderInd || CtlError.Object.OBCityInd || CtlError.Object.OBContentInd)) {
      if (!lst) {
        CtlError.AddError("Live Start Time is required");
      } else if (lst && leti) {
        if (OBMisc.Dates.IsAfter(lst, leti)) {
          CtlError.AddError("Live Start Time must be before Live End Time");
        }
      }
    }
  },
  PlayEndDateTimeValid: function (Value, Rule, CtlError) {
    var leti = CtlError.Object.PlayEndDateTime();
    var isPlaceholder = CtlError.Object.PlaceHolderInd();
    if (!leti) {
      CtlError.AddError("Live End Time is required");
    }
  }
};

ROProductionBO = {
  CreatedTypeHTML: function (ROProduction, Element) {
    if (ROProduction.CreationTypeID() == 2) {
      return " Manual";
    } else {
      return "Imported";
    }
  },
  CreatedTypeCss: function (ROProduction, Element) {
    if (ROProduction.CreationTypeID() == 2) {
      return "btn btn-xs btn-danger";
    } else {
      return "btn btn-xs btn-default";
    }
  },

  //set
  triggerProductionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionTypeIDCriteriaBeforeRefresh: function (args) {

  },

  triggerEventTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setEventTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },

  triggerProductionVenueIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionVenueIDCriteriaBeforeRefresh: function (args) {
  },

  triggerEventManagerAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setEventManagerCriteriaBeforeRefresh: function (args) {
  },

  canEditProduction: function (ROProduction) {
    return (ROProduction.IsBeingEditedBy() === null || ROProduction.IsBeingEditedBy() == ViewModel.CurrentUserID())
  },
  getTitle: function (ROProduction) {
    return (ROProduction.IsBeingEditedBy() === null ? "Edit Production - " + ROProduction.ProductionRefNo() : "Production " + ROProduction.ProductionRefNo() + " is being edited by " + ROProduction.InEditByName())
  },
  editorImagePath: function (ROProduction) {
    return window.SiteInfo.SitePath + '/' + ROProduction.EditImagePath()
  },
  openProduction: function (ROProduction) {
    OBMisc.Notifications.GritterInfo("Checking edit state", "Please wait", 500)
    ResourceBookingBO.checkEditState(ROProduction.ResourceBookingID(),
      {
        onSuccess: data => {
          ROProduction.IsBeingEditedBy(data.IsBeingEditedByUserID)
          ROProduction.InEditDateTime(data.InEditDateTime)
          ROProduction.InEditByName(data.IsBeingEditedByName)
          ROProduction.EditImagePath(data.EditImagePath)
          if (ROProduction.IsBeingEditedBy() == null || ROProduction.IsBeingEditedBy() == ViewModel.CurrentUserID()) {
            //window.siteHubManager.bookingIntoEdit(ROPRoduction.ResourceBookingID())
            let path = Singular.RootPath + "/Productions/Production.aspx?P=" + ROProduction.ProductionID().toString(); // + '&PSAID=' + ROProduction.ProductionSystemAreaID().toString();
            window.open(path, '_blank');
          } else { 
            OBMisc.Notifications.GritterWarning("Booking Locked", "This booking is still being edited by " + ROProduction.InEditByName(), 1000)
          }
        },
        onFail: errorText => {
          OBMisc.Notifications.GritterError("Error", errorText, 1000)
        }
      })
  }
};

ProductionDetailBO = {

  saveManualProduction: function (manualProduction, afterSuccess, afterFail) {
    ViewModel.CallServerMethod("SaveManualProduction",
      {
        ManualProduction: manualProduction.Serialise()
      },
      function (response) {
        if (response.Success) {
          if (afterSuccess) { afterSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
          if (afterFail) { afterFail(response) }
        }
      })
  },

  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionTypeID()) {
      CtlError.AddError("Production Type is required");
    }
  },
  EventTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.EventTypeID()) {
      CtlError.AddError("Event Type is required");
    }
  },
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  },
  TeamsValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  },
  GenRefNoValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.CreationTypeID() == 1 && !CtlError.Object.SynergyGenRefNo()) {
      CtlError.AddError("Gen Ref is required");
    }
  },
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    //if (!CtlError.Object.ProductionVenueID()) {
    //  CtlError.AddError("Production Venue is required");
    //}
  },
  PlayStartDateTimeValid: function (Value, Rule, CtlError) {
    //var lst = CtlError.Object.PlayStartDateTime();
    //var leti = CtlError.Object.PlayEndDateTime();
    //if (!lst) {
    //  CtlError.AddError("Live Start Time is required");
    //} else if (lst && leti) {
    //  if (OBMisc.Dates.IsAfter(lst, leti)) {
    //    CtlError.AddError("Live Start Time must be before Live End Time");
    //  }
    //}
  },
  PlayEndDateTimeValid: function (Value, Rule, CtlError) {
    //var leti = CtlError.Object.PlayEndDateTime();
    //var isPlaceholder = CtlError.Object.PlaceHolderInd();
    //if (!leti) {
    //  CtlError.AddError("Live End Time is required");
    //}
  },

  ProductionTypeIDSet: function () { },
  onProductionTypeIDSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {
      businessObject.ProductionTypeID(null)
      businessObject.ProductionType("")
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
    else {
      businessObject.ProductionTypeID(selectedItem.ProductionTypeID)
      businessObject.EventTypeID(null)
      businessObject.EventType("")
    }
  },
  triggerProductionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionTypeIDCriteriaBeforeRefresh: function (args) {
  },
  afterProductionTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  EventTypeIDSet: function () { },
  onEventTypeIDSelected: function (selectedItem, businessObject) {
    businessObject.EventTypeID(selectedItem.EventTypeID)
  },
  triggerEventTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setEventTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterEventTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  ProductionVenueIDSet: function () { },
  onProductionVenueIDSelected: function (selectedItem, businessObject) {
    //businessObject.EventTypeID(selectedItem.EventTypeID)
  },
  triggerProductionVenueIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setProductionVenueIDCriteriaBeforeRefresh: function (args) {
    //args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterProductionVenueIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  }
}
