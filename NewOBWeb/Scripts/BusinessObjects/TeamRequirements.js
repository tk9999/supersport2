﻿TeamRequirementDetailBO = {
  GetScheduledHoursForPeriod: function (self) {
    //var ThisYearGroupPeriod = ClientData.ROYearGroupPeriodList.Find('YearGroupPeriodID', self.YearGroupPeriodID());
    var EmployeeHours = [];
    var Hours = 0;
    var TotalHours = 0;
    var HRID = 0;
    var CurrentHR = ViewModel.CurrentTeamRequirementDetailHumanresource();
    var CurrentHRList = ViewModel.TeamRequirementDetailHumanresourceList();
    if (self.YearGroupPeriodID()) {
      //if (ThisYearGroupPeriod) {
      Singular.GetDataStateless('OBLib.Shifts.ICR.ReadOnly.ROHumanResourceGroupShiftList, OBLib', {
        HumanResourceID: null,
        StartDate: self.StartDate(),
        EndDate: self.EndDate(),
        SystemID: ViewModel.CurrentSystemID(),
        ProductionAreaID: ViewModel.CurrentProductionAreaID(),
        TeamID: self.GetParent().GetParent().TeamID()
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.HumanResourceGroupShiftList);
          Singular.HideLoadingBar();
          for (var i = 0; i < ViewModel.HumanResourceGroupShiftList().length; i++) {
            for (var n = 0; n < ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList().length; n++) {
              HRID = ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].HumanResourceID();
              var sd = new Date(ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].StartDateTime());
              var ed = new Date(ViewModel.HumanResourceGroupShiftList()[i].ROHumanResourceShiftList()[n].EndDateTime());
              var diff = ed - sd;
              var hh = Math.floor(diff / 1000 / 60 / 60);
              Hours += hh;
            }
            CurrentHR.HumanResourceID(HRID);
            CurrentHR.HoursWorked(Hours);
            CurrentHRList.push(CurrentHR);
            EmployeeHours.push([HRID, Hours]);
            TotalHours += Hours;
            Hours = 0;
          }
          self.TotalScheduledHoursWorked(TotalHours);
          for (var p = 0; p < EmployeeHours.length; p++) {
            ViewModel.genericlist.push(EmployeeHours[p]);
          }
          //ViewModel.genericlist(EmployeeHours);
        }
      });
    }
    else {
      self.TotalScheduledHoursWorked(0);
    }
    TotalHours = 0;
  }
  //CustomFormatter: function () {
  //    var f = new KOFormatterObject();
  //    f.IncludeChildren = true;
  //    f.IncludeClean = false;
  //    f.IncludeCleanInArray = false;
  //    f.IncludeCleanProperties = true;
  //    f.IncludeIsNew = true;
  //    return f;
  //}
}