﻿
ROUserNotificationBO = {
  toString: function (self) { return self.NotificationTitle() },
  //DataAccess
  get: function (options) {
    Singular.GetDataStateless("OBLib.Users.ReadOnly.ROUserNotificationList, OBLib", options.criteria, function (response) {
      if (response.Success) {
        if (options.onSuccess) { options.onSuccess.call(response.Data[0], response) }
      } else {
        if (options.onFail) { options.onFail(response) }
      }
    })
  }
};

ROUserNotificationListCriteriaBO = {
  StartDateSet: function (self) {
    this.refresh(self)
  },
  EndDateSet: function (self) {
    this.refresh(self)
  },
  refresh: function (self) {
    ViewModel.ROUserNotificationListManager().Refresh()
    //if (!self.IsProcessing()) {
    //  self.IsProcessing(true)
    //  ROUserNotificationBO.get({
    //    criteria: {
    //      UserID: ViewModel.CurrentUserID(), //we can get away with this kind of sin right here as this object is only used in a special case,
    //      StartDate: self.StartDate(), //we can get away with this kind of sin right here as this object is only used in a special case,
    //      EndDate: self.EndDate() //we can get away with this kind of sin right here as this object is only used in a special case
    //    },
    //    onSuccess: function (response) {
    //      ViewModel.ROUserNotificationList.Set(response.Data)
    //      self.IsProcessing(false)
    //    },
    //    onFail: function (response) {
    //      self.IsProcessing(false)
    //      OBMisc.Notifications.GritterError("Error", response.ErrorText, 500)
    //    }
    //  })
    //}
  }
};