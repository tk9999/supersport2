﻿AdHocBookingBOOld = {
  triggerAdHocBookingTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setAdHocBookingTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    //args.Data.StartDateTime = new Date(args.Object.OnAirTimeStart()).format("dd MMM yyyy HH:mm")
    //args.Data.EndDateTime = new Date(args.Object.OnAirTimeEnd()).format("dd MMM yyyy HH:mm")
  },
  afterAdHocBookingTypeIDRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onAdHocBookingTypeSelected: function (selectedItem, businessObject) {
    //businessObject.ResourceID(selectedItem.ResourceID)
    //RoomScheduleAreaBO.updateAll(businessObject)
    ////if (!selectedItem.IsAvailable) {
    ////  businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    ////} else {
    ////  businessObject.RoomClashCount(0)
    ////}
  },
  AdHocBookingTypeIDSet: function (self) {
    this.SetGenRefNo(self);
  },
  StartDateTimeSet: function (self) {
    self.AdHocBookingHumanResourceList().Iterate(function (Item, Index) {
      Item.StartDateTime(self.StartDateTime());
    });
  },
  EndDateTimeSet: function (self) {
    self.AdHocBookingHumanResourceList().Iterate(function (Item, Index) {
      Item.EndDateTime(self.EndDateTime());
    });
  },
  afterAttendeeSelected: function (SelectedHRGroup) {
    Singular.SendCommand("AttendeeSelected",
      {
        HumanResourceID: SelectedHRGroup.HR.HumanResourceID,
        HumanResource: SelectedHRGroup.HR.HRName,
        ResourceID: SelectedHRGroup.HR.ResourceID
      }, function (response) {
        ResourceSchedulerLive.RefreshAttendees();
      })
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    BookingHR = CtlError.Object;

    var AdhocBooking = BookingHR.GetParent();

    var AdhocBookingStartDate = new Date(AdhocBooking.StartDateTime());
    var AdhocBookingEndDate = new Date(AdhocBooking.EndDateTime());
    var BookingHRStartDate = new Date(BookingHR.StartDateTime());

    var res = (BookingHRStartDate.getTime() > AdhocBookingStartDate.getTime() && BookingHRStartDate.getTime() < AdhocBookingStartDate.getTime());
    if (res) {
      CtlError.AddError("AdHoc Booking Human Resource Start Date is not within the AdHoc Booking dates");
      //BookingHR.InvalidAdHocBookingHumanResourceStartDate("AdHoc Booking Human Resource Start Date is not within the AdHoc Booking dates");
    }
    else {
      //BookingHR.InvalidAdHocBookingHumanResourceStartDate("");
    }
  },
  EndDateTimeValid: function (Value, Rule, CtlError) {
    BookingHR = CtlError.Object;

    var AdhocBooking = BookingHR.GetParent();

    var AdhocBookingStartDate = new Date(AdhocBooking.StartDateTime());
    var AdhocBookingEndDate = new Date(AdhocBooking.EndDateTime());
    var BookingHREndDate = new Date(BookingHR.EndDateTime());

    var res = (BookingHREndDate.getTime() > AdhocBookingStartDate.getTime() && BookingHREndDate.getTime() < AdhocBookingStartDate.getTime());
    if (res) {
      CtlError.AddError("AdHoc Booking Human Resource Start Date is not within the AdHoc Booking dates");
      //BookingHR.InvalidAdHocBookingHumanResourceStartDate("AdHoc Booking Human Resource Start Date is not within the AdHoc Booking dates");
    }
    else {
      //BookingHR.InvalidAdHocBookingHumanResourceStartDate("");
    }
  },
  DescriptionValid: function (Value, Rule, CtlError) {
    var ahb = CtlError.Object;
    if (ahb.Description().trim().length == 0) {
      CtlError.AddError("Description is required")
    }
  },
  CheckRulesIndSet: function (AdhocBooking) {
    if (AdhocBooking.CheckRulesInd()) {
      Singular.SendCommand("CheckClashingAdHocBookingHumanResource",
                     {
                       AdHocBookingID: AdhocBooking.AdHocBookingID()
                     }, function (response) {
                       AdhocBooking.CheckRulesInd(false);
                     });
    }
  },
  ToString: function (AdHocBooking) {
    var ahbt = '',
        startDateString = '';
    if (AdHocBooking.AdHocBookingTypeID()) {
      var ROBookingType = ClientData.ROAdHocBookingTypeList.Find(AdHocBooking.AdHocBookingTypeID());
      if (ROBookingType) {
        ahbt = ROBookingType.AdHocBookingType;
      }
    };
    if (AdHocBooking.StartDateTime()) {
      startDateString = new Date(AdHocBooking.StartDateTime()).format('dd MMM HH:mm');
    };
    return (ahbt.length > 0 ? ahbt + ' - ' : '') + AdHocBooking.Title() + ' from ' + startDateString;
  },
  AddNewButtonEnabledICRShiftScreen: function () {
    if (ViewModel.AdHocBookingList().length == 0) {
      return true;
    }
    return false;
  },
  TitleSet: function (self) {
    //update Room Booking description
    AdHocBookingBOOld.UpdateParentDescription(self);
    //update HR Booking descriptions
    var descript = AdHocBookingBOOld.GetFullDescription(self);
    AdHocBookingBOOld.UpdateChildDescriptions(self, descript);
  },
  GetFullDescription: function (AdHocBooking) {
    var FullDescription = '';
    if (AdHocBooking.AdHocBookingTypeID()) {
      var ahbt = ClientData.ROAdHocBookingTypeList.Find('AdHocBookingTypeID', AdHocBooking.AdHocBookingTypeID());
      if (ahbt) {
        FullDescription += ahbt.AdHocBookingType;
      }
    };
    if (AdHocBooking.Title().trim().length > 0) {
      if (FullDescription.trim().length > 0) {
        FullDescription += ': ' + AdHocBooking.Title();
      }
    };
    return FullDescription;
  },
  DescriptionSet: function (self) {
    //update Room Booking description
    //AdHocBookingBO.UpdateParentDescription(self);
    //update HR Booking descriptions
    //var descript = AdHocBookingBO.GetFullDescription(self);
    //AdHocBookingBO.UpdateChildDescriptions(self, descript);
  },
  UpdateParentDescription: function (AdHocBooking) {
    var Parent = AdHocBooking.GetParent();
    if (Parent) {
      if (Parent.ObjectType && Parent.ObjectType() == "OBLib.Rooms.RoomSchedule, OBLib") {
        Parent.BookingDescription(AdHocBooking.Description());
        Parent.Title(AdHocBooking.Title());
      };
    }
  },
  UpdateChildDescriptions: function (AdHocBooking, newDescript) {
    AdHocBooking.AdHocBookingHumanResourceList().Iterate(function (Item, Index) {
      AdHocBookingHumanResourceBO.UpdateBookingDescription(Item, newDescript);
    });
  },
  GenRefDetails: {
    MinGenRefNumber: 473603,
    Production: 6
  },
  SetGenRefNo: function (self) {
    self.GenRefNo(null);
    var setting = ClientData.ROSystemAreaAdHocBookingTypeList.Find("AdHocBookingTypeID", self.AdHocBookingTypeID());
    if (setting) {
      if (setting.AdHocBookingTypeID == this.GenRefDetails.Production) {
        //self.DefaultGenRefNo(self.DefaultGenRefNo);
        self.GenRefNo(null);
      }
      else {
        self.GenRefNo(setting.DefaultGenRefNo);
      }
      self.SystemAreaAdHocBookingTypeID(setting.SystemAreaAdHocBookingTypeID);
    }
  },

  //other
  save: function (obj, onSaveSuccess, onSaveFailed) {
    ViewModel.CallServerMethod("SaveAdHocBookingListOld", {
      AdHocBooking: obj.Serialise()
    },
    function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 500)
        onSaveSuccess(response)
      }
      else {
        OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
        onSaveFailed(response)
      }
    })
  }
};

AdHocBookingHumanResourceBO = {
  triggerHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setHumanResourceCriteriaBeforeRefresh: function (args) {
    //args.Data.SystemID = args.Object.SystemID()
    //args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    //args.Data.StartDateTime = new Date(args.Object.OnAirTimeStart()).format("dd MMM yyyy HH:mm")
    //args.Data.EndDateTime = new Date(args.Object.OnAirTimeEnd()).format("dd MMM yyyy HH:mm")
  },
  afterHumanResourceRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onHumanResourceSelected: function (selectedItem, businessObject) {
    //businessObject.ResourceID(selectedItem.ResourceID)
    //RoomScheduleAreaBO.updateAll(businessObject)
    ////if (!selectedItem.IsAvailable) {
    ////  businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    ////} else {
    ////  businessObject.RoomClashCount(0)
    ////}
  },
  HumanResourceIDSet: function (AdHocBookingHumanResource) {
    //if (!AdHocBookingHumanResource.GetParent().CheckRulesInd()) {
    //  AdHocBookingHumanResource.GetParent().CheckRulesInd(true);
    //}
  },
  StartDateTimeSet: function (AdHocBookingHumanResource) {
    //if (!AdHocBookingHumanResource.GetParent().CheckRulesInd()) {
    //  AdHocBookingHumanResource.GetParent().CheckRulesInd(true);
    //}
  },
  EndDateTimeSet: function (AdHocBookingHumanResource) {
    //if (!AdHocBookingHumanResource.GetParent().CheckRulesInd()) {
    //  AdHocBookingHumanResource.GetParent().CheckRulesInd(true);
    //}
  },
  CheckClashingAdHocBookingHumanResource: function (Value, Rule, CtlError) {
    //var AdHocBookingHumanResource = CtlError.Object;
    //if (AdHocBookingHumanResource.EndDateTime() && AdHocBookingHumanResource.IsDirty()) {
    //  if (AdHocBookingHumanResource.GetParent().CheckRulesInd()) {
    //    Singular.SendCommand("CheckClashingAdHocBookingHumanResource",
    //             {
    //               AdHocBookingID: AdHocBookingHumanResource.GetParent().AdHocBookingID()
    //             }, function (response) {
    //             });
    //  }
    //}
  },
  ToString: function (AdHocBookingHumanResource) {
    return AdHocBookingHumanResource.HumanResource();
  },
  StartDateTimeValid: function (Value, Rule, CtlError) {
    var AdHocBookingHumanResource = CtlError.Object;
    var AdhocBooking = AdHocBookingHumanResource.GetParent();
    var AdhocBookingStartDate = new Date(AdhocBooking.StartDateTime());
    var AdhocBookingEndDate = new Date(AdhocBooking.EndDateTime());
    var BookingHRStartDate = new Date(AdHocBookingHumanResource.StartDateTime());
    var BookingHREndDate = new Date(AdHocBookingHumanResource.EndDateTime());

    //Check that the Attendee's time is between the AdHoc booking's start and end times
    var isAfterBookingStartTime = OBMisc.Dates.IsAfter(AdhocBookingStartDate, BookingHRStartDate);
    var isAfterBookingEndTime = OBMisc.Dates.IsAfter(BookingHREndDate, AdhocBookingEndDate);

    var res = (isAfterBookingStartTime || isAfterBookingEndTime);
    if (res) {
      CtlError.AddError("Start Time is not within the AdHoc Booking Times");
    }
    //Check that the Attendee's end time is after the start time
    var res2 = OBMisc.Dates.IsAfter(BookingHRStartDate, BookingHREndDate);
    if (res2) {
      CtlError.AddError('Start Time must be before End Time');
    }

  },
  TeamIDValid: function (Value, Rule, CtlError) {
    //BookingHR = CtlError.Object;
    //var SysID = null;
    //if (typeof ViewModel.CurrentUserSystemID == 'undefined') {
    //  SysID = ViewModel.UserSystemID();
    //} else {
    //  SysID = ViewModel.CurrentUserSystemID();
    //};
    ////var SysID = (ViewModel.CurrentUserSystemID() ? ViewModel.CurrentUserSystemID() : ViewModel.UserSystemID());
    //if (SysID == 4) {
    //  if (!BookingHR.SystemTeamID()) {
    //    CtlError.AddError("Team is required");
    //  }
    //}
  },
  HasClashesValid: function (Value, Rule, CtlError) {
    AdHocBookingHumanResource = CtlError.Object;
    if (AdHocBookingHumanResource.ClashList().length > 0) {
      var err = AdHocBookingHumanResource.HumanResource().toString() + " has " + AdHocBookingHumanResource.ClashList().length.toString() + " clashes with this booking";
      CtlError.AddError(err);
    }
  },
  RemoveAttendee: function (AdHocBookingHumanResource) {
    //Singular.SendCommand("RemoveAttendee", {
    //  GuidToRemove: AdHocBookingHumanResource.Guid().toString()
    //}, function (response) {

    //});
  },
  AttendeeAlreadyAdded: function (Value, Rule, CtlError) {
    var AdHocBookingHumanResource = CtlError.Object;
    var AdHocBooking = AdHocBookingHumanResource.GetParent();
    if (AdHocBooking) {
      var OtherBookings = AdHocBooking.AdHocBookingHumanResourceList().Filter("HumanResourceID", AdHocBookingHumanResource.HumanResourceID());
      var bookingCount = 0
      OtherBookings.Iterate(function (ahbhr, indx) {
        if (ahbhr.Guid() != AdHocBookingHumanResource.Guid()) {
          bookingCount += 1;
        }
      });
    };
    if (bookingCount > 0) {
      CtlError.AddWarning(AdHocBookingHumanResource.HumanResource() + " is already added to this booking");
    };
  },
  UpdateBookingDescription: function (AdHocBookingHumanResource, newDescription) {
    AdHocBookingHumanResource.ResourceBookingDescription(newDescription);
  }
};

AdHocBookingBO = {
  //dropdowns
  triggerAdHocBookingTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAdHocBookingTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterAdHocBookingTypeIDRefreshAjax: function (args) {

  },
  onAdHocBookingTypeIDSelected: function (item, businessObject) {
    businessObject.GenRefNo(item.DefaultGenRefNo);
    //if (item.AdHocBookingTypeID == 6) {
    //  businessObject.GenRefNo(null)
    //} else {
      
    //}
  },

  triggerUserSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setUserSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.UserID = ViewModel.CurrentUserID()
  },
  afterUserSystemAreaRefreshAjax: function (args) {

  },
  onUserSystemAreaSelected: function (item, businessObject) {
    if (item) {
      businessObject.SystemID(item.SystemID)
      businessObject.ProductionAreaID(item.ProductionAreaID)
    }
    else {
      businessObject.SystemID(null)
      businessObject.ProductionAreaID(null)
    }
    businessObject.AdHocBookingTypeID(null)
    businessObject.AdHocBookingType("")
  },

  triggerCountryIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setCountryIDCriteriaBeforeRefresh: function (args) {
  },
  afterCountryIDRefreshAjax: function (args) {

  },
  onCountryIDSelected: function (item, businessObject) {

  },

  //set
  AdHocBookingTypeIDSet: function (self) {
    
  },
  StartDateTimeSet: function (self) {
    //if (self.IsNew()) {
    self.StartDateTimeBuffer(self.StartDateTime())
    //}
  },
  EndDateTimeSet: function (self) {
    //if (self.IsNew()) {
    self.EndDateTimeBuffer(self.EndDateTime())
    //}
  },
  TitleSet: function (self) {

  },
  DescriptionSet: function (self) {
    //update Room Booking description
    //AdHocBookingBO.UpdateParentDescription(self);
    //update HR Booking descriptions
    //var descript = AdHocBookingBO.GetFullDescription(self);
    //AdHocBookingBO.UpdateChildDescriptions(self, descript);
  },
  SystemIDSet: function (self) {
    //this.SetGenRefNo(self);
  },
  ProductionAreaIDSet: function (self) {
    //this.SetGenRefNo(self);
  },

  //cans
  canEdit: function (fieldName, adhocBooking) {
    switch (fieldName) {
      case "GenRefNo":
        return false;
        break;
      case "UsingDefaultGenRefReason":
        return (adhocBooking.AdHocBookingTypeID() == 6 && adhocBooking.GenRefNo() == 100006)
        break;
      case "GenRefNoSearch":
        return (adhocBooking.AdHocBookingTypeID() == 6)
        break;
      default:
        return true;
        break;
    }
  },
  canView: function (fieldName, adhocBooking) {
    switch (fieldName) {
      case "GenRefNo":
      case "UsingDefaultGenRefReason":
        return (adhocBooking.AdHocBookingTypeID() != 6)
        break;
      case "GenRefNoSearch":
        return (adhocBooking.AdHocBookingTypeID() == 6)
        break;
      case "GenRefNoSearchModal":
        return adhocBooking.ShowFindSynergyEvent()
        break;
      default:
        return true;
        break;
    }
  },
  css: function (fieldName, adhocBooking) {
    switch (fieldName) {
      case "AdHocBookingDetails":
        if (adhocBooking.ShowFindSynergyEvent()) {
          return "animated slideOutUp fast go"
        } else {
          return "animated slideInDown fast go"
        }
        break;
      case "GenRefNoSearchModal":
        if (adhocBooking.ShowFindSynergyEvent()) {
          return "animated slideInUp fast go"
        } else {
          return "animated slideOutDown fast go"
        }
        break;
      default:
        return true;
        break;
    }
  },

  //rules
  DatesValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.StartDateTimeBuffer() || !CtlError.Object.StartDateTime()
      || !CtlError.Object.EndDateTime() || !CtlError.Object.EndDateTimeBuffer()) {
      CtlError.AddError("Please fill in all dates")
    } else {
      var ct = CtlError.Object.StartDateTimeBuffer();
      var st = CtlError.Object.StartDateTime();
      var et = CtlError.Object.EndDateTime();
      var wt = CtlError.Object.EndDateTimeBuffer();
      if (ct && st) {
        if (OBMisc.Dates.IsAfter(ct, st)) {
          CtlError.AddError("Call Time must be before Start Time");
        }
      }
      if (st && et) {
        if (OBMisc.Dates.IsAfter(ct, et)) {
          CtlError.AddError("Start Time must be before End Time");
        }
      }
      if (et && wt) {
        if (OBMisc.Dates.IsAfter(ct, wt)) {
          CtlError.AddError("End Time must be before Wrap Time");
        }
      }
    }
  },
  GenRefNoValid: function (Value, Rule, CtlError) {

    if (CtlError.Object.AdHocBookingTypeID() == 6 && CtlError.Object.GenRefNo() == null) {
      CtlError.AddError("The Gen Ref for the production is required")
    }

    if (CtlError.Object.AdHocBookingTypeID() == 6 && CtlError.Object.GenRefNo() == 100006 && CtlError.Object.UsingDefaultGenRefReason().trim().length == 0) {
      CtlError.AddError("Reason for using the default genref number is required")
    }

  },

  //other
  AdHocBookingBOToString: function (AdHocBooking) {
    var ahbt = '',
      startDateString = '';
    if (AdHocBooking.AdHocBookingTypeID()) {
      var ROBookingType = ClientData.ROAdHocBookingTypeList.Find(AdHocBooking.AdHocBookingTypeID());
      if (ROBookingType) {
        ahbt = ROBookingType.AdHocBookingType;
      }
    };
    if (AdHocBooking.StartDateTime()) {
      startDateString = new Date(AdHocBooking.StartDateTime()).format('dd MMM HH:mm');
    };
    return (ahbt.length > 0 ? ahbt + ' - ' : '') + AdHocBooking.Title() + ' from ' + startDateString;
  },
  GetFullDescription: function (AdHocBooking) {
    var FullDescription = '';
    if (AdHocBooking.AdHocBookingTypeID()) {
      var ahbt = ClientData.ROAdHocBookingTypeList.Find('AdHocBookingTypeID', AdHocBooking.AdHocBookingTypeID());
      if (ahbt) {
        FullDescription += ahbt.AdHocBookingType;
      }
    };
    if (AdHocBooking.Title().trim().length > 0) {
      if (FullDescription.trim().length > 0) {
        FullDescription += ': ' + AdHocBooking.Title();
      }
    };
    return FullDescription;
  },
  GenRefDetails: {
    MinGenRefNumber: 473603,
      Production: 6
  },
  SetGenRefNo: function (self) {
    //self.GenRefNo(null);
    //var settings = ClientData.ROSystemAreaAdHocBookingTypeList.Filter("AdHocBookingTypeID", self.AdHocBookingTypeID())
    //settings = settings.Filter("SystemID", self.SystemID())
    //settings = settings.Filter("ProductionAreaID", self.ProductionAreaID())
    //if (settings.length == 1) {
    //  var setting = settings[0]
    //  if (setting.AdHocBookingTypeID == this.GenRefDetails.Production) {
    //    self.GenRefNo(null);
    //  }
    //  else {
    //    self.GenRefNo(setting.DefaultGenRefNo);
    //  }
    //}
  },
  ROAdHocBookingTypeOptions: function (ROSystemAreaAdHocBookingTypeList, self) {
    var result = []
    if (self.SystemID() && self.ProductionAreaID()) {
      ROSystemAreaAdHocBookingTypeList.Iterate(function (itm, indx) {
        if (itm.SystemID == self.SystemID() && itm.ProductionAreaID == self.ProductionAreaID()) {
          result.push(itm)
        }
      })
    }
    return result
  },
  GetUserSystems: function (ROSystemList, self) {
    var results = []
    ROSystemList.Iterate(function (roSys, indx) {
      var itm = ClientData.ROUserSystemList.Find("SystemID", roSys.SystemID)
      if (itm) { results.push(roSys) }
    })
    return results
  },
  dialogTitle: function (adHocBooking) {
    if (adHocBooking) {
      if (adHocBooking.IsNew() && adHocBooking.Title().trim().length == 0) {
        return "New AdHoc Booking"
      }
      return adHocBooking.Title()
    }
    return "Blank AdHoc Booking"
  },
  findSynergyEvent: function (adHocBooking) {
    adHocBooking.ShowFindSynergyEvent(!adHocBooking.ShowFindSynergyEvent());
  },

  setupAsTraining: function (self, startTime, roomBookingArea) {
    self.AdHocBookingTypeID(85)
    self.Title("Carte Blanche")
    //self.ProductionType("News")
    //self.EventTypeID(1184)
    //self.EventType("Carte Blanche")
    //self.TeamsPlaying("N/A")
    var ct = new Date(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), 16, 15)
    var sd = new Date(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), 16, 15)
    var ed = new Date(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), 20, 0)
    var wt = new Date(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), 20, 00)
    self.PlayStartDateTime(sd)
    self.PlayEndDateTime(ed)
    if (roomBookingArea) {
      roomBookingArea.Title(self.Title())
      roomBookingArea.CallTime(ct)
      roomBookingArea.OnAirTimeStart(sd)
      roomBookingArea.OnAirTimeEnd(ed)
      roomBookingArea.WrapTime(wt)
      roomBookingArea.ResourceBookingDescription(self.Title())
    }
  },
  getAdHocBooking: function (adHocBookingID, callback) {
    Singular.GetDataStateless("OBLib.AdHoc.AdHocBookingList",
      {
        AdHocBookingID: adHocBookingID,
        AdHocBookingOnly: true
      },
      function (response) {
        if (response.Success) {
          if (callback) { callback(response) }
        }
        else {
          OBMisc.Notifications.GritterError('Error Processing', response.ErrorText, 1000)
        }
      })
  },
   
  getROAdHocBookingList: function (options) {
    Singular.GetDataStateless("OBLib.AdHoc.AdHocBookingList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          OBMisc.Notifications.GritterError("Fetch Booking Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  save: function (adHocBooking, options) {
    ViewModel.CallServerMethod("SaveAdHocBooking",
      { AdHocBooking: adHocBooking.Serialise() },
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Save Booking Failed", response.ErrorText, 1000)
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  getPromise(criteria) {
    return Singular.GetDataStatelessPromise("OBLib.AdHoc.AdHocBookingList, OBLib", criteria);
  }
};

//here to make the set expressions happy
ROAdHocBookingCriteriaBO = {
  StartDateSet: function (self) {

  },
  EndDateSet: function (self) {

  },
  TitleSet: function (self) {

  },
  CreatedBySet: function (self) {

  },
  RefNoSet: function (self) {

  }
};