﻿//#region AdHocBulkAccommodationGuestBO
AdHocBulkAccommodationGuestBO = {
  CanEdit: function (Guest, FieldName) {
    switch (FieldName) {
      case 'BookedButtonEnable':
        if (!Guest.CancelledInd() && ViewModel.CurrentAccommodation() && !ViewModel.CurrentAccommodation().CancelledInd()) {
          return true;
        } else {
          return false;
        }
        break;
      case 'BookedButtonVisible':
        if (Guest.ROAdHocBulkAccommodationGuestClashList().length == 0 && !Guest.CancelledInd()) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return true;
        break;
    }
  },
  CancelledReasonRequired: function (Value, Rule, CtlError) {
    var CurrentBulkAccommodationGuest = CtlError.Object;
    if (CurrentBulkAccommodationGuest.CancelledInd() && CurrentBulkAccommodationGuest.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkAccommodationGuest.HumanResource());
    }
  },
  UpdateCancelledReason: function (self) {
    var bag = self;
    var ahr = ViewModel.CurrentAccommodation().AccommodationHumanResourceList().Find("AccommodationHumanResourceID", bag.AccommodationHumanResourceID());
    if (ahr) {
      ahr.CancelledReason(bag.CancelledReason());
    }
  },
  ClearCancelledReason: function (self) {
    if (self.CancelledInd() == false) {
      self.CancelledReason("");
    }
  },
  AdHocBulkAccommodationGuestBOToString: function (self) {
    var HumanResource = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID());
    var HR = (HumanResource == undefined) ? '' : ' - Human Resource: ' + HumanResource.HumanResource;
    return 'Ad Hoc Bulk Accommodation Guest' + HR;
  }
}
//#endregion
//#region AdHocBulkChauffeurDriverPassengerBO
AdHocBulkChauffeurDriverPassengerBO = {
  HasMainPassenger: function (Value, Rule, CtlError) {
    var passenger = CtlError.Object;
    var bulklist = ViewModel.BulkChauffeurDriverPassengerList();
    var AtLeastOneOnCar = false;
    var MainPassengerCount = 0;
    var MainPassengerCancelled = false;
    for (var n = 0; n < bulklist.length; n++) {
      if (bulklist[n].OnChauffeurCarInd() & !bulklist[n].CancelledInd()) {
        AtLeastOneOnCar = true;
      }
      if (bulklist[n].MainPassengerInd()) {
        MainPassengerCount++;
        if (bulklist[n].CancelledInd()) {
          MainPassengerCancelled = true;
          MainPassengerCount--;
        }
      }
    }
    if (AtLeastOneOnCar & MainPassengerCount == 0)
      CtlError.AddError("Main Passenger Required");
  },
  CancelledReasonRequired: function (Value, Rule, CtlError) {
    var CurrentBulkChauffeurDriverPassenger = CtlError.Object;
    if (CurrentBulkChauffeurDriverPassenger.CancelledInd() && CurrentBulkChauffeurDriverPassenger.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkChauffeurDriverPassenger.HumanResource());
    }
  },
  UpdateCancelledReason: function (self) {
    var brcp = self;
    var rchr = ViewModel.CurrentChauffeur().ChauffeurDriverHumanResourceList().Find("ChauffeurDriverHumanResourceID", brcp.ChauffeurDriverHumanResourceID());
    rchr.CancelledReason(brcp.CancelledReason());
  },
  ClearCancelledReason: function (self) {
    if (self.CancelledInd() == false) {
      self.CancelledReason("");
    }
  },
  AdHocBulkChauffeurDriverPassengerBOToString: function (self) {
    var HumanResource = (self.HumanResource() == undefined) ? '' : ' - Human Resource: ' + self.HumanResource();
    var MainPassenger = (self.MainPassengerInd() == undefined) ? '' : ' - Main Passenger: ' + self.MainPassengerInd();
    return 'Ad Hoc Bulk Chauffeur Driver Passenger' + HumanResource + MainPassenger;
  }
}
//#endregion
//#region AdHocBulkFlightPassengerBO
AdHocBulkFlightPassengerBO = {
  FlightClassRequired: function (Value, Element, Args) {
    var CurrentBulkFlightPassenger = CtlError.Object;
    if (CurrentBulkFlightPassenger.OnFlightInd() && CurrentBulkFlightPassenger.FlightClassID() == null) {
      CtlError.AddError("Flight Class Required");
    }
  },
  CancelledReasonRequired: function (Value, Element, CtlError) {
    var CurrentBulkFlightPassenger = CtlError.Object;
    if (CurrentBulkFlightPassenger.CancelledInd() && CurrentBulkFlightPassenger.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkFlightPassenger.HumanResource());
    }
  },
  SetFlightClass: function (self) {
    if (self.OnFlightInd() == true) {
      var FlightClass = ClientData.ROFlightClassList.Find('FlightClassID', 1);
      self.FlightClassID(FlightClass.FlightClassID);
    }
    else {
      self.FlightClassID(null);
    }
  },
  UpdateCancelledReason: function (self) {
    var bfp = self;
    var fhr = ViewModel.BulkFlightPassengerList().Find("FlightHumanResourceID", bfp.FlightHumanResourceID());
    if (fhr != undefined) {
      fhr.CancelledReason(bfp.CancelledReason());
    }
  },
  AdHocBulkFlightPassengerBOToString: function (self) {
    var HumanResource = (self.HumanResource() == undefined) ? '' : ' - Human Resource: ' + self.HumanResource();
    return 'Ad Hoc Bulk Flight Passenger' + HumanResource;
  }
}
//#endregion
//#region AdHocBulkRentalCarPassengerBO
AdHocBulkRentalCarPassengerBO = {
  CanEdit: function (RentalCar, FieldName) {
    switch (FieldName) {
      case 'OnCarEnable':
        if (ViewModel.IsValid() && !RentalCar.CancelledInd()) {
          return true;
        } else {
          return false;
        }
        break;
      case 'OnCarVisible':
        if (RentalCar.ROAdHocBulkRentalCarPassengerClashList().length == 0) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return true;
        break;
    }
  },
  DriverOnCar: function (Value, Rule, CtlError) {
    var passenger = CtlError.Object;
    var checkOnce = false;
    var bulklist = ViewModel.BulkRentalCarPassengerList();
    var AtLeastOneOnCar = false;
    var DriverCount = 0;
    var DriverCancelled = false;
    for (var n = 0; n < bulklist.length; n++) {
      if (bulklist[n].OnRentalCarInd()) {
        if (!bulklist[n].CancelledInd()) {
          AtLeastOneOnCar = true;
        }
      }
      //if (bulklist[n].OnRentalCarInd() & !bulklist[n].CancelledInd()) {
      //  AtLeastOneOnCar = true;
      //}
      if (bulklist[n].DriverInd()) {
        DriverCount++;
        if (bulklist[n].CancelledInd()) {
          DriverCancelled = true;
          DriverCount--;
        }
      }
    }
    if (AtLeastOneOnCar) {
      if (!checkOnce) {
        if (DriverCount == 0) {
          checkOnce = true;
          CtlError.AddError("Driver Required");
        }
        else if (DriverCount > 1) {
          checkOnce = true;
          CtlError.AddError("Only One Driver Required");
        }
      }
    }
    //if (AtLeastOneOnCar & DriverCount == 0 & !checkOnce ) {
    //  checkOnce = true;
    //  CtlError.AddError("Driver Required");
    //}
    //else if (AtLeastOneOnCar & DriverCount > 1 & !checkOnce) {
    //  checkOnce = true;
    //  CtlError.AddError("Only One Driver Required");
    //}
  },
  CancelledReasonRequired: function (Value, Element, CtlError) {
    var CurrentBulkRentalCarPassenger = CtlError.Object;
    if (CurrentBulkRentalCarPassenger.CancelledInd() && CurrentBulkRentalCarPassenger.CancelledReason().trim() == "") {
      CtlError.AddError("Cancelled Reason Required for " + CurrentBulkRentalCarPassenger.HumanResource());
    }
  },
  UpdateCancelledReason: function (self) {
    var brcp = self;
    var rchr = ViewModel.CurrentRentalCar().RentalCarHumanResourceList().Find("RentalCarHumanResourceID", brcp.RentalCarHumanResourceID());
    if (rchr != undefined) {
      rchr.CancelledReason(brcp.CancelledReason());
    }
  },
  ClearCancelledReason: function (self) {
    if (self.CancelledInd() == false) {
      self.CancelledReason("");
    }
  },
  UpdateDriverInd: function (self) {
    var brcp = self;
    var rchr = ViewModel.CurrentRentalCar().RentalCarHumanResourceList().Find("RentalCarHumanResourceID", brcp.RentalCarHumanResourceID());
    if (rchr == undefined) {
      var len = ViewModel.CurrentRentalCar().RentalCarHumanResourceList().length;
      var rchrList = ViewModel.CurrentRentalCar().RentalCarHumanResourceList();
      for (var i = 0; i < len; i++) {
        if (rchrList[i].CancelledInd() == false && rchrList[i].HumanResourceID() == brcp.HumanResourceID()) {
          rchr = rchrList[i];
        } //if
      } //for
      //if (brcp.DriverInd()) {
      //  TravelVM.CurrentRentalCar().Driver(brcp.HumanResource())
      //} else {
      //  TravelVM.CurrentRentalCar().Driver('');
      //}
    } //if
    if (rchr != undefined) {
      rchr.DriverInd(brcp.DriverInd());
      rchr.CoDriverInd(brcp.CoDriverInd());
      //DriverValid();
    }
  },
  AdHocBulkRentalCarPassengerBOToString: function (self) {
    var HumanResource = (self.HumanResource() == undefined) ? '' : ' - Passenger: ' + self.HumanResource();
    var Driver = (self.DriverInd() == undefined) ? '' : ' - Driver: ' + self.DriverInd();
    var CoDriver = (self.CoDriverInd() == undefined) ? '' : ' - Co-Driver: ' + self.CoDriverInd();
    return 'Ad Hoc Bulk Rental Car Passenger' + HumanResource + Driver + CoDriver;
  }
}
//#endregion
//#region SnTDetailBO
SnTDetailBO = {
  PolicyDetails: PolicyDetails = {
    Other_Countries_Domestic: 12,
    Currency_USDollar: 16,
    All_other_countries_International: 5,
    South_African_Booking: 1
  },
  TotalAmountValid: function (Value, Rule, CtlError) {
    var ahsd = CtlError.Object;
    if (ahsd.CalculatedPolicy() != undefined) {
      var ROGroupSnT;
      ROGroupSnT = ClientData.ROGroupSnTList.Find("GroupSnTID", ahsd.GroupSnTID());
      if (ROGroupSnT.InternationalInd == false) {
        var Total = ahsd.BreakfastAmount() + ahsd.LunchAmount() + ahsd.DinnerAmount() + ahsd.IncidentalAmount();
        if (Total > ROGroupSnT.TotalAmount && ahsd.ChangedReason().trim() == "") {
          CtlError.AddError("Total S&T is greater than the allowed maximum, please specify a reason why");
        }
      }
    }
  },
  UpdateFields: function (self) {
    if (self.GroupSnTID()) {
      var BookingCountryID = ViewModel.TravelRequisition().CountryID(); // Country where the booking is to take place
      var ROGroupSnT;
      ROGroupSnT = ClientData.ROGroupSnTList.Find("GroupSnTID", self.GroupSnTID());
      if (self.GroupSnTID() == 13 || self.GroupSnTID() == 17) {
        var d1 = new Date(self.SnTDay());
        var d2 = new Date(ROGroupSnT.EffectiveDate);
        if (d1 >= d2) {
          self.GroupSnTID(17);
        } else {
          self.GroupSnTID(13);
        }
      } else {
        self.GroupSnTID(ROGroupSnT.GroupSnTID);
      }
      self.InternationalInd(ROGroupSnT.InternationalInd);
      self.CurrencyID(ROGroupSnT.CurrencyID);
      if (ROGroupSnT.DestinationCountryID == 0) {
        self.DestinationCountryID(BookingCountryID);
      }
    }
  },
  UpdateMealAmounts: function (self) {
  },
  CanEdit: function (GroupHumanResourceSnT, FieldName) {
    switch (FieldName) {
      case 'BulkEditVisible':
        var AtLeastOneDetail = false;
        ViewModel.TravelRequisition().TravellerList().Iterate(function (itm, ind) {
          if (itm.SnTDetailList().length > 0) {
            AtLeastOneDetail = true;
          }
        });
        if (ViewModel.TravelRequisition().TravellerList().length > 0 && AtLeastOneDetail) {
          return true;
        } else {
          return false;
        }
        break;
      case 'GenerateButtonEnabled':
        var TravellerSelected = false;
        var SnTDaySelected = false;
        ViewModel.TravelRequisition().TravellerList().Iterate(function (itm, ind) {
          if (itm.SnTSelectInd()) {
            TravellerSelected = true;
          }
        });
        ViewModel.SnTDayList().Iterate(function (itm, ind) {
          if (itm.SelectedInd()) {
            SnTDaySelected = true;
          }
        });
        if (ViewModel.BulkGroupPolicyDetailList()[0]) {
          if (ViewModel.IsValid() &&
            (ViewModel.BulkGroupPolicyDetailList()[0].BreakfastInd() ||
              ViewModel.BulkGroupPolicyDetailList()[0].LunchInd() ||
              ViewModel.BulkGroupPolicyDetailList()[0].DinnerInd() ||
              ViewModel.BulkGroupPolicyDetailList()[0].IncidentalInd()) &&
            TravellerSelected && SnTDaySelected) {
            return true;
          } else {
            return false;
          }
        }
        break;
      case 'BreakfastVisible':
        if (((GroupHumanResourceSnT.BreakfastProductionAreaID() != undefined) &&
          (GroupHumanResourceSnT.BreakfastAdHocBookingID() != ViewModel.TravelRequisition().AdHocBookingID() ||
            !(GroupHumanResourceSnT.BreakfastProductionAreaID() === ViewModel.CurrentProductionAreaID())))) {
          return true;
        } else {
          return false;
        }
        break;
      case 'LunchVisible':
        if (((GroupHumanResourceSnT.LunchProductionAreaID() != undefined) &&
          (GroupHumanResourceSnT.LunchAdHocBookingID() != ViewModel.TravelRequisition().AdHocBookingID() ||
            !(GroupHumanResourceSnT.LunchProductionAreaID() === ViewModel.CurrentProductionAreaID())))) {
          return true;
        } else {
          return false;
        }
        break;
      case 'DinnerVisible':
        if (((GroupHumanResourceSnT.DinnerProductionAreaID() != undefined) &&
          (GroupHumanResourceSnT.DinnerAdHocBookingID() != ViewModel.TravelRequisition().AdHocBookingID() ||
            !(GroupHumanResourceSnT.DinnerProductionAreaID() === ViewModel.CurrentProductionAreaID())))) {
          return true;
        } else {
          return false;
        }
        break;
      case 'IncidentalVisible':
        if (((GroupHumanResourceSnT.IncidentalProductionAreaID() != undefined) &&
          (GroupHumanResourceSnT.IncidentalAdHocBookingID() != ViewModel.TravelRequisition().AdHocBookingID() ||
            !(GroupHumanResourceSnT.IncidentalProductionAreaID() === ViewModel.CurrentProductionAreaID())))) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return true;
        break;
    }
  },
  SnTDetailBOToString: function (self) {
    var HumanResource = ClientData.ROHumanResourceList.Find('HumanResourceID', self.HumanResourceID);
    var HR = (HumanResource == undefined) ? '' : ' - Human Resource: ' + HumanResource;
    var SnTDay = (self.SnTDay() == undefined) ? '' : ' - S&T Day: ' + self.SnTDay();
    var Breakfast = (self.BreakfastAdHocBookingID() == undefined) ? 'No' : 'Yes';
    var Lunch = (self.LunchAdHocBookingID() == undefined) ? 'No' : 'Yes';
    var Dinner = (self.DinnerAdHocBookingID() == undefined) ? 'No' : 'Yes';
    var Incidental = (self.IncidentalAdHocBookingID() == undefined) ? 'No' : 'Yes';
    return 'Ad Hoc S&T Detail' + HR + SnTDay + ' - ' +
      'Breakfast: ' + Breakfast + ' - ' +
      'Lunch: ' + Lunch + ' - ' +
      'Dinner: ' + Dinner + ' - ' +
      'Incidental: ' + Incidental;
  }
}
//#endregion

TravellerBO = {
  DoesTravellerExist: function (Value, Rule, CtlError) {
    var Traveller = CtlError.Object;
    if (self) {
      if (Traveller && Traveller.HumanResourceID() != null) {
        var count = 0;
        if (ViewModel.TravelRequisition() != null) {
          ViewModel.TravelRequisition().TravellerList().Iterate(function (trvlr, trIndex) {
            if (trvlr.HumanResourceID()) {
              var Duplicates = ViewModel.TravelRequisition().TravellerList().Filter('HumanResourceID', trvlr.HumanResourceID());
              if (Duplicates.length > 0) {
                Duplicates.Iterate(function (dupe, dupeIndex) {
                  if (dupe.Guid() != trvlr.Guid()) {
                    CtlError.AddError('A Traveller with the name ' + trvlr.HumanResource() + ' already exists');
                  }
                });
              }
            }
          });
        }
      }
    }
  },
  HumanResourceIDSet: function (self) {
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.TravelRequisitionID = args.Object.GetParent().TravelRequisitionID()
    args.Data.PageSize = 1000
  },
  onHumanResourceSelected: function (selectedItem, businessObject) {
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  afterHumanResourceRefreshAjax: function () {
  },
  //GetTravellerDropDown: function (self) {
  //  if (ViewModel.DropDownTravellers().length == 0) {
  //    Singular.GetDataStateless('OBLib.Travel.Travellers.ReadOnly.ROTravellerPagedList, OBLib', {
  //      PageSize: 1000,
  //      TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID()
  //    }, function (args) {
  //      if (args.Success) {
  //        KOFormatter.Deserialise(args.Data, ViewModel.DropDownTravellers);
  //      }
  //      Singular.HideLoadingBar();
  //    });
  //  }
  //  return ViewModel.DropDownTravellers;
  //},
  CanEdit: function (object, field) {
    switch (field) {
      case 'enableSave':
        if (ViewModel.IsValid()) {
          return true
        } else {
          return false
        }
        break
      case 'removeItemVisible':
        if (object.IsNew()) {
          return true
        } else {
          return false
        }
        break
      default:
        return true
    }
  },
  TravellerBOToString: function (self) {
    var HumanResource = (self.HumanResource() == undefined) ? '' : ' - Human Resource: ' + self.HumanResource();
    var StartDate = (self.StartDateTime() == undefined) ? '' : ' - Start Date: ' + self.StartDateTime().format('dd-MMM-yy');
    var EndDate = (self.EndDateTime() == undefined) ? '' : ' - End Date: ' + self.EndDateTime().format('dd-MMM-yy');
    return 'Traveller' + HumanResource + StartDate + EndDate;
  }
}

AdHocTravelBookingBO = {
  GenRefDetails: {
    MinGenRefNumber: 473603,
    Production: 6
  },

  //dropdowns
  triggerAdHocBookingTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setAdHocBookingTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterAdHocBookingTypeIDRefreshAjax: function (args) {

  },
  onAdHocBookingTypeIDSelected: function (item, businessObject) {
    if (item) {
      businessObject.GenRefNo(item.DefaultGenRefNo)
    } else {
      businessObject.GenRefNo(null)
    }
  },

  triggerUserSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setUserSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.UserID = ViewModel.CurrentUserID()
    //args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterUserSystemAreaRefreshAjax: function (args) {

  },
  onUserSystemAreaSelected: function (item, businessObject) {
    if (item) {
      businessObject.SystemID(item.SystemID)
      businessObject.ProductionAreaID(item.ProductionAreaID)
      businessObject.GenRefNo(item.DefaultGenRefNo)
    } else {
      businessObject.SystemID(null)
      businessObject.ProductionAreaID(null)
      businessObject.GenRefNo(null)
    }
    //if (item.AdHocBookingTypeID == this.GenRefDetails.Production) {
    //  businessObject.GenRefNo(null)
    //} else {
    //  businessObject.GenRefNo(item.DefaultGenRefNo)
    //}
  },

  triggerCountryIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setCountryIDCriteriaBeforeRefresh: function (args) {
  },
  afterCountryIDRefreshAjax: function (args) {

  },
  onCountryIDSelected: function (item, businessObject) {

  },

  //rules
  StartBeforeEndValid: function (Value, Rule, CtlError) {
    var Booking = CtlError.Object;
    if (Booking.EndDateTime() != undefined && Booking.StartDateTime() != undefined) {
      var d1 = new Date(Booking.StartDateTime());
      var d2 = new Date(Booking.EndDateTime());
      if (d2.getTime() < d1.getTime()) {
        CtlError.AddError("Start date must be before end date");
      }
    }
  },
  GenRefNoValid: function (Value, Rule, CtlError) {
    var Booking = CtlError.Object;
    if (Booking.GenRefNo() != undefined) {
      if (Booking.GenRefNo() < AdHocTravelBookingBO.GenRefDetails.MinGenRefNumber) {
        if (Booking.DefaultGenRefNo()) {
          if (Booking.AdHocBookingTypeID() == AdHocTravelBookingBO.GenRefDetails.Production && Booking.GenRefNo() != Booking.DefaultGenRefNo()) {
            CtlError.AddError("Gen Ref No cannot be less than " + AdHocTravelBookingBO.GenRefDetails.MinGenRefNumber);
          }
          //else if (Booking.AdHocBookingTypeID() == GenRefDetails.Production && Booking.GenRefNo() == Booking.DefaultGenRefNo()) {
          //  CtlError.AddWarning("Default Gen Ref Number being used for production");
          //}
        }
      }
    }
  },
  BookingTypeValid: function (Value, Rule, CtlError) {
    var Booking = CtlError.Object;
    if (Booking.AdHocBookingTypeID() == AdHocTravelBookingBO.GenRefDetails.Production && Booking.GenRefNo() == undefined) {
      CtlError.AddError("Valid Gen Ref No Required if Production Travel Type");
    }
  },
  ReasonForDefaultValid: function (Value, Rule, CtlError) {
    var Booking = CtlError.Object;
    var BookingType = ClientData.ROSystemAreaAdHocBookingTypeList.Find("AdHocBookingTypeID", Booking.AdHocBookingTypeID());
    if (BookingType) {
      if (Booking.GenRefNo() && Booking.AdHocBookingTypeID() == AdHocTravelBookingBO.GenRefDetails.Production && BookingType.ProductionAreaID != 7) {
        if (Booking.DefaultGenRefNo() == Booking.GenRefNo() && (Booking.UsingDefaultGenRefReason() == null || Booking.UsingDefaultGenRefReason().trim() == "")) {
          CtlError.AddError("Enter a reason for using the default Gen Ref number");
        }
      }
    }
  },

  //set
  GenRefNoSet: function (self) {
    self.GenRefNo(null);
    var Booking = ClientData.ROSystemAreaAdHocBookingTypeList.Find("AdHocBookingTypeID", self.AdHocBookingTypeID());
    if (Booking) {
      if (Booking.AdHocBookingTypeID == AdHocTravelBookingBO.GenRefDetails.Production && Booking.ProductionAreaID != 7) {
        self.DefaultGenRefNo(Booking.DefaultGenRefNo);
        self.GenRefNo(null);
      }
      else {
        self.GenRefNo(Booking.DefaultGenRefNo);
      }
    }
  },
  SetStartDate: function (self) {
    var d = new Date()
    if (!self.StartDateTime()) {
      self.StartDateTime(d)
    }
  },
  SetEndDate: function (self) {
    var d = new Date()
    if (!self.EndDateTime()) {
      self.EndDateTime(d)
    }
  },
  AdHocBookingTypeIDSet: function (self) {

  },

  //other
  AdHocTravelBookingBOToString: function (self) {
    var Title = (self.Title() == undefined) ? '' : ' - Title: ' + self.Title();
    var Country = (self.Country() == undefined) ? '' : ' - Country: ' + self.Country();
    var System = ClientData.ROSystemList.Find('SystemID', self.SystemID());
    var ProductionArea = ClientData.ROProductionAreaList.Find('ProductionAreaID', self.ProductionAreaID());
    var Sys = (System == undefined) ? '' : ' - System: ' + System.System;
    var ProdArea = (ProductionArea == undefined) ? '' : ' - Production Area: ' + ProductionArea.ProductionArea;
    return 'Ad Hoc Travel Booking' + Title + Sys + ProdArea + Country;
  },
  CanEdit: function (AdHocTravelBooking, FieldName) {
    switch (FieldName) {
      case 'AdHocBookingTypeID':
        return (AdHocTravelBooking.SystemID() && AdHocTravelBooking.ProductionAreaID())
        break;
      case 'DefaultGenRefReason':
        if (AdHocTravelBooking.UsingDefaultGenRefReason() != undefined) {
          if (AdHocTravelBooking.GenRefNo() == AdHocTravelBooking.DefaultGenRefNo() &&
            (AdHocTravelBooking.UsingDefaultGenRefReason() || AdHocTravelBooking.UsingDefaultGenRefReason().trim() == ""))
            return true;
        }
        break;
      case 'GenRefNoEnabled':
        if (AdHocTravelBooking.AdHocBookingTypeID() == ViewModel.ProductionAdHocBookingType()) {
          var BookingType = ClientData.ROSystemAreaAdHocBookingTypeList.Find("AdHocBookingTypeID", AdHocTravelBooking.AdHocBookingTypeID());
          if (BookingType) {
            if (BookingType.ProductionAreaID != ViewModel.Blitz()) {
              return true;
            } else {
              return false;
            }
          }
        }
        return false;
        break;
      case 'EditBookingDetails':
        if (AdHocTravelBooking.CanEditBooking) {
          return true;
        } else {
          return false;
        }
        return false;
        break;
      default:
        return true;
        break;
    }
  },
  RetrieveProduction: function (self) {
    var BookingType = ClientData.ROSystemAreaAdHocBookingTypeList.Find("AdHocBookingTypeID", self.AdHocBookingTypeID());
    if (self.GenRefNo() != self.DefaultGenRefNo()) {
      self.UsingDefaultGenRefReason('');
      ViewModel.UsingDefaultForProduction(false);
    }
    else if (self.GenRefNo() == self.DefaultGenRefNo()) {
      ViewModel.UsingDefaultForProduction(true);
      self.HasValidGenRef(true);
    }
    if (self.AdHocBookingTypeID() == AdHocTravelBookingBO.GenRefDetails.Production && self.GenRefNo() != self.DefaultGenRefNo() && self.GenRefNo() > AdHocTravelBookingBO.GenRefDetails.MinGenRefNumber && BookingType.ProductionAreaID != 7) {
      Singular.SendCommand("Search",
        {
          GenRefNo: self.GenRefNo()
        },
        function (response) {
          if (response != true) { // No Production Found for Gen Ref
            window.alert('No Production Found for this Gen Ref No');
            self.HasValidGenRef(false);
          }
          else { // Production found for Gen Ref
            self.HasValidGenRef(true);
            if (($("#EditAdHocBooking").data('bs.modal') || { isShown: false }).isShown) {
              $("#ChangedGenRefFromEdit").modal();
            }
            else if (($("#NewAdHocBooking").data('bs.modal') || { isShown: false }).isShown) {
              $("#NewAdHocBooking").removeClass('modal-sm');
              $("#NewAdHocBooking").addClass('modal-lg');
              $("#NewAdHocBooking").find('.modal-dialog').css({
                width: 'auto',
                height: 'auto',
              });
              $("#NewAdHocBooking").css({
                width: 'auto',
                height: 'auto',
                'margin-left': '10%',
                'margin-right': '10%',
                'overflow-x': 'hidden'
              });
            }
          }
        });
    }
    else {
      //Using Default Gen Ref Number for Booking Type
    }
  },
  HasValidGenRef: function (Value, Rule, CtlError) {
    var Booking = CtlError.Object;
    var BookingType = ClientData.ROSystemAreaAdHocBookingTypeList.Find("AdHocBookingTypeID", Booking.AdHocBookingTypeID());
    if (BookingType) {
      if (Booking.GenRefNo() && Booking.AdHocBookingTypeID() == AdHocTravelBookingBO.GenRefDetails.Production && BookingType.ProductionAreaID != 7) {
        if (Booking.HasValidGenRef() == false) {
          CtlError.AddError("Invalid Gen Ref No");
        }
      }
    }
  },
  InitDate: function (self) {
    var d = new Date()
    self.StartDateTime(d)
    self.EndDateTime(d)
  }

}
