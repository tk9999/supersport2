﻿ROSynergyEventBO = {
  ChannelShortNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    var css = "";
    if (self.PrimaryChannelInd()) {
      css = "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-info bold";
    } else {
      css = "btn btn-xs btn-default bold";
    }
    //if (ViewModel.BigTextMode()) {
    //  css += " black-text big-text-mode"
    //}
    return css;
  },
  ChannelShortNameHtml: function (self) {
    return self.ChannelShortName();
  }
}

SynergyEventBO = {
  //other
  GetToString: function (self) {
    return self.Title()
  },
  SynergyEventBOToString: function (self) {
    return self.Title()
  },
  FilterByOwner: function (list, instance) {
    var results = []
    return list
  },
  sendRequest: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("RoomAllocationRequestSynergyEvent", {
      allocationRequest: ser
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        //success message
        obj.BookingCount(response.Data.BookingCount)
        obj.SynergyEventChannelList().Iterate(function (itm, indx) {
          response.Data.SynergyEventChannelList.Iterate(function (ch, chIndx) {
            if (itm.ImportedEventChannelID() == ch.ImportedEventChannelID) {
              itm.RoomScheduleCount(ch.RoomScheduleCount)
            }
          })
        })
        obj.ResourceIDMCRController(null)
        obj.ResourceIDSCCROperator(null)
        obj.RoomID(null)
        obj.CallTime(null)
        obj.WrapTime(null)
        OBMisc.Notifications.GritterSuccess('Allocated Successfully', RoomAllocatorBO.GetToString(obj), 1500)
      }
      else {
        //error message
        OBMisc.Notifications.GritterError('Allocation Failed', response.ErrorText, 3000)
      }
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  sendRequestOB: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("OBAllocationRequestSynergyEvent", {
      allocationRequest: ser
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        obj.ProductionID(response.Data.ProductionID)
        obj.ProductionRefNo(response.Data.ProductionRefNo)
        obj.ProductionSystemAreaID(response.Data.ProductionSystemAreaID)
        OBMisc.Notifications.GritterSuccess('Allocated Successfully', SynergyEventBO.GetToString(obj), 500)
      }
      else {
        //error message
        OBMisc.Notifications.GritterError('Allocation Failed', response.ErrorText, 2000)
      }
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  ChannelShortNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    var css = "";
    if (self.PrimaryChannelInd()) {
      css = "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      css = "btn btn-xs btn-info bold";
    } else {
      css = "btn btn-xs btn-default bold";
    }
    //if (ViewModel.BigTextMode()) {
    //  css += " black-text big-text-mode"
    //}
    return css;
  },
  ChannelShortNameHtml: function (self) {
    return self.ChannelShortName();
  },
  ChannelNameCss: function (self) {
    var MaximoChannels = [51, 52, 53];
    var BlitzChannels = [48];
    if (self.PrimaryChannelInd()) {
      return "btn btn-xs btn-default bold";
    } else if (MaximoChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-warning bold";
    } else if (BlitzChannels.indexOf(self.ChannelID()) >= 0) {
      return "btn btn-xs btn-info bold";
    } else {
      return "btn btn-xs btn-default bold";
    }
  },
  ChannelNameHtml: function (self) {
    return self.ChannelName();
  },
  updateBookingTimesText: function (obj) {
    var s = ""
    if (obj.CallTime()) {
      s = new Date(obj.CallTime()).format("dd MMM HH:mm")
    } else {
      s = new Date(obj.FirstStart()).format("dd MMM HH:mm")
    }
    if (obj.CallTime()) {
      s += " - " + new Date(obj.WrapTime()).format("dd MMM HH:mm")
    } else {
      s += " - " + new Date(obj.LastEnd()).format("dd MMM HH:mm")
    }
    obj.BookingTimes(s)
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  bookingTimesButtonCSS: function (obj) {
    if (obj.TimesValid() && obj.RoomClashCount() == 0) {
      return "btn btn-xs btn-default btn-block bold"
    } else {
      return "btn btn-xs btn-danger btn-block animated rubberBand infinite go bold"
    }
  },
  buttonHTML: function (obj) {
    return obj.BookingCount().toString()
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },

  //cans
  canUpload: function (obj) {
    //if (!obj.IsProcessing()) {
    //&& obj.IsValid() 
    if (obj.RoomID() && obj.CallTime() && obj.WrapTime() && !obj.IsProcessing() && obj.RoomClashCount() == 0) {
      return true
    }
    //}
    return false
  },
  canUploadOB: function (obj) {
    return (obj.IsValid() && !obj.IsProcessing() && (obj.ProductionSystemAreaID() == null))
  },
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case 'ProductionSpecRequirementID':
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
      case 'TeamsPlaying':
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
      case 'VehicleID':
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
      case 'ProductionVenueID':
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
      case 'LiveDate':
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
      case 'LiveEndDate':
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
      default:
        if (instance.IsProcessing() || instance.ProductionSystemAreaID()) { return false } else { return true }
        break;
    }
  },
  CanDelete: function (self) {
    return false
  },

  //set
  SystemIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  ProductionAreaIDSet: function (SynergyEvent) {
    //SynergyEvent.RefreshCounter(SynergyEvent.RefreshCounter() + 1)
  },
  RoomIDSet: function (SynergyEvent) {
    if (SynergyEvent.RoomID()) {
      var callTimeRequired = false,
          wrapTimeRequired = false;

      //get area settings
      var areaSettings = ClientData.ROSystemProductionAreaCallTimeSettingList.Filter('SystemID', SynergyEvent.SystemID())
      areaSettings = areaSettings.Filter('ProductionAreaID', SynergyEvent.ProductionAreaID())
      var roomSettings = areaSettings.Filter('RoomID', SynergyEvent.RoomID())

      var roomSetting = null;
      if (roomSettings.length == 1) {
        roomSetting = roomSettings[0]
        callTimeRequired = roomSetting.RoomCallTimeRequired
        wrapTimeRequired = roomSetting.RoomWrapTimeRequired
      }
      else if (roomSettings.length > 1) {
        //check dates
        roomSettings.Iterate(function (rm, rmIndx) {
          var effectiveDate = new moment(rm.EffectiveDate)
          var effectiveEndDate = (rm.EffectiveEndDate ? new moment(rm.EffectiveEndDate) : new moment(SynergyEvent.LastEnd()))
          var dateRange = moment().range(effectiveDate, effectiveEndDate)
          var startDate = moment(SynergyEvent.FirstStart())
          if (startDate.isBetween(effectiveDate, effectiveEndDate)) {
            roomSetting = rm
            return //exit loop
          }
        })
      } else {
        callTimeRequired = false;
        wrapTimeRequired = false;
        roomSetting = null;
      }

      //call and wrap time variables
      var ctMinutes = 0,
          wtMinutes = 0;

      //check call time
      if (callTimeRequired) {
        if (roomSetting) {
          var ct = moment(new Date(SynergyEvent.FirstStart())).add(-roomSetting.CallTimeMinutes, 'minutes').toDate()
          SynergyEvent.CallTime(ct)
        }
        else {
          SynergyEvent.CallTime(SynergyEvent.FirstStart())
        }
      }
      else {
        SynergyEvent.CallTime(SynergyEvent.FirstStart())
      }

      //check wrap time
      if (wrapTimeRequired) {
        if (roomSetting) {
          var wt = moment(new Date(SynergyEvent.LastEnd())).add(roomSetting.WrapTimeMinutes, 'minutes').toDate()
          SynergyEvent.WrapTime(wt)
        }
        else {
          SynergyEvent.WrapTime(SynergyEvent.LastEnd())
        }
      }
      else {
        SynergyEvent.WrapTime(SynergyEvent.LastEnd())
      }

    }
    else {
      SynergyEvent.RoomClashCount(0)
      SynergyEvent.CallTime(null)
      SynergyEvent.WrapTime(null)
    }
  },
  CallTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },
  FirstStartSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    SynergyEvent.IsProcessing(false)
  },
  LastEndSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
    SynergyEvent.IsProcessing(true)
    //Check Room availability
    if (SynergyEvent.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", SynergyEvent.RoomID())
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), room.ResourceID, null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.RoomClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.RoomClashCount(0)
    }
    //Check Controller availability
    if (SynergyEvent.ResourceIDMCRController()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.MCRControllerClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.MCRControllerClashCount(0)
    }
    //Check Operator availability
    if (SynergyEvent.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(SynergyEvent.FirstStart(), SynergyEvent.LastEnd(), SynergyEvent.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             SynergyEvent.SCCROperatorClashCount(response.Data.length)
                                           },
                                           function (response) {
                                             SynergyEvent.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                           })
    }
    else {
      SynergyEvent.SCCROperatorClashCount(0)
    }
    if (SynergyEvent.LastEnd()) {
      var lastEnd = new Date(SynergyEvent.LastEnd())
      var newWrapTime = new Date(lastEnd.getTime() + (1 * 36e5)) //Default Wrap Time to 1 hr after end time
      SynergyEvent.WrapTime(newWrapTime)
    }
    SynergyEvent.IsProcessing(false)
  },
  WrapTimeSet: function (SynergyEvent) {
    this.updateBookingTimesText(SynergyEvent)
  },

  //Rules
  LiveDateValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.LiveDate();
    var leti = CtlError.Object.LiveEndDateTime();
    if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live Start Time must be before Live End Time");
      }
    }
  },
  LiveEndDateTimeValid: function (Value, Rule, CtlError) {
    var leti = CtlError.Object.LiveEndDateTime();
    if (!leti) {
      CtlError.AddError("Live End Time is required");
    }
  },
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.OBInd() && CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required");
    }
  },
  TeamsValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.OBInd() && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams are required");
    }
  },
  TeamsPlayingValid: function (Value, Rule, CtlError) {
    if ((CtlError.Object.ProductionAreaID() == 1) && CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams Playing is required");
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.FirstStart())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.FirstStart(), obj.LastEnd())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.LastEnd(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      //clash
      if (obj.RoomClashCount() > 0) {
        CtlError.AddError('Room not available')
      }

      if (invalidTimeCount > 0) {
        obj.TimesValid(false)
      } else { obj.TimesValid(true) }

    }
  },
  HoursValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.CallTime() != undefined && obj.LastEnd() != undefined) {
      var callTime = new Date(obj.CallTime())
      var endTime = new Date(obj.LastEnd())
      var hours = Math.abs(endTime - callTime) / 36e5 // 36e5 = 60*60*1000(Convert ms to hrs)
      if (hours > 14) {
        CtlError.AddError('Shift is more than 14 Hours long')
      }
    }
  },
  OBViewDateValid: function (value, Rule, CtlError) {
    var obj = CtlError.Object;
    var DateOne = moment(new Date(obj.LiveDate()))
    var DateTwo = moment(new Date(obj.LiveEndDate()))
    var isSameOrBefore = DateTwo.isSameOrBefore(DateOne, 'minute')
    if (isSameOrBefore) {
      CtlError.AddError('End Date is incorrect')
    }
  },

  //RoomID
  beforeRoomIDDropDownShown: function () {
    console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    if (selectedItem && (selectedItem.IsGap || selectedItem.IsSufficientTime)) {
      businessObject.RoomClashCount(0)
    } else {
      businessObject.RoomClashCount(businessObject.RoomClashCount + 1)
    }
  },
  triggerAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.FirstStart()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },

  //VehicleID
  triggerVehicleAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setVehicleCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date().format("dd MMM yyyy HH:mm")
    args.Data.CurrentProductionVenueID = args.Object.ProductionVenueID()
  },
  afterVehicleRefreshAjax: function (args) {

  },
  VehicleIDBeforeSet: function (self) {
    //self.PreviousVehicleID(self.VehicleID())
  },
  VehicleIDSet: function (self) {
    var me = this;
  },
  onVehicleSelected: function (item, businessObject) {
    var x = null
  },

  //ProductionVenueID
  ProductionVenueIDSet: function (self) { },
  onProductionVenueSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionVenueID(selectedItem.ProductionVenueID)
    }
    else {
      businessObject.ProductionVenueID(null)
    }
  },
  triggerProductionVenueAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setProductionVenueCriteriaBeforeRefresh: function (args) {
    //args.Data.StartDateTime = new Date(args.Object.FirstStart()).format("dd MMM yyyy HH:mm")
    //args.Data.EndDateTime = new Date(args.Object.LastEnd()).format("dd MMM yyyy HH:mm")
  },
  afterProductionVenueRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },

  //ProductionSpecRequirementID
  triggerProductionSpecRequirementAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSpecRequirementCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterProductionSpecRequirementRefreshAjax: function (args) {

  },
  ProductionSpecRequirementIDSet: function (self) { },
  onProductionSpecRequirementSelected: function (item, businessObject) { }
};

SynergyEventListCriteriaBO = {
  PrimaryChannelsOnlySet: function (self) { },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  LiveSet: function (self) { },
  DelayedSet: function (self) { },
  PremierSet: function (self) { },
  GenRefNoSet: function (self) { },
  GenRefNoStringSet: function (self) { },
  PrimaryChannelsOnlySet: function (self) { },
  AllChannelsSet: function (self) { },
  GenreSet: function (self) { },
  SeriesSet: function (self) { },
  TitleSet: function (self) { },
  KeywordSet: function (self) { }
}

SynergyEventChannelBO = {
  DefaultRoomIDSet: function (SynergyEventChannel) {
    SynergyEventChannelBO.GetCalculatedTimesForRoom(SynergyEventChannel);
  },
  GetCalculatedTimesForRoom: function (SynergyEventChannel) {
    if (SynergyEventChannel.DefaultRoomID() && SynergyEventChannel.ScheduleDateTime() && SynergyEventChannel.ScheduleEndDate()) {
      ViewModel.CallServerMethod('GetCalculatedTimesForRoom', {
        DefaultRoomID: SynergyEventChannel.DefaultRoomID(),
        OnAirStartDateTime: SynergyEventChannel.ScheduleDateTime(),
        OnAirEndDateTime: SynergyEventChannel.ScheduleEndDate()
      }, function (args) {
        if (args.Data) {
          SynergyEventChannel.DefaultRoomCallTime(new Date(args.Data.CallTimeStart));
          SynergyEventChannel.DefaultRoomWrapTime(new Date(args.Data.WrapTimeEnd));
          //if (SynergyEventChannel.DefaultRoomID()) {
          //  var Room = ClientData.RORoomList.Find("RoomID", SynergyEventChannel.DefaultRoomID());
          //  //SynergyEventChannel.DefaultRoomTypeID(Room.RoomTypeID);
          //}
        }
      });
    } else {
      SynergyEventChannel.DefaultRoomCallTime(null);
      SynergyEventChannel.DefaultRoomWrapTime(null);
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.RoomNotAvailableReason().trim().length > 0) {
      CtlError.AddError(CtlError.Object.RoomNotAvailableReason());
    }
  },
  SynergyEventChannelBOToString: function (self) {
    return self.ChannelShortName()
  }
}

ROSynergyEventListCriteriaBO = {
  ByDateSet: function (ROSynergyEventListCriteria) {
    ROSynergyEventListCriteria.StartDate(ROSynergyEventListCriteria.ByDate());
    ROSynergyEventListCriteria.EndDate(ROSynergyEventListCriteria.ByDate());
    ViewModel.ROSynergyEventDefaultListCriteria().StartDate(ROSynergyEventListCriteria.ByDate());
    ViewModel.ROSynergyEventDefaultListCriteria().EndDate(ROSynergyEventListCriteria.ByDate());
    //var sdAfterED = OBMisc.Dates.IsAfter(ROSynergyEventListCriteria.StartDate(), ROSynergyEventListCriteria.EndDate());
    //if (sdAfterED) {
    //  ROSynergyEventListCriteria.EndDate(ROSynergyEventListCriteria.StartDate());
    //}
  }
}

ROSynergyChangeBO = {
  get: function (options) {
    Singular.GetDataStateless("OBLib.Synergy.ReadOnly.ROSynergyChangeList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          if (options.onFail) { options.onFail(response) }
        }
      })
  }
}

RoomAllocatorListCriteriaBO = {
  PrimaryChannelsOnlySet: function (self) {

  },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  LiveSet: function (self) { },
  DelayedSet: function (self) { },
  PremierSet: function (self) { },
  GenRefNoSet: function (self) { },
  PrimaryChannelsOnlySet: function (self) { },
  AllChannelsSet: function (self) { },
  GenreSet: function (self) { },
  SeriesSet: function (self) { },
  TitleSet: function (self) { }
}

AllocatorChannelListCriteriaBO = {
  PrimaryChannelsOnlySet: function (self) { },
  StartDateSet: function (self) { },
  EndDateSet: function (self) { },
  LiveSet: function (self) { },
  DelayedSet: function (self) { },
  PremierSet: function (self) { },
  GenRefNoSet: function (self) { },
  PrimaryChannelsOnlySet: function (self) { },
  AllChannelsSet: function (self) { },
  GenreSet: function (self) { },
  SeriesSet: function (self) { },
  TitleSet: function (self) { },
  channelSelected: function (channel) { },
  KeywordSet: function (self) { }
}

EquipmentAllocatorListCriteriaBO = {
  PrimaryChannelsOnlySet: function (self) {

  }
}

ROSynergyChangePagedListCriteriaBO = {
  GenRefNumberSet: function (self) { },
  ScheduleNumberSet: function (self) { },
  ChangeStartDateTimeSet: function (self) { },
  ChangeEndDateTimeSet: function (self) { },
  SynergyChangeTypeIDSet: function (self) { },
  ColumnNameSet: function (self) { }
}

RoomAllocatorBO = {
  FilterByOwner: function (list, instance) {
    var results = []
    list.Iterate(function (itm, indx) {
      ViewModel.UserSystemList().Iterate(function (sys, indx2) {
        if (sys.SystemID() == itm.OwningSystemID) {
          sys.UserSystemAreaList().Iterate(function (pa, indx3) {
            if (pa.ProductionAreaID() == itm.OwningProductionAreaID) {
              results.push(itm);
            }
          });
        }
      });
    });
    return results
  },
  editTimes: function (object, element) {

  },
  GetToString: function (self) {
    return self.GenreSeries() + ' - ' + self.Title()
  },
  IconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },
  getEventStatusCssClass: function (self) {
    if (self.EventStatus() == "L") {
      return 'btn btn-xs btn-block event-live'
    } else if (self.EventStatus() == "D") {
      return 'btn btn-xs btn-block event-delayed'
    } else if (self.EventStatus() == "P") {
      return 'btn btn-xs btn-block event-premier'
    } else {
      return 'btn btn-xs btn-block event-default'
    }
  },
  bookingTimesButtonCSS: function (obj) {
    if (obj.TimesValid() && obj.RoomClashCount() == 0) {
      return "btn btn-xs btn-default btn-block bold black"
    } else {
      return "btn btn-xs btn-danger btn-block animated rubberBand infinite go bold black"
    }
  },
  showRoomAllocatorEventTime: function () {

  },
  buttonHTML: function (obj) {
    return obj.BookingCount().toString()
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },

  //Data Access
  get: function (options) {
    Singular.GetDataStateless("OBLib.Rooms.RoomAllocatorList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          if (options.onFail) { options.onFail(response) }
        }
      })
  },

  //Other
  sendRequest: function (obj) {
    obj.IsProcessing(true)
    ViewModel.DisableUpload(true)
    var ser = KOFormatterFull.Serialise(obj)
    ViewModel.CallServerMethod("RoomAllocationRequest", {
      allocationRequest: ser
    }, function (response) {
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
        //success message
        $.gritter.add({
          title: 'Allocated Successfully',
          text: RoomAllocatorBO.GetToString(obj),
          class_name: 'success',
          time: 500
        })
        obj.BookingCount(obj.BookingCount() + 1)
      }
      else {
        //error message
        $.gritter.add({
          title: 'Allocation Failed',
          text: '<p>' + RoomAllocatorBO.GetToString(obj) + '</p>' +
                '<p>' + response.ErrorText + '</p>',
          class_name: 'danger',
          time: 1000
        })
      }
      var ro = ViewModel.LatestResult.Set(response)
      ViewModel.AllocationResults().push(ro)
      ViewModel.LatestResult(null)
      ViewModel.HistoryText(ViewModel.AllocationResults().length.toString() + ' items')
      obj.IsProcessing(false)
      ViewModel.DisableUpload(false)
    })
  },
  canUpload: function (obj) {
    if (!ViewModel.DisableUpload()) {
      if (obj.RoomID() && obj.CallTime() && obj.WrapTime() && obj.IsValid() && !obj.IsProcessing() && obj.RoomClashCount() == 0) {
        return true
      }
    }
    return false
  },

  //Cans
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case 'ResourceIDMCRController':
      case 'ResourceIDSCCROperator':
        return (instance.SystemID() == 5)
        break
      default:
        return !instance.IsProcessing()
        break;
    }
  },

  //Validation
  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.StartDateTime())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.StartDateTime(), obj.OnAirEndDateTime())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.OnAirEndDateTime(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      //clash
      if (obj.RoomClashCount() > 0) {
        CtlError.AddError('Room not available')
      }

      if (invalidTimeCount > 0) {
        obj.TimesValid(false)
      } else { obj.TimesValid(true) }

    }
  },

  //Set-------------------------------------------------------
  RoomIDSet: function (self) {

  },

  //DropDowns-------------------------------------------------
  //RoomID
  beforeRoomIDDropDownShown: function () {
    //console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    var me = this;
    ViewModel.CurrentRoomAllocatorRoomSchedule(new RoomAllocatorRoomScheduleObject())
    ViewModel.CurrentRoomAllocatorRoomSchedule().GenRefNumber(businessObject.GenRefNumber())
    ViewModel.CurrentRoomAllocatorRoomSchedule().GenreSeries(businessObject.GenreSeries())
    ViewModel.CurrentRoomAllocatorRoomSchedule().ProductionID(businessObject.ProductionID())
    ViewModel.CurrentRoomAllocatorRoomSchedule().ResourceBookingID(0)
    ViewModel.CurrentRoomAllocatorRoomSchedule().Title(businessObject.Title())
    ViewModel.CurrentRoomAllocatorRoomSchedule().Room(selectedItem.Room)
    ViewModel.CurrentRoomAllocatorRoomSchedule().RoomID(selectedItem.RoomID)
    ViewModel.CurrentRoomAllocatorRoomSchedule().CallTime(null)
    ViewModel.CurrentRoomAllocatorRoomSchedule().StartDateTime(new Date(businessObject.ScheduleDateTime()))
    ViewModel.CurrentRoomAllocatorRoomSchedule().EndDateTime(new Date(businessObject.ScheduleEndDate()))
    ViewModel.CurrentRoomAllocatorRoomSchedule().WrapTime(null)
    ViewModel.CurrentRoomAllocatorRoomSchedule().SystemID(businessObject.SystemID())
    ViewModel.CurrentRoomAllocatorRoomSchedule().ProductionAreaID(businessObject.ProductionAreaID())
    //newBooking.SystemID()
    //newBooking.ProductionAreaID()
    //newBooking.RoomClashCount()
    ViewModel.CurrentRoomAllocatorRoomSchedule().ScheduleNumber(businessObject.ScheduleNumber())
    RoomAllocatorRoomScheduleBO.doSetup(ViewModel.CurrentRoomAllocatorRoomSchedule(), {
      onSuccess: function (response) {
        ViewModel.CurrentRoomAllocatorRoomSchedule.Set(response.Data)
        RoomAllocatorPage.showRoomScheduleModal()
        ViewModel.CurrentRoomAllocatorRoomSchedule().IsProcessing(false)
      }
    })
  },
  triggerRoomIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setRoomIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.ScheduleDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.ScheduleEndDate()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomIDRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.ScheduleDateTime()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.ScheduleEndDate()).format("dd MMM yyyy HH:mm")
  },
  afterRoomIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onRoomIDCellCreate: function (cellElement, index, ColumnData, RowData) {

  }

}

RoomAllocatorRoomScheduleBO = {
  GetToString: function (self) {
    return self.Title()
  },
  getRoomScheduleArea: function (self) {
    self.IsProcessing(true)
    RoomScheduleAreaBO.get({
      criteria: {
        RoomScheduleID: self.RoomScheduleID(),
        ProductionSystemAreaID: self.ProductionSystemAreaID()
      },
      onSuccess: function (response) {
        ViewModel.CurrentRoomScheduleArea.Set(response.Data[0])
        RoomScheduleAreaBO.showModal()
        self.IsProcessing(false)
      },
      onFail: function (response) {

      }
    })
  },
  doSetup: function (self, options) {
    var me = this;
    self.IsProcessing(true)
    ViewModel.CallServerMethod("SetupRoomAllocatorRoomSchedule", {
      RoomAllocatorRoomSchedule: self.Serialise()
    }, function (response) {
      if (response.Success) {
        if (options.onSuccess) {
          options.onSuccess.call(self, response)
        }
      }
      else {

      }
      ViewModel.CurrentRoomAllocatorRoomSchedule().IsProcessing(false)
    })
  },
  canSave: function (self) {
    //&& self.RoomClashCount() == 0
    if (self.RoomID() && self.CallTime() && self.WrapTime() && self.IsValid() && !self.IsProcessing()) {
      return true
    }
    return false
  },
  removeItem: function (self, parent) {
    self.DeleteRoomSchedule(true)
    //parent.RoomAllocatorRoomScheduleList.RemoveNoCheck(self)
  },
  confirmDelete: function (self, parent) {
    self.IsProcessing(true)
    ViewModel.CallServerMethod("DeleteRoomAllocatorRoomSchedule", {
      RoomAllocatorRoomSchedule: self.Serialise()
    },
    function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Deleted Successfully", "", 250)
        parent.RoomAllocatorRoomScheduleList.RemoveNoCheck(self)
      }
      else {
        OBMisc.Notifications.GritterError("Delete Failed", response.ErrorText, 1000)
      }
      self.IsProcessing(false)
    })
  },
  cancelDelete: function (self) { self.DeleteRoomSchedule(false) },

  //DataAccess
  get: function (options) {
    Singular.GetDataStateless("OBLib.Rooms.RoomAllocatorRoomScheduleList, OBLib",
      options.criteria,
      function (response) {
        if (response.Success) {
          ViewModel.CurrentRoomAllocatorRoomSchedule.Set(response.Data[0])
          if (options.onSuccess) { options.onSuccess(response) }
        }
        else {
          if (options.onFail) { options.onFail(response) }
        }
      })
  },
  saveItem: function (item, options) {
    ViewModel.CallServerMethod("SaveRoomAllocatorRoomSchedule",
                               { RoomAllocatorRoomSchedule: item.Serialise() },
                               function (response) {
                                 if (response.Success) {
                                   if (options.onSuccess) { options.onSuccess(response) }
                                 }
                                 else {
                                   if (options.onFail) { options.onFail(response) }
                                 }
                               })
  },

  //UI
  buttonHTML: function (obj) {
    return "Create Booking"
  },
  buttonCSS: function (obj) {
    if (obj.BookingCount() > 0) {
      return "btn btn-xs btn-success"
    } else {
      return "btn btn-xs btn-default"
    }
  },
  iconCss: function (obj) {
    if (obj.IsProcessing()) {
      return "fa fa-refresh fa-spin"
    }
    else {
      return "fa fa-cloud-upload"
    }
  },

  //DropDowns
  beforeRoomIDDropDownShown: function () {
    //console.log(this)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    var me = this;
    RoomAllocatorRoomScheduleBO.doSetup(ViewModel.CurrentRoomAllocatorRoomSchedule(), {
      onSuccess: function (response) {
        ViewModel.CurrentRoomAllocatorRoomSchedule.Set(response.Data)
        ViewModel.CurrentRoomAllocatorRoomSchedule().IsProcessing(false)
      }
    })
  },
  triggerRoomIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setRoomIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
  },
  beforeRoomIDRefreshAjax: function (args, self) {
    self.IsProcessing(true)
    args.SystemID = self.SystemID()
    args.ProductionAreaID = self.ProductionAreaID()
    args.StartDateTime = new Date(self.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.EndDateTime = new Date(self.EndDateTime()).format("dd MMM yyyy HH:mm")
  },
  afterRoomIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onRoomIDCellCreate: function (cellElement, index, ColumnData, RowData) {

  },

  //Set
  RoomIDSet: function (self) {
  },
  CallTimeSet: function (RoomAlloctator) {
  },
  StartDateTimeSet: function (RoomAlloctator) {
  },
  EndDateTimeSet: function (RoomAlloctator) {
  },
  WrapTimeSet: function (RoomAlloctator) {
  },

  //Rules
  RoomIDValid: function (Value, Something, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.StartDateTime())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.StartDateTime(), obj.EndDateTime())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.EndDateTime(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      ////clash
      //if (obj.RoomClashCount() > 0) {
      //  CtlError.AddError('Room not available')
      //}

    }
  }
}