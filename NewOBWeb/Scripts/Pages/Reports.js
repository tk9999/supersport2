﻿var ROProductionTypes = new ROProductionTypePagedListManager();
var ROHumanResources = new ROHumanResourceListManager();
var ROProductions = new ROProductionListManager();
var ROEventTypes = new ROEventTypeManager();
var RODisciplines = new RODisciplineListManager();
var ROContractTypes = new ROContractTypeListManager();
var RORoomListPaged = new RORoomListPagedManager();
var ROEquipments = new ROEquipmentListManager();
var ROAdHocBookings = new ROROAdHocBookingListManager();
var ROCities = new ROCityPagedListManager();
//var ROCities = new ROCityListManager();
var ROProvinces = new ROProvincePagedListManager();
//var ROTeams = new ROTeamListManager();
var fetchTimeout = setTimeout(function () { });
var DELAY = 400;
var FindHRControl = new FindROHumanResourceControl({
  onRowSelected: function (ROHumanResource) {
    this.hide();
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
    ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.HumanResource());
  }
});
FindHRControl.AddSystem = function (obj) {
  obj.IsSelected(!obj.IsSelected());
  ROHumanResourcePagedListCriteriaBO.SetXMLCriteria();
  FindHRControl.refresh();
}
FindHRControl.AddProductionArea = function (obj) {
  obj.IsSelected(!obj.IsSelected());
  ROHumanResourcePagedListCriteriaBO.SetXMLCriteria();
  FindHRControl.refresh();
}

FindHRControl.afterModalOpened = function (obj) {
  $(ViewModel.ROHumanResourceListCriteria().Keyword.BoundElements).focus();
}
//#region RO Human Resource Find List Criteria BO
ROHumanResourceFindListCriteriaBO = {
  FindHumanResourceGDS: function (param) {
    ViewModel.IsReportBusy(true);

    if(ViewModel.Report().ReportCriteriaGeneric().DisciplineID)
		ViewModel.Report().ReportCriteriaGeneric().DisciplineID(param.DisciplineID());
    if(ViewModel.Report().ReportCriteriaGeneric().FirstName)
		ViewModel.Report().ReportCriteriaGeneric().FirstName(param.FirstName());
    if(ViewModel.Report().ReportCriteriaGeneric().Surname)
		ViewModel.Report().ReportCriteriaGeneric().Surname(param.Surname());
    if(ViewModel.Report().ReportCriteriaGeneric().PreferredName)
		ViewModel.Report().ReportCriteriaGeneric().PreferredName(param.PreferredName());

    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: param.SystemID(),
      SystemIDs: param.SystemIDs(),
      SystemIDList: param.SystemIDList(),
      ProductionAreaID: param.ProductionAreaID(),
      ProductionAreaIDs: param.ProductionAreaIDs(),
      ProductionAreaIDList: param.ProductionAreaIDList(),
      FirstName: param.FirstName(),
      Surname: param.Surname(),
      PreferredName: param.PreferredName(),
      DisciplineID: param.DisciplineID(),
      ContractTypeID: param.ContractTypeID(),
      ContractTypeIDs: param.ContractTypeIDs()
    },
    function (self) {
      if (self.Success) {
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([])
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(self.Data);
        $("#HumanResources").addClass('FadeDisplay');
        $("#SelectedHumanResources").addClass('FadeDisplay');
        $("#HumanResourceFilters").addClass('FadeDisplay');
      }
      ViewModel.IsReportBusy(false)
    })
  },
  FirstNameSet: function (self) {
    this.FindHumanResourceGDS(self);
  },
  PreferredNameSet: function (self) {
    this.FindHumanResourceGDS(self);
  },
  SurnameSet: function (self) {
    this.FindHumanResourceGDS(self);
  },  
  SystemIDSet: function (self) {
    this.FindHumanResourceGDS(self);
  },
  ProductionAreaIDSet: function (self) {
    this.FindHumanResourceGDS(self);
  },
  ContractTypeIDSet: function (self) {
    this.FindHumanResourceGDS(self);
  },
  DisciplineIDSet: function (self) {
    this.FindHumanResourceGDS(self);
  }
}
//#endregion
ROHumanResourcePagedListCriteriaBO = {
  ContractTypeIDSet: function (self) { FindHRControl.refresh() },
  DisciplineIDSet: function (self) { FindHRControl.refresh() },
  FirstnameSet: function (self) { FindHRControl.refresh() },
  SecondNameSet: function (self) { FindHRControl.refresh() },
  PreferredNameSet: function (self) { FindHRControl.refresh() },
  KeywordSet: function (self) { FindHRControl.refresh() },
  SurnameSet: function (self) { FindHRControl.refresh() },
  SystemIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceListManager().IsLoading()) {
      FindHRControl.refresh();
    }
  },
  ProductionAreaIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceListManager().IsLoading()) {
      FindHRControl.refresh();
    }
  },
  SetXMLCriteria: function () {
    //clear current selections
    var selectedSystems = []
    var selectedAreas = []
    ViewModel.UserSystemList().Iterate(function (uSys, uSysInd) {
      if (uSys.IsSelected()) {
        selectedSystems.push(uSys.SystemID())
      }
      uSys.UserSystemAreaList().Iterate(function (uSyAr, uSyArInd) {
        if (uSyAr.IsSelected()) {
          selectedAreas.push(uSyAr.ProductionAreaID())
        }
      })
    })
    ViewModel.ROHumanResourceListCriteria().SystemIDsXml(OBMisc.Xml.getXmlIDs(selectedSystems))
    ViewModel.ROHumanResourceListCriteria().ProductionAreaIDsXml(OBMisc.Xml.getXmlIDs(selectedAreas))
  }
};
//#endregion
//#region RO Production Find Criteria BO
ROProductionFindListCriteriaBO = {
  FindProductionGDS: function (param) {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionFindList, OBLib', {
      PageSize: 1000,
      KeyWord: param.KeyWord(),
      ProductionRefNo: param.ProductionRefNo(),
      ProductionTypeID: param.ProductionTypeID()
    }, function (self) {
      if (self.Success) {
        ViewModel.Report().ReportCriteriaGeneric().ROProductionList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROProductionList.Set(self.Data);
      }
      ViewModel.IsReportBusy(false);
    });
    return ViewModel.Report().ReportCriteriaGeneric().ROProductionList;
  },
  ProductionRefNoSet: function (self) {
    this.FindProductionGDS(self);
  },
  ProductionKeyWordSet: function (self) {
    this.FindProductionGDS(self);
  }
}
//#endregion
//#region RO Production Type Criteria BO
ROProductionTypeCriteriaBO = {
  FindProductionTypeGDS: function (param) {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport, OBLib', {
      PageSize: 1000,
      KeyWord: param.KeyWord()
    }, function (self) {
      if (self.Success) {
        ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList.Set(self.Data);
      }
      ViewModel.IsReportBusy(false);
    });
    return ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList;
  },
  KeywordSet: function (self) {
    this.FindProductionTypeGDS(self);
  }
}
//#endregion
//#region RO Event Types Criteria BO
ROEventTypesCriteriaBO = {
  SetEventTypes: function (self) {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport, OBLib', {
      PageSize: 1000,
      ProductionTypeID: ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(),
      KeyWord: ViewModel.ROEventTypeReportPagedListCriteria().KeyWord(),
      SortColumn: "EventType"
    }, function (self) {
      if (self.Success) {
        KOFormatter.Deserialise(self.Data, ViewModel.ROFilteredEventTypeReportList);
        var tempList = ViewModel.ROFilteredEventTypeReportList();
        var list = Singular.MergeSort(tempList, function (c) {
          return c.EventType();
        }, 1);
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set(self.Data);
      }
      ViewModel.IsReportBusy(false);
    });
  }
}
//#endregion

function SelectAllCurrentProductionsInList(start, end) {
  ViewModel.SelectAllProductions(!ViewModel.SelectAllProductions());
  if (ViewModel.SelectAllProductions()) {
    ViewModel.ROProductionListPagingManager().SelectedItems.Set([])
    ViewModel.Report().ReportCriteriaGeneric().ProductionIDs.Set([])
    Singular.ShowLoadingBar();
    Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionList, OBLib', {
      PageSize: 1000,
      TxDateFrom: start,
      TxDateTo: end
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROProductionList);
        for (var i = 0; i < ViewModel.ROProductionList().length - 2; ++i) {
          var NewProduction = ViewModel.ROProductionList()[i].ProductionDescription();
          var ni = new SelectedItemObject()
          ni.ID(ViewModel.ROProductionList()[i].ProductionID())
          ni.Description(ViewModel.ROProductionList()[i].Title())
          ViewModel.ROProductionListPagingManager().SelectedItems().push(ni);
          ViewModel.Report().ReportCriteriaGeneric().ProductionIDs.push(ViewModel.ROProductionList()[i].ProductionID());
          if (!ViewModel.Report().ReportCriteriaGeneric().Production()) {
            ViewModel.Report().ReportCriteriaGeneric().Production("");
          }
          if (ViewModel.Report().ReportCriteriaGeneric().Production().trim().length > 0) {
            NewProduction = ', ' + NewProduction;
          };
          var newString = ViewModel.Report().ReportCriteriaGeneric().Production() + NewProduction;
          newString = newString.replace(",,", "");
          if (newString.endsWith(",")) {
            newString = newString.substring(0, newString.length - 1);
          }
          ViewModel.Report().ReportCriteriaGeneric().Production(newString);
        }
        Singular.HideLoadingBar();
        ViewModel.ROProductionListCriteria().TxDateFrom(start);
        ViewModel.ROProductionListCriteria().TxDateTo(end);
        ViewModel.ROProductionListPagingManager().Refresh();
      }
    });
  }
  else {
    var newString = "";
    ViewModel.ROProductionListPagingManager().SelectedItems.Set([]);
    ViewModel.Report().ReportCriteriaGeneric().ProductionIDs.Set([]);
    ViewModel.Report().ReportCriteriaGeneric().Production(newString);
    ViewModel.ROProductionListCriteria().TxDateFrom(start);
    ViewModel.ROProductionListCriteria().TxDateTo(end);
    ViewModel.ROProductionListPagingManager().Refresh();
  }

  if (ViewModel.SelectAllProductions()) {
    $("#ProdPagedGrid tr").each(function () {
      //window.alert('line');
      $(this).click();
    });
  }
};

function SelectAllCurrentBookingsInList(start, end) {
  ViewModel.SelectAllAdHocBookings(!ViewModel.SelectAllAdHocBookings());
  if (ViewModel.SelectAllAdHocBookings()) {
    ViewModel.ROAdHocBookingListPagingManager().SelectedItems.Set([])
    ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.Set([])
    Singular.ShowLoadingBar();
    Singular.GetDataStateless('OBLib.AdHoc.ReadOnly.ROAdHocBookingList, OBLib', {
      PageSize: 1000,
      StartDate: start,
      EndDate: end
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROAdHocBookingList);
        for (var i = 0; i < ViewModel.ROAdHocBookingList().length - 2; ++i) {
          var NewBooking = ViewModel.ROAdHocBookingList()[i].Description();
          //ViewModel.ROProductionListPagingManager().SelectedItems().push(ViewModel.ROAdHocBookingList()[i]);
          //ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.push(ViewModel.ROAdHocBookingList()[i]);
          var ni = new SelectedItemObject()
          ni.ID(ViewModel.ROAdHocBookingList()[i].AdHocBookingID())
          ni.Description(ViewModel.ROAdHocBookingList()[i].Title())
          ViewModel.ROAdHocBookingListPagingManager().SelectedItems().push(ni);
          ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.push(ViewModel.ROAdHocBookingList()[i].AdHocBookingID());
          if (!ViewModel.Report().ReportCriteriaGeneric().AdHocBooking()) {
            ViewModel.Report().ReportCriteriaGeneric().AdHocBooking("");
          }
          if (ViewModel.Report().ReportCriteriaGeneric().AdHocBooking().trim().length > 0) {
            NewBooking = ', ' + NewBooking;
          };
          var newString = ViewModel.Report().ReportCriteriaGeneric().AdHocBooking() + NewBooking;
          newString = newString.replace(",,", "");
          if (newString.endsWith(",")) {
            newString = newString.substring(0, newString.length - 1);
          }
          ViewModel.Report().ReportCriteriaGeneric().AdHocBooking(newString);
        }
      }
      Singular.HideLoadingBar();
      ViewModel.ROAdHocBookingListCriteria().StartDate(start);
      ViewModel.ROAdHocBookingListCriteria().EndDate(end);
      ViewModel.ROAdHocBookingListPagingManager().Refresh();
    });
  }
  else {
    var newString = "";
    ViewModel.ROAdHocBookingListPagingManager().SelectedItems.Set([]);
    ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.Set([]);
    ViewModel.Report().ReportCriteriaGeneric().AdHocBooking(newString);
    ViewModel.ROAdHocBookingListCriteria().StartDate(start);
    ViewModel.ROAdHocBookingListCriteria().EndDate(end);
    ViewModel.ROAdHocBookingListPagingManager().Refresh();
  }

  //$("#BookingsPagedGrid tr").each(function () {
  //  //window.alert('line');
  //  $(this).click();
  //});
  //$("#SelectAllBookingsBtn").click();

  if (ViewModel.SelectAllAdHocBookings()) {
    $("#BookingsPagedGrid tr").each(function () {
      //window.alert('line');
      $(this).click();
    });
  }

};

//#region Clear Array Functions
function ClearHRs() {
  ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().HumanResources("");
}

function ClearDisciplines() {
  ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().Disciplines("");
}

function ClearProductionTypes() {
  ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().ProductionType("");
}

function ClearContractTypes() {
  ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().ContractTypes("");
}

function ClearProductionTypes() {
  ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().ProductionType("");
}

function ClearProvinces() {
  ViewModel.Report().ReportCriteriaGeneric().ProvinceIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().Provinces("");
}

function ClearCities() {
  ViewModel.Report().ReportCriteriaGeneric().HRCityIDs.Set([]);
  ViewModel.Report().ReportCriteriaGeneric().Cities("");
}
// #endregion

//#region Common Find Functions
function FindProvinces(element) {
  ROProvinces.Init();
  ROProvinces.Manager().SingleSelect(false);
  ROProvinces.Manager().MultiSelect(true);
  ROProvinces.SetOptions({
    AfterRowSelected: function (ROProvince) {

      ViewModel.Report().ReportCriteriaGeneric().ProvinceIDs.push(ROProvince.ProvinceID());
      var NewProvince = ROProvince.Province();
      if (!ViewModel.Report().ReportCriteriaGeneric().Provinces()) {
        ViewModel.Report().ReportCriteriaGeneric().Provinces("")
      }
      if (ViewModel.Report().ReportCriteriaGeneric().Provinces().trim().length > 0) {
        NewProvince = ', ' + NewProvince
      };
      var newString = ViewModel.Report().ReportCriteriaGeneric().Provinces() + NewProvince;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().Provinces(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      //ViewModel.Report().ReportCriteriaGeneric().ProvinceIDs.pop(DeSelectedItem.ID());
      var index = ViewModel.Report().ReportCriteriaGeneric().ProvinceIDs.indexOf(DeSelectedItem.ID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProvinceIDs.splice(index, 1);
      }
      var newString = ViewModel.Report().ReportCriteriaGeneric().Provinces().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      ViewModel.Report().ReportCriteriaGeneric().Provinces(newString);
    }
  });
  $('#ROProvinces').modal()
  ROProvinces.Manager().Refresh();
}
//#endregion

//#region Find Cities
function FindCities(element) {
  ROCities.Init();
  ROCities.Manager().SingleSelect(false);
  ROCities.Manager().MultiSelect(true);
  ROCities.SetOptions({
    AfterRowSelected: function (ROCity) {
      ViewModel.Report().ReportCriteriaGeneric().HRCityIDs.push(ROCity.CityID());
      var NewCity = ROCity.City();
      if (!ViewModel.Report().ReportCriteriaGeneric().Cities()) {
        ViewModel.Report().ReportCriteriaGeneric().Cities("")
      }
      if (ViewModel.Report().ReportCriteriaGeneric().Cities().trim().length > 0) {
        NewCity = ', ' + NewCity
      };
      var newString = ViewModel.Report().ReportCriteriaGeneric().Cities() + NewCity;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().Cities(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      //ViewModel.Report().ReportCriteriaGeneric().HRCityIDs.pop(DeSelectedItem.ID());
      var index = ViewModel.Report().ReportCriteriaGeneric().HRCityIDs.indexOf(DeSelectedItem.ID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().HRCityIDs.splice(index, 1);
      }
      var newString = ViewModel.Report().ReportCriteriaGeneric().Cities().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      ViewModel.Report().ReportCriteriaGeneric().Cities(newString);
    }
  });
  $('#ROCities').modal()
  ROCities.Manager().Refresh()
}
//#endregion

function FindEquipments(element) {
  ROEquipments.Init();
  ROEquipments.Manager().SingleSelect(false);
  ROEquipments.Manager().MultiSelect(true);
  ROEquipments.SetOptions({
    AfterRowSelected: function (ROEquipment) {
      var EquipmentsXMLString = ROEquipments.getXMLString(ROEquipments.Manager().SelectedItems())
      ViewModel.ROEquipmentScheduleListCriteria().EquipmentIDs(EquipmentsXMLString);
      ViewModel.ROEquipmentScheduleListPagingManager().Refresh();
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      var EquipmentsXMLString = ROEquipments.getXMLString(ROEquipments.Manager().SelectedItems())
      ViewModel.ROEquipmentScheduleListCriteria().EquipmentIDs(EquipmentsXMLString);
      ViewModel.ROEquipmentScheduleListPagingManager().Refresh();
    }
  });
  $('#ROEquipmentScheduleSelectModal').dialog({
    title: 'Select Bookings',
    height: '600px',
    width: '1024px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROEquipments.RefreshList();
    }
  });
}

//function FindTeam(element) {
//  ROTeams.Init();
//  ROTeams.Manager().SingleSelect(true);
//  ROTeams.Manager().MultiSelect(false);
//  ROTeams.SetOptions({
//    AfterRowSelected: function (ROTeam) {
//      ViewModel.Report().ReportCriteriaGeneric().TeamID(ROTeam.TeamID());
//      ViewModel.Report().ReportCriteriaGeneric().Team(ROTeam.TeamName());
//    },
//    AfterRowDeSelected: function (DeSelectedItem) {
//      ViewModel.Report().ReportCriteriaGeneric().TeamID(null);
//      ViewModel.Report().ReportCriteriaGeneric().TeamName("");
//    }
//  });
//  $('#ROTeamListModal').dialog({
//    title: 'Select Team',
//    height: '500px',
//    width: '900px',
//    position: {
//      my: 'left top',
//      at: 'right top',
//      of: $(element)
//    },
//    open: function (event, ui) {
//      ROTeams.RefreshList();
//    }
//  });
//}

//#region Find HR Booking
function FindHRBooking(element) {
  ROAdHocBookings.Init();
  ROAdHocBookings.Manager().SingleSelect(true);
  ROAdHocBookings.Manager().MultiSelect(false);
  ROAdHocBookings.SetOptions({
    AfterRowSelected: function (ROAdHocBooking) {
      ViewModel.Report().ReportCriteriaGeneric().AdHocBookingID(ROAdHocBooking.AdHocBookingID());
      ViewModel.Report().ReportCriteriaGeneric().AdHocBooking(ROAdHocBooking.Title());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().AdHocBookingID(null);
      ViewModel.Report().ReportCriteriaGeneric().AdHocBooking("");
    }
  });
  $('#ROAdHocBookingListModal').modal()
  ROAdHocBookings.Manager().Refresh();
}
//#endregion

function FindMultipleHRBookings(element) {
  ROAdHocBookings.Init();
  ROAdHocBookings.Manager().SingleSelect(false);
  ROAdHocBookings.Manager().MultiSelect(true);
  ROAdHocBookings.SetOptions({
    AfterRowSelected: function (ROAdHocBooking) {
      ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.push(ROAdHocBooking.AdHocBookingID());
      var NewBooking = ROAdHocBooking.Description();
      if (!ViewModel.Report().ReportCriteriaGeneric().AdHocBooking()) {
        ViewModel.Report().ReportCriteriaGeneric().AdHocBooking("");
      }
      if (ViewModel.Report().ReportCriteriaGeneric().AdHocBooking().trim().length > 0) {
        NewBooking = ', ' + NewBooking;
      };
      var nb = (NewBooking.length > 0) ? NewBooking : '';
      var newString = ViewModel.Report().ReportCriteriaGeneric().AdHocBooking() + nb;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().AdHocBooking(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      //ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.pop(DeSelectedItem.ID());
      ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs.Set([])
      ViewModel.ROAdHocBookingListPagingManager().SelectedItems().Iterate(function (si, siInd) {
        ViewModel.Report().ReportCriteriaGeneric().AdHocBookingIDs().push(si.ID())
      })
      var newString = "";
      if (ViewModel.Report().ReportCriteriaGeneric().AdHocBooking() != null)
        newString = ViewModel.Report().ReportCriteriaGeneric().AdHocBooking().toString().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      if (newString.endsWith(" ,")) {
        newString = newString.substring(0, newString.length - 2);
      }
      ViewModel.Report().ReportCriteriaGeneric().AdHocBooking(newString);
    }
  });
  $('#ROAdHocBookingListSelectAllModal').modal();
  //$('#ROAdHocBookingListSelectAllModal').dialog({
  //  title: 'Select Ad Hoc Booking(s)',
  //  height: '600px',
  //  width: '1024px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ViewModel.SelectAllAdHocBookings(false);
  //    ViewModel.ROAdHocBookingListPagingManager().SelectedItems.Set([]);
  //    ViewModel.Report().ReportCriteriaGeneric().AdHocBooking('');
  //    ROProductions.RefreshList();
  //  }
  //});
}

function FindSingleHR(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.PreferredFirstSurname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
      ViewModel.Report().ReportCriteriaGeneric().HumanResource("");
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select Human Resource',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindCity(element) {
  ROCities.Init();
  ROCities.Manager().SingleSelect(true);
  ROCities.Manager().MultiSelect(false);
  ROCities.SetOptions({
    AfterRowSelected: function (ROCity) {
      ViewModel.Report().ReportCriteriaGeneric().CityID(ROCity.CityID());
      ViewModel.Report().ReportCriteriaGeneric().City(ROCity.City());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().CityID(null);
      ViewModel.Report().ReportCriteriaGeneric().City('');
    }
  });
  $('#ROCityModal').dialog({
    title: 'Select City',
    height: '500px',
    width: '900px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROCities.RefreshList();
    }
  });
}

function FindCrewMember(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.PreferredFirstSurname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
      ViewModel.Report().ReportCriteriaGeneric().HumanResource("");
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select Crew Member',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindMultipleHR(element, options) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(false);
  ROHumanResources.Manager().MultiSelect(true);
  if (options) {
    ROHumanResources.SetOptions({
      AfterRowSelected: function (ROHumanResource) { options.AfterRowSelected(ROHumanResource) },
      AfterRowDeSelected: function (ROHumanResource) { options.AfterRowDeSelected(ROHumanResource) }
    });
  } else {
    ROHumanResources.SetOptions({
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.PreferredFirstSurname());
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        ViewModel.Report().ReportCriteriaGeneric().HumanResource().replace(DeSelectedItem.Description() + ",", "")
      }
    });
  };
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select Human Resources',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindICRHR(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.Criteria().SystemID(ViewModel.CurrentUserSystemID());
  ROHumanResources.Criteria().ProductionAreaID(ViewModel.CurrentUserProductionAreaID());
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.PreferredFirstSurname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
      ViewModel.Report().ReportCriteriaGeneric().HumanResource("");
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select HR',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindProductionHR(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.Criteria().ProductionAreaID(2);
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.PreferredFirstSurname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
      ViewModel.Report().ReportCriteriaGeneric().HumanResource();
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select HR',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindManagerHR(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.Criteria().ManagerInd(true);
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().ManagerHumanResourceID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().ManagerHumanResource(ROHumanResource.Firstname() + " " + ROHumanResource.Surname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().ManagerHumanResourceID(null);
      ViewModel.Report().ReportCriteriaGeneric().ManagerHumanResource("");
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select HR',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindEventManager(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.Criteria().DisciplineID(7);
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.Firstname() + " " + ROHumanResource.Surname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
      ViewModel.Report().ReportCriteriaGeneric().HumanResource("");
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select HR',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindUnitSupervisor(element) {
  ROHumanResources.Init();
  ROHumanResources.Manager().SingleSelect(true);
  ROHumanResources.Manager().MultiSelect(false);
  ROHumanResources.Criteria().DisciplineID(11);
  ROHumanResources.SetOptions({
    AfterRowSelected: function (ROHumanResource) {
      ViewModel.Report().ReportCriteriaGeneric().UnitSupervisorID(ROHumanResource.HumanResourceID());
      ViewModel.Report().ReportCriteriaGeneric().UnitSupervisor(ROHumanResource.Firstname() + " " + ROHumanResource.Surname());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().UnitSupervisorID(null);
      ViewModel.Report().ReportCriteriaGeneric().UnitSupervisor("");
    }
  });
  $('#ROHumanResourceListModal').modal();
  ROHumanResources.Manager().Refresh();
  //$('#ROHumanResourceListModal').dialog({
  //  title: 'Select HR',
  //  height: '500px',
  //  width: '900px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROHumanResources.RefreshList();
  //  }
  //});
}

function FindProduction(element, options) {
  ROProductions.Init();
  ROProductions.Manager().SingleSelect(true);
  ROProductions.Manager().MultiSelect(false);
  if (options) {
    ROProductions.SetOptions({
      AfterRowSelected: function (ROProduction) { options.AfterRowSelected(ROProduction) },
      AfterRowDeSelected: function (ROProduction) { options.AfterRowDeSelected(ROProduction) }
    });
  } else {
    ROProductions.SetOptions({
      AfterRowSelected: function (ROProduction) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionID(ROProduction.ProductionID());
        ViewModel.Report().ReportCriteriaGeneric().Production(ROProduction.ProductionDescription());
      },
      AfterRowDeSelected: function (ROProduction) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
        ViewModel.Report().ReportCriteriaGeneric().Production("");
      }
    });
  };
  $('#ROProductionListModal').modal();
  //$('#ROProductionListModal').dialog({
  //  title: 'Select Production',
  //  height: '600px',
  //  width: '1024px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ROProductions.RefreshList();
  //  }
  //});
}

function FindMultipleProductions(element, options) {
  ROProductions.Init();
  ROProductions.Manager().SingleSelect(false);
  ROProductions.Manager().MultiSelect(true);
  ROProductions.SetOptions({
    AfterRowSelected: function (ROProduction) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionIDs.push(ROProduction.ProductionID());
      var NewProduction = ROProduction.ProductionDescription();
      if (!ViewModel.Report().ReportCriteriaGeneric().Production()) {
        ViewModel.Report().ReportCriteriaGeneric().Production("");
      }
      if (ViewModel.Report().ReportCriteriaGeneric().Production().trim().length > 0) {
        NewProduction = ', ' + NewProduction;
      };
      var newString = ViewModel.Report().ReportCriteriaGeneric().Production() + NewProduction;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().Production(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      //ViewModel.Report().ReportCriteriaGeneric().ProductionIDs.pop(DeSelectedItem.ID());
      ViewModel.Report().ReportCriteriaGeneric().ProductionIDs.Set([])
      ViewModel.ROProductionListPagingManager().SelectedItems().Iterate(function (si, siInd) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionIDs().push(si.ID())
      })
      var oldArray = ViewModel.Report().ReportCriteriaGeneric().ProductionIDs();
      var index = oldArray.indexOf(DeSelectedItem.ID());
      if (index != -1) {
        oldArray.splice(index, 1);
      };
      ViewModel.Report().ReportCriteriaGeneric().ProductionIDs(oldArray);

      var newString = "";
      if (ViewModel.Report().ReportCriteriaGeneric().Production() != null)
        newString = ViewModel.Report().ReportCriteriaGeneric().Production().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      if (newString.endsWith(" ,")) {
        newString = newString.substring(0, newString.length - 2);
      }
      ViewModel.Report().ReportCriteriaGeneric().Production(newString);
    }
  });
  $('#ROProductionListSelectAllModal').modal();
  ROProductions.Manager().Refresh();
  //$('#ROProductionListSelectAllModal').dialog({
  //  title: 'Select Production(s)',
  //  height: '600px',
  //  width: '1024px',
  //  position: {
  //    my: 'left top',
  //    at: 'right top',
  //    of: $(element)
  //  },
  //  open: function (event, ui) {
  //    ViewModel.SelectAllProductions(false);
  //    ViewModel.ROProductionListPagingManager().SelectedItems.Set([]);
  //    ViewModel.Report().ReportCriteriaGeneric().Production('');
  //    ROProductions.RefreshList();
  //  }
  //});
}

function FindMultipleProductionTypes(element, options) {
  ROProductions.Init();
  ROProductions.Manager().SingleSelect(false);
  ROProductions.Manager().MultiSelect(true);
  ROProductions.SetOptions({
    AfterRowSelected: function (ROProductionType) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.push(ROProductionType.ProductionTypeID());
      var NewProductionType = ROProductionType.ProductionType();
      if (!ViewModel.Report().ReportCriteriaGeneric().ProductionType()) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionType("");
      }
      if (ViewModel.Report().ReportCriteriaGeneric().Production().trim().length > 0) {
        NewProduction = ', ' + NewProduction;
      };
      var newString = ViewModel.Report().ReportCriteriaGeneric().ProductionType() + NewProductionType;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().ProductionType(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.pop(DeSelectedItem.ID());
      var newString = "";
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionType() != null)
        newString = ViewModel.Report().ReportCriteriaGeneric().ProductionType().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      if (newString.endsWith(" ,")) {
        newString = newString.substring(0, newString.length - 2);
      }
      ViewModel.Report().ReportCriteriaGeneric().ProductionType(newString);
    }
  });
  $('#ROProductionTypes').dialog({
    title: 'Select Production Type(s)',
    height: '600px',
    width: '1024px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
    }
  });
}

function FindProductionType(element) {
  ROProductionTypes.Init();
  ROProductionTypes.Manager().SingleSelect(true);
  ROProductionTypes.Manager().MultiSelect(false);
  ROProductionTypes.SetOptions({
    AfterRowSelected: function (ROProductionType) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(ROProductionType.ProductionTypeID());
      ViewModel.Report().ReportCriteriaGeneric().ProductionType(ROProductionType.ProductionType());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
      ViewModel.Report().ReportCriteriaGeneric().ProductionType("");
    }
  });
  $('#ROProductionTypes').dialog({
    title: 'Select Production Type',
    height: '500px',
    width: '900px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROProductionTypes.RefreshList();
    }
  });
}

function FindEventTypeProductionType(element) {
  ROEventTypes.Init();
  ROEventTypes.Manager().SingleSelect(true);
  ROEventTypes.Manager().MultiSelect(false);
  ROEventTypes.SetOptions({
    AfterRowSelected: function (ROEventType) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(ROEventType.ProductionTypeID());
      //ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(ROEventType.EventTypeID());    
      ViewModel.Report().ReportCriteriaGeneric().EventType(ROEventType.EventType());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
      //ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
      ViewModel.Report().ReportCriteriaGeneric().EventType("");
    }
  });
  $('#ROEventTypesModal').dialog({
    title: 'Select Production Type (Event Type)',
    height: '600px',
    width: '800px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROEventTypes.RefreshList();
    }
  });
}

function FindEventType(element) {
  ROEventTypes.Init();
  ROEventTypes.Manager().SingleSelect(true);
  ROEventTypes.Manager().MultiSelect(false);
  ROEventTypes.SetOptions({
    AfterRowSelected: function (ROEventType) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(ROEventType.EventTypeID());
      ViewModel.Report().ReportCriteriaGeneric().EventType(ROEventType.EventType());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
      ViewModel.Report().ReportCriteriaGeneric().EventType("");
    }
  });
  $('#ROEventTypesModal').dialog({
    title: 'Select Production Type',
    height: '600px',
    width: '800px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROEventTypes.RefreshList();
    }
  });
}

function FindRoom(element) {
  RORoomListPaged.Init();
  RORoomListPaged.Manager().SingleSelect(true);
  RORoomListPaged.Manager().MultiSelect(false);
  RORoomListPaged.SetOptions({
    AfterRowSelected: function (RORoom) {
      ViewModel.Report().ReportCriteriaGeneric().RoomID(RORoom.RoomID());
      ViewModel.Report().ReportCriteriaGeneric().Room(RORoom.Room());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().RoomID(null);
      ViewModel.Report().ReportCriteriaGeneric().Room("");
    }
  });
  $('#RORoomListModal').dialog({
    title: 'Select Room',
    height: '500px',
    width: '900px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      RORoomListPaged.RefreshList();
    }
  });
}

function FindSingleDiscipline(element) {
  RODisciplines.Init();
  RODisciplines.Manager().SingleSelect(true);
  RODisciplines.Manager().MultiSelect(false);
  RODisciplines.SetOptions({
    AfterRowSelected: function (RODiscipline) {
      ViewModel.Report().ReportCriteriaGeneric().DisciplineID(RODiscipline.DisciplineID());
      ViewModel.Report().ReportCriteriaGeneric().Discipline(RODiscipline.Discipline());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().DisciplineID(null);
      ViewModel.Report().ReportCriteriaGeneric().Discipline("");
    }
  });
  $('#RODisciplineModal').dialog({
    title: 'Select Discipline',
    height: '500px',
    width: '900px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      RODisciplines.RefreshList();
    }
  });
}

//#region Find Multiple Disciplines
function FindMultipleDisciplines(element) {
  RODisciplines.Init();
  RODisciplines.Manager().SingleSelect(false);
  RODisciplines.Manager().MultiSelect(true);
  RODisciplines.SetOptions({
    AfterRowSelected: function (RODiscipline) {
      ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.push(RODiscipline.DisciplineID());
      var NewDiscipline = RODiscipline.Discipline();
      if (!ViewModel.Report().ReportCriteriaGeneric().Disciplines()) {
        ViewModel.Report().ReportCriteriaGeneric().Disciplines("")
      }
      if (ViewModel.Report().ReportCriteriaGeneric().Disciplines().trim().length > 0) {
        NewDiscipline = ', ' + NewDiscipline
      };
      var newString = ViewModel.Report().ReportCriteriaGeneric().Disciplines() + NewDiscipline;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().Disciplines(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.Set([]);
      var newString = ViewModel.Report().ReportCriteriaGeneric().Disciplines().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      newString = newString.replace(" ,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      if (newString.toString().charAt(0) == ',' || newString.toString().charAt(0) == ' ') {
        newString = newString.substring(1, newString.length);
      }
      ViewModel.Report().ReportCriteriaGeneric().Disciplines(newString);
    }
  });
  $('#RODisciplines').modal();
  RODisciplines.Manager().Refresh();
}
//#endregion

function FindMultipleContractTypes(element) {
  ROContractTypes.Init();
  ROContractTypes.Manager().SingleSelect(false);
  ROContractTypes.Manager().MultiSelect(true);
  ROContractTypes.SetOptions({
    AfterRowSelected: function (ROContractType) {
      ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.push(ROContractType.ContractTypeID());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.Set([]);
    }
  });
  $('#ROContractTypeModal').dialog({
    title: 'Select Contract Type',
    height: '500px',
    width: '900px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROContractTypes.RefreshList();
    }
  });
}

//#endregion Common Find Functions

//#region Reports
//#region Productions Budgeted Report
ProductionsBudgetedReport = {
  AddProductionType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(obj.ProductionTypeID())
      ProductionsBudgetedReport.GetAllowedEventTypes();
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
    }
  },
  GetAllowedEventTypes: function () {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport, OBLib', {
      PageSize: 1000,
      ProductionTypeID: ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(),
      KeyWord: ViewModel.Report().ReportCriteriaGeneric().EventTypes()
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredEventTypeReportList);
        var tempList = ViewModel.ROFilteredEventTypeReportList();
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set([]);
        tempList.Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.push(itm);
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
  AddEventType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(obj.EventTypeID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
  }
};
//#endregion

HumanResourceDetailsReport = {
  FindHR: function (element) {
    FindSingleHR(element);
  },
  GetDiscipline: function (element) {
    FindSingleDiscipline(element);
  }
};

CreditorInvoicesReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID())
      CreditorInvoicesReport.GetAllowedHumanResources();
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddHumanResource: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(obj.HumanResourceID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
    }
  },
  GetAllowedHumanResources: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID()
    }, function (args) {
      if (args.Success) {
        $("#HumanResources").addClass('FadeDisplay');
        $("#HumanResourceFilters").addClass('FadeDisplay');
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(args.Data);
      }
      ViewModel.IsReportBusy(false);
    })

  }
};
//#endregion

//#region Creditor Invoice Details Report
CreditorInvoiceDetailsReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
      CreditorInvoiceDetailsReport.GetAllowedHumanResources();
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddHumanResource: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(obj.HumanResourceID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
    }
  },
  GetAllowedHumanResources: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID()
    }, function (args) {
      if (args.Success) {
        $("#HumanResources").addClass('FadeDisplay');
        $("#HumanResourceFilters").addClass('FadeDisplay');
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(args.Data);
      }
      ViewModel.IsReportBusy(false);
    });
  }
};
//#endregion

ProductionHRCostDetailsReport = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

ProductionHRCostReport = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

ISPTimesheetWithRatesReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

ProductionCostReportLeon = {
  FindProductionType: function (element) {
    FindProductionType(element);
  },
  FindProduction: function (element) {
    FindProduction(element);
  },
  FindEventType: function (element) {
    FindEventTypeProductionType(element);
  }
};

ISPTimesheetReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

TempProductionCostReport = {
  FindProductionType: function (element) {
    FindProductionType(element);
  },
  FindProduction: function (element) {
    FindProduction(element)
  },
  FindEventType: function (element) {
    FindEventTypeProductionType(element);
  }
};

RateUpdateReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

SnTMonthlyPerPersonPerProductionReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  },
  FindProduction: function (element, start, end) {
    ViewModel.ROProductionListPagingManager().PageSize(100);
    ViewModel.ROProductionListCriteria().TxDateFrom(start);
    ViewModel.ROProductionListCriteria().TxDateTo(end);
    //FindProduction(element);
    FindMultipleProductions(element);
  },
  FindHRBooking: function (element, start, end) {
    ViewModel.ROAdHocBookingListPagingManager().PageSize(100);
    ViewModel.ROAdHocBookingListCriteria().StartDate(start);
    ViewModel.ROAdHocBookingListCriteria().EndDate(end);
    //FindHRBooking(element);
    FindMultipleHRBookings(element);
  },
  SetStatProdCriteria: function (statprod) {
    ViewModel.ROAdHocBookingListCriteria().ProductionServicesInd(statprod.ProductionServicesInd());
    ViewModel.ROAdHocBookingListCriteria().ProductionContentInd(statprod.ProductionContentInd());
    ViewModel.ROAdHocBookingListCriteria().OutsideBroadcastInd(statprod.OutsideBroadcastInd());
    ViewModel.ROAdHocBookingListCriteria().StudioInd(statprod.StudioInd());
    ViewModel.ROAdHocBookingListCriteria().SystemID(statprod.SystemID());
    ViewModel.ROAdHocBookingListCriteria().ProductionAreaID(statprod.ProductionAreaID());
  },
  ShowProductions: function (status) {
    status.ReportForProductionsInd(!status.ReportForProductionsInd());
    if (status.ReportForProductionsInd()) {
      $("#SNTProductionsFieldSet").addClass('FadeDisplay');
    } else {
      $("#SNTProductionsFieldSet").removeClass('FadeDisplay');
      $("#SNTProductionsFieldSet").addClass('FadeHide');
    }
  },
  ShowAdHoc: function (status) {
    status.ReportForAdHocInd(!status.ReportForAdHocInd());
    if (status.ReportForAdHocInd()) {
      $("#SNTAdHocBookingsFieldSet").addClass('FadeDisplay');
    } else {
      $("#SNTAdHocBookingsFieldSet").removeClass('FadeDisplay');
      $("#SNTAdHocBookingsFieldSet").addClass('FadeHide');
    }
  }
};

CrewSchedulePerProductionReport = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

EventManagerPlanningHoursReport = {
  FindEventManager: function (element) {
    FindEventManager(element);
  }
};

MonthlySubsistenceandTravelAllowanceReport = {
  FindHR: function (element) {
    FindSingleHR(element);
  },
};

ProductionPlanningHours = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

ProductionCostsReport = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

PettyCashReport = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

ProductionFacilitiesBookingsReport = {
  FindHR: function (element) {
    FindProductionHR(element)
  }
};

StaffShiftDetailsReport = {
  FindHR: function (element) {
    FindProductionHR(element);
  }
}

ProductionServicesStudioSMSReport = {
  FindRoom: function (element) {
    FindRoom(element);
  },

  FindHR: function (element) {
    FindSingleHR(element);
  }
};

MonthlyScheduleByIndividualReport = {
  FindHR: function (element) {
    FindSingleHR(element);
  }
}

ProductionMonthlyShiftsReport = {
  FindHR: function (element) {
    FindSingleHR(element);
  }
};

HRTravelDetailReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

TravelRecProductionServicesReport = {
  FindProduction: function (element) {
    FindProduction(element, {
      AfterRowSelected: function (ROProduction) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionID(ROProduction.ProductionID());
        ViewModel.Report().ReportCriteriaGeneric().Production(ROProduction.ProductionDescription());
      },
      AfterRowDeSelected: function (ROProduction) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
        ViewModel.Report().ReportCriteriaGeneric().Production("");
      }
    });
  }
};

HRTimeSheets = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

HRLeave = {
  FindHR: function (element) {
    FindSingleHR(element);
  }
};

HRContractTypes = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

HRRates = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

HRSkillsNew = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

HoursBookedReport = {
  ClearContractTypes: function () {
    ClearContractTypes();
  },
  ClearDisciplines: function () {
    ClearDisciplines();
  },
  GetDiscipline: function (element) {
    FindMultipleDisciplines(element);
  },
  GetContractType: function (element) {
    FindMultipleContractTypes(element);
  },
  FindHR: function (element) {
    FindSingleHR(element);
  }
};

HRSnTReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  },
  ClearHR: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs([]);
    ViewModel.Report().ReportCriteriaGeneric().HumanResources("");
  }
};

HRShiftsReport = {
  FindHR: function (element) {
    FindSingleHR(element);
  }
};

HRSkillLevelsReport = {
  FindProductionType: function (element) {
    FindProductionType(element);
  }
};

HRSkillsReport = {
  FindProductionType: function (element) {
    FindProductionType(element);
  }
};

ProductionHRShiftDetailReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  }
};

//#region Annual Leave Report
AnnualLeaveReport = {
  AddSystem: function (obj) {
    if (obj.IsSelected()) {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs().indexOf(obj.SystemID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs().splice(index, 1);
      }
    }
    else {
      if (ViewModel.Report().ReportCriteriaGeneric().SystemIDs().indexOf(obj.SystemID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs().push(obj.SystemID());
      }
    }
    obj.IsSelected(!obj.IsSelected());
  },
  AddProductionArea: function (obj) {
    if (obj.IsSelected()) {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().indexOf(obj.ProductionAreaID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().splice(index, 1);
      }
    }
    else {
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().indexOf(obj.ProductionAreaID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().push(obj.ProductionAreaID());
      }
    }
    obj.IsSelected(!obj.IsSelected());
  },
  AddUnitSupervisor: function (obj) {
    if (obj.IsSelected()) {
      var index = ViewModel.Report().ReportCriteriaGeneric().UnitSupervisorIDs().indexOf(obj.HumanResourceID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().UnitSupervisorIDs().splice(index, 1);
      }
    }
    else {
      if (ViewModel.Report().ReportCriteriaGeneric().UnitSupervisorIDs().indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().UnitSupervisorIDs().push(obj.HumanResourceID());
      }
    }
    obj.IsSelected(!obj.IsSelected());
  },
  AddManager: function (obj) {
    if (obj.IsSelected()) {
      var index = ViewModel.Report().ReportCriteriaGeneric().ManagerIDs().indexOf(obj.HumanResourceID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ManagerIDs().splice(index, 1);
      }
    }
    else {
      if (ViewModel.Report().ReportCriteriaGeneric().ManagerIDs().indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ManagerIDs().push(obj.HumanResourceID());
      }
    }
    obj.IsSelected(!obj.IsSelected())
  }
};
//#endregion

//#region Human Resource Skills Report
HumanResourceSkillsReport = {
  AddSystem: function (obj) {
    if (obj.IsSelected()) {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs().indexOf(obj.SystemID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs().splice(index, 1);
      }
    }
    else {
      if (ViewModel.Report().ReportCriteriaGeneric().SystemIDs().indexOf(obj.SystemID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs().push(obj.SystemID());
      }
    }
    obj.IsSelected(!obj.IsSelected());
  },
  AddProductionArea: function (obj) {
    if (obj.IsSelected()) {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().indexOf(obj.ProductionAreaID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().splice(index, 1);
      }
    }
    else {
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().indexOf(obj.ProductionAreaID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().push(obj.ProductionAreaID());
      }
    }
    obj.IsSelected(!obj.IsSelected());
  },
  AddProductionType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(obj.ProductionTypeID());
    } else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
    }
  }
};
//#endregion

//#region Ad Hoc Travel
AdHocTravel = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index != undefined) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index != undefined) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  FindAdHocBooking: function (element, start, end) {
    ViewModel.ROAdHocBookingListPagingManager().PageSize(100);
    ViewModel.ROAdHocBookingListCriteria().StartDate(start);
    ViewModel.ROAdHocBookingListCriteria().EndDate(end);
    ViewModel.ROAdHocBookingListCriteria().SystemID();
    ViewModel.ROAdHocBookingListCriteria().ProductionAreaID();
    FindHRBooking(element);
  },
  ClearBooking: function () {
    ViewModel.Report().ReportCriteriaGeneric().AdHocBookingID(null);
    ViewModel.Report().ReportCriteriaGeneric().AdHocBooking('');
  }
};
//#endregion

//#region HR Timesheet Report
HRTimeSheetReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.ROHumanResourceFindListCriteria().SystemID(obj.SystemID());
    } else {
      ViewModel.ROHumanResourceFindListCriteria().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.ROHumanResourceFindListCriteria().ProductionAreaID(obj.ProductionAreaID());
    } else {
      ViewModel.ROHumanResourceFindListCriteria().ProductionAreaID(null);
    }
  },
  AddHumanResource: function (obj) {
    ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
      itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
    });
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(obj.HumanResourceID());
    } else {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
    }
  }
};
//#endregion

//#region Production Status Report
ProductionStatusReport = {
  AddProductionType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList().Iterate(function (itm, ind) {
        /*prevent multi-select on butttons :- 
          is item selected ? (yes) change to deselected : (no) keep item state unchanged
        */
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(obj.ProductionTypeID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
    }
  },
  FindCrewMember: function () {
    FindHRControl.show();
    FindHRControl.refresh();
  },
  ClearHR: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
    ViewModel.Report().ReportCriteriaGeneric().HumanResource('');
  }
};
//#endregion

//#region Production Schedule Report
ProductionScheduleReport = {
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
    }
  },
};
//#endregion

//#region Weekend Production Report
WeekendProductionReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID())
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  }
};
//#endregion

BoardReport = {
  FindProductionTypeEventType: function (element) {
    FindEventTypeProductionType(element);
  }
};

CrewScheduleReport = {
  FindProduction: function (element) {
    FindProduction(element);
  },
  FindCrewMember: function (element) {
    FindCrewMember(element);
  }
};

ProductionPAFormReport = {
  FindProduction: function (element) {
    FindProduction(element);
  }
};

HumanResourceSnTReport = {
  FindProduction: function (element, start, end) {
    ViewModel.ROProductionListPagingManager().PageSize(100);
    ViewModel.ROProductionListCriteria().TxDateFrom(start);
    ViewModel.ROProductionListCriteria().TxDateTo(end);
    // FindProductionSelectAll(element);
    FindMultipleProductions(element);
  },
  FindHRBooking: function (element, start, end) {
    ViewModel.ROAdHocBookingListPagingManager().PageSize(100);
    ViewModel.ROAdHocBookingListCriteria().StartDate(start);
    ViewModel.ROAdHocBookingListCriteria().EndDate(end);
    //FindHRBooking(element);
    FindMultipleHRBookings(element);
  },
  SetStatProdCriteria: function (statprod) {
    ViewModel.ROAdHocBookingListCriteria().ProductionServicesInd(statprod.ProductionServicesInd());
    ViewModel.ROAdHocBookingListCriteria().ProductionContentInd(statprod.ProductionContentInd());
    ViewModel.ROAdHocBookingListCriteria().OutsideBroadcastInd(statprod.OutsideBroadcastInd());
    ViewModel.ROAdHocBookingListCriteria().StudioInd(statprod.StudioInd());
    ViewModel.ROAdHocBookingListCriteria().SystemID(statprod.SystemID());
    ViewModel.ROAdHocBookingListCriteria().ProductionAreaID(statprod.ProductionAreaID());
  },
  ShowProductions: function (status) {
    status.ReportForProductionsInd(!status.ReportForProductionsInd());
    if (status.ReportForProductionsInd()) {
      $("#ProductionsFieldSet").addClass('FadeDisplay');
    } else {
      $("#ProductionsFieldSet").removeClass('FadeDisplay');
      $("#ProductionsFieldSet").addClass('FadeHide');
    }
  },
  ShowAdHoc: function (status) {
    status.ReportForAdHocInd(!status.ReportForAdHocInd());
    if (status.ReportForAdHocInd()) {
      $("#AdHocBookingsFieldSet").addClass('FadeDisplay');
    } else {
      $("#AdHocBookingsFieldSet").removeClass('FadeDisplay');
      $("#AdHocBookingsFieldSet").addClass('FadeHide');
    }
  }
};

SkillsDatabaseReport = {
  FindHR: function (element) {
    FindSingleHR(element);
  },
  FindProductionType: function (element) {
    FindProductionType(element);
  }
  //FindRoom: function (element) {
  //  FindRoom(element);
  //}
};

EquipmentBookingReport = {
  SelectBookings: function (element) {
    FindEquipments(element);
  }
};

DisciplinesBookedForStudiosReport = {
  ClearHRs: function () {
    ClearHRs();
  },
  FindHR: function (element) {
    FindMultipleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  },
  ClearDisciplines: function (element) {
    ClearDisciplines();
  },
  GetDiscipline: function (element) {
    FindMultipleDisciplines(element);
  }
};

//#region Human Resource Accreditation Report
HumanResourceAccreditationReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID())
      HumanResourceAccreditationReport.GetAllowedHumanResources();
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID())
      HumanResourceAccreditationReport.GetAllowedHumanResources();
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddAllHR: function (obj) {
    obj.SelectAllInd(!obj.SelectAllInd());
    if (obj.SelectAllInd()) {
      ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(itm.HumanResourceID());
        ViewModel.Report().ReportCriteriaGeneric().HumanResources.push(itm);
        itm.IsSelected(true);
      });
    } else {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.Set([]);
      ViewModel.Report().ReportCriteriaGeneric().HumanResources.Set([]);
      ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
        itm.IsSelected(false);
      });
    }
  },
  AddHumanResource: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(obj.HumanResourceID());
      }
      if (ViewModel.Report().ReportCriteriaGeneric().HumanResources.indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResources.push(obj);
      }
    }
    else {
      var HR = ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Find('HumanResourceID', obj.HumanResourceID());
      if (HR) {
        HR.IsSelected(false);
      }
      var indexofID = ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID());
      if (indexofID != -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.splice(indexofID, 1);
      }
      var indexOfObj = $.map(ViewModel.Report().ReportCriteriaGeneric().HumanResources(), function (x, index) {
        if (x.HumanResourceID === obj.HumanResourceID) {
          return index;
        }
      });
      if (indexOfObj != -1) { //Item is in list if index != -1, so we can remove it at that index
        ViewModel.Report().ReportCriteriaGeneric().HumanResources.splice(indexOfObj, 1);
      }
    }
  },
  GetAllowedHumanResources: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID()
    }, function (args) {
      if (args.Success) {
        $("#HumanResources").addClass('FadeDisplay');
        $("#SelectedHumanResources").addClass('FadeDisplay');
        $("#HumanResourceFilters").addClass('FadeDisplay');
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(args.Data);
        ViewModel.Report().ReportCriteriaGeneric().HumanResources().Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm2, ind2) {
            if (itm.HumanResourceID() == itm2.HumanResourceID()) {
              itm2.IsSelected(true);
            }
          });
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
};
//#endregion

//#region Production Content - Travel Req Report
ProductionContentTravelReqReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index != undefined) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index != undefined) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
    }
  }
};
//#endregion

//#region Monthly Production Crew Schedule
MonthlyProductionCrewScheduleReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddEventManager: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROEventManagerList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().EventManagerHumanResourceID(obj.HumanResourceID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().EventManagerHumanResourceID(null);
    }
  },
  AddProductionType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(obj.ProductionTypeID());
      this.GetAllowedEventTypes();
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
    }
  },
  GetAllowedEventTypes: function () {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport, OBLib', {
      PageSize: 1000,
      ProductionTypeID: ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(),
      KeyWord: ViewModel.Report().ReportCriteriaGeneric().EventTypes(),
      SortColumn: "EventType"
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredEventTypeReportList);
        var tempList = ViewModel.ROFilteredEventTypeReportList();
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set([]);
        tempList.Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.push(itm);
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
  AddEventType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(obj.EventTypeID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
  }
};
//#endregion

//#region Easy Reports
//#region Bookings by Discipline
BookingsByDiscipline = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    } else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  }
};
//#endregion
//#region JC Dashboard
JCDashboard = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      }
      )
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  }
};
//#endregion
//#region Human Resource Details
HumanResourceDetails = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      }
      )
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
      HumanResourceDetails.GetAllowedHumanResources();
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddHumanResource: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      }
      )
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(obj.HumanResourceID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
    }
  },
  GetAllowedHumanResources: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(),
      FirstName: ViewModel.Report().ReportCriteriaGeneric().FirstName(),
      Surname: ViewModel.Report().ReportCriteriaGeneric().Surname(),
      PreferredName: ViewModel.Report().ReportCriteriaGeneric().PreferredName()
    }, function (args) {
      if (args.Success) {
        $("#HumanResources").addClass('FadeDisplay')
        $("#HumanResourceFilters").addClass('FadeDisplay');
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([])
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(args.Data)
      }
      ViewModel.IsReportBusy(false)
    })

  },
  GetDiscipline: function (element) {
    FindSingleDiscipline(element);
  },
};
//#endregion
//#endregion

//#region Monthly Schedule By Individual Report
MonthlyScheduleByIndividualReport = {
  FindHR: function (element) {
    FindSingleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  },
  ClearHR: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs([]);
    ViewModel.Report().ReportCriteriaGeneric().HumanResources("");
  },

  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.SetXMLCriteria()
    MonthlyScheduleByIndividualReport.RefreshDisciplines()
    MonthlyScheduleByIndividualReport.GetAllowedHumanResources()
  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.SetXMLCriteria()
    MonthlyScheduleByIndividualReport.RefreshDisciplines()
    MonthlyScheduleByIndividualReport.GetAllowedHumanResources()
  },
  AddDiscipline: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    ViewModel.Report().ReportCriteriaGeneric().DisciplineIDList([])
    ViewModel.Report().ReportCriteriaGeneric().RODisciplineList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().DisciplineIDList.push(itm.DisciplineID())
      }
    })
    MonthlyScheduleByIndividualReport.GetAllowedHumanResources()
  },
  RefreshDisciplines: function () {
    RODisciplineReportBO.get({
      criteria: {
        SystemIDs: ViewModel.Report().ReportCriteriaGeneric().SystemIDs(),
        ProductionAreaIDs: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()
      },
      onSuccess: function (response) {
        ViewModel.Report().ReportCriteriaGeneric().RODisciplineList.Set(response.Data)
      }
    })
  },

  AddHumanResource: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.SetXMLHumanResourceIDs()
  },
  RefreshHR: function () {
    MonthlyScheduleByIndividualReport.GetAllowedHumanResources()
  },
  AddContractType: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.indexOf(obj.ContractTypeID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.push(obj.ContractTypeID());
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.indexOf(obj.ContractTypeID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.splice(index, 1);
      }
    }
    MonthlyScheduleByIndividualReport.GetAllowedHumanResources()
  },

  SetXMLCriteria: function () {
    //clear current selections
    ViewModel.Report().ReportCriteriaGeneric().SystemIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    ViewModel.UserSystemList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs().push(itm.SystemID())
        itm.UserSystemAreaList().Iterate(function (usA, usAIndx) {
          if (usA.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().push(usA.ProductionAreaID())
          }
        })
      }
    })
  },
  SetXMLHumanResourceIDs: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceFindList().Iterate(function (hr, hrInd) {
      if (hr.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs().push(hr.HumanResourceID())
      }
    })
    //ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDsXML(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs(), null))
  },
  GetAllowedHumanResources: function () {
    if (ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().length > 0 && ViewModel.Report().ReportCriteriaGeneric().SystemIDs().length > 0) {
      ViewModel.IsReportBusy(true)
      Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
        PageSize: 1000,
        SystemIDs: OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().SystemIDs(), null),
        ProductionAreaIDs: OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs(), null),
        FirstName: ViewModel.Report().ReportCriteriaGeneric().FirstName(),
        Surname: ViewModel.Report().ReportCriteriaGeneric().Surname(),
        PreferredName: ViewModel.Report().ReportCriteriaGeneric().PreferredName(),
        DisciplineIDList: ViewModel.Report().ReportCriteriaGeneric().DisciplineIDList(),
        ContractTypeIDList: ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList()
      }, function (args) {
        if (args.Success) {
          $("#HumanResources").addClass('FadeDisplay')
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceFindList.Set([])
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceFindList.Set(args.Data)
        }
        ViewModel.IsReportBusy(false)
      })
    }
  }

};
//#endregion

//#region Human Resource Schedule Excel Report
HumanResourceScheduleExcelReport = {
  FindHR: function (element) {
    FindSingleHR(element, {
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(ROHumanResource.HumanResourceID());
        var newHR = ROHumanResource.HumanResource();
        if (!ViewModel.Report().ReportCriteriaGeneric().HumanResources()) { ViewModel.Report().ReportCriteriaGeneric().HumanResources("") }
        if (ViewModel.Report().ReportCriteriaGeneric().HumanResources().trim().length > 0) {
          newHR = ', ' + newHR
        };
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources() + newHR;
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.pop(DeSelectedItem.ID());
        var newString = ViewModel.Report().ReportCriteriaGeneric().HumanResources().replace(DeSelectedItem.Description(), "");
        newString = newString.replace(",,", "");
        if (newString.endsWith(",")) {
          newString = newString.substring(0, newString.length - 1);
        }
        if (newString.endsWith(", ")) {
          newString = newString.substring(0, newString.length - 2);
        }
        ViewModel.Report().ReportCriteriaGeneric().HumanResources(newString);
      }
    });
  },
  ClearHR: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs([]);
    ViewModel.Report().ReportCriteriaGeneric().HumanResources("");
  },

  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.SetXMLCriteria()
    HumanResourceScheduleExcelReport.GetAllowedHumanResources()
    HumanResourceScheduleExcelReport.RefreshDisciplines()

  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.SetXMLCriteria()
    HumanResourceScheduleExcelReport.GetAllowedHumanResources()
    HumanResourceScheduleExcelReport.RefreshDisciplines()
  },
  AddDiscipline: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    ViewModel.Report().ReportCriteriaGeneric().DisciplineIDList([])
    ViewModel.Report().ReportCriteriaGeneric().RODisciplineList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().DisciplineIDList.push(itm.DisciplineID())
      }
    })
    HumanResourceScheduleExcelReport.GetAllowedHumanResources()
  },
  RefreshDisciplines: function () {
    RODisciplineReportBO.get({
      criteria: {
        SystemIDs: ViewModel.Report().ReportCriteriaGeneric().SystemIDs(),
        ProductionAreaIDs: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()
      },
      onSuccess: function (response) {
        ViewModel.Report().ReportCriteriaGeneric().RODisciplineList.Set(response.Data)
      }
    })
  },
  AddHumanResource: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.SetXMLHumanResourceIDs()
  },
  RefreshHR: function () {
    HumanResourceScheduleExcelReport.GetAllowedHumanResources()
  },
  AddContractType: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.indexOf(obj.ContractTypeID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.push(obj.ContractTypeID());
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.indexOf(obj.ContractTypeID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList.splice(index, 1);
      }
    }
    HumanResourceScheduleExcelReport.GetAllowedHumanResources()
  },

  SetXMLCriteria: function () {
    //clear current selections
    ViewModel.Report().ReportCriteriaGeneric().SystemIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    ViewModel.UserSystemList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs().push(itm.SystemID())
        itm.UserSystemAreaList().Iterate(function (usA, usAIndx) {
          if (usA.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().push(usA.ProductionAreaID())
          }
        })
      }
    })
  },
  SetXMLHumanResourceIDs: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceFindList().Iterate(function (hr, hrInd) {
      if (hr.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs().push(hr.HumanResourceID())
      }
    })
    //ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDsXML(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs(), null))
  },
  GetAllowedHumanResources: function () {
    if (ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().length > 0 && ViewModel.Report().ReportCriteriaGeneric().SystemIDs().length > 0) {
      ViewModel.IsReportBusy(true)
      Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib',

        {
          PageSize: 1000,
          SystemIDs: OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().SystemIDs(), null),
          ProductionAreaIDs: OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs(), null),
          FirstName: ViewModel.Report().ReportCriteriaGeneric().FirstName(),
          Surname: ViewModel.Report().ReportCriteriaGeneric().Surname(),
          PreferredName: ViewModel.Report().ReportCriteriaGeneric().PreferredName(),
          DisciplineIDList: ViewModel.Report().ReportCriteriaGeneric().DisciplineIDList(),
          ContractTypeIDList: ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDList()
        },

      function (args) {
        if (args.Success) {
          $("#HumanResources").addClass('FadeDisplay')
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceFindList.Set([])
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceFindList.Set(args.Data)
        }
        ViewModel.IsReportBusy(false)
      })
    }
  }

};
//#endregion

//#endregion Reports

function FindEquipment() {
  ROEquipments.ShowModal();
};

Singular.OnPageLoad(function () {
  FindHRControl.setup();
  ROEquipments.Init();
  if (ViewModel.Report() != undefined) {
    if (ViewModel.Report().ReportName() == "Skills Availability") {
      if (ViewModel.Report().ReportCriteriaGeneric().SystemID() != undefined) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
      }
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID() != undefined) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
      }
      if (ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs().length > 0) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.Set([]);
      }
      if (ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs().length > 0) {
        ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.Set([]);
      }
      if (ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs().length > 0) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.Set([]);
      }
    }
  }
  $("#Disciplines").removeClass('FadeDisplay');
  $("#Disciplines").addClass('FadeHide');
  if (!$("#ProductionsFieldSet").hasClass('FadeDisplay')) {
    $("#ProductionsFieldSet").addClass('FadeHide');
  }
  if (!$("#AdHocBookingsFieldSet").hasClass('FadeDisplay')) {
    $("#AdHocBookingsFieldSet").addClass('FadeHide');
  }
  if (!$("#SNTAdHocBookingsFieldSet").hasClass('FadeDisplay')) {
    $("#SNTAdHocBookingsFieldSet").addClass('FadeHide');
  }
  if (!$("#SNTProductionsFieldSet").hasClass('FadeDisplay')) {
    $("#SNTProductionsFieldSet").addClass('FadeHide');
  }
});

//#region Production Region Report
ProductionRegionReport = {
  AddProductionType: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.indexOf(obj.ProductionTypeID()) == -1) { //Item not in list
        ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.push(obj.ProductionTypeID()); //So we add it
      }
      if (ViewModel.Report().ReportCriteriaGeneric().Productions.indexOf(obj.ProductionTypeID()) == -1) { //Item not in list
        ViewModel.Report().ReportCriteriaGeneric().Productions.push(obj); //So we add it
      }
    }
    else { //Item deselected
      var indexOfID = ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.indexOf(obj.ProductionTypeID()); //Find out item index
      if (indexOfID != -1) { //Item is in list if index != -1, so we can remove it at that index
        ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.splice(indexOfID, 1);
      }
      var indexOfObj = $.map(ViewModel.Report().ReportCriteriaGeneric().Productions(), function (x, index) {
        if (x.ProductionTypeID === obj.ProductionTypeID) {
          return index;
        }
      });
      if (indexOfObj != -1) { //Item is in list if index != -1, so we can remove it at that index
        ViewModel.Report().ReportCriteriaGeneric().Productions.splice(indexOfObj, 1);
      }
    }
  },
  ClearDisciplines: function () {
    ClearDisciplines();
  },
  ClearProvinces: function () {
    ClearProvinces();
  },
  ClearCities: function () {
    ClearCities();
  },
  FindCities: function (element) {
    FindCities(element);
  },
  GetDiscipline: function (element) {
    FindMultipleDisciplines(element);
  },
  FindProvinces: function (element) {
    FindProvinces(element);
  },
};
//#endregion

// FindMultipleProductionTypes
function FindMultipleProductionTypes(element) {
  ROProductionTypes.Init();
  ROProductionTypes.Manager().SingleSelect(false);
  ROProductionTypes.Manager().MultiSelect(true);
  ROProductionTypes.SetOptions({
    AfterRowSelected: function (ROProductionType) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.push(ROProductionType.ProductionTypeID());
      var NewProductionType = ROProductionType.ProductionType();
      if (!ViewModel.Report().ReportCriteriaGeneric().ProductionType()) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionType("")
      }
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionType().trim().length > 0) {
        NewProductionType = ', ' + NewProductionType
      };
      var newString = ViewModel.Report().ReportCriteriaGeneric().ProductionType() + NewProductionType;
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      ViewModel.Report().ReportCriteriaGeneric().ProductionType(newString);
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.Set([]);
      var newString = ViewModel.Report().ReportCriteriaGeneric().ProductionType().replace(DeSelectedItem.Description(), "");
      newString = newString.replace(",,", "");
      if (newString.endsWith(",")) {
        newString = newString.substring(0, newString.length - 1);
      }
      if (newString.endsWith(", ")) {
        newString = newString.substring(0, newString.length - 2);
      }
      ViewModel.Report().ReportCriteriaGeneric().ProductionType(newString);
    }
  });


  $('#ROProductionTypes').dialog({
    title: 'Select Production Type',
    height: '500px',
    width: '900px',
    position: {
      my: 'left top',
      at: 'right top',
      of: $(element)
    },
    open: function (event, ui) {
      ROProductionTypes.RefreshList();
    }
  });
}

BiometricsReport = {
  ClearHR: function () {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.Set([]);
    ViewModel.Report().ReportCriteriaGeneric().HumanResources('');
  },
  FindHR: function (element) {
    FindSingleHR(element);
  },
  AddHumanResource: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      }
      )
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(obj.HumanResourceID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
    }
  },
  RefreshHR: function () {
    this.GetAllowedHumanResources();
  },
  GetAllowedHumanResources: function () {
    ViewModel.Report().ReportCriteriaGeneric().BusyHRList(true);
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredHumanResourceList);
        var tempList = ViewModel.ROFilteredHumanResourceList();
        var list = Singular.MergeSort(tempList, function (c) {
          return c.Firstname();
        }, 1);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([]);
        for (var n = 0; n < list.length; n++) {
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.push(list[n]);
        }
      }
      Singular.HideLoadingBar();
      ViewModel.Report().ReportCriteriaGeneric().BusyHRList(false);
    });
    return ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList;
  },
  DelayedRefreshList: function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      BiometricsReport.GetAllowedHumanResources();
      ViewModel.ROHumanResourceFindListManager().Refresh()
    }, DELAY); // create a new timeout
  },
};

//#region Production Overview
ProductionOverview = {
  AddProductionType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(obj.ProductionTypeID())
      ProductionOverview.GetAllowedEventTypes();
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
    }
  },
  GetAllowedEventTypes: function () {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport, OBLib', {
      PageSize: 1000,
      ProductionTypeID: ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(),
      KeyWord: ViewModel.Report().ReportCriteriaGeneric().EventTypes(),
      SortColumn: "EventType"
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredEventTypeReportList);
        var tempList = ViewModel.ROFilteredEventTypeReportList();
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set([]);
        tempList.Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.push(itm);
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
  AddEventType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(obj.EventTypeID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
  },
};
//#endregion

//#region Production Venue Report
ProductionVenueReport = {
  AddProductionType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(obj.ProductionTypeID())
      ProductionVenueReport.GetAllowedEventTypes();
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(null);
    }
  },
  GetAllowedProductionTypes: function () {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport, OBLib', {
      PageSize: 1000,
      KeyWord: ViewModel.Report().ReportCriteriaGeneric().ProductionTypes()
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredProductionTypeList);
        var tempList = ViewModel.ROFilteredProductionTypeList();
        ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList.Set([]);
        tempList.Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList.push(itm);
        });
      }
      ViewModel.IsReportBusy(false);
    });
    return ViewModel.Report().ReportCriteriaGeneric().ROProductionTypeList;
  },
  GetAllowedEventTypes: function () {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport, OBLib', {
      PageSize: 1000,
      ProductionTypeID: ViewModel.Report().ReportCriteriaGeneric().ProductionTypeID(),
      KeyWord: ViewModel.Report().ReportCriteriaGeneric().EventTypes(),
      SortColumn: "EventType"
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredEventTypeReportList);
        var tempList = ViewModel.ROFilteredEventTypeReportList();
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set([]);
        tempList.Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.push(itm);
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
  AddEventType: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(obj.EventTypeID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().EventTypeID(null);
    }
  }
};
//#endregion

//#region Studio PA Form - Sign In
StudioPAFormSignIn = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  }
};
//#endregion

//#region Studio PA Form - Sign Out
StudioPAFormSignOut = {
  AddSystem: function (obj) {
    ViewModel.UserSystemList().Iterate(function (itm, ind) {
      itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
    });
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID())
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (sys, ind) {
        sys.UserSystemAreaList().Iterate(function (pa, indx) {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  }
};
//#endregion

//#region OB PA Production Form - Sign In
OBProductionPAFormReport = {
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
    }
  }
};
//#endregion

//#region OB PA Production Form - Sign In
OBProductionPAForm2 = {
  GetAllowedProductions: function () {
    if (ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo() != null) {
      ViewModel.IsReportBusy(true);
      Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionFindList, OBLib', {
        PageSize: 1000,
        ProductionRefNo: ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo()
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredProductionList);
          var tempList = ViewModel.ROFilteredProductionList();
          var list = Singular.MergeSort(tempList, function (c) {
            return c.ProductionID();
          }, 1);
          ViewModel.Report().ReportCriteriaGeneric().ROProductionList.Set([]);
          for (var n = 0; n < list.length; n++) {
            ViewModel.Report().ReportCriteriaGeneric().ROProductionList.push(list[n]);
          }
        }
        ViewModel.IsReportBusy(false);
      });
      return ViewModel.Report().ReportCriteriaGeneric().ROProductionList;
    }
  },
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
    }
  },
};
//#endregion

HumanResourceDelayedRefreshList = {

  DelayedRefreshList: function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      HumanResourceDelayedRefreshList.GetAllowedHumanResources();
      ViewModel.ROHumanResourceFindListManager().Refresh()
    }, DELAY); // create a new timeout
  },
  GetAllowedHumanResources: function () {
    ViewModel.Report().ReportCriteriaGeneric().BusyHRList(true);
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(),
      KeyWord: ViewModel.Report().ReportCriteriaGeneric().HumanResource()
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredHumanResourceList);
        var tempList = ViewModel.ROFilteredHumanResourceList();
        var list = Singular.MergeSort(tempList, function (c) {
          return c.Firstname();
        }, 1);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([]);
        for (var n = 0; n < list.length; n++) {
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.push(list[n]);
        }
      }
      Singular.HideLoadingBar();
      ViewModel.Report().ReportCriteriaGeneric().BusyHRList(false);
    });
    return ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList;
  },
};

//#region Commentator Reports
//#region Travel Advances Report
TravelAdvancesReport = {
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID(null));
    }
  },
  GetAllowedProductions: function () {
    if (ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo() != null) {
      ViewModel.IsReportBusy(true)
      Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionFindList, OBLib', {
        PageSize: 1000,
        KeyWord: ViewModel.Report().ReportCriteriaGeneric().Production(),
        ProductionRefNo: ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo(),
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredProductionList);
          var tempList = ViewModel.ROFilteredProductionList();
          ViewModel.Report().ReportCriteriaGeneric().ROProductionList.Set([]);
          for (var n = 0; n < tempList.length; n++) {
            ViewModel.Report().ReportCriteriaGeneric().ROProductionList.push(list[n]);
          }
        }
        ViewModel.IsReportBusy(false)
      });
      return ViewModel.Report().ReportCriteriaGeneric().ROProductionList;
    }
  },
};
//#endregion
//#region Travel Advances Per Person Report
TravelAdvancesPerPersonReport = {
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID(null));
    }
  },
  GetAllowedProductions: function () {
    if (ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo() != null) {
      ViewModel.IsReportBusy(true)
      Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionFindList, OBLib', {
        PageSize: 1000,
        KeyWord: ViewModel.Report().ReportCriteriaGeneric().Production(),
        ProductionRefNo: ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo(),
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredProductionList);
          var tempList = ViewModel.ROFilteredProductionList();
          ViewModel.Report().ReportCriteriaGeneric().ROProductionList.Set([]);
          for (var n = 0; n < tempList.length; n++) {
            ViewModel.Report().ReportCriteriaGeneric().ROProductionList.push(list[n]);
          }
        }
        ViewModel.IsReportBusy(false)
      });
      return ViewModel.Report().ReportCriteriaGeneric().ROProductionList;
    }
  },
};
//#endregion
//#endregion

//#region OB S&T Report
OBSnTReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      for (var i = 0; i < ViewModel.Report().ReportCriteriaGeneric().ROSystemList().length; i++) {
        if (ViewModel.Report().ReportCriteriaGeneric().ROSystemList()[i].IsSelected() == true) {
          ViewModel.Report().ReportCriteriaGeneric().ROSystemList()[i].IsSelected(false);
        }
      }
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID())
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      for (var i = 0; i < ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList().length; i++) {
        if (ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList()[i].IsSelected() == true) {
          ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList()[i].IsSelected(false);
        }
      }
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddProduction: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ROProductionList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(obj.ProductionID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionID(null);
    }
  },
  GetAllowedProductions: function () {
    if (ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo() != null) {
      ViewModel.IsReportBusy(true)
      Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROProductionFindList, OBLib', {
        PageSize: 1000,
        KeyWord: ViewModel.Report().ReportCriteriaGeneric().Production(),
        ProductionRefNo: ViewModel.Report().ReportCriteriaGeneric().ProductionRefNo()
      }, function (args) {
        if (args.Success) {
          KOFormatter.Deserialise(args.Data, ViewModel.ROFilteredProductionList);
          var tempList = ViewModel.ROFilteredProductionList();
          ViewModel.Report().ReportCriteriaGeneric().ROProductionList.Set([]);
          for (var n = 0; n < tempList.length; n++) {
            ViewModel.Report().ReportCriteriaGeneric().ROProductionList.push(list[n]);
          }
        }
        ViewModel.IsReportBusy(false)
      });
      return ViewModel.Report().ReportCriteriaGeneric().ROProductionList;
    }
  }
};
//#endregion

HoursPerMonthReportBO = {
  triggerSystemYearAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setSystemYearCriteriaBeforeRefresh: function (args) {
    args.Data.PageNo = 1
    args.Data.PageSize = 25
    args.Data.SortColumn = 'YearStartDate'
    args.Data.SortAsc = false
  },
  afterSystemYearmRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onSystemYearSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {

    } else {

    }
  }
}

TeamHoursPerQuarterReportBO = {
  triggerSystemYearAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setSystemYearCriteriaBeforeRefresh: function (args) {
    args.Data.PageNo = 1
    args.Data.PageSize = 25
    args.Data.SortColumn = 'YearStartDate'
    args.Data.SortAsc = false
  },
  afterSystemYearmRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onSystemYearSelected: function (selectedItem, businessObject) {
    if (!selectedItem) {

    } else {

    }
  }
}

StageHandScheduleReport = {
  AddSystem: function (obj) {
    //when changing Sub-Dept, reset sub-dept as well as area selections before handling the new selection
    ViewModel.UserSystemList().Iterate(function (itm, itmIndx) {
      itm.IsSelected(false)
      itm.UserSystemAreaList().Iterate(function (itmArea, itmAreaIndx) {
        itmArea.IsSelected(false)
      })
    })
    //handle the new selection
    obj.IsSelected(!obj.IsSelected())
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddProductionArea: function (obj) {
    //when changing the Area, reset only the area selections before handling the new selection
    ViewModel.UserSystemList().Iterate(function (itm, itmIndx) {
      itm.UserSystemAreaList().Iterate(function (itmArea, itmAreaIndx) {
        itmArea.IsSelected(false)
      })
    })
    //handle the new selection
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null);
    }
  },
  AddHumanResource: function (obj) {
    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs([])
    obj.IsSelected(!obj.IsSelected());
    ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (disc, discIndx) {
      if (disc.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs().push(disc.HumanResourceID())
      }
    })
    //obj.IsSelected(!obj.IsSelected());
    //if (obj.IsSelected()) {
    //  if (ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID()) == -1) {
    //    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(obj.HumanResourceID());
    //  }
    //  if (ViewModel.Report().ReportCriteriaGeneric().HumanResources.indexOf(obj.HumanResourceID()) == -1) {
    //    ViewModel.Report().ReportCriteriaGeneric().HumanResources.push(obj);
    //  }
    //}
    //else {
    //  var HR = ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Find('HumanResourceID', obj.HumanResourceID());
    //  if (HR) {
    //    HR.IsSelected(false);
    //  }
    //  var indexofID = ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID());
    //  if (indexofID != -1) {
    //    ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.splice(indexofID, 1);
    //  }
    //  var indexOfObj = $.map(ViewModel.Report().ReportCriteriaGeneric().HumanResources(), function (x, index) {
    //    if (x.HumanResourceID === obj.HumanResourceID) {
    //      return index;
    //    }
    //  });
    //  if (indexOfObj != -1) { //Item is in list if index != -1, so we can remove it at that index
    //    ViewModel.Report().ReportCriteriaGeneric().HumanResources.splice(indexOfObj, 1);
    //  }
    //}
  },
  GetAllowedHumanResources: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID()
    }, function (args) {
      if (args.Success) {
        $("#HumanResources").addClass('FadeDisplay');
        $("#SelectedHumanResources").addClass('FadeDisplay');
        $("#HumanResourceFilters").addClass('FadeDisplay');
        //set to new list
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(args.Data);
        //re-apply old selections
        ViewModel.Report().ReportCriteriaGeneric().HumanResources().Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm2, ind2) {
            if (itm.HumanResourceID() == itm2.HumanResourceID()) {
              itm2.IsSelected(true);
            }
          });
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
};