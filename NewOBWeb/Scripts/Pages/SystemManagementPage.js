﻿//#region System Management Page
SystemManagementPage = {
	showActions: function (user) {
		ViewModel.CurrentSystemID(user.GetParent().SystemID());
		$("#Actions").removeClass('FadeHide');
		$("#Actions").addClass('FadeDisplay animated slideInRight go');
	},
	manageSystemYearRequirements: function () {
		var me = this;
		ViewModel.SystemYearRequirementManagementSelected(!ViewModel.SystemYearRequirementManagementSelected());
		if (ViewModel.SystemYearRequirementManagementSelected()) {
			ViewModel.ROSystemYearListManager().Refresh();
			if ($("#ROSystemYearRequirementTable").hasClass('FadeHide')) {
				$("#ROSystemYearRequirementTable").removeClass('FadeHide');
			}
			$("#ROSystemYearRequirementTable").addClass('FadeDisplay animated slideInRight go');
		} else {
			$("#ROSystemYearRequirementTable").removeClass('FadeDisplay');
			$("#ROSystemYearRequirementTable").addClass('FadeHide');
		}
	},
	filterSystems: function (List, obj) {
		var results = []
		List.Iterate(function (itm, indx) {
			if (itm.DepartmentID == obj.CurrentDepartmentID()) {
				results.push(itm)
			}
		})
		return results;
	},
	filterProductionAreas: function (List, obj) {
		var results = []
		List.Iterate(function (itm, indx) {
			if (itm.SystemID == obj.CurrentSystemID()) {
				results.push(itm)
			}
		})
		return results;
	},
	canShowOptions: function () {
		return (ViewModel.CurrentDepartmentID() && ViewModel.CurrentSystemID())
	},

	refreshYearlyRequirements: function () {
		ViewModel.ROSystemYearListCriteria().SystemID(ViewModel.CurrentSystemID())
		ViewModel.ROSystemYearListManager().Refresh()
	},
	editSystemYearRequirement: function (yearlyRequirement) {
		var me = this
		Singular.GetDataStateless("OBLib.Maintenance.SystemManagement.SystemYearList, OBLib", {
			SystemYearID: yearlyRequirement.SystemYearID()
		}, function (response) {
			if (response.Success) {
				ViewModel.CurrentSystemYear.Set(response.Data[0])
				me.showYearlyRequirementModal()
			} else {
				OBMisc.Notifications.GritterError("Error Retrieving Year Requirements", response.ErrorText, 3000)
			}
		})
	},
	newYearlyRequirement: function () {
		ViewModel.CurrentSystemYear(new SystemYearObject())
		ViewModel.CurrentSystemYear().SystemID(ViewModel.CurrentSystemID())
		this.showYearlyRequirementModal()
	},
	saveYearlyRequirment: function (yearlyRequirement) {
		var me = this;
		ViewModel.CallServerMethod("SaveSystemYear",
      {
      	SystemYear: yearlyRequirement.Serialise()
      },
      function (response) {
      	if (response.Success) {
      		OBMisc.Notifications.GritterSuccess("Saved Successfully", "Yearly Requirement has been saved", 1500)
      		ViewModel.CurrentSystemYear.Set(response.Data)
      	} else {
      		OBMisc.Notifications.GritterError("Error Saving Year Requirements", response.ErrorText, 3000)
      	}
      })
	},
	showYearlyRequirementModal: function () {
		$("#EditYearRequirementModal").off("shown.bs.modal")
		$("#EditYearRequirementModal").off("hidden.bs.modal")
		$("#EditYearRequirementModal").on("shown.bs.modal", function () {
			Singular.Validation.CheckRules(ViewModel.CurrentSystemYear())
			ViewModel.ROSystemYearListManager().Refresh()
		})
		$("#EditYearRequirementModal").on("hidden.bs.modal", function () {
			ViewModel.ROSystemYearListManager().Refresh()
		})
		$("#EditYearRequirementModal").modal()
	},
	setupTeamYears: function (systemYear) {
		var teamNumbers = ClientData.ROSystemTeamNumberList.Filter("SystemID", systemYear.SystemID())
		//console.log(teamNumbers)
		systemYear.SystemYearTeamNumberList([])
		//Teams-------------------------------------------------------------------------------
		//------------------------------------------------------------------------------------
		teamNumbers.Iterate(function (tm, tmIndx) {
			var teamRequirement = systemYear.SystemYearTeamNumberList.AddNew()
			teamRequirement.SystemTeamNumberID(tm.SystemTeamNumberID)
			teamRequirement.SystemID(systemYear.SystemID())
			teamRequirement.SystemTeamNumber(tm.SystemTeamNumber)
			teamRequirement.SystemTeamNumberName(tm.SystemTeamNumberName)
			systemYear.SystemYearMonthGroupList().Iterate(function (mg, mgIndx) {
				//Month Groups------------------------------------------------------------------------
				//------------------------------------------------------------------------------------
				var teamMonthGroup = teamRequirement.SystemYearTeamNumberMonthGroupList.AddNew()
				teamMonthGroup.SystemTeamNumberID(teamRequirement.SystemTeamNumberID())
				teamMonthGroup.SystemYearMonthGroupID(mg.SystemYearMonthGroupID())
				teamMonthGroup.MonthGroupStartDate(mg.MonthGroupStartDate())
				teamMonthGroup.MonthGroupEndDate(mg.MonthGroupEndDate())
				teamMonthGroup.RequiredHoursGroup(540)
				//Months------------------------------------------------------------------------------
				//------------------------------------------------------------------------------------
				mg.SystemYearMonthGroupMonthList().Iterate(function (mgm, mgmIndx) {
					var teamMonthGroupMonth = teamMonthGroup.SystemYearTeamNumberMonthGroupMonthList.AddNew()
					teamMonthGroupMonth.SystemYearMonthGroupMonthID(mgm.SystemYearMonthGroupMonthID())
					teamMonthGroupMonth.MonthStartDate(mgm.MonthStartDate())
					teamMonthGroupMonth.MonthEndDate(mgm.MonthEndDate())
					var timesheetMonthName = tm.SystemTeamNumberName + ': ' + new Date(teamMonthGroupMonth.MonthStartDate()).format('dd MMM yy')
					teamMonthGroupMonth.SystemTimesheetMonth(timesheetMonthName)
					teamMonthGroupMonth.RequiredHours(180)
				})
			})
		})
	},

	//Disable / Enable tabs
	canEdit: function (tab, vm) {
		switch(tab) {
			case 'YearlyRequirements':
				if (vm.CurrentSystemID() != null) {
					return true
				}
				else {
					return false
				}
				break;
			case 'Timesheets':
				if (vm.CurrentSystemID() != null) {
					return true
				}
				else {
					return false
				}
				break;
			case 'Rates':
				if ((vm.CurrentProductionAreaID() != null) && (vm.CurrentSystemID() == 5)) {
					return true
				}
				else {
					return false
				}
				break;
			case 'Allowances':
				if ((vm.CurrentProductionAreaID() != null) && ((vm.CurrentSystemID() == 5) || (vm.CurrentSystemID() == 4))) {
					return true
				}
				else {
					return false
				}
				break;
			case 'ICR Rates':
				if (vm.CurrentSystemID() == 4) {
					return true
				}
				else {
					return false
				}
				break;
			case 'Disciplines':
				if (vm.CurrentProductionAreaID() != null) {
					return true
				}
				else {
					return false
				}
				break;
			case 'CallTimeSettings':
				if (vm.CurrentProductionAreaID() != null) {
					return true
				}
				else {
					return false
				}
				break;
			case 'ChannelDefaults':
				if (vm.CurrentProductionAreaID() != null) {
					return true
				}
				else {
					return false
				}
				break;
			case 'AreaSettings':
				if (vm.CurrentUserID == 1) {
					return true
				}
				else {
					return false
				}
				break;
			case 'AreaStatus':
				if (vm.CurrentUserID == 1) {
					return true
				}
				else {
					return false
				}
				break;
			case 'TimelineTypes':
				if (vm.CurrentProductionAreaID() != null) {
					return true
				}
				else {
					return false
				}
        break;
      case 'AutomaticImports':
        if (vm.CurrentProductionAreaID() != null) {
          return true
        }
        else {
          return false
        }
        break;
		}
	},

	//Timesheet Requirements
	refreshTimesheetRequirements: function () {
		ViewModel.ROTimesheetRequirementPagedListCriteria().SystemID(ViewModel.CurrentSystemID())
		ViewModel.ROTimesheetRequirementPagedListManager().Refresh()
	},
	editTimesheetRequirement: function (roTimesheetRequirement) {
		var me = this
		TimesheetRequirementBO.getTimesheetRequirement(roTimesheetRequirement.TimesheetRequirementID(),
                                                    function (response) {
                                                    	ViewModel.CurrentTimesheetRequirement.Set(response.Data)
                                                    	me.showCurrentTimesheetRequirement()
                                                    	//TimesheetRequirementBO.refreshStats(ViewModel.CurrentTimesheetRequirement())
                                                    },
                                                    function (response) {

                                                    })
	},
	saveCurrentTimesheetMonth: function (timesheetRequirement) {
		var me = this
		TimesheetRequirementBO.saveTimesheetRequirement(timesheetRequirement,
                                                    function (response) {
                                                    	ViewModel.CurrentTimesheetRequirement.Set(response.Data)
                                                    	$("#CurrentTimesheetRequirement").modal('hide')
                                                    	me.refreshTimesheetRequirements()
                                                    },
                                                    function (response) {

                                                    })
	},
	newTimesheetRequirement: function () {
		ViewModel.CurrentTimesheetRequirement(new TimesheetRequirementObject())
		this.showCurrentTimesheetRequirement()
	},
	showCurrentTimesheetRequirement: function () {
		$("#CurrentTimesheetRequirement").off('shown.bs.modal')
		$("#CurrentTimesheetRequirement").off('hide.bs.modal')
		$("#CurrentTimesheetRequirement").off('hidden.bs.modal')
		$("#CurrentTimesheetRequirement").on('shown.bs.modal', function () {
			Singular.Validation.CheckRules(ViewModel.CurrentTimesheetRequirement())
			OBMisc.UI.activateTab("TimesheetRequirementHR")
		})
		$("#CurrentTimesheetRequirement").on('hide.bs.modal', function () {

		})
		$("#CurrentTimesheetRequirement").on('hidden.bs.modal', function () {
			SystemManagementPage.refreshTimesheetRequirements()
		})
		$("#CurrentTimesheetRequirement").modal()
	},

	//set
	CurrentDepartmentIDSet: function (vm) {
		vm.CurrentSystemID(null)
		vm.CurrentProductionAreaID(null)
		SystemManagementPage.CurrentProductionAreaIDSet(vm)
	},
	CurrentSystemIDSet: function (vm) {
		SystemManagementPage.CurrentProductionAreaIDSet(vm)
		vm.ROTimesheetRequirementPagedListCriteria().SystemID(vm.CurrentSystemID())
		vm.ROTimesheetRequirementPagedListManager().Refresh()
		vm.ROSystemYearListCriteria().SystemID(vm.CurrentSystemID())
		vm.ROSystemYearListManager().Refresh()
	},
	CurrentProductionAreaIDSet: function (vm) {
		SystemProductionAreaBO.get({
			criteria: {
				SystemID: vm.CurrentSystemID(),
				ProductionAreaID: vm.CurrentProductionAreaID()
			},
			onSuccess: function (response) {
				if (response.Data.length == 1) {
					vm.SystemProductionArea.Set(response.Data[0])
				}
				else {
					vm.SystemProductionArea(null)
					//OBMisc.Notifications.GritterError("Select Area","",500)
				}
			}
		})
	},

	//AutoDropDown for Disciplines
	onDisciplineSelected: function (selectedItem, businessObject) {
		if (selectedItem) {
			businessObject.Discipline(selectedItem.Discipline)
		} else {
			businessObject.SystemProductionAreaDisciplineID(null)
		}
	},
	triggerDisciplineAutoPopulate: function (args) {
		args.AutoPopulate = true
		args.Object.IsProcessing(true)
	},
	setDisciplineCriteriaBeforeRefresh: function (args) {
		args.Data.SystemProductionAreaID = args.Object.GetParent().SystemProductionAreaID()
	},
	afterDisciplineRefreshAjax: function (args) {
		args.Object.IsProcessing(false)
	},

	//AutoDropDown for Human Resources
	onHRSelected: function (selectedItem, businessObject) {
		if (selectedItem) {
			businessObject.FullName(selectedItem.FullName)
		} else {
			businessObject.HumanResourceID(null)
		}
	},
	triggerHRAutoPopulate: function (args) {
		args.AutoPopulate = true
		args.Object.IsProcessing(true)
	},
	setHRCriteriaBeforeRefresh: function (args) {
		args.Data.SystemProductionAreaID = args.Object.GetParent().SystemProductionAreaID()
	},
	afterHRRefreshAjax: function (args) {
		args.Object.IsProcessing(false)
	},

	//AutoDropDown for Position Types
	onPositionTypeSelected: function (selectedItem, businessObject) {
		if (selectedItem) {
			businessObject.PositionType(selectedItem.PositionType)
		} else {
			businessObject.PositionTypeID(null)
		}
	},
	triggerPositionTypeAutoPopulate: function (args) {
		args.AutoPopulate = true
		args.Object.IsProcessing(true)
	},
	setPositionTypeCriteriaBeforeRefresh: function (args) {
		//not filtering for a specific area / system
		//args.Data.SystemID = args.Object.GetParent().SystemID()
	},
	afterPositionTypeRefreshAjax: function (args) {
		args.Object.IsProcessing(false)
	},

	//AutoDropDown for Positions
	onPositionSelected: function (selectedItem, businessObject) {
		if (selectedItem) {
			businessObject.Position(selectedItem.Position)
		} else {
			businessObject.PositionID(null)
		}
	},
	triggerPositionAutoPopulate: function (args) {
		args.AutoPopulate = true
		args.Object.IsProcessing(true)
	},
	setPositionCriteriaBeforeRefresh: function (args) {
		//not filtering for a specific area / system
		//args.Data.SystemID = args.Object.GetParent().SystemID()
	},
	afterPositionRefreshAjax: function (args) {
		args.Object.IsProcessing(false)
	},

	//Rates
	previousPageICRRates: function () { },
	nextPageICRRates: function () { },

	//Sexy State Buttons
	WeekendIndClick: function (obj) {
		obj.WeekendInd(SystemManagementPage.ThreeStateButtonInd(obj.WeekendInd()));
	},
	WeekendIndCss: function (obj) {
		return SystemManagementPage.ThreeStateButtonIndCss(obj.WeekendInd());
	},
	WeekendIndHtml: function (obj) {
		return SystemManagementPage.ThreeStateWeekendButtonIndHtml(obj.WeekendInd());
	},
	ExtraShiftIndClick: function (obj) {
		obj.ExtraShiftInd(SystemManagementPage.ThreeStateButtonInd(obj.ExtraShiftInd()));
	},
	ExtraShiftIndCss: function (obj) {
		return SystemManagementPage.ThreeStateButtonIndCss(obj.ExtraShiftInd());
	},
	ExtraShiftIndHtml: function (obj) {
		return SystemManagementPage.ThreeStateExtraShiftButtonIndHtml(obj.ExtraShiftInd());
	},
	HourlyRateIndClick: function (obj) {
		obj.HourlyRateInd(!obj.HourlyRateInd());
	},
	HourlyRateIndCss: function (obj) {
		return SystemManagementPage.ThreeStateButtonIndCss(obj.HourlyRateInd());
	},
	HourlyRateIndHtml: function (obj) {
		if (obj.HourlyRateInd()) {
			return '<span class=\"glyphicon glyphicon-ok\"></span> Yes';
		}
		else {
			return '<span class=\"glyphicon glyphicon-remove\"></span> No';
		}
	},
	ThreeStateButtonInd: function (OldValue) {
		if (OldValue == null) {
			return true;
		}
		else if (OldValue == true) {
			return false;
		}
		else if (OldValue == false) {
			return null;
		}
	},
	ThreeStateButtonIndCss: function (Value) {
		if (Value == null) {
			return 'btn btn-xs btn-warning';
		}
		else if (Value == true) {
			return 'btn btn-xs btn-success';
		}
		else if (Value == false) {
			return 'btn btn-xs btn-info';
		}
	},
	ThreeStateWeekendButtonIndHtml: function (Value) {
		if (Value == null) {
			return 'Both';//'<span class=\"glyphicon glyphicon-minus\"></span> Both';
		}
		else if (Value == true) {
			return 'Weekend';//'<span class=\"glyphicon glyphicon-ok\"></span> Weekend';
		}
		else if (Value == false) {
			return 'Weekday';//'<span class=\"glyphicon glyphicon-remove\"></span> Weekday';
		}
	},
	ThreeStateExtraShiftButtonIndHtml: function (Value) {
		if (Value == null) {
			return 'Both'; //'<span class=\"glyphicon glyphicon-minus\"></span> Both';
		}
		else if (Value == true) {
			return 'Extra Shift'; //'<span class=\"glyphicon glyphicon-ok\"></span> Extra Shift';
		}
		else if (Value == false) {
			return 'Non-Extra Shift'; //'<span class=\"glyphicon glyphicon-remove\"></span> Non-Extra Shift';
		}
	},

	//Save Function

	saveSystemProductionArea: function (list) {
		var options = {
			data: list, onSuccess: function (a) {
				document.getElementById('saveSPAButton').classList.remove('flash')
				document.getElementById('saveSPAButton').classList.remove('slowest')
				document.getElementById('saveSPAButton').classList.remove('infinite')
			}
		}
		SystemProductionAreaBO.save(options)

	}

}

//#endregion
