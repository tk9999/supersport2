﻿TravelVM = {

  //viewmodel
  editROTravelRequisitionPaged: function (ROTravelRequisitionPaged) {
    var me = this
    TravelRequisitionBO.get({
      criteria: { TravelRequisitionID: ROTravelRequisitionPaged.TravelRequisitionID() },
      onSuccess: function (response) {
        ViewModel.TravelRequisition.Set(response.Data[0])
        ViewModel.ROHumanResourceTravelReqListCriteria().TravelRequisitionID(ViewModel.TravelRequisition().TravelRequisitionID())
        $('#FindTravelRequisition').modal('hide')
        me.fetchTravellers()
        OBMisc.UI.activateTab("Travellers")
      }
    })
  },
  pageHeaderDetails: function () {
    return '<h3>' + ViewModel.TravelRequisition().EventTitle() + ' <small>' + ' (' + ViewModel.TravelRequisition().StartTimeString() + ' - ' + ViewModel.TravelRequisition().EndTimeString() + ')</small></h3><br>';
  },
  onPageLoad: function () {

    ViewModel.ROTravelRequisitionListPagedCriteria().PageSize(25)
    ViewModel.ROTravelRequisitionListPagedPagingManager().PageSize(25)

    if (ViewModel.TravelRequisition() && ViewModel.TravelRequisition().IsAdHoc() == true) {
      OBMisc.UI.activateTab("Travellers")
      TravelVM.fetchTravellers()
    } else {
      //OBMisc.UI.activateTab("TravelDetails")
      OBMisc.UI.activateTab("Travellers")
      TravelVM.fetchTravellers()
    }

    $('#FindROHumanResourceModal').off('hide.bs.modal')
    $('#FindROHumanResourceModal').on('hide.bs.modal', function (e) {
      TravelVM.onFindROHumanResourceModalClosed(e)
    })

    //flights
    $('#AddFlightPassengerModal').off('shown.bs.modal')
    $('#AddFlightPassengerModal').on('shown.bs.modal', function (e) {
      FlightBO.updateClashes(TravelVM.currentFlight());
    });
    $('#AddFlightPassengerModal').off('hide.bs.modal')
    $('#AddFlightPassengerModal').on('hide.bs.modal', function (e) {
      TravelVM.closeAddFlightPassengerModal(e)
    });

    //rental cars
    $('#AddRentalCarPassengerModal').off('shown.bs.modal')
    $('#AddRentalCarPassengerModal').on('shown.bs.modal', function (e) {
      RentalCarBO.updateClashes(TravelVM.currentRentalCar());
    })
    $('#AddRentalCarPassengerModal').off('hide.bs.modal')
    $('#AddRentalCarPassengerModal').on('hide.bs.modal', function (e) {
      TravelVM.closeAddRentalCarPassengerModal();
    })

    //chauffeur
    $('#AddChauffeurDrivePassengerModal').off('shown.bs.modal')
    $('#AddChauffeurDrivePassengerModal').on('shown.bs.modal', function (e) {
      ChauffeurDriverBO.updateClashes(TravelVM.currentChauffeur());
    })
    $('#AddChauffeurDrivePassengerModal').off('hide.bs.modal')
    $('#AddChauffeurDrivePassengerModal').on('hide.bs.modal', function (e) {
      TravelVM.closeAddChauffeurDrivePassengerModal(e);
    })

    //accommodation
    $('#AddAccommodationGuestModal').off('shown.bs.modal')
    $('#AddAccommodationGuestModal').on('shown.bs.modal', function (e) {
      AccommodationBO.updateClashes(TravelVM.currentAccommodation());
    })
    $('#AddAccommodationGuestModal').off('hide.bs.modal')
    $('#AddAccommodationGuestModal').on('hide.bs.modal', function (e) {
      TravelVM.closeAddAccommodationGuestModal(e);
    })

    $("#AdHocBookingModal").off('show.bs.modal')
    $("#AdHocBookingModal").on('show.bs.modal', function (e) {
      Singular.Validation.CheckRules(ViewModel.AdHocBooking())
    })

  },
  findRequisition: function () {
    ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh();
    $('#FindTravelRequisition').modal();
  },
  onNewTravellerSelected: function (ROHumanResourceTR) {
    var wasSelected = ROHumanResourceTR.IsSelected()
    ROHumanResourceTR.IsSelected(!ROHumanResourceTR.IsSelected())
    if (wasSelected) {
      //remove from ToAdd list
      var itemToRemove = ViewModel.TravellersToAddList().Find("HumanResourceID", ROHumanResourceTR.HumanResourceID())
      if (itemToRemove) { ViewModel.TravellersToAddList.RemoveNoCheck(itemToRemove) }
    } else {
      //add to ToAdd list
      var newObj = ViewModel.TravellersToAddList.AddNew()
      newObj.TravelRequisitionID(ViewModel.TravelRequisition().TravelRequisitionID())
      newObj.HumanResourceID(ROHumanResourceTR.HumanResourceID())
      newObj.HumanResource(ROHumanResourceTR.Firstname() + ' ' + ROHumanResourceTR.Surname())
      newObj.SnTStartDate(ViewModel.TravelRequisition().StartDate())
      newObj.SnTEndDate(ViewModel.TravelRequisition().EndDate())
    }
  },
  selectSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    ROHumanResourceTravelReqListCriteriaBO.SetXMLCriteria()
    ROHumanResourceTravelReqListCriteriaBO.afterRefresh()
  },
  selectProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    ROHumanResourceTravelReqListCriteriaBO.SetXMLCriteria()
    ROHumanResourceTravelReqListCriteriaBO.afterRefresh()
  },
  canView: function (what) {
    switch (what) {
      case "EditAdHocBookingButton":
        return (ViewModel.TravelRequisition().AdHocBookingID() ? true : false);
        break;
      default:
        return true;
        break;
    }
  },
  showTravellersModal: function () {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh()
    $('#FindROHumanResourceModal').modal()
  },
  backToProduction() {
    window.location.href = window.SiteInfo.SitePath + "/Productions/Production.aspx?P=" + ViewModel.TravelRequisition().ProductionID().toString();
  },

  //adhoc booking
  newAdHocBooking: function () {
    ViewModel.AdHocBooking(new AdHocBookingObject())
    //ViewModel.AdHocBooking().StartDateTime(null)
    //ViewModel.AdHocBooking().EndDateTime(null)
    ViewModel.AdHocBooking().RequiresTravel(true)
    $("#AdHocBookingModal").modal()
  },
  async editAdHocBooking() {
    try {
      let data = await AdHocBookingBO.getPromise({
        AdHocBookingID: ViewModel.TravelRequisition().AdHocBookingID()
      });
      ViewModel.AdHocBooking.Set(data[0]);
      Singular.Validation.CheckRules(ViewModel.AdHocBooking());
      $("#AdHocBookingModal").modal();
    }
    catch (errorText) {
      OBMisc.Notifications.GritterError("Fetch failed", errorText, 1000);
    };
  },
  saveAdHocBooking: function () {
    var me = this;
    var wasNew = ViewModel.AdHocBooking().IsNew();
    ViewModel.AdHocBooking().IsProcessing(true);
    AdHocBookingBO.save(ViewModel.AdHocBooking(), {
      onSuccess: function (response) {
        ViewModel.AdHocBooking.Set(response.Data)
        window.setTimeout(function () {
          if (wasNew) {
            OBMisc.Notifications.GritterSuccess("AdHoc Booking Saved", "Requisition will load shortly", 250)
            ViewModel.TravelRequisition(null);
            TravelRequisitionBO.get({
              criteria: { TravelRequisitionID: ViewModel.AdHocBooking().CreatedTravelRequisitionID() },
              onSuccess: function (response) {
                ViewModel.TravelRequisition.Set(response.Data[0]);
                ViewModel.ROHumanResourceTravelReqListCriteria().TravelRequisitionID(ViewModel.TravelRequisition().TravelRequisitionID());
                $('#FindTravelRequisition').modal('hide');
                me.fetchTravellers();
                OBMisc.UI.activateTab("Travellers");
                ViewModel.AdHocBooking().IsProcessing(false);
                $("#AdHocBookingModal").modal('hide');
              }
            })
          } else {
            OBMisc.Notifications.GritterSuccess("AdHoc Booking Saved", "", 250);
            ViewModel.AdHocBooking().IsProcessing(false);
            $("#AdHocBookingModal").modal('hide');
          }
        }, 500)
      },
      onFail: function (response) {
        console.log(response)
      }
    })
  },

  //requisition
  async saveRequisition() {
    try {
      let result = await TravelRequisitionBO.savePromise(ViewModel.TravelRequisition());
      KOFormatter.Deserialise(result, ViewModel.TravelRequisition());
      OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 250);
    } catch (err) {
      OBMisc.Notifications.GritterError("Save Failed", err, 1000);
    } finally {

    }
  },
  printRequisition: function (requisition) {
    TravelRequisitionBO.printRequisition(requisition, {})
  },

  //travellers
  fetchTravellers() {
    if (ViewModel.TravelRequisition()) {
      ViewModel.TravelRequisition().TravellersFetched(false)
      if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().TravellersFetched()) {
        TravelRequisitionTravellerBO.get({
          criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
          onSuccess: function (response) {
            ViewModel.TravelRequisition().TravellerList.Set(response.Data)
            ViewModel.TravelRequisition().TravellersFetched(true)
          }
        })
      }
    }
  },
  onFindROHumanResourceModalClosed: function () {
    var me = this
    //ViewModel.AddingNewTraveller(false);
    if (ViewModel.TravellersToAddList().length > 0) {
      TravelRequisitionTravellerBO.saveList(ViewModel.TravellersToAddList, {
        onSuccess: function (response) {
          ViewModel.TravellersToAddList([])
          ViewModel.ROHumanResourceTravelReqListManager().Refresh()
          ViewModel.TravelRequisition().TravellersFetched(false)
          me.fetchTravellers()
          OBMisc.Notifications.GritterSuccess("Adding Travellers Succeeded", "", 250)
        }
      })
    }
  },
  addSystemCriteria: function (system) {
    var newValue = !system.IsSelected()
    ViewModel.ROTravelRequisitionListPagedCriteria().PreventRefresh(true)
    this.clearUserSystemSelections()
    this.clearUserSystemAreaSelections()
    system.IsSelected(newValue)
    ViewModel.ROTravelRequisitionListPagedCriteria().SystemID(null)
    ViewModel.ROTravelRequisitionListPagedCriteria().ProductionAreaID(null)
    if (system.IsSelected()) {
      ViewModel.ROTravelRequisitionListPagedCriteria().SystemID(system.SystemID())
    }
    ViewModel.ROTravelRequisitionListPagedCriteria().PreventRefresh(false)
    ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
  },
  addProductionAreaCriteria: function (area) {
    var newValue = !area.IsSelected()
    ViewModel.ROTravelRequisitionListPagedCriteria().PreventRefresh(true)
    this.clearUserSystemAreaSelections()
    area.IsSelected(newValue)
    ViewModel.ROTravelRequisitionListPagedCriteria().ProductionAreaID(null)
    if (area.IsSelected()) {
      ViewModel.ROTravelRequisitionListPagedCriteria().ProductionAreaID(area.ProductionAreaID())
    }
    ViewModel.ROTravelRequisitionListPagedCriteria().PreventRefresh(false)
    ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
  },
  clearUserSystemSelections: function () {
    ViewModel.UserSystemList().Iterate(function (itm, indx) {
      itm.IsSelected(false)
    })
  },
  clearUserSystemAreaSelections: function () {
    ViewModel.UserSystemList().Iterate(function (itm, indx) {
      itm.UserSystemAreaList().Iterate(function (area, areaIndx) {
        area.IsSelected(false)
      })
    })
  },
  async cancelSelectedTravellers() {
    let resp = confirm("Are you sure?");
    if (resp == true) {
      for (traveller of ViewModel.TravelRequisition().TravellerList()) {
        if (traveller.IsSelected()) {
          traveller.IsProcessing(true);
          try {
            let succ = await TravelRequisitionTravellerBO.cancelTraveller(traveller);
            console.log(succ);
            OBMisc.Notifications.GritterSuccess("Travel Cancelled", succ.HumanResource, 150);
            KOFormatter.Deserialise(succ, traveller);
            traveller.IsProcessing(false);
          } catch (err) {
            console.log(err);
            OBMisc.Notifications.GritterSuccess(succ.HumanResource + " Failed", err, 500);
            traveller.IsProcessing(false);
          }
        }
      }
    }
  },
  async removeSelectedTravellers() {
    let resp = confirm("Are you sure?");
    if (resp == true) {
      for (traveller of ViewModel.TravelRequisition().TravellerList()) {
        if (traveller.IsSelected()) {
          traveller.IsProcessing(true);
          try {
            let succ = await TravelRequisitionTravellerBO.removeTraveller(traveller);
            console.log(succ);
            OBMisc.Notifications.GritterSuccess("Travel Cancelled", traveller.HumanResource() + " has been removed", 150);
            KOFormatter.Deserialise(succ, traveller);
            traveller.IsProcessing(false);
          } catch (err) {
            console.log(err);
            OBMisc.Notifications.GritterError(traveller.HumanResource() + " failed", err, 500);
            traveller.IsProcessing(false);
          }
        }
      }
      this.fetchTravellers();
    }
  },

  //flights
  fetchFlights: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().FlightsFetched()) {
      FlightBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().FlightList.Set(response.Data)
          ViewModel.TravelRequisition().FlightsFetched(true)
        }
      })
    }
  },
  currentFlight: function () {
    if (ViewModel.CurrentFlightID()) {
      return ViewModel.TravelRequisition().FlightList().Find("FlightID", ViewModel.CurrentFlightID())
    }
  },
  bookFlightPassengers: function (Flight) {
    var me = this;
    if (Flight.IsNew()) {
      me.saveFlight(Flight, {
        onSuccess: function (response) {
          me.editFlight(Flight)
        }
      })
    }
    else {
      me.editFlight(Flight)
    }
  },
  saveFlight: function (Flight, options) {
    var me = this;
    Flight.TravelRequisitionID(Flight.GetParent().TravelRequisitionID())
    FlightBO.saveItem(Flight, {
      onSuccess: function (response) {
        if (response.Success) { options.onSuccess(response) }
      },
      onFail(response) {
        Flight.IsProcessing(false);
      }
    })
  },
  editFlight: function (Flight) {
    ViewModel.CurrentFlightID(Flight.FlightID());
    FlightBO.getPassengers(Flight, {
      onSuccess: function (response) {
        $('#AddFlightPassengerModal').modal()
      }
    })
  },
  closeAddFlightPassengerModal: function (e) {
    var me = this
    if (me.currentFlight().IsDirty()) {
      me.saveFlight(me.currentFlight(), {
        onSuccess: function (response) {

        }
      })
    }
  },
  removeSelectedFlights: function () {
    var r = confirm("Are you sure you want to remove these flights from this requisition?")
    if (r == true) {
      TravelRequisitionBO.removeSelectedFlights(ViewModel.TravelRequisition())
    }
  },

  //rental cars
  fetchRentalCars: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().RentalCarsFetched()) {
      RentalCarBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().RentalCarList.Set(response.Data)
          ViewModel.TravelRequisition().RentalCarsFetched(true)
        }
      })
    }
  },
  currentRentalCar: function () {
    if (ViewModel.CurrentRentalCarGuid()) {
      return ViewModel.TravelRequisition().RentalCarList().Find("Guid", ViewModel.CurrentRentalCarGuid())
    }
    //if (ViewModel.CurrentRentalCarID()) {
    //  return ViewModel.TravelRequisition().RentalCarList().Find("RentalCarID", ViewModel.CurrentRentalCarID())
    //}
  },
  bookRentalCarPassengers: function (RentalCar) {
    var me = this;
    if (RentalCar.IsNew() && RentalCar.IsValid()) {
      me.saveRentalCar(RentalCar, {
        onSuccess: function (response) {
          me.editRentalCar(RentalCar)
        }
      })
    }
    else {
      me.editRentalCar(RentalCar)
    }
  },
  saveRentalCar: function (RentalCar, options) {
    var me = this;
    RentalCar.TravelRequisitionID(RentalCar.GetParent().TravelRequisitionID());
    RentalCarBO.save(RentalCar, {
      onSuccess: function (response) {
        if (response.Success) { options.onSuccess(response) }
      },
      onFail(response) {
        RentalCar.IsProcessing(false);
      }
    })
  },
  editRentalCar: function (RentalCar) {
    if (!RentalCar.IsNew() || (RentalCar.IsNew() && RentalCar.DriverRentalCarHumanResourceID() == 0)) {
      ViewModel.CurrentRentalCarGuid(RentalCar.Guid());
      RentalCarBO.getPassengers(RentalCar, {
        onSuccess: function (response) {
          $('#AddRentalCarPassengerModal').modal()
        }
      })
    }
  },
  closeAddRentalCarPassengerModal: function (e) {
    var me = this
    if (me.currentRentalCar().IsDirty()) {
      me.saveRentalCar(me.currentRentalCar(), {
        onSuccess: function (response) {

        }
      })
    }
  },
  removeSelectedRentalCars: function () {
    var r = confirm("Are you sure you want to remove these rental cars from this requisition?")
    if (r == true) {
      TravelRequisitionBO.removeSelectedRentalCars(ViewModel.TravelRequisition())
    }
  },

  //accommodation
  fetchAccommodation: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().AccommodationFetched()) {
      AccommodationBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().AccommodationList.Set(response.Data)
          ViewModel.TravelRequisition().AccommodationFetched(true)
        }
      })
    }
  },
  currentAccommodation: function () {
    if (ViewModel.CurrentAccommodationID()) {
      return ViewModel.TravelRequisition().AccommodationList().Find("AccommodationID", ViewModel.CurrentAccommodationID())
    }
  },
  bookAccommodationGuests: function (Accommodation) {
    var me = this;
    if (Accommodation.IsNew()) {
      me.saveAccommodation(Accommodation, {
        onSuccess: function (response) {
          me.editAccommodation(Accommodation)
        }
      })
    }
    else {
      me.editAccommodation(Accommodation)
    }
  },
  saveAccommodation: function (Accommodation, options) {
    var me = this;
    Accommodation.TravelRequisitionID(Accommodation.GetParent().TravelRequisitionID())
    AccommodationBO.save(Accommodation, {
      onSuccess: function (response) {
        if (response.Success) { options.onSuccess(response) }
      }
    })
  },
  editAccommodation: function (Accommodation) {
    ViewModel.CurrentAccommodationID(Accommodation.AccommodationID())
    AccommodationBO.getGuests(Accommodation, {
      onSuccess: function (response) {
        $('#AddAccommodationGuestModal').modal()
      }
    })
  },
  closeAddAccommodationGuestModal: function (e) {
    var me = this
    if (me.currentAccommodation().IsDirty()) {
      me.saveAccommodation(me.currentAccommodation(), {
        onSuccess: function (response) {

        }
      })
    }
  },
  removeSelectedAccommodation: function () {
    var r = confirm("Are you sure you want to remove these accommodation bookings from this requisition?")
    if (r == true) {
      TravelRequisitionBO.removeSelectedAccommodation(ViewModel.TravelRequisition())
    }
  },

  //chauffeur
  fetchChauffeurs: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().ChaufferDrivesFetched()) {
      ChauffeurDriverBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().ChauffeurDriverList.Set(response.Data)
          ViewModel.TravelRequisition().ChaufferDrivesFetched(true)
        }
      })
    }
  },
  currentChauffeur: function () {
    if (ViewModel.CurrentChauffeurGuid()) {
      return ViewModel.TravelRequisition().ChauffeurDriverList().Find("Guid", ViewModel.CurrentChauffeurGuid())
    }
    //if (ViewModel.CurrentChauffeurID()) {
    //  return ViewModel.TravelRequisition().ChauffeurDriverList().Find("ChauffeurDriverID", ViewModel.CurrentChauffeurID())
    //}
  },
  bookChaufferPassengers: function (ChauffeurDriver) {
    var me = this;
    if (ChauffeurDriver.IsNew() && ChauffeurDriver.IsValid()) {
      me.saveChauffeurDriver(ChauffeurDriver, {
        onSuccess: function (response) {
          me.editChauffeurDriver(ChauffeurDriver)
        }
      })
    }
    else {
      me.editChauffeurDriver(ChauffeurDriver)
    }
  },
  saveChauffeurDriver: function (ChauffeurDriver, options) {
    var me = this;
    ChauffeurDriver.TravelRequisitionID(ChauffeurDriver.GetParent().TravelRequisitionID())
    ChauffeurDriverBO.save(ChauffeurDriver, {
      onSuccess: function (response) {
        if (response.Success) { options.onSuccess(response) }
      }
    })
  },
  editChauffeurDriver: function (ChauffeurDriver) {
    ViewModel.CurrentChauffeurGuid(ChauffeurDriver.Guid())
    //ViewModel.CurrentChauffeurID(ChauffeurDriver.ChauffeurDriverID())
    ChauffeurDriverBO.getPassengers(ChauffeurDriver, {
      onSuccess: function (response) {
        $('#AddChauffeurDrivePassengerModal').modal()
      }
    })
  },
  closeAddChauffeurDrivePassengerModal: function () {
    var me = this
    if (me.currentChauffeur().IsDirty()) {
      me.saveChauffeurDriver(me.currentChauffeur(), {
        onSuccess: function (response) {

        }
      })
    }
  },
  removeSelectedChauffeurDrives: function () {
    var r = confirm("Are you sure you want to remove these chauffeur bookings from this requisition?")
    if (r == true) {
      TravelRequisitionBO.removeSelectedChauffeurDrives(ViewModel.TravelRequisition())
    }
  },

  //advances
  fetchAdvances: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().TravelAdvancesFetched()) {
      TravelAdvanceDetailBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().ProductionTravelAdvanceDetailList.Set(response.Data)
          ViewModel.TravelRequisition().TravelAdvancesFetched(true)
        }
      })
    }
  },
  removeSelectedTravelAdvances: function () {
    var r = confirm("Are you sure you want to remove these travel advances from this requisition?")
    if (r == true) {
      TravelRequisitionBO.removeSelectedTravelAdvances(ViewModel.TravelRequisition())
    }
  },

  //comments
  fetchComments: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().CommentsFetched()) {
      TravelReqCommentBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().TravelReqCommentList.Set(response.Data)
          ViewModel.TravelRequisition().CommentsFetched(true)
        }
      })
    }
  },
  removeSelectedTravelComments: function () {
    var r = confirm("Are you sure you want to remove these comments from this requisition?")
    if (r == true) {
      TravelRequisitionBO.removeSelectedComments(ViewModel.TravelRequisition())
    }
  },

  //S&T
  fetchSnT: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().TravellerSnTFetched()) {
      TravelRequisitionBO.fetchSnT(ViewModel.TravelRequisition())
    }
  },
  bulkEditSnT: function () {
    //unique to VM so not required on TravelRequisitionBO object
    ViewModel.SnTTemplate(new SnTTemplateObject())
    ViewModel.SnTTemplate().TravelRequisitionID(ViewModel.TravelRequisition().TravelRequisitionID())
    SnTTemplateTravellerBO.get({
      criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
      onSuccess: function (response) {
        ViewModel.SnTTemplate().SnTTemplateTravellerList.Set(response.Data)
      }
    })
    $('#SnTTemplateModal').modal()
  },
  applyBulkGroupPolicy: function (template, options) {
    var r = confirm("Are you sure you want to apply the S&T template?");
    if (r == true) {
      SnTTemplateBO.applyTemplate(template, {
        onSuccess: function (response) {
          ViewModel.TravelRequisition().TravellerSnTFetched(false)
          TravelRequisitionBO.fetchSnT(ViewModel.TravelRequisition())
          $('#SnTTemplateModal').modal('hide')
        },
        onFail: function (response) {

        }
      })
    }
  },
  resetSnTs: function () {
    var r = confirm("Are you sure you want to reset the selected S&T?");
    if (r == true) {
      TravelRequisitionBO.resetSnT(ViewModel.TravelRequisition())
    }
  },

  //travel availability
  fetchTravelAvailability: function () {
    if (ViewModel.TravelRequisition() && !ViewModel.TravelRequisition().TravelAvailabilityFetched()) {
      ROTravelAvailableHumanResourceBO.get({
        criteria: { TravelRequisitionID: ViewModel.TravelRequisition().TravelRequisitionID() },
        onSuccess: function (response) {
          ViewModel.TravelRequisition().ROTravelAvailableHumanResourceList.Set(response.Data)
          ViewModel.TravelRequisition().TravelAvailabilityFetched(true)
        }
      })
    }
  },

  //traveller summary
  viewTravellerDetail: function (ROTraveller) {
    this.fetchTravellerSummary(ROTraveller.HumanResourceID(), ROTraveller.TravelRequisitionID())
  },
  viewCrewMemberDetail: function (crewMember) {
    this.fetchTravellerSummary(crewMember.HumanResourceID(), ViewModel.TravelRequisition().TravelRequisitionID())
  },
  fetchTravellerSummary: function (humanResourceID, travelRequisitionID) {
    ROTravellerSummaryBO.get({
      criteria: {
        HumanResourceID: humanResourceID,
        TravelRequisitionID: travelRequisitionID
      },
      onSuccess: function (response) {
        ViewModel.CurrentROTravellerSummary.Set(response.Data[0])
        $('#TravellerSummaryModal').modal()
      },
      onFail: function (response) {

      }
    })
  },

  //bulk rental cars
  currentBulkRentalCar: function () {
    return ViewModel.CurrentBulkRentalCar()
  },
  setupBulkRentalCar: function () {
    ViewModel.CurrentBulkRentalCar(new BulkRentalCarObject())
    this.showBulkRentalCarModal()
  },
  showBulkRentalCarModal: function () {
    $("#BulkRentalCarModal").off('shown.bs.modal')
    $("#BulkRentalCarModal").off('hidden.bs.modal')
    $("#BulkRentalCarModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentBulkRentalCar())
    })
    $("#BulkRentalCarModal").on('hidden.bs.modal', function () {
      ViewModel.CurrentBulkRentalCar(null);
      Singular.Validation.CheckRules(ViewModel);
    })
    $("#BulkRentalCarModal").modal()
  }

};

//#region Criteria Overrides for Paged Lists

ROTravelRequisitionListPagedCriteriaBO = {
  TitleSet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  StartDateSet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  EndDateSet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  CreatedBySet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  RefNoSet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  SystemIDSet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  ProductionAreaIDSet: function (self) {
    if (!self.PreventRefresh()) {
      ViewModel.ROTravelRequisitionListPagedPagingManager().Refresh()
    }
  },
  HumanResourceIDSet(self) { },
  HumanResourceSet(self) { },
  setHumanResourceIDCriteriaBeforeRefresh(args) {
    args.Data.SystemIDs = [args.Object.SystemID()];
    args.Data.ProductionAreaIDs = [args.Object.ProductionAreaID()];
  },
  triggerHumanResourceIDAutoPopulate(args) {
    args.AutoPopulate = true;
  },
  afterHumanResourceIDRefreshAjax(args) {

  },
  onHumanResourceIDSelected(selectedItem, businessObject) {

  }
};

ROHumanResourceTravelReqListCriteriaBO = {
  KeyWordSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  ContractTypeIDSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  DisciplineIDSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  FirstnameSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  SecondNameSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  PreferredNameSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  SurnameSet: function (self) {
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  SystemIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceTravelReqListManager().IsLoading()) {
      this.afterRefresh();
    }
  },
  ProductionAreaIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceTravelReqListManager().IsLoading()) {
      this.afterRefresh();
    }
  },
  SetXMLCriteria: function () {
    //clear current selections
    var selectedSystems = [];
    var selectedAreas = [];
    ViewModel.UserSystemList().Iterate(function (uSys, uSysInd) {
      if (uSys.IsSelected()) {
        selectedSystems.push(uSys.SystemID());
      }
      uSys.UserSystemAreaList().Iterate(function (uSyAr, uSyArInd) {
        if (uSyAr.IsSelected()) {
          selectedAreas.push(uSyAr.ProductionAreaID());
        }
      })
    })
    ViewModel.ROHumanResourceTravelReqListCriteria().SystemIDsXml(OBMisc.Xml.getXmlIDs(selectedSystems));
    ViewModel.ROHumanResourceTravelReqListCriteria().ProductionAreaIDsXml(OBMisc.Xml.getXmlIDs(selectedAreas));
    ViewModel.ROHumanResourceTravelReqListManager().Refresh();
    this.afterRefresh();
  },
  afterRefresh: function () {
    //ViewModel.ROHumanResourceTravelReqListManager().afterRefresh = function () {
    //  ViewModel.TravellersToAddList().Iterate(function (itm, ind) {
    //    ViewModel.ROHumanResourceTravelReqList().Iterate(function (itm2, ind2) {
    //      if (itm.HumanResourceID() == itm2.HumanResourceID()) {
    //        itm2.Booked(true);
    //      }
    //    });
    //  });
    //};
  }
};

ROHumanResourcePagedListCriteriaBO = {
  ContractTypeIDSet: function (self) { FindHRControl.refresh() },
  DisciplineIDSet: function (self) { FindHRControl.refresh() },
  FirstnameSet: function (self) { FindHRControl.refresh() },
  SecondNameSet: function (self) { FindHRControl.refresh() },
  PreferredNameSet: function (self) { FindHRControl.refresh() },
  SurnameSet: function (self) { FindHRControl.refresh() },
  KeywordSet: function (self) { FindHRControl.refresh() },
  SystemIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceListManager().IsLoading()) {
      FindHRControl.refresh()
    }
  },
  ProductionAreaIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceListManager().IsLoading()) {
      FindHRControl.refresh()
    }
  },
  SetXMLCriteria: function () {
    //clear current selections
    var selectedSystems = []
    var selectedAreas = []
    ViewModel.UserSystemList().Iterate(function (uSys, uSysInd) {
      if (uSys.IsSelected()) {
        selectedSystems.push(uSys.SystemID())
      }
      uSys.UserSystemAreaList().Iterate(function (uSyAr, uSyArInd) {
        if (uSyAr.IsSelected()) {
          selectedAreas.push(uSyAr.ProductionAreaID())
        }
      })
    })
    ViewModel.ROHumanResourceListCriteria().SystemIDsXml(OBMisc.Xml.getXmlIDs(selectedSystems))
    ViewModel.ROHumanResourceListCriteria().ProductionAreaIDsXml(OBMisc.Xml.getXmlIDs(selectedAreas))
  }
};

ROSynergyEventSearchBO = {
  IsSelectedSet: function (self) {
    if (self.IsSelected() && ViewModel.AdHocBooking()) {
      ViewModel.AdHocBooking().GenRefNo(self.GenRefNumber())
    } else {
      ViewModel.AdHocBooking().GenRefNo(null)
    }
  }
};

//#endregion

//#region On Page Load

Singular.OnPageLoad(function () {

  TravelVM.onPageLoad()

})

//#endregion