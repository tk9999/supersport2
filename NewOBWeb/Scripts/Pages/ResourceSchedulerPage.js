﻿var CurrentHROffPeriodControl = null,
    SecondmentModal = new HRSecondmentModal({
      afterSaveSuccess: function (control, response) {
        console.log(control, response)
      }
    }),
    OffPeriodModal = new HROffPeriodModal({
      afterSaveSuccess: function (control, response) {
        console.log(control, response)
      }
    }),
    CurrentStudioSupervisorShiftControl = new StudioSupervisorShiftModal(),
    CurrentPlayoutOpsShiftControl = new PlayoutOpsShiftModal(),
    CurrentPlayoutOpsShiftControlMCR = new PlayoutOpsShiftModalMCR(),
    CurrentICRShiftControl = new ICRShiftModal(),
    CurrentStageHandShiftControl = new StageHandShiftModal(),
    CurrentContentShiftControl = new ContentShiftModal();

Singular.OnPageLoad(function () {

  window.Scheduler = new ResourceSchedulerBase({})

})