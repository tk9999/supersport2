﻿EquipmentPage = {
  findEquipment: function () {
    this.refreshROEquipmentList()
    $("#FindROEquipment").modal()
  },
  refreshROEquipmentList: function () {
    ViewModel.ROEquipmentListPagingManager().Refresh()
  },
  editROEquipment: function (roEquipment) {
    EquipmentBO.get({
      criteria: { EquipmentID: roEquipment.EquipmentID() },
      onSuccess: function (response) {
        ViewModel.CurrentEquipment.Set(response.Data[0])
        Singular.Validation.CheckRules(ViewModel.CurrentEquipment())
        $("#FindROEquipment").modal('hide')
      }
    })
  },
  saveEquipment: function (equipment) {
    equipment.IsProcessing(true)
    EquipmentBO.save(equipment, {
      onSuccess: function (response) {
        KOFormatter.Deserialise(response.Data, equipment)
        equipment.IsProcessing(false)
        Singular.Validation.CheckRules(ViewModel.CurrentEquipment())
        $("#FindROEquipment").modal('hide')
      }
    })
  },
  newEquipment: function () {
    ViewModel.CurrentEquipment(new EquipmentObject())
    Singular.Validation.CheckRules(ViewModel.CurrentEquipment())
  }
}

Singular.OnPageLoad(function () {

})
