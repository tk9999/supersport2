﻿UserSecurityPage = {
  addUserVisibility: function ()
  {
    ViewModel.UserTimesheetVisibleDisciplineList.AddNew();
  },
  removeUserVisibility: function (obj)
  {
    var ThisUSer = ViewModel.UserTimesheetVisibleDisciplineList().Find("Guid", obj.Guid());
    if (ThisUSer)
    {
      ViewModel.UserTimesheetVisibleDisciplineList.Remove(ThisUSer);
    }
  },
  pushUsers: function (obj)
  {
    obj.IsSelected(!obj.IsSelected());
    var index = ViewModel.UserIDsToRemoveList.indexOf(obj.UserTimesheetVisibleDisciplineID());
    if (index > -1)
    {
      ViewModel.UserIDsToRemoveList.splice(index, 1);
    } else
    {
      ViewModel.UserIDsToRemoveList.push(obj.UserTimesheetVisibleDisciplineID());
    }
  },
  removeUsers: function ()
  {
    var listToSend = KOFormatterFull.Serialise(ViewModel.UserTimesheetVisibleDisciplineList());
    Singular.ShowLoadingBar();
    ViewModel.CallServerMethod("RemoveUserVisDiscAccess", {
      list: ViewModel.UserIDsToRemoveList(),
      Users: listToSend
    }, function (response)
    {
      if (response.Success)
      {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "User Timesheet Visibility Updated", 1500, 'top-right');
        ViewModel.UserTimesheetVisibleDisciplineListManager().Refresh();
      } else
      {
        OBMisc.Notifications.GritterError("Error Updating User Timesheet Visibility: " + response.ErrorText, "", 3000, 'top-right');
      }
      Singular.HideLoadingBar();
    });
  },
  saveUserTimesheetVisibility: function ()
  {
    var listToSend = KOFormatterFull.Serialise(ViewModel.UserTimesheetVisibleDisciplineList());
    Singular.ShowLoadingBar();
    ViewModel.CallServerMethod("SaveUserTimesheetVisibility", {
      UserTimesheetVisibleDisciplineList: listToSend
    }, function (response)
    {
      if (response.Success)
      {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "User Timesheet Visibility Updated", 1500, 'top-right');
        ViewModel.UserTimesheetVisibleDisciplineListManager().Refresh();
      } else
      {
        OBMisc.Notifications.GritterError("Error Updating User Timesheet Visibility: " + response.ErrorText, "", 3000, 'top-right');
      }
      Singular.HideLoadingBar();
    });
  },
  SelectTab: function (TabID)
  {
    var elem = $("." + TabID)
    $(elem).tab('show');
  },
  getVMBrokenRulesHTML: function ()
  {
    var ErrorString = ''
    if (ViewModel.IsValid() == false)
    {
      ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(ViewModel);
    }
    return ErrorString;
  },
  checkVMValidity: function ()
  {
    var valid = true;
    if (!ViewModel.IsValid())
    {
      valid = false;
    }
    return valid;
  },
  openBulkAddTimesheetAccess: function ()
  {
    var n = ViewModel.TemplateUserTimesheetVisibleDisciplineList.AddNew();
    n.UserID(0);
    var listToSend = KOFormatterFull.Serialise(ViewModel.UsersToAddList())
    ViewModel.CallServerMethod("CleanUserList", {
      UsersList: listToSend
    }, function (response)
    {
      if (response.Success)
      {
        KOFormatter.Deserialise(response.Data, ViewModel.UsersToAddList)
        ViewModel.ROUserFindListPagedListCriteria().KeyWord('')
        ViewModel.ROUserFindListPagedListManager().Refresh()
        $("#BulkAddTimesheetAccess").modal();
      }
    });
  },
  addSystem: function (obj)
  {
    ViewModel.UserSystemList().Iterate(function (itm, ind)
    {
      itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
    });
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected())
    {
      ViewModel.UserTimesheetVisibleDisciplineListCriteria().SystemID(obj.SystemID());
      ViewModel.ROUserFindListPagedListCriteria().SystemID(obj.SystemID());
      ViewModel.ROUserFindListPagedListManager().Refresh();
    }
    else
    {
      ViewModel.UserTimesheetVisibleDisciplineListCriteria().SystemID(null);
      ViewModel.ROUserFindListPagedListCriteria().SystemID(null);
      ViewModel.ROUserFindListPagedListManager().Refresh();
    }
  },
  addProductionArea: function (obj)
  {
    if (!obj.IsSelected())
    {
      ViewModel.UserSystemList().Iterate(function (sys, ind)
      {
        sys.UserSystemAreaList().Iterate(function (pa, indx)
        {
          pa.IsSelected() ? pa.IsSelected(!pa.IsSelected()) : pa.IsSelected(pa.IsSelected());
        });
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected())
    {
      ViewModel.UserTimesheetVisibleDisciplineListCriteria().ProductionAreaID(obj.ProductionAreaID());
      ViewModel.ROUserFindListPagedListCriteria().ProductionAreaID(obj.ProductionAreaID());
      ViewModel.ROUserFindListPagedListManager().Refresh();
    }
    else
    {
      ViewModel.UserTimesheetVisibleDisciplineListCriteria().ProductionAreaID(null);
      ViewModel.ROUserFindListPagedListCriteria().ProductionAreaID(null);
      ViewModel.ROUserFindListPagedListManager().Refresh();
    }
  },
  selectUser: function (obj)
  {
    obj.IsSelected(!obj.IsSelected());
    var listToSend = KOFormatterFull.Serialise(ViewModel.UsersToAddList());
    Singular.ShowLoadingBar()
    ViewModel.CallServerMethod("UpdateBulkUserList", {
      UserID: obj.UserID(),
      ThisUserList: listToSend
    }, function (response)
    {
      if (response.Success)
      {
        KOFormatter.Deserialise(response.Data, ViewModel.UsersToAddList)
      }
      Singular.HideLoadingBar()
    });
  },
  closeBulkAddTimesheetAccess: function ()
  {
    Singular.ShowLoadingBar()
    ViewModel.CallServerMethod("PostApplyCleanup", {
    }, function (response)
    {
      if (response.Success)
      {
        ViewModel.UserTimesheetVisibleDisciplineListManager().Refresh();
        ViewModel.TemplateUserTimesheetVisibleDisciplineList.Set([])
        Singular.Validation.CheckRules(ViewModel)
      }
      Singular.HideLoadingBar()
    });
  },
  applyTemplate: function (crit)
  {
    var listToSend = KOFormatterFull.Serialise(ViewModel.UsersToAddList());
    Singular.ShowLoadingBar();
    ViewModel.CallServerMethod("ApplyTemplate", {
      UsersToAddList: listToSend,
      DisciplineID: ViewModel.TemplateUserTimesheetVisibleDisciplineList()[0].DisciplineID(),
      SystemID: ViewModel.TemplateUserTimesheetVisibleDisciplineList()[0].SystemID(),
      ProductionAreaID: ViewModel.TemplateUserTimesheetVisibleDisciplineList()[0].ProductionAreaID()
    }, function (response)
    {
      if (response.Success)
      {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "User Timesheet Visibility Updated", 1500, 'top-right');
        ViewModel.ROUserFindListPagedListManager().Refresh();
      } else
      {
        OBMisc.Notifications.GritterError("Error Updating User Timesheet Visibility: " + response.ErrorText, "", 3000, 'top-right');
      }
      Singular.HideLoadingBar();
    });
  },

  //Functions Moved from Users.aspx

  AddNewUserSystemGroup: function (user)
  {
    Singular.AddAndFocus(user.UserSystemList)
  }, // ?
  CloseUserModal: function ()
  {
    $('#UserModal').modal('hide');
  }, // ?
  SaveCurrentUser: function ()
  {
    //Call server method for saving the user
    ViewModel.CallServerMethod("SaveUser", {
      //Convert to KnockOut Object
      User: ViewModel.SelectedUser().Serialise()
    },
    function (response)
    {
      if (response.Success)
      {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 350)
        $('#UserModal').modal('hide');
      }
      else
      {
        OBMisc.Notifications.GritterError("Failed to Save User", response.ErrorText, 500)
      }
    });
  }, //edited
  CancelCurrentUser: function ()
  {
    ViewModel.SelectedUser(null);
    ViewModel.CurrentHumanResource(null);
    $('#UserModal').modal('hide');
  },
  AddNewUser: function ()
  {
    ViewModel.CallServerMethod("NewCurrentUser", {
    }, function (response)
    {

      ViewModel.SelectedUser.Set(response.Data)

      $('#UserModal').off('shown.bs.modal')
      $('#UserModal').on('shown.bs.modal', function ()
      {
        Singular.Validation.CheckRules(ViewModel.SelectedUser())
      });
      $('#UserModal').modal();

    });
  }, //edited
  ShowFilters: function ()
  {
    $('#ShowFilters').modal();
  },
  EditUser: function (ROuser)
  {
    ViewModel.CallServerMethod("SetCurrentUser", {
      UserID: ROuser.UserID()
    }, function (response)
    {
      if (response.Success)
      {
        //Show the set user on the ViewModel
        ViewModel.SelectedUser.Set(response.Data)
        $('#UserModal').modal();
      }
      else
      {
        OBMisc.Notifications.GritterError("Failed to locate user", response.ErrorText, 500)
      }
    });
  }, //edited
  IsCurrentUserValid: function ()
  {
    var valid = true;
    if (ViewModel.SelectedUser())
    {
      Singular.Validation.CheckRules(ViewModel.SelectedUser());
      return ViewModel.SelectedUser().IsValid();
    }
    return valid;
  }, // ?
  CanSendLoginDetails: function ()
  {
    if (ViewModel.SelectedUser())
    {
      if (ViewModel.SelectedUser().IsValid()
         && !ViewModel.SelectedUser().IsNew())
      {
        return true;
      }
    }
    return false;
  },
  CanResetPassword: function ()
  {
    if (ViewModel.SelectedUser())
    {
      if (ViewModel.SelectedUser().IsValid()
         && !ViewModel.SelectedUser().IsNew())
      {
        return true;
      }
    }
    return false;
  },
  DeleteROUser: function (ROUser)
  {

    Singular.ShowMessageQuestion("Delete User",
      "Are you sure you wish to delete this user?",
      function ()
      {
        ViewModel.IsProcessing(true)
        ViewModel.CallServerMethod("DeleteUser", { UserID: ROUser.UserID() },
          function (response)
          {
            if (response.Success)
            {
              ViewModel.UserPagingInfo().Refresh()
              OBMisc.Notifications.GritterSuccess("Deleted " + ROUser.FirstName() + " " + ROUser.Surname() + " Successfully", "", 250)
              ViewModel.IsProcessing(false)
            }
            else
            {
              OBMisc.Notifications.GritterError("Failed to Delete", response.ErrorText, 500)
              ViewModel.IsProcessing(false)
            };
          });
      },
      function ()
      {
        //do nothing
      });

  }, //edited
  UserValid: function ()
  {
    if (ViewModel.SelectedUser())
    {
      return ViewModel.SelectedUser().IsValid();
    }
    return false;
  },
  BulkSendPasswords: function ()
  {
    ViewModel.IsProcessing(true);
    var SelectedUserIDs = [];
    ViewModel.ROUserList().Iterate(function (User, Index)
    {
      if (User.IsSelected())
      {
        SelectedUserIDs.push(User.UserID());
      }
    })
    ViewModel.CallServerMethod("BulkSendPasswords", {
      SystemIDs: ViewModel.UserListCriteria().SystemIDs(),
      ProductionAreaIDs: ViewModel.UserListCriteria().ProductionAreaIDs(),
      ContractTypeIDs: ViewModel.UserListCriteria().ContractTypeIDs(),
      FilterName: ViewModel.UserListCriteria().FilterName(),
      SelectedUserIDs: SelectedUserIDs
    },
    function (response)
    {
      if (response.Success)
      {
        OBMisc.Notifications.GritterSuccess("Send Login Details Succeeded", "", 250);
      }
      else
      {
        OBMisc.Notifications.GritterError("Send Login Details Failed", response.ErrorText, 1000);
      }
      ViewModel.IsProcessing(false);
    })
  },
  BulkResetPasswords: function ()
  {
    ViewModel.IsProcessing(true)
    var SelectedUserIDs = [];
    ViewModel.ROUserList().Iterate(function (User, Index)
    {
      if (User.IsSelected())
      {
        SelectedUserIDs.push(User.UserID());
      }
    })
    ViewModel.CallServerMethod("BulkResetPasswords", {
      SystemIDs: ViewModel.UserListCriteria().SystemIDs(),
      ProductionAreaIDs: ViewModel.UserListCriteria().ProductionAreaIDs(),
      ContractTypeIDs: ViewModel.UserListCriteria().ContractTypeIDs(),
      FilterName: ViewModel.UserListCriteria().FilterName(),
      SelectedUserIDs: SelectedUserIDs
    },
    function (response)
    {
      if (response.Success)
      {
        OBMisc.Notifications.GritterSuccess("Reset Passwords Succeeded", "", 250)
      }
      else
      {
        OBMisc.Notifications.GritterError("Reset Passwords Failed", response.ErrorText, 1000)
      }
      ViewModel.IsProcessing(false)
    })
  }
}

Singular.OnPageLoad(function ()
{
  UserSecurityPage.SelectTab("Users");
  ViewModel.UserTimesheetVisibleDisciplineListManager().Refresh();
  ViewModel.ROUserFindListPagedListManager().Refresh();
  $('#BulkAddTimesheetAccess').on('hide.bs.modal', function (e)
  {
    UserSecurityPage.closeBulkAddTimesheetAccess();
  });
  $('#UserModal').on('hidden.bs.modal', function (e)
  {
    if (ViewModel.SelectedUser())
    {
      ViewModel.SelectedUser(null)
      //if (!ViewModel.SelectedUser().IsValid()) {
      //  alert('Please fix the errors');
      //  return false;
      //} else if (ViewModel.SelectedUser().IsDirty()) {
      //  e.preventDefault();
      //  alert('Please Save your changes');
      //  return false;
      //}
    }
  })
});