﻿//page is now ready, initialize the calendar...
var DELAY = 250; // ms
var fetchTimeout = null;
var ROBookingsControl = null;

UserProfilePage = {
  /*-----------Queries--------------*/
  QueryDetails: function () {
    $("#QueryDetailsModal").modal();
    $('#QueryDetailsModal').on('shown.bs.modal', function () {
      ViewModel.QueryModalOpen(true)
      ViewModel.HumanResourceSwap(null)
      Singular.Validation.CheckRules(ViewModel)
    });
    $('#QueryDetailsModal').on('hidden.bs.modal', function (e) {
      ViewModel.QueryModalOpen(false)
      ViewModel.HumanResourceSwap(null)
      Singular.Validation.CheckRules(ViewModel)
    })
  },
  submitDetailsQuery: function () {
    Singular.SendCommand("QueryDetails", {}, function (response) {
      if (response.Success) {
        $('#QueryDetailsModal').modal('hide')
        OBMisc.Notifications.GritterSuccess('Query Submitted', 'Your query has been submitted successfully', 1000)
      }
      else {
        OBMisc.Notifications.GritterError('Query Not Submitted', 'Your query was not submitted, an error occured', 1000)
      }
    })
  },

  /*-----------Timesheets--------------*/
  SelectTimesheetMonth: function () {
    $('#SelectTimesheetMonth').on('shown.bs.modal', function (e) {
      ViewModel.ROTimesheetMonthPagedListManager().Refresh();
    });
    $("#SelectTimesheetMonth").modal();
  },
  TimesheetMonthSelected: function (ROTimesheetMonth) {
    Singular.SendCommand("TimesheetMonthSelected", {
      TimesheetMonthID: ROTimesheetMonth.TimesheetMonthID()
    }, function (response) {
      $("#SelectTimesheetMonth").modal('hide');
    });
  },
  DelayedFilterROTimesheetMonthList: function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      ViewModel.ROTimesheetMonthPagedListManager().Refresh();
    }, DELAY);
  },
  ViewTimesheetSummary: function () {
    $("#TimesheetSummary").modal();
  },
  EditCrewTimesheet: function (CrewTimesheet) {
    ViewModel.CurrentCrewTimesheetGuid(CrewTimesheet.Guid());
    $("#EditCrewTimesheet").modal();
  },
  SaveCrewTimesheet: function (CrewTimesheet) {
    Singular.SendCommand("SaveCrewTimesheet",
      {},
      function (response) {

      })
  },
  SelectPaymentRun: function () {
    $('#SelectPaymentRun').on('shown.bs.modal', function (e) {
      ViewModel.ROPaymentRunListManager().Refresh();
    });
    $("#SelectPaymentRun").modal();
  },
  PaymentRunSelected: function (ROPaymentRun) {
    ViewModel.CurrentPaymentRun(ROPaymentRun)
    Singular.GetDataStateless("OBLib.Timesheets.FreelancerTimesheetList, OBLib",
                              {
                                HumanResourceID: ViewModel.ROUserProfile().HumanResourceID(),
                                StartDate: ROPaymentRun.StartDate(),
                                EndDate: ROPaymentRun.EndDate(),
                                SystemID: ROPaymentRun.SystemID(),
                                ProductionAreaID: null
                              },
                              function (response) {
                                if (response.Success) {
                                  ViewModel.CurrentFreelancerTimesheetList.Set(response.Data)
                                  $("#SelectPaymentRun").modal('hide')
                                }
                                else {
                                  OBMisc.Notifications.GritterError("Error Fetching Timesheet", response.ErrorText, 1000)
                                  $("#SelectPaymentRun").modal('hide')
                                }
                              });
    //Singular.SendCommand("PaymentRunSelected", {
    //  PaymentRunID: ROPaymentRun.PaymentRunID()
    //}, function (response) {
    //  $("#SelectPaymentRun").modal('hide');
    //})
  },
  printFreelancerTimesheet: function (ROPaymentRun) {
    Singular.ShowLoadingBar()
    ViewModel.CallServerMethod("PrintServicesFreelancerTimesheet", {
      HumanResourceID: ViewModel.ROUserProfile().HumanResourceID(),
      EndDate: ROPaymentRun.EndDate()
    },
    function (response) {
      if (response.Success) {
        Singular.DownloadFile(null, response.Data);
      } else {
        OBMisc.Notifications.GritterError('Error Downloading', response.ErrorText, null)
      }
      Singular.HideLoadingBar()
    })
  },

  /*-----------Schedule--------------*/
  GetCalendarEvents: function (start, end, timezone, callback) {
    ViewModel.FetchingSchedule(true)
    var allBookings = [];
    ViewModel.ScheduleStartDate(start)
    ViewModel.ScheduleEndDate(end)
    ViewModel.CallServerMethod("GetUserSchedule", {
      StartDate: start.toDate().format('dd MMM yyyy'),
      EndDate: end.toDate().format('dd MMM yyyy')
    },
    function (response) {
      if (response.Success) {
        var Resource = response.Data[0]
        Resource.ROResourceScheduleList.Iterate(function (sched, schedIndex) {
          var sd, ed;
          if (sched.StartDateTimeBuffer) {
            sd = new Date(sched.StartDateTimeBuffer);
          } else {
            sd = new Date(sched.StartDateTime);
          }
          if (sched.EndDateTimeBuffer) {
            ed = new Date(sched.EndDateTimeBuffer);
          } else {
            ed = new Date(sched.EndDateTime);
          }
          obj = {
            title: sched.ResourceBookingDescription,
            start: sd,
            end: ed,
            allDay: false,
            booking: sched,
            travel: null,
            borderColor: '#3a87ad'
          }
          allBookings.push(obj);
        })
        Resource.ROResourceTravelDayList.Iterate(function (trv, trvIndex) {
          obj = {
            title: '',
            start: new Date(trv.StartDateTime),
            end: new Date(trv.EndDateTime),
            allDay: true,
            booking: null,
            travel: trv,
            backgroundColor: '#fff',
            borderColor: '#fff'
            //backgroundColor: _getBackgroundColor(ROHRSchedule)
          }
          allBookings.push(obj);
        })
      }
      else {

      }
      callback(allBookings)
      ViewModel.FetchingSchedule(false)
    })
  },
  SetupCalendar: function () {
    $('#calendar').fullCalendar({
      header: {
        left: 'title',
        center: '',
        right: 'month,agendaWeek,agendaDay, today, prev,next',
      },
      editable: false,
      events: function (start, end, timezone, callback) {
        UserProfilePage.GetCalendarEvents(start, end, timezone, callback);
      },
      timeFormat: 'HH:mm',
      eventRender: function (event, element) {
        element.find(".fc-event-title").remove();
        //element.find(".fc-event-time").remove();
        var content = element.find(".fc-content"); //.remove();
        var new_description = '',
            flightIcon = '<span class="fa fa-plane travel-icon travel-flight"></span>',
            carIcon = '<span class="fa fa-car travel-icon travel-rental-car"></span>',
            accomIcon = '<span class="fa fa-hotel travel-icon travel-accommodation"></span>'
        if (event.travel != null) {
          if (event.travel.TotalFlights > 0) {
            new_description += flightIcon;
          }
          if (event.travel.TotalAccommodation > 0) {
            new_description += accomIcon;
          }
          if (event.travel.TotalCars > 0) {
            new_description += carIcon;
          }
          element.find(".fc-event-title").remove();
          $(content).append(new_description);
        } else if (event.booking != null) {
          element.find(".fc-event-title").remove();
          //new_description = '<span class="hr-booking">' + event.booking.ResourceBookingDescription + '</span>';
          //$(content).append(new_description);
        }
      },
      eventOrder: function (a, b) {
        if (a.travel != null) {
          return -1000000000
        } else {
          a.start - b.start
        }
      },
      start: new Date(ViewModel.ScheduleStartDate()),
      end: new Date(ViewModel.ScheduleEndDate())
    });
  },
  SelectTab: function (TabID) {
    var elem = $("." + TabID)
    $(elem).tab('show');
  },
  CalendarTabSelected: function () {
    $('#calendar').fullCalendar('render')
  },
  AddTabEvents: function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      switch (e.target.hash) {
        case "#MySchedule":
          UserProfilePage.CalendarTabSelected()
          break;
      }
      //e.target // newly activated tab
      //e.relatedTarget // previous active tab
    })
  },
  downloadSchedule: function () {
    var date = $("#calendar").fullCalendar('getDate');
    var monthInt = date.toDate().getMonth() + 1;
    ViewModel.CallServerMethod("DownloadMonthlyScheduleStateless", {
      HumanResourceID: ViewModel.ROUserProfile().HumanResourceID(),
      StartDate: date.clone().startOf('month').format("DD MMMM YYYY"),
      EndDate: date.clone().endOf('month').format("DD MMMM YYYY")
    }, function (response) {
      if (response.Success) {
        Singular.DownloadFile(null, response.Data);
      } else {
        OBMisc.Notifications.GritterError('Error Downloading', response.ErrorText, null)
      }
    })
  },

  /*-----------Swaps--------------*/
  SCurrentMonth: function () {
    if (ViewModel.SelectedMonth()) {
      return ViewModel.SelectedMonth();
    }
    return "";
  },
  RequestSwap: function (timelineGroup) {

    ViewModel.HumanResourceSwap(null)
    ViewModel.HumanResourceSwap(new HumanResourceSwapObject())
    ViewModel.HumanResourceSwap().HumanResourceID(ViewModel.ROUserProfile().HumanResourceID())
    ViewModel.HumanResourceSwap().HumanResource(ViewModel.ROUserProfile().DisplayName())
    ViewModel.HumanResourceSwap().ContractType(ViewModel.ROUserProfile().ContractType())
    ViewModel.HumanResourceSwap().SwapWithHumanResource(timelineGroup.Resource.ResourceName)
    ViewModel.HumanResourceSwap().SwapWithHumanResourceID(timelineGroup.Resource.HumanResourceID)
    ViewModel.HumanResourceSwap().SwapWithContractType(timelineGroup.Resource.ContractType)

    $('#HumanResourceSwapModal').off('shown.bs.modal')
    $('#HumanResourceSwapModal').off('hidden.bs.modal')
    $('#HumanResourceSwapModal').on('shown.bs.modal', function () {
      ViewModel.SwapModalOpen(true)
      Singular.Validation.CheckRules(ViewModel.HumanResourceSwap())
    })
    $('#HumanResourceSwapModal').on('hidden.bs.modal', function (e) {
      ViewModel.SwapModalOpen(false)
      ViewModel.HumanResourceSwap(null)
    })
    $("#HumanResourceSwapModal").modal();
  },
  SubmitSwap: function () {
    HumanResourceSwapBO2.saveItem(ViewModel.HumanResourceSwap(), {
      onSuccess: function (response) {
        $("#HumanResourceSwapModal").modal('hide')
      },
      onFail: function (response) { }
    })
    //Singular.SendCommand("SaveSwap", {}, function (response) {
    //  if (response.Success) {
    //    $("#HumanResourceSwapModal").modal('hide')
    //    OBMisc.Notifications.GritterSuccess('Swap Submitted', 'Your swap has been submitted successfully', 1000)
    //    ViewModel.ROHumanResourceSwapListManager().Refresh()
    //  }
    //  else {
    //    OBMisc.Notifications.GritterError('Swap Not Submitted', 'Your swap was not submitted, an error occured', 1000)
    //  }
    //})
  },
  ROHumanResourceSelected: function (ROHumanResource) {
    ViewModel.HumanResourceSwap().SwapWithHumanResourceID(ROHumanResource.HumanResourceID());
    ViewModel.HumanResourceSwap().SwapWithHumanResource(ROHumanResource.HumanResource());
  },
  FetchUserProfileShifts: function () {

  },

  /*-----------Shifts--------------*/
  fetchShifts: function () {
    ViewModel.UserProfileShiftListCriteria().IsProcessing(true)
    var sd = new Date(ViewModel.UserProfileShiftListCriteria().StartDate()).format('dd MMM yyyy')
    var ed = new Date(ViewModel.UserProfileShiftListCriteria().EndDate()).format('dd MMM yyyy')
    Singular.GetDataStateless("OBLib.Users.UserProfileShiftList", {
      HumanResourceID: ViewModel.UserProfileShiftListCriteria().HumanResourceID(),
      StartDate: sd,
      EndDate: ed
    },
    function (response) {
      if (response.Success) {
        ViewModel.UserProfileShiftList.Set(response.Data)
      }
      else {
        OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the shifts', response.ErrorText, null)
      }
      ViewModel.UserProfileShiftListCriteria().IsProcessing(false)
    })
  },
  saveShifts: function () {
    ViewModel.UserProfileShiftListCriteria().IsProcessing(true)
    var data = KOFormatterFull.Serialise(ViewModel.UserProfileShiftList())
    ViewModel.CallServerMethod("SaveUserProfileShiftList", {
      shiftList: data
    },
    function (response) {
      if (response.Success) {
        ViewModel.UserProfileShiftList.Set(response.Data)
        OBMisc.Notifications.GritterSuccess("Shifts Saved", "", 250)
      }
      else {
        OBMisc.Notifications.GritterError("Error Saving Shifts", response.ErrorText, 1000)
      }
      ViewModel.UserProfileShiftListCriteria().IsProcessing(false)
    })
  },
  currentUserProfileShift: function () {
    if (ViewModel.CurrentUserProfileShiftGuiD() && ViewModel.CurrentUserProfileShiftGuiD().length > 0) {
      return ViewModel.UserProfileShiftList().Find('Guid', ViewModel.CurrentUserProfileShiftGuiD())
    }
    return null
  },
  queryShift: function (shift) {
    ViewModel.CurrentUserProfileShiftGuiD(shift.Guid())
    $("#QueryShift").off('shown.bs.modal')
    $("#QueryShift").off('hidden.bs.modal')
    $("#QueryShift").on('shown.bs.modal', function () {
      shift.IsDisputing(true)
      Singular.Validation.CheckRules(shift)
    })
    $("#QueryShift").on('hidden.bs.modal', function () {
      shift.IsDisputing(false)
      Singular.Validation.CheckRules(shift)
    })
    $("#QueryShift").modal()
  },
  queryCss: function (shift) {
    if (!shift.StaffDisputeReason() || shift.StaffDisputeReason().length == 0) {
      return "btn btn-xs btn-default"
    }
    else {
      return "btn btn-xs btn-warning"
    }
  },
  queryText: function (shift) {
    if (!shift.StaffDisputeReason() || shift.StaffDisputeReason().length == 0) {
      return "Query"
    }
    else {
      return "Queried"
    }
  },
  canQuery: function () {
    return true
  },
  doneQuerying: function () {
    $("#QueryShift").modal('hide')
  }
};

function CurrentCrewTimesheet() {
  if (ViewModel.CurrentCrewTimesheetGuid() != "") {
    return ViewModel.OBCityTimesheet().CrewTimesheetList().Find('Guid', ViewModel.CurrentCrewTimesheetGuid())
  }
};

Singular.OnPageLoad(function () {
  UserProfilePage.SetupCalendar();
  UserProfilePage.SelectTab("Info");
  UserProfilePage.AddTabEvents();
  ROBookingsControl = new ROUserBookingsControl({
    onGroupSelected: function (datasetGroup) {
      if (ViewModel.ROUserProfile().SwapsAllowed() && datasetGroup.Resource.CanSwapWithCurrentUser) {
        UserProfilePage.RequestSwap(datasetGroup)
      }
    }
  })
  ViewModel.ROHumanResourceSwapListManager().Refresh()
  //#region Overrides
  ROHumanResourcePagedListBO.Criteria.DisciplineIDSet = function (critInstance) {
    console.log(critInstance);
    ViewModel.ROHumanResourceListManager().Refresh();
  }
  ROHumanResourcePagedListBO.Criteria.KeywordUserProfileSet = function (critInstance) {
    console.log(critInstance);
    critInstance.Keyword(critInstance.KeywordUserProfile());
    ViewModel.ROHumanResourceListManager().Refresh();
  }
  //#endregion

});