﻿var ROAdHocBookings,
    DELAY = 350,
    fetchTimeout = null;

TimesheetManagementPage = {

  //Common
  PageLoad: function () {
    ViewModel.ROAdHocBookingListPagingManager().SingleSelect(true);
    ViewModel.ROAdHocBookingListPagingManager().MultiSelect(false);
  },
  SelectTimesheetMonth: function () {
    $("#SelectTimesheetMonth").modal();
    $('#SelectTimesheetMonth').on('shown.bs.modal', function (e) {
      TimesheetManagementPage.RefreshTimesheetMonths();
    });
  },
  HasTimesheetMonth: function () {
    return (ViewModel.CurrentROTimesheetMonth() ? true : false);
  },
  ROHRSelected: function (ROHumanResourcePaged) {
    var current = null,
        hr = null;
    if (ViewModel.AddToAdHocBooking()) {
      current = ViewModel.CurrentAdHocBooking().AdHocBookingHumanResourceList().Find('HumanResourceID', ROHumanResourcePaged.HumanResourceID());
      if (!current) {
        hr = new SelectedHRObject();
        hr.HumanResourceID(ROHumanResourcePaged.HumanResourceID());
        hr.HumanResource(ROHumanResourcePaged.HumanResource());
        hr.ResourceID(ROHumanResourcePaged.ResourceID());
        ViewModel.SelectedHumanResourceList.push(hr);
      }
    } else if (ViewModel.AddToAdHocCrewTimesheet()) {
      current = ViewModel.CurrentAdhocCrewTimesheetList()[0].AdhocCrewTimesheetHumanResourceList().Find('HumanResourceID', ROHumanResourcePaged.HumanResourceID());
      if (!current) {
        hr = new SelectedHRObject();
        hr.HumanResourceID(ROHumanResourcePaged.HumanResourceID());
        hr.HumanResource(ROHumanResourcePaged.HumanResource());
        hr.ResourceID(ROHumanResourcePaged.ResourceID());
        ViewModel.SelectedHumanResourceList.push(hr);
      }
    }
  },
  DelayedROHRRefreshList: function () {
    ViewModel.ROHRListManager().Refresh();
  },
  ShowHRSelect: function () {
    $('#SelectHRModal').on('shown.bs.modal', function (e) {
      ViewModel.ROHRListManager().Refresh();
      $('#SelectHRModal').css({ zIndex: '2000' });
    });
    $("#SelectHRModal").modal();
  },
  HideHRSelect: function () {
    $("#SelectHRModal").modal('hide');
  },
  TimesheetMonthSelected: function (ROTimesheetMonth) {
    Singular.SendCommand("FetchTimesheetMonthData", { TimesheetMonthID: ROTimesheetMonth.TimesheetMonthID() }, function (response) {
      TimesheetManagementPage.CloseTimesheetMonthsModal();
      TimesheetManagementPage.RefreshAdHocCrewTimesheetList();
      TimesheetManagementPage.RefreshROAdhocBookingList();
      TimesheetManagementPage.RefreshTimesheetSummary();
    });
  },
  CloseTimesheetMonthsModal: function () {
    $("#SelectTimesheetMonth").modal('hide');
  },
  RefreshTimesheetMonths: function () {
    ViewModel.ROTimesheetMonthPagedListManager().PageNo(1);
    ViewModel.ROTimesheetMonthPagedListManager().Refresh();
  },
  DelayedRefreshTimesheetMonths: function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      TimesheetManagementPage.RefreshTimesheetMonths();
    }, DELAY);
  },
  HideAllModals: function () {
    $("#AdHocCrewTimesheetsContainer").removeClass('go');
    $("#CrewTimesheetsContainer").removeClass('go');
  },

  //AdHoc CrewTimesheets
  CanClickAdHocCrewTimesheetsButton: function () {
    return this.HasTimesheetMonth();
  },
  ShowAdHocCrewTimesheets: function () {
    this.HideAllModals();
    $("#AdHocCrewTimesheetsContainer").addClass('go');
  },
  NewAdHocCrewTimesheet: function () {
    Singular.SendCommand("NewAdHocCrewTimesheet", {}, function (response) {
      TimesheetManagementPage.ShowAdHocCrewTimesheetModal();
    });
  },
  ShowAdHocCrewTimesheetModal: function () {
    $('#AdHocCrewTimesheet').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(ViewModel.CurrentAdhocCrewTimesheetList()[0]);
      ViewModel.AddToAdHocBooking(false);
      ViewModel.AddToAdHocCrewTimesheet(true);
    });
    $("#AdHocCrewTimesheet").modal();
  },
  HideAdHocCrewTimesheetModal: function () {
    $("#AdHocCrewTimesheet").modal('hide');
  },
  SaveAdhocCrewTimesheet: function () {
    Singular.SendCommand("SaveAdHocCrewTimesheets", {}, function (response) {
      TimesheetManagementPage.HideAdHocCrewTimesheetModal();
      ViewModel.ROAdhocCrewTimesheetListPagingManager().Refresh();
    });
  },
  EditAdHocCrewTimesheet: function (ROAdHocCrewTimesheet) {
    this.HideAdHocCrewTimesheetModal();
    Singular.SendCommand("EditAdHocCrewTimesheet", {
      AdHocCrewTimesheetID: ROAdHocCrewTimesheet.AdhocCrewTimesheetID()
    }, function (response) {
      TimesheetManagementPage.ShowAdHocCrewTimesheetModal();
    });
  },
  CanAddAttendees: function () {
    if (ViewModel.CurrentAdhocCrewTimesheetList().length > 0) {
      return ViewModel.CurrentAdhocCrewTimesheetList()[0].IsValid();
    }
    return false;
  },
  ShowAddAttendees: function () {
    this.ShowHRSelect();
  },
  DeleteAdhocCrewTimesheet: function () {
    Singular.SendCommand("DeleteAdHocCrewTimesheet", {}, function (response) {
      TimesheetManagementPage.HideAdHocCrewTimesheetModal();
      ViewModel.ROAdhocCrewTimesheetListPagingManager().Refresh();
    });
  },
  AddSelectedHRToAdHocCrewTimesheet: function () {
    var me = this;
    Singular.SendCommand("AddHRToAddHocCrewTimesheet", {}, function (response) {
      me.HideHRSelect();
    });
  },
  RefreshAdHocCrewTimesheetList: function () {
    ViewModel.ROAdhocCrewTimesheetListPagingManager().Refresh();
  },

  //Timesheets
  EditTimesheetSummary: function (ROOBCityTimesheetSummary) {
    var me = this;
    Singular.SendCommand("GetOBCityTimesheet", {
      HumanResourceID: ROOBCityTimesheetSummary.HumanResourceID()
    }, function (response) {
      me.ShowTimesheetModal();
    });
  },
  ShowTimesheetModal: function () {
    $('#SelectHRTimesheet').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(TimesheetManagementPage.CurrentCrewTimesheet());
    });
    $("#SelectHRTimesheet").modal();
  },
  HideTimesheetModal: function () {
    $("#SelectHRTimesheet").modal('hide');
  },
  GetPercUsedColour: function (ROOBCityTimesheetSummary) {
    var defaultStyle = "";
    if (Between(ROOBCityTimesheetSummary.PercUsed(), 80, 90)) {
      return "Yellow";
    }
    else if (Between(ROOBCityTimesheetSummary.PercUsed(), 90, 100)) {
      return "Orange";
    }
    else if (ROOBCityTimesheetSummary.PercUsed() > 100) {
      return "Red";
    }
    else if (Between(ROOBCityTimesheetSummary.PercUsed(), 80, 90)) {
      return "Green";
    }
  },
  CanClickTimesheetsButton: function () {
    return this.HasTimesheetMonth();
  },
  ShowTimesheets: function () {
    this.HideAllModals();
    $("#CrewTimesheetsContainer").addClass('go');
  },
  EditCrewTimesheet: function (CrewTimesheet) {
    ViewModel.CurrentCrewTimesheetGuid(CrewTimesheet.Guid());
    this.ShowCurrentCrewTimesheet();
  },
  CurrentCrewTimesheet: function () {
    if (ViewModel.CurrentCrewTimesheetGuid() != "") {
      return ViewModel.OBCityTimesheet().CrewTimesheetList().Find('Guid', ViewModel.CurrentCrewTimesheetGuid())
    }
  },
  ShowCurrentCrewTimesheet: function () {
    $('#EditCrewTimesheet').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(TimesheetManagementPage.CurrentCrewTimesheet());
    });
    $("#EditCrewTimesheet").modal();
  },
  SaveCurrentCrewTimesheet: function (options) {
    Singular.SendCommand("SaveCurrentCrewTimesheet", {}, function (response) {
      $("#EditCrewTimesheet").modal('hide');
      if (options.afterSave) { options.afterSave() }
    });
  },
  RefreshTimesheetSummary: function () {
    ViewModel.ROOBCityTimesheetSummaryListCriteria().TimesheetMonthID(ViewModel.CurrentROTimesheetMonth().TimesheetMonthID());
    ViewModel.ROOBCityTimesheetSummaryListPagingManager().Refresh();
  },
  DelayedRefreshTimesheetSummary: function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      TimesheetManagementPage.RefreshTimesheetSummary();
    }, DELAY);
  },
  PercUsedCss: function (ROOBCityTimesheetSummary) {

  },
  AddAdjustment: function (OBCityTimesheet) {
    this.ShowAdjustmentsModal();
  },
  ShowAdjustmentsModal: function () {
    $('#CrewTimesheetAdjustmentsModal').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(TimesheetManagementPage.CurrentCrewTimesheet());
    });
    $("#CrewTimesheetAdjustmentsModal").modal();
  },
  HideAdjustmentsModal: function () {
    $("#CrewTimesheetAdjustmentsModal").modal('hide');
  },
  SetupNewAdjustment: function () {
    ViewModel.NewAdjustment(new TimesheetAdjustmentObject());
    ViewModel.NewAdjustment().HumanResourceID(ViewModel.OBCityTimesheet().HumanResourceID());
    ViewModel.NewAdjustment().AdjustedTimesheetMonthID(ViewModel.CurrentROTimesheetMonth().TimesheetMonthID());
    Singular.Validation.CheckRules(ViewModel.NewAdjustment());
  },
  AddNewAdjustmetToList: function () {
    ViewModel.OBCityTimesheet().TimesheetAdjustmentList().push(ViewModel.NewAdjustment());
    this.SaveCurrentCrewTimesheet({
      afterSave: function () {
        Singular.Validation.CheckRules(ViewModel.OBCityTimesheet());
      }
    });
  },

  //AdHoc Bookings
  AddNewAdHocBooking: function () {
    Singular.SendCommand("NewAdHocBooking", {}, function (response) {
      TimesheetManagementPage.ShowAdHocBooking();
    });
  },
  RefreshROAdhocBookingList: function () {
    ViewModel.ROAdHocBookingListPagingManager().Refresh();
  },
  ROAdhocBookingSelected: function (ROAdhocBooking) {
    Singular.SendCommand("EditAdHocBooking", { AdhocBookingID: ROAdhocBooking.AdHocBookingID() }, function (response) {
      TimesheetManagementPage.ShowAdHocBooking();
    });
  },
  ShowAdHocBooking: function () {
    $('#CurrentAdHocBooking').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(ViewModel.CurrentAdHocBooking());
      ViewModel.AddToAdHocBooking(true);
      ViewModel.AddToAdHocCrewTimesheet(false);
    });
    $("#CurrentAdHocBooking").modal();
  },
  HideAdHocBooking: function () {
    $("#CurrentAdHocBooking").modal('hide');
  },
  DeleteAdHocBooking: function (AdHocBooking) {
    Singular.SendCommand("DeleteAdHocBooking", {}, function (response) {
      if (response.Success) {
        TimesheetManagementPage.HideAdHocBooking();
      }
    });
  },
  SaveAdHocBooking: function (AdHocBooking) {
    Singular.SendCommand("SaveAdHocBooking", {}, function (response) {
      if (response.Success) {
        TimesheetManagementPage.HideAdHocBooking();
      }
    });
  }

};

Singular.OnPageLoad(function () {
  TimesheetManagementPage.PageLoad();
});

function ShowAdHoc() {
  $("#SelectAdHoc").modal();
  $('#SelectAdHoc').on('shown.bs.modal', function (e) {
    // RefreshAdHoc();
  });
};

function FindMonthButtonText() {
  if (ViewModel.CurrentROTimesheetMonth()) {
    return ViewModel.CurrentROTimesheetMonth().MonthYear();
  } else {
    return "Select Month"
  }
};

function CloseAddAttendees() {
  $("#AssignHRToAdHocBooking").modal('hide')
};

function New(ROAdHocBooking) {
  Singular.SendCommand("EditAdHocBooking",
             {
               AdHocBookingID: ROAdHocBooking.AdHocBookingID()
             }, function (response) {
               Edit();
             });
}

function Edit() {
  $("#ROAdHocBookingListModal").dialog({
    title: "Select Ad Hoc Booking",
    closeText: "Close",
    height: 720,
    width: 1024,
    modal: true,
    resizeable: false,
    open: function (event, ui) {
    },
    beforeClose: function (event, ui) {
    }
  })
};

function AddToAdHocBooking() {
  Singular.SendCommand('AddHRToAddHocBooking', {},
  function (response) {
    CloseAddAttendees()
  });
};

function Between(x, min, max) {
  return x >= min && x <= max;
}

function NewAdHocHumanResource(parent, data) {
  var ni = data.AdHocBookingHumanResourceList.AddNew();
  ni.StartDateTime(data.StartDateTime());
  ni.EndDateTime(data.EndDateTime());
}

function SaveBooking() {
  Singular.SendCommand('SaveAdHocBooking', {}, function () { });
}

function IsAdHocBookingListValid() {
  var valid = true;
  ViewModel.AdHocBookingList().Iterate(function (ab, Index) {
    if (!ab.IsValid()) {
      valid = false;
    }
    else {
      ab.AdHocBookingHumanResourceList().Iterate(function (abhr, Index) {
        if (!abhr.IsValid()) {
          valid = false;
        }
      });
    }
  });
  return valid;
};