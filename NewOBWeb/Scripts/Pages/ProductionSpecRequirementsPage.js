﻿ProductionSpecRequirementsPage = {
  refresh: function () {
    ViewModel.ROProductionSpecRequirementPagedListManager().Refresh()
  },
  addNewSpec: function () {
    var me = this;
    ViewModel.ProductionSpecRequirement(new ProductionSpecRequirementObject())
    me.showSpecModal()
  },
  editSpec: function (roSpec) {
    var me = this;
    ProductionSpecRequirementBO.get(roSpec.ProductionSpecRequirementID(),
                                     function (response) {
                                       ViewModel.ProductionSpecRequirement.Set(response.Data)
                                       me.showSpecModal()
                                     },
                                     function (response) { })
  },
  saveSpec: function (spec) {
    var me = this;
    spec.IsProcessing(true)
    ProductionSpecRequirementBO.save(spec,
                                     function (response) {
                                       spec.IsProcessing(false)
                                       ViewModel.ProductionSpecRequirement.Set(response.Data)
                                       me.hideSpecModal()
                                     },
                                     function (response) {
                                       spec.IsProcessing(false)
                                     })
  },
  showSpecModal: function () {
    $("#CurrentProductionSpecRequirement").modal()
  },
  hideSpecModal: function () {
    $("#CurrentProductionSpecRequirement").modal('hide')
  },
  addPosition: function (spec) {
    var newPos = spec.ProductionSpecRequirementPositionList.AddNew()
  },
  deletePosition: function (posType) {
    var parent = posType.GetParent()
    parent.ProductionSpecRequirementPositionList.RemoveNoCheck(posType)
  },
  addEquipmentType: function (spec) {
    var newEqType = spec.ProductionSpecRequirementEquipmentTypeList.AddNew()
  },
  deleteEquipmentType: function (eqType) {
    var parent = eqType.GetParent()
    parent.ProductionSpecRequirementEquipmentTypeList.RemoveNoCheck(eqType)
  }
}

Singular.OnPageLoad(function () {
  ProductionSpecRequirementsPage.refresh()
  $("#CurrentProductionSpecRequirement").off('hidden.bs.modal')
  $("#CurrentProductionSpecRequirement").on('hidden.bs.modal', function () {
    ViewModel.ProductionSpecRequirement(null)
    ProductionSpecRequirementsPage.refresh()
  })
  $("#CurrentProductionSpecRequirement").off('shown.bs.modal')
  $("#CurrentProductionSpecRequirement").on('shown.bs.modal', function () {
    Singular.Validation.CheckRules(ViewModel.ProductionSpecRequirement())
  })
})
