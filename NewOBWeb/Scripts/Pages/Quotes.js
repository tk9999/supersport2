﻿
//ReadOnlyCombos--------------------------------------------------------------
var ROEventTypes,
    ROEventTypesCriteria,
    ROProductionVenues,
    RODebtors,
    RODisciplines,
    ROAllowedDisciplines,
    ROCompanyRateCards,
    ROSupplierRateCards,
    ROQuoteListManagerObj;

function CurrentHeader() {
  if (ViewModel.CurrentHeaderID()) {
    return ViewModel.CurrentQuote().QuoteHeadingList().Find('QuoteHeadingID', ViewModel.CurrentHeaderID());
  } else {
    return null;
  }
}

function HideModalHeaderDetail() {
  $("#CurrentHeaderDetail").dialog('destroy');
}

function CurrentQuoteHeadingDetail() {
  if (!CurrentHeader()) {
    return null;
  }
  if (ViewModel.CurrentHeaderDetailGuid() && ViewModel.CurrentHeaderDetailGuid().trim().length > 0) {
    return CurrentHeader().QuoteHeadingDetailList().Find('Guid', ViewModel.CurrentHeaderDetailGuid());
  } else {
    return null; 
  }
};
//Event Types
ROEventTypes = new ROEventTypeManager({
  beforeOpen: function () {
    ViewModel.ROEventTypePagedListManager().SingleSelect(true);
    ViewModel.ROEventTypePagedListManager().MultiSelect(false);
    ROEventTypes.SetOptions({
      AfterRowSelected: function (ROEventType) {
        if (ViewModel.CurrentQuote()) {
          ViewModel.CurrentQuote().EventTypeID(ROEventType.EventTypeID());
          ViewModel.CurrentQuote().ProductionTypeID(ROEventType.ProductionTypeID());
          ViewModel.CurrentQuote().ProductionTypeEventType(ROEventType.ProductionType() + " (" + ROEventType.EventType() + ")");
        }
          if (ViewModel.ROQuoteListCriteria()) {
            ViewModel.ROQuoteListCriteria().EventTypeID(ROEventType.EventTypeID());
            ViewModel.ROQuoteListCriteria().ProductionTypeID(ROEventType.ProductionTypeID());
           ViewModel.ROQuoteListCriteria().ProductionTypeEventType(ROEventType.ProductionType() + " (" + ROEventType.EventType() + ")");
          }
        ROEventTypes.HideModal();
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        if (ViewModel.CurrentQuote()) {
          ViewModel.CurrentQuote().EventTypeID(null);
          ViewModel.CurrentQuote().ProductionTypeID(null);
         ViewModel.CurrentQuote().ProductionTypeEventType("");
          ROEventTypes.HideModal();
        }
          if (ViewModel.ROQuoteListCriteria()) {
            ViewModel.ROQuoteListCriteria().EventTypeID(null);
            ViewModel.ROQuoteListCriteria().ProductionTypeEventType("");
            ViewModel.ROQuoteListCriteria().ProductionTypeID(null);
            ROEventTypes.HideModal();
          }
        }
    });
  }
});

//Venues--------------------------------------------
ROProductionVenues = new ROProductionVenueManager({
  beforeOpen: function () {
    ViewModel.ROProductionVenueListManager().SingleSelect(true);
    ViewModel.ROProductionVenueListManager().MultiSelect(false);
    ROProductionVenues.SetOptions({
      AfterRowSelected: function (ROProductionVenue) {
        ViewModel.CurrentQuote().VenueID(ROProductionVenue.ProductionVenueID());
        ViewModel.CurrentQuote().ProductionVenue(ROProductionVenue.ProductionVenue());
        ROProductionVenues.HideModal();
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.CurrentQuote().VenueID(null);
        ViewModel.CurrentQuote().ProductionVenue('');
        ROProductionVenues.HideModal();
      }
    });
  }
});
//Debtor-----------------
RODebtors = new RODebtorListManager({
  beforeOpen: function () {
    ViewModel.RODebtorListManager().SingleSelect(true);
    ViewModel.RODebtorListManager().MultiSelect(false);
    RODebtors.SetOptions({
      AfterRowSelected: function (RODebtor) {
        ViewModel.CurrentQuote().DebtorID(RODebtor.DebtorID());
        ViewModel.CurrentQuote().Debtor(RODebtor.Debtor());
        RODebtors.HideModal();
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.CurrentQuote().DebtorID(null);
        ViewModel.CurrentQuote().Debtor('');
        RODebtors.HideModal();
      }
    });
  }
});


//Allowed Disciplines---------------------------------
ROAllowedDisciplines = new ROAllowedDisciplineSelectManager({
  AfterRowSelected: function (ROAllowedDiscipline) {
    if (CurrentQuoteHeadingDetail()) {
      CurrentQuoteHeadingDetail().DisciplineID(ROAllowedDiscipline.DisciplineID());
      CurrentQuoteHeadingDetail().PositionID(ROAllowedDiscipline.PositionID());
      CurrentQuoteHeadingDetail().DisciplinePosition(ROAllowedDiscipline.Discipline() + " " + ROAllowedDiscipline.Position())
    }
    ROAllowedDisciplines.HideModal();
  },
  AfterRowDeSelected: function (DeSelectedItem) {

  },
  beforeOpen: function () {
    ViewModel.ROAllowedDisciplineSelectListPagingManager().SingleSelect(true);
    ViewModel.ROAllowedDisciplineSelectListPagingManager().MultiSelect(false);
  }
});

//Company Rate Card---------------------------------
ROCompanyRateCards = new ROCompanyRateCardManager({
  AfterRowSelected: function (ROCompanyRateCard) {
    if (CurrentQuoteHeadingDetail()) {
      CurrentQuoteHeadingDetail().CompanyRateCardID(ROCompanyRateCard.CompanyRateCardID());
      CurrentQuoteHeadingDetail().Company(ROCompanyRateCard.Company());
      CurrentQuoteHeadingDetail().Rate(ROCompanyRateCard.Rate())
      CurrentQuoteHeadingDetail().CompanyRateCard(ROCompanyRateCard.Company() + " " + ROCompanyRateCard.Rate())
    }
    ROCompanyRateCards.HideModal();
  },
  AfterRowDeSelected: function (DeSelectedItem) {

  },
  beforeOpen: function () {
    ViewModel.ROCompanyRateCardListPagingManager().SingleSelect(true);
    ViewModel.ROCompanyRateCardListPagingManager().MultiSelect(false);
  }
});

//Supplier Rate Card---------------------------------
ROSupplierRateCards = new ROSupplierRateCardManager({
  AfterRowSelected: function (ROSupplierRateCard) {
    if (CurrentQuoteHeadingDetail()) {
      CurrentQuoteHeadingDetail().SupplierRateCardID(ROSupplierRateCard.SupplierRateCardID());
      CurrentQuoteHeadingDetail().Supplier(ROSupplierRateCard.Supplier());
      CurrentQuoteHeadingDetail().Amount(ROSupplierRateCard.Amount())
      CurrentQuoteHeadingDetail().SupplierRateCard(ROSupplierRateCard.Supplier() + " " + ROSupplierRateCard.Amount())
    }
    ROSupplierRateCards.HideModal();
  },
  AfterRowDeSelected: function (DeSelectedItem) {

  },
  beforeOpen: function () {
    ViewModel.ROSupplierRateCardListPagingManager().SingleSelect(true);
    ViewModel.ROSupplierRateCardListPagingManager().MultiSelect(false);
  }
});

ROQuoteListManagerObj = new ROQuoteListManager({
  AfterRowSelected: function (ROQuote) {
    QuoteSelected(ROQuote)
    ROQuoteListManagerObj.HideModal();
  },
  AfterRowDeSelected: function (DeSelectedItem) {
    // RemoveFromNotificationList(DeSelectedItem.ID())
  },
  beforeOpen: function () {
    ViewModel.ROQuoteListPagingManager().SingleSelect(true);
    ViewModel.ROQuoteListPagingManager().MultiSelect(false);
  }
});

function CanShowHeadingDetails() {

  if (!ViewModel.FoundQuote()) {
    return false;
  }
  return true;
}

function FindQuote() {
  ROQuoteListManagerObj.ShowModal()
};

function FindQuotes() {
  //ViewModel.ROQuoteListPagingInfo().Refresh();
  ViewModel.CurrentQuoteVisible(false);
  Search(1);
};

function QuoteSelected(ROQuote) {
  ViewModel.CurrentHeaderID(null);
  ViewModel.CurrentHeaderDetailGuid(null);
  Singular.SendCommand('FetchQuote', { QuoteID: ROQuote.QuoteID() }, function (response) {
    ViewModel.FindQuoteVisible(false);
    Quotes.SetTotalQuoteAmount(ViewModel.CurrentQuote());
  });

};

function QuoteLoaded() {
  return (ViewModel.CurrentQuote() ? true : false);
}

function InternalQuoteClick(Quote) {
  Quote.InternalInd(!Quote.InternalInd());
}

function InternalQuoteCss(Quote) {
  if (Quote.InternalInd()) {
    return "btn btn-xs btn-primary"
  } else {
    return "btn btn-xs btn-danger"
  }
}

function InternalQuoteHtml(Quote) {
  if (Quote.InternalInd()) {
    return "<i class='fa fa-check-square-o'></i>" + " Internal";
  } else {
    return "<i class='fa fa-minus-square-o'></i>" + " External";
  }
}

function AddQuoteHeading() {
  var qh = ViewModel.CurrentQuote().QuoteHeadingList.AddNew();
}

function RemoveQuoteHeading(QuoteHeading) {
  ViewModel.CurrentQuote().QuoteHeadingList.Remove(QuoteHeading);
}

function CanViewHeaderDetails(Header) {
  if (Header) {
    return (Header.QuoteHeadingID() ? true : false);
  }
  return false;
};

function ShowHeaderDetails(Header) {
  SetHeaderDetails(Header);
  ViewModel.CurrentHeaderDetailsVisible(true);
  //$("#QuoteHeader").modal();
};

function SetHeaderDetails(Header) {
  ViewModel.FoundQuote(true);
  ViewModel.CurrentHeaderID(Header.QuoteHeadingID());
}

function CurrentHeader() {
  if (ViewModel.CurrentHeaderID()) {
    return ViewModel.CurrentQuote().QuoteHeadingList().Find('QuoteHeadingID', ViewModel.CurrentHeaderID());
  } else {
    ViewModel.CurrentHeaderDetailGuid(null);
    return null;
  }
}

function NewQuote() {
  ViewModel.CurrentHeaderID(null);
  ViewModel.CurrentHeaderDetailGuid(null);
  Singular.SendCommand("NewQuote", {}, function (response) {
  })
}

function AddQuoteHeadingDetail() {
  var qhd = CurrentHeader().QuoteHeadingDetailList.AddNew();
}

function RemoveQuoteHeadingDetail(QuoteHeadingDetail) {
  CurrentHeader().QuoteHeadingDetailList.Remove(QuoteHeadingDetail);
}

function SetTotalQuoteAmount(Quote) {
  ViewModel.CurrentQuote().QuoteAmount(9999999);
}

function CanOverrideQuoteAmount(Quote) {
  return true;
}

function ShowQuoteOverride(Quote) {
  $("#QuoteAmountOverride").dialog({
    title: "Quote Amount Override",
    closeText: "Close",
    height: 350,
    width: 800,
    modal: true,
    resizeable: true
  });
}

function CommitQuoteOverride(Quote) {
  $("#QuoteAmountOverride").dialog('destroy');
}

function CancelQuoteOverride(Quote) {
  Quote.QuoteOverrideAmount(null);
  Quote.QuoteOverrideBy(null);
  Quote.QuoteOverrideByName("");
  Quote.QuoteOverrideDateTime(null);
  Quote.QuoteOverrideReason("");
  $("#QuoteAmountOverride").dialog('destroy');
}

function ShowQuoteHeaderOverride(Header) {
  SetHeaderDetails(Header);
  $("#CurrentHeaderOverride").dialog({
    title: "Header Override",
    closeText: "Close",
    height: 350,
    width: 800,
    modal: true,
    resizeable: true
  });
}

function CommitQuoteHeaderOverride(Header) {
  $("#CurrentHeaderOverride").dialog('destroy');
}

function CancelQuoteHeaderOverride(Header) {
  Header.AmountOverride(null);
  Header.AmountOverrideBy(null);
  Header.AmountOverrideByName("");
  Header.AmountOverrideDateTime(null);
  Header.AmountOverrideReason("");
  $("#CurrentHeaderOverride").dialog('destroy');
}

function CanOverrideHeaderAmount(Header) {
  return true;
}

function CostDescriptionClick(HeaderDetail) {
  ViewModel.CurrentHeaderDetailGuid(HeaderDetail.Guid());
  $("#CurrentHeaderDetail").dialog({
    title: "Header Detail",
    closeText: "Close",
    height: 720,
    width: 1024,
    modal: true,
    resizeable: true
  });
}

function CurrentQuoteHeadingDetail() {
  if (ViewModel.CurrentHeaderDetailGuid() && ViewModel.CurrentHeaderDetailGuid().trim().length > 0) {
    if (CurrentHeader().QuoteHeadingDetailList()) {
      return CurrentHeader().QuoteHeadingDetailList().Find('Guid', ViewModel.CurrentHeaderDetailGuid());
    }
  }
  return null;

};

function CostDescriptionCss(HeaderDetail) {
  return "btn btn-xs btn-default"
}

function CostDescriptionHtml(HeaderDetail) {

  var ButtonText = '';
  var Discipline = null;
  var Position = null;
  var Area = null;
  var Supplier = null;


  if (HeaderDetail.DisciplineID()) {
    Discipline = ClientData.RODisciplineList.Find('DisciplineID', HeaderDetail.DisciplineID());
  }
  if (HeaderDetail.PositionID()) {
    Position = ClientData.ROPositionList.Find('PositionID', HeaderDetail.PositionID());
  }
  if (HeaderDetail.ProductionAreaID()) {
    Area = ClientData.ROProductionAreaList.Find('ProductionAreaID', HeaderDetail.ProductionAreaID());
  }
  if (HeaderDetail.SupplierID()) {
    Supplier = ClientData.ROSupplierList.Find('SupplierID', HeaderDetail.SupplierID());
  }

  ButtonText += HeaderDetail.Quantity().toString() + " x "

  if (Discipline) {
    ButtonText += Discipline.Discipline;
  };

  if (Position) {
    ButtonText += (ButtonText.trim().length == 0 ? '' : ' - ') + Position.Position;
  };

  if (Area) {
    ButtonText += (ButtonText.trim().length == 0 ? '' : ' - ') + Area.ProductionArea;
  };

  if (Supplier) {
    ButtonText += (ButtonText.trim().length == 0 ? '' : ' - ') + ' (' + Supplier.Supplier + ')';
  };

  return ButtonText;

}

function CanviewReports(CurrentQuote) {
  if (CurrentQuote) {
    if (!CurrentQuote.IsNew()) {
      return true;
    }
  }
  return false
}

function FilterCompanyRateCards(List, Item) {
  var SupplierID = Item.SupplierID();
  if (SupplierID) {
    SupplierID = null; //for garbage collection
    return [];
  } else {
    var DisciplineID = Item.DisciplineID();
    var PositionID = Item.PositionID();
    var ProductionAreaID = Item.ProductionAreaID();
    var ProductionTypeID = Item.GetParent().GetParent().ProductionTypeID();
    var EventTypeID = Item.GetParent().GetParent().EventTypeID();
    var ApplicableRateCards = [];
    var r = {};
    ApplicableRateCards = [];
    List.Iterate(function (RateCard, RCIndex) {
      var Applicable = false;
      if ((RateCard.ProductionAreaID == null || RateCard.ProductionAreaID == ProductionAreaID)
           && (RateCard.ProductionTypeID == null || RateCard.ProductionTypeID == ProductionTypeID)
           && (RateCard.EventTypeID == null || RateCard.EventTypeID == EventTypeID)
           && (RateCard.DisciplineID == null || RateCard.DisciplineID == DisciplineID)
           && (RateCard.PositionID == null || RateCard.PositionID == PositionID)) {
        Applicable = true;
      }
      if (Applicable) {
        r = {
          CompanyRateCardID: RateCard.CompanyRateCardID,
          Discipline: RateCard.Discipline,
          Position: RateCard.Position,
          HumanResource: RateCard.HumanResource,
          ProductionType: RateCard.ProductionType,
          EventType: RateCard.EventType,
          Area: RateCard.Area,
          RateType: RateCard.RateType,
          Rate: RateCard.Rate
        }
        ApplicableRateCards.push(r);
        r = null;
      }
    });
    return ApplicableRateCards;
  }
}

function FilterSupplierRateCards(List, Item) {
  var SupplierID = Item.SupplierID();
  if (SupplierID) {
    SupplierID = null; //for garbage collection
    var ProductionAreaID = Item.ProductionAreaID();
    var ProductionTypeID = Item.GetParent().GetParent().ProductionTypeID();
    var EventTypeID = Item.GetParent().GetParent().EventTypeID();
    var CostTypeID = Item.CostTypeID();
    var ApplicableRateCards = [];
    var r = {};
    ApplicableRateCards = [];
    List.Iterate(function (RateCard, RCIndex) {
      var Applicable = false;
      if ((RateCard.ProductionAreaID == null || RateCard.ProductionAreaID == ProductionAreaID)
           && (RateCard.ProductionTypeID == null || RateCard.ProductionTypeID == ProductionTypeID)
           && (RateCard.EventTypeID == null || RateCard.EventTypeID == EventTypeID)
           && (RateCard.CostTypeID == null || RateCard.CostTypeID == CostTypeID)) {
        Applicable = true;
      }
      if (Applicable) {
        r = {
          SupplierRateCardID: RateCard.SupplierRateCardID,
          ProductionType: RateCard.ProductionType,
          EventType: RateCard.EventType,
          Area: RateCard.Area,
          CostType: RateCard.CostType,
          Quantity: RateCard.Quantity,
          Amount: RateCard.Amount
        }
        ApplicableRateCards.push(r);
        r = null;
      }
    });
    return ApplicableRateCards;
  } else {
    return [];
  }
}

function Search(PageNo) {
  Singular.SendCommand("Search", { PageNo: PageNo }, function (response) {

  });
}

function FirstQuotePage() {
  Search(1);
}

function PrevQuotePage() {
  var cn = ViewModel.ROQuoteListCriteria().PageNo();
  Search(cn - 1);
}

function NextQuotePage() {
  var cn = ViewModel.ROQuoteListCriteria().PageNo();
  Search(cn + 1);
}

function LastQuotePage() {
  Search(ViewModel.TotalPages());
}

function GetTotalQuotePageDescription() {
  return " of " + ViewModel.TotalPages().toString();
}

function ClearProductionTypeEventType() {
  if (ViewModel.ROQuoteListCriteria()) {
    ViewModel.ROQuoteListCriteria().EventTypeID(null);
    ViewModel.ROQuoteListCriteria().ProductionTypeEventType("");
    ViewModel.ROQuoteListCriteria().ProductionTypeID(null);
  }
}