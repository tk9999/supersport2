﻿ContentOBVM = {
  OnRequirementsTabSelected: function () {
    if (!ViewModel.RequiredEquipmentManager().HasDoneInitialLoad()) {
      ViewModel.RequiredEquipmentManager().Refresh();
      ViewModel.RequiredEquipmentManager().HasDoneInitialLoad(true);
    }
    if (!ViewModel.RequiredPositionsManager().HasDoneInitialLoad()) {
      ViewModel.RequiredPositionsManager().Refresh();
      ViewModel.RequiredPositionsManager().HasDoneInitialLoad(true);
    }
  },
  OnOutsourceServicesTabSelected: function () {
    if (!ViewModel.OutsourceServicesManager().HasDoneInitialLoad()) {
      ViewModel.OutsourceServicesManager().Refresh();
      ViewModel.OutsourceServicesManager().HasDoneInitialLoad(true);
    }
  },
  OnTimelinesTabSelected: function () {
    if (!ViewModel.TimelineManager().HasDoneInitialLoad()) {
      ViewModel.TimelineManager().Refresh();
      ViewModel.TimelineManager().HasDoneInitialLoad(true);
    }
  },
  OnCrewTabSelected: function () {
    if (!ViewModel.ROProductionHumanResourceManager().HasDoneInitialLoad()) {
      ViewModel.ROProductionHumanResourceManager().Refresh();
      ViewModel.ROProductionHumanResourceManager().HasDoneInitialLoad(true);
    }
  }
}