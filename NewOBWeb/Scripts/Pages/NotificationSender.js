﻿
var ROScheduleSenderHumanResources;

function AddResourceToNotificationList(ROScheduleSenderHumanResource) {

  var newSelectedResource = ViewModel.NewROScheduleSenderHumanResourceList.AddNew();
  newSelectedResource.HumanResourceID(ROScheduleSenderHumanResource.HumanResourceID());
  newSelectedResource.Firstname(ROScheduleSenderHumanResource.Firstname());
  newSelectedResource.PreferredName(ROScheduleSenderHumanResource.PreferredName());
  newSelectedResource.Surname(ROScheduleSenderHumanResource.Surname());
  newSelectedResource.EmailAddress(ROScheduleSenderHumanResource.EmailAddress());
  newSelectedResource.CellPhoneNumber(ROScheduleSenderHumanResource.CellPhoneNumber());
  newSelectedResource.IDNo(ROScheduleSenderHumanResource.IDNo());
  newSelectedResource.EmployeeCode(ROScheduleSenderHumanResource.EmployeeCode());
  newSelectedResource.StartDate(ROScheduleSenderHumanResource.StartDate());
  newSelectedResource.EndDate(ROScheduleSenderHumanResource.EndDate());
};

function RemoveFromNotificationList(HumanResourceID) {
  var Found = ViewModel.NewROScheduleSenderHumanResourceList().Find("HumanResourceID", HumanResourceID);
  ViewModel.NewROScheduleSenderHumanResourceList.Remove(Found);
}


ROScheduleSenderHumanResources = new ROScheduleSenderHumanResourceManager({
  AfterRowSelected: function (ROScheduleSenderHumanResource) {
    AddResourceToNotificationList(ROScheduleSenderHumanResource)
  },
  AfterRowDeSelected: function (DeSelectedItem) {
    RemoveFromNotificationList(DeSelectedItem.ID())
  },
  beforeOpen: function () {
  }
});

function SendEmail(ScheduleSenderHumanResource) {

  var Email = ScheduleSenderHumanResource.EmailList.AddNew();
  Email.ToEmailAddress(ScheduleSenderHumanResource.EmailAddress());
  Email.FromEmailAddress("");
  Email.FriendlyFrom("");
  Email.Subject("Crew Schedule from " + ScheduleSenderHumanResource.StartDate() + " to  " + ScheduleSenderHumanResource.EndDate());
  Email.Body("");
}

function SendSmS(ScheduleSenderHumanResource) {
  var SmS = ScheduleSenderHumanResource.SmSList.AddNew();
  SmS.Message("The message goes here");
}
function SelectHR() {
  $("#SelectHR").dialog({
    title: "Select Recipients",
    closeText: "Close",
    height: 720,
    width: 1024,
    modal: true,
    resizeable: true
  });
}

Singular.OnPageLoad(function () {

  ViewModel.ROScheduleSenderHumanResourceListPagingManager().SingleSelect(false);
  ViewModel.ROScheduleSenderHumanResourceListPagingManager().MultiSelect(true);
  ROScheduleSenderHumanResources.Init();
});