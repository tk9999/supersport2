﻿var ROCreditorInvoiceS;
var tempDatepicker = null;

  ROCreditorInvoiceS = new ROCreditorInvoiceManager({
  AfterRowSelected: function (ROEventType) {

  },
  AfterRowDeSelected: function (DeSelectedItem) {

    //ROEventTypes.HideModal();
  },
  beforeOpen: function () {
  }
});


function AddCreditorInvoice() {
  var inv = ViewModel.SelectedPaymentRun().CreditorInvoiceList.AddNew();
  inv.PageNumber(ViewModel.PageNumber());
};

function RemoveCreditorInvoice(CreditorInvoice) {
  ViewModel.SelectedPaymentRun().CreditorInvoiceList.Remove(CreditorInvoice);
};

function AddCreditorInvoiceDetail(CreditorInvoice) {
  CreditorInvoice.CreditorInvoiceDetailList.AddNew();
};

function RemoveCreditorInvoiceDetail(CreditorInvoiceDetail) {
  CreditorInvoiceDetail.GetParent().CreditorInvoiceDetailList.Remove(CreditorInvoiceDetail);
};

function BulkAdd() {
  //  ViewModel.CopyMonthSelected(false);
  //  ViewModel.BulkAddSelected(true);
};

function DeclineBulkAdd() {
  //  ViewModel.BulkAddSelected(false);
  //  ViewModel.CopyMonthSelected(false);
};

function CommitBulkAdd() {
  //  Singular.ShowLoadingBar();
  //  for (i = 0; i < ViewModel.Quantity(); i++) {
  //    var NewInvoice = ViewModel.CreditorInvoiceList.AddNew();
  //    NewInvoice.InvoiceDate(ViewModel.InvoiceDate());
  //    NewInvoice.AccountID(ViewModel.AccountID());
  //    NewInvoice.CostCentreID(ViewModel.CostCentreID());
  //    NewInvoice.CreditorID(ViewModel.CreditorID());
  //    NewInvoice.HumanResourceID(ViewModel.HumanResourceID());
  //    NewInvoice.CompletedInd(false);
  //  };
  //  ViewModel.BulkAddSelected(false);
  //  ViewModel.CopyMonthSelected(false);
  //  Singular.HideLoadingBar();
};

function BackToPaymentRun() {
  ViewModel.PaymentRunVisible(true);
  ViewModel.SelectPaymentRunVisible(false);
  ViewModel.BulkAddVisible(false);
  ViewModel.CopyPaymentRunVisible(false);
};

function CompleteAll() {
  for (j = 0; j < ViewModel.SelectedPaymentRun().CreditorInvoiceList().length; j++) {
    ViewModel.SelectedPaymentRun().CreditorInvoiceList()[j].CompletedInd(true);
  }
};

function UnCompleteAll() {
  for (j = 0; j < ViewModel.SelectedPaymentRun().CreditorInvoiceList().length; j++) {
    ViewModel.SelectedPaymentRun().CreditorInvoiceList()[j].CompletedInd(false);
  }
};

function IsVisible(CreditorInvoice) {
  if (ViewModel.CurrentPageNumber() == CreditorInvoice.PageNumber()) {
    return true;
  } else {
    return false;
  };
};

function SelectPaymentRun() {
  Singular.SendCommand('GetROPaymentRunList', { SendModel: false }, function () {
    ViewModel.PaymentRunVisible(false);
    ViewModel.SelectPaymentRunVisible(true);
    ViewModel.BulkAddVisible(false);
    ViewModel.CopyPaymentRunVisible(false);
  });
  //$("#SelectPaymentRun").modal();
};

function PaymentRunSelected(ROPaymentRun) {
  var newURL = Singular.CurrentPath + "?PR=" + ROPaymentRun.PaymentRunID().toString();
  window.location.href = newURL;
  //Singular.SendCommand('GetPaymentRun', { SendModel: false, PaymentRunID: ROPaymentRun.PaymentRunID() }, function () {
  //  SetCreditorInvoiceTotals();
  //});
  //GetPaymentRun(ROPaymentRun);
};

function InvoiceCompleted(CreditorInvoice) {
  if (CreditorInvoice.CompletedInd() == false) { CreditorInvoice.CompletedInd(true) }
  else if (CreditorInvoice.CompletedInd() == true) { CreditorInvoice.CompletedInd(false) }
};

function InvoiceCompletedCss(CreditorInvoice) {
  var defaultStyle = "";
  if (CreditorInvoice.CompletedInd()) { defaultStyle = "btn btn-xs btn-success" }
  else { defaultStyle = "btn btn-xs btn-default" }
  return defaultStyle;
};

function InvoiceCompletedHTML(CreditorInvoice) {
  var html = "";
  if (CreditorInvoice.CompletedInd()) {
    html = "<span class='glyphicon glyphicon-check'></span>" + " Yes"
  } else {
    html = "<span class='glyphicon glyphicon-minus-sign'></span>" + " No"
  }
  return html;
};

function AddDocument(CreditorInvoice) {
  CreditorInvoice.Attachment(new TemporaryDocumentObject());
};

function RemoveDocument(CreditorInvoice) {
  CreditorInvoice.Attachment(null);
};

function InvoicesVisible() {
  if (ViewModel.SelectedPaymentRun() && !ViewModel.SelectedPaymentRun().IsNew()) {
    return true;
  } else {
    return false;
  }
};

function ROPaymentRunStatusCss(ROPaymentRun) {
  switch (ROPaymentRun.PaymentRunStatusID()) {
    case 1: //open
      return 'btn btn-xs btn-primary'
      break;
    case 2: //closed
      return 'btn btn-xs btn-warning'
      break;
    case 3: //completed
      return 'btn btn-xs btn-success'
      break;
    default: //default
      return 'btn btn-xs btn-default'
  }
};

function ROPaymentRunHtml(ROPaymentRun) {
  switch (ROPaymentRun.PaymentRunStatusID()) {
    case 1: //open
      return "<span class='glyphicon glyphicon-eye-open'></span> " + " " + ROPaymentRun.Status();
      break;
    case 2: //closed
      return "<span class='glyphicon glyphicon-eye-close'></span> " + " " + ROPaymentRun.Status();
      break;
    case 3: //completed
      return "<span class='glyphicon glyphicon-ok'></span> " + " " + ROPaymentRun.Status();
      break;
    default: //default
      return "<span class='glyphicon glyphicon-minus'></span> " + " " + ROPaymentRun.Status();
  }
};

function CanCopyPaymentRun() {
  if (ViewModel.SelectedPaymentRun()) {
    if (!ViewModel.SelectedPaymentRun().IsNew() && !ViewModel.SelectedPaymentRun().IsDirty()) {
      return true;
    }
  }
  return false;
};

function SetupCopyPaymentRun() {
  Singular.SendCommand('GetROPaymentRunList', { SendModel: false }, function () {
    ViewModel.PaymentRunVisible(false);
    ViewModel.SelectPaymentRunVisible(false);
    ViewModel.BulkAddVisible(false);
    ViewModel.CopyPaymentRunVisible(true);
  });
};

function CopyPaymentRun(ROPaymentRun) {
  Singular.SendCommand('CopyPaymentRun', { PaymentRunIDToCopy: ROPaymentRun.PaymentRunID() }, function () {

  });
};

function OnCurrentPage(CreditorInvoice) {
  if (CreditorInvoice.PageNumber() == ViewModel.PageNumber()) {
    return true;
  }
  return false;
};

function PreviousPage() {
  ViewModel.SwitchingPage(true);
  Singular.ShowLoadingBar();
  if (ViewModel.PageNumber() == 1) {
    ViewModel.PageNumber(ViewModel.PageCount());
  } else {
    ViewModel.PageNumber(ViewModel.PageNumber() - 1);
  }
  ViewModel.SwitchingPage(false);
  Singular.HideLoadingBar();
}

function NextPage() {
  ViewModel.SwitchingPage(true);
  Singular.ShowLoadingBar();
  if (ViewModel.PageNumber() == ViewModel.PageCount()) {
    ViewModel.PageNumber(1);
  } else {
    ViewModel.PageNumber(ViewModel.PageNumber() + 1);
  }
  ViewModel.SwitchingPage(false);
  Singular.HideLoadingBar();
}

function GetAllowedStatusList(List, Item) {

  var Allowed = [];
  var Returned = [];

  //add the current one if there is one selected
  if (Item.PaymentRunStatusID()) {
    Allowed.push({ PaymentRunStatusID: Item.PaymentRunStatusID(), PaymentRunStatus: List.Find('PaymentRunStatusID', Item.PaymentRunStatusID()).PaymentRunStatus, OrderNo: List.Find('PaymentRunStatusID', Item.PaymentRunStatusID()).OrderNo });
    var CurrentItem = List.Find('PaymentRunStatusID', ViewModel.SelectedPaymentRun().PaymentRunStatusID())
    //loop through the list
    for (var i = 0; i < List.length; i++) {
      var curr = List[i];
      if (curr.OrderNo < CurrentItem.OrderNo && ViewModel.CanMoveStatusBackward()) {
        Allowed.push({ PaymentRunStatusID: curr.PaymentRunStatusID, PaymentRunStatus: List.Find('PaymentRunStatusID', curr.PaymentRunStatusID).PaymentRunStatus, OrderNo: List.Find('PaymentRunStatusID', curr.PaymentRunStatusID).OrderNo });
      } else if (curr.OrderNo > CurrentItem.OrderNo && ViewModel.CanMoveStatusForward()) {
        Allowed.push({ PaymentRunStatusID: curr.PaymentRunStatusID, PaymentRunStatus: List.Find('PaymentRunStatusID', curr.PaymentRunStatusID).PaymentRunStatus, OrderNo: List.Find('PaymentRunStatusID', curr.PaymentRunStatusID).OrderNo });
      }
    }
  } else {
    Allowed = List;
  };
  var Sorted = [];
  Sorted = Allowed.sort(function (a, b) { return a.OrderNo - b.OrderNo })
  return Sorted;
};

function SetCreditorInvoiceTotals() {
  if (ViewModel.SelectedPaymentRun()) {
    for (j = 0; j < ViewModel.SelectedPaymentRun().CreditorInvoiceList().length; j++) {
      var CreditorInvoice = ViewModel.SelectedPaymentRun().CreditorInvoiceList()[j];
      var TotalAmtExVat = 0;
      var TotalVat = 0;
      var Total = 0
      for (i = 0; i < CreditorInvoice.CreditorInvoiceDetailList().length; i++) {
        var CurrentDetail = CreditorInvoice.CreditorInvoiceDetailList()[i];
        TotalAmtExVat += CurrentDetail.AmountExclVAT();
        TotalVat += CurrentDetail.VATAmount();
        Total += CurrentDetail.TotalAmount();
      };
      CreditorInvoice.AmountExclVAT(TotalAmtExVat);
      CreditorInvoice.VATAmount(TotalVat);
      CreditorInvoice.TotalAmount(Total);
    }
  };
};

function ShowHRInvoiceCriteria() {
  //ViewModel.HRInvoiceCriteria(new InvoiceReports_CreditorInvoicesCriteriaObject());
  $("#HRInvoices").modal();
};

function PaymentRunComplete() {
  if (ViewModel.SelectedPaymentRun()) {
    if (ViewModel.SelectedPaymentRun().PaymentRunStatusID() == 3) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
}

function FilterByHumanResource() {
  if (ViewModel.SelectedPaymentRun.IsDirty) {
    Singular.ShowMessageQuestion(" Human Resource Filter", "Are you sure you want to filter the list? There are unsaved changes",
    //Yes
    function () {
      Singular.ShowLoadingBar();
      Singular.SendCommand('FilterByHumanResource', {
      },
      function (d) {
        Singular.HideLoadingBar();
        window.location.href = d;
      });
    },
    //No
    function () {
    });
  }
  else {
    Singular.SendCommand('FilterByHumanResource', {}, function () { });
  }
}

function InvoicesByHumanResource(CreditorInvoice) {

  ROCreditorInvoiceS.Criteria().HumanResourceID(CreditorInvoice.HumanResourceID());
  ROCreditorInvoiceS.RefreshList();
  ROCreditorInvoiceS.ShowModal();
}

function ShowDateSelect(busObj, btn) {
  ViewModel.CurrentInvoiceGuid(busObj.Guid());
  busObj.SelectDatesVisible(true);
  if (!busObj.Expanded()) { busObj.Expanded(true) }
  var td = $(btn).parents('td');
  tempDatepicker = $(td).find('div.hasDatepicker');
  if (tempDatepicker.length == 0) {
    tempDatepicker = '<div></div>'
  }
  tempDatepicker = $(tempDatepicker).datepicker({
    showOptions: { direction: "up" },
    onSelect: function (datetext, inst) {
      console.log(datetext, inst);
      var inv = ViewModel.SelectedPaymentRun().CreditorInvoiceList().Find("Guid", busObj.Guid());
      var CreditorInvoiceDetailList = inv.CreditorInvoiceDetailList();
      var Found = DateExists(CreditorInvoiceDetailList, new Date(datetext));
      if (Found == false) {
        var newDet = inv.CreditorInvoiceDetailList.AddNew();
        newDet.AccountID(null);
        newDet.CreditorInvoiceDetailDate(new Date(datetext));
      }
    }
  });
  td.append(tempDatepicker);
};

function HideDateSelect(busObj, btn) {
  busObj.SelectDatesVisible(false);
  $(tempDatepicker).datepicker('destroy');
};

$(document).ready(function () {
  SetCreditorInvoiceTotals();
  $('#HRInvoices').on('hide.bs.modal', function (e) {
    //ViewModel.HRInvoiceCriteria(null);
  });
});

Singular.OnPageLoad(function () {
  SetCreditorInvoiceTotals();
})