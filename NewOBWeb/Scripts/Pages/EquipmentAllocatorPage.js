﻿EquipmentAllocatorPage = {
  Refresh: function () {
    ViewModel.EquipmentAllocationManager().Refresh()
    ViewModel.EquipmentAllocationManagerByVenue().Refresh()
    $("#Filters").modal('hide')
  },
  resultBadgeHTML: function () {

  },
  showResultHistory: function () {

  },
  showBookingTimes: function (equipmentAllocator, buttonElement) {
    ViewModel.CurrentEventGuid(equipmentAllocator.Guid())
    this.showCurrentSynergyEventTimesPopup(equipmentAllocator, buttonElement)
  },
  showChannelSelector: function () {
    if ($("#ChannelSelectPannel").hasClass("flipInX go")) {
      $("#ChannelSelectPannel").removeClass("flipInX go")
      $("#ChannelSelectPannel").addClass("flipOutX go")
      window.setTimeout(function () {
        $("#ChannelSelectPannel").css('display', 'none')
      }, 750)
    }
    else {
      $("#ChannelSelectPannel").removeClass("flipOutX go")
      $("#ChannelSelectPannel").css('display', 'block')
      $("#ChannelSelectPannel").addClass("flipInX go")
    }
  },
  currentEquipmentAllocator: function () {
    if (ViewModel.CurrentEventGuid()) {
      return ViewModel.EquipmentAllocationList().Find("Guid", ViewModel.CurrentEventGuid())
    }
    return null;
  },
  showCurrentSynergyEventTimesPopup: function (equipmentAllocator, buttonElement) {
    $("#BookingTimesModal").modal()
    //var dimensionProperties = buttonElement.getBoundingClientRect()
    //var elem = document.getElementById("EditTimesPanelSynergyEvent")
    //elem.style.display = "block"
    //elem.style.position = "absolute"
    //elem.style.top = dimensionProperties.top.toString() + "px"
    //elem.style.left = dimensionProperties.left.toString() + "px"
    //synergyEvent.CallTime.BoundElements[0].focus()
  },
  doneEditingTimes: function (synergyEvent, buttonElement) {
    ViewModel.CurrentEventGuid(null)
    this.hideCurrentSynergyEventTimesPopup(synergyEvent, buttonElement)
  },
  hideCurrentSynergyEventTimesPopup: function (synergyEvent, doneButtonElement) {
    $("#BookingTimesModal").modal('hide')
  },
  showFilters: function () {
    $("#Filters").modal()
  }
};