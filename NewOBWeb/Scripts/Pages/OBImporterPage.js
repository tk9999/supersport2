﻿
ROChannelBO.IsSelectedSet = function (self) {

  if (self.IsSelected()) {
    var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
    if (!item) {
      self.ChannelID()
      var ni = new SelectedItemObject()
      ni.ID(self.ChannelID())
      ni.Description(self.ChannelShortName())
      ViewModel.SelectedChannelItems().push(ni)
    }
  }
  else {
    var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
    if (item) {
      ViewModel.SelectedChannelItems().pop(item)
    }
  }

  var xmlCriteria = ""
  if (ViewModel.SelectedChannelItems().length > 0) {
    xmlCriteria = '<DataSet>'
    ViewModel.SelectedChannelItems().Iterate(function (item, indx) {
      xmlCriteria += "<Table ID='" + item.ID().toString() + "' />";
    })
    xmlCriteria += '</DataSet>'
  }

  ViewModel.RoomAllocatorCriteria().SelectedChannelIDsXML(xmlCriteria)

}

AllocatorChannelListCriteriaBO = {
  SystemIDSet: function (self) {
    ViewModel.FreezeSetExpressions(true)
    ViewModel.SynergyEventList().Iterate(function (itm, indx) {
      itm.SystemID(self.SystemID())
      if (ClientData.ROUserSystemAreaList.length == 1) {
        itm.ProductionAreaID(ClientData.ROUserSystemAreaList[0].ProductionAreaID)
      }
    })
    ViewModel.RoomAllocatorList().Iterate(function (itm, indx) {
      itm.SystemID(self.SystemID())
      if (ClientData.ROUserSystemAreaList.length == 1) {
        itm.ProductionAreaID(ClientData.ROUserSystemAreaList[0].ProductionAreaID)
      }
    })
    ViewModel.FreezeSetExpressions(false)
  },
  StartDateSet: function (self) {
    ViewModel.RoomAllocatorCriteria().StartDate(self.StartDate())
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      ViewModel.RoomAllocatorListManager().Refresh()
      RoomAllocatorPage.refreshChannelAllocator()
      RoomAllocatorPage.refreshGridByEvent()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  EndDateSet: function (self) {
    ViewModel.RoomAllocatorCriteria().EndDate(self.EndDate())
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      RoomAllocatorPage.refreshChannelAllocator()
      RoomAllocatorPage.refreshGridByEvent()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  LiveSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Live(self.Live())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  DelayedSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Delayed(self.Delayed())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  PremierSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Premier(self.Premier())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  GenRefNoSet: function (self) {
    ViewModel.RoomAllocatorCriteria().GenRefNo(self.GenRefNo())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  GenRefNoStringSet: function (self) {
    ViewModel.RoomAllocatorCriteria().GenRefNoString(self.GenRefNoString())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  PrimaryChannelsOnlySet: function (self) {
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      self.AllChannels(false)
      ViewModel.ROChannelList().Iterate(function (ch, chInd) {
        if (self.PrimaryChannelsOnly()) {
          if (ch.PrimaryInd()) { if (!ch.IsSelected()) { ch.IsSelected(true) } }
          else {
            ch.IsSelected(false)
          }
        } else {
          ch.IsSelected(false)
        }
      })
      ViewModel.RoomAllocatorListManager().Refresh()
      RoomAllocatorPage.refreshChannelAllocator()
      RoomAllocatorPage.refreshGridByEvent()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  AllChannelsSet: function (self) {
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      self.PrimaryChannelsOnly(false)
      ViewModel.ROChannelList().Iterate(function (ch, chInd) {
        if (self.AllChannels()) {
          ch.IsSelected(true)
        } else {
          ch.IsSelected(false)
        }
      })
      ViewModel.RoomAllocatorListManager().Refresh()
      RoomAllocatorPage.refreshChannelAllocator()
      RoomAllocatorPage.refreshGridByEvent()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  GenreSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Genre(self.Genre())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  SeriesSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Series(self.Series())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  },
  TitleSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Title(self.Title())
    ViewModel.RoomAllocatorListManager().Refresh()
    RoomAllocatorPage.refreshChannelAllocator()
    RoomAllocatorPage.refreshGridByEvent()
  }
}

Availability = {
  checkRoomAvailability: function (obj, StartDateTime, EndDateTime, afterCheck) {
    if (obj.RoomID()) {
      obj.IsProcessing(true)
      var room = ClientData.RORoomList.Find("RoomID", obj.RoomID())
      OBMisc.Resources.getROResourceBookings(StartDateTime, EndDateTime, room.ResourceID, null, null, null, null, null,
                                             function (response) {
                                               obj.IsProcessing(false)
                                               obj.RoomClashCount(response.Data.length)
                                               afterCheck(response)
                                             },
                                             function (response) {
                                               obj.IsProcessing(false)
                                               OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                               afterCheck(response)
                                             })
    }
    else {
      obj.RoomClashCount(0)
      obj.IsProcessing(false)
      afterCheck(null)
    }
  },
  checkMCRContAvailability: function (obj, StartDateTime, EndDateTime) {
    if (obj.ResourceIDMCRController()) {
      obj.IsProcessing(true)
      OBMisc.Resources.getROResourceBookings(StartDateTime, EndDateTime, obj.ResourceIDMCRController(), null, null, null, null, null,
                                           function (response) {
                                             obj.IsProcessing(false)
                                             obj.MCRControllerClashCount(response.Data.length)
                                             afterCheck(response)
                                           },
                                           function (response) {
                                             obj.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                             afterCheck(response)
                                           })
    }
    else {
      obj.MCRControllerClashCount(0)
      obj.IsProcessing(false)
      afterCheck(null)
    }
  },
  checkSCCROpAvailability: function (obj, StartDateTime, EndDateTime) {
    if (obj.ResourceIDSCCROperator()) {
      obj.IsProcessing(true)
      OBMisc.Resources.getROResourceBookings(StartDateTime, EndDateTime, obj.ResourceIDSCCROperator(), null, null, null, null, null,
                                           function (response) {
                                             obj.IsProcessing(false)
                                             obj.SCCROperatorClashCount(response.Data.length)
                                             afterCheck(response)
                                           },
                                           function (response) {
                                             obj.IsProcessing(false)
                                             OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
                                             afterCheck(response)
                                           })
    }
    else {
      obj.SCCROperatorClashCount(0)
      obj.IsProcessing(false)
      afterCheck(null)
    }
  },
  checkAllAvailability: function (obj, StartDateTime, EndDateTime) {
    var me = this
    obj.IsProcessing(true)
    me.checkRoomAvailability(obj, StartDateTime, EndDateTime,
      function (response) {
        me.checkMCRContAvailability(obj, StartDateTime, EndDateTime, function (response) {
          me.checkSCCROpAvailability(obj, StartDateTime, EndDateTime, function (response) {
            obj.IsProcessing(false)
          })
        })
      })
  }
}

SynergyEventChannelBO = {
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case "ChannelButton":
        var parent = instance.GetParent()
        var settings = ClientData.ROSystemProductionAreaList.Filter("SystemID", parent.SystemID()).Filter("ProductionAreaID", parent.ProductionAreaID())
        var setting = null
        if (settings.length == 1) {
          return settings[0].RequiresChannelsForRoomScheduling
        }
        return false
      default:
        return !instance.IsProcessing()
        break;
    }
  },
  channelClicked: function (self) {
    self.IsSelected(!self.IsSelected())
  },
  buttonHTML: function (self) {
    if (self.RoomScheduleCount() > 0) {
      return self.ChannelShortName() + ' <span class="badge badge-white">' + self.RoomScheduleCount().toString() + '</span>'
    } else {
      return self.ChannelShortName()
    }
  },
  buttonCSS: function (self) {
    if (self.IsSelected()) {
      return "btn btn-xs btn-primary"
    }
    else {
      return "btn btn-xs btn-default"
    }
  }
}

SynergyEventListCriteriaBO = {
  SystemIDSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  StartDateSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  EndDateSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  LiveSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  DelayedSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  PremierSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  GenRefNoSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  GenRefNoStringSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  PrimaryChannelsOnlySet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  AllChannelsSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  GenreSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  SeriesSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  TitleSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  },
  KeywordSet: function (self) {
    ViewModel.SynergyEventListManager().Refresh()
  }
}

OBImporterPage = {
  refreshSynergyEvents: function () {
    ViewModel.SynergyEventListManager().Refresh()
  },
  getSlugs: function (AllocatorChannelEvent) {
    AllocatorChannelEvent.IsProcessing(true)
    OBMisc.Slugs.getSlugs(AllocatorChannelEvent.GenRefNumber(),
      function (response) {
        AllocatorChannelEvent.SlugItemList.Set(response.Data)
        AllocatorChannelEvent.IsProcessing(false)
      },
      function (response) {
        OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000)
        AllocatorChannelEvent.IsProcessing(false)
      })
  },
  getSlugsSynergyEvent: function () {
    ViewModel.CurrentSynergyEvent().IsProcessing(true)
    OBMisc.Slugs.getSlugs(ViewModel.CurrentSynergyEvent().GenRefNumber(),
      function (response) {
        ViewModel.CurrentSynergyEvent().SlugItemList.Set(response.Data)
        ViewModel.CurrentSynergyEvent().IsProcessing(false)
      },
      function (response) {
        OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000)
        ViewModel.CurrentSynergyEvent().IsProcessing(false)
      })
  },
  currentSynergyEvent: function () {
    if (ViewModel.CurrentSynergyEventGuid()) {
      return ViewModel.SynergyEventList().Find("Guid", ViewModel.CurrentSynergyEventGuid())
    }
    return null;
  },
  createManually: function () {
    $("#ManualProduction").on("shown.bs.modal", function (response) {
      ViewModel.ManualProduction(new ManualProductionObject())
      Singular.Validation.CheckRules(ViewModel.ManualProduction())
    })
    $("#ManualProduction").modal()
  },
  saveManualProduction: function (production) {
    ProductionDetailBO.saveManualProduction(production,
      function (response) {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "", 500)
        production.ProductionID(response.Data.ProductionID)
        production.ProductionRefNo(response.Data.ProductionRefNo)
        ViewModel.ManualProductionCreated(true)
      },
      function (response) {
        ViewModel.ManualProductionCreated(false)
      })
  },
  doNewImport: function () {
    ViewModel.SynergyEventListCriteria().IsProcessing(true)
    OBMisc.Notifications.GritterInfo("Refreshing :(", "Please don't click anything", 1000)
    ViewModel.CallServerMethod("DoNewSynergyImport", {
      StartDate: ViewModel.SynergyEventListCriteria().StartDate().format('ddd dd MMM yyyy'),
      EndDate: ViewModel.SynergyEventListCriteria().EndDate().format('ddd dd MMM yyyy')
    }, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Data Refreshed", "How Refreshing :)", 1000)
      }
      else {
        OBMisc.Notifications.GritterError("Data Refresh Failed", response.ErrorText, 1000)
      }
      ViewModel.SynergyEventListCriteria().IsProcessing(false)
    })
  },
  setupNewManualProduction: function () {
    ViewModel.ManualProductionCreated(false)
    ViewModel.ManualProduction(new ManualProductionObject())
  }
}

Singular.OnPageLoad(function () {

})
