﻿Singular.OnPageLoad(function () {
  ROTeams.Init();
  ROTeams.Manager().SingleSelect(true);
  ROTeams.Manager().MultiSelect(false);
  ROTeams.SetOptions({
    AfterRowSelected: function (ROTeam) {
    },
    AfterRowDeslected: function (DeSelectedItem) {
    }
  });
  ViewModel.ROTeamListManager().Refresh();
});//OnPageLoad

Teams = {
  Methods: {
    SaveTeamShiftPattern: function () {
      Singular.SendCommand("SaveTeamShiftPattern", {}, function (response) {
        if (response) {
          alert("Saved");
        }
      });
    },
    SelectTab: function (TabID) {
      var elem = $("." + TabID)
      $(elem).tab('show');
    },
    //WhatClicked: function (e) {
    //  var target = $(event.target);
    //  var BO = ko.dataFor(target[0]);
    //  },
    AddNewDiscHRICR: function (obj) {
      var Team = ClientData.TeamList.Find("TeamID", obj.TeamID());
      if (Team) {
        var sTeam = Teams.Methods.CustomFormatter().Deserialise(obj);
        sTeam.TeamDisciplineHumanResourceList.AddNew();
      }
    },
    AddNewDiscHR: function () {
      var dhr = ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList.AddNew();
    },//AddNewDiscHR
    EditPatternDetails: function (obj) {
      $("#EditPatternDetails").modal();
    },//EditPatternDetails
    DivHTML: function () {
      return "<span id='divTitle'><strong>Options</strong></span><br>" +
             "<button id='EditTeamBtn' type='button' class='btn-xs btn-default btn btn-block' data-original-button-class='btn-default'><span>Edit Team</span></button>" +
             "<br><button id='ManagePatternsBtn' type='button' class='btn btn-xs btn-default btn-block' data-original-button-class='btn-default'><span>Manage Patterns</span></button>";
    },
    showInfo: function (event, button) {
      $("#menu").hide();
      var offset = $(button).offset();
      var topOffset = $(button).offset().top - $(window).scrollTop();
      $("#menu").css({
        position: "absolute",
        top: (topOffset - 10) + "px",
        left: (offset.left - 450) + "px",
        width: "150px",
        height: "120px"
      }).show();
    },
    OpenTeamOptions: function (e, obj) {
      obj.SelectedInd(!obj.SelectedInd());
      //ViewModel.TeamSelected().TeamID(obj.TeamID());
      //ViewModel.TeamSelected().SelectedInd(obj.SelectedInd());
      //ViewModel.TeamSelected().TeamName(obj.TeamName()); 
      //ViewModel.TeamSelected().SystemAreaShiftPatternID(obj.SystemAreaShiftPatternID());
      ViewModel.ROTeamListManager().SelectedItems.Set([]);
      Teams.Methods.showInfo(event, e);
    },
    ManagePatterns: function (team) {
      $("#menu").hide();
    },
    OpenDiscHRModal: function (obj) {
      ViewModel.ROTeamListManager().SelectedItems.Set([]);
      obj.SelectedInd(!obj.SelectedInd());
      Singular.SendCommand("GetEditableShiftPattern", {
        SystemAreaShiftPatternID: obj.SystemAreaShiftPatternID(),
        TeamID: obj.TeamID()
      }, function (response) {
        $('#DiscHR').modal();
      });
    },
    //SetEditableTeam: function (obj) {
    //  Singular.SendCommand("SetEditableTeam", {
    //    TeamID: obj.TeamID()
    //  }, function (response) {
    //    if (!response) {
    //      ViewModel.TeamList().Iterate(function (itm, ind) {
    //        Singular.Validation.CheckRules(itm);
    //      });
    //    }
    //  });
    //},
    GetEditableShiftPattern: function (obj) {
      ViewModel.ROTeamListManager().SelectedItems.Set([]);
      obj.SelectedInd(!obj.SelectedInd());
      Singular.SendCommand("GetEditableShiftPattern", {
        SystemAreaShiftPatternID: obj.SystemAreaShiftPatternID(),
        TeamID: obj.TeamID()
      }, function (response) {
        if (!response) {
          ViewModel.TeamShiftPatternList().Iterate(function (itm, ind) {
            Singular.Validation.CheckRules(itm);
          });
        }
        $('#Patterns').modal();
      });
    },//GetEditableShiftPattern
    ApplyToTeam: function (obj) {
      ViewModel.EditableShiftPattern(obj);
      Singular.SendCommand("ApplyShiftPattern", {}, function (response) {
        if (response.Success) {
          alert("Pattern Assigned to Team");
        }
      });
    },
    RemoveTeam: function (obj) {
      ViewModel.RemovingTeam(true);
      ViewModel.CurrentTeamDisciplineHumanResource(null);
      var Team = Teams.Methods.CustomFormatter().Serialise(obj);
      ViewModel.CallServerMethod("RemoveTeam", {
        Team: Team
      }, function (response) {
        if (response.Success) {
          window.alert("Team Removed");
          ViewModel.EditableShiftPattern(null);
          ViewModel.EditableTeam(null);
          ViewModel.ROTeamListManager().Refresh();
        }
        else {
          window.alert("Cannot Remove: Team has Schedules");
        }
      });
      ViewModel.RemovingTeam(false);
    },//RemoveTeam
    DeselectBeforeEdit: function (obj) {
      ViewModel.ROTeamList().Iterate(function (item, index) {
        item.SelectedInd(false);
      });
    },//DeselectBeforeEdit
    ThisWeek: function (obj) {
      var defaultStyle = "";
      switch (obj.WeekNo()) {
        case 1:
          defaultStyle = 'Week1';
          break;
        case 2:
          defaultStyle = 'Week2';
          break;
        case 3:
          defaultStyle = 'Week3';
          break;
        case 4:
          defaultStyle = 'Week4';
          break;
        default:
          defaultStyle = '';
      }
      return defaultStyle;
    },//ThisWeek
    ThisTeam: function (obj) {
      var defaultStyle = "";
      if (obj.SelectedInd()) {
        defaultStyle = 'SelectedTeam';
      }
      return defaultStyle;
    },//ThisTeam
    GetSystemAreaPattern: function (obj) {
      var SASP = ClientData.ROSystemAreaShiftPatternList().Find('ID', obj.SystemAreaShiftPatternID());
      if (SASP) {
        return onj.SystemAreaShiftPatternID();
      }
    },//GetSystemAreaPattern
    SelectTeam: function (Team) {
      Team.SelectedInd(!Team.SelectedInd());
    },//SelectTeam
    SelectTeamCss: function (Team) {
      if (Team.SelectedInd()) {
        return "btn-custom btn-primary";
      } else {
        return "btn-custom btn-warning";
      }
    },//SelectTeamCss
    SelectTeamHtml: function (Team) {
      if (Team.SelectedInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span> Yes" } else { return "<span class=\"glyphicon glyphicon-remove\"></span> No" }
    },//SelectTeamHtml
    PopulateHRSchedule: function () {
      Singular.ShowLoadingBar();
      Singular.SendCommand('PopulateHRSchedule', {
        TeamID: null,
        StartDatePLO: null,
        EndDatePLO: null,
        PlayoutOpsInd: false
      },
       function (d) {
         if (d == 1) //Success
           alert('Schedule Generated');
         else if (d == 2) //No HR
           alert('No Human Resources Attached to this Team');
         else if (d == 3)
           alert('Please fix validation errors');
         Singular.HideLoadingBar();
       });
    },//PopulateHRSchedule
    CanSelectHR: function (ROICRSkilledHR) {
      var n = 0;
      for (var i = 0; i < ViewModel.ROICRSkilledHRList().length - 1; i++) {
        if (ROICRSkilledHR.HumanResourceID() == ViewModel.ROICRSkilledHRList()[i].HumanResourceID() && ViewModel.ROICRSkilledHRList()[i].OnTeamInd()) {
          n++;
        }
      }
      if (n > 0) {
        return false;
      }
      else
        return true;
    },//CanSelectHR
    CanSelectHRPLO: function (ROPLOSkilledHR) {
      var n = 0;
      for (var i = 0; i < ViewModel.ROPLOSkilledHRList().length - 1; i++) {
        if (ROPLOSkilledHR.HumanResourceID() == ViewModel.ROPLOSkilledHRList()[i].HumanResourceID() && ViewModel.ROPLOSkilledHRList()[i].OnTeamInd()) {
          n++;
        }
      }
      if (n > 0) {
        return false;
      }
      else
        return true;
    },//CanSelectHRPLO
    FilterTeamHumanResources: function () {
      Singular.ShowLoadingBar();
      if (ViewModel.ROICRSkilledHRList().length > 0) {
        if (!ViewModel.ROHumanResourceFilter() || ViewModel.ROHumanResourceFilter() == '') {
          ViewModel.ROICRSkilledHRList().Iterate(function (sk, Index) {
            sk.Visible(true);
          });
        } else {
          ViewModel.ROICRSkilledHRList().Iterate(function (sk, Index) {
            if (sk.HumanResource().toLowerCase().indexOf(ViewModel.ROHumanResourceFilter().toLowerCase()) >= 0) {
              sk.Visible(true);
            } else {
              sk.Visible(false);
            };
          });
        }
      }
      else {
        if (!ViewModel.ROHumanResourceFilter() || ViewModel.ROHumanResourceFilter() == '') {
          ViewModel.ROPLOSkilledHRList().Iterate(function (sk, Index) {
            sk.Visible(true);
          });
        } else {
          ViewModel.ROPLOSkilledHRList().Iterate(function (sk, Index) {
            if (sk.HumanResource().toLowerCase().indexOf(ViewModel.ROHumanResourceFilter().toLowerCase()) >= 0) {
              sk.Visible(true);
            } else {
              sk.Visible(false);
            };
          });
        }
      }
      Singular.HideLoadingBar();
    },//FilterTeamHumanResources
    //FilterTeamHumanResourcesPLO: function () {
    //  Singular.ShowLoadingBar();
    //  if (!ViewModel.ROHumanResourceFilter() || ViewModel.ROHumanResourceFilter() == '') {
    //    ViewModel.ROPLOSkilledHRList().Iterate(function (sk, Index) {
    //      sk.Visible(true);
    //    });
    //  } else {
    //    ViewModel.ROPLOSkilledHRList().Iterate(function (sk, Index) {
    //      if (sk.HumanResource().toLowerCase().indexOf(ViewModel.ROHumanResourceFilter().toLowerCase()) >= 0) {
    //        sk.Visible(true);
    //      } else {
    //        sk.Visible(false);
    //      };
    //    });
    //  }
    //  Singular.HideLoadingBar();
    //},//FilterTeamHumanResourcesPLO
    OnTeamCss: function (BulkTeamHumanResource) {
      if (BulkTeamHumanResource.OnTeamInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
    },//OnTeamCss
    OnTeamHtml: function (BulkTeamHumanResource) {
      if (BulkTeamHumanResource.OnTeamInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
    },//OnTeamHtml
    GetHRCss: function (TeamDisciplineHumanResource) {
      if (TeamDisciplineHumanResource.HumanResourceID()) {
        return "btn-custom btn-primary TeamDisciplineHumanResource";
      } else {
        return "btn-custom btn-default TeamDisciplineHumanResource";
      }
    },//GetHRCss
    OnTeam: function (BulkTeamHumanResource) {
      BulkTeamHumanResource.OnTeamInd(!BulkTeamHumanResource.OnTeamInd());
      ViewModel.CurrentTeamDisciplineHumanResource().HumanResourceID(BulkTeamHumanResource.HumanResourceID());
      ViewModel.CurrentTeamDisciplineHumanResource().HumanResource(BulkTeamHumanResource.HumanResource());
      if (BulkTeamHumanResource.OnTeamInd()) {
        Singular.SendCommand("UpdateTeamHRList", {
          HumanResourceID: BulkTeamHumanResource.HumanResourceID(),
          HumanResource: BulkTeamHumanResource.HumanResource()
        }, function (response) {
          if (response != undefined) {
            if (ViewModel.CurrentSystemID() == 4) { //ICR
              Singular.GetDataStateless('OBLib.Shifts.ICR.ReadOnly.ROICRSkilledHRList, OBLib', {
                DisciplineID: response.Item1,
                TeamID: response.Item2,
                ProductionAreaID: response.Item3
              },
              function (args) {
                if (args.Success) {
                  KOFormatter.Deserialise(args.Data, ViewModel.ROICRSkilledHRList);
                  Singular.HideLoadingBar();
                  $('#BulkTeamHumanResources').modal();
                }
              });
            }
            else if (ViewModel.CurrentSystemID() == 5) { //PLO
              Singular.GetDataStateless('OBLib.Shifts.PlayoutOperations.ReadOnly.ROPLOSkilledHRList, OBLib', {
                DisciplineID: response.Item1,
                TeamID: response.Item2,
                ProductionAreaID: response.Item3
              },
              function (args) {
                if (args.Success) {
                  KOFormatter.Deserialise(args.Data, ViewModel.ROPLOSkilledHRList);
                  ViewModel.ROPLOSkilledHRList().Iterate(function (itm, ind) {
                    Singular.Validation.CheckRules(itm);
                  });
                  Singular.HideLoadingBar();
                  //$('#BulkTeamHumanResourcesPLO').modal();
                }
              });
            }
          }
        });
      }
      else {
        // Not on Team
      }
    },//OnTeam
    OnTeamICR: function (BulkTeamHumanResource) {
      BulkTeamHumanResource.OnTeamInd(!BulkTeamHumanResource.OnTeamInd());
      ViewModel.CurrentTeamDisciplineHumanResource().HumanResourceID(BulkTeamHumanResource.HumanResourceID());
      ViewModel.CurrentTeamDisciplineHumanResource().HumanResource(BulkTeamHumanResource.HumanResource());
      if (BulkTeamHumanResource.OnTeamInd()) {
        Singular.SendCommand("UpdateTeamHRListICR", {
          HumanResourceID: BulkTeamHumanResource.HumanResourceID(),
          HumanResource: BulkTeamHumanResource.HumanResource(),
          TeamID: ViewModel.CurrentTeamDisciplineHumanResource().GetParent().TeamID()
        }, function (response) {
          if (response != undefined) {
            if (ViewModel.CurrentSystemID() == 4) { //ICR
              Singular.GetDataStateless('OBLib.Shifts.ICR.ReadOnly.ROICRSkilledHRList, OBLib', {
                DisciplineID: response.Item1,
                TeamID: response.Item2,
                ProductionAreaID: response.Item3
              },
              function (args) {
                if (args.Success) {
                  KOFormatter.Deserialise(args.Data, ViewModel.ROICRSkilledHRList);
                  Singular.HideLoadingBar();
                  $('#BulkTeamHumanResources').modal();
                }
              });
            }
            else if (ViewModel.CurrentSystemID() == 5) { //PLO
              Singular.GetDataStateless('OBLib.Shifts.PlayoutOperations.ReadOnly.ROPLOSkilledHRList, OBLib', {
                DisciplineID: response.Item1,
                TeamID: response.Item2,
                ProductionAreaID: response.Item3
              },
              function (args) {
                if (args.Success) {
                  KOFormatter.Deserialise(args.Data, ViewModel.ROPLOSkilledHRList);
                  ViewModel.ROPLOSkilledHRList().Iterate(function (itm, ind) {
                    Singular.Validation.CheckRules(itm);
                  });
                  Singular.HideLoadingBar();
                  //$('#BulkTeamHumanResourcesPLO').modal();
                }
              });
            }
          }
        });
      }
      else {
        // Not on Team
      }
    },//OnTeam
    EditShiftPatterns: function () {
      OldShiftPatterns.Search();
      $('#ShiftPatterns').modal();
    },
    TeamAreaDetails: function () {
      if (ViewModel.SelectedProductionAreaOnUI()) {
        return "Select Team " + '<small>' + ViewModel.SelectedProductionAreaOnUIString() + '</small>';
      }
      return "";
    },
    DiscHRDetails: function () {
      if (ViewModel.EditableTeam()) {
        return "Disciplines and Human Resources for Team<br>" + '<small>' + ViewModel.EditableTeam().TeamName() + '</small>';
      }
      return "";
    },
    MainTeamPatternDetails: function () {
      if (ViewModel.EditableTeam()) {
        return "Shift Patterns as per Team<br>" + '<small>' + ViewModel.EditableTeam().TeamName() + '</small>';
      }
      return "";
    }, //MainTeamPatternDetails
    TeamPatternDetails: function () {
      if (ViewModel.EditableTeam()) {
        return "Shift Patterns " + '<small>' + ViewModel.EditableTeam().TeamName() + '</small>';
      }
      return "";
    },//TeamDetails
    TeamDetails: function () {
      if (ViewModel.CurrentTeamDisciplineHumanResource()) {
        var DisciplineName = ClientData.ROProductionAreaAllowedDisciplineList.Find("DisciplineID", ViewModel.CurrentTeamDisciplineHumanResource().DisciplineID());
        if (DisciplineName) {
          return "Add Human Resource<br>" + '<small>' + DisciplineName.Discipline/*ViewModel.CurrentTeamDisciplineHumanResource().GetParent().TeamName()*/ + '</small>';
        }
      }
      return "";
    },//TeamDetails
    BookTeamHR: function (TeamDisciplineHumanResource) {
      ViewModel.CurrentTeamDisciplineHumanResource(TeamDisciplineHumanResource);
      ViewModel.CurrentTeamDisciplineHumanResourceID(TeamDisciplineHumanResource.TeamDisciplineHumanResourceID());
      ViewModel.CurrentDiscipline(TeamDisciplineHumanResource.DisciplineID());
      //ViewModel.CurrentTeamDisciplineHumanResource().HumanResource(TeamDisciplineHumanResource.HumanResource());
      Singular.ShowLoadingBar();
      if (ViewModel.CurrentSystemID() == 4) {
        Singular.GetDataStateless('OBLib.Shifts.ICR.ReadOnly.ROICRSkilledHRList, OBLib', {
          DisciplineID: TeamDisciplineHumanResource.DisciplineID(),
          TeamID: TeamDisciplineHumanResource.GetParent().TeamID(),
          ProductionAreaID: TeamDisciplineHumanResource.GetParent().ProductionAreaID()
        },
        function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.ROICRSkilledHRList);
            Singular.HideLoadingBar();
            $('#BulkTeamHumanResources').modal();
          }
        });
      }
      else if (ViewModel.CurrentSystemID() == 5) {
        Singular.GetDataStateless('OBLib.Shifts.PlayoutOperations.ReadOnly.ROPLOSkilledHRList, OBLib', {
          DisciplineID: TeamDisciplineHumanResource.DisciplineID(),
          TeamID: TeamDisciplineHumanResource.GetParent().TeamID(),
          ProductionAreaID: TeamDisciplineHumanResource.GetParent().ProductionAreaID()
        },
        function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.ROPLOSkilledHRList);
            Singular.HideLoadingBar();
            $('#BulkTeamHumanResourcesPLO').modal();
          }
        });
      }
    },//BookTeamHR
    HasDiscipline: function (obj) {
      if (obj.DisciplineID()) {
        return true;
      } else {
        return false;
      }
    },
    AddTeam: function () {
      Singular.SendCommand("NewTeam", {}, function (response) {
        ViewModel.NewTeam().SystemID(null);
        ViewModel.NewTeam().SystemID(ViewModel.CurrentSystemID());
        ViewModel.NewTeam().ProductionAreaID(null);
        ViewModel.NewTeam().ProductionAreaID(ViewModel.SelectedProductionAreaOnUI());
        $("#AddTeam").modal();
      });
    },
    IsTeamValid: function () {
      var valid = true;
      if (ViewModel.NewTeam()) {
        if (!ViewModel.NewTeam().IsValid()) {
          valid = false
        }
      }
      return valid;
    },
    GetTeamBrokenRulesHTML: function () {
      var ErrorString = ''
      if (ViewModel.NewTeam().IsValid() == false) {
        ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(ViewModel.NewTeam());
      }
      return ErrorString;
    },
    SaveNewTeam: function (obj) {
      Singular.SendCommand("SaveNewTeam", {}, function (response) {
        if (response.Success) {
          window.alert("Team Saved");
          ViewModel.ROTeamListManager().SortColumn('CreatedDateTime');
          ViewModel.ROTeamListManager().SortAsc(false);
          ViewModel.ROTeamListManager().Refresh();
          $("#AddTeam").modal('hide');
        }
      })
      //var SavableTeam = Teams.Methods.CustomFormatter().Serialise(obj);
      //ViewModel.CallServerMethod("SaveNewTeam", {
      //  Team: SavableTeam
      //}, function (response) {
      //  if (response.Success) {
      //    window.alert("Team Saved");
      //    ViewModel.ROTeamListManager().SortColumn('CreatedDateTime');
      //    ViewModel.ROTeamListManager().SortAsc(false);
      //    ViewModel.ROTeamListManager().Refresh();
      //    $("#AddTeam").modal('hide');
      //  }
      //});
    },
    RemoveTeamShiftPattern: function (obj) {
      //ViewModel.CurrentTeamShiftPattern(null);
      var ThisTSP = ViewModel.TeamShiftPatternList().Find("Guid", obj.Guid());
      if (ThisTSP) {
        ThisTSP.PatternName('');
        ThisTSP.SystemAreaShiftPatternID(null);
      }
    },
    RemoveTempTSP: function (obj) {
      var ThisTSP = ViewModel.TeamShiftPatternList().Find("Guid", obj.Guid());
      if (ThisTSP) {
        ViewModel.TeamShiftPatternList.Remove(ThisTSP);
        ViewModel.CurrentTeamShiftPattern(null);
      }
    },
    //RemoveHrDiscICR: function (obj) {
    //  var Team = ClientData.TeamList.Find("TeamID", obj.GetParent().TeamID());
    //  if (Team) {
    //    var TeamDiscHR = Team.TeamDisciplineHumanResourceList().Find("TeamDisciplineHumanResourceID", obj.TeamDisciplineHumanResourceID());
    //    if (TeamDiscHR) {
    //      Team.TeamDisciplineHumanResourceList().Remove(TeamDiscHR);
    //    }
    //  }
    //},
    RemoveHrDisc: function (obj) {
      var DiscHR = Teams.Methods.CustomFormatter().Serialise(obj);
      if (ViewModel.CurrentTeamDisciplineHumanResource() != null) {
        ViewModel.CurrentTeamDisciplineHumanResource(null);
        ViewModel.CurrentTeamDisciplineHumanResourceID(null);
        ViewModel.CurrentDiscipline(null);
      }
      ViewModel.CallServerMethod("RemoveHrDisc", {
        TDHR: DiscHR
      }, function (response) {
        if (response.Success) {
          Singular.SendCommand("Refresh", { TeamID: response.Data }, function (response) { });
        }
      });
    },
    CustomFormatter: function () {
      var f = new KOFormatterObject();
      f.IncludeChildren = true;
      f.IncludeClean = false;
      f.IncludeCleanInArray = false;
      f.IncludeCleanProperties = true;
      f.IncludeIsNew = true;
      return f;
    },
    ShowTeams: function (obj) {
      Singular.SendCommand("SetProductionArea", {
        ProductionAreaID: obj.ProductionAreaID(),
        ProductionArea: obj.ProductionArea()
      }, function (response) {
        ViewModel.ROTeamListCriteria().SystemID(obj.SystemID());
        ViewModel.ROTeamListCriteria().ProductionAreaID(obj.ProductionAreaID());
        ViewModel.ROTeamListManager().Refresh();
        ViewModel.ThisHRSystemList().Iterate(function (itm, ind) {
          itm.IsSelected(false);
          $("#TeamPanel").removeClass('FadeDisplay');
          $("#TeamPanel").addClass('FadeHide');
        });
        obj.IsSelected(!obj.IsSelected());
        if (obj.IsSelected() && obj.RequiresTeams()) {
          $("#TeamPanel").removeClass('FadeHide');
          $("#TeamPanel").addClass('FadeDisplay');
          $("#TeamDiv").addClass('FadeDisplay');
          $("#ShiftPatternsEdit").removeClass('FadeHide');
          $("#ShiftPatternsEdit").addClass('FadeDisplay');         
        }
      });
    },
    AddTeamShiftPattern: function () {
      var tsp = ViewModel.TeamShiftPatternList.AddNew();
      tsp.TeamID(ViewModel.EditableTeam().TeamID());
      tsp.TeamName(ViewModel.EditableTeam().TeamName());
    },
    CheckDHRValidity: function () {
      var valid = true;
      if (ViewModel.EditableTeam()) {
        if (ViewModel.EditableTeam().TeamDisciplineHumanResourceList().length > 0) {
          ViewModel.EditableTeam().TeamDisciplineHumanResourceList().Iterate(function (itm, ind) {
            if (!itm.IsValid()) {
              valid = false;
            }
          });
        }
      }
      return valid;
    },
    GetDHRBrokenRulesHTML: function () {
      var ErrorString = ''
      if (ViewModel.EditableTeam()) {
        if (ViewModel.EditableTeam().TeamDisciplineHumanResourceList().length > 0) {
          ViewModel.EditableTeam().TeamDisciplineHumanResourceList().Iterate(function (itm, ind) {
            if (!itm.IsValid()) {
              ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(itm);
              Singular.Validation.CheckRules(itm);
            }
          });
        }
      }
      return '<strong>Please fix the following validaton rules: </strong><br>' + ErrorString;
    },
    GetTeamBrokenRulesHTML: function () {
      var ErrorString = ''
      if (ViewModel.TeamList().length > 0) {
        ViewModel.TeamList().Iterate(function (itm, ind) {
          if (!itm.IsValid()) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(itm);
            Singular.Validation.CheckRules(itm);
          }
        });
      }
      return '<strong>Please fix the following validaton rules: </strong><br>' + ErrorString;
    },
    CheckTeamValidity: function () {
      var valid = true;
      if (ViewModel.TeamList().length > 0) {
        ViewModel.TeamList().Iterate(function (itm, ind) {
          if (!itm.IsValid()) {
            valid = false;
          }
        });
      }
      return valid;
    },
    GetTeamShiftPatternsBrokenRulesHTML: function () {
      var ErrorString = ''
      if (ViewModel.TeamShiftPatternList().length > 0) {
        ViewModel.TeamShiftPatternList().Iterate(function (itm, ind) {
          if (!itm.IsValid()) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(itm);
            Singular.Validation.CheckRules(itm);
          }
        });
      }
      return '<strong>Please fix the following validaton rules: </strong><br>' + ErrorString;
    },
    CheckValidity: function () {
      var valid = true;
      if (ViewModel.TeamShiftPatternList().length > 0) {
        ViewModel.TeamShiftPatternList().Iterate(function (itm, ind) {
          if (!itm.IsValid()) {
            valid = false;
          }
        });
      }
      return valid;
    },
    GetSystemAreaShiftPatternsBrokenRulesHTML: function () {
      var ErrorString = ''
      if (ViewModel.SystemAreaShiftPatternList().length > 0) {
        ViewModel.SystemAreaShiftPatternList().Iterate(function (itm, ind) {
          if (!itm.IsValid()) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(itm);
            Singular.Validation.CheckRules(itm);
          }
        });
      }
      return '<strong>Please fix the following validation rules: </strong><br>' + ErrorString;
    },
    CheckSystemAreaShiftPatternValidity: function () {
      var valid = true;
      if (ViewModel.SystemAreaShiftPatternList().length > 0) {
        ViewModel.SystemAreaShiftPatternList().Iterate(function (itm, ind) {
          if (!itm.IsValid()) {
            valid = false;
          }
        });
      }
      return valid;
    },
    // Pattern Modal
    AddPattern: function (obj) {
      var ThisTSP = ViewModel.TeamShiftPatternList().Find("Guid", ViewModel.CurrentTeamShiftPattern().Guid());
      if (ThisTSP) {
        ThisTSP.PatternName(obj.PatternName());
        ThisTSP.SystemAreaShiftPatternID(obj.SystemAreaShiftPatternID());
      }
      ViewModel.CurrentTeamShiftPattern().PatternName(obj.PatternName());
      ViewModel.CurrentTeamShiftPattern().SystemAreaShiftPatternID(obj.SystemAreaShiftPatternID());
    },
    SelectPattern: function (obj) {
      $('#PatternModal').modal();
      ViewModel.CurrentTeamShiftPattern(obj);
    }
  } //Methods
} //Teams

TeamShiftPatterns = {
  SaveTeamShiftPattern: function () {
    Singular.SendCommand("SaveTeamShiftPattern", {}, function (response) {
      if (response) {
        alert("Saved");
      }
    });
  },
  GenerateHRShifts: function (obj, plops) {
    Singular.ShowLoadingBar();
    Singular.SendCommand('PopulateHRSchedule', {
      TeamID: obj.TeamID(),
      StartDate: obj.StartDate(),
      EndDate: obj.EndDate(),
      PlayoutOpsInd: plops
    },
    function (d) {
      if (d == 1) //Success
        alert('Schedule Generated');
      else if (d == 2) //No HR
        alert('No Human Resources Attached to this Team');
      else if (d == 3)
        alert('Please fix validation errors');
      Singular.HideLoadingBar();
    });
  }
}; //TeamShiftPatterns

OldShiftPatterns = {
  SaveICR: function () {
    Singular.SendCommand("SaveICR", {}, function (response) { });
  },
  AddSystemAreaShiftPatternList: function () {
    ViewModel.SystemAreaShiftPatternList.AddNew()
  },

  CopyShiftPatternWeekDay: function (ShiftPatternWeekDay) {
    Singular.FocusAfterRender = true;
    var NewSPWD = ShiftPatternWeekDay.GetParent().SystemAreaShiftPatternWeekDayList.AddNew();
    NewSPWD.WeekNo(ShiftPatternWeekDay.WeekNo());
    NewSPWD.StartTime(ShiftPatternWeekDay.StartTime());
    NewSPWD.EndTime(ShiftPatternWeekDay.EndTime());
    NewSPWD.OffDay(ShiftPatternWeekDay.OffDay());
    //NewSPWD.DayShift(ShiftPatternWeekDay.DayShift());
  },

  UpdateNoWeekDays: function (SystemAreaShiftPattern) {
    SystemAreaShiftPattern.SystemAreaShiftPatternWeekDayList().Clear();
    for (var i = 0; i < SystemAreaShiftPattern.ShiftDuration() ; i++) {
      for (var k = 0; k < 7; k++) {
        var NewSPWD = SystemAreaShiftPattern.SystemAreaShiftPatternWeekDayList.AddNew();
        NewSPWD.WeekNo(i + 1);
        NewSPWD.WeekDay(k + 1);
      }
    }
  },

  SetupAssignPattern: function (SystemAreaShiftPattern) {
    CurrentPattern = SystemAreaShiftPattern;
    $("#AssignHRToShift").modal();
  },

  GetROHRQuickSearchList: function () {
    if (ViewModel.ROHRQuickSearchString().trim().length >= 3) {
      Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHRQuickSearchList, OBLib',
      {
        SystemAreaShiftPatternID: CurrentPattern.SystemAreaShiftPatternID(),
        StartDate: new Date(CurrentPattern.StartDate()),
        EndDate: new Date(CurrentPattern.EndDate()),
        HumanResource: ViewModel.ROHRQuickSearchString()
      },
      function (args) {
        ViewModel.ROHRQuickSearchList.Set(args.Data);
      })
    } else {
      ViewModel.ROHRQuickSearchList.removeAll();
    }
  },

  AlreadyOnPatternIndClick: function (ROHRQuickSearch) { },
  AlreadyOnPatternIndCss: function (ROHRQuickSearch) { if (ROHRQuickSearch.AlreadyOnPatternInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' } },
  AlreadyOnPatternIndHtml: function (ROHRQuickSearch) { if (ROHRQuickSearch.AlreadyOnPatternInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" } },

  AddSelectedHR: function (ROHRQuickSearch) {
    var e = ViewModel.SelectedHRList().Find('HumanResourceID', ROHRQuickSearch.HumanResourceID());
    if (e || ROHRQuickSearch.AlreadyOnPatternInd()) {
      //already added
    } else {
      var n = ViewModel.SelectedHRList.AddNew();
      n.HumanResourceID(ROHRQuickSearch.HumanResourceID());
      n.HumanResource(ROHRQuickSearch.HumanResource());
      n.ActionID(1);
    }
  },

  RemoveSelectedHR: function (SelectedHR) {
    ViewModel.SelectedHRList.Remove(SelectedHR);
  },

  AssignPattern: function () {
    Singular.ShowLoadingBar();
    Singular.SendCommand('AssignPatternToHR', { SystemAreaShiftPatternID: CurrentPattern.SystemAreaShiftPatternID() },
    function (d) {
      $("#AssignHRToShift").modal('hide');
      Singular.HideLoadingBar();
      CurrentPattern = null;
      Singular.ShowMessage('Shifts', 'Shifts Generated', 2)
    });
  },

  CustomFormatter: function () {
    var f = new KOFormatterObject();
    f.IncludeChildren = true;
    f.IncludeClean = false;
    f.IncludeCleanInArray = false;
    f.IncludeCleanProperties = true;
    f.IncludeIsNew = true;
    return f;
  },

  Search: function () {
    Singular.ShowLoadingBar();
    Singular.SendCommand("UpdateSASPList", {
      SystemID: ViewModel.SASPSystemID(),
      ProductionAreaID: ViewModel.SASPProductionAreaID(),
      StartDate: ViewModel.SASPStartDate(),
      EndDate: ViewModel.SASPEndDate(),
    }, function (response) {
      Singular.HideLoadingBar();
    });
  },

  SaveChanges: function () {
    var List = OldShiftPatterns.CustomFormatter().Serialise(ViewModel.SystemAreaShiftPatternList());
    Singular.ShowLoadingBar();
    ViewModel.CallServerMethod("SaveList", { SystemAreaShiftPatternList: List }, function (response) {
      if (response.Success) {
        Singular.HideLoadingBar();
        alert("Saved");
        OldShiftPatterns.Search();
      } else {
      }
    });
  },

  DeleteShiftPattern: function (ShiftPattern) {
    ViewModel.CurrentShiftPattern(ShiftPattern);
    var ThisSP = ViewModel.SystemAreaShiftPatternList().Find("Guid", ShiftPattern.Guid());
    if (ThisSP) {
      var deleteSP = confirm("Are you sure you wish to delete " + ThisSP.PatternName() + "?");
      if (deleteSP == true) {
        Singular.SendCommand("DeleteShiftPattern", {}, function (response) {
          ViewModel.CurrentShiftPattern(null);
          OldShiftPatterns.AfterDelete(response)
        });
      }
    }
  },

  AfterDelete: function (response) {
    OldShiftPatterns.Search();
  }
}; //OldShiftPatterns

$(document).on('hidden.bs.modal', '#EditPatternDetails', function (e) {
  Singular.SendCommand("Save", {}, function (response) { });
});

$(document).on('hidden.bs.modal', '#AddTeam', function (e) {
  Singular.SendCommand("ClearTeam", {}, function (response) {
  });
});

$(document).on('hidden.bs.modal', '#BulkTeamHumanResources', function (e) {
  if (ViewModel.CurrentProductionAreaID() != 3 && ViewModel.CurrentProductionID != 4) {
    Singular.SendCommand("CheckTeamRules", {}, function (response) { });
  }
});

$(document).on('hidden.bs.modal', '#BulkTeamHumanResourcesPLO', function (e) {
  ViewModel.LastHRID(null);
});

$(document).on('hidden.bs.modal', '#DiscHR', function (e) {
  ViewModel.ROTeamListManager().Refresh();
});

$(document).on('hidden.bs.modal', '#Patterns', function (e) {
  ViewModel.ROTeamListManager().Refresh();
});

$(document).on('hidden.bs.modal', '#ShiftPatterns', function (e) {
  if (ViewModel.SystemAreaShiftPatternList().length > 0) {
    ViewModel.SystemAreaShiftPatternList.Set([]);
  }
  Singular.SendCommand("CheckSASPRules", {}, function (response) { });
});

$(document).on('hidden.bs.modal', '#PatternAndHRModal', function (e) {
  if (ViewModel.TeamShiftPatternList().length > 0) {
    ViewModel.TeamShiftPatternList.Set([]);
  }
});