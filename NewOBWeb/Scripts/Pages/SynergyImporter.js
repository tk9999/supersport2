﻿//Spec Requirements---------------------------------
ROSpecRequirements = new ROProductionSpecsManager({
  ModalID: "#ROProductionSpecListModal",
  AfterRowSelected: function (ROSpec) {
    ViewModel.CurrentSynergyEvent().ProductionSpecRequirementID(ROSpec.ProductionSpecRequirementID());
    ViewModel.CurrentSynergyEvent().Spec(ROSpec.ProductionSpecRequirementName());
    ViewModel.CurrentROProductionSpec(ROSpec);
    //ROSpecRequirements.HideModal();
  },
  AfterRowDeSelected: function (DeSelectedItem) {
    ViewModel.CurrentSynergyEvent().ProductionSpecRequirementID(null);
    ViewModel.CurrentSynergyEvent().Spec('');
  },
  beforeOpen: function () {
    ViewModel.ROProductionSpecListManager().SingleSelect(true);
    ViewModel.ROProductionSpecListManager().MultiSelect(false);
  }
});

var ROSynergyEvents;
ROSynergyEvents = new ROSynergyEventListManager({
  AfterRowSelected: function (ROSynergyEventChannel) {
  },
  AfterRowDeSelected: function (DeSelectedItem) {
  },
  beforeOpen: function () {
    ViewModel.ROSynergyEventListManager().SingleSelect(false);
    ViewModel.ROSynergyEventListManager().MultiSelect(true);
  },
  afterOpen: function () {
    $("li[data-target='#Step1']").addClass("active");
    $("#Step1").addClass("active");
  }
});

ROSelectResources = new ROResourceSelectManager({
  AfterRowSelected: function (ROResource) {
    var ne = new SelectedEqObject();
    ne.ResourceID(ROResource.ResourceID());
    ne.EquipmentID(ROResource.EquipmentID());
    ne.ResourceName(ROResource.ResourceName());
    ViewModel.SelectedResources.push(ne);
  },
  AfterRowDeSelected: function (DeSelectedItem) {
    var ne = ViewModel.SelectedResources().Find("ResourceID", DeSelectedItem.ID());
    if (ne) {
      ViewModel.SelectedResources.Remove(ne);
    }
  },
  beforeShowModal: function (businessObject) {
    ViewModel.AddEquipmentToImportedEventID(businessObject.ImportedEventID());
  },
  beforeOpen: function () {
    ViewModel.ROResourceSelectListManager().MultiSelect(true);
  },
  beforeClose: function () {
    ViewModel.AddEquipmentToImportedEventID(null);
  }
});

SynergyImporter = {
  DoNewImport: function () {
    Singular.SendCommand("DoNewImport",
                         {

                         },
                         function (response) {

                         });
  },
  ShowImportSetup: function (ROSynergyEvent) {
    Singular.GetDataStateless("OBLib.Synergy.SynergyEventList, OBLib",
      {
        ImportedEventID: ROSynergyEvent.ImportedEventID()
      },
      function (response) {
        ViewModel.CurrentSynergyEvent.Set(response.Data[0]);
        $("#ImportSetup").dialog({
          title: "Setup Event",
          closeText: "Close",
          height: 720,
          width: 1280,
          modal: true,
          resizeable: true,
          open: function (event, ui) {

          },
          beforeClose: function (event, ui) {

          }
        });
      });
  },
  LastImportDateTime: function () {
    return new Date(ViewModel.LastImportDateTime()).format('dd MMMM yyyy HH:mm');
  },
  LastImportBy: function () {
    return ViewModel.LastImportBy();
  },
  SetupNewImport: function () {
    ManualMoveStep(2)
  },
  SkipNewImport: function () {
    ManualMoveStep(3)
  },
  SetupOB: function (SynergyEvent) {
    SynergyEvent.HasSelectedPlanningArea(false);
    SynergyEvent.SatOpsInd(false);
    SynergyEvent.StudioInd(false);
    SynergyEvent.OBInd(true);
    SynergyEvent.HasSelectedPlanningArea(true);
  },
  SetupStudio: function (SynergyEvent) {
    SynergyEvent.HasSelectedPlanningArea(false);
    SynergyEvent.SatOpsInd(false);
    SynergyEvent.StudioInd(true);
    SynergyEvent.OBInd(false);
    SynergyEvent.HasSelectedPlanningArea(true);
  },
  SetupSatOps: function (SynergyEvent) {
    SynergyEvent.HasSelectedPlanningArea(false);
    SynergyEvent.SatOpsInd(true);
    SynergyEvent.StudioInd(false);
    SynergyEvent.OBInd(false);
    SynergyEvent.HasSelectedPlanningArea(true);
  },
  Commit: function (SynergyEvent) {
    ViewModel.CallServerMethod("Commit",
      {
        Event: KOFormatter.Serialise(SynergyEvent)
      },
      function (response) {

      });
  },
  ShowFilters: function () {
    $("#Filters").dialog({
      title: "Filters",
      closeText: "Close",
      height: 360,
      width: 1024,
      modal: true,
      resizeable: true
    });
  },
  RefreshFromModal: function () {
    $("#Filters").dialog('destroy');
    ROSynergyEvents.RefreshList();
  },
  ShowEventModal: function (ROSynergyEvent) {
    Singular.GetDataStateless("OBLib.Synergy.SynergyEventList, OBLib",
      {
        ImportedEventID: ROSynergyEvent.ImportedEventID()
      },
      function (response) {
        ViewModel.CurrentSynergyEvent.Set(response.Data[0]);
        ViewModel.CurrentSynergyEvent().OBInd(true);
        $("#OBSetup").dialog({
          title: "Setup OB",
          closeText: "Close",
          height: 720,
          width: 1024,
          modal: true,
          resizeable: true,
          open: function (event, ui) {

          },
          beforeClose: function (event, ui) {

          }
        });
      });
  },
  ShowEquipmentModal: function () {

  },
  CreateOBBookings: function () {
    Singular.SendCommand("CreateOBBookings",
      {},
      function (response) {

      });
  },
  CreateStudioBookings: function () {
    Singular.SendCommand("CreateStudioBookings",
      {},
      function (response) {

      });
  },
  CreateSatOpsBookings: function () {
    Singular.SendCommand("CreateSatOpsBookings",
      {},
      function (response) {

      });
  },
  AddEquipment: function () {
    Singular.SendCommand("AddEquipment",
      {

      },
      function (response) {
        ViewModel.SelectedResources([]);
        ViewModel.AddEquipmentToImportedEventID(null);
        ROSelectResources.HideModal();
      });
  },
  ImportStartDateValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.ImportWizardStep() == 2 && !CtlError.Object.ImportStartDate()) {
      CtlError.AddError("Import From Date is required");
    }
  },
  ImportEndDateValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.ImportWizardStep() == 2 && !CtlError.Object.ImportEndDate()) {
      CtlError.AddError("Import To Date is required");
    }
  },
  PrevImportWizardStep: function () {
    if (ViewModel.ImportWizardStep() > 1) {
      //if (SynergyImporter.IsCurrentStepValid()) {
      var PrevStepNumID = "Step" + ViewModel.ImportWizardStep().toString();
      ViewModel.ImportWizardStep((ViewModel.ImportWizardStep() - 1));
      MoveStep(PrevStepNumID)
      //} else {
      //  alert("current step is not valid");
      //}
    }
  },
  NextImportWizardStep: function () {
    if (ViewModel.ImportWizardStep() < ViewModel.ImportWizardMaxStepNum()) {
      if (SynergyImporter.IsCurrentStepValid()) {
        var PrevStepNumID = "Step" + ViewModel.ImportWizardStep().toString();
        ViewModel.ImportWizardStep((ViewModel.ImportWizardStep() + 1));
        MoveStep(PrevStepNumID)
      } else {
        alert("current step is not valid");
      }
    }
  },
  IsCurrentStepValid: function () {
    switch (ViewModel.ImportWizardStep()) {
      case 1:
        return true;
        break;
      case 2:
        if (ViewModel.ImportStartDate() && ViewModel.ImportEndDate()) {
          return true;
        } else {
          return false;
        }
        break;
      case 3:
        if (ROSynergyEvents.Criteria().OBInd() || ROSynergyEvents.Criteria().StudioInd() || ROSynergyEvents.Criteria().SatOpsInd()) {
          return true;
        } else {
          return false;
        }
        break;
      case 4:
        if (ROSynergyEvents.SelectedItems().length > 0) {
          return true;
        } else {
          return false;
        }
        break;
      case 5:
        return ViewModel.IsValid();
        break;
    };
  }
}

function ManualMoveStep(NewStep) {
  $(".steps li.active").removeClass("active");
  $("#Step" + ViewModel.ImportWizardStep().toString()).removeClass("active");
  ViewModel.ImportWizardStep(NewStep);
  if (ViewModel.ImportWizardStep() == 4) {
    ROSynergyEvents.RefreshList();
  }
  var Step = "Step" + ViewModel.ImportWizardStep().toString();
  $("li[data-target='#" + Step + "']").addClass("active");
  $("#" + Step).addClass("active");
};

function MoveStep(PrevStepNumID) {
  if (ViewModel.ImportWizardStep() == 4) {
    ROSynergyEvents.RefreshList();
  } else if (ViewModel.ImportWizardStep() == 5) {
    Singular.SendCommand("FetchSynergyEvents",
                         {
                           OBInd: ViewModel.ROSynergyEventListCriteria().OBInd(),
                           StudioInd: ViewModel.ROSynergyEventListCriteria().StudioInd(),
                           SatOpsInd: ViewModel.ROSynergyEventListCriteria().SatOpsInd()
                         },
                         function (response) {

                         });
  }
  $(".steps li.active").removeClass("active");
  $("#" + PrevStepNumID).removeClass("active");
  var Step = "Step" + ViewModel.ImportWizardStep().toString();
  $("li[data-target='#" + Step + "']").addClass("active");
  $("#" + Step).addClass("active");
};

function GetTotalSelectedSynergyEvents() {
  if (ViewModel.SelectedSynergyEvents()) {
    return ViewModel.SelectedSynergyEvents().length.toString();
  } else {
    return "0"
  }
};

Singular.OnPageLoad(function () {

  $("li[data-target='#Step1']").addClass("active");
  $("#Step1").addClass("active");

  $('.main').addClass('big-text-mode').addClass('black-text');

  ROSpecRequirements.Init();

  ROSynergyEvents.Init(null, {
    AfterRowSelected: function (ROSynergyEvent) {
      ViewModel.SelectedEventIDs().push(ROSynergyEvent.ImportedEventID());
    },
    AfterRowDeSelected: function (DeSelectedItem) {
      ViewModel.SelectedEventIDs.Remove(DeSelectedItem.ID());
    }
  }, function () {
    ViewModel.ROSynergyEventListManager().SingleSelect(false);
    ViewModel.ROSynergyEventListManager().MultiSelect(true);
  });

  ROSelectResources.Init(null, function () {
    ViewModel.ROResourceSelectListManager().SetOptions({
      AfterRowSelected: function (ROResource) {
        var ne = new SelectedEqObject();
        ne.ResourceID(ROResource.ResourceID());
        ne.EquipmentID(ROResource.EquipmentID());
        ne.ResourceName(ROResource.ResourceName());
        ViewModel.SelectedResources.push(ne);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        var ne = ViewModel.SelectedResources().Find("ResourceID", DeSelectedItem.ID());
        if (ne) {
          ViewModel.SelectedResources.Remove(ne);
        }
      }
    })
  });

});