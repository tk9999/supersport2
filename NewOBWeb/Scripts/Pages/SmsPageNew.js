﻿var dragSrcEl = null;

SmsPage = {
  //variables
  findSmsElem: null,
  createSmsElem: null,
  createBatchElem: null,

  //Common
  onPageLoad: function () {

    var container = document.getElementById("page-content")
    container.classList.add("aside")

    $("#FindSms").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.SmsSearchListCriteria())
    })

    ViewModel.SmsSearchListCriteria().Scenario(1)
    ViewModel.SmsSearchListCriteria().StartDay(new Date())
    ViewModel.SmsSearchListCriteria().EndDay(new Date())

    this.findSmsElem = document.getElementById("find-smses");
    this.createSmsElem = document.getElementById("create-sms");
    this.createBatchElem = document.getElementById("create-batch");

  },
  selectMenuOption: function (element) {
    var menuOptions = document.querySelectorAll("li.menu-option")
    menuOptions.forEach(function (elem) {
      elem.classList.remove('active')
    })
    element.parentNode.classList.add('active')
  },

  findSms: function (element) {
    ViewModel.CurrentSms(null)
    ViewModel.CurrentSmsBatch(null)
    ViewModel.FindSms(true)
    ViewModel.CreateSms(false)
    ViewModel.GenerateSms(false)
    ViewModel.SmsSearchList([])
    Singular.Validation.CheckRules(ViewModel)
    this.selectMenuOption(element)
    //this.createSmsElem.classList.remove("sms-option-visible")
    //this.createBatchElem.classList.remove("sms-option-visible")
    //this.createSmsElem.classList.add("sms-option-hidden")
    //this.createBatchElem.classList.add("sms-option-hidden")
    //this.findSmsElem.classList.remove("sms-option-hidden")
    //this.findSmsElem.classList.add("sms-option-visible")
  },
  newSms: function (element) {
    ViewModel.CurrentSms(null)
    ViewModel.CurrentSmsBatch(null)
    ViewModel.CurrentSms(new SmsObject())
    ViewModel.CreateSms(true)
    ViewModel.GenerateSms(false)
    ViewModel.FindSms(false)
    Singular.Validation.CheckRules(ViewModel)
    this.selectMenuOption(element)
    //this.createBatchElem.classList.remove("sms-option-visible")
    //this.findSmsElem.classList.remove("sms-option-visible")
    //this.createBatchElem.classList.add("sms-option-hidden")
    //this.findSmsElem.classList.add("sms-option-hidden")
    //this.createSmsElem.classList.remove("sms-option-hidden")
    //this.createSmsElem.classList.add("sms-option-visible")
  },
  newSmsBatch: function (element) {
    ViewModel.CurrentSms(null)
    ViewModel.CurrentSmsBatch(null)
    ViewModel.CurrentSmsBatch(new SmsBatchObject())
    ViewModel.CreateSms(false)
    ViewModel.GenerateSms(true)
    ViewModel.FindSms(false)
    Singular.Validation.CheckRules(ViewModel)
    this.selectMenuOption(element)
    //this.findSmsElem.classList.remove("sms-option-visible")
    //this.createSmsElem.classList.remove("sms-option-visible")
    //this.findSmsElem.classList.add("sms-option-hidden")
    //this.createSmsElem.classList.add("sms-option-hidden")
    //this.createBatchElem.classList.remove("sms-option-hidden")
    //this.createBatchElem.classList.add("sms-option-visible")
  },

  refreshSearch: function () {
    ViewModel.SmsSearchListManager().PageNo(1)
    ViewModel.SmsSearchListManager().Refresh()
    $("#FindSms").modal('hide')
  },
  showAdvancedFilters: function () {
    $("#FindSms").modal()
  },

  firstPage: function () {
    ViewModel.SmsSearchListManager().First()
  },
  previousPage: function () {
    ViewModel.SmsSearchListManager().Prev()
  },
  nextPage: function () {
    ViewModel.SmsSearchListManager().Next()
  },
  lastPage: function () {
    ViewModel.SmsSearchListManager().Last()
  },

  saveSms: function () {
    var me = this
    SmsBO.saveSms(ViewModel.CurrentSms(), {
      onSuccess: function (response) {
        ViewModel.CurrentSmsBatch.Set(response.Data[0])
        //base
        ViewModel.SmsSearchListCriteria().Scenario(0)
        //general
        ViewModel.SmsSearchListCriteria().CreatedStartDate(response.Data.CreatedDate)
        ViewModel.SmsSearchListCriteria().CreatedEndDate(response.Data.CreatedDate)
        ViewModel.SmsSearchListCriteria().CreatedByUserID(response.Data.CreatedBy)
        ViewModel.SmsSearchListCriteria().CreatedBy(ViewModel.CurrentUserName())
        ViewModel.SmsSearchListCriteria().RecipientHumanResourceID(null)
        ViewModel.SmsSearchListCriteria().RecipientName("")
        //batch
        ViewModel.SmsSearchListCriteria().SmsBatchID(null)
        ViewModel.SmsSearchListCriteria().BatchName("")
        //area
        ViewModel.SmsSearchListCriteria().SystemIDs([])
        ViewModel.SmsSearchListCriteria().ProductionAreaIDs([])
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaStartDate(null)
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaEndDate(null)
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaID(null)
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaName("")
        //day
        ViewModel.SmsSearchListCriteria().StartDay(null)
        ViewModel.SmsSearchListCriteria().EndDay(null)
        var elem = document.getElementById("findsms")
        me.findSms(elem)
        me.refreshSearch()
      },
      onFail: function (response) {

      }
    })
  },
  saveBatch: function () {
    var me = this
    SmsBatchBO.saveItem(ViewModel.CurrentSmsBatch(), {
      onSuccess: function (response) {
        ViewModel.CurrentSmsBatch.Set(response.Data[0])
        //base
        ViewModel.SmsSearchListCriteria().Scenario(3)
        //general
        ViewModel.SmsSearchListCriteria().CreatedStartDate(response.Data[0].CreatedDateTime)
        ViewModel.SmsSearchListCriteria().CreatedEndDate(response.Data[0].CreatedDateTime)
        ViewModel.SmsSearchListCriteria().CreatedByUserID(response.Data[0].CreatedBy)
        ViewModel.SmsSearchListCriteria().CreatedBy(ViewModel.CurrentUserName())
        ViewModel.SmsSearchListCriteria().RecipientHumanResourceID(null)
        ViewModel.SmsSearchListCriteria().RecipientName("")
        //batch
        ViewModel.SmsSearchListCriteria().SmsBatchID(response.Data[0].SmsBatchID)
        ViewModel.SmsSearchListCriteria().BatchName(response.Data[0].BatchName)
        //area
        ViewModel.SmsSearchListCriteria().SystemIDs([])
        ViewModel.SmsSearchListCriteria().ProductionAreaIDs([])
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaStartDate(null)
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaEndDate(null)
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaID(null)
        ViewModel.SmsSearchListCriteria().ProductionSystemAreaName("")
        //day
        ViewModel.SmsSearchListCriteria().StartDay(null)
        ViewModel.SmsSearchListCriteria().EndDay(null)
        var elem = document.getElementById("findsms")
        me.findSms(elem)
        me.refreshSearch()
      },
      onFail: function (response) {

      }
    })
  },

  batchCriteriaColumnCss: function (batch) {
    if (batch.SelectRecipientsManually()) {
      return "animated-column col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"
    } else if (batch.SmsBatchTemplateID() == 3) {
      return "animated-column col-xs-12 col-sm-12 col-md-5 col-lg-3 col-xl-3"
    } else {
      return "animated-column col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4"
    }
  },
  selectRecipientsColumnCss: function (batch) {
    if (batch.SelectRecipientsManually()) {
      return "animated-column col-xs-12 col-sm-12 col-md-6 col-lg-9 col-xl-9"
    } else {
      return "animated-column hidden-column col-xs-12 col-sm-12 col-md-6 col-lg-8 col-xl-8"
    }
  },

  selectRecipients: function () {
    $("#SelectRecipients").modal()
  },
  addSelectedToSMS: function () {
    if (ViewModel.CurrentSms()) {
      this.addSelectedToSmsSingle()
    } else if (ViewModel.CurrentSmsBatch()) {
      this.addSelectedToSmsBatch()
    }
  },

  canEditCritria: function (criteriaObject, fieldName) {
    switch (fieldName) {
      case 'RefreshSearchButton':
        return (criteriaObject.IsValid())
        break;
      default:
        return true
        break;
    }
  },

  backFromSmsSearch: function (smsSearch, element) {
    smsSearch.InEditMode(false)
  },
  saveSmsSearch: function (smsSearch, element) {
    smsSearch.IsProcessing(true)
    SmsSearchBO.saveItem(smsSearch, {
      onSuccess: function (response) {
        KOFormatter.Deserialise(response.Data[0], smsSearch)
        smsSearch.InEditMode(false)
        smsSearch.IsProcessing(false)
      },
      onFail: function (response) {

        smsSearch.IsProcessing(false)
      }
    })
    smsSearch.InEditMode(false)
  },

  selectAllOnPage: function () {
    ViewModel.SmsSearchList().Iterate(function (em, emIndx) {
      em.IsSelected(!em.IsSelected())
    })
  },
  sendSelectedSmses: function () {
    var ids = []
    ViewModel.SmsSearchList().Iterate(function (em, ind) {
      if (em.IsSelected()) {
        ids.push(em.SmsID())
      }
    })
    if (ids.length == 0) {
      OBMisc.Notifications.GritterError('Nothing Selected', "No smses are selected", 3000)
    } else {
      ViewModel.CallServerMethod("PrepareSmsesForSending",
        {
          SmsBatchID: null,
          SmsIDs: ids
        },
        function (response) {
          if (response.Success) {
            OBMisc.Notifications.GritterSuccess('Success', response.ErrorText + " smses will be sent in 1 minute", 1500)
          } else {
            OBMisc.Notifications.GritterError('Error Generating', response.ErrorText, 3000)
          }
        })
    }
  },

  deleteSelectedSmses: function () {
    var me = this;
    var smsIDs = []
    ViewModel.SmsSearchList().Iterate(function (sms, emIndx) {
      if (sms.IsSelected()) {
        smsIDs.push(sms.SmsID())
      }
    })
    if (smsIDs.length > 0) {
      me.DeleteSmses(smsIDs, function (response) {
      })
    }
  },
  DeleteSmses: function (smsIDs, afterDelete) {
    var me = this
    ViewModel.CallServerMethod("DeleteSmses", {
      SmsIDs: smsIDs
    },
    function (response) {
      if (response.Success) {
        var deletedDescription = response.Data.DeletedCount.toString() + " smses have been deleted"
        var cannotDeleteDescription = response.Data.CannotDeleteCount.toString() + " smses cannot be deleted"
        var fullDescription = deletedDescription + " <br/> " + cannotDeleteDescription
        OBMisc.Notifications.GritterInfo('Delete Executed', fullDescription, 1500)
        me.refreshSearch()
      } else {
        OBMisc.Notifications.GritterError('Error Deleting', response.ErrorText, 3000)
      }
      afterDelete(response)
    })
  },


  FilterSmses: function () {
    ViewModel.SmsSearchListManager().Refresh()
  },
  //Find Human Resource Recipients
  GetSystemXmlIDs: function () {
    var me = this;
    var selected = [];
    ViewModel.UserSystemList().Iterate(function (subDept, dIndx) {
      if (subDept.IsSelected()) {
        selected.push(subDept.SystemID())
      }
    })
    return selected
  },
  GetProductionAreaXmlIDs: function () {
    var me = this;
    var selected = [];
    ViewModel.UserSystemList().Iterate(function (subDept, dIndx) {
      subDept.UserSystemAreaList().Iterate(function (area, dIndx) {
        if (area.IsSelected()) {
          selected.push(area.ProductionAreaID())
        }
      })
    })
    return selected
  },
  GetDisciplineXmlIDs: function () {
    var me = this;
    var selected = [];
    ViewModel.SelectableDisciplines().Iterate(function (discipline, dIndx) {
      if (discipline.IsSelected()) {
        selected.push(discipline.DisciplineID())
      }
    })
    return selected
  },
  GetContractTypeXmlIDs: function () {
    var me = this;
    var selected = [];
    ViewModel.SelectableContractTypes().Iterate(function (ct, dIndx) {
      if (ct.IsSelected()) {
        selected.push(ct.ContractTypeID())
      }
    })
    return selected
  },
  GetFilteredDisciplines: function () {
    var me = this;
    if (!ViewModel.IsProcessing()) {
      ViewModel.IsProcessing(true)
      Singular.GetDataStateless('OBLib.Maintenance.General.ReadOnly.RODisciplineSelectList, OBLib', {
        SystemIDs: me.GetSystemXmlIDs(),
        ProductionAreaIDs: me.GetProductionAreaXmlIDs()
      },
      function (response) {
        ViewModel.SelectableDisciplines.Set(response.Data);
        ViewModel.IsProcessing(false)
      })
    }
  },
  GetFilteredHumanResources: function () {
    if (!ViewModel.IsProcessing()) {
      ViewModel.SelectableHumanResourcesManager().PageSize(1000)
      var me = this;
      ViewModel.IsProcessing(true)
      ViewModel.SelectableHumanResourcesManager().afterRefresh = function () {
        ViewModel.IsProcessing(false)
      }
      ViewModel.SelectableHumanResourcesCriteria().SystemIDs(me.GetSystemXmlIDs())
      ViewModel.SelectableHumanResourcesCriteria().ProductionAreaIDs(me.GetProductionAreaXmlIDs())
      ViewModel.SelectableHumanResourcesCriteria().DisciplineIDs(me.GetDisciplineXmlIDs())
      ViewModel.SelectableHumanResourcesCriteria().ContractTypeIDs(me.GetContractTypeXmlIDs())
      ViewModel.SelectableHumanResourcesManager().Refresh()
    }
  },

  OnSubDeptSelect: function (ROSystem) {
    if (!ViewModel.IsProcessing()) {
      ROSystem.IsSelected(!ROSystem.IsSelected())
      this.GetFilteredDisciplines()
      this.GetFilteredHumanResources()
    }
  },
  OnAreaSelect: function (ROProductionArea) {
    if (!ViewModel.IsProcessing()) {
      ROProductionArea.IsSelected(!ROProductionArea.IsSelected())
      this.GetFilteredDisciplines()
      this.GetFilteredHumanResources()
    }
  },
  OnContractTypeSelect: function (ROContractType) {
    if (!ViewModel.IsProcessing()) {
      ROContractType.IsSelected(!ROContractType.IsSelected())
      this.GetFilteredHumanResources()
    }
  },
  OnDisciplineSelect: function (RODiscipline) {
    if (!ViewModel.IsProcessing()) {
      RODiscipline.IsSelected(!RODiscipline.IsSelected())
      this.GetFilteredHumanResources()
    }
  },

  SelectAllDisciplines: function () {
    ViewModel.SelectableDisciplines().Iterate(function (itm, indx) {
      if (!itm.IsSelected()) {
        itm.IsSelected(true);
      }
    })
    this.GetFilteredHumanResources()
  },
  ClearDisciplines: function () {
    ViewModel.SelectableDisciplines().Iterate(function (itm, itmInd) {
      itm.IsSelected(false);
    })
  },
  SelectAllHumanResources: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, indx) {
      if (SmsPage.canSelectContact(itm)) {
        itm.IsSelected(true)
      }
    })
  },
  ClearRecipients: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, itmInd) {
      itm.IsSelected(false)
    })
    ViewModel.SelectedRecipients([])
  },
  SelectAllContractTypes: function () {
    ViewModel.SelectableContractTypes().Iterate(function (itm, indx) {
      if (!itm.IsSelected()) {
        itm.IsSelected(true);
      }
    })
  },
  ClearContractTypes: function () {
    ViewModel.SelectableContractTypes().Iterate(function (itm, itmInd) {
      itm.IsSelected(false);
    })
  },

  ROHumanResourceFindCSS: function (ROHumanResourceContact) {
    if (ROHumanResourceContact.IsSelected()) {
      return 'btn btn-xs btn-primary'
    }
    else {
      return 'btn btn-xs btn-default'
    }
  },
  ROHumanResourceFindHTML: function (ROHumanResourceContact) {
    var icon = '<i class="fa fa-check-square-o" /> '
    var name = '<strong>' + ROHumanResourceContact.PreferredFirstSurname() + '</strong>'
    var primaryNum = (ROHumanResourceContact.HasValidMobileNumberPrimary() ? ' <strong class="strong-default">' + ROHumanResourceContact.CellPhoneNumber().toString() + '</strong>' : ' <strong class="strong-default"> No Primary Number </strong>')
    var secondaryNum = (ROHumanResourceContact.HasValidMobileNumberSecondary() ? ', <strong class="strong-default">' + ROHumanResourceContact.AlternativeContactNumber().toString() + '</strong>' : '')

    if (ROHumanResourceContact.IsSelected()) {
      return icon
              + name
              + ' - '
              + primaryNum
              + secondaryNum
    }
    else {
      return name
              + ' - '
              + primaryNum
              + secondaryNum
    }
  },

  //Single SmS
  addSelectedToSmsSingle: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        var isOnSmsPrimary = ViewModel.CurrentSms().SmsRecipientList().Find('CellNo', itm.CellPhoneNumber())
        var isOnSmsSecondary = ViewModel.CurrentSms().SmsRecipientList().Find('CellNo', itm.AlternativeContactNumber())
        if (!isOnSmsPrimary && itm.HasValidMobileNumberPrimary()) {
          SmsBO.getNewRecipientPrimary(ViewModel.CurrentSms(), itm)
        }
        if (!isOnSmsSecondary && itm.HasValidMobileNumberSecondary()) {
          SmsBO.getNewRecipientSecondary(ViewModel.CurrentSms(), itm)
        }
      }
    })
    OBMisc.Notifications.GritterSuccess("Human Resources", "Successfully Added to SMS List", 250);
  },

  //Batch
  addSelectedToSmsBatch: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        var isOnSmsPrimary = ViewModel.CurrentSmsBatch().SmsBatchRecipientList().Find('CellNo', itm.CellPhoneNumber())
        var isOnSmsSecondary = ViewModel.CurrentSmsBatch().SmsBatchRecipientList().Find('CellNo', itm.AlternativeContactNumber())
        if (!isOnSmsPrimary && itm.HasValidMobileNumberPrimary()) {
          SmsBatchBO.GetNewRecipientPrimary(ViewModel.CurrentSmsBatch(), itm)
        }
        if (!isOnSmsSecondary && itm.HasValidMobileNumberSecondary()) {
          SmsBatchBO.GetNewRecipientSecondary(ViewModel.CurrentSmsBatch(), itm)
        }
      }
    })
    OBMisc.Notifications.GritterSuccess("Human Resources", "Successfully Added to SMS List", 250);
  },


  FirstHRPage: function () {
    ViewModel.SelectableHumanResourcesManager().PageNo(1)
    this.GetFilteredHumanResources()
  },
  PreviousHRPage: function () {
    var cp = ViewModel.SelectableHumanResourcesManager().PageNo()
    ViewModel.SelectableHumanResourcesManager().PageNo(cp - 1)
    if (cp - 1 < 1) {
      ViewModel.SelectableHumanResourcesManager().PageNo(ViewModel.SelectableHumanResourcesManager().Pages())
    } else {
      ViewModel.SelectableHumanResourcesManager().PageNo(cp - 1)
    }
    this.GetFilteredHumanResources()
  },
  NextHRPage: function () {
    var cp = ViewModel.SelectableHumanResourcesManager().PageNo()
    if (cp + 1 > ViewModel.SelectableHumanResourcesManager().Pages()) {
      ViewModel.SelectableHumanResourcesManager().PageNo(1)
    } else {
      ViewModel.SelectableHumanResourcesManager().PageNo(cp + 1)
    }
    this.GetFilteredHumanResources()
  },
  LastHRPage: function () {
    ViewModel.SelectableHumanResourcesManager().PageNo(ViewModel.SelectableHumanResourcesManager().Pages())
    this.GetFilteredHumanResources()
  },


  SelectRecipientsSingleSms: function () {
    this.AddSelectedToSMS = this.AddSelectedToSmsSingle
    $("#SelectRecipients").modal()
  },
  DoneSelectingRecipients: function () {
    $("#SelectRecipients").modal('hide');
  },
  SaveAndSendSms: function () {
    Singular.SendCommand("SaveAndSendSms", {}, function (response) {
      if (response.Success) {
        OBMisc.Modals.ShowMessage(null, "success", "Sms Saved", "Your Sms has been saved successfully", "check in a few minutes to see the send statuses");
        SmsPage.ResetSearchCriteria();
        ViewModel.ROSmsFlatListManager().Refresh();
      } else {
        OBMisc.Modals.ShowMessage(null, "error", "Error Saving Sms", "Your Sms could not be saved", response.ErrorText)
      }
    });
  },
  SaveSms: function () {
    var me = this;
    Singular.SendCommand("SaveSms", {}, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Sms Saved", "Your Sms has been saved successfully", 1500)
        me.FindSms()
      } else {
        OBMisc.Notifications.GritterError("Error Saving Sms", response.ErrorText, 3000)
      }
    });
  },
  onContactClicked: function (ROHumanResourceContact) {
    ROHumanResourceContact.IsSelected(!ROHumanResourceContact.IsSelected())
  },
  canSelectContact: function (ROHumanResourceContact) {
    if (ROHumanResourceContact.HasValidMobileNumberPrimary() || ROHumanResourceContact.HasValidMobileNumberSecondary()) {
      return true
    }
    return false
  },

  RefreshSmsFlatList: function () {
    ViewModel.ROSmsFlatListManager().Refresh()
  },

  updateDragHandlers: function () {
    //var me = this;
    var draggables = document.querySelectorAll('div.smsbatch-crewgroup-hr');
    [].forEach.call(draggables, function (col) {
      col.removeEventListener('dragstart', SmsPage.handleDragStart, false)
      //col.removeEventListener('drop', SmsPage.handleDrop, false)
      col.addEventListener('dragstart', SmsPage.handleDragStart, false)
      //col.addEventListener('dragenter', handleDragEnter, false)
      //col.addEventListener('dragover', handleDragOver, false);
      //col.addEventListener('dragleave', handleDragLeave, false);
      //col.addEventListener('drop', SmsPage.handleDrop, false);
      //col.addEventListener('dragend', handleDragEnd, false);
    })

    var droppables = document.querySelectorAll('td.crewgroup-members');
    [].forEach.call(droppables, function (col) {
      //col.removeEventListener('dragstart', SmsPage.handleDragStart, false)
      col.removeEventListener('drop', SmsPage.handleDrop, false)
      col.removeEventListener('dragover', SmsPage.handleDragOver, false)
      //col.addEventListener('dragstart', SmsPage.handleDragStart, false);
      //col.addEventListener('dragenter', handleDragEnter, false)
      col.addEventListener('dragover', SmsPage.handleDragOver, false);
      //col.addEventListener('dragleave', handleDragLeave, false);
      col.addEventListener('drop', SmsPage.handleDrop, false)
      //col.addEventListener('dragend', handleDragEnd, false);
    })
  },

  handleDragStart: function (e) {
    // Target (this) element is the source node.
    e.dataTransfer.effectAllowed = 'move';
    dragSrcEl = this;
    dragSrcEl.style.cursor = '-webkit-grabbing';
  },
  handleDragOver: function (e) {
    e.preventDefault();
    // Set the dropEffect to move
    e.dataTransfer.dropEffect = "move";
    dragSrcEl.style.cursor = '-webkit-grabbing';
  },
  handleDrop: function (e) {
    // this/e.target is current target element.
    e.preventDefault();
    e.stopPropagation();
    dragSrcEl.style.cursor = '-webkit-grabbing';

    var oldParent = ko.dataFor(dragSrcEl).GetParent(),
        newParent = null,
        item = ko.dataFor(dragSrcEl);

    dragSrcEl.style.cursor = 'pointer';

    if (e.target.classList.contains('crewgroup-members')) {
      newParent = ko.dataFor(e.target)
    }
    else if (e.target.classList.contains('smsbatch-crewgroup-hr')) {
      newParent = ko.dataFor(e.target).GetParent()
    }

    if (e.stopPropagation) {
      e.stopPropagation(); // Stops some browsers from redirecting.
    }

    //Don't do anything if dropping the same column we're dragging.
    if (dragSrcEl != this) {
      // Set the source column's HTML to the HTML of the column we dropped on.
      oldParent.SmsBatchCrewGroupHumanResourceList.RemoveNoCheck(item)
      newParent.SmsBatchCrewGroupHumanResourceList.Add(item)
    }

    //reregister handlers
    SmsPage.updateDragHandlers()

    //cleanup
    dragSrcEl = null;
    return false;
  },
  afterSmsBatchCrewGroupAdded: function (newItem) {
    SmsPage.updateDragHandlers()
  }

}

SmsBatchBO.onProductionSystemAreaIDSelected = function (item, businessObject) {
  ROPSACrewGroupBO.get({
    criteria: {
      ProductionSystemAreaID: item.ProductionSystemAreaID
    },
    onSuccess: function (response) {
      response.Data.Iterate(function (cg, cgInd) {
        //crew
        var smsBatchCrewGroup = businessObject.SmsBatchCrewGroupList.AddNew()
        smsBatchCrewGroup.CrewTypeID(cg.CrewTypeID)
        smsBatchCrewGroup.GroupName(cg.CrewType)
        //hr
        cg.ROPSACrewGroupHRList.Iterate(function (cgh, cghInd) {
          var smsBatchCrewGroupHR = smsBatchCrewGroup.SmsBatchCrewGroupHumanResourceList.AddNew()
          smsBatchCrewGroupHR.HumanResourceID(cgh.HumanResourceID)
          smsBatchCrewGroupHR.HRName(cgh.HRName)
          smsBatchCrewGroupHR.ImageFileName(cgh.ImageFileName)
        })
      })
      SmsPage.updateDragHandlers()
    },
    onFail: function (response) {

    }
  })
}

Singular.OnPageLoad(function () {
  SmsPage.onPageLoad();
});