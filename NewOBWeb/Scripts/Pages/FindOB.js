﻿var CurrentProductionID = null;

FindOBPage = {
    processSignalRChanges: function(stringifiedChanges) {
        var response = JSON.parse(stringifiedChanges)
        var editStatusChanges = response.Data.EditStatusBookings
        editStatusChanges.Iterate(function(change, changeIndex) {
            var prd = ViewModel.ROProductionList().Find("ResourceBookingID", change.ResourceBookingID)
            if (prd) {
                prd.IsBeingEditedBy(change.IsBeingEditedByUserID)
                prd.InEditDateTime(change.InEditDateTime)
                prd.InEditByName(change.IsBeingEditedByName)
                prd.EditImagePath(change.IsBeingEditedByImagePath)
            }
        })
        //
    }
}

function UpdateCriteria() {
    ViewModel.ROProductionListCriteria().ProductionServicesInd(ViewModel.ProductionServicesInd());
    ViewModel.ROProductionListCriteria().ProductionContentInd(ViewModel.ProductionContentInd());
    ViewModel.ROProductionListCriteria().OutsideBroadcastInd(ViewModel.OutsideBroadcastInd());
    ViewModel.ROProductionListCriteria().StudioInd(ViewModel.StudioInd());
    ViewModel.ROProductionListCriteria().OBVan(ViewModel.OBVan());
    ViewModel.ROProductionListCriteria().Externalvan(ViewModel.Externalvan());
};

function AddDocuments(ROProduction) {
    CurrentProductionID = ROProduction.ProductionID();
    try {

        let prom = Singular.GetDataStatelessPromise('OBLib.Productions.Correspondence.ProductionDocumentList, OBLib', {
            ParentID: ROProduction.ProductionID(),
            ParentTable: 'Productions',
            SystemID: ViewModel.CurrentSystemID(),
            ProductionAreaID: 1
        });
        prom.then(data => {
            ViewModel.ProductionDocumentList.Set(data);
        });

        $('#ProductionDocuments').modal();
    }
    catch (err) {

    }
};

function ClearCriteria() {
    ViewModel.ROProductionListCriteria().ProductionTypeID(null);
    ViewModel.ROProductionListCriteria().EventTypeID(null);
    ViewModel.ROProductionListCriteria().ProductionVenueID(null);
    ViewModel.ROProductionListCriteria().TxDateFrom(null);
    ViewModel.ROProductionListCriteria().TxDateTo(null);
    ViewModel.ROProductionListCriteria().EventManagerID(null);
    ViewModel.ROProductionListCriteria().ProductionRefNo("");
};

function GetPostProdRptHref(ROPRoduction) {
    return Singular.RootPath + "/Reports/PostProductionReport.aspx?ProductionID=" + ROPRoduction.ProductionID().toString();
};

function GetProductionStatColour(ROProduction) {
    var defaultStyle = "";
    switch (ROProduction.ProductionAreaStatusID()) {
        case 2:
            defaultStyle = 'CrewFinalised';
            break;
        case 3:
            defaultStyle = 'CrewFinalised';
            break;
        case 4:
            defaultStyle = 'Reconciled';
            break;
        case 5:
            defaultStyle = 'Cancelled';
            break;
    }
    if (ROProduction.VisionViewInd()) {
        defaultStyle = 'VisionView';
    }
    return defaultStyle;
};

function Refresh() {
    ViewModel.ROProductionListPagingManager().Refresh();
};

function DocumentTypeIDSet() {

};

function AfterDocumentAdded(newDoc) {
    newDoc.DocumentTypeID(null);
    newDoc.ProductionIDOverride(CurrentProductionID);
    newDoc.ParentID(CurrentProductionID);
    newDoc.ParentTable('Productions');
    newDoc.SystemID(ViewModel.ROProductionListCriteria().SystemID());
    newDoc.ProductionAreaID(ViewModel.ROProductionListCriteria().ProductionAreaID());
};

function SaveDocuments() {
    ViewModel.CallServerMethodPromise("SaveDocuments", { ProductionDocumentList: ViewModel.ProductionDocumentList.Serialise() }).then(data => {
        $('#ProductionDocuments').modal('hide');
    }),
    errorText => {

    }
};

Singular.OnPageLoad(function() {

    Refresh();

});