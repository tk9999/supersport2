﻿FeedBackReportPage = {
  FilterAdHocSettings: function (FullList, ReportSetting) {
    var RSList = [];
    FullList.Iterate(function (Item, index) {
      if (Item.FeedbackReportAdHocTypeID !== null) {
        RSList.push(Item)
      }
    });
    return RSList;
  },
  SaveCurrentReport: function () {
    ViewModel.CallServerMethod('SaveCurrentReport', { CurrentReport: ViewModel.CurrentReport().Serialise() },
      function (data) {
      }, true);
  },
  CreatePrintReport: function () {

    ViewModel.DownloadFile('CreatePrintoutReport',
      { DisplayInd: true, FeedbackReportID: ViewModel.CurrentReport().FeedbackReportID(), ReportName: ViewModel.CurrentReport().ReportName() },
      function (data) { });

  }
}

StudiosFeedBackReportPage = {

  ReportDateSet: function (vm) {
    ViewModel.CallServerMethodPromise("SetupStudioFeedbackReport",
      { ReportDate: vm.ReportDate() })
      .then(data => {
        ViewModel.CurrentReport.Set(data.Report)
        ViewModel.CurrentReportRows(data.Rows)
        Singular.Validation.CheckRules(ViewModel.CurrentReport())
      })
  },

  ReportDateValid: function (Value, Rule, CtlError) {
    if (ViewModel.CurrentReport() === null && ViewModel.ReportDate() === null) {
      CtlError.AddError("Report Date is required")
    }
  },

  EditReportSubSection: function (ReportDetail) {

    ViewModel.CurrentReportDetail.Set(ReportDetail);

    $("#ReportDetail").modal('show');

  },

  SaveCurrentReport: function (studioReport) {
    //ViewModel.CallServerMethodPromise('SaveCurrentStudioReport', 
    //  { CurrentReport: studioReport.Serialise() })
    //  .then((data) => {
    //    KOFormatterFull.Deserialise(data, studioReport)
    //  }),
    //  errorText => { 

    //  };
    StudioFeedbackReportBO.save(studioReport, {
      onSuccess: function (data) {

      },
      onFail: function (errorText) {

      }
    })
  },
  SaveCurrentSubReport: function (feedbackReport) {
    FeedbackReportBO.save(feedbackReport, {
      onSuccess: function (data) {
        StudioFeedbackSectionBO.hideSubReportModal()
      },
      onFail: function (errorText) {

      }
    })
  },

  CreatePrintReport: function () {

    ViewModel.DownloadFile('CreatePrintoutReport',
      { DisplayInd: true, FeedbackReportID: ViewModel.CurrentReport().FeedbackReportID(), ReportName: ViewModel.CurrentReport().ReportName(), ReportDate: ViewModel.CurrentReport().ReportDate() },
      function (data) { });

  },
  FormatBookingTimes: function (bookingStartDateTime, bookingEndDateTime) {
    if (bookingStartDateTime && bookingEndDateTime) {
      var sd = new Date(bookingStartDateTime).format('dd MMM HH:mm');
      var ed = new Date(bookingEndDateTime).format('HH:mm');
      return sd + ' - ' + ed;
    };
    return "";
  },
  GetBookingDate: function (bookingDateTime) {
    if (bookingDateTime) {
      return new Date(bookingDateTime).format('dd-MMM-yy');
    }

    return "";
  }
}