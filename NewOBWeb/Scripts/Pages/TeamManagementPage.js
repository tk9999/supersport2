﻿//#region OnPageLoad
Singular.OnPageLoad(function () {
  ViewModel.HRSelectedList.Set([]);
  ViewModel.SystemAreaShiftPatternList.Set([]);
  ViewModel.CurrentSystemID(ViewModel.CurrentSystemID());
  $("#TeamActions").addClass('FadeDisplay animated slideInRight go');
});
//#endregion

//#region System Management Page
TeamManagementPage = {

  SystemIDSet: function () {
    this.refreshTeams()
    this.fetchSystemAreaShiftPatterns()
    this.refreshTeamNumbers()
  },

  //#region User Actions
  manageTeams: function () {
    ViewModel.TeamManagementSelected(!ViewModel.TeamManagementSelected());
    if (ViewModel.SystemAreaShiftPatternSelected()) {
      ViewModel.ROSystemAreaShiftPatternList.Set([]);
      ViewModel.SystemAreaShiftPatternSelected(false);
      $("#SystemAreaShiftPatternTable").removeClass('FadeDisplay');
      $("#SystemAreaShiftPatternTable").addClass('FadeHide');
    }
    if (ViewModel.TeamNumbersSelected()) {
      ViewModel.TeamNumbersSelected(false);
      $("#TeamNumbers").removeClass('FadeDisplay');
      $("#TeamNumbers").addClass('FadeHide');
    }
    if (ViewModel.TeamManagementSelected()) {
      ViewModel.ROSystemTeamListManager().Refresh();
      $("#SystemTeamTable").removeClass('FadeHide');
      $("#SystemTeamTable").addClass('FadeDisplay animated slideInRight go');
    } else {
      $("#SystemTeamTable").addClass('FadeHide');
      $("#SystemTeamTable").removeClass('FadeDisplay');
    }
  },
  manageSystemAreaShiftPatterns: function () {
    ViewModel.SystemAreaShiftPatternSelected(!ViewModel.SystemAreaShiftPatternSelected());
    if (ViewModel.TeamManagementSelected()) {
      ViewModel.TeamManagementSelected(false);
      $("#SystemTeamTable").addClass('FadeHide');
      $("#SystemTeamTable").removeClass('FadeDisplay');
    }
    if (ViewModel.TeamNumbersSelected()) {
      ViewModel.TeamNumbersSelected(false);
      $("#TeamNumbers").removeClass('FadeDisplay');
      $("#TeamNumbers").addClass('FadeHide');
    }
    if (ViewModel.SystemAreaShiftPatternSelected()) {
      ViewModel.ROSystemAreaShiftPatternListCriteria().FetchTemplates(true);
      ViewModel.ROSystemAreaShiftPatternListCriteria().SystemID(ViewModel.CurrentSystemID());
      ViewModel.ROSystemAreaShiftPatternListCriteria().ProductionAreaID(ViewModel.CurrentProductionAreaID());
      ViewModel.ROSystemAreaShiftPatternListManager().Refresh();
      $("#SystemAreaShiftPatternTable").removeClass('FadeHide');
      $("#SystemAreaShiftPatternTable").addClass('FadeDisplay animated slideInRight go');
    } else {
      $("#SystemAreaShiftPatternTable").removeClass('FadeDisplay');
      $("#SystemAreaShiftPatternTable").addClass('FadeHide');
    }
  },
  manageTeamNumbers: function () {
    ViewModel.TeamNumbersSelected(!ViewModel.TeamNumbersSelected());
    if (ViewModel.TeamManagementSelected()) {
      ViewModel.TeamManagementSelected(false);
      $("#SystemTeamTable").addClass('FadeHide');
      $("#SystemTeamTable").removeClass('FadeDisplay');
    }
    if (ViewModel.SystemAreaShiftPatternSelected()) {
      ViewModel.SystemAreaShiftPatternSelected(false);
      $("#SystemAreaShiftPatternTable").removeClass('FadeDisplay');
      $("#SystemAreaShiftPatternTable").addClass('FadeHide');
    }
    if (ViewModel.TeamNumbersSelected()) {
      $("#TeamNumbers").removeClass('FadeHide');
      $("#TeamNumbers").addClass('FadeDisplay animated slideInRight go');
    } else {
      $("#TeamNumbers").removeClass('FadeDisplay');
      $("#TeamNumbers").addClass('FadeHide');
    }
    this.refreshTeamNumbers()
  },
  //#endregion

  //#region System Teams
  refreshTeams: function () {
    ViewModel.ROSystemTeamListCriteria().SystemID(ViewModel.CurrentSystemID());
    ViewModel.ROSystemTeamListManager().Refresh();
  },
  fetchEditableShiftPatterns: function (obj) {
    Singular.SendCommand("GetTeamShiftPatterns", {
      SystemTeamID: obj.SystemTeamID()
    }, function (response) {
      $('#EditTeamModal').modal();
    });
  },
  addNewTeam: function () {
    Singular.SendCommand("NewSystemTeam", {}, function (response) {
      ViewModel.EditableSystemTeamShiftPattern().SystemID(ViewModel.CurrentSystemID());
      $('#EditTeamModal').modal();
    });
  },
  removeTeam: function (obj) {
    Singular.SendCommand("RemoveTeam", {
      SystemTeamID: obj.SystemTeamID()
    }, function (response) {
      ViewModel.ROSystemTeamListManager().SortColumn('ModifiedDateTime');
      ViewModel.ROSystemTeamListManager().SortAsc(false);
      ViewModel.ROSystemTeamListManager().Refresh();
    });
  },//RemoveTeam
  saveNewTeam: function (obj, optionalArg) {
    optionalArg = (typeof optionalArg === 'undefined') ? false : optionalArg;
    Singular.SendCommand("SaveNewTeam", { SaveAndNew: optionalArg }, function (response) {
      if (response) {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "Team has been saved", 1500, 'bottom-right');
        ViewModel.ROSystemTeamListManager().SortColumn('ModifiedDateTime');
        ViewModel.ROSystemTeamListManager().SortAsc(false);
        ViewModel.ROSystemTeamListManager().Refresh();
      } else {
        OBMisc.Notifications.GritterError("Error Saving Team", "", 3000, 'bottom-right');
      }
      if (response.Data) {
        TeamManagementPage.addNewTeam();
      } else {
        // $('#AddTeamModal').modal('hide');
      }
    });
  },
  getTeamDetailsBrokenRulesHTML: function () {
    var ErrorString = ''
    if (ViewModel.IsValid() == false) {
      ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(ViewModel);
    }
    return ErrorString;
  },
  checkTeamDetailsValidity: function () {
    var valid = true;
    if (!ViewModel.IsValid()) {
      valid = false;
    }
    return valid;
  },
  getEditableShiftPatternBrokenRulesHTML: function () {
    var ErrorString = ''
    if (ViewModel.EditableSystemAreaShiftPattern()) {
      if (ViewModel.EditableSystemAreaShiftPattern().IsValid() == false) {
        ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(ViewModel.EditableSystemAreaShiftPattern());
      }
    }
    return ErrorString;
  },
  checkEditableShiftPatternValidity: function () {
    var valid = true;
    if (ViewModel.EditableSystemAreaShiftPattern()) {
      if (!ViewModel.EditableSystemAreaShiftPattern().IsValid()) {
        valid = false;
      }
    }
    return valid;
  },
  //#endregion

  //#region System Area Shift Patterns
  DelayedRefreshList: function (man) {
    fetchTimeout = setTimeout(function () {
      man.Refresh();
    }, 400);
  },
  refreshSASP: function () {
    ViewModel.EditableSystemTeamShiftPattern().SystemAreaShiftPatternID(null);
    ViewModel.EditableSystemTeamShiftPattern().ShiftDuration(null);
    ViewModel.EditableSystemTeamShiftPattern().PatternName('');
    ViewModel.EditableSystemTeamShiftPattern().StartDate(null);
    ViewModel.EditableSystemTeamShiftPattern().EndDate(null);
    ViewModel.EditableSystemTeamShiftPattern().SystemAreaShiftPatternWeekDayList.Set([]);
  },
  fetchSystemAreaShiftPatterns: function () {
    ViewModel.ROSystemAreaShiftPatternListCriteria().SystemID(ViewModel.CurrentSystemID());
    ViewModel.ROSystemAreaShiftPatternListCriteria().ProductionAreaID(ViewModel.CurrentProductionAreaID());
    ViewModel.ROSystemAreaShiftPatternListCriteria().FetchTemplates(true);
    ViewModel.ROSystemAreaShiftPatternListManager().Refresh();
  },
  addSASP: function () {
    Singular.SendCommand("GetSystemProductionArea", {}, function (response) {
      if (response.Success) {
        $('#AddSystemAreaShiftPatternModal').modal();
      }
    });
  },
  addSASPWeekDay: function () {
    var newSASPWD = ViewModel.EditableSystemAreaShiftPattern().SystemAreaShiftPatternWeekDayList.AddNew();
  },
  addSASPWeekDayNonTemplate: function () {
    var newSASPWD = ViewModel.EditableSystemTeamShiftPattern().SystemAreaShiftPatternWeekDayList.AddNew();
    ViewModel.TemplateUpDate(false);
  },
  removeSASPWeekDay: function (obj) {
    var ThisSASPWD = ViewModel.EditableSystemAreaShiftPattern().SystemAreaShiftPatternWeekDayList().Find("Guid", obj.Guid());
    if (!ThisSASPWD.SystemAreaShiftPatternWeekDayID()) {
      ViewModel.EditableSystemAreaShiftPattern().SystemAreaShiftPatternWeekDayList.Remove(ThisSASPWD);
    } else {
      Singular.SendCommand("RemoveSASPWeekDay", { SystemAreaShiftPatternWeekDayID: obj.SystemAreaShiftPatternWeekDayID() }, function (response) {
      });
    }
  },
  removeSASPWeekDayNonTemplate: function (obj) {
    var ThisSASPWD = ViewModel.EditableSystemTeamShiftPattern().SystemAreaShiftPatternWeekDayList().Find("Guid", obj.Guid());
    if (ThisSASPWD) {
      ViewModel.EditableSystemTeamShiftPattern().SystemAreaShiftPatternWeekDayList.Remove(ThisSASPWD);
      if (ThisSASPWD.SystemAreaShiftPatternWeekDayID() != undefined) ViewModel.TemplateUpDate(false);
      else ViewModel.TemplateUpDate(true);
    }
  },
  removeSASP: function (obj) {
    Singular.SendCommand("RemoveSystemAreaShiftPattern", { SystemAreaShiftPatternID: obj.SystemAreaShiftPatternID() }, function (response) {
      ViewModel.ROSystemAreaShiftPatternListCriteria().SystemID(ViewModel.CurrentSystemID());
      ViewModel.ROSystemAreaShiftPatternListCriteria().FetchTemplates(true);
      ViewModel.ROSystemAreaShiftPatternListCriteria().ProductionAreaID(ViewModel.CurrentProductionAreaID());
      ViewModel.ROSystemAreaShiftPatternListManager().Refresh();
    });
  },
  editPattern: function (obj) {
    Singular.SendCommand("FetchSASPForEdit", { SystemAreaShiftPatternID: obj.SystemAreaShiftPatternID() }, function (response) {
      if (response) {
        $('#EditSystemAreaShiftPatternModal').modal();
      }
    });
  },
  saveSASPChanges: function (optionalArg) {
    optionalArg = (typeof optionalArg === 'undefined') ? false : optionalArg;
    Singular.SendCommand("SaveSystemAreaShiftPatterns", { SaveAndNew: optionalArg }, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "Shift Pattern has been saved", 1500, 'bottom-right');
      } else {
        OBMisc.Notifications.GritterError("Error Saving Team", response.ErrorText, 3000, 'bottom-right');
      }
    });
  },
  getPatternInfo: function(obj){
    return obj.HRUsingPatternInd() ? '<h3> Pattern Details <br><small><font color = "#FFA500">Shifts Already Generated from Pattern</font></small></h3>' : '<h3> Pattern Details </h3>';
  },
  //#endregion

  //#region Team Discipline Human Resources
  getHRCss: function (TeamDisciplineHumanResource) {
    if (TeamDisciplineHumanResource.HumanResourceID()) {
      return "btn-custom btn-primary TeamDisciplineHumanResource";
    } else {
      return "btn-custom btn-default TeamDisciplineHumanResource";
    }
  },
  bookTeamHR: function (TeamDisciplineHumanResource) {
    if (TeamDisciplineHumanResource.HumanResourceID() != undefined) {
      ViewModel.TeamDisciplineHumanResourceToReplace(TeamDisciplineHumanResource);
    }
    ViewModel.CurrentDisciplineID(TeamDisciplineHumanResource.DisciplineID());
    Singular.ShowLoadingBar();
    Singular.GetDataStateless('OBLib.TeamManagement.ReadOnly.ROSystemTeamSkilledHRList, OBLib', {
      DisciplineID: TeamDisciplineHumanResource.DisciplineID(),
      SystemTeamID: TeamDisciplineHumanResource.GetParent().SystemTeamID(),
      SystemID: TeamDisciplineHumanResource.GetParent().SystemID(),
      HumanResourceIDs: TeamManagementPage.GetHumanResourceXmlIDs()
    },
    function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROSystemTeamSkilledHRList);
        Singular.HideLoadingBar();
        ViewModel.ROSystemTeamSkilledHRList().Iterate(function (itm, ind) {
          var ThisTDHR = ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().Find("HumanResourceID", itm.HumanResourceID());
          if (itm.OnTeamInd() && !ThisTDHR) itm.OnTeamInd(false);
          else if (!itm.OnTeamInd() && ThisTDHR) itm.OnTeamInd(true);
        });
        $('#SystemTeamHumanResourcesModal').modal();
      }
    });
  },
  removeTDHR: function (obj) {
    var ThisTDHR = ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().Find("Guid", obj.Guid());
    if (!ThisTDHR.TeamDisciplineHumanResourceID()) {
      ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList.Remove(ThisTDHR);
      if (ThisTDHR.HumanResourceID()) {
        var index = ViewModel.HRSelectedList.indexOf(ThisTDHR.HumanResourceID());
        if (index > -1) {
          ViewModel.HRSelectedList.splice(index, 1);
        }
      }
    } else {
      Singular.SendCommand("RemoveTDHR", { TeamDisciplineHumanResourceID: obj.TeamDisciplineHumanResourceID() }, function (response) { });
    }
  },
  filterTeamHumanResources: function () {
    Singular.ShowLoadingBar();
    if (ViewModel.ROSystemTeamSkilledHRList().length > 0) {
      if (!ViewModel.Filter().ROHumanResourceFilter() || ViewModel.Filter().ROHumanResourceFilter() == '') {
        ViewModel.ROSystemTeamSkilledHRList().Iterate(function (sk, Index) {
          sk.Visible(true);
        });
      } else {
        ViewModel.ROSystemTeamSkilledHRList().Iterate(function (sk, Index) {
          if (sk.HumanResource().toLowerCase().indexOf(ViewModel.ROHumanResourceFilter().toLowerCase()) >= 0) {
            sk.Visible(true);
          } else {
            sk.Visible(false);
          };
        });
      }
    }
    Singular.HideLoadingBar();
  },
  canSelectHR: function (ROSkilledHR) {
    if (ViewModel.EditableSystemTeamShiftPattern()) {
      var TDHR = ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().Find('HumanResourceID', ROSkilledHR.HumanResourceID());
      if (TDHR) {
        if (TDHR.HasSchedule())
          return false;
        else
          return true;
      } else {
        return true;
      }
    } else
      return true;
  },
  GetHumanResourceXmlIDs: function () {
    return OBMisc.Xml.getXmlIDs(ViewModel.HRSelectedList());
  },
  onTeam: function (BulkTeamHumanResource) {
    BulkTeamHumanResource.OnTeamInd(!BulkTeamHumanResource.OnTeamInd());
    if (BulkTeamHumanResource.OnTeamInd()) {
      // Adding HR to list
      Singular.SendCommand("UpdateTeamHRList", {
        HumanResourceID: BulkTeamHumanResource.HumanResourceID(),
        HumanResource: BulkTeamHumanResource.HumanResource(),
        DisciplineID: ViewModel.CurrentDisciplineID()
      }, function (response) {
        Singular.GetDataStateless('OBLib.TeamManagement.ReadOnly.ROSystemTeamSkilledHRList, OBLib', {
          DisciplineID: response.Item1,
          SystemTeamID: response.Item2,
          SystemID: response.Item3,
          HumanResourceIDs: TeamManagementPage.GetHumanResourceXmlIDs()
        },
          function (args) {
            if (args.Success) {
              KOFormatter.Deserialise(args.Data, ViewModel.ROSystemTeamSkilledHRList);
              Singular.HideLoadingBar();
              ViewModel.ROSystemTeamSkilledHRList().Iterate(function (itm, ind) {
                var ThisTDHR = ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().Find("HumanResourceID", itm.HumanResourceID());
                if (itm.OnTeamInd() && !ThisTDHR) itm.OnTeamInd(false);
                else if (!itm.OnTeamInd() && ThisTDHR) itm.OnTeamInd(true);
              });
              $('#SystemTeamHumanResourcesModal').modal();
            }
          });
      });
    }
    else {
      // Removing HR from list
      var indexID = ViewModel.HRSelectedList.indexOf(BulkTeamHumanResource.HumanResourceID());
      if (indexID > -1) {
        ViewModel.HRSelectedList.splice(indexID, 1);
      }
      var indexHR = 0;
      ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList().Iterate(function (itm, ind) {
        if (itm.HumanResourceID() === BulkTeamHumanResource.HumanResourceID()) {
          indexHR = ind;
          ViewModel.EditableSystemTeamShiftPattern().TeamDisciplineHumanResourceList.Remove(itm);
        }
      });
      if (BulkTeamHumanResource.OnTeamInd()) BulkTeamHumanResource.OnTeamInd(false);
    }
  },
  //#endregion

  //#region Team Shift Patterns
  generateHRShifts: function (obj) {
    Singular.ShowLoadingBar();
    ViewModel.CallServerMethod('PopulateHRSchedule', {
      TeamShiftPatternID: obj.TeamShiftPatternID(),
      HumanResourceID: null,
      SystemTeamID: obj.SystemTeamID()
    },
    function (response) {
      if (response.Success)
        alert('Schedule Generated');
      else
        alert(response.ErrorText);
      Singular.HideLoadingBar();
    });
  },
  generateHRShiftsForIndividual: function (obj) {
    Singular.ShowLoadingBar();
    ViewModel.CallServerMethod('PopulateHRSchedule', {
      TeamShiftPatternID: obj.GetParent().TeamShiftPatternID(),
      HumanResourceID: obj.HumanResourceID(),
      SystemTeamID: obj.GetParent().SystemTeamID()
    },
    function (response) {
      if (response.Success)
        alert('Schedule Generated');
      else
        alert(response.ErrorText);
      Singular.HideLoadingBar();
    });
  },
  //#endregion

  //#region Team Numbers
  refreshTeamNumbers: function () {
    Singular.SendCommand("GetTeamNumbers", {}, function (response) {
    })
  },
  addTeamNumber: function () {
    var newTN = ViewModel.TeamNumberList.AddNew();
    newTN.SystemID(ViewModel.ROUserSystemList()[0].SystemID());
  },
  saveTeamNumbers: function () {
    Singular.SendCommand("SaveTeamNumbers", {}, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Saved Successfully", "Team Number(s) Saved Successfully", 1500)
      } else {
        OBMisc.Notifications.GritterError("Error Saving Team Number(s)", response.ErrorText, 3000)
      }
    });
  }
  //#endregion
}
//#endregion

//#region Modal Triggers On Close
$(document).on('hidden.bs.modal', '#AddTeamModal', function (e) {
  Singular.SendCommand("CleanupNewteam", {}, function (response) {
    ViewModel.ROSystemTeamListManager().Refresh();
    ViewModel.CurrentEditableSystemTeamShiftPatternID(null);
  });
});
$(document).on('hidden.bs.modal', '#EditTeamModal', function (e) {
  ViewModel.ROSystemTeamListManager().Refresh();
  ViewModel.CurrentEditableSystemTeamShiftPatternID(null);
});
$(document).on('hidden.bs.modal', '#SystemTeamHumanResourcesModal', function (e) {
  ViewModel.CurrentTeamDisciplineHumanResource(null);
  ViewModel.TeamDisciplineHumanResourceToReplace(null);
  ViewModel.CurrentDisciplineID(null);
});
$(document).on('hide.bs.modal', '#EditTeamModal', function (e) {
  ViewModel.EditableSystemTeamShiftPattern(null);
});
$(document).on('hidden.bs.modal', '#AddSystemAreaShiftPatternModal', function (e) {
  Singular.SendCommand("RemoveSASPAfterSave", {}, function (response) {
    ViewModel.ROSystemAreaShiftPatternListCriteria().SystemID(ViewModel.CurrentSystemID());
    ViewModel.ROSystemAreaShiftPatternListCriteria().ProductionAreaID(ViewModel.CurrentProductionAreaID());
    ViewModel.ROSystemAreaShiftPatternListCriteria().FetchTemplates(true);
    ViewModel.ROSystemAreaShiftPatternListManager().Refresh();
    ViewModel.HRSelectedList.Set([]);
    ViewModel.EditableSystemAreaShiftPattern(null);
  });
});
$(document).on('hidden.bs.modal', '#EditSystemAreaShiftPatternModal', function (e) {
  ViewModel.ROSystemAreaShiftPatternListCriteria().SystemID(ViewModel.CurrentSystemID());
  ViewModel.ROSystemAreaShiftPatternListCriteria().ProductionAreaID(ViewModel.CurrentProductionAreaID());
  ViewModel.ROSystemAreaShiftPatternListCriteria().FetchTemplates(true);
  ViewModel.ROSystemAreaShiftPatternListManager().Refresh();
  ViewModel.HRSelectedList.Set([]);
  ViewModel.EditableSystemAreaShiftPattern(null);
});
//#endregion

//#region Client Side List Filters

function FilterSystemAllowedAreasSASP() {
  var tempAllowed = [];
  ClientData.ROSystemProductionAreaList.Iterate(function (itm2, ind2) {
    if (itm2.SystemID == ViewModel.CurrentSystemID()) {
      tempAllowed.push(itm2);
    }
  });
  return tempAllowed;
};
function GetAllowedDisciplines(List, Item) {
  var AllowedDisciplines = [];
  var AlreadyInList = false;
  if (List) {
    List.Iterate(function (disc, discIndx) {
      if (disc.SystemID == Item.GetParent().SystemID() && disc.ProductionAreaID == Item.GetParent().ProductionAreaID()) {
        AllowedDisciplines.push(disc)
      }
    })
  }
  return AllowedDisciplines;
};
function FilterProductionAreas(List, Item) {
  var Allowed = [];
  List.Iterate(function (itm, ind) {
    var tempPA = ClientData.ROSystemProductionAreaList.Find('ProductionAreaID', itm.ProductionAreaID);
    if (tempPA) {
      if (tempPA.SystemID == ViewModel.ROSystemAreaShiftPatternListCriteria().SystemID()) Allowed.push(itm);
    }
  });
  return Allowed;
};
function GetAllowedSystems(List, Item) {
  return List;
};

//#endregion

//#region Enums
Systems = {
  ICR: 4,
  PLO: 5
};
//#endregion

ROSystemAreaShiftPatternListCrtieriaBO = {
  SystemIDSet: function (self) {
    ViewModel.ROSystemAreaShiftPatternListManager().Refresh()
  },
  ProductionAreaIDSet: function (self) {
    ViewModel.ROSystemAreaShiftPatternListManager().Refresh()
  }
}