﻿var roomBookingsControl = null

ROChannelBO.IsSelectedSet = function (self) {
  if (!ViewModel.IsBusyRefreshing()) {
    RoomAllocatorPage.refreshActiveTab()
  }
  //if (self.IsSelected()) {
  //  var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
  //  if (!item) {
  //    self.ChannelID()
  //    var ni = new SelectedItemObject()
  //    ni.ID(self.ChannelID())
  //    ni.Description(self.ChannelShortName())
  //    ViewModel.SelectedChannelItems().push(ni)
  //  }
  //}
  //else {
  //  var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
  //  if (item) {
  //    ViewModel.SelectedChannelItems().pop(item)
  //  }
  //}

  //var xmlCriteria = ""
  //if (ViewModel.SelectedChannelItems().length > 0) {
  //  xmlCriteria = '<DataSet>'
  //  ViewModel.SelectedChannelItems().Iterate(function (item, indx) {
  //    xmlCriteria += "<Table ID='" + item.ID().toString() + "' />";
  //  })
  //  xmlCriteria += '</DataSet>'
  //}
  //ViewModel.RoomAllocatorPlayoutCriteria().SelectedChannelIDsXML(xmlCriteria)
}

SchEvtBO = {
  save: function (SchEvt, options) {
    ViewModel.CallServerMethod("SaveSchEvt",
      { SchEvt: SchEvt.Serialise() },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Save Succeeded", response.ErrorText, 250)
          KOFormatter.Deserialise(response.Data, SchEvt)
          $("#RoomScheduleModal").modal('hide')
          if (options && options.onSuccess) {
            window.siteHubManager.changeMade();
            options.onSuccess(response);
          }
        } else {
          if (options && options.onFail) {
            options.onFail(response);
          }
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
        }
      })
  }
}

SchEvtChBO = {
  acknowledgeChanges(self) {
    self.IsProcessing();
    self.ChangeAlert("");
    ViewModel.CallServerMethod("AcknowledgeScheduleChanges",
      { SchEvtCh: self.Serialise() },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess("Acknowledged", "", 250);
          self.IsProcessing(false);
        } else {
          OBMisc.Notifications.GritterError("Acknowledge Failed", response.ErrorText, 1000);
          self.IsProcessing(false);
        }
      })
  },
  getEventStatusCssClass: function (self) {
    if (self.EventStatus() == "L") {
      return 'btn btn-xs btn-block event-live'
    } else if (self.EventStatus() == "D") {
      return 'btn btn-xs btn-block event-delayed'
    } else if (self.EventStatus() == "P") {
      return 'btn btn-xs btn-block event-premier'
    } else {
      return 'btn btn-xs btn-block event-default'
    }
  },
  channelCss: function (obj) {
    return obj.ButtonStyleCssClass() + ' channel'
  },
  canEdit: function (self, fieldName) {
    switch (fieldName) {
      case 'ChannelShortName':
        var evt = self.GetParent()
        var canDrag = true
        evt.RoomSchedList().Iterate(function (rm, rmIndx) {
          rm.RoomSchedChList().Iterate(function (ch, chIndx) {
            if (ch.ScheduleNumber() == self.ScheduleNumber()) {
              canDrag = false
            }
          })
        })
        return canDrag
        break;
      default:
        return true
        break;
    }
  },

  addRoom: function (self, defaultRoom) {
    var roomSchedule = self.GetParent().RoomSchedList.AddNew()
    roomSchedule.IsProcessing(true)
    var parent = self.GetParent()
    roomSchedule.SystemID(5)
    var sd = moment(self.ScheduleDateTime())
    sd = sd.add('hour', -1)
    roomSchedule.CallTime(sd.toDate())
    roomSchedule.StartDateTime(self.ScheduleDateTime())
    roomSchedule.EndDateTime(self.ScheduleEndDate())
    roomSchedule.WrapTime(self.ScheduleEndDate())
    roomSchedule.GenRefNumber(self.GenRefNumber())

    var roomSchedCh = roomSchedule.RoomSchedChList.AddNew()
    roomSchedCh.RoomScheduleID(roomSchedule.RoomScheduleID())
    roomSchedCh.ProductionChannelID(self.ProductionChannelID())
    roomSchedCh.ScheduleNumber(self.ScheduleNumber())
    roomSchedCh.ChannelShortName(self.ChannelShortName())

    var defaultController = RoomSchedBO.addCrew(roomSchedule)
    defaultController.Discipline("SCCR Controller")
    defaultController.DisciplineID(294)
    defaultController.CallTime(roomSchedule.CallTime())
    defaultController.StartDateTime(roomSchedule.StartDateTime())
    defaultController.EndDateTime(roomSchedule.EndDateTime())
    defaultController.WrapTime(roomSchedule.WrapTime())
    roomSchedule.IsProcessing(false)

    //if (defaultRoom) {
    //  roomSchedule.Room(defaultRoom.Room)
    //  roomSchedule.RoomID(defaultRoom.RoomID)
    //  ViewModel.CurrentSchEvtChRm().Room(defaultRoom.Room)
    //  ViewModel.CurrentSchEvtChRm().RoomID(defaultRoom.RoomID)
    //}

    //roomSchedule.ChannelCount(1)
    Singular.Validation.CheckRules(roomSchedule)
    RoomSchedBO.editRoomSchedule(roomSchedule, defaultRoom)
    RoomAllocatorPage.addDragDrop()

    //return roomSchedule
  },
  removeRoomBooking: function (self, roomBooking) {
    self.GetParent().IsProcessing(true);
    ViewModel.CallServerMethod("DeleteSchEvt", {
      RoomSched: roomBooking.Serialise()
    }, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Delete Succeeded", "", 250);
        self.RoomSchedList.RemoveNoCheck(roomBooking);
        window.siteHubManager.changeMade();
      } else {
        OBMisc.Notifications.GritterError("Delete Failed", response.ErrorText, 1000)
      }
    })
  },

  onRoomSelected: function (selectedItem, businessObject) {
    SchEvtChBO.quickAddRoom(businessObject, selectedItem)
  },
  triggerRoomIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = 5
    args.Data.ProductionAreaID = null
    args.Data.StartDateTime = new Date(args.Object.ScheduleDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.ScheduleEndDate()).format("dd MMM yyyy HH:mm")
  },
  afterRoomIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  RoomIDSet: function (self) { },
  quickAddRoom: function (channelObj, selectedRoom) {
    var genref = channelObj.GetParent()
    var channelAlreadyAllocated = null

    //check if is my channel
    //var isMyChannel = roomSchedule.RoomSchedChList().Find("ScheduleNumber", channelObj.ScheduleNumber())

    //find in other roomschedules
    genref.RoomSchedList().Iterate(function (rs, rsIndx) {
      var ch = rs.RoomSchedChList().Find("ScheduleNumber", channelObj.ScheduleNumber())
      if (ch) {
        channelAlreadyAllocated = ch
      }
    })

    //channel not added to any room schedule
    if (!channelAlreadyAllocated) {
      //create a room schedule
      SchEvtChBO.addRoom(channelObj, selectedRoom)
    }
  }
};

RoomSchedBO = {
  addCrew: function (self) {
    var newCrewMember = self.SchEvtChRmHrList.AddNew()
    newCrewMember.CallTime(self.CallTime())
    newCrewMember.StartDateTime(self.StartDateTime())
    newCrewMember.EndDateTime(self.EndDateTime())
    newCrewMember.WrapTime(self.WrapTime())
    Singular.Validation.CheckRules(newCrewMember)
    return newCrewMember
  },
  addChannel: function (roomSched, channelObj) {

  },
  removeCrewMember: function (self, crewMember) {
    ViewModel.CallServerMethod("DeleteSchEvtChRmHr", {
      SchEvtChRmHr: crewMember.Serialise()
    }, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
        self.SchEvtChRmHrList.RemoveNoCheck(crewMember);
        window.siteHubManager.updateHumanResourceStats(crewMember.HumanResourceID(), crewMember.CallTime());
        window.siteHubManager.changeMade();
        //window.siteHubManager.notifyHumanResourceStatsUpdate(crewMember);
      } else {
        OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
      }
    })
  },
  removeChannel: function (self, channel) {
    var chCount = self.RoomSchedChList().length
    self.RoomSchedChList.RemoveNoCheck(channel)
    //self.ChannelCount(chCount - 1)
    Singular.Validation.CheckRules(self.GetParent())
    RoomAllocatorPage.addDragDrop()
  },
  editRoomSchedule: function (self, defaultRoom) {
    window.defaultRoom = defaultRoom
    ViewModel.CurrentSchEvtChRm(self)
    var isOpen = $('#RoomScheduleModal').hasClass('in')
    if (!isOpen) {
      $("#RoomScheduleModal").modal()
    } else {
      RoomSchedBO.setupDefaultRoom()
    }
  },
  roomBookingButtonCss: function (self) {
    if (self.IsValid()) {
      return 'btn btn-xs btn-default dropdown-toggle'
    } else {
      return 'btn btn-xs btn-danger dropdown-toggle animated pulse infinite fast'
    }
  },

  CallTimeSet: function (self) {
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self)
    }
  },
  StartTimeSet: function (self) {
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self)
    }
  },
  EndTimeSet: function (self) {
    self.WrapTime(self.EndDateTime());
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self)
    }
  },
  WrapTimeSet: function (self) {
    RoomSchedBO.updateClashes(self)
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self)
    }
  },

  RoomIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.ClashCount() > 0) {
      obj.ClashList().Iterate(function (cls, clsIndx) {
        CtlError.AddError(cls)
      })
    }
  },
  ChannelCountValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.RoomSchedChList().length == 0) {
      CtlError.AddError("Channel count cannot be 0")
    }
  },

  onRoomSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaID(selectedItem.OwningProductionAreaID)
      businessObject.ResourceID(selectedItem.ResourceID)
    } else {
      businessObject.ProductionAreaID(null)
      businessObject.ResourceID(null)
    }
    if (!businessObject.IsProcessing()) {
      RoomSchedBO.updateClashes(businessObject)
    }
  },
  triggerRoomIDAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
  },
  afterRoomIDRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  RoomIDSet: function (self) { },

  canEdit: function (self, fieldName) {
    switch (fieldName) {
      case 'SaveButton':
        return self.IsValid()
        break;
      case 'roomBookingNode':
        return !self.IsProcessing()
      default:
        return true
        break;
    }
  },
  save: function (SchEvtChRm) {
    //var channel = SchEvtChRm.GetParent()
    var evt = SchEvtChRm.GetParent()
    evt.IsProcessing(true);
    SchEvtChRm.IsProcessing(true);
    SchEvtBO.save(evt, {
      onSuccess: function (response) {
        evt.IsProcessing(false);
        KOFormatterFull.Deserialise(response.Data, evt);
        evt.RoomSchedList().Iterate(function (roomSched, rsidx) {
          roomSched.SchEvtChRmHrList().Iterate(function (hrSched, hrIdx) {
            window.siteHubManager.updateHumanResourceStats(hrSched.HumanResourceID(), hrSched.CallTime());
            window.siteHubManager.changeMade();
            //window.siteHubManager.notifyHumanResourceStatsUpdate(hrSched);
          });
        });
        SchEvtChRm.IsProcessing(false);
        //SchEvtChRm.Set(response.Data)
      },
      onFail: function (response) {
        if (response.ErrorText == "Errors were found on the room bookings") {
          KOFormatterFull.Deserialise(response.Data, evt)
          Singular.Validation.CheckRules(evt)
        }
      }
    })
  },
  updateClashes: function (SchEvtChRm) {
    if (SchEvtChRm.StartDateTime() && SchEvtChRm.EndDateTime()) {
      SchEvtChRm.IsProcessing(true)
      ViewModel.CallServerMethod("UpdateRmClashes",
        { SchEvtChRm: SchEvtChRm.Serialise() },
        function (response) {
          if (response.Success) {
            KOFormatter.Deserialise(response.Data, SchEvtChRm, true)
            Singular.Validation.CheckRules(SchEvtChRm)
            SchEvtChRm.IsProcessing(false)
            //OBMisc.Notifications.GritterSuccess("Save Succeeded", response.ErrorText, 250)
          } else {
            OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
          }
        })
    }
  },

  setupDefaultRoom: function () {
    if (window.defaultRoom) {
      ViewModel.CurrentSchEvtChRm().ProductionAreaID(window.defaultRoom.OwningProductionAreaID)
      ViewModel.CurrentSchEvtChRm().Room(window.defaultRoom.Room)
      ViewModel.CurrentSchEvtChRm().RoomID(window.defaultRoom.RoomID)
    }
    window.defaultRoom = null
  }
}

RoomSchedChanBO = {
  canEdit: function (self, fieldName) {
    switch (fieldName) {
      case 'roomChannelNode':
        return !self.GetParent().IsProcessing()
      default:
        return true
        break;
    }
  }
}

SchEvtChRmHrBO = {
  crewButtonText: function (crewMember) {
    //console.log(crewMember)
    return (crewMember.HRName() + ': ' + crewMember.Discipline() + ', ' + crewMember.BookingTimes())
  },
  bookingsTimesButtonCss: function (crewMember) {
    if (crewMember.BookingTimes().length == 0) {
      return 'btn btn-sm btn-danger btn-block'
    }
    return 'btn btn-sm btn-default btn-block'
  },
  updateBookingTimes: function (crewMember) {
    var st = ""
    var et = ""
    if (crewMember.CallTime()) {
      st = new Date(crewMember.CallTime()).format('HH:mm')
      //crewMember.StartDateTime(crewMember.CallTime());
    } else {
      if (crewMember.StartDateTime()) {
        st = new Date(crewMember.StartDateTime()).format('HH:mm')
      }
    }
    if (crewMember.WrapTime()) {
      et = new Date(crewMember.WrapTime()).format('HH:mm')
    } else {
      if (crewMember.EndDateTime()) {
        crewMember.WrapTime(crewMember.EndDateTime())
        et = new Date(crewMember.EndDateTime()).format('HH:mm')
      }
    }
    crewMember.BookingTimes(st + ' - ' + et)
  },

  CallTimeSet: function (self) {
    SchEvtChRmHrBO.updateBookingTimes(self)
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self.GetParent())
    }
  },
  StartTimeSet: function (self) {
    SchEvtChRmHrBO.updateBookingTimes(self)
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self.GetParent())
    }
  },
  EndTimeSet: function (self) {
    self.WrapTime(self.EndDateTime())
    SchEvtChRmHrBO.updateBookingTimes(self)
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self.GetParent())
    }
  },
  WrapTimeSet: function (self) {
    SchEvtChRmHrBO.updateBookingTimes(self)
    if (!self.IsProcessing()) {
      RoomSchedBO.updateClashes(self.GetParent())
    }
  },

  HumanResourceIDValid: function (Value, Rule, CtlError) {
    var obj = CtlError.Object
    if (obj.ClashCount() > 0) {
      obj.ClashList().Iterate(function (cls, clsIndx) {
        CtlError.AddError(cls)
      })
    }
  },

  onHRSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.HumanResourceShiftID(selectedItem.HumanResourceShiftID)
      businessObject.ResourceID(selectedItem.ResourceID)
      //businessObject.ShiftTypeID(selectedItem.ShiftTypeID)
    } else {
      businessObject.HumanResourceShiftID(null)
      businessObject.HumanResourceID(null)
      businessObject.ResourceID(null)
    }
    if (!businessObject.GetParent().IsProcessing()) {
      RoomSchedBO.updateClashes(businessObject.GetParent())
    }
  },
  triggerHRAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setHRCriteriaBeforeRefresh: function (args) {
    //args.Data.SystemID = args.Object.SystemID()
    //args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.StartDateTime = new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
  },
  afterHRRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  HumanResourceIDSet: function (self) { },

  triggerDisciplineAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setDisciplineCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
  },
  afterDisciplineRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onDisciplineSelected: function (obj) { },
  DisciplineIDSet: function (self) { },

  canEdit: function (self, fieldName) {
    switch (fieldName) {
      case 'roomSchedHRNode':
        return !self.GetParent().GetParent().IsProcessing()
        break;
      case 'HumanResourceID':
        return self.IsNew()
        break;
      default:
        return true
        break;
    }
  }
}

//defaultChannels = [1, 191, 192, 22, 23, 24, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 62, 63, 66, 68, 70, 71, 72, 73, 76, 77, 78, 79, 198, 195, 199]
var defaultChannels = [1, 22, 23, 24, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 62, 63, 66, 68, 69, 70, 71, 72, 73, 76, 77, 78, 79, 191, 192, 195, 196, 198, 199, 200, 65]

RoomAllocatorPage = {
  selectAllChannels: function (self, mustSelect) {
    ViewModel.FreezeSetExpressions(true)
    self.PrimaryChannelsOnly(false)
    ViewModel.ROChannelList().Iterate(function (ch, chInd) {
      ch.IsSelected(mustSelect)
    });
    ViewModel.FreezeSetExpressions(false)
    RoomAllocatorPage.refreshActiveTab()
  },
  selectDefaultChannels: function () {
    ViewModel.FreezeSetExpressions(true)
    ViewModel.ROChannelList().Iterate(function (ch, chInd) {
      ch.IsSelected(ch.IsDefault())
      ch.IsSelected(defaultChannels.indexOf(ch.ChannelID()) > -1);
    });
    ViewModel.FreezeSetExpressions(false)
    RoomAllocatorPage.refreshActiveTab()
  },
  AddChannel: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.SelectedChannels().push(obj.ChannelID());
    }
    else {
      var index = ViewModel.SelectedChannels().indexOf(obj.ChannelID());
      if (index > -1) {
        ViewModel.SelectedChannels().splice(index, 1);
      }
    }
    ViewModel.AllocatorChannelPlayoutCriteria().SelectedChannelIDsXML(OBMisc.Xml.getXmlIDs(ViewModel.SelectedChannels()))
  },
  EventDetails: function () {
    if (ViewModel.CurrentSynergyEventPlayout()) {
      return 'Channel Allocation ' + '<small>(' + ViewModel.CurrentSynergyEventPlayout().GenreSeries() + ')</small>'
    }
  },
  disableAutoDatePicker: function (e) {
    $("#txtDate").datepicker({
      beforeShow: function (input, inst) {
        return false;
      }
    })
  },
  showDatePicker: function (e) {
    $("#txtDate").datepicker({
      beforeShow: function (input, inst) {
        return true;
      }
    })
    $("#txtDate").datepicker().datepicker("show")
    //var focused = document.activeElement;
  },
  Refresh: function () {
    ViewModel.RoomAllocatorPlayoutListManager().Refresh()
  },
  addDragDrop: function () {
    $("div.channel:not([disabled])").draggable({
      cancel: false,
      revert: true,
      helper: "clone",
      //containment: $("div.channel").parents("tr.event-row"),
      scroll: false,
      start: function (event, ui) {
        if (event.currentTarget) {
          var isDisabled = event.currentTarget.attributes.getNamedItem('disabled')
          if (isDisabled) {
            event.preventDefault()
          }
        }
      }
    })
    $("div.room-booking").droppable({
      drop: function (event, ui) {
        var channel = ko.dataFor(ui.draggable[0])
        var roomSchedule = ko.dataFor(this)
        var roomSchedCh = null
        if (roomSchedule.GenRefNumber() == 0) {
          roomSchedule.GenRefNumber(roomSchedule.GetParent().GenRefNumber())
        }
        roomSchedCh = roomSchedule.RoomSchedChList().Find("ScheduleNumber", channel.ScheduleNumber())
        if (roomSchedCh == null && (roomSchedule.GenRefNumber() == channel.GenRefNumber())) {
          roomSchedCh = roomSchedule.RoomSchedChList.AddNew()
          roomSchedCh.RoomScheduleID(roomSchedule.RoomScheduleID())
          roomSchedCh.ProductionChannelID(channel.ProductionChannelID())
          roomSchedCh.ScheduleNumber(channel.ScheduleNumber())
          roomSchedCh.ChannelShortName(channel.ChannelShortName())
        }
        //roomSchedule.ChannelCount(1)
        Singular.Validation.CheckRules(roomSchedule)
      }
    })
  },
  ChannelCSS: function (obj) {
    return obj.ScheduleCssClass() == 'event-row-deleted' ? 'animated pulse infinite fast' : '';
  },
  resultBadgeHTML: function () {

  },
  showResultHistory: function () {

  },
  showChannelSelector: function () {
    if ($("#ChannelSelectPannel").hasClass("flipInX go")) {
      $("#ChannelSelectPannel").removeClass("flipInX go")
      $("#ChannelSelectPannel").addClass("flipOutX go")
      window.setTimeout(function () {
        $("#ChannelSelectPannel").css('display', 'none')
      }, 750)
    }
    else {
      $("#ChannelSelectPannel").removeClass("flipOutX go")
      $("#ChannelSelectPannel").css('display', 'block')
      $("#ChannelSelectPannel").addClass("flipInX go")
    }
  },
  showRoomChooser: function (RoomAllocator, element) {
    console.log(RoomAllocator)
    console.log(element)
  },
  showAllocatorChannelFilters: function () {
    $("#ChannelEventFilters").modal()
  },
  refreshChannelAllocator: function () {
    this.refreshActiveTab()
  },
  getSlugs: function (AllocatorChannelEvent) {
    AllocatorChannelEvent.IsProcessing(true)
    OBMisc.Slugs.getSlugs(AllocatorChannelEvent.GenRefNumber(),
      function (response) {
        AllocatorChannelEvent.SlugItemList.Set(response.Data)
        AllocatorChannelEvent.IsProcessing(false)
      },
      function (response) {
        OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000, 'bottom-right')
        AllocatorChannelEvent.IsProcessing(false)
      })
  },
  getSlugsSynergyEvent: function () {
    ViewModel.CurrentSynergyEventPlayout().IsProcessing(true)
    OBMisc.Slugs.getSlugs(ViewModel.CurrentSynergyEventPlayout().GenRefNumber(),
      function (response) {
        ViewModel.CurrentSynergyEventPlayout().SlugItemList.Set(response.Data)
        ViewModel.CurrentSynergyEventPlayout().IsProcessing(false)
      },
      function (response) {
        OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000, 'bottom-right')
        ViewModel.CurrentSynergyEventPlayout().IsProcessing(false)
      })
  },
  showAllocatorChannelModal: function () {
    $("#CurrentAllocatorChannelEventModal").off()
    $("#CurrentAllocatorChannelEventModal").on("shown.bs.modal", function () {
      if (!roomBookingsControl) {
        roomBookingsControl = new ROResourceBookingsControl({ excludeHeader: true }, "RoomBookingsTimeline", "bookingsDatePicker", null)
      }
      var roomResourceIDsXml = OBMisc.Xml.getXmlIDs(AllocatorChannelEventBO.GetMyRooms(), "ResourceID")
      roomBookingsControl.criteria.resourceIDsXML = roomResourceIDsXml
      var allocatorCriteria = ViewModel.AllocatorChannelPlayoutCriteria()
      roomBookingsControl.criteria.startdate = moment(new Date(allocatorCriteria.StartDate())).add(-1, 'days').startOf('day').toDate();
      roomBookingsControl.criteria.enddate = moment(new Date(allocatorCriteria.EndDate())).add(1, 'days').endOf('day').toDate();
      roomBookingsControl.refresh()
      OBMisc.UI.activateTab("ScheduleEvent")
    })
    $("#CurrentAllocatorChannelEventModal").on("hide.bs.modal", function () {
    })
  },
  getSelectedChannelXml: function () {
    //channel selection
    var selectedChannelIDs = []
    ViewModel.ROChannelList().Iterate(function (ch, chInd) {
      if (ch.IsSelected()) { selectedChannelIDs.push(ch.ChannelID()) }
    })
    return OBMisc.Xml.getXmlIDs(selectedChannelIDs)
  },
  currentSynergyEvent: function () {
    if (ViewModel.CurrentSynergyEventGuid()) {
      return ViewModel.SynergyEventPlayoutList().Find("Guid", ViewModel.CurrentSynergyEventGuid())
    }
    return null;
  },
  currentRoomAllocator: function () {
    if (ViewModel.CurrentRoomAllocatorGuid()) {
      thisRoomAllocator = ViewModel.RoomAllocatorPlayoutList().Find("Guid", ViewModel.CurrentRoomAllocatorGuid())
      return thisRoomAllocator
    }
    return null;
  },
  currentSynergySchedulePlayout: function () {
    if (ViewModel.CurrentSynergyScheduleGuid()) {
      return ViewModel.SynergySchedulePlayoutList().Find("Guid", ViewModel.CurrentSynergyScheduleGuid())
    }
    return null;
  },
  showSynergyEventTimes: function (synergyEvent, buttonElement) {
    var me = this
    ViewModel.CurrentSynergyEventGuid(synergyEvent.Guid())
    $("#SynergyEventModal").off('shown.bs.modal')
    $("#SynergyEventModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(me.currentSynergyEvent())
    })
    $("#SynergyEventModal").modal()
    //this.showCurrentSynergyEventTimesPopup(synergyEvent, buttonElement)
  },
  showCurrentSynergyEventTimesPopup: function (synergyEvent, buttonElement) {
    //$("#EditTimesPanelSynergyEvent").draggable()
    $("#EditTimesPanelSynergyEvent").show()
    var dimensionProperties = buttonElement.getBoundingClientRect()
    var elem = document.getElementById("EditTimesPanelSynergyEvent")
    elem.style.display = "block"
    elem.style.position = "absolute"
    elem.style.top = dimensionProperties.top.toString() + "px"
    elem.style.left = dimensionProperties.left.toString() + "px"
    //synergyEvent.CallTime.BoundElements[0].focus()
  },
  doneEditingTimes: function (synergyEvent, buttonElement) {
    ViewModel.CurrentSynergyEventGuid(null)
    this.hideCurrentSynergyEventTimesPopup(synergyEvent, buttonElement)
  },
  hideCurrentSynergyEventTimesPopup: function (synergyEvent, doneButtonElement) {
    var elem = document.getElementById("EditTimesPanelSynergyEvent")
    elem.style.display = "none"
  },
  showRoomAllocatorEventTimes: function (roomAllocator, buttonElement) {
    var me = this
    ViewModel.CurrentRoomAllocatorGuid(roomAllocator.Guid())
    $("#RoomAllocatorModal").off('shown.bs.modal')
    $("#RoomAllocatorModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(me.currentRoomAllocator())
    })
    $("#RoomAllocatorModal").modal()
  },

  //New
  refreshActiveTab: function () {
    var activeTab = $('#ViewTabs .active'),
      activeTabID = null
    if (activeTab) {
      activeTabID = activeTab.find(' > a').attr('href')
    }
    if (activeTabID) {
      ViewModel.IsBusyRefreshing(true)
      switch (activeTabID) {
        case "#GraphicalView":
          RoomAllocatorPage.refreshGraphical()
          break;
        case "#GridViewByChannel":
          RoomAllocatorPage.refreshGridByChannel()
          break;
        case "#GridViewByChannel2":
          RoomAllocatorPage.refreshGridByChannel2()
          break;
      }
    }
  },
  refreshGraphical: function () {
    if (window.gTimeline) {
      window.gTimeline.refresh()
    }
  },
  refreshGridByChannel: function () {
    var channelIDXml = this.getSelectedChannelXml()
    var allocatorCriteria = ViewModel.AllocatorChannelPlayoutCriteria()
    ViewModel.SchEvtListCriteria().StartDate(allocatorCriteria.StartDate())
    ViewModel.SchEvtListCriteria().EndDate(allocatorCriteria.EndDate())
    ViewModel.SchEvtListCriteria().SelectedChannelIDsXML(channelIDXml)
    ViewModel.SchEvtListCriteria().SystemID(allocatorCriteria.SystemID())
    ViewModel.SchEvtListCriteria().ProductionAreaID(allocatorCriteria.ProductionAreaID())
    ViewModel.SchEvtListCriteria().Live(allocatorCriteria.Live())
    ViewModel.SchEvtListCriteria().Delayed(allocatorCriteria.Delayed())
    ViewModel.SchEvtListCriteria().Premier(allocatorCriteria.Premier())
    ViewModel.SchEvtListCriteria().GenRefNo(allocatorCriteria.GenRefNo())
    ViewModel.SchEvtListCriteria().Genre(allocatorCriteria.Genre())
    ViewModel.SchEvtListCriteria().Series(allocatorCriteria.Series())
    ViewModel.SchEvtListCriteria().Title(allocatorCriteria.Title())
    ViewModel.SchEvtListCriteria().GenRefNoString(allocatorCriteria.GenRefNoString())
    ViewModel.SchEvtListCriteria().Keyword(allocatorCriteria.Keyword())
    ViewModel.SchEvtListManager().Refresh()
    ViewModel.SchEvtListManager().afterRefresh = function () {
      RoomAllocatorPage.addDragDrop()
    }
  },

  showFilters: function () {
    $("#SynergyFilterModal").modal()
  },
  selectedDatesHTML: function (crit) {
    return new Date(crit.StartDate()).format('ddd dd MMM') + ' - ' + new Date(crit.EndDate()).format('ddd dd MMM')
  },
  refreshSynergyChanges: function () {
    ViewModel.ROSynergyChangePagedListManager().Refresh()
  }
}

//#region Graphical Control

var gAllocator = function () {
  var me = this;

  me.timelineElement = document.getElementById("timeline");
  me.timelineOptions = {}
  me.resources = null;
  me.resourceBookings = null;
  me.visTimeline = null;
  me.isBusy = false;
  me.currentItem = null;

  me.setupTimeline()

}

gAllocator.prototype.setupTimeline = function () {
  var me = this;
  me.resources = new vis.DataSet();
  me.resourceBookings = new vis.DataSet();

  var allocatorCriteria = ViewModel.AllocatorChannelPlayoutCriteria()
  var sd = moment(allocatorCriteria.StartDate()).startOf('day').toDate(),
    ed = moment(allocatorCriteria.EndDate()).endOf('day').toDate();
  var min = moment(sd).add(-2, 'days').toDate(),
    max = moment(ed).add(2, 'days').toDate();

  me.timelineOptions = {
    editable: false,
    selectable: true,
    multiselect: false,
    orientation: 'top',
    showCurrentTime: true,
    //maxHeight: '80vh',
    //showCustomTime: false,
    clickToUse: false,
    stack: true,
    start: sd,
    end: ed,
    min: min,
    max: max,
    zoomMin: 1000 * 60 * 60,             // 1 hour
    zoomMax: 1000 * 60 * 60 * 24 * 7 * 31,     // 1 month
    autoResize: true,
    type: 'range',
    moveable: true,
    margin: { item: 0 },
    width: '100%',
    order: function (booking1, booking2) {
      if (booking1.start > booking2.start) { return 1 }
      else if (booking1.start < booking2.start) { return -1 }
      else { return 0 };
    },
    groupOrder: function (channel1, channel2) {
      return (channel1.Channel.Priority - channel2.Channel.Priority);
    },
    onAdd: function (item, callback) {
      callback(null)
    },
    onUpdate: function (item, callback) {
      callback(null)
    },
    onMoving: function (item, callback) {
      callback(null)
    },
    onMove: function (item, callback) {
      callback(null)
    },
    onRemove: function (item, callback) {
      callback(null)
    },
    snap: function (date, scale, step) {
      var minute = 60 * 1000;
      return Math.round(date / minute) * minute;
    },
    dataAttributes: ['resourceid', 'resourcebookingid']
  };

  me.visTimeline = new vis.Timeline(me.timelineElement);
  me.visTimeline.setOptions(me.timelineOptions);

  me.visTimeline.setGroups(me.resources);
  me.visTimeline.setItems(me.resourceBookings);

  me.visTimeline.on('rangechanged', _rangeChangedInternal)
  me.visTimeline.on('itemSelected', _itemSelectedInternal)
  me.visTimeline.on('itemDeSelected', _itemDeSelectedInternal)

  function _rangeChangedInternal(props) {
    if (!me.isBusy) {
      ViewModel.FreezeSetExpressions(true)
      ViewModel.AllocatorChannelPlayoutCriteria().StartDate(props.start)
      ViewModel.AllocatorChannelPlayoutCriteria().EndDate(props.end)
      ViewModel.FreezeSetExpressions(false)
      var lowerBound = me.timelineOptions.min
      var upperBound = me.timelineOptions.max
      var isToocloseToLowerBound = (moment(lowerBound).diff(props.start, 'days', true) >= 0 ? true : false)
      var isTooCloseToUpperBound = (moment(upperBound).diff(props.end, 'days', true) <= 0 ? true : false)
      if (isToocloseToLowerBound || isTooCloseToUpperBound) {
        me.onBoundsReached(props)
      }
    }
  }

  function _itemSelectedInternal(properties) {
    ViewModel.CurrentSynergyEventPlayout(null)
    var item = me.resourceBookings.get(properties.items[0])
    me.currentItem = item.Booking.GenRefNumber
    var mSystemID = ViewModel.RoomAllocatorPlayoutCriteria().SystemID()
    var mProductionAreaID = ViewModel.RoomAllocatorPlayoutCriteria().ProductionAreaID()
    ViewModel.CallServerMethod("GetSynergyEventPlayout", {
      StartDateTime: new Date(item.Booking.ScheduleDateTime).format('dd MMM yyyy HH:mm'),
      EndDateTime: new Date(item.Booking.ScheduleEndDate).format('dd MMM yyyy HH:mm'),
      GenRefNumber: item.Booking.GenRefNumber,
      SystemID: mSystemID,
      ProductionAreaID: mProductionAreaID,
      ImportedEventID: item.Booking.ImportedEventID,
      ImportedEventChannelID: item.Booking.ImportedEventChannelID
    },
      function (response) {
        if (response.Success) {
          ViewModel.CurrentSynergyEventChannelPlayoutList.Set([])
          ViewModel.CurrentSynergyEventPlayout.Set(response.Data)
          response.Data.SynergyEventChannelPlayoutList.Iterate(function (itm, ind) {
            var temp = ViewModel.CurrentSynergyEventChannelPlayoutList.AddNew()
            KOFormatter.Deserialise(itm, temp)
          })
          ViewModel.CurrentSynergyEventChannelPlayoutList().Iterate(function (itm, ind) {
            itm.SystemID(ViewModel.CurrentSynergyEventPlayout().SystemID()) //Playout Operations
            if (!itm.Room().length > 0) {
              itm.ProductionAreaID(12) //SCCR Randburg
            } else {
              itm.ProductionAreaID(ViewModel.CurrentSynergyEventPlayout().ProductionAreaID())
            }
            //var sccrctrlid = itm.ResourceIDSCCROperator()
            //var sccrctrl = itm.SCCROperator()
            //itm.ResourceIDSCCROperator(sccrctrlid)
            //itm.SCCROperator(sccrctrl)
          })
          Singular.Validation.CheckRules(ViewModel.CurrentSynergyEventPlayout())
          ViewModel.CurrentSynergyEventChannelPlayoutList().Iterate(function (itm, ind) {
            Singular.Validation.CheckRules(itm)
          })
          RoomAllocatorPage.showAllocatorChannelModal();
          RoomAllocatorPage.getSlugsSynergyEvent();
          $("#CurrentAllocatorChannelEventModal").modal();
        } else {
          OBMisc.Notifications.GritterError("Error Fetching Bookings", response.ErrorText, 3000, 'bottom-right')
        }
      })
  }

  function _itemDeSelectedInternal(properties) {
    console.log(properties)
  }

  //busy div
  me.busyDiv = document.createElement('div');
  me.busyDiv.className = 'loading-custom';

  //busy icon
  me.busyDivIcon = document.createElement('i');
  me.busyDivIcon.className = 'fa fa-refresh fa-5x fa-spin';
  me.busyDiv.appendChild(me.busyDivIcon);
  me.timelineElement.appendChild(me.busyDiv)

  me.getData(min, max)

}

gAllocator.prototype.getSelectedChannelXmlIDs = function () {
  //channel selection
  var selectedChannelIDs = []
  ViewModel.ROChannelList().Iterate(function (ch, chInd) {
    if (ch.IsSelected()) { selectedChannelIDs.push(ch.ChannelID()) }
  })
  return OBMisc.Xml.getXmlIDs(selectedChannelIDs)
};

gAllocator.prototype.onBoundsReached = function (props) {
  var me = this;

  //calculate the new date ranges
  me.timelineOptions.start = props.start
  me.timelineOptions.end = props.end
  var newMin = me.timelineOptions.start
  var newMax = me.timelineOptions.end
  newMin = moment(newMin).add(-2, 'days').toDate()
  newMax = moment(newMax).add(2, 'days').toDate()

  me.timelineOptions.min = newMin
  me.timelineOptions.max = newMax

  me.visTimeline.setOptions({
    start: me.timelineOptions.start,
    end: me.timelineOptions.end,
    min: me.timelineOptions.min,
    max: me.timelineOptions.max
  })
  ViewModel.FreezeSetExpressions(true)
  ViewModel.AllocatorChannelPlayoutCriteria().StartDate(me.timelineOptions.start)
  ViewModel.AllocatorChannelPlayoutCriteria().EndDate(me.timelineOptions.end)
  ViewModel.FreezeSetExpressions(false)

  me.getData(newMin, newMax)

};

gAllocator.prototype.refresh = function () {
  var me = this
  me.getData(me.timelineOptions.min, me.timelineOptions.max)
};

gAllocator.prototype.getData = function (startDate, endDate) {
  var me = this
  me.setSchedulerBusy()
  var allocatorCriteria = ViewModel.AllocatorChannelPlayoutCriteria()
  var channelIDXml = me.getSelectedChannelXmlIDs()
  Singular.GetDataStateless("OBLib.Rooms.AllocatorChannelPlayoutList", {
    StartDate: startDate,
    EndDate: endDate,
    SelectedChannelIDsXML: channelIDXml,
    SystemID: allocatorCriteria.SystemID(),
    ProductionAreaID: allocatorCriteria.ProductionAreaID(),
    Live: allocatorCriteria.Live(),
    Delayed: allocatorCriteria.Delayed(),
    Premier: allocatorCriteria.Premier(),
    GenRefNo: allocatorCriteria.GenRefNo(),
    Genre: allocatorCriteria.Genre(),
    Series: allocatorCriteria.Series(),
    Title: allocatorCriteria.Title()
  }, function (response) {
    if (response.Success) {
      me.clearBookings()
      me.addResourcesAndBookings(response.Data)
    }
    else {
      OBMisc.Notifications.GritterError("Error Fetching Bookings", response.ErrorText, 3000, 'bottom-right')
    }
    me.setSchedulerNotBusy()
    ViewModel.IsBusyRefreshing(false)
  })
};

gAllocator.prototype.addResourcesAndBookings = function (resources) {
  var me = this;
  var grp = null,
    resourcesToAdd = [],
    bookingsToAdd = [];
  me.resources.clear()
  me.resourceBookings.clear()

  //add the groups
  resources.Iterate(function (res, resIndex) {
    grp = me.createGroupItem(res)
    resourcesToAdd.push(grp)
  })
  me.resources.add(resourcesToAdd)

  //add the bookings
  resources.Iterate(function (res, resIndex) {
    grp = me.getGroupByChannelID(res.ChannelID)
    if (grp) {
      var groupBookings = me.addBookings(res.AllocatorChannelPlayoutEventList, grp)
      //bookingsToAdd.push(groupBookings)
    }
  })
  me.resourceBookings.add(bookingsToAdd)

  //update the timeline with its groups and items
  me.visTimeline.setGroups(me.resources)
  me.visTimeline.setItems(me.resourceBookings)
};

gAllocator.prototype.getGroupByChannelID = function (ChannelID) {
  var me = this;
  //var allResources = me.resources.get();
  var GR = null;
  me.resources.get().Iterate(function (res, GroupIndex) {
    if (!GR) {
      if (res.Channel.ChannelID == ChannelID) {
        GR = res;
      };
    }
  });
  return GR;
};

gAllocator.prototype.createGroupItem = function (resourceData) {
  var me = this;
  return {
    content: "<i class='fa fa-tv'></i> <span class='resource-channel'>" + resourceData.ChannelShortName + "</span>",
    Channel: resourceData
  }
};

gAllocator.prototype.addBookings = function (resourceBookings, Group) {
  var me = this
  var tempItem = null
  resourceBookings.Iterate(function (itm, indx) {
    tempItem = {
      content: me.getGroupItemContent(itm),
      start: new Date(itm.ScheduleDateTime),
      end: new Date(itm.ScheduleEndDate),
      group: Group.id,
      type: 'range',
      className: me.getGroupItemCssClass(itm),
      Booking: itm,
      editable: false
    }
    me.resourceBookings.add(tempItem)
  })
  //return templist;
};

gAllocator.prototype.getGroupItemContent = function (bookingData) {
  var me = this;
  //style="font-weight:bold;font-size:700;margin:0px"
  var resstr = bookingData.MCRController.length > 0 ? bookingData.MCRController : bookingData.SCCROperator
  return '<div>' +
    (bookingData.BookingCount > 0 ? '<span class="label label-primary">' + bookingData.BookingCount.toString() + '</span>' : '') +
    '<div>' + bookingData.GenreSeries + '</div>' +
    '<div>' + new Date(bookingData.ScheduleDateTime).format('HH:mm') + ' - ' + new Date(bookingData.ScheduleEndDate).format('HH:mm') + '</div>' +
    '<div>' + bookingData.Title + '</div>' +
    '<div>' + bookingData.Room + '</div>' +
    '<div>' + resstr + '</div>' +
    '</div>'
  //<span class="label label-primary pull-right">6</span>
};

gAllocator.prototype.getGroupItemCssClass = function (bookingData) {
  if (bookingData.EventStatus == "L") {
    return 'event-live'
  } else if (bookingData.EventStatus == "D") {
    return 'event-delayed'
  } else if (bookingData.EventStatus == "P") {
    return 'event-premier'
  } else {
    return 'event-default'
  }
};

gAllocator.prototype.setSchedulerBusy = function () {
  var me = this;
  me.isBusy = true;
  this.busyDiv.style.display = 'block';
};

gAllocator.prototype.setSchedulerNotBusy = function () {
  var me = this;
  me.isBusy = false;
  this.busyDiv.style.display = 'none';
};

gAllocator.prototype.clearBookings = function () {
  this.resources.clear()
  this.resourceBookings.clear()
};

//#endregion

Singular.OnPageLoad(function () {
  window.gTimeline = new gAllocator()
  $("#EditTimesPanelSynergyEvent").hide()
  RoomAllocatorPage.selectDefaultChannels()

  $("#RoomScheduleModal").on('shown.bs.modal', function () {
    RoomSchedBO.setupDefaultRoom()
  })

})