﻿var CurrentHumanResourceControl = new HumanResourceControl({})
var FindHRControl = new FindROHumanResourceControl({
  onRowSelected: function (ROHumanResource) {
    this.hide();
    HumanResourcesPage.fetchHumanResource(ROHumanResource);
  }
})
var FindCountryControl = new FindROCountryControl({
  onCountrySelected: function (control, ROCountry) {
    control.hide();
    ViewModel.CurrentHumanResource().NationalityCountyID(ROCountry.CountryID());
    ViewModel.CurrentHumanResource().Country(ROCountry.Country());
    //ViewModel.CurrentHumanResource().CityID(null);
    //ViewModel.CurrentHumanResource().City('');
    Singular.Validation.CheckRules(ViewModel.CurrentHumanResource());
  }
})
var FindHomeCityModal = new FindROCityModal({
  pagingManagerName: 'ROCityListManager',
  listName: 'ROCityList',
  criteriaName: 'ROCityListCriteria',
  beforeShow: function (ctrl) {
    //ctrl.criteria().CountryID(ViewModel.CurrentHumanResource().NationalityCountyID());
  },
  onCitySelected: function (ctrl, ROCity) {
    ctrl.hide();
    ViewModel.CurrentHumanResource().CityID(ROCity.CityID());
    ViewModel.CurrentHumanResource().City(ROCity.City());
    Singular.Validation.CheckRules(ViewModel.CurrentHumanResource());
  },
  onCityUnSelected: function (ctrl, ROCity) {
    ViewModel.CurrentHumanResource().CityID(null);
    ViewModel.CurrentHumanResource().City('');
    Singular.Validation.CheckRules(ViewModel.CurrentHumanResource());
  },
  searchByCountryEnabled: true
})

var SecondmentModal = new HRSecondmentModal({
  afterSaveSuccess: function (control, response) {
    OBMisc.Notifications.GritterSuccess("Secondment Saved Successfully", "", 1500)
    ViewModel.ROHumanResourceSecondmentListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.ROHumanResourceSecondmentListManager().Refresh()
    HumanResourcesPage.updateHRSilent()
  }
})
var OffPeriodModal = new HROffPeriodModal({
  afterSaveSuccess: function (control, response) {
    ViewModel.ROHumanResourceOffPeriodListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
    ViewModel.ROHumanResourceOffPeriodListManager().Refresh()
    HumanResourcesPage.updateHRSilent(function (response) {

    })
  }
})
var TBCProductions = new PHRTBCControl({})
var SkillModal = new HRSkillModal({
  afterSaveSuccess: function (control, response) {
    ViewModel.ROHumanResourceSkillListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.ROHumanResourceSkillListManager().Refresh()
    HumanResourcesPage.updateHRSilent(function (response) {
      if (ViewModel.CurrentHumanResource().PrimarySkillCount() == 0) {
        OBMisc.Notifications.GritterError('Primary Skill Missing', ViewModel.CurrentHumanResource().Firstname() + ' has no primary skills', 5000)
      } else if (ViewModel.CurrentHumanResource().PrimarySkillCount() > 1) {
        OBMisc.Notifications.GritterError('Too Many Primary Skills', ViewModel.CurrentHumanResource().Firstname() + ' has too many primary skills', 5000)
      }
    })
  },
  onSkillFetched: function (inst, response) {
    OBMisc.UI.activateTab("PositionsTab")
  }
})
var AvailabilityModal = new HRAvailabilityModal()

FindHRControl.AddSystem = function (obj) {
  obj.IsSelected(!obj.IsSelected())
  ROHumanResourcePagedListCriteriaBO.SetXMLCriteria()
  FindHRControl.refresh()
}
FindHRControl.AddProductionArea = function (obj) {
  obj.IsSelected(!obj.IsSelected())
  ROHumanResourcePagedListCriteriaBO.SetXMLCriteria()
  FindHRControl.refresh()
}

FindHRControl.afterModalOpened = function (obj) {
  $(ViewModel.ROHumanResourceListCriteria().Keyword.BoundElements).focus();
}

ROHumanResourcePagedListCriteriaBO.ShowMyAreaOnlySet = function (self) {
  FindHRControl.refresh()
}

HumanResourcesPage = {
  //******** Human Resource *********************************
  fetchHumanResource: function (ROHumanResource) {
    Singular.SendCommand("FetchHumanResource", {
      HumanResourceID: ROHumanResource.HumanResourceID()
    }, function (response) {
      Singular.Validation.CheckRules(ViewModel.CurrentHumanResource(), true, false);
      HumanResourceBO.GetHRHDuplicates(ViewModel.CurrentHumanResource())
      OBMisc.Tools.enableNumericKeysOnly({ id: "", cssClass: "txtCellPhoneNumber" })
      OBMisc.Tools.enableNumericKeysOnly({ id: "", cssClass: "txtAlternativeContactNumber" })
    })
  },
  newHumanResource: function () {
    Singular.SendCommand("NewHumanResource", {}, function (response) {
      OBMisc.Tools.enableNumericKeysOnly({ id: "", cssClass: "txtCellPhoneNumber" });
      OBMisc.Tools.enableNumericKeysOnly({ id: "", cssClass: "txtAlternativeContactNumber" });
    });
  },
  updateHRSilent: function (afterFetch) {
    ViewModel.CurrentHumanResource().IsProcessing(true)
    Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
      HumanResourceID: ViewModel.CurrentHumanResource().HumanResourceID()
    }, function (response) {
      if (response.Success) {
        var hr = response.Data[0]
        ViewModel.CurrentHumanResource().PrimarySystemID(hr.PrimarySystemID)
        ViewModel.CurrentHumanResource().PrimaryProductionAreaID(hr.PrimaryProductionAreaID)
        //ViewModel.CurrentHumanResource().PrimarySystemAreaCount(hr.PrimarySystemAreaCount)
        ViewModel.CurrentHumanResource().DuplicateCount(hr.DuplicateCount)
        //ViewModel.CurrentHumanResource().CurrentUserCanAuthoriseLeave(hr.CurrentUserCanAuthoriseLeave)
        //ViewModel.CurrentHumanResource().PrimaryAreaName(hr.PrimaryAreaName)
        ViewModel.CurrentHumanResource().PrimarySkillID(hr.PrimarySkillID)
        ViewModel.CurrentHumanResource().PrimarySkillDisciplineID(hr.PrimarySkillDisciplineID)
        //ViewModel.CurrentHumanResource().PrimarySkillDiscipline(hr.PrimarySkillDiscipline)
        ViewModel.CurrentHumanResource().SkillCount(hr.SkillCount)
        ViewModel.CurrentHumanResource().PrimarySkillCount(hr.PrimarySkillCount)
        ViewModel.CurrentHumanResource().MissingRateCount(hr.MissingRateCount)
        ViewModel.CurrentHumanResource().CurrentUserIsHRManager(hr.CurrentUserIsHRManager)
        Singular.Validation.CheckRules(ViewModel.CurrentHumanResource(), true, false)
        if (afterFetch) {
          afterFetch(response)
        }
      }
      else {
        OBMisc.Notifications.GritterError('Error During Rule Checking', response.ErrorText, null);
      }
    })
  },
  enableContractType: function (humanresource) {
    if (humanresource.ContractTypeID() == 8 || humanresource.ContractTypeID() == 7 || humanresource.ContractTypeID() == 3) {
      return true;
    } else {
      return false;
    }
  },
  saveHumanResource: function (humanresource) {
    Singular.SendCommand("Save", {},
      function (response) {
        if (response.Success) {
          HumanResourcesPage.onSkillsTabSelected();
          OBMisc.Notifications.GritterSuccess("Saved", humanresource.Firstname() + ' has been saved successfully', 1000)
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
        }
      })
  },

  //******** Skills *****************************************
  newSkill: function (humanResource) {
    SkillModal.newSkillFromHRScreen(humanResource)
  },
  onSkillsTabSelected: function () {
    ViewModel.ROHumanResourceSkillListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
    ViewModel.ROHumanResourceSkillListManager().Refresh();
  },
  onSkillSelected: function (ROSkill) {
    SkillModal.editSkillHRScreen(ROSkill)
  },
  deleteSelectedSkills: function (humanResource) {
    ViewModel.ROHumanResourceSkillListManager().IsLoading(true)
    //$("#ApplyResultModal").modal()
    var ids = [];
    ViewModel.ROHumanResourceSkillList().Iterate(function (roHRSkill, secIndx) {
      if (roHRSkill.IsSelected()) { ids.push(roHRSkill.HumanResourceSkillID()) }
    })
    ViewModel.CallServerMethod("DeleteHumanResourceSkills", {
      HumanResourceSkillIDs: ids
    },
    function (response) {
      if (response.Success) {
        if (Array.isArray(response.Data)) {
          response.Data.Iterate(function (itm, indx) {
            if (itm.Success) {
              OBMisc.Notifications.GritterSuccess("Deleted Successfully", itm.ErrorText, 1000)
            } else {
              OBMisc.Notifications.GritterError("Delete Failed", itm.ErrorText, 3000)
            }
          })
          HumanResourcesPage.updateHRSilent()
        } else {

        }
      }
      else {
        OBMisc.Notifications.GritterError('Error During Delete', response.ErrorText, null);
      }
      ViewModel.ROHumanResourceSkillListManager().Refresh()
    })
  },

  //******** Off Periods ************************************
  newOffPeriod: function (humanResource) {
    OffPeriodModal.newOffPeriodFromHRScreen(humanResource.HumanResourceID());
  },
  onOffPeriodsTabSelected: function () {
    ViewModel.ROHumanResourceOffPeriodListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
    ViewModel.ROHumanResourceOffPeriodListManager().Refresh();
  },
  onOffPeriodSelected: function (ROOffPeriod) {
    OffPeriodModal.editOffPeriodHRScreen(ROOffPeriod);
  },
  deleteSelectedOffPeriods: function (humanResource) {
    ViewModel.ROHumanResourceOffPeriodListManager().IsLoading(true)
    var ids = [];
    ViewModel.ROHumanResourceOffPeriodList().Iterate(function (offPeriod, secIndx) {
      if (offPeriod.IsSelected()) { ids.push(offPeriod.HumanResourceOffPeriodID()) }
    })
    ViewModel.CallServerMethod("DeleteHumanResourceOffPeriods", {
      HumanResourceOffPeriodIDs: ids
    },
    function (response) {
      if (response.Success) {
        if (Array.isArray(response.Data)) {
          response.Data.Iterate(function (itm, indx) {
            if (itm.Success) {
              OBMisc.Notifications.GritterSuccess("Deleted Successfully", itm.ErrorText, 1500)
            }
            else {
              OBMisc.Notifications.GritterError("Delete Failed", itm.ErrorText, 3000)
            }
          })
        } else {

        }
      }
      else {
        OBMisc.Notifications.GritterError('Error During Delete', response.ErrorText, 3000);
      }
      ViewModel.ROHumanResourceOffPeriodListManager().Refresh()
    })
  },
  onOffPeriodUnAuthorised: function (ROOffPeriod) {
    ViewModel.ROHumanResourceOffPeriodListManager().IsLoading(true)
    if (ROOffPeriod.CanAuthLeave() && ROOffPeriod.AuthorisedID() == 2) {
      ViewModel.CallServerMethod("UnAuthoriseLeave", {
        HumanResourceOffPeriodID: ROOffPeriod.HumanResourceOffPeriodID()
      }, function (response) {
        if (response.Success) {
          ViewModel.ROHumanResourceOffPeriodListManager().Refresh()
          OBMisc.Notifications.GritterSuccess('UnAuthorised Successfully', "Off Period unauthorised successfully", 1500)
        } else {
          OBMisc.Notifications.GritterError('Error UnAuthorising', response.ErrorText, 3000)
        }
      })
    }
  },
  specifyUnAvailability: function (humanResource) {
    AvailabilityModal.specifyUnAvailabilityHRScreen(humanResource);
  },
  onFindAvailabilityTabSelected: function () {
    ViewModel.ROHumanResourceUnAvailabilityListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.ROHumanResourceUnAvailabilityListCriteria().StartDate(null)
    ViewModel.ROHumanResourceUnAvailabilityListCriteria().EndDate(null)
    ViewModel.ROHumanResourceUnAvailabilityListCriteria().PageNo(1)
    ViewModel.ROHumanResourceUnAvailabilityListManager().Refresh()
  },
  deleteSelectedUnAvailability: function () {
    var toDelete = []
    ViewModel.ROHumanResourceUnAvailabilityList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        toDelete.push(itm)
      }
    })
    HumanResourceBO.deleteUnAvailability(toDelete, {
      onSuccess: function (response) {
        ViewModel.ROHumanResourceUnAvailabilityListManager().Refresh()
      }
    })
  },

  //******** Secondments ************************************
  onSecondmentsTabSelected: function () {
    ViewModel.ROHumanResourceSecondmentListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
    ViewModel.ROHumanResourceSecondmentListManager().Refresh();
  },
  onSecondmentSelected: function (ROSecondment) {
    SecondmentModal.editSecondmentHRScreen(ROSecondment)
  },
  deleteSelectedSecondments: function (humanResource) {
    ViewModel.ROHumanResourceSecondmentListManager().IsLoading(true);

    var ids = [];
    ViewModel.ROHumanResourceSecondmentList().Iterate(function (secondment, secIndx) {
      if (secondment.IsSelected()) { ids.push(secondment.HumanResourceSecondmentID()) }
    })

    ViewModel.CallServerMethod("DeleteSecondments", {
      HumanResourceSecondmentIDs: ids
    }, function (response) {
      if (response.Success) {
        if (Array.isArray(response.Data)) {
          response.Data.Iterate(function (itm, indx) {
            if (itm.Success) {
              OBMisc.Notifications.GritterSuccess("Deleted Successfully", itm.ErrorText, 1500)
            }
            else {
              OBMisc.Notifications.GritterError("Delete Failed", itm.ErrorText, 3000)
            }
          })
        } else {

        }
      }
      else {
        OBMisc.Notifications.GritterError('Error During Delete', response.ErrorText, 3000);
      }
      ViewModel.ROHumanResourceSecondmentListManager().Refresh();
    })

  },
  newSecondment: function (humanResource) {
    SecondmentModal.newSecondmentHRScreen(humanResource);
  },

  //******** TBC ********************************************
  onTBCSelected: function () {
    TBCProductions.getListForHRScreen(ViewModel.CurrentHumanResource().HumanResourceID())
  },

  //******** Documents **************************************
  onDocumentsTabSelected: function () {
    var me = this
    ViewModel.CurrentHumanResource().IsProcessing(true)
    Singular.SendCommand("FetchHRDocuments", {
      HumanResourceID: ViewModel.CurrentHumanResource().HumanResourceID()
    }, function (response) {
      if (response.Success) {
        //ViewModel.HRDocumentList.Set(response.Data)
      } else {
        OBMisc.Notifications.GritterError('Error Fetching Documents', response.ErrorText, null)
      }
      ViewModel.CurrentHumanResource().IsProcessing(false)
    })
    //Singular.GetDataStateless("OBLib.HR.HRDocumentList", {
    //  HumanResourceID: ViewModel.CurrentHumanResource().HumanResourceID()
    //}, function (response) {
    //  if (response.Success) {
    //    ViewModel.HRDocumentList.Set(response.Data)
    //  } else {
    //    OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the documents', response.ErrorText, null)
    //  }
    //  ViewModel.CurrentHumanResource().IsProcessing(false)
    //})
  },

  //******** Areas **************************************
  isPlayoutsUser: function () {
    return ClientData.ROUserSystemList.map(function (elem) { return elem.SystemID }).indexOf(5) !== -1
  },
  onAreasTabSelected: function () {
    //var me = this
    //ViewModel.CurrentHumanResource().IsProcessing(true)
    //Singular.GetDataStateless("OBLib.HR.HRSystemSelectList", {
    //  HumanResourceID: ViewModel.CurrentHumanResource().HumanResourceID()
    //}, function (response) {
    //  if (response.Success) {
    //    ViewModel.HRSystemSelectList.Set(response.Data)
    //  } else {
    //    OBMisc.Modals.Error('Error Processing', 'An error occured while fetching the documents', response.ErrorText, null)
    //  }
    //  ViewModel.CurrentHumanResource().IsProcessing(false)
    //})
  },

  //******** Timesheets **************************************
  onTimesheetsTabSelected: function () {
    ViewModel.ROHumanResourceTimesheetPagedListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.ROHumanResourceTimesheetPagedListManager().Refresh()
  },
  editROHumanResourceTimesheetPaged: function (ROHumanResourceTimesheetPaged) {
    var me = this;
    HumanResourceTimesheetBO.getTimesheet(ROHumanResourceTimesheetPaged.HumanResourceTimesheetID(),
                                          function (response) {
                                            ViewModel.CurrentHumanResourceTimesheet.Set(response.Data)
                                            me.showTimesheetModal()
                                          },
                                          function (response) {

                                          })
  },
  showTimesheetModal: function () {
    $("#CurrentHRTimesheetModal").off('hidden.bs.modal')
    $("#CurrentHRTimesheetModal").on('hidden.bs.modal', function () {
      ViewModel.CurrentHumanResourceTimesheet(null)
    })
    $("#CurrentHRTimesheetModal").off('shown.bs.modal')
    $("#CurrentHRTimesheetModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentHumanResourceTimesheet())
    })
    $("#CurrentHRTimesheetModal").modal()
  },
  newTimesheet: function (humanResource) {
    ViewModel.CurrentHumanResourceTimesheet(new HumanResourceTimesheetObject())
    ViewModel.CurrentHumanResourceTimesheet().HumanResourceID(humanResource.HumanResourceID())
    ViewModel.CurrentHumanResourceTimesheet().HumanResource(humanResource.Firstname())
    this.showTimesheetModal()
  },
  saveHumanResourceTimesheet: function (humanResourceTimesheet) {
    var me = this;
    HumanResourceTimesheetBO.save(humanResourceTimesheet, {
      onSaveSuccess: function (response) {
        $("#CurrentHRTimesheetModal").modal('hide')
        ViewModel.ROHumanResourceTimesheetPagedListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
        ViewModel.ROHumanResourceTimesheetPagedListManager().Refresh()
        HumanResourceTimesheetBO.getTimesheet(response.Data.HumanResourceTimesheetID,
                                               function (response) {
                                                 ViewModel.CurrentHumanResourceTimesheet.Set(response.Data)
                                                 me.showTimesheetModal()
                                               },
                                               function (response) {

                                               })
      }
    })
  },
  deleteSelectedTimesheets: function () {
    var selected = []
    ViewModel.ROHumanResourceTimesheetPagedList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        selected.push(itm.Serialise())
      }
    })
    ViewModel.CallServerMethod("DeleteROHumanResourceTimesheets", {
      HumanResourceTimesheets: selected
    }, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Delete Succeeded", "", 250)
        ViewModel.ROHumanResourceTimesheetPagedListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
        ViewModel.ROHumanResourceTimesheetPagedListManager().Refresh()
      }
      else {
        OBMisc.Notifications.GritterError("Delete Failed", response.ErrorText, 1000)
        ViewModel.ROHumanResourceTimesheetPagedListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
        ViewModel.ROHumanResourceTimesheetPagedListManager().Refresh()
      }
    })
  },

  //******** AddToBooking **************************************
  onAddToBookingSelected: function () { 
    ViewModel.HumanResourceAddToBooking(new HumanResourceAddToBookingObject())
    ViewModel.HumanResourceAddToBooking().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.HumanResourceAddToBooking().HumanResourceName(ViewModel.CurrentHumanResource().Firstname())
    Singular.Validation.CheckRules(ViewModel.HumanResourceAddToBooking())
  }
};

ROHumanResourcePagedListCriteriaBO = {
  ContractTypeIDSet: function (self) { FindHRControl.refresh() },
  DisciplineIDSet: function (self) { FindHRControl.refresh() },
  FirstnameSet: function (self) { FindHRControl.refresh() },
  SecondNameSet: function (self) { FindHRControl.refresh() },
  PreferredNameSet: function (self) { FindHRControl.refresh() },
  SurnameSet: function (self) { FindHRControl.refresh() },
  KeywordSet: function (self) { FindHRControl.refresh() },
  SystemIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceListManager().IsLoading()) {
      FindHRControl.refresh()
    }
  },
  ProductionAreaIDsXmlSet: function () {
    if (!ViewModel.ROHumanResourceListManager().IsLoading()) {
      FindHRControl.refresh()
    }
  },
  SetXMLCriteria: function () {
    //clear current selections
    var selectedSystems = []
    var selectedAreas = []
    ViewModel.UserSystemList().Iterate(function (uSys, uSysInd) {
      if (uSys.IsSelected()) {
        selectedSystems.push(uSys.SystemID())
      }
      uSys.UserSystemAreaList().Iterate(function (uSyAr, uSyArInd) {
        if (uSyAr.IsSelected()) {
          selectedAreas.push(uSyAr.ProductionAreaID())
        }
      })
    })
    ViewModel.ROHumanResourceListCriteria().SystemIDsXml(OBMisc.Xml.getXmlIDs(selectedSystems))
    ViewModel.ROHumanResourceListCriteria().ProductionAreaIDsXml(OBMisc.Xml.getXmlIDs(selectedAreas))
  }
}

Singular.OnPageLoad(function () {
  FindHRControl.setup()
  FindCountryControl.setup()
  FindHomeCityModal.setup()
  SecondmentModal.setup()
  ViewModel.CurrentHumanResourceUnAvailabilityTemplate(null)
  HumanResourceBO.OnDuplicatesDetected = function (humanResource) {
    CurrentHumanResourceControl.showDuplicates(humanResource)
  }
})