﻿var ROHumanResources = new ROHumanResourceListManager();

//#region Production Reports

RoomBookingGridReport = {
  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.AfterSubDeptsAndAreasUpdated()
  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.AfterSubDeptsAndAreasUpdated()
  },
  AfterSubDeptsAndAreasUpdated: function () {
    //clear values
    ViewModel.Report().ReportCriteriaGeneric().SystemIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    //repopulate values
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(us.SystemID()) }
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        if (usa.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(usa.ProductionAreaID()) }
      })
    })
    //refresh disciplines if something is selected
    if (ViewModel.Report().ReportCriteriaGeneric().SystemIDs().length > 0) {
      RODisciplineReportBO.get({
        criteria: {
          SystemIDs: ViewModel.Report().ReportCriteriaGeneric().SystemIDs(),
          ProductionAreaIDs: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()
        },
        onSuccess: function (response) {
          ViewModel.Report().ReportCriteriaGeneric().RODisciplineList.Set(response.Data)
        }
      })
    }
  },
  AddContractType: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ROContractTypeList().Iterate(function (ct, ctIndx) {
      if (ct.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.push(ct.ContractTypeID()) }
    })
  },
  AddDiscipline: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs([])
    ViewModel.Report().ReportCriteriaGeneric().RODisciplineList().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.push(itm.DisciplineID())
      }
    })
  },
  RORoomTypeReportSelected: function (roomType) {
    roomType.IsSelected(!roomType.IsSelected())
    roomType.RORoomTypeReportRoomList().Iterate(function (itm, indx) {
      itm.IsSelected(roomType.IsSelected())
    })
    //update selections
    ViewModel.Report().ReportCriteriaGeneric().RoomIDs([])
    ViewModel.Report().ReportCriteriaGeneric().RORoomTypeReportList().Iterate(function (us, usIndx) {
      us.RORoomTypeReportRoomList().Iterate(function (usa, usaIndx) {
        if (usa.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().RoomIDs.push(usa.RoomID()) }
      })
    })
  },
  AddRoom: function (roomTypeRoom) {
    roomTypeRoom.IsSelected(!roomTypeRoom.IsSelected())
    ViewModel.Report().ReportCriteriaGeneric().RoomIDs([])
    //update selections
    ViewModel.Report().ReportCriteriaGeneric().RORoomTypeReportList().Iterate(function (us, usIndx) {
      us.RORoomTypeReportRoomList().Iterate(function (usa, usaIndx) {
        if (usa.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().RoomIDs.push(usa.RoomID()) }
      })
    })
  },
  GetDisciplines: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineReportList, OBLib', {
      PageSize: 1000,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID()
    }, function (args) {
      if (args.Success) {
        $("#Disciplines").addClass('FadeDisplay');
        $("#Rooms").addClass('FadeDisplay');
        $("#RoomTypes").addClass('FadeDisplay');
        ViewModel.Report().ReportCriteriaGeneric().RODisciplineList.Set(args.Data)
      }
      ViewModel.IsReportBusy(false)
    });
    return ViewModel.Report().ReportCriteriaGeneric().ROSystemProductionAreaDisciplineList;

  }
}

ProductionFacilitiesBookingsReport = {
  RORoomTypeReportSelected: function (roomType) {
    roomType.IsSelected(!roomType.IsSelected())
    roomType.RORoomTypeReportRoomList().Iterate(function (itm, indx) {
      itm.IsSelected(roomType.IsSelected())
    })
    //update selections
    ViewModel.Report().ReportCriteriaGeneric().RoomIDs([])
    ViewModel.Report().ReportCriteriaGeneric().RORoomTypeReportList().Iterate(function (us, usIndx) {
      us.RORoomTypeReportRoomList().Iterate(function (usa, usaIndx) {
        if (usa.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().RoomIDs.push(usa.RoomID()) }
      })
    })
  },
  AddRoom: function (roomTypeRoom) {
    roomTypeRoom.IsSelected(!roomTypeRoom.IsSelected())
    ViewModel.Report().ReportCriteriaGeneric().RoomIDs([])
    //update selections
    ViewModel.Report().ReportCriteriaGeneric().RORoomTypeReportList().Iterate(function (us, usIndx) {
      us.RORoomTypeReportRoomList().Iterate(function (usa, usaIndx) {
        if (usa.IsSelected()) { ViewModel.Report().ReportCriteriaGeneric().RoomIDs.push(usa.RoomID()) }
      })
    })
  }
}

CommentatorPAFormReportNew = {
  SystemSelected: function (system) {
    system.IsSelected(!system.IsSelected())
    this.updatePSAList()
  },
  ProductionAreaSelected: function (area) {
    area.IsSelected(!area.IsSelected())
    this.updatePSAList()
  },
  PSASelected: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.updateSelectedPSAIDs()
  },
  StartDateSet: function (obj) {
    this.updatePSAList()
  },
  EndDateSet: function (obj) {
    this.updatePSAList()
  },
  KeywordSet: function (obj) {
    this.updatePSAList()
  },
  updatePSAList: function () {
    var rptCrit = ViewModel.Report().ReportCriteriaGeneric()
    var systemIDs = []
    var productionAreaIDs = []
    ViewModel.UserSystemList().Iterate(function (sys, sysIndx) {
      if (sys.IsSelected()) {
        systemIDs.push(sys.SystemID())
        sys.UserSystemAreaList().Iterate(function (area, areaIndx) {
          if (area.IsSelected()) {
            productionAreaIDs.push(area.ProductionAreaID())
          }
        })
      }
    })
    if (rptCrit.StartDate() && rptCrit.EndDate() && systemIDs.length > 0 && productionAreaIDs.length > 0) {
      ViewModel.IsReportBusy(true)
      ROPSAReportBO.get({
        criteria: {
          StartDate: rptCrit.StartDate(),
          EndDate: rptCrit.EndDate(),
          SystemIDs: systemIDs,
          ProductionAreaIDs: productionAreaIDs,
          ProductionSystemAreaName: rptCrit.Keyword()
        },
        onSuccess: function (response) {
          ViewModel.Report().ReportCriteriaGeneric().ROPSAReportList.Set(response.Data)
          ViewModel.IsReportBusy(false)
          //console.log(response)
        },
        onFail: function (response) {
          ViewModel.IsReportBusy(false)
        }
      })
    }
  },
  selectAll: function () {
    var me = this
    ViewModel.Report().ReportCriteriaGeneric().ROPSAReportList().Iterate(function (psa, psaIndx) {
      psa.IsSelected(true)
    })
    this.updateSelectedPSAIDs()
  },
  deselectAll: function () {
    var me = this
    ViewModel.Report().ReportCriteriaGeneric().ROPSAReportList().Iterate(function (psa, psaIndx) {
      psa.IsSelected(false)
    })
    this.updateSelectedPSAIDs()
  },
  updateSelectedPSAIDs: function () {
    ViewModel.Report().ReportCriteriaGeneric().ProductionSystemAreaIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ROPSAReportList().Iterate(function (psa, psaIndx) {
      if (psa.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionSystemAreaIDs().push(psa.ProductionSystemAreaID())
      }
    })
  }
}

//#endregion

//#region Finance Reports

InvoiceChecksProductionServicesReport = {
  RoomIDSet: function (area) {
    //RoomScheduleAreaBO.updateAll(area)
  },
  triggerRoomAutoPopulate: function (args) {
    console.log(args)
    args.AutoPopulate = false
    //args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    console.log(args)
    //args.Data.SystemID = args.Object.SystemID()
    //args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    //args.Data.StartDateTime = new Date(args.Object.OnAirTimeStart()).format("dd MMM yyyy HH:mm")
    //args.Data.EndDateTime = new Date(args.Object.OnAirTimeEnd()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
    console.log(args)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    //var me = this
    console.log(selectedItem)
    console.log(businessObject)
    //if (businessObject.IsOwner()) {
    //  businessObject.Room(selectedItem.Room)
    //  businessObject.RoomID(selectedItem.RoomID)
    //  businessObject.ResourceID(selectedItem.ResourceID)
    //  businessObject.RoomResourceIDOwner(selectedItem.ResourceID)
    //  RoomScheduleAreaBO.updateAll(businessObject)
    //}
  }
}

UtilisationReport = {
  AddSystem: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.Report().ReportCriteriaGeneric().SystemIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.IsSelected(false)
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        usa.IsSelected(false)
      })
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs([us.SystemID()])
      }
    })
  },
  AddArea: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null)
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        usa.IsSelected(false)
      })
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
          if (usa.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(usa.ProductionAreaID())
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([usa.ProductionAreaID()])
          }
        })
      }
    })
  },
  canEdit: function (what, reportCriteria) {
    switch (what) {
      case 'StartDate':
      case 'EndDate':
        return (reportCriteria.SystemID() && reportCriteria.ProductionAreaID())
      default:
        return true;
        break;
    }
  }
};

GenericWorkOrderReport = {
  AddResource: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().ResourceIDs.indexOf(obj.ResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ResourceIDs.push(obj.ResourceID());
        //GenericWorkOrderReport.AddResource();
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ResourceIDs.indexOf(obj.ResourceID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ResourceIDs.splice(index, 1);
        //GenericWorkOrderReport.AddResource();
      }
    }
  },
  AddSystem: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.IsSelected(false)
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
      }
    })
    GenericWorkOrderReport.RefreshResources(obj)
  },
  AddResourceType: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    ViewModel.Report().ReportCriteriaGeneric().ResourceTypeIDs([])
    ViewModel.Report().ReportCriteriaGeneric().ROResourceTypeList().Iterate(function (rt, usIndx) {
      if (rt.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().ResourceTypeIDs.push(rt.ResourceTypeID())
      }
    })
    GenericWorkOrderReport.RefreshResources(obj)
  },
  KeywordSet: function (self) {
    GenericWorkOrderReport.RefreshResources(self);
  },
  RefreshResources: function (obj) {
    ViewModel.IsReportBusy(true);
    Singular.GetDataStateless('OBLib.Maintenance.Resources.ReadOnly.ROResourceList, OBLib', {
      ResourceName: null,
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ResourceTypeIDs: ViewModel.Report().ReportCriteriaGeneric().ResourceTypeIDs(),
      Keyword: ViewModel.Report().ReportCriteriaGeneric().Keyword(),
      ProductionAreaID: null,
      PageNo: 1,
      PageSize: 10000,
      SortColumn: 'ResourceName',
      SortAsc: true
    },
        function (response) {
          if (response.Success) {
            ViewModel.Report().ReportCriteriaGeneric().ResourceTypeIDs([])
            ViewModel.Report().ReportCriteriaGeneric().ROResourceList.Set(response.Data)
          }
          ViewModel.IsReportBusy(false)
        })
  }

  };

//#endregion

//#region ICR Reports

ICRShiftPaymentReport = {
  FindICRHumanResources: function (element) {
    ROHumanResources.Init();
    ROHumanResources.Manager().SingleSelect(true);
    ROHumanResources.Manager().MultiSelect(false);
    ROHumanResources.Criteria().SystemID(ViewModel.CurrentUserSystemID());
    ROHumanResources.Criteria().ProductionAreaID(ViewModel.CurrentUserProductionAreaID());
    ROHumanResources.SetOptions({
      AfterRowSelected: function (ROHumanResource) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(ROHumanResource.HumanResourceID());
        ViewModel.Report().ReportCriteriaGeneric().HumanResource(ROHumanResource.PreferredFirstSurname());
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceID(null);
        ViewModel.Report().ReportCriteriaGeneric().HumanResource("");
      }
    });
    $('#ROHumanResourceListModal').modal();
    ROHumanResources.Manager().Refresh();
  },
  FindTeam: function (element) {
    //FindTeam(element);
  },
  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    //if (obj.IsSelected()) {
    //  ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID())
    //}
    this.AddAreas()
  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    this.AddAreas()
  },
  AddAreas: function () {
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndex) {
      if (us.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
        us.UserSystemAreaList().Iterate(function (usa, usaIndex) {
          if (usa.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs().push(usa.ProductionAreaID())
          }
        })
      }
    })
  },
  enableButton: function (self) { return true }
};

//#endregion

//#region Playout Reports

//#region Playout Ops Helper Functions
PLOHelpers = {
  AllowedRooms: function (List, Item) {
    var Allowed = [];
    if (List) {
      List.Iterate(function (itm2, ind2) {
        if (ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(itm2.OwningSystemID) > -1 &&
						ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(itm2.OwningProductionAreaID) > -1) {
          Allowed.push(itm2);
        }
      });
    }
    return Allowed;
  }
};
//#endregion

//#region SCCR Calculator
SCCRCalculatorReport = {
  FindEventType: function (element) {
    FindEventType(element)
  },
  FindChannel: function (element) {
    FindChannel(element);
  },
  UpdateGraphs: function (obj) {
    UpdateGraphs(obj);
  },
  RefreshEvents: function () {
    SCCRCalculatorReport.GetFilteredEvents();
  },
  GetFilteredEvents: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.Maintenance.Productions.ReadOnly.ROEventTypePagedList, OBLib', {
      PageSize: 1000,
      ProductionType: ViewModel.Report().ReportCriteriaGeneric().ProductionType(),
      EventType: ViewModel.Report().ReportCriteriaGeneric().EventType()
    }, function (args) {
      if (args.Success) {
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set([])
        ViewModel.Report().ReportCriteriaGeneric().ROEventTypeListReportList.Set(args.Data)
      }
      ViewModel.IsReportBusy(false)
    })

  },
  DelayedRefreshList: function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      SCCRCalculatorReport.GetFilteredEvents();
      ViewModel.ROEventTypePagedListManager().Refresh()
    }, DELAY); // create a new timeout
  },
  onEventTypeSelected: function (selectedItem, businessObject) {
  },
  triggerEventTypeAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setEventTypeCriteriaBeforeRefresh: function (args) {
    args.Data.ProductionTypeID = args.Object.ProductionTypeID()
  },
  afterEventTypeRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  EventTypeSet: function () {
  }
};
//#endregion

//#region Graphing Functions: Update Graphs
function UpdateGraphs(obj) {
  $("#Stats").addClass('FadeDisplay');
  ViewModel.CallServerMethod("GetCalculatorReport", {
    StartDate: obj.StartDate(),
    EndDate: obj.EndDate(),
    MorningInd: obj.MorningInd(),
    AfternoonInd: obj.AfternoonInd(),
    EveningInd: obj.EveningInd(),
    NightInd: obj.NightInd(),
    EventTypeID: obj.EventTypeID(),
    ChannelID: obj.ChannelID(),
    GenRefNumber: obj.GenRefNumber(),
    NoOfExtraEvents: obj.NoOfExtraEvents()
  }, function (response) {
    if (response) {
      var YearMonthChannelCategories = ['Morning (06:00 - 11:59)',
                                        'Afternoon (12:00 - 17:59)',
                                        'Evening (18:00 - 23:59)',
                                        'Night (00:00 - 05:59)'];
      var SCCRSFMseriesOptions = [];
      var SeriesSelectedArray = [];
      var SCCRSFMseriesMonth = "";
      var SCCRSFMseriesMonthArray = new Array();
      var MorningArray = new Array();
      var AfternoonArray = new Array();
      var EveningArray = new Array();
      var NightArray = new Array();
      var SCCRSFMtempMonth = "";
      var lastParsedMonth = -1;
      SeriesSelectedArray = [obj.MorningInd(),
                             obj.AfternoonInd(),
                             obj.EveningInd(),
                             obj.NightInd()];
      response.Data.Iterate(function (itm, ind) {
        if (itm != null) {
          if ((itm.indexOf('$') == 0) && (itm.charAt(1) != '$') && (isNaN(parseInt(itm.charAt(1))) && itm.charAt(1) != '-' && itm != '$')) { //Extract Months for x-axis
            SCCRSFMtempMonth = itm.replace('$', '');
            SCCRSFMseriesMonthArray.push(SCCRSFMtempMonth);
            SCCRSFMseriesMonth = SCCRSFMtempMonth;
            lastParsedMonth = 0;
          }
          else if (((itm.indexOf('$') == 0) && (!isNaN(parseInt(itm.charAt(1))) || itm.charAt(1) == '-')) || itm == '$' || SCCRSFMseriesMonth != "") {
            //Extract Available SCCRs for y-axis
            var tempData = '0';
            if (itm != '$') {
              tempData = itm.replace('$', '');
            }
            if (lastParsedMonth == 0) {
              MorningArray.push(parseInt(tempData)); // Push morning val
            }
            if (lastParsedMonth == 1) {
              AfternoonArray.push(parseInt(tempData)); // Push afternoon val
            }
            if (lastParsedMonth == 2) {
              EveningArray.push(parseInt(tempData)); // Push evening val
            }
            if (lastParsedMonth == 3) {
              NightArray.push(parseInt(tempData)); // Push night val
            }
            lastParsedMonth++;
          }
        }
      });
      for (var i = 0; i < YearMonthChannelCategories.length; i++) {
        SCCRSFMseriesOptions[i] = {
          name: YearMonthChannelCategories[i],
          data: (i == 0) ? MorningArray : (i == 1) ? AfternoonArray : (i == 2) ? EveningArray : (i == 3) ? NightArray : [],
          visible: SeriesSelectedArray[i]
        };
      }
      lastParsedMonth = -1;
      var options2 = {
        chart: {
          renderTo: 'SCCRShortFallContainer',
          type: 'line',
          zoomType: 'xy'
        },
        title: {
          text: 'SCCR Utilization per Month'
        },
        subtitle: {
          text: response.Data[response.Data.length - 5] + ' to ' + response.Data[response.Data.length - 4]
        },
        xAxis: {
          categories: SCCRSFMseriesMonthArray,
        },
        yAxis: [{
          title: {
            text: 'SCCR\'s'
          },
          min: 0,
          plotLines: [{
            color: '#FF0000',
            width: 2,
            value: parseInt(response.Data[response.Data.length - 1])
          },
          {
            color: '#FF0000',
            width: 3,
            value: 0
          }],
          plotBands: [{
          }],
        }],
        tooltip: {
          shared: true,
          valueSuffix: ' SCCR(s)'
        },
        credits: {
          enabled: false
        }
      };
      var chart2 = new Highcharts.Chart(options2, function (ch) {
        $.each(SCCRSFMseriesOptions, function (itemNo, item) {
          ch.addSeries({
            name: item.name,
            data: item.data,
            visible: item.visible
          }, false);

        });
        ch.redraw();
      });
    }
    //#endregion
    //#region SCCR Utilization per Weekday
    var SCCRSFWDseriesOptions = [];
    var SCCRSFWDseriesWeekdayArray = new Array();
    var MorningArraySCCRsPWD = new Array();
    var AfternoonArraySCCRsPWD = new Array();
    var EveningArraySCCRsPWD = new Array();
    var NightArraySCCRsPWD = new Array();
    var SCCRSFMtempWeekday = "";
    var lastParsedWeekday = -1;
    response.Data.Iterate(function (itm, ind) {
      if (itm != null) {
        if ((itm.indexOf('$') == 0) && (itm.charAt(1) == '$') && (isNaN(parseInt(itm.charAt(2))) && itm.charAt(2) != '-' && itm != '$$')) { //Extract Weekdays for x-axis
          SCCRSFMtempWeekday = itm.replace('$$', '');
          SCCRSFWDseriesWeekdayArray.push(SCCRSFMtempWeekday);
          lastParsedWeekday = 0;
        }
        else if (((itm.indexOf('$') == 0) && (itm.charAt(1) == '$') && (!isNaN(parseInt(itm.charAt(2))) || itm.charAt(2) == '-')) || itm == '$$') {
          //Extract Available SCCRs for y-axis
          var tempData = '0';
          if (itm != '$$') {
            tempData = itm.replace('$$', '');
          }
          if (lastParsedWeekday == 0) {
            MorningArraySCCRsPWD.push(parseInt(tempData)); // Push morning val
          }
          if (lastParsedWeekday == 1) {
            AfternoonArraySCCRsPWD.push(parseInt(tempData)); // Push afternoon val
          }
          if (lastParsedWeekday == 2) {
            EveningArraySCCRsPWD.push(parseInt(tempData)); // Push evening val
          }
          if (lastParsedWeekday == 3) {
            NightArraySCCRsPWD.push(parseInt(tempData)); // Push night val
          }
          lastParsedWeekday++;
        }
      }
    });
    for (var i = 0; i < YearMonthChannelCategories.length; i++) {
      SCCRSFWDseriesOptions[i] = {
        name: YearMonthChannelCategories[i],
        data: (i == 0) ? MorningArraySCCRsPWD : (i == 1) ? AfternoonArraySCCRsPWD : (i == 2) ? EveningArraySCCRsPWD : (i == 3) ? NightArraySCCRsPWD : [],
        visible: SeriesSelectedArray[i]
      };
    }
    lastParsedMonth = -1;
    var options3 = {
      chart: {
        renderTo: 'SCCRShortFallWeekdayContainer',
        type: 'line',
        zoomType: 'xy'
      },
      title: {
        text: 'SCCR Utilization per Weekday'
      },
      subtitle: {
        text: response.Data[response.Data.length - 5] + ' to ' + response.Data[response.Data.length - 4]
      },
      xAxis: {
        categories: SCCRSFWDseriesWeekdayArray,
        plotBands: [{ // visualize the weekend
          from: 5.5,
          to: 6.5,
          color: 'rgba(68, 170, 213, .2)'
        },
        { // visualize the weekend
          from: -0.5,
          to: 0.5,
          color: 'rgba(68, 170, 213, .2)'
        }]
      },
      yAxis: [{
        title: {
          text: 'SCCR\'s'
        },
        min: 0,
        plotLines: [{
          color: '#FF0000',
          width: 2,
          value: parseInt(response.Data[response.Data.length - 1])
        },
        {
          color: '#FF0000',
          width: 3,
          value: 0
        }],
        plotBands: [{
        }],
      }],
      tooltip: {
        shared: true,
        valueSuffix: ' SCCR(s)'
      },
      credits: {
        enabled: false
      }//,
    };
    var chart3 = new Highcharts.Chart(options3, function (ch) {
      $.each(SCCRSFWDseriesOptions, function (itemNo, item) {
        ch.addSeries({
          name: item.name,
          data: item.data,
          visible: item.visible
        }, false);

      });
      ch.redraw();
    });
    //#endregion
    Singular.HideLoadingBar();
  });
};
//#endregion

ReportCommon = {

  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  },
  AddSystemXml: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
    }
    ViewModel.ROHumanResourceFindListCriteria().SystemIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().SystemIDs()));

  },
  AddSystemList: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
    }
    ViewModel.ROHumanResourceFindListCriteria().SystemIDList(ViewModel.Report().ReportCriteriaGeneric().SystemIDs());

  },
  AddProductionAreaXml: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
    }
    ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()));

  },
  AddProductionAreaList: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
    }
    ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDList(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs());

  },
  GetAllowedHumanResourcesList: function () {
    ViewModel.IsReportBusy(true)
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib', {
      PageSize: 1000,
      SystemIDList: ViewModel.Report().ReportCriteriaGeneric().SystemIDs(),
      ProductionAreaIDList: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()
    }, function (args) {
      if (args.Success) {
        $("#HumanResources").addClass('FadeDisplay');
        $("#SelectedHumanResources").addClass('FadeDisplay');
        $("#HumanResourceFilters").addClass('FadeDisplay');
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set([]);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList.Set(args.Data);
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceSelectedList().Iterate(function (itm, ind) {
          ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm2, ind2) {
            if (itm.HumanResourceID() == itm2.HumanResourceID()) {
              itm2.IsSelected(true);
            }
          });
        });
      }
      ViewModel.IsReportBusy(false);
    });
  },
  AddHumanResource: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(obj.HumanResourceID());
      }
      if (ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceSelectedList.indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceSelectedList.push(obj);
      }
    }
    else {
      var HR = ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Find('HumanResourceID', obj.HumanResourceID());
      if (HR) {
        HR.IsSelected(false);
      }
      var indexofID = ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID());
      if (indexofID != -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.splice(indexofID, 1);
      }
      var indexOfObj = $.map(ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceSelectedList(), function (x, index) {
        if (x.HumanResourceID === obj.HumanResourceID) {
          return index;
        }
      });
      if (indexOfObj != -1) { //Item is in list if index != -1, so we can remove it at that index
        ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceSelectedList.splice(indexOfObj, 1);
      }
    }
  }

};

//#region Playout Operations Live Events Report
PlayoutOperationsLiveEventsReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystemList(obj)
    //ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    //ReportCommon.GetAllowedHumanResourcesList();
  }
};
//#endregion

//#region Booking Report
BookingsReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystemList(obj)
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddHumanResource: function (obj) {
    ReportCommon.AddHumanResource(obj)
  },
  AllowedRooms: function (List, Item) {
    return PLOHelpers.AllowedRooms(List, Item);
  }
};
//#endregion

//#region FreelanceAgreementReport
FreelanceAgreementReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystemList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddHumanResource: function (obj) {
    ReportCommon.AddHumanResource(obj)
  },
  AllowedRooms: function (List, Item) {
    return PLOHelpers.AllowedRooms(List, Item);
  }
};
//#endregion

//#region Playout Operations SCCR Shift Report
PlayoutOperationsSCCRShiftReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystemList(obj)
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddHumanResource: function (obj) {
    ReportCommon.AddHumanResource(obj)
  },
  AllowedRooms: function (List, Item) {
    return PLOHelpers.AllowedRooms(List, Item);
  }
};
//#endregion

//#region Playout Operations ASRA Report
PlayoutOperationsASRAReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystem(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  }
};
//#endregion

//#region Bookings By Controller Report
BookingsByControllerReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystemList(obj)
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddHumanResource: function (obj) {
    ReportCommon.AddHumanResource(obj)
  },
  AllowedRooms: function (List, Item) {
    return PLOHelpers.AllowedRooms(List, Item);
  }
};
//#endregion

//#region General Bookings Report
GeneralBookingsReport = {
  AddSystem: function (obj) {
    ReportCommon.AddSystemList(obj)
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddProductionArea: function (obj) {
    ReportCommon.AddProductionAreaList(obj);
    ReportCommon.GetAllowedHumanResourcesList();
  },
  AddHumanResource: function (obj) {
    ReportCommon.AddHumanResource(obj)
  },
  AllowedRooms: function (List, Item) {
    return PLOHelpers.AllowedRooms(List, Item);
  }
};
//#endregion

PlayoutBookingStatsReport = {
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      ViewModel.UserSystemList().Iterate(function (itm, ind) {
        itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
      });
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
    }
  }
};

PlayoutISPHoursSummaryReport = {
    AddSystem: function (obj) {
        if (!obj.IsSelected()) {
            ViewModel.UserSystemList().Iterate(function (itm, ind) {
                itm.IsSelected() ? itm.IsSelected(!itm.IsSelected()) : itm.IsSelected(itm.IsSelected());
            });
        }
        obj.IsSelected(!obj.IsSelected());
        if (obj.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
        }
        else {
            ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
        }
    }
};

//#endregion

//#region HumanResource Reports

BiometricLogsReport = {
  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
    }
    //ViewModel.ROHumanResourceFindListCriteria().SystemIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().SystemIDs()));
    //this.GetAllowedHumanResources();
  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
    }
    //ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()));
    //this.GetAllowedHumanResources();
  },
};

SkillsAvailabilityReport = {
  AddContractType: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.indexOf(obj.ContractTypeID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.push(obj.ContractTypeID())
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.indexOf(obj.ContractTypeID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ContractTypeIDs.splice(index, 1)
      }
    }
  },
  AddSystem: function (obj) {
    if (!obj.IsSelected()) {
      for (var i = 0; i < ViewModel.Report().ReportCriteriaGeneric().ROSystemList().length; i++) {
        if (ViewModel.Report().ReportCriteriaGeneric().ROSystemList()[i].IsSelected() == true) {
          ViewModel.Report().ReportCriteriaGeneric().ROSystemList()[i].IsSelected(false);
        }
      }
    }
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID())
      SkillsAvailabilityReport.GetAllowedDisciplines()
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
      SkillsAvailabilityReport.GetAllowedDisciplines();
    }
  },
  AddProductionArea: function (obj) {
    if (!obj.IsSelected()) {
      for (var i = 0; i < ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList().length; i++) {
        if (ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList()[i].IsSelected() == true) {
          ViewModel.Report().ReportCriteriaGeneric().ROProductionAreaList()[i].IsSelected(false);
        }
      }
    }
    obj.IsSelected(!obj.IsSelected())
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(obj.ProductionAreaID())
      SkillsAvailabilityReport.GetAllowedDisciplines()
    }
    else {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null)
      SkillsAvailabilityReport.GetAllowedDisciplines()
    }
  },
  AddProductionType: function (obj) {
    obj.IsSelected(!obj.IsSelected())
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.indexOf(obj.ProductionTypeID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.push(obj.ProductionTypeID());
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.indexOf(obj.ProductionTypeID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionTypeIDs.splice(index, 1);
      }
    }
  },
  AddDiscipline: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.indexOf(obj.DisciplineID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.push(obj.DisciplineID());
        SkillsAvailabilityReport.GetAllowedPositions();
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.indexOf(obj.DisciplineID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.splice(index, 1);
        SkillsAvailabilityReport.GetAllowedPositions();
      }
    }
  },
  AddPosition: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      var x = new DisciplinePositionObject()
      KOFormatter.Deserialise({ DisciplineID: obj.DisciplineID(), PositionID: obj.PositionID() }, x)
      if (ViewModel.Report().ReportCriteriaGeneric().Positions.indexOf(x) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().Positions.push(x);
      }
    }
    else {
      var y = new DisciplinePositionObject()
      KOFormatter.Deserialise({ DisciplineID: obj.DisciplineID(), PositionID: obj.PositionID() }, y)
      var index = ViewModel.Report().ReportCriteriaGeneric().Positions.indexOf(y);
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().Positions.splice(index, 1);
      }
    }
  },
  AddHumanResource: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.push(obj.HumanResourceID());
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.indexOf(obj.HumanResourceID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().HumanResourceIDs.splice(index, 1);
      }
    }
  },

  GetAllowedDisciplines: function () {
    Singular.GetDataStateless('OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineListSelect, OBLib', {
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID()
    },
      function (response) {
        if (response.Success) {
          ViewModel.Report().ReportCriteriaGeneric().RODisciplineList.Set(response.Data)
          if (ViewModel.Report().ReportCriteriaGeneric().RODisciplineList().length > 0) {
            $("#Disciplines").removeClass('FadeHide');
            $("#Disciplines").addClass('FadeDisplay');
          }
          else {
            ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs.Set([]);
            $("#Disciplines").removeClass('FadeDisplay');
            $("#Disciplines").addClass('FadeHide');
          }
        }
        //Singular.HideLoadingBar();
      })
    return ViewModel.Report().ReportCriteriaGeneric().RODisciplineList;
  },

  GetAllowedPositions: function () {
    Singular.GetDataStateless('OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeListSelect, OBLib', {
      SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID(),
      ProductionAreaID: ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(),
      DisciplineIDs: ViewModel.Report().ReportCriteriaGeneric().DisciplineIDs()
    },
      function (response) {
        if (response.Success) {
          ViewModel.Report().ReportCriteriaGeneric().ROPositionList.Set(response.Data)
        }
      })
  }
};

TimesheetNewPolicyReport = {
AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemID(obj.SystemID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(null);
      }
    }
  }
};

//#endregion

//#region Studio Reports

StudioBookingsShiftReport = {
  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
    }
    //ViewModel.ROHumanResourceFindListCriteria().SystemIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().SystemIDs()));
    //this.GetAllowedHumanResources();
  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
    }
    //ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()));
    //this.GetAllowedHumanResources();
  },
};

ServicesStudioDataChecksReport = {
  AddSystem: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().SystemIDs.push(obj.SystemID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().SystemIDs.indexOf(obj.SystemID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs.splice(index, 1);
      }
    }
    //ViewModel.ROHumanResourceFindListCriteria().SystemIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().SystemIDs()));
    //this.GetAllowedHumanResources();
  },
  AddProductionArea: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.push(obj.ProductionAreaID());
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
      if (index > -1) {
        ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs.splice(index, 1);
      }
    }
    //ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs(OBMisc.Xml.getXmlIDs(ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs()));
    //this.GetAllowedHumanResources();
  },
};

//#endregion

//#region Users

UserActionSummaryReport = {
  AddSystem: function (obj)
  {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.UserSystemList().Iterate(function (us, usIndx)
    {
      us.IsSelected(false)
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx)
    {
      if (us.IsSelected())
      {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
      }
    })
  }
};

//#endregion

//#region Equipment

EquipmentWorkOrderReport = {
  AddEquipment: function (obj) {
    obj.IsSelected(!obj.IsSelected());
    if (obj.IsSelected()) {
      if (ViewModel.Report().ReportCriteriaGeneric().EquimentIDs.indexOf(obj.EquimentID()) == -1) {
        ViewModel.Report().ReportCriteriaGeneric().EquimentIDs.push(obj.EquimentID());
        EquipmentWorkOrderReport.AddEquipment();
      }
    }
    else {
      var index = ViewModel.Report().ReportCriteriaGeneric().EquimentIDs.indexOf(obj.EquimentID());
      if (index != -1) {
        ViewModel.Report().ReportCriteriaGeneric().EquimentIDs.splice(index, 1);
        EquipmentWorkOrderReport.AddEquipment();
      }
    }
  },
  AddSystem: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.IsSelected(false)
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
        Singular.GetDataStateless('OBLib.Equipment.ReadOnly.ROCircuitSelectList, OBLib', {
          EquipmentName: null,
          StartDateTime: ViewModel.Report().ReportCriteriaGeneric().StartDate(),
          EndDateTime: ViewModel.Report().ReportCriteriaGeneric().EndDate(),
          SystemID: ViewModel.Report().ReportCriteriaGeneric().SystemID()
        },
        function (response) {
          if (response.Success) {
            ViewModel.Report().ReportCriteriaGeneric().ROCircuitSelectList.Set(response.Data)
          }
        })
      }
    })
  }
};

//#endregion

//#region Notifications

SmsDeliveryReport = {
  AddSystem: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.Report().ReportCriteriaGeneric().SystemIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.IsSelected(false)
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        usa.IsSelected(false)
      })
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs([us.SystemID()])
      }
    })
  },
  AddArea: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null)
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        usa.IsSelected(false)
      })
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
          if (usa.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(usa.ProductionAreaID())
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([usa.ProductionAreaID()])
          }
        })
      }
    })
  },

  ScenarioSet: function (crit) {
    if (crit.Scenario() == 0) {
      crit.StartDay(null)
      crit.EndDay(null)
      crit.ProductionSystemAreaStartDate(null)
      crit.ProductionSystemAreaEndDate(null)
      crit.ProductionSystemAreaID(null)
      crit.SmsBatchID(null)
      crit.SmsBatchCrewTypeID(null)
    } else if (crit.Scenario() == 1) {
      //crit.StartDay(null)
      //crit.EndDay(null)
      crit.ProductionSystemAreaStartDate(null)
      crit.ProductionSystemAreaEndDate(null)
      crit.ProductionSystemAreaID(null)
      crit.SmsBatchID(null)
      crit.SmsBatchCrewTypeID(null)
    } else if (crit.Scenario() == 2) {
      crit.StartDay(null)
      crit.EndDay(null)
      //crit.ProductionSystemAreaStartDate(null)
      //crit.ProductionSystemAreaEndDate(null)
      //crit.ProductionSystemAreaID(null)
      crit.SmsBatchID(null)
      crit.SmsBatchCrewTypeID(null)
    } else if (crit.Scenario() == 3) {
      crit.StartDay(null)
      crit.EndDay(null)
      crit.ProductionSystemAreaStartDate(null)
      crit.ProductionSystemAreaEndDate(null)
      crit.ProductionSystemAreaID(null)
      //crit.SmsBatchID(null)
      //crit.SmsBatchCrewTypeID(null)
    }
  },

  //psa
  triggerProductionSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.SystemIDs = [args.Object.SystemID()]
    args.Data.ProductionAreaIDs = [args.Object.ProductionAreaID()]
    args.Data.StartDate = args.Object.ProductionSystemAreaStartDate()
    args.Data.EndDate = args.Object.ProductionSystemAreaEndDate()
  },
  afterProductionSystemAreaRefreshAjax: function (args) {

  },
  ProductionSystemAreaIDSet: function (self) {

  },
  onProductionSystemAreaIDSelected: function (item, businessObject) {

  },

  //user
  triggerCreatedByUserIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setCreatedByUserIDCriteriaBeforeRefresh: function (args) {

  },
  afterCreatedByUserIDRefreshAjax: function (args) {

  },
  CreatedByUserIDSet: function (self) {

  },
  onUserIDSelected: function (item, businessObject) {

  },

  //batch
  triggerSmsBatchIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setSmsBatchIDCriteriaBeforeRefresh: function (args) {
    args.Data.CreatedBy = args.Object.CreatedByUserID()
    args.Data.CreatedStartDate = args.Object.CreatedStartDate()
    args.Data.CreatedEndDate = args.Object.CreatedEndDate()
  },
  afterSmsBatchIDRefreshAjax: function (args) {

  },
  SmsBatchIDSet: function (self) {

  },
  onSmsBatchIDSelected: function (item, businessObject) {

  },

  triggerSmsBatchCrewTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setSmsBatchCrewTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SmsBatchID = args.Object.SmsBatchID()
  },
  afterSmsBatchCrewTypeIDRefreshAjax: function (args) {

  },
  SmsBatchCrewTypeIDSet: function (self) {

  },
  onSmsBatchCrewTypeIDSelected: function (item, businessObject) {

  },

  //recipient
  triggerRecipientHumanResourceIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setRecipientHumanResourceIDCriteriaBeforeRefresh: function (args) {
    args.Data.PageNo = 1
    args.Data.PageSize = 1000
    args.Data.SortColumn = "FirstName"
    args.Data.SortAsc = true
  },
  afterRecipientHumanResourceIDRefreshAjax: function (args) {

  },
  RecipientHumanResourceIDSet: function (self) {

  },
  onRecipientHumanResourceIDSelected: function (item, businessObject) {

  }
};

SmsDeliveryOBEventReport = {
  AddSystem: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().SystemID(null)
    ViewModel.Report().ReportCriteriaGeneric().SystemIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.IsSelected(false)
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        usa.IsSelected(false)
      })
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        ViewModel.Report().ReportCriteriaGeneric().SystemID(us.SystemID())
        ViewModel.Report().ReportCriteriaGeneric().SystemIDs([us.SystemID()])
      }
    })
  },
  AddArea: function (obj) {
    //reset
    var prevValue = obj.IsSelected()
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(null)
    ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([])
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
        usa.IsSelected(false)
      })
    })
    //set the new value
    obj.IsSelected(!prevValue)
    //set the crit value
    ViewModel.UserSystemList().Iterate(function (us, usIndx) {
      if (us.IsSelected()) {
        us.UserSystemAreaList().Iterate(function (usa, usaIndx) {
          if (usa.IsSelected()) {
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaID(usa.ProductionAreaID())
            ViewModel.Report().ReportCriteriaGeneric().ProductionAreaIDs([usa.ProductionAreaID()])
          }
        })
      }
    })
  },
  canEdit: function (what, reportCriteria) {
    switch (what) {
      case 'ProductionSystemAreaStartDate':
      case 'ProductionSystemAreaEndDate':
      case 'ProductionSystemAreaID':
        return (reportCriteria.SystemID() && reportCriteria.ProductionAreaID())
      default:
        return true;
        break;
    }
  },

  //psa
  triggerProductionSystemAreaAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setProductionSystemAreaCriteriaBeforeRefresh: function (args) {
    args.Data.SystemIDs = [args.Object.SystemID()]
    args.Data.ProductionAreaIDs = [args.Object.ProductionAreaID()]
    args.Data.StartDate = args.Object.ProductionSystemAreaStartDate()
    args.Data.EndDate = args.Object.ProductionSystemAreaEndDate()
  },
  afterProductionSystemAreaRefreshAjax: function (args) {

  },
  ProductionSystemAreaIDSet: function (self) {

  },
  onProductionSystemAreaIDSelected: function (item, businessObject) {

  }

};

//#endregion