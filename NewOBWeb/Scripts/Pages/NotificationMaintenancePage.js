﻿NotificationMaintenancePage = {
  GetNotificationGroups: function (ForNotificationGroupID) {
    ViewModel.NotificationGroupsBusy(true);
    Singular.GetDataStateless("OBLib.Notifications.ReadOnly.RONotificationGroupList, OBLib",
      {
        NotificationGroupID: ForNotificationGroupID,
        CheckForUserID: ViewModel.CurrentUserID()
      },
      function (response) {
        ViewModel.RONotificationGroups.Set(response.Data);
        ViewModel.NotificationGroupsBusy(false);
        NotificationMaintenancePage.SetContacts(null, ViewModel.NewNotificationGroup().NotificationGroupID(), function (response) {
          var contacts = response.Data;
          if (contacts) {
            ViewModel.CurrentUserNotifictionGroupContacts.Set(contacts);
            ViewModel.ShowGroupContacts(true);
          }
        });
      });
  },
  SetNotificationGroups: function (ForNotificationGroupID) {
    Singular.GetDataStateless("OBLib.Notifications.ReadOnly.RONotificationGroupList, OBLib",
      {
        NotificationGroupID: ForNotificationGroupID,
        CheckForUserID: ViewModel.CurrentUserID()
      },
      function (response) {
        ViewModel.RONotificationGroups.Set(response.Data);
      });
  },
  AddNewGroup: function () {
    Singular.SendCommand("NewNotificationGroup", {}, function (response) {
      NotificationMaintenancePage.ShowNewNotificationGroup();
    });
  },
  ShowNewNotificationGroup: function () {
    $('#NewNotificationGroup').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(ViewModel.NewNotificationGroup());
    });
    $("#NewNotificationGroup").modal();
  },
  CancelNewNotificationGroup: function () {
    Singular.SendCommand("CancelNewNotificationGroup", {}, function (response) {
      $("#NewNotificationGroup").modal('hide');
    });
  },
  SaveNewNotificationGroup: function () {
    Singular.SendCommand("SaveNewNotificationGroup", {}, function (response) {
      if (response.Success) {
        $("#NewNotificationGroup").modal('hide');
        NotificationMaintenancePage.GetNotificationGroups(null);
      } else {

      }
    })
  },
  CommitContact: function () {
    ViewModel.NewNotificationGroupContactList.push(ViewModel.NewNotificationGroupContact());
    ViewModel.NewNotificationGroupContact(new NotificationGroupContactObject());
    Singular.Validation.CheckRules(ViewModel.NewNotificationGroupContact());
  },
  GetContacts: function (RONotificationGroup) {
    ViewModel.ContactsToDelete([]);
    ViewModel.CurrentNotificationGroupID(RONotificationGroup.NotificationGroupID());
    NotificationMaintenancePage.SetContacts(RONotificationGroup, null, function (response) {
      var contacts = response.Data;
      if (contacts) {
        ViewModel.CurrentUserNotifictionGroupContacts.Set(contacts);
        ViewModel.ShowGroupContacts(true);
      }
    });
  },
  SetContacts: function (RONotificationGroup, NotificationGroupID, afterFetch) {
    var IDtoFetch = (RONotificationGroup ? RONotificationGroup.NotificationGroupID() : NotificationGroupID);
    ViewModel.CurrentNotificationGroupID(IDtoFetch);
    if (IDtoFetch) {
      ViewModel.NotificationGroupContactsBusy(true);
      Singular.GetDataStateless("OBLib.Notifications.ReadOnly.RONotificationGroupContactList, OBLib",
        {
          NotificationGroupID: IDtoFetch
        },
        function (response) {
          if (afterFetch) {
            afterFetch(response);
          };
          ViewModel.NotificationGroupContactsBusy(false);
        });
    }
  },
  CancelNewGroupContacts: function () {
    ViewModel.NewNotificationGroupContact(new NotificationGroupContactObject());
    ViewModel.NewNotificationGroupContactList([]);
    NotificationMaintenancePage.HideNewContactsModal();
  },
  SaveNewGroupContacts: function () {
    Singular.SendCommand("SaveNewGroupContacts", {
      NotificationGroupID: ViewModel.CurrentNotificationGroupID()
    }, function (response) {
      if (response.Success) {

        ////replace the ROGroup with the updated one
        //var grp = ViewModel.RONotificationGroups().Find("NotificationGroupID", response.Data.NotificationGroupID);
        //if (grp) {
        //  grp.Set(response.Data);
        //};
        //replace the ROGroup with the updated one
        NotificationMaintenancePage.SetNotificationGroups(null);

        //update the ROContacts
        NotificationMaintenancePage.SetContacts(null, ViewModel.CurrentNotificationGroupID(), function (response) {
          var contacts = response.Data;
          if (contacts) {
            ViewModel.CurrentUserNotifictionGroupContacts.Set(contacts);
            ViewModel.ShowGroupContacts(true);
            ViewModel.CurrentNotificationGroupID(null);
            NotificationMaintenancePage.HideNewContactsModal();
          }
        });

      } else {

      }
    });
  },
  AddNewContact: function () {
    ViewModel.NewNotificationGroupContact(new NotificationGroupContactObject());
    NotificationMaintenancePage.ShowNewContactsModal();
  },
  ShowNewContactsModal: function () {
    $('#NewNotificationGroupContact').on('shown.bs.modal', function (e) {
      Singular.Validation.CheckRules(ViewModel.NewNotificationGroupContact());
    });
    $("#NewNotificationGroupContact").modal();
  },
  HideNewContactsModal: function () {
    $("#NewNotificationGroupContact").modal('hide');
  },
  ContactClicked: function (ROContact, Element) {
    var s = $(Element);
    if (s.hasClass('selected')) {
      s.removeClass('selected');
      ViewModel.ContactsToDelete().pop(ROContact.NotificationGroupContactID());
    } else {
      s.addClass('selected');
      ViewModel.ContactsToDelete().push(ROContact.NotificationGroupContactID());
    }
    $(Element)
  },
  DeleteSelectedContacts: function () {
    Singular.SendCommand("DeleteSelectedContacts", {}, function (response) {
      if (response.Success) {
        //replace the ROGroup with the updated one
        NotificationMaintenancePage.SetNotificationGroups(null);
        //update the ROContacts
        NotificationMaintenancePage.SetContacts(null, ViewModel.CurrentNotificationGroupID(), function (response) {
          var contacts = response.Data;
          if (contacts) {
            ViewModel.CurrentUserNotifictionGroupContacts.Set(contacts);
            ViewModel.ShowGroupContacts(true);
            //ViewModel.CurrentNotificationGroupID(null);
            NotificationMaintenancePage.HideNewContactsModal();
          }
        });
      }
    });
  }
};