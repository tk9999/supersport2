﻿ROChannelBO.IsSelectedSet = function (self) {

  if (self.IsSelected()) {
    var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
    if (!item) {
      self.ChannelID()
      var ni = new SelectedItemObject()
      ni.ID(self.ChannelID())
      ni.Description(self.ChannelShortName())
      ViewModel.SelectedChannelItems().push(ni)
    }
  }
  else {
    var item = ViewModel.SelectedChannelItems().Find("ID", self.ChannelID())
    if (item) {
      ViewModel.SelectedChannelItems().pop(item)
    }
  }

  var xmlCriteria = ""
  if (ViewModel.SelectedChannelItems().length > 0) {
    xmlCriteria = '<DataSet>'
    ViewModel.SelectedChannelItems().Iterate(function (item, indx) {
      xmlCriteria += "<Table ID='" + item.ID().toString() + "' />";
    })
    xmlCriteria += '</DataSet>'
  }

  ViewModel.RoomAllocatorCriteria().SelectedChannelIDsXML(xmlCriteria)

}

AllocatorChannelListCriteriaBO = {
  SystemIDSet: function (self) {
    ViewModel.FreezeSetExpressions(true)
    ViewModel.RoomAllocatorList().Iterate(function (itm, indx) {
      itm.SystemID(self.SystemID())
      if (ClientData.ROUserSystemAreaList.length == 1) {
        itm.ProductionAreaID(ClientData.ROUserSystemAreaList[0].ProductionAreaID)
      }
    })
    ViewModel.FreezeSetExpressions(false)
  },
  StartDateSet: function (self) {
    ViewModel.RoomAllocatorCriteria().StartDate(self.StartDate())
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      RoomAllocatorPage.refreshActiveTab()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  EndDateSet: function (self) {
    ViewModel.RoomAllocatorCriteria().EndDate(self.EndDate())
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      RoomAllocatorPage.refreshActiveTab()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  LiveSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Live(self.Live())
    RoomAllocatorPage.refreshActiveTab()
  },
  DelayedSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Delayed(self.Delayed())
    RoomAllocatorPage.refreshActiveTab()
  },
  PremierSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Premier(self.Premier())
    RoomAllocatorPage.refreshActiveTab()
  },
  GenRefNoSet: function (self) {
    ViewModel.RoomAllocatorCriteria().GenRefNo(self.GenRefNo())
    RoomAllocatorPage.refreshActiveTab()
  },
  GenRefNoStringSet: function (self) {
    ViewModel.RoomAllocatorCriteria().GenRefNoString(self.GenRefNoString())
    RoomAllocatorPage.refreshActiveTab()
  },
  PrimaryChannelsOnlySet: function (self) {
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      self.AllChannels(false)
      ViewModel.ROChannelList().Iterate(function (ch, chInd) {
        if (self.PrimaryChannelsOnly()) {
          if (ch.PrimaryInd()) { if (!ch.IsSelected()) { ch.IsSelected(true) } }
          else {
            ch.IsSelected(false)
          }
        } else {
          ch.IsSelected(false)
        }
      })
      RoomAllocatorPage.refreshActiveTab()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  AllChannelsSet: function (self) {
    if (!ViewModel.FreezeSetExpressions()) {
      ViewModel.FreezeSetExpressions(true)
      self.PrimaryChannelsOnly(false)
      ViewModel.ROChannelList().Iterate(function (ch, chInd) {
        if (self.AllChannels()) {
          ch.IsSelected(true)
        } else {
          ch.IsSelected(false)
        }
      })
      RoomAllocatorPage.refreshActiveTab()
      ViewModel.FreezeSetExpressions(false)
    }
  },
  GenreSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Genre(self.Genre())
    RoomAllocatorPage.refreshActiveTab()
  },
  SeriesSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Series(self.Series())
    RoomAllocatorPage.refreshActiveTab()
  },
  TitleSet: function (self) {
    ViewModel.RoomAllocatorCriteria().Title(self.Title())
    RoomAllocatorPage.refreshActiveTab()
  },
  channelSelected: function (channel) {
    channel.IsSelected(!channel.IsSelected())
    RoomAllocatorPage.refreshActiveTab()
  }
}

Availability = {
  checkRoomAvailability: function (obj, StartDateTime, EndDateTime, afterCheck) {
    if (obj.RoomID()) {
      var room = ClientData.RORoomList.Find("RoomID", obj.RoomID())
      OBMisc.Resources.getROResourceBookings(StartDateTime, EndDateTime, room.ResourceID, null, null, null, null, null,
        function (response) {
          obj.RoomClashCount(response.Data.length)
          afterCheck(response)
        },
        function (response) {
          OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
          afterCheck(response)
        })
    }
    else {
      obj.RoomClashCount(0)
      afterCheck(null)
    }
  },
  checkMCRContAvailability: function (obj, StartDateTime, EndDateTime, afterCheck) {
    if (obj.ResourceIDMCRController()) {
      obj.IsProcessing(true)
      OBMisc.Resources.getROResourceBookings(StartDateTime, EndDateTime, obj.ResourceIDMCRController(), null, null, null, null, null,
        function (response) {
          obj.MCRControllerClashCount(response.Data.length)
          afterCheck(response)
        },
        function (response) {
          OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
          afterCheck(response)
        })
    }
    else {
      obj.MCRControllerClashCount(0)
      obj.IsProcessing(false)
    }
  },
  checkSCCROpAvailability: function (obj, StartDateTime, EndDateTime, afterCheck) {
    if (obj.ResourceIDSCCROperator()) {
      OBMisc.Resources.getROResourceBookings(StartDateTime, EndDateTime, obj.ResourceIDSCCROperator(), null, null, null, null, null,
        function (response) {
          obj.SCCROperatorClashCount(response.Data.length)
          afterCheck(response)
        },
        function (response) {
          OBMisc.Notifications.GritterError("Clash Check Failed", response.ErrorText, 3000)
          afterCheck(response)
        })
    }
    else {
      obj.SCCROperatorClashCount(0)
      afterCheck(null)
    }
  },
  checkAllAvailability: function (obj, StartDateTime, EndDateTime) {
    var me = this
    obj.IsProcessing(true)
    me.checkRoomAvailability(obj, StartDateTime, EndDateTime,
      function (response) {
        //me.checkMCRContAvailability(obj, StartDateTime, EndDateTime, function (response) {
        //  me.checkSCCROpAvailability(obj, StartDateTime, EndDateTime, function (response) {
        //    obj.IsProcessing(false)
        //  })
        //})
      })
  }
}

SynergyEventChannelBO = {
  CanEdit: function (instance, propertyName) {
    switch (propertyName) {
      case "ChannelButton":
        var parent = instance.GetParent()
        var settings = ClientData.ROSystemProductionAreaList.Filter("SystemID", parent.SystemID()).Filter("ProductionAreaID", parent.ProductionAreaID())
        var setting = null
        if (settings.length == 1) {
          return settings[0].RequiresChannelsForRoomScheduling
        }
        return false
      default:
        return !instance.IsProcessing()
        break;
    }
  },
  channelClicked: function (self) {
    self.IsSelected(!self.IsSelected())
  },
  buttonHTML: function (self) {
    if (self.RoomScheduleCount() > 0) {
      return self.ChannelShortName() + ' <span class="badge badge-white">' + self.RoomScheduleCount().toString() + '</span>'
    } else {
      return self.ChannelShortName()
    }
  },
  buttonCSS: function (self) {
    if (self.IsSelected()) {
      return "btn btn-xs btn-primary"
    }
    else {
      return "btn btn-xs btn-default"
    }
  }
}

ROSynergyChangePagedListCriteriaBO = {
  GenRefNumberSet: function (self) { RoomAllocatorPage.refreshSynergyChanges() },
  ScheduleNumberSet: function (self) { RoomAllocatorPage.refreshSynergyChanges() },
  ChangeStartDateTimeSet: function (self) { RoomAllocatorPage.refreshSynergyChanges() },
  ChangeEndDateTimeSet: function (self) { RoomAllocatorPage.refreshSynergyChanges() },
  SynergyChangeTypeIDSet: function (self) { RoomAllocatorPage.refreshSynergyChanges() },
  ColumnNameSet: function (self) { RoomAllocatorPage.refreshSynergyChanges() }
}

RoomAllocatorPage = {
  currentRoomAllocator: function () {
    if (ViewModel.CurrentRoomAllocatorGuid()) {
      return ViewModel.RoomAllocatorList().Find("Guid", ViewModel.CurrentRoomAllocatorGuid())
    }
    return null;
  },
  Refresh: function () {
    ViewModel.RoomAllocatorListManager().Refresh()
  },
  resultBadgeHTML: function () {

  },
  showResultHistory: function () {

  },
  showChannelSelector: function () {
    if ($("#ChannelSelectPannel").hasClass("flipInX go")) {
      $("#ChannelSelectPannel").removeClass("flipInX go")
      $("#ChannelSelectPannel").addClass("flipOutX go")
      window.setTimeout(function () {
        $("#ChannelSelectPannel").css('display', 'none')
      }, 750)
    }
    else {
      $("#ChannelSelectPannel").removeClass("flipOutX go")
      $("#ChannelSelectPannel").css('display', 'block')
      $("#ChannelSelectPannel").addClass("flipInX go")
    }
  },
  showRoomChooser: function (RoomAllocator, element) {
    console.log(RoomAllocator)
    console.log(element)
  },
  showAllocatorChannelFilters: function () {
    $("#ChannelEventFilters").modal()
  },
  getSlugs: function (SynergyEvent) {
    SynergyEvent.IsProcessing(true)
    OBMisc.Slugs.getSlugs(SynergyEvent.GenRefNumber(),
      function (response) {
        ViewModel.CurrentSynergyEvent().IsProcessing(false)
        SynergyEvent.SlugItemList.Set(response.Data)
      },
      function (response) {
        ViewModel.CurrentSynergyEvent().IsProcessing(false)
        OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000)
      })
  },
  getSlugsSynergyEvent: function () {
    ViewModel.CurrentSynergyEvent().IsProcessing(true)
    OBMisc.Slugs.getSlugs(ViewModel.CurrentSynergyEvent().GenRefNumber(),
      function (response) {
        ViewModel.CurrentSynergyEvent().IsProcessing(false)
        ViewModel.CurrentSynergyEvent().SlugItemList.Set(response.Data)
      },
      function (response) {
        ViewModel.CurrentSynergyEvent().IsProcessing(false)
        OBMisc.Notifications.GritterError("Error Fetching Slugs", response.ErrorText, 3000)
      })
  },
  showAllocatorChannelModal: function () {
    $("#CurrentAllocatorChannelEventModal").off("shown.bs.modal")
    $("#CurrentAllocatorChannelEventModal").on("shown.bs.modal", function () {
      RoomAllocatorPage.getSlugsSynergyEvent()
    })
  },
  getSelectedChannelXml: function () {
    //channel selection
    var selectedChannelIDs = []
    ViewModel.ROChannelList().Iterate(function (ch, chInd) {
      if (ch.IsSelected()) { selectedChannelIDs.push(ch.ChannelID()) }
    })
    return OBMisc.Xml.getXmlIDs(selectedChannelIDs)
  },
  refreshGridByChannel: function () {
    //var channelIDXml = RoomAllocatorPage.getSelectedChannelXml()
    //ViewModel.RoomAllocatorCriteria().SelectedChannelIDsXML(channelIDXml)
    //ViewModel.RoomAllocatorCriteria().PageNo(1)
    //ViewModel.RoomAllocatorCriteria().PageSize(5)
    var channelIDXml = this.getSelectedChannelXml()
    ViewModel.RoomAllocatorCriteria().PageNo(1)
    ViewModel.RoomAllocatorCriteria().PageSize(5)
    ViewModel.RoomAllocatorCriteria().StartDate(ViewModel.AllocatorChannelCriteria().StartDate())
    ViewModel.RoomAllocatorCriteria().EndDate(ViewModel.AllocatorChannelCriteria().EndDate())
    ViewModel.RoomAllocatorCriteria().SelectedChannelIDsXML(channelIDXml)
    ViewModel.RoomAllocatorCriteria().SystemID(ViewModel.AllocatorChannelCriteria().SystemID())
    ViewModel.RoomAllocatorCriteria().ProductionAreaID(ViewModel.AllocatorChannelCriteria().ProductionAreaID())
    ViewModel.RoomAllocatorCriteria().Live(ViewModel.AllocatorChannelCriteria().Live())
    ViewModel.RoomAllocatorCriteria().Delayed(ViewModel.AllocatorChannelCriteria().Delayed())
    ViewModel.RoomAllocatorCriteria().Premier(ViewModel.AllocatorChannelCriteria().Premier())
    ViewModel.RoomAllocatorCriteria().GenRefNo(ViewModel.AllocatorChannelCriteria().GenRefNo())
    ViewModel.RoomAllocatorCriteria().Genre(ViewModel.AllocatorChannelCriteria().Genre())
    ViewModel.RoomAllocatorCriteria().Series(ViewModel.AllocatorChannelCriteria().Series())
    ViewModel.RoomAllocatorCriteria().Title(ViewModel.AllocatorChannelCriteria().Title())
    ViewModel.RoomAllocatorCriteria().GenRefNoString(ViewModel.AllocatorChannelCriteria().GenRefNoString())
    ViewModel.RoomAllocatorCriteria().Keyword(ViewModel.AllocatorChannelCriteria().Keyword())
    ViewModel.RoomAllocatorListManager().Refresh()
  },
  showSynergyEventTimes: function (synergyEvent, buttonElement) {
    var me = this
    ViewModel.CurrentSynergyEventGuid(synergyEvent.Guid())
    $("#SynergyEventModal").off('shown.bs.modal')
    $("#SynergyEventModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(me.currentSynergyEvent())
    })
    $("#SynergyEventModal").modal()
    //this.showCurrentSynergyEventTimesPopup(synergyEvent, buttonElement)
  },
  showCurrentSynergyEventTimesPopup: function (synergyEvent, buttonElement) {
    //$("#EditTimesPanelSynergyEvent").draggable()
    $("#EditTimesPanelSynergyEvent").show()
    var dimensionProperties = buttonElement.getBoundingClientRect()
    var elem = document.getElementById("EditTimesPanelSynergyEvent")
    elem.style.display = "block"
    elem.style.position = "absolute"
    elem.style.top = dimensionProperties.top.toString() + "px"
    elem.style.left = dimensionProperties.left.toString() + "px"
    //synergyEvent.CallTime.BoundElements[0].focus()
  },
  doneEditingTimes: function (synergyEvent, buttonElement) {
    ViewModel.CurrentSynergyEventGuid(null)
    this.hideCurrentSynergyEventTimesPopup(synergyEvent, buttonElement)
  },
  hideCurrentSynergyEventTimesPopup: function (synergyEvent, doneButtonElement) {
    var elem = document.getElementById("EditTimesPanelSynergyEvent")
    elem.style.display = "none"
  },
  showRoomAllocatorEventTimes: function (roomAllocator, buttonElement) {
    var me = this
    ViewModel.CurrentRoomAllocatorGuid(roomAllocator.Guid())
    $("#RoomAllocatorModal").off('shown.bs.modal')
    $("#RoomAllocatorModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(me.currentRoomAllocator())
    })
    $("#RoomAllocatorModal").modal()
  },
  showFilters: function () {
    $("#SynergyFilterModal").modal()
  },
  selectedDatesHTML: function (crit) {
    return new Date(crit.StartDate()).format('ddd dd MMM') + ' - ' + new Date(crit.EndDate()).format('ddd dd MMM')
  },
  refreshSynergyChanges: function () {
    ViewModel.ROSynergyChangePagedListManager().Refresh()
  },
  refreshActiveTab: function () {
    var activeTab = $('ul.nav-tabs .active'),
      activeTabID = null
    if (activeTab) {
      activeTabID = activeTab.find(' > a').attr('href')
    }
    if (activeTabID) {
      switch (activeTabID) {
        case "#GridViewByChannel":
          RoomAllocatorPage.refreshGridByChannel()
          break;
      }
    }
  },
  showRoomScheduleModal: function () {
    $("#RoomAllocatorRoomScheduleModal").off('shown.bs.modal')
    $("#RoomAllocatorRoomScheduleModal").off('hidden.bs.modal')
    $("#RoomAllocatorRoomScheduleModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentRoomAllocatorRoomSchedule())
    })
    $("#RoomAllocatorRoomScheduleModal").on('hidden.bs.modal', function () {
      ViewModel.CurrentRoomAllocatorRoomSchedule(null)
      RoomAllocatorPage.refreshActiveTab()
    })
    $("#RoomAllocatorRoomScheduleModal").modal()
  },
  hideRoomScheduleModal: function () {
    $("#RoomAllocatorRoomScheduleModal").modal('hide')
  },
  currentRoomScheduleTitle: function () {
    if (ViewModel.CurrentRoomAllocatorRoomSchedule()) {
      return ViewModel.CurrentRoomAllocatorRoomSchedule().Title()
    }
    return ""
  },
  saveCurrentRoomAllocatorRoomSchedule: function () {
    var me = this;
    ViewModel.CurrentRoomAllocatorRoomSchedule().IsProcessing(true);
    RoomAllocatorRoomScheduleBO.saveItem(ViewModel.CurrentRoomAllocatorRoomSchedule, {
      onSuccess: function (response) {
        OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
        window.SiteHub.server.sendNotifications()
        RoomAllocatorBO.get({
          criteria: {
            SystemID: ViewModel.AllocatorChannelCriteria().SystemID(),
            ProductionAreaID: ViewModel.AllocatorChannelCriteria().ProductionAreaID(),
            StartDate: null,
            EndDate: null,
            Live: true,
            Delayed: true,
            Premier: true,
            GenRefNo: ViewModel.CurrentRoomAllocatorRoomSchedule().GenRefNumber()
          },
          onSuccess: function (response) {
            me.hideRoomScheduleModal()
            ViewModel.RoomAllocatorListManager().Refresh()
            ViewModel.CurrentRoomAllocatorRoomSchedule().IsProcessing(false);
            //response.Data.Iterate(function (upd, updIndx) {
            //  var sched = ViewModel.RoomAllocatorList().Find("ScheduleNumber", upd.ScheduleNumber)
            //  if (sched) {
            //    OBMisc.Knockout.updateObservable(sched, upd)
            //  }
            //})
          },
          onFail: function (response) {
            ViewModel.CurrentRoomAllocatorRoomSchedule().IsProcessing(false);
          }
        })
      },
      onFail: function (response) {
        OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
      }
    })
  }
}

Singular.OnPageLoad(function () {
  ViewModel.AllocatorChannelCriteria().StartDate(new Date())
  ViewModel.AllocatorChannelCriteria().EndDate(new Date())
  $(" a[href='#GridViewByChannel']").tab('show')
  RoomAllocatorPage.refreshActiveTab()
})