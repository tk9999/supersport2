﻿EmailsPage = {
  //Common
  OnPageLoad: function () {
    Singular.HideLoadingBar()
    Singular.DownloadPath = function (doc) {
      var pguid = $('#hPageGuid').val();
      var dPath = Singular.RootPath + '/Library/FileDownloader.ashx?';
      return dPath + 'DocumentID=' + Singular.UnwrapFunction(doc.DocumentID);
    };
    ViewModel.ROEmailSearchListManager().Refresh();
  },
  Find: function () {
    ViewModel.FindEmail(true)
    ViewModel.CreateEmail(false)
    ViewModel.GenerateEmail(false)
    ViewModel.CurrentEmail(null)
    ViewModel.CurrentEmailBatch(null)
    ViewModel.ROEmailSearchListManager().Refresh()
  },

  GetSystemIDs: function () {
    var me = this;
    var SystemIDs = []
    ViewModel.UserSystemList().Iterate(function (subDept, dIndx) {
      if (subDept.IsSelected()) {
        SystemIDs.push(subDept.SystemID())
      }
    })
    return SystemIDs
  },
  GetProductionAreaIDs: function () {
    var me = this;
    var ProductionAreaIDs = []
    ViewModel.UserSystemList().Iterate(function (subDept, dIndx) {
      if (subDept.IsSelected()) {
        subDept.UserSystemAreaList().Iterate(function (prodArea, dIndx) {
          if (prodArea.IsSelected()) {
            ProductionAreaIDs.push(prodArea.ProductionAreaID())
          }
        })
      }
    })
    return ProductionAreaIDs
  },
  GetDisciplineIDs: function () {
    var me = this;
    var DisciplineIDs = [];
    ViewModel.SelectableDisciplines().Iterate(function (discipline, dIndx) {
      if (discipline.IsSelected()) {
        DisciplineIDs.push(discipline.DisciplineID())
      };
    });
    return DisciplineIDs;
  },
  GetContractTypeIDs: function () {
    var me = this;
    var ContractTypeIDs = [];
    ViewModel.SelectableContractTypes().Iterate(function (ct, dIndx) {
      if (ct.IsSelected()) {
        ContractTypeIDs.push(ct.ContractTypeID())
      };
    });
    return ContractTypeIDs;
  },

  GetFilteredDisciplines: function () {
    var me = this;
    if (!ViewModel.IsFilteringData()) {
      ViewModel.IsFilteringData(true)
      Singular.GetDataStateless('OBLib.Maintenance.General.ReadOnly.RODisciplineSelectList, OBLib', {
        SystemIDs: me.GetSystemIDs(),
        ProductionAreaIDs: me.GetProductionAreaIDs()
      }, function (response) {
        ViewModel.SelectableDisciplines.Set(response.Data);
        ViewModel.IsFilteringData(false)
      })
    }
  },
  GetFilteredHumanResources: function () {
    if (!ViewModel.IsFilteringData()) {
      var me = this;
      ViewModel.IsFilteringData(true)
      ViewModel.SelectableHumanResourcesManager().afterRefresh = function () {
        ViewModel.IsFilteringData(false)
      }
      ViewModel.SelectableHumanResourcesCriteria().SystemIDs(me.GetSystemIDs())
      ViewModel.SelectableHumanResourcesCriteria().ProductionAreaIDs(me.GetProductionAreaIDs())
      ViewModel.SelectableHumanResourcesCriteria().DisciplineIDs(me.GetDisciplineIDs())
      ViewModel.SelectableHumanResourcesCriteria().ContractTypeIDs(me.GetContractTypeIDs())
      ViewModel.SelectableHumanResourcesManager().Refresh()
    }
  },

  OnSubDeptSelect: function (ROSystem) {
    if (!ViewModel.IsFilteringData()) {
      ROSystem.IsSelected(!ROSystem.IsSelected())
      this.GetFilteredDisciplines()
      this.GetFilteredHumanResources()
    }
  },
  OnAreaSelect: function (ROProductionArea) {
    if (!ViewModel.IsFilteringData()) {
      ROProductionArea.IsSelected(!ROProductionArea.IsSelected())
      this.GetFilteredDisciplines()
      this.GetFilteredHumanResources()
    }
  },
  OnContractTypeSelect: function (ROContractType) {
    if (!ViewModel.IsFilteringData()) {
      ROContractType.IsSelected(!ROContractType.IsSelected())
      this.GetFilteredHumanResources()
    }
  },
  OnDisciplineSelect: function (RODiscipline) {
    if (!ViewModel.IsFilteringData()) {
      RODiscipline.IsSelected(!RODiscipline.IsSelected())
      this.GetFilteredHumanResources()
    }
  },

  SelectAllDisciplines: function () {
    ViewModel.SelectableDisciplines().Iterate(function (itm, indx) {
      if (!itm.IsSelected()) {
        itm.IsSelected(true);
      }
    })
    this.GetFilteredHumanResources()
  },
  ClearDisciplines: function () {
    ViewModel.SelectableDisciplines().Iterate(function (itm, itmInd) {
      itm.IsSelected(false);
    })
  },
  SelectAllHumanResources: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, indx) {
      itm.IsSelected(true)
    })
  },
  ClearRecipients: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, itmInd) {
      itm.IsSelected(false)
    })
    ViewModel.SelectedRecipients([])
  },
  SelectAllContractTypes: function () {
    ViewModel.SelectableContractTypes().Iterate(function (itm, indx) {
      if (!itm.IsSelected()) {
        itm.IsSelected(true);
      }
    })
  },
  ClearContractTypes: function () {
    ViewModel.SelectableContractTypes().Iterate(function (itm, itmInd) {
      itm.IsSelected(false);
    })
  },

  AddSelectedToEmail: function () {

  },
  AddSelectedToEmailSingle: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        var isOnEmailPrimary = ViewModel.CurrentEmail().SoberEmailRecipientList().Find('EmailAddress', itm.EmailAddress())
        var isOnEmailSecondary = ViewModel.CurrentEmail().SoberEmailRecipientList().Find('EmailAddress', itm.AlternativeEmailAddress())
        if (!isOnEmailPrimary && itm.HasValidEmailAddressPrimary()) {
          SoberEmailBO.GetNewRecipientPrimary(ViewModel.CurrentEmail(), itm)
        }
        if (!isOnEmailSecondary && itm.HasValidEmailAddressSecondary()) {
          SoberEmailBO.GetNewRecipientSecondary(ViewModel.CurrentEmail(), itm)
        }
      }
    })
    OBMisc.Notifications.GritterSuccess("Human Resources", "Successfully Added to Email List", 1500);
    this.UpdateEmailRecipientProperty(ViewModel.CurrentEmail())
  },
  DoneSelectingRecipients: function () {
    $("#SelectRecipients").modal('hide')
  },
  ROHumanResourceFindCSS: function (ROHumanResourceFind) {
    if (ROHumanResourceFind.IsSelected()) {
      return 'btn btn-xs btn-primary'
    }
    else {
      return 'btn btn-xs btn-default'
    }
  },
  ROHumanResourceFindHTML: function (ROHumanResourceFind) {
    if (ROHumanResourceFind.IsSelected()) {
      return '<i class="fa fa-check-square-o" /> '
              + '<strong>' + ROHumanResourceFind.PreferredFirstSurname() + '</strong>'
              + ' - '
              + ((ROHumanResourceFind.EmailAddress().toString().length == 0) ? '<strong class="strong-default"> No Email Address </strong>'
                                                                             : '<strong class="strong-default">' + ROHumanResourceFind.EmailAddress().toString() + '</strong>')
    }
    else {
      return '<strong>' + ROHumanResourceFind.PreferredFirstSurname() + '</strong>'
                        + ' - '
                        + ((ROHumanResourceFind.EmailAddress().toString().length == 0) ? '<strong class="strong-danger"> No Email Address </strong>'
                                                                                       : '<strong class="strong-primary">' + ROHumanResourceFind.EmailAddress().toString() + '</strong>')

    }
  },
  onROHumanResourceClicked: function (ROHumanResourceFind) {
    ROHumanResourceFind.IsSelected(!ROHumanResourceFind.IsSelected())
  },
  UpdateEmailRecipientProperty: function (Email) {
    Email.ToEmailAddress('')
    Email.SoberEmailRecipientList().Iterate(function (em, emIndx) {
      var nextEM = em.EmailAddress()
      if (nextEM.trim().length > 0) {
        if (emIndx == 0) {
          Email.ToEmailAddress(nextEM)
        }
        else {
          var current = Email.ToEmailAddress()
          Email.ToEmailAddress(current + ';' + nextEM)
        }
      }
    })
  },

  FirstHRPage: function () {
    ViewModel.SelectableHumanResourcesManager().PageNo(1)
    this.GetFilteredHumanResources()
  },
  PreviousHRPage: function () {
    var cp = ViewModel.SelectableHumanResourcesManager().PageNo()
    ViewModel.SelectableHumanResourcesManager().PageNo(cp - 1)
    if (cp - 1 < 1) {
      ViewModel.SelectableHumanResourcesManager().PageNo(ViewModel.SelectableHumanResourcesManager().Pages())
    } else {
      ViewModel.SelectableHumanResourcesManager().PageNo(cp - 1)
    }
    this.GetFilteredHumanResources()
  },
  NextHRPage: function () {
    var cp = ViewModel.SelectableHumanResourcesManager().PageNo()
    if (cp + 1 > ViewModel.SelectableHumanResourcesManager().Pages()) {
      ViewModel.SelectableHumanResourcesManager().PageNo(1)
    } else {
      ViewModel.SelectableHumanResourcesManager().PageNo(cp + 1)
    }
    this.GetFilteredHumanResources()
  },
  LastHRPage: function () {
    ViewModel.SelectableHumanResourcesManager().PageNo(ViewModel.SelectableHumanResourcesManager().Pages())
    this.GetFilteredHumanResources()
  },

  //Single Email
  NewEmail: function () {
    ViewModel.CurrentEmail(null)
    ViewModel.CurrentEmailBatch(null)
    ViewModel.CurrentEmail.Set(new SoberEmailObject())
    ViewModel.CreateEmail(true)
    ViewModel.GenerateEmail(false)
    ViewModel.FindEmail(false)
    Singular.Validation.CheckRules(ViewModel.CurrentEmail())
  },
  SelectRecipientsSingleEmail: function () {
    this.AddSelectedToEmail = this.AddSelectedToEmailSingle
    $("#SelectRecipients").modal()
  },
  EditSingleEmail: function (SoberEmail) {

  },

  //Single Email Summary Box
  TotalRecipients: function () {
    if (ViewModel.CurrentSms()) {
      return "Total Recipients: " + ViewModel.CurrentSms().SmsRecipientList().length.toString();
    } else {
      return "Total Recipients: 0";
    }
  },

  //Batch
  NewEmailBatch: function () {
    ViewModel.CurrentEmail(null)
    ViewModel.CurrentEmailBatch(null)
    ViewModel.CurrentEmailBatch(new EmailBatchObject())
    ViewModel.CreateEmail(false)
    ViewModel.GenerateEmail(true)
    ViewModel.FindEmail(false)
    Singular.Validation.CheckRules(ViewModel.CurrentEmailBatch())
  },
  GenerateBatch: function () {
    Singular.SendCommand("GenerateBatch", {
    },
    function (response) {
      if (response.Success) {
        ViewModel.ROCurrentBatchEmailListCriteria().EmailBatchID(ViewModel.CurrentEmailBatch().EmailBatchID())
        ViewModel.ROCurrentBatchEmailListManager().PageNo(1)
        ViewModel.ROCurrentBatchEmailListManager().Refresh()
      }
      else {
        OBMisc.Notifications.GritterError('Error Generating', response.ErrorText, 3000)
      }
    })
  },
  EditBatch: function (BatchID) {
    Singular.SendCommand("EditBatch",
      {
        EmailBatchID: BatchID
      },
      function (response) {
        if (response.Success) {
          ViewModel.ROCurrentBatchEmailListCriteria().EmailBatchID(ViewModel.CurrentEmailBatch().EmailBatchID())
          ViewModel.ROCurrentBatchEmailListManager().PageNo(1)
          ViewModel.ROCurrentBatchEmailListManager().Refresh()
          $("#FindBatch").modal('hide');
        } else {

        }
      });
  },
  RefreshROEmailBatchList: function () {
    ViewModel.ROBatchEmailListManager().Refresh();
  },
  SendEmails: function () {
    Singular.SendCommand("SendCurrentBatchEmails", {}, function (response) {

    });
  },
  SendEmail: function () {
    //Singular.SendCommand("Send", {}, function (response) {
    //  if (response.Success) {
    //    OBMisc.Modals.ShowMessage(null, "success", "Sms Saved", "Your Sms has been saved successfully", "check in a few minutes to see the send statuses")
    //  } else {
    //    OBMisc.Modals.ShowMessage(null, "error", "Error Saving Sms", "Your Sms could not be saved", response.ErrorText)
    //  }
    //});
  },
  SaveAndSendEmail: function () {
    Singular.SendCommand("SaveAndSend", {}, function (response) {
      if (response.Success) {
        OBMisc.Modals.ShowMessage(null, "success", "Email Saved", "Your email has been saved successfully", "check in a few minutes to see the send statuses");
        ViewModel.ROEmailSearchListManager().PageNo(1)
        ViewModel.ROEmailSearchListManager().Refresh()
      } else {
        OBMisc.Modals.ShowMessage(null, "error", "Error Saving Email", "Your email could not be saved", response.ErrorText)
      }
    });
  },
  SaveEmail: function () {
    Singular.SendCommand("SaveEmail", {}, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Success', "Email Saved Successfully", null)
        ViewModel.ROEmailSearchListManager().Refresh()
      } else {
        OBMisc.Notifications.GritterError("Error Saving", response.ErrorText, 3000)
      }
    });
  },
  onContactClicked: function (ROHumanResourceContact) {
    ROHumanResourceContact.IsSelected(!ROHumanResourceContact.IsSelected())
  },
  canSelectContact: function (ROHumanResourceContact) {
    //if (ROHumanResourceContact.HasValidMobileNumberPrimary() || ROHumanResourceContact.HasValidMobileNumberSecondary()) {
    //  return true
    //}
    //return false
    return true
  },
  SelectEmailBatchRecipientsManually: function () {
    this.AddSelectedToEmail = this.AddSelectedToEmailBatch
    $("#SelectRecipients").modal()
  },
  AddSelectedToEmailBatch: function () {
    ViewModel.SelectableHumanResources().Iterate(function (itm, indx) {
      if (itm.IsSelected()) {
        var isOnEmailBatchPrimary = ViewModel.CurrentEmailBatch().EmailBatchRecipientList().Find('EmailAddress', itm.EmailAddress())
        var isOnEmailBatchSecondary = ViewModel.CurrentEmailBatch().EmailBatchRecipientList().Find('EmailAddress', itm.AlternativeEmailAddress())
        if (!isOnEmailBatchPrimary && itm.HasValidEmailAddressPrimary()) {
          EmailBatchBO.GetNewRecipientPrimary(ViewModel.CurrentEmailBatch(), itm)
        }
        if (!isOnEmailBatchSecondary && itm.HasValidEmailAddressSecondary()) {
          EmailBatchBO.GetNewRecipientSecondary(ViewModel.CurrentEmailBatch(), itm)
        }
      }
    })
    OBMisc.Notifications.GritterSuccess("Human Resources", "Successfully Added to SMS List", 1500);
    //this.UpdateEmailRecipientProperty(ViewModel.CurrentEmail())
  },

  DeleteSelectedBatchEmails: function () {
    var me = this;
    ViewModel.ROBatchEmailListManager().SelectedItems().Iterate(function (itm, indx) {
      ViewModel.SelectedEmailIDsToDelete().push(itm.ID());
    });
    Singular.SendCommand("DeleteBatchEmails", {
    }, function (response) {
      if (response.Success) {
        if (response.Data.WarningMessage) {
          OBMisc.Modals.ShowMessage(null, "warning", "Data Saved with Warnings", "Most emails have been deleted successfully", response.Data.WarningMessage)
        }
        ViewModel.SelectedEmailIDsToDelete([])
        ViewModel.ROBatchEmailListManager().SelectedItems([])
        me.RefreshROEmailBatchList()
      } else {

      }
    });
  },
  SelectAllOnCurrentBatchPage: function () {
    var me = this;
    ViewModel.ROBatchEmailList().Iterate(function (item, indx) {
      me.OnROBatchEmailSelected(item);
    });
  },
  DeSelectAllOnCurrentBatchPage: function () {
    var me = this;
    ViewModel.ROBatchEmailList().Iterate(function (item, indx) {
      me.OnROBatchEmailSelected(item);
    });
  },
  OnROBatchEmailSelected: function (ROBatchEmail) {
    var item = ViewModel.ROBatchEmailListManager().SelectedItems().Find("ID", ROBatchEmail.EmailID());
    if (item) {
      ViewModel.ROBatchEmailListManager().SelectedItems().pop(item);
      ROBatchEmail.IsSelected(false);
    } else {
      var si = new SelectedItemObject();
      si.ID(ROBatchEmail.EmailID());
      si.Description(ROBatchEmail.Subject());
      ViewModel.ROBatchEmailListManager().SelectedItems().push(si);
      ROBatchEmail.IsSelected(true);
    }
    //return si;
  },
  IsROSoberEmailSelectedRowCss: function (ROSoberEmail) {
    if (ROSoberEmail.IsSelected()) {
      return 'items email-selected';
    } else {
      return 'items';
    }
  },
  IsROSoberEmailSelectedButtonCss: function (ROSoberEmail) {
    var item = ViewModel.ROBatchEmailListManager().SelectedItems().Find("ID", ROSoberEmail.EmailID());
    if (ROSoberEmail.IsSelected() && item) {
      return 'btn btn-primary btn-xs';
    } else {
      return 'btn btn-default btn-xs';
    }
  },
  IsROSoberEmailSelectedButtonHTML: function (ROSoberEmail) {
    var item = ViewModel.ROBatchEmailListManager().SelectedItems().Find("ID", ROSoberEmail.EmailID());
    if (ROSoberEmail.IsSelected() && item) {
      return '<i class="fa fa-check-square-o" />';
    } else {
      return '<i class="fa fa-minus" />';
    }
  },

  //Downloading

  //Sending
  SendAllEmailsForBatch: function (EmailBatch) {
    ViewModel.CallServerMethod("PrepareEmailsForSending",
      {
        EmailBatchID: EmailBatch.EmailBatchID(),
        EmailIDsXml: ""
      },
      function (response) {
        if (response.Success) {
          OBMisc.Notifications.GritterSuccess('Success', response.ErrorText + " emails will be sent in 1 minute", null)
        } else {
          OBMisc.Notifications.GritterError('Error Generating', response.ErrorText, 1500)
        }
      })
  },
  SendSelectedEmails: function () {
    var ids = []
    ViewModel.ROEmailSearchList().Iterate(function (em, ind) {
      if (em.IsSelected()) {
        ids.push(em.EmailID())
      }
    })
    if (ids.length == 0) {
      OBMisc.Notifications.GritterError('Nothing Selected', "No emails are selected", 3000)
    } else {
      var idsXml = OBMisc.Xml.getXmlIDs(ids)
      ViewModel.CallServerMethod("PrepareEmailsForSending",
        {
          EmailBatchID: null,
          EmailIDsXml: idsXml
        },
        function (response) {
          if (response.Success) {
            OBMisc.Notifications.GritterSuccess('Success', response.ErrorText + " emails will be sent in 1 minute", 1500)
          } else {
            OBMisc.Notifications.GritterError('Error Generating', response.ErrorText, 3000)
          }
        })
    }
  },
  SendSelectedEmailsForBatch: function (EmailBatch) {
    var ids = []
    ViewModel.ROCurrentBatchEmailList().Iterate(function (em, ind) {
      if (em.IsSelected()) {
        ids.push(em.EmailID())
      }
    })
    if (ids.length == 0) {
      OBMisc.Notifications.GritterError('Nothing Selected', "No emails are selected", 3000)
    } else {
      var idsXml = OBMisc.Xml.getXmlIDs(ids)
      ViewModel.CallServerMethod("PrepareEmailsForSending",
        {
          EmailBatchID: null,
          EmailIDsXml: idsXml
        },
        function (response) {
          if (response.Success) {
            OBMisc.Notifications.GritterSuccess('Success', response.ErrorText + " emails will be sent in 1 minute", 1500)
          } else {
            OBMisc.Notifications.GritterError('Error Generating', response.ErrorText, 3000)
          }
        })
    }
  },
  SelectAllOnPage: function () {
    ViewModel.ROEmailSearchList().Iterate(function (em, emIndx) {
      em.IsSelected(true)
    })
  },
  UnSelectAllOnPage: function () {
    ViewModel.ROEmailSearchList().Iterate(function (em, emIndx) {
      em.IsSelected(false)
    })
  },
  //Delete Selected
  DeleteSelected: function () {
    var me = this;
    var emailIDs = []
    var emailBatchIDs = []
    ViewModel.ROEmailSearchList().Iterate(function (em, emIndx) {
      if (em.IsSelected()) {
        if (em.IsBatch()) {
          if (em.SentCount() == 0) {
            emailBatchIDs.push(em.EmailBatchID())
          } else {
            OBMisc.Notifications.GritterWarning('Batch Excluded', 'Some emails have already been sent', 2000)
          }
        } else {
          if (em.SentCount() == 0) {
            emailIDs.push(em.EmailID())
          } else {
            OBMisc.Notifications.GritterWarning('Email Excluded', 'Some emails have already been sent', 2000)
          }
        }
      }
    })
    if (emailBatchIDs.length > 0 && emailIDs.length > 0) {
      me.DeleteEmailBatches(emailBatchIDs, function (response) {
        me.DeleteEmails(emailIDs)
      })
    } else {
      if (emailBatchIDs.length > 0) {
        me.DeleteEmailBatches(emailBatchIDs, function (response) {
        })
      }
      if (emailIDs.length > 0) {
        me.DeleteEmails(emailIDs, function (response) {
        })
      }
    }
  },
  DeleteEmailBatches: function (emailBatchIDs, afterDelete) {
    ViewModel.ROEmailSearchListManager().IsLoading(true)
    ViewModel.CallServerMethod("DeleteEmailBatches", {
      EmailBatchIDs: emailBatchIDs
    },
    function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Success', "Batch/es deleted successfully", 1500)
        ViewModel.ROEmailSearchListManager().Refresh()
      } else {
        OBMisc.Notifications.GritterError('Error Deleting', response.ErrorText, 3000)
      }
      ViewModel.ROEmailSearchListManager().IsLoading(false)
      afterDelete(response)
    })
  },
  DeleteEmails: function (emailIDs, afterDelete) {
    ViewModel.ROEmailSearchListManager().IsLoading(true)
    ViewModel.CallServerMethod("DeleteEmails", {
      EmailIDs: emailIDs
    },
    function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Success', "Email/s deleted successfully", 1500)
        ViewModel.ROEmailSearchListManager().Refresh()
      } else {
        OBMisc.Notifications.GritterError('Error Deleting', response.ErrorText, 3000)
      }
      ViewModel.ROEmailSearchListManager().IsLoading(false)
      afterDelete(response)
    })
  }

}

ROEmailSearchListCriteriaBO = {
  CreatedStartDateSet: function (self) {
    if (!ViewModel.ROEmailSearchListManager().IsLoading()) {
      ViewModel.ROEmailSearchListManager().Refresh()
    }
  },
  CreatedEndDateSet: function (self) {
    if (!ViewModel.ROEmailSearchListManager().IsLoading()) {
      ViewModel.ROEmailSearchListManager().Refresh()
    }
  },
  CreatedByMeSet: function (self) {
    if (!ViewModel.ROEmailSearchListManager().IsLoading()) {
      if (self.CreatedByMe()) {
        self.CreatedByOther("")
      }
      ViewModel.ROEmailSearchListManager().Refresh()
    }
  },
  CreatedByOtherSet: function (self) {
    if (!ViewModel.ROEmailSearchListManager().IsLoading()) {
      ViewModel.ROEmailSearchListManager().Refresh()
    }
  }
}

Singular.OnPageLoad(function () {
  Singular.HideLoadingBar()
  EmailsPage.OnPageLoad()
});