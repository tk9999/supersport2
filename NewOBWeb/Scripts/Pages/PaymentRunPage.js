﻿var hrTimeout = null

PaymentRunPage = {
  nextPagecommandName: "NextInvoice",
  detailDateSelector: null,

  findPaymentRun: function (copy) {
    if (copy) {
      PaymentRunPage.onPaymentRunSelected = PaymentRunPage.copyPaymentRun;
    } else {
      PaymentRunPage.onPaymentRunSelected = PaymentRunPage.editPaymentRun;
    }
    $('#SelectPaymentRun').on('shown.bs.modal', function () {
      ViewModel.ROPaymentRunListCriteria().PaymentRunID(null);
      ViewModel.ROPaymentRunListManager().Refresh();
    });
    $('#SelectPaymentRun').modal();
  },
  editPaymentRun: function (ROPaymentRun) {
    var me = this;
    Singular.SendCommand("EditPaymentRun", {
      PaymentRunID: ROPaymentRun.PaymentRunID()
    }, function (response) {
      ViewModel.SelectedROPaymentRun(ROPaymentRun);
      ViewModel.ROCreditorInvoiceListCriteria().PaymentRunID(ROPaymentRun.PaymentRunID());
      me.RefreshCreditorInvoiceList();
      $('#SelectPaymentRun').modal('hide');
    });

  },
  copyPaymentRun: function (ROPaymentRun) {
    var me = this;
    Singular.SendCommand("CopyPaymentRun", {
      FromPaymentRunID: ROPaymentRun.PaymentRunID(),
      ToPaymentRunID: ViewModel.CurrentPaymentRun().PaymentRunID()
    }, function (response) {
      ViewModel.ROCreditorInvoiceListCriteria().PaymentRunID(ViewModel.CurrentPaymentRun().PaymentRunID());
      me.RefreshCreditorInvoiceList();
      $('#SelectPaymentRun').modal('hide');
    });
  },

  newPaymentRun: function () {
    $("#NewPaymentRun").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentPaymentRun());
    });
    $("#NewPaymentRun").on('hidden.bs.modal', function () {

    });

    ViewModel.CurrentPaymentRun(new PaymentRunObject());
    ViewModel.CurrentPaymentRun().PaymentRunStatusID(1);
    var nd = new Date();
    //ViewModel.CurrentPaymentRun().Month(nd.getMonth());
    //ViewModel.CurrentPaymentRun().Month(nd.getMonth());
    ViewModel.CurrentPaymentRun().Year(nd.getFullYear());

    $("#NewPaymentRun").modal();
  },
  cancelNewPaymentRun: function () {
    ViewModel.CurrentPaymentRun(null);
    $("#NewPaymentRun").modal('hide');
  },
  saveNewPaymentRun: function () {
    var me = this;
    Singular.SendCommand("SavePaymentRun", {

    }, function (response) {
      $("#NewPaymentRun").modal('hide');
      ViewModel.SelectedROPaymentRun.Set(response.ROPaymentRun);
      ViewModel.ROCreditorInvoiceListCriteria().PaymentRunID(ViewModel.SelectedROPaymentRun().PaymentRunID());
      me.RefreshCreditorInvoiceList();
    });
  },
  onPaymentRunSelected: function (ROPaymentRun) {
    ViewModel.ROCreditorInvoiceListCriteria().PaymentRunID(ROPaymentRun.PaymentRunID())
    ViewModel.ROInvoicePreviewListCriteria().PaymentRunID(ROPaymentRun.PaymentRunID())
  },
  previewInvoices: function () {
    ViewModel.ROCreditorInvoiceListCriteria().PaymentRunID(ViewModel.CurrentPaymentRun().PaymentRunID())
    ViewModel.ROInvoicePreviewListCriteria().PaymentRunID(ViewModel.CurrentPaymentRun().PaymentRunID())
    if (ViewModel.ROInvoicePreviewList().length == 0) {
      this.RefreshCreditorInvoicePreviewList()
    }
  },
  actualInvoices: function () {
    ViewModel.ROCreditorInvoiceListCriteria().PaymentRunID(ViewModel.CurrentPaymentRun().PaymentRunID())
    ViewModel.ROInvoicePreviewListCriteria().PaymentRunID(ViewModel.CurrentPaymentRun().PaymentRunID())
    if (ViewModel.ROCreditorInvoiceList().length == 0) {
      this.RefreshCreditorInvoiceList()
    }
  },
  alerts: function () {
    ViewModel.IsFetchingAlerts(true)
    ViewModel.ROPaymentRunAlertGroupList([])
    Singular.GetDataStateless("OBLib.Invoicing.ReadOnly.ROPaymentRunAlertGroupList", {
      PaymentRunID: ViewModel.CurrentPaymentRun().PaymentRunID()
    }, function (response) {
      if (response.Success) {
        ViewModel.ROPaymentRunAlertGroupList.Set(response.Data)
      }
      else {
        OBMisc.Modals.Error('Error', 'There was an error while fetching the alerts', response.ErrorText, {})
      }
      ViewModel.IsFetchingAlerts(false)
    })
  },
  currentAlertGroup: function () {
    if (ViewModel.CurrentAlertGroupID()) {
      return ViewModel.ROPaymentRunAlertGroupList().Find("AlertGroupID", ViewModel.CurrentAlertGroupID())
    }
    return null
  },
  alertSelected: function (alertGroup) {
    ViewModel.CurrentAlertGroupID(alertGroup.AlertGroupID())
  },
  newInvoiceAndOpen: function () {

    PaymentRunPage.newInvoice()

    $('#EditCreditorInvoice').on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentInvoice())
    })

    $('#EditCreditorInvoice').on('hidden.bs.modal', function () {
      ViewModel.CurrentInvoice(null)
      var pr = Singular.GetDataStateless("OBLib.Invoicing.New.PaymentRunList",
        {
          PaymentRunID: ViewModel.CurrentPaymentRun().PaymentRunID(),
          ParentOnly: true
        },
        function (response) {
          if (response.Success) {
            if (response.Data.length == 1) {
              var updatedPR = response.Data[0];
              ViewModel.CurrentPaymentRun().TotalInvoices(updatedPR.TotalInvoices);
              ViewModel.CurrentPaymentRun().TotalAmount(updatedPR.TotalAmount);
            }
          }
          else {
            OBMisc.Modals.Error('Error', 'There was an error while updating the Payment Run information', response.ErrorText, {});
          }
        });
      ViewModel.ROCreditorInvoiceListManager().Refresh();
    })

    $("#EditCreditorInvoice").modal()

  },
  newInvoice: function () {

    ViewModel.CurrentInvoice(new CreditorInvoiceObject())
    ViewModel.CurrentInvoice().PaymentRunID(ViewModel.CurrentPaymentRun().PaymentRunID())

    if (ViewModel.CurrentPaymentRun().SystemID() == 1) {

    }
    else if (ViewModel.CurrentPaymentRun().SystemID() == 2) {
      
    }

  },

  currentPaymentRunHeading: function (ROPaymentRun) {
    if (ViewModel.CurrentPaymentRun()) {
      return ViewModel.CurrentPaymentRun().MonthYearString();
    }
  },

  RefreshCreditorInvoiceList: function () {
    ViewModel.ROCreditorInvoiceListManager().Refresh();
  },
  RefreshCreditorInvoicePreviewList: function () {
    ViewModel.ROInvoicePreviewListManager().Refresh()
  },
  editROCreditorInvoice: function (ROCreditorInvoice) {

    $('#EditCreditorInvoice').on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentInvoice())
    })

    $('#EditCreditorInvoice').on('hidden.bs.modal', function () {
      var pr = Singular.GetDataStateless("OBLib.Invoicing.New.PaymentRunList",
        {
          PaymentRunID: ViewModel.CurrentPaymentRun().PaymentRunID(),
          ParentOnly: true
        },
        function (response) {
          if (response.Success) {
            if (response.Data.length == 1) {
              var updatedPR = response.Data[0];
              ViewModel.CurrentPaymentRun().TotalInvoices(updatedPR.TotalInvoices);
              ViewModel.CurrentPaymentRun().TotalAmount(updatedPR.TotalAmount);
            }
          }
          else {
            OBMisc.Modals.Error('Error', 'There was an error while updating the Payment Run information', response.ErrorText, {});
          }
        });
      ViewModel.ROCreditorInvoiceListManager().Refresh()
    })

    Singular.SendCommand("EditROCreditorInvoice", {
      CreditorInvoiceID: ROCreditorInvoice.CreditorInvoiceID(),
      RowNum: ROCreditorInvoice.RowNum()
    }, function (response) {
      $("#EditCreditorInvoice").modal();
      ViewModel.IsFetchingInvoice(false);
    });

  },
  onROInvoiceSelected: function (roInvoice) {
    roInvoice.IsSelected(!roInvoice.IsSelected())
    if (roInvoice.IsSelected()) {
      var n = new SelectedItemObject()
      n.ID(roInvoice.CreditorInvoiceID())
      n.Description(roInvoice.HumanResource())
      ViewModel.ROCreditorInvoiceListManager().SelectedItems().push(n)
    } else {
      var si = ViewModel.ROCreditorInvoiceListManager().SelectedItems().Find("ID", roInvoice.CreditorInvoiceID())
      if (si) {
        ViewModel.ROCreditorInvoiceListManager().SelectedItems().RemoveItem(si)
      }
    }
  },
  deleteSelectedInvoices: function () {
    ViewModel.ROCreditorInvoiceListManager().IsLoading(true)
    ViewModel.CallServerMethod("DeleteCreditorInvoices", {
      selectedItems: KOFormatterFull.Serialise(ViewModel.ROCreditorInvoiceListManager().SelectedItems())
    }, function (response) {
      if (response.Success) {
        ViewModel.ROCreditorInvoiceListManager().Refresh()
      }
      else {
        OBMisc.Modals.Error('Error Deleting', 'An error occured while deleting the invoices', response.ErrorText, null)
      }
    })
  },
  delayedRefresh: function () {
    clearTimeout(hrTimeout)
    hrTimeout = setTimeout(function () {
      PaymentRunPage.RefreshCreditorInvoiceList()
    }, 250)
  },
  previousInvoice: function (CurrentInvoice) {
    var me = this;
    me.nextPagecommandName = "PreviousInvoice";
    me._previousInvoice(CurrentInvoice);
  },
  _previousInvoice: function (CurrentInvoice) {
    var me = this;
    var CurrentROInvoice = ViewModel.ROCreditorInvoiceList().Find("CreditorInvoiceID", CurrentInvoice.CreditorInvoiceID());
    var CurrentInvoiceIndex = ViewModel.ROCreditorInvoiceList().indexOf(CurrentROInvoice);
    //if current invoice is the first item
    if (CurrentInvoiceIndex == 0) {
      var currentPage = ViewModel.ROCreditorInvoiceListManager().PageNo();
      var newPageNo = currentPage - 1;
      if (newPageNo < 1) {
        ViewModel.ROCreditorInvoiceListManager().PageNo(ViewModel.ROCreditorInvoiceListManager().Pages());
      } else {
        ViewModel.ROCreditorInvoiceListManager().PageNo(newPageNo);
      };
      ViewModel.ROCreditorInvoiceListManager().afterRefresh = me.afterPreviousPageFetched;
      ViewModel.ROCreditorInvoiceListManager().Refresh();
    } else {
      var newInvoice = ViewModel.ROCreditorInvoiceList()[CurrentInvoiceIndex - 1];
      Singular.SendCommand(me.nextPagecommandName, {
        NextInvoiceID: newInvoice.CreditorInvoiceID()
      }, function (response) {

      });
    }
  },
  afterPreviousPageFetched: function (response) {
    var me = this;
    var newInvoice = ViewModel.ROCreditorInvoiceList()[ViewModel.ROCreditorInvoiceList().length - 1];
    Singular.SendCommand(PaymentRunPage.nextPagecommandName, {
      NextInvoiceID: newInvoice.CreditorInvoiceID()
    }, function (response) {
      ViewModel.ROCreditorInvoiceListManager().afterRefresh = null;
    });
  },
  _nextInvoice: function (CurrentInvoice) {
    var me = this;
    var CurrentROInvoice = ViewModel.ROCreditorInvoiceList().Find("CreditorInvoiceID", CurrentInvoice.CreditorInvoiceID());
    var CurrentInvoiceIndex = ViewModel.ROCreditorInvoiceList().indexOf(CurrentROInvoice);
    //if current invoice is the last item
    if (CurrentInvoiceIndex == ViewModel.ROCreditorInvoiceList().length - 1) {
      var currentPage = ViewModel.ROCreditorInvoiceListManager().PageNo();
      var newPageNo = currentPage + 1;
      if (newPageNo > ViewModel.ROCreditorInvoiceListManager().Pages()) {
        ViewModel.ROCreditorInvoiceListManager().PageNo(1);
      } else {
        ViewModel.ROCreditorInvoiceListManager().PageNo(newPageNo);
      };
      ViewModel.ROCreditorInvoiceListManager().afterRefresh = me.afterNextPageFetched;
      ViewModel.ROCreditorInvoiceListManager().Refresh();
    } else {
      if (CurrentInvoiceIndex < 0) {
        CurrentInvoiceIndex = 0;
      }
      var newInvoice = ViewModel.ROCreditorInvoiceList()[CurrentInvoiceIndex + 1];
      Singular.SendCommand(me.nextPagecommandName, {
        NextInvoiceID: newInvoice.CreditorInvoiceID()
      }, function (response) {

      });
    }
  },
  nextInvoice: function (CurrentInvoice) {
    var me = this;
    me.nextPagecommandName = "NextInvoice";
    me._nextInvoice(CurrentInvoice);
  },
  saveAndNextInvoice: function (CurrentInvoice) {
    var me = this;
    me.nextPagecommandName = "SaveAndNextInvoice";
    me._nextInvoice(CurrentInvoice);
  },
  afterNextPageFetched: function (response) {
    var me = this;
    var newInvoice = ViewModel.ROCreditorInvoiceList()[0];
    Singular.SendCommand(PaymentRunPage.nextPagecommandName, {
      NextInvoiceID: newInvoice.CreditorInvoiceID()
    },
    function (response) {
      ViewModel.ROCreditorInvoiceListManager().afterRefresh = null;
    });
  },
  saveNewInvoice: function () {
    Singular.SendCommand("SaveNewInvoice", {}, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Invoice Saved", "", 1000)
      }
      else {
        OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
      }
    })
  },
  saveAndNewInvoice: function () {
    Singular.SendCommand("SaveAndNewInvoice", {}, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Invoice Saved", "", 1000)
        PaymentRunPage.newInvoice()
        Singular.Validation.CheckRules(ViewModel.CurrentInvoice())
      }
      else {
        OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
      }
    })
  },

  generatePaymentRunInvoices: function (PaymentRun) {
    var me = this
    Singular.SendCommand("GenerateInvoices", {}, function (response) {
      if (response.Success) {
        ViewModel.ROCreditorInvoiceList([])
        me.actualInvoices()
      }
      else {
        OBMisc.Modals.Error('Error Generating', 'An error occured while generating the invoices', response.ErrorText, null)
      }
    })
  },
  savePaymentRun: function (PaymentRun) {
    Singular.SendCommand("SavePaymentRun", {}, function (response) {
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess("Payment Run Saved", "", 1000)
      }
      else {
        OBMisc.Notifications.GritterError("Failed to Save", "Payment Run could not be saved", 2000)
        OBMisc.Modals.Error('Error', 'There was an error while saving the Payment Run', response.ErrorText, {})
      }
    })
  },

  showDetailDateSelector: function (invoice, element) {
    var me = this;

    var modalBody = $(element).parents('div.modal-body');

    me.detailDateSelector = $(modalBody).find('div.hasDatepicker');

    if (me.detailDateSelector.length == 0) {
      me.detailDateSelector = '<input></input>'
    };

    me.detailDateSelector = $(me.detailDateSelector).datepicker({
      //showOptions: { direction: "up" },
      showButtonPanel: true,
      closeText: "Close",
      closeOnSelect: false,
      onSelect: function (datetext, inst) {
        var Found = DateExists(invoice.CreditorInvoiceDetailList(), new Date(datetext));
        if (Found == false) {
          var newDet = invoice.CreditorInvoiceDetailList.AddNew()
          newDet.CreditorInvoiceDetailDate(new Date(datetext))
          if (ViewModel.CurrentPaymentRun().SystemID() == 1) {
          }
          else if (ViewModel.CurrentPaymentRun().SystemID() == 2) {
            newDet.AccountID(null)
            newDet.CreditorInvoiceDetailTypeID(5)
            newDet.ProductionAreaID(5)
            newDet.AccountID(24)
            newDet.CostCentreID(2)
          }
        }
        return false;
      }
    });

    //position the date picker
    var buttonPos = element.getBoundingClientRect();
    var input = me.detailDateSelector[0];
    input.style.position = 'absolute';
    input.style.top = (buttonPos.top - buttonPos.height - element.offsetTop) + "px";
    input.style.left = (buttonPos.left - buttonPos.width) + "px";
    input.style.zIndex = 10000;
    input.style.visibility = "hidden";

    //append to DOM
    $(modalBody).append(me.detailDateSelector);
    $(me.detailDateSelector).datepicker('show')

  }
};

function DateExists(List, date) {

  for (var i = 0 ; i < List.length; i++) {
    if (OBMisc.Dates.SameDay(List[i].CreditorInvoiceDetailDate(), new Date(date))) {
      return true;
    }
  }
  return false;
}