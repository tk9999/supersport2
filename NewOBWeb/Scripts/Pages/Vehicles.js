﻿


function CheckDates(Value, Rule, RuleArgs) {

  if (RuleArgs.Object.IsDirty && RuleArgs.Object.IsDirty()) {

    if (RuleArgs.Object.StartDate() && RuleArgs.Object.EndDate()) {
      var StartDate = new Date(RuleArgs.Object.StartDate()).getTime();
      var EndDate = new Date(RuleArgs.Object.EndDate()).getTime();

      if (!RuleArgs.IgnoreResults) {
        if (EndDate <= StartDate) {
          RuleArgs.AddError("Start date must be less than end date");
          return;
        }
      }
    }
  }
}

function VehicleHDCompatibleIndClick(Vehicle) {
  Vehicle.HDCompatibleInd(!Vehicle.HDCompatibleInd());
};

function VehicleHDCompatibleIndCss(Vehicle) {
  if (Vehicle.HDCompatibleInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
};

function VehicleHDCompatibleIndHtml(Vehicle) {
  if (Vehicle.HDCompatibleInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
};

//Select Ind
function VehicleEquipmentSelectIndClick(ROVehicleEquipment) {
  ROVehicleEquipment.SelectInd(!ROVehicleEquipment.SelectInd());
};

function VehicleEquipmentSelectIndCss(ROVehicleEquipment) {
  if (ROVehicleEquipment.SelectInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
};

function VehicleEquipmentSelectIndHtml(ROVehicleEquipment) {
  if (ROVehicleEquipment.SelectInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
};

//ROEquipment Filter Select Ind
//function availableIndClick() {

//  if (ViewModel.AvailableInd() == true) {
//    ViewModel.AvailableInd(false);
//  }
//  else if (ViewModel.AvailableInd() == false) {
//    ViewModel.AvailableInd(null)
//  } else {
//    ViewModel.AvailableInd(true)
//  }
//  GetROEquipmentList();
//};

function availableIndCss() {
  return 'btn btn-md btn-primary'

};

function availableIndHtml() {
  return "Available Only"
};

Singular.OnPageLoad(function () {
  $('#PageTabControl a[href="#Vehicles"]').tab('show')
});

function FindVehicle() {
  $("#FindVehicle").dialog({
    title: "Find Vehicle",
    closeText: "Close",
    height: 720,
    width: 1024,
    modal: true,
    resizeable: true
  });
}

function FindEquipment() {
  ViewModel.AvailableInd(true);
  ViewModel.EquipmentFilter("");
  ViewModel.ROEquipmentList([]);
  $("#FindEquipment").dialog({
    title: "Find Equipment",
    closeText: "Close",
    height: 720,
    width: 1024,
    modal: true,
    resizeable: true
  });
  GetROEquipmentList();
}

function EditVehicle(ROVehicle) {

  Singular.Validation.IgnoreAsyncRules = true;

  Singular.SendCommand("EditVehicle",
                       {
                         VehicleID: ROVehicle.VehicleID()
                       },
                       function (response) {
                         $("#FindVehicle").dialog('destroy');
                        // ViewModel.CurrentVehicle.CheckAllRules();
                        // Singular.Validation.IgnoreAsyncRules = false;
                         Singular.Validation.CheckRules(ViewModel.CurrentVehicle());
                       });

};

function MainTabChange(NewIndex) {
  ViewModel.TabIndex(NewIndex);
}


function EditEquipment(Equipment) {

  Singular.SendCommand("EditEquipment",
                       {
                         EquipmentID: Equipment.EquipmentID()
                       },
                       function (response) {
                         $("#FindEquipment").dialog('destroy');
                       });

};

function GetHRCss(obj) {
  if (obj.HumanResourceID()) {
    return "btn-custom btn-primary HumanResourceID";
  } else {
    return "btn-custom btn-default HumanResourceID";
  }
} //GetHRCss

function FilterROHumanResource() {
  if (ViewModel.ROHumanResourceFilter().length >= 3) {
    Singular.ShowLoadingBar();
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceList, OBLib', {
      FilterName: ViewModel.ROHumanResourceFilter()
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROHumanResourceList);
        $("#EquipmentButton").html("HTML(mText)");
        Singular.HideLoadingBar();
      }
    });
  }
}//FilterROHumanResource  

function FindROHumanResource(DataObject, FindHRType) {
  ViewModel.FindHRType(FindHRType);
  ViewModel.CurrentParentGUID(DataObject.Guid());
  if (FindHRType == 2) {
    ViewModel.CurrentParentGUID(DataObject.GetParent().Guid());
    ViewModel.CurrentChildGUID(DataObject.Guid());
  }
  ViewModel.ROHumanResourceFilter("");
  ViewModel.ROHumanResourceList([]);

  $('#SelectROHumanResource').dialog({
    title: "Select Human Resource",
    closeText: "Close",
    height: 720,
    width: 1024,
    modal: true,
    resizeable: true
  });
};
function FilterDisciplineList(List, Item) {

  var Allowed = [];

  var HRlist = ClientData.ROHumanResourceFilterlist;

  var HumanResourceSkillList = HRlist().Find("HumanResourceID", Item.HumanResourceID()).ROHumanResourceSkillList();
  var HRSkillList = HumanResourceSkillList;
  for (var i = 0; i < HumanResourceSkillList.length ; i++) {
    for (var k = 0; k < List.length; k++) {
      if (List[k].DisciplineID == HRSkillList[i].DisciplineID()) {
        Allowed.push({ DisciplineID: List[k].DisciplineID, Discipline: List[k].Discipline });
      }
    }
  }
  //return Allowed;

  return Allowed;
}
function SetHumanResourceID(ROHumanResource) {
  var currVHR = undefined
  ViewModel.CurrentVehicle().VehicleHumanResourceList().Iterate(function (vhr, Index) {
    if (vhr.Guid() == ViewModel.CurrentParentGUID()) {
      currVHR = vhr
    }
  });

  if (ViewModel.FindHRType() == 1) { //For Vehicle HR
    currVHR.HumanResource(ROHumanResource.PreferredFirstSurname());
    currVHR.HumanResourceID(ROHumanResource.HumanResourceID());
    //Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceFullList, OBLib', {
    //  HumanResourceID: currVHR.HumanResourceID()
    //}, function (args) {
    //  if (args.Success) {
    //    KOFormatter.Deserialise(args.Data, ViewModel.ROHumanResourceFullList);
    //  }
    //});
  } else if (ViewModel.FindHRType() == 2) { //Attendee
    currVHR.VehicleHumanResourceTempPositionList().Iterate(function (vhrtp, Index) {
      if (vhrtp.Guid() == ViewModel.CurrentChildGUID()) {
        vhrtp.HumanResource(ROHumanResource.PreferredFirstSurname());
        vhrtp.HumanResourceID(ROHumanResource.HumanResourceID());
      }
    });
  }
  $('#SelectROHumanResource').dialog('destroy');
}

function FilterEquipment() {
  if (ViewModel.EquipmentFilter().length >= 3) {

    var Available = false
    if (ViewModel.TabIndex() == 0) {
      ViewModel.AvailableInd(true)
    }
    GetROEquipmentList();

  }
}

function GetROEquipmentList() {
  Singular.ShowLoadingBar();
  Singular.GetDataStateless('OBLib.Maintenance.Equipment.ReadOnly.ROEquipmentList, OBLib', {
    AvailableInd: ViewModel.AvailableInd(),
    PageSize: 20,
    FilterName: ViewModel.EquipmentFilter()
  }, function (args) {
    if (args.Success) {
      KOFormatter.Deserialise(args.Data, ViewModel.ROEquipmentList);
      Singular.HideLoadingBar();
    }
  });

}

function GetROVehicleEquipmentList() {
  Singular.ShowLoadingBar();
  ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
  Singular.HideLoadingBar();

}

function ClearFilter() {

  if (ViewModel.ROHumanResourceFilter().length > 0) {
    ViewModel.ROHumanResourceFilter("");
  }
  ViewModel.EquipmentFilter("");
}

function DeleteVehicleEquipment() {

  var tempList = ViewModel.ROVehicleEquipmentList().Filter("SelectInd", true);
  var ret = [];
  for (var i = 0; i < tempList.length; i++) {
    ret.push(tempList[i].VehicleEquipmentID());
  }

  Singular.SendCommand("DeleteVehicleEquipment", { IDs: ret }, function () {
    ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
  });

}

function AllocateVehicleEquipment() {

  var tempList = ViewModel.ROEquipmentList().Filter("SelectInd", true);
  var ret = [];
  for (var i = 0; i < tempList.length; i++) {
    ret.push(tempList[i].EquipmentID());
  }

  ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
  Singular.SendCommand("addVehicleEquipment", { IDs: ret }, function () {
    ViewModel.ROVehicleEquipmentListPagingManager().Refresh();
  });

  $("#FindEquipment").dialog('destroy');

}

VehicleHumanResourceBO = {

  CheckRulesIndSet: function (VehicleHumanResource) {
    if (VehicleHumanResource.CheckRulesInd()) {
      Singular.SendCommand("CheckVehicleHumanResourceRules",
                     {
                       Guid: VehicleHumanResource.Guid(),
                       VehicleHumanResourceID: VehicleHumanResource.VehicleHumanResourceID()
                     }, function (response) {
                       VehicleHumanResource.CheckRulesInd(false);
                       //Singular.Validation.CheckRules(VehicleHumanResource);
                     });
    }
  },
  SetDisciplineID: function (VehicleHumanResource) {
    if (!VehicleHumanResource.CheckRulesInd()) {
      VehicleHumanResource.CheckRulesInd(true);
    }
  },

  SetHumanResourceID: function (VehicleHumanResource) {
    if (!VehicleHumanResource.CheckRulesInd()) {
      VehicleHumanResource.CheckRulesInd(true);
    }
  }
}
