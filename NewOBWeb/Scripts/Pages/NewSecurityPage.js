﻿SecurityGroupPage = {
    CurrentGroupName: function () {
        if (ViewModel.CurrentGroup()) {
            return ViewModel.CurrentGroup().SecurityGroup()
        }
        return ""
    },
    AddSecurityRole: function (obj) {
        obj.SelectedInd(!obj.SelectedInd());
    },
    SaveSecurityGroup: function (SecurityGroup) {
        Singular.SendCommand("SaveGroups", {},
          function (response) {
              if (response.Success) {
                  OBMisc.Notifications.GritterSuccess("Saved", 'Security Groups have been saved successfully', 2000)
              }
              else {
                  OBMisc.Notifications.GritterError("Save Failed", "Security groups have not been saved, please retry", 2000)
              }
          })
    },
    SaveSecurityRoles: function (SecurityRoles) {
        Singular.SendCommand("Save", {},
          function (response) {
              if (response.Success) {
                  OBMisc.Notifications.GritterSuccess("Saved", 'Security Roles has been saved successfully', 2000)
                  $("#GroupsRoles").addClass('animated slideOutDown');
                  $("#SecurityGroupsRoles").removeClass('animated slideInUp');
              }
              else {
                  OBMisc.Notifications.GritterError("Failed to Save " + CurrentGroup(), response.ErrorText, 2000)
              }
          })
    },
    UpdateFromSecurityRoleList: function () {
        //End Edit Roles
        var sgrl = ViewModel.CurrentGroup().SecurityGroupRoleList;
        SecurityGroupPage.IterateRoles(sgrl(), function (sgr, sr) {
            if (sgr) {
                sgr.IsSelected(sr.SelectedInd());
            } else if (sr.SelectedInd()) {
                sgrl.Add({ SecurityRoleID: sr.SecurityRoleID(), IsSelected: true });
            }
        });
    },
    PopulateRoleList: function (Group) {
        //Start Edit Roles
        SecurityGroupPage.IterateRoles(Group.SecurityGroupRoleList(), function (sgr, sr) {
            sr.SelectedInd(sgr && sgr.IsSelected());
        });
    },
    Undo: function () {
        ViewModel.CurrentGroup(null);
        Singular.ShowMessage('Undo', 'Changes not saved have been undone.');
        $("#GroupsRoles").addClass('animated slideOutDown');
        $("#SecurityGroupsRoles").removeClass('animated slideInUp');
    },
    EditSecurityGroupRoles: function (Group) {
        if (Group.IsNew()) {
            Singular.ShowMessage('Edit Roles', 'Please save this group before editing its roles.');
        } else {
            //Populate the SecurityRole list with the groups roles.
            SecurityGroupPage.PopulateRoleList(Group);
            ViewModel.CurrentGroup(Group);
            OBMisc.Notifications.GritterSuccess(SecurityGroupPage.CurrentGroupName(Group), "has been loaded", 2000);
            $("#GroupsRoles").removeClass('FadeHide');
            $("#GroupsRoles").removeClass('animated slideOutDown');
            $("#GroupsRoles").addClass('animated slideInUp');
            $("#SecurityGroupsRoles").removeClass('animated slideInUp');
            SecurityGroupPage.GoBackToTop();
        }
    },
    IterateRoles: function (GroupRoleList, Callback) {
        ViewModel.ROSecurityRoleHeaderList().Iterate(function (srh) {
            srh.ROSecurityRoleList().Iterate(function (sr) {
                var sgr = GroupRoleList.Find('SecurityRoleID', sr.SecurityRoleID());
                Callback(sgr, sr);
            });
        });
    },
    GoBackToTop: function () {
        window.goBackToTop();
    },
    DisableDeleteButton: function () {
        CurrentGroup().SecurityGroupBase().CanDelete();
    }
    }

