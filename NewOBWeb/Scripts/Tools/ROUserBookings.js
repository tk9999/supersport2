﻿var pmUtil = {
  getAbsoluteLeft: function (elem) {
    return elem.getBoundingClientRect().left;
  },
  getAbsoluteTop: function (elem) {
    return elem.getBoundingClientRect().top;
  },
  getTarget: function (event) {
    // code from http://www.quirksmode.org/js/events_properties.html
    if (!event) {
      event = window.event;
    }
    var target;
    if (event.target) {
      target = event.target;
    } else if (event.srcElement) {
      target = event.srcElement;
    }
    if (target.nodeType != undefined && target.nodeType == 3) {
      // defeat Safari bug
      target = target.parentNode;
    }
    return target;
  },
  getEventProperties: function (inst, event) {
    var clientX = event.center ? event.center.x : event.clientX;
    var clientY = event.center ? event.center.y : event.clientY;

    var businessObject = null;
    var element = sUtil.getTarget(event);
    if (element.hasOwnProperty('business-object')) {
      businessObject = element['business-object'];
    };

    return {
      event: event,
      businessObject: businessObject,
      pageX: event.srcEvent ? event.srcEvent.pageX : event.pageX,
      pageY: event.srcEvent ? event.srcEvent.pageY : event.pageY
    };
  },
  randomUUID: function () {
    var S4 = function () {
      return Math.floor(
        Math.random() * 0x10000 /* 65536 */
      ).toString(16);
    };
    return (
      S4() + S4() + '-' +
      S4() + '-' +
      S4() + '-' +
      S4() + '-' +
      S4() + S4() + S4()
    );
  }
};

var ROUserBookingsControl = function (options) {
  var me = this
  this.dom = {}
  this.dom.container = document.getElementById(this.personnelManagerID)

  var sd = moment().startOf('month')
  var ed = moment().endOf('month')
  var hrFetchTimeout = null

  //options
  me.defaultOptions = {
    containerID: 'userBookingsContainer',
    timelineID: 'userBookingsTimeline',
    pageNo: 1,
    pageSize: 25,
    totalPages: 1,
    startDate: sd.toDate(),
    endDate: ed.toDate(),
    firstName: "",
    surname: "",
    preferredName: "",
    onGroupSelected: function (datasetGroup) {

    }
  }
  me.defaultOptions = $.extend({}, me.defaultOptions, options)

  //properties
  me.resources = new vis.DataSet()
  me.resourceBookings = new vis.DataSet()
  me.visTimeline = null

  //methods
  me.refresh = function () {
    me.showLoading()
    ViewModel.CallServerMethod('GetROUserBookingList',
      {
        StartDate: new Date(me.defaultOptions.startDate),
        EndDate: new Date(me.defaultOptions.endDate),
        PageNo: me.defaultOptions.pageNo,
        PageSize: me.defaultOptions.pageSize,
        FirstName: me.defaultOptions.firstName,
        Surname: me.defaultOptions.surname,
        PreferredName: me.defaultOptions.preferredName
      },
      function (response) {
        if (response.Success) {
          me.clearDataSets()
          me.processBookingsData(response.Data.List)
          me.defaultOptions.totalPages = response.Data.TotalPages
          me.defaultOptions.totalRecords = response.Data.TotalRecords
          me.redrawPageInfo()
        }
        else {

        }
        me.hideLoading()
      })
  }
  me.processBookingsData = function (bookingsData) {
    var me = this,
      tempGroup = null,
      tempScheduleItem;
    bookingsData.Iterate(function (res, resIndex) {

      tempGroup = {
        content: (res.CanSwapWithCurrentUser ? '<i class="fa fa-exchange"> ' : '') + '<span>' + res.ResourceName + ' (' + res.ContractType + ')' + '</span>',
        Resource: res,
        className: (res.CanSwapWithCurrentUser ? 'swap' : '')
      }
      me.resources.add(tempGroup)

      res.ROUserResourceBookingList.Iterate(function (resB, resBIndx) {
        var sd = new Date(resB.StartDateTimeBuffer ? resB.StartDateTimeBuffer : resB.StartDateTime);
        var ed = new Date(resB.EndDateTimeBuffer ? resB.EndDateTimeBuffer : resB.EndDateTime);
        tempScheduleItem = {
          group: tempGroup.id,
          content: '<span>' + resB.ResourceBookingDescription + '</span>',
          start: sd,
          end: ed,
          type: 'range',
          className: resB.StatusCssClass,
          Booking: resB
        }
        me.resourceBookings.add(tempScheduleItem)
      })
    })

    me.visTimeline.setGroups(me.resources)
    me.visTimeline.setItems(me.resourceBookings)

  }
  me.clearDataSets = function () {
    this.resources = new vis.DataSet()
    this.resourceBookings = new vis.DataSet()
  }
  me.render = function () {
    var me = this;

    //get the container
    this.dom.container = document.getElementById(me.defaultOptions.containerID)

    //create the header
    this.dom.header = document.createElement('div')
    this.dom.header.className = 'sbr-personnel-manager-header block-flat-small'
    this.dom.headerRow = document.createElement("div")
    this.dom.headerRow.className = "row"
    this.dom.headerRow.style.paddingLeft = "15px"
    this.dom.headerRow.style.paddingRight = "15px"
    this.dom.headerLeft = document.createElement('div')
    this.dom.headerLeft.className = "pull-left"
    this.dom.headerRight = document.createElement('div')
    this.dom.headerRight.className = "pull-right"

    //daterange picker
    this.dom.daterangeAddOn = document.createElement('span')
    this.dom.daterangeAddOn.className = "add-on input-group-addon primary"
    this.dom.daterangeAddOnIcon = document.createElement('i')
    this.dom.daterangeAddOnIcon.className = "fa fa-th"
    this.dom.daterangeAddOn.appendChild(this.dom.daterangeAddOnIcon)

    //hr filters
    this.dom.hrInputGroup = document.createElement("div")
    this.dom.hrInputGroup.className = "input-group pull-left"
    this.dom.hrFirstNameInput = document.createElement('input')
    this.dom.hrFirstNameInput.className = "form-control input-sm"
    this.dom.hrFirstNameInput.setAttribute('type', 'text');
    this.dom.hrFirstNameInput.setAttribute('placeholder', 'Firstname...')
    this.dom.hrFirstNameInput.style.width = "200px"
    this.dom.hrFirstNameInput.id = "hrFirstNameInput"

    this.dom.hrSurnameInput = document.createElement('input')
    this.dom.hrSurnameInput.className = "form-control input-sm"
    this.dom.hrSurnameInput.setAttribute('type', 'text')
    this.dom.hrSurnameInput.setAttribute('placeholder', 'Surname...')
    this.dom.hrSurnameInput.style.width = "200px"
    this.dom.hrSurnameInput.id = "hrSurnameInput"

    this.dom.hrPreferredNameInput = document.createElement('input')
    this.dom.hrPreferredNameInput.className = "form-control input-sm"
    this.dom.hrPreferredNameInput.setAttribute('type', 'text')
    this.dom.hrPreferredNameInput.setAttribute('placeholder', 'Pref. Name...')
    this.dom.hrPreferredNameInput.style.width = "200px"
    this.dom.hrPreferredNameInput.id = "hrPreferredNameInput"

    this.dom.hrInputGroup.appendChild(this.dom.hrFirstNameInput)
    this.dom.hrInputGroup.appendChild(this.dom.hrSurnameInput)
    this.dom.hrInputGroup.appendChild(this.dom.hrPreferredNameInput)

    //refresh button
    this.dom.refreshScheduleButton = document.createElement('span')
    this.dom.refreshScheduleButton.className = "btn btn-sm btn-default pager-button previous-page"
    this.dom.refreshScheduleButton.innerHTML = "<span class='fa fa-refresh'></span> Refresh"

    this.dom.previousMonthButton = document.createElement('span')
    this.dom.previousMonthButton.className = "btn btn-sm btn-primary previous-month"
    this.dom.previousMonthButton.innerHTML = "<span class='fa fa fa-angle-left'></span> Prev Month"
    this.dom.nextMonthButton = document.createElement('span')
    this.dom.nextMonthButton.className = "btn btn-sm btn-primary next-month"
    this.dom.nextMonthButton.innerHTML = "Next Month <span class='fa fa-angle-right'></span>"

    //timeline div
    this.dom.timelineContainer = document.createElement('div')
    this.dom.timelineContainer.className = 'sbr-personnel-manager-container block-flat-small'

    //timeline element
    this.dom.timelineElement = document.createElement('div')
    this.dom.timelineElement.id = me.defaultOptions.timelineID

    //footer
    this.dom.footer = document.createElement('div')
    this.dom.footer.className = 'sbr-personnel-manager-footer block-flat-small'
    this.dom.footerRow = document.createElement("div")
    this.dom.footerRow.className = "row"
    this.dom.footerRow.style.paddingLeft = "15px"
    this.dom.footerRow.style.paddingRight = "15px"
    this.dom.footerLeft = document.createElement('div')
    this.dom.footerLeft.className = "pull-left"
    this.dom.footerRight = document.createElement('div')
    this.dom.footerRight.className = "pull-right"

    //paging buttons
    this.dom.pageButtonContainer = document.createElement('span')

    this.dom.firstPageButton = document.createElement('span')
    this.dom.firstPageButton.className = "btn btn-sm btn-primary pager-button first-page"
    this.dom.firstPageButton.innerHTML = "<span class='fa fa fa-angle-double-left'></span> First"

    this.dom.previousPageButton = document.createElement('span')
    this.dom.previousPageButton.className = "btn btn-sm btn-primary pager-button previous-page"
    this.dom.previousPageButton.innerHTML = "<span class='fa fa fa-angle-left'></span> Previous"

    this.dom.nextPageButton = document.createElement('span')
    this.dom.nextPageButton.className = "btn btn-sm btn-primary pager-button next-page"
    this.dom.nextPageButton.innerHTML = "Next <span class='fa fa-angle-right'></span>"

    this.dom.lastPageButton = document.createElement('span')
    this.dom.lastPageButton.className = "btn btn-sm btn-primary pager-button last-page"
    this.dom.lastPageButton.innerHTML = "Last <span class='fa fa-angle-double-right'></span>"

    this.dom.pageDetails = document.createElement('span')
    this.dom.pageDetails.innerHTML = this.defaultOptions.pageNo.toString() + " of " + this.defaultOptions.totalPages.toString()


    //page button apped
    this.dom.pageButtonContainer.appendChild(this.dom.firstPageButton)
    this.dom.pageButtonContainer.appendChild(this.dom.previousPageButton)
    this.dom.pageButtonContainer.appendChild(this.dom.pageDetails)
    this.dom.pageButtonContainer.appendChild(this.dom.nextPageButton)
    this.dom.pageButtonContainer.appendChild(this.dom.lastPageButton)

    //loading
    this.dom.loadingDiv = document.createElement('div')
    this.dom.loadingDiv.style.display = 'none'
    this.dom.loadingDiv.className = "loading-custom"
    this.dom.loadingIcon = document.createElement('i')
    this.dom.loadingIcon.className = "fa fa-refresh fa-spin"
    this.dom.loadingDiv.appendChild(this.dom.loadingIcon)

    //header append
    //left
    this.dom.headerLeft.appendChild(this.dom.hrInputGroup)
    this.dom.headerRow.appendChild(this.dom.headerLeft)

    //right
    this.dom.headerRight.appendChild(this.dom.refreshScheduleButton)
    this.dom.headerRight.appendChild(this.dom.previousMonthButton)
    this.dom.headerRight.appendChild(this.dom.nextMonthButton)
    this.dom.headerRow.appendChild(this.dom.headerRight)

    //append header to row
    this.dom.header.appendChild(this.dom.headerRow)

    //timeline append
    this.dom.timelineContainer.appendChild(this.dom.timelineElement)

    //footer append
    this.dom.footerRight.appendChild(this.dom.pageButtonContainer)
    this.dom.footerRow.appendChild(this.dom.footerLeft)
    this.dom.footerRow.appendChild(this.dom.footerRight)
    this.dom.footer.appendChild(this.dom.footerRow)

    if (this.dom.container) { 
      //main container append
      this.dom.container.appendChild(this.dom.header)
      this.dom.container.appendChild(this.dom.timelineContainer)
      this.dom.container.appendChild(this.dom.footer)
      this.dom.container.appendChild(this.dom.loadingDiv)
    }

    //add the click binding
    $(this.dom.header).on('click', function (event) {
      me.getHeaderProperties(event)
    })
    $(this.dom.header).on('keyup', 'input', function (event) {
      me.getHeaderProperties(event)
    })
    $(this.dom.footer).on('click', function (event) {
      me.getFooterProperties(event);
    })

  }
  me.setupTimeline = function () {
    var me = this;

    //create the timeline
    me.visTimeline = new vis.Timeline(me.dom.timelineElement)

    //instantiate the datasets
    me.resources = new vis.DataSet()
    me.resourceBookings = new vis.DataSet()

    //setup options
    me.timelineOptions = {
      editable: false,
      selectable: false,
      orientation: 'top',
      showCurrentTime: true,
      clickToUse: false,
      maxHeight: "800px",
      stack: true,
      multiselect: false,
      start: me.defaultOptions.startDate,
      end: me.defaultOptions.endDate,
      min: me.defaultOptions.startDate,
      max: me.defaultOptions.endDate,
      zoomMin: 1000 * 60 * 60 * 24,             // 1 day
      zoomMax: 1000 * 60 * 60 * 24 * 7 * 31,     // 1 month
      autoResize: true,
      type: 'range',
      moveable: true,
      margin: { item: 0 },
      width: '100%',
      order: function (booking1, booking2) {
        //console.log(booking1, booking2);
        if (booking1.start > booking2.start) { return 1 }
        else if (booking1.start < booking2.start) { return -1 }
        else { return 0 };
      },
      groupOrder: function (resource1, resource2) {
        return (resource1.Resource.ResourceOrder - resource2.Resource.ResourceOrder);
      },
      onAdd: function (item, callback) {
        callback(null);
      },
      onUpdate: function (item, callback) {
        callback(null);
      },
      onMoving: function (item, callback) {
        callback(null);
      },
      onMove: function (item, callback) {
        callback(null);
      },
      onRemove: function (item, callback) {
        callback(null);
      },
      snap: function (date, scale, step) {
        var minute = 60 * 1000;
        return Math.round(date / minute) * minute;
      },
      dataAttributes: ['resourceid', 'resourcebookingid']
    };

    //set the options
    me.visTimeline.setOptions(me.timelineOptions);

    //set the resources and bookings to the timeline
    me.visTimeline.setGroups(me.resources);
    me.visTimeline.setItems(me.resourceBookings);

    //add the event listeners
    me.visTimeline.on('groupSelected', _groupSelectedInternal);
    //me.visTimeline.on('groupDeSelected', _groupDeSelectedInternal);
    //me.visTimeline.eventPropertiesFetched = function (eventProperties) {
    ////do stuff before opening up for the developer
    //var group = null;
    //switch (eventProperties.what) {
    //  case 'group-label':
    //    //eventProperties.groupObj.handleSelect();
    //    group = me.resources.get(eventProperties.groupObj.groupId);
    //    if (eventProperties.groupObj.selected) {
    //      me.selectedGroups.push(group);
    //      me.onGroupSelected(group, eventProperties);
    //    } else {
    //      me.selectedGroups.pop(group);
    //      me.onGroupDeselected(group, eventProperties);
    //    };
    //    break;
    //}
    ////do the developers stuff
    ////me.onEventPropertiesFetched(eventProperties);
    //};

    //adjust view

    //#region Private/Internal methods

    function _groupSelectedInternal(properties) {
      var dsGroup = me.resources.get(properties.group.groupId);
      me.defaultOptions.onGroupSelected(dsGroup)
    };

    function _groupDeSelectedInternal(properties) {
      var dsGroup = me.resources.get(properties.group.groupId);
      properties.dataSetGroup = dsGroup;
      me.options.onGroupDeselected(properties);
    };

    //#endregion

  }
  me.firstPage = function () {
    this.defaultOptions.pageNo = 1
    this.refresh()
  }
  me.previousPage = function () {
    this.defaultOptions.pageNo = this.defaultOptions.pageNo - 1
    if (this.defaultOptions.pageNo < 1) {
      this.defaultOptions.pageNo = this.defaultOptions.totalPages
    }
    this.refresh()
  }
  me.nextPage = function () {
    this.defaultOptions.pageNo = this.defaultOptions.pageNo + 1
    if (this.defaultOptions.pageNo > this.defaultOptions.totalPages) {
      this.defaultOptions.pageNo = 1
    }
    this.refresh()
  }
  me.lastPage = function () {
    this.defaultOptions.pageNo = this.defaultOptions.totalPages
    this.refresh()
  }
  me.redrawPageInfo = function () {
    var me = this;
    me.dom.pageDetails.innerHTML = me.defaultOptions.pageNo.toString() + " of " + me.defaultOptions.totalPages.toString();
  }
  me.getHeaderProperties = function (event) {
    var me = this;
    var element = pmUtil.getTarget(event);

    if (element.className.indexOf("pager-button") >= 0) {
      if (element.className.indexOf("first-page") >= 0) {
        me.firstPage();
      } else if (element.className.indexOf("previous-page") >= 0) {
        me.previousPage();
      } else if (element.className.indexOf("next-page") >= 0) {
        me.nextPage();
      } else if (element.className.indexOf("last-page") >= 0) {
        me.lastPage();
      }
    }
    else if (element.className.indexOf("previous-month") >= 0) {
      me.previousMonth();
    }
    else if (element.className.indexOf("next-month") >= 0) {
      me.nextMonth();
    }
    else if (element.id == "hrPreferredNameInput"
      || element.id == "hrSurnameInput"
      || element.id == "hrFirstNameInput") {
      if (event.type == 'keyup') {
        me.defaultOptions.firstName = document.getElementById("hrFirstNameInput").value
        me.defaultOptions.surname = document.getElementById("hrSurnameInput").value
        me.defaultOptions.preferredName = document.getElementById("hrPreferredNameInput").value
        clearTimeout(hrFetchTimeout);// clear any running timeout
        hrFetchTimeout = setTimeout(function () {
          me.refresh()
        }, 250)
      }
    };

    var eventProps = {
      event: event,
      elem: element,
      tgt: event.target,
      inst: this,
      pageX: event.srcEvent ? event.srcEvent.pageX : event.pageX,
      pageY: event.srcEvent ? event.srcEvent.pageY : event.pageY
    };

    return eventProps;
  }
  me.getFooterProperties = function (event) {
    var me = this;
    var element = pmUtil.getTarget(event);

    if (element.className.indexOf("pager-button") >= 0) {
      if (element.className.indexOf("first-page") >= 0) {
        me.firstPage();
      } else if (element.className.indexOf("previous-page") >= 0) {
        me.previousPage();
      } else if (element.className.indexOf("next-page") >= 0) {
        me.nextPage();
      } else if (element.className.indexOf("last-page") >= 0) {
        me.lastPage();
      }
    };

    var eventProps = {
      event: event,
      elem: element,
      tgt: event.target,
      inst: this,
      pageX: event.srcEvent ? event.srcEvent.pageX : event.pageX,
      pageY: event.srcEvent ? event.srcEvent.pageY : event.pageY
    };

    //console.log(eventProps);

    return eventProps;
  }
  me.showLoading = function () {
    var me = this
    me.dom.loadingDiv.style.display = ''
  }
  me.hideLoading = function () {
    me.dom.loadingDiv.style.display = 'none'
  }
  me.previousMonth = function () {
    var sd = moment(me.defaultOptions.startDate).add(-1, 'month').toDate()
    var ed = moment(me.defaultOptions.endDate).add(-1, 'month').toDate()
    me.defaultOptions.startDate = sd
    me.defaultOptions.endDate = ed
    me.visTimeline.setOptions({ min: sd, start: sd, end: ed, max: ed })
    this.refresh()
  }
  me.nextMonth = function () {
    var sd = moment(me.defaultOptions.startDate).add(1, 'month').toDate()
    var ed = moment(me.defaultOptions.endDate).add(1, 'month').toDate()
    me.defaultOptions.startDate = sd
    me.defaultOptions.endDate = ed
    me.visTimeline.setOptions({ min: sd, start: sd, end: ed, max: ed })
    this.refresh()
  }

  this.render()
  this.setupTimeline()
  this.refresh()

}