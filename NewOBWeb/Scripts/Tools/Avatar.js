﻿//Singular.HandleFileSelected = function () {
//  //override and do nothing.
//}

AvatarBO = {
  ChooseImage: function (obj, Type) {
    console.log(obj, Type);
    Singular.ShowFileDialog(function (e) {
      Singular.UploadFile(e.target.files[0], ViewModel.TempImage(), 'png,jpg,jpeg', function (Result) {
        if (Result.Success) {
          console.log(Result);
          obj.UserProfileLargeImageID(Result.DocumentID);
        } else {
          alert(Result.Response);
        }
      }, 'Image=true&ImageTypeID=' + Type + '&Stateless=true&ForHumanResourceID=0');
    });
  }
}