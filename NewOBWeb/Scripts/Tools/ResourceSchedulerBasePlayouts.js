﻿//#region Misc
var w = window,
		d = document,
		e = d.documentElement,
		g = d.getElementsByTagName('body')[0],
		globalX = w.innerWidth || e.clientWidth || g.clientWidth,
		globalY = w.innerHeight || e.clientHeight || g.clientHeight;

sUtil = {
  getAbsoluteLeft: function (elem) {
    return elem.getBoundingClientRect().left;
  },
  getAbsoluteTop: function (elem) {
    return elem.getBoundingClientRect().top;
  },
  getTarget: function (event) {
    // code from http://www.quirksmode.org/js/events_properties.html
    if (!event) {
      event = window.event;
    }
    var target;
    if (event.target) {
      target = event.target;
    } else if (event.srcElement) {
      target = event.srcElement;
    }
    if (target.nodeType != undefined && target.nodeType == 3) {
      // defeat Safari bug
      target = target.parentNode;
    }
    return target;
  },
  getContextProperties: function (event) {
    var element = sUtil.getTarget(event);
    var tr = element.parentNode.parentNode;
    var businessObject = ko.dataFor(tr);
    return {
      event: event,
      element: element
    };
  },
  getdraggedEventProperties: function (event) {
    var clientX = event.center ? event.center.x : event.clientX;
    var clientY = event.center ? event.center.y : event.clientY;
    var element = sUtil.getTarget(event);
    //var businessObject = ko.dataFor(element);
    var timelineGroup = element['timeline-group'];
    var group = null;
    if (timelineGroup) {
      group = window.Scheduler.getGroupByGUID(timelineGroup.groupId);
    };
    return {
      event: event,
      timelineGroup: timelineGroup,
      group: group,
      pageX: event.srcEvent ? event.srcEvent.pageX : event.pageX,
      pageY: event.srcEvent ? event.srcEvent.pageY : event.pageY
    };
  },
  randomUUID: function () {
    var S4 = function () {
      return Math.floor(
					Math.random() * 0x10000 /* 65536 */
			).toString(16);
    };
    return (
				S4() + S4() + '-' +
						S4() + '-' +
						S4() + '-' +
						S4() + '-' +
						S4() + S4() + S4()
				);
  }
};

//#endregion

//#region Resource Scheduler

function ResourceSchedulerBase(options) {
  var me = this;

  //scheduler control
  this.dom = {};
  this.dom.container = document.getElementById("ResourceScheduler");
  this.schedulerData = ClientData.ResourceSchedulerData;
  this.defaultOptions = {
    areaNotAddedModalID: "AreaNotAddedModal",
    synergyMenuID: "SynergyMenu",
    synergyControlName: "SynergyEvents",
    offPeriodControlName: 'OffPeriodModal',
    secondmentControlName: 'SecondmentModal',
    equipmentScheduleModalID: 'CurrentEquipmentSchedule',
    roomScheduleControl: "CurrentRoomScheduleControl",
    roRoomScheduleControl: "CurrentRORoomScheduleControl",
    studioSupervisorShiftControlName: "CurrentStudioSupervisorShiftControl",
    playoutOpsShiftControlName: "CurrentPlayoutOpsShiftControl",
    playoutOpsShiftControlNameMCR: "CurrentPlayoutOpsShiftControlMCR",
    //playoutOpsShiftControlNameSCCR: "CurrentPlayoutOpsShiftControlSCCR",
    icrShiftControlName: "CurrentICRShiftControl",
    stagehandShiftControlName: "CurrentStageHandShiftControl",
    programmingShiftControlName: "CurrentProgrammingShiftControl",
    editSchedulerModalID: "EditResourceSchedulerModal",
    roomScheduleArea: ""
  };

  this.defaultOptions = $.extend({}, this.defaultOptions, options);
  this.selectedResourceBookingItems = [];
  this.selectedGroups = [];
  this.isReadOnly = this.schedulerData.ReadOnlyAccess;
  this.isAdministrator = this.schedulerData.IsAdministrator;
  this.inEditMode = false;

  //own controls
  this.resourcesPanelisVisible = true;

  //external controls
  this.areaNotAddedModal = function () { return $('#' + me.defaultOptions.areaNotAddedModalID) }
  this.equipmentScheduleModal = function () { return $('#' + me.defaultOptions.equipmentScheduleModalID) }
  this.synergyEventsControl = function () {
    return window[me.defaultOptions.synergyControlName];
  }
  this.selectedSynergyEvents = function () {
    if (ViewModel) { return ViewModel.SelectedROSynergyEventList() }
    return [];
  }
  this.synergyEventRoomRequests = function () {
    if (ViewModel) { return ViewModel.SynergyEventRoomRequestList() }
  }
  this.synergyEventEquipmentRequests = function () {
    if (ViewModel) { return ViewModel.SynergyEventEquipmentRequestList() }
  }
  this.roRoomScheduleControl = function () {
    return window[me.defaultOptions.roRoomScheduleControl]
  }
  this.copyBookingControl = null
  this.roomScheduleControl = function () {
    return window[me.defaultOptions.roomScheduleControl]
  }

  this.resourcePopoverModal = document.getElementById('RSResourcePopoverModal')

  //room requests
  this.addSynergyEventRoomRequest = function (req) {
    if (ViewModel) { return ViewModel.SynergyEventRoomRequestList.Add(req) }
  }
  this.setSynergyEventRoomRequests = function (newValue) {
    if (ViewModel) { return ViewModel.SynergyEventRoomRequestList(newValue) }
  }
  this.serialiseSynergyEventRoomRequests = function () {
    //setup a KOFormatter
    var kof = new KOFormatterObject();
    kof.IncludeChildren = true;
    kof.IncludeClean = true;
    kof.IncludeCleanInArray = true;
    kof.IncludeCleanProperties = true;
    kof.IncludeIsNew = true;
    evnts = kof.Serialise(me.synergyEventRoomRequests());
    return evnts
  }

  //equipment requests
  this.addSynergyEventEquipmentRequest = function (req) {
    if (ViewModel) { return ViewModel.SynergyEventEquipmentRequestList.Add(req) }
  }
  this.setSynergyEventEquipmentRequests = function (newValue) {
    if (ViewModel) { return ViewModel.SynergyEventEquipmentRequestList(newValue) }
  }
  this.serialiseSynergyEventEquipmentRequests = function () {
    //setup a KOFormatter
    var kof = new KOFormatterObject();
    kof.IncludeChildren = true;
    kof.IncludeClean = true;
    kof.IncludeCleanInArray = true;
    kof.IncludeCleanProperties = true;
    kof.IncludeIsNew = true;
    evnts = kof.Serialise(me.synergyEventEquipmentRequests());
    return evnts
  }

  this.offPeriodModal = function () {
    return window[me.defaultOptions.offPeriodControlName];
  };
  this.secondmentModal = function () {
    return window[me.defaultOptions.secondmentControlName];
  };
  this.studioSupervisorShiftModal = function () {
    return window[me.defaultOptions.studioSupervisorShiftControlName];
  };
  this.playoutOpsShiftModal = function () {
    return window[me.defaultOptions.playoutOpsShiftControlName];
  };
  this.playoutOpsShiftModalMCR = function () {
    return window[me.defaultOptions.playoutOpsShiftControlNameMCR];
  };
  //this.playoutOpsShiftModalSCCR = function () {
  //  return window[me.defaultOptions.playoutOpsShiftControlNameSCCR];
  //};
  this.icrShiftModal = function () {
    return window[me.defaultOptions.icrShiftControlName];
  };
  this.stagehandShiftModal = function () {
    return window[me.defaultOptions.stagehandShiftControlName];
  };
  this.programmingShiftModal = function () {
    return window[me.defaultOptions.programmingShiftControlName];
  };

  //hub
  this.hubManager = window.siteHubManager;

  //timeline
  this.timelineElement = null;
  this.timelineOptions = {}
  this.resources = null;
  this.resourceBookings = null;
  this.visTimeline = null;
  this.currentItem = null;
  this.currentGroup = null;
  this.contextMenuProperties = null;
  this.isFetchingData = false;
  this.hoverTimeout = null;

  //helpers
  this.tempItem = null;
  this.tempGroupItem = null; //to help with efficiency
  this.tempCssClass = '';
  this.tempGroupToAddTo = null;
  this.tempIcon = null;
  this.tempDiv = null;

  if (this.init) { this.init() }

};

ResourceSchedulerBase.prototype.init = function () {
  var me = this
  me.copyBookingControl = new CopyBookingModal()
}

ResourceSchedulerBase.prototype.create = function () {
  var me = this

  this.dom.container = document.getElementById("ResourceScheduler")
  this.dom.container.className = 'row animated go slideInUp go'
  this.schedulerData = ClientData.ResourceSchedulerData
  this.isBusy = false;
  this.sizeSlider = null

  //groups container
  this.dom.groupsContainer = document.createElement('div')
  this.dom.groupsContainer.className = 'groups-container split split-horizontal'

  //groups header
  this.dom.groupsHeader = document.createElement('div')
  this.dom.groupsHeader.className = 'groups-header'

  //groups content
  this.dom.groupsContent = document.createElement('div')
  this.dom.groupsContent.className = 'groups-content'

  //groups panel
  this.dom.groupsPanel = document.createElement('div')
  this.dom.groupsPanel.className = 'groups-panel split split-horizontal'

  //sub-groups
  this.dom.resourcesPanel = document.createElement('div')
  this.dom.resourcesPanel.className = 'resources-panel split split-horizontal'

  this.dom.groupsContent.appendChild(this.dom.groupsPanel)
  this.dom.groupsContent.appendChild(this.dom.resourcesPanel)
  this.dom.groupsContainer.appendChild(this.dom.groupsHeader)
  this.dom.groupsContainer.appendChild(this.dom.groupsContent)

  //timeline container
  this.dom.timelineContainer = document.createElement('div')
  this.dom.timelineContainer.id = 'timeline-container'
  this.dom.timelineContainer.className = 'timeline-container split split-horizontal'

  //legend controls
  this.legendData = null;
  this.dom.legendDiv = document.createElement('div');
  this.dom.legendDiv.id = "statusLegend"
  this.dom.legendDiv.className = "hide-legend"
  this.showStatusLegend = false;

  //timeline header
  this.dom.timelineHeader = document.createElement('div')
  this.dom.timelineHeader.className = 'timeline-header'

  //timeline element
  this.dom.timelineElement = document.createElement('div')
  this.dom.timelineElement.id = 'resourceBookings'

  //appends
  this.dom.timelineContainer.appendChild(this.dom.timelineHeader)
  this.dom.timelineContainer.appendChild(this.dom.legendDiv)
  this.dom.timelineContainer.appendChild(this.dom.timelineElement)

  //busy div
  this.dom.busyDiv = document.createElement('div')
  this.dom.busyDiv.className = 'sbr-scheduler-busy loading-custom'
  this.dom.busyDiv.id = "schedulerBusy"

  //busy icon
  this.dom.busyDivIcon = document.createElement('i')
  this.dom.busyDivIcon.className = 'fa fa-refresh fa-5x fa-spin'
  this.dom.busyDiv.appendChild(this.dom.busyDivIcon)
  this.dom.timelineContainer.appendChild(this.dom.busyDiv)

  this.dom.container.appendChild(this.dom.groupsContainer)
  this.dom.container.appendChild(this.dom.timelineContainer)

  me.getLegendData()
  me.createGroupsTree()
  me.createResourceList()
  me.addHeaderControls()
  me.setupTimeline()

  //handle events
  $('div.groups-header, div.timeline-header, #ResourceScheduler span.tree-toggler-content').on('click', function (event) {
    me.onHeaderClick(event)
  })

  //setup controls
  function _setupDateRangePicker() {
    //initial setup of date picker
    $('#btnDateRange').daterangepicker({
      format: 'ddd D MMM YYYY',
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        'Last 7 Days': [moment().subtract('days', 6), moment()],
        'Last 30 Days': [moment().subtract('days', 29), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      linkedCalendars: true,
      autoUpdateInput: true
    })
    //listen to the apply event
    $('#btnDateRange').on('show.daterangepicker', function (event, picker) {
      picker.showCalendars();
      me.onDateRangeShown(event, picker);
    })
    //listen to the apply event
    $('#btnDateRange').on('apply.daterangepicker', function (event, picker) {
      me.onDateRangeChanged(event, picker);
    })
  }

  _setupDateRangePicker()

  Split(['div.groups-container', 'div.timeline-container'], {
    sizes: [20, 80],
    direction: 'horizontal',
    minSize: [40, 400],
    onDrag: function () {
      console.log(this)
    }
  })

  Split(['div.groups-panel', 'div.resources-panel'], {
    sizes: [50, 50],
    direction: 'horizontal',
    minSize: [20, 20],
    onDrag: function () {
      console.log(this)
    }
  })

  me.sizeSlider = new Slider("#sizeSlider", {
    formatter: function (value) {
      return value.toString();
    },
    min: 15,
    max: 30,
    value: 23,
    tooltip_position: 'bottom'
  })
	.on('slideStop', function (value) {
	  me.resizeItemHeight(value)
	})

};

ResourceSchedulerBase.prototype.onHeaderClick = function (event) {
  var me = this;

  var elem = sUtil.getTarget(event)
  var elementFound = false
  var eventProps = {
    event: event,
    element: elem,
    targetElement: elem.target,
    tgt: event.target,
    inst: this,
    pageX: event.srcEvent ? event.srcEvent.pageX : event.pageX,
    pageY: event.srcEvent ? event.srcEvent.pageY : event.pageY
  }
  var parentLI = $(elem).closest('li.selectable-resource')

  if (elem.id == "btnApply" || elem.parentNode.id == "btnApply") {
    elementFound = true
    me.onApply(eventProps);
  }
  else if (elem.id == 'btnEdit' || elem.parentNode.id == 'btnEdit') {
    elementFound = true
    me.intoEdit(eventProps)
  }
  else if (elem.id == 'btnShowStats' || elem.parentNode.id == 'btnShowStats') {
    elementFound = true
    me.schedulerData.ShowHRStats = !me.schedulerData.ShowHRStats
    if (me.schedulerData.ShowHRStats) {
      me.dom.showStatsButton.className = 'btn btn-sm'
      me.dom.showStatsButton.innerHTML = "<i class='fa fa-check-square-o fa-2x' /> Show Stats"
    } else {
      me.dom.showStatsButton.className = 'btn btn-sm'
      me.dom.showStatsButton.innerHTML = "<i class='fa fa-bar-chart-o fa-2x' /> Show Stats"
    }
    me.refreshResourcesAndBookings()
  }
  else if (elem.id == 'btnStatusLegend' || elem.parentNode.id == 'btnStatusLegend') {
    elementFound = true
    me.showStatusLegend = !me.showStatusLegend
    if (me.showStatusLegend) {
      me.showLegendPanel()
    } else {
      me.hideLegendPanel()
    }
    //me.refreshResourcesAndBookings()
  }
  else if (elem.id == 'btnSwitchResources' || elem.parentNode.id == 'btnSwitchResources') {
    elementFound = true
    me.schedulerData.ShowResources = !me.schedulerData.ShowResources
    if (me.schedulerData.ShowResources) {
      me.dom.switchResourcesButton.className = 'btn btn-sm'
      me.dom.switchResourcesButton.innerHTML = "<i class='fa fa-eye-slash fa-2x' /> Hide Resources"
      me.showResourcesPanel()
    }
    else {
      me.dom.switchResourcesButton.className = 'btn btn-sm btn-default'
      me.dom.switchResourcesButton.innerHTML = "<i class='fa fa-eye fa-2x' /> Show Resources"
      me.hideResourcesPanel()
    }
  }
  else if (elem.id == 'btnRefresh' || elem.parentNode.id == 'btnRefresh') {
    elementFound = true
    me.refreshResourcesAndBookings()
  }
  else if (parentLI.length == 1) {
    me.resourceNodeClicked(event, elem, parentLI[0])
  }

  if (elementFound) {
    event.preventDefault();
    me.onHeaderChanged(eventProps)
  }

}

ResourceSchedulerBase.prototype.resourceNodeClicked = function (event, element, parentLI) {
  var me = this;
  var label = $(parentLI).children('label.tree-toggler')
  var content = $(label).children('span.tree-toggler-content')
  var icon = $(content).children(".fa.toggle-icon")//label.children(".fa.toggle-icon")
  var li = $(parentLI)
  var isSubGroup = li.hasClass('scheduler-subgroup')
  var grp, subgrp, resource, opened;

  //icon
  if (icon.hasClass("fa-folder-o")) {
    if (isSubGroup) {
      icon.removeClass("fa-folder-o").addClass("fa-check-square-o sub-group-selected")
    }
    else {
      icon.removeClass("fa-folder-o").addClass("fa-folder-open-o")
    }
    opened = true
  }
  else {
    if (isSubGroup) {
      icon.removeClass("fa-check-square-o sub-group-selected").addClass("fa-folder-o")
    }
    else {
      icon.removeClass("fa-folder-open-o").addClass("fa-folder-o")
    }
    opened = false
  }

  //business object
  if (li.hasClass('scheduler-group')) {
    grp = li[0].item
  }
  else if (li.hasClass('scheduler-subgroup')) {
    subgrp = li[0].item
    if (opened) {
      me.addResourcesToTimeline(subgrp.ResourceSchedulerSubGroupResourceList)
    }
    else {
      me.removeResourcesFromTimeline(subgrp.ResourceSchedulerSubGroupResourceList)
    }
  }
  else if (parentLI.hasClass('scheduler-resource')) {
    resource = li[0].item
  }

  //expand the tree
  if (!isSubGroup) {
    $(label).parent().children('ul.tree').toggle(300, function () {
      $(label).parent().toggleClass("open")
      $(".tree .nscroller").nanoScroller({ preventPageScrolling: true })
    })
  }

  me.refreshResourcesAndBookings()
}

ResourceSchedulerBase.prototype.intoEdit = function (event) {
  var me = this
  me.inEditMode = true
  me.dom.editSchedulerButton.className = "btn btn-sm"
  ResourceSchedulerBO.getScheduler(me.schedulerData.ResourceSchedulerID,
																	function (response) {
																	  ViewModel.EditResourceScheduler.Set(response.Data)
																	  me.showEditScheduler()
																	},
																	function (response) {

																	})
};

ResourceSchedulerBase.prototype.outOfEdit = function () {
  var me = this
  me.inEditMode = false
  me.dom.editSchedulerButton.className = "btn btn-sm btn-default"
};

ResourceSchedulerBase.prototype.createGroupsTree = function () {
  var me = this;
  var data = me.schedulerData;

  if (me.dom.groupsTree) {
    var elem = document.getElementById("groupsTree")
    var parent = elem.parentNode
    parent.removeChild(elem)
    me.dom.groupsTree = null
  }

  me.dom.groupsTree = document.createElement('div')
  me.dom.groupsTree.className = 'page-aside tree'
  me.dom.groupsTree.id = "groupsTree"

  //Setup Panels-----------------------------------
  var currentGroup = null,
			currentGroupLabel = null,
			currentGroupLabelIcon = null,
			currentGroupLabelText = null,
			currentGroupSelectAllIcon = null,
			currentGroupLabelContent = null,
			groupList = null;

  groupList = document.createElement('ul')
  groupList.className = 'nav nav-list treeview groupList'

  //Groups---------------------------------------------------------
  data.ResourceSchedulerGroupList.Iterate(function (grp, grpIndx) {
    //building
    currentGroup = document.createElement('li')
    currentGroup.item = grp
    currentGroup.id = "group-" + grp.ResourceSchedulerGroupID.toString()
    currentGroup.className = "selectable-resource scheduler-group"
    currentGroupLabel = document.createElement('label')
    currentGroupLabel.className = 'tree-toggler nav-header'
    currentGroupLabelContent = document.createElement('span')
    currentGroupLabelContent.className = "tree-toggler-content"
    currentGroupLabelIcon = document.createElement('i')
    currentGroupLabelIcon.className = "fa fa-folder-o toggle-icon"
    currentGroupLabelText = document.createElement('span')
    currentGroupLabelText.className = "toggle-icon-text"
    currentGroupLabelText.innerHTML = grp.ResourceSchedulerGroup

    //append before sub groups
    currentGroupLabelContent.appendChild(currentGroupLabelIcon)
    currentGroupLabelContent.appendChild(currentGroupLabelText)
    currentGroupLabel.appendChild(currentGroupLabelContent)
    currentGroup.appendChild(currentGroupLabel)

    //Sub Groups--------------------------------------------------------
    if (grp.ResourceSchedulerSubGroupList.length > 0) {
      var subGroup = null,
					subGroupLabel = null,
					subGroupLabelContent = null,
					subGroupLabelIcon = null,
					subGroupLabelText = null,
					subGroupLabelEditContainer = null,
					subGroupLabelEditIcon = null,
					subGroupLabelEditText = null,
					subGroupList = null;

      subGroupList = document.createElement('ul')
      subGroupList.className = "nav nav-list tree subGroupList"
      subGroupList.style.display = "none"
      //me.dom.subGroupList.setAttribute('id', 'subGroupList')

      grp.ResourceSchedulerSubGroupList.Iterate(function (rssg, rssgInd) {
        subGroup = document.createElement('li')
        subGroup.item = rssg
        subGroup.id = "subgroup-" + rssg.ResourceSchedulerSubGroupID.toString()
        subGroup.className = "selectable-resource scheduler-subgroup"
        subGroupLabel = document.createElement('label')
        subGroupLabel.className = 'tree-toggler nav-header'

        subGroupLabelContent = document.createElement('span')
        subGroupLabelContent.className = "tree-toggler-content"

        subGroupLabelIcon = document.createElement('i')
        subGroupLabelIcon.className = "fa fa-folder-o toggle-icon"
        subGroupLabelText = document.createElement('span')
        subGroupLabelText.innerHTML = rssg.ResourceSchedulerSubGroup

        subGroupLabelContent.appendChild(subGroupLabelIcon)
        subGroupLabelContent.appendChild(subGroupLabelText)
        subGroupLabel.appendChild(subGroupLabelContent)
        subGroup.appendChild(subGroupLabel)

        //Sub Group Resources---------------------------------------
        if (rssg.ResourceSchedulerSubGroupResourceList.length > 0) {
          var sgResourceItem = null,
							sgResourceAnchor = null;

          var subGroupResourceList = document.createElement('ul')
          subGroupResourceList.className = "nav nav-list tree subGroupResourceList"
          subGroupResourceList.style.display = "none"

          rssg.ResourceSchedulerSubGroupResourceList.Iterate(function (res, resIndx) {
            sgResourceItem = document.createElement('li')
            sgResourceItem.item = res
            sgResourceItem.id = "subgroupresource-" + res.ResourceSchedulerSubGroupResourceID.toString()
            sgResourceItem.className = "selectable-resource scheduler-resource"
            sgResourceAnchor = document.createElement('a')
            sgResourceAnchor.setAttribute('href', '#')
            sgResourceAnchor.innerHTML = res.ResourceName
            sgResourceItem.appendChild(sgResourceAnchor)
            subGroupResourceList.appendChild(sgResourceItem)
          })
          subGroup.appendChild(subGroupResourceList)
        }

        $(subGroup).data(rssg)
        subGroupList.appendChild(subGroup)
      })

      currentGroup.appendChild(subGroupList)
    }

    //appending after sub groups
    groupList.appendChild(currentGroup)
    $(currentGroup).data(grp)
  })

  me.dom.groupsTree.appendChild(groupList)
  me.dom.groupsPanel.appendChild(me.dom.groupsTree)

};

ResourceSchedulerBase.prototype.addHeaderControls = function () {
  var me = this;

  if (me.isAdministrator) {
    me.dom.editSchedulerButton = document.createElement('button')
    me.dom.editSchedulerButton.setAttribute('id', 'btnEdit')
    me.dom.editSchedulerButton.setAttribute('type', 'button')
    me.dom.editSchedulerButton.innerHTML = "<i class='fa fa-edit fa-2x'></i> Edit"
    me.dom.editSchedulerButton.className = "btn btn-sm hvr-overline-from-center"
    me.dom.groupsHeader.appendChild(me.dom.editSchedulerButton)
  }

  //Status Legend
  me.dom.statusLegendButton = document.createElement('button');
  me.dom.statusLegendButton.id = 'btnStatusLegend';
  me.dom.statusLegendButton.className = 'btn btn-sm hvr-overline-from-center';
  me.dom.statusLegendButton.innerHTML = "<i class='fa fa-list fa-2x' /> Legend"
  me.dom.statusLegendButton.setAttribute('type', 'button');
  me.dom.statusLegendButton.setAttribute('title', 'View Legend')
  me.dom.timelineHeader.appendChild(me.dom.statusLegendButton)

  //daterange picker
  me.dom.dateRangeButton = document.createElement('button');
  me.dom.dateRangeButton.id = 'btnDateRange';
  me.dom.dateRangeButton.className = 'btn btn-sm hvr-overline-from-center';
  me.dom.dateRangeButton.innerHTML = "<i class='fa fa-calendar fa-2x' /> Dates"
  me.dom.dateRangeButton.setAttribute('type', 'button');
  me.dom.dateRangeButton.setAttribute('title', 'Select Dates')
  me.dom.timelineHeader.appendChild(me.dom.dateRangeButton)

  //show stats button
  me.dom.showStatsButton = document.createElement('button')
  me.dom.showStatsButton.id = 'btnShowStats'
  me.dom.showStatsButton.className = 'btn btn-sm hvr-overline-from-center'
  me.dom.showStatsButton.innerHTML = "<i class='fa fa-bar-chart-o fa-2x' /> Show Stats"
  me.dom.showStatsButton.setAttribute('type', 'button')
  me.dom.showStatsButton.setAttribute('title', 'Show Stats')
  me.dom.timelineHeader.appendChild(me.dom.showStatsButton)

  //show-hide resources button
  me.dom.switchResourcesButton = document.createElement('button')
  me.dom.switchResourcesButton.id = 'btnSwitchResources'
  me.dom.switchResourcesButton.className = 'btn btn-sm hvr-overline-from-center'
  me.dom.switchResourcesButton.innerHTML = "<i class='fa fa-eye-slash fa-2x' /> Hide Resources"
  me.dom.switchResourcesButton.setAttribute('type', 'button')
  me.dom.switchResourcesButton.setAttribute('title', 'Hide Resources')
  me.dom.timelineHeader.appendChild(me.dom.switchResourcesButton)

  //refresh button
  me.dom.refreshButton = document.createElement('button')
  me.dom.refreshButton.id = 'btnRefresh'
  me.dom.refreshButton.className = 'btn btn-sm hvr-overline-from-center'
  me.dom.refreshButton.innerHTML = "<i class='fa fa-refresh fa-2x' /> Refresh"
  me.dom.refreshButton.setAttribute('type', 'button')
  me.dom.refreshButton.setAttribute('title', 'Refresh')
  me.dom.timelineHeader.appendChild(me.dom.refreshButton)

  //size slider
  me.dom.sizeSlider = document.createElement('input')
  me.dom.sizeSlider.id = 'sizeSlider'
  me.dom.sizeSlider.setAttribute('data-slider-id', 'sizeSliderID')
  me.dom.sizeSlider.setAttribute('type', 'text')
  //me.dom.sizeSlider.className = 'btn btn-sm hvr-overline-from-center'
  me.dom.timelineHeader.appendChild(me.dom.sizeSlider)
};

ResourceSchedulerBase.prototype.createResourceList = function () {
  var me = this;

  me.dom.rightPanel = document.createElement('div')
  me.dom.rightPanel.className = 'ms-container'
  me.dom.rightPanel.id = "sidebarRight"

  me.dom.buttonContainer = document.createElement('div')
  me.dom.buttonContainer.className = 'btn-group';
  me.dom.buttonContainer.style.width = "98%"

  me.dom.addSelectedResourcesButton = document.createElement('button')
  me.dom.addSelectedResourcesButton.style.width = "20%"
  me.dom.addSelectedResourcesButton.id = "addSelectedSchedulerButton"
  me.dom.addSelectedResourcesButton.setAttribute('type', 'button')
  me.dom.addSelectedResourcesButton.className = "btn btn-xs btn-default"
  me.dom.addSelectedResourcesButton.innerHTML = "<i class='fa fa-check-circle-o'></i>"
  me.dom.addSelectedResourcesButton.title = "select"

  me.dom.removeSelectedResourcesButton = document.createElement('button')
  me.dom.removeSelectedResourcesButton.style.width = "20%"
  me.dom.removeSelectedResourcesButton.id = "removeSelectedSchedulerButton"
  me.dom.removeSelectedResourcesButton.setAttribute('type', 'button')
  me.dom.removeSelectedResourcesButton.className = "btn btn-xs btn-default"
  me.dom.removeSelectedResourcesButton.innerHTML = "<i class='fa fa-times-circle-o'></i>"
  me.dom.removeSelectedResourcesButton.title = "deselect"

  me.dom.deleteSelectedResourcesButton = document.createElement('button')
  me.dom.deleteSelectedResourcesButton.style.width = "20%"
  me.dom.deleteSelectedResourcesButton.id = "deleteSelectedSchedulerButton"
  me.dom.deleteSelectedResourcesButton.setAttribute('type', 'button')
  me.dom.deleteSelectedResourcesButton.className = "btn btn-xs btn-default"
  me.dom.deleteSelectedResourcesButton.innerHTML = "<i class='fa fa-trash-o'></i>"
  me.dom.deleteSelectedResourcesButton.title = "remove selected"

  me.dom.selectionContainer = document.createElement('div')
  me.dom.selectionContainer.className = "ms-selection"

  me.dom.resourceSelectionList = document.createElement('ul')
  me.dom.resourceSelectionList.className = 'ms-list'
  me.dom.resourceSelectionList.setAttribute("id", "resourceSelectionList")
  me.dom.resourceSelectionList.setAttribute("tabindex", -1)

  me.dom.selectionContainer.appendChild(me.dom.resourceSelectionList)
  me.dom.buttonContainer.appendChild(me.dom.addSelectedResourcesButton)
  me.dom.buttonContainer.appendChild(me.dom.removeSelectedResourcesButton)
  me.dom.buttonContainer.appendChild(me.dom.deleteSelectedResourcesButton)
  me.dom.rightPanel.appendChild(me.dom.buttonContainer)
  me.dom.rightPanel.appendChild(me.dom.selectionContainer)

  $(me.dom.resourceSelectionList).selectable()
  $(me.dom.addSelectedResourcesButton).on('click', function () {
    var resources = []
    $("li.ui-selected", me.dom.resourceSelectionList).each(function () {
      this.className = "ms-elem-selection ms-selected"
      resources.push(this.resource)
    })
    me.addGroups(resources)
    me.refreshResourcesAndBookings()
  })
  $(me.dom.removeSelectedResourcesButton).on('click', function () {
    var resourceIDs = []
    $("li.ui-selected", me.dom.resourceSelectionList).each(function () {
      resourceIDs.push(this.resource.ResourceID)
      this.className = "ms-elem-selection"
    })
    me.removeGroups(resourceIDs)
  })
  $(me.dom.deleteSelectedResourcesButton).on('click', function () {
    var resourceIDs = []
    $("li.ui-selected", me.dom.resourceSelectionList).each(function (itm) {
      resourceIDs.push(this.resource.ResourceID)
      me.dom.resourceSelectionList.removeChild(this)
      //this.className = "ms-elem-selection"
    })
    me.removeGroups(resourceIDs)
  })

  me.dom.resourcesPanel.appendChild(me.dom.rightPanel)
};

ResourceSchedulerBase.prototype.createLegendPanel = function (legendList) {
  var me = this

  var cont = document.getElementById("timeline-container")

  legendList.Iterate(function (stat, statIndex) {

    var legendLine = document.createElement('div')
    legendLine.style.display = "block"

    var legendKey = document.createElement('div')
    legendKey.className = stat.LegendStatusKey
    legendKey.style.display = "inline-block"
    legendKey.style.border = "1px solid #000"
    legendKey.style.width = "22px"
    legendKey.style.height = "8px"

    var legendName = document.createElement('span')
    legendName.className = "legendName"
    legendName.style.display = "inline-block"
    legendName.innerHTML = stat.LegendStatus

    legendLine.appendChild(legendKey)
    legendLine.appendChild(legendName)

    me.dom.legendDiv.appendChild(legendLine)

    legendLine = null
    legendKey = null
    legendName = null
  })

  //cont.appendChild(me.dom.legendDiv)

  //legendDiv = null
}

ResourceSchedulerBase.prototype.showResourcesPanel = function (clickEvent) {
  var me = this
  me.dom.groupsContainer.className = 'groups-container split split-horizontal'
  me.dom.timelineContainer.className = 'timeline-container split split-horizontal'
}

ResourceSchedulerBase.prototype.hideResourcesPanel = function (clickEvent) {
  var me = this
  me.dom.groupsContainer.className = 'groups-container split split-horizontal'
  me.dom.timelineContainer.className = 'timeline-container split split-horizontal'
}

ResourceSchedulerBase.prototype.showLegendPanel = function (clickEvent) {
  var me = this
  me.dom.legendDiv.classList.toggle('view-legend')
  me.dom.legendDiv.classList.toggle('hide-legend')
}

ResourceSchedulerBase.prototype.hideLegendPanel = function (clickEvent) {
  var me = this
  me.dom.legendDiv.classList.toggle('view-legend')
  me.dom.legendDiv.classList.toggle('hide-legend')

}

ResourceSchedulerBase.prototype.getLegendData = function () {
  me = this;
  Singular.GetDataStateless("OBLib.Maintenance.PlayoutOps.ReadOnly.ROLegendStatusList", {
    SystemID: 5 /*,
			ProductionAreaID: 10*/
  },
	function (response) {
	  if (response.Success) {
	    me.legendData = response.Data
	    me.createLegendPanel(me.legendData)
	    //OBMisc.Notifications.GritterSuccess("Scheduler Saved", "", 500)
	  }
	  else {
	    OBMisc.Notifications.GritterError("Error Fetching Legend", response.ErrorText, 3000)
	  }
	})
};

ResourceSchedulerBase.prototype.refreshSchedulerData = function () {
  var data = null,
			me = this;
  Singular.GetDataStateless("OBLib.ResourceSchedulers.New.ResourceSchedulerList", {
    ResourceSchedulerID: me.schedulerData.ResourceSchedulerID,
    FetchGroups: true,
    FetchSubGroups: true,
    FetchSubGroupResources: true
  },
	function (response) {
	  if (response.Success) {
	    me.schedulerData = response.Data[0]
	    OBMisc.Notifications.GritterSuccess("Scheduler Saved", "", 500)
	  }
	  else {
	    OBMisc.Notifications.GritterError("Error Fetching Scheduler", response.ErrorText, 3000)
	  }
	})
};



ResourceSchedulerBase.prototype.addResourcesToTimeline = function (resourceList) {
  var me = this
  var groupsToAdd = []
  resourceList.Iterate(function (item, indx) {
    var alreadyAdded = (me.getGroupByResourceID(item.ResourceID) ? true : false)
    if (!alreadyAdded) {
      groupsToAdd.push(item)
      var li = document.createElement('li')
      if (item.SelectedByDefault) {
        li.className = "ms-elem-selection ms-selected"
      }
      else {
        li.className = "ms-elem-selection"
      }
      li.setAttribute("id", item.ResourceID.toString() + "-selection")
      li.resource = item
      var txt = document.createElement("span")
      txt.innerHTML = item.ResourceName
      li.appendChild(txt)
      me.dom.resourceSelectionList.appendChild(li)
    }
  })
  if (groupsToAdd.length > 0) {
    me.addGroups(groupsToAdd)
  }
};

ResourceSchedulerBase.prototype.removeResourcesFromTimeline = function (resourceList) {
  var me = this;
  var resourceIDs = []
  resourceList.Iterate(function (item, indx) {
    resourceIDs.push(item.ResourceID)
    var id = item.ResourceID.toString() + "-selection"
    var li = document.getElementById(id)
    try {
      me.dom.resourceSelectionList.removeChild(li)
    } catch (ex) {
      //if this breaks, it is because the resource node has already been removed when another group that the resource is also in, was removed
      //console.log(ex)
    }
  })
  me.removeGroups(resourceIDs)
};

ResourceSchedulerBase.prototype.addSubGroupResources = function (subGroup) {
  var me = this;
  me.addResourcesToSelectionList(subGroup.ResourceSchedulerSubGroupResourceList)
  me.addGroups(subGroup.ResourceSchedulerSubGroupResourceList)
  //me.refreshResourcesAndBookings()
};

ResourceSchedulerBase.prototype.removeSubGroupResources = function (subGroup) {
  var me = this;
  var resourceIDs = []
  subGroup.ResourceSchedulerSubGroupResourceList.Iterate(function (itm, itmIndx) { resourceIDs.push(itm.ResourceID) })
  me.removeGroups(resourceIDs)
  me.removeResourcesFromSelectionList(subGroup.ResourceSchedulerSubGroupResourceList)
};

ResourceSchedulerBase.prototype.addResourcesToSelectionList = function (subGroupResourceList) {
  var me = this;
  subGroupResourceList.Iterate(function (item, indx) {
    var li = document.createElement('li')
    li.className = "ms-elem-selection ms-selected"
    li.setAttribute("id", item.ResourceID.toString() + "-selection")
    li.resource = item
    var txt = document.createElement("span")
    txt.innerHTML = item.ResourceName
    li.appendChild(txt)
    me.dom.resourceSelectionList.appendChild(li)
  })
};

ResourceSchedulerBase.prototype.removeResourcesFromSelectionList = function (resourceList) {
  var me = this;
  resourceList.Iterate(function (item, indx) {
    var id = item.ResourceID.toString() + "-selection"
    var li = document.getElementById(id)
    //li.className = "ms-elem-selection ms-selected"
    //li.setAttribute("id", item.ResourceID.toString() + "-selection")
    //var txt = document.createElement("span")
    //txt.innerHTML = item.ResourceName
    //li.appendChild(txt)
    me.dom.resourceSelectionList.removeChild(li)
  })
};

//DateRangePicker
ResourceSchedulerBase.prototype.onDateRangeChanged = function (event, picker) {
  var me = this;
  me.onBoundsReached(picker.startDate, picker.endDate, true)
  //me.updateTimelineDates(picker.startDate, picker.endDate)
};

ResourceSchedulerBase.prototype.updateTimelineDates = function (startDateMoment, endDateMoment) {
  var me = this

  var selectedStart = startDateMoment
  var selectedEnd = endDateMoment

  if (me.schedulerData.DefaultDateViewTypeID == 1) {
    //today view
    selectedStart.startOf('day')
    selectedEnd.endOf('day')
  }
  else if (me.schedulerData.DefaultDateViewTypeID == 2) {
    //3 day view
    //add 1 day to either side
    selectedStart.add(-1, 'days')
    selectedEnd.add(1, 'days')
  }
  else if (me.schedulerData.DefaultDateViewTypeID == 3) {
    //current week
    selectedStart.startOf('week')
    selectedEnd.endOf('week')
  }
  else if (me.schedulerData.DefaultDateViewTypeID == 4) {
    //current month
    selectedStart.startOf('month')
    selectedEnd.endOf('month')
  }

  //start date
  me.schedulerData.CurrentStartDate = selectedStart.toDate()
  var maxS = selectedStart.clone()
  maxS.add(-me.schedulerData.MaxDaysBefore, 'days')
  me.schedulerData.MaxStartDate = maxS.toDate()

  //end date
  me.schedulerData.CurrentEndDate = selectedEnd.toDate()
  var maxE = selectedEnd.clone()
  maxE.add(me.schedulerData.MaxDaysAfter, 'days')
  me.schedulerData.MaxEndDate = maxE.toDate()

  me.timelineOptions.start = me.schedulerData.CurrentStartDate
  me.timelineOptions.end = me.schedulerData.CurrentEndDate
  me.timelineOptions.min = me.schedulerData.MaxStartDate
  me.timelineOptions.max = me.schedulerData.MaxEndDate

  me.visTimeline.setOptions(me.timelineOptions)
  me.refreshResourcesAndBookings()

};

ResourceSchedulerBase.prototype.onDateRangeShown = function (event, picker) {
  picker.showCalendars();
};

ResourceSchedulerBase.prototype.selectResources = function () {
  $("div#resources").css({ left: '0px' });
};

ResourceSchedulerBase.prototype.onHeaderChanged = function (eventProps) {
  return eventProps;
};

ResourceSchedulerBase.prototype.addCustomHeaderContent = function (headerControlGroup) {

};

ResourceSchedulerBase.prototype.onApply = function (eventProps) {
  var me = this;

  $("#ApplyResultsModal").on('shown.bs.modal', function () {
    me.applySelections(eventProps);
  });

  $("#ApplyResultsModal").on('hidden.bs.modal', function () {
    me.afterSelectionsApplied();
  });

  $("#ApplyResultsModal").modal();

};

ResourceSchedulerBase.prototype.applySelections = function (eventProps) {
  var me = this;

  var selections = me.visTimeline.itemSet.getAllSelections();
  //if (eventProps) {
  //  if (selections.items.indexOf(eventProps.bookingItem.id) < 0) {
  //    selections.items.push(eventProps.bookingItem.id)
  //    //TODO: trigger select
  //  }
  //}
  var _resources = me.getGroupsByGuids(selections.groups);
  var _resourceBookings = me.getResourceBookingItems(selections.items);

  //create the selection template to be passed to the server
  ViewModel.ApplySelectionTemplateList([]);

  _resourceBookings.Iterate(function (item, bIndx) {
    _resources.Iterate(function (resource, rIndx) {
      var newSelectionTemplate = ViewModel.ApplySelectionTemplateList.AddNew();
      OBMisc.Notifications.GritterError("Disabled", "Redo fetch of discipline in new Group Structure", 3000)
      //var rsSubGroup = me.getRSSubGroupData(resource.Resource.RSSubGroupResourceID);
      //newSelectionTemplate.ResourceBookingID(item.Booking.ResourceBookingID);
      //newSelectionTemplate.ResourceID(resource.Resource.ResourceID);
      //newSelectionTemplate.DisciplineID(rsSubGroup.DisciplineID);
      //newSelectionTemplate.ProductionSystemAreaID(item.Booking.ProductionSystemAreaID);
    });
  });

  var selectionTemplateList = KOFormatter.Serialise(ViewModel.ApplySelectionTemplateList());
  ViewModel.CallServerMethod("ApplySelections", {
    selectionTemplates: selectionTemplateList
  }, function (response) {
    if (response.Success) {
      ViewModel.ApplyResultList.Set(response.Data);
    } else {
      OBMisc.Modals.Error('Error Processing', 'An error occured while applying the selections', response.ErrorText, null)
    }
    me.clearSelections();
  });

};

ResourceSchedulerBase.prototype.afterSelectionsApplied = function () {
  var me = this;
  ViewModel.ApplyResultList([]);
  me.clearSelectionEdits();
};

ResourceSchedulerBase.prototype.applyResultSuccess = function () {
  var newList = [];
  ViewModel.ApplyResultList().Iterate(function (ar, arInd) {
    if (ar.MessageTypeID() == 1) {
      newList.push(ar);
    }
  });
  return newList;
};

ResourceSchedulerBase.prototype.applyResultWarning = function () {
  var newList = [];
  ViewModel.ApplyResultList().Iterate(function (ar, arInd) {
    if (ar.MessageTypeID() == 2) {
      newList.push(ar);
    }
  });
  return newList;
};

ResourceSchedulerBase.prototype.applyResultError = function () {
  var newList = [];
  ViewModel.ApplyResultList().Iterate(function (ar, arInd) {
    if (ar.MessageTypeID() == 3) {
      newList.push(ar);
    }
  });
  return newList;
};

ResourceSchedulerBase.prototype.applyResultInfo = function () {
  var newList = [];
  ViewModel.ApplyResultList().Iterate(function (ar, arInd) {
    var tps = [1, 2, 3];
    if (tps.indexOf(ar.MessageTypeID()) < 0) {
      newList.push(ar);
    }
  });
  return newList;
};

ResourceSchedulerBase.prototype.applyResultCss = function (applyResult) {
  switch (applyResult.MessageTypeID()) {
    case 1:
      return 'alert alert-success alert-white apply-alert';
      break;
    case 2:
      return 'alert alert-warning alert-white apply-alert';
      break;
    case 3:
      return 'alert alert-danger alert-white apply-alert';
      break;
    default:
      return 'alert alert-info alert-white apply-alert';
      break;
  }
};

ResourceSchedulerBase.prototype.applyResultStrongText = function (applyResult) {
  var hasDescript = (applyResult.BookingDescription().trim().length > 0 ? true : false);
  var spacer = (hasDescript ? ': ' : '');
  var descript = (hasDescript ? applyResult.BookingDescription() : '');
  switch (applyResult.MessageTypeID()) {
    case 1:
      return applyResult.ResourceName() + spacer + descript;
      break;
    case 2:
      return applyResult.ResourceName() + spacer + descript;
      break;
    case 3:
      return applyResult.ResourceName() + spacer + descript;
      break;
    default:
      return applyResult.ResourceName() + spacer + descript;
      break;
  }
};

ResourceSchedulerBase.prototype.applyResultIconCss = function (applyResult) {
  switch (applyResult.MessageTypeID()) {
    case 1:
      return 'fa fa-check';
      break;
    case 2:
      return 'fa fa-exclamation-triangle';
      break;
    case 3:
      return 'fa fa-exclamation-circle';
      break;
    default:
      return 'fa fa-exclamation';
      break;
  }
};

ResourceSchedulerBase.prototype.applyResultMessage = function (applyResult) {
  if (applyResult.HasProcessed()) {
    if (applyResult.ProcessedSuccessfully()) {
      return applyResult.ProcessedSuccessfullyMessage();
    } else {
      return applyResult.ProcessingFailedMessage();
    }
  } else {
    return applyResult.Message();
  }
};

ResourceSchedulerBase.prototype.clearSelections = function () {
  var me = this;
  var selections = me.visTimeline.itemSet.getAllSelections();
  var resources = me.getGroupsByGuids(selections.groups);
  var resourceBookings = me.getResourceBookingItems(selections.items);
  resources.Iterate(function (grp, grpIndx) {
    var internalGrp = me.visTimeline.itemSet.groups[grp.id];
    internalGrp.unselect();
  });
  me.visTimeline.itemSet.groupSelection = [];
  resourceBookings.Iterate(function (rb, rbIndx) {
    var internalRB = me.visTimeline.itemSet.items[rb.id];
    internalRB.unselect();
  });
  me.visTimeline.itemSet.setSelection([]);
  me.visTimeline.itemSet.selection = [];
};

ResourceSchedulerBase.prototype.clearSelectionEdits = function () {
  var me = this;
  var selections = me.visTimeline.itemSet.getAllSelections();
  var resources = me.getGroupsByGuids(selections.groups);
  var resourceBookings = me.getResourceBookingItems(selections.items);
  resources.Iterate(function (grp, grpIndx) {
    var internalGrp = me.visTimeline.itemSet.groups[grp.id];
    internalGrp.handleSelect();
  });
  resourceBookings.Iterate(function (rb, rbIndx) {
    me.hubManager.bookingOutOfEdit(rb.Booking.ResourceBookingID);
  });
};

ResourceSchedulerBase.prototype.setSchedulerBusy = function () {
  var me = this;
  me.isBusy = true;
  this.dom.busyDiv.style.display = 'block';
};

ResourceSchedulerBase.prototype.setSchedulerNotBusy = function () {
  var me = this;
  me.isBusy = false;
  this.dom.busyDiv.style.display = 'none';
};

ResourceSchedulerBase.prototype.showSynergyMenu = function () {
  var me = this;
  var synergyMenuSelector = "#" + me.defaultOptions.synergyMenuID
  $(synergyMenuSelector).addClass('cbp-spmenu-open')
  $(synergyMenuSelector).css({ width: '1080px', right: '0px' })
  me.synergyEventsControl().criteria().StartDate(me.schedulerData.CurrentStartDate)
  me.synergyEventsControl().criteria().EndDate(me.schedulerData.CurrentEndDate)
  me.synergyEventsControl().GetSynergyEvents()
  me.makeGroupsDroppable()
};

ResourceSchedulerBase.prototype.hideSynergyMenu = function () {
  var me = this;
  var synergyMenuSelector = "#" + me.defaultOptions.synergyMenuID
  $(synergyMenuSelector).css({ width: '-1080px', right: '-1080px' })
  $(synergyMenuSelector).removeClass('cbp-spmenu-open')
  me.removeDropFromGroups()
};

ResourceSchedulerBase.prototype.editScheduler = function (eventProps) {
  var me = this;

  $("#EditSchedulerModal").off('shown.bs.modal')
  $("#EditSchedulerModal").off('hidden.bs.modal')

  $("#EditSchedulerModal").on('shown.bs.modal', function () {
    ResourceSchedulerBO.getScheduler(me.schedulerData.ResourceSchedulerID)
  })

  $("#EditSchedulerModal").on('hidden.bs.modal', function () {

  })

  $("#EditSchedulerModal").modal()

};

ResourceSchedulerBase.prototype.getRSResourceList = function (resourceID, resourceIDsXML, resourceBookingID, resourceBookingIDsXML, startDateBuffer, startDate, endDate, endDateBuffer, afterFetch, afterFail) {
  var me = this

  Singular.GetDataStateless("OBLib.Resources.RSResourceList", {
    ResourceID: resourceID,
    ResourceIDs: resourceIDsXML,
    ResourceBookingID: resourceBookingID,
    ResourceBookingIDs: resourceBookingIDsXML,
    StartDateBuffer: startDateBuffer,
    StartDate: startDate,
    EndDate: endDate,
    EndDateBuffer: endDateBuffer,
    ResourceSchedulerID: me.schedulerData.ResourceSchedulerID
  }, function (response) {
    if (response.Success) {
      if (afterFetch) {
        afterFetch(response)
      }
    }
    else {
      OBMisc.Notifications.GritterError("Error Fetching Bookings", response.ErrorText, 3000)
      if (afterFail) {
        afterFail(response)
      }
    }
  })

};

ResourceSchedulerBase.prototype.addRSResourceData = function (resources) {
  var me = this;
  var grp = null
  resources.Iterate(function (res, resIndex) {
    grp = me.getGroupByResourceID(res.ResourceID)
    if (!grp) {
      //add the group
      grp = me.createGroupItem(res)
      me.resources.add(grp)
      //add bookings
      me.addBookings(res.RSResourceBookingList)
    } else {
      //update group data
      grp.Resource = res
      //update the group content
      grp.content = me.getGroupContent(res)
      //trigger the ui change
      me.resources.update(grp)
      //add bookings
      me.addBookings(res.RSResourceBookingList)
    }
  })
  me.visTimeline.setGroups(me.resources)
};

ResourceSchedulerBase.prototype.refreshResourcesAndBookings = function () {
  var me = this;

  me.setSchedulerBusy()

  var sb = new Date(me.schedulerData.MaxStartDate);
  var s = new Date(me.schedulerData.CurrentStartDate);
  var e = new Date(me.schedulerData.CurrentEndDate);
  var eb = new Date(me.schedulerData.MaxEndDate);
  var sdb = sb.format('dd MMM yyyy HH:mm');
  var sd = s.format('dd MMM yyyy HH:mm');
  var ed = e.format('dd MMM yyyy HH:mm');
  var edb = eb.format('dd MMM yyyy HH:mm');
  var selectedResourceIDs = me.getSelectedResourceXmlIDs();

  me.getRSResourceList(null, selectedResourceIDs, null, "", sdb, sd, ed, edb,
											 function (response) {
											   me.clearBookings()
											   me.addRSResourceData(response.Data)
											   me.setSchedulerNotBusy()
											 },
											 function (response) {
											   me.setSchedulerNotBusy()
											 })

};

//Scheduler Editing
ResourceSchedulerBase.prototype.liClicked = function (obj, element) {
  event.stopPropagation() //<-- stop child li's from triggering parent li click event
  var label = $(element).children('label.tree-toggler')
  var icon = $(label).find(".fa.toggle-icon")
  //icon
  if (icon.hasClass("fa-folder-o")) {
    icon.removeClass("fa-folder-o").addClass("fa-folder-open-o")
  }
  else {
    icon.removeClass("fa-folder-open-o").addClass("fa-folder-o")
  }
  //expand the tree
  $(label).parent().children('ul.tree').toggle(300, function () {
    $(label).parent().toggleClass("open")
    $(".tree .nscroller").nanoScroller({ preventPageScrolling: true })
  })
}

ResourceSchedulerBase.prototype.liIconClicked = function (obj, element) {
  event.stopPropagation() //<-- stop child li's from triggering parent li click event
  var label = $(element).parent('label.tree-toggler')
  var li = $(label).parent('li')
  var icon = $(label).find(".fa.toggle-icon")
  //icon
  if (icon.hasClass("fa-folder-o")) {
    icon.removeClass("fa-folder-o").addClass("fa-folder-open-o")
  }
  else {
    icon.removeClass("fa-folder-open-o").addClass("fa-folder-o")
  }
  //expand the tree
  $(label).parent().children('ul.tree').toggle(300, function () {
    $(label).parent().toggleClass("open")
    $(".tree .nscroller").nanoScroller({ preventPageScrolling: true })
  })
}

ResourceSchedulerBase.prototype.addEditModeEvents = function () {
  var me = this;
  //add the sortability to the resourceNodes
  if (me.isAdministrator) {
    //group sorting
    $("#EditResourceSchedulerModal ul.scheduler-groups").sortable({
      delay: 100,
      placeholder: "ui-state-highlight",
      update: function (event, ui) {
        me.afterItemSorted()
      }
    })
    //sub group sorting
    $("#EditResourceSchedulerModal ul.scheduler-subgroups").sortable({
      delay: 100,
      placeholder: "ui-state-highlight",
      update: function (event, ui) {
        me.afterItemSorted()
      }
    })
    //resource sorting
    $("#EditResourceSchedulerModal ul.scheduler-subgroupresources").sortable({
      delay: 100,
      placeholder: "ui-state-highlight",
      update: function (event, ui) {
        me.afterItemSorted()
      }
    })
  }
}

ResourceSchedulerBase.prototype.afterItemSorted = function () {
  //groups--------------------------------------------------------------
  var groups = $("#EditResourceSchedulerModal ul.scheduler-groups > li")
  groups.each(function (grpIndx) {
    //var group = this.item
    //group.GroupOrder = grpIndx + 1
    ko.dataFor(this).GroupOrder(grpIndx + 1)
    //sub groups--------------------------------------------------------
    var subGroups = $(this).find('ul.scheduler-subgroups > li')
    subGroups.each(function (subGroupIndx) {
      //var subGroup = group.ResourceSchedulerSubGroupList.Find("ResourceSchedulerSubGroupID", this.item.ResourceSchedulerSubGroupID)
      //subGroup.SubGroupOrder = subGroupIndx + 1
      ko.dataFor(this).SubGroupOrder(subGroupIndx + 1)
      //sub group resources---------------------------------------------
      var subGroupResources = $(this).find('ul.scheduler-subgroupresources > li')
      subGroupResources.each(function (subGroupResourceIndx) {
        //var subGroupResource = subGroup.ResourceSchedulerSubGroupResourceList.Find("ResourceSchedulerSubGroupResourceID", this.item.ResourceSchedulerSubGroupResourceID) //this.item
        var resourceGlobalIndex = $(this).index('li.orderable-subgroupresource')
        //subGroupResource.ResourceOrder = resourceGlobalIndex + 1
        ko.dataFor(this).ResourceOrder(resourceGlobalIndex + 1)
      })
    })
    OBMisc.Notifications.GritterInfo("Ordering Updated", "Save your changes", 1500)
  })
};

ResourceSchedulerBase.prototype.showEditScheduler = function () {
  var me = this
  $("#" + me.defaultOptions.editSchedulerModalID).off("shown.bs.modal")
  $("#" + me.defaultOptions.editSchedulerModalID).on("shown.bs.modal", function () {
    Singular.Validation.CheckRules(ViewModel.EditResourceScheduler())
    me.addEditModeEvents()
  })
  $("#" + me.defaultOptions.editSchedulerModalID).modal()
};

ResourceSchedulerBase.prototype.addNewGroup = function (scheduler) {
  scheduler.ResourceSchedulerGroupList.AddNew()
};

ResourceSchedulerBase.prototype.removeSchedulerGroup = function (group) {
  var prnt = group.GetParent()
  prnt.ResourceSchedulerGroupList.RemoveNoCheck(group)
};

ResourceSchedulerBase.prototype.addNewSubGroup = function (schedulerGroup) {
  schedulerGroup.ResourceSchedulerSubGroupList.AddNew()
};

ResourceSchedulerBase.prototype.removeSchedulerSubGroup = function (subGroup) {
  var prnt = subGroup.GetParent()
  prnt.ResourceSchedulerSubGroupList.RemoveNoCheck(subGroup)
};

ResourceSchedulerBase.prototype.repopulateSubGroup = function (subGroup) {
  //ViewModel.CallServerMethod("PopulateResourcesForSubGroup",
  //                           {
  //                             ResourceSchedulerSubGroup: subGroup.Serialise()
  //                           },
  //                           function (response) {
  //                             if (response.Success) {

  //                             }
  //                             else {

  //                             }
  //                           })
};

ResourceSchedulerBase.prototype.removeSchedulerResource = function (subGroupResource) {
  var prnt = subGroupResource.GetParent()
  prnt.ResourceSchedulerSubGroupResourceList.RemoveNoCheck(subGroupResource)
};

ResourceSchedulerBase.prototype.saveResourceScheduler = function (scheduler) {
  var me = this
  ViewModel.CallServerMethod("SaveResourceScheduler", {
    Scheduler: scheduler.Serialise()
  },
		function (response) {
		  if (response.Success) {
		    ViewModel.EditResourceScheduler.Set(response.Data)
		    me.refreshSchedulerData()
		    me.addEditModeEvents()
		    OBMisc.Notifications.GritterSuccess("Scheduler Saved Successfully", "", 500)
		  } else {
		    OBMisc.Notifications.GritterError("Error Saving Scheduler", response.ErrorText, 1000)
		  }
		})
  //KOFormatterFull.IncludeClean = true
  //KOFormatterFull.IncludeCleanProperties = true
  //var ser = KOFormatterFull.Serialise(ViewModel.EditResourceScheduler())
  //ViewModel.CallServerMethod("SaveResourceScheduler", {
  //  scheduler: ser
  //}, function (response) {
  //  if (response.Success) {
  //    me.refreshSchedulerData()
  //    OBMisc.Notifications.GritterSuccess("Scheduler Saved Successfully", "", 1500)
  //  } else {
  //    OBMisc.Notifications.GritterError("Error Saving Scheduler", response.ErrorText, 3000)
  //  }
  //})
};


ResourceSchedulerBase.prototype.autoCalculateSchedulerOrdering = function (scheduler) {
  var me = this;
  me.afterItemSorted()
};

//#endregion

//#region Timeline 

//timeline methods
ResourceSchedulerBase.prototype.setupTimeline = function () {
  var me = this;

  //instantiate the datasets
  me.resources = new vis.DataSet();
  me.resourceBookings = new vis.DataSet();
  me.currentEventProps = null;
  me.itemDragging = false;

  var sd = new Date(this.schedulerData.CurrentStartDate),
			ed = new Date(this.schedulerData.CurrentEndDate),
			minD = new Date(this.schedulerData.MaxStartDate),
			maxD = new Date(this.schedulerData.MaxEndDate);

  //setup options
  me.timelineOptions = {
    editable: {
      add: false,
      updateTime: false,
      updateGroup: false,
      remove: false,
      update: true
    },
    selectable: true,
    multiselect: true,
    orientation: 'top',
    showCurrentTime: true,
    maxHeight: '84vh',
    //showCustomTime: false,
    clickToUse: false,
    stack: true,
    start: sd,
    end: ed,
    min: minD,
    max: maxD,
    zoomMin: 1000 * 60 * 60,             // 1 hour
    zoomMax: 1000 * 60 * 60 * 24 * 7 * 31,     // 1 month
    autoResize: true,
    type: 'range',
    moveable: true,
    margin: { item: 0 },
    width: '100%',
    order: function (booking1, booking2) {
      if (booking1.start > booking2.start) { return 1 }
      else if (booking1.start < booking2.start) { return -1 }
      else { return 0 };
    },
    groupOrder: function (resource1, resource2) {
      return (resource1.Resource.ResourceOrder - resource2.Resource.ResourceOrder);
    },
    onAdd: function (item, callback) {
      if (!me.isReadOnly) {
        _onItemAddInternal(item, callback);
      } else { callback(null) }
    },
    onUpdate: function (item, callback) {
      _onItemUpdateInternal(item, callback);
    },
    onMoving: function (item, callback) {
      callback(null)
      //if (!me.isReadOnly) {
      //  _onItemMovingInternal(item, callback);
      //} else { callback(null) }
    },
    onMove: function (item, callback) {
      callback(null)
      //if (!me.isReadOnly) {
      //  _onItemMovedInternal(item, callback);
      //} else { callback(null) }
    },
    onRemove: function (item, callback) {
      callback(null)
      if (!me.isReadOnly) {
        _onItemRemoveInternal(item, callback);
      } else { callback(null) }
    },
    snap: function (date, scale, step) {
      var minute = 60 * 1000;
      return Math.round(date / minute) * minute;
    },
    dataAttributes: ['resourceid', 'resourcebookingid'],
    itemHeight: 23
  };

  //create the datasets
  me.addInitialGroups();

  //create the timeline
  me.visTimeline = new vis.Timeline(me.dom.timelineElement);
  me.visTimeline.setOptions(me.timelineOptions);

  me.visTimeline.eventPropertiesFetched = function (eventprops) {
    _oneventPropertiesFetchedInternal(eventprops);
  }

  //set the resources and bookings to the timeline
  me.visTimeline.setGroups(me.resources);
  me.visTimeline.setItems(me.resourceBookings);

  //me.timeTooltip = document.createElement('div');
  //me.timeTooltip.setAttribute('id', 'CurrentTimeTooltip');
  //document.body.appendChild(me.timeTooltip);

  //add the event listeners

  me.visTimeline.on('rangechange', _rangeChangeInternal);
  me.visTimeline.on('rangechanged', _rangeChangedInternal);
  if (!me.isReadOnly) {
    me.visTimeline.on('itemSelected', _itemSelectedInternal);
    me.visTimeline.on('itemDeSelected', _itemDeSelectedInternal);
    me.visTimeline.on('groupSelected', _groupSelectedInternal);
    me.visTimeline.on('groupDeSelected', _groupDeSelectedInternal);
    me.visTimeline.on('afterCtrlReleased', _afterCtrlReleasedInternal);
    me.visTimeline.on('timechange', _timechangeInternal);
    me.visTimeline.on('timechanged', _timeChangedInternal);
    me.visTimeline.on('contextmenu', _contextMenuInternal);
    me.visTimeline.on('hover', _onHoverInternal);
    //me.visTimeline.on('select', _itemSelectedInternal);
    //me.visTimeline.on('afterItemsSelected', _afterItemsSelectedInternal);
  }

  //adjust view
  me.visTimeline.fit();

  me.refreshResourcesAndBookings();

  //#region Private/Internal methods

  function _itemSelectedInternal(properties) {
    var selections = me.visTimeline.itemSet.getAllSelections();
    var resourceBookings = me.getResourceBookingItems(selections.items);
    //add background group
    resourceBookings.Iterate(function (itm, indx) {
      me.addBackgroundGroup(itm.Booking);
    })
    //add popover
    //if (resourceBookings.length == 1) {
    //  var itm = resourceBookings[0]
    //  if (itm.Booking.PopoverContent.trim().length > 0) {
    //    _showPopover(itm.Booking)
    //  }
    //}
    me.onItemSelected(properties);
  }

  function _itemDeSelectedInternal(properties) {
    var deSelectedItems = me.getResourceBookingItems(properties.items);
    deSelectedItems.Iterate(function (itm, indx) {
      //remove the background group
      me.removeBackgroundGroup(itm.Booking)
      //remove the popover
      _removePopover(itm.Booking)
    })
    me.onItemDeselected(properties)
  }

  function _groupSelectedInternal(properties) {
    me.onGroupSelected(properties);
  }

  function _groupDeSelectedInternal(properties) {
    me.onGroupDeselected(properties);
  }

  function _afterCtrlReleasedInternal(properties) {
    //if (me.selectedResourceBookingItems.length > 0) {
    //  me.selectedResourceBookingItems.Iterate(function (itm, indx) {
    //    //me.hubManager.bookingOutOfEdit(itm.Booking.ResourceBookingID);
    //  });
    //};
    //var selectedBookings = me.getResourceBookingItems(properties.items);
    //me.selectedResourceBookingItems = me.selectedResourceBookingItems.concat(selectedBookings);
    //selectedBookings.Iterate(function (itm, indx) {
    //  me.hubManager.bookingIntoEdit(itm.Booking.ResourceBookingID);
    //});
    me.afterCtrlReleased(properties);
  }

  function _timechangeInternal(id, time) {
    //_doTimeChangeChecks(id, time);
  }

  function _timeChangedInternal(id, time) {
    //_doTimeChangedChecks(id, time)
  }

  function _rangeChangeInternal(start, end, byUser) {
    //me.timeTooltip.style.display = "none"
  }

  function _rangeChangedInternal(props) {
    if (!me.isBusy) {
      me.onRangeChanged(props.start, props.end, props.byUser)
    }
  }

  function _contextMenuInternal(props) {
    props.event.preventDefault();
    var eventProps = this.getEventProperties(event);
    switch (eventProps.what) {
      case "item":
        me.onItemContextMenu(eventProps);
        break;
      case "background":
        me.onBackgroundContextMenu(eventProps);
        break;
      case "group-label":
        me.onGroupContextMenu(eventProps);
        break;
      default:
        console.log({ message: 'unknown context requested', data: eventProps });
        break;
    }
  }

  function _onHoverInternal(eventProps) {
    me.onHover.call(me, eventProps)
  };

  function _onItemAddInternal(GroupItem, VisCallback) {
    var groupToAddTo = me.getGroupByGUID(GroupItem.group)
    if (groupToAddTo.Resource.CanAddBookings) {
      me.onItemAdd(GroupItem, VisCallback, groupToAddTo)
    } else {
      OBMisc.Modals.NotAuthorised("Not Authorised", "You are not authorised to add bookings to this resource", "");
    }
  }

  function _onItemUpdateInternal(GroupItem, VisCallback) {
    //no need to prevent default behaviour, so we just call the callback function immediately
    //VisCallback(GroupItem);
    //set the current item being worked on
    me.currentItem = GroupItem;
    //no need to set call back here as we have already called it
    //me.currentCallback = VisCallback

    //get the group of the selected item, pass this to the public method
    var group = me.getGroupByGUID(GroupItem.group);
    var visGroup = me.visTimeline.itemSet.groupsData;

    //call the public method that the dev can customise
    me.onItemDoubleClick(GroupItem, group, visGroup);
  }

  function _onItemMovingInternal(GroupItem, VisCallback) {
    me.itemDragging = true;
    if (GroupItem.Booking.IsLocked) {
      VisCallback(null);
      OBMisc.Modals.NotAuthorised("Not Authorised", "You are not allowed to move this booking", GroupItem.Booking.IsLockedReason)
    }
    else if (GroupItem.Booking.IsCancelled) {
      VisCallback(null);
      OBMisc.Modals.NotAuthorised("Not Authorised", "You are not allowed to move cancelled bookings")
    }
    else {

      var oldGroup = me.getGroupByResourceID(GroupItem.Booking.ResourceID)
      var newGroup = me.getGroupByGUID(GroupItem.group);
      var internalGroup = me.getInternalGroupByGUID(GroupItem.group)

      //can you move between groups
      if (oldGroup.Resource.CanMoveBookings && newGroup.Resource.CanMoveBookings) {
        //#region Yes

        //update the booking times
        GroupItem.Booking.StartDateTimeBuffer = GroupItem.start
        GroupItem.Booking.StartDateTime = new moment(GroupItem.Booking.StartDateTimeBuffer).add(GroupItem.Booking.StartBufferMinutes, 'minutes').toDate()
        GroupItem.Booking.EndDateTime = new moment(GroupItem.end).add(-GroupItem.Booking.EndBufferMinutes, 'minutes').toDate()
        GroupItem.Booking.EndDateTimeBuffer = GroupItem.end

        //get background group if applicable
        var backgroundGroup = me.getBackgroundGroup(GroupItem.Booking)
        if (backgroundGroup) {
          //make sure it moves as well
          backgroundGroup.start = GroupItem.start
          backgroundGroup.end = GroupItem.end
          me.resourceBookings.update(backgroundGroup)
        }

        //#region Tooltip
        //var itemElement = $('div[data-resourcebookingid="' + GroupItem.Booking.ResourceBookingID.toString() + '"]')
        //if (itemElement.length == 1) {
        //  itemElement = itemElement[0]
        //  var rect = itemElement.getBoundingClientRect()
        //  me.timeTooltip.style.left = rect.left + "px"
        //  me.timeTooltip.style.top = (rect.top - 40) + "px"
        //  me.timeTooltip.innerHTML = GroupItem.start.format("dd MMM yy HH:mm") + "   -   " + GroupItem.end.format("HH:mm")
        //  me.timeTooltip.style.display = "block"
        //}
        //#endregion

        //#region Check Clash

        var hasClashes = me.itemIsClashing(GroupItem)
        if (hasClashes) {
          if (!GroupItem.Booking.IsClashing) {
            GroupItem.Booking.IsClashing = true
            GroupItem.className = me.getDefaultBookingCssClass(GroupItem.Booking)
          }
        }
        else {
          if (GroupItem.Booking.IsClashing) {
            GroupItem.Booking.IsClashing = false
            GroupItem.className = me.getDefaultBookingCssClass(GroupItem.Booking)
          }
        }

        //#endregion

        //pass to the public method
        VisCallback(GroupItem)
        me.onItemMoving(GroupItem, VisCallback, oldGroup, newGroup)

        //#endregion
      }
      else {
        //#region No
        var canMoveOldMessage = "";
        if (oldGroup.Resource.CanMoveBookings) {
          canMoveOldMessage = "You are not authorised to move bookings from " + oldGroup.Resource.ResourceName;
        };
        var canMoveNewMessage = "";
        if (newGroup.Resource.CanMoveBookings) {
          canMoveNewMessage = "You are not authorised to move bookings to " + newGroup.Resource.ResourceName + "";
        };
        var totalMessage = (canMoveOldMessage.length > 0 ? "<p>" + canMoveOldMessage + "</p>" : "") +
													 (canMoveNewMessage.length > 0 ? "<p>" + canMoveNewMessage + "</p>" : "");
        VisCallback(null);
        OBMisc.Modals.NotAuthorised("Not Authorised", "You are not authorised to move bookings from/to either resource", totalMessage);
        //#endregion
      }

    }

  }

  function _onItemMovedInternal(GroupItem, VisCallback) {
    me.itemDragging = false;
    //set the current item being worked on
    me.currentItem = GroupItem;
    //no need to set call back here as we have already called it
    me.currentCallback = VisCallback
    //get the old group of the selected item, pass this to the public method
    var oldGroup = me.getGroupByResourceID(GroupItem.Booking.ResourceID);
    //get the new group of the selected item, pass this to the public method
    var newGroup = me.getGroupByGUID(GroupItem.group);
    //call the public method that the dev can customise
    me.onItemMoved(GroupItem, VisCallback, oldGroup, newGroup);
  }

  function _onItemRemoveInternal(GroupItem, VisCallback) {
    var groupToRemoveFrom = me.getGroupByGUID(GroupItem.group);
    if (groupToRemoveFrom.Resource.CanDeleteBookings) {
      if (GroupItem.Booking.IsLocked) {
        OBMisc.Notifications.GritterError("Not Authorised", "<div>You are not authorised to delete this booking</div><div>" + GroupItem.Booking.IsLockedReason + "</div>", 3000);
      } else {
        me.onItemRemove(GroupItem, VisCallback, groupToRemoveFrom);
      };
    } else {
      OBMisc.Notifications.GritterError("Not Authorised", "You are not authorised to delete bookings from this resource", 3000);
    };
  }

  function _oneventPropertiesFetchedInternal(eventProperties) {
    //do stuff before opening up for the developer
    me.onEventPropertiesFetched(eventProperties);
  }

  function _showPopover(ResourceBooking) {
    var elem = $("[data-resourcebookingid='" + ResourceBooking.ResourceBookingID.toString() + "']")
    $(elem).popover({
      html: true,
      content: ResourceBooking.PopoverContent,
      title: 'Summary',
      trigger: 'manual',
      container: elem,
      animation: true,
      placement: 'bottom'
    })
    $(elem).popover('show')
  };

  function _removePopover(ResourceBooking) {
    var elem = $("[data-resourcebookingid='" + ResourceBooking.ResourceBookingID.toString() + "']")
    if (elem) {
      $(elem).popover('hide')
      $(elem).popover('destroy')
    }
  };

  //#endregion

};

ResourceSchedulerBase.prototype.onItemSelected = function (properties) {
  var me = this;
  if (!me.isReadOnly) { } else { }
};

ResourceSchedulerBase.prototype.onItemDeselected = function (properties) {
  var me = this;
  if (!me.isReadOnly) { } else { }
};

ResourceSchedulerBase.prototype.onGroupSelected = function (properties) {
  var me = this;
  if (!me.isReadOnly) { } else { }
};

ResourceSchedulerBase.prototype.onGroupDeselected = function (properties) {
  var me = this;
  if (!me.isReadOnly) { } else { }
};

ResourceSchedulerBase.prototype.afterCtrlReleased = function (properties) {
  var me = this;
  if (!me.isReadOnly) { } else { }
};

ResourceSchedulerBase.prototype.onEventPropertiesFetched = function (eventProperties) {
  var me = this;
  if (!me.isReadOnly) { } else { }
};

ResourceSchedulerBase.prototype.onRangeChanged = function (start, end, byUser) {
  var me = this;

  var lowerBound = window.Scheduler.timelineOptions.min
  var upperBound = window.Scheduler.timelineOptions.max
  var isToocloseToLowerBound = (moment(lowerBound).diff(start, 'days', true) >= -1 ? true : false)
  var isTooCloseToUpperBound = (moment(upperBound).diff(end, 'days', true) <= 1 ? true : false)
  if (isToocloseToLowerBound || isTooCloseToUpperBound) {
    me.setSchedulerBusy()
    me.onBoundsReached(start, end, byUser)
  }
};

ResourceSchedulerBase.prototype.onItemAdd = function (item, callback, group) {
  //disable add via doubleClick on timeline
  callback(null);
};

ResourceSchedulerBase.prototype.onItemDoubleClick = function (item, group) {
  //the dev can override this method
  var me = this;
  switch (item.Booking.ResourceBookingTypeID) {
    //1	HR - OB City
    case 1:
      alert('OB City bookings cannot be opened here');
      break;
      //2	HR - OB (Content)
    case 2:
      alert('OB City bookings cannot be opened here');
      break;
      //3	HR - Studio
    case 3:
      if (item.Booking.HasAreaAccess) { me.beforeRoomScheduleFetch(item, group); }
      break;
      //4	HR - Leave
    case 4:
      me.updateHROffPeriod(item, group);
      break;
      //5	HR - Secondment
    case 5:
      alert('HR - Secondment bookings cannot be opened here');
      break;
      //6	HR - Ad Hoc
    case 6:
      alert('HR - Ad hoc bookings cannot be opened here');
      break;
      //7	HR - Prep Time
    case 7:
      alert('HR - Prep Time bookings cannot be opened here');
      break;
      //8	Room - Production
    case 8:
      if (item.Booking.HasAreaAccess) { me.beforeRoomScheduleFetch(item, group); }
      break;
      //9	Room - AdHoc
    case 9:
      if (item.Booking.HasAreaAccess) { me.beforeRoomScheduleFetch(item, group); }
      break;
      //10 Equipment - Feed
    case 10:
      if (item.Booking.HasAreaAccess) { me.updateEquipmentFeed(item, group); }
      break;
      //11 Vehicle
    case 11:
      me.updatePlayoutsOpsShift(item, group);
      break;
      //12 HR - Placeholder
    case 12:
      me.updatePlayoutsOpsShift(item, group);
      break;
      //13 Room - Placeholder
    case 13:
      alert('Room - Placeholder bookings cannot be opened here');
      break;
      //14 Equipment - Service
    case 14:
      alert('Equipment Service bookings cannot be opened here');
      break;
      //15 Vehicle - Placeholder
    case 15:
      alert('Vehicle - Placeholder bookings cannot be opened here');
      break;
      //HR - AdHoc Training
    case 16:
      alert('HR - AdHoc Training bookings cannot be opened here');
      break;
    case 17:
      //HR - AdHoc Meeting
      alert('HR - AdHoc Meeting bookings cannot be opened here');
      break;
    case 18:
      //HR - AdHoc Conference
      alert('/HR - AdHoc Conference bookings cannot be opened here');
      break;
    case 19:
      me.updateStudioSupervisorShift(item, group);
      break;
    case 20:
      me.updateICRShift(item, group);
      break;
    case 21:
      me.updatePlayoutsOpsShift(item, group);
      break;
    case 22:
      me.updateProgrammingShift(item, group);
      break;
    case 23:
      me.updateStageHandShift(item, group);
      break;
  };
};

ResourceSchedulerBase.prototype.onItemMoving = function (item, callback, oldGroup, newGroup) {
  var me = this;
  if (!me.isReadOnly) { callback(item) } else { }
};

ResourceSchedulerBase.prototype.onItemMoved = function (item, callback, oldGroup, newGroup) {
  var me = this;
  if (me.isReadOnly) {
    callback(null);
    return;
  } else {
    callback(item);
    //the dev can override this method
    var me = this;
    switch (item.Booking.ResourceBookingTypeID) {
      //1	HR - OB City
      case 1:
        alert('OB City bookings cannot be move ');
        break;
        //2	HR - OB (Content)
      case 2:
        alert('OB City bookings cannot be moved ');
        break;
        //3	HR - Studio
      case 3:
        alert('HR - Studio bookings cannot be moved ');
        break;
        //4	HR - Leave
      case 4:
        alert('HR - Leave bookings cannot be moved ');
        break;
        //5	HR - Secondment
      case 5:
        alert('HR - Secondment bookings cannot be moved ');
        break;
        //6	HR - Ad Hoc
      case 6:
        alert('HR - Ad hoc bookings cannot be moved ');
        break;
        //7	HR - Prep Time
      case 7:
        alert('HR - Prep Time bookings cannot be moved ');
        break;
        //8	Room - Production
      case 8:
        me.roomProductionMoved(item, callback, oldGroup, newGroup);
        break;
        //9	Room - AdHoc
      case 9:
        me.roomAdHocMoved(item, callback, oldGroup, newGroup);
        break;
        //10 Equipment - Feed
      case 10:
        me.feedBookingMoved(item, callback, oldGroup, newGroup)
        break;
        //11 Vehicle
      case 11:
        alert('Vehicle bookings cannot be moved ');
        break;
        //12 HR - Placeholder
      case 12:
        alert('HR Placeholder bookings cannot be moved ');
        break;
        //13 Room - Placeholder
      case 13:
        alert('Room - Placeholder bookings cannot be moved ');
        break;
        //14 Equipment - Service
      case 14:
        alert('Equipment Service bookings cannot be moved ');
        break;
        //15 Vehicle - Placeholder
      case 15:
        alert('Vehicle - Placeholder bookings cannot be moved ');
        break;
        //HR - AdHoc Training
      case 16:
        alert('HR - AdHoc Training bookings cannot be moved ');
        break;
      case 17:
        //HR - AdHoc Meeting
        alert('HR - AdHoc Meeting bookings cannot be moved ');
        break;
      case 18:
        //HR - AdHoc Conference
        alert('HR - AdHoc Conference bookings cannot be moved ');
        break;
      case 19:
        //Event Facilitator Shift
        alert('Event Facilitator Shift bookings cannot be moved ');
        break;
      case 20:
        //ICR Shift
        alert('ICR bookings cannot be moved ');
        break;
    }
  }
};

ResourceSchedulerBase.prototype.onItemRemove = function (item, callback, group) {
  var me = this;
  if (me.isReadOnly) {
    callback(null)
  }
  else {
    //the dev can override this method
    var me = this;
    switch (item.Booking.ResourceBookingTypeID) {
      //1	HR - OB City
      case 1:
        alert('OB City bookings cannot be opened here');
        break;
        //2	HR - OB (Content)
      case 2:
        alert('OB City bookings cannot be opened here');
        break;
        //3	HR - Studio
      case 3:
        //alert('HR - Studio bookings cannot be opened here');
        callback(null);
        me.removeProductionHRBooking(item, group);
        break;
        //4	HR - Leave
      case 4:
        //me.updateHROffPeriod(item, group);
        break;
        //5	HR - Secondment
      case 5:
        alert('HR - Secondment bookings cannot be opened here');
        break;
        //6	HR - Ad Hoc
      case 6:
        alert('HR - Ad hoc bookings cannot be opened here');
        break;
        //7	HR - Prep Time
      case 7:
        alert('HR - Prep Time bookings cannot be opened here');
        break;
        //8	Room - Production
      case 8:
        callback(null);
        me.removeAreaFromRoomSchedule(item, group);
        break;
        //9	Room - AdHoc
      case 9:
        callback(null)
        me.removeAreaFromRoomSchedule(item, group);
        break;
        //10 Equipment - Feed
      case 10:
        callback(null)
        me.removeFeed(item, group)
        break;
        //11 Vehicle
      case 11:
        me.removePlayoutOpsShifts(item, group)
        break;
        //12 HR - Placeholder
      case 12:
        alert('HR Placeholder bookings cannot be opened here');
        break;
        //13 Room - Placeholder
      case 13:
        alert('Room - Placeholder bookings cannot be opened here');
        break;
        //14 Equipment - Service
      case 14:
        alert('Equipment Service bookings cannot be opened here');
        break;
        //15 Vehicle - Placeholder
      case 15:
        alert('Vehicle - Placeholder bookings cannot be opened here');
        break;
        //HR - AdHoc Training
      case 16:
        alert('HR - AdHoc Training bookings cannot be opened here');
        break;
      case 17:
        //HR - AdHoc Meeting
        alert('HR - AdHoc Meeting bookings cannot be opened here');
        break;
      case 18:
        //HR - AdHoc Conference
        alert('/HR - AdHoc Conference bookings cannot be opened here');
        break;
      case 19:
        callback(null)
        me.removeStudioSupervisorShifts(item, group)
        break;
      case 20:
        me.removeICRShifts(item, group)
        break;
      case 21:
        callback(null)
        me.removePlayoutOpsShifts(item, group)
        break;
      case 22:
        callback(null)
        me.removeProgrammingShifts(item, group)
        break;
      case 23:
        callback(null)
        me.removeStageHandShifts(item, group)
        break;
    }
  }
};

ResourceSchedulerBase.prototype.onBackgroundContextMenu = function (eventProperties) {
  var me = this;
  if (!me.isReadOnly) {
    var groupItem = me.resources.get(eventProperties.group);
    var bookingTime = eventProperties.time.format('dd MMM yyyy HH:mm');
    timelineContext.destroy();
    timelineContext.init();
    var optionsArray = [{ header: 'Options' }];

    switch (groupItem.Resource.ResourceTypeID) {
      case 1:
        //Human
        me.humanResourceBackgroundContextMenuOptions(optionsArray, groupItem, eventProperties);
        break;
      case 2:
        //Room
        me.roomBackgroundContextMenuOptions(optionsArray, groupItem, eventProperties);
        break;
      case 3:
        //Vehicle
        me.vehicleBackgroundContextMenuOptions(optionsArray, groupItem, eventProperties);
        break;
      case 4:
        //Channel
        break;
      case 5:
        //Equipment
        me.equipmentBackgroundContextMenuOptions(optionsArray, groupItem, eventProperties);
        break;
      case 6:
        //Custom
        me.customBackgroundContextMenuOptions(optionsArray, groupItem, eventProperties);
        break;
    };

    timelineContext.show('.inline-menu', optionsArray, eventProperties);
  }
  else {

  }
};

ResourceSchedulerBase.prototype.onGroupContextMenu = function (eventProperties) {
  var me = this;
};

ResourceSchedulerBase.prototype.onItemContextMenu = function (eventProperties, callback) {
  var me = this;
  if (!me.isReadOnly) {
    var GroupItem = me.resources.get(eventProperties.group);
    var BookingItem = me.resourceBookings.get(eventProperties.item);
    var bookingTime = eventProperties.time.format('dd MMM yyyy HH:mm');
    timelineContext.destroy();
    timelineContext.init();
    var optionsArray = [{ header: 'Options' }];
    switch (BookingItem.Booking.ResourceBookingTypeID) {
      //1	HR - OB City
      case 1:
        me.hrOBCityContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //2	HR - OB (Content)
      case 2:
        me.hrOBContentContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //3	HR - Studio
      case 3:
        me.hrStudioContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //4	HR - Leave
      case 4:
        me.hrLeaveContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //5	HR - Secondment
      case 5:
        me.hrSecondmentContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //6	HR - Ad Hoc Team Building
      case 6:
        me.hrAdHocTeamBuildingContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //7	HR - Prep Time
      case 7:
        me.hrPrepTimeContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //8	Room - Production
      case 8:
        me.roomProductionContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //9	Room - AdHoc
      case 9:
        me.roomAdHocContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //10 Equipment - Feed
      case 10:
        me.equipmentFeedContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //11 Vehicle
      case 11:
        me.playoutShiftOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //12 HR - Placeholder
      case 12:
        me.playoutShiftOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //13 Room - Placeholder
      case 13:
        me.roomPlaceholderContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //14 Equipment - Service
      case 14:
        me.equipmentServiceContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //15 Vehicle - Placeholder
      case 15:
        me.vehiclePlaceholderContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //HR - AdHoc Training
      case 16:
        me.hrAdHocTrainingContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //HR - AdHoc Meeting
      case 17:
        me.hrAdHocMeetingContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //HR - AdHoc Conference
      case 18:
        me.hrAdHocConferenceContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //Event Facilitator Shift
      case 19:
        me.hrEventFacilitatorShiftContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //ICR Shift
      case 20:
        me.hrICRShiftContextMenuOptions(optionsArray, GroupItem, BookingItem, eventProperties);
        break;
        //Playout Shift
      case 21:
        me.playoutShiftOptions(optionsArray, GroupItem, BookingItem, eventProperties)
        break;
      case 22:
        //Programming Shift
        me.programmingShiftOptions(optionsArray, GroupItem, BookingItem, eventProperties)
        break;
        //StageHand Shift
      case 23:
        me.stageHandShiftOptions(optionsArray, GroupItem, BookingItem, eventProperties)
        break;
    };
    timelineContext.show('.inline-menu', optionsArray, eventProperties);
  }
  else {

  }
};

ResourceSchedulerBase.prototype.isTooCloseToBounds = function () {
  var me = this;
  //function _rangeChangedInternal(props) {
  var lowerBound = window.Scheduler.timelineOptions.min
  var upperBound = window.Scheduler.timelineOptions.max
  var isToocloseToLowerBound = (moment(lowerBound).diff(props.start, 'days', true) >= -3 ? true : false)
  var isTooCloseToUpperBound = (moment(upperBound).diff(props.end, 'days', true) <= 3 ? true : false)
  if (isToocloseToLowerBound || isTooCloseToUpperBound) {
    return true
  }
  //}
}

ResourceSchedulerBase.prototype.resizeItemHeight = function (newHeight) {
  var me = this;
  me.visTimeline.setOptions({ itemHeight: newHeight })
  me.visTimeline.redraw()
  me.visTimeline.fit()
};

ResourceSchedulerBase.prototype.previousDay = function () {
  var me = this;
  if (!me.isBusy) {
    me.setSchedulerBusy()
    var x1 = me.timelineOptions.start;
    me.timelineOptions.start = moment(me.timelineOptions.start).add(-1, 'days').hours(0).minutes(0).seconds(0).toDate()
    var x2 = me.timelineOptions.start
    me.timelineOptions.end = moment(x2).add(1, 'days').hours(0).minutes(0).seconds(0).toDate()
    me.visTimeline.setOptions({
      start: me.timelineOptions.start,
      end: me.timelineOptions.end
    })
    if (me.isTooCloseToBounds) {
      me.onBoundsReached()
    }
    else {
      me.setSchedulerNotBusy()
    }
  }
};

ResourceSchedulerBase.prototype.nextDay = function () {
  var me = this;
  if (!me.isBusy) {
    me.setSchedulerBusy()
    var x1 = me.timelineOptions.end;
    me.timelineOptions.end = moment(me.timelineOptions.end).add(1, 'days').hours(0).minutes(0).seconds(0).toDate()
    var x2 = me.timelineOptions.end
    me.timelineOptions.start = moment(x2).add(-1, 'days').hours(0).minutes(0).seconds(0).toDate()
    me.visTimeline.setOptions({
      start: me.timelineOptions.start,
      end: me.timelineOptions.end
    })
    if (me.isTooCloseToBounds) {
      me.onBoundsReached()
    }
    else {
      me.setSchedulerNotBusy()
    }
  }
};

ResourceSchedulerBase.prototype.onBoundsReached = function (start, end, byUser) {
  var me = this;

  if (start) { me.timelineOptions.start = start }
  if (end) { me.timelineOptions.end = end }

  var oldMin = new Date(me.schedulerData.MaxStartDate)
  var oldMax = new Date(me.schedulerData.MaxEndDate)
  me.schedulerData.CurrentStartDate = me.timelineOptions.start
  me.schedulerData.CurrentEndDate = me.timelineOptions.end
  me.schedulerData.MaxStartDate = moment(me.timelineOptions.start).add(-3, 'days').toDate()
  me.schedulerData.MaxEndDate = moment(me.timelineOptions.end).add(3, 'days').toDate()

  me.timelineOptions.min = me.schedulerData.MaxStartDate
  me.timelineOptions.max = me.schedulerData.MaxEndDate
  me.visTimeline.setOptions({
    min: me.timelineOptions.min,
    max: me.timelineOptions.max
  })

  //get the resource ids
  var selectedResourceIDs = me.getSelectedResourceXmlIDs();

  //determine new lower bound ranges to fetch bookings for
  //var oldMinVal = oldMinFetchDate.format('dd MMM yyyy HH:mm');
  var newMinVal = me.schedulerData.MaxStartDate.format('dd MMM yyyy HH:mm');

  //determine new upper bound ranges to fetch bookings for
  //var oldMaxVal = oldMaxFetchDate.format('dd MMM yyyy HH:mm');
  var newMaxVal = me.schedulerData.MaxEndDate.format('dd MMM yyyy HH:mm');

  me.refreshResourcesAndBookings()

  //me.getRSResourceList(null, selectedResourceIDs, null, "", newMinVal, newMaxVal,
  //                     function (response) {
  //                       me.clearBookings()
  //                       me.addRSResourceData(response.Data)
  //                       me.setSchedulerNotBusy()
  //                     },
  //                     function (response) {
  //                       me.setSchedulerNotBusy()
  //                     })

};

ResourceSchedulerBase.prototype.onHover = function (eventProperties) {
  var me = this;
  clearTimeout(me.hoverTimeout)
  if (eventProperties.what == 'item') {

  }
  else if (eventProperties.what == 'group-label') {
    console.log(eventProperties)
    var grp = me.getGroupByGUID(eventProperties.group)
    console.log(grp)
    if (grp.Resource) { console.log(grp.Resource) }
    if (grp && grp.Resource && grp.Resource.ResourceTypeID == 1) {
      me.hoverTimeout = setTimeout(function () {
        me.resourcePopoverModal.style.display = "none"
        ViewModel.CurrentRSResource.Set(grp.Resource)
        me.resourcePopoverModal.focus()
        me.resourcePopoverModal.onmouseleave = function () {
          me.resourcePopoverModal.style.display = "none"
        }
        me.resourcePopoverModal.style.display = "block"
        me.resourcePopoverModal.style.visibility = "hidden"
        me.resourcePopoverModal.style.width = "600px"
        me.resourcePopoverModal.style.height = "400px"
        me.resourcePopoverModal.style.top = (eventProperties.pageY + 400).toString() + 'px'
        me.resourcePopoverModal.style.left = (eventProperties.pageX - 25).toString() + 'px'
        var isFullyOnScreen = $(me.resourcePopoverModal).isOnScreen(false)
        if (!isFullyOnScreen) {
          me.resourcePopoverModal.style.top = ((eventProperties.pageY - 400) < 0 ? eventProperties.pageY : (eventProperties.pageY - 400)).toString() + 'px'
          me.resourcePopoverModal.style.left = (eventProperties.pageX - 25).toString() + 'px'
          var isFullyOnScreen2 = $(me.resourcePopoverModal).isOnScreen(false)
        } else {
          me.resourcePopoverModal.style.top = (eventProperties.pageY + 0).toString() + 'px'
          me.resourcePopoverModal.style.left = (eventProperties.pageX - 25).toString() + 'px'
          var isFullyOnScreen3 = $(me.resourcePopoverModal).isOnScreen(false)
        }
        me.resourcePopoverModal.style.visibility = "visible"
      }, 2000)
    } else {
      me.resourcePopoverModal.style.display = "none"
    }
  }
};

ResourceSchedulerBase.prototype.closeHRPopover = function () {
  var me = this
  me.resourcePopoverModal.style.display = "none"
};

ResourceSchedulerBase.prototype.clearBookings = function () {
  this.resourceBookings.clear()
};

//#endregion

//#region Context Menus

ResourceSchedulerBase.prototype.newRoomScheduleTemplate = function (groupItem, roomScheduleTemplateID, event) {
  var me = this
  if (!me.isReadOnly) {
    ViewModel.CurrentRoomScheduleTemplate(new RoomScheduleTemplateObject())
    ViewModel.CurrentRoomScheduleTemplate().ResourceID(groupItem.Resource.ResourceID)
    ViewModel.CurrentRoomScheduleTemplate().Room(groupItem.Resource.ResourceName)
    ViewModel.CurrentRoomScheduleTemplate().RoomID(groupItem.Resource.RoomID)
    ViewModel.CurrentRoomScheduleTemplate().OnAirTimeStart(event.data.eventProps.time.format('dd MMM yyyy HH:mm'))
    ViewModel.CurrentRoomScheduleTemplate().OnAirTimeEnd(event.data.eventProps.time.format('dd MMM yyyy HH:mm'))
    ViewModel.CurrentRoomScheduleTemplate().TemplateID(roomScheduleTemplateID)
    RoomScheduleTemplateBO.doSetupOld(ViewModel.CurrentRoomScheduleTemplate, {
      onSuccess: function (response) {
        $("#RoomScheduleTemplateModal").modal()
      },
      onFail: function (response) {
        OBMisc.Notifications.GritterError("Error During Setup", response.ErrorText, 1000)
      }
    })
  }
};

//#region Background

ResourceSchedulerBase.prototype.humanResourceBackgroundContextMenuOptions = function (optionsArray, groupItem, eventProperties) {
  var me = this;

  if (!me.isReadOnly) {
    //Off Period
    //optionsArray.push({
    //  text: 'Off Period', href: "#",
    //  args: { eventProps: eventProperties, group: groupItem },
    //  action: function (event) {
    //    me.offPeriodModal().newOffPeriodResourceScheduler(groupItem)
    //  }
    //})

    //Secondment
    //optionsArray.push({
    //  text: 'Secondment', href: "#",
    //  args: { eventProps: eventProperties, group: groupItem },
    //  action: function (event) {
    //    me.secondmentModal().newSecondmentResourceScheduler(groupItem)
    //  }
    //})

    //Shifts
    if (me.schedulerData.SystemID == 1) {
      //check for studio supervisors
      //check for stage hands
      if (groupItem.Resource.IsStageHand) {
        optionsArray.push({
          text: 'StageHand Shift', href: "#",
          args: { eventProps: eventProperties, group: groupItem },
          action: function (event) {
            me.stagehandShiftModal().newShiftResourceScheduler(groupItem, event)
          }
        })
      }
    }

    if (me.schedulerData.SystemID == 4) {
      optionsArray.push({
        text: 'ICR Shift', href: "#",
        args: { eventProps: eventProperties, group: groupItem },
        action: function (event) {
          me.icrShiftModal().newShiftResourceScheduler(groupItem, event)
        }
      })
    }

    if (me.schedulerData.SystemID == 5) {
      //optionsArray.push({
      //  text: 'Shift (Generic)', href: "#",
      //  args: { eventProps: eventProperties, group: groupItem },
      //  action: function (event) {
      //    me.playoutOpsShiftModal().newShiftResourceScheduler(groupItem, event)
      //  }
      //})
      optionsArray.push({
        text: 'Shift (MCR)', href: "#",
        args: { eventProps: eventProperties, group: groupItem },
        action: function (event) {
          me.playoutOpsShiftModalMCR().newShiftResourceScheduler(groupItem, event)
        }
      })
      //optionsArray.push({
      //  text: 'Shift (SCCR)', href: "#",
      //  args: { eventProps: eventProperties, group: groupItem },
      //  action: function (event) {
      //    //me.playoutOpsShiftModalSCCR().newShiftResourceScheduler(groupItem, event)
      //  }
      //})
    }

    if (me.schedulerData.SystemID == 8) {
      optionsArray.push({
        text: 'Programming Shift', href: "#",
        args: { eventProps: eventProperties, group: groupItem },
        action: function (event) {
          me.programmingShiftModal().newShiftResourceScheduler(groupItem, event)
        }
      })
    }

  }
};

ResourceSchedulerBase.prototype.customBackgroundContextMenuOptions = function (optionsArray, groupItem, eventProperties) {
  var me = this;
  if (!me.isReadOnly) {
    if (groupItem.Resource.ResourceID == 5091) {
      //Studio Supervisor Shift
      optionsArray.push({
        text: 'New Shift', href: "#",
        args: { eventProps: eventProperties, group: groupItem },
        action: function (event) {
          me.studioSupervisorShiftModal().newShiftResourceScheduler(groupItem, event)
        }
      })
    }
  }
};

ResourceSchedulerBase.prototype.roomBackgroundContextMenuOptions = function (optionsArray, groupItem, eventProperties) {
  var me = this;
  if (!me.isReadOnly) {
    var createOptions = {
      text: "Create",
      subMenu: []
    }

    //Booking Types
    createOptions.subMenu.push({
      text: 'Blank Production', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 4, event)
      }
    })

    //Team Building
    createOptions.subMenu.push({
      text: 'Team Building', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 5, event)
      }
    })

    //Training
    createOptions.subMenu.push({
      text: 'Training', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 6, event)
      }
    })

    //Meeting
    createOptions.subMenu.push({
      text: 'Meeting', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {

        me.newRoomScheduleTemplate(groupItem, 7, event)
      }
    })

    //Studio Maintenance
    createOptions.subMenu.push({
      text: 'Maintenance', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {

        me.newRoomScheduleTemplate(groupItem, 8, event)
      }
    })

    //Conference
    createOptions.subMenu.push({
      text: 'Conference', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 8, event)
      }
    })

    //Carte Blanche
    createOptions.subMenu.push({
      text: 'Carte Blanche', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 1, event)
      }
    })

    //Blitz AM
    createOptions.subMenu.push({
      text: 'Blitz AM', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 2, event)
      }
    })

    //Blitz PM
    createOptions.subMenu.push({
      text: 'Blitz PM', href: "#",
      args: { eventProps: eventProperties },
      action: function (event) {
        me.newRoomScheduleTemplate(groupItem, 3, event)
      }
    })

    optionsArray.push(createOptions);
  }
};

ResourceSchedulerBase.prototype.vehicleBackgroundContextMenuOptions = function (optionsArray, groupItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.equipmentBackgroundContextMenuOptions = function (optionsArray, groupItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.newProductionRoomSchedule = function (groupItem, productionRoomScheduleTemplateID, event) {
  var me = this
  if (!me.isReadOnly) {
    ViewModel.CurrentRoomScheduleArea(null)
    ViewModel.CurrentRoomScheduleArea(new RoomScheduleAreaObject())
    ViewModel.CurrentRoomScheduleArea().RoomScheduleTypeID(1)
    ViewModel.CurrentRoomScheduleArea().ResourceBookingTypeID(8)
    ViewModel.CurrentRoomScheduleArea().RoomID(null)
    ViewModel.CurrentRoomScheduleArea().Room("")
    ViewModel.CurrentRoomScheduleArea().ProductionAreaStatusID(1)
    ViewModel.CurrentRoomScheduleArea().ProductionAreaStatus("New")
    ViewModel.CurrentRoomScheduleArea().CallTime(null)
    ViewModel.CurrentRoomScheduleArea().OnAirTimeStart(null)
    ViewModel.CurrentRoomScheduleArea().OnAirTimeEnd(null)
    ViewModel.CurrentRoomScheduleArea().WrapTime(null)
    ViewModel.CurrentRoomScheduleArea().OwningSystemID(null)
    ViewModel.CurrentRoomScheduleArea().OwningProductionAreaID(null)
    ViewModel.CurrentRoomScheduleArea().SystemID(null)
    ViewModel.CurrentRoomScheduleArea().ProductionAreaID(null)
    ViewModel.CurrentRoomScheduleArea().IsOwner(true)
    ViewModel.CurrentProduction(new ProductionObject())
    switch (productionRoomScheduleTemplateID) {
      case 1:
        ProductionBO.setupAsBlitzAM(ViewModel.CurrentProduction(), event.data.eventProps.time, ViewModel.CurrentRoomScheduleArea())
        break;
      case 2:
        ProductionBO.setupAsBlitzPM(ViewModel.CurrentProduction(), event.data.eventProps.time, ViewModel.CurrentRoomScheduleArea())
        break;
      case 3:
        ProductionBO.setupAsCarteBlanche(ViewModel.CurrentProduction(), event.data.eventProps.time, ViewModel.CurrentRoomScheduleArea())
        break;
    }
    Singular.Validation.CheckRules(ViewModel.CurrentRoomScheduleArea())
    Singular.Validation.CheckRules(ViewModel.CurrentProduction())
    me.afterRoomScheduleSetup(null, groupItem)
  }
};

ResourceSchedulerBase.prototype.newAdHocRoomSchedule = function (groupItem, startTime, adhocBookingTypeID) {
  var me = this
  if (!me.isReadOnly) {
    ViewModel.CurrentRoomSchedule(null)
    Singular.SendCommand("NewAdHocRoomSchedule", {
      ResourceID: groupItem.Resource.ResourceID,
      ResourceName: groupItem.Resource.ResourceName,
      ResourceTypeID: groupItem.Resource.ResourceTypeID,
      RoomID: groupItem.Resource.RoomID,
      DateOfBooking: startTime,
      AdHocBookingTypeID: adhocBookingTypeID
    },
		function (response) {
		  //show it
		  if (response.Success) {
		    me.afterGenericRoomScheduleSetup(null, groupItem, response)
		  } else {
		    OBMisc.Modals.Error('Error Processing', 'There was an error creating the AdHoc Booking', response.ErrorText, null)
		  }
		})
  }
};

//#endregion

//#region Groups


//#endregion

//#region Items

ResourceSchedulerBase.prototype.haveBookingTimesChanged = function (GroupItem) {

  //current
  var sdb = GroupItem.Booking.StartDateTimeBuffer,
			 sd = GroupItem.Booking.StartDateTime,
			 ed = GroupItem.Booking.EndDateTime,
			edb = GroupItem.Booking.EndDateTimeBuffer;

  var osdb = GroupItem.Booking.OriginalStartDateTimeBuffer,
			 osd = GroupItem.Booking.OriginalStartDateTime,
			 oed = GroupItem.Booking.OriginalEndDateTime,
			oedb = GroupItem.Booking.OriginalEndDateTimeBuffer;

  //Start Buffer
  var startBufferDifferent = false
  if (sdb && osdb) {
    //both have values compare the dates
    var sdbValue = new moment(new Date(GroupItem.Booking.StartDateTimeBuffer)),
				osdbValue = new moment(new Date(GroupItem.Booking.OriginalStartDateTimeBuffer))
    if (!sdbValue.isSame(osdbValue, 'minute')) {
      startBufferDifferent = true
    }
  }
    //one has a value, but the other doesnt
  else if ((sdb && !osdb) || (!sdb && osdb)) {
    startBufferDifferent = true
  }

  //Start
  var startDifferent = false
  if (sd && osd) {
    //both have values compare the dates
    var sdValue = new moment(new Date(GroupItem.Booking.StartDateTime)),
				osdValue = new moment(new Date(GroupItem.Booking.OriginalStartDateTime))
    if (!sdValue.isSame(osdValue, 'minute')) {
      startDifferent = true
    }
  }
    //one has a value, but the other doesnt
  else if ((sd && !osd) || (!sd && osd)) {
    startDifferent = true
  }

  //End
  var endDifferent = false
  if (ed && oed) {
    //both have values compare the dates
    var edValue = new moment(new Date(GroupItem.Booking.EndDateTime)),
				oedValue = new moment(new Date(GroupItem.Booking.OriginalEndDateTime))
    if (!edValue.isSame(oedValue, 'minute')) {
      endDifferent = true
    }
  }
    //one has a value, but the other doesnt
  else if ((ed && !oed) || (!ed && oed)) {
    endDifferent = true
  }

  //End Buffer
  var endBufferDifferent = false
  if (edb && oedb) {
    //both have values compare the dates
    var edbValue = new moment(new Date(GroupItem.Booking.EndDateTimeBuffer)),
				oedbValue = new moment(new Date(GroupItem.Booking.OriginalEndDateTimeBuffer))
    if (!edbValue.isSame(oedbValue, 'minute')) {
      endBufferDifferent = true
    }
  }
    //one has a value, but the other doesnt
  else if ((edb && !oedb) || (!edb && oedb)) {
    endBufferDifferent = true
  }

  if (startBufferDifferent
			|| startDifferent
			|| endDifferent
			|| endBufferDifferent) {
    return true
  }

  return false

};

ResourceSchedulerBase.prototype.discardTimeChanges = function (event) {
  var me = this;

  var item = event.data.GroupItem,
			osdb = new moment(new Date(event.data.GroupItem.Booking.OriginalStartDateTimeBuffer)),
			osd = new moment(new Date(event.data.GroupItem.Booking.OriginalStartDateTime)),
			oed = new moment(new Date(event.data.GroupItem.Booking.OriginalEndDateTime)),
			oedb = new moment(new Date(event.data.GroupItem.Booking.OriginalEndDateTimeBuffer));

  item.Booking.StartDateTimeBuffer = osdb.toDate()
  item.Booking.StartDateTime = osd.toDate()
  item.Booking.EndDateTime = oed.toDate()
  item.Booking.EndDateTimeBuffer = oedb.toDate()
  item.start = item.Booking.StartDateTimeBuffer
  item.end = item.Booking.EndDateTimeBuffer

  var hasClashes = me.itemIsClashing(item)
  if (hasClashes) {
    if (!item.Booking.IsClashing) {
      item.Booking.IsClashing = true
      item.className = me.getDefaultBookingCssClass(item.Booking)
    }
  } else {
    if (item.Booking.IsClashing) {
      item.Booking.IsClashing = false
      item.className = me.getDefaultBookingCssClass(item.Booking)
    }
  }

  me.resourceBookings.update(item)

};

ResourceSchedulerBase.prototype.itemIsClashing = function (GroupItem) {
  var me = this;
  var internalGroup = me.getInternalGroupByGUID(GroupItem.group)
  var internalGroupItems = internalGroup.orderedItems.byStart
  var foundClashes = false
  var clashCount = 0
  for (i = 0; i < internalGroupItems.length; i++) {
    if (internalGroupItems[i].id != GroupItem.id) {
      var sd = internalGroupItems[i].data.start,
					ed = internalGroupItems[i].data.end
      if (OBMisc.Dates.DatesOverlapExclusive(sd, ed, GroupItem.start, GroupItem.end)) {
        //foundClashes = true
        //break
        clashCount += 1
      }
    }
  }
  //return foundClashes
  return (clashCount > 0)
};

ResourceSchedulerBase.prototype.hrOBContentContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrStudioContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrLeaveContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrSecondmentContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrAdHocTeamBuildingContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrPrepTimeContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.vehicleContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrPlaceholderContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.roomPlaceholderContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.vehiclePlaceholderContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrAdHocTrainingContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrAdHocMeetingContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrAdHocConferenceContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrEventFacilitatorShiftContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {

  }
};

ResourceSchedulerBase.prototype.hrICRShiftContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {
    //Permanently Delete Bookings
    if (!bookingItem.Booking.IsLocked) {
      optionsArray.push({
        text: 'Remove',
        href: "#", args: { eventProps: eventProperties },
        action: function (event) {
          console.log(event)
          me.removeICRShifts(bookingItem, groupItem, event.data)
        }
      })
    }
  }
};

ResourceSchedulerBase.prototype.playoutShiftOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly && !bookingItem.Booking.IsLocked) {
    optionsArray.push({
      text: 'Remove',
      href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
      action: function (event) {
        me.removePlayoutOpsShifts(event.data.bookingItem, event.data.groupItem)
      }
    })
    var statusOptions = {
      text: "Status",
      subMenu: []
    }

    var selections = me.visTimeline.itemSet.getAllSelections();
    if (selections.items.indexOf(bookingItem.id) < 0) {
      selections.items.push(bookingItem.id)
      //TODO: trigger select
    }

    var item1 = me.getResourceBookingItem(selections.items[0])
    var item2 = me.getResourceBookingItem(selections.items[1])

    //Merge Shifts
    //&& (item1.Booking.ResourceBookingTypeID == 21 && item2.Booking.ResourceBookingTypeID == 21)
    if (!bookingItem.Booking.IsCancelled && !bookingItem.Booking.IsLocked && selections.items.length == 2) {
      optionsArray.push({
        text: 'Merge Shifts',
        href: "#", args: { eventProps: eventProperties, selections: selections, shift1: item1.Booking, shift2: item2.Booking },
        action: function (event) {
          ViewModel.CallServerMethod("MergeShifts", {
            HumanResourceShiftID1: event.data.shift1.HumanResourceShiftID,
            HumanResourceShiftID2: event.data.shift2.HumanResourceShiftID
          }, function (response) {
            if (response.Success) {
              OBMisc.Notifications.GritterSuccess("Merge Succeeded", "", 250)
            }
            else {
              OBMisc.Notifications.GritterError("Merge Failed", ex.message, 1000)
            }
          })
        }
      })
    }

    //&& bookingItem.Booking.ResourceBookingTypeID == 21
    if (!bookingItem.Booking.IsCancelled && !bookingItem.Booking.IsLocked && selections.items.length == 1) {
      optionsArray.push({
        text: 'Sync Shift',
        href: "#", args: { eventProps: eventProperties, selections: selections, shift: bookingItem.Booking },
        action: function (event) {
          ViewModel.CallServerMethod("SyncShift", {
            HumanResourceShiftID: event.data.shift.HumanResourceShiftID
          }, function (response) {
            if (response.Success) {
              OBMisc.Notifications.GritterSuccess("Sync Performed", "", 250)
            }
            else {
              OBMisc.Notifications.GritterError("Sync Failed", ex.message, 1000)
            }
          })
        }
      })
    }

    //Status Options
    var systemFilteredStatuses = ClientData.ROSystemProductionAreaStatusSelectList.Filter("SystemID", bookingItem.Booking.SystemID)
    var areaFilteredStatuses = systemFilteredStatuses.Filter("ProductionAreaID", bookingItem.Booking.ProductionAreaID)
    areaFilteredStatuses.Iterate(function (stat, statIndex) {
      statusOptions.subMenu.push({
        text: stat.ProductionAreaStatus,
        href: "#",
        args: {
          ROProductionAreaAllowedStatus: stat,
          eventProps: eventProperties,
          group: groupItem,
          bookingItem: bookingItem
        },
        action: function (event) {
          me.changeBookingStatusesOld.call(me, event)
        }
      })
    })
    optionsArray.push(statusOptions)
  }
};

ResourceSchedulerBase.prototype.programmingShiftOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly && !bookingItem.Booking.IsLocked) {
    optionsArray.push({
      text: 'Remove',
      href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
      action: function (event) {
        me.removeProgrammingShifts(event.data.bookingItem, event.data.groupItem)
      }
    })
  }
};

ResourceSchedulerBase.prototype.stageHandShiftOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly && !bookingItem.Booking.IsLocked) {
    optionsArray.push({
      text: 'Remove',
      href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
      action: function (event) {
        me.removeStageHandShifts(event.data.bookingItem, event.data.groupItem)
      }
    })
  }
};

//#endregion

//#endregion

//#region Resources and Bookings

//Methods

ResourceSchedulerBase.prototype.getSelectedResourceXmlIDs = function () {
  var me = this;
  var selectedItems = me.resources.get();
  var xmlResourceIDs = "";
  selectedItems.Iterate(function (itm, indx) {
    xmlResourceIDs += "<Table ID='" + itm.Resource.ResourceID.toString() + "' />";
  });
  xmlResourceIDs = '<DataSet>' + xmlResourceIDs + '</DataSet>';
  return xmlResourceIDs;
};

ResourceSchedulerBase.prototype.getGroupByResourceID = function (ResourceID) {
  var me = this;
  var allResources = me.resources.get();
  var GR = null;
  allResources.Iterate(function (Group, GroupIndex) {
    if (!GR) {
      if (Group.Resource.ResourceID == ResourceID) {
        GR = Group;
      };
    }
  });
  return GR;
};

ResourceSchedulerBase.prototype.getGroupsByGuids = function (guids) {
  var me = this;
  var allResources = me.resources.get();
  var Groups = [];
  allResources.Iterate(function (Group, GroupIndex) {
    if (guids.indexOf(Group.id) >= 0) {
      Groups.push(Group);
    };
  });
  return Groups;
};

ResourceSchedulerBase.prototype.getGroupsByResourceIDs = function (ResourceIDs) {
  var me = this;
  var allResources = me.resources.get();
  var Groups = [];
  allResources.Iterate(function (Group, GroupIndex) {
    if (ResourceIDs.indexOf(Group.Resource.ResourceID) >= 0) {
      Groups.push(Group);
    };
  });
  return Groups;
};

ResourceSchedulerBase.prototype.addInitialGroups = function () {
  var me = this;
  var grps = [];
  this.schedulerData.ResourceSchedulerGroupList.Iterate(function (rsGroup, rsrtgIndex) {
    rsGroup.ResourceSchedulerSubGroupList.Iterate(function (rsSubGroup, rsrtsgIndex) {
      if (rsSubGroup.SelectedByDefault) {
        me.addResourcesToTimeline(rsSubGroup.ResourceSchedulerSubGroupResourceList)
      }
    })
  })
  me.resources.add(grps);
};

ResourceSchedulerBase.prototype.addGroups = function (groupsData) {
  var me = this;
  var grps = [];
  groupsData.Iterate(function (grpData, grpInd) {
    grps.push(me.createGroupItem(grpData));
  });
  me.resources.add(grps);
};

ResourceSchedulerBase.prototype.removeGroups = function (resourceIDs) {
  var me = this;
  var grpsToRemove = me.getGroupsByResourceIDs(resourceIDs);
  me.resources.remove(grpsToRemove);
};

ResourceSchedulerBase.prototype.addBookings = function (resourceBookings) {
  var me = this
  var templist = []
  resourceBookings.Iterate(function (itm, indx) {
    templist.push(me.createBookingItem(itm))
  })
  me.resourceBookings.add(templist)
  templist = null
  me.visTimeline.setItems(me.resourceBookings)
};

ResourceSchedulerBase.prototype.getGroupByGUID = function (guid) {
  return this.resources.get(guid);
};

ResourceSchedulerBase.prototype.getInternalGroupByGUID = function (guid) {
  var me = this
  return me.visTimeline.itemSet.groups[guid]
}

ResourceSchedulerBase.prototype.createGroupItem = function (resourceData) {
  var me = this;

  //clear the variables
  me.tempGroupItem = null;

  //create the item
  switch (resourceData.ResourceTypeID) {
    case 1:
      me.tempGroupItem = me.createHRGroup(resourceData);
      break;
    case 2:
      me.tempGroupItem = me.createRoomGroup(resourceData);
      break;
    case 3:
      me.tempGroupItem = me.createVehicleGroup(resourceData);
      break;
    case 4:
      me.tempGroupItem = me.createChannelGroup(resourceData);
      break;
    case 5:
      me.tempGroupItem = me.createEquipmentGroup(resourceData);
      break;
    case 6:
      me.tempGroupItem = me.createCustomGroup(resourceData);
      break;
  };
  return me.tempGroupItem;

};

ResourceSchedulerBase.prototype.createBookingItem = function (resourceBookingData) {
  var me = this;

  //clear the variables
  me.tempItem = null;
  var templist = [];

  //get the group to which the item belongs
  var resourceIDToUse = null
  if (resourceBookingData.ResourceIDOverride) { resourceIDToUse = resourceBookingData.ResourceIDOverride } else { resourceIDToUse = resourceBookingData.ResourceID }
  me.tempGroupToAddTo = me.getGroupByResourceID(resourceIDToUse);

  if (me.tempGroupToAddTo) {
    switch (resourceBookingData.ResourceBookingTypeID) {
      //1	HR - OB City
      case 1:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //2	HR - OB (Content)
      case 2:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //3	HR - Studio
      case 3:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //4	HR - Leave
      case 4:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //5	HR - Secondment
      case 5:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //6	HR - Ad Hoc
      case 6:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //7	HR - Prep Time
      case 7:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //8	Room - Production
      case 8:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //9	Room - AdHoc
      case 9:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //10 Equipment - Feed
      case 10:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //11 Vehicle
      case 11:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //12 HR - Not Available
      case 12:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //13 Room - Placeholder
      case 13:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //14 Equipment - Service
      case 14:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
        //15 Vehicle - Placeholder
      case 15:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 16:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 17:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 18:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 19:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 20:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 21:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 22:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
      case 23:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, me.tempGroupToAddTo);
        break;
    };
  };

  return me.tempItem;

};

//Resources (Groups)
ResourceSchedulerBase.prototype.createHRGroup = function (resourceData) {
  var me = this;
  return {
    content: me.getHRContent(resourceData),
    Resource: resourceData
  };
};

ResourceSchedulerBase.prototype.getHRContent = function (resourceData) {
  var me = this,
			progressClass = 'progress-bar-info',
			iconClass = '';

  //if (resourceData.PercUsed >= 80) {
  //  progressClass = "progress-bar-success"
  //  iconClass = "fa-success"
  //}
  //else if (resourceData.PercUsed >= 90) {
  //  progressClass = "progress-bar-warning"
  //  iconClass = "fa-warning"
  //}
  //else if (resourceData.PercUsed >= 100) {
  //  progressClass = "progress-bar-danger"
  //  iconClass = "fa-danger"
  //}
  // + resourceData.HumanResourceID.toString() 
  //var basic = "<div style='float:left; width:100%'>" +
  //               //"<img class='resource-image' src='../Images/Profile/152" + "_5.jpg' alt='Avatar'> " + resourceData.ResourceName +
  //               "<span class='resource-icon' style='padding-right: 10px'><i class='fa fa-user " + iconClass + "'></i></span>" +
  //               "<span class='resource-hr' style='padding-right: 10px'>" + resourceData.ResourceName + "</span>" +
  //            "</div>"

  var stats = "<div style='float:left; width:100%'>" +
								 "<span class='resource-hr-est-hrs' style='padding-right: 10px'> CO: " + resourceData.StartingBalanceHours + "</span>" +
								 "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Tgt: " + resourceData.TargetHours + "</span>" +
								 "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Curr: " + resourceData.TotalHours + "</span>" +
								 "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Req: " + resourceData.ReqHours + "</span>" +
							"</div>"

  var basic = '<div class="resource-group">' +
									'<div class="human-resource-name">' + resourceData.ResourceName + '</div>' +
									'<div class="progress medium">' +
											'<div class="progress-bar progress-bar-success" style="width: ' + (resourceData.UtilPerc ? resourceData.UtilPerc.toString() : '0') + '%"></div>' +
									'</div>' +
							'</div>'

  //if (me.schedulerData.ShowHRStats) {
  //  return basic //+ stats
  //}
  //else {
  return basic
  //}

};

ResourceSchedulerBase.prototype.createRoomGroup = function (resourceData) {
  var me = this;
  return {
    content: me.getRoomContent(resourceData),
    Resource: resourceData
  };
};

ResourceSchedulerBase.prototype.getRoomContent = function (resourceData) {
  return "<i class='fa fa-video-camera'></i> <span class='resource-room'>" + resourceData.ResourceName + "</span>";
};

ResourceSchedulerBase.prototype.createVehicleGroup = function (resourceData) {
  var me = this;
  return {
    content: me.getVehicleContent(resourceData),
    Resource: resourceData
  };
};

ResourceSchedulerBase.prototype.getVehicleContent = function (resourceData) {
  return "<i class='fa fa-truck'></i> <span class='resource-vehicle'>" + resourceData.ResourceName + "</span>";
};

ResourceSchedulerBase.prototype.createChannelGroup = function (resourceData) {
  var me = this;
  return {
    content: me.getChannelContent(resourceData),
    Resource: resourceData
  };
};

ResourceSchedulerBase.prototype.getChannelContent = function (resourceData) {
  return "<i class='fa fa-tv'></i> <span class='resource-vehicle'>" + resourceData.ResourceName + "</span>";
};

ResourceSchedulerBase.prototype.createEquipmentGroup = function (resourceData) {
  var me = this;
  return {
    content: me.getEquipmentContent(resourceData),
    Resource: resourceData
  };
};

ResourceSchedulerBase.prototype.getEquipmentContent = function (resourceData) {
  return "<i class='fa fa-wrench'></i> <span class='resource-equipment'>" + resourceData.ResourceName + "</span>";
};

ResourceSchedulerBase.prototype.createCustomGroup = function (resourceData) {
  var me = this;
  return {
    content: me.getCustomContent(resourceData),
    Resource: resourceData
  };
};

ResourceSchedulerBase.prototype.getCustomContent = function (resourceData) {
  return "<i class='fa fa-wrench'></i> <span class='resource-equipment'>" + resourceData.ResourceName + "</span>";
};

ResourceSchedulerBase.prototype.getGroupContent = function (resourceData) {
  var me = this;

  //clear the variables
  me.tempGroupContent = null;

  //create the item
  switch (resourceData.ResourceTypeID) {
    case 1:
      me.tempGroupContent = me.getHRContent(resourceData);
      break;
    case 2:
      me.tempGroupContent = me.getRoomContent(resourceData);
      break;
    case 3:
      me.tempGroupContent = me.getVehicleContent(resourceData);
      break;
    case 4:
      me.tempGroupContent = me.getChannelContent(resourceData);
      break;
    case 5:
      me.tempGroupContent = me.getEquipmentContent(resourceData);
      break;
    case 6:
      me.tempGroupContent = me.getCustomContent(resourceData);
      break;
  };
  return me.tempGroupContent;
};

//Bookings (Items)
ResourceSchedulerBase.prototype.getDefaultBookingItem = function (resourceBookingData, Group) {
  var me = this;
  return {
    content: me.getDefaultBookingContent(resourceBookingData), // '<div title="blah">' + ResourceBooking.ResourceBookingDescription + '</div>',
    start: new Date(resourceBookingData.StartDateTimeBuffer ? resourceBookingData.StartDateTimeBuffer : resourceBookingData.StartDateTime),
    end: new Date(resourceBookingData.EndDateTimeBuffer ? resourceBookingData.EndDateTimeBuffer : resourceBookingData.EndDateTime),
    group: Group.id,
    type: 'range',
    className: me.getDefaultBookingCssClass(resourceBookingData),
    Booking: resourceBookingData,
    editable: !resourceBookingData.IsLocked,
    resourceid: resourceBookingData.ResourceID,
    resourcebookingid: resourceBookingData.ResourceBookingID
  };
};

ResourceSchedulerBase.prototype.getDefaultBookingCssClass = function (resourceBookingData) {
  var me = this;
  var baseClass = resourceBookingData.StatusCssClass;

  //clash
  if (resourceBookingData.IsClashing) {
    baseClass += " item-clashing";
  }

  if ([8, 9].indexOf(resourceBookingData.ResourceBookingTypeID) >= 0) {
    if (!me.roomScheduleContainsMyArea(resourceBookingData)) {
      baseClass += " vis-item-transparent";
    }
  }

  return baseClass
};

ResourceSchedulerBase.prototype.getDefaultBookingContent = function (resourceBookingData) {
  var me = this;
  me.tempCssClass = "";
  me.tempIcon = "";
  me.tempDiv = null;
  me.hoverDiv = null;

  //locked
  if (resourceBookingData.IsLocked) {
    me.tempCssClass += " item-locked";
    me.tempIcon += " <i class='fa fa-lock'></i> "
  }

  if (me.hasMissingRequirements(resourceBookingData)) {
    //me.tempCssClass += ' animated infinite tada go '
    me.tempIcon += " <i class='fa fa-warning animated infinite tada go'></i> "
  }

  //me.hoverDiv = "<div class=booking-info>" + resourceBookingData.PopoverContent + "</div>"
  //+ " title='" + resourceBookingData.PopoverContent + "'" =
  // 
  me.tempDiv = "<div class='" + me.tempCssClass + "' title='" + resourceBookingData.PopoverContent + "'>" + me.tempIcon + resourceBookingData.ResourceBookingDescription + "</div>"


  //on hover content

  //edit mode
  var editDetailsDiv = "";
  if (resourceBookingData.IsBeingEditedBy != "") {
    editDetailsDiv = "<div class='item-in-edit-details'>" + "<i class='fa fa-edit'></i> " + "<span>" + resourceBookingData.IsBeingEditedBy + "</span>" + "</div>";
    me.tempDiv += editDetailsDiv
  }

  if (!resourceBookingData.HasAreaAccess && resourceBookingData.IsBeingEditedBy == "" && !resourceBookingData.IsProcessing) {
    me.tempDiv += "<div class='item-in-edit-details'>" + "<i class='fa fa-lock'></i> " + "<span>" + "Other Area" + "</span>" + "</div>";
  }

  //busy
  var isBusyDiv = "";
  if (resourceBookingData.IsProcessing) {
    isBusyDiv = "<div class='item-isProcessing'>" + "<i class='fa fa-refresh fa-spin'></i>" + "</div>";
    me.tempDiv += isBusyDiv
  };

  //me.tempCssClass = null;
  //me.tempIcon = null;
  return me.tempDiv;

};

ResourceSchedulerBase.prototype.updateHROffPeriod = function (item, group, options) {
  //the dev can override this method
  var me = this;

};

ResourceSchedulerBase.prototype.updateResourceBookingItem = function (resourceBookingData) {
  var me = this;
  var newItem = me.createBookingItem(resourceBookingData);
  var oldItem = me.getResourceBooking(resourceBookingData.ResourceBookingID);
  if (oldItem) {
    //already have this booking on screen, update it
    me.replaceOldWithNew(oldItem, newItem);
  } else {
    //don't have this booking on screen, add it
    if (newItem) {
      me.resourceBookings.add(newItem);
    }
  };
};

ResourceSchedulerBase.prototype.replaceOldWithNew = function (oldItem, newItem) {
  var me = this;

  oldItem.content = newItem.content;
  oldItem.start = newItem.start;
  oldItem.end = newItem.end;
  oldItem.group = newItem.group;
  oldItem.className = newItem.className;
  oldItem.Booking = newItem.Booking;
  oldItem.editable = !newItem.Booking.IsLocked;
  oldItem.resourceid = newItem.Booking.ResourceID;
  oldItem.resourcebookingid = newItem.Booking.ResourceBookingID;
  me.resourceBookings.update(oldItem);

};

ResourceSchedulerBase.prototype.getResourceBooking = function (ResourceBookingID) {
  var me = this;
  var AllItems = me.resourceBookings.get();
  var Item = null;
  AllItems.Iterate(function (GroupItem, GroupItemIndex) {
    if (!Item && GroupItem.type != 'background') {
      if (GroupItem.Booking.ResourceBookingID == ResourceBookingID) {
        Item = GroupItem;
      };
    }
  });
  return Item;
}

ResourceSchedulerBase.prototype.getResourceBookingItem = function (guid) {
  var me = this;
  return me.resourceBookings.get(guid);
  //var Item = null;
  //AllItems.Iterate(function (GroupItem, GroupItemIndex) {
  //  if (!Item) {
  //    if (GroupItem.Booking.ResourceBookingID == ResourceBookingID) {
  //      Item = GroupItem;
  //    };
  //  }
  //});
  //return Item;
}

ResourceSchedulerBase.prototype.getResourceBookingItems = function (guidArray) {
  var me = this;
  var AllItems = me.resourceBookings.get(guidArray);
  return AllItems;
};

ResourceSchedulerBase.prototype.removeBookingByResourceBookingID = function (resourceBookingID) {
  var me = this;
  var tempGroupItem = me.getResourceBooking(resourceBookingID);
  if (tempGroupItem) {
    me.removeBackgroundGroup(tempGroupItem.Booking);
    me.resourceBookings.remove(tempGroupItem);
  }
  tempGroupItem = null;
};

ResourceSchedulerBase.prototype.processAddedBooking = function (resourceBookingData) {
  var me = this;
  me.updateResourceBookingItem(resourceBookingData);
};

ResourceSchedulerBase.prototype.processUpdatedBooking = function (resourceBookingData) {
  var me = this;
  me.updateResourceBookingItem(resourceBookingData);
};

ResourceSchedulerBase.prototype.processBookingEditStatus = function (resourceBookingData) {
  var me = this;
  me.setBookingEditStatus(resourceBookingData.ResourceBookingID, resourceBookingData.IsBeingEditedBy, resourceBookingData.InEditDateTime);
};

ResourceSchedulerBase.prototype.setBookingEditStatus = function (resourceBookingID, isBeingEditedBy, inEditDateTime) {
  var me = this;
  var oldItem = me.getResourceBooking(resourceBookingID);
  if (oldItem) {
    oldItem.Booking.IsBeingEditedBy = isBeingEditedBy;
    oldItem.Booking.InEditDateTime = inEditDateTime;
    oldItem.content = me.getDefaultBookingContent(oldItem.Booking);
    me.resourceBookings.update(oldItem);
  }
};

ResourceSchedulerBase.prototype.getBackgroundItem = function (backgroundItemID) {
  var me = this;
  var AllItems = me.resourceBookings.get();
  var Item = null;
  AllItems.Iterate(function (itm, GroupItemIndex) {
    if (!Item) {
      if (itm.id == backgroundItemID) {
        Item = itm;
      };
    }
  });
  return Item;
};

ResourceSchedulerBase.prototype.getResourceBookingClashes = function (resourceBookingsArray, afterClashesFetched, afterFetchClashesFailed) { };

ResourceSchedulerBase.prototype.setItemBusy = function (item) {
  var me = this;
  item.Booking.IsProcessing = true;
  item.content = me.getDefaultBookingContent(item.Booking);
  me.resourceBookings.update(item);
};

ResourceSchedulerBase.prototype.setItemNotBusy = function (item) {
  var me = this;
  item.Booking.IsProcessing = false;
  item.content = me.getDefaultBookingContent(item.Booking);
  me.resourceBookings.update(item);
};

//Background
ResourceSchedulerBase.prototype.getBackgroundGroup = function (resourceBooking) {
  return this.resourceBookings.get('BG:' + resourceBooking.ResourceBookingID.toString());
};

ResourceSchedulerBase.prototype.addBackgroundGroup = function (resourceBooking) {
  var me = this;
  var idToUse = 'BG:' + resourceBooking.ResourceBookingID.toString();
  var existingItem = me.resourceBookings.get(idToUse);
  if (!existingItem) {
    var newItemtem = {
      id: 'BG:' + resourceBooking.ResourceBookingID.toString(),
      content: '',
      start: (resourceBooking.StartDateTimeBuffer ? resourceBooking.StartDateTimeBuffer : resourceBooking.StartDateTime),
      end: (resourceBooking.EndDateTimeBuffer ? resourceBooking.EndDateTimeBuffer : resourceBooking.EndDateTime),
      type: 'background',
      className: 'room-schedule-background'
    };
    me.resourceBookings.add(newItemtem);
    newItemtem = null;
  }
  existingItem = null;
  idToUse = null;
};

ResourceSchedulerBase.prototype.removeBackgroundGroup = function (resourceBooking) {
  var me = this;
  var idToFind = 'BG:' + resourceBooking.ResourceBookingID.toString();
  var item = me.getBackgroundItem(idToFind);
  if (item) {
    me.resourceBookings.remove(item);
  }
};

//#endregion

//#region SignalR

ResourceSchedulerBase.prototype.processSignalRChanges = function (stringifiedChanges) {
  var me = this;
  //this will parse the data into a json object and pass it on to be processed
  var parsedChanges = JSON.parse(stringifiedChanges);
  var updatedBookings = parsedChanges.Data.UpdatedBookings;
  var addedBookings = parsedChanges.Data.AddedBookings;
  var removedBookingIDs = parsedChanges.Data.RemovedBookingIDs;
  var editStatusBookings = parsedChanges.Data.EditStatusBookings;
  //Process the updates
  updatedBookings.Iterate(function (updBooking, indx) {
    window.Scheduler.processUpdatedBooking(updBooking);
  });
  //Process the additions
  addedBookings.Iterate(function (addedBooking, indx) {
    window.Scheduler.processAddedBooking(addedBooking);
  });
  //Process the deletes
  removedBookingIDs.Iterate(function (id, indx) {
    window.Scheduler.removeBookingByResourceBookingID(id);
  });
};

ResourceSchedulerBase.prototype.processUpdatedTimesheetStats = function (serialisedResult) {
  var me = this;
  //this will parse the data into a json object and pass it on to be processed
  var result = JSON.parse(serialisedResult);
  //Process the updates
  result.Data.Iterate(function (resourceTimesheetStat, indx) {
    var resourceGroup = me.getGroupByResourceID(resourceTimesheetStat.ResourceID)
    if (resourceGroup) {
      resourceGroup.Resource.StartingBalanceHours = resourceTimesheetStat.StartingBalanceHours
      resourceGroup.Resource.TargetHours = resourceTimesheetStat.TargetHours
      resourceGroup.Resource.TotalHours = resourceTimesheetStat.TotalHours
      resourceGroup.Resource.ReqHours = resourceTimesheetStat.RequiredHours
      resourceGroup.Resource.UtilPerc = (resourceTimesheetStat.UtilPerc * 100)
      resourceGroup.content = me.getHRContent(resourceGroup.Resource)
      me.resources.update(resourceGroup)
    }
  })
};

//#endregion

//#region Room Scheduling

ResourceSchedulerBase.prototype.roomProductionContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {
    me.defaultRoomOptions(optionsArray, groupItem, bookingItem, eventProperties)
  }
};

ResourceSchedulerBase.prototype.roomAdHocContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {
    me.defaultRoomOptions(optionsArray, groupItem, bookingItem, eventProperties)
  }
};

ResourceSchedulerBase.prototype.defaultRoomOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this
  if (!me.isReadOnly) {
    if (bookingItem.Booking.HasAreaAccess) {
      //#region Status Options
      var statusOptions = {
        text: "Status",
        subMenu: []
      }
      if (!bookingItem.Booking.IsLocked && !bookingItem.Booking.IsCancelled && bookingItem.Booking.HasAreaAccess) {

        //Status Options
        var systemFilteredStatuses = ClientData.ROSystemProductionAreaStatusSelectList.Filter("SystemID", bookingItem.Booking.SystemID)
        var areaFilteredStatuses = systemFilteredStatuses.Filter("ProductionAreaID", bookingItem.Booking.ProductionAreaID)
        areaFilteredStatuses.Iterate(function (stat, statIndex) {
          statusOptions.subMenu.push({
            text: stat.ProductionAreaStatus,
            href: "#",
            args: {
              ROProductionAreaAllowedStatus: stat,
              eventProps: eventProperties,
              group: groupItem,
              bookingItem: bookingItem
            },
            action: function (event) {
              me.changeBookingStatusesOld.call(me, event)
            }
          })
        })

        if (statusOptions.subMenu.length > 0) {
          optionsArray.push(statusOptions);
        }

        //Time Options
        if (me.haveBookingTimesChanged(bookingItem)) {
          if (!bookingItem.Booking.IsClashing) {
            optionsArray.push({
              text: 'Apply Changes',
              href: "#",
              args: {
                eventProps: eventProperties,
                groupItem: groupItem,
                bookingItem: bookingItem
              },
              action: function (event) {
                me.applyRoomScheduleTimeChanges(event)
              }
            })
          }
          optionsArray.push({
            text: 'Discard Changes',
            href: "#",
            args: {
              eventProps: eventProperties,
              Group: groupItem,
              GroupItem: bookingItem
            },
            action: function (event) {
              me.discardTimeChanges(event)
            }
          })
        }

      }
      else {
        //if Cancelled
        if (bookingItem.Booking.IsCancelled) {
          optionsArray.push({
            text: 'Reinstate',
            href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
            action: function (event) {
              me.reinstateRoomSchedule(event)
            }
          })
        }
        //if Locked
        if (bookingItem.Booking.IsLocked) {
          optionsArray.push({
            text: 'UnReconcile',
            href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
            action: function (event) {
              me.unreconcileRoomSchedule(event)
            }
          })
        }
      }
      //#endregion
      //#region Other Options

      var selections = me.visTimeline.itemSet.getAllSelections();
      if (selections.items.indexOf(bookingItem.id) < 0) {
        selections.items.push(bookingItem.id)
        //TODO: trigger select
      }

      //Copy Crew
      if (!bookingItem.Booking.IsCancelled && !bookingItem.Booking.IsLocked && selections.items.length == 2) {
        optionsArray.push({
          text: 'Copy Crew',
          href: "#", args: { eventProps: eventProperties, selections: selections },
          action: function (event) {
            me.copyCrew(event)
          }
        })
      }

      //Permanently Delete Bookings
      if (!bookingItem.Booking.IsLocked) {
        optionsArray.push({
          text: 'Delete',
          href: "#", args: { eventProps: eventProperties, selections: selections },
          action: function (event) {
            me.deleteRoomScheduleArea(event, true)
          }
        })
      }

      //#endregion
    }
    else {
      //#region Not My Booking Options
      optionsArray.push({
        text: 'Add My Area',
        href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
        action: function (event) {
          var customArgs = event.data;
          var selections = me.visTimeline.itemSet.getAllSelections();
          if (selections.items.indexOf(customArgs.bookingItem.id) < 0) {
            selections.items.push(customArgs.bookingItem.id)
            //TODO: trigger select
          }
          selections = me.visTimeline.itemSet.getAllSelections();
          var resources = me.getGroupsByGuids(selections.groups);
          var resourceBookings = me.getResourceBookingItems(selections.items);
          me.addAreaToRoomSchedules(resourceBookings);
          me.clearSelections();
        }
      })
      //#endregion
    }
  }
};

ResourceSchedulerBase.prototype.roomScheduleContainsMyArea = function (roomSchedule) {
  //the dev can override this method
  //var me = this,
  //    systemID = ViewModel.SystemID(),
  //    productionAreaID = ViewModel.DefaultProductionAreaID(),
  //    hasArea = false;
  ////if (roomSchedule.RSResourceBookingAreaList.length > 0) {
  ////  var filteredBySystem = roomSchedule.RSResourceBookingAreaList.Filter('SystemID', systemID);
  ////  var filteredBySystemAndArea = filteredBySystem.Filter('ProductionAreaID', productionAreaID);
  ////  hasArea = (filteredBySystemAndArea.length > 0);
  ////}
  //hasArea = (roomSchedule.RSResourceBookingAreaList.length > 0)
  return true;
};

ResourceSchedulerBase.prototype.hasMissingRequirements = function (resourceBookingData) {
  //the dev can override this method
  //var me = this,
  //    systemID = ViewModel.SystemID(),
  //    productionAreaID = ViewModel.DefaultProductionAreaID(),
  //    hasArea = false;
  //if (resourceBookingData.RSResourceBookingAreaList.length > 0) {
  //  //var filteredBySystem = resourceBookingData.RSResourceBookingAreaList.Filter('SystemID', systemID)
  //  //var filteredBySystemAndArea = filteredBySystem.Filter('ProductionAreaID', productionAreaID)
  //  //if (filteredBySystemAndArea.length == 1) {
  //  return (resourceBookingData.RSResourceBookingAreaList[0].HasMissingRequirements)
  //  //}
  //}
  return false
};

ResourceSchedulerBase.prototype.beforeRoomScheduleFetch = function (item, group) {
  //the dev can override this method
  var me = this //,
  //systemID = ViewModel.SystemID(),
  //productionAreaID = ViewModel.DefaultProductionAreaID();
  if (!me.isReadOnly) {
    me.fetchRoomSchedule(item, group, null)
    //var indx = 0
    //if (item.Booking.RSResourceBookingAreaList.length > 0) {

    //} else {
    //  //if no areas, add the current SystemID and ProductionAreaID
    //  ViewModel.ResourceBookingClean.Set(item.Booking);
    //  me.showAreaNotAddedModal(item, group);
    //}
  }
  else {
    me.fetchRORoomSchedule(item, group)
  }
};

ResourceSchedulerBase.prototype.changeBookingStatusesOld = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    //1. make sure everything is actually selected
    var selections = me.visTimeline.itemSet.getAllSelections()
    if (selections.items.indexOf(event.data.bookingItem.id) < 0) { selections.items.push(event.data.bookingItem.id)  /*TODO: trigger select*/ }
    //2. set the selected items as busy
    var resourceBookingItems = me.getResourceBookingItems(selections.items)
    resourceBookingItems.Iterate(function (itm, indx) {
      me.setItemBusy(itm)
    })
    //3. send the requests
    window.siteHubManager.changeBookingStatusesOld(resourceBookingItems, event.data.ROProductionAreaAllowedStatus.ProductionAreaStatusID)
  }
};

ResourceSchedulerBase.prototype.copyProductionSystemAreaCrew = function (item, group, userOptions, customArgs) {
  var me = this;
  if (!me.isReadOnly) {
    var defaultOptions = {
      onSuccess: function (scheduler, item, group, response) {
        me.setItemNotBusy(item);
      },
      onFail: function (scheduler, item, group, response) {
        me.setItemNotBusy(item);
      }
    };
    var options = $.extend({}, me.defaultOptions, userOptions);
    if ([8, 9].indexOf(item.Booking.ResourceBookingTypeID) >= 0) { //only RoomSchedules
      if (!item.Booking.IsLocked && !item.Booking.IsCancelled) { //ignore the special cases
        //var myAreaIsAdded = me.roomScheduleContainsMyArea(item.Booking);
        if (item.Booking.HasAreaAccess) {
          window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
          me.setItemBusy(item);
          ViewModel.CallServerMethod("ChangeRoomScheduleStatusID",
						{
						  ResourceBookingID: item.Booking.ResourceBookingID,
						  NewStatusID: customArgs.ROProductionAreaAllowedStatus.ProductionAreaStatusID
						},
						function (response) {
						  window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
						  if (response.Success) {
						    options.onSuccess(me, item, group, response);
						  } else {
						    options.onFail(me, item, group, response);
						  }
						})
        }
      }
    };
  }
};

ResourceSchedulerBase.prototype.fetchRoomSchedule = function (item, group, newResourceBookingID) {
  var me = this;
  if (!me.isReadOnly) {
    me.currentItem = item;
    me.currentGroup = group;
    me.setItemBusy(item)
    RoomScheduleAreaBO.getRoomScheduleArea({
      criteria: {
        roomScheduleID: item.Booking.RoomScheduleID,
        productionSystemAreaID: item.Booking.ProductionSystemAreaID,
        resourceBookingID: item.Booking.ResourceBookingID
      },
      onSuccess: (data) => {
        ViewModel.CurrentProduction(null);
        ViewModel.CurrentAdHocBooking(null);
        ViewModel.CurrentRoomScheduleArea.Set(data[0]);
        //ViewModel.CurrentRoomScheduleArea().Room(response.Data.Room)
        //ViewModel.CurrentRoomScheduleArea().RoomID(response.Data.RoomID)
        RoomScheduleAreaBO.showModal()
        me.setItemNotBusy(item)
      },
      onFail: (response) => {
        me.setItemNotBusy(item)
      }
    })
  }
  else {
    alert("open read only screen")
  }
};

//ResourceSchedulerBase.prototype.afterRoomScheduleFetch = function (item, group, response) {
//  var me = this;
//  if (me.isReadOnly) { } else { }
//};

ResourceSchedulerBase.prototype.fetchRORoomSchedule = function (item, group) {
  var me = this;
  me.roRoomScheduleControl().getRORoomSchedule(item.Booking.RoomScheduleID, item.Booking.ProductionSystemAreaID)
};

ResourceSchedulerBase.prototype.roomProductionMoved = function (item, callback, oldGroup, newGroup) {
  var me = this;
  if (!me.isReadOnly) {

  }
  else {
    callback(null)
  }
  //var allowedToDragToResourceTypeIDs = [2];
  //var hasDBClash = false;
  ////check if can move
  //if (!item.Booking.IsLocked
  //    && !item.Booking.IsCancelled
  //    && allowedToDragToResourceTypeIDs.indexOf(newGroup.Resource.ResourceTypeID) >= 0) {
  //  me.getResourceBookingClashes([item.Booking],
  //                              //Success
  //                              function (response) {
  //                              },
  //                              //Failure
  //                              function (response) {
  //                              });
  //} else {
  //}
};

ResourceSchedulerBase.prototype.roomAdHocMoved = function (item, callback, oldGroup, newGroup) {
  var me = this;
  if (!me.isReadOnly) {

  }
  else {
    callback(null)
  }
};

ResourceSchedulerBase.prototype.removeAreaFromRoomSchedule = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    //call the method
    me.setItemBusy(item);
    window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
    ViewModel.CallServerMethod("DeleteAreaFromRoomSchedule", {
      ResourceBookingID: item.Booking.ResourceBookingID
    }, function (response) {
      window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
      if (response.Success) {
        window.SiteHub.server.sendNotifications()
      }
      else {
        OBMisc.Notifications.GritterError('Error Processing', response.ErrorText, 3000)
      }
    });
    //window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
    //CurrentRoomScheduleControl.beforeRoomScheduleDelete(item, group);
  }
  else {

  }
};

ResourceSchedulerBase.prototype.addAreaToRoomSchedules = function (items) {
  var me = this;
  items.Iterate(function (item, itemIndx) {
    if ([8, 9].indexOf(item.Booking.ResourceBookingTypeID) >= 0) {
      me.addAreaToRoomScheduleSilent(item, null, {
        onSuccess: function (scheduler, item, group, response) {
          me.setItemNotBusy(item)
          scheduler.removeBookingByResourceBookingID(item.Booking.ResourceBookingID)
          me.currentItem = null
          me.currentGroup = null
        },
        onFail: function (scheduler, item, group, response) {
          me.setItemNotBusy(item)
        }
      });
    }
    else {
      //remove item from selection
    }
  });
};

ResourceSchedulerBase.prototype.afterRoomScheduleSetup = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    CurrentRoomScheduleControl.showRoomSchedule(item, group, null)
  }
};

ResourceSchedulerBase.prototype.applyRoomScheduleTimeChanges = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    if ([8, 9].indexOf(item.Booking.ResourceBookingTypeID) >= 0) { //only RoomSchedules
      if (!item.Booking.IsLocked && !item.Booking.IsCancelled && !item.Booking.IsClashing) { //ignore the special cases
        window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
        me.setItemBusy(item)
        ViewModel.CallServerMethod("ApplyRoomScheduleTimeChanges",
					{
					  ResourceBooking: item.Booking
					},
					function (response) {
					  window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
					  if (response.Success) {
					    me.setItemNotBusy(item);
					    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
					  }
					  else {
					    OBMisc.Notifications.GritterError('Error Saving Booking', response.ErrorText, 3000)
					  }
					})
      }
    }
  }
  else {

  }
};

ResourceSchedulerBase.prototype.reinstateRoomSchedule = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    me.setItemBusy(item)
    var customArgs = event.data
    window.siteHubManager.bookingIntoEdit(event.data.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    ViewModel.CallServerMethod("ReInstateRoomSchedule",
		{
		  ResourceBookingID: event.data.bookingItem.Booking.ResourceBookingID
		},
		function (response) {
		  window.SiteHub.server.sendNotifications()
		  me.setItemNotBusy(item)
		  window.siteHubManager.bookingOutOfEdit(customArgs.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  if (response.Success) {
		    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  }
		  else {
		    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Room Booking', response.ErrorText, null)
		  }
		})
  }
  else {

  }
};

ResourceSchedulerBase.prototype.unreconcileRoomSchedule = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    me.setItemBusy(item)
    var customArgs = event.data
    window.siteHubManager.bookingIntoEdit(event.data.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    ViewModel.CallServerMethod("UnReconcileRoomSchedule",
		{
		  ResourceBookingID: event.data.bookingItem.Booking.ResourceBookingID
		},
		function (response) {
		  window.SiteHub.server.sendNotifications()
		  me.setItemNotBusy(item)
		  window.siteHubManager.bookingOutOfEdit(customArgs.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  if (response.Success) {
		    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  }
		  else {
		    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Room Booking', response.ErrorText, null)
		  }
		})
  }
  else {

  }
};

ResourceSchedulerBase.prototype.deleteRoomScheduleArea = function (args, permanentDelete) {
  var me = this;
  if (!me.isReadOnly) {
    var itemsToDelete = []
    try {
      var productionSystemAreaIDs = []
      var rb = null,
					item = null;
      args.data.selections.items.Iterate(function (rsa, rsaIndx) {
        item = me.getResourceBookingItem(rsa)
        me.setItemBusy(item)
        productionSystemAreaIDs.push(item.Booking.ProductionSystemAreaID)
        itemsToDelete.push(item)
      })
      ViewModel.CallServerMethod("DeleteRoomScheduleAreas",
																 {
																   ProductionSystemAreaIDs: productionSystemAreaIDs,
																   PermanentDelete: permanentDelete
																 },
																function (response) {
																  if (response.Success) {
																    OBMisc.Notifications.GritterSuccess("Delete Succeeded", "", 500)
																  }
																  else {
																    OBMisc.Notifications.GritterError("Delete Failed", response.ErrorText, 1000)
																  }
																})
    } catch (ex) {
      itemsToDelete.Iterate(function (itm, indx) {
        me.setItemNotBusy(itm)
      })
      OBMisc.Notifications.GritterError("An Error Occured :(", ex.message, 1500)
    }
  }
}

//#endregion

//#region Human Resources

ResourceSchedulerBase.prototype.removeProductionHRBooking = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    //call the method
    me.setItemBusy(item);
    window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
    ViewModel.CallServerMethod("ProductionHRRemoveResourceBooking", {
      ProductionHRResourceBookingID: item.Booking.ResourceBookingID
    }, function (response) {
      if (response.Success) {
        me.resourceBookings.remove(item.id);
        //window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
      } else {
        OBMisc.Modals.Error('Error Processing', 'An error occured during the removal of the crew member booking', response.ErrorText, null)
        window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
      }
    })
  }
  else {

  }
};

//#endregion

//#region Off Period

//#endregion

//#region Equipment Scheduling 

ResourceSchedulerBase.prototype.equipmentFeedContextMenuOptions = function (optionsArray, groupItem, bookingItem, eventProperties) {
  var me = this;
  if (!me.isReadOnly) {
    //****************************add the allowed statuses******************************
    var statusOptions = {
      text: "Status",
      subMenu: []
    }

    if (!bookingItem.Booking.IsLocked && !bookingItem.Booking.IsCancelled) {

      //Status Options
      var systemFilteredStatuses = ClientData.ROSystemProductionAreaStatusSelectList.Filter("SystemID", 2) //Content
      var areaFilteredStatuses = systemFilteredStatuses.Filter("ProductionAreaID", 8) //SatOps
      areaFilteredStatuses.Iterate(function (stat, statIndex) {
        statusOptions.subMenu.push({
          text: stat.ProductionAreaStatus,
          href: "#",
          args: {
            ROProductionAreaAllowedStatus: stat,
            eventProps: eventProperties,
            group: groupItem,
            bookingItem: bookingItem
          },
          action: function (event) {
            var customArgs = event.data
            var selections = me.visTimeline.itemSet.getAllSelections()
            if (selections.items.indexOf(customArgs.bookingItem.id) < 0) {
              selections.items.push(customArgs.bookingItem.id)
              //TODO: trigger select
            }
            var resourceBookings = me.getResourceBookingItems(selections.items)
            me.changeEquipmentScheduleStatuses(resourceBookings, customArgs)
            me.clearSelections()
          }
        })
      })

      //Time Options
      if (me.haveBookingTimesChanged(bookingItem)) {
        optionsArray.push({
          text: 'Apply Time Changes',
          href: "#",
          args: {
            eventProps: eventProperties,
            groupItem: groupItem,
            bookingItem: bookingItem
          },
          action: function (event) {
            me.applyEquipmentScheduleTimeChanges(event)
          }
        })
        optionsArray.push({
          text: 'Discard Time Changes',
          href: "#",
          args: {
            eventProps: eventProperties,
            Group: groupItem,
            GroupItem: bookingItem
          },
          action: function (event) {
            me.discardTimeChanges(event)
          }
        })
      }

      //Copy Options
      optionsArray.push({
        text: 'Copy / Repeat',
        href: "#",
        args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
        action: function (event) { me.showCopyEquipmentSchedule(event) }
      })

    }
    else {
      //If Cancelled
      if (bookingItem.Booking.IsCancelled) {
        optionsArray.push({
          text: 'Reinstate',
          href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
          action: function (event) {
            me.reinstateEquipmentSchedule(event)
          }
        })
      }
      //If Locked
      if (bookingItem.Booking.IsLocked) {
        optionsArray.push({
          text: 'UnReconcile',
          href: "#", args: { eventProps: eventProperties, groupItem: groupItem, bookingItem: bookingItem },
          action: function (event) {
            me.unreconcileEquipmentSchedule(event)
          }
        })
      }
    }

    if (statusOptions.subMenu.length > 0) {
      optionsArray.push(statusOptions);
    }
  }
  else {

  }
};

ResourceSchedulerBase.prototype.showCopyEquipmentSchedule = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    var customArgs = event.data
    me.copyBookingControl.showModal(event.data.bookingItem.Booking)
    //me.setItemBusy(item)
    //window.siteHubManager.bookingIntoEdit(event.data.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    //ViewModel.CallServerMethod("ReInstateEquipmentSchedule",
    //{
    //  ResourceBookingID: event.data.bookingItem.Booking.ResourceBookingID
    //},
    //function (response) {
    //  window.SiteHub.server.sendNotifications()
    //  me.setItemNotBusy(item)
    //  window.siteHubManager.bookingOutOfEdit(customArgs.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    //  if (response.Success) {
    //    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    //  }
    //  else {
    //    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Feed', response.ErrorText, null)
    //  }
    //})
  }
};

ResourceSchedulerBase.prototype.updateEquipmentFeed = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    me.beforeEquipmentScheduleFetch(item, group)
    Singular.SendCommand("EditEquipmentSchedule",
		{
		  EquipmentScheduleID: me.currentItem.Booking.EquipmentScheduleID
		},
		function (response) {
		  if (response.Success) {
		    me.afterEquipmentScheduleFetch(me.currentItem, me.currentGroup, response);
		  } else {
		    OBMisc.Modals.Error('Error Fetching Feed', 'An error occured while fetching the Feed', response.ErrorText, null)
		  }
		})
  }
  else {

  }
};

ResourceSchedulerBase.prototype.beforeEquipmentScheduleFetch = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {

  }
  else {

  }
};

ResourceSchedulerBase.prototype.onEquipmentScheduleShown = function (modal, item, group) {
  var me = this;
  if (!me.isReadOnly) {

  }
  else {

  }
};

ResourceSchedulerBase.prototype.onEquipmentScheduleHidden = function (modal, item, group) {
  var me = this;
  if (!me.isReadOnly) {

  }
  else {

  }
};

ResourceSchedulerBase.prototype.afterEquipmentScheduleFetch = function (item, group, response) {
  var me = this;
  if (!me.isReadOnly) {
    me.equipmentScheduleModal().off('shown.bs.modal')
    me.equipmentScheduleModal().off('hidden.bs.modal')
    //once shown
    me.equipmentScheduleModal().on('shown.bs.modal', function () {
      me.equipmentScheduleModal().draggable({ handle: ".modal-header" });
      me.onEquipmentScheduleShown(me.equipmentScheduleModal(), item, group)
    })
    //once hidden
    me.equipmentScheduleModal().on('hidden.bs.modal', function (e) {
      me.onEquipmentScheduleHidden(me.equipmentScheduleModal(), item, group)
    })
    //show
    me.equipmentScheduleModal().modal()
  }
  else {

  }
};

ResourceSchedulerBase.prototype.saveEquipmentFeed = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    Singular.SendCommand("SaveEquipmentFeed",
													{},
													function (response) {
													  if (response.Success) {
													    me.afterEquipmentFeedSaved(me.currentItem, me.currentGroup, response);
													  } else {
													    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Feed', response.ErrorText, null)
													  }
													})
  }
  else {

  }
};

ResourceSchedulerBase.prototype.removeFeed = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    OBMisc.Modals.Warning("Delete Feed",
	"Are you sure you wish to delete this feed?",
	"",
	{
	  cancelButton: {
	    cssClass: 'btn-warning',
	    text: 'No',
	    callback: function (modal, event) {
	      modal.modal('hide')
	    }
	  },
	  confirmButton: {
	    cssClass: 'btn-success',
	    text: 'Yes',
	    callback: function (modal, event) {
	      //call the method
	      me.setItemBusy(item)
	      window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
	      ViewModel.CallServerMethod("DeleteFeed", {
	        ResourceBookingID: item.Booking.ResourceBookingID
	      }, function (response) {
	        window.SiteHub.server.sendNotifications()
	        if (response.Success) {
	          modal.modal('hide')
	        }
	        else {
	          OBMisc.Modals.Error('Error Processing', 'There was an error removing the feed', response.ErrorText, null)
	        }
	      })
	    }
	  }
	},
 "fa-trash")
  }
  else {

  }
};

ResourceSchedulerBase.prototype.feedBookingMoved = function (item, callback, oldGroup, newGroup) {
  var me = this;
  if (!me.isReadOnly) {

  }
  else {

  }
  //var allowedToDragToResourceTypeIDs = [5];
  //var hasDBClash = false;
  ////check if can move
  //if (!item.Booking.IsLocked
  //    && !item.Booking.IsCancelled
  //    && allowedToDragToResourceTypeIDs.indexOf(newGroup.Resource.ResourceTypeID) >= 0) {
  //  me.getResourceBookingClashes([item.Booking],
  //                              //Success
  //                              function (response) {
  //                              },
  //                              //Failure
  //                              function (response) {
  //                              });
  //}
  //else {
  //}
};

ResourceSchedulerBase.prototype.changeEquipmentScheduleStatuses = function (items, customArgs) {
  var me = this;
  if (!me.isReadOnly) {
    items.Iterate(function (item, itemIndx) {
      me.changeEquipmentScheduleStatus(item, null, {
        onSuccess: function (scheduler, item, group, response) {
          //if it succeeds
          scheduler.setItemNotBusy(item);
          window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
          scheduler.currentItem = null;
          scheduler.currentGroup = null;
        },
        onFail: function (scheduler, item, group, response) {
          //if it fails
          window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
          scheduler.setItemNotBusy(item);
        }
      },
			customArgs);
    })
  }
  else {

  }
};

ResourceSchedulerBase.prototype.changeEquipmentScheduleStatus = function (item, group, userOptions, customArgs) {
  var me = this;
  if (!me.isReadOnly) {
    var defaultOptions = {
      onSuccess: function (scheduler, item, group, response) {
        me.setItemNotBusy(item);
      },
      onFail: function (scheduler, item, group, response) {
        me.setItemNotBusy(item);
      }
    }
    var options = $.extend({}, me.defaultOptions, userOptions);

    if ([10].indexOf(item.Booking.ResourceBookingTypeID) >= 0) { //only RoomSchedules
      if (!item.Booking.IsLocked && !item.Booking.IsCancelled) { //ignore the special cases

        window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
        me.setItemBusy(item);
        ViewModel.CallServerMethod("ChangeEquipmentScheduleStatusID",
					{
					  ResourceBookingID: item.Booking.ResourceBookingID,
					  NewStatusID: customArgs.ROProductionAreaAllowedStatus.ProductionAreaStatusID
					},
					function (response) {
					  window.SiteHub.server.sendNotifications()
					  window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID());
					  if (response.Success) {
					    options.onSuccess(me, item, group, response);
					  } else {
					    options.onFail(me, item, group, response);
					  }
					})

      }
    }
  }
  else {

  }
};

ResourceSchedulerBase.prototype.applyEquipmentScheduleTimeChanges = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    if ([10].indexOf(item.Booking.ResourceBookingTypeID) >= 0) { //only RoomSchedules
      if (!item.Booking.IsLocked && !item.Booking.IsCancelled) { //ignore the special cases

        window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
        me.setItemBusy(item)
        //var rb = KOFormatterFull.Serialise(item.Booking)
        ViewModel.CallServerMethod("ApplyEquipmentScheduleTimeChanges",
					{
					  ResourceBooking: item.Booking,
					  NewResourceID: event.data.groupItem.Resource.ResourceID
					},
					function (response) {
					  window.SiteHub.server.sendNotifications()
					  window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
					  if (response.Success) {
					    me.setItemNotBusy(item)
					    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
					  }
					  else {
					    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Feed', response.ErrorText, null)
					  }
					})
      }
    }
  }
};

ResourceSchedulerBase.prototype.reinstateEquipmentSchedule = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    me.setItemBusy(item)
    var customArgs = event.data
    window.siteHubManager.bookingIntoEdit(event.data.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    ViewModel.CallServerMethod("ReInstateEquipmentSchedule",
		{
		  ResourceBookingID: event.data.bookingItem.Booking.ResourceBookingID
		},
		function (response) {
		  window.SiteHub.server.sendNotifications()
		  me.setItemNotBusy(item)
		  window.siteHubManager.bookingOutOfEdit(customArgs.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  if (response.Success) {
		    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  }
		  else {
		    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Feed', response.ErrorText, null)
		  }
		})
  }
  else {

  }
};

ResourceSchedulerBase.prototype.unreconcileEquipmentSchedule = function (event) {
  var me = this;
  if (!me.isReadOnly) {
    var item = event.data.bookingItem
    me.setItemBusy(item)
    var customArgs = event.data
    window.siteHubManager.bookingIntoEdit(event.data.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
    ViewModel.CallServerMethod("UnReconcileEquipmentSchedule",
		{
		  ResourceBookingID: event.data.bookingItem.Booking.ResourceBookingID
		},
		function (response) {
		  window.SiteHub.server.sendNotifications()
		  me.setItemNotBusy(item)
		  window.siteHubManager.bookingOutOfEdit(customArgs.bookingItem.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  if (response.Success) {
		    window.siteHubManager.bookingOutOfEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
		  }
		  else {
		    OBMisc.Modals.Error('Error Saving Feed', 'An error occured while saving the Feed', response.ErrorText, null)
		  }
		})
  }
  else {

  }
};

//#endregion

//#region Shifts 

//Playout
ResourceSchedulerBase.prototype.removePlayoutOpsShifts = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    var selections = me.visTimeline.itemSet.getAllSelections()
    if (selections.items.indexOf(item.id) < 0) { selections.items.push(item.id) }
    var resourceBookings = me.getResourceBookingItems(selections.items)

    var title = (resourceBookings.length > 1 ? 'Delete Shifts?' : 'Delete Shift')
    var message = (resourceBookings.length > 1 ? 'Are you sure you wish to delete the selected Shifts?' : 'Are you sure you wish to delete the selected Shift')

    OBMisc.Modals.Warning(title,
													message,
													"",
													{
													  cancelButton: {
													    cssClass: 'btn-warning',
													    text: 'No',
													    callback: function (modal, event) {
													      modal.modal('hide')
													    }
													  },
													  confirmButton: {
													    cssClass: 'btn-success',
													    text: 'Yes',
													    callback: function (modal, event) {
													      var bookings = []
													      resourceBookings.Iterate(function (rb, rbI) {
													        bookings.push(rb.Booking)
													        me.setItemBusy(rb)
													      })
													      //call the method
													      //me.setItemBusy(item)
													      //window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
													      ViewModel.CallServerMethod("DeletePlayoutOpsShifts", {
													        shifts: bookings
													      }, function (response) {
													        window.SiteHub.server.sendNotifications()
													        if (response.Success) {
													          modal.modal('hide')
													          OBMisc.Notifications.GritterSuccess("Shifts Removed", "", 2000)
													          me.clearSelections()
													        }
													        else {
													          OBMisc.Notifications.GritterError('Error Removing Shifts', response.ErrorText, 3000)
													        }
													      })
													    }
													  }
													},
												 "fa-trash", "modal-xxxs")
  }
  else {

  }
};

ResourceSchedulerBase.prototype.updatePlayoutsOpsShift = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    me.setItemBusy(item)
    if (item.Booking.ResourceBookingTypeID == 11) {
      me.playoutOpsShiftModalMCR().editShiftSchedulerScreen(item.Booking, function (response) {
        me.setItemNotBusy(item)
      })
    }
    else if (item.Booking.ResourceBookingTypeID == 12 || item.Booking.ResourceBookingTypeID == 21) {
      SCCRShiftBO.editShiftSchedulerScreen(item, {
        onSuccess: function (response) {
          ViewModel.CurrentPlayoutOpsShiftSCCR.Set(response.Data[0])
          SCCRShiftBO.showModal()
          me.setItemNotBusy(item)
        },
        onFail: function (response) {
          me.setItemNotBusy(item)
        }
      })
    }
  }
};

//Studio Supervisor
ResourceSchedulerBase.prototype.removeStudioSupervisorShifts = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    var selections = me.visTimeline.itemSet.getAllSelections()
    if (selections.items.indexOf(item.id) < 0) { selections.items.push(item.id) }
    var resourceBookings = me.getResourceBookingItems(selections.items)

    var title = (resourceBookings.length > 1 ? 'Delete Shifts?' : 'Delete Shift')
    var message = (resourceBookings.length > 1 ? 'Are you sure you wish to delete the selected Supervisor Shifts?' : 'Are you sure you wish to delete the selected Supervisor Shift')

    OBMisc.Modals.Warning(title,
													message,
													"",
													{
													  cancelButton: {
													    cssClass: 'btn-warning',
													    text: 'No',
													    callback: function (modal, event) {
													      modal.modal('hide')
													    }
													  },
													  confirmButton: {
													    cssClass: 'btn-success',
													    text: 'Yes',
													    callback: function (modal, event) {
													      var bookings = []
													      resourceBookings.Iterate(function (rb, rbI) {
													        bookings.push(rb.Booking)
													      })
													      //call the method
													      //me.setItemBusy(item)
													      //window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
													      ViewModel.CallServerMethod("DeleteStudioSupervisorShifts", {
													        shifts: bookings
													      }, function (response) {
													        window.SiteHub.server.sendNotifications()
													        if (response.Success) {
													          modal.modal('hide')
													          OBMisc.Notifications.GritterSuccess("Shifts Removed", "", 2000)
													          me.clearSelections()
													        }
													        else {
													          OBMisc.Notifications.GritterError('Error Removing Shifts', response.ErrorText, 3000)
													        }
													      })
													    }
													  }
													},
												 "fa-trash")
  }
  else {

  }
};

ResourceSchedulerBase.prototype.updateStudioSupervisorShift = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    me.setItemBusy(item)
    me.studioSupervisorShiftModal().editShiftSchedulerScreen(item.Booking,
																														 function (response) {
																														   me.setItemNotBusy(item)
																														 },
																														 function (response) {
																														   me.setItemNotBusy(item)
																														 })
  }
};

ResourceSchedulerBase.prototype.afterStudioSupervisorShiftFetched = function (item, group, response) {
  var me = this;
  if (!me.isReadOnly) {
    me.setItemNotBusy(item)
    me.studioSupervisorShiftModal().showShiftModal()
  }
};

//Programming
ResourceSchedulerBase.prototype.removeProgrammingShifts = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    var selections = me.visTimeline.itemSet.getAllSelections()
    if (selections.items.indexOf(item.id) < 0) { selections.items.push(item.id) }
    var resourceBookings = me.getResourceBookingItems(selections.items)

    var title = (resourceBookings.length > 1 ? 'Delete Shifts?' : 'Delete Shift')
    var message = (resourceBookings.length > 1 ? 'Are you sure you wish to delete the selected Shifts?' : 'Are you sure you wish to delete the selected Shift')

    OBMisc.Modals.Warning(title,
													message,
													"",
													{
													  cancelButton: {
													    cssClass: 'btn-warning',
													    text: 'No',
													    callback: function (modal, event) {
													      modal.modal('hide')
													    }
													  },
													  confirmButton: {
													    cssClass: 'btn-success',
													    text: 'Yes',
													    callback: function (modal, event) {
													      var bookings = []
													      resourceBookings.Iterate(function (rb, rbI) {
													        bookings.push(rb.Booking)
													        me.setItemBusy(rb)
													      })
													      //call the method
													      //me.setItemBusy(item)
													      //window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
													      ViewModel.CallServerMethod("DeleteGenericShifts", {
													        bookings: bookings
													      }, function (response) {
													        window.SiteHub.server.sendNotifications()
													        if (response.Success) {
													          modal.modal('hide')
													          OBMisc.Notifications.GritterSuccess("Shifts Removed", "", 2000)
													          me.clearSelections()
													        }
													        else {
													          OBMisc.Notifications.GritterError('Error Removing Shifts', response.ErrorText, 3000)
													        }
													      })
													    }
													  }
													},
												 "fa-trash", "modal-xs")
  }
  else {

  }
};

ResourceSchedulerBase.prototype.updateProgrammingShift = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    me.setItemBusy(item)
    me.programmingShiftModal().editShiftSchedulerScreenStateless(item.Booking, function (response) {
      me.setItemNotBusy(item)
    })
  }
};

//StageHand
ResourceSchedulerBase.prototype.removeStageHandShifts = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    var selections = me.visTimeline.itemSet.getAllSelections()
    if (selections.items.indexOf(item.id) < 0) { selections.items.push(item.id) }
    var resourceBookings = me.getResourceBookingItems(selections.items)

    var title = (resourceBookings.length > 1 ? 'Delete Shifts?' : 'Delete Shift')
    var message = (resourceBookings.length > 1 ? 'Are you sure you wish to delete the selected Shifts?' : 'Are you sure you wish to delete the selected Shift')

    OBMisc.Modals.Warning(title,
													message,
													"",
													{
													  cancelButton: {
													    cssClass: 'btn-warning',
													    text: 'No',
													    callback: function (modal, event) {
													      modal.modal('hide')
													    }
													  },
													  confirmButton: {
													    cssClass: 'btn-success',
													    text: 'Yes',
													    callback: function (modal, event) {
													      var bookings = []
													      resourceBookings.Iterate(function (rb, rbI) {
													        bookings.push(rb.Booking)
													        me.setItemBusy(rb)
													      })
													      //call the method
													      //me.setItemBusy(item)
													      //window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
													      ViewModel.CallServerMethod("DeleteStageHandShifts", {
													        shifts: bookings
													      }, function (response) {
													        window.SiteHub.server.sendNotifications()
													        if (response.Success) {
													          modal.modal('hide')
													          OBMisc.Notifications.GritterSuccess("Shifts Removed", "", 2000)
													          me.clearSelections()
													        }
													        else {
													          OBMisc.Notifications.GritterError('Error Removing Shifts', response.ErrorText, 3000)
													        }
													      })
													    }
													  }
													},
												 "fa-trash", "modal-xs")
  }
  else {

  }
};

ResourceSchedulerBase.prototype.updateStageHandShift = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    me.setItemBusy(item)
    me.stagehandShiftModal().editShiftSchedulerScreenStateless(item.Booking, function (response) {
      me.setItemNotBusy(item)
    })
  }
};

//ICR
ResourceSchedulerBase.prototype.updateICRShift = function (item, group) {
  var me = this;
  if (!me.isReadOnly) {
    me.setItemBusy(item)
    me.icrShiftModal().editShiftSchedulerScreenStateless(item.Booking, function (response) {
      me.setItemNotBusy(item)
    })
  }
};

ResourceSchedulerBase.prototype.removeICRShifts = function (item, group, eventProperties) {
  var me = this;
  if (!me.isReadOnly) {
    var selections = me.visTimeline.itemSet.getAllSelections()
    if (selections.items.indexOf(item.id) < 0) { selections.items.push(item.id) }
    var resourceBookings = me.getResourceBookingItems(selections.items)

    var title = (resourceBookings.length > 1 ? 'Delete Shifts?' : 'Delete Shift')
    var message = (resourceBookings.length > 1 ? 'Are you sure you wish to delete the selected Shifts?' : 'Are you sure you wish to delete the selected Shift')

    OBMisc.Modals.Warning(title,
													message,
													"",
													{
													  cancelButton: {
													    cssClass: 'btn-warning',
													    text: 'No',
													    callback: function (modal, event) {
													      modal.modal('hide')
													    }
													  },
													  confirmButton: {
													    cssClass: 'btn-success',
													    text: 'Yes',
													    callback: function (modal, event) {
													      var bookings = []
													      resourceBookings.Iterate(function (rb, rbI) {
													        bookings.push(rb.Booking)
													        me.setItemBusy(rb)
													      })
													      //call the method
													      //me.setItemBusy(item)
													      //window.siteHubManager.bookingIntoEdit(item.Booking.ResourceBookingID, ViewModel.CurrentUserName(), ViewModel.CurrentUserID())
													      ViewModel.CallServerMethod("DeleteICRShifts", {
													        shifts: bookings
													      }, function (response) {
													        window.SiteHub.server.sendNotifications()
													        if (response.Success) {
													          modal.modal('hide')
													          OBMisc.Notifications.GritterSuccess("Shifts Removed", "", 2000)
													          me.clearSelections()
													        }
													        else {
													          OBMisc.Notifications.GritterError('Error Removing Shifts', response.ErrorText, 3000)
													        }
													      })
													    }
													  }
													},
												 "fa-trash", "modal-xs")
  }
  else {

  }
}

//#endregion