﻿//ROContractTypeList----------------------------------------------
var ROContractTypeListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROContractTypeListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ContractTypeID())
    n.Description(Item.ContractType())
    return n
  }
  Me.ItemIsSelectedCss = function (ROContractType) {
    var Found = ViewModel.ROContractTypeListManager().SelectedItems().Find("ID", ROContractType.ContractTypeID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROContractType) {
    var Found = ViewModel.ROContractTypeListManager().SelectedItems().Find("ID", ROContractType.ContractTypeID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROContractTypeModal").dialog({
      title: "Select Contract Type",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROContractTypeModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.ROContractTypeListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROContractTypeList();
  }
  Me.Manager = function () {
    return ViewModel.ROContractTypeListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROContractType) {
        localOptions.AfterRowSelected(ROContractType)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//RODisciplineList----------------------------------------------
var RODisciplineListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.RODisciplineListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.DisciplineID())
    n.Description(Item.Discipline())
    return n
  }
  Me.ItemIsSelectedCss = function (RODiscipline) {
    var Found = ViewModel.RODisciplineListManager().SelectedItems().Find("ID", RODiscipline.DisciplineID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (RODiscipline) {
    var Found = ViewModel.RODisciplineListManager().SelectedItems().Find("ID", RODiscipline.DisciplineID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#RODisciplineModal").dialog({
      title: "Select Discipline",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#RODisciplineModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.RODisciplineListCriteria();
  };
  Me.List = function () {
    return ViewModel.RODisciplineList();
  }
  Me.Manager = function () {
    return ViewModel.RODisciplineListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (RODiscipline) {
        localOptions.AfterRowSelected(RODiscipline)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//ROProductionTypePagedList----------------------------------------------
var ROProductionTypePagedListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionTypePagedListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionTypeID())
    n.Description(Item.ProductionType())
    return n
  }
  Me.ItemIsSelectedCss = function (ROProductionType) {
    var Found = ViewModel.ROProductionTypePagedListManager().SelectedItems().Find("ID", ROProductionType.ProductionTypeID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProductionType) {
    var Found = ViewModel.ROProductionTypePagedListManager().SelectedItems().Find("ID", ROProductionType.ProductionTypeID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROProductionTypeModal").dialog({
      title: "Select Production Type",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROProductionTypeModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.ROProductionTypePagedListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROProductionTypePagedList();
  }
  Me.Manager = function () {
    return ViewModel.ROProductionTypePagedListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROProductionType) {
        localOptions.AfterRowSelected(ROProductionType)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//ROEventTypePagedList---------------------------------------------------
var ROEventTypeManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  };
  Me.RefreshList = function () {
    ViewModel.ROEventTypePagedListManager().Refresh();
  };
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.EventTypeID())
    n.Description(Item.EventType())
    return n
  };
  Me.ItemIsSelectedCss = function (ROEventType) {
    var Found = ViewModel.ROEventTypePagedListManager().SelectedItems().Find("ID", ROEventType.EventTypeID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  };
  Me.ItemIsSelectedHtml = function (ROEventType) {
    var Found = ViewModel.ROEventTypePagedListManager().SelectedItems().Find("ID", ROEventType.EventTypeID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  };
  Me.ShowModal = function () {

    $("#ROEventTypesModal").dialog({
      title: "Select Event Type",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (localOptions.beforeOpen) { localOptions.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  };
  Me.HideModal = function () {
    $("#ROEventTypesModal").dialog('destroy');
  };
  Me.Criteria = function () {
    return ViewModel.ROEventTypePagedListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROEventTypePagedList();
  }
  Me.Manager = function () {
    return ViewModel.ROEventTypePagedListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROEventType) {
        localOptions.AfterRowSelected(ROEventType)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  Me.ClearSelection = function () {
    Me.Manager().SelectedItems([]);
  };
  return Me;
}

//ROProductionVenueList--------------------------------------------------
var ROProductionVenueManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionVenueListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionVenueID())
    n.Description(Item.ProductionVenue())
    return n
  }
  Me.ItemIsSelectedCss = function (ROProductionVenue) {
    var Found = ViewModel.ROProductionVenueListManager().SelectedItems().Find("ID", ROProductionVenue.ProductionVenueID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProductionVenue) {
    var Found = ViewModel.ROProductionVenueListManager().SelectedItems().Find("ID", ROProductionVenue.ProductionVenueID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    //ViewModel.ROProductionVenueListManager().SetOptions({
    //  AfterRowSelected: function (ROProductionVenue) {
    //    options.AfterRowSelected(ROProductionVenue)
    //  },
    //  AfterRowDeSelected: function (DeSelectedItem)  {
    //    options.AfterRowDeSelected(DeSelectedItem)
    //  }
    //});

    $("#ROProductionVenuesModal").dialog({
      title: "Select Production Venue",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  }
  Me.HideModal = function () {
    $("#ROProductionVenuesModal").dialog('destroy');
  };
  Me.Criteria = function () {
    return ViewModel.ROProductionVenueListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROProductionVenueList();
  }
  Me.Manager = function () {
    return ViewModel.ROProductionVenueListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROProductionDetailList-------------------------------------------------
var ROProductionDetailManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionDetailListPagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionID())
    n.Description(Item.Title())
    return n
  }
  Me.ItemIsSelectedCss = function (ROProductionDetail) {
    var Found = ViewModel.ROProductionDetailListPagingManager().SelectedItems().Find("ID", ROProductionDetail.ProductionID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProductionDetail) {
    var Found = ViewModel.ROProductionDetailListPagingManager().SelectedItems().Find("ID", ROProductionDetail.ProductionID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    ViewModel.ROProductionDetailListPagingManager().SetOptions({
      AfterRowSelected: function (ROProductionDetail) {
        options.AfterRowSelected(ROProductionDetail)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#ROProductionDetailsModal").dialog({
      title: "Select Production",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  },
  Me.HideModal = function () {
    $("#ROProductionDetailsModal").dialog('destroy');
  }
  return Me;
}

//ROTurnAroundPointList--------------------------------------------------
var ROTurnAroundPointManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROTurnAroundPointPagedListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.TurnAroundPointID())
    n.Description(Item.TurnAroundPoint())
    return n
  }
  Me.ItemIsSelectedCss = function (ROTurnAroundPoint) {
    var Found = ViewModel.ROTurnAroundPointPagedListManager().SelectedItems().Find("ID", ROTurnAroundPoint.TurnAroundPointID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROTurnAroundPoint) {
    var Found = ViewModel.ROTurnAroundPointPagedListManager().SelectedItems().Find("ID", ROTurnAroundPoint.TurnAroundPointID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    //Setup Production Type Paged Grid
    ViewModel.ROTurnAroundPointPagedListManager().SetOptions({
      AfterRowSelected: function (ROTurnAroundPoint) {
        options.AfterRowSelected(ROTurnAroundPoint)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#ROTurnAroundPointsModal").dialog({
      title: "Select TurnAround Points",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROTurnAroundPointsModal").dialog('destroy');
  }
  return Me;
}

//RORoomListPaged--------------------------------------------------------
var RORoomListPagedManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.RORoomListPagedManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.RoomID())
    n.Description(Item.Room())
    return n
  }
  Me.ItemIsSelectedCss = function (RORoom) {
    var Found = ViewModel.RORoomListPagedManager().SelectedItems().Find("ID", RORoom.RoomID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (RORoom) {
    var Found = ViewModel.RORoomListPagedManager().SelectedItems().Find("ID", RORoom.RoomID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.Criteria = function () {
    return ViewModel.RORoomListCriteria();
  };
  Me.List = function () {
    return ViewModel.RORoomList();
  }
  Me.Manager = function () {
    return ViewModel.RORoomListPagedManager()
  };
  Me.ShowModal = function () {

    //Setup Production Type Paged Grid
    ViewModel.RORoomListPagedManager().SetOptions({
      AfterRowSelected: function (RORoom) {
        options.AfterRowSelected(RORoom)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#RORoomListPagedModal").dialog({
      title: "Select Room",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#RORoomListPagedModal").dialog('destroy');
  }
  Me.Init = function () {
    ViewModel.RORoomListPagedManager().MultiSelect(false);
    ViewModel.RORoomListPagedManager().SingleSelect(true);
    ViewModel.RORoomListPagedManager().SetOptions({
      AfterRowSelected: function (RORoom) {
        localOptions.AfterRowSelected(RORoom);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem);
      }
    });
    //$("#ROHumanResourceListModal").dialog('destroy');
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROProductionBookedResourceTypeList-------------------------------------
var ROProductionBookedResourceTypeManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionBookedResourceTypeListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.RoomID())
    n.Description(Item.Room())
    return n
  }
  Me.ItemIsSelectedCss = function (BookedResourceType) {
    var Found = ViewModel.ROProductionBookedResourceTypeListManager().SelectedItems().Find("ID", BookedResourceType.ResourceTypeID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (BookedResourceType) {
    var Found = ViewModel.ROProductionBookedResourceTypeListManager().SelectedItems().Find("ID", BookedResourceType.ResourceTypeID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.Setup = function () {
    ViewModel.RORoomListPagedManager().SingleSelect(false);
    ViewModel.RORoomListPagedManager().MultiSelect(false);
    //Setup Production Type Paged Grid
    ViewModel.ROProductionBookedResourceTypeListManager().SetOptions({
      AfterRowSelected: function (BookedResourceType) {
        //options.AfterRowSelected(BookedResourceType)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        //options.AfterRowDeSelected(DeSelectedItem)
      }
    });
  }
  return Me;
}

//RODebtorList-----------------------------------------------------------
var RODebtorListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.RODebtorListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.DebtorID())
    n.Description(Item.Debtor())
    return n
  }
  Me.ItemIsSelectedCss = function (RODebtor) {
    var Found = ViewModel.RODebtorListManager().SelectedItems().Find("ID", RODebtor.DebtorID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (RODebtor) {
    var Found = ViewModel.RODebtorListManager().SelectedItems().Find("ID", RODebtor.DebtorID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#RODebtorListModal").dialog({
      title: "Select Debtor",
      closeText: "Close",
      height: 360,
      width: 900,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }

  Me.HideModal = function () {
    $("#RODebtorListModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.RODebtorListCriteria();
  };
  Me.List = function () {
    return ViewModel.RODebtorList();
  }
  Me.Manager = function () {
    return ViewModel.RODebtorListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (RODebtor) {
        localOptions.AfterRowSelected(RODebtor)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//ROQuoteList-----------------------------------------------------------
var ROQuoteListManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROQuoteListPagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.QuoteID())
    n.Description(Item.EventName())
    return n
  }
  Me.ItemIsSelectedCss = function (ROQuote) {
    var Found = ViewModel.ROQuoteListPagingManager().SelectedItems().Find("ID", ROQuote.QuoteID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROQuote) {
    var Found = ViewModel.ROQuoteListPagingManager().SelectedItems().Find("ID", ROQuote.QuoteID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    ViewModel.ROQuoteListPagingManager().SetOptions({
      AfterRowSelected: function (ROQuote) {
        options.AfterRowSelected(ROQuote)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#FindQuote").dialog({
      title: "Select Quote",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  }
  Me.HideModal = function () {
    $("#FindQuote").dialog('destroy');
  }
  Me.Init = function (options) {
    options.init();
  }
  return Me;
}

//ROSynergyEventList-----------------------------------------------------
var ROSynergyEventListManager = function (options) {
  var Me = this;
  var localOptions;
  $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROSynergyEventListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    if (ViewModel.ROSynergyEventListCriteria().OBInd() || ViewModel.ROSynergyEventListCriteria().SatOpsInd()) {
      var n = new SelectedItemObject()
      n.ID(Item.ImportedEventID())
      n.Description(Item.Title())
      return n
    } else {
      var n = new SelectedItemObject()
      n.ID(Item.ImportedEventChannelID())
      n.Description(Item.Title())
      return n
    }
  }
  Me.ItemIsSelectedCss = function (ROSynergyEventChannel) {
    var Found;
    if (ViewModel.ROSynergyEventListCriteria().OBInd() || ViewModel.ROSynergyEventListCriteria().SatOpsInd()) {
      Found = ViewModel.ROSynergyEventListManager().SelectedItems().Find("ID", ROSynergyEventChannel.ImportedEventID());
    } else {
      Found = ViewModel.ROSynergyEventListManager().SelectedItems().Find("ID", ROSynergyEventChannel.ImportedEventChannelID());
    }
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROSynergyEventChannel) {
    var Found;
    if (ViewModel.ROSynergyEventListCriteria().OBInd() || ViewModel.ROSynergyEventListCriteria().SatOpsInd()) {
      Found = ViewModel.ROSynergyEventListManager().SelectedItems().Find("ID", ROSynergyEventChannel.ImportedEventID());
    } else {
      Found = ViewModel.ROSynergyEventListManager().SelectedItems().Find("ID", ROSynergyEventChannel.ImportedEventChannelID());
    }
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }

  Me.SelectedItems = function () {
    return ViewModel.ROSynergyEventListManager().SelectedItems();
  };
  Me.ClearSelection = function () {
    ViewModel.ROSynergyEventListManager().SelectedItems([]);
  };
  Me.Criteria = function () {
    return ViewModel.ROSynergyEventListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROSynergyEventList();
  }
  Me.Manager = function () {
    return ViewModel.ROSynergyEventListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SelectedItems = function () {
    return ViewModel.ROSynergyEventListManager().SelectedItems();
  };
  Me.ClearSelection = function () {
    ViewModel.ROSynergyEventListManager().SelectedItems([]);
  };
  Me.Criteria = function () {
    return ViewModel.ROSynergyEventListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROSynergyEventList();
  }
  Me.Manager = function () {
    return ViewModel.ROSynergyEventListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.ShowModal = function () {

    $("#ROSynergyEventListModal").dialog({
      title: "Import Events",
      closeText: "Close",
      height: 810,
      width: '80%',
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
        if (options.afterOpen) { options.afterOpen() }
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROSynergyEventListModal").dialog('destroy');
  }
  Me.Init = function (newOptions, managerOptions, customInit) {
    if (newOptions) {
      $.extend({}, localOptions, newOptions);
    }
    if (managerOptions) {
      ViewModel.ROSynergyEventListManager().SetOptions(managerOptions);
    }
    if (customInit) {
      customInit();
    }
  };
  return Me;
}

//ROResourceSelect-------------------------------------------------------
var ROResourceSelectManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend(localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROResourceSelectListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ResourceID())
    n.Description(Item.ResourceName())
    return n
  }
  Me.ItemIsSelectedCss = function (ROResource) {
    var Found = ViewModel.ROResourceSelectListManager().SelectedItems().Find("ID", ROResource.ResourceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROResource) {
    var Found = ViewModel.ROResourceSelectListManager().SelectedItems().Find("ID", ROResource.ResourceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.Criteria = function () {
    return ViewModel.ROHRBookingListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROHRBookingList();
  }
  Me.Manager = function () {
    return ViewModel.ROResourceSelectListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SelectedItems = function () {
    return ViewModel.ROResourceSelectListManager().SelectedItems();
  };
  Me.ShowModal = function (businessObject) {

    ////Setup Production Type Paged Grid
    //ViewModel.ROResourceSelectListManager().SetOptions({
    //  AfterRowSelected: function (ROResource) {
    //    options.AfterRowSelected(ROResource)
    //  },
    //  AfterRowDeSelected: function (DeSelectedItem) {
    //    options.AfterRowDeSelected(DeSelectedItem)
    //  }
    //});
    //if (localOptions.beforeShowModal) {
    //  localOptions.beforeShowModal(businessObject);
    //};

    //$("#ROResourceSelectListModal").dialog({
    //  title: "Select Resources",
    //  closeText: "Close",
    //  height: 720,
    //  width: 1024,
    //  modal: true,
    //  resizeable: true,
    //  open: function (event, ui) {
    //    if (localOptions.beforeOpen) { localOptions.beforeOpen() }
    //    Me.RefreshList();
    //  },
    //  beforeClose: function (event, ui) {
    //    if (localOptions.beforeClose) { localOptions.beforeClose() }
    //  }
    //})

  }
  Me.HideModal = function () {
    $("#ROResourceSelectListModal").dialog('destroy');
  }
  Me.Init = function (newOptions, customInit) {
    //if (newOptions) {
    //  localOptions = $.extend(localOptions, newOptions);
    //}
    //if (customInit) {
    //  customInit();
    //}
  }
  return Me;
}

//ROCreditorInvoiceList----------------------------------------------------
var ROCreditorInvoiceManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROCreditorInvoiceDetailListPagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ResourceID())
    n.Description(Item.ResourceName())
    return n
  }
  Me.ItemIsSelectedCss = function (ROCreditorInvoice) {
    var Found = ViewModel.ROCreditorInvoiceDetailListPagingManager().SelectedItems().Find("ID", ROCreditorInvoice.CreditorInvoiceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROCreditorInvoice) {
    var Found = ViewModel.ROCreditorInvoiceDetailListPagingManager().SelectedItems().Find("ID", ROCreditorInvoice.CreditorInvoiceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.Criteria = function () {
    return ViewModel.ROCreditorInvoiceDetailListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROCreditorInvoiceDetailList();
  }
  Me.Manager = function () {
    return ViewModel.ROCreditorInvoiceDetailListPagingManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SelectedItems = function () {
    return ViewModel.ROCreditorInvoiceDetailListPagingManager().SelectedItems();
  };
  Me.ShowModal = function () {

    $("#InvoicesByHumanResource").modal({
      title: "Select Resources",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (localOptions.beforeOpen) { localOptions.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
        if (localOptions.beforeClose) { localOptions.beforeClose() }
      }
    })
    Me.RefreshList();
  }
  Me.HideModal = function () {
    $("#InvoicesByHumanResource").modal('hide');
  }
  Me.Init = function (newOptions, customInit) {

  }
  return Me;
}

//ROBookingStatusList----------------------------------------------------
var ROBookingStatusListManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionAreaAllowedStatusListPagedManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionAreaStatusID())
    n.Description(Item.ProductionAreaStatus())
    return n
  }
  Me.ItemIsSelectedCss = function (ROBookingStatus) {
    var Found = ViewModel.ROProductionAreaAllowedStatusListPagedManager().SelectedItems().Find("ID", ROBookingStatus.ProductionAreaStatusID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROBookingStatus) {
    var Found = ViewModel.ROProductionAreaAllowedStatusListPagedManager().SelectedItems().Find("ID", ROBookingStatus.ProductionAreaStatusID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    //Setup Production Type Paged Grid
    ViewModel.ROProductionAreaAllowedStatusListPagedManager().SetOptions({
      AfterRowSelected: function (ROBookingStatus) {
        options.AfterRowSelected(ROBookingStatus)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#ROBookingStatusListModal").dialog({
      title: "Select Status",
      closeText: "Close",
      height: 500,
      width: 500,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROBookingStatusListModal").dialog('destroy');
  }
  return Me;
}

//ROPSAStatusList--------------------------------------------------------
var ROPSAStatusListManager = function (options) {
  var Me = this;
  var localOptions;
  $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROPSAStatusesListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionAreaStatusID())
    n.Description(Item.ProductionAreaStatus())
    return n
  }
  Me.ItemIsSelectedCss = function (ROPSAStatus) {
    var Found = ViewModel.ROPSAStatusesListManager().SelectedItems().Find("ID", ROPSAStatus.ProductionAreaStatusID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROPSAStatus) {
    var Found = ViewModel.ROPSAStatusesListManager().SelectedItems().Find("ID", ROPSAStatus.ProductionAreaStatusID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    ////Setup Production Type Paged Grid
    //ViewModel.ROPSAStatusesListManager().SetOptions({
    //  AfterRowSelected: function (ROPSAStatus) {
    //    options.AfterRowSelected(ROPSAStatus)
    //  },
    //  AfterRowDeSelected: function (DeSelectedItem) {
    //    options.AfterRowDeSelected(DeSelectedItem)
    //  }
    //});

    $("#ROPSAStatusesListModal").dialog({
      title: "Select Status",
      closeText: "Close",
      height: 500,
      width: 500,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROPSAStatusesListModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.ROPSAStatusesListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROPSAStatusesList();
  }
  Me.Manager = function () {
    return ViewModel.ROPSAStatusesListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.Init = function (newOptions, customInit) {
    if (newOptions) {
      localOptions = $.extend({}, localOptions, newOptions);
    }
    if (customInit) {
      customInit();
    }
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROResourceList---------------------------------------------------------
var ROResourceListManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROResourceListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ResourceID())
    n.Description(Item.ResourceName())
    return n
  }
  Me.ItemIsSelectedCss = function (ROResource) {
    var Found = ViewModel.ROResourceListManager().SelectedItems().Find("ID", ROResource.ResourceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROResource) {
    var Found = ViewModel.ROResourceListManager().SelectedItems().Find("ID", ROResource.ResourceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROResourceListModal").dialog({
      title: "Select Resource",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROResourceListModal").dialog('destroy');
  }
  return Me;
}

//ROSpecRequiremnts------------------------------------------------------
var ROProductionSpecsManager = function (options) {
  var Me = this;
  var localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionSpecListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionSpecRequirementID())
    n.Description(Item.ProductionSpecRequirementName())
    return n
  }
  Me.ItemIsSelectedCss = function (ROSpecRequirement) {
    var Found = ViewModel.ROProductionSpecListManager().SelectedItems().Find("ID", ROSpecRequirement.ProductionSpecRequirementID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROSpecRequirement) {
    var Found = ViewModel.ROProductionSpecListManager().SelectedItems().Find("ID", ROSpecRequirement.ProductionSpecRequirementID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $(localOptions.ModalID).dialog({
      title: "Select Resource",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $(localOptions.ModalID).dialog('destroy');
  }
  Me.Init = function () {
    //Setup Production Type Paged Grid
    ViewModel.ROProductionSpecListManager().SetOptions({
      AfterRowSelected: function (ROSpecRequirement) {
        localOptions.AfterRowSelected(ROSpecRequirement)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
    ViewModel.ROProductionSpecListManager().SingleSelect(true);
    ViewModel.ROProductionSpecListManager().MultiSelect(false);
  }
  return Me;
}

//ROAudioSettingList-----------------------------------------------------
var ROAudioSettingListManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROAudioSettingListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.AudioSettingID())
    n.Description(Item.AudioSetting())
    return n
  }
  Me.ItemIsSelectedCss = function (ROAudioSetting) {
    var Found = ViewModel.ROAudioSettingListManager().SelectedItems().Find("ID", ROAudioSetting.AudioSettingID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROAudioSetting) {
    var Found = ViewModel.ROAudioSettingListManager().SelectedItems().Find("ID", ROAudioSetting.AudioSettingID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROAudioSettingListModal").dialog({
      title: "Select AudioSetting",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROAudioSettingListModal").dialog('destroy');
  }
  return Me;
}

//ROGenreSeriesMappingList-----------------------------------------------
var ROGenreSeriesMappingListManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROGenreSeriesMappingListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.GenreSeriesMappingID())
    n.Description(Item.GenreCode())
    return n
  }
  Me.ItemIsSelectedCss = function (ROGenreSeriesMapping) {
    var Found = ViewModel.ROGenreSeriesMappingListManager().SelectedItems().Find("ID", ROGenreSeriesMapping.GenreSeriesMappingID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROGenreSeriesMapping) {
    var Found = ViewModel.ROGenreSeriesMappingListManager().SelectedItems().Find("ID", ROGenreSeriesMapping.GenreSeriesMappingID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    //Setup Production Type Paged Grid
    ViewModel.ROGenreSeriesMappingListManager().SetOptions({
      AfterRowSelected: function (ROGenreSeriesMapping) {
        options.AfterRowSelected(ROGenreSeriesMapping)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });


    $("#ROGenreSeriesMappingListModal").dialog({
      title: "Select GenreSeriesMapping",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROGenreSeriesMappingListModal").dialog('destroy');
  };
  Me.Init = function () {
    ViewModel.ROGenreSeriesMappingListManager().MultiSelect(false);
    ViewModel.ROGenreSeriesMappingListManager().SingleSelect(true);
    ViewModel.ROGenreSeriesMappingListManager().SetOptions({
      AfterRowSelected: function (ROGenreSeriesMapping) {
        options.AfterRowSelected(ROGenreSeriesMapping)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//ROHumanResourceList----------------------------------------------------
var ROHumanResourceListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList();
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROHumanResourceListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    Item.SelectedInd(!Item.SelectedInd());
    var n = new SelectedItemObject();
    n.ID(Item.HumanResourceID());
    n.Description(Item.HumanResource());
    return n;
  }
  Me.CheckSelected = function () {
    for (var i = 0; i < ViewModel.ROHRBookingListManager().SelectedItems().length; i++) {
      if (ViewModel.ROHRBookingListManager().SelectedItems()[i].SelectedInd())
        return false;
    }
    return true;
  }
  Me.ItemIsSelectedCss = function (ROHumanResource) {
    var Found = ViewModel.ROHumanResourceListManager().SelectedItems().Find("ID", ROHumanResource.HumanResourceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROHumanResource) {
    var Found = ViewModel.ROHumanResourceListManager().SelectedItems().Find("ID", ROHumanResource.HumanResourceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  };
  Me.Criteria = function () {
    return ViewModel.ROHumanResourceListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROHumanResourceList();
  }
  Me.Manager = function () {
    return ViewModel.ROHumanResourceListManager()
  };
  Me.ItemClicked = function (ROHumanResource) {
    ROHumanResource.SelectedInd(!ROHumanResource.SelectedInd());
  }
  Me.ShowModal = function () {

    //Setup Production Type Paged Grid
    ViewModel.ROHumanResourceListManager().SetOptions({
      AfterRowSelected: function (ROHumanResource) {
        localOptions.AfterRowSelected(ROHumanResource);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem);
      }
    });

    $("#ROHumanResourceListModal").dialog({
      title: "Select Human Resource",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROHumanResourceListModal").dialog('destroy');
  };
  Me.Init = function () {
    ViewModel.ROHumanResourceListManager().MultiSelect(false);
    ViewModel.ROHumanResourceListManager().SingleSelect(true);
    ViewModel.ROHumanResourceListManager().SetOptions({
      AfterRowSelected: function (ROHumanResource) {
        localOptions.AfterRowSelected(ROHumanResource);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem);
      }
    });
    //$("#ROHumanResourceListModal").dialog('destroy');
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROScheduleSenderHumanResourceListPagingManager-------------------------
var ROScheduleSenderHumanResourceManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROScheduleSenderHumanResourceListPagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.HumanResourceID())
    n.Description(Item.Firstname())
    return n
  }
  Me.ItemIsSelectedCss = function (ROScheduleSenderHumanResource) {
    var Found = ViewModel.ROScheduleSenderHumanResourceListPagingManager().SelectedItems().Find("ID", ROScheduleSenderHumanResource.HumanResourceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROScheduleSenderHumanResource) {
    var Found = ViewModel.ROScheduleSenderHumanResourceListPagingManager().SelectedItems().Find("ID", ROScheduleSenderHumanResource.HumanResourceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.Init = function () {
    //Setup Production Type Paged Grid
    ViewModel.ROScheduleSenderHumanResourceListPagingManager().SetOptions({
      AfterRowSelected: function (ROScheduleSenderHumanResource) {
        options.AfterRowSelected(ROScheduleSenderHumanResource)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });
  }
  return Me;
}

//ROAllowedDisciplineSelectList------------------------------------------
var ROAllowedDisciplineSelectManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROAllowedDisciplineSelectListPagingManager().Refresh();
  }
  Me.ClearSelection = function () {
    ViewModel.ROAllowedDisciplineSelectListPagingManager().SelectedItems([]);
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.RowNo())
    n.Description(Item.Discipline() + ' ' + Item.Position())
    return n
  }
  Me.ItemIsSelectedCss = function (ROAllowedDisciplineSelect) {
    var Found = ViewModel.ROAllowedDisciplineSelectListPagingManager().SelectedItems().Find("ID", ROAllowedDisciplineSelect.RowNo());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROAllowedDisciplineSelect) {
    var Found = ViewModel.ROAllowedDisciplineSelectListPagingManager().SelectedItems().Find("ID", ROAllowedDisciplineSelect.RowNo());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    ViewModel.ROAllowedDisciplineSelectListPagingManager().SetOptions({
      AfterRowSelected: function (ROAllowedDisciplineSelect) {
        options.AfterRowSelected(ROAllowedDisciplineSelect)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#ROAllowedDisciplineSelectModal").dialog({
      title: "Select Disciplines",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  }
  Me.HideModal = function () {
    $("#ROAllowedDisciplineSelectModal").dialog('destroy');
  }
  Me.Init = function (options) {
    options.init();
  }
  return Me;
}

//ROImportedEventSettingList---------------------------------------------
var ROImportedEventSettingListManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROImportedEventSettingListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ImportedEventSettingID())
    n.Description(Item.ImportedEventSettingID())
    return n
  }
  Me.ItemIsSelectedCss = function (ROImportedEventSetting) {
    var Found = ViewModel.ROImportedEventSettingListManager().SelectedItems().Find("ID", ROImportedEventSetting.ImportedEventSettingID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROImportedEventSetting) {
    var Found = ViewModel.ROImportedEventSettingListManager().SelectedItems().Find("ID", ROImportedEventSetting.ImportedEventSettingID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROImportedEventSettingListModal").dialog({
      title: "Settings",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROImportedEventSettingListModal").dialog('destroy');
  }
  return Me;
}

//ROProductionHumanResourceBaseList--------------------------------------
var ROProductionHumanResourceBaseListManager = function (options) {
  var Me = this;
  var localOptions;
  $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionHumanResourceBaseListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionHumanResourceID())
    n.Description(Item.DisciplinePosition() + " - " + Item.HumanResource());
    return n
  }
  Me.ItemIsSelectedCss = function (ROProductionHumanResourceBase) {
    var Found = ViewModel.ROProductionHumanResourceBaseListManager().SelectedItems().Find("ID", ROProductionHumanResourceBase.ProductionHumanResourceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProductionHumanResourceBase) {
    var Found = ViewModel.ROProductionHumanResourceBaseListManager().SelectedItems().Find("ID", ROProductionHumanResourceBase.ProductionHumanResourceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span> Select"
    } else {
      return "<span class='fa fa-times'><span> Select"
    }
  }
  Me.Criteria = function () {
    return ViewModel.ROProductionHumanResourceBaseListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROProductionHumanResourceBaseList();
  }
  Me.Manager = function () {
    return ViewModel.ROProductionHumanResourceBaseListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.ShowModal = function () {

    $("#ROProductionHumanResourceBaseListModal").dialog({
      title: "Select ProductionHumanResourceBase",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROProductionHumanResourceBaseListModal").dialog('destroy');
  }
  Me.Init = function (newOptions, customInit) {
    if (newOptions) {
      localOptions = $.extend({}, localOptions, newOptions);
    }
    if (customInit) {
      customInit();
    }
  }
  return Me;
}

//ROHRBookingList--------------------------------------------------------
var ROHRBookingListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROHRBookingListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.HumanResourceID())
    n.Description(Item.HumanResource());
    return n
  }
  Me.ItemIsSelectedCss = function (ROHRBooking) {
    var Found = ViewModel.ROHRBookingListManager().SelectedItems().Find("ID", ROHRBooking.HumanResourceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROHRBooking) {
    var Found = ViewModel.ROHRBookingListManager().SelectedItems().Find("ID", ROHRBooking.HumanResourceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span> Select"
    } else {
      return "<span class='fa fa-times'><span> Select"
    }
  }
  Me.Criteria = function () {
    return ViewModel.ROHRBookingListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROHRBookingList();
  }
  Me.Manager = function () {
    return ViewModel.ROHRBookingListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.ShowModal = function () {

    //$("#ROHRBookingListModal").dialog({
    //  title: "Select ProductionHumanResourceBase",
    //  closeText: "Close",
    //  height: 800,
    //  width: 1024,
    //  modal: true,
    //  resizeable: true,
    //  open: function (event, ui) {
    //    if (options.beforeOpen) { options.beforeOpen() }
    //    Me.RefreshList();
    //  },
    //  beforeClose: function (event, ui) {

    //  }
    //})

  }
  Me.HideModal = function () {
    $("#ROHRBookingListModal").dialog('destroy');
  }
  Me.Init = function (newOptions, customInit) {
    if (newOptions) {
      localOptions = $.extend({}, localOptions, newOptions);
    }
    if (customInit) {
      customInit();
    }
  }
  return Me;
};

//ROCompanyRateCardManagerList-------------------------------------------
var ROCompanyRateCardManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROCompanyRateCardListPagingManager().Refresh();
  }
  Me.ClearSelection = function () {
    ViewModel.ROCompanyRateCardListPagingManager().SelectedItems([]);
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.CompanyRateCardID())
    n.Description(Item.Company() + ' ' + Item.Rate())
    return n
  }
  Me.ItemIsSelectedCss = function (ROCompanyRateCard) {
    var Found = ViewModel.ROCompanyRateCardListPagingManager().SelectedItems().Find("ID", ROCompanyRateCard.CompanyRateCardID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROCompanyRateCard) {
    var Found = ViewModel.ROCompanyRateCardListPagingManager().SelectedItems().Find("ID", ROCompanyRateCard.CompanyRateCardID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    ViewModel.ROCompanyRateCardListPagingManager().SetOptions({
      AfterRowSelected: function (ROCompanyRateCard) {
        options.AfterRowSelected(ROCompanyRateCard)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#ROCompanyRateCardListModal").dialog({
      title: "Select Company Rate Card",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  }
  Me.HideModal = function () {
    $("#ROCompanyRateCardListModal").dialog('destroy');
  }
  Me.Init = function (options) {
    options.init();
  }
  return Me;
};

//ROSupplierRateCardManager
var ROSupplierRateCardManager = function (options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROSupplierRateCardListPagingManager().Refresh();
  }
  Me.ClearSelection = function () {
    ViewModel.ROSupplierRateCardListPagingManager().SelectedItems([]);
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.SupplierRateCardID())
    n.Description(Item.Supplier() + ' ' + Item.Amount())
    return n
  }
  Me.ItemIsSelectedCss = function (ROSupplierRateCard) {
    var Found = ViewModel.ROSupplierRateCardListPagingManager().SelectedItems().Find("ID", ROSupplierRateCard.SupplierRateCardID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROSupplierRateCard) {
    var Found = ViewModel.ROSupplierRateCardListPagingManager().SelectedItems().Find("ID", ROSupplierRateCard.SupplierRateCardID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    ViewModel.ROSupplierRateCardListPagingManager().SetOptions({
      AfterRowSelected: function (ROSupplierRateCard) {
        options.AfterRowSelected(ROSupplierRateCard)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $("#ROSupplierRateCardsModal").dialog({
      title: "Select Supplier Rate Card",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {
      }
    })

  }
  Me.HideModal = function () {
    $("#ROSupplierRateCardsModal").dialog('destroy');
  }
  Me.Init = function (options) {
    options.init();
  }
  return Me;
};

//RORoomScheduleList-----------------------------------------------------
var RORORoomScheduleListManager = function (options) {
  var Me = this;
  var localOptions;
  $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.RORoomScheduleListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.RoomScheduleID())
    n.Description(Item.Title());
    return n
  }
  Me.ItemIsSelectedCss = function (RORoomSchedule) {
    var Found = ViewModel.RORoomScheduleListManager().SelectedItems().Find("ID", RORoomSchedule.RoomScheduleID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (RORoomSchedule) {
    var Found = ViewModel.RORoomScheduleListManager().SelectedItems().Find("ID", RORoomSchedule.RoomScheduleID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span> Select"
    } else {
      return "<span class='fa fa-times'><span> Select"
    }
  }
  Me.Criteria = function () {
    return ViewModel.RORoomScheduleListCriteria();
  };
  Me.List = function () {
    return ViewModel.RORoomScheduleList();
  }
  Me.Manager = function () {
    return ViewModel.RORoomScheduleListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.ShowModal = function () {

    //$("#ROHRBookingListModal").dialog({
    //  title: "Select ProductionHumanResourceBase",
    //  closeText: "Close",
    //  height: 800,
    //  width: 1024,
    //  modal: true,
    //  resizeable: true,
    //  open: function (event, ui) {
    //    if (options.beforeOpen) { options.beforeOpen() }
    //    Me.RefreshList();
    //  },
    //  beforeClose: function (event, ui) {

    //  }
    //})

  }
  Me.HideModal = function () {
    $("#ROHRBookingListModal").dialog('destroy');
  }
  Me.Init = function (newOptions, customInit) {
    if (newOptions) {
      localOptions = $.extend({}, localOptions, newOptions);
    }
    Me.Manager().SetOptions({
      AfterRowSelected: function (RORoomSchedule) {
        localOptions.AfterRowSelected(RORoomSchedule)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
    if (customInit) {
      customInit();
    }
  }
  return Me;
};

//ROCityListManager------------------------------------------------------
var ROCityListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROCityListManager().Refresh();
  }
  Me.ItemClicked = function (ROCity) {
    //ROCountry.SelectedInd(!ROCountry.SelectedInd());
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject();
    if (ViewModel.CurrentHumanResource()) {
      ViewModel.CurrentHumanResource().CityID(Item.CityID());
    }
    n.ID(Item.CityID());
    n.Description(Item.City());
    return n;
  }
  Me.ItemIsSelectedCss = function (ROCity) {
    var Found = ViewModel.ROCityListManager().SelectedItems().Find("ID", ROCity.CityID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROCity) {
    var Found = ViewModel.ROCityListManager().SelectedItems().Find("ID", ROCity.CityID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }

  Me.ShowModal = function () {
    ViewModel.ROCityListManager().Refresh();
    $("#CityModal").dialog({
      title: "Select City",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    ViewModel.ROCityListCriteria().CountryID(null);
    ViewModel.ROCityListCriteria().Country('');
    $("#CityModal").dialog('destroy');
  };
  Me.Criteria = function () {
    return ViewModel.ROCityListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROCityList();
  }
  Me.Init = function () {
    ViewModel.ROCityListManager().MultiSelect(false);
    ViewModel.ROCityListManager().SingleSelect(true);
    ViewModel.ROCityListManager().SetOptions({
      AfterRowSelected: function (ROCity) {
        options.AfterRowSelected(ROCity);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem);
      }
    });
    $("#CityModal").dialog('destroy');
  };
  Me.Manager = function () {
    return ViewModel.ROCityListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROCountryListManager---------------------------------------------------
var ROCountryListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList();
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROCountryListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject();
    n.ID(Item.CountryID());
    n.Description(Item.Country());
    return n;
  }
  Me.ItemIsSelectedCss = function (ROCountry) {
    var Found = ViewModel.ROCountryListManager().SelectedItems().Find("ID", ROCountry.CountryID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROCountry) {
    var Found = ViewModel.ROCountryListManager().SelectedItems().Find("ID", ROCountry.CountryID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ItemClicked = function (ROCountry) {
    //ROCountry.SelectedInd(!ROCountry.SelectedInd());
  }
  Me.ShowModal = function () {

    ////Setup Production Type Paged Grid
    //ViewModel.ROCountryListManager().SetOptions({
    //  AfterRowSelected: function (ROCountry) {
    //    options.AfterRowSelected(ROCountry);
    //  },
    //  AfterRowDeSelected: function (DeSelectedItem) {
    //    options.AfterRowDeSelected(DeSelectedItem);
    //  }
    //});


    $("#CountryModal").dialog({
      title: "Select Country",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#CountryModal").dialog('destroy');
  };
  //Me.Init = function () {
  //  ViewModel.ROCountryListManager().MultiSelect(false);
  //  ViewModel.ROCountryListManager().SingleSelect(true);
  //  ViewModel.ROCountryListManager().SetOptions({
  //    AfterRowSelected: function (ROCountry) {
  //      options.AfterRowSelected(ROCountry);
  //    },
  //    AfterRowDeSelected: function (DeSelectedItem) {
  //      options.AfterRowDeSelected(DeSelectedItem);
  //    }
  //  });
  //  $("#CountryModal").dialog('destroy');
  //};
  Me.Init = function (newOptions, customInit) {
    if (newOptions) {
      localOptions = $.extend({}, localOptions, newOptions);
    }
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROCountry) {
        localOptions.AfterRowSelected(ROCountry)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
    if (customInit) {
      customInit();
    }
  }

  Me.Manager = function () {
    return ViewModel.ROCountryListManager()
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROEquipmentListManager-------------------------------------------------
var ROEquipmentListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList();
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROEquipmentListPagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject();
    n.ID(Item.EquipmentID());
    n.Description(Item.EquipmentDescription());
    return n;
  }
  Me.ItemIsSelectedCss = function (ROEquipment) {
    var Found = ViewModel.ROEquipmentListPagingManager().SelectedItems().Find("ID", ROEquipment.EquipmentID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROEquipment) {
    var Found = ViewModel.ROEquipmentListPagingManager().SelectedItems().Find("ID", ROEquipment.EquipmentID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.getXMLString = function (list) {
    var returnString = '<IDs>';
    for (var i = 0; i < list.length; i++) {
      returnString += '<ID>' + list[i].ID() + '</ID>';
    }
    returnString += '</IDs>';
    return returnString;
  }
  Me.ShowModal = function () {

    //////Setup Production Type Paged Grid
    ////ViewModel.ROEquipmentListPagingManager().SetOptions({
    ////  AfterRowSelected: function (ROEquipment) {
    ////    options.AfterRowSelected(ROEquipment);
    ////  },
    ////  AfterRowDeSelected: function (DeSelectedItem) {
    ////    options.AfterRowDeSelected(DeSelectedItem);
    ////  }
    ////});


    //$("#FindEquipment").dialog({
    //  title: "Select ROCountryMapping",
    //  closeText: "Close",
    //  height: 800,
    //  width: 1024,
    //  modal: true,
    //  resizeable: true,
    //  open: function (event, ui) {
    //    if (options.beforeOpen) { options.beforeOpen() }
    //    Me.RefreshList();
    //  },
    //  beforeClose: function (event, ui) {

    //  }
    //})

  }
  Me.HideModal = function () {
    //$("#FindEquipment").dialog('destroy');
  };
  Me.Criteria = function () {
    return ViewModel.ROEEquipmentListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROEquipmentList();
  }
  Me.Manager = function () {
    return ViewModel.ROEquipmentListPagingManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROEventType) {
        localOptions.AfterRowSelected(ROEventType)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
};

//ROAdHocBookingListManager----------------------------------------------
var ROROAdHocBookingListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList();
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROAdHocBookingListPagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject();
    n.ID(Item.AdHocBookingID());
    n.Description(Item.AdHocBookingID());
    return n;
  }
  Me.ItemIsSelectedCss = function (ROAdHocBooking) {
    var Found = ViewModel.ROAdHocBookingListPagingManager().SelectedItems().Find("ID", ROAdHocBooking.AdHocBookingID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROAdHocBooking) {
    var Found = ViewModel.ROAdHocBookingListPagingManager().SelectedItems().Find("ID", ROAdHocBooking.AdHocBookingID());
    if (Found) {
      return "<span class='fa fa-check-square-o'> Selected<span>"
    } else {
      return "<span>Select<span>"
    }
    //class='fa fa-times'
  }
  Me.ShowModal = function () {

    $("#ROAdHocBookingListModal").dialog({
      title: "Select Ad Hoc Booking",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: false,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROAdHocBookingListModal").dialog('destroy');
  };
  Me.Criteria = function () {
    return ViewModel.ROAdHocBookingListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROAdHocBookingList();
  }
  Me.Manager = function () {
    return ViewModel.ROAdHocBookingListPagingManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    ViewModel.ROAdHocBookingListPagingManager().SetOptions({
      AfterRowSelected: function (ROAdHocBooking) {
        localOptions.AfterRowSelected(ROAdHocBooking)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
    Me.RefreshList();
  };
  return Me;
}

//ROProductionList----------------------------------------------
var ROProductionListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProductionListPagingManager().Refresh();
  };
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionID())
    n.Description(Item.ProductionDescription())
    return n
  }
  Me.ItemIsSelectedCss = function (ROProduction) {
    var Found = ViewModel.ROProductionListPagingManager().SelectedItems().Find("ID", ROProduction.ProductionID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProduction) {
    var Found = ViewModel.ROProductionListPagingManager().SelectedItems().Find("ID", ROProduction.ProductionID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROProductionListModal").dialog({
      title: "Select Production",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: false,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROProductionListModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.ROProductionListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROProductionList();
  }
  Me.Manager = function () {
    return ViewModel.ROProductionListPagingManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROProduction) {
        localOptions.AfterRowSelected(ROProduction)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//ROSynergyVenueDateGroupList-----------------------------------------------------
var ROSynergyVenueDateGroupListManager = function (options) {
  var Me = this;
  var localOptions;
  $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROSynergyVenueDateGroupListManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.GroupIDValue())
    n.Description(Item.Venue())
    return n
  }
  Me.ItemIsSelectedCss = function (ROSynergyVenueDateGroup) {
    var Found;
    Found = ViewModel.ROSynergyVenueDateGroupListManager().SelectedItems().Find("ID", ROSynergyVenueDateGroup.GroupIDValue());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROSynergyVenueDateGroup) {
    var Found;
    Found = ViewModel.ROSynergyVenueDateGroupListManager().SelectedItems().Find("ID", ROSynergyVenueDateGroup.GroupIDValue());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.SelectedItems = function () {
    return ViewModel.ROSynergyVenueDateGroupListManager().SelectedItems();
  };
  Me.ClearSelection = function () {
    ViewModel.ROSynergyVenueDateGroupListManager().SelectedItems([]);
  };
  Me.Criteria = function () {
    return ViewModel.ROSynergyVenueDateGroupListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROSynergyVenueDateGroupList();
  }
  Me.Manager = function () {
    return ViewModel.ROSynergyVenueDateGroupListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SelectedItems = function () {
    return ViewModel.ROSynergyVenueDateGroupListManager().SelectedItems();
  };
  Me.ClearSelection = function () {
    ViewModel.ROSynergyVenueDateGroupListManager().SelectedItems([]);
  };
  Me.Criteria = function () {
    return ViewModel.ROSynergyVenueDateGroupListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROSynergyVenueDateGroupList();
  }
  Me.Manager = function () {
    return ViewModel.ROSynergyVenueDateGroupListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.ShowModal = function () {
  }
  Me.HideModal = function () {
  }
  Me.Init = function (newOptions, managerOptions, customInit) {
    if (newOptions) {
      $.extend({}, localOptions, newOptions);
    }
    if (managerOptions) {
      ViewModel.ROSynergyVenueDateGroupListManager().SetOptions(managerOptions);
    }
    if (customInit) {
      customInit();
    }
  };
  return Me;
}

//PageManager------------------------------------------------------------
var PageManagerClient = function (ModalID, Title, PagingManager, List, Criteria, options) {
  var Me = this;
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    PagingManager().Refresh();
  }
  Me.OnItemSelected = function (Item) {
    //options.OnItemSelected(Item);
    var n = new SelectedItemObject()
    n.ID(Item.RoomID())
    n.Description(Item.Room())
    return n
  }
  Me.ItemIsSelectedCss = function (RORoomPaged) {
    var Found = PagingManager().SelectedItems().Find("ID", RORoomPaged.RoomID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (RORoomPaged) {
    var Found = PagingManager().SelectedItems().Find("ID", RORoomPaged.RoomID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    //Setup Production Type Paged Grid
    PagingManager().SetOptions({
      AfterRowSelected: function (RORoomPaged) {
        options.AfterRowSelected(RORoomPaged)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem)
      }
    });

    $(ModalID).dialog({
      title: Title,
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) {
          options.beforeOpen();
        }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $(ModalID).dialog('destroy');
  }
  return Me;
}

//ROProductionsBudgetedList----------------------------------------------
var ProductionsBudgetedPagingManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList()
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ProductionsBudgetedPagingManager().Refresh();
  };
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject()
    n.ID(Item.ProductionID())
    n.Description(Item.ProductionDescription())
    return n
  }
  Me.ItemIsSelectedCss = function (ROProductionBudgeted) {
    var Found = ViewModel.ProductionsBudgetedPagingManager().SelectedItems().Find("ID", ROProductionBudgeted.ProductionID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProductionBudgeted) {
    var Found = ViewModel.ProductionsBudgetedPagingManager().SelectedItems().Find("ID", ROProductionBudgeted.ProductionID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }
  Me.ShowModal = function () {

    $("#ROProductionBudgetedModal").dialog({
      title: "Select Out Of Budget Production",
      closeText: "Close",
      height: 720,
      width: 1024,
      modal: true,
      resizeable: false,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROProductionBudgetedModal").dialog('destroy');
  }
  Me.Criteria = function () {
    return ViewModel.ProductionsBudgetedListCriteria();
  };
  Me.List = function () {
    return ViewModel.ProductionsBudgetedList();
  }
  Me.Manager = function () {
    return ViewModel.ProductionsBudgetedPagingManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  Me.Init = function () {
    //Setup Production Type Paged Grid
    Me.Manager().SetOptions({
      AfterRowSelected: function (ROProductionBudgeted) {
        localOptions.AfterRowSelected(ROProductionBudgeted)
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        localOptions.AfterRowDeSelected(DeSelectedItem)
      }
    });
  };
  return Me;
}

//ROCityPagedListManager------------------------------------------------------
var ROCityPagedListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
   clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList();
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROCityListManager().Refresh();
  }
  Me.ItemClicked = function (ROCity) {
    //ROCountry.SelectedInd(!ROCountry.SelectedInd());
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject();
    n.ID(Item.CityID());
    n.Description(Item.City());
    return n;
  }
  Me.ItemIsSelectedCss = function (ROCity) {
    var Found = ViewModel.ROCityListManager().SelectedItems().Find("ID", ROCity.CityID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROCity) {
    var Found = ViewModel.ROCityListManager().SelectedItems().Find("ID", ROCity.CityID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }

  Me.ShowModal = function () {
    ViewModel.ROCityListManager().Refresh();
    $("#CityModal").dialog({
      title: "Select City",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#CityModal").dialog('destroy');
  };
  Me.Criteria = function () {
    return ViewModel.ROCityListCriteria();
  };
  Me.List = function () {
    return ViewModel.ROCityList();
  }
  Me.Init = function () {
    ViewModel.ROCityListManager().MultiSelect(false);
    ViewModel.ROCityListManager().SingleSelect(true);
    ViewModel.ROCityListManager().SetOptions({
      AfterRowSelected: function (ROCity) {
        options.AfterRowSelected(ROCity);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem);
      }
    });
    $("#CityModal").dialog('destroy');
  };
  Me.Manager = function () {
    return ViewModel.ROCityListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}

//ROProvincePagedListManager------------------------------------------------------
var ROProvincePagedListManager = function (options) {
  var Me = this;
  var localOptions;
  localOptions = $.extend({}, localOptions, options);
  Me.DelayedRefreshList = function () {
    clearTimeout(fetchTimeout);// clear any running timeout
    fetchTimeout = setTimeout(function () {
      Me.RefreshList();
    }, DELAY); // create a new timeout
  }
  Me.RefreshList = function () {
    ViewModel.ROProvincePagedListManager().Refresh();
  }
  Me.ItemClicked = function (ROProvince) {
    //ROCountry.SelectedInd(!ROCountry.SelectedInd());
  }
  Me.OnItemSelected = function (Item) {
    var n = new SelectedItemObject();
    n.ID(Item.ProvinceID());
    n.Description(Item.ProvinceID());
    return n;
  }
  Me.ItemIsSelectedCss = function (ROProvince) {
    var Found = ViewModel.ROProvincePagedListManager().SelectedItems().Find("ID", ROProvince.ProvinceID());
    if (Found) {
      return "btn btn-xs btn-primary"
    } else {
      return "btn btn-xs btn-default"
    }
  }
  Me.ItemIsSelectedHtml = function (ROProvince) {
    var Found = ViewModel.ROProvincePagedListManager().SelectedItems().Find("ID", ROProvince.ProvinceID());
    if (Found) {
      return "<span class='fa fa-check-square-o'><span>"
    } else {
      return "<span class='fa fa-times'><span>"
    }
  }

  Me.ShowModal = function () {
    ViewModel.ROProvincePagedListManager().Refresh();
    $("#ROProvinceModal").dialog({
      title: "Select City",
      closeText: "Close",
      height: 800,
      width: 1024,
      modal: true,
      resizeable: true,
      open: function (event, ui) {
        if (options.beforeOpen) { options.beforeOpen() }
        Me.RefreshList();
      },
      beforeClose: function (event, ui) {

      }
    })

  }
  Me.HideModal = function () {
    $("#ROProvinceModal").dialog('destroy');
  };
  //Me.Criteria = function () {
  //  return ViewModel.ROCityListCriteria();
  //};
  //Me.List = function () {
  //  return ViewModel.ROCityList();
  //}
  Me.Init = function () {
    ViewModel.ROProvincePagedListManager().MultiSelect(false);
    ViewModel.ROProvincePagedListManager().SingleSelect(true);
    ViewModel.ROProvincePagedListManager().SetOptions({
      AfterRowSelected: function (ROProvince) {
        options.AfterRowSelected(ROProvince);
      },
      AfterRowDeSelected: function (DeSelectedItem) {
        options.AfterRowDeSelected(DeSelectedItem);
      }
    });
    //$("#ROProvinceModal").dialog('destroy');
  };
  Me.Manager = function () {
    return ViewModel.ROProvincePagedListManager()
  };
  Me.TotalRecords = function () {
    return Me.Manager().TotalRecords();
  };
  Me.SetOptions = function (options) {
    Me.Manager().SetOptions(options);
  };
  return Me;
}





