﻿window.SiteHub = $.connection.siteHub;

window.SiteHub.client.ReceiveNotifications = function (serialisedNotifications) {
  window.siteHubManager.processNotifications(serialisedNotifications)
};

window.SiteHub.client.ReceiveChanges = function (stringifiedChanges) {
  if (window.Scheduler) {
    window.Scheduler.processSignalRChanges(stringifiedChanges)
  };
  if (window.scheduler) {
    window.scheduler.processSignalRChanges(stringifiedChanges)
  };
  if (window.FindOBPage) {
    window.FindOBPage.processSignalRChanges(stringifiedChanges)
  };
  if (window.currentBookingAssigner) {
    window.currentBookingAssigner.processSignalRChanges(stringifiedChanges);
  };
  if (window.allocator) {
    window.allocator.processSignalRChanges(stringifiedChanges)
  };
};

window.SiteHub.client.ReceiveUpdatedBookings = function (stringifiedChanges) {
  //if (window.Scheduler) {
  //  window.Scheduler.processSignalRChanges(stringifiedChanges)
  //};
  //if (window.scheduler) {
  //  window.scheduler.processSignalRChanges(stringifiedChanges)
  //};
  //if (window.FindOBPage) {
  //  window.FindOBPage.processSignalRChanges(stringifiedChanges)
  //};
  //if (window.allocator) {
  //  window.allocator.processSignalRChanges(stringifiedChanges)
  //};
  //if (window.currentBookingAssigner) {
  //  window.currentBookingAssigner.processSignalRChanges(stringifiedChanges);
  //};
};

window.SiteHub.client.RegisteredWithSiteHub = function () {
  let scheduler = (window.Scheduler || window.scheduler)
  if (scheduler) {
    var userData = {
      ConnectionID: $.connection.hub.id,
      Username: ViewModel.CurrentUserName().toString(),
      UserID: ViewModel.CurrentUserID().toString(),
      ResourceSchedulerID: scheduler.schedulerData.ResourceSchedulerID.toString()
    }
    window.siteHubManager.registerWithResourceScheduler(userData)
  }
  else if (window.allocator) {
    var userData = {
      ConnectionID: $.connection.hub.id,
      Username: ViewModel.CurrentUserName().toString(),
      UserID: ViewModel.CurrentUserID().toString()
    }
    window.SiteHub.server.registerWithSynergyImporter(JSON.stringify(userData))
  }
  else if (window.location.href.indexOf("Production.aspx") >= 0) {
    if (ViewModel.CurrentProduction() && ViewModel.CurrentProduction().ProductionSystemAreaList().length == 1) {
      if (!ViewModel.CurrentProduction().ProductionSystemAreaList()[0].IsBeingEditedBySomeoneElse()) {
        window.siteHubManager.bookingIntoEdit(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ResourceBookingID())
      }
    }
  }
};

//resource scheduler
window.SiteHub.client.RegisteredWithResourceScheduler = function () {
  let scheduler = (window.Scheduler || window.scheduler)
  if (window.Scheduler) {
    window.Scheduler.create()
  }
  else if (window.scheduler) {
    window.scheduler.afterConnected()
  }
  else {
    window.Scheduler.afterRegistered()
  }
};

window.SiteHub.client.ReceiveConnectedSchedulers = function (schedulersDataString) {
  const schedulersData = JSON.parse(schedulersDataString)
  if (window.Scheduler) {
    window.Scheduler.addOnlineUsers(schedulersData)
  } else if (window.scheduler) {
    window.scheduler.addOnlineUsers(schedulersData);
  }
};

window.SiteHub.client.SchedulerConnected = function (schedulersDataString) {
  const schedulersData = JSON.parse(schedulersDataString);
  if (window.Scheduler) {
    window.Scheduler.addOnlineUser(schedulersData);
  } else if (window.scheduler) {
    window.scheduler.addOnlineUser(schedulersData);
  }
};

window.SiteHub.client.SchedulerDisconnected = function (connectionID) {
  if (window.Scheduler) {
    window.Scheduler.removeOfflineUser(connectionID)
  } else if (window.scheduler) {
    window.scheduler.removeOfflineUser(connectionID);
  }
};

window.SiteHub.client.ReceiveUpdatedTimesheetStats = function (serialisedResult) {
  if (window.Scheduler) {
    window.Scheduler.processUpdatedTimesheetStats(serialisedResult)
  }
};

//Importer
window.SiteHub.client.RegisteredWithSynergyImporter = function () {
  if (window.allocator) {
    window.allocator.afterRegistered()
  }
};

window.SiteHub.client.ReceiveOnlineSynergyUsers = function (schedulersDataString) {
  if (window.allocator) {
    var schedulersData = JSON.parse(schedulersDataString)
    window.allocator.addOnlineUsers(schedulersData)
  }
};

window.SiteHub.client.ReceiveSynergyChanges = function (synergyChangeSummaryString) {
  if (window.allocator) {
    var synergyChangeSummaryData = JSON.parse(synergyChangeSummaryString)
    window.allocator.allocateSynergyChanges(synergyChangeSummaryData)
  }
};

window.SiteHub.client.SynergyImporterUserConnected = function (schedulersDataString) {
  if (window.Scheduler) {
    var schedulersData = JSON.parse(schedulersDataString)
    window.Scheduler.addOnlineUsers(schedulersData)
  }
};

window.SiteHub.client.SynergyImporterUserConnectedDisconnected = function (connectionID) {
  if (window.Scheduler) {
    window.Scheduler.removeOfflineUser(connectionID)
  }
};

//ProductionSystemArea
window.SiteHub.client.ReceiveRoomScheduleImported = function (roomScheduleData) {
  if (window.allocator) {
    window.allocator.onImportedRoomScheduleReceived(roomScheduleData)
  }
};

window.SiteHub.client.ReceiveRoomScheduleAreaRemoved = function (roomScheduleAreaData) {
  if (window.allocator) {
    roomScheduleAreaData = JSON.parse(roomScheduleAreaData)
    window.allocator.onRoomScheduleRemovedReceived(roomScheduleAreaData)
  }
};

window.SiteHub.client.ReceiveChangesOB = function (stringifiedChanges) {
  if (ROProduction) {
    ROProduction.processSignalRChanges(stringifiedChanges)
  }
};



SiteHubManager = function (options) {
  var me = this
  var defaultOptions = {
    onConnectedToHub: function () { }
  }

  me.options = $.extend({}, defaultOptions, options)
  $.connection.hub.url = window.SiteInfo.SitePath + "/signalr"
  $.connection.hub.disconnected(me.reconnecToHub)
  me.startHub()
};

SiteHubManager.prototype.startHub = function () {
  var me = this
  if (window.SiteInfo.IsInternalSite || window.SiteInfo.IsQASite) {
    $.connection.hub.start({ transport: 'serverSentEvents' },
      me.registerWithHub)
      .fail(me.hubFailed)
  }
  else {
    $.connection.hub.start().done(me.registerWithHub)
      .fail(me.hubFailed)
  }
};

SiteHubManager.prototype.reconnecToHub = function () {
  if (window.SiteInfo.IsInternalSite || window.SiteInfo.IsQASite) {
    $.connection.hub.start({ transport: 'serverSentEvents' })
  }
  else {
    $.connection.hub.start()
  }
}

SiteHubManager.prototype.registerWithHub = function () {
  var userData = {
    ConnectionID: $.connection.hub.id,
    Username: ViewModel.CurrentUserName().toString(),
    UserID: ViewModel.CurrentUserID().toString()
  }
  window.SiteHub.server.registerWithSiteHub(JSON.stringify(userData)).done(function () {
    window.siteHubManager.getMyNotifications()
  })
};

SiteHubManager.prototype.registerWithResourceScheduler = function (userData) {
  window.SiteHub.server.registerWithResourceScheduler(JSON.stringify(userData))
};

SiteHubManager.prototype.hubFailed = function () {
  var me = this
  setTimeout(function () {
    console.log('reconnecting')
    window.siteHubManager.reconnecToHub()
  }, 10000); // Restart connection after 10 second/s.
};

SiteHubManager.prototype.bookingIntoEdit = function (resourceBookingID) {
  window.SiteHub.server.setEdit('{' +
    '"ResourceBookingID"' + ':"' + resourceBookingID.toString() + '",' +
    '"Username"' + ':"' + ViewModel.CurrentUserName().toString() + '",' +
    '"UserID"' + ':"' + ViewModel.CurrentUserID().toString() + '",' +
    '}');
};

SiteHubManager.prototype.bookingOutOfEdit = function (resourceBookingID) {
  window.SiteHub.server.releaseEdit('{' +
    '"ResourceBookingID"' + ':"' + resourceBookingID.toString() + '",' +
    '"Username"' + ':"' + ViewModel.CurrentUserName().toString() + '",' +
    '"UserID"' + ':"' + ViewModel.CurrentUserID().toString() + '",' +
    '}');
};

SiteHubManager.prototype.updateHumanResourceStats = function (humanResourceID, startDate) {
  window.SiteHub.server.updateHumanResourceStats('{' +
    '"HumanResourceID"' + ':"' + humanResourceID.toString() + '",' +
    '"BookingDate"' + ':"' + '\/Date(' + moment(startDate).valueOf() + ')\/' + '",' +
    '"UserID"' + ':"' + ViewModel.CurrentUserID().toString() + '"' +
    '}');
  //""+date.valueOf()+""
  //new Date(startDate).format('dd MMMM yyyy')
};

SiteHubManager.prototype.notifyHumanResourceStatsUpdate = function (shift) {
  window.SiteHub.server.notifyHumanResourceStatsUpdate('{' +
    '"HumanResourceID"' + ':"' + shift.HumanResourceID().toString() + '",' +
    '"BookingDate"' + ':"' + shift.StartDateTime().toString() + '",' +
    '"UserID"' + ':"' + ViewModel.CurrentUserID().toString() + '"' +
    '}');
};

SiteHubManager.prototype.sendNotifications = function () {
  window.SiteHub.server.sendNotifications();
};

SiteHubManager.prototype.getMyNotifications = function () {
  window.SiteHub.server.getNotifications(ViewModel.CurrentUserID());
};

SiteHubManager.prototype.showMyNotificationsModal = function () {
  //console.log(event)
  $("#UserNotificationsModal").off('shown.bs.modal')
  $("#UserNotificationsModal").on('shown.bs.modal', function () {
    ROUserNotificationListCriteriaBO.refresh(ViewModel.ROUserNotificationListCriteria());
  })
  $("#UserNotificationsModal").modal()
};

SiteHubManager.prototype.receiveNotifications = function (serialisedNotifications) {

};

SiteHubManager.prototype.processNotifications = function (serialisedNotifications) {
  var me = this;
  //variables
  var notificationSummaryList = JSON.parse(serialisedNotifications)
  var activityNode = document.getElementById("userNotifications")
  var activityNodeUL = document.getElementById("userNotificationsUL")
  var notificationCountNode = activityNode.querySelector("span.bubble")
  //cleanup
  $(activityNodeUL).off('click')
  $(activityNodeUL).children().remove()
  //repopulate
  notificationSummaryList.Iterate(function (usr, uIndex) {
    if (usr.UserID == ViewModel.CurrentUserID()) {
      usr.ROUNSummaryNotificationList.Iterate(function (usrN, unIndex) {
        //list item
        var newNode = document.createElement('li')
        newNode.setAttribute('title', usrN.NotificationTextLong)
        newNode.businessObject = usrN
        //anchor
        var newNodeAnchor = document.createElement('a')
        newNodeAnchor.setAttribute('href', '#')
        //anchor icon
        var newNodeAnchorIcon = document.createElement('i')
        newNodeAnchorIcon.setAttribute('class', 'fa fa-frown-o info')
        //title (bold)
        var newNodeAnchorB = document.createElement('b')
        newNodeAnchorB.innerHTML = usrN.NotificationTitle
        //title (normal)
        var newNodeAnchorS = document.createElement('span')
        newNodeAnchorS.innerHTML = usrN.NotificationTitle
        //ago
        var newNodeAnchorD = document.createElement('span')
        newNodeAnchorD.setAttribute('class', 'date')
        newNodeAnchorD.innerHTML = usrN.Ago
        //append
        newNodeAnchor.appendChild(newNodeAnchorIcon)
        newNodeAnchor.appendChild(newNodeAnchorB)
        newNodeAnchor.innerHTML += usrN.NotificationTextShort
        newNodeAnchor.appendChild(newNodeAnchorD)
        newNode.appendChild(newNodeAnchor)
        activityNodeUL.appendChild(newNode)
      })
      //update
      notificationCountNode.innerHTML = usr.TotalUnReadCount.toString()
    }
  })
  //re-add events
  $(activityNodeUL).on('click', 'li', function (e) {
    switch (this.businessObject.NotificationTypeID) {
      case 1:
        me.showContentServicesDifferences(this.businessObject)
        break;
      case 2:
        me.downloadSynergyChangesWithBookingInfo(this, this.businessObject, null)
        break;
    }
  })
};

SiteHubManager.prototype.showContentServicesDifferences = function (ROUserNotification) {
  $("#ContentServicesDifferencesModal").off('shown.bs.modal')
  $("#ContentServicesDifferencesModal").off('hidden.bs.modal')
  if (ViewModel.ContentServicesDifferenceList == undefined) {
    $.gritter.add({
      title: 'Unauthorized',
      text: 'This page is not allowed to display booking differences',
      class_name: 'warning',
      time: 500,
      position: 'top-right',
      sticky: false
    })
  } else {
    $("#ContentServicesDifferencesModal").on('shown.bs.modal', function () {
      ViewModel.ContentServicesDifferenceList.Set([])
      ContentServicesDifferenceBO.get({
        criteria: {
          StartDate: ViewModel.ContentServicesDifferenceListCriteria().StartDate() //we can get away with this kind of sin right here as this object is only used in a special case
        },
        onSuccess: function (response) {
          ViewModel.ContentServicesDifferenceList.Set(response.Data)
        },
        onFail: function (response) {
          OBMisc.Notifications.GritterError("Error", response.ErrorText, 500)
        }
      })
    })
    $("#ContentServicesDifferencesModal").on('hidden.bs.modal', function () {
      ViewModel.ContentServicesDifferenceList.Set([])
      Singular.Validation.CheckRules(ViewModel)
    })
    $("#ContentServicesDifferencesModal").modal()
  }
};

SiteHubManager.prototype.downloadSynergyChangesWithBookingInfo = function (element, ROUserNotification, ROUserNotificationKO) {
  //if (ROUserNotificationKO) { ROUserNotification = ROUserNotificationKO.Serialise() }
  var synergyImportID = (ROUserNotificationKO ? ROUserNotificationKO.SynergyImportID() : ROUserNotification.SynergyImportID)
  if (synergyImportID) {
    ViewModel.CallServerMethod("DownloadSynergyChangesWithBookingInfo", {
      SynergyImportID: synergyImportID
    }, function (response) {
      if (response.Success) {
        Singular.DownloadFile(null, response.Data);
      } else {
        OBMisc.Notifications.GritterError('Download Failed', response.ErrorText, 1000)
      }
    })
  } else {
    OBMisc.Notifications.GritterError('Download Failed', 'Import not provided', 1000)
  }
};

SiteHubManager.prototype.changeBookingStatusesOld = function (resourceBookingItems, newStatusID) {
  let requests = []
  resourceBookingItems.forEach((rb, rbIndx) => {
    const nr = {
      ResourceBookingID: rb.Booking.ResourceBookingID,
      ResourceBookingTypeID: rb.Booking.ResourceBookingTypeID,
      ProductionSystemAreaID: rb.Booking.ProductionSystemAreaID,
      NewStatusID: newStatusID,
      UserID: ViewModel.CurrentUserID(),
      UserName: ViewModel.CurrentUserName()
    }
    requests.push(nr)
  })
  const requestsString = JSON.stringify(requests);
  window.SiteHub.server.changeStatuses(requestsString)
};

SiteHubManager.prototype.changeBookingStatuses = function (resourceBookings, newStatusID) {
  let requests = []
  resourceBookings.forEach((rb, rbIndx) => {
    const nr = {
      ResourceBookingID: rb.businessObject.ResourceBookingID,
      ResourceBookingTypeID: rb.businessObject.ResourceBookingTypeID,
      ProductionSystemAreaID: rb.businessObject.ProductionSystemAreaID,
      NewStatusID: newStatusID,
      UserID: ViewModel.CurrentUserID(),
      UserName: ViewModel.CurrentUserName()
    }
    requests.push(nr)
  })
  const requestsString = JSON.stringify(requests);
  window.SiteHub.server.changeStatuses(requestsString)
};

SiteHubManager.prototype.changeMade = function () {
  window.SiteHub.server.changeMade();
};

SiteHubManager.prototype.bookingUpdated = function () {
  window.SiteHub.server.bookingUpdated();
};

SiteHubManager.prototype.roomScheduleImported = function (importedRoomScheduleData) {
  var requestString = JSON.stringify(importedRoomScheduleData);
  window.SiteHub.server.sendRoomScheduleImported($.connection.hub.id, requestString)
};

SiteHubManager.prototype.roomScheduleAreaRemoved = function (roomScheduleAreaData) {
  var requestString = JSON.stringify(roomScheduleAreaData);
  window.SiteHub.server.sendRoomScheduleAreaRemoved($.connection.hub.id, requestString)
};

SiteHubManager.prototype.getConnectedSchedulers = function (resourceSchedulerID) {
  window.SiteHub.server.getConnectedSchedulers(resourceSchedulerID.toString());
};




