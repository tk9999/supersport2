﻿//#region Production
var ft

ProductionControl = function (options) {
  var me = this
  me.options = {}
  var defaultOptions = {
    modalID: "CurrentProductionModal",
    productionIstanceName: "CurrentProduction",
    onSaveSuccess: function (control, response) {

    },
    onSaveFailed: function (control, response) {

    },
    onShown: function () {

    },
    afterEditFinished: function (production) {

    },
    onClosed: function () {

    }
  }
  me.options = $.extend({}, defaultOptions, options)

  me.productionKO = function () {
    if (ViewModel) {
      return ViewModel[me.options.productionIstanceName];
    }
    return null
  }

};

ProductionControl.prototype.setup = function () {
  var me = this

  ViewModel.ROProductionVenueListManager().SingleSelect(true)
  ViewModel.ROEventTypePagedListManager().SingleSelect(true)

  $("#" + me.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" });
    me.options.onShown()
  })

  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.options.onClosed()
  })

};

ProductionControl.prototype.production = function () {
  var me = this
  //if (ViewModel) { return ViewModel[me.options.productionIstanceName]() }
  return null
};

ProductionControl.prototype.currentTitle = function () {
  var me = this
  if (me.production()) {
    return me.production().Title()
  };
  return "Room Booking"
};

ProductionControl.prototype.setModalPosition = function (ModalID, activatingElement) {
  var elementInfo = activatingElement.getBoundingClientRect();
  var isInInputGroup = $(activatingElement).parents('.input-group');
  var positionInfo = null;
  if (isInInputGroup.length == 1) {
    positionInfo = isInInputGroup[0].getBoundingClientRect();
  };
  var width = elementInfo.left - positionInfo.left;
  $(ModalID)[0].style.left = positionInfo.left - width + "px";
  $(ModalID)[0].style.top = positionInfo.top + 10 + "px";
};

ProductionControl.prototype.save = function (production) {
  var me = this
  if (production.IsDirty()) {
    if (!production.IsProcessing()) {
      me.production().IsProcessing(true)
      //production.IsProcessing(true)
      ViewModel.CallServerMethod("SaveCurrentProduction", {
        productionToSave: KOFormatterFull.Serialise(production)
      },
        function (response) {
          if (response.Success) {
            me.setProduction(response.Data)
            me.options.onSaveSuccess(me, response)
            me.hideProductionModal()
          }
          else {
            me.options.onSaveFailed(me, response)
          }
          me.production().IsProcessing(false)
        })
    }
  } else {
    me.hideProductionModal()
  }
};

ProductionControl.prototype.getProduction = function (productionID, genRefNumber, callback) {
  var me = this
  Singular.GetDataStateless("OBLib.Productions.Base.ProductionList",
    {
      ProductionID: productionID,
      GenRefNumber: genRefNumber
    },
    function (response) {
      if (response.Success) {
        if (callback) { callback(response) }
      }
      else {
        OBMisc.Notifications.GritterError('Error Processing', response.ErrorText, 1000)
      }
    })
}

ProductionControl.prototype.setProduction = function (newValue) {
  var me = this
}

ProductionControl.prototype.showModal = function () {
  var me = this;
  $("#" + this.options.modalID).modal()
};

ProductionControl.prototype.hideModal = function (production) {
  $("#" + this.options.modalID).modal('hide')
};

ProductionControl.prototype.selectGenreSeries = function (roomSchedule, element) {
  this.setModalPosition("#ROEventTypeSelectModal", element)

  $("#ROEventTypeSelectModal").off('shown.bs.modal')

  $("#ROEventTypeSelectModal").on('shown.bs.modal', function () {
    $("#ROEventTypeSelectModal").draggable({
      handle: ".modal-header"
    })
    ViewModel.ROEventTypePagedListManager().Refresh()
  })

  $("#ROEventTypeSelectModal").modal()

};

ProductionControl.prototype.clearGenreSeries = function (roomSchedule, element) {
  this.production().ProductionTypeID(null);
  this.production().EventTypeID(null);
  this.production().ProductionTypeEventType("");
};

ProductionControl.prototype.onGenreSeriesSelected = function (Item) {
  //custom stuff
  this.production().ProductionTypeID(Item.ProductionTypeID());
  this.production().EventTypeID(Item.EventTypeID());
  this.production().ProductionTypeEventType(Item.ProductionType() + ' (' + Item.EventType() + ')');

  //library stuff
  var n = new SelectedItemObject();
  n.ID(Item.EventTypeID());
  n.Description(Item.EventType());
  return n

  $("#ROEventTypeSelectModal").modal('hide');
};

ProductionControl.prototype.isGenreSeriesSelectedCss = function (GenreSeries) {
  var prd = this.production();
  if (prd) {
    if (prd.EventTypeID() == GenreSeries.EventTypeID()) {
      return "btn btn-xs btn-primary"
    }
  }
  return "btn btn-xs btn-default"
};

ProductionControl.prototype.isGenreSeriesSelectedHtml = function (GenreSeries) {
  var prd = this.production();
  if (prd) {
    if (prd.EventTypeID() == GenreSeries.EventTypeID()) {
      return "<span class='fa fa-check-square-o'><span>"
    }
  }
  return "<span class='fa fa-times'><span>"
};

ProductionControl.prototype.refreshGenreSeries = function (GenreSeries) {
  clearTimeout(ft)
  ft = setTimeout(function () {
    ViewModel.ROEventTypePagedListManager().Refresh()
  }, 250)
};

ProductionControl.prototype.delayedRefreshGenreSeries = function (GenreSeries) {
  clearTimeout(ft)
  ft = setTimeout(function () {
    ViewModel.ROEventTypePagedListManager().Refresh()
  }, 250)
};

ProductionControl.prototype.selectProductionVenue = function (roomSchedule, element) {
  this.setModalPosition("#ROProductionVenueSelectModal", element)

  $("#ROProductionVenueSelectModal").off('shown.bs.modal')

  $("#ROProductionVenueSelectModal").on('shown.bs.modal', function () {
    $("#ROProductionVenueSelectModal").draggable({
      handle: ".modal-header"
    })
    ViewModel.ROProductionVenueListManager().Refresh()
  })

  $("#ROProductionVenueSelectModal").modal()

};

ProductionControl.prototype.clearProductionVenue = function (roomSchedule, element) {
  if (this.production()) {
    this.production().ProductionVenueID(null);
    this.production().ProductionVenue("");
  }
};

ProductionControl.prototype.onProductionVenueSelected = function (Item) {
  //custom stuff
  ViewModel.CurrentRoomSchedule().ProductionList()[0].ProductionVenueID(Item.ProductionVenueID());
  ViewModel.CurrentRoomSchedule().ProductionList()[0].ProductionVenue(Item.ProductionVenue());

  //library stuff
  var n = new SelectedItemObject();
  n.ID(Item.ProductionVenueID());
  n.Description(Item.ProductionVenue());
  $("#ROProductionVenueSelectModal").modal('hide');
  return n
};

ProductionControl.prototype.isProductionVenueSelectedCss = function (ProductionVenue) {
  var prd = this.production();
  if (prd) {
    if (prd.ProductionVenueID() == ProductionVenue.ProductionVenueID()) {
      return "btn btn-xs btn-primary"
    }
  }
  return "btn btn-xs btn-default"
};

ProductionControl.prototype.isProductionVenueSelectedHtml = function (ProductionVenue) {
  var prd = this.production();
  if (prd) {
    if (prd.ProductionVenueID() == ProductionVenue.ProductionVenueID()) {
      return "<span class='fa fa-check-square-o'><span>"
    }
  }
  return "<span class='fa fa-times'><span>";
};

ProductionControl.prototype.refreshProductionVenue = function (ProductionVenue) {

};

ProductionControl.prototype.delayedRefreshProductionVenue = function (ProductionVenue) {

};

ProductionEditorCtrl = function (options) {
  var me = this
  me.options = {}
  var defaultOptions = {
    modalID: "ProductionModalEdit",
    productionIstanceName: "CurrentProduction",
    onSaveSuccess: function (control, response) {

    },
    onSaveFailed: function (control, response) {

    },
    onShown: function () {

    },
    onClosed: function () {

    }
  }
  me.options = $.extend({}, defaultOptions, options)

  me.setup()

};

ProductionEditorCtrl.prototype.setup = function () {
  var me = this

  $("#" + me.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" });
    me.options.onShown()
  })

  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.options.onClosed()
  })

};

ProductionEditorCtrl.prototype.currentProduction = function () {
  var me = this
  if (ViewModel) { return ViewModel[me.options.productionIstanceName]() }
  return null
};

ProductionEditorCtrl.prototype.currentProductionKO = function () {
  var me = this
  if (ViewModel) { return ViewModel[me.options.productionIstanceName] }
  return null
};

ProductionEditorCtrl.prototype.currentTitle = function () {
  var me = this
  if (me.currentProduction()) {
    return me.currentProduction().Title()
  };
  return "Production"
};

ProductionEditorCtrl.prototype.save = function () {
  var me = this
  ProductionBO.save(me.currentProduction(),
    function (response) {
      me.setProduction(response.Data)
      me.options.onSaveSuccess(me, response)
      me.hideModal()
    },
    function (response) {

    })
};

ProductionEditorCtrl.prototype.getProduction = function (productionID, IsOBView, IsRoomView, onSuccess, onFailed) {
  var me = this
  ProductionBO.getProduction(productionID, IsOBView, IsRoomView,
    function (response) {
      me.setProduction(response.Data[0])
      onSuccess(response)
    },
    function (response) {
      onFailed(response)
    })
}

ProductionEditorCtrl.prototype.setProduction = function (newValue) {
  var me = this
  me.currentProductionKO().Set(newValue)
}

ProductionEditorCtrl.prototype.showModal = function () {
  var me = this;
  $("#" + this.options.modalID).modal()
};

ProductionEditorCtrl.prototype.hideModal = function () {
  $("#" + this.options.modalID).modal('hide')
};

//#endregion

//#region AdHoc

AdHocBookingControl = function (options) {
  var me = this
  me.options = {}
  var defaultOptions = {
    modalID: "CurrentAdHocModal",
    instanceName: "CurrentAdHocBooking",
    onSaveSuccess: function (control, response) {

    },
    onSaveFailed: function (control, response) {

    },
    onShown: function () {

    },
    onClosed: function () {

    }
  }
  me.options = $.extend({}, defaultOptions, options)

  me.setInstance = function (newValue) {
    var me = this
    ViewModel[me.options.instanceName]['Set'](newValue)
  }
  me.instanceKO = function () {
    if (ViewModel) {
      return ViewModel[me.options.instanceName];
    }
    return null
  }

};

AdHocBookingControl.prototype.setup = function () {
  var me = this

  //ViewModel.ROProductionVenueListManager().SingleSelect(true)
  //ViewModel.ROEventTypePagedListManager().SingleSelect(true)

  $("#" + me.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" });
    me.options.onShown()
  })

  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.options.onClosed()
  })

};

AdHocBookingControl.prototype.adHocBooking = function () {
  var me = this
  //if (ViewModel) { return ViewModel[me.options.productionIstanceName]() }
  return null
};

AdHocBookingControl.prototype.currentInstance = function () {
  var me = this
  if (ViewModel) { return ViewModel[me.options.instanceName]() }
  return null
};

AdHocBookingControl.prototype.getAdHocBooking = function (adHocBookingID, callback) {
  var me = this
  Singular.GetDataStateless("OBLib.AdHoc.AdHocBookingList",
    {
      AdHocBookingID: adHocBookingID,
      AdHocBookingOnly: true
    },
    function (response) {
      if (response.Success) {
        if (callback) { callback(response) }
      }
      else {
        OBMisc.Notifications.GritterError('Error Processing', response.ErrorText, 1000)
      }
    })
}

AdHocBookingControl.prototype.setAdHocBooking = function (newValue) {
  var me = this
  ViewModel[me.options.instanceName]['Set'](newValue)
}

AdHocBookingControl.prototype.currentTitle = function () {
  var me = this
  if (me.currentInstance()) {
    return me.currentInstance().Title()
  };
  return "Room Booking"
};

AdHocBookingControl.prototype.setModalPosition = function (ModalID, activatingElement) {
  var elementInfo = activatingElement.getBoundingClientRect();
  var isInInputGroup = $(activatingElement).parents('.input-group');
  var positionInfo = null;
  if (isInInputGroup.length == 1) {
    positionInfo = isInInputGroup[0].getBoundingClientRect();
  };
  var width = elementInfo.left - positionInfo.left;
  $(ModalID)[0].style.left = positionInfo.left - width + "px";
  $(ModalID)[0].style.top = positionInfo.top + 10 + "px";
};

AdHocBookingControl.prototype.save = function (currentInstance) {
  //var me = this
  //if (currentInstance.IsDirty()) {
  //  if (!currentInstance.IsProcessing()) {
  //    me.currentInstance().IsProcessing(true)
  //    ViewModel.CallServerMethod("SaveRSAdHocBooking", {
  //      rsAdhocBooking: KOFormatterFull.Serialise(production)
  //    },
  //    function (response) {
  //      if (response.Success) {
  //        me.setInstance(response.Data)
  //        me.options.onSaveSuccess(me, response)
  //        me.hideModal()
  //      }
  //      else {
  //        me.options.onSaveFailed(me, response)
  //      }
  //      me.currentInstance().IsProcessing(false)
  //    })
  //  }
  //}
  //else {
  //  me.hideModal()
  //}
};

AdHocBookingControl.prototype.getInstance = function (adHocBookingID) {
  //var me = this
  //Singular.GetDataStateless("OBLib.Productions.Base.ProductionList",
  //  {
  //    AdHocBookingID: adHocBookingID
  //  },
  //  function (response) {
  //    if (response.Success) {
  //      me.setInstance(response.Data[0])
  //    }
  //    else {
  //      console.log(response)
  //    }
  //  })
}

AdHocBookingControl.prototype.showModal = function () {
  var me = this;
  $("#" + this.options.modalID).modal()
};

AdHocBookingControl.prototype.hideModal = function (production) {
  $("#" + this.options.modalID).modal('hide')
};

//#endregion

//#region Shifts

PlayoutOpsShiftModal = function (options) {
  var me = this
  me.options = {}

  var defaultOptions = {
    modalID: "CurrentPlayoutOpsShiftModal",
    shiftInstanceName: 'CurrentPlayoutOpsShift',
    onModalOpened: function () { },
    onModalHidden: function () { },
    onSaveSuccess: function (response) { },
    onSaveFailed: function (response) { },
    afterSaveSuccess: function (ctrl, response) {
      OBMisc.Notifications.GritterSuccess("Shift Allocated", "Saved", 1500, 'top-right');
    },
  }
  me.options = $.extend({}, defaultOptions, options)

  //on show
  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
    if (me.currentShift()) { Singular.Validation.CheckRules(me.currentShift()) }
    me.options.onModalOpened.call(me)
  })

  //on hide
  $("#" + me.options.modalID).off('hidden.bs.modal')
  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.setCurrentShift(null)
    me.options.onModalHidden.call(me)
  })

};

PlayoutOpsShiftModal.prototype.currentShift = function () {
  if (ViewModel) {
    return ViewModel[this.options.shiftInstanceName]();
  };
  return null;
};

PlayoutOpsShiftModal.prototype.setCurrentShift = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

PlayoutOpsShiftModal.prototype.clearCurrentShift = function () {
  ViewModel[this.options.shiftInstanceName](null);
};

PlayoutOpsShiftModal.prototype.newShiftResourceScheduler = function (groupItem, event) {
  var me = this;
  me.setCurrentShift(null)
  var newShift = new PlayoutOperationsShiftObject()
  newShift.SystemID(5)
  newShift.ProductionAreaID(null)
  newShift.DisciplineID(null)
  newShift.ShiftTypeID(null)
  var bookingStartTime = event.data.eventProps.time; //.format('dd MMM yyyy HH:mm')
  var bookingEndTime = moment(event.data.eventProps.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
  newShift.StartDateTime(new Date(bookingStartTime))
  newShift.EndDateTime(new Date(bookingEndTime))
  newShift.ProductionAreaStatusID(1)
  newShift.HumanResourceID(groupItem.Resource.HumanResourceID)
  newShift.HumanResource(groupItem.Resource.ResourceName)
  newShift.ResourceID(groupItem.Resource.ResourceID)
  me.setCurrentShift(newShift)
  Singular.Validation.CheckRules(newShift)
  me.showShiftModal()
};

PlayoutOpsShiftModal.prototype.setCurrentShiftStateless = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

PlayoutOpsShiftModal.prototype.editShiftSchedulerScreen = function (resourceBooking, onSuccess) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    me.showShiftModal()
    if (onSuccess) { onSuccess(response) }
  })
};

PlayoutOpsShiftModal.prototype.editShift = function (humanResourceShiftID, onFetchSuccess) {
  var me = this;
  PlayoutOperationsShiftBO.get(humanResourceShiftID,
    function (response) {
      me.setCurrentShift(response.Data.Shift)
      onFetchSuccess.call(me, response)
    },
    function (response) { })
};

PlayoutOpsShiftModal.prototype.editShiftResourceScheduler = function (resourceBooking, afterFetch) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    this.showShiftModal()
  })
};

PlayoutOpsShiftModal.prototype.canSave = function (humanResourceShift) {
  var me = this
  if (humanResourceShift.IsValid() && !humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

PlayoutOpsShiftModal.prototype.saveShift = function (humanResourceShift) {
  var me = this;
  PlayoutOperationsShiftBO.save(humanResourceShift,
    function (response) {
      me.setCurrentShift(response.Data.Shift)
      me.hideShiftModal()
      //window.SiteHubManager.updateHumanResourceStats(response.Data);
      if (me.options.onSaveSuccess) { me.options.onSaveSuccess.call(me, response) }
    },
    function (response) {
      if (me.options.onSaveFailed) { me.options.onSaveFailed.call(me, response) }
    })
};

PlayoutOpsShiftModal.prototype.saveShiftStateless = function (humanResourceShift) {
  var me = this;
  ViewModel.CallServerMethod("SavePlayoutOpsShiftStateless", {
    PlayoutShift: humanResourceShift.Serialise()
  }, function (response) {
    if (response.Success) {
      me.setCurrentShiftStateless(response.Data)
      me.clearCurrentShift()
      me.hideShiftModal()
      //window.SiteHubManager.updateHumanResourceStats(response.Data);
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Modals.Error('Error During Save', 'An error occured while saving the Shift', response.ErrorText, null)
    }
  });
};


PlayoutOpsShiftModal.prototype.setAndShowShiftModal = function (shiftData) {
  var me = this;
  me.setCurrentShift(shiftData)
  me.showShiftModal()
};

PlayoutOpsShiftModal.prototype.showShiftModal = function () {
  var me = this;
  $("#" + this.options.modalID).modal()
};

PlayoutOpsShiftModal.prototype.hideShiftModal = function () {
  var me = this;
  $("#" + this.options.modalID).modal('hide')
};

PlayoutOpsShiftModal.prototype.showClashes = function (humanResourceShift) {

};


PlayoutOpsShiftModalMCR = function (options) {
  var me = this
  me.options = {}

  var defaultOptions = {
    modalID: "CurrentPlayoutOpsShiftModalMCR",
    shiftInstanceName: 'CurrentPlayoutOpsShiftMCR',
    onModalOpened: function () { },
    onModalHidden: function () { },
    onSaveSuccess: function (response) { },
    onSaveFailed: function (response) { },
    afterSaveSuccess: function (ctrl, response) {
      OBMisc.Notifications.GritterSuccess("Shift Allocated", "Saved", 1500, 'top-right');
    },
  }
  me.options = $.extend({}, defaultOptions, options)

  //on show
  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
    if (me.currentShift()) {
      Singular.Validation.CheckRules(me.currentShift())
    }
    me.options.onModalOpened.call(me)
  })

  //on hide
  $("#" + me.options.modalID).off('hidden.bs.modal')
  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.setCurrentShift(null)
    me.options.onModalHidden.call(me)
  })

};

PlayoutOpsShiftModalMCR.prototype.setCurrentShiftStateless = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

PlayoutOpsShiftModalMCR.prototype.currentShift = function () {
  if (ViewModel) {
    return ViewModel[this.options.shiftInstanceName]();
  };
  return null;
};

PlayoutOpsShiftModalMCR.prototype.setCurrentShift = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

PlayoutOpsShiftModalMCR.prototype.clearCurrentShift = function () {
  ViewModel[this.options.shiftInstanceName](null);
};

PlayoutOpsShiftModalMCR.prototype.newShiftResourceScheduler = function (groupItem, event) {
  var me = this;
  me.setCurrentShift(null)
  var newShift = new MCRShiftObject()
  newShift.SystemID(5)
  newShift.ProductionAreaID(null)
  newShift.DisciplineID(null)
  newShift.ShiftTypeID(null)
  var bookingStartTime = event.data.eventProps.time; //.format('dd MMM yyyy HH:mm')
  var bookingEndTime = moment(event.data.eventProps.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
  newShift.StartDateTime(new Date(bookingStartTime))
  newShift.EndDateTime(new Date(bookingEndTime))
  newShift.ProductionAreaStatusID(1)
  newShift.HumanResourceID(groupItem.Resource.HumanResourceID)
  newShift.HumanResource(groupItem.Resource.ResourceName)
  newShift.ResourceID(groupItem.Resource.ResourceID)
  me.setCurrentShift(newShift)
  Singular.Validation.CheckRules(newShift)
  me.showShiftModal()
};

PlayoutOpsShiftModalMCR.prototype.saveShiftStateless = function (humanResourceShift) {
  var me = this;
  ViewModel.CallServerMethod("SavePlayoutOpsShiftMCRStateless", {
    PlayoutShift: humanResourceShift.Serialise()
  }, function (response) {
    if (response.Success) {
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      me.setCurrentShiftStateless(response.Data)
      me.clearCurrentShift()
      me.hideShiftModal()
      window.siteHubManager.updateHumanResourceTimesheet(response.Data)
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Modals.Error('Error During Save', 'An error occured while saving the Shift', response.ErrorText, null)
    }
  });
};

PlayoutOpsShiftModalMCR.prototype.editShiftSchedulerScreen = function (resourceBooking, onSuccess) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    me.showShiftModal()
    if (onSuccess) { onSuccess(response) }
  })
};

PlayoutOpsShiftModalMCR.prototype.editShift = function (humanResourceShiftID, onFetchSuccess) {
  var me = this;
  MCRShiftBO.get(humanResourceShiftID,
    function (response) {
      me.setCurrentShift(response.Data.Shift)
      MCRShiftBO.refreshROChannelList(me.currentShift())
      onFetchSuccess.call(me, response)
    },
    function (response) { })
};

PlayoutOpsShiftModalMCR.prototype.editShiftResourceScheduler = function (resourceBooking, afterFetch) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    this.showShiftModal()
  })
};

PlayoutOpsShiftModalMCR.prototype.canSave = function (humanResourceShift) {
  var me = this
  if (humanResourceShift.IsValid() && !humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

PlayoutOpsShiftModalMCR.prototype.saveShift = function (humanResourceShift) {
  var me = this;
  PlayoutOperationsShiftBO.save(humanResourceShift,
    function (response) {
      me.setCurrentShift(response.Data.Shift)
      me.hideShiftModal()
      //window.SiteHubManager.updateHumanResourceStats(response.Data);
      if (me.options.onSaveSuccess) { me.options.onSaveSuccess.call(me, response) }
    },
    function (response) {
      if (me.options.onSaveFailed) { me.options.onSaveFailed.call(me, response) }
    })
};

PlayoutOpsShiftModalMCR.prototype.setAndShowShiftModal = function (shiftData) {
  var me = this;
  me.setCurrentShift(shiftData)
  me.showShiftModal()
};

PlayoutOpsShiftModalMCR.prototype.showShiftModal = function () {
  var me = this;

  $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
  $("#" + this.options.modalID).modal()
};

PlayoutOpsShiftModalMCR.prototype.hideShiftModal = function () {
  var me = this;
  $("#" + this.options.modalID).modal('hide')
};

PlayoutOpsShiftModalMCR.prototype.showClashes = function (humanResourceShift) {

};


StudioSupervisorShiftModal = function (options) {
  var me = this
  me.options = {}

  var defaultOptions = {
    modalID: "CurrentStudioSupervisorShiftModal",
    shiftInstanceName: 'CurrentStudioSupervisorShift',
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  }

  me.options = $.extend({}, defaultOptions, options)
  me.generalTimeout = null
  me.timeoutDelay = 200

  me.currentHumanResourceData = null

};

StudioSupervisorShiftModal.prototype.currentShift = function () {
  if (ViewModel) {
    return ViewModel[this.options.shiftInstanceName]();
  };
  return null;
};

StudioSupervisorShiftModal.prototype.setCurrentShift = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

StudioSupervisorShiftModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentShift().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
      //me.hideShiftModal()
    }
  })
};

StudioSupervisorShiftModal.prototype.newShiftResourceScheduler = function (groupItem, event) {
  var me = this;
  me.setCurrentShift(null)
  var newShift = new StudioSupervisorShiftObject()
  newShift.SystemID(1)
  newShift.ProductionAreaID(2)
  newShift.DisciplineID(7)
  newShift.ShiftTypeID(19)
  var bookingStartTime = event.data.eventProps.time; //.format('dd MMM yyyy HH:mm')
  var bookingEndTime = moment(event.data.eventProps.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
  newShift.StartDateTime(new Date(bookingStartTime))
  newShift.EndDateTime(new Date(bookingEndTime))
  newShift.ProductionAreaStatusID(1)
  me.setCurrentShift(newShift)
  me.showShiftModal()
};

StudioSupervisorShiftModal.prototype.clearCurrentShift = function () {
  ViewModel[this.options.shiftInstanceName](null);
};

StudioSupervisorShiftModal.prototype.editShiftSchedulerScreen = function (resourceBooking, onSuccess, onFail) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    if (response.Success) {
      me.showShiftModal()
      onSuccess(response)
    }
    else {
      onFail(response)
    }
  })
};

StudioSupervisorShiftModal.prototype.editShift = function (humanResourceShiftID, onFetchSuccess) {
  var me = this
  ViewModel.CallServerMethod("EditStudioSupervisorShift", {
    HumanResourceShiftID: humanResourceShiftID
  }, function (response) {
    if (response.Success) {
      me.setCurrentShift(response.Data.Shift)
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

StudioSupervisorShiftModal.prototype.cancelShiftModal = function (humanResourceShift) {
  var me = this
  if (me.currentShift().IsNew()) {
    me.clearCurrentShift()
  };
  this.hideShiftModal()
};

StudioSupervisorShiftModal.prototype.canSave = function (humanResourceShift) {
  var me = this
  if (humanResourceShift.IsValid() && !humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

StudioSupervisorShiftModal.prototype.canCancel = function (humanResourceShift) {
  var me = this
  if (!humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

StudioSupervisorShiftModal.prototype.saveShift = function (humanResourceShift) {
  var me = this;
  humanResourceShift.IsProcessing(true)
  ViewModel.CallServerMethod("SaveStudioSupervisorShift", {
    StudioSupervisorShift: humanResourceShift.Serialise()
  }, function (response) {
    if (response.Success) {
      me.clearCurrentShift()
      me.hideShiftModal()
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
    }
    humanResourceShift.IsProcessing(false)
  });
};

StudioSupervisorShiftModal.prototype.setShiftBusy = function () {
  var me = this;
  me.currentShift().IsProcessing(true)
};

StudioSupervisorShiftModal.prototype.setShiftNotBusy = function () {
  var me = this;
  me.currentShift().IsProcessing(false)
};

StudioSupervisorShiftModal.prototype.showShiftModal = function () {
  var me = this;

  $("#" + me.options.modalID).off('shown.bs.modal')
  $("#" + me.options.modalID).off('hidden.bs.modal')

  $("#" + me.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
    if (me.currentShift()) {
      HumanResourceShiftBaseBO.checkClashes(me.currentShift())
      Singular.Validation.CheckRules(me.currentShift())
    }
    if (me.options.onModalOpened) { me.options.onModalOpened(me) }
  })
  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentShift();
  })

  $("#" + this.options.modalID).modal()
};

StudioSupervisorShiftModal.prototype.hideShiftModal = function () {
  $("#" + this.options.modalID).modal('hide');
};


ICRShiftModal = function (options) {
  var me = this
  me.options = {}

  var defaultOptions = {
    modalID: "CurrentICRShiftModal",
    shiftInstanceName: 'CurrentICRShift',
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  }

  me.options = $.extend({}, defaultOptions, options)
  me.generalTimeout = null
  me.timeoutDelay = 200

  me.currentHumanResourceData = null

};

ICRShiftModal.prototype.currentShift = function () {
  if (ViewModel) {
    return ViewModel[this.options.shiftInstanceName]();
  };
  return null;
};

ICRShiftModal.prototype.setCurrentShift = function (newValue) {
  ViewModel[this.options.shiftInstanceName](newValue);
};

ICRShiftModal.prototype.setCurrentShiftStateless = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

ICRShiftModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentShift().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
      //me.hideShiftModal()
    }
  })
};

ICRShiftModal.prototype.newShiftResourceScheduler = function (groupItem, event) {
  var me = this;
  me.setCurrentShift(null)
  var newShift = new ICRShiftObject()
  newShift.SystemID(4)
  newShift.ProductionAreaID(null)
  newShift.DisciplineID(null)
  newShift.ShiftTypeID(null)
  newShift.ResourceID(groupItem.Resource.ResourceID)
  newShift.HumanResourceID(groupItem.Resource.HumanResourceID)
  newShift.HumanResource(groupItem.Resource.ResourceName)
  var bookingStartTime = event.data.eventProps.time; //.format('dd MMM yyyy HH:mm')
  var bookingEndTime = moment(event.data.eventProps.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
  newShift.StartDateTime(new Date(bookingStartTime))
  newShift.EndDateTime(new Date(bookingEndTime))
  newShift.ProductionAreaStatusID(1)
  me.setCurrentShift(newShift)
  me.showShiftModal()
};

ICRShiftModal.prototype.clearCurrentShift = function () {
  ViewModel[this.options.shiftInstanceName](null);
};

ICRShiftModal.prototype.editShiftSchedulerScreenStateless = function (resourceBooking, afterFetch) {
  var me = this;
  me.editShiftStateless(resourceBooking.HumanResourceShiftID, function (response) {
    me.setCurrentShiftStateless(response.Data.Shift)
    me.showShiftModal()
    if (afterFetch) { afterFetch(response) }
  })
};

ICRShiftModal.prototype.editShiftStateless = function (humanResourceShiftID, onFetchSuccess) {
  ViewModel.CallServerMethod("EditICRShiftServerMethod", {
    HumanResourceShiftID: humanResourceShiftID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

ICRShiftModal.prototype.cancelShiftModal = function (humanResourceShift) {
  var me = this
  if (me.currentShift().IsNew()) {
    me.clearCurrentShift()
  };
  this.hideShiftModal()
};

ICRShiftModal.prototype.canSave = function (humanResourceShift) {
  var me = this
  if (humanResourceShift.IsValid() && !humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

ICRShiftModal.prototype.canCancel = function (humanResourceShift) {
  var me = this
  if (!humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

ICRShiftModal.prototype.saveShift = function (humanResourceShift) {
  var me = this;
  Singular.SendCommand("SaveICRShift", {}, function (response) {
    if (response.Success) {
      me.clearCurrentShift()
      me.hideShiftModal()
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
    }
  });
};

ICRShiftModal.prototype.saveShiftStateless = function (humanResourceShift) {
  var me = this;
  ViewModel.CallServerMethod("SaveICRShift", {
    ICRShift: humanResourceShift.Serialise()
  }, function (response) {
    if (response.Success) {
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      me.setCurrentShiftStateless(response.Data)
      me.clearCurrentShift()
      me.hideShiftModal()
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
    }
  });
};

ICRShiftModal.prototype.setAndShowShiftModal = function (shiftData) {
  var me = this;
  me.setCurrentShiftStateless(shiftData)
  me.showShiftModal()
};

ICRShiftModal.prototype.showShiftModal = function () {
  var me = this;
  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
    if (me.currentShift()) { Singular.Validation.CheckRules(me.currentShift()) }
    if (me.options.onModalOpened) { me.options.onModalOpened(me) }
  })
  $("#" + this.options.modalID).modal();
};

ICRShiftModal.prototype.hideShiftModal = function () {
  var me = this;
  $("#" + me.options.modalID).off('hidden.bs.modal')
  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentShift()
  })
  $("#" + this.options.modalID).modal('hide')
};

ICRShiftModal.prototype.showClashes = function (humanResourceShift) {

};


StageHandShiftModal = function (options) {
  var me = this
  me.options = {}

  var defaultOptions = {
    modalID: "CurrentStageHandShiftModal",
    shiftInstanceName: 'CurrentStageHandShift',
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  }

  me.options = $.extend({}, defaultOptions, options)
  me.generalTimeout = null
  me.timeoutDelay = 200

  me.currentHumanResourceData = null

};

StageHandShiftModal.prototype.currentShift = function () {
  if (ViewModel) {
    return ViewModel[this.options.shiftInstanceName]();
  };
  return null;
};

StageHandShiftModal.prototype.setCurrentShift = function (newValue) {
  ViewModel[this.options.shiftInstanceName](newValue);
};

StageHandShiftModal.prototype.setCurrentShiftStateless = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

StageHandShiftModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentShift().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
      //me.hideShiftModal()
    }
  })
};

StageHandShiftModal.prototype.newShiftResourceScheduler = function (groupItem, event) {
  var me = this;
  me.setCurrentShift(null)
  var newShift = new StageHandShiftObject()
  newShift.SystemID(1)
  newShift.ProductionAreaID(2)
  newShift.DisciplineID(296)
  newShift.Discipline("Stage Hand")
  newShift.ShiftTypeID(24)
  //newShift.ShiftType("StageHand Shift")
  newShift.HumanResourceID(groupItem.Resource.HumanResourceID)
  newShift.ResourceID(groupItem.Resource.ResourceID)
  newShift.HumanResource(groupItem.Resource.ResourceName)
  var bookingStartTime = event.data.eventProps.time; //.format('dd MMM yyyy HH:mm')
  var bookingEndTime = moment(event.data.eventProps.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
  newShift.StartDateTime(new Date(bookingStartTime))
  newShift.EndDateTime(new Date(bookingEndTime))
  newShift.ProductionAreaStatusID(1)
  me.setCurrentShift(newShift)
  me.showShiftModal()
};

StageHandShiftModal.prototype.clearCurrentShift = function () {
  ViewModel[this.options.shiftInstanceName](null);
};

StageHandShiftModal.prototype.editShiftSchedulerScreen = function (resourceBooking) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    me.showShiftModal()
  })
};

StageHandShiftModal.prototype.editShift = function (humanResourceShiftID, onFetchSuccess) {
  Singular.SendCommand("EditStageHandShift", {
    HumanResourceShiftID: humanResourceShiftID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

StageHandShiftModal.prototype.editShiftSchedulerScreenStateless = function (resourceBooking, afterFetch) {
  var me = this;
  me.editShiftStateless(resourceBooking.HumanResourceShiftID, function (response) {
    me.setCurrentShiftStateless(response.Data.Shift)
    me.showShiftModal()
    if (afterFetch) { afterFetch(response) }
  })
};

StageHandShiftModal.prototype.editShiftStateless = function (humanResourceShiftID, onFetchSuccess) {
  ViewModel.CallServerMethod("EditStageHandShiftServerMethod", {
    HumanResourceShiftID: humanResourceShiftID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

StageHandShiftModal.prototype.cancelShiftModal = function (humanResourceShift) {
  var me = this
  if (me.currentShift().IsNew()) {
    me.clearCurrentShift()
  };
  this.hideShiftModal()
};

StageHandShiftModal.prototype.canSave = function (humanResourceShift) {
  var me = this
  if (humanResourceShift.IsValid() && !humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

StageHandShiftModal.prototype.canCancel = function (humanResourceShift) {
  var me = this
  if (!humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

StageHandShiftModal.prototype.saveShift = function (humanResourceShift) {
  var me = this;
  Singular.SendCommand("SaveStageHandShift", {}, function (response) {
    if (response.Success) {
      me.clearCurrentShift()
      me.hideShiftModal()
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
    }
  });
};

StageHandShiftModal.prototype.saveShiftStateless = function (humanResourceShift) {
  var me = this;
  ViewModel.CallServerMethod("SaveStageHandShiftStateless", {
    StageHandShift: humanResourceShift.Serialise()
  }, function (response) {
    if (response.Success) {
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      me.setCurrentShiftStateless(response.Data)
      me.clearCurrentShift()
      me.hideShiftModal()
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
    }
  });
};

StageHandShiftModal.prototype.setAndShowShiftModal = function (shiftData) {
  var me = this;
  me.setCurrentShiftStateless(shiftData)
  me.showShiftModal()
};

StageHandShiftModal.prototype.showShiftModal = function () {
  var me = this;
  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
    if (me.currentShift()) { Singular.Validation.CheckRules(me.currentShift()) }
    if (me.options.onModalOpened) { me.options.onModalOpened(me) }
  })
  $("#" + this.options.modalID).modal();
};

StageHandShiftModal.prototype.hideShiftModal = function () {
  var me = this;
  $("#" + me.options.modalID).off('hidden.bs.modal')
  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentShift()
  })
  $("#" + this.options.modalID).modal('hide')
};

StageHandShiftModal.prototype.showClashes = function (humanResourceShift) {

};


ContentShiftModal = function (options) {
  var me = this
  me.options = {}

  var defaultOptions = {
    modalID: "CurrentContentShiftModal",
    shiftInstanceName: 'CurrentContentShift',
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  }
  me.options = $.extend({}, defaultOptions, options)

};

ContentShiftModal.prototype.currentShift = function () {
  if (ViewModel) {
    return ViewModel[this.options.shiftInstanceName]();
  };
  return null;
};

ContentShiftModal.prototype.setCurrentShift = function (newValue) {
  ViewModel[this.options.shiftInstanceName](newValue);
};

ContentShiftModal.prototype.setCurrentShiftStateless = function (newValue) {
  ViewModel[this.options.shiftInstanceName].Set(newValue);
};

ContentShiftModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        //me.hideShiftModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentShift().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
      //me.hideShiftModal()
    }
  })
};

ContentShiftModal.prototype.newShiftResourceScheduler = function (groupItem, event) {
  var me = this;
  me.setCurrentShift(null)
  var newShift = new ContentShiftObject()
  newShift.SystemID(groupItem.Resource.OwningSystemID)
  newShift.ProductionAreaID(2)
  newShift.Discipline("")
  newShift.DisciplineID(null)
  newShift.ShiftType("")
  newShift.ShiftTypeID(null)
  newShift.HumanResourceID(groupItem.Resource.HumanResourceID)
  newShift.ResourceID(groupItem.Resource.ResourceID)
  newShift.HumanResource(groupItem.Resource.ResourceName)
  var bookingStartTime = null,
    bookingEndTime = null;
  if (groupItem.Resource.OwningSystemID == 1) {
    bookingStartTime = new Date(event.data.eventProps.time).setHours(12, 0, 0, 0); //.format('dd MMM yyyy HH:mm')
    bookingEndTime = new Date(bookingStartTime).setHours(23, 59, 59, 0); //.format('dd MMM yyyy HH:mm')
  } else {
    bookingStartTime = event.data.eventProps.time; //.format('dd MMM yyyy HH:mm')
    bookingEndTime = moment(event.data.eventProps.time).add(12, 'hours').toDate() //.format('dd MMM yyyy HH:mm')
  }
  newShift.StartDateTime(new Date(bookingStartTime))
  newShift.EndDateTime(new Date(bookingEndTime))
  newShift.ProductionAreaStatusID(1)
  me.setCurrentShift(newShift)
  Singular.Validation.CheckRules(newShift)
  me.showShiftModal()
};

ContentShiftModal.prototype.clearCurrentShift = function () {
  ViewModel[this.options.shiftInstanceName](null);
};

ContentShiftModal.prototype.editShiftSchedulerScreen = function (resourceBooking) {
  var me = this;
  me.editShift(resourceBooking.HumanResourceShiftID, function (response) {
    me.showShiftModal()
  })
};

ContentShiftModal.prototype.editShift = function (humanResourceShiftID, onFetchSuccess) {
  Singular.SendCommand("EditContentShift", {
    HumanResourceShiftID: humanResourceShiftID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

ContentShiftModal.prototype.editShiftSchedulerScreenStateless = function (resourceBooking, afterFetch) {
  var me = this;
  me.editShiftStateless(resourceBooking.HumanResourceShiftID, function (response) {
    me.setCurrentShiftStateless(response.Data.Shift)
    me.showShiftModal()
    if (afterFetch) { afterFetch(response) }
  })
};

ContentShiftModal.prototype.editShiftStateless = function (humanResourceShiftID, onFetchSuccess) {
  ViewModel.CallServerMethod("EditGenericShift", {
    HumanResourceShiftID: humanResourceShiftID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

ContentShiftModal.prototype.cancelShiftModal = function (humanResourceShift) {
  var me = this
  if (me.currentShift().IsNew()) {
    me.clearCurrentShift()
  };
  this.hideShiftModal()
};

ContentShiftModal.prototype.canSave = function (humanResourceShift) {
  var me = this
  if (humanResourceShift.IsValid() && !humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

ContentShiftModal.prototype.canCancel = function (humanResourceShift) {
  var me = this
  if (!humanResourceShift.IsProcessing()) {
    return true
  }
  return false
};

ContentShiftModal.prototype.saveShiftStateless = function (humanResourceShift) {
  var me = this;
  ViewModel.CallServerMethod("SaveContentShiftStateless", {
    ContentShift: humanResourceShift.Serialise()
  }, function (response) {
    if (response.Success) {
      OBMisc.Notifications.GritterSuccess('Saved Successfully', '', 250)
      me.setCurrentShiftStateless(response.Data)
      me.clearCurrentShift()
      me.hideShiftModal()
      //window.siteHubManager.changeMade()
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000)
    }
  });
};

ContentShiftModal.prototype.setAndShowShiftModal = function (shiftData) {
  var me = this;
  me.setCurrentShiftStateless(shiftData)
  me.showShiftModal()
};

ContentShiftModal.prototype.showShiftModal = function () {
  var me = this;
  //----remove old handlers----
  $("#" + this.options.modalID + ' a[data-toggle="tab"]').off('shown.bs.tab')
  $("#" + this.options.modalID).off('shown.bs.modal')
  //----add handlers----
  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    //add draggability
    $("#" + me.options.modalID).draggable({ handle: ".modal-header" })
    //roomschedule criteria object
    me.currentShift().RoomScheduleCriteria(new ContentShiftRoomScheduleList_CriteriaObject())
    me.currentShift().RoomScheduleCriteria().HumanResourceShiftID((me.currentShift().HumanResourceShiftID()))
    me.currentShift().RoomScheduleCriteria().StartDateTime((me.currentShift().StartDateTime()))
    me.currentShift().RoomScheduleCriteria().EndDateTime((me.currentShift().EndDateTime()))
    me.currentShift().RoomScheduleCriteria().SystemID((me.currentShift().SystemID()))
    me.currentShift().RoomScheduleCriteria().ProductionAreaID((me.currentShift().ProductionAreaID()))
    //check rules
    if (me.currentShift()) { Singular.Validation.CheckRules(me.currentShift()) }
    //on modal opened event handler
    if (me.options.onModalOpened) { me.options.onModalOpened(me) }
    $("#" + me.options.modalID + " a[href='#ShiftRoomSchedules']").tab("show")
  })
  //on tab changed handler
  $("#" + this.options.modalID + ' a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
    me.onTabChanged(target)
  })
  //show the modal
  $("#" + this.options.modalID).modal();
};

ContentShiftModal.prototype.onTabChanged = function (activatedTab) {

};

ContentShiftModal.prototype.hideShiftModal = function () {
  var me = this;
  $("#" + me.options.modalID).off('hidden.bs.modal')
  $("#" + me.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentShift()
  })
  $("#" + this.options.modalID).modal('hide')
};

//#endregion

//#region Off Periods

HROffPeriodModal = function (options) {
  var me = this;
  me.options = {};
  var defaultOptions = {
    modalID: "CurrentHumanResourceOffPeriodModal",
    offPeriodInstanceName: 'CurrentHumanResourceOffPeriod',
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  };
  me.options = $.extend({}, defaultOptions, options);
  me.generalTimeout = null;
  me.timeoutDelay = 200;

  me.currentHumanResourceData = null;

};

HROffPeriodModal.prototype.currentOffPeriod = function () {
  if (ViewModel) {
    return ViewModel[this.options.offPeriodInstanceName]();
  };
  return null;
};

HROffPeriodModal.prototype.currentROOffPeriod = function () {
  return ViewModel.CurrentROHumanResourceOffPeriod()
};

HROffPeriodModal.prototype.setCurrentOffPeriod = function (newValue) {
  ViewModel[this.options.offPeriodInstanceName](newValue);
};

HROffPeriodModal.prototype.setCurrentROOffPeriod = function (newValue) {
  ViewModel.CurrentROHumanResourceOffPeriod(newValue)
};

HROffPeriodModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        me.hideOffPeriodModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        me.hideOffPeriodModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentOffPeriod().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
      me.hideOffPeriodModal()
    }
  })
};

HROffPeriodModal.prototype.clearCurrentOffPeriod = function () {
  ViewModel[this.options.offPeriodInstanceName](null);
};

HROffPeriodModal.prototype.newOffPeriodFromHRScreen = function (humanResourceID) {
  var me = this
  ROHumanResourceDetailBO.getItem({
    criteria: {
      HumanResourceID: humanResourceID
    },
    onSuccess: function (response) {
      me.setCurrentOffPeriod(new HumanResourceOffPeriodObject())
      me.currentOffPeriod().IsFreelancer(this.IsFreelancer)
      me.currentOffPeriod().ManagerHumanResourceID(this.ManagerHumanResourceID)
      if (this.ManagerHumanResourceID == ViewModel.CurrentUserHumanResourceID()) {
        me.currentOffPeriod().CanAuthoriseLeave(true)
      }
      else {
        me.currentOffPeriod().CanAuthoriseLeave(false)
      }
      me.currentOffPeriod().SystemID(this.PrimarySystemID)
      me.currentOffPeriod().ProductionAreaID(this.PrimaryProductionAreaID)
      me.currentOffPeriod().HumanResourceID(this.HumanResourceID)
      me.currentOffPeriod().ResourceID(this.ResourceID)
      me.currentOffPeriod().AuthorisedID(1)
      me.showOffPeriodModal()
    }
  })
};

HROffPeriodModal.prototype.newOffPeriodResourceScheduler = function (groupItem) {
  var me = this;

  me.setCurrentOffPeriod(new HumanResourceOffPeriodObject())
  me.currentOffPeriod().IsProcessing(true)
  me.showOffPeriodModal()
  var gi = groupItem

  me.getHumanResourceDetails(gi.Resource.HumanResourceID, gi.Resource.ResourceName, function (humanResourceData) {
    //setup the OffPeriod
    var humanResource = humanResourceData
    me.currentOffPeriod().IsFreelancer(humanResource.IsFreelancer)
    me.currentOffPeriod().ManagerHumanResourceID(humanResource.ManagerHumanResourceID)
    me.currentOffPeriod().CanAuthoriseLeave(humanResource.CurrentUserCanAuthoriseLeave)
    me.currentOffPeriod().SystemID(humanResource.PrimarySystemID)
    me.currentOffPeriod().ProductionAreaID(humanResource.PrimaryProductionAreaID)
    me.currentOffPeriod().HumanResourceID(humanResource.HumanResourceID)
    me.currentOffPeriod().ResourceID(humanResource.ResourceID)
    me.currentOffPeriod().AuthorisedID(1)
    me.currentOffPeriod().IsProcessing(false)
  })

};

HROffPeriodModal.prototype.editOffPeriodHRScreen = function (ROOffPeriod) {
  var me = this;
  if (ROOffPeriod.IsAuthorised()) {
    ViewModel.CurrentROHumanResourceOffPeriod.Set(ROOffPeriod)
    me.showOffPeriodModal()
  } else {
    me.editOffPeriod(ROOffPeriod.HumanResourceOffPeriodID(), function (response) {
      me.showOffPeriodModal()
    })
  }
};

HROffPeriodModal.prototype.editOffPeriodResourceSchedulerScreen = function (item, group) {
  var me = this;
  me.editOffPeriod(item.Booking.HumanResourceOffPeriodID, function (response) {
    me.showOffPeriodModal()
  })
};

HROffPeriodModal.prototype.editOffPeriod = function (humanResourceOffPeriodID, onFetchSuccess) {
  Singular.SendCommand("EditOffPeriod", {
    HumanResourceOffPeriodID: humanResourceOffPeriodID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000);
    }
  })
};

HROffPeriodModal.prototype.isUnAuthorisedLeave = function () {
  if (this.currentOffPeriod().AuthorisedID()) {
    if (this.currentOffPeriod().AuthorisedID() == 2 && !this.currentOffPeriod().IsNew()) {
      return false;
    }
  }
  return true;
};

HROffPeriodModal.prototype.cancelOffPeriod = function (humanResourceOffPeriod) {
  if (ViewModel.CurrentHumanResourceOffPeriod().IsNew()) {
    ViewModel.CurrentHumanResourceOffPeriod(null);
  };
  this.hideOffPeriodModal();
};

HROffPeriodModal.prototype.save = function (humanResourceOffPeriod) {
  var me = this;
  Singular.SendCommand("SaveOffPeriod", {}, function (response) {
    if (response.Success) {
      me.setCurrentOffPeriod(null)
      me.hideOffPeriodModal()
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000);
    }
  });
};

HROffPeriodModal.prototype.saveAndNew = function (humanResourceOffPeriod) {
  var me = this;
  if (humanResourceOffPeriod.IsDirty()) {
    Singular.SendCommand("SaveOffPeriod", {}, function (response) {
      if (response.Success) {
        if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
        ViewModel.CurrentHumanResourceOffPeriod(null);
        ViewModel.CurrentHumanResourceOffPeriod(new HumanResourceOffPeriodObject());
        ViewModel.CurrentHumanResourceOffPeriod().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
        ViewModel.CurrentHumanResourceOffPeriod().ResourceID(ViewModel.CurrentHumanResource().ResourceID());
        ViewModel.CurrentHumanResourceOffPeriod().SystemID(ViewModel.CurrentHumanResource().PrimarySystemID());
        ViewModel.CurrentHumanResourceOffPeriod().ProductionAreaID(ViewModel.CurrentHumanResource().PrimaryProductionAreaID());
        Singular.Validation.CheckRules(ViewModel.CurrentHumanResourceOffPeriod());
      } else {
        OBMisc.Modals.Error('Error During Save', 'An error occured while saving the Off Period', response.ErrorText, null);
      }
    })
  } else {
    ViewModel.CurrentHumanResourceOffPeriod(null);
    ViewModel.CurrentHumanResourceOffPeriod(new HumanResourceOffPeriodObject());
    ViewModel.CurrentHumanResourceOffPeriod().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
    ViewModel.CurrentHumanResourceOffPeriod().ResourceID(ViewModel.CurrentHumanResource().ResourceID());
    Singular.Validation.CheckRules(ViewModel.CurrentHumanResourceOffPeriod());
  }
};

HROffPeriodModal.prototype.showOffPeriodModal = function () {
  var me = this;

  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).off('shown.bs.modal')

  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    if (me.currentOffPeriod()) { Singular.Validation.CheckRules(me.currentOffPeriod()) }
    $('a[href="#OffPeriod"]').tab('show')
    //if (me.options.onModalOpened) { me.options.onModalOpened(me) }
    //else { Singular.Validation.CheckRules(me.currentOffPeriod()) }
  });

  $("#" + this.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentOffPeriod();
    //Singular.Validation.CheckRules(me.currentOffPeriod());
  });

  $("#" + this.options.modalID).modal();
};

HROffPeriodModal.prototype.hideOffPeriodModal = function () {
  $("#" + this.options.modalID).modal('hide');
};


HRAvailabilityModal = function (options) {
  var me = this;
  me.options = {};
  var defaultOptions = {
    modalID: "CurrentHumanResourceUnAvailabilityModal",
    templateInstanceName: 'CurrentHumanResourceUnAvailabilityTemplate',
    onSaveSuccess: function (ctrl, response) { },
    onSaveFail: function (ctrl, response) { },
    onModalOpened: function () { },
    onModalClosed: function () { }
  };
  me.options = $.extend({}, defaultOptions, options);
  me.$modal = function () { return $("#" + this.options.modalID) }

  me.setupDatePicker = function () {
    me.detailDateSelector = $('#UnAvailableDays');
    me.detailDateSelector = $(me.detailDateSelector).datepicker({
      showButtonPanel: true,
      //minDate: minD,
      //maxDate: maxD,
      dateFormat: "ddd dd MMM yyyy",
      closeText: "Close",
      closeOnSelect: false,
      beforeShowDay: function (date) {
        var stringDate = date.format("ddd dd MMM yyyy");
        var isSelected = false //(me.currentCopySetting().DatesToCopyTo().indexOf(stringDate) >= 0 ? true : false)
        return [true, (isSelected ? "day-selected" : ""),]
      },
      onSelect: function (datetext, inst) {
        var d = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)
        var st = new Date(me.currentTemplate().StartDateTime())
        var et = new Date(me.currentTemplate().EndDateTime())
        //add new
        var ni = me.currentTemplate().HumanResourceUnAvailabilityList.AddNew()
        var day = parseInt(inst.selectedDay)
        ni.StartDate(new Date(inst.selectedYear, inst.selectedMonth, day, st.getHours(), st.getMinutes(), 0, 0))
        if (et < st) {
          ni.EndDate(new Date(inst.selectedYear, inst.selectedMonth, (day + 1), et.getHours(), et.getMinutes(), 0, 0))
        } else {
          ni.EndDate(new Date(inst.selectedYear, inst.selectedMonth, day, et.getHours(), et.getMinutes(), 0, 0))
        }
        ni.Detail(me.currentTemplate().Detail())
        ni.HumanResourceID(me.currentTemplate().HumanResourceID())
        return false;
      }
    })
    $(me.detailDateSelector).datepicker('show')
  }

};

HRAvailabilityModal.prototype.currentTemplate = function () {
  if (ViewModel) {
    return ViewModel[this.options.templateInstanceName]();
  };
  return null;
};

HRAvailabilityModal.prototype.setCurrentTemplate = function (newValue) {
  ViewModel[this.options.templateInstanceName](newValue);
};

HRAvailabilityModal.prototype.specifyUnAvailabilityHRScreen = function (humanResource) {
  this.setCurrentTemplate(new HumanResourceUnAvailabilityTemplateObject())
  this.currentTemplate().StartDateTime(new Date())
  this.currentTemplate().EndDateTime(new Date())
  this.currentTemplate().HumanResourceID(humanResource.HumanResourceID())
  this.show()
};

HRAvailabilityModal.prototype.saveAvailabilityTemplate = function (template) {
  var me = this;
  HumanResourceUnAvailabilityTemplateBO.saveItem(template, {
    onSuccess: function (response) {
      ViewModel.ROHumanResourceUnAvailabilityListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
      ViewModel.ROHumanResourceUnAvailabilityListManager().PageNo(1)
      ViewModel.ROHumanResourceUnAvailabilityListManager().Refresh()
      me.setCurrentTemplate(new HumanResourceUnAvailabilityTemplateObject())
      me.currentTemplate().HumanResourceUnAvailabilityList([])
      $('a[href="#FindAvailability"]').tab('show')
    },
    onFail: function (response) {

    }
  })
};

HRAvailabilityModal.prototype.show = function () {
  var me = this;

  $("#" + this.options.modalID).off('hide.bs.modal')
  $("#" + this.options.modalID).off('shown.bs.modal')

  $("#" + this.options.modalID).on('hide.bs.modal', function () {
    me.hide.call(me)
  })

  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    if (me.currentTemplate()) { Singular.Validation.CheckRules(me.currentTemplate()) }
    me.setupDatePicker()
    ViewModel.ROHumanResourceUnAvailabilityListCriteria().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
    ViewModel.ROHumanResourceUnAvailabilityListManager().PageNo(1)
    ViewModel.ROHumanResourceUnAvailabilityListManager().Refresh()
    $('a[href="#FindAvailability"]').tab('show')
    $("#" + me.options.modalID).modal();
  })

  $("#" + this.options.modalID).modal();
};

HRAvailabilityModal.prototype.hide = function () {
  var me = this;
  me.setCurrentTemplate(null)
  ViewModel.CurrentHumanResourceUnAvailabilityTemplate(null)
  Singular.Validation.CheckRules(ViewModel)
};

//#endregion

//#region Secondments

HRSecondmentModal = function (options) {
  var me = this;
  me.options = {};
  var defaultOptions = {
    modalID: "HRSecondmentsModal",
    secondmentInstanceName: 'CurrentHumanResourceSecondment',
    secondmentManagerName: "ROHumanResourceSecondmentListManager",
    secondmentCriteriaName: "ROHumanResourceSecondmentListCriteria",
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  };
  me.options = $.extend({}, defaultOptions, options);
  me.generalTimeout = null;
  me.timeoutDelay = 200;

  me.currentHumanResourceData = null;

  me.secondmentManagerKO = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentManagerName];
    }
    return null;
  };
  me.secondmentManager = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentManagerName]();
    }
    return null;
  };
  me.secondmentCriteriaKO = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentCriteriaName];
    }
    return null;
  };
  me.secondmentCriteria = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentCriteriaName]();
    }
    return null;
  };

};

HRSecondmentModal.prototype.setup = function () {
  var me = this;

  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    if (me.currentSecondment()) { Singular.Validation.CheckRules(me.currentSecondment()) }
  })

  $("#" + this.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentSecondment();
  })

};

HRSecondmentModal.prototype.currentSecondment = function () {
  if (ViewModel) {
    return ViewModel[this.options.secondmentInstanceName]();
  };
  return null;
};

HRSecondmentModal.prototype.setCurrentSecondment = function (newValue) {
  ViewModel[this.options.secondmentInstanceName](newValue);
};

HRSecondmentModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        me.hideOffPeriodModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
        me.hideOffPeriodModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentSecondment().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 1000)
      me.hideOffPeriodModal()
    }
  })
};

HRSecondmentModal.prototype.clearCurrentSecondment = function () {
  ViewModel[this.options.secondmentInstanceName](null);
};

HRSecondmentModal.prototype.newSecondmentHRScreen = function (humanResource) {
  ViewModel.CurrentHumanResourceSecondment(new HumanResourceSecondmentObject());
  ViewModel.CurrentHumanResourceSecondment().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
  ViewModel.CurrentHumanResourceSecondment().ResourceID(ViewModel.CurrentHumanResource().ResourceID());
  this.showSecondmentModal();
};

HRSecondmentModal.prototype.newSecondmentResourceScheduler = function (groupItem) {
  var me = this;
  me.setCurrentSecondment(new HumanResourceSecondmentObject())
  me.currentSecondment().IsProcessing(true)
  me.showSecondmentModal()
  var gi = groupItem
  me.getHumanResourceDetails(gi.Resource.HumanResourceID, gi.Resource.ResourceName, function (humanResourceData) {
    //setup the Secondment
    me.currentSecondment().HumanResourceID(humanResourceData.HumanResourceID)
    me.currentSecondment().ResourceID(humanResourceData.ResourceID)
    me.currentSecondment().IsProcessing(false)
  })
};

HRSecondmentModal.prototype.editSecondmentHRScreen = function (ROSecondment) {
  var me = this;
  me.editSecondment(ROSecondment.HumanResourceSecondmentID(), function (response) {
    me.showSecondmentModal()
  })
};

HRSecondmentModal.prototype.editSecondmentResourceSchedulerScreen = function (item, group) {
  var me = this;
  me.editSecondment(item.Booking.HumanResourceSecondmentID, function (response) {
    me.showSecondmentModal()
  })
};

HRSecondmentModal.prototype.editSecondment = function (humanResourceSecondmentID, onFetchSuccess) {
  Singular.SendCommand("EditSecondment", {
    HumanResourceSecondmentID: humanResourceSecondmentID
  }, function (response) {
    if (response.Success) {
      onFetchSuccess(response)
    } else {
      OBMisc.Notifications.GritterError('Error Fetching Secondment', response.ErrorText, 1000);
    }
  })
};

HRSecondmentModal.prototype.cancelSecondment = function (humanResourceSecondment) {
  if (ViewModel.CurrentHumanResourceSecondment().IsNew()) {
    ViewModel.CurrentHumanResourceSecondment(null);
  };
  this.hideSecondmentModal();
};

HRSecondmentModal.prototype.save = function (humanResourceSecondment) {
  var me = this;
  Singular.SendCommand("SaveSecondment", {}, function (response) {
    if (response.Success) {
      me.setCurrentSecondment(null)
      me.hideSecondmentModal()
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
      //      me.secondmentManager().Refresh();
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000);
    }
  });
};

HRSecondmentModal.prototype.saveAndNew = function (humanResourceSecondment) {
  var me = this;
  Singular.SendCommand("SaveSecondment", {}, function (response) {
    if (response.Success) {
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
      ViewModel.CurrentHumanResourceSecondment(null);
      ViewModel.CurrentHumanResourceSecondment(new HumanResourceSecondmentObject());
      ViewModel.CurrentHumanResourceSecondment().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
      ViewModel.CurrentHumanResourceSecondment().ResourceID(ViewModel.CurrentHumanResource().ResourceID());
      Singular.Validation.CheckRules(ViewModel.CurrentHumanResourceSecondment());
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000);
    }
  })
};

HRSecondmentModal.prototype.showSecondmentModal = function () {
  var me = this;

  //$("#" + this.options.secondmentModalID).on('shown.bs.modal', function () {
  //  Singular.Validation.CheckRules(ViewModel.CurrentHumanResourceSecondment());
  //});

  //$("#" + this.options.secondmentModalID).on('hidden.bs.modal', function () {

  //});

  $("#" + this.options.modalID).modal();

};

HRSecondmentModal.prototype.hideSecondmentModal = function () {
  $("#" + this.options.modalID).modal('hide');
};

HRSecondmentModal.prototype.selectSecondmentCountry = function (secondment, element) {
  ViewModel.CurrentSecondmentROCountryListManager().Refresh();
};

HRSecondmentModal.prototype.onSecondmentCountrySelected = function (ROCountry) {
  ViewModel.CurrentHumanResourceSecondment().CountryID(ROCountry.CountryID());
  ViewModel.CurrentHumanResourceSecondment().Country(ROCountry.Country());
  ROCountry.IsSelected(true);
};

HRSecondmentModal.prototype.selectSecondmentDebtor = function (secondment, element) {
  ViewModel.CurrentSecondmentRODebtorListManager().Refresh();
};

HRSecondmentModal.prototype.secondmentDelayedCountryRefresh = function (secondment, element) {
  var me = this;
  clearTimeout(me.generalTimeout);
  me.generalTimeout = setTimeout(function () {
    ViewModel.CurrentSecondmentRODebtorListManager().Refresh();
  }, me.timeoutDelay);
};

HRSecondmentModal.prototype.onSecondmentDebtorSelected = function (RODebtor) {
  ViewModel.CurrentHumanResourceSecondment().DebtorID(RODebtor.DebtorID());
  ViewModel.CurrentHumanResourceSecondment().Debtor(RODebtor.Debtor());
  RODebtor.IsSelected(true);
};

HRSecondmentModal.prototype.secondmentDelayedDebtorRefresh = function (secondment, element) {
  var me = this;
  clearTimeout(me.generalTimeout);
  me.generalTimeout = setTimeout(function () {
    ViewModel.CurrentSecondmentRODebtorListManager().Refresh();
  }, me.timeoutDelay);
};

//#endregion

//#region Skills

HRSkillModal = function (options) {
  var me = this;
  me.options = {};
  var defaultOptions = {
    modalID: "CurrentHRSkillModal",
    skillInstanceName: 'CurrentHumanResourceSkill',
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: function (inst) { },
    onSkillFetched: function (inst, response) {

    }
  };
  me.options = $.extend({}, defaultOptions, options);
  me.generalTimeout = null;
  me.timeoutDelay = 200;

  me.currentHumanResourceData = null;

};

HRSkillModal.prototype.currentSkill = function () {
  if (ViewModel) {
    return ViewModel[this.options.skillInstanceName]();
  };
  return null;
};

HRSkillModal.prototype.setCurrentSkill = function (newValue) {
  ViewModel[this.options.skillInstanceName](newValue);
};

HRSkillModal.prototype.getHumanResourceDetails = function (humanResourceID, humanResourceName, onFetchSuccess) {
  var me = this;
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: humanResourceID
  }, function (response) {
    if (response.Success) {
      if (response.Data.length == 0) {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', 'The personal details for ' + humanResourceName + ' could not be retrieved. Please view their profile in the Human Resource Screen for any errors.', 3000)
        me.hideSkillModal()
      }
      else if (response.Data.length == 1) {
        me.currentHumanResourceData = response.Data[0];
        onFetchSuccess(response.Data[0])
      }
      else {
        me.currentHumanResourceData = null
        OBMisc.Notifications.GritterError('Error During Fetch', 'The personal details for ' + humanResourceName + ' could not be retrieved. Please view their profile in the Human Resource Screen for any errors.', 3000)
        me.hideSkillModal()
      }
    }
    else {
      me.currentHumanResourceData = null
      me.currentSkill().IsProcessing(false)
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 3000)
      me.hideSkillModal()
    }
  })
};

HRSkillModal.prototype.clearCurrentSkill = function () {
  ViewModel[this.options.skillInstanceName](null);
};

HRSkillModal.prototype.newSkillFromHRScreen = function (humanResource) {
  var hrsk = new HumanResourceSkillObject()
  var hasPrimarySkill = (humanResource.PrimarySkillCount() > 0 ? true : false)
  hrsk.CanChangePrimaryIndicator(!hasPrimarySkill)
  this.setCurrentSkill(hrsk)
  this.currentSkill().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID())
  this.showSkillModal()
};

HRSkillModal.prototype.editSkillHRScreen = function (ROSkill) {
  var me = this;
  var n = new SelectedItemObject()
  n.ID(ROSkill.HumanResourceSkillID())
  n.Description(ROSkill.Discipline())
  me.editSkill(ROSkill.HumanResourceSkillID(), function (response) {
    me.showSkillModal()
  })
  return n
};

HRSkillModal.prototype.editSkill = function (humanResourceSkillID) {
  var me = this
  Singular.SendCommand("EditHumanResourceSkill", {
    HumanResourceSkillID: humanResourceSkillID
  }, function (response) {
    if (response.Success) {
      me.showSkillModal()
      me.options.onSkillFetched(me, response)
    } else {
      OBMisc.Notifications.GritterError('Error During Fetch', response.ErrorText, 3000);
    }
  })
};

HRSkillModal.prototype.cancelSkillModal = function (skill) {
  var me = this
  me.setCurrentSkill(null)
  me.hideSkillModal()
};

HRSkillModal.prototype.save = function (humanResourceOffPeriod) {
  var me = this;
  Singular.SendCommand("SaveHumanResourceSkill", {}, function (response) {
    if (response.Success) {
      me.setCurrentSkill(null);
      me.hideSkillModal();
      HumanResourcesPage.onSkillsTabSelected();
      Singular.Validation.CheckRules(ViewModel.CurrentHumanResource());
      //if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 3000);
    }
  });
};

HRSkillModal.prototype.saveAndNew = function (humanResourceOffPeriod) {
  var me = this;
  Singular.SendCommand("SaveHumanResourceSkill", {}, function (response) {
    if (response.Success) {
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
      //ViewModel.CurrentHumanResourceOffPeriod(null);
      //ViewModel.CurrentHumanResourceOffPeriod(new HumanResourceOffPeriodObject());
      //ViewModel.CurrentHumanResourceOffPeriod().HumanResourceID(ViewModel.CurrentHumanResource().HumanResourceID());
      //ViewModel.CurrentHumanResourceOffPeriod().ResourceID(ViewModel.CurrentHumanResource().ResourceID());
      //Singular.Validation.CheckRules(ViewModel.CurrentHumanResourceOffPeriod());
    } else {
      OBMisc.Notifications.GritterError('Error During Save', response.ErrorText, 1000);
    }
  })
};

HRSkillModal.prototype.showSkillModal = function () {
  var me = this;

  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).off('hidden.bs.modal')

  $("#" + this.options.modalID).on('shown.bs.modal', function () {
    if (me.currentSkill()) { Singular.Validation.CheckRules(me.currentSkill()) }
    me.options.onModalOpened(me)
  })
  $("#" + this.options.modalID).on('hidden.bs.modal', function () {
    me.clearCurrentSkill()
  })

  $("#" + this.options.modalID).modal();
};

HRSkillModal.prototype.hideSkillModal = function () {
  $("#" + this.options.modalID).modal('hide');
};

HRSkillModal.prototype.newPosition = function (skill) {
  var newPos = skill.HumanResourceSkillPositionList.AddNew()
};

HRSkillModal.prototype.modalCss = function () {
  var me = this;
  if (me.disciplineRequired()) {
    return "modal-dialog modal-lg"
  }
  return "modal-dialog modal-xs"
};

HRSkillModal.prototype.disciplineRequired = function () {
  var me = this;
  if (me.currentSkill()) {
    if (me.currentSkill().DisciplineID()) {
      var RODiscipline = ClientData.RODisciplineList.Find("DisciplineID", me.currentSkill().DisciplineID())
      if (RODiscipline) {
        if (RODiscipline.PositionRequiredInd) {
          return true
        }
      }
    }
  }
  return false
};

HRSkillModal.prototype.headingText = function () {
  var me = this;
  if (me.currentSkill()) {
    if (me.currentSkill().DisciplineID()) {
      var RODiscipline = ClientData.RODisciplineList.Find("DisciplineID", me.currentSkill().DisciplineID())
      if (RODiscipline) {
        return RODiscipline.Discipline
      }
    }
  }
  return "Edit Skill"
};

HRSkillModal.prototype.newRate = function (skill) {
  var me = this
  if (skill.CurrentUserIsHRManager()) {
    var nd = skill.HumanResourceSkillRateList.AddNew()
  }
};

//#endregion

//#region PHR TBC

PHRTBCControl = function (options) {
  var me = this;
  me.options = {};

  var defaultOptions = {
    tableID: "ProductionHumanResourceTBCList",
    controlInstanceName: 'PHRTBC',
    listName: "ProductionHumanResourceTBCList",
    afterSaveSuccess: function (ctrl, response) {

    },
    onModalOpened: null
  };
  me.options = $.extend({}, defaultOptions, options);

  me.generalTimeout = null;
  me.timeoutDelay = 200;
  me.isProcessing = false;

  me.list = function () {
    if (ViewModel) {
      return ViewModel[this.options.listName]()
    }
    return null
  }

  me.setList = function (newValue) {
    if (ViewModel) {
      return ViewModel[this.options.listName](newValue)
    }
    return null
  }

  me.serialiseList = function () {
    var me = this;
    if (ViewModel) {
      var arr = me.list();
      var serArr = [];
      arr.Iterate(function (item, indx) {
        serArr.push(item.Serialise())
      })
      return serArr
    }
    return null
  }

  me.deserialiseList = function (serialised) {
    if (ViewModel) {
      return ViewModel[this.options.listName].Set(serialised)
    }
    return null
  }

};

PHRTBCControl.prototype.setBusy = function (state) {
  ViewModel.ProductionHumanResourceTBCListIsBusy(state)
};

PHRTBCControl.prototype.isBusy = function () {
  return ViewModel.ProductionHumanResourceTBCListIsBusy()
};

PHRTBCControl.prototype.isListValid = function () {
  var me = this;
  if (me.list()) {
    var invalidCount = 0;
    me.list().Iterate(function (i, ind) {
      if (!i.IsValid()) {
        invalidCount += 1
      }
    })
    if (invalidCount > 0) {
      return false
    } else {
      return true
    }
  }
  return false
};

PHRTBCControl.prototype.getListForHRScreen = function (humanResourceID) {
  var me = this
  me.setBusy(true)
  if (humanResourceID) {
    Singular.GetDataStateless("OBLib.HR.ProductionHumanResourcesTBCList", {
      HumanResourceID: humanResourceID
    }, function (response) {
      me.setBusy(false)
      if (response.Success) {
        ViewModel.ProductionHumanResourceTBCList.Set(response.Data)
      } else {
        OBMisc.Notifications.GritterError('Error Fetching TBC Productions', response.ErrorText, 1000)
      }
    })
  } else {
    me.setBusy(false)
    OBMisc.Notifications.GritterError('Error Fetching TBC Productions', "HumanResourceID could not be determined", 1000)
  }
};

PHRTBCControl.prototype.saveInHRScreen = function () {
  var me = this
  me.setBusy(true)
  var ser = me.serialiseList()
  ViewModel.CallServerMethod("SaveProductionHumanResourceTBC",
    {
      list: ser
    },
    function (response) {
      me.setBusy(false)
      if (response.Success) {
        OBMisc.Notifications.GritterSuccess('Saved successfully', 'TBC Productions have been saved successfully', 1500)
        me.getListForHRScreen(ViewModel.CurrentHumanResource().HumanResourceID())
      }
      else {
        OBMisc.Notifications.GritterError('Save Failed', response.ErrorText, 3000)
      }
    })
};

//#endregion

//#region Controls

CopyBookingModal = function (options) {
  var me = this;
  me.options = {};
  var defaultOptions = {
    modalID: "CopyBookingModal",
    copyBookingInstanceName: 'CopySetting',
    afterCopySuccess: function (ctrl, response) { }
  };
  me.options = $.extend({}, defaultOptions, options);
  me.generalTimeout = null;
  me.timeoutDelay = 200;
};

CopyBookingModal.prototype.currentCopySetting = function () {
  if (ViewModel) {
    return ViewModel[this.options.copyBookingInstanceName]();
  };
  return null;
};

CopyBookingModal.prototype.clearCurrentCopySetting = function () {
  ViewModel[this.options.copyBookingInstanceName](null);
};

CopyBookingModal.prototype.setCurrentCopySetting = function (newValue) {
  ViewModel[this.options.copyBookingInstanceName](newValue);
};

CopyBookingModal.prototype.submitSpecificDates = function (copySetting) {
  var me = this;
  copySetting.IsProcessing(true)
  var ser = KOFormatterFull.Serialise(copySetting)
  ViewModel.CallServerMethod("CopyResourceBookingToSpecificDates", {
    setting: ser
  }, function (response) {
    if (response.Success) {
      me.setCurrentCopySetting(null)
      me.hideModal()
      response.Data.Iterate(function (copyResult, indx) {
        if (copyResult.Success) {
          OBMisc.Notifications.GritterSuccess(copyResult.Title, copyResult.Description, 1500)
        }
        else {
          OBMisc.Notifications.GritterError(copyResult.Title, copyResult.Description, 3000)
        }
      })
      if (me.options.afterSaveSuccess) { me.options.afterSaveSuccess(me, response) }
      copySetting.IsProcessing(false)
    }
    else {
      OBMisc.Notifications.GritterError('Error During Copy', response.ErrorText, 1000);
      copySetting.IsProcessing(false)
    }
  });
};

CopyBookingModal.prototype.showModal = function (resourceBooking) {
  var me = this;
  var cs = new CopySettingObject()
  cs.ResourceID(resourceBooking.ResourceID)
  cs.EquipmentID(resourceBooking.EquipmentID)
  cs.EquipmentName(resourceBooking.ResourceName)
  cs.ResourceBookingID(resourceBooking.ResourceBookingID)
  cs.ResourceBookingTypeID(resourceBooking.ResourceBookingTypeID)
  cs.ResourceBookingDescription(resourceBooking.ResourceBookingDescription)
  cs.StartDateTimeBuffer(resourceBooking.StartDateTimeBuffer)
  cs.StartDateTime(resourceBooking.StartDateTime)
  cs.EndDateTime(resourceBooking.EndDateTime)
  cs.EndDateTimeBuffer(resourceBooking.EndDateTimeBuffer)
  me.setCurrentCopySetting(cs)
  var d1 = moment(new Date(cs.StartDateTime()))
  var d2 = moment(new Date(cs.StartDateTime()))
  var minD = d1.add(-1, 'months').toDate()
  var maxD = d2.add(12, 'months').toDate()

  $("#" + this.options.modalID).off('shown.bs.modal')
  $("#" + this.options.modalID).off('hidden.bs.modal')

  $("#" + this.options.modalID).on('shown.bs.modal', function () {

    if (me.currentCopySetting()) { Singular.Validation.CheckRules(me.currentCopySetting()) }

    var element = $("#" + me.options.modalID)
    var modalBody = element.find('div.modal-body')

    me.detailDateSelector = $(modalBody).find('div#multiSelectDate')
    me.detailDateSelector = $(me.detailDateSelector).datepicker({
      showButtonPanel: true,
      minDate: minD,
      maxDate: maxD,
      dateFormat: "ddd dd MMM yyyy",
      closeText: "Close",
      closeOnSelect: false,
      beforeShowDay: function (date) {
        var stringDate = date.format("ddd dd MMM yyyy");
        var isSelected = (me.currentCopySetting().DatesToCopyTo().indexOf(stringDate) >= 0 ? true : false)
        return [true, (isSelected ? "day-selected" : ""),]
      },
      onSelect: function (datetext, inst) {
        var d = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)
        datetext = d.format("ddd dd MMM yyyy")
        var index = me.currentCopySetting().DatesToCopyTo().indexOf(datetext)
        if (index < 0) {
          me.currentCopySetting().DatesToCopyTo().push(datetext)
        }
        else {
          me.currentCopySetting().DatesToCopyTo().splice(index, 1)
        }
        return false;
      }
    })
    $(me.detailDateSelector).datepicker('show')

  })

  $("#" + this.options.modalID).on('hidden.bs.modal', function () {

  })

  $("#" + this.options.modalID).modal();
};

CopyBookingModal.prototype.hideModal = function () {
  $("#" + this.options.modalID).modal('hide');
};

ContentServicesDifferenceModal = {
  modalID: "#ContentServicesDifferencesModal",
  listName: "ContentServicesDifferenceList",
  save: function () {
    var me = this;
    var listObject = ViewModel[me.listName]
    ViewModel.CallServerMethod("ActionContentServicesDifferences",
      {
        Differences: listObject.Serialise()
      },
      function (response) {
        if (response.Success) {
          //window.siteHubManager.changeMade()
          OBMisc.Notifications.GritterSuccess("Save Succeeded", "", 250)
          $(me.modalID).modal("hide")
          ViewModel[me.listName]([])
        }
        else {
          OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 1000)
        }
      })
  }
}

//#endregion