﻿function PersonnelManagers(options) {
	var me = this

	//ui properties
	me.dom = {}
	me.dom.personnelManagerContainer = document.getElementById("personnel-manager-container")

	//options and variables
	me.defaultOptions = {}
	me.defaultOptions = $.extend({}, me.defaultOptions, options)
	me.dateData = []
	me.personnelData = []
	me.currentView = null

	var currentDate = new Date();
	var year = currentDate.getFullYear();
	var month = currentDate.getMonth();

	/*me.startDate = new Date(year, month, 1, 0, 0, 0, 0)
	me.endDate = new Date(year, month + 1, 0, 0, 0, 0, 0)*/
	me.startDate = new Date(ViewModel.StartDate())
	me.endDate = new Date(ViewModel.EndDate())

	lastDay = moment(me.endDate)

	me.numberOfDays = moment.duration(lastDay.diff(me.startDate)).asDays()
	me.rowWidth = (40 * me.numberOfDays)
	me.totalMinutes = (me.numberOfDays * 24 * 60)
	me.pixelsPerDay = (me.totalMinutes / me.rowWidth)
	me.searchModalID = "SelectHumanResources"
	me.legendContainerID = "StatusLegend"

	//method calls
	me.create()
}

PersonnelManagers.prototype.create = function () {
	var me = this

	////create the busy icon
	//me.dom.loadingDiv = document.createElement("div")
	//me.dom.loadingDiv.className = "loader"
	//me.dom.loadingDiv.innerHTML = "Loading..."
	//me.dom.loadingDiv.style.display = 'block'

	//create the container
	me.dom.container = document.createElement("div")
	me.dom.container.id = "personnel-manager"
	me.dom.container.className = "personnel-manager"

	//create the tabs
	me.dom.tabs = document.createElement("ul")
	me.dom.tabs.id = "view-tabs"
	me.dom.tabs.setAttribute("role", "tablist")
	me.dom.tabs.className = "view-tabs nav nav-tabs"
	me.dom.tabsContent = document.createElement("div")
	me.dom.tabsContent.id = "view-tabs-content"
	me.dom.tabsContent.className = "view-tabs-content tab-content"

	//monthly grid tab
	me.dom.monthlyGridTab = document.createElement("li")
	me.dom.monthlyGridTabAnchor = document.createElement("a")
	me.dom.monthlyGridTabAnchor.innerHTML = "Monthly Grid"
	me.dom.monthlyGridTabAnchor.setAttribute("href", "#MonthlyGrid")
	me.dom.monthlyGridTabAnchor.setAttribute("role", "tab")
	me.dom.monthlyGridTabAnchor.setAttribute("data-toggle", "tab")
	me.dom.monthlyGridTabContent = document.createElement("div")
	me.dom.monthlyGridTabContent.id = "MonthlyGrid"
	me.dom.monthlyGridTabContent.className = "tab-pane"
	me.dom.monthlyGridTab.appendChild(me.dom.monthlyGridTabAnchor)
	me.dom.tabsContent.appendChild(me.dom.monthlyGridTabContent)
	me.dom.tabs.appendChild(me.dom.monthlyGridTab)

	////monthly calendar tab
	//me.dom.monthlyCalendarTab = document.createElement("li")
	//me.dom.monthlyCalendarTabAnchor = document.createElement("a")
	//me.dom.monthlyCalendarTabAnchor.innerHTML = "Monthly Calendar"
	//me.dom.monthlyCalendarTabAnchor.setAttribute("href", "#MonthlyCalendar")
	//me.dom.monthlyCalendarTabAnchor.setAttribute("role", "tab")
	//me.dom.monthlyCalendarTabAnchor.setAttribute("data-toggle", "tab")
	//me.dom.monthlyCalendarTabContent = document.createElement("div")
	//me.dom.monthlyCalendarTabContent.id = "MonthlyCalendar"
	//me.dom.monthlyCalendarTabContent.className = "tab-pane"
	//me.dom.monthlyCalendarTab.appendChild(me.dom.monthlyCalendarTabAnchor)
	//me.dom.tabsContent.appendChild(me.dom.monthlyCalendarTabContent)
	//me.dom.tabs.appendChild(me.dom.monthlyCalendarTab)

	//other tabs

	//monthly view
	me.dom.monthlyView = document.createElement('div')
	me.dom.monthlyView.id = "monthly-view"
	me.dom.monthlyView.className = "monthly-view"

	me.dom.monthlyView.leftColumn = document.createElement('div')
	me.dom.monthlyView.leftColumn.id = "left-column"
	me.dom.monthlyView.leftColumn.className = "left-column"

	me.dom.monthlyView.leftColumn.dateSelection = document.createElement('div')
	me.dom.monthlyView.leftColumn.dateSelection.id = "date-selection"
	me.dom.monthlyView.leftColumn.dateSelection.className = "date-selection top-row"

	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton = document.createElement('button')
	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton.className = "btn btn-md btn-info"
	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton.id = "select-crew"
	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton.setAttribute('type', 'button')
	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton.setAttribute('title', 'Select Crew')
	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton.innerHTML = "<i class='fa fa-search' />"
	me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton.onclick = function (event) { me.showSearchModal.call(me, event) }

	me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton = document.createElement('button')
	me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton.className = "btn btn-md btn-danger"
	me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton.id = "previous-month"
	me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton.setAttribute('type', 'button')
	me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton.innerHTML = "<"
	me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton.onclick = function (event) { me.onPreviousMonthClicked.call(me, event) }

	me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton = document.createElement('button')
	me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton.className = "btn btn-md btn-danger"
	me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton.id = "select-month"
	me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton.setAttribute('type', 'button')
	me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton.innerHTML = me.startDate.format('MMM yyyy')

	me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton = document.createElement('button')
	me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton.className = "btn btn-md btn-danger"
	me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton.id = "next-month"
	me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton.setAttribute('type', 'button')
	me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton.innerHTML = ">"
	me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton.onclick = function (event) { me.onNextMonthClicked.call(me, event) }

	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton = document.createElement('button')
	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton.className = "btn btn-md btn-info"
	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton.id = "view-legend"
	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton.setAttribute('type', 'button')
	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton.setAttribute('title', 'View Legend')
	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton.innerHTML = "<i class='fa fa-life-bouy' />"
	me.dom.monthlyView.leftColumn.dateSelection.showLegendButton.onclick = function (event) { me.showLegend.call(me, event) }

	me.dom.monthlyView.leftColumn.personnelSelection = document.createElement('div')
	me.dom.monthlyView.leftColumn.personnelSelection.id = "personnel-selection"
	me.dom.monthlyView.leftColumn.personnelSelection.className = "personnel-selection"

	me.dom.monthlyView.rightColumn = document.createElement('div')
	me.dom.monthlyView.rightColumn.id = "right-column"
	me.dom.monthlyView.rightColumn.className = "right-column"

	me.dom.monthlyView.rightColumn.daysDiv = document.createElement('div')
	me.dom.monthlyView.rightColumn.daysDiv.id = "days"
	me.dom.monthlyView.rightColumn.daysDiv.className = "div-days top-row"

	me.dom.monthlyView.rightColumn.personnelDaysDiv = document.createElement('div')
	me.dom.monthlyView.rightColumn.personnelDaysDiv.id = "personnel-days"
	me.dom.monthlyView.rightColumn.personnelDaysDiv.className = "personnel-days"

	me.dom.monthlyView.leftColumn.dateSelection.appendChild(me.dom.monthlyView.leftColumn.dateSelection.selectCrewButton)
	me.dom.monthlyView.leftColumn.dateSelection.appendChild(me.dom.monthlyView.leftColumn.dateSelection.previousMonthButton)
	me.dom.monthlyView.leftColumn.dateSelection.appendChild(me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton)
	me.dom.monthlyView.leftColumn.dateSelection.appendChild(me.dom.monthlyView.leftColumn.dateSelection.nextMonthButton)
	me.dom.monthlyView.leftColumn.dateSelection.appendChild(me.dom.monthlyView.leftColumn.dateSelection.showLegendButton)
	me.dom.monthlyView.leftColumn.appendChild(me.dom.monthlyView.leftColumn.dateSelection)
	me.dom.monthlyView.leftColumn.appendChild(me.dom.monthlyView.leftColumn.personnelSelection)
	me.dom.monthlyView.rightColumn.appendChild(me.dom.monthlyView.rightColumn.daysDiv)
	me.dom.monthlyView.rightColumn.appendChild(me.dom.monthlyView.rightColumn.personnelDaysDiv)
	me.dom.monthlyView.appendChild(me.dom.monthlyView.leftColumn)
	me.dom.monthlyView.appendChild(me.dom.monthlyView.rightColumn)

	//append the universe
	me.dom.container.appendChild(me.dom.tabs)
	me.dom.container.appendChild(me.dom.tabsContent)

	me.dom.monthlyGridTabContent.appendChild(me.dom.monthlyView)
	//me.dom.container.appendChild(me.dom.monthlyView)
	//me.dom.personnelManagerContainer.appendChild(me.dom.loadingDiv)
	me.dom.personnelManagerContainer.appendChild(me.dom.container)

	////search modal
	//me.dom.hrSearchModal = document.createElement("div")
	//me.dom.hrSearchModal.className = "modal fade"
	//me.dom.hrSearchModal.setAttribute("tabindex", "-1")
	//me.dom.hrSearchModal.setAttribute("role", "dialog")

	//me.dom.hrSearchModal.dialog = document.createElement("div")
	//me.dom.hrSearchModal.dialog.className = "modal-dialog modal-xl"
	//me.dom.hrSearchModal.dialog.setAttribute("role", "document")

	//me.dom.hrSearchModal.dialog.content = document.createElement("div")
	//me.dom.hrSearchModal.dialog.content.className = "modal-content"

	//me.dom.hrSearchModal.dialog.content.header = document.createElement("div")
	//me.dom.hrSearchModal.dialog.content.header.className = "modal-header modal-primary"

	//me.dom.hrSearchModal.dialog.content.header.closeButton = document.createElement("button")
	//me.dom.hrSearchModal.dialog.content.header.closeButton.className = "close"
	//me.dom.hrSearchModal.dialog.content.header.closeButton.setAttribute("type", "button")
	//me.dom.hrSearchModal.dialog.content.header.closeButton.setAttribute("data-dismiss", "modal")
	//me.dom.hrSearchModal.dialog.content.header.closeButton.setAttribute("aria-label", "Close")

	//me.dom.hrSearchModal.dialog.content.header.closeButton.span = document.createElement('span')
	//me.dom.hrSearchModal.dialog.content.header.closeButton.span.setAttribute("aria-hidden", "true")
	//me.dom.hrSearchModal.dialog.content.header.closeButton.span.innerHTML = "&times;"

	//me.dom.hrSearchModal.dialog.content.header.heading = document.createElement('h4')
	//me.dom.hrSearchModal.dialog.content.header.heading.className = "modal-title"
	//me.dom.hrSearchModal.dialog.content.header.heading.innerHTML = "Filter Human Resources"

	//me.dom.hrSearchModal.dialog.content.header.appendChild(me.dom.hrSearchModal.dialog.content.header.heading)
	//me.dom.hrSearchModal.dialog.content.header.closeButton.appendChild(me.dom.hrSearchModal.dialog.content.header.closeButton.span)
	//me.dom.hrSearchModal.dialog.content.header.appendChild(me.dom.hrSearchModal.dialog.content.header.closeButton)


	//me.dom.hrSearchModal.dialog.content.body = document.createElement("div")
	//me.dom.hrSearchModal.dialog.content.body.className = "modal-body"

	//me.dom.hrSearchModal.dialog.content.footer = document.createElement("div")
	//me.dom.hrSearchModal.dialog.content.footer.className = "modal-footer"

	//me.dom.hrSearchModal.dialog.content.appendChild(me.dom.hrSearchModal.dialog.content.header)
	//me.dom.hrSearchModal.dialog.content.appendChild(me.dom.hrSearchModal.dialog.content.body)
	//me.dom.hrSearchModal.dialog.content.appendChild(me.dom.hrSearchModal.dialog.content.footer)
	//me.dom.hrSearchModal.dialog.appendChild(me.dom.hrSearchModal.dialog.content)
	//me.dom.hrSearchModal.appendChild(me.dom.hrSearchModal.dialog)

	OBMisc.UI.activateTab("MonthlyGrid")
	me.refresh()
}

PersonnelManagers.prototype.getPersonnelData = function (options) {
	var me = this
	Singular.GetDataStateless("OBLib.HR.PersonnelManager.PMResourceList", options.criteria, function (response) {
		if (response.Success) {
			me.dom.monthlyView.leftColumn.dateSelection.selectMonthButton.innerHTML = me.startDate.format('MMM yyyy')
			options.afterSuccess.call(me, response)
		}
		else {
			OBMisc.Notifications.GritterError("Data Refresh Failed", response.ErrorText, 1000)
		}
	})
}

PersonnelManagers.prototype.getDateData = function (options) {
	var me = this
	Singular.GetDataStateless("OBLib.HR.PersonnelManager.PMDateList", options.criteria, function (response) {
		if (response.Success) {
			options.afterSuccess.call(me, response)
		}
		else {
			OBMisc.Notifications.GritterError("Data Refresh Failed", response.ErrorText, 1000)
		}
	})
}

PersonnelManagers.prototype.getShiftTypes = function (options) {
	var me = this
	Singular.GetDataStateless("OBLib.Maintenance.PlayoutOps.ReadOnly.ROLegendList", options.criteria, function (response) {
		if (response.Success) {
			options.afterSuccess.call(me, response)
		}
		else {
			OBMisc.Notifications.GritterError("Data Refresh Failed", response.ErrorText, 1000)
		}
	})
}

PersonnelManagers.prototype.refresh = function () {
	var me = this
	me.startDate = new Date(ViewModel.StartDate())
	me.endDate = new Date(ViewModel.EndDate())
	me.getDateData({
		criteria: {
			StartDate: ViewModel.StartDate().format('dd MMM yyyy'),
			EndDate: ViewModel.EndDate().format('dd MMM yyyy')
		},
		afterSuccess: function (response) {
			me.dateData = []
			me.dateData = response.Data
			me.getPersonnelData({
			    criteria: {
			        OnlyBookedInd: ViewModel.OnlyBookedInd(),
					StartDate: ViewModel.StartDate().format('dd MMM yyyy'),
					EndDate: ViewModel.EndDate().format('dd MMM yyyy'),
					SystemIDs: me.getSelectedSystemIDs(),
					ProductionAreaIDs: me.getSelectedProductionAreaIDs(),
					ContractTypeIDs: me.getSelectedContractTypeIDs(),
					DisciplineIDs: me.getSelectedDisciplineIDs(),
					SystemTeamNumberIDs: me.getSelectedSystemTeamNumberIDs(),
					HumanResourceIDs: me.getSelectedHumanResourceIDs()
				},
				afterSuccess: function (response) {
					//cleanup
					me.personnelData = []
					me.personnelData = response.Data
					while (me.dom.monthlyView.rightColumn.daysDiv.hasChildNodes()) {
						me.dom.monthlyView.rightColumn.daysDiv.removeChild(me.dom.monthlyView.rightColumn.daysDiv.lastChild)
					}
					while (me.dom.monthlyView.leftColumn.personnelSelection.hasChildNodes()) {
						me.dom.monthlyView.leftColumn.personnelSelection.removeChild(me.dom.monthlyView.leftColumn.personnelSelection.lastChild)
					}
					while (me.dom.monthlyView.rightColumn.personnelDaysDiv.hasChildNodes()) {
						me.dom.monthlyView.rightColumn.personnelDaysDiv.removeChild(me.dom.monthlyView.rightColumn.personnelDaysDiv.lastChild)
					}
					me.drawDays()
					me.drawPersonnel()
					me.drawPersonnelBookingsAll()
				}
			})
		}
	})

	me.getShiftTypes({
		criteria: {
			SystemID: 5 /*,
			ProductionAreaID: 10*/
		},
		afterSuccess: function (response) {

			var legendDiv = document.createElement('div')
			var cont = document.getElementById("date-selection")

			legendDiv.id = me.legendContainerID
			legendDiv.className = "hide-legend"

			response.Data.Iterate(function (stat, statIndex) {

				var legendLine = document.createElement('div')
				legendLine.style.display = "block"

				var legendKey = document.createElement('div')
				legendKey.className = stat.LegendKey
				legendKey.style.display = "inline-block"
				legendKey.style.border = "1px solid #000"
				legendKey.style.width = "22px"
				legendKey.style.height = "8px"

				var legendName = document.createElement('span')
				legendName.className = "legendName"
				legendName.style.display = "inline-block"
				legendName.innerHTML = stat.Legend

				legendLine.appendChild(legendKey)
				legendLine.appendChild(legendName)

				legendDiv.appendChild(legendLine)

				legendLine = null
				legendKey = null
				legendName = null
			})

			cont.appendChild(legendDiv)

			legendDiv = null
		}
	})
}

PersonnelManagers.prototype.onPreviousMonthClicked = function (event) {
	var me = this
	var newStartDate = moment(me.startDate).add(-1, 'month')
	me.startDate = new Date(newStartDate.toDate().getFullYear(), newStartDate.toDate().getMonth(), 1, 0, 0, 0, 0)
	me.endDate = newStartDate.clone().endOf('month').toDate()
	ViewModel.StartDate(me.startDate)
	ViewModel.EndDate(me.endDate)
	me.refresh()
}

PersonnelManagers.prototype.onNextMonthClicked = function (event) {
	var me = this
	var newStartDate = moment(me.startDate).add(1, 'month')
	me.startDate = new Date(newStartDate.toDate().getFullYear(), newStartDate.toDate().getMonth(), 1, 0, 0, 0, 0)
	me.endDate = newStartDate.clone().endOf('month').toDate()
	ViewModel.StartDate(me.startDate)
	ViewModel.EndDate(me.endDate)
	me.refresh()
}

PersonnelManagers.prototype.drawDays = function () {
	var me = this;
	me.dateData.Iterate(function (day, dayIndex) {
		var dayColumn = document.createElement('div')
		dayColumn.className = "day-column"
		dayColumn.style.width = me.pixelsPerDay.toString() + "px"
		if (day.IsPublicHoliday) { dayColumn.className += " public-holiday" }
		if (day.IsToday) { dayColumn.className += " today" }
		dayColumn.data = day
		var dayNumber = document.createElement('div')
		dayNumber.className = "day-number"
		dayNumber.innerHTML = day.MonthDay.toString()
		var dayName = document.createElement('div')
		dayNumber.className = "day-name"
		dayName.innerHTML = day.WeekDayNameShort
		dayColumn.appendChild(dayNumber)
		dayColumn.appendChild(dayName)
		me.dom.monthlyView.rightColumn.daysDiv.appendChild(dayColumn)
	})
}

PersonnelManagers.prototype.drawPersonnel = function () {
	var me = this;
	//populate
	me.personnelData.Iterate(function (person, personIndex) {
		//left column
		var personSelectionRow = document.createElement('div')
		personSelectionRow.className = "person-row"
		personSelectionRow.data = person
		personSelectionRow.innerHTML = person.ResourceName
		me.dom.monthlyView.leftColumn.personnelSelection.appendChild(personSelectionRow)
		//right column
		var personBookingsRow = document.createElement('div')
		personBookingsRow.className = 'person-row-day'
		//personBookingsRow.style.width = me.pixelsPerDay.toString() + "px"
		personBookingsRow.data = person
		me.dateData.Iterate(function (day, dayIndex) {
			var personDayColumn = document.createElement('div')
			personDayColumn.className = "day-column day-column-person"
			personDayColumn.style.width = me.pixelsPerDay.toString() + "px"
			if (day.IsWeekend) { personDayColumn.className += " weekend" }
			personDayColumn.data = day
			personBookingsRow.appendChild(personDayColumn)
		})
		me.dom.monthlyView.rightColumn.personnelDaysDiv.appendChild(personBookingsRow)
	})
}

PersonnelManagers.prototype.drawPersonnelBookingsAll = function () {
	var me = this;
	var rows = document.getElementsByClassName("person-row-day")
	for (i = 0; i < rows.length; i++) {
		var row = rows[i]
		me.drawPersonnelBookings(row)
	}
};

PersonnelManagers.prototype.drawPersonnelBookings = function (personBookingsRow) {
	var me = this
	//row dimensions
	var rowDimensions = personBookingsRow.getBoundingClientRect()
	//console.log(rowDimensions)
	var rowX = rowDimensions.left
	var offsetX = personBookingsRow.offsetLeft
	var rowY = rowDimensions.top
	var offsetY = personBookingsRow.offsetTop
	var rowHeight = rowDimensions.height
	//bookings
	var person = personBookingsRow.data
	person.PMResourceBookingList.Iterate(function (booking, bookingIndx) {
		//calculate dimensions per booking
		var sd = moment(booking.StartDateTime)
		var ed = moment(booking.EndDateTime)
		var bookingStartMinutes = moment.duration(sd.diff(me.startDate)).asMinutes()
		var bookingMinutes = moment.duration(ed.diff(sd)).asMinutes()
		var width = (bookingMinutes / (me.rowWidth * 24 * 60 / me.totalMinutes))
		var left = (bookingStartMinutes / (me.rowWidth * 24 * 60 / me.totalMinutes)) + offsetX
		var title = booking.ResourceBookingDescription
		title += ': ' + sd.toDate().format('dd MMM yy HH:mm')
		title += ' - ' + ed.toDate().format('dd MMM yy HH:mm')
		var bookingDiv = document.createElement('div')
		bookingDiv.id = booking.ResourceBookingID
		bookingDiv.className = "resource-booking " + booking.StatusCssClass
		bookingDiv.style.position = 'absolute'
		bookingDiv.style.height = "5px"
		bookingDiv.style.top = (offsetY + 20).toString() + "px"
		bookingDiv.style.left = left.toString() + "px"
		bookingDiv.style.width = width.toString() + "px"
		bookingDiv.setAttribute('title', title)
		personBookingsRow.appendChild(bookingDiv)
		bookingDiv = null
	})
}

PersonnelManagers.prototype.showSearchModal = function () {
	var me = this
	$('#' + me.searchModalID).off('shown.bs.modal')
	$('#' + me.searchModalID).off('hidden.bs.modal')
	$('#' + me.searchModalID).on('shown.bs.modal', function () {

	})
	$('#' + me.searchModalID).on('hidden.bs.modal', function () {

	})
	$('#' + me.searchModalID).modal()
}

PersonnelManagers.prototype.showLegend = function () {
	var me = this
	var legendDiv = document.getElementById(me.legendContainerID)

	legendDiv.classList.toggle('view-legend')
	legendDiv.classList.toggle('hide-legend')

}

PersonnelManagers.prototype.criteriaConfirmed = function () {
	var me = this
	me.refresh()
	$('#' + me.searchModalID).modal('hide')
}

PersonnelManagers.prototype.getSelectedSystemIDs = function () {
	var selectedSystems = [];
	ViewModel.UserSystemList().Iterate(function (us, usIndex) {
		if (us.IsSelected()) {
			selectedSystems.push(us.SystemID())
		}
	})
	//ViewModel.ROHumanResourceListCriteria.SystemIDs(selectedSystems)
	return selectedSystems
}

PersonnelManagers.prototype.getSelectedProductionAreaIDs = function () {
	var selectedAreas = [];
	ViewModel.UserSystemList().Iterate(function (us, usIndex) {
		if (us.IsSelected()) {
			us.UserSystemAreaList().Iterate(function (usa, usaIndex) {
				if (usa.IsSelected()) {
					selectedAreas.push(usa.ProductionAreaID())
				}
			})
		}
	})
	//ViewModel.ROHumanResourceListCriteria.ProductionAreaIDs(selectedAreas)
	return selectedAreas
}

PersonnelManagers.prototype.getSelectedContractTypeIDs = function () {
	var selectedContractTypes = [];
	ViewModel.ROContractTypeList().Iterate(function (us, usIndex) {
		if (us.IsSelected()) {
			selectedContractTypes.push(us.ContractTypeID())
		}
	})
	//ViewModel.ROHumanResourceListCriteria.ContractTypeIDs(selectedContractTypes)
	return selectedContractTypes
}

PersonnelManagers.prototype.getSelectedDisciplineIDs = function () {
	var selectedDisciplines = [];
	ViewModel.RODisciplineSelectList().Iterate(function (us, usIndex) {
		if (us.IsSelected()) {
			selectedDisciplines.push(us.DisciplineID())
		}
	})
	//ViewModel.ROHumanResourceListCriteria.DisciplineIDs(selectedDisciplines)
	return selectedDisciplines
}

PersonnelManagers.prototype.getSelectedSystemTeamNumberIDs = function () {
	var selectedTeams = [];
	ViewModel.ROSystemTeamNumberList().Iterate(function (us, usIndex) {
		if (us.IsSelected()) {
			selectedTeams.push(us.SystemTeamNumberID())
		}
	})
	//ViewModel.ROHumanResourceListCriteria.SystemTeamNumberIDs(selectedTeams)
	return selectedTeams
}

PersonnelManagers.prototype.getSelectedHumanResourceIDs = function () {
	var selectedResources = [];
	ViewModel.ROHumanResourceList().Iterate(function (us, usIndex) {
		if (us.SelectedInd()) {
			selectedResources.push(us.HumanResourceID())
		}
	})
	//ViewModel.ROHumanResourceListCriteria.HumanResourceList()
	return selectedResources
}

PersonnelManagers.prototype.selectAllDisciplines = function (select) {
	ViewModel.RODisciplineSelectList().Iterate(function (us, usIndex) {
		us.IsSelected(select)
		if (select) {
			ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs.push(us.DisciplineID());
		}
		else {
			var index = ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs.indexOf(us.DisciplineID());
			if (index > -1) {
				ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs.splice(index, 1);
			}
		}
	})
	this.GetAllowedHumanResources();
}

PersonnelManagers.prototype.selectAllHumanResources = function (obj) {
  ViewModel.ROHumanResourceList().Iterate(function (us, usIndex) {
    us.SelectedInd(obj.SelectAllHrInd())
  });
  obj.SelectAllHrInd(!obj.SelectAllHrInd())
  //ViewModel.SelectAllHrInd(select);
}

PersonnelManagers.prototype.AddSystem = function (obj) {
	obj.IsSelected(!obj.IsSelected());
	if (obj.IsSelected()) {
		ViewModel.ROHumanResourceFindListCriteria().SystemIDs.push(obj.SystemID());
	}
	else {
		var index = ViewModel.ROHumanResourceFindListCriteria().SystemIDs.indexOf(obj.SystemID());
		if (index > -1) {
			ViewModel.ROHumanResourceFindListCriteria().SystemIDs.splice(index, 1);
		}
	}
	//ViewModel.ROHumanResourceFindListCriteria().SystemIDs(OBMisc.Xml.getXmlIDs(ViewModel.SystemIDs()));
	this.GetAllowedHumanResources();
},
PersonnelManagers.prototype.AddProductionArea = function (obj) {
	obj.IsSelected(!obj.IsSelected());
	if (obj.IsSelected()) {
		ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs.push(obj.ProductionAreaID());
	}
	else {
		var index = ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs.indexOf(obj.ProductionAreaID());
		if (index > -1) {
			ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs.splice(index, 1);
		}
	}
	//ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs(OBMisc.Xml.getXmlIDs(ViewModel.ProductionAreaIDs()));
	this.GetAllowedHumanResources();
},
PersonnelManagers.prototype.AddDiscipline = function (obj) {
	obj.IsSelected(!obj.IsSelected());
	if (obj.IsSelected()) {
		ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs.push(obj.DisciplineID());
	}
	else {
		var index = ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs.indexOf(obj.DisciplineID());
		if (index > -1) {
			ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs.splice(index, 1);
		}
	}
	//ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs(OBMisc.Xml.getXmlIDs(ViewModel.DisciplineIDs()));
	this.GetAllowedHumanResources();
},
PersonnelManagers.prototype.AddSystemTeamNumber = function (obj) {
	obj.IsSelected(!obj.IsSelected());
	if (obj.IsSelected()) {
		ViewModel.ROHumanResourceFindListCriteria().SystemTeamNumberIDs.push(obj.SystemTeamNumberID());
	}
	else {
		var index = ViewModel.ROHumanResourceFindListCriteria().SystemTeamNumberIDs.indexOf(obj.SystemTeamNumberID());
		if (index > -1) {
			ViewModel.ROHumanResourceFindListCriteria().SystemTeamNumberIDs.splice(index, 1);
		}
	}
	//ViewModel.ROHumanResourceFindListCriteria().SystemTeamNumberIDs(OBMisc.Xml.getXmlIDs(ViewModel.SystemTeamNumberIDs()));
	this.GetAllowedHumanResources();
},
PersonnelManagers.prototype.AddContractType = function (obj) {
	obj.IsSelected(!obj.IsSelected());
	if (obj.IsSelected()) {
		ViewModel.ROHumanResourceFindListCriteria().ContractTypeIDs.push(obj.ContractTypeID());
	}
	else {
		var index = ViewModel.ROHumanResourceFindListCriteria().ContractTypeIDs.indexOf(obj.ContractTypeID());
		if (index > -1) {
			ViewModel.ROHumanResourceFindListCriteria().ContractTypeIDs.splice(index, 1);
		}
	}
	//ViewModel.ROHumanResourceFindListCriteria().ContractTypeIDs(OBMisc.Xml.getXmlIDs(ViewModel.ContractTypeIDs()));
	this.GetAllowedHumanResources();
},
PersonnelManagers.prototype.OnlyBooked = function (obj) {
    obj.OnlyBookedInd(!obj.OnlyBookedInd());
    ViewModel.OnlyBookedInd(obj.OnlyBookedInd());
    this.GetAllowedHumanResources();
},
/*
PersonnelManagers.prototype.AddHumanResource = function (obj) {
	obj.IsSelected(!obj.IsSelected());
	if (obj.IsSelected()) {
		if (ViewModel.ROHumanResourceSelectedList.indexOf(obj.HumanResourceID()) == -1) {
			ViewModel.ROHumanResourceSelectedList.push(obj);
		}
	}
	else {
		var HR = ViewModel.ROHumanResourceList().Find('HumanResourceID', obj.HumanResourceID());
		if (HR) {
			HR.IsSelected(false);
		}
		var indexofID = ViewModel.HumanResourceIDs.indexOf(obj.HumanResourceID());
		if (indexofID != -1) {
			ViewModel.HumanResourceIDs.splice(indexofID, 1);
		}
		var indexOfObj = $.map(ViewModel.ROHumanResourceSelectedList(), function (x, index) {
			if (x.HumanResourceID === obj.HumanResourceID) {
				return index;
			}
		});
		if (indexOfObj != -1) { //Item is in list if index != -1, so we can remove it at that index
			ViewModel.ROHumanResourceSelectedList.splice(indexOfObj, 1);
		}
	}
},
*/
PersonnelManagers.prototype.GetAllowedHumanResources = function () {
	//ViewModel.IsReportBusy(true)
	Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceListPersonnelManager, OBLib', {
		SystemIDs: ViewModel.ROHumanResourceFindListCriteria().SystemIDs(),
		ProductionAreaIDs: ViewModel.ROHumanResourceFindListCriteria().ProductionAreaIDs(),
		ContractTypeIDs: ViewModel.ROHumanResourceFindListCriteria().ContractTypeIDs(),
		DisciplineIDs: ViewModel.ROHumanResourceFindListCriteria().DisciplineIDs(),
		SystemTeamNumberIDs: ViewModel.ROHumanResourceFindListCriteria().SystemTeamNumberIDs(),
		FirstName: ViewModel.ROHumanResourceFindListCriteria().FirstName(),
		PreferredName: ViewModel.ROHumanResourceFindListCriteria().PreferredName(),
		Surname: ViewModel.ROHumanResourceFindListCriteria().Surname(),
		OnlyBookedInd: ViewModel.OnlyBookedInd(),
		StartDateTime: ViewModel.StartDate(),
		EndDateTime: ViewModel.EndDate()
	}, function (args) {
		if (args.Success) {
			$("#HumanResources").addClass('FadeDisplay');
			//$("#SelectedHumanResources").addClass('FadeDisplay');
			//$("#HumanResourceFilters").addClass('FadeDisplay');
			ViewModel.ROHumanResourceList.Set([]);
			ViewModel.ROHumanResourceList.Set(args.Data);
			//ViewModel.ROHumanResourceSelectedList().Iterate(function (itm, ind) {
			//  ViewModel.Report().ReportCriteriaGeneric().ROHumanResourceList().Iterate(function (itm2, ind2) {
			//    if (itm.HumanResourceID() == itm2.HumanResourceID()) {
			//      itm2.IsSelected(true);
			//    }
			//  });
			//});
		}
		//ViewModel.IsReportBusy(false);
	});
}

PersonnelManagers.prototype.FirstNameSet = function (self) {
	this.GetAllowedHumanResources(self);
}
PersonnelManagers.prototype.PreferredNameSet = function (self) {
	this.GetAllowedHumanResources(self);
}
PersonnelManagers.prototype.SurnameSet = function (self) {
	this.GetAllowedHumanResources(self);
}