﻿//#region base classes

class allocatorView {

  constructor(parent, options) {
    //define some basic properties that all views should have
    this.parent = parent

    this.dom = {}
    this.dom.timeColumn = null
    this.dom.viewHeaderColumn = null
    this.dom.viewDataColumn = null

    this.viewData = {}
    this.viewItems = {}

    this.eventStates = {
      mousedDown: false
    }
    this.positions = {
      //mouse cursor
      mouseX: 0,
      mouseY: 0,
      //mouse pointer
      currentX: null,
      currentY: null,
      lastX: null,
      lastY: null,
      distanceX: 0,
      distanceY: 0,
      //channgel and time positions
      channelX: 0,
      channelY: 0,
      //heights
      columnHeight: this.parent.defaultOptions.pixelsPerMinute * 60 * 24,
      bufferedHeight: ((this.parent.defaultOptions.pixelsPerMinute * 60 * 24)),
      //draggy div
      dragDivXStart: 0,
      dragDivYStart: 0,
      dragDivX: 0,
      dragDivY: 0
    }

    this.currentDate = null;
    this.changeCount = 0;

  }

  drawView() { }

  addEventHandlers() { }

  cleanupUI() {
    while (this.parent.dom.viewContainer.firstChild) {
      this.parent.dom.viewContainer.removeChild(this.parent.dom.viewContainer.firstChild);
    };
  };

  redraw() {
    this.cleanupUI();
    this.createItems();
    this.drawView();
  };

  onKeyUp(event) { };
};

//#endregion

//#region allocator

class allocator {
  //this method defines the structure of the allocator class
  constructor(options) {

    //options
    this.defaultOptions = {
      containerID: "allocator",
      minutesPerInterval: 30,
      pixelsPerMinute: 3,
      headerHeight: 30,
      containerWidth: 0,
      containerHeight: 0,
      channelWidth: 175,
      channelBorderWidth: 1,
      timeWidth: 60,
      headNavHeight: 45,
      headNavTotalBorderWidth: 0,
      initialChannels: ["SS1", "SS2", "SS3", "SS4", "SS5", "SS6", "SS7", "SS8", "SS9", "SSM", "SSM2", "SSME", "SSM3"],
      hourHeight: 0,
      dayHeaderHeight: 40,
      venueWidth: 200,
      venueEventHeight: 200,
      seriesHeaderHeight: 30,
      seriesWidth: 280,
      seriesHeight: 180
    };
    this.defaultOptions = $.extend({}, this.defaultOptions, options);
    this.defaultOptions.hourHeight = this.defaultOptions.pixelsPerMinute * 60;

    //criteria
    this.criteria = {
      lastChannelSelection: [],
      lastDate: null,
      channelShortNames: [],
      previousStartDate: moment(),
      previousEndDate: moment(),
      startDate: moment(new Date()),
      endDate: moment(new Date()),
      live: true,
      delayed: true,
      premier: true,
      repeat: true
    };

    //data
    this.datasource = [];
    this.schedules = [];
    this.resourcebookings = [];
    this.channels = [];
    this.channelData = [];
    this.dataSet = {};
    this.onlineUsers = {};

    //other
    this.selectedSchedules = [];

    //ui
    this.dom = {};
    this.dom.container = document.getElementById(this.defaultOptions.containerID);
    this.dom.viewContainer = null;
    this.dom.dragDiv = document.createElement("div");
    this.dom.dragDiv.id = "div-drag-select";
    this.dom.loadingBar = document.createElement("div");
    this.dom.loadingBar.innerHTML = '<div class="cssload-thecube">'
      + '<div class="cssload-cube cssload-c1"></div>'
      + '<div class="cssload-cube cssload-c2"></div>'
      + '<div class="cssload-cube cssload-c4"></div>'
      + '<div class="cssload-cube cssload-c3"></div>'
    '</div>';
    this.dom.settingsButton = null;

    this.dimensions = {
      reservedHeight: (45 + this.defaultOptions.headNavHeight + this.defaultOptions.headNavTotalBorderWidth),
      dayHeight: (this.defaultOptions.hourHeight * 24),
      dayVenueHeight: ((this.defaultOptions.hourHeight * 24) - this.defaultOptions.dayHeaderHeight),
      containerDimensions: {}
    };

    //views
    this.viewOptions = [];
    this.selectedView = "";
    this.views = {
      byDay: null,
      byVenue: null,
      bySeries: null
    };

    //user views
    let contentAccess = ClientData.ROUserSystemAreaList.filter(area => { return (area.SystemID == 2) });
    let satopsAccess = ClientData.ROUserSystemAreaList.filter(area => { return (area.SystemID == 2 && area.ProductionAreaID == 8) });
    let playoutAccess = ClientData.ROUserSystemAreaList.filter(area => { return (area.SystemID == 5) });
    let maximoAccess = ClientData.ROUserSystemAreaList.filter(area => { return (area.SystemID == 2 && area.ProductionAreaID == 6) });
    //add relevant view options
    if (contentAccess.length > 0 && satopsAccess.length == 0) {
      this.viewOptions.push({ value: 'byDay', view: 'By Day' });
      this.viewOptions.push({ value: 'bySeries', view: 'By Series' });
      this.selectedView = 'byDay';
      this.isContentUser = true;
    };
    if (satopsAccess.length > 0) {
      this.viewOptions.push({ value: 'byVenue', view: 'By Venue' });
      this.viewOptions.push({ value: 'bySeries', view: 'By Series' });
      this.selectedView = 'byVenue';
      this.isSatOpsUser = true;
    };
    if (playoutAccess.length > 0) {
      this.viewOptions.push({ value: 'byDay', view: 'By Day' });
      this.selectedView = 'byDay';
      this.isPlayoutUser = true;
    };
    if (maximoAccess.length > 0) {
      this.viewOptions.push({ value: 'byDay', view: 'By Day' });
      this.selectedView = 'byDay';
      this.isMaximoUser = true;
    };
    //{ value: 'byDay', view: 'By Day' },
    //{ value: 'byVenue', view: 'By Venue' },
    //{ value: 'bySeries', view: 'By Series' }

    //controls
    this.dateSelector = null;
    this.channelSelector = null;

    //other
    this.isMultiSystemUser = (ClientData.ROUserSystemList.length > 0);
    this.defaultUserSystem = ClientData.ROUserSystemList[0];
    this.defaultUserArea = null;
    if (this.defaultUserSystem) {
      this.defaultUserArea = ClientData.ROUserSystemAreaList.find(area => { return (area.SystemID == this.defaultUserSystem.SystemID) });
    };
    this.selections = [];
    this.selectedSchedule = function () {
      if (this.selections.length == 1) { return this.selections[0]; } else { return null; }
    };

    ////we need to store the value of the listener, so that it can be removed every time it 
    //this.onKeyUpValue = this.onKeyUp.bind(this);
    window.addEventListener("keyup", this.onKeyUp.bind(this));

    this.init();
  };

  //static
  static get arrowKeys() {
    return {
      LEFT: 37,
      RIGHT: 39
    }
  };

  //this method is used to do all initial setup after the structure has been defined
  init() {

    /*while this is happening, the page will be waiting for a response from signalr,
      once this response has been received, the creation of the control will continue */

    //1. show the loading bar
    this.showLoadingBar()

    //2. create the channel data
    if (ClientData.ROChannelList) {
      this.channelData = ClientData.ROChannelList;
      ClientData.ROChannelList.Iterate((ch, chIndx) => {
        this.channels.push({ id: ch.ChannelShortName, text: ch.ChannelName })
      })
    }

  };

  showLoadingBar() {
    this.dom.container.style.visibility = 'hidden';
    this.dom.loadingBar.className = 'animated fadeIn fast go'
    this.dom.container.parentElement.insertBefore(this.dom.loadingBar, this.dom.container)
  }

  hideLoadingBar() {
    //hide loading bar
    this.dom.loadingBar.className = "animated fadeOut slowest go"
    //set a timeout to give the animation above some time to complete
    setTimeout(() => {
      //remove loading bar
      this.dom.container.parentElement.removeChild(this.dom.loadingBar)
      //show the layout
      this.dom.container.style.visibility = 'visible';
      this.dom.container.className = 'row animated fadeIn fastest go'
    }, 400)
  }

  afterRegistered() {
    this.criteria.channelShortNames = this.getDefaultChannels()
    this.refresh()
  };

  getDefaultChannels() {
    let defaultSystem = ClientData.ROUserSystemList[0],
      systemChannels = ClientData.ROSystemProductionAreaChannelDefaultList.filter(ch => { return defaultSystem.SystemID == ch.SystemID }),
      results = [];

    if (this.isMaximoUser) {
      systemChannels = systemChannels.filter(ch => { return ch.ProductionAreaID == 6 });
    };

    systemChannels.Iterate(function (chan, chanIndx) {
      if (results.indexOf(chan.ChannelShortName) < 0) {
        results.push(chan.ChannelShortName)
      }
    })
    return results
  };

  refresh() {
    let sd = this.criteria.startDate.toDate().format('yyyy MMM dd'),
      ed = this.criteria.endDate.toDate().format('yyyy MMM dd'),
      statuses = [];

    if (this.criteria.live) {
      statuses.push("L")
    }

    if (this.criteria.delayed) {
      statuses.push("D")
    }

    if (this.criteria.premier) {
      statuses.push("P")
    }

    if (this.dom.loadingBar.classList.contains('fadeOut')) {
      this.showLoadingBar()
    }

    this.getAllData()
  }

  getAllData() {
    Promise.all([this.getSchedulesData(), this.getBookingsData()]).then((dataArray) => {
      //set the data properties
      this.schedules = dataArray[0];
      this.resourcebookings = dataArray[1];
      this.genrefResourceBookings = [];
      this.resourcebookings.forEach(item => {
        this.genrefResourceBookings = this.genrefResourceBookings.concat(item.ResourceBookingGenRefNumberList);
      });
      //draw the allocator only
      this.draw();
    })
    //.catch(reason => {
    //  alert(reason)
    //})
  }

  getSchedulesData() {
    return Singular.GetDataStatelessPromise("OBLib.Synergy.SynergyScheduleList", {
      ChannelIDs: null,
      ChannelShortNames: null,
      StartDate: this.criteria.startDate.toDate().format('dd MMM yyyy'),
      EndDate: this.criteria.endDate.toDate().format('dd MMM yyyy'),
      ExcludeHighlights: true
    });
    //this.criteria.channelShortNames
  }

  getBookingsData() {
    return Singular.GetDataStatelessPromise("OBLib.Resources.ResourceBookingList", {
      ResourceTypeIDs: [2, 5],
      StartDate: this.criteria.startDate.toDate().format('dd MMM yyyy'),
      EndDate: this.criteria.endDate.toDate().format('dd MMM yyyy')
    })
  }

  draw() {
    let nonEmptyChannelCount = 0,
      sidebar = document.querySelector('div.cl-sidebar'),
      sideBarDimensions = {};

    this.cleanupUI();

    //try {
    //1. calculate and set dimensions of the container
    sideBarDimensions = sidebar.getBoundingClientRect();
    this.defaultOptions.containerWidth = (screen.width - sideBarDimensions.width);
    this.defaultOptions.containerHeight = (document.body.clientHeight - this.dimensions.reservedHeight);
    this.dom.container.style.width = this.defaultOptions.containerWidth.toString() + 'px';
    this.dom.container.style.height = this.defaultOptions.containerHeight.toString() + 'px';
    this.dimensions.containerDimensions = this.dom.container.getBoundingClientRect();

    //2. draw the header
    this.drawHeader()

    //3. draw the view container
    this.drawViewContainer()

    //4. draw the settings button
    this.dom.settingsButton = document.createElement('button');
    this.dom.settingsButton.className = "btn btn-xs btn-info";
    this.dom.settingsButton.setAttribute('type', 'button');
    this.dom.settingsButton.setAttribute('id', 'settings-button');
    this.dom.settingsButton.innerHTML = '<fa class="fa fa-gears"></fa>';
    this.dom.settingsButton.style.title = 'Settings';
    this.dom.settingsButton.style.position = 'absolute';
    this.dom.settingsButton.style.top = "0px";
    this.dom.settingsButton.style.left = "0px";
    this.dom.settingsButton.style.width = this.defaultOptions.timeWidth.toString() + 'px';
    this.dom.settingsButton.style.height = this.defaultOptions.headerHeight.toString() + 'px';

    //5. draw the current view
    this.drawSelectedView();

    //6. add the draggy thing

    //7. draw the settings modal
    this.drawSettingsModal();

    //8. add the event handlers
    this.addEventHandlers();

    this.hideLoadingBar();
  }

  drawHeader() {

    this.dom.header = document.createElement('div')
    this.dom.header.className = 'header-row'
    this.dom.header.style.height = this.defaultOptions.headNavHeight.toString() + 'px'

    this.dom.leftHeader = document.createElement('div')
    this.dom.leftHeader.className = 'pull-left timeline-header-left'
    this.dom.pageHeader = document.createElement('h2')
    this.dom.pageHeader.innerHTML = 'Synergy Allocator'
    this.dom.pageHeader.id = "allocator-header"

    this.dom.rightHeader = document.createElement('div')
    this.dom.rightHeader.className = 'pull-right timeline-header-right'
    this.dom.currentDatesHeader = document.createElement('h3')
    this.dom.currentDatesHeader.innerHTML = 'None'
    this.dom.currentDatesHeader.id = "current-dates-header"

    this.dom.leftHeader.appendChild(this.dom.pageHeader)
    this.dom.rightHeader.appendChild(this.dom.currentDatesHeader)
    this.dom.header.appendChild(this.dom.leftHeader)
    this.dom.header.appendChild(this.dom.rightHeader)
    this.dom.container.appendChild(this.dom.header)
    this.updateHeaderDate()

  }

  updateHeaderDate() {
    let sd = this.criteria.startDate.toDate(),
      ed = this.criteria.endDate.toDate()

    if (this.criteria.startDate.isSame(this.criteria.endDate, 'day')) {
      this.dom.currentDatesHeader.innerHTML = sd.format("dd MMM yy")
    }
    else {
      this.dom.currentDatesHeader.innerHTML = sd.format("dd MMM yy") + " - " + ed.format("dd MMM yy")
    }
  }

  drawViewContainer() {
    this.dom.viewContainer = document.createElement('div')
    this.dom.viewContainer.className = 'view-container'
    this.dom.viewContainer.style.height = this.dom.container.style.height
    this.dom.container.appendChild(this.dom.viewContainer)
  }

  cleanupSelectedView() {
    switch (this.selectedView) {
      case 'byDay':
        this.views.byDay.cleanupUI();
        break;
      case 'byVenue':
        this.views.byVenue.cleanupUI();
        break;
      case 'bySeries':
        this.views.bySeries.cleanupUI();
        break;
    }
  }

  drawSelectedView() {
    switch (this.selectedView) {
      case 'byDay':
        if (!this.views.byDay) {
          this.views.byDay = new byDayView(this, {});
        } else {
          this.views.byDay.redraw();
        }
        break;
      case 'byVenue':
        if (!this.views.byVenue) {
          this.views.byVenue = new byVenueView(this, {});
        } else {
          this.views.byVenue.redraw();
        }
        break;
      case 'bySeries':
        if (!this.views.bySeries) {
          this.views.bySeries = new bySeriesView(this, {});
        } else {
          this.views.bySeries.redraw();
        }
        break;
    };
  };

  drawSettingsModal() {
    let modalTemplate = //'<div class="modal fade" tabindex="-1" role="dialog">' +
      '<div class="modal-dialog modal-sm" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>' +
      '<h4 class="modal-title">Settings & Criteria</h4>' +
      '</div>' +
      '<div class="modal-body" id="settings-body">' +
      '<div class="row">' +
      '<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
      '<div class="form-horizontal group-border-dashed" id="settings-modal-content-container">' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div class="modal-footer">' +
      '<button type="button" class="btn btn-info btn-flat" id="btnUpdateSettings">Update Settings</button>' +
      '</div>' +
      '</div>' +
      '</div>';

    this.dom.settingsModal = document.createElement('div')
    this.dom.settingsModal.className = "modal fade in md-modal colored-header info md-effect-10 md-show"
    this.dom.settingsModal.setAttribute("tabindex", "-1")
    this.dom.settingsModal.setAttribute("role", "dialog")
    this.dom.settingsModal.id = "settingsModal"
    this.dom.settingsModal.innerHTML = modalTemplate
    this.dom.container.parentElement.appendChild(this.dom.settingsModal)

    this.dom.settingsModalBody = document.getElementById('settings-modal-content-container')

    //this.dom.datesButton = document.createElement('button');
    //this.dom.datesButton.className = 'btn btn-xs btn-default';
    //this.dom.datesButton.setAttribute("type", "button");
    //this.dom.datesButton.setAttribute("id", "bookingsAssigner-selectDates");
    //this.dom.datesButton.innerHTML = `<i class="fa fa-calendar"></i> ${this.criteria.startDate.format("DD MMM YY")} ${this.criteria.endDate.format("DD MMM YY")}`;
    //this.dom.settingsModalBody.appendChild(this.dom.datesButton)

    this.dom.datesContainer = document.createElement('div')
    this.dom.datesContainer.className = "form-group"
    this.dom.datesContainer.innerHTML = `<label class="col-sm-3 control-label">Dates</label>
      <div class="col-md-9 col-sm-9 col-xs-9">
          <button type="button" id="bookingsAssigner-selectDates" class="btn btn-sm btn-default btn-block">
            <i class="fa fa-calendar" ></i> ${this.criteria.startDate.format("DD MMM YYYY")} ${this.criteria.endDate.format("DD MMM YYYY")}
          </button>
      </div>`
    this.dom.settingsModalBody.appendChild(this.dom.datesContainer);

    this.dom.selectedChannelsElement = document.createElement('div')
    this.dom.selectedChannelsElement.className = "form-group"
    this.dom.selectedChannelsElement.innerHTML = '<label class="col-sm-3 control-label">Channels</label>' +
      '<div class=" col-md-9 col-xs-9">' +
      '<div class="btn-group pull-right">' +
      '<button type="button" id="btn-defaultchannels" class="btn btn-sm btn-default">Default</button>' +
      '<button type="button" id="btn-allchannels" class="btn btn-sm btn-default">All</button>' +
      '<button type="button" id="btn-nochannels" class="btn btn-sm btn-default">None</button>' +
      '<button type="button" id="btn-randburgChannels" class="btn btn-sm btn-default">Randburg</button>' +
      '<button type="button" id="btn-samrandChannels" class="btn btn-sm btn-default">Samrand</button>' +
      '</div>' +
      '<select class="channel-select"></select>'
    '</div>'
    this.dom.settingsModalBody.appendChild(this.dom.selectedChannelsElement)


    this.dom.showSuggestionsElement = document.createElement('div')
    this.dom.showSuggestionsElement.className = "form-group"
    this.dom.showSuggestionsElement.innerHTML = '<label class="col-sm-3 control-label">Statuses</label>' +
      '<div class="input-group col-md-9 col-sm-9 col-xs-9">' +
      '<div class="btn-group pull-left">' +
      '<button type="button" id="btn-live" class="btn btn-sm btn-danger"><i class="fa fa-check-square-o"></i> Live</button>' +
      '<button type="button" id="btn-delayed" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Delayed</button>' +
      '<button type="button" id="btn-premier" class="btn btn-sm btn-warning"><i class="fa fa-check-square-o"></i> Premier</button>' +
      '<button type="button" id="btn-repeat" class="btn btn-sm btn-info"><i class="fa fa-check-square-o"></i> Repeat</button>' +
      '</div>' +
      '</div>'
    this.dom.settingsModalBody.appendChild(this.dom.showSuggestionsElement)

    this.dom.showSuggestionsElement = document.createElement('div')
    this.dom.showSuggestionsElement.className = "form-group"
    this.dom.showSuggestionsElement.innerHTML = '<label class="col-sm-3 control-label">Suggestions</label>' +
      '<div class="input-group col-md-9 col-sm-9 col-xs-9">' +
      '<button type="button" class="btn btn-sm btn-primary" id="showSuggestions">Show Suggestions</button>' +
      '</div>'
    this.dom.settingsModalBody.appendChild(this.dom.showSuggestionsElement)

    //instantiate the controls
    //initial setup of date picker
    this.dom.datesButton = document.getElementById("bookingsAssigner-selectDates");
    $(this.dom.datesButton).daterangepicker({
      format: 'DD MMM',
      startDate: this.criteria.startDate,
      endDate: this.criteria.endDate,
      linkedCalendars: true,
      autoUpdateInput: true
    });
    $(this.dom.datesButton).on('show.daterangepicker', (event, picker) => { picker.showCalendars(); });
    $(this.dom.datesButton).on('apply.daterangepicker', (event, picker) => {
      this.criteria.previousStartDate = this.criteria.startDate;
      this.criteria.previousEndDate = this.criteria.endDate;
      this.criteria.startDate = picker.startDate.startOf('day');
      this.criteria.endDate = picker.endDate.endOf('day');
      this.dom.datesButton.innerHTML = `<i class="fa fa-calendar" ></i> ${this.criteria.startDate.format("DD MMM YYYY")} ${this.criteria.endDate.format("DD MMM YYYY")}`;
    });

    this.channelSelector = $('select.channel-select').select2({
      placeholder: 'Select some channels',
      tags: true,
      multiple: true,
      width: '100%',
      data: this.channels,
      allowClear: true
    })
    //set initial value
    this.channelSelector.val(this.getDefaultChannels).trigger('change.select2');

    $(this.dom.settingsModal).on('click', (evt) => {
      switch (evt.target.id) {
        case "btnUpdateSettings":
          this.settingsUpdated()
          break;
        case "showSuggestions":
          this.showBookingSuggestions(evt)
          this.hideSettings()
          break;
        case "btn-defaultchannels":
          this.channelSelector.val(this.getDefaultChannels).trigger('change.select2');
          break;
        case "btn-allchannels":
          this.channelSelector.val(this.defaultOptions.allChannels).trigger('change.select2');
          break;
        case "btn-nochannels":
          this.channelSelector.val([]).trigger('change.select2');
          break;
        case "btn-randburgChannels":
          this.channelSelector.val(this.getChannelsForSite(1)).trigger('change.select2');
          break;
        case "btn-samrandChannels":
          this.channelSelector.val(this.getChannelsForSite(2)).trigger('change.select2');
          break;
        case "btn-live":
          this.criteria.live = !this.criteria.live;
          if (this.criteria.live) {
            event.target.classList.remove('btn-default')
            event.target.classList.add('btn-danger')
            event.target.innerHTML = '<i class="fa fa-check-square-o"></i> Live'
          } else {
            event.target.classList.add('btn-default')
            event.target.classList.remove('btn-danger')
            event.target.innerHTML = 'Live'
          }
          break;
        case "btn-delayed":
          this.criteria.delayed = !this.criteria.delayed;
          if (this.criteria.delayed) {
            event.target.classList.remove('btn-default')
            event.target.classList.add('btn-success')
            event.target.innerHTML = '<i class="fa fa-check-square-o"></i> Delayed'
          } else {
            event.target.classList.add('btn-default')
            event.target.classList.remove('btn-success')
            event.target.innerHTML = 'Delayed'
          }
          break;
        case "btn-premier":
          this.criteria.premier = !this.criteria.premier;
          if (this.criteria.premier) {
            event.target.classList.remove('btn-default')
            event.target.classList.add('btn-warning')
            event.target.innerHTML = '<i class="fa fa-check-square-o"></i> Premier'
          } else {
            event.target.classList.add('btn-default')
            event.target.classList.remove('btn-warning')
            event.target.innerHTML = 'Premier'
          }
          break;
        case "btn-repeat":
          this.criteria.repeat = !this.criteria.repeat;
          if (this.criteria.repeat) {
            event.target.classList.remove('btn-default')
            event.target.classList.add('btn-info')
            event.target.innerHTML = '<i class="fa fa-check-square-o"></i> Repeat'
          } else {
            event.target.classList.add('btn-default')
            event.target.classList.remove('btn-info')
            event.target.innerHTML = 'Repeat'
          }
          break;
      }
    })

  };

  showSettings() {
    this.channelSelector.val(this.criteria.channelShortNames).trigger('change.select2');
    $("#settingsModal").modal()
  }

  hideSettings() {
    $("#settingsModal").modal('hide')
  }

  addEventHandlers() {
    $(this.dom.viewContainer).off('contextmenu');

    this.dom.settingsButton.addEventListener('click', (event) => {
      contextMenu.destroy()
      this.showSettings()
    });

    $(this.dom.viewContainer).on('contextmenu', (event) => {
      //fire off some inital method calls
      event.preventDefault();
      event.stopPropagation();
      contextMenu.destroy();
      contextMenu.init();
      if (!event.ctrlKey) {
        this.resetSelections()
      };

      let options = [];
      let isResourceBooking = event.target.classList.contains('resource-booking');

      if (isResourceBooking) {
        if (event.target.classList.contains('resource-booking')) {
          let bookingData = event.target.businessObject;
          if ([8, 9].indexOf(bookingData.ResourceBookingTypeID) >= 0) {
            options.push({
              text: '<i class="fa fa-trash fa-red"> Delete',
              href: "#", args: { /*nothing special to pass, we will have access to the selectedSchedules already*/ },
              action: async (event) => {
                let prevHTML = event.target.innerHTML;
                event.target.innerHTML = `<i class="fa fa-refresh fa-spin"></i> Deleting`;
                try {
                  let result = await RoomScheduleAreaBO.deleteRoomSchedulesPromise({ ProductionSystemAreaIDs: [bookingData.ProductionSystemAreaID] });
                }
                catch (err) {
                  event.target.innerHTML = prevHTML;
                }
              }
            })
          }
        }
      }
      else {

        //view options
        let viewOpts = {
          text: "Views", subMenu: []
        };
        this.viewOptions.forEach(viewOption => {
          viewOpts.subMenu.push({
            text: viewOption.view,
            href: "#", args: { viewOption },
            action: (event) => {
              this.cleanupSelectedView();
              this.selectedView = viewOption.value;
              this.drawSelectedView();
            }
          })
        });
        options.push(viewOpts);

        //schedule options
        let parentScheduleElement = $(event.target).parents('div.channel-schedule'),
          scheduleElement = null, scheduleItem = null;

        if (event.target.classList.contains('channel-schedule')) {
          scheduleItem = event.target.scheduleItem;
        }
        else if (parentScheduleElement.length == 1) {
          scheduleItem = parentScheduleElement[0].scheduleItem;
        };

        if (scheduleItem && !scheduleItem.isSelected) {
          this.toggleSelect(scheduleItem);
        };

        if (this.selectedSchedules.length > 0) {
          let eventItems = this.selectedSchedules.map(item => {
            return {
              GenRefNumber: item.schedule.GenRefNumber, GenreDesc: item.schedule.GenreDesc,
              SeriesNumber: item.schedule.SeriesNumber, SeriesTitle: item.schedule.SeriesTitle,
              Title: item.schedule.Title, LiveDate: item.schedule.LiveDate,
              StartTime: new Date(item.schedule.ScheduleDateTime),
              EndTime: new Date(item.schedule.ScheduleEndDate),
              SynergyScheduleList: this.selectedSchedules.filter(scheduleObject => { return (item.schedule.GenRefNumber == scheduleObject.schedule.GenRefNumber) })
                .map(scheduleObject => { return scheduleObject.schedule })
            };
          });
          //convert each object to a stringified version (so that the items are no refrenced by value and not address) 
          //then create a new set from this (which will ensure uniqueness of values)
          //then finally convert back to an object
          let uniqueEventItems = eventItems.uniqueObjects();
          //if we only have 1 unique genref
          //if (uniqueGenRefs.length == 1) {
          options.push({
            text: 'Create Booking',
            href: "#", args: { /*nothing special to pass, we will have access to the selectedSchedules already*/ },
            action: (event) => {
              this.views.byDay.createBookings(uniqueEventItems, {
                SystemID: this.defaultUserSystem.SystemID,
                ProductionAreaID: (this.defaultUserArea ? this.defaultUserArea.ProductionAreaID : null)
              });
            } /* this.bulkBookingSelected.call(me, this, event) */
          });
          //} 
          //else if (uniqueGenRefs.length > 1) { 
          //          options.push({
          //            text: 'Create Booking',
          //            href: "#", args: { /*nothing special to pass, we will have access to the selectedSchedules already*/ },
          //            action: function (event) { } /* this.bulkBookingSelected.call(me, this, event) */
          //          })
          //        }
        }

        //satops options
        if (this.isSatOpsUser && this.selectedView == "byVenue") {
          options.push({
            text: 'Manual Import', href: "#",
            args: {},
            action: (contextMenuEvent) => {
              let eventItem = {
                GenRefNumber: event.target.eventItem.GenRefNumber, GenreDesc: event.target.eventItem.GenreDesc,
                SeriesNumber: event.target.eventItem.SeriesNumber, SeriesTitle: event.target.eventItem.SeriesTitle,
                Title: event.target.eventItem.Title, LiveDate: event.target.eventItem.LiveDate,
                StartTime: new Date(event.target.eventItem.ScheduleDateTime),
                EndTime: new Date(event.target.eventItem.ScheduleEndDate),
                SynergyScheduleList: []
              };
              this.views.byVenue.createEquipmentBookings([eventItem], {
                SystemID: this.defaultUserSystem.SystemID,
                ProductionAreaID: (this.defaultUserArea ? this.defaultUserArea.ProductionAreaID : null)
              });
            }
          });
        };

      };

      contextMenu.show('.inline-menu', options, event);
    });

  };

  //getDefaultChannels() {
  //  var defaultSystem = ClientData.ROUserSystemList[0],
  //    systemProductionAreas = ClientData.ROSystemProductionAreaList.Filter("SystemID", defaultSystem.SystemID),
  //    results = [];

  //  var jSONResults = [];

  //  systemProductionAreas.Iterate(function (spa, spaInd) {
  //    if (spa.ChannelDefaultsJSON.length > 0) {
  //      jSONResults = JSON.parse(spa.ChannelDefaultsJSON)
  //    }

  //    jSONResults.Iterate(function (chn, chnIndx) {
  //      if (results.indexOf(chn.ChannelShortName) < 0) {
  //        results.push(chn.ChannelShortName)
  //      }
  //    });
  //    jSONResults = [];
  //  });
  //  return results
  //}

  getChannelsForSite(siteID) {
    let results = [],
      siteChannels = ClientData.ROSystemProductionAreaChannelDefaultList.Filter("SiteID", siteID);
    siteChannels.forEach((chan, chanIndx) => {
      if (results.indexOf(chan.ChannelShortName) < 0) {
        results.push(chan.ChannelShortName)
      }
    })
    return results
  };

  settingsUpdated() {
    this.criteria.channelShortNames = (this.channelSelector.val() ? this.channelSelector.val() : []);
    this.hideSettings();
    this.refresh();
  };

  cleanupUI() {
    while (this.dom.container.firstChild) {
      this.dom.container.removeChild(this.dom.container.firstChild);
    }
    if (this.dom.settingsModal) {
      this.dom.container.parentElement.removeChild(this.dom.settingsModal)
    }
  };

  toggleSelect(scheduleItem) {
    if (scheduleItem.canSelect) {
      scheduleItem.isSelected = !scheduleItem.isSelected
      if (scheduleItem.isSelected) {
        this.selectedSchedules.push(scheduleItem)
      } else {
        this.selectedSchedules.pop(scheduleItem)
      }
      this.views.byDay.updateSchedule(scheduleItem)
    }
  };

  processSignalRChanges(stringifiedChanges) {
    const objectified = JSON.parse(stringifiedChanges);
    const updatedBookings = objectified.Data.UpdatedBookings;
    const addedBookings = objectified.Data.AddedBookings;
    const removedBookingIDs = objectified.Data.RemovedBookingIDs;
    if (this.views.byVenue) {
      this.views.byVenue.processSignalRChanges(updatedBookings, addedBookings, removedBookingIDs);
    };
    if (this.views.byDay) {
      this.views.byDay.processSignalRChanges(updatedBookings, addedBookings, removedBookingIDs);
    };
    if (this.views.bySeries) {
      this.views.bySeries.processSignalRChanges(updatedBookings, addedBookings, removedBookingIDs);
    };
  };

  onKeyUp(event) {
    event.cancelBubble = true;
    event.preventDefault();
    event.stopPropagation();
    switch (this.selectedView) {
      case 'byDay':
        this.views.byDay.onKeyUp(event);
        break;
      case 'byVenue':
        this.views.byVenue.onKeyUp(event);
        break;
      case 'bySeries':
        this.views.bySeries.onKeyUp(event);
        break;
    };
  };

  static showByEventModal() {
    $("#ByEventModal").off('shown.bs.modal')
    $("#ByEventModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentByEventTemplate());
    })
    $("#ByEventModal").modal();
  }

  static hideByEventModal() {
    $("#ByEventModal").off('hidden.bs.modal');
    $("#ByEventModal").on('hidden.bs.modal', function () {
      ViewModel.ByEventTemplate(null);
    })
    $("#ByEventModal").modal('hide');
  }

  resetSelections() {
    this.selectedSchedules.forEach(selectedSchedule => {
      this.toggleSelect(selectedSchedule);
    });
  };
};

class byDayView extends allocatorView {
  constructor(parent, options) {
    super(parent, options)

    this.viewItems.channels = [];
    this.viewItems.days = [];
    this.viewItems.schedules = [];
    this.viewItems.dayChannels = [];

    this.createItems();
    this.drawView();
  }

  createItems() {
    let existingItems = [];

    this.viewItems.channels = []
    this.viewItems.days = []
    this.viewItems.schedules = []

    let day = null, scheduleItem = null, dayItem = null, channelItem = null;
    let schedules = this.parent.schedules.filter(schedule => { return (this.parent.criteria.channelShortNames.length == 0 || (this.parent.criteria.channelShortNames.indexOf(schedule.ChannelShortName) >= 0)) });

    schedules.forEach(schedule => {
      day = null, scheduleItem = null, dayItem = null, channelItem = null;

      //channel items
      existingItems = this.viewItems.channels.filter(item => { return item.channel.ChannelShortName === schedule.ChannelShortName })
      if (existingItems.length == 0) {
        let channel = this.parent.channelData.filter(item => { return item.ChannelShortName === schedule.ChannelShortName })
        channel = channel[0]
        channelItem = this.createChannelItem(channel, this.viewItems.channels.length + 1)
        this.viewItems.channels.push(channelItem)
      }
      else {
        channelItem = existingItems[0]
      }

      //day items
      existingItems = []
      day = new Date(schedule.ScheduleDateTime)
      day.setHours(0, 0, 0, 0)
      existingItems = this.viewItems.days.filter((item, dayIndex) => { return item.day.getTime() === day.getTime() })
      if (existingItems.length == 0) {
        dayItem = this.createDayItem(day, this.viewItems.days.length + 1)
        this.viewItems.days.push(dayItem)
      }
      else {
        dayItem = existingItems[0]
      }

      //schedule items
      scheduleItem = this.createScheduleItem(schedule, channelItem, dayItem)
      this.viewItems.schedules.push(scheduleItem)
    })

  }

  createChannelItem(channel) {
    return { channel: channel, dom: { channelElement: null } }
  }

  createScheduleItem(schedule, channel, day) {
    return {
      schedule: schedule,
      channel: channel,
      day: day,
      dom: {
        scheduleElement: null, eventInfoElement: null, resourceBookingsElement: null
      },
      canSelect: true,
      isSelected: false
    }
  }

  createDayItem(day, sortNumber) {
    return { day: day, sortNumber: sortNumber, dom: { dayElement: null, dayHeader: null, schedulesElement: null } }
  }

  drawView() {

    while (this.parent.dom.viewContainer.firstChild) {
      this.parent.dom.viewContainer.removeChild(this.parent.dom.viewContainer.firstChild);
    };

    this.parent.dom.viewContainer.appendChild(this.parent.dom.settingsButton)
    this.dom.timeColumn = document.createElement('div')
    this.dom.timeColumn.className = "time-column"
    this.dom.viewHeaderColumn = document.createElement('div')
    this.dom.viewHeaderColumn.className = "view-header"
    this.dom.viewDataColumn = document.createElement('div')
    this.dom.viewDataColumn.className = "view-data"

    //view time
    this.dom.timeColumn.style.top = this.parent.defaultOptions.headerHeight.toString() + 'px'
    this.dom.timeColumn.style.left = '0px'
    this.dom.timeColumn.style.width = this.parent.defaultOptions.timeWidth.toString() + 'px'

    //view header
    this.dom.viewHeaderColumn.style.top = '0px'
    this.dom.viewHeaderColumn.style.left = (this.parent.defaultOptions.timeWidth - 1).toString() + 'px' //TODO border width to be accounted for
    this.dom.viewHeaderColumn.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px'
    this.dom.viewHeaderColumn.style.width = (this.viewItems.channels.length * this.parent.defaultOptions.channelWidth).toString() + 'px'

    //view data
    this.dom.viewDataColumn.style.top = this.parent.defaultOptions.headerHeight.toString() + 'px'
    this.dom.viewDataColumn.style.left = this.parent.defaultOptions.timeWidth.toString() + 'px'
    this.dom.viewDataColumn.style.height = this.parent.dimensions.dayHeight.toString() + 'px'
    this.dom.viewDataColumn.style.width = (this.viewItems.channels.length * this.parent.defaultOptions.channelWidth).toString() + 'px'

    //append the 3 main elements to the view container
    this.parent.dom.viewContainer.appendChild(this.dom.viewHeaderColumn)
    this.parent.dom.viewContainer.appendChild(this.dom.timeColumn)
    this.parent.dom.viewContainer.appendChild(this.dom.viewDataColumn)

    //draw the headers

    //draw the times column
    this.viewItems.days.forEach(day => {
      //for each day, we will create the time intervals
      let startHour = 0, hourNode = null, hourNodeDimensions = {};
      while (startHour < 24) {
        //create our interval element
        hourNode = null
        hourNode = document.createElement('div')
        //if this is the first interval/start of the day
        if (startHour == 0) {
          //add the title of the current day
          let dayNameNode = null
          dayNameNode = document.createElement('div')
          dayNameNode.innerHTML = day.day.format('ddd dd')
          dayNameNode.title = day.day.format('ddd dd MMM yyyy')
          dayNameNode.className = 'day-name'
          hourNode.appendChild(dayNameNode)
          day.dom.firstHourNode = hourNode
        }
        else if (startHour > 0 && startHour < 10) {
          hourNode.innerHTML = '0' + startHour.toString() + ':00'
        }
        else if (startHour >= 10) {
          hourNode.innerHTML = startHour.toString() + ':00'
        }
        hourNode.style.position = 'relative'
        hourNode.style.height = this.parent.defaultOptions.hourHeight.toString() + 'px'
        hourNode.className = 'hour-node'
        this.dom.timeColumn.appendChild(hourNode)
        startHour += 1
      }
    })

    //draw schedules for each day
    this.viewItems.days.forEach((day, dayIndex) => {
      //fill out our object
      this.drawDay(day)
      //append to view
      this.dom.viewDataColumn.appendChild(day.dom.dayElement)
    })

    //draw channel header as well as a channel column for each day
    this.viewItems.channels.forEach((channel, channelIndex) => {
      channel.dom.channelElement = document.createElement('div')
      channel.dom.channelElement.className = "venue-header"
      channel.dom.channelElement.innerHTML = channel.channel.ChannelName
      channel.dom.channelElement.title = channel.channel.ChannelName
      channel.dom.channelElement.style.width = this.parent.defaultOptions.channelWidth.toString() + 'px'
      channel.dom.channelElement.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px'
      channel.dom.channelElement.style.top = '0px'
      channel.dom.channelElement.style.left = (channelIndex * this.parent.defaultOptions.channelWidth).toString() + 'px'
      this.dom.viewHeaderColumn.appendChild(channel.dom.channelElement)
      //add a channel header for each day
      this.viewItems.days.forEach(day => {
        let dimensions = day.dom.dayElement.getBoundingClientRect()
        let daychannelElement = document.createElement('div')
        daychannelElement.className = "channel-column"
        //daychannel.innerHTML = channel.channel.ChannelName
        //channelHeader.title = channel.channel.ChannelName
        daychannelElement.style.width = this.parent.defaultOptions.channelWidth.toString() + 'px'
        daychannelElement.style.height = '4320px' //dimensions.height.toString() + 'px'
        //daychannel.style.top = '40px'
        daychannelElement.style.left = (channelIndex * this.parent.defaultOptions.channelWidth).toString() + 'px'
        day.dom.dayElement.appendChild(daychannelElement)
        let dayChannel = { day: day, channel: channel, dayChannelElement: daychannelElement }
        this.viewItems.dayChannels.push(dayChannel)
      })
    })

    this.viewItems.schedules.forEach(schedule => {
      this.drawSchedule(schedule)
    });

    this.addEventHandlers();

  };

  drawDay(dayItem) {

    //day element
    dayItem.dom.dayElement = document.createElement('div');
    dayItem.dom.dayElement.className = "day";
    //dayItem.dom.dayHeader = document.createElement('div')
    //dayItem.dom.dayHeader.className = "day-header"
    //dayItem.dom.dayHeader.innerHTML = dayItem.day.format("ddd dd MMMM yyyy")
    //dayItem.dom.dayHeader.style.height = this.parent.defaultOptions.dayHeaderHeight.toString() + 'px'
    //dayItem.dom.dayElement.appendChild(dayItem.dom.dayHeader)

    let dimensions = dayItem.dom.firstHourNode.getBoundingClientRect();
    dayItem.dom.dayElement.style.top = ((dayItem.sortNumber - 1) * (dimensions.height * 24)).toString() + 'px';
    dayItem.dom.dayElement.style.height = (dimensions.height - 60).toString() + 'px';

    //day schedule (by venue)
    this.dom.schedulesElement = document.createElement('div');
    this.dom.schedulesElement.className = "schedule-by-venue";

  }

  updatePosition(newX, newY) {
    //new x of header = new x of channel
    this.positions.channelX = newX
    this.dom.viewDataColumn.style.left = (this.parent.defaultOptions.timeWidth + this.positions.channelX).toString() + 'px'
    this.dom.viewHeaderColumn.style.left = (this.parent.defaultOptions.timeWidth + this.positions.channelX).toString() + 'px'
    //new y of time = new y of channel
    this.positions.channelY = newY
    this.dom.viewDataColumn.style.top = (this.parent.defaultOptions.headerHeight + this.positions.channelY).toString() + 'px'
    this.dom.timeColumn.style.top = (this.parent.defaultOptions.headerHeight + this.positions.channelY).toString() + 'px'
  }

  addEventHandlers() {
    //remove old handlers
    $(this.parent.dom.viewContainer).off('mousewheel DOMMouseScroll');
    $(this.parent.dom.viewContainer).off('pointerdown pointermove pointerup pointerleave');
    $(this.parent.dom.viewContainer).off('click');
    $(this.parent.dom.viewContainer).off('mouseenter mouseleave');
    $(window).off('resize');
    $(window).off('onkeydown');
    $(window).off('onkeyup');

    //add the handlers
    $(this.parent.dom.viewContainer).on('mousewheel DOMMouseScroll', (event) => {
      event.preventDefault()
      if (event.ctrlKey) { return }
      this.positions.distanceY = (-1 * event.originalEvent.deltaY);
      let movementY = (this.positions.channelY + this.positions.distanceY);
      this.positions.channelY = movementY;
      if (this.positions.channelY > 0) { this.positions.channelY = 0; }
      this.updatePosition(this.positions.channelX, this.positions.channelY);
    })

    $(this.parent.dom.viewContainer).on('pointerdown pointermove pointerup pointerleave', (event) => {
      event.preventDefault();
      this.positions.mouseX = event.clientX;
      this.positions.mouseY = event.clientY;

      if (event.type == 'pointerdown') {
        this.eventStates.mousedDown = true;
        //this.setDragDivStartPosition(event)
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = event.clientX;
          this.positions.lastY = event.clientY;
          this.positions.currentX = event.clientX;
          this.positions.currentY = event.clientY;
        }
        return
      }

      if (event.type == 'pointerleave') {
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = this.positions.channelX;
          this.positions.lastY = this.positions.channelY;
          this.positions.currentX = this.positions.channelX;
          this.positions.currentY = this.positions.channelY;
        }
        return
      }

      if (event.type == 'pointerup') {
        this.eventStates.mousedDown = false;
        //this.resetDragDiv()
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = this.positions.channelX;
          this.positions.lastY = this.positions.channelY;
          this.positions.currentX = this.positions.channelX;
          this.positions.currentY = this.positions.channelY;
        }
        return
      }

      if (event.type == 'pointermove') {
        if (event.ctrlKey && this.eventStates.mousedDown) {
          //update dimensions of drag-select div
          //this.updateDragDivDimensions(event)
        }
        else if (this.eventStates.mousedDown) {
          //move the channel thing
          //position data
          this.positions.lastX = this.positions.currentX;
          this.positions.lastY = this.positions.currentY;
          this.positions.currentX = event.clientX;
          this.positions.currentY = event.clientY;

          //calculate distances moved
          this.positions.distanceX = 0;
          this.positions.distanceX += Math.sqrt(Math.pow(this.positions.lastX - this.positions.currentX, 2));
          if (this.positions.currentX < this.positions.lastX) {
            this.positions.distanceX = (-1 * this.positions.distanceX);
          };

          this.positions.distanceY = 0;
          this.positions.distanceY += Math.sqrt(Math.pow(this.positions.lastY - this.positions.currentY, 2));
          if (this.positions.currentY < this.positions.lastY) {
            this.positions.distanceY = (-1 * this.positions.distanceY);
          };

          //determine new x of channel
          var moveX = (this.positions.channelX + this.positions.distanceX)
          if (moveX > 0) { moveX = 0 };

          //determine new y of channel
          var moveY = (this.positions.channelY + this.positions.distanceY)
          if (moveY > 0) { moveY = 0 };

          this.updatePosition(moveX, moveY);
        }
      }

    })

    $(this.parent.dom.viewContainer).on('click', function (event) {
      //contextMenu.destroy()
      //contextMenu.hide()
    });

    $(this.parent.dom.viewContainer).on('click', 'div.channel-schedule', (event) => {
      contextMenu.destroy()
      if (event.ctrlKey) {
        this.parent.toggleSelect(event.currentTarget.scheduleItem)
      } else {
        //  if (event.target.classList.contains('room-schedule')) {
        //    //this.toggleMenu.call(me, event)
        //    //this.editRoomSchedule.call(me, event)
        //    RoomScheduleAreaBO.get({
        //      criteria: {
        //        RoomScheduleID: event.target.businessObject.RoomScheduleID,
        //        ProductionSystemAreaID: event.target.businessObject.ProductionSystemAreaID,
        //      },
        //      onSuccess: function (response) {
        //        RoomScheduleAreaBO.showModal(response)
        //      }
        //    })
        //  } else if (event.target.classList.contains('event-info') || event.target.parentElement.classList.contains('event-info')) {
        //    //this.newRoomSchedule.call(me, event)
        //    this.showChannelScheduleModal()
        //  }
        //  //else if (event.target.classList.contains('room-schedule-edit')) {
        //  //  this.editRoomSchedule.call(me, event)
        //  //} else if (event.target.classList.contains('room-schedule-delete')) {
        //  //  this.deleteRoomSchedule.call(me, event)
        //  //}
      }
    });

    $(this.parent.dom.viewContainer).on('click', 'button#settings-button', (event) => {
      contextMenu.destroy();
      this.parent.showSettings();
    });

    $(this.parent.dom.viewContainer).on('click', 'div.resource-booking', async (event) => {
      try {
        let bookingdata = event.target.businessObject;
        //room bookings
        if ([8, 9].indexOf(bookingdata.ResourceBookingTypeID) >= 0) {
          let data = await RoomScheduleAreaBO.getPromise({ RoomScheduleID: bookingdata.RoomScheduleID, ProductionSystemAreaID: bookingdata.ProductionSystemAreaID });
          ViewModel.CurrentRoomScheduleArea.Set(data[0]);
          RoomScheduleAreaBO.showModal();
        };
        //feed bookings
        if (bookingdata.ResourceBookingTypeID == 10) {

        };
      }
      catch (err) {

      }
    });

    $(this.parent.dom.viewContainer).on('mouseenter mouseleave', 'div.room-schedule', function (event) {
      //switch (event.type) {
      //  case "mouseenter":
      //    this.onMouseOver.call(me, event)
      //    break;
      //  case "mouseleave":
      //    this.onMouseOut.call(me, event)
      //    break;
      //}
    });

    $(window).on('resize', function () {
      //this.dom.container.style.height = (document.body.clientHeight - this.positions.reservedHeight).toString() + 'px'
      //this.rowObj.element.style.height = this.dom.container.style.height
    });

    $(window).on('keydown onkeydown', function (event) {
      //this.ctrlHeld = event.ctrlKey
    });

    //$(window).on('keyup onkeyup', function (event) {
    //  //this.ctrlHeld = event.ctrlKey
    //  //this.eventStates.mousedDown = this.ctrlHeld
    //  //this.positions.dragDivXStart = this.positions.mouseX
    //  //this.positions.dragDivYStart = this.positions.mouseY
    //  //if (!this.ctrlHeld) {
    //  //  this.resetDragDiv()
    //  //}
    //});

    $("#BulkBookingModal").on('hidden.bs.modal', function () {
      this.resetSelections()
      //this.resetSelectableStates()
    });
  };

  drawSchedule(scheduleObject) {
    let dayChannel = this.viewItems.dayChannels.filter(item => { return (item.channel === scheduleObject.channel && item.day === scheduleObject.day) });
    dayChannel = dayChannel[0]

    scheduleObject.dom.scheduleElement = document.createElement('div')
    scheduleObject.dom.scheduleElement.className = 'channel-schedule'
    scheduleObject.dom.scheduleElement.id = 'schedule-' + scheduleObject.schedule.ScheduleNumber.toString()
    scheduleObject.dom.scheduleElement.scheduleItem = scheduleObject

    scheduleObject.dom.eventInfoElement = document.createElement('div')
    scheduleObject.dom.eventInfoElement.className = 'event-info'

    scheduleObject.dom.resourceBookingsElement = document.createElement('div')
    scheduleObject.dom.resourceBookingsElement.className = 'booking-list'

    scheduleObject.dom.scheduleElement.appendChild(scheduleObject.dom.eventInfoElement)
    scheduleObject.dom.scheduleElement.appendChild(scheduleObject.dom.resourceBookingsElement)
    dayChannel.dayChannelElement.appendChild(scheduleObject.dom.scheduleElement)

    this.updateSchedule(scheduleObject)

    let resourceBookings = this.parent.resourcebookings.filter(item => {
      return (item.ResourceBookingGenRefNumberList.filter(item2 => { return (item2.GenRefNumber == scheduleObject.schedule.GenRefNumber) }).length > 0)
    })

    if (resourceBookings.length > 0) {
      resourceBookings.forEach(item => {
        this.drawResourceBookingElement(scheduleObject.dom.resourceBookingsElement, item)
      })
    }

  }

  updateSchedule(scheduleObject) {

    //determine new day and channel
    let dayChannel = this.viewItems.dayChannels.filter(item => { return (item.channel === scheduleObject.channel && item.day === scheduleObject.day) })
    dayChannel = dayChannel[0]

    //if channel or day has changed then we need to do some magic
    //TODO:

    //calculate heights
    let scheduleHeight = (scheduleObject.schedule.TotalMinutes * this.parent.defaultOptions.pixelsPerMinute)

    //calculate positions
    let startPixel = (scheduleObject.schedule.StartMinute * this.parent.defaultOptions.pixelsPerMinute)

    //update dimensions
    scheduleObject.dom.scheduleElement.style.top = startPixel.toString() + 'px'
    scheduleObject.dom.scheduleElement.style.height = scheduleHeight.toString() + 'px'
    scheduleObject.dom.scheduleElement.style.width = (this.parent.defaultOptions.channelWidth - this.parent.defaultOptions.channelBorderWidth).toString() + 'px'

    //set the color based on event status
    if (scheduleObject.schedule.EventStatus == 'L') { scheduleObject.dom.scheduleElement.className += ' live'; }
    else if (scheduleObject.schedule.EventStatus == 'D') { scheduleObject.dom.scheduleElement.className += ' delayed'; }
    else if (scheduleObject.schedule.EventStatus == 'P') { scheduleObject.dom.scheduleElement.className += ' premier'; }
    else if (scheduleObject.schedule.EventStatus == 'R') { scheduleObject.dom.scheduleElement.className += ' repeat'; }

    if (scheduleObject.isSelected) {
      scheduleObject.dom.scheduleElement.classList.add("selected")
    } else {
      scheduleObject.dom.scheduleElement.classList.remove("selected")
    }

    //set titles and HTML
    let st = new Date(scheduleObject.schedule.ScheduleDateTime).format('HH:mm'),
      et = new Date(scheduleObject.schedule.ScheduleEndDate).format('HH:mm');

    if (scheduleHeight <= 50) {
      scheduleObject.dom.eventInfoElement.innerHTML = '<p class="schedule-short-text" title="' + scheduleObject.schedule.SeriesTitle + '">' + scheduleObject.schedule.SeriesTitle + '</p>'
      scheduleObject.dom.eventInfoElement.title = st + " - " + et + '\n'
        + scheduleObject.schedule.SeriesTitle + '\n'
        + scheduleObject.schedule.Title
    }
    else if (scheduleHeight > 50 && scheduleHeight < 75) {
      scheduleObject.dom.eventInfoElement.innerHTML = '<p class="schedule-short-text">' + st + " - " + et + '</p>'
      scheduleObject.dom.eventInfoElement.title = st + " - " + et + '\n'
        + scheduleObject.schedule.SeriesTitle + '\n'
        + scheduleObject.schedule.Title
    }
    else if (scheduleHeight >= 75 && scheduleHeight <= 100) {
      scheduleObject.dom.eventInfoElement.innerHTML = '<p class="schedule-short-text">' + st + " - " + et + '</p>'
        + '<p class="schedule-short-text" title="' + scheduleObject.schedule.SeriesTitle + '">' + scheduleObject.schedule.SeriesTitle + '</p>'
        + '<p class="schedule-short-text" title="' + scheduleObject.schedule.Title + '">' + scheduleObject.schedule.Title + '</p>'
      scheduleObject.dom.scheduleElement.appendChild(scheduleObject.dom.eventInfoElement)
      scheduleObject.dom.scheduleElement.className += ' schedule-medium'
    }
    else {
      scheduleObject.dom.eventInfoElement.innerHTML = '<p class="wrap-text">' + st + ' - ' + et + '</p>'
        + '<p class="wrap-text" title="' + scheduleObject.schedule.SeriesTitle + '">' + scheduleObject.schedule.SeriesTitle + '</p>'
        + '<p class="wrap-text" title="' + scheduleObject.schedule.Title + '">' + scheduleObject.schedule.Title + '</p>'
    }

    //TODO: determine if scrollbars needed for booking list (most of the time it won't be)
    //scheduleObject.dom.scheduleElement
    //scheduleObject.dom.eventInfoElement
    //scheduleObject.dom.resourceBookingsElement

    //TODO: create resource booking item objects
    //schedNode.appendChild(iconInfo)
    //roomSchedules = [], //this.resourcebookings.Filter("GenRefNumber", scheduleObject.schedule.GenRefNumber),
    ////get the room schedules
    //roomSchedules = this.checkRoomSchedules(scheduleObject.schedule, roomSchedules)
    //var roomScheduleItems = [],
    //  roomSchedItem = null
    //roomSchedules.Iterate(function (roomSched, rsIndx) {
    //  roomSchedItem = {
    //    resourceBookingID: roomSched.ResourceBookingID,
    //    GenRefNumber: roomSched.GenRefNumber,
    //    roomScheduleID: roomSched.RoomScheduleID,
    //    businessObject: roomSched,
    //    element: this.drawRoomSchedule(roomInfo, roomSched)
    //  }
    //  roomScheduleItems.push(roomSchedItem)
    //  this.roomScheduleItems.push(roomSchedItem)
    //})
    //scheduleObject.dom.iconInfoElement = iconInfo //schedResult.iconInfoElement

  }

  createBookings(eventItems, options) {
    ViewModel.CurrentByEventTemplate(null);
    ViewModel.CurrentByEventTemplate(new ByEventTemplateObject());
    ViewModel.CurrentByEventTemplate().DefaultSystemID(options.SystemID);
    ViewModel.CurrentByEventTemplate().DefaultProductionAreaID(options.ProductionAreaID);
    //ViewModel.CurrentByEventTemplate().EventItemList.Set(eventItems)
    //create a room schedule for each object
    eventItems.forEach(item => {
      let newRoomSchedule = ViewModel.CurrentByEventTemplate().RoomScheduleList.AddNew(),
        sd = moment(item.StartTime),
        ed = moment(item.EndTime);

      //populate the room schedule
      newRoomSchedule.GenRefNumber(item.GenRefNumber);
      newRoomSchedule.GenreDesc(item.GenreDesc);
      newRoomSchedule.SeriesTitle(item.SeriesTitle);
      newRoomSchedule.Title(item.Title);
      newRoomSchedule.ProductionTitle(newRoomSchedule.Title());
      newRoomSchedule.SystemID(options.SystemID);
      newRoomSchedule.ProductionAreaID(options.ProductionAreaID);
      newRoomSchedule.CallTime(null);
      newRoomSchedule.StartTime(sd.toDate());
      newRoomSchedule.EndTime(ed.toDate());
      newRoomSchedule.WrapTime(null);
      newRoomSchedule.ProductionID(null);
      newRoomSchedule.IsOwner(true);
      newRoomSchedule.ProductionAreaStatusID(1);
      newRoomSchedule.ProductionAreaStatus("New");
      newRoomSchedule.StatusCssClass("New");
      newRoomSchedule.RoomScheduleTypeID(1);

      //populate the channels
      item.SynergyScheduleList.forEach(channel => {
        let newChannel = newRoomSchedule.RoomScheduleScheduleNumberList.AddNew();
        newChannel.ScheduleNumber(channel.ScheduleNumber);
        newChannel.ChannelShortName(channel.ChannelShortName);
      });

    })
    this.showModal()
  }

  showModal() {
    $("#ByEventModal").off('shown.bs.modal')
    $("#ByEventModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentByEventTemplate());
    })
    $("#ByEventModal").modal();
  }

  hideModal() {
    $("#ByEventModal").off('hidden.bs.modal');
    $("#ByEventModal").on('hidden.bs.modal', function () {
      ViewModel.ByEventTemplate(null);
    })
    $("#ByEventModal").modal('hide');
  }

  processSignalRChanges(updatedBookings, addedBookings, removedBookingIDs) {

    //Process the updates
    updatedBookings.forEach((updBooking, indx) => {
      //get the event items with this genrefnumber
      let elems = document.querySelectorAll('[bookingID="' + updBooking.ResourceBookingID.toString() + '"]');
      elems.forEach(elem => {
        this.updateResourceBookingElement(updBooking, elem);
      });
    });

    //Process the additions
    addedBookings.forEach((addedBooking, indx) => {
      //get the event items with this genrefnumber
      let scheduleItems = this.viewItems.schedules.filter(eItem => {
        return (addedBooking.ResourceBookingGenRefNumberList.filter(gItem => {
          return (gItem.GenRefNumber == eItem.schedule.GenRefNumber)
        }).length > 0)
      });
      scheduleItems.forEach(scheduleItem => {
        this.drawResourceBookingElement(scheduleItem.dom.resourceBookingsElement, addedBooking);
      });
    });

    //Process the deletes
    removedBookingIDs.forEach((resourceBookingID, indx) => {
      let elems = this.dom.viewDataColumn.querySelectorAll('[bookingID="' + resourceBookingID.toString() + '"]');
      elems.forEach(elem => {
        this.removeResourceBookingElement(elemt);
      });
    });

  };

  drawResourceBookingElement(container, resourceBooking) {
    let resourceBookingElement = document.createElement('div');
    this.updateResourceBookingElement(resourceBooking, resourceBookingElement);
    container.appendChild(resourceBookingElement);
  };

  updateResourceBookingElement(resourceBooking, elem) {
    let st = new Date(resourceBooking.StartDateTime).format('HH:mm'),
      et = new Date(resourceBooking.EndDateTime).format('HH:mm');
    let schedules = this.parent.schedules.filter(schedule => { return (schedule.GenRefNumber == resourceBooking.GenRefNumber) }),
      comparisonSchedule = null;

    //find a schedule to compare the roomschedule to
    if (schedules.length == 1) {
      comparisonSchedule = schedules[0];
    }
    else if (schedules.length > 1) {
      var onlyLive = schedules.Filter("EventStatus", "L");
      var onlyDelayed = schedules.Filter("EventStatus", "D");
      var onlyPremier = schedules.Filter("EventStatus", "P");
      if (onlyLive.length > 0) {
        comparisonSchedule = onlyLive[0]
      }
      else if (onlyDelayed.length > 0) {
        comparisonSchedule = onlyDelayed[0]
      }
      else if (onlyPremier.length > 0) {
        comparisonSchedule = onlyPremier[0]
      }
    };

    let timesMatch = true;
    if (comparisonSchedule) {
      let rbSD = moment(resourceBooking.StarDateTime);
      let rbED = moment(resourceBooking.StarDateTime);
      let schedSD = moment(resourceBooking.StarDateTime);
      let schedED = moment(resourceBooking.StarDateTime);
      timesMatch = (rbSD.isSame(schedSD, 'minute') || rbED.isSame(schedED, 'minute'));
    };

    elem.businessObject = resourceBooking;
    elem.title = (resourceBooking.PopoverContent.length > 0 ? resourceBooking.PopoverContent : st + ' - ' + et);
    elem.setAttribute('bookingID', resourceBooking.ResourceBookingID.toString());
    if (!timesMatch) {
      elem.innerHTML = `<i class="fa fa-warning fa-orange"></i> ${resourceBooking.ResourceName}`;
    } else {
      elem.innerHTML = `${resourceBooking.ResourceName}`;
    };

    //class modification
    elem.className = '';
    // restarting the animation
    // without the following line of code, restarting the animation wouldn't work.
    void elem.offsetWidth;
    elem.className = 'resource-booking short-text'
    requestAnimationFrame((timestamp) => {
      if (!timesMatch) {
        elem.className += ' animated pulse infinite go';
      }
      else {
        elem.className += ' vanishIn';
      };
    });

  };

  removeResourceBookingElement(elem) {
    elem.parentNode.removeChild(elem);
  };
};

class byVenueView extends allocatorView {
  constructor(parent, options) {
    super(parent, options)

    this.viewItems.days = [];
    this.viewItems.venues = [];
    this.viewItems.events = [];
    this.viewItems.feeds = [];
    this.viewItems.dayVenues = [];
    this.currentDayVenue = null;
    this.currentDayEvent = null;

    this.dimensions = {
      dayHeight: 0
    };

    let pageContent = document.getElementById("page-content");
    this.circuitsModal = new selectCircuitModal(pageContent, { onBodyClicked: this.onCircuitSelected.bind(this) });

    this.createItems();
    this.drawView();
  }

  createItems() {
    let venueCodeSearch = "", venueNameSearch = "", newVenueItem = null,
      venues = [], existingItems = [],
      day = null, newDayItem = null,
      newEventItem = null, dayVenueItem = null;

    this.viewItems.venues = []
    this.viewItems.days = []

    this.parent.schedules.forEach(schedule => {

      //check if the venue is already added
      venueCodeSearch = (schedule.VenueCode.trim().length == 0 ? "Unknown" : schedule.VenueCode)
      venueNameSearch = (schedule.Venue.trim().length == 0 ? "Unknown" : schedule.Venue)
      existingItems = this.viewItems.venues.filter(item => { return item.venueName == venueNameSearch })
      if (existingItems.length == 0) {
        newVenueItem = null
        newVenueItem = this.createVenueItem(venueCodeSearch, venueNameSearch, this.viewItems.venues.length + 1)
        this.viewItems.venues.push(newVenueItem)
      }
      else {
        newVenueItem = existingItems[0]
      }

      //reset variables
      existingItems = []

      //check if the date is already added
      day = new Date(schedule.ScheduleDateTime)
      day.setHours(0, 0, 0, 0)
      existingItems = this.viewItems.days.filter(item => { return item.day.getTime() === day.getTime() })
      if (existingItems.length == 0) {
        newDayItem = null
        newDayItem = this.createDayItem(day, this.viewItems.days.length + 1)
        this.viewItems.days.push(newDayItem)
      }
      else {
        newDayItem = existingItems[0]
      }

      //reset variables
      existingItems = []

      //check if the event is already added
      existingItems = this.viewItems.events.filter(item => { return item.GenRefNumber === schedule.GenRefNumber })
      if (existingItems.length == 0) {
        newEventItem = null
        newEventItem = this.createEventItem(schedule)
        this.viewItems.events.push(newEventItem)
      }
      else {
        //validate against what is there already
        newEventItem = existingItems[0]
      }

    })

    this.viewItems.days.forEach(dayItem => {

      let tempEvents = this.viewItems.events.filter(item => {
        return (new Date(new Date(item.ScheduleDateTime).setHours(0, 0, 0, 0)).getTime() === dayItem.day.getTime());
      });

      let tempVenues = tempEvents.map(item => { return item.VenueName })
      tempVenues = new Set(tempVenues)

      tempVenues.forEach(venueName => {

        let dayVenue = {
          venueName: venueName,
          parent: dayItem,
          startTime: null,
          dayEvents: [],
          dom: { headerElement: null, columnElement: null }
        }

        //events for the day and sort the,
        dayVenue.dayEvents = tempEvents.filter(item => {
          return (item.VenueName === venueName)
        })
        dayVenue.dayEvents = dayVenue.dayEvents.sort((item1, item2) => { return (new Date(item1.ScheduleDateTime).getTime() - new Date(item2.ScheduleDateTime).getTime()) })

        //set the min start time
        let times = dayVenue.dayEvents.map(item => {
          return new Date(item.ScheduleDateTime).getTime()
        })
        dayVenue.startTime = new Date(Math.min.apply(null, times))

        dayItem.venues.push(dayVenue)
      })

      //sort the venues
      dayItem.venues = dayItem.venues.sort((item1, item2) => { return (item1.startTime.getTime() - item2.startTime.getTime()) })

    })

  }

  createVenueItem(venueCode, venueName, sortNumber) {
    return {
      venueCode: venueCode, venueName: venueName, sortNumber: sortNumber,
      dom: { headerElement: null }
    }
  }

  createDayItem(day, sortNumber) {
    return {
      day: day, sortNumber: sortNumber,
      venues: [],
      dom: { dayElement: null, dayHeader: null, shortcutElement: null }
    }
  }

  createEventItem(schedule, dayItem) {
    return {
      GenRefNumber: schedule.GenRefNumber, GenreDesc: schedule.GenreDesc, Genre: schedule.Genre, SeriesTitle: schedule.SeriesTitle, Title: schedule.Title,
      VenueName: (schedule.Venue.trim().length == 0 ? "Unknown" : schedule.Venue), ScheduleDateTime: schedule.ScheduleDateTime,
      ScheduleEndDate: schedule.ScheduleEndDate, EventStatusDesc: schedule.EventStatusDesc,
      LiveDate: schedule.LiveDate,
      parent: null, dom: { dayElement: null, dayHeader: null }, bookings: []
    };
  };

  drawView() {

    while (this.parent.dom.viewContainer.firstChild) {
      this.parent.dom.viewContainer.removeChild(this.parent.dom.viewContainer.firstChild);
    };

    this.parent.defaultOptions.timeWidth = 100;
    this.parent.dom.settingsButton.style.width = this.parent.defaultOptions.timeWidth.toString() + 'px';

    this.parent.dom.viewContainer.appendChild(this.parent.dom.settingsButton);
    this.dom.timeColumn = document.createElement('div');
    this.dom.timeColumn.className = "time-column";
    this.dom.timeColumn.style.top = this.parent.defaultOptions.headerHeight.toString() + 'px';
    this.dom.timeColumn.style.left = '0px';
    this.dom.timeColumn.style.width = this.parent.defaultOptions.timeWidth.toString() + 'px';

    this.dom.viewDataColumn = document.createElement('div');
    this.dom.viewDataColumn.className = "view-data";
    this.dom.viewDataColumn.style.top = '0px';
    this.dom.viewDataColumn.style.left = this.parent.defaultOptions.timeWidth.toString() + 'px';
    this.dom.viewDataColumn.style.width = (this.viewItems.venues.length * this.parent.defaultOptions.venueWidth).toString() + 'px';

    //append the 3 main elements to the view container
    this.parent.dom.viewContainer.appendChild(this.dom.timeColumn);
    this.parent.dom.viewContainer.appendChild(this.dom.viewDataColumn);

    //draw schedules for each day
    this.viewItems.days.forEach(dayItem => {
      //element
      this.drawDay(dayItem);
      //shortcut
      dayItem.dom.shortcutElement = document.createElement('div');
      dayItem.dom.shortcutElement.className = "day-shortcut";
      dayItem.dom.shortcutElement.innerHTML = dayItem.day.format("dd MMM yyyy");
      dayItem.dom.shortcutElement.businessObject = dayItem;
      this.dom.timeColumn.appendChild(dayItem.dom.shortcutElement);
    });

    this.addEventHandlers();

    if (this.viewItems.days.length > 0) {
      let dayItem = this.viewItems.days[0];
      this.currentDate = moment(dayItem.day);
      dayItem.isSelected = true;
      this.updateDayItemShortcut(dayItem);
    };
  };

  drawDay(dayItem) {

    //draw day element
    let numbers = dayItem.venues.map(item => { return item.dayEvents.length });
    let maxEvents = Math.max.apply(null, numbers);
    let dayHeight = (maxEvents * this.parent.defaultOptions.venueEventHeight);
    dayItem.height = dayHeight;

    dayItem.dom.dayElement = document.createElement('div');
    dayItem.dom.dayElement.className = "day";
    dayItem.dom.dayElement.style.position = "relative";
    dayItem.dom.dayElement.style.display = "block";
    dayItem.dom.dayElement.style.height = dayItem.height.toString() + 'px';
    dayItem.dom.dayElement.style.width = (this.viewItems.venues.length * this.parent.defaultOptions.venueWidth) + 'px';

    this.dom.viewDataColumn.style.height = dayItem.height.toString() + 'px';
    this.dom.timeColumn.style.height = dayItem.height.toString() + 'px';

    //draw day header
    dayItem.dom.dayHeader = document.createElement('div');
    dayItem.dom.dayHeader.className = "day-header";
    dayItem.dom.dayHeader.innerHTML = dayItem.day.format('ddd dd MMM');
    dayItem.dom.dayHeader.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px';
    dayItem.dom.dayHeader.style.width = "100%";

    //draw day venue headers
    dayItem.dom.dayVenueHeaders = document.createElement('div');
    dayItem.dom.dayVenueHeaders.className = "day-venue-headers";
    dayItem.dom.dayVenueHeaders.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px';
    dayItem.dom.dayVenueHeaders.style.width = "100%";

    //draw day venues
    dayItem.dom.dayVenues = document.createElement('div');
    dayItem.dom.dayVenues.className = "day-venues";
    dayItem.dom.dayVenues.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px';
    dayItem.dom.dayVenues.style.width = "100%"; //(this.viewItems.venues.length * this.parent.defaultOptions.venueWidth) + 'px'

    this.dom.viewDataColumn.appendChild(dayItem.dom.dayElement);
    dayItem.dom.dayElement.appendChild(dayItem.dom.dayHeader);
    dayItem.dom.dayElement.appendChild(dayItem.dom.dayVenueHeaders);
    dayItem.dom.dayElement.appendChild(dayItem.dom.dayVenues);

    //draw the venues
    dayItem.venues.forEach((dayVenueItem, index) => {

      //draw venue header
      dayVenueItem.dom.headerElement = document.createElement('div');
      dayVenueItem.dom.headerElement.className = "venue-header";
      dayVenueItem.dom.headerElement.innerHTML = dayVenueItem.venueName;
      dayVenueItem.dom.headerElement.title = dayVenueItem.venueName;
      dayVenueItem.dom.headerElement.style.width = this.parent.defaultOptions.venueWidth.toString() + 'px';
      dayVenueItem.dom.headerElement.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px';
      dayVenueItem.dom.headerElement.style.top = dayItem.dom.dayElement.style.top;
      dayVenueItem.dom.headerElement.style.left = (index * this.parent.defaultOptions.venueWidth).toString() + 'px';
      dayVenueItem.dom.headerElement.businessObject = dayVenueItem;

      //draw venue column
      dayVenueItem.dom.columnElement = document.createElement('div');
      dayVenueItem.dom.columnElement.className = "venue-day";
      dayVenueItem.dom.columnElement.style.width = this.parent.defaultOptions.venueWidth.toString() + 'px';
      dayVenueItem.dom.columnElement.style.height = dayItem.height.toString() + 'px';
      dayVenueItem.dom.columnElement.style.top = (2 * this.parent.defaultOptions.headerHeight).toString() + 'px'; //dayVenueItem.dom.headerElement.style.height
      dayVenueItem.dom.columnElement.style.left = (index * this.parent.defaultOptions.venueWidth).toString() + 'px';

      dayItem.dom.dayVenueHeaders.appendChild(dayVenueItem.dom.headerElement);
      dayItem.dom.dayVenues.appendChild(dayVenueItem.dom.columnElement);

      //draw the events for the venue
      dayVenueItem.dayEvents.forEach(eventItem => {
        eventItem.parent = dayVenueItem;
        this.drawEventElement(eventItem);
      });

    });

  };

  addEventHandlers() {
    //remove old handlers
    $(this.parent.dom.viewContainer).off('mousewheel DOMMouseScroll')
    $(this.parent.dom.viewContainer).off('pointerdown pointermove pointerup pointerleave')
    $(this.parent.dom.viewContainer).off('click')
    $(this.parent.dom.viewContainer).off('mouseenter mouseleave')
    $(window).off('resize')
    $(window).off('onkeydown')
    $(window).off('onkeyup')

    //add the handlers
    $(this.parent.dom.viewContainer).on('mousewheel DOMMouseScroll', (event) => {
      event.preventDefault()
      if (event.ctrlKey) { return }
      this.positions.distanceY = (-1 * event.originalEvent.deltaY)
      let movementY = (this.positions.channelY + this.positions.distanceY)
      //this.positions.channelY = movementY
      if (this.positions.channelY > 0) { this.positions.channelY = 0 }
      this.updatePosition(this.positions.channelX, movementY)
    })

    $(this.parent.dom.viewContainer).on('pointerdown pointermove pointerup pointerleave', (event) => {
      event.preventDefault();
      this.positions.mouseX = event.clientX;
      this.positions.mouseY = event.clientY;

      if (event.type == 'pointerdown') {
        this.eventStates.mousedDown = true
        //this.setDragDivStartPosition(event)
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = event.clientX
          this.positions.lastY = event.clientY
          this.positions.currentX = event.clientX
          this.positions.currentY = event.clientY
        };
        return
      };

      if (event.type == 'pointerleave') {
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = this.positions.channelX
          this.positions.lastY = this.positions.channelY
          this.positions.currentX = this.positions.channelX
          this.positions.currentY = this.positions.channelY
        };
        return
      };

      if (event.type == 'pointerup') {
        this.eventStates.mousedDown = false
        //this.resetDragDiv()
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = this.positions.channelX
          this.positions.lastY = this.positions.channelY
          this.positions.currentX = this.positions.channelX
          this.positions.currentY = this.positions.channelY
        };
        return
      };

      if (event.type == 'pointermove') {
        if (event.ctrlKey && this.eventStates.mousedDown) {
          //update dimensions of drag-select div
          //this.updateDragDivDimensions(event)
        }
        else if (this.eventStates.mousedDown) {
          //move the channel thing
          //position data
          this.positions.lastX = this.positions.currentX;
          this.positions.lastY = this.positions.currentY;
          this.positions.currentX = event.clientX;
          this.positions.currentY = event.clientY;

          //calculate distances moved
          this.positions.distanceX = 0
          this.positions.distanceX += Math.sqrt(Math.pow(this.positions.lastX - this.positions.currentX, 2))
          if (this.positions.currentX < this.positions.lastX) {
            this.positions.distanceX = (-1 * this.positions.distanceX);
          };

          this.positions.distanceY = 0
          this.positions.distanceY += Math.sqrt(Math.pow(this.positions.lastY - this.positions.currentY, 2))
          if (this.positions.currentY < this.positions.lastY) {
            this.positions.distanceY = (-1 * this.positions.distanceY);
          };

          //determine new x of channel
          var moveX = (this.positions.channelX + this.positions.distanceX);
          if (moveX > 0) { moveX = 0 };

          //determine new y of channel
          var moveY = (this.positions.channelY + this.positions.distanceY);
          if (moveY > 0) { moveY = 0 };

          this.updatePosition(moveX, moveY);
        };
      };

    });

    $(this.parent.dom.viewContainer).on('click', (event) => {
      contextMenu.destroy();
      contextMenu.hide();

      this.currentDayVenue = null;
      this.currentDayEvent = null;

      const target = event.target;
      const isVenueHeader = target.classList.contains('venue-header');
      const isEventCaret = target.classList.contains('fa-caret-down');
      const isDayShortcut = target.classList.contains('day-shortcut');

      if (isVenueHeader) {
        this.currentDayVenue = target.businessObject;
        this.showCircuitSelection();
      }
      else if (isEventCaret) {
        this.currentDayEvent = target.parentNode.eventItem;
        this.showCircuitSelection();
      }
      else if (isDayShortcut) {
        this.scrollToDay(target.businessObject);
      }
      else {
        this.circuitsModal.hideLoadingBar();
      };

    });

    $(this.parent.dom.viewContainer).on('mouseenter mouseleave', 'div.room-schedule', function (event) {
      //switch (event.type) {
      //  case "mouseenter":
      //    this.onMouseOver.call(me, event)
      //    break;
      //  case "mouseleave":
      //    this.onMouseOut.call(me, event)
      //    break;
      //}
    })

    $(window).on('resize', function () {
      //this.dom.container.style.height = (document.body.clientHeight - this.positions.reservedHeight).toString() + 'px'
      //this.rowObj.element.style.height = this.dom.container.style.height
    })

    $(window).on('keydown onkeydown', function (event) {
      //this.ctrlHeld = event.ctrlKey
    })

    //$(window).on('keyup onkeyup', function (event) {
    //  //this.ctrlHeld = event.ctrlKey
    //  //this.eventStates.mousedDown = this.ctrlHeld
    //  //this.positions.dragDivXStart = this.positions.mouseX
    //  //this.positions.dragDivYStart = this.positions.mouseY
    //  //if (!this.ctrlHeld) {
    //  //  this.resetDragDiv()
    //  //}
    //})

    $("#BulkBookingModal").on('hidden.bs.modal', function () {
      this.resetSelections()
      //this.resetSelectableStates()
    })
  }

  updatePosition(newX, newY) {
    //dimensions of the relevant elements
    let containerDims = this.parent.dom.container.getBoundingClientRect()
    let scrolledDims = this.dom.viewDataColumn.getBoundingClientRect()
    let containerBottom = containerDims.bottom
    let containerRight = containerDims.right

    //new x positions
    if ((scrolledDims.right > containerDims.right) || (newX > this.positions.channelX)) {
      let newLeft = (this.parent.defaultOptions.timeWidth + this.positions.channelX)
      if (newLeft > this.parent.defaultOptions.timeWidth) {
        newLeft = this.parent.defaultOptions.timeWidth
        this.positions.channelX = this.parent.defaultOptions.timeWidth
      } else {
        this.positions.channelX = newX
      }
      this.dom.viewDataColumn.style.left = newLeft.toString() + 'px'
      //this.dom.viewHeaderColumn.style.left = newLeft.toString() + 'px'
    }

    //new y positions
    //if ((scrolledDims.bottom > containerDims.bottom) || (newY > this.positions.channelY)) {
    let newTop = newY
    if (newTop > 0) {
      newTop = 0; //this.parent.defaultOptions.headerHeight
      this.positions.channelY = newTop
    } else {
      this.positions.channelY = newTop
    }
    this.dom.viewDataColumn.style.top = newTop.toString() + 'px'
    //}
  }

  drawEventElement(eventObject) {

    eventObject.dom.eventElement = document.createElement('div')
    eventObject.dom.eventElement.id = 'event-' + eventObject.GenRefNumber.toString()
    eventObject.dom.eventElement.eventItem = eventObject
    eventObject.dom.eventElement.style.height = this.parent.defaultOptions.venueEventHeight.toString() + 'px'

    //eventObject.dom.eventInfoElement = document.createElement('div')
    //eventObject.dom.eventInfoElement.className = 'event-info'

    eventObject.dom.resourceBookingsElement = document.createElement('div')
    eventObject.dom.resourceBookingsElement.className = 'booking-list'

    this.updateEventElement(eventObject)

    //eventObject.dom.eventElement.appendChild(eventObject.dom.eventInfoElement)
    eventObject.dom.eventElement.appendChild(eventObject.dom.resourceBookingsElement)
    eventObject.parent.dom.columnElement.appendChild(eventObject.dom.eventElement)

    let resourceBookings = this.parent.resourcebookings.filter(item => {
      return (item.ResourceBookingGenRefNumberList.filter(item2 => { return (item2.GenRefNumber == eventObject.GenRefNumber) }).length > 0)
    });

    eventObject.bookings = resourceBookings;
    resourceBookings.forEach(item => {
      this.drawResourceBookingElement(eventObject.dom.resourceBookingsElement, item)
    });

  };

  updateEventElement(eventObject) {
    let st = new Date(eventObject.ScheduleDateTime).format('HH:mm'),
      et = new Date(eventObject.ScheduleEndDate).format('HH:mm'),
      venueDimensions = eventObject.parent.dom.headerElement.getBoundingClientRect();

    eventObject.dom.eventElement.style.left = venueDimensions.left.toString() + 'px'
    eventObject.dom.eventElement.style.width = venueDimensions.width.toString() + 'px'
    eventObject.dom.eventElement.className = "day-venue-schedule " + eventObject.EventStatusDesc.toString().toLowerCase()
    eventObject.dom.eventElement.innerHTML = '<i class="fa fa-caret-down pull-right" style="position: relative;"></i>' +
      '<p>' + st + ' - ' + et + '</p>' +
      '<p>' + eventObject.SeriesTitle + '</p>' +
      '<p>' + eventObject.Title + '</p>';
  };

  drawResourceBookingElement(container, resourceBooking) {
    let resourceBookingElement = document.createElement('div')
    this.updateResourceBookingElement(resourceBooking, resourceBookingElement)
    container.appendChild(resourceBookingElement)
  }

  updateResourceBookingElement(resourceBooking, elem) {
    var st = new Date(resourceBooking.StartDateTime).format('HH:mm'),
      et = new Date(resourceBooking.EndDateTime).format('HH:mm');

    elem.businessObject = resourceBooking
    elem.title = (resourceBooking.PopoverContent.length > 0 ? resourceBooking.PopoverContent : st + ' - ' + et)
    elem.innerHTML = resourceBooking.ResourceName;
    elem.setAttribute('bookingID', resourceBooking.ResourceBookingID.toString());

    //class modification
    elem.className = ''
    // restarting the animation
    // without the following line of code, restarting the animation wouldn't work.
    void elem.offsetWidth;
    elem.className = 'resource-booking short-text'
    requestAnimationFrame(function (timestamp) {
      elem.className += ' vanishIn'
    })

  };

  removeResourceBookingElement(container, elem) {

  };

  async showCircuitSelection() {
    try {
      await this.circuitsModal.showLoadingBar();
      this.circuitsModal.show();
      this.circuitsModal.clearBody();

      let circuitData = await this.getCircuitAvailability();
      //list
      let circuitList = document.createElement('ul');
      circuitList.className = "select-circuit-list";
      this.circuitsModal.body.appendChild(circuitList);
      circuitList.style.height = (this.circuitsModal.currentDimensions.height - 80).toString() + 'px';
      //list items
      circuitData.forEach(circuit => {
        let circuitElement = document.createElement('li');
        circuitElement.className = "option-circuit";
        circuitElement.innerHTML = "<i class='fa fa-wrench'></i> " + circuit.EquipmentName;
        circuitElement.businessObject = circuit;
        circuitList.appendChild(circuitElement);
      });
      this.circuitsModal.body.appendChild(circuitList);

      await this.circuitsModal.hideLoadingBar();
    }
    catch (err) {
      OBMisc.Notifications.GritterError("Fetch circuits failed", err.message, 1000);
    }
  };

  getCircuitAvailability() {
    return Singular.GetDataStatelessPromise("OBLib.Equipment.ReadOnly.ROCircuitSelectList, OBLib", {
      SystemID: 2,
      EquipmentName: ""
    })
  };

  async onCircuitSelected(event) {
    const clickeditem = event.target;
    let circuit = null;

    await this.circuitsModal.showLoadingBar();

    //get the selected circuit
    if (clickeditem.classList.contains("option-circuit")) {
      circuit = clickeditem.businessObject;
    } else if (parentElement.classList.contains("option-circuit")) {
      circuit = clickeditem.parentNode.businessObject;
    };

    if (circuit && (this.currentDayVenue || this.currentDayEvent)) {
      let genrefs = [];

      if (this.currentDayVenue) {
        genrefs = this.currentDayVenue.dayEvents.map(dayEvent => { return dayEvent.GenRefNumber });
      } else if (this.currentDayEvent) {
        genrefs.push(this.currentDayEvent.GenRefNumber);
      };
      //do the import
      try {
        let data = await EquipmentFeedBO.importFeed({ EquipmentID: circuit.EquipmentID, GenRefNumbers: genrefs });
        OBMisc.Notifications.GritterSuccess("Imported Successfully", "", 250);
        await this.circuitsModal.hideLoadingBar();
        this.circuitsModal.hide();
      } catch (err) {
        OBMisc.Notifications.GritterError("Error importing", err.message, 1000);
        await this.circuitsModal.hideLoadingBar();
      }
    } else {
      OBMisc.Notifications.GritterError("Error importing", "circuit and genref numbers could not be determined", 1000);
      await this.circuitsModal.hideLoadingBar();
    };

  };

  processSignalRChanges(updatedBookings, addedBookings, removedBookingIDs) {

    //Process the updates
    updatedBookings.forEach((updBooking, indx) => {
      //get the event items with this genrefnumber
      let elems = document.querySelectorAll('[bookingID="' + updBooking.ResourceBookingID.toString() + '"]');
      elems.forEach(elem => {
        this.updateResourceBookingElement(updBooking, elem);
      });
    });

    //Process the additions
    addedBookings.forEach((addedBooking, indx) => {
      //get the event items with this genrefnumber
      let eventItems = this.viewItems.events.filter(eItem => {
        return (addedBooking.ResourceBookingGenRefNumberList.filter(gItem => {
          return (gItem.GenRefNumber == eItem.GenRefNumber)
        }).length > 0)
      });
      //
      eventItems.forEach(eventItem => {
        this.drawResourceBookingElement(eventItem.dom.resourceBookingsElement, addedBooking);
      });
    });

    //Process the deletes
    removedBookingIDs.forEach((resourceBookingID, indx) => {
      let elems = document.querySelectorAll('[bookingID="' + resourceBookingID.toString() + '"]');
      elems.forEach(elem => {
        elem.parentNode.removeChild(elem);
      });
    });

  };

  scrollToDay(dayItem) {
    let newY = 0;
    let dims = dayItem.dom.dayElement.getBoundingClientRect();
    let sortNum = dayItem.sortNumber;
    if (dayItem.sortNumber == 1) {
      newY = 0;
    } else {
      let totalHeightOfItemsBeforeMe = this.viewItems.days.filter(item => { return item.sortNumber < sortNum }).reduce((prevTotal, dayItem) => { return prevTotal + dayItem.height }, 0);
      newY = (-1 * totalHeightOfItemsBeforeMe);
    };
    this.updatePosition(0, newY);
    this.currentDate = moment(dayItem.day);
    this.updateDayItemShortcut(dayItem);
  };

  onKeyUp(event) {
    event.preventDefault();
    event.stopPropagation();
    let dateChanged = false;

    let previousDayItem = this.viewItems.days.find(item => { return (item.day.getTime() === this.currentDate.toDate().getTime()); });

    //update the current date
    if (allocator.arrowKeys.LEFT == event.which) {
      //previous day
      this.currentDate.add(-1, 'days').startOf('day');
      dateChanged = true;
    } else if (allocator.arrowKeys.RIGHT == event.which) {
      //next day
      this.currentDate.add(1, 'days').startOf('day');
      dateChanged = true;
    };

    if (dateChanged) {

      let dayItem = this.viewItems.days.find(item => { return (item.day.getTime() === this.currentDate.toDate().getTime()); });
      if (dayItem) {
        //unselect previous day
        previousDayItem.isSelected = false;
        this.updateDayItemShortcut(previousDayItem);
        //select new day
        dayItem.isSelected = true;
        this.scrollToDay(dayItem);
      }
      else {
        alert('refresh data');
      };
    };

  };

  updateDayItemShortcut(dayItem) {
    if (dayItem.isSelected) {
      dayItem.dom.shortcutElement.className = "day-shortcut selected";
    } else {
      dayItem.dom.shortcutElement.className = "day-shortcut";
    };
  };

  createEquipmentBookings(eventItems, options) {
    ViewModel.CurrentByEventTemplate(null);
    ViewModel.CurrentByEventTemplate(new ByEventTemplateObject());
    ViewModel.CurrentByEventTemplate().Mode("Equipment");
    //create a room schedule for each object
    eventItems.forEach(item => {
      let newEquipmentSchedule = ViewModel.CurrentByEventTemplate().EquipmentScheduleBasicList.AddNew(),
        sd = moment(item.StartTime),
        ed = moment(item.EndTime);

      //populate the room schedule
      newEquipmentSchedule.GenRefNumber(item.GenRefNumber);
      newEquipmentSchedule.GenreDesc(item.GenreDesc);
      newEquipmentSchedule.SeriesTitle(item.SeriesTitle);
      newEquipmentSchedule.Title(item.Title);
      newEquipmentSchedule.SystemID(options.SystemID);
      newEquipmentSchedule.ProductionAreaID(options.ProductionAreaID);
      newEquipmentSchedule.CallTime(sd.clone().add(-15, 'minutes').toDate());
      newEquipmentSchedule.StartTime(sd.toDate());
      newEquipmentSchedule.EndTime(ed.toDate());
      newEquipmentSchedule.WrapTime(ed.clone().add(15, 'minutes').toDate());
      newEquipmentSchedule.ProductionID(null);
      newEquipmentSchedule.FeedTypeID(1);

    })
    allocator.showByEventModal();
  };
};

class bySeriesView extends allocatorView {
  constructor(parent, options) {
    super(parent, options)

    this.viewItems.series = [];
    this.viewItems.events = [];

    this.circuits = [];
    this.circuitsElement = [];

    this.createItems();
    this.drawView();
  }

  createItems() {
    let existingItems = [],
      day = null, seriesItem = null,
      eventItem = null;

    this.viewItems.series = []

    this.parent.schedules.forEach(schedule => {
      //check if the series is already added
      existingItems = this.viewItems.series.filter(item => { return item.seriesNumber == schedule.SeriesNumber })
      if (existingItems.length == 0) {
        seriesItem = null
        seriesItem = this.createSeriesItem(schedule.SeriesNumber, schedule.SeriesTitle, this.viewItems.series.length + 1)
        this.viewItems.series.push(seriesItem)
      }
      else {
        seriesItem = existingItems[0]
      }

      //reset variables
      existingItems = []

      //check if the event is already added
      existingItems = this.viewItems.events.filter(item => { return item.GenRefNumber === schedule.GenRefNumber })
      if (existingItems.length == 0) {
        eventItem = null
        eventItem = this.createEventItem(schedule, seriesItem)
        this.viewItems.events.push(eventItem)
      }
      else {
        //validate against what is there already
        eventItem = existingItems[0]
      }

    })

    this.viewItems.series = this.viewItems.series.sort((series1, series2) => {
      if (series1.seriesTitle < series2.seriesTitle) return -1;
      if (series1.seriesTitle > series2.seriesTitle) return 1;
      return 0
    })

    this.viewItems.events.forEach(eventItem => {
      eventItem.schedules = this.parent.schedules.filter(schedule => { return (schedule.GenRefNumber === eventItem.GenRefNumber) })
        .sort((schedule1, schedule2) => { return (new Date(schedule1.ScheduleDateTime).getTime() - new Date(schedule2.ScheduleDateTime).getTime()) })

      let liveSchedules = eventItem.schedules.filter(schedule => { return (schedule.EventStatus === "L") })
      let liveSchedule = null
      if (liveSchedules.length > 0) {
        liveSchedule = liveSchedules[0]
      } else {
        liveSchedule = eventItem.schedules[0]
      }
      eventItem.LiveDate = new Date(liveSchedule.ScheduleDateTime)
      eventItem.LiveEndDate = new Date(liveSchedule.ScheduleEndDate)
    })

    this.viewItems.series.forEach(series => {
      series.EventItemList = this.viewItems.events.filter(event => { return (event.SeriesNumber === series.seriesNumber) })
      series.EventItemList = series.EventItemList.sort((evt1, evt2) => { return (evt1.LiveDate.getTime() - evt2.LiveDate.getTime()) })
    })
  }

  createSeriesItem(SeriesNumber, SeriesTitle, sortNumber) {
    return {
      seriesNumber: SeriesNumber, seriesTitle: SeriesTitle, sortNumber: sortNumber,
      EventItemList: [],
      dom: { headerElement: null, columnElement: null, cardElement: null }
    }
  }

  createEventItem(schedule, seriesItem) {
    return {
      GenRefNumber: schedule.GenRefNumber, GenreDesc: schedule.GenreDesc, SeriesNumber: schedule.SeriesNumber,
      SeriesTitle: schedule.SeriesTitle, Title: schedule.Title, LiveDate: null,
      parent: seriesItem, dom: { eventElement: null },
      schedules: []
    }
  }

  drawView() {

    while (this.parent.dom.viewContainer.firstChild) {
      this.parent.dom.viewContainer.removeChild(this.parent.dom.viewContainer.firstChild);
    };

    this.parent.dom.viewContainer.appendChild(this.parent.dom.settingsButton)
    this.dom.timeColumn = document.createElement('div')
    this.dom.timeColumn.className = "time-column"
    this.dom.timeColumn.style.top = this.parent.defaultOptions.headerHeight.toString() + 'px'
    this.dom.timeColumn.style.left = '0px'
    this.dom.timeColumn.style.width = this.parent.defaultOptions.timeWidth.toString() + 'px'

    this.dom.viewHeaderColumn = document.createElement('div')
    this.dom.viewHeaderColumn.className = "view-header"
    this.dom.viewHeaderColumn.style.top = '0px'
    this.dom.viewHeaderColumn.style.left = this.parent.defaultOptions.timeWidth.toString() + 'px'
    this.dom.viewHeaderColumn.style.height = this.parent.defaultOptions.headerHeight.toString() + 'px'

    this.dom.viewDataColumn = document.createElement('div')
    this.dom.viewDataColumn.className = "view-data"
    this.dom.viewDataColumn.style.top = this.parent.defaultOptions.headerHeight.toString() + 'px'
    this.dom.viewDataColumn.style.left = this.parent.defaultOptions.timeWidth.toString() + 'px'
    this.dom.viewDataColumn.style.width = (this.viewItems.series.length * this.parent.defaultOptions.seriesWidth).toString() + 'px'

    //append the 3 main elements to the view container
    this.parent.dom.viewContainer.appendChild(this.dom.timeColumn)
    this.parent.dom.viewContainer.appendChild(this.dom.viewHeaderColumn)
    this.parent.dom.viewContainer.appendChild(this.dom.viewDataColumn)

    //draw schedules for each day
    let availableWidth = (this.parent.defaultOptions.containerWidth - this.parent.defaultOptions.timeWidth);
    let cardsPerRow = (availableWidth / this.parent.defaultOptions.seriesWidth)
    let numberOfRows = (this.viewItems.series.length / cardsPerRow)
    cardsPerRow = Math.floor(cardsPerRow)
    numberOfRows = Math.ceil(numberOfRows)
    let widthPerc = (100 / cardsPerRow)

    let currentSeriesIndex = 0;
    for (let indx = 0; indx < numberOfRows; indx++) {

      let row = document.createElement('div')
      row.className = "series-row"
      row.style.height = (this.parent.defaultOptions.seriesHeaderHeight + this.parent.defaultOptions.seriesHeight).toString() + 'px'
      this.dom.viewDataColumn.appendChild(row)

      let cardCount = cardsPerRow
      while (cardCount > 0) {
        let series = this.viewItems.series[currentSeriesIndex]
        this.drawSeries(series, row, widthPerc)
        cardCount--
        currentSeriesIndex++
      }

      numberOfRows--
    }

    this.dom.viewHeaderColumn.style.width = this.parent.dom.container.style.width; //((this.viewItems.series.length * this.parent.defaultOptions.seriesWidth) + 10).toString() + 'px'
    this.dom.viewDataColumn.style.width = this.parent.dom.container.style.width;
    this.dom.timeColumn.style.height = this.parent.dom.container.style.height;

    this.addEventHandlers()

  }

  drawSeries(seriesItem, row, widthPerc) {

    //draw column
    seriesItem.dom.columnElement = document.createElement('div')
    seriesItem.dom.columnElement.className = "series-column";
    seriesItem.dom.columnElement.style.width = widthPerc.toString() + '%' //this.parent.defaultOptions.seriesWidth.toString() + 'px'
    seriesItem.dom.columnElement.style.height = (this.parent.defaultOptions.seriesHeaderHeight + this.parent.defaultOptions.seriesHeight).toString() + 'px'
    seriesItem.dom.columnElement.item = seriesItem
    //seriesItem.dom.columnElement.style.top = '0px'
    //seriesItem.dom.columnElement.style.left = ((seriesItem.sortNumber - 1) * this.parent.defaultOptions.seriesWidth).toString() + 'px'
    row.appendChild(seriesItem.dom.columnElement)

    //draw header
    seriesItem.dom.headerElement = document.createElement('div')
    seriesItem.dom.headerElement.className = "series-header"
    seriesItem.dom.headerElement.innerHTML = seriesItem.seriesTitle
    seriesItem.dom.headerElement.title = seriesItem.seriesTitle
    //seriesItem.dom.headerElement.style.width = this.parent.defaultOptions.seriesWidth.toString() + 'px'
    seriesItem.dom.headerElement.style.height = this.parent.defaultOptions.seriesHeaderHeight.toString() + 'px'
    //seriesItem.dom.headerElement.style.left = ((seriesItem.sortNumber - 1) * this.parent.defaultOptions.seriesWidth).toString() + 'px'
    seriesItem.dom.columnElement.appendChild(seriesItem.dom.headerElement)

    //draw card
    seriesItem.dom.cardElement = document.createElement('div')
    seriesItem.dom.cardElement.className = "series-card"
    //seriesItem.dom.cardElement.style.width = this.parent.defaultOptions.seriesWidth.toString() + 'px'
    seriesItem.dom.cardElement.style.height = this.parent.defaultOptions.seriesHeight.toString() + 'px'
    seriesItem.dom.cardElement.innerHTML = seriesItem.EventItemList.length.toString() + ' Event/s'
    seriesItem.dom.columnElement.appendChild(seriesItem.dom.cardElement)

  }

  addEventHandlers() {
    //remove old handlers
    $(this.parent.dom.viewContainer).off('mousewheel DOMMouseScroll')
    $(this.parent.dom.viewContainer).off('pointerdown pointermove pointerup pointerleave')
    $(this.parent.dom.viewContainer).off('click')
    $(this.parent.dom.viewContainer).off('mouseenter mouseleave')
    $(window).off('resize')
    $(window).off('onkeydown')
    $(window).off('onkeyup')

    //add the handlers
    $(this.parent.dom.viewContainer).on('mousewheel DOMMouseScroll', (event) => {
      event.preventDefault()
      if (event.ctrlKey) { return }
      this.positions.distanceY = (-1 * event.originalEvent.deltaY)
      let movementY = (this.positions.channelY + this.positions.distanceY)
      //this.positions.channelY = movementY
      if (this.positions.channelY > 0) { this.positions.channelY = 0 }
      this.updatePosition(this.positions.channelX, movementY)
    })

    $(this.parent.dom.viewContainer).on('pointerdown pointermove pointerup pointerleave', (event) => {
      event.preventDefault()
      this.positions.mouseX = event.clientX
      this.positions.mouseY = event.clientY

      if (event.type == 'pointerdown') {
        this.eventStates.mousedDown = true
        //this.setDragDivStartPosition(event)
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = event.clientX
          this.positions.lastY = event.clientY
          this.positions.currentX = event.clientX
          this.positions.currentY = event.clientY
        }
        return
      }

      if (event.type == 'pointerleave') {
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = this.positions.channelX
          this.positions.lastY = this.positions.channelY
          this.positions.currentX = this.positions.channelX
          this.positions.currentY = this.positions.channelY
        }
        return
      }

      if (event.type == 'pointerup') {
        this.eventStates.mousedDown = false
        //this.resetDragDiv()
        if (!event.ctrlKey) {
          //move the channel thing
          this.positions.lastX = this.positions.channelX
          this.positions.lastY = this.positions.channelY
          this.positions.currentX = this.positions.channelX
          this.positions.currentY = this.positions.channelY
        }
        return
      }

      if (event.type == 'pointermove') {
        if (event.ctrlKey && this.eventStates.mousedDown) {
          //update dimensions of drag-select div
          //this.updateDragDivDimensions(event)
        }
        else if (this.eventStates.mousedDown) {
          //move the channel thing
          //position data
          this.positions.lastX = this.positions.currentX
          this.positions.lastY = this.positions.currentY
          this.positions.currentX = event.clientX
          this.positions.currentY = event.clientY

          //calculate distances moved
          this.positions.distanceX = 0
          this.positions.distanceX += Math.sqrt(Math.pow(this.positions.lastX - this.positions.currentX, 2))
          if (this.positions.currentX < this.positions.lastX) {
            this.positions.distanceX = (-1 * this.positions.distanceX)
          }

          this.positions.distanceY = 0
          this.positions.distanceY += Math.sqrt(Math.pow(this.positions.lastY - this.positions.currentY, 2))
          if (this.positions.currentY < this.positions.lastY) {
            this.positions.distanceY = (-1 * this.positions.distanceY)
          }

          //determine new x of channel
          var moveX = (this.positions.channelX + this.positions.distanceX)
          if (moveX > 0) { moveX = 0 }

          //determine new y of channel
          var moveY = (this.positions.channelY + this.positions.distanceY)
          if (moveY > 0) { moveY = 0 }

          this.updatePosition(moveX, moveY)
        }
      }

    })

    $(this.parent.dom.viewContainer).on('click', function (event) {
      contextMenu.destroy()
      contextMenu.hide()
    })

    $(this.parent.dom.viewContainer).on('click', 'div.series-column', (event) => {
      contextMenu.destroy()
      contextMenu.hide()
      this.showExpandedCard(event.currentTarget.item)
    })

    $(this.parent.dom.viewContainer).on('mouseenter mouseleave', 'div.room-schedule', function (event) {
      //switch (event.type) {
      //  case "mouseenter":
      //    this.onMouseOver.call(me, event)
      //    break;
      //  case "mouseleave":
      //    this.onMouseOut.call(me, event)
      //    break;
      //}
    })

    $(window).on('resize', function () {
      //this.dom.container.style.height = (document.body.clientHeight - this.positions.reservedHeight).toString() + 'px'
      //this.rowObj.element.style.height = this.dom.container.style.height
    })

    $(window).on('keydown onkeydown', function (event) {
      //this.ctrlHeld = event.ctrlKey
    })

    //$(window).on('keyup onkeyup', function (event) {
    //  //this.ctrlHeld = event.ctrlKey
    //  //this.eventStates.mousedDown = this.ctrlHeld
    //  //this.positions.dragDivXStart = this.positions.mouseX
    //  //this.positions.dragDivYStart = this.positions.mouseY
    //  //if (!this.ctrlHeld) {
    //  //  this.resetDragDiv()
    //  //}
    //})

  }

  updatePosition(newX, newY) {
    //dimensions of the relevant elements
    let containerDims = this.parent.dom.container.getBoundingClientRect()
    let scrolledDims = this.dom.viewDataColumn.getBoundingClientRect()
    let containerBottom = containerDims.bottom
    let containerRight = containerDims.right

    //new x positions
    if ((scrolledDims.right > containerDims.right) || (newX > this.positions.channelX)) {
      let newLeft = (this.parent.defaultOptions.timeWidth + this.positions.channelX)
      if (newLeft > this.parent.defaultOptions.timeWidth) {
        newLeft = this.parent.defaultOptions.timeWidth
        this.positions.channelX = this.parent.defaultOptions.timeWidth
      } else {
        this.positions.channelX = newX
      }
      this.dom.viewDataColumn.style.left = newLeft.toString() + 'px'
      this.dom.viewHeaderColumn.style.left = newLeft.toString() + 'px'
    }

    //new y positions
    if ((scrolledDims.bottom > containerDims.bottom) || (newY > this.positions.channelY)) {
      let newTop = ((this.parent.defaultOptions.headerHeight) + newY)
      if (newTop > 0) {
        newTop = this.parent.defaultOptions.headerHeight
        this.positions.channelY = newTop
      } else {
        this.positions.channelY = newTop
      }
      this.dom.viewDataColumn.style.top = newTop.toString() + 'px'
    }
  }

  showExpandedCard(seriesItem) {
    ViewModel.CurrentSeriesItem(null)
    ViewModel.CurrentSeriesItem.Set(seriesItem)
    ViewModel.CurrentSeriesItem().EventItemList().forEach(item => {
      let newItem = ViewModel.CurrentSeriesItem().EquipmentScheduleBasicList.AddNew(),
        sd = moment(item.LiveDate()),
        ed = moment(item.LiveEndDate());

      //add in the buffered start and end times
      let bsd = sd.clone().subtract(5, 'minutes'),
        bed = ed.clone().add(5, 'minutes');

      newItem.GenRefNumber(item.GenRefNumber())
      newItem.GenreDesc(item.GenreDesc())
      newItem.SeriesTitle(item.SeriesTitle())
      newItem.Title(item.Title())
      newItem.FeedTypeID(1)
      newItem.SystemID(2)
      newItem.ProductionAreaID(8)
      newItem.CallTime(bsd.toDate())
      newItem.StartTime(sd.toDate())
      newItem.EndTime(ed.toDate())
      newItem.WrapTime(bed.toDate())
      newItem.ProductionID(null)
    })
    this.showModal()
  }

  showModal() {
    $("#BySeriesModal").off('shown.bs.modal')
    $("#BySeriesModal").on('shown.bs.modal', function () {
      Singular.Validation.CheckRules(ViewModel.CurrentSeriesItem())
    })
    $("#BySeriesModal").modal()
  }

  hideModal() {
    $("#BySeriesModal").off('hidden.bs.modal')
    $("#BySeriesModal").on('hidden.bs.modal', function () {
      ViewModel.CurrentSeriesItem(null)
    })
    $("#BySeriesModal").modal('hide')
  }

};

class selectCircuitModal extends modalBase {
  //options = { title: `Select Circuit`, onBodyClicked: (event) => { } }
  constructor(parentElement, options) {
    super(parentElement, options);

    this.currentDimensions = {
      width: 0,
      height: 0
    };

    this.draw();
  };

  draw() {
    super.draw();
    this.backdrop.id = "selectCircuitBackdrop";
    this.modal.id = "selectCircuitModalmodal";
    //this.position();
  };

  position() {
    //dimensions
    let dimensions = this.parentElement.getBoundingClientRect();
    this.currentDimensions.width = (dimensions.width - (0.5 * dimensions.width));
    this.currentDimensions.height = (dimensions.height - (0.5 * dimensions.height));

    //backdrop
    this.backdrop.style.display = 'block';
    this.backdrop.style.opacity = '0.5';
    this.backdrop.style.width = dimensions.width.toString() + 'px';
    this.backdrop.style.height = dimensions.height.toString() + 'px';

    //modal
    this.modal.style.display = 'block';
    this.modal.style.width = this.currentDimensions.width.toString() + 'px';
    this.modal.style.height = this.currentDimensions.height.toString() + 'px';
    this.modal.style.left = (0.25 * dimensions.width).toString() + 'px';
    this.modal.style.top = (0.25 * dimensions.height).toString() + 'px';
  };

  show() {
    super.show().then(() => {
      this.position();
    });
  };

  //hide() {
  //  super.hide();
  //};
};

//#endregion

//#region business objects

SynergyScheduleGroupBO = {
  //Set

  StartDateTimeSet: function (area) {

  },
  EndDateTimeSet: function (area) {

  },
  WrapTimeEndSet: function (area) {

  },

  RoomIDSet: function (area) {

  },
  triggerRoomAutoPopulate: function (args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.StartTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndTime()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax: function (args) {
    args.Object.IsProcessing(false)
  },
  onRoomSelected: function (selectedItem, businessObject) {
    var me = this;

    //set productionareaid if SystemID
    if (selectedItem) {
      businessObject.RoomID(selectedItem.RoomID)
      businessObject.Room(selectedItem.Room)
      businessObject.ResourceID(selectedItem.ResourceID)
      businessObject.ProductionAreaID(selectedItem.OwningProductionAreaID)
      businessObject.ProductionArea(selectedItem.OwningProductionArea)
    }

    //update the individual roomschedules
    businessObject.RoomScheduleList().Iterate(function (rs, rsIndx) {
      rs.RoomID(businessObject.RoomID())
      rs.Room(businessObject.Room())
      rs.ResourceID(businessObject.ResourceID())
      rs.ProductionAreaID(businessObject.ProductionAreaID())
    })

    //do the setup if a room is selected
    if (businessObject.RoomID()) {
      SynergyScheduleGroupBO.doSetup(ViewModel.CurrentScheduleGroup(), {
        onSuccess: function (response) {
          //check the clashes
          SynergyScheduleGroupBO.checkClashes.call(SynergyScheduleGroupBO, businessObject)
        }
      })
    }

  },

  checkClashes: function (roomScheduleGroup) {
    roomScheduleGroup.RoomScheduleList().Iterate(function (rs, rsIndx) {
      rs.IsProcessing(true)
    })
    ViewModel.CallServerMethod("CheckRoomScheduleGroupClashes", {
      RoomScheduleGroup: roomScheduleGroup.Serialise()
    },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, roomScheduleGroup)
        }
        else {
          OBMisc.Notifications.GritterError("Check Clashes Failed", response.ErrorText, 500)
        }
        Singular.Validation.CheckRules(roomScheduleGroup)
        roomScheduleGroup.RoomScheduleList().Iterate(function (rs, rsIndx) {
          Singular.Validation.CheckRules(rs)
          rs.IsProcessing(false)
        })
        roomScheduleGroup.IsProcessing(false)
      })
  },

  SystemIDSet: function (self) {
  },
  ProductionAreaIDSet: function (self) {
  },

  addCrewMember: function (roomScheduleGroup) {
    var newItem = roomScheduleGroup.SynergyScheduleGroupPHRList.AddNew()
    newItem.StartDateTimeBuffer(roomScheduleGroup.CallTime())
    newItem.StartDateTime(roomScheduleGroup.StartTime())
    newItem.EndDateTime(roomScheduleGroup.EndTime())
    newItem.EndDateTimeBuffer(roomScheduleGroup.WrapTime())
    roomScheduleGroup.RoomScheduleList().Iterate(function (rs, rsIndx) {
      var linkedNewItem = rs.RoomSchedulePHRList.AddNew()
      linkedNewItem.LinkedGuid(newItem.Guid())
      linkedNewItem.CallTime(rs.CallTime())
      linkedNewItem.StartTime(rs.StartTime())
      linkedNewItem.EndTime(rs.EndTime())
      linkedNewItem.WrapTime(rs.WrapTime())
    })
  },
  removeCrewMember: function (roomScheduleGroup, phr) {
    var gid = phr.Guid()
    roomScheduleGroup.RoomScheduleList().Iterate(function (rs, rsIndx) {
      var linkedItemToRemove = rs.RoomSchedulePHRList().Find("LinkedGuid", gid)
      if (linkedItemToRemove) { rs.RoomSchedulePHRList.RemoveNoCheck(linkedItemToRemove) }
    })
    roomScheduleGroup.SynergyScheduleGroupPHRList.RemoveNoCheck(phr)
  },

  canEdit: function (what, inst) {
    switch (what) {
      case "AddCrewMemberButton":
        return !(inst.IsProcessing() || !inst.RoomID() || !inst.ProductionAreaID())
        break;
      case "SaveBulkBookingButton":
        return inst.IsValid()
        break;
      default:
        return true
        break;
    }
  },

  doSetup: function (self, options) {
    var me = this;
    if (self.RoomID() && self.SystemID() && self.ProductionAreaID()) {
      self.IsProcessing(true)
      ViewModel.CallServerMethod("SetupRoomScheduleGroup", {
        RoomScheduleGroup: self.Serialise()
      },
        function (response) {
          if (response.Success) {
            KOFormatter.Deserialise(response.Data, self)
            if (options.onSuccess) { options.onSuccess(response) }
          } else {
            OBMisc.Notifications.GritterError("Error Setting Up Bookings", response.ErrorText, 1000)
          }
          self.IsProcessing(false)
        })
    }
  },
  saveBulkBooking: function (roomScheduleGroup, options) {
    roomScheduleGroup.IsProcessing(false)
    ViewModel.CallServerMethod("SaveRoomScheduleGroup",
      {
        RoomScheduleGroup: roomScheduleGroup.Serialise()
      },
      function (response) {
        if (response.Success) {
          KOFormatter.Deserialise(response.Data, roomScheduleGroup)
          if (options && options.onSuccess) { options.onSuccess(response) }
        } else {
          OBMisc.Notifications.GritterError("Error saving bookings", response.ErrorText, 1000)
        }
        roomScheduleGroup.IsProcessing(false)
      });
  }

};

RoomScheduleBO = {
  //other
  callTimeDescription(self) {
    var ct = (self.CallTime() ? new Date(self.CallTime()).format("HH:mm") : '')
    var sd = new Date(self.StartTime()).format("HH:mm")
    //var wt = (self.WrapTime() ? new Date(self.WrapTime()).format("HH:mm") : '')
    return 'Call Time: ' + (ct.length > 0 ? ct : sd)
  },
  cardDescription(self) {
    var ct = new Date(self.CallTime()).format("HH:mm")
    //var sd = new Date(self.StartTime()).format("HH:mm")
    var ed = new Date(self.EndTime()).format("HH:mm")
    return self.Room() + ' ' + (ct.length == 0 ? '(no call time) ' : ct) + " - " + ed
    //return 'Room:      ' + self.Room() + ' ' + sd + " - " + ed
  },
  cssClass(self) {
    if (self.IsValid()) {
      return 'event-info-bulk'
    } else {
      return 'event-info-bulk error'
    }
  },

  //Set
  CallTimeSet(self) {

  },
  StartTimeSet(self) {
    RoomScheduleBO.checkClashes(self)
  },
  EndTimeSet(self) {
    RoomScheduleBO.checkClashes(self)
  },
  WrapTimeSet(self) {

  },
  SystemIDSet(self) {
  },
  ProductionAreaIDSet(self) {
  },

  //dropdowns
  RoomIDSet(area) {

  },
  triggerRoomAutoPopulate(args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh(args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
    args.Data.StartDateTime = new Date(args.Object.StartTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.EndTime()).format("dd MMM yyyy HH:mm")
  },
  afterRoomRefreshAjax(args) {
    args.Object.IsProcessing(false)
  },
  onRoomSelected(selectedItem, businessObject) {
    var me = this;
    if (businessObject.IsOwner()) {
      businessObject.Room(selectedItem.Room)
      businessObject.RoomID(selectedItem.RoomID)
      businessObject.ResourceID(selectedItem.ResourceID)
      businessObject.ResourceIDOwner(selectedItem.ResourceID)
      if (businessObject.IsNew()) {
        businessObject.CallTime(selectedItem.ExpectedCallTime)
        businessObject.WrapTime(selectedItem.ExpectedWrapTime)
      }
    }
    else {
      businessObject.Room("")
      businessObject.RoomID(null)
      businessObject.ResourceID(null)
      businessObject.ResourceIDOwner(null)
      if (businessObject.IsNew()) {
        businessObject.CallTime(null)
        businessObject.WrapTime(null)
      }
    }
    RoomScheduleBO.checkClashes(businessObject)
  },

  ProductionAreaStatusIDSet(area) {
    //RoomScheduleAreaBO.setStatusClass(self)
    //RoomScheduleAreaBO.updateAll(area)
  },
  triggerStatusAutoPopulate(args) {
    args.AutoPopulate = true
  },
  setStatusCriteriaBeforeRefresh(args) {
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterStatusRefreshAjax(args) {

  },
  onStatusSelected(selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.ProductionAreaStatus(selectedItem.ProductionAreaStatus)
      businessObject.StatusCssClass(selectedItem.CssClass)
    }
    else {
      businessObject.ProductionAreaStatus("")
      businessObject.StatusCssClass("")
    }
    //RoomScheduleAreaBO.updateBookingStatus(businessObject)
    //RoomScheduleAreaBO.updateCrew(businessObject, true, false, false)
  },

  //business rules
  ClashesValid(Value, Rule, CtlError) {
    if (CtlError.Object.Clashes().length > 0) {
      CtlError.AddError(CtlError.Object.Clashes())
    }
  },
  RoomIDValid(Value, Something, CtlError) {
    var obj = CtlError.Object;
    if (obj.RoomID()) {
      var ctAfterStart = OBMisc.Dates.IsAfter(obj.CallTime(), obj.StartTime())
      var onAirStartAfterEnd = OBMisc.Dates.IsAfter(obj.StartTime(), obj.EndTime())
      var onAirEndAfterWrap = OBMisc.Dates.IsAfter(obj.EndTime(), obj.WrapTime())

      var invalidTimeCount = 0

      //call time
      if (!obj.CallTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (ctAfterStart) {
        invalidTimeCount += 1
        CtlError.AddError('Call Time must be before On Air Start Time')
      }

      //on air
      if (onAirStartAfterEnd) {
        invalidTimeCount += 1
        CtlError.AddError('On Air Start must be before On Air End Time')
      }

      //wrap
      if (!obj.WrapTime()) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time required')
      } else if (onAirEndAfterWrap) {
        invalidTimeCount += 1
        CtlError.AddError('Wrap Time must be before On Air End Time')
      }

      ////clash
      //if (obj.RoomClashCount() > 0) {
      //  CtlError.AddError('Room not available')
      //}

    }
  },

  //cans
  canEdit(fieldName, roomSchedule) {
    switch (fieldName) {
      case "RoomID":
      case "CallTime":
      case "StartTime":
      case "EndTime":
      case "WrapTime":
        return (!roomSchedule.IsProcessing() && !roomSchedule.ImportedSuccessfully())
        break;
      default:
        return true;
        break;
    }
  },
  canView(fieldName, roomSchedule) {
    switch (fieldName) {
      case "ImportButton":
        return (!roomSchedule.IsProcessing() && roomSchedule.IsValid() && roomSchedule.IsDirty() && !roomSchedule.ImportedSuccessfully());
        break;
      case "IsInvalidIcon":
        return !roomSchedule.IsValid()
        break;
      case "ImportFailedIcon":
        return false //roomSchedule.ImportFailed()
        break;
      case "IsProcessingIcon":
        return roomSchedule.IsProcessing()
        break;
      default:
        return true;
        break;
    }
  },

  //methods
  save(roomSchedule) {
    roomSchedule.IsProcessing(true)
    return ViewModel.CallServerMethodPromise("SaveRoomSchedule", { RoomSchedule: roomSchedule.Serialise() })
  },
  checkClashes(self) {
    if (self.ResourceID() && self.StartTime() && self.EndTime() && !self.IsProcessing()) {
      self.IsProcessing(true)
      ResourceBookingBO.getClashes([{
        Guid: self.Guid(),
        ResourceBookingID: self.ResourceBookingID(),
        ResourceID: self.ResourceID(),
        ResourceBookingTypeID: 8,
        StartDateTime: self.StartTime(),
        EndDateTime: self.EndTime(),
        RoomScheduleID: self.RoomScheduleID()
      }],
        {
          onSuccess: function (data) {
            let clashes = ""
            data.forEach((clash, index) => {
              if (index > 0) { clashes += "\n"; }
              clashes += clash.ResourceBookingDescription + ': ' + clash.ClashTimes;
            })
            self.Clashes(clashes)
            self.IsProcessing(false)
          },
          onFail: function (errorText) {
            OBMisc.Notifications.GritterError("Clash Check Failed", errorText, 1000)
            self.IsProcessing(false)
          }
        })
    }
  },
  startTimeString(self) {
    if (self.StartTime()) {
      return new Date(self.StartTime()).format('ddd dd MMM HH:mm');
    }
    return "";
  }
};

SynergyScheduleGroupPHRBO = {
  //discipline
  triggerDisciplineIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterDisciplineIDRefreshAjax: function (args) { },
  onDisciplineIDSelected: function (item, businessObject) {
    SynergyScheduleGroupPHRBO.updateRoomSchedulePHR(businessObject)
  },

  //position type
  triggerPositionTypeIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionTypeIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionTypeIDRefreshAjax: function (args) { },
  onPositionTypeIDSelected: function (item, businessObject) {
    //if (item) {
    //  businessObject.PositionTypeID(item.PositionTypeID)
    //  businessObject.PositionID(item.PositionID)
    //  businessObject.Position(item.Position)
    //} else {
    //  businessObject.PositionTypeID(null)
    //  businessObject.PositionID(null)
    //  businessObject.Position("")
    //}
    SynergyScheduleGroupPHRBO.updateRoomSchedulePHR(businessObject)
  },

  //position
  triggerPositionIDAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setPositionIDCriteriaBeforeRefresh: function (args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionIDRefreshAjax: function (args) { },
  onPositionIDSelected: function (item, businessObject) {
    if (item) {
      businessObject.PositionTypeID(item.PositionTypeID)
      businessObject.PositionID(item.PositionID)
      businessObject.Position(item.Position)
    } else {
      businessObject.PositionTypeID(null)
      businessObject.PositionID(null)
      businessObject.Position("")
    }
    SynergyScheduleGroupPHRBO.updateRoomSchedulePHR(businessObject)
  },

  //hr
  triggerHumanResourceAutoPopulate: function (args) {
    args.AutoPopulate = true
  },
  setHumanResourceCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = new Date(args.Object.GetParent().StartTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(args.Object.GetParent().EndTime()).format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.PositionID = args.Object.PositionID()
    args.Data.PositionTypeID = args.Object.PositionTypeID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.CallTime = new Date(args.Object.GetParent().CallTime()).format("dd MMM yyyy HH:mm")
    args.Data.WrapTime = new Date(args.Object.GetParent().WrapTime()).format("dd MMM yyyy HH:mm")
  },
  afterHumanResourceRefreshAjax: function (args) {

  },
  onHumanResourceSelected: function (item, businessObject) {
    if (item) {
      businessObject.ResourceID(item.ResourceID)
    } else {
      businessObject.ResourceID(null)
    }
    SynergyScheduleGroupPHRBO.updateRoomSchedulePHR(businessObject)
    SynergyScheduleGroupBO.checkClashes(businessObject.GetParent())
  },
  onCellCreate: function (element, index, columnData, businessObject) {
    element.parentElement.style.width = "520px"
    element.parentNode.className = "hr-availability-list"
    element.className = 'hr-availability';
    element.innerHTML = '<div class="avatar">' +
      '<img src="' + window.SiteInfo.SitePath + '/Images/Profile/avatar3_50.jpg' + '" alt="Avatar">' +
      '</div>' +
      '<div class="info">' +
      '<table class="table table-condensed no-border">' +
      '<tbody>' +
      //header
      '<tr>' +
      '<td style="text-align:left;"><h4>' + businessObject.HRName + '</h4></td>' +
      '<td style="text-align:center; vertical-align:middle;"><span class="' + businessObject.ContractTypeCss + '">' + businessObject.ContractType + '</span></td>' +
      '<td style="text-align:right;"><h5>' + businessObject.TotalHours.toString() + ' / ' + businessObject.RequiredHours.toString() + ' Hours</h5></td>' +
      '</tr>' +
      //progress bar
      '<tr>' +
      '<td colspan="3">' +
      '<div class="progress small">' +
      '<div class="' + businessObject.ProgressBarCss + '" style="width:' + businessObject.HrsPercString + '"></div>' +
      '</div>' +
      '</td>' +
      '</tr>' +
      //other
      '<tr>' +
      '<td colspan="3"><span title="' + businessObject.ClashesTitle + '" class="' + businessObject.ClashesCss + '" style="width:100%;">' + businessObject.IsAvailableString + '</span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PublicHolidaysString + '" class="label label-info">' + businessObject.PublicHolidayCount.toString() + ' Public Holidays </span></td>' +
      '<td><span title="' + businessObject.OffWeekendsString + '" class="label label-info">' + businessObject.OffWeekendCount.toString() + ' Off Weekend/s </span></td>' +
      '<td><span title="' + businessObject.ConsecutiveDaysString + '" class="' + businessObject.ConsecutiveDaysCss + '">' + businessObject.ConsecutiveDays.toString() + ' Consecutive Days </span></td>' +
      '</tr>' +
      '<tr>' +
      '<td><span title="' + businessObject.PrevShiftEndString + '" class="' + businessObject.ShortfallPrevCss + '">' + businessObject.ShortfallPrevString + ' Shortfall </span></td>' +
      '<td></td>' +
      '<td><span title="' + businessObject.NextShiftStartString + '" class="' + businessObject.ShortfallNextCss + '">' + businessObject.ShortfallNextString + ' Shortfall </span></td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</div>'
  },
  HumanResourceIDSet: function (self) { },

  //set
  StartDateTimeBufferSet: function (hrBooking) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
  },
  StartDateTimeSet: function (hrBooking) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
    //if (!hrBooking.FreezeSetExpressions()) {
    //  RoomScheduleAreaBO.checkClashes(hrBooking.GetParent(),
    //                            function (response) {
    //                              //do something
    //                            },
    //                            function (response) {
    //                              //do something
    //                            })
    //}
  },
  EndDateTimeSet: function (hrBooking) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
    //if (!hrBooking.FreezeSetExpressions()) {
    //  RoomScheduleAreaBO.checkClashes(hrBooking.GetParent(),
    //                            function (response) {

    //                            },
    //                            function (response) {

    //                            })
    //}
  },
  EndDateTimeBufferSet: function (hrBooking) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
  },

  //rules
  CallTimeValid: function (Value, Rule, CtlError) {

  },
  StartTimeValid: function (Value, Rule, CtlError) {

  },
  EndTimeValid: function (Value, Rule, CtlError) {

  },
  WrapTimeValid: function (Value, Rule, CtlError) {

  },

  canEdit: function (self, FieldName) {
    switch (FieldName) {
      case "HumanResourceID":
        return (self.DisciplineID())
        break;
      default:
        return true
        break;
    }
  },
  updateRoomSchedulePHR: function (groupPHR) {
    var grp = groupPHR.GetParent()
    grp.RoomScheduleList().Iterate(function (rs, rsIndx) {
      rs.RoomSchedulePHRList().Iterate(function (phr, phrIndx) {
        if (phr.LinkedGuid() == groupPHR.Guid()) {
          phr.DisciplineID(groupPHR.DisciplineID())
          phr.Discipline(groupPHR.Discipline())
          phr.PositionTypeID(groupPHR.PositionTypeID())
          phr.PositionType(groupPHR.PositionType())
          phr.PositionID(groupPHR.PositionID())
          phr.Position(groupPHR.Position())
          phr.HumanResourceID(groupPHR.HumanResourceID())
          phr.HumanResource(groupPHR.HumanResource())
          phr.ResourceID(groupPHR.ResourceID())
        }
      })
    })
  }
};

RoomSchedulePHRBO = {
  //discipline
  triggerDisciplineIDAutoPopulate(args) {
    args.AutoPopulate = true
  },
  setDisciplineIDCriteriaBeforeRefresh(args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterDisciplineIDRefreshAjax(args) { },
  onDisciplineIDSelected(item, businessObject) { },

  //position type
  triggerPositionTypeIDAutoPopulate(args) {
    args.AutoPopulate = true
  },
  setPositionTypeIDCriteriaBeforeRefresh(args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionTypeIDRefreshAjax(args) { },
  onPositionTypeIDSelected(item, businessObject) {
    //if (item) {
    //  businessObject.PositionTypeID(item.PositionTypeID)
    //  businessObject.PositionID(item.PositionID)
    //  businessObject.Position(item.Position)
    //} else {
    //  businessObject.PositionTypeID(null)
    //  businessObject.PositionID(null)
    //  businessObject.Position("")
    //}
  },

  //position
  triggerPositionIDAutoPopulate(args) {
    args.AutoPopulate = true
  },
  setPositionIDCriteriaBeforeRefresh(args) {
    args.Data.SystemID = args.Object.GetParent().SystemID()
    args.Data.ProductionAreaID = args.Object.GetParent().ProductionAreaID()
    args.Data.DisciplineID = args.Object.DisciplineID()
    args.Data.HumanResourceID = args.Object.HumanResourceID()
  },
  afterPositionIDRefreshAjax(args) { },
  onPositionTypeIDSelected(item, businessObject) {
    if (item) {
      businessObject.PositionTypeID(item.PositionTypeID)
      businessObject.PositionID(item.PositionID)
      businessObject.Position(item.Position)
    } else {
      businessObject.PositionTypeID(null)
      businessObject.PositionID(null)
      businessObject.Position("")
    }
  },

  //hr
  triggerHumanResourceAutoPopulate(args) {
    args.AutoPopulate = true
  },
  setHumanResourceCriteriaBeforeRefresh(args) {
    var phr = args.Object
    var rs = phr.GetParent()
    args.Data.StartDateTime = new Date(rs.StartTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = new Date(rs.Endtime()).format("dd MMM yyyy HH:mm")
    args.Data.DisciplineID = phr.DisciplineID()
    args.Data.PositionID = phr.PositionID()
    args.Data.PositionTypeID = phr.PositionTypeID()
    args.Data.ProductionAreaID = rs.ProductionAreaID()
    args.Data.SystemID = rs.SystemID()
  },
  afterHumanResourceRefreshAjax(args) {

  },
  onHumanResourceSelected(item, businessObject) {
    if (item) {
      businessObject.ResourceID(item.ResourceID)
    } else {
      businessObject.ResourceID(null)
    }
    //the value of 'this', in the context of this method is the dropdown control and not the RoomScheduleAreaProductionHumanResourceBO js object
    //RoomScheduleAreaProductionHumanResourceBO.checkHumanResource(businessObject)
  },

  //set
  CallTimeSet: function (phr) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
  },
  StartTimeSet: function (phr) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
    //if (!hrBooking.FreezeSetExpressions()) {
    //  RoomScheduleAreaBO.checkClashes(hrBooking.GetParent(),
    //                            function (response) {
    //                              //do something
    //                            },
    //                            function (response) {
    //                              //do something
    //                            })
    //}
  },
  EndTimeSet: function (phr) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
    //if (!hrBooking.FreezeSetExpressions()) {
    //  RoomScheduleAreaBO.checkClashes(hrBooking.GetParent(),
    //                            function (response) {

    //                            },
    //                            function (response) {

    //                            })
    //}
  },
  WrapTimeSet: function (phr) {
    //RoomScheduleAreaProductionHRBookingBO.updateMismatch(hrBooking)
  },

  //rules
  CallTimeValid: function (Value, Rule, CtlError) {

  },
  StartTimeValid: function (Value, Rule, CtlError) {

  },
  EndTimeValid: function (Value, Rule, CtlError) {

  },
  WrapTimeValid: function (Value, Rule, CtlError) {

  }
};

SeriesItemBO = {
  //set
  EquipmentIDSet: function (self) {
  },

  //dropdowns
  triggerCircuitAutoPopulate: function (args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh: function (args) {
    args.Data.StartDateTime = null //new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = null //new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = 2 //args.Object.SystemID()
    args.Data.ProductionAreaID = 8 //args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax: function (args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected: function (selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.EquipmentName(selectedItem.EquipmentName)
      businessObject.DefaultEquipmentID(selectedItem.EquipmentID)
      businessObject.DefaultResourceID(selectedItem.ResourceID)
      businessObject.EquipmentScheduleBasicList().forEach(equipmentSchedule => {
        equipmentSchedule.EquipmentName(businessObject.EquipmentName())
        equipmentSchedule.ResourceID(businessObject.DefaultResourceID())
        equipmentSchedule.EquipmentID(businessObject.DefaultEquipmentID())
        EquipmentScheduleBasicBO.getClashes(equipmentSchedule)
      })
    }
    else {
      businessObject.EquipmentName("")
      businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
    }
  },
  async saveAll(seriesItem) {
    //for of will allow you to use 'await' inside a for loop
    for (let equipmentSchedule of seriesItem.EquipmentScheduleBasicList()) {
      try {
        //await each save result
        let data = await EquipmentScheduleBasicBO.getCreateFeedPromise(equipmentSchedule)
        SeriesItemBO.checkAllClashes(seriesItem)
        KOFormatter.Deserialise(data, equipmentSchedule)
        equipmentSchedule.IsProcessing(false)
      }
      catch (errorText) {
        eventItem.EquipmentScheduleBasic().IsProcessing(false)
      }
    }
    window.SiteHub.server.sendNotifications()
    window.siteHubManager.changeMade()
  },
  checkAllClashes(seriesItem) {
    seriesItem.EquipmentScheduleBasicList().forEach(item => {
      EquipmentScheduleBasicBO.getClashes(item)
    })
  }
};

EventItemBO = {
  async saveAll(byEventTemplate) {
    //for of will allow you to use 'await' inside a for loop
    for (let equipmentSchedule of byEventTemplate.EquipmentScheduleBasicList()) {
      try {
        //await each save result
        let data = await EquipmentScheduleBasicBO.getCreateFeedPromise(equipmentSchedule);
        EventItemBO.checkAllClashes(equipmentSchedule.GetParent());
        KOFormatter.Deserialise(data, equipmentSchedule);
        equipmentSchedule.IsProcessing(false);
        OBMisc.Notifications.GritterSuccess("Import succeeded", "", 250);
      }
      catch (errorText) {
        equipmentSchedule.IsProcessing(false);
        OBMisc.Notifications.GritterSuccess("Import failed", errorText, 1000);
      }
    }
    window.SiteHub.server.sendNotifications()
    window.siteHubManager.changeMade()
  },
  checkAllClashes(byEventTemplate) {
    byEventTemplate.EquipmentScheduleBasicList().forEach(item => {
      EquipmentScheduleBasicBO.getClashes(item);
    })
  }
};

ByEventTemplateBO = {
  //set
  EquipmentIDSet(self) {
  },
  RoomIDSet(self) {
  },

  //dropdowns
  triggerCircuitAutoPopulate(args) {
    args.AutoPopulate = true
    //args.Object.IsProcessing(true)
  },
  setCircuitCriteriaBeforeRefresh(args) {
    args.Data.StartDateTime = null //new Date(args.Object.StartDateTime()).format("dd MMM yyyy HH:mm")
    args.Data.EndDateTime = null //new Date(args.Object.EndDateTime()).format("dd MMM yyyy HH:mm")
    //args.Data.EquipmentName = //ViewModel.ROCircuitAvailabilityListCriteria().EquipmentName()
    args.Data.SystemID = 2 //args.Object.SystemID()
    args.Data.ProductionAreaID = 8 //args.Object.ProductionAreaID()
  },
  afterCircuitRefreshAjax(args) {
    //args.Object.IsProcessing(false)
  },
  onCircuitSelected(selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.EquipmentName(selectedItem.EquipmentName)
      businessObject.DefaultEquipmentID(selectedItem.EquipmentID)
      businessObject.DefaultResourceID(selectedItem.ResourceID)
      businessObject.EquipmentScheduleBasicList().forEach(equipmentSchedule => {
        equipmentSchedule.EquipmentName(businessObject.EquipmentName())
        equipmentSchedule.ResourceID(businessObject.DefaultResourceID())
        equipmentSchedule.EquipmentID(businessObject.DefaultEquipmentID())
        EquipmentScheduleBasicBO.getClashes(equipmentSchedule)
      })
    }
    else {
      businessObject.EquipmentName("")
      businessObject.EquipmentID(null)
      businessObject.ResourceID(null)
    }
  },

  //equipment
  async saveAllEquipmentSchedules(seriesItem) {
    //for of will allow you to use 'await' inside a for loop
    for (let equipmentSchedule of seriesItem.EquipmentScheduleBasicList()) {
      try {
        //await each save result
        let data = await EquipmentScheduleBasicBO.getCreateFeedPromise(equipmentSchedule)
        SeriesItemBO.checkAllEquipmentClashes(seriesItem)
        KOFormatter.Deserialise(data, equipmentSchedule)
        equipmentSchedule.IsProcessing(false)
      }
      catch (errorText) {
        equipmentSchedule.IsProcessing(false)
      }
    }
    window.SiteHub.server.sendNotifications()
    window.siteHubManager.changeMade()
  },
  checkAllEquipmentClashes(seriesItem) {
    seriesItem.EquipmentScheduleBasicList().forEach(item => {
      EquipmentScheduleBasicBO.getClashes(item)
    })
  },

  //room
  triggerRoomAutoPopulate(args) {
    args.AutoPopulate = true
    args.Object.IsProcessing(true)
  },
  setRoomCriteriaBeforeRefresh(args) {
    args.Data.StartDateTime = null
    args.Data.EndDateTime = null
    args.Data.SystemID = args.Object.SystemID()
    args.Data.ProductionAreaID = args.Object.ProductionAreaID()
  },
  afterRoomRefreshAjax(args) {
    args.Object.IsProcessing(false)
  },
  onRoomSelected(selectedItem, businessObject) {
    if (selectedItem) {
      businessObject.RoomName(selectedItem.RoomName)
      businessObject.DefaultRoomID(selectedItem.RoomID)
      businessObject.DefaultResourceID(selectedItem.ResourceID)
      businessObject.RoomScheduleList().forEach(equipmentSchedule => {
        equipmentSchedule.RoomName(businessObject.RoomName())
        equipmentSchedule.ResourceID(businessObject.DefaultResourceID())
        equipmentSchedule.RoomID(businessObject.DefaultRoomID())
        equipmentSchedule.SystemID(businessObject.DefaultSystemID())
        equipmentSchedule.ProductionAreaID(businessObject.DefaultProductionAreaID())
        RoomScheduleBO.checkClashes(equipmentSchedule)
      })
    }
    else {
      businessObject.RoomName("")
      businessObject.DefaultRoomID(null)
      businessObject.DefaultResourceID(null)
      equipmentSchedule.SystemID(null)
      equipmentSchedule.ProductionAreaID(null)
    }
  },

  async saveRoomSchedule(roomSchedule) {
    try {
      //await each save result
      let data = await RoomScheduleBO.save(roomSchedule);
      ByEventTemplateBO.checkAllRoomClashes(roomSchedule.GetParent());
      KOFormatter.Deserialise(data, roomSchedule);
      roomSchedule.IsProcessing(false);
      roomSchedule.ImportedSuccessfully(true);
      window.SiteHub.server.sendNotifications();
      window.siteHubManager.changeMade();
    }
    catch (errorText) {
      roomSchedule.IsProcessing(false);
      roomSchedule.ImportedSuccessfully(false);
    }
  },
  async saveAllRoomSchedules(template) {
    //for of will allow you to use 'await' inside a for loop
    for (let roomSchedule of template.RoomScheduleList()) {
      try {
        //await each save result
        let data = await RoomScheduleBO.save(roomSchedule);
        ByEventTemplateBO.checkAllRoomClashes(seriesItem);
        KOFormatter.Deserialise(data, roomSchedule);
        roomSchedule.IsProcessing(false);
      }
      catch (errorText) {
        roomSchedule.IsProcessing(false);
      }
    };
    window.SiteHub.server.sendNotifications();
    window.siteHubManager.changeMade();
  },
  checkAllRoomClashes(template) {
    template.RoomScheduleList().forEach(item => {
      RoomScheduleBO.checkClashes(item)
    })
  }

};

//#endregion

//#region other

//Finds an item in the list
Array.prototype.FindSerialised = function (Property, Value) {
  for (var i = 0; i < this.length; i++) {
    if (this[i][Property] == Value)
      return this[i];
  };
};

Array.prototype.FilterSerialised = function (Property, Value) {
  var ret = [];
  for (var i = 0; i < this.length; i++) {
    if (this[i][Property] == Value) {
      ret.push(this[i]);
    }
  }
  return ret;
};

Array.prototype.uniqueObjects = function () { return Array.from(new Set(this.map(JSON.stringify))).map(JSON.parse) }

Array.prototype.uniqueValues = function () { return Array.from(new Set(this)) }

//#endregion