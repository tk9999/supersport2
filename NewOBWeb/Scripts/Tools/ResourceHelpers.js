﻿ResourceHelper = {
  tempItem: null,

  //#region Groups---------------------------------------------------------------------------------------------------------------
  //All
  createGroupItem: function (resourceData) {
    var me = this;

    //clear the variables
    me.tempGroupItem = null;

    //create the item
    switch (resourceData.ResourceTypeID) {
      case 1:
        me.tempGroupItem = me.createHRGroup(resourceData);
        break;
      case 2:
        me.tempGroupItem = me.createRoomGroup(resourceData);
        break;
      case 3:
        me.tempGroupItem = me.createVehicleGroup(resourceData);
        break;
      case 4:
        me.tempGroupItem = me.createChannelGroup(resourceData);
        break;
      case 5:
        me.tempGroupItem = me.createEquipmentGroup(resourceData);
        break;
      case 6:
        me.tempGroupItem = me.createCustomGroup(resourceData);
        break;
    };
    return me.tempGroupItem;

  },
  getGroupContent: function (resourceData) {
    var me = this;
    //clear the variables
    me.tempGroupContent = null;
    //create the item
    switch (resourceData.ResourceTypeID) {
      case 1:
        me.tempGroupContent = me.getHRContent(resourceData);
        break;
      case 2:
        me.tempGroupContent = me.getRoomContent(resourceData);
        break;
      case 3:
        me.tempGroupContent = me.getVehicleContent(resourceData);
        break;
      case 4:
        me.tempGroupContent = me.getChannelContent(resourceData);
        break;
      case 5:
        me.tempGroupContent = me.getEquipmentContent(resourceData);
        break;
      case 6:
        me.tempGroupContent = me.getCustomContent(resourceData);
        break;
    };
    return me.tempGroupContent;
  },

  //HumanResources
  createHRGroup: function (resourceData) {
    var me = this;
    return {
      content: me.getHRContent(resourceData),
      Resource: resourceData
    }
  },
  getHRContent: function (resourceData) {
    var progressClass = 'progress-bar-info',
        iconClass = ''
    if (resourceData.PercUsed >= 80) {
      progressClass = "progress-bar-success"
      iconClass = "fa-success"
    }
    else if (resourceData.PercUsed >= 90) {
      progressClass = "progress-bar-warning"
      iconClass = "fa-warning"
    }
    else if (resourceData.PercUsed >= 100) {
      progressClass = "progress-bar-danger"
      iconClass = "fa-danger"
    }
    return "<div style='float:left; width:100%'>" +
                   "<span class='resource-icon' style='padding-right: 10px'><i class='fa fa-user " + iconClass + "'></i></span>" +
                   "<span class='resource-hr' style='padding-right: 10px'>" + resourceData.ResourceName + "</span>" +
           "</div>" +
           "<div style='float:left; width:100%'>" +
                   "<span class='resource-hr-est-shift' style='padding-right: 10px; padding-left:20px;'> Shifts: " + resourceData.TotalShifts + "</span>" +
                   "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Hrs: " + resourceData.TotalHours + "</span>" +
            "</div>" +
            "<div class='progress-bar " + progressClass + "' role='progressbar' aria-valuenow='" + resourceData.PercUsed.toString() + "' aria-valuemin='0' aria-valuemax='100' style='width:" + resourceData.PercUsed.toString() + "%; height:5px'></div>";
  },

  //Rooms
  createRoomGroup: function (resourceData) {
    var me = this;
    return {
      content: me.getRoomContent(resourceData),
      Resource: resourceData
    }
  },
  getRoomContent: function (resourceData) {
    return "<i class='fa fa-video-camera'></i> <span class='resource-room'>" + resourceData.ResourceName + "</span>";
  },

  //Vehicles
  createVehicleGroup: function (resourceData) {
    var me = this;
    return {
      content: me.getVehicleContent(resourceData),
      Resource: resourceData
    }
  },
  getVehicleContent: function (resourceData) {
    return "<i class='fa fa-truck'></i> <span class='resource-vehicle'>" + resourceData.ResourceName + "</span>";
  },

  //Channels
  createChannelGroup: function (resourceData) {
    var me = this;
    return {
      content: me.getChannelContent(resourceData),
      Resource: resourceData
    }
  },
  getChannelContent: function (resourceData) {
    return "<i class='fa fa-tv'></i> <span class='resource-vehicle'>" + resourceData.ResourceName + "</span>";
  },

  //Equipment
  createEquipmentGroup: function (resourceData) {
    var me = this;
    return {
      content: me.getEquipmentContent(resourceData),
      Resource: resourceData
    }
  },
  getEquipmentContent: function (resourceData) {
    return "<i class='fa fa-wrench'></i> <span class='resource-equipment'>" + resourceData.ResourceName + "</span>";
  },

  //Custom
  createCustomGroup: function (resourceData) {
    var me = this;
    return {
      content: me.getCustomContent(resourceData),
      Resource: resourceData
    };
  },
  getCustomContent: function (resourceData) {
    return "<i class='fa fa-wrench'></i> <span class='resource-equipment'>" + resourceData.ResourceName + "</span>";
  },

  //#endregion---------------------------------------------------------------------------------------------------------------------

  //#region Bookings---------------------------------------------------------------------------------------------------------------
  createBookingItem: function (resourceBookingData) {
    var me = this;
    me.tempItem = null
    var resourceIDToUse = null
    if (resourceBookingData.ResourceIDOverride) { resourceIDToUse = resourceBookingData.ResourceIDOverride } else { resourceIDToUse = resourceBookingData.ResourceID }
    me.tempGroupToAddTo = me.getGroupByResourceID(resourceIDToUse)
    if (me.tempGroupToAddTo) {
      me.tempItem = this.creatBookingItemBase(me.tempGroupToAddTo, resourceBookingData)
    }
    return me.tempItem
  },
  createBookingItemWithGroup: function (group, resourceBookingData) {
    var me = this
    me.tempItem = null
    me.tempItem = this.creatBookingItemBase(group, resourceBookingData)
    return me.tempItem
  },
  creatBookingItemBase: function (group, resourceBookingData) {
    var me = this
    me.tempItem = null
    switch (resourceBookingData.ResourceBookingTypeID) {
      //1	HR - OB City
      case 1:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //2	HR - OB (Content)
      case 2:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //3	HR - Studio
      case 3:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //4	HR - Leave
      case 4:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //5	HR - Secondment
      case 5:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //6	HR - Ad Hoc
      case 6:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //7	HR - Prep Time
      case 7:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //8	Room - Production
      case 8:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //9	Room - AdHoc
      case 9:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //10 Equipment - Feed
      case 10:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //11 Vehicle
      case 11:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //12 HR - Placeholder
      case 12:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //13 Room - Placeholder
      case 13:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //14 Equipment - Service
      case 14:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
        //15 Vehicle - Placeholder
      case 15:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
      case 16:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
      case 17:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
      case 18:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
      case 19:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
      case 20:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
      case 21:
        me.tempItem = me.getDefaultBookingItem(resourceBookingData, group);
        break;
    }
    return me.tempItem
  },
  getDefaultBookingItem: function (resourceBookingData, Group) {
    var me = this;
    return {
      content: me.getDefaultBookingContent(resourceBookingData),
      start: new Date(resourceBookingData.StartDateTimeBuffer ? resourceBookingData.StartDateTimeBuffer : resourceBookingData.StartDateTime),
      end: new Date(resourceBookingData.EndDateTimeBuffer ? resourceBookingData.EndDateTimeBuffer : resourceBookingData.EndDateTime),
      group: Group.id,
      type: 'range',
      className: me.getDefaultBookingCssClass(resourceBookingData),
      Booking: resourceBookingData,
      editable: !resourceBookingData.IsLocked,
      resourceid: resourceBookingData.ResourceID,
      resourcebookingid: resourceBookingData.ResourceBookingID
    };
  },
  getDefaultBookingCssClass: function (resourceBookingData) {
    var me = this;
    var baseClass = resourceBookingData.StatusCssClass;

    //clash
    if (resourceBookingData.IsClashing) {
      baseClass += " item-clashing";
    }

    try {
      if ([8, 9].indexOf(resourceBookingData.ResourceBookingTypeID) >= 0) {
        if (!me.roomScheduleContainsMyArea(resourceBookingData)) {
          baseClass += " vis-item-transparent";
        }
      }
    } catch (ex) {

    }

    return baseClass
  },
  getDefaultBookingContent: function (resourceBookingData) {
    var me = this;
    me.tempCssClass = "";
    me.tempIcon = "";
    me.tempDiv = null;

    //locked
    if (resourceBookingData.IsLocked) {
      me.tempCssClass += " item-locked";
      me.tempIcon += " <i class='fa fa-lock'></i> "
    }

    if (me.hasMissingRequirements(resourceBookingData)) {
      //me.tempCssClass += ' animated infinite tada go '
      me.tempIcon += " <i class='fa fa-warning animated infinite tada go'></i> "
    }

    me.tempDiv = "<div class='" + me.tempCssClass + "'>" + me.tempIcon + resourceBookingData.ResourceBookingDescription + "</div>"

    //edit mode
    var editDetailsDiv = "";
    if (resourceBookingData.IsBeingEditedBy != "") {
      editDetailsDiv = "<div class='item-in-edit-details'>" + "<i class='fa fa-edit'></i> " + "<span>" + resourceBookingData.IsBeingEditedBy + "</span>" + "</div>";
      me.tempDiv += editDetailsDiv
    }

    //busy
    var isBusyDiv = "";
    if (resourceBookingData.IsProcessing) {
      isBusyDiv = "<div class='item-isProcessing'>" + "<i class='fa fa-refresh fa-spin'></i>" + "</div>";
      me.tempDiv += isBusyDiv
    };

    me.tempCssClass = null;
    me.tempIcon = null;
    return me.tempDiv;

  },
  hasMissingRequirements: function (resourceBookingData) {
    //the dev can override this method
    try {
      var me = this,
    systemID = ViewModel.SystemID(),
    productionAreaID = ViewModel.DefaultProductionAreaID(),
    hasArea = false;
      if (resourceBookingData.RSResourceBookingAreaList.length > 0) {
        var filteredBySystem = resourceBookingData.RSResourceBookingAreaList.Filter('SystemID', systemID)
        var filteredBySystemAndArea = filteredBySystem.Filter('ProductionAreaID', productionAreaID)
        if (filteredBySystemAndArea.length == 1) {
          return (filteredBySystemAndArea[0].HasMissingRequirements)
        }
      }
    } catch (ex) {
      return false
    }
    return false
  },
  //#endregion---------------------------------------------------------------------------------------------------------------------

  //#region Methods

  getGroupByResourceID: function (resourcesDataSet, resourceID) {
    var me = this;
    var allResources = resourcesDataSet.get();
    var GR = null;
    allResources.Iterate(function (Group, GroupIndex) {
      if (!GR) {
        if (Group.Resource.ResourceID == resourceID) {
          GR = Group;
        };
      }
    });
    return GR;
  },
  addBookings: function (resourceBookingsDataSet, group, resourceBookings) {
    var me = this
    var templist = []
    resourceBookings.Iterate(function (itm, indx) {
      templist.push(ResourceHelper.createBookingItemWithGroup(group, itm))
    })
    resourceBookingsDataSet.add(templist)
  },

  //#endregion

  //#region Clash Checking

  getClashesByBooking: function (booking, afterFetch) {

    var templateBooking = {
      Guid: '',
      ResourceBookingID: null,
      ResourceID: null,
      ResourceBookingTypeID: null,
      ResourceBookingDescription: '',
      StartDateTimeBuffer: null,
      StartDateTime: null,
      EndDateTime: null,
      EndDateTimeBuffer: null,
      IsBeingEditedBy: '',
      InEditDateTime: null,
      HumanResourceShiftID: null,
      IsCancelled: false,
      HumanResourceOffPeriodID: null,
      HumanResourceSecondmentID: null,
      ProductionHRID: null,
      StatusCssClass: '',
      IgnoreClashes: false,
      IgnoreClashesReason: '',
      RoomScheduleID: null,
      IsLocked: false,
      IsLockedReason: '',
      IsTBC: false,
      EquipmentScheduleID: null,
      ProductionSystemAreaID: null,
      ParentResourceBookingID: null
    }
    templateBooking = $.extend({}, templateBooking, booking)
    var rbc = new ResourceBookingCleanObject()
    //rbc.Set(templateBooking)
    var s = KOFormatter.Serialise(booking)

    ViewModel.CallServerMethod("GetSingleBookingClashes",
    {
      ResourceBookingClean: s
    },
    function (response) {
      if (response.Success) {
        afterFetch(response)
      } else {
        OBMisc.Modals.Error('Error During Clash Check', 'An error occured while checking clashes', response.ErrorText, null)
      }
    })
  },
  getClashesByVariable: function (ResourceID, ResourceBookingID, StartDateTime, EndDateTime, afterFetch, HumanResourceShiftID) {
    var templateBooking = null
    if (ResourceID && StartDateTime && EndDateTime) {
      var rs = new ResourceBookingCleanObject()
      rs.ResourceBookingID(ResourceBookingID)
      rs.ResourceID(ResourceID)
      rs.StartDateTime(StartDateTime)
      rs.EndDateTime(EndDateTime)
      rs.HumanResourceShiftID(HumanResourceShiftID)
      templateBooking = rs.Serialise()
      ViewModel.CallServerMethod("GetSingleBookingClashes",
      {
        ResourceBookingClean: templateBooking
      },
      function (response) {
        if (!response.Success) {
          OBMisc.Notifications.GritterError('Error During Clash Check', response.ErrorText, null)
        }
        afterFetch(response)
      })
    }
  }

  //#endregion
}