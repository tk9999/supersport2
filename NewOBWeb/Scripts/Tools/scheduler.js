﻿class scheduler {

  constructor() {
    const me = this

    //options
    this.defaultOptions = {
      editSchedulerModal: "#EditResourceSchedulerModalNew"
    }
    this.isAdministrator = ViewModel.IsSchedulerAdministrator

    //ui properties and objects
    this.dom = {}
    this.dom.container = document.getElementById("scheduler-container")
    this.dom.container.className = 'resource-scheduler'
    this.dom.loadingBar = document.createElement("div")
    this.dom.loadingBar.innerHTML = `<div class="cssload-thecube">
                                        <div class="cssload-cube cssload-c1"></div>
                                        <div class="cssload-cube cssload-c2"></div>
                                        <div class="cssload-cube cssload-c4"></div>
                                        <div class="cssload-cube cssload-c3"></div>
                                     </div>
                                     <div id="loadingAction"></div>`;
    this.dom.sidebar = document.getElementsByClassName('cl-sidebar')[0];
    this.dom.currentUsers = null;

    //setup dimensions and positions
    let sidebarDimensions = this.dom.sidebar.getBoundingClientRect();
    this.dimensions = {
      reservedWidth: (sidebarDimensions.width + 60),
      reservedHeight: 108,
      resourcesColumnInitialWidth: 300,
      resourcesColumnBorderWidth: 1,
      resourcebookingsColumnBorderWidth: 1
    }
    this.positions = {
    }

    //resource properties
    this.allItems = []
    this.groupTarget = null
    this.subgroupTarget = null
    this.bookingsData = []

    //timeline properties
    this.dom.timelineElement = null
    this.timelineOptions = {
      editable: {
        add: false,
        updateTime: false,
        updateGroup: false,
        remove: false,
        overrideItems: false
      },
      selectable: true,
      multiselect: true,
      orientation: 'top',
      showCurrentTime: true,
      //showCustomTime: false,
      clickToUse: false,
      stack: false,
      start: null, //will be set later
      end: null, //will be set later
      min: null, //will be set later
      max: null, //will be set later
      zoomMin: 1000 * 60 * 60,                // 1 hour
      zoomMax: 1000 * 60 * 60 * 24 * 7 * 31,  // 1 month
      autoResize: true,
      type: 'range',
      moveable: true,
      margin: { item: -1 },
      width: '100%',
      height: '', //will be set later
      maxHeight: '', //will be set later
      order: function (booking1, booking2) {
        if (booking1.start > booking2.start) { return 1 }
        else if (booking1.start < booking2.start) { return -1 }
        else { return 0 };
      },
      groupOrder: function (resource1, resource2) {
        return (resource1.resource.ResourceOrder - resource2.resource.ResourceOrder);
      },
      onAdd: function (item, callback) {
        callback(null)
      },
      onUpdate: function (item, callback) {
        callback(null)
        let group = me.timelineResources.get(item.group)
        me.editTimelineItem(group, item)
      },
      onMoving: function (item, callback) {
        callback(null)
      },
      onMove: function (item, callback) {
        callback(null)
      },
      onRemove: function (item, callback) {
        callback(null)
      },
      snap: function (date, scale, step) {
        var minute = 60 * 1000;
        return Math.round(date / minute) * minute;
      },
      //itemHeight: this.schedulerData.MinViewGroupHeight,
      //new options
      //horizontalScroll: true,
      showTooltips: true,
      verticalScroll: true,
      itemHeight: ClientData.ResourceSchedulerData.MinViewGroupHeight
    }
    this.timelineResources = new vis.DataSet()
    this.timelineBookings = new vis.DataSet()
    this.timeline = null
    this.contextOptions = []

    //other properties
    this.schedulerData = ClientData.ResourceSchedulerData
    this.onlineUsers = {}
    this.readOnlyMode = false

    this.showLoadingBar()

    return this
  }

  afterConnected() {
    //draw the interface
    this.draw()
  };

  addOnlineUsers(usersData) {
    this.dom.currentUsers.innerHTML = `<b>Currently Online:</b>`;
    usersData.forEach((oUser) => {
      window.setTimeout(() => {
        if (oUser.ResourceSchedulerID == this.schedulerData.ResourceSchedulerID) {
          this.addOnlineUser(oUser)
        }
      }, 250);
    })
  };

  addOnlineUser(oUser) {
    let iconContainer = document.createElement('div');
    iconContainer.className = 'online-user-avatar-container animated rubberBand fast';
    iconContainer.setAttribute("title", oUser.Username);
    let icon = document.createElement('img');
    icon.className = "avatar avatar-round";
    icon.setAttribute("src", window.SiteInfo.SitePath + '/Images/Profile/' + oUser.ImageName);
    icon.setAttribute("alt", oUser.Username);
    icon.setAttribute("title", oUser.Username);
    icon.addEventListener('error', evt => {
      evt.target.setAttribute("src", window.SiteInfo.SitePath + '/Images/InternalPhotos/anonymous-user-icon.png');
    });
    iconContainer.appendChild(icon);
    this.dom.currentUsers.appendChild(iconContainer);
    let obj = { businessObject: oUser, element: iconContainer };
    Object.defineProperty(this.onlineUsers, oUser.ConnectionID, { value: obj, enumerable: true, writable: true, configurable: true });
    return obj;
  };

  removeOfflineUser(connectionID) {
    let onlineUser = this.onlineUsers[connectionID];
    if (onlineUser) {
      this.dom.currentUsers.removeChild(iconContainer);
      delete this.onlineUsers[connectionID]
    };
  };

  showLoadingBar() {
    return new Promise((resolve, reject) => {
      try {
        this.dom.container.style.visibility = 'hidden';
        this.dom.loadingBar.className = 'animated fadeIn fast go';
        this.dom.container.parentElement.insertBefore(this.dom.loadingBar, this.dom.container);
        resolve();
      }
      catch (err) {
        reject(err);
      }
    });
  };

  hideLoadingBar(afterHide = () => { }) {
    //hide loading bar
    this.dom.loadingBar.className = "animated fadeOut slowest go"
    //set a timeout to give the animation above some time to complete
    setTimeout(() => {
      //remove loading bar
      this.dom.container.parentElement.removeChild(this.dom.loadingBar)
      //show the layout
      this.dom.container.style.visibility = 'visible';
      this.dom.container.className = 'row animated fadeIn fastest go';
      afterHide();
    }, 400)
  };

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  };

  async setLoadingAction(actionDescription, delay) {
    const actionText = document.querySelector('#loadingAction');
    if (actionText) {
      actionText.innerHTML = actionDescription;
    };
    await this.sleep(delay);
  };

  async draw() {
    /* 1. get inital variables and set the initial dimensions of the container */
    let pageWidth = document.body.clientWidth;
    let expectedHeight = (document.body.clientHeight - this.dimensions.reservedHeight);
    let containerDimensions = this.dom.container.getBoundingClientRect();
    this.dom.container.style.height = expectedHeight;
    this.dom.container.style.width = pageWidth;

    /* 2. create the containers that will house the the resource selection and timeline controls */
    this.dom.resourcesColumn = document.createElement('div');
    this.dom.resourcesColumn.className = 'resources-column';
    this.dom.container.appendChild(this.dom.resourcesColumn);
    this.dom.resourcesColumn.style.height = expectedHeight.toString() + 'px';
    this.dom.resourcesColumn.style.width = this.dimensions.resourcesColumnInitialWidth.toString() + 'px';

    this.dom.editRow = document.createElement('div');
    this.dom.editRow.className = "edit-row";
    this.dom.resourcesColumn.appendChild(this.dom.editRow);

    this.dom.editSchedulerButton = document.createElement('i');
    this.dom.editSchedulerButton.id = "edit-scheduler";
    this.dom.editSchedulerButton.className = "fa fa-edit";
    this.dom.editSchedulerButton.setAttribute('title', 'Edit Scheduler');
    this.dom.editRow.appendChild(this.dom.editSchedulerButton);

    this.dom.refreshSchedulerButton = document.createElement('i');
    this.dom.refreshSchedulerButton.id = "refresh-scheduler";
    this.dom.refreshSchedulerButton.className = "fa fa-refresh";
    this.dom.refreshSchedulerButton.setAttribute('title', 'Refresh Data');
    this.dom.editRow.appendChild(this.dom.refreshSchedulerButton);

    this.dom.chooseDatesButton = document.createElement('i');
    this.dom.chooseDatesButton.id = "chooseDates-scheduler";
    this.dom.chooseDatesButton.className = "fa fa-calendar";
    this.dom.chooseDatesButton.setAttribute('title', 'Change Dates');
    this.dom.editRow.appendChild(this.dom.chooseDatesButton);

    this.dom.assignBookingsButton = document.createElement('i');
    this.dom.assignBookingsButton.id = "assign-bookings";
    this.dom.assignBookingsButton.className = "fa fa-american-sign-language-interpreting";
    this.dom.assignBookingsButton.setAttribute('title', 'Assign Shifts');
    this.dom.editRow.appendChild(this.dom.assignBookingsButton);

    this.dom.managePatternsButton = document.createElement('i');
    this.dom.managePatternsButton.id = "manage-patterns";
    this.dom.managePatternsButton.className = "fa fa-paypal";
    this.dom.managePatternsButton.setAttribute('title', 'Manage Shift Patterns');
    this.dom.editRow.appendChild(this.dom.managePatternsButton);

    this.dom.resourcesColumn.style.width = this.dimensions.resourcesColumnInitialWidth.toString() + 'px'

    this.dom.resourceBookingsColumn = document.createElement('div');
    this.dom.resourceBookingsColumn.className = 'resourcebookings-column';
    this.dom.container.appendChild(this.dom.resourceBookingsColumn);
    this.dom.resourceBookingsColumn.style.height = expectedHeight.toString() + 'px';
    let resourceBookingsWidth = (this.dimensions.reservedWidth + this.dimensions.resourcesColumnInitialWidth + this.dimensions.resourcesColumnBorderWidth + this.dimensions.resourcebookingsColumnBorderWidth);
    this.dom.resourceBookingsColumn.style.width = (pageWidth - resourceBookingsWidth).toString() + 'px';

    this.timelineOptions.height = expectedHeight.toString() + 'px';
    this.timelineOptions.maxHeight = this.timelineOptions.height;

    //select resources modal button
    this.dom.selectResourcesButton = document.createElement('i');
    this.dom.selectResourcesButton.id = "select-resources";
    this.dom.selectResourcesButton.className = "fa fa-check-square-o";
    this.dom.selectResourcesButton.setAttribute('title', 'Select Resources');
    this.dom.editRow.appendChild(this.dom.selectResourcesButton);
    if (pageWidth < 1100) {
      this.dom.selectResourcesButton.style.visibility = '';
    } else {
      this.dom.selectResourcesButton.style.visibility = 'hidden';
    };

    /* 3. populate the resources container */
    this.schedulerData.ResourceSchedulerGroupList.forEach(item => {
      this.createResourceGroupItem(item);
    });

    /* 4. add the online users */
    const usersContainer = document.querySelector('div.navbar-collapse');
    this.dom.currentUsers = document.createElement('div');
    this.dom.currentUsers.className = "current-users";
    usersContainer.appendChild(this.dom.currentUsers);

    /* 5. draw the timeline control */
    await this.setLoadingAction('<h3>Initializing...</h3>', 750);
    this.drawTimelineControl();

    /* 6. fetch bookings */
    this.fetchInitialBookings();
  };

  createResourceGroupItem(resourceSchedulerGroup) {

    //define group item
    let groupItem = {
      type: 'resource-group',
      selected: false,
      expanded: false,
      itemHeight: 40,
      element: null,
      contentElement: null,
      subGroupsElement: null,
      businessObject: resourceSchedulerGroup,
      subItems: []
    }

    //draw group
    groupItem.element = document.createElement('div')
    groupItem.element.className = 'resource-group'
    groupItem.element.item = groupItem
    groupItem.selected = groupItem.businessObject.SelectedByDefault;
    groupItem.element.style.height = groupItem.itemHeight.toString() + 'px'

    groupItem.contentElement = document.createElement('div')
    groupItem.contentElement.className = 'resource-group-content'
    groupItem.contentElement.innerHTML = this.getGroupItemHTML(groupItem)
    groupItem.element.appendChild(groupItem.contentElement)

    this.dom.resourcesColumn.appendChild(groupItem.element)
    this.allItems.push(groupItem)

    //create sub items
    groupItem.subGroupsElement = document.createElement('div')
    groupItem.subGroupsElement.className = "resource-group-subgroup-list scheduler-tree-child-list"
    groupItem.subGroupsElement.style.height = '0px'
    groupItem.element.appendChild(groupItem.subGroupsElement)
    groupItem.businessObject.ResourceSchedulerSubGroupList.forEach(subItem => {
      this.createSubGroupItem(groupItem, subItem)
    })

    return groupItem
  };

  createSubGroupItem(groupItem, subGroup) {

    //define subgroup item
    let subGroupItem = {
      type: 'resource-subgroup',
      selected: false,
      expanded: false,
      element: null,
      contentElement: null,
      resourcesElement: null,
      businessObject: subGroup,
      groupItem: groupItem,
      resources: [],
      subItems: []
    };

    //draw subgroup
    subGroupItem.element = document.createElement('div')
    subGroupItem.element.className = 'resource-subgroup'
    subGroupItem.element.item = subGroupItem
    subGroupItem.selected = subGroupItem.businessObject.SelectedByDefault;

    subGroupItem.contentElement = document.createElement('div')
    subGroupItem.contentElement.className = 'resource-subgroup-content'
    subGroupItem.contentElement.innerHTML = this.getSubGroupItemHTML(subGroupItem)
    subGroupItem.element.appendChild(subGroupItem.contentElement)

    groupItem.subGroupsElement.appendChild(subGroupItem.element)
    groupItem.subItems.push(subGroupItem)
    this.allItems.push(subGroupItem)

    //create resource items
    subGroupItem.resourcesElement = document.createElement('div')
    subGroupItem.resourcesElement.className = "resource-subgroup-resource-list scheduler-tree-child-list"
    subGroupItem.resourcesElement.style.height = '0px'
    subGroupItem.element.appendChild(subGroupItem.resourcesElement)
    subGroupItem.businessObject.ResourceSchedulerSubGroupResourceList.forEach(resourceItem => {
      this.createSubGroupResourceItem(subGroupItem, resourceItem)
    })

    return subGroupItem
  }

  createSubGroupResourceItem(subGroupItem, resource) {

    //define subgroup item
    let resourceItem = {
      type: 'resource',
      selected: false,
      element: null,
      businessObject: resource,
      subGroupItem: subGroupItem,
      timelineGroup: null, //this will be set later
      bookingItems: [] //this will be populated later
    }

    //draw subgroup
    resourceItem.element = document.createElement('div')
    resourceItem.element.className = 'resource-item'
    resourceItem.element.item = resourceItem
    resourceItem.selected = subGroupItem.businessObject.SelectedByDefault;
    resourceItem.element.innerHTML = (resourceItem.selected ? '<i class="fa fa-check-square-o"></i> ' : '')
      + resourceItem.businessObject.ResourceName

    subGroupItem.resourcesElement.appendChild(resourceItem.element)
    subGroupItem.subItems.push(resourceItem)
    this.allItems.push(resourceItem)

    return resourceItem
  }

  createTimelineItems() {
    let grp = null, item = null, tempResources = [], tempBookings = [];
    //reset current
    this.timelineResources.clear();
    this.timelineBookings.clear();
    this.timeline.setGroups(this.timelineResources);
    this.timeline.setItems(this.timelineBookings);

    //create the groups
    this.allItems.filter(item => { return (item.type == 'resource') })
      .forEach(res => {
        //create the group
        if (res.selected) {
          grp = null;
          //add the group
          grp = this.createTimelineGroup(res);
          let groupGuid = this.timelineResources.add(grp)[0];
          grp = this.timelineResources.get(groupGuid);
          res.timelineGroup = grp;
          //create the items
          this.bookingsData.filter(bookingData => {
            return (bookingData.DisplayResourceID === res.businessObject.ResourceID);
          }).forEach(booking => {
            item = null
            item = this.createTimelineItem(res.timelineGroup, booking);
            let itemGuid = this.timelineBookings.add(item)[0];
            item = this.timelineBookings.get(itemGuid);
            res.bookingItems.push(item);
          });
        };
      });

    //set new
    this.timeline.setGroups(this.timelineResources)
    this.timeline.setItems(this.timelineBookings)
  }

  addSubGroupToTimeline(subGroupItem) {
    let grp = null, item = null, tempResources = [], tempBookings = [];
    //update subgroup
    subGroupItem.selected = true
    subGroupItem.contentElement.innerHTML = this.getSubGroupItemHTML(subGroupItem)
    subGroupItem.subItems.forEach(res => {
      //create the group
      res.selected = true
      grp = null
      //add the group
      grp = this.createTimelineGroup(res)
      let groupGuid = this.timelineResources.add(grp)[0]
      grp = this.timelineResources.get(groupGuid)
      res.timelineGroup = grp
      //create the items
      this.bookingsData.filter(item => { return (item.ResourceID === res.businessObject.ResourceID) })
        .forEach(booking => {
          item = null
          item = this.createTimelineItem(res.timelineGroup, booking)
          let itemGuid = this.timelineBookings.add(item)[0]
          item = this.timelineBookings.get(itemGuid)
          res.bookingItems.push(item)
        })
    });

    //update group
    subGroupItem.groupItem.selected = true
    subGroupItem.groupItem.contentElement.innerHTML = this.getGroupItemHTML(subGroupItem.groupItem)

    //update bookings and resources
    this.timeline.setGroups(this.timelineResources);
    this.timeline.setItems(this.timelineBookings);
    //this.timeline.redraw();
  }

  removeSubGroupFromTimeline(subGroupItem) {
    let grp = null, item = null, tempResources = [], tempBookings = [], groupItemSelectionCount = 0;
    //update subgroup
    subGroupItem.selected = false
    subGroupItem.contentElement.innerHTML = this.getSubGroupItemHTML(subGroupItem)
    subGroupItem.subItems.forEach(res => {
      //create the group
      res.selected = false
      this.timelineResources.remove(res.timelineGroup.id)
    })

    //update group
    groupItemSelectionCount = subGroupItem.groupItem.subItems.filter(item => { return item.selected }).length
    subGroupItem.groupItem.selected = (groupItemSelectionCount > 0)
    subGroupItem.groupItem.contentElement.innerHTML = this.getGroupItemHTML(subGroupItem.groupItem)

    //update bookings and resources
    this.timeline.setGroups(this.timelineResources)
    this.timeline.setItems(this.timelineBookings)
  }

  getGroupItemHTML(groupItem) {
    return (groupItem.selected ? '<i class="fa fa-check-square-o"></i> ' : '')
      + groupItem.businessObject.ResourceSchedulerGroup + ' (' + groupItem.businessObject.ResourceSchedulerSubGroupList.length.toString() + ' item/s)'
  }

  getSubGroupItemHTML(subGroupItem) {
    return (subGroupItem.selected ? '<i class="fa fa-check-square-o"></i> ' : '')
      + subGroupItem.businessObject.ResourceSchedulerSubGroup + ' (' + subGroupItem.businessObject.ResourceSchedulerSubGroupResourceList.length.toString() + ' item/s)'
  }

  createTimelineGroup(resourceItem) {
    //create the item
    let item = {
      content: '',
      resourceItem: resourceItem,
      resource: resourceItem.businessObject
    }
    //create the content
    switch (resourceItem.businessObject.ResourceTypeID) {
      case 1:
        item.content = this.getHRContent(resourceItem.businessObject)
        break;
      case 2:
        item.content = "<span class='resource-room'>" + resourceItem.businessObject.ResourceName + "</span>"
        break;
      case 3:
        item.content = "<span class='resource-vehicle'>" + resourceItem.businessObject.ResourceName + "</span>"
        break;
      case 4:
        item.content = "<span class='resource-channel'>" + resourceItem.businessObject.ResourceName + "</span>"
        break;
      case 5:
        item.content = "<span class='resource-equipment'>" + resourceItem.businessObject.ResourceName + "</span>"
        break;
      case 6:
        item.content = "<span class='resource-custom'>" + resourceItem.businessObject.ResourceName + "</span>"
        break;
    }
    return item
  }

  createTimelineItem(timelineGroup, bookingData) {
    let tempItem = null, templist = [];

    //get the group to which the item belongs
    if (timelineGroup) {
      switch (bookingData.ResourceBookingTypeID) {
        case 1:
        case 2:
        case 3:
          //1 =	HR OB (Services), 2 =	HR OB (Content), 3 = HR Room
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 4:
        case 5:
          //4	= HR Leave, 5 = HR Secondment
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 6:
        case 7:
          //6	HR - Ad Hoc, 7	HR - Prep Time
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 8:
        case 9:
          //8	Room - Production, 9	Room - AdHoc
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 10:
        case 14:
          //10 = Equipment - Feed, 14 = Equipment - Service
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 11:
        case 12:
        case 21:
          //Playout MCR, PLayout SCCR, Playout Generic
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        //13 HR Unavailable
        case 13:
          if (!bookingData.DisplayInBackground) {
            tempItem = this.getBookingItem(timelineGroup, bookingData)
          } else {
            tempItem = this.createBackgroundItem(bookingData, timelineGroup)
          }
          break;
        //15 Vehicle - Placeholder
        case 15:
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 16:
        case 17:
        case 18:
          //HR - AdHoc Training, HR - AdHoc Conference, HR - AdHoc Meeting
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        case 19:
        case 20:
        case 22:
        case 23:
        case 24:
        case 25:
          //Studio Supervisor Shift, ICR Shift, Programming Shift, StageHand Shift, Prep Shift, HQ Shift
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
        //Outside Broadcast
        case 26:
          tempItem = this.getBookingItem(timelineGroup, bookingData)
          break;
      }
    }

    return tempItem;

  }

  getBookingItem(timelineGroup, bookingData) {
    return {
      content: this.getBookingItemContent(bookingData),
      start: new Date(bookingData.StartDateTimeBuffer ? bookingData.StartDateTimeBuffer : bookingData.StartDateTime),
      end: new Date(bookingData.EndDateTimeBuffer ? bookingData.EndDateTimeBuffer : bookingData.EndDateTime),
      group: timelineGroup.id,
      type: 'range',
      className: this.getBookingItemCssClass(bookingData),
      businessObject: bookingData,
      editable: {
        remove: false,
        updateGroup: false,
        updateTime: false
      }
    };
  };

  getBookingItemContent(bookingData) {
    let tempCssClass = "", tempIcon = "", isBusyDiv = "",
      editDetailsDiv = "", tempDiv = null

    //locked
    if (bookingData.IsLocked) {
      tempCssClass += " item-locked";
      tempIcon += " <i class='fa fa-lock'></i> "
    }

    //popover
    tempDiv = "<div class='" + tempCssClass + "'" + " title='" + bookingData.PopoverContent + "'" + ">" + tempIcon + bookingData.ResourceBookingDescription + "</div>"

    //edit mode
    if (bookingData.IsBeingEditedBy != "") {
      editDetailsDiv = "<div class='item-in-edit-details'>" + "<i class='fa fa-edit'></i> " + "<span>" + bookingData.IsBeingEditedBy + "</span>" + "</div>"
      tempDiv += editDetailsDiv
    }

    //area access
    if (!bookingData.HasAreaAccess && bookingData.IsBeingEditedBy == "" && !bookingData.IsProcessing) {
      tempDiv += "<div class='item-in-edit-details'>" + "<i class='fa fa-lock'></i> " + "<span>" + (bookingData.AreaName ? "Unknown" : "") + "</span>" + "</div>"
    }

    //busy
    if (bookingData.IsProcessing) {
      isBusyDiv = "<div class='item-isProcessing'>" + "<i class='fa fa-refresh fa-spin'></i>" + "</div>"
      tempDiv += isBusyDiv
    }

    return tempDiv
  }

  getBookingItemCssClass(bookingData) {
    if (bookingData.IsClashing) {
      bookingData.StatusCssClass += " item-clashing";
    }
    return bookingData.StatusCssClass
  }

  getHRContent(resourceData) {
    return "<span class='resource-human'>" + resourceData.ResourceName + "</span>"
    //let  progressClass = 'progress-bar-info',
    //     iconClass = '';

    //if (resourceData.PercUsed >= 80) {
    //  progressClass = "progress-bar-success"
    //  iconClass = "fa-success"
    //}
    //else if (resourceData.PercUsed >= 90) {
    //  progressClass = "progress-bar-warning"
    //  iconClass = "fa-warning"
    //}
    //else if (resourceData.PercUsed >= 100) {
    //  progressClass = "progress-bar-danger"
    //  iconClass = "fa-danger"
    //}
    // + resourceData.HumanResourceID.toString() 
    //var basic = "<div style='float:left; width:100%'>" +
    //               //"<img class='resource-image' src='../Images/Profile/152" + "_5.jpg' alt='Avatar'> " + resourceData.ResourceName +
    //               "<span class='resource-icon' style='padding-right: 10px'><i class='fa fa-user " + iconClass + "'></i></span>" +
    //               "<span class='resource-hr' style='padding-right: 10px'>" + resourceData.ResourceName + "</span>" +
    //            "</div>"

    //var stats = "<div style='float:left; width:100%'>" +
    //               "<span class='resource-hr-est-hrs' style='padding-right: 10px'> CO: " + resourceData.StartingBalanceHours + "</span>" +
    //               "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Tgt: " + resourceData.TargetHours + "</span>" +
    //               "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Curr: " + resourceData.TotalHours + "</span>" +
    //               "<span class='resource-hr-est-hrs' style='padding-right: 10px'> Req: " + resourceData.ReqHours + "</span>" +
    //            "</div>"

    //var basic = '<div class="resource-group">' +
    //                    resourceData.ResourceName +
    //                //'<div class="human-resource-name">' + resourceData.ResourceName + '</div>' +
    //                //'<div class="progress medium">' +
    //                //    '<div class="progress-bar progress-bar-success" style="width: ' + (resourceData.UtilPerc ? resourceData.UtilPerc.toString() : '0') + '%"></div>' +
    //                //'</div>' +
    //            '</div>'

    //if (me.schedulerData.ShowHRStats) {
    //  return basic //+ stats
    //}
    //else {
    //return basic
    //}
  }

  editTimelineItem(timelineGroup, timelineItem) {
    const me = this
    //get the group to which the item belongs
    switch (timelineItem.businessObject.ResourceBookingTypeID) {
      case 1:
      case 2:
      case 6:
      case 7:
      case 13:
      case 15:
      case 16:
      case 17:
      case 18:
      case 26:
        // 1 =	HR OB (Services), 2 =	HR OB (Content)
        // 6 =	HR - Ad Hoc, 7 = HR - Prep Time
        // 13 = HR Unavailable
        // 15 = Vehicle - Placeholder
        // 16 = HR - AdHoc Training, 17 = HR - AdHoc Conference, 18 = HR - AdHoc Meeting
        // 26 = Outside Broadcast
        // do nothing
        break;
      case 3:
      case 8:
      case 9:
        this.setItemBusy(timelineItem);
        // 3 = HR Room, 8	Room - Production, 9	Room - AdHoc
        RoomScheduleAreaBO.getRoomScheduleArea({
          criteria: {
            roomScheduleID: timelineItem.businessObject.RoomScheduleID,
            productionSystemAreaID: timelineItem.businessObject.ProductionSystemAreaID,
            resourceBookingID: timelineItem.businessObject.ResourceBookingID
          },
          onSuccess: (data) => {
            ViewModel.CurrentProduction(null);
            ViewModel.CurrentAdHocBooking(null);
            ViewModel.CurrentRoomScheduleArea.Set(data[0]);
            //ViewModel.CurrentRoomScheduleArea().Room(response.Data.Room)
            //ViewModel.CurrentRoomScheduleArea().RoomID(response.Data.RoomID)
            RoomScheduleAreaBO.showModal()
            me.setItemNotBusy(timelineItem)
          },
          onFail: (response) => {
            me.setItemNotBusy(timelineItem)
          }
        })
        break;
      case 4:
        // 4	= HR Leave
        //HumanResourceOffPeriodBO.get()
        break;
      case 5:
        // 5 = HR Secondment
        //HumanResourceSecondmentBO.get()
        break;
      case 10:
        // 10 = Equipment - Feed      
        break;
      case 14:
        // 14 = Equipment - Service       
        break;
      case 11:
      case 12:
      case 21:
        //PlayoutShiftBO.get()
        break;
        break;
      case 20:
        //ICRShiftBO.get()
        break;
      case 19:
      case 22:
      case 23:
      case 24:
      case 25:
        this.setItemBusy(timelineItem);
        GenericShiftBO.editShiftScheduler(timelineItem, timelineGroup, (data) => {
          this.setItemNotBusy(timelineItem);
        }, (errorText) => {
          this.setItemNotBusy(timelineItem);
        });
        break;
    }
  }

  setItemBusy(timelineItem) {
    timelineItem.businessObject.IsProcessing = true
    timelineItem.businessObject.IsBeingEditedBy = ViewModel.CurrentUserName()
    timelineItem.content = this.getBookingItemContent(timelineItem.businessObject)
    this.timelineBookings.update(timelineItem)
  };

  setItemNotBusy(timelineItem) {
    timelineItem.businessObject.IsProcessing = false
    timelineItem.businessObject.IsBeingEditedBy = ""
    timelineItem.content = this.getBookingItemContent(timelineItem.businessObject)
    this.timelineBookings.update(timelineItem)
  };

  drawTimelineControl() {

    //create the timeline element
    this.dom.timelineElement = document.createElement('div')
    this.dom.timelineElement.id = 'timeline'
    this.dom.resourceBookingsColumn.appendChild(this.dom.timelineElement)

    //setup the initial dates
    let initialSD = moment(this.schedulerData.CurrentStartDate).startOf('month'), //this.schedulerData.CurrentStartDate
      initialED = moment(this.schedulerData.CurrentEndDate).endOf('month'); //this.schedulerData.CurrentEndDate
    let bufferedSD = initialSD.clone().add('months', -1).toDate(),
      bufferedED = initialED.clone().add('months', 2).toDate();
    let focusedSD = moment(this.schedulerData.CurrentStartDate), //this.schedulerData.CurrentStartDate
      focusedED = moment(this.schedulerData.CurrentEndDate); //this.schedulerData.CurrentEndDate
    this.timelineOptions.start = focusedSD.toDate();
    this.timelineOptions.end = focusedED.toDate();
    this.timelineOptions.min = bufferedSD;
    this.timelineOptions.max = bufferedED;

    //setup the timeline without its resource and bookings for now
    this.timeline = new vis.Timeline(this.dom.timelineElement)
    this.timeline.setGroups(this.timelineResources)
    this.timeline.setItems(this.timelineBookings)
    this.timeline.setOptions(this.timelineOptions)

  };

  async fetchInitialBookings() {

    let sd = this.timelineOptions.min.format('dd MMM yyyy'),
      ed = this.timelineOptions.max.format('dd MMM yyyy');

    await this.showLoadingBar();
    await this.setLoadingAction('<h3>Fetching bookings from ' + sd + ' to ' + ed, 500);

    let IDs = this.allItems.filter(item => { return (item.type == 'resource') }).map(item => { return item.businessObject.ResourceID })

    this.getResourceBookings({
      criteria: {
        ResourceIDs: IDs,
        StartDate: sd,
        EndDate: ed
      },
      onSuccess: async (data) => {
        await this.setLoadingAction('<h3>Bookings fetched</h3>', 500);
        this.bookingsData = data;
        await this.setLoadingAction('<h3>Drawing...</h3>', 500);
        this.createTimelineItems();
        this.addEventListeners();
        this.timeline.setWindow(this.timelineOptions.start, this.timelineOptions.end);
        this.hideLoadingBar(() => {
          //hiding the loading bar is causing a draw issue on the timeline, so just call redraw manually after the hide is complete
          this.timeline.redraw();
        });
        window.siteHubManager.getConnectedSchedulers(this.schedulerData.ResourceSchedulerID);
      }
    })
  };

  async refreshBookings(defaults = { beforeFetch: () => { } }) {
    let sd = this.timelineOptions.min.format('dd MMM yyyy'),
      ed = this.timelineOptions.max.format('dd MMM yyyy');

    await this.showLoadingBar();
    await this.setLoadingAction('<h3>Fetching bookings from ' + sd + ' to ' + ed, 500);

    defaults.beforeFetch();

    let IDs = this.allItems.filter(item => { return (item.type == 'resource') }).map(item => { return item.businessObject.ResourceID })
    this.getResourceBookings({
      criteria: {
        ResourceIDs: IDs,
        StartDate: sd,
        EndDate: ed
      },
      onSuccess: async (data) => {
        await this.setLoadingAction('<h3>Bookings fetched</h3>', 500);
        this.bookingsData = data;
        await this.setLoadingAction('<h3>Drawing...</h3>', 500);
        this.createTimelineItems();
        this.timeline.setOptions(this.timelineOptions);
        this.hideLoadingBar();
        window.siteHubManager.getConnectedSchedulers(this.schedulerData.ResourceSchedulerID);
      }
    })
  };

  removeEventListeners() {
    this.dom.resourcesColumn.removeEventListener('click', this.resourcesColumnClick);
    this.timeline.off('contextmenu');
    window.removeEventListener('resize', this.onWindowResized);
  };

  addEventListeners() {
    this.dom.resourcesColumn.addEventListener('click', this.resourcesColumnClick.bind(this));
    this.timeline.on('contextmenu', this.timelineContextMenu.bind(this));
    window.addEventListener('resize', this.onWindowResized.bind(this));
  };

  chooseDates() {
    //initial setup of date picker
    $(this.dom.chooseDatesButton).daterangepicker({
      format: 'ddd D MMM YYYY',
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        'Last 7 Days': [moment().subtract('days', 6), moment()],
        'Last 30 Days': [moment().subtract('days', 29), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
      },
      startDate: this.timelineOptions.start,
      endDate: this.timelineOptions.end,
      linkedCalendars: true,
      autoUpdateInput: true
    });
    $(this.dom.chooseDatesButton).on('show.daterangepicker', (event, picker) => { picker.showCalendars(); });
    $(this.dom.chooseDatesButton).on('apply.daterangepicker', this.onDateRangeChanged.bind(this));
  };

  onDateRangeChanged(event, picker) {
    this.timelineOptions.start = picker.startDate.startOf('day');
    this.timelineOptions.end = picker.endDate.endOf('day');
    this.timelineOptions.min = this.timelineOptions.start.clone().add('months', -1).startOf('month').toDate();
    this.timelineOptions.max = this.timelineOptions.end.clone().add('months', 2).endOf('month').toDate();
    this.refreshBookings({
      beforeFetch: () => {
        this.timeline.setOptions(this.timelineOptions);
      }
    });
  };

  resourcesColumnClick(evt) {
    evt.stopPropagation();
    this.groupTarget = null;
    this.subgroupTarget = null;
    if (evt.target.classList.contains("resource-group") || evt.target.parentNode.classList.contains("resource-group")) {
      this.groupTarget = (evt.target.item || evt.target.parentNode.item);
      requestAnimationFrame(() => { this.updateResourceGroups() });
    }
    else if (evt.target.classList.contains("resource-subgroup") || evt.target.parentNode.classList.contains("resource-subgroup")) {
      this.subgroupTarget = (evt.target.item || evt.target.parentNode.item);
      this.subGroupClicked();
      requestAnimationFrame(() => { this.updateResourceGroups() });
    }
    else if (evt.target.id == "edit-scheduler") {
      this.editScheduler();
    }
    else if (evt.target.id == "refresh-scheduler") {
      this.refreshBookings();
    }
    else if (evt.target.id == "chooseDates-scheduler") {
      this.chooseDates();
    }
    else if (evt.target.id == "assign-bookings") {
      this.assignBookings();
    }
    else if (evt.target.id == "manage-patterns") {
      this.managePatterns();
    }
    else if (evt.target.id == "select-resources") {
      this.selectResources();
    };
  };

  timelineContextMenu(props) {
    let eventProps = this.timeline.getEventProperties(props.event);
    let timelineItem = null, timelineGroup = null;

    props.event.preventDefault();
    timelineContext.destroy();
    timelineContext.init();
    this.contextOptions = [{ header: 'Options' }];

    switch (eventProps.what) {
      case "item":
        timelineItem = this.timelineBookings.get(eventProps.item)
        timelineGroup = this.timelineResources.get(timelineItem.group)
        this.itemContextMenu(timelineGroup, timelineItem, props)
        break;
      case "background":
        timelineGroup = this.timelineResources.get(eventProps.group)
        this.backgroundContextMenu(timelineGroup, props)
        break;
      case "group-label":
        timelineGroup = this.timelineResources.get(eventProps.group, props.event)
        this.groupContextMenu(timelineGroup, props)
        break;
      default:
        break;
    }

  };

  itemContextMenu(timelineGroup, timelineItem, props) {
    if (!this.readOnlyMode) {

      //1. make the current item is also selected
      this.ensureSelected(props.item, props.actualItem);

      //booking time
      let bookingTime = timelineItem.start.format('dd MMM yyyy HH:mm')

      switch (timelineItem.businessObject.ResourceBookingTypeID) {
        //4	= HR - Leave
        case 4:
          //this.leaveContextOptions(GroupItem, BookingItem, eventProperties);
          break;
        //5	= HR - Secondment
        case 5:
          //this.secondmentContextOptions(GroupItem, BookingItem, eventProperties);
          break;
        case 3:
        case 8:
        case 9:
          //3 = HR - Room, 8	= Room - Production, 9 = Room - AdHoc
          this.roomContextMenuOptions(timelineGroup, timelineItem, props);
          break;
        case 13:
          //13 HR - Unavailable
          //this.unavailableContextOptions(GroupItem, BookingItem, eventProperties);
          break;
        case 10:
        case 14:
          //10 Equipment - Feed, 14 = Equipment - Service
          //this.equipmentContextOptions(GroupItem, BookingItem, eventProperties);
          break;
        case 20:
          //ICR Shift
          //this.icrShiftContextOptions(GroupItem, BookingItem, eventProperties);
          break;
        case 11:
        case 12:
        case 21:
          //Playout Shift
          //this.playoutShiftOptions(GroupItem, BookingItem, eventProperties)
          break;
        case 19:
        case 22:
        case 23:
        case 24:
        case 25:
        case 26:
          //Other Shifts
          this.standardShiftOptions(timelineGroup, timelineItem, props)
          break;
        case 1:
        case 2:
        case 6:
        case 7:
        case 15:
        case 16:
        case 17:
        case 18:
          //do nothing
          break;
      }
      timelineContext.show('.inline-menu', this.contextOptions, props.event)
    }
  };

  roomContextMenuOptions(groupItem, bookingItem, props) {
    if (bookingItem.businessObject.HasAreaAccess) {
      //statuses-----------------------------------
      const statusOptions = this.getStatusOptions(bookingItem.businessObject.SystemID, bookingItem.businessObject.ProductionAreaID,
        { /*no custom args*/ },
        (statusSelectedEvent) => { this.changeBookingStatuses(statusSelectedEvent.data.status); });
      this.contextOptions.push(statusOptions);

      //copy crew----------------------------------
      const selections = this.getSelectedItems();
      if (!bookingItem.businessObject.IsCancelled && !bookingItem.businessObject.IsLocked && selections.length == 2) {
        this.contextOptions.push({
          text: 'Copy Crew',
          href: "#", args: { eventProps: props, selections: selections },
          action: (event) => {
            this.copyCrew(event)
          }
        })
      };

      //remove------------------------------------
      if (!bookingItem.businessObject.IsLocked) {
        this.contextOptions.push({
          text: 'Remove',
          href: "#", args: { eventProps: props, selections: selections },
          action: (event) => {
            this.deleteRoomScheduleAreas(event, false)
          }
        })
      };
    };

  };

  getStatusOptions(systemID, productionAreaID, otherArgs, actionMethod) {
    const filteredStatuses = ClientData.ROSystemProductionAreaStatusSelectList.filter(status => { return (status.SystemID == systemID && status.ProductionAreaID == productionAreaID) });
    const statusOptions = {
      text: "Status",
      subMenu: []
    };
    filteredStatuses.forEach((stat) => {
      let newArgs = $.extend({}, otherArgs);
      newArgs.status = stat;
      statusOptions.subMenu.push({
        text: stat.ProductionAreaStatus,
        href: "#",
        args: newArgs,
        action: actionMethod
      });
    });
    return statusOptions;
  };

  standardShiftOptions(groupItem, bookingItem, props) {
    this.contextOptions.push({
      text: 'Remove', href: "#",
      args: { groupItem: groupItem, bookingItem: bookingItem },
      action: (args) => {
        const selectedItems = this.getSelectedItems();
        const selectedBookings = this.getSelectedBookings();
        selectedItems.forEach(item => {
          this.setItemBusy(item);
        });
        GenericShiftBO.removeShifts(selectedBookings,
          (data) => {
            //success
          },
          (errorText) => {
            //fail
          });
      }
    })
  };

  backgroundContextMenu(timelineGroup, props) {
    if (!this.readOnlyMode) {
      //let bookingTime = props.time.format('dd MMM yyyy HH:mm');;
      this.contextOptions = [{ header: 'Options' }]
      switch (timelineGroup.resource.ResourceTypeID) {
        case 1:
          //Human
          this.humanResourceBackgroundContextOptions(timelineGroup, props);
          break;
        case 2:
          //Room
          this.roomBackgroundContextOptions(timelineGroup, props);
          break;
        case 3:
          //Vehicle
          //this.vehicleBackgroundContextOptions(groupItem, eventProperties)
          break;
        case 4:
          //Channel
          break;
        case 5:
          //Equipment
          //this.equipmentBackgroundContextOptions(groupItem, eventProperties)
          break;
        case 6:
          //Custom
          this.customBackgroundContextOptions(timelineGroup, props);
          break;
      }
      timelineContext.show('.inline-menu', this.contextOptions, event)
    }
  };

  humanResourceBackgroundContextOptions(timelineGroup, props) {
    if (!this.readOnlyMode) {

      //Services
      if (this.schedulerData.SystemID == 1) {
        this.contextOptions.push({
          text: 'New Shift', href: "#",
          args: { group: timelineGroup, origEvt: props },
          action: (args) => {
            GenericShiftBO.newShiftResourceScheduler(args, {
              SystemID: 1, HumanResourceID: args.data.group.resource.HumanResourceID, ResourceID: args.data.group.resource.ResourceID, HumanResource: args.data.group.resource.ResourceName,
              ProductionAreaID: null,
              ShiftTypeID: null, ShiftType: "",
              DisciplineID: null, Discipline: ""
            })
          }
        });
      };

      //Content
      if (this.schedulerData.SystemID == 2) {
        this.contextOptions.push({
          text: 'New Shift', href: "#",
          args: { group: timelineGroup, origEvt: props },
          action: (args) => {
            GenericShiftBO.newShiftResourceScheduler(args, {
              SystemID: 2, HumanResourceID: args.data.group.resource.HumanResourceID, ResourceID: args.data.group.resource.ResourceID, HumanResource: args.data.group.resource.ResourceName,
              ProductionAreaID: null,
              ShiftTypeID: null, ShiftType: "",
              DisciplineID: null, Discipline: ""
            });
          }
        });
      };

      //ICR
      if (this.schedulerData.SystemID == 4) {
        this.contextOptions.push({
          text: 'New Shift', href: "#",
          args: { group: timelineGroup, origEvt: props },
          action: (args) => {
            ICRShiftBO.newShiftResourceScheduler(args, {
              SystemID: 4, HumanResourceID: args.data.group.resource.HumanResourceID, ResourceID: args.data.group.resource.ResourceID, HumanResource: args.data.group.resource.ResourceName,
              ProductionAreaID: null,
              ShiftTypeID: null, ShiftType: "",
              DisciplineID: null, Discipline: ""
            });
          }
        });
      };

      //Programming
      if (this.schedulerData.SystemID == 8) {
        this.contextOptions.push({
          text: 'New Shift', href: "#",
          args: { group: timelineGroup, origEvt: props },
          action: (args) => {
            GenericShiftBO.newShiftResourceScheduler(args, {
              SystemID: 8, HumanResourceID: args.data.group.resource.HumanResourceID, ResourceID: args.data.group.resource.ResourceID, HumanResource: args.data.group.resource.ResourceName,
              ProductionAreaID: null,
              ShiftTypeID: null, ShiftType: "",
              DisciplineID: null, Discipline: ""
            })
          }
        });
      };

      ////Playout
      //if (this.schedulerData.SystemID == 5) {
      //  this.contextOptions.push({
      //    text: 'Playout Shift', href: "#",
      //    args: { group: timelineGroup },
      //    action: function (args) {
      //      //me.playoutOpsShiftModal().newShiftResourceScheduler(groupItem, args)
      //    }
      //  });
      //};

    }
  };

  roomBackgroundContextOptions(timelineGroup, props) {
    if (!this.readOnlyMode) {
      const createOptions = { text: "Create", subMenu: [] };

      if ([2, 5].indexOf(this.schedulerData.SystemID) >= 0) {

        //Blank Production
        createOptions.subMenu.push({
          text: 'Blank Production', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 4, event); }
        });

        //Team Building
        createOptions.subMenu.push({
          text: 'Team Building', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 4, event); }
        });

        //Training
        createOptions.subMenu.push({
          text: 'Training', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 6, event); }
        });

        //Meeting
        createOptions.subMenu.push({
          text: 'Meeting', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 7, event); }
        });

        //Studio Maintenance
        createOptions.subMenu.push({
          text: 'Maintenance', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 8, event); }
        });

        //Conference
        createOptions.subMenu.push({
          text: 'Conference', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 8, event); }
        });

        //Carte Blanche
        createOptions.subMenu.push({
          text: 'Carte Blanche', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 1, event); }
        });

        //Blitz AM
        createOptions.subMenu.push({
          text: 'Blitz AM', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 2, event); }
        });

        //Blitz PM
        createOptions.subMenu.push({
          text: 'Blitz PM', href: "#", args: { eventProps: props },
          action: (event) => { RoomScheduleTemplateBO.newRoomScheduleTemplate(timelineGroup.resource, 3, event); }
        });
      };

      this.contextOptions.push(createOptions);
    }
  };

  customBackgroundContextOptions(timelineGroup, props) {
    if (!this.readOnlyMode) {
      if (timelineGroup.resource.ResourceID == 5201) {
        //Studio Supervisor Shift
        this.contextOptions.push({
          text: 'New Shift', href: "#", args: { eventProps: props, group: timelineGroup },
          action: (args) => {
            GenericShiftBO.newShiftResourceScheduler(args, {
              SystemID: 1, ProductionAreaID: 2,
              HumanResourceID: null, ResourceID: null, HumanResource: "",
              ShiftTypeID: 19, ShiftType: "Studio Supervisor Shift",
              DisciplineID: 7, Discipline: "Event Facilitator"
            });
          }
        });
      };
    };
  };

  groupContextMenu(timelineGroup) {

  };

  updateResourceGroups() {
    if (this) {
      requestAnimationFrame(this.updateResourceGroups)
      const allGroups = this.allItems.filter(item => { return (item.type == 'resource-group') })
      allGroups.forEach((group, groupIndex) => {
        if (this.groupTarget === group) {
          group.expanded = !group.expanded
        }
        let prevIndex = (groupIndex - 1)
        let previousItem = allGroups[prevIndex]
        let calculatedHeight = ((group.itemHeight * group.subItems.length) + group.itemHeight)
        if (group.expanded) {
          group.element.style.height = calculatedHeight.toString() + 'px'
          group.subGroupsElement.style.height = (group.itemHeight * group.subItems.length).toString() + 'px'
        }
        else {
          group.element.style.height = group.itemHeight.toString() + 'px'
          group.subGroupsElement.style.height = '0px'
        }
      })
    }
  };

  subGroupClicked() {
    if (this.subgroupTarget.selected) {
      //remove the items from the timeline
      this.removeSubGroupFromTimeline(this.subgroupTarget)
    }
    else {
      //add the items to the timeline
      this.addSubGroupToTimeline(this.subgroupTarget)
    }
  };

  getResourceBookings(options) {
    let bookingsRequest = Singular.GetDataStatelessPromise("OBLib.Resources.ResourceBookingList", options.criteria)
    bookingsRequest.then((data) => {
      if (options.onSuccess) { options.onSuccess.call(this, data) }
    }),
      ((errorText) => {
        OBMisc.Notifications.GritterError("Error Fetching Bookings", errorText, 1000)
      })
  };

  async editScheduler() {

    //show busy
    this.dom.editSchedulerButton.className = "fa fa-refresh fa-spin"
    this.dom.editSchedulerButton.innerHTML = ``;

    //setup
    try {
      const data = await ResourceSchedulerBO.getPromise({
        criteria: {
          ResourceSchedulerID: this.schedulerData.ResourceSchedulerID,
          FetchGroups: true,
          FetchSubGroups: true,
          FetchSubGroupResources: true
        }
      });
      ViewModel.ResourceScheduler.Set(data[0]);
      Singular.Validation.CheckRules(ViewModel.ResourceScheduler());
      addEditModeEvents.call(this);

      $(this.defaultOptions.editSchedulerModal).off("shown.bs.modal");
      $(this.defaultOptions.editSchedulerModal).on("shown.bs.modal", () => {
        OBMisc.UI.activateTab('GeneralSettings');
        this.dom.editSchedulerButton.className = "fa fa-edit";
        this.dom.editSchedulerButton.innerHTML = ``;
      });
      $(this.defaultOptions.editSchedulerModal).modal();

    }
    catch (err) {
      OBMisc.Notifications.GritterError("Fetch scheduler failed", err.message, 1000);
      this.dom.editSchedulerButton.className = "fa fa-edit";
      this.dom.editSchedulerButton.innerHTML = ``;
    };

    function addEditModeEvents() {
      if (this.isAdministrator) {
        //group sorting
        $(this.defaultOptions.editSchedulerModal + " ul.scheduler-groups").sortable({
          delay: 100,
          placeholder: "ui-state-highlight",
          update: function (event, ui) {
            afterItemSorted() //.call(this)
          }
        })
        //sub group sorting
        $(this.defaultOptions.editSchedulerModal + " ul.scheduler-subgroups").sortable({
          delay: 100,
          placeholder: "ui-state-highlight",
          update: function (event, ui) {
            afterItemSorted() //.call(this)
          }
        })
        //resource sorting
        $(this.defaultOptions.editSchedulerModal + " ul.scheduler-subgroupresources").sortable({
          delay: 100,
          placeholder: "ui-state-highlight",
          update: function (event, ui) {
            afterItemSorted() //.call(this)
          }
        })
      }
    }

    function afterItemSorted() {
      //groups--------------------------------------------------------------
      let groups = $(this.defaultOptions.editSchedulerModal + " ul.scheduler-groups > li")
      groups.each(function (grpIndx) {
        ko.dataFor(this).GroupOrder(grpIndx + 1)
        //sub groups--------------------------------------------------------
        let subGroups = $(this).find('ul.scheduler-subgroups > li')
        subGroups.each(function (subGroupIndx) {
          ko.dataFor(this).SubGroupOrder(subGroupIndx + 1)
          //sub group resources---------------------------------------------
          let subGroupResources = $(this).find('ul.scheduler-subgroupresources > li')
          subGroupResources.each(function (subGroupResourceIndx) {
            let resourceGlobalIndex = $(this).index('li.orderable-subgroupresource')
            ko.dataFor(this).ResourceOrder(resourceGlobalIndex + 1)
          })
        })
        OBMisc.Notifications.GritterInfo("Ordering Updated", "Save your changes", 250)
      })
    }

  };

  processSignalRChanges(stringifiedChanges) {
    //this will parse the data into a json object and pass it on to be processed
    let parsedChanges = JSON.parse(stringifiedChanges)
    let updatedBookings = parsedChanges.Data.UpdatedBookings
    let addedBookings = parsedChanges.Data.AddedBookings
    let removedBookingIDs = parsedChanges.Data.RemovedBookingIDs
    //Process the updates
    updatedBookings.Iterate((updBooking, indx) => {
      this.updateTimelineItem(updBooking)
    })
    //Process the additions
    addedBookings.Iterate((addedBooking, indx) => {
      let timelineGroup = this.getTimelineGroup(addedBooking.DisplayResourceID)
      let item = this.createTimelineItem(timelineGroup, addedBooking);
      this.timelineBookings.add(item);
    })
    //Process the deletes
    removedBookingIDs.Iterate((resourceBookingID, indx) => {
      let timelineItem = this.getTimelineItem(resourceBookingID);
      if (timelineItem) {
        this.timelineBookings.remove(timelineItem.id);
      };
    })
  };

  updateTimelineItem(bookingData) {
    //get the group
    let timelineGroup = this.getTimelineGroup(bookingData.DisplayResourceID)

    //get the item (if it already exists)
    let oldTimelineItem = this.timelineBookings.get({
      filter: function (item) {
        return (item.businessObject.ResourceBookingID == bookingData.ResourceBookingID)
      }
    })
    if (oldTimelineItem.length > 0) { oldTimelineItem = oldTimelineItem[0] } else { oldTimelineItem = null }

    //create a new version of the timeline item
    let newTimelineItem = this.createTimelineItem(timelineGroup, bookingData)

    if (oldTimelineItem) {
      //already have this booking on screen, update it
      oldTimelineItem.content = newTimelineItem.content
      oldTimelineItem.start = newTimelineItem.start
      oldTimelineItem.end = newTimelineItem.end
      oldTimelineItem.group = newTimelineItem.group
      oldTimelineItem.className = newTimelineItem.className
      oldTimelineItem.businessObject = newTimelineItem.businessObject
      oldTimelineItem.editable = {
        remove: false,
        updateGroup: false,
        updateTime: false
      }
      this.timelineBookings.update(oldTimelineItem)
    }
    else {
      //don't have this booking on screen, add it
      if (newTimelineItem) {
        //me.resourceBookings.add(newItem);
      }
    }
  };

  getTimelineGroup(resourceID) {
    let results = []
    results = this.timelineResources.get({
      filter: function (item) {
        return (item.resource.ResourceID == resourceID)
      }
    })
    if (results.length == 1) { return results[0] } else { return null }
  };

  getTimelineItem(resourceBookingID) {
    let results = []
    results = this.timelineBookings.get({
      filter: (item) => {
        return (item.businessObject.ResourceBookingID == resourceBookingID)
      }
    });
    return (results.length == 1 ? results[0] : null);
  };

  onWindowResized() {

    let pageWidth = document.body.clientWidth;
    let expectedHeight = (document.body.clientHeight - this.dimensions.reservedHeight);
    let sidebarDimensions = this.dom.sidebar.getBoundingClientRect();

    this.dom.container.style.height = expectedHeight;
    this.dom.container.style.width = pageWidth;

    //height
    this.dom.resourcesColumn.style.height = expectedHeight.toString() + 'px';
    this.dom.resourceBookingsColumn.style.height = expectedHeight.toString() + 'px';
    this.timelineOptions.height = expectedHeight.toString() + 'px';
    this.timelineOptions.maxHeight = this.timelineOptions.height;

    //width - oh the fun
    if (pageWidth < 1100) {
      if (pageWidth > 760) {
        this.dimensions.reservedWidth = (sidebarDimensions.width + 40);
      } else {
        this.dimensions.reservedWidth = 0;
      };
      //resources
      this.dom.resourcesColumn.style.display = 'block';
      this.dom.resourcesColumn.style.width = '100%';
      this.dom.resourcesColumn.style.height = '30px';
      this.dom.resourcesColumn.style.overflowY = 'hidden';
      //bookings
      let resourceBookingsWidth = (this.dimensions.reservedWidth + this.dimensions.resourcebookingsColumnBorderWidth);
      this.dom.resourceBookingsColumn.style.width = (pageWidth - resourceBookingsWidth).toString() + 'px';
      this.dom.resourceBookingsColumn.style.display = 'block';
      this.dom.resourceBookingsColumn.style.width = '100%';
      this.timelineOptions.height = (expectedHeight - 30).toString() + 'px';
      this.timelineOptions.maxHeight = this.timelineOptions.height;
    }
    else if (pageWidth >= 1100) {
      this.dimensions.reservedWidth = (sidebarDimensions.width + 40);
      //resources
      this.dom.resourcesColumn.style.width = this.dimensions.resourcesColumnInitialWidth.toString() + 'px';
      this.dom.resourcesColumn.style.display = 'inline-block';
      this.dom.resourcesColumn.style.overflowY = 'auto';
      //bookings
      let resourceBookingsWidth = (this.dimensions.reservedWidth + this.dimensions.resourcesColumnInitialWidth + this.dimensions.resourcesColumnBorderWidth + this.dimensions.resourcebookingsColumnBorderWidth);
      this.dom.resourceBookingsColumn.style.width = (pageWidth - resourceBookingsWidth).toString() + 'px';
      this.dom.resourceBookingsColumn.style.display = 'inline-block';
    };

    if (pageWidth < 1100) {
      this.dom.selectResourcesButton.style.visibility = '';
    } else {
      this.dom.selectResourcesButton.style.visibility = 'hidden';
    };

  };

  isItemSelected(id) {
    const selections = this.timeline.itemSet.getSelectedItems();
    return (selections.indexOf(id) >= 0);
  };

  ensureSelected(id, actualItem) {
    //1. make the current item is also selected
    if (!this.isItemSelected(id)) {
      const selections = this.timeline.itemSet.getSelectedItems();
      selections.push(id);
      this.timeline.itemSet.setSelection(selections);
      actualItem.select();
    };
  };

  getSelectedItems() {
    const selectedIDs = this.timeline.itemSet.getSelectedItems();
    return this.timelineBookings.get(selectedIDs);
  };

  getSelectedBookings() {
    return this.getSelectedItems().map(item => {
      return item.businessObject
    });
  };

  changeBookingStatuses(status) {
    if (!this.readOnlyMode) {
      const selectedItems = this.getSelectedItems();
      //2. set the selected items as busy
      selectedItems.forEach(item => {
        this.setItemBusy(item);
      });
      //3. send the requests
      window.siteHubManager.changeBookingStatuses(selectedItems, status.ProductionAreaStatusID);
    };
  };

  async deleteRoomScheduleAreas(args, permanentDelete) {
    if (!this.readOnlyMode) {
      let itemsToDelete = []
      try {

        //variables
        let [productionSystemAreaIDs, rb, item] = [[], null, null];
        const items = this.getSelectedItems();

        //loop
        items.forEach((item) => {
          this.setItemBusy(item);
          productionSystemAreaIDs.push(item.businessObject.ProductionSystemAreaID);
          itemsToDelete.push(item);
        });
        //promise
        let result = await ViewModel.CallServerMethodPromise("DeleteRoomScheduleAreas", { ProductionSystemAreaIDs: productionSystemAreaIDs, PermanentDelete: permanentDelete });
        OBMisc.Notifications.GritterSuccess("Delete Succeeded", "", 250);
      }
      catch (ex) {
        itemsToDelete.forEach((itm) => {
          this.setItemNotBusy(itm)
        })
        OBMisc.Notifications.GritterError("An Error Occured :(", ex.message, 1000)
      };
    };
  };

  async copyCrew(args) {
    if (!this.readOnlyMode) {
      const fromItem = me.getResourceBookingItem(args.data.selections[0]);
      const toItem = me.getResourceBookingItem(args.data.selections[1]);
      const fromRB = fromItem.Booking;
      const toRB = toItem.Booking;
      this.setItemBusy(fromItem);
      this.setItemBusy(toItem);
      try {
        let result = await ViewModel.CallServerMethodPromise("CopyCrewResourceScheduler", { FromBooking: fromRB, ToBooking: toRB });
        OBMisc.Notifications.GritterSuccess("Copy Succeeded", "", 500)
        this.setItemNotBusy(fromItem)
        this.setItemNotBusy(toItem)
        if (result.ErrorText.length == 0) {
          OBMisc.Notifications.GritterSuccess("Copy Succeeded", "", 500);
        }
        else {
          OBMisc.Notifications.GritterWarning("Partially Copied", response.ErrorText, 1000)
        }
      }
      catch (error) {
        this.setItemNotBusy(fromItem)
        this.setItemNotBusy(toItem)
      };
    };
  };

  assignBookings() {
    //show busy
    this.dom.assignBookingsButton.className = "fa fa-refresh fa-spin";
    this.dom.assignBookingsButton.innerHTML = ``;

    //setup the assigner
    if (!window.currentBookingAssigner) {
      let sd = moment(window.scheduler.timeline.range.start).startOf('month'),
        ed = moment(window.scheduler.timeline.range.start).endOf('month');
      window.currentBookingAssigner = new bookingAssigner({ containerID: "page-content", startDate: sd, endDate: ed });
      window.currentBookingAssigner.show();
      window.currentBookingAssigner.refresh();
    }
    else {
      window.currentBookingAssigner.show();
      window.currentBookingAssigner.refresh();
    };

    //hide busy
    this.dom.assignBookingsButton.className = "fa fa-american-sign-language-interpreting";
    this.dom.assignBookingsButton.innerHTML = ``;
  };

  managePatterns() {
    //show busy
    this.dom.managePatternsButton.className = "fa fa-refresh fa-spin";
    this.dom.managePatternsButton.innerHTML = ``;

    ViewModel.CurrentSAShiftPattern(null);

    if (ViewModel.CurrentSAShiftPatternList() == null) {
      ViewModel.CurrentSAShiftPatternList([]);
    };
    if (ViewModel.CurrentSAShiftPatternCriteria() == null) {
      ViewModel.CurrentSAShiftPatternCriteria(new ROSAShiftPatternList_CriteriaObject());
      ViewModel.CurrentSAShiftPatternCriteria().SystemID(ClientData.ROUserSystemList[0].SystemID);
      let areas = ClientData.ROUserSystemAreaList.filter(item => { return (item.SystemID == ClientData.ROUserSystemList[0].SystemID) });
      if (areas.length > 0) {
        ViewModel.CurrentSAShiftPatternCriteria().ProductionAreaID(areas[0].ProductionAreaID);
      };
      ViewModel.CurrentSAShiftPatternCriteria().StartDate(this.schedulerData.CurrentStartDate);
      ViewModel.CurrentSAShiftPatternCriteria().EndDate(this.schedulerData.CurrentEndDate);
    };

    $("#SAShiftPatternModal").off('shown.bs.modal');
    $("#SAShiftPatternModal").off('hidden.bs.modal');

    $("#SAShiftPatternModal").on('shown.bs.modal', () => {
      Singular.Validation.CheckRules(ViewModel.CurrentSAShiftPatternCriteria());
      SAShiftPatternCriteriaBO.refresh(ViewModel.CurrentSAShiftPatternCriteria());
    });

    $("#SAShiftPatternModal").on('hidden.bs.modal', () => {
      ViewModel.CurrentSAShiftPattern(null);
      ViewModel.CurrentSAShiftPatternList(null);
      ViewModel.CurrentSAShiftPatternCriteria(null);
      Singular.Validation.CheckRules(ViewModel);
    });

    $("#SAShiftPatternModal").modal();

    //hide busy
    this.dom.managePatternsButton.className = "fa fa-paypal";
    this.dom.managePatternsButton.innerHTML = ``;
  };

  selectResources() {
    if (this.dom.resourcesColumn.classList.contains('selecting')) {
      this.dom.resourcesColumn.style.height = '30px';
      this.dom.resourcesColumn.style.overflowY = 'hidden';
      this.dom.resourcesColumn.classList.remove('selecting');
    } else { 
      this.dom.resourcesColumn.style.height = '800px';
      this.dom.resourcesColumn.style.overflowY = 'auto';
      this.dom.resourcesColumn.classList.add('selecting');
    }
  };

};