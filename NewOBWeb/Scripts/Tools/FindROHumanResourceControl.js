﻿FindROHumanResourceControl = function (options) {
  var me = this;

  me.options = {};
  var defaultOptions = {
    modalID: "FindROHumanResourceModal",
    pagingManager: function () {
      if (ViewModel) { return ViewModel.ROHumanResourceListManager() }
    },
    criteria: function () {
      if (ViewModel) { return ViewModel.ROHumanResourceListCriteria() }
    },
    list: function () {
      if (ViewModel) { return ViewModel.ROHumanResourceList() }
    },
    delay: 200,
    onRowSelected: null,
    SingleSelect: true,
    MultiSelect: false
  };
  me.options = $.extend({}, defaultOptions, options);

  if (me.options.onRowSelected) {
    me.onRowSelected = me.options.onRowSelected;
  }

  me.fetchTimeout = null;
  me.modalSelector = "#" + me.options.modalID;

  me.setup = function () {

    $(me.modalSelector).off('shown.bs.modal');
    $(me.modalSelector).off('hidden.bs.modal');

    $(me.modalSelector).on('shown.bs.modal', function () {
      $(me.modalSelector).draggable({
        handle: ".modal-header"
      });
      me.options.pagingManager().Refresh();
      me.afterModalOpened()
    });

    $(me.modalSelector).on('hidden.bs.modal', function () {
      me.afterModalClosed()
    });

  };

  me.setup();

  return me;

};

FindROHumanResourceControl.prototype.show = function () {
  var me = this;

  $(me.modalSelector).modal();
};

FindROHumanResourceControl.prototype.hide = function () {
  var me = this;
  $(me.modalSelector).modal('hide');
};

FindROHumanResourceControl.prototype.refresh = function () {
  var me = this;
  me.options.pagingManager().Refresh();
};

FindROHumanResourceControl.prototype.delayedRefresh = function () {
  var me = this;
  clearTimeout(me.fetchTimeout);
  me.fetchTimeout = setTimeout(function () {
    me.options.pagingManager().Refresh();
  }, me.options.delay);
};

FindROHumanResourceControl.prototype.onRowSelected = function (ROHumanResource) {
  var me = this;
  console.log(ROHumanResource);
};

FindROHumanResourceControl.prototype.AddSystem = function (obj) {
    obj.IsSelected(!obj.IsSelected())
};

FindROHumanResourceControl.prototype.AddProductionArea = function (obj) {
    obj.IsSelected(!obj.IsSelected())
};

FindROHumanResourceControl.prototype.afterModalOpened = function () {
 
};

FindROHumanResourceControl.prototype.afterModalClosed = function () {

};