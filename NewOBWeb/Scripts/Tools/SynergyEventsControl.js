﻿SynergyEventsControl = function (options) {
  var me = this;

  var defaultOptions = {
    criteriaName: "ROSynergyEventListCriteria",
    listName: "ROSynergyEventList",
    selectedItemListName: "SelectedROSynergyEventList",
    isProcessingName: "IsProcessing"
  };
  defaultOptions = $.extend({}, defaultOptions, options);

  me.criteria = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.criteriaName]();
    }
    return null;
  };
  me.list = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.listName]();
    }
    return null;
  };
  me.listKO = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.listName];
    }
    return null;
  };
  me.isProcessing = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.isProcessingName]();
    }
    return null;
  };
  me.isProcessingKO = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.isProcessingName];
    }
    return null;
  };
  me.setProcessing = function (newValue) {
    ViewModel[defaultOptions.isProcessingName](newValue);
  };

  me.selectedItems = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.selectedItemListName]();
    }
    return null;
  };
  me.selectedItemsKO = function () {
    if (ViewModel) {
      return ViewModel[defaultOptions.selectedItemListName];
    }
    return null;
  };

  me.timeout = null;
  me.timeoutDelay = 150;
};

SynergyEventsControl.prototype.Back = function () {
  window.Scheduler.hideSynergyMenu()
};

SynergyEventsControl.prototype.ShowFilters = function () {
  ViewModel.SynergyEventsVisible(false);
  ViewModel.SynergyFiltersVisible(true);
};

SynergyEventsControl.prototype.HideFilters = function () {
  ViewModel.SynergyEventsVisible(true);
  ViewModel.SynergyFiltersVisible(false);
};

SynergyEventsControl.prototype.DelayedRefresh = function () {
  var me = this;
  clearTimeout(me.timeout);
  SynergyEventsControlTimeOut = setTimeout(function () {
    me.GetSynergyEvents()
  },
  me.timeoutDelay);
};

SynergyEventsControl.prototype.Refresh = function () {
  var me = this;
  me.GetSynergyEvents();
  me.HideFilters();
};

SynergyEventsControl.prototype.GetSynergyEvents = function (options) {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  if (!me.isProcessing()) {
    me.setProcessing(true);
    ViewModel.CallServerMethod("GetROSynergyEventList", {
      Keyword: crit.Keyword(),
      StartDate: crit.StartDate(),
      EndDate: crit.EndDate(),
      IncludeUnknownDates: crit.IncludeUnknownDates(),
      Live: crit.Live(),
      Delayed: crit.Delayed(),
      Premier: crit.Premier(),
      HighlightsInd: crit.HighlightsInd(),
      SAInd: crit.SAInd(),
      ImportedEventID: crit.ImportedEventID(),
      GenRefNumber: crit.GenRefNumber(),
      ViewBy: crit.ViewBy(),
      PageNo: crit.PageNo(),
      PageSize: crit.PageSize(),
      SortAsc: crit.SortAsc(),
      SortColumn: 'StartDateTime'
    }, function (response) {
      if (response.Success) {
        ViewModel.LastImportDateDescription(response.Data.LastImportDescription);
        ViewModel.TotalSynergyRecords(response.Data.TotalRecords);
        ViewModel.TotalSynergyPages(response.Data.TotalPages);
        me.listKO().Set(response.Data.List);
        me.setProcessing(false);
        if (options && options.afterFetch) {
          options.afterFetch(me, response);
        };
      } else {

      }
    });
  }
};

SynergyEventsControl.prototype.FirstPage = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  crit.PageNo(1);
  me.Refresh();
};

SynergyEventsControl.prototype.PreviousPage = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  var newPageNum = crit.PageNo() - 1;
  if (newPageNum <= 0) {
    crit.PageNo(ViewModel.TotalSynergyPages());
  } else {
    crit.PageNo(newPageNum);
  }
  me.Refresh();
};

SynergyEventsControl.prototype.NextPage = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  var newPageNum = crit.PageNo() + 1;
  if (newPageNum > ViewModel.TotalSynergyPages()) {
    crit.PageNo(1);
  } else {
    crit.PageNo(newPageNum);
  }
  me.Refresh();
};

SynergyEventsControl.prototype.LastPage = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  crit.PageNo(ViewModel.TotalSynergyPages());
  me.Refresh();
};

SynergyEventsControl.prototype.PageDescription = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  return "Page " + crit.PageNo().toString() + " of " + ViewModel.TotalSynergyPages().toString();
};

SynergyEventsControl.prototype.SelectionCount = function () {
  var me = this;
  return  me.selectedItems().length.toString() + " items selected";
};

SynergyEventsControl.prototype.scheduledTimeHTML = function (ROSynergyEvent) {
  var sd = moment(new Date(ROSynergyEvent.ScheduleDateTime())),
      ed = moment(new Date(ROSynergyEvent.ScheduleEndDate()));
  if (ed.isAfter(sd, 'day')) {
    return '<label>' + ROSynergyEvent.ScheduleDateString() + ' ' + ROSynergyEvent.ScheduleStartString() + '</label> - ' +
           '<label class="red">' + ROSynergyEvent.ScheduleEndString() + '</label>';
  } else {
    return '<label>' + ROSynergyEvent.ScheduleDateString() + ' ' + ROSynergyEvent.ScheduleStartString() + '</label> - ' +
           '<label>' + ROSynergyEvent.ScheduleEndString() + '</label>';
  };
};

SynergyEventsControl.prototype.itemCss = function (ROSynergyEvent) {
  var me = this;
  var item = me.selectedItems().Find("ImportedEventChannelID", ROSynergyEvent.ImportedEventChannelID());
  if (item) {
    return 'synergy-event-row draggable-item selected';
  } else {
    return 'synergy-event-row draggable-item';
  }
};

SynergyEventsControl.prototype.itemClicked = function (ROSynergyEvent, element) {
  var me = this;

  var item = me.selectedItems().Find("ImportedEventChannelID", ROSynergyEvent.ImportedEventChannelID());
  if (item) {
    element.classList.remove('selected');
    me.selectedItems().pop(item);
  } else {
    var x = new SynergyEventChannelObject();
    x.GenRefNumber(ROSynergyEvent.GenRefNumber());
    x.ImportedEventChannelID(ROSynergyEvent.ImportedEventChannelID());
    x.StartDateTime(ROSynergyEvent.ScheduleDateTime());
    x.EndDateTime(ROSynergyEvent.ScheduleEndDate());
    x.ResourceTypeID(null);
    x.ResourceID(null);
    x.ResourceName("");
    x.RoomID(null);
    x.SystemID(ViewModel.SystemID());
    x.ProductionAreaID(ViewModel.DefaultProductionAreaID());
    ViewModel.SelectedROSynergyEventList.push(x);
    element.classList.add('selected');
  }
};

SynergyEventsControl.prototype.ClearSelections = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  me.selectedItemsKO().Set([]);
};

SynergyEventsControl.prototype.DoNewImport = function () {
  var me = this;
  crit = me.criteria();
  lst = me.list();
  var sd = moment(new Date(crit.StartDate())).add(31, 'days')._d;
  var ed = moment(new Date(crit.EndDate())).add(-7, 'days')._d;
  if (!me.isProcessing) {
    me.setProcessing(true);
    ViewModel.CallServerMethod("DoNewSynergyImport", {
      StartDate: sd.format('dd MMM yyyy'),
      EndDate: ed.format('dd MMM yyyy')
    }, function (response) {
      if (response.Success) {
        me.setProcessing(false);
        me.Refresh();
      } else {
        //show error
      }
    });
  }
};