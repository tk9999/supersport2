﻿liveEvents = function (containerID, options)
{
  var me = this;

  //properties
  me.dom = {}
  me.data = []
  me.cards = []
  me.criteria = {
    startDate: new Date(),
    endDate: new Date(),
    SystemIDs: [],
    ProductionAreaIDs: [],
    roomID: null
  }
  //                    min x sec x milliseconds
  me.timeOutInterval = (15 * 60 * 1000)
  me.refreshTimeout = null
  me.lastRefreshed = null
  me.isBusy = false
  me.hoverTimeout = null

  //set container
  me.dom.container = document.getElementById(containerID)

  //setup loading div
  me.dom.loadingDiv = document.createElement("div")
  me.dom.loadingDiv.className = "loader"
  me.dom.loadingDiv.innerHTML = "Loading..."

  //create card container
  me.dom.cardContainer = document.createElement('section')
  me.dom.cardContainer.className = "card-container row"
  me.dom.container.appendChild(me.dom.cardContainer)

  //append
  me.dom.container.appendChild(me.dom.loadingDiv)

  //start
  me.refresh()

  return me;
};

liveEvents.prototype.init = function ()
{
  var me = this;

};

liveEvents.prototype.addSystem = function (userSystem)
{
  var me = this;
  userSystem.IsSelected(!userSystem.IsSelected())
  me.updateSystemAreaCriteria()
  me.refresh()
};

liveEvents.prototype.addProductionArea = function (userSystemArea)
{
  var me = this;
  userSystemArea.IsSelected(!userSystemArea.IsSelected())
  me.updateSystemAreaCriteria()
  me.refresh()
};

liveEvents.prototype.updateSystemAreaCriteria = function ()
{
  var me = this;
  me.criteria.SystemIDs = []
  me.criteria.ProductionAreaIDs = []
  ViewModel.UserSystemList().Iterate(function (userSystem, userSystemIndx)
  {
    if (userSystem.IsSelected())
    {
      me.criteria.SystemIDs.push(userSystem.SystemID())
      userSystem.UserSystemAreaList().Iterate(function (userSystemArea, userSystemAreaIndx)
      {
        if (userSystemArea.IsSelected())
        {
          me.criteria.ProductionAreaIDs.push(userSystemArea.ProductionAreaID())
        }
      })
    }
  })
};

liveEvents.prototype.refresh = function ()
{
  var me = this;
  if (me.isBusy) { return }
  else {
    me.isBusy = true
    me.dom.cardContainer.style.display = 'none'
    me.dom.loadingDiv.style.display = 'block'
    LiveEventHRCardBO.getList({
      criteria: {
        StartDate: me.criteria.startDate.format('dd-MMM-yyyy'),
        EndDate: me.criteria.endDate.format('dd-MMM-yyyy'),
        SystemIDs: me.criteria.SystemIDs,
        ProductionAreaIDs: me.criteria.ProductionAreaIDs,
        RoomID: me.criteria.roomID,
        EventBasedInd: me.criteria.EventBasedInd,
        ShiftBasedInd: me.criteria.ShiftBasedInd,
        LeaveInd: me.criteria.LeaveInd
      },
      onSuccess: function (response)
      {
        me.data = []
        me.cards = []
        while (me.dom.cardContainer.hasChildNodes())
        {
          me.dom.cardContainer.removeChild(me.dom.cardContainer.lastChild);
        }
        me.data = response.Data
        me.createCards()
        me.isBusy = false
        me.lastRefreshed = new Date()
        ViewModel.LiveEventHRCardListCriteria().LastRefreshTime(me.lastRefreshed.format('HH:mm:ss'))
        me.startTimer()
      }
    })
  }
};

liveEvents.prototype.createCards = function ()
{
  var me = this;
  me.data.Iterate(function (cardData, cardDataIndex)
  {
    me.createCard.call(me, cardData)
  })
  me.dom.loadingDiv.style.display = 'none'
  me.dom.cardContainer.style.display = 'block'
  me.dom.cardContainer.className = "card-container row"
};

liveEvents.prototype.startTimer = function ()
{
  var me = this;

  if (me.refreshTimeout) clearTimeout(me.refreshTimeout)
  me.refreshTimeout = window.setTimeout(function ()
  {
    me.refresh()
  }, me.timeOutInterval)

};

liveEvents.prototype.createCard = function (cardData)
{
  var me = this;
  var newCard = {}
  newCard.data = cardData

  newCard.dom = document.createElement('div')
  newCard.dom.className = 'col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-2'

  newCard.dom.block = document.createElement('div')
  newCard.dom.block.className = 'live-event-card'

  if (newCard.data.IsLeave) 
    newCard.dom.block.className = newCard.dom.block.className + " block-flat-leave"
  else
    newCard.dom.block.className = newCard.dom.block.className + ' block-flat'

  newCard.dom.face = document.createElement('div')
  newCard.dom.face.className = 'face'
  newCard.dom.faceImage = document.createElement('img')
  newCard.dom.faceImage.setAttribute('alt', 'user-avatar')
  newCard.dom.faceImage.setAttribute('src', window.SiteInfo.SitePath + '/' + newCard.data.PictureName)
  newCard.dom.faceImage.className = 'face-image'
  newCard.dom.face.appendChild(newCard.dom.faceImage)

  newCard.dom.header = document.createElement('h4')
  newCard.dom.header.innerHTML = newCard.data.HRName

  newCard.dom.eventNameLabel = document.createElement('strong')
  if (newCard.data.IsShift)
  {
    newCard.dom.eventNameLabel.innerHTML = 'Shift Type'
  } else if (newCard.data.IsLeave)
  {
    newCard.dom.eventNameLabel.innerHTML = 'Description'  
  } else
  {
    newCard.dom.eventNameLabel.innerHTML = 'Event Name'
  }

  newCard.dom.eventName = document.createElement('p')
  newCard.dom.eventName.innerHTML = newCard.data.Title

  newCard.dom.eventLocationLabel = document.createElement('strong')
  newCard.dom.eventLocationLabel.innerHTML = 'Location'
  newCard.dom.eventLocation = document.createElement('p')
  if (newCard.data.IsShift)
  {
    newCard.dom.eventLocation.innerHTML = newCard.data.SiteName
  } else
  {
    newCard.dom.eventLocation.innerHTML = newCard.data.Room + ' (' + newCard.data.SiteName + ')'
  }

  newCard.dom.eventCallTimeLabel = document.createElement('strong')
  newCard.dom.eventCallTimeLabel.innerHTML = 'Call Time'
  newCard.dom.eventCallTime = document.createElement('p')
  newCard.dom.eventCallTime.innerHTML = new Date(newCard.data.CallTime).format('HH:mm')

  newCard.dom.eventHRContactNumbersLabel = document.createElement('strong')
  newCard.dom.eventHRContactNumbersLabel.innerHTML = 'Contact Details'
  newCard.dom.eventHRContactNumbers = document.createElement('p')
  var hasPrimaryContact = (newCard.data.CellPhoneNumber.trim().length > 0 ? true : false)
  var hasAlternateContact = (newCard.data.AlternativeContactNumber.trim().length > 0 ? true : false)
  if (hasPrimaryContact || hasAlternateContact)
  {
    newCard.dom.eventHRContactNumbers.innerHTML = newCard.data.CellPhoneNumber.toString() + (hasAlternateContact ? ' / ' + newCard.data.AlternativeContactNumber.toString() : '')
  } else
  {
    newCard.dom.eventHRContactNumbers.innerHTML = '<i class="fa fa-frown-o"></i> None Found'
  }

  newCard.dom.productionAssistantLabel = document.createElement('strong')
  newCard.dom.productionAssistantLabel.innerHTML = 'PA'
  newCard.dom.productionAssistant = document.createElement('p')
  newCard.dom.productionAssistant.innerHTML = newCard.data.ProductionAssistant

  newCard.dom.producerLabel = document.createElement('strong')
  newCard.dom.producerLabel.innerHTML = 'Producer'
  newCard.dom.producer = document.createElement('p')
  newCard.dom.producer.innerHTML = newCard.data.Producer

  newCard.dom.hasArrivedLabel = document.createElement('span')
  if (!newCard.data.CallTimePassed)
  {
    newCard.dom.hasArrivedLabel.className = "label label-default"
    newCard.dom.hasArrivedLabel.innerHTML = "<i class='fa fa-clock-o'></i> <span> Awaiting Arrival</span>"
  }
  else if (newCard.data.HasArrived)
  {
    newCard.dom.hasArrivedLabel.className = "label label-success"
    newCard.dom.hasArrivedLabel.innerHTML = "<i class='fa fa-check-square-o'></i> <span> Arrived: " + new Date(newCard.data.ArrivedDateTime).format('HH:mm') + "</span>"
  } else
  {
    newCard.dom.hasArrivedLabel.className = "label label-danger flash infinite slowest"
    newCard.dom.hasArrivedLabel.innerHTML = "<i class='fa fa-frown-o'></i> <span>Has Not Arrived</span>"
  }

  newCard.dom.IsLeaveLabel = document.createElement('p')
  if (newCard.data.IsLeave) {    
    newCard.dom.IsLeaveLabel.innerHTML = "<span class='label label-default flash infinite slowest'><i class='fa fa-frown-o'></i> <span>On Leave</span></span>"
  }

  newCard.dom.block.appendChild(newCard.dom.face)
  newCard.dom.block.appendChild(newCard.dom.header)
  newCard.dom.block.appendChild(newCard.dom.eventHRContactNumbersLabel)
  newCard.dom.block.appendChild(newCard.dom.eventHRContactNumbers)
  newCard.dom.block.appendChild(newCard.dom.eventNameLabel)
  newCard.dom.block.appendChild(newCard.dom.eventName)

  if (!newCard.data.IsLeave) {
    
    newCard.dom.block.appendChild(newCard.dom.eventLocationLabel)
    newCard.dom.block.appendChild(newCard.dom.eventLocation)
    newCard.dom.block.appendChild(newCard.dom.eventCallTimeLabel)
    newCard.dom.block.appendChild(newCard.dom.eventCallTime)
    newCard.dom.block.appendChild(newCard.dom.producerLabel)
    newCard.dom.block.appendChild(newCard.dom.producer)
    newCard.dom.block.appendChild(newCard.dom.productionAssistantLabel)
    newCard.dom.block.appendChild(newCard.dom.productionAssistant)
    newCard.dom.block.appendChild(newCard.dom.hasArrivedLabel)    
  } else {
    newCard.dom.leaveStartDateLabel = document.createElement('strong')
    newCard.dom.leaveStartDateLabel.innerHTML = 'Leave Start'
    newCard.dom.leaveStartDate = document.createElement('p')
    newCard.dom.leaveStartDate.innerHTML = new Date(newCard.data.StartDateTime).format('yyyy-MM-dd HH:mm')

    newCard.dom.leaveEndDateLabel = document.createElement('strong')
    newCard.dom.leaveEndDateLabel.innerHTML = 'Leave End'
    newCard.dom.leaveEndDate = document.createElement('p')
    newCard.dom.leaveEndDate.innerHTML = new Date(newCard.data.EndDateTime).format('yyyy-MM-dd HH:mm')

    newCard.dom.block.appendChild(newCard.dom.leaveStartDateLabel)
    newCard.dom.block.appendChild(newCard.dom.leaveStartDate)
    newCard.dom.block.appendChild(newCard.dom.leaveEndDateLabel)
    newCard.dom.block.appendChild(newCard.dom.leaveEndDate)
    newCard.dom.block.appendChild(newCard.dom.IsLeaveLabel)
  }
  
  //newCard.dom.appendChild(newCard.dom.disciplinesTable)

  newCard.dom.appendChild(newCard.dom.block)
  me.dom.cardContainer.appendChild(newCard.dom)
  me.cards.push(newCard)

  //<table class="no-border no-strip skills">
  //     <tbody class="no-border-x no-border-y">
  //          <tr><td style="width:20%;"><b>Website</b></td><td>www.website.com</td></tr>
  //          <tr><td style="width:20%;"><b>E-mail</b></td><td>johnny@example.com</td></tr>
  //          <tr><td style="width:20%;"><b>Mobile</b></td><td>(999) 999-9999</td></tr>
  //          <tr><td style="width:20%;"><b>Location</b></td><td>Montreal, Canada</td></tr>
  //     </tbody>
  //</table>

};