﻿FindROCityModal = function (options) {
  var me = this;

  me.options = {};
  var defaultOptions = {
    modalID: "ROCityListModal",
    pagingManagerName: '',
    listName: '',
    criteriaName: '',
    delay: 200,
    onCitySelected: null,
    onCityUnSelected: null,
    beforeShow: null,
    searchByCountryEnabled: false
  };
  me.options = $.extend({}, defaultOptions, options);

  me.fetchTimeout = null;
  me.modalSelector = "#" + me.options.modalID;
  me.searchByCountryEnabled = function () {
    return (me.options.searchByCountryEnabled ? me.options.searchByCountryEnabled : false);
  };


  me.setup = function () {
    var me = this;

    $(me.modalSelector).on('shown.bs.modal', function () {
      me.pagingManager().SingleSelect(true);
      $(me.modalSelector).draggable({
        handle: ".modal-header"
      });
      me.pagingManager().Refresh();
    });

    $(me.modalSelector).on('hidden.bs.modal', function () {

    });


    me.pagingManager().SetOptions({
      AfterRowSelected: function (ROCity) {
        ROCity.IsSelected(true);
        if (me.options.onCitySelected) {
          me.options.onCitySelected(me, ROCity);
        };
      },
      AfterRowDeSelected: function (ROCity) {
        ROCity.IsSelected(false);
        if (me.options.onCityUnSelected) {
          me.options.onCityUnSelected(me, ROCity);
        }
      }
    })


  };

  //me.setup();

  return me;

};

FindROCityModal.prototype.pagingManager = function () {
  if (ViewModel) {
    return ViewModel[this.options.pagingManagerName]();
  }
  return null;
  //var oldValue = viewModel[value](); // read from the observable
  //viewModel[value]('New Value');     // write to the observable 
}

FindROCityModal.prototype.criteria = function () {
  if (ViewModel) {
    return ViewModel[this.options.criteriaName]();
  }
  return null;
}

FindROCityModal.prototype.list = function (newList) {
  if (ViewModel) {
    if (newList) {
      ViewModel[this.options.listName](newList);
    }
    return ViewModel[this.options.listName]();
  }
};

FindROCityModal.prototype.show = function () {
  var me = this;
  if (me.options.beforeShow) { me.options.beforeShow(me); }
  $(me.modalSelector).modal();
};

FindROCityModal.prototype.hide = function () {
  var me = this;
  $(me.modalSelector).modal('hide');
};

FindROCityModal.prototype.refresh = function () {
  var me = this;
  me.pagingManager().Refresh();
};

FindROCityModal.prototype.delayedRefresh = function () {
  var me = this;
  clearTimeout(me.fetchTimeout);
  me.fetchTimeout = setTimeout(function () {
    me.pagingManager().Refresh();
  }, me.options.delay);
};

FindROCityModal.prototype.onCitySelected = function (ROCity, element, event) {
  var si = new SelectedItemObject();
  si.ID(ROCity.CityID());
  si.Description(ROCity.City());
  return si;
};
