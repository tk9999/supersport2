﻿HumanResourceControl = function (options) {
  var me = this;
  me.options = {};
  var defaultOptions = {
    modalID: "CurrentRoomScheduleModal",
    personnelManagerModalID: "PersonnelManagerModal",
    beforeRoomScheduleDeleteModalID: "BeforeRoomScheduleDeleteModal",
    humanResourcePropertyName: "CurrentHumanResource",
    secondmentManagerName: "ROHumanResourceSecondmentListManager",
    secondmentCriteriaName: "ROHumanResourceSecondmentListCriteria",
    secondmentModalID: "HRSecondmentsModal"
  };
  me.options = $.extend({}, defaultOptions, options);
  me.generalTimeout = null;
  me.timeoutDelay = 200;

  me.currentHumanResourceObject = function () {
    if (ViewModel) {
      return ViewModel[me.options.humanResourcePropertyName]()
    }
    return null;
  };
  me.secondmentManagerKO = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentManagerName];
    }
    return null;
  };
  me.secondmentManager = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentManagerName]();
    }
    return null;
  };
  me.secondmentCriteriaKO = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentCriteriaName];
    }
    return null;
  };
  me.secondmentCriteria = function () {
    if (ViewModel) {
      return ViewModel[me.options.secondmentCriteriaName]();
    }
    return null;
  };

};

HumanResourceControl.prototype.setModalPosition = function (ModalID, activatingElement) {
  var elementInfo = activatingElement.getBoundingClientRect();
  var isInInputGroup = $(activatingElement).parents('.input-group');
  var positionInfo = null;
  if (isInInputGroup.length == 1) {
    positionInfo = isInInputGroup[0].getBoundingClientRect();
  };
  var width = elementInfo.left - positionInfo.left;
  $(ModalID)[0].style.left = positionInfo.left - width + "px";
  $(ModalID)[0].style.top = positionInfo.top + 10 + "px";
};

HumanResourceControl.prototype.currentHumanResourceName = function () {
  if (ViewModel.CurrentRoomSchedule()) {
    return ViewModel.CurrentRoomSchedule().Title();
  };
  return "Room Booking";
};

HumanResourceControl.prototype.currentHumanResource = function () {
  if (ViewModel.CurrentHumanResource()) {
    return ViewModel.CurrentHumanResource()
  };
  return null;
};

HumanResourceControl.prototype.updateHRSilent = function () {
  ViewModel.CurrentHumanResource().IsProcessing(true)
  Singular.GetDataStateless("OBLib.HR.HumanResourceList", {
    HumanResourceID: ViewModel.CurrentHumanResource().HumanResourceID()
  }, function (response) {
    if (response.Success) {
      var hr = response.Data[0]
      ViewModel.CurrentHumanResource().PrimarySystemID(hr.PrimarySystemID)
      ViewModel.CurrentHumanResource().PrimaryProductionAreaID(hr.PrimaryProductionAreaID)
      ViewModel.CurrentHumanResource().PrimarySystemAreaCount(hr.PrimarySystemAreaCount)
      ViewModel.CurrentHumanResource().DuplicateCount(hr.DuplicateCount)
      ViewModel.CurrentHumanResource().CurrentUserCanAuthoriseLeave(hr.CurrentUserCanAuthoriseLeave)
      ViewModel.CurrentHumanResource().PrimaryAreaName(hr.PrimaryAreaName)
      ViewModel.CurrentHumanResource().PrimarySkillID(hr.PrimarySkillID)
      ViewModel.CurrentHumanResource().PrimarySkillDisciplineID(hr.PrimarySkillDisciplineID)
      ViewModel.CurrentHumanResource().PrimarySkillDiscipline(hr.PrimarySkillDiscipline)
      ViewModel.CurrentHumanResource().SkillCount(hr.SkillCount)
      ViewModel.CurrentHumanResource().PrimarySkillCount(hr.PrimarySkillCount)
      ViewModel.CurrentHumanResource().MissingRateCount(hr.MissingRateCount)
      ViewModel.CurrentHumanResource().CurrentUserIsHRManager(hr.CurrentUserIsHRManager)
      Singular.Validation.CheckRules(ViewModel.CurrentHumanResource(), true, false)
    }
    else {
      OBMisc.Modals.Error('Error During Rule Checking', 'An error while checking the rules for the current human resource', response.ErrorText, null);
    }
  })
}

HumanResourceControl.prototype.save = function (roomSchedule) {
  Singular.SendCommand("SaveCurrentRoomSchedule", {

  }, function (response) {

  });
};

HumanResourceControl.prototype.canSave = function (roomSchedule) {
  var roomScheduleValid = roomSchedule.IsValid();
  var productionValid = false;
  var adhocBookingValid = false;
  //var areaValid = false;
  if (roomSchedule.ProductionList().length == 1) { productionValid = roomSchedule.ProductionList()[0].IsValid() } else { productionValid = true }
  if (roomSchedule.AdHocBookingList().length == 1) { adhocBookingValid = roomSchedule.AdHocBookingList()[0].IsValid() } else { adhocBookingValid = true }
  //if (roomSchedule.AdHocBookingList().length == 1) { adhocBookingValid = roomSchedule.AdHocBookingList()[0].IsValid() } else { adhocBookingValid = true }
  return (roomScheduleValid && productionValid && adhocBookingValid);
};

HumanResourceControl.prototype.currentSystemAndArea = function (humanResourceOffPeriod) {
  return "Add to " + ViewModel.CurrentSystemName() & " (" & ViewModel.CurrentProductionAreaName() & ")"
};

HumanResourceControl.prototype.removeLeave = function (humanResourceOffPeriod) { };

HumanResourceControl.prototype.currentSecondment = function () {
  if (ViewModel.CurrentHumanResourceSecondment()) {
    return ViewModel.CurrentHumanResourceSecondment()
  };
  return null;
};

HumanResourceControl.prototype.addNewDocument = function () {
  var me = this
  //var x = new Public //Property TempImage As New Singular.Documents.TemporaryDocumentNotRequired
  var nd = ViewModel.HRDocumentList.AddNew()
  nd.ParentID(me.currentHumanResource().HumanResourceID())
  //nd.Attachment(new TemporaryDocumentNotRequiredObject());
};

HumanResourceControl.prototype.saveDocuments = function () {
  var me = this
  me.currentHumanResource().IsProcessing(true)
  //var ser = ViewModel.HRDocumentList.Serialise(false)
  //      list: ser
  Singular.SendCommand("SaveHRDocuments",
    {},
    function (response) {
      if (response.Success) {
        //ViewModel.HRDocumentList.Set(response.Data)
        OBMisc.Notifications.GritterSuccess('Saved Successfully', 'Documents have been saved successfully', 1500)
      } else {
        OBMisc.Notifications.GritterError('Error Saving Documents', response.ErrorText, 3000)
      }
      me.currentHumanResource().IsProcessing(false)
    })
};

HumanResourceControl.prototype.deleteSelectedDocuments = function () {
  var me = this
  me.currentHumanResource().IsProcessing(true)
  var toRemove = []
  ViewModel.HRDocumentList().Iterate(function (doc, docInd) {
    if (doc.IsSelected()) {
      toRemove.push(doc)
    }
  })
  toRemove.Iterate(function (doc, docInd) {
    ViewModel.HRDocumentList.RemoveNoCheck(doc)
  })
  Singular.SendCommand("DeleteHRDocuments",
  {},
  function (response) {
    if (response.Success) {
      OBMisc.Notifications.GritterSuccess('Deleted Successfully', 'Documents have been deleted successfully', 1500)
    } else {
      OBMisc.Notifications.GritterError('Error Deleting Documents', response.ErrorText, 3000)
    }
    me.currentHumanResource().IsProcessing(false)
  })
};

HumanResourceControl.prototype.showDuplicates = function (hr) {
  $("#ROHRDuplicatesModal").off("shown.bs.modal")
  $("#ROHRDuplicatesModal").on("shown.bs.modal", function () {
    Singular.Validation.CheckRules(hr)
  })
  $("#ROHRDuplicatesModal").modal()
};

HumanResourceControl.prototype.hideDuplicates = function (hr) {
  $("#ROHRDuplicatesModal").off("hidden.bs.modal")
  $("#ROHRDuplicatesModal").on("hidden.bs.modal", function () {
    
  })
  $("#ROHRDuplicatesModal").modal('hide')
};

HumanResourceControl.prototype.submitDuplicates = function (hr) {
  var me = this;
  ViewModel.CallServerMethod("SubmitHRDuplicates",
    {
      list: KOFormatterFull.Serialise(hr.ROHRDuplicateList())
    },
    function (response) {
      if (response.Success) {
        HumanResourceBO.GetHRHDuplicates(me.currentHumanResource(), function (response) {
          OBMisc.Notifications.GritterSuccess("Saved", 'duplicates have been updated successfully', 1000)
          Singular.Validation.CheckRules(me.currentHumanResource())
          if (me.currentHumanResource().ROHRDuplicateList().length == 0) {
            me.hideDuplicates()
          }
        })
      }
      else {
        OBMisc.Notifications.GritterError("Save Failed", response.ErrorText, 2000)
      }
    })
};

HumanResourceControl.prototype.allDuplicatesValid = function (hr) {
  var allValid = true
  //hr.ROHRDuplicateList().Iterate(function (itm, itmIndx) {
  //  if (!itm.IsValid()) {
  //    allValid = false
  //  }
  //})
  return allValid
};