﻿FindROCountryControl = function (options) {
  var me = this;

  me.options = {};
  var defaultOptions = {
    modalID: "ROCountryListModal",
    pagingManager: function () {
      if (ViewModel) { return ViewModel.ROCountryListManager() }
    },
    criteria: function () {
      if (ViewModel) { return ViewModel.ROCountryListCriteria() }
    },
    list: function () {
      if (ViewModel) { return ViewModel.ROCountryList() }
    },
    delay: 200,
    onCountrySelected: null
  };
  me.options = $.extend({}, defaultOptions, options);

  me.fetchTimeout = null;
  me.modalSelector = "#" + me.options.modalID;

  me.setup = function () {
    var me = this;

    $(me.modalSelector).on('shown.bs.modal', function () {
      me.options.pagingManager().SingleSelect(true);
      $(me.modalSelector).draggable({
        handle: ".modal-header"
      });
      me.options.pagingManager().Refresh();
    });

    $(me.modalSelector).on('hidden.bs.modal', function () {

    });

  };

  me.setup();

  return me;

};

FindROCountryControl.prototype.show = function () {
  var me = this;
  $(me.modalSelector).modal();
};

FindROCountryControl.prototype.hide = function () {
  var me = this;
  $(me.modalSelector).modal('hide');
};

FindROCountryControl.prototype.refresh = function () {
  var me = this;
  me.options.pagingManager().Refresh();
};

FindROCountryControl.prototype.delayedRefresh = function () {
  var me = this;
  clearTimeout(me.fetchTimeout);
  me.fetchTimeout = setTimeout(function () {
    me.options.pagingManager().Refresh();
  }, me.options.delay);
};

FindROCountryControl.prototype.onCountrySelected = function (ROCountry) {
  var me = this;
  if (me.options.onCountrySelected) {
    me.options.onCountrySelected(me, ROCountry);
  } else {
    console.log(ROCountry);
  }
};