﻿//Singular.HandleFileSelected = function () {
//  //override and do nothing.
//}

var ChooseImage = function (obj, Type) {
  var imageType = ClientData.ROImageTypeList.Find('ImageTypeID', Type);
  Singular.ShowFileDialog(function (e) {
    var sizeInBytes = e.target.files[0].size;
    var sizeInKB = sizeInBytes / 1024;
    if (sizeInKB > imageType.FileSizeLimit) {
      alert('Image exceeds the file size limit of ' + (imageType.FileSizeLimit / 1024).toString() + 'MB');
    }
    else {
      obj.IsLoadingImage(true)
      Singular.UploadFile(e.target.files[0], ViewModel.TempImage(), 'png,jpg,jpeg', function (Result) {
        if (Result.Success) {
          obj.AccreditationImageID(Result.DocumentID);
        } else {
          alert(Result.Response);
        }
        obj.IsLoadingImage(false)
      },
      'Image=true&ImageTypeID=' + Type + '&Stateless=true&ForHumanResourceID=' + obj.HumanResourceID());
    }
  });
}