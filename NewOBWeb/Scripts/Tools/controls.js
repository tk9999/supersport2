﻿//#region Common

class modalBase {
  constructor(parentElement, options) {
    this.options = {
      title: `Select Circuit`,
      onBodyClicked: (event) => { },
      onCloseClicked: (event) => {
        this.hide();
      }
    };
    $.extend(this.options, options);

    this.parentElement = parentElement;
    this.backdrop = null;
    this.modal = null;
    this.body = null;
    this.header = null;
    this.loadingBar = null;

    this.modalDimensions = {
      bodyHeight: 0
    };
  };

  get dimensions() {
    return this.modal.getBoundingClientRect();
  };

  draw() {
    //backdrop
    this.backdrop = document.createElement('div');
    this.backdrop.className = "sober-backdrop";
    this.backdrop.display = 'none';
    this.parentElement.appendChild(this.backdrop);

    //modal
    this.modal = document.createElement('div');
    this.modal.className = "sober-modal";
    this.modal.style.display = 'none';
    this.parentElement.appendChild(this.modal);

    //header
    this.header = document.createElement('h4');
    this.header.innerHTML = `${this.options.title}`;
    this.modal.appendChild(this.header);

    //close button
    this.closeModal = document.createElement('i');
    this.closeModal.className = "fa fa-close fa-2x sober-modal-close pull-right";
    this.modal.appendChild(this.closeModal);

    //body
    this.body = document.createElement('div');
    this.body.className = "sober-modal-body";
    this.modal.appendChild(this.body);

    //loading bar
    this.loadingBar = document.createElement('div');
    this.loadingBar.className = "tri-bar-loader";
    this.loadingBar.style.display = "none";
    this.modal.appendChild(this.loadingBar);

    this.body.addEventListener("click", this.options.onBodyClicked.bind(this));
    this.closeModal.addEventListener("click", this.options.onCloseClicked.bind(this));
  };

  show() {
    return new Promise((resolve, reject) => {
      try {
        this.backdrop.style.display = 'block';
        this.modal.style.display = 'block';
        this.setDimensions();
        resolve(this);
      } catch (err) {
        reject(err);
      }
    });
  };

  hide() {
    return new Promise((resolve, reject) => {
      try {
        this.backdrop.style.display = 'none';
        this.modal.style.display = 'none';
        resolve(this);
      } catch (err) {
        reject(err);
      }
    });
  };

  clearBody() {
    while (this.body.firstChild) {
      this.body.removeChild(this.body.firstChild);
    };
  };

  setDimensions() {
    const dims = this.dimensions;
    this.modalDimensions.bodyHeight = (dims.height - 20);
    this.body.height = this.modalDimensions.bodyHeight.toString() + 'px';
  };

  showLoadingBar() {
    return new Promise((resolve, reject) => {
      try {
        this.loadingBar.style.display = 'block';
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  };

  hideLoadingBar() {
    return new Promise((resolve, reject) => {
      try {
        this.loadingBar.style.display = 'none';
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  };

  static positionFromParent(modalElement, backdropElement, fixedHeight) {

    //dimensions
    let myDimensions = {};
    let parentDimensions = modalElement.parentNode.getBoundingClientRect();
    myDimensions.width = (parentDimensions.width - (0.1 * parentDimensions.width));
    myDimensions.height = (parentDimensions.height - (0.1 * parentDimensions.height));
    if (fixedHeight) {
      myDimensions.height = fixedHeight;
    };

    //backdrop
    if (backdropElement) { 
    backdropElement.style.display = 'block';
    backdropElement.style.opacity = '0.5';
    backdropElement.style.width = parentDimensions.width.toString() + 'px';
    backdropElement.style.height = parentDimensions.height.toString() + 'px';
    };

    //modal
    modalElement.style.display = 'block';
    modalElement.style.width = myDimensions.width.toString() + 'px';
    modalElement.style.height = myDimensions.height.toString() + 'px';
    modalElement.style.left = (0.05 * parentDimensions.width).toString() + 'px';
    modalElement.style.top = (0.05 * parentDimensions.height).toString() + 'px';

  };

  static hideModal(closeIcon) { 
    let modal = closeIcon.parentNode;
    modal.style.display = 'none';
  };
};

class humanResourceElement {
  constructor(parentElement, hrData, options = {}) {
    this.parentElement = parentElement;
    this.element = null;
    this.data = hrData;
  };

  get name() {
    return (this.data.Firstname + ' ' + this.data.Surname);
  };

  drawForShiftAssigner() {
    this.element = document.createElement('div');
    this.element.className = "humanResourceItem";
    this.element.classInstance = this;
    this.element.innerHTML = this.name;
    this.parentElement.appendChild(this.element);
  };
};

class dayElement {
  constructor(parentElement, dayData, options = {}) {
    this.parentElement = parentElement;
    this.element = null;
    this.data = dayData;
    this.staticHeight = null;
    this.staticWidth = null;
  };

  get dimensions() {
    return this.element.getBoundingClientRect();
  };

  get xPosition() {
    return this.dimensions.left;
  };

  get xCoordiates() {
    return [this.dimensions.left, this.dimensions.left + this.dimensions.width];
  };

  get yPosition() {
    return this.dimensions.top;
  };

  drawForShiftAssigner() {
    //day info
    this.element = document.createElement('div');
    this.element.className = "day-header";
    this.element.innerHTML = `<div class="day-name">${this.data.format("ddd")}</div>
                              <div class="day-number">${this.data.format("DD")}</div>`;
    this.parentElement.appendChild(this.element);
    this.staticHeight = this.dimensions.height;
    this.staticWidth = this.dimensions.width;
  };
};

class hrBookingsElement {
  constructor(parentElement, hrItem, options = { width: null }) {
    this.parentElement = parentElement;
    this.element = null;
    this.hrItem = hrItem;
    this.options = options;
  };

  get dimensions() {
    return this.element.getBoundingClientRect();
  };

  get xPosition() {
    return this.dimensions.left;
  };

  get yPosition() {
    return this.dimensions.top;
  };

  drawForShiftAssigner() {
    //day info
    this.element = document.createElement('div');
    this.element.className = "hrBookings";
    this.parentElement.appendChild(this.element);
    if (this.options.width) {
      this.element.style.width = this.options.width.toString() + 'px';
    };
    this.element.classInstance = this;
  };
};

class humanResourceDayElement {
  constructor(parentElement, hrItem, dayItem, dayBookingsData = [], options = {}) {
    this.parentElement = parentElement;
    this.element = null;
    this.dayItem = dayItem;
    this.hrItem = hrItem;
    this.isBusy = false;
    this.disabled = false;
    this.disabledReason = "";
    this.bookingsData = dayBookingsData;
    this.bookingItems = [];
    this.staticDimensions = null;
    this.busyIcon = document.createElement('div');
    this.busyIcon.className = "feedbackIcon-container";
    this.busyIcon.innerHTML = `<i class="fa fa-refresh fa-spin feedback-icon"></i>`;
  };

  get dimensions() {
    return this.element.getBoundingClientRect();
  };

  get xPosition() {
    return this.dimensions.left;
  };

  get xCoordiates() {
    return [this.dimensions.left, this.dimensions.left + this.dimensions.width];
  };

  get yPosition() {
    return this.dimensions.top;
  };

  drawForShiftAssigner() {
    //day info
    this.element = document.createElement('div');
    this.element.className = "bookingsAssigner-hrDay";
    this.element.classInstance = this;
    //this.element.innerHTML = this.dayItem.data.format('dd DD') + ' ' + this.hrItem.data.Firstname;
    this.parentElement.appendChild(this.element);
    this.staticDimensions = this.element.getBoundingClientRect();
    this.bookingsData.forEach(bookingData => {
      let bookingItem = new bookingItemElement(this.element, bookingData, {});
      this.bookingItems.push(bookingItem);
      bookingItem.drawForShiftAssigner();
    });
  };

  addBooking(bookingData) {
    let bookingItem = new bookingItemElement(this.element, bookingData, {});
    this.bookingItems.push(bookingItem);
    bookingItem.drawForShiftAssigner();
  };

  setBusy(busyReason) {
    this.isBusy = true;
    this.element.title = this.disabledReason;
    //this.element.classList.add("feedback-icon");
    this.element.appendChild(this.busyIcon);
    //this.element.innerHTML = `
    //    <i class="fa fa-refresh fa-spin"></i>
    //  `;
  };

  setNotBusy() {
    this.isBusy = false;
    this.element.title = "";
    this.element.removeChild(this.busyIcon);
  };

  enable() {
    this.disabled = false;
    this.disabledReason = "";
    this.element.title = "";
  };

  disable(disableReason) {
    this.disabled = true;
    this.disabledReason = disableReason;
    this.element.title = this.disabledReason;
  };

  saveFailed() {
    this.setNotBusy();
    this.enable();
    ////update the icon
    //this.element.innerHTML = `
    //    <i class="fa fa-times-rectangle"></i>
    //  `;
    //window.setTimeout(() => {
    //  this.element.innerHTML = ``;
    //  this.element.classList.remove("feedback-icon");
    //}, 500);
  };

  saveSucceeded() {
    this.setNotBusy();
    //this.disable("shift saved");
    //update the icon
    //this.element.innerHTML = `
    //    <i class="fa fa-check-square-o fa-pink"></i>
    //  `;
  };
};

class bookingItemElement {
  constructor(parentElement, bookingData, options = {}) {
    this.parentElement = parentElement;
    this.bookingData = bookingData;
    this.element = null;
  };

  updateBooking() {
    //calculations
    let parentRect = this.parentElement.classInstance.staticDimensions;
    let itemHeight = 10;
    let mySD = moment(this.bookingData.StartDateTimeBuffer || this.bookingData.StartDateTime);
    let myED = moment(this.bookingData.EndDateTimeBuffer || this.bookingData.EndDateTime);
    let currentDay = this.parentElement.classInstance.dayItem.data;
    let dayStart = currentDay.clone().startOf('day'),
      dayEnd = currentDay.clone().endOf('day');
    let bookingMinutes = moment.duration(myED.diff(mySD)).asMinutes();
    let dayMinutes = moment.duration(dayEnd.diff(dayStart)).asMinutes();
    let minuteWidth = (parentRect.width / dayMinutes);
    let startMinute = mySD.diff(dayStart, 'minutes');
    let startMinuteLeft = (startMinute * minuteWidth);
    let bookingWidth = (bookingMinutes * minuteWidth);

    //update
    this.element.style.position = "relative";
    this.element.style.left = startMinuteLeft.toString() + "px"; //parentRect.left.toString() + 'px';
    //this.element.style.top = '15px';//((parentRect.top + parentRect.height) - itemHeight).toString() + 'px';
    this.element.style.height = itemHeight.toString() + 'px';
    this.element.style.width = bookingWidth.toString() + 'px';
    this.element.title = `
      ${mySD.format("ddd DD HH:mm")} - ${myED.format("ddd DD HH:mm")}
      ${this.bookingData.ResourceBookingDescription}
`;
  };

  drawForShiftAssigner() {
    this.element = document.createElement('div');
    this.element.className = "bookingItem-element";
    this.element.id = "allocatorBookingID-" + this.bookingData.ResourceBookingID.toString();
    this.updateBooking();
    this.parentElement.appendChild(this.element);
    this.element.classInstance = this;
  };
};

class soberDropDown {
  static getSettings(event) {
    let settingName = event.target.getAttribute("soberDD");
    const namespaces = settingName.split(".");
    let settings = null;
    let context = window.dropDowns;
    for (let i = 0; i < namespaces.length; i++) {
      context = context[namespaces[i]];
    };
    return context;
  };

  static async show(event) {

    const settings = soberDropDown.getSettings(event);
    const businessObject = event.target.businessObject;

    let element = document.querySelector(".soberDropDown");
    let body = document.querySelector("body");

    //cleanup and creation
    if (element) {
      body.removeChild(element);
      element.inputTarget = null;
      element = null;
    };
    element = document.createElement('div');
    element.className = ".soberDropDown";
    body.appendChild(element);

    //busy icon
    let busyIcon = document.createElement("i");
    busyIcon.className = "fa fa-refresh fa-spin soberDropDown-busy";
    element.appendChild(busyIcon);

    //local variables and settings
    let minHeight = 120;
    let maxHeight = 400;
    let targetDimensions = event.target.getBoundingClientRect();

    element.className = "soberDropDown";
    element.style.width = targetDimensions.width.toString() + 'px';
    element.style.left = targetDimensions.left.toString() + 'px';
    element.style.top = (targetDimensions.top + targetDimensions.height).toString() + 'px';
    element.style.maxHeight = maxHeight.toString() + "px";
    element.style.textAlign = "center";
    element.style.display = "block";
    element.inputTarget = event.target;

    element.addEventListener("click", (event) => { soberDropDown.onClick.call(this, event, settings, businessObject) });

    await OBMisc.Tools.sleep(500);

    let localList = [];
    if (settings.listSource == "server") {
      //get the data
      let crit = settings.setCriteria(businessObject);
      localList = await Singular.GetDataStatelessPromise(settings.listNamespace, crit);
    }
    else if (settings.listSource == "clientData") {
      //calculate height
      localList = ClientData[settings.listNamespace];
      let count = localList.length;
      let expectedHeight = (count * 28);

      if (expectedHeight < minHeight) {
        element.style.height = minHeight.toString() + "px";
      }
      else if (expectedHeight > maxHeight) {
        element.style.height = maxHeight.toString() + "px";
      }
      else {
        element.style.height = expectedHeight.toString() + "px";
      };
      //add options
      if (settings.filterList) {
        localList = settings.filterList(localList, businessObject);
      };
    };


    localList.forEach(item => {
      let dropDownItem = document.createElement('div');
      dropDownItem.className = "soberDropDown-option";
      settings.displayFields.forEach(field => {
        dropDownItem.innerHTML = item[field];
        dropDownItem.businessObject = item;
      });
      element.appendChild(dropDownItem);
    });

    element.removeChild(busyIcon);
    element.style.textAlign = "left";
    element.focus();

  };

  static onClick(event, settings, businessObject) {
    if (event.target.classList.contains("soberDropDown-option")) {
      event.currentTarget.inputTarget.actualValue = event.target.businessObject[settings.valueMember];
      event.currentTarget.inputTarget.value = event.target.businessObject[settings.displayMember];
      if (settings.onItemSelected) {
        settings.onItemSelected.call(businessObject, { inputElement: event.currentTarget.inputTarget, selectedItem: event.target.businessObject });
      };
      this.hide(event.currentTarget);
    };
  };

  static hide(dropdown) {
    dropdown.style.display = "none";
  };
};

//#endregion

//#region booking assigner

class bookingAssigner {

  constructor(options) {
    //properties
    this.criteria = {
      bookingType: null,
      systemID: null,
      productionAreaID: null,
      disciplineID: null,
      discipline: '',
      shiftTypeID: 34,
      shiftType: 'Generic Shift',
      startTime: '13:00',
      endTime: '23:00',
      shiftTitle: "Test",
      startDate: moment('August 01 2017'),
      endDate: moment('August 31 2017')
    };

    this.isTicking = false;
    this.selectedSystemAreaShiftType = null;
    this.selectedDiscipline = null;
    this.humanResourcesData = [];
    this.hrItems = [];
    this.dayItems = [];
    this.hrDayItems = [];

    this.dimensions = {
      height: null,
      width: null,
      padding: 20,
      daysWidth: null,
      bookingsColumnHeight: null,
      bookingsColumnWidth: null,
      bookingsColumnHeaderHeight: 50,
      bookingsColumnContentHeight: null,
      bookingsColumnContentWidth: null
    };

    this.positions = {
      lastX: 0,
      lastY: 0,
      currentX: 0,
      currentY: 0,
      currentScroll: 0
    };

    this.eventStates = {
      mousedDown: false
    };

    this.dateSelector = null;

    //object to hold all UI properties for this control
    this.dom = {};
    this.dom.container = document.getElementById(options.containerID);

    //create the modal which will contain the assigner
    this.dom.modalBackdrop = document.createElement('div');
    this.dom.modalBackdrop.id = "bookingAssigner-backdrop";
    this.dom.modalBackdrop.style.display = 'none';

    this.dom.modal = document.createElement('div');
    this.dom.modal.id = "bookingAssigner-modal";
    this.dom.modal.style.display = 'none';

    //loading bar
    this.dom.loadingBar = document.createElement("div");
    this.dom.loadingBar.innerHTML = '<div class="cssload-thecube">'
      + '<div class="cssload-cube cssload-c1"></div>'
      + '<div class="cssload-cube cssload-c2"></div>'
      + '<div class="cssload-cube cssload-c4"></div>'
      + '<div class="cssload-cube cssload-c3"></div>'
    '</div>';

    //modal
    this.dom.container.appendChild(this.dom.modalBackdrop);
    this.dom.container.appendChild(this.dom.modal);

    //shift setup
    this.dom.shiftSetupBackdrop = document.createElement('div');
    this.dom.shiftSetupBackdrop.id = "shiftSetupPanel-backdrop";

    this.dom.shiftSetupContent = document.createElement('div');
    this.dom.shiftSetupContent.id = "shiftSetupPanel-content";
    this.dom.shiftSetupContent.innerHTML = `
        <h4>Shift Setup</h4>
        <i class="fa fa-close fa-2x bookingAssigner-closeIcon pull-right"></i>
        <div class="col-xs-12">
          <div class="form-group">
            <label>Sub-Dept</label>
            <input type="text" class="form-control" id="bookingsAssigner-subdept" soberDD="bookingAssigner.systemID" />
          </div>
          <div class="form-group">
            <label>Area</label>
            <input type="text" class="form-control" id="bookingsAssigner-area" soberDD="bookingAssigner.productionAreaID" />
          </div>
          <div class="form-group">
            <label>Discipline</label>
            <input type="text" class="form-control" id="bookingsAssigner-discipline" soberDD="bookingAssigner.disciplineID" />
          </div>
          <div class="form-group">
            <label>Shift Type</label>
            <input type="text" class="form-control" id="bookingsAssigner-shifttype" soberDD="bookingAssigner.shiftTypeID" />
          </div>
          <div class="form-group">
            <label>Shift Title</label>
            <input type="text" class="form-control" id="bookingsAssigner-shiftTitle" />
          </div>
          <div class="form-group pull-right">
            <button type="button" class="btn btn-md btn-primary" id="bookingsAssigner-btnShiftSetupComplete"><i class="fa fa-check-square-o"></i> Done</button>
          </div>
    `;
    this.dom.modal.appendChild(this.dom.shiftSetupBackdrop);
    this.dom.modal.appendChild(this.dom.shiftSetupContent);

    this.initialDraw();

  };

  static getSystemCriteria() {
    console.log('system criteria');
  };

  static onSystemSelected(args) {
    //let inputElement = args.inputElement;
    //let selectedItem = args.selectedItem;
    this.criteria.systemID = args.selectedItem.SystemID;
    this.criteria.system = args.selectedItem.System;
    args.inputElement.dispatchEvent(new Event('onchange', { bubbles: true }));
  };

  static getProductionAreaCriteria() {

  };

  static onProductionAreaSelected(args) {
    this.criteria.productionAreaID = args.selectedItem.ProductionAreaID;
    this.criteria.productionArea = args.selectedItem.ProductionArea;
    args.inputElement.dispatchEvent(new Event('onchange', { bubbles: true }));
  };

  static getDisciplineCriteria(businessObject) {
    return {
      SystemID: businessObject.criteria.systemID,
      ProductionAreaID: businessObject.criteria.productionAreaID
    }
  };

  static onDisciplineSelected(args) {
    this.criteria.disciplineID = args.selectedItem.DisciplineID;
    this.criteria.discipline = args.selectedItem.Discipline;
    args.inputElement.dispatchEvent(new Event('onchange', { bubbles: true }));
  };

  static getShiftTypeCriteria(businessObject) {
    return {
      SystemID: businessObject.criteria.systemID,
      ProductionAreaID: businessObject.criteria.productionAreaID
    }
  };

  static onShiftTypeSelected(args) {
    this.criteria.shiftTypeID = args.selectedItem.ShiftTypeID;
    this.criteria.shiftType = args.selectedItem.ShiftType;
    this.dom.shiftTitleInput.value = args.selectedItem.ShiftType;
    this.dom.shiftTitleInput.actualValue = this.dom.shiftTitleInput.value;
    this.criteria.shiftTitle = this.dom.shiftTitleInput.actualValue;
    args.inputElement.dispatchEvent(new Event('onchange', { bubbles: true }));
  };

  showLoadingBar() {
    //this.dom.container.style.visibility = 'hidden'
    //this.dom.container.className = 'row animated fadeOut fastest go'
    this.dom.loadingBar.className = 'animated fadeIn fast go'
    this.dom.modal.appendChild(this.dom.loadingBar)
  };

  hideLoadingBar() {
    //hide loading bar
    this.dom.loadingBar.className = "animated fadeOut slowest go"
    //set a timeout to give the animation above some time to complete
    window.setTimeout(() => {
      //remove loading bar
      try {
        this.dom.modal.removeChild(this.dom.loadingBar)
      }
      catch (err) {

      }
    }, 400)
  };

  async refresh() {
    if (this.criteria.systemID && this.criteria.productionAreaID && this.criteria.disciplineID) {
      this.showLoadingBar();
      //try {
      let hrData = await this.getHRData();
      this.humanResourcesData = hrData;
      if (this.humanResourcesData.length > 0) {
        this.bookingsData = await this.getBookingsData();
      } else {
        this.bookingsData = [];
      }
      //draw the content
      this.drawContent();
      //hide loading bar after 1s
      setTimeout(() => { this.hideLoadingBar() }, 1000);
      //}
      //catch (err) {
      //  OBMisc.Notifications.GritterError('Error in setup', err.message, 1000);
      //};
    };
  };

  initialDraw() {

    //while (this.dom.modal.hasChildNodes()) {
    //  this.dom.modal.removeChild(this.dom.modal.childNodes[0])
    //};

    this.dom.header = document.createElement('h4');
    this.dom.header.innerHTML = `Quick Assign Shifts`;
    this.dom.modal.appendChild(this.dom.header);

    //close icon
    this.dom.closeIcon = document.createElement('i');
    this.dom.closeIcon.className = "fa fa-close fa-2x bookingAssigner-closeIcon pull-right";
    this.dom.modal.appendChild(this.dom.closeIcon);

    //settings
    this.dom.settingsRow = document.createElement('div');
    this.dom.settingsRow.className = 'bookingAssigner-settingsRow';
    this.dom.settingsRow.innerHTML = `<button type="button" class="btn btn-xs btn-default" id="bookingsAssigner-setupShift">
                                          <i class="fa fa-gears"></i> Setup Shift
                                      </button>
                                      <button type="button" class="btn btn-xs btn-default" id="bookingsAssigner-selectDates"><i class="fa fa-calendar"></i> Dates</button>
                                      <button type="button" class="btn btn-xs btn-default" id="bookingsAssigner-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                                     `;
    this.dom.modal.appendChild(this.dom.settingsRow);

    //people selection
    this.dom.crewColumn = document.createElement('div');
    this.dom.crewColumn.className = 'bookingAssigner-crewColumn';
    this.dom.crewColumnHeader = document.createElement('div');
    this.dom.crewColumnHeader.className = "bookingAssigner-crewColumn-header";
    this.dom.crewColumnHeader.innerHTML = "Crew";
    this.dom.crewColumnNames = document.createElement('div');
    this.dom.crewColumnNames.className = "bookingAssigner-crewColumn-names";
    this.dom.crewColumn.appendChild(this.dom.crewColumnHeader);
    this.dom.crewColumn.appendChild(this.dom.crewColumnNames);
    this.dom.modal.appendChild(this.dom.crewColumn);

    //bookings header
    this.dom.bookingsColumn = document.createElement('div');
    this.dom.bookingsColumn.className = 'bookingAssigner-bookingsColumn';
    this.dom.bookingsColumnHeader = document.createElement('div');
    this.dom.bookingsColumnHeader.className = "bookingAssigner-bookingsColumn-header";
    this.dom.bookingsColumnHeader.style.height = this.dimensions.bookingsColumnHeaderHeight.toString() + 'px';
    this.dom.bookingsColumn.appendChild(this.dom.bookingsColumnHeader);

    //bookings container
    this.dom.bookingsColumnContent = document.createElement('div');
    this.dom.bookingsColumnContent.className = "bookingAssigner-bookingsColumn-content";
    this.dom.bookingsColumn.appendChild(this.dom.bookingsColumnContent);

    this.dom.modal.appendChild(this.dom.bookingsColumn);

    this.dom.chooseDatesButton = document.getElementById("bookingsAssigner-selectDates");
    this.dom.setupShiftButton = document.getElementById("bookingsAssigner-setupShift");
    this.dom.refreshButton = document.getElementById("bookingsAssigner-refresh");

    //shift setup
    this.dom.subDeptInput = document.getElementById("bookingsAssigner-subdept");
    this.dom.areaInput = document.getElementById("bookingsAssigner-area");
    this.dom.disciplineInput = document.getElementById("bookingsAssigner-discipline");
    this.dom.shiftTypeInput = document.getElementById("bookingsAssigner-shifttype");
    this.dom.shiftTitleInput = document.getElementById("bookingsAssigner-shiftTitle");
    this.dom.shiftSetupCompleteButton = document.getElementById("bookingsAssigner-btnShiftSetupComplete");

    //add the event handlers
    this.addEventHandlers();

  };

  drawContent() {
    //cleanup
    this.cleanupContent();
    this.dayItems = [];
    this.hrItems = [];
    this.hrDayItems = [];

    //days
    let sd = this.criteria.startDate.clone(), ed = this.criteria.endDate.clone();

    //dimensions of days container
    let dayCount = (ed.diff(sd, 'days') + 1);

    this.dimensions.daysWidth = ((dayCount + 1) * 50);
    this.dom.bookingsColumnHeader.style.width = this.dimensions.daysWidth.toString() + "px";
    this.dom.bookingsColumnContent.style.width = this.dimensions.daysWidth.toString() + "px";

    //generate the day items
    while (sd.toDate() <= ed.toDate()) {
      let currentDay = sd.clone();
      let dayItem = new dayElement(this.dom.bookingsColumnHeader, currentDay, {});
      this.dayItems.push(dayItem);
      dayItem.drawForShiftAssigner();
      sd = sd.add(1, 'day');
    };

    //crew
    this.humanResourcesData.forEach((hr) => {
      //hr row
      let hrItem = new humanResourceElement(this.dom.crewColumnNames, hr, {});
      hrItem.drawForShiftAssigner();
      this.hrItems.push(hr);
      //hr days container
      let hrBookings = new hrBookingsElement(this.dom.bookingsColumnContent, hrItem, { width: this.dimensions.daysWidth });
      hrBookings.drawForShiftAssigner();
      //hr days
      this.dayItems.forEach(dayItem => {
        let currentHRBookings = this.bookingsData.filter(booking => { return (booking.ResourceID == hrItem.data.ResourceID) });
        let currentHRDayBookings = currentHRBookings.filter(booking => {
          if (booking.StartDateTimeBuffer) {
            return (dayItem.data.isSame(moment(booking.StartDateTimeBuffer), 'day'));
          } else {
            return (dayItem.data.isSame(moment(booking.StartDateTime), 'day'));
          }
        });
        let hrDay = new humanResourceDayElement(hrBookings.element, hrItem, dayItem, currentHRDayBookings, {});
        hrDay.drawForShiftAssigner();
        this.hrDayItems.push(hrDay);
      });
    });

    this.dimensions.bookingsColumnContentHeight = this.dom.bookingsColumnContent.getBoundingClientRect().height;
    this.dimensions.bookingsColumnContentWidth = this.dom.bookingsColumnContent.getBoundingClientRect().width;
  };

  cleanupContent() {
    while (this.dom.bookingsColumnHeader.firstChild) {
      this.dom.bookingsColumnHeader.removeChild(this.dom.bookingsColumnHeader.firstChild);
    }
    while (this.dom.crewColumnNames.firstChild) {
      this.dom.crewColumnNames.removeChild(this.dom.crewColumnNames.firstChild);
    }
    while (this.dom.bookingsColumnContent.firstChild) {
      this.dom.bookingsColumnContent.removeChild(this.dom.bookingsColumnContent.firstChild);
    }
  };

  show() {
    this.showLoadingBar();

    //dimensions
    let dimensions = this.dom.container.getBoundingClientRect();
    this.dimensions.width = (dimensions.width - (0.1 * dimensions.width));
    this.dimensions.height = (dimensions.height - (0.1 * dimensions.height));

    //backdrop
    this.dom.modalBackdrop.style.display = 'block';
    this.dom.modalBackdrop.style.opacity = '0.5';
    //this.dom.modalBackdrop.style.width = dimensions.width.toString() + 'px';
    //this.dom.modalBackdrop.style.height = dimensions.height.toString() + 'px';

    //modal
    this.dom.modal.style.display = 'block';
    this.dom.modal.style.width = this.dimensions.width.toString() + 'px';
    this.dom.modal.style.height = this.dimensions.height.toString() + 'px';
    this.dom.modal.style.left = (0.05 * dimensions.width).toString() + 'px';
    this.dom.modal.style.top = (0.05 * dimensions.height).toString() + 'px';

    //columns
    let columnHeight = (this.dimensions.height - this.dimensions.padding - 90);
    this.dom.crewColumn.style.height = (this.dimensions.height - this.dimensions.padding - 90).toString() + 'px';
    this.dom.bookingsColumn.style.height = columnHeight.toString() + 'px';
    //this.dom.bookingsColumnContent.style.height = (columnHeight - 50).toString() + 'px';
    this.dimensions.bookingsColumnHeight = columnHeight;

    //column content
    this.dom.crewColumnNames.style.height = (columnHeight - 50).toString() + 'px';

    this.dom.crewColumn.style.width = "20%";
    this.dom.bookingsColumn.style.width = "80%";
    this.dimensions.bookingsColumnWidth = this.dom.bookingsColumn.getBoundingClientRect().width;

    this.update();

    this.hideLoadingBar();
  };

  addEventHandlers() {

    //initial setup of date picker
    $(this.dom.chooseDatesButton).daterangepicker({
      format: 'ddd D MMM YYYY',
      startDate: this.criteria.startDate,
      endDate: this.criteria.endDate,
      linkedCalendars: true,
      autoUpdateInput: true
    });
    $(this.dom.chooseDatesButton).on('show.daterangepicker', (event, picker) => { picker.showCalendars(); });
    $(this.dom.chooseDatesButton).on('apply.daterangepicker', this.datesUpdated.bind(this));

    //setup shift
    this.dom.setupShiftButton.addEventListener("click", this.showShiftSetup.bind(this));

    //refresh
    this.dom.refreshButton.addEventListener("click", this.refresh.bind(this));

    //close modal
    this.dom.closeIcon.addEventListener("click", this.hide.bind(this));

    //humanresource clicked
    $(this.dom.crewColumn).on('click', 'div.humanresource', (elem) => {

    });

    this.dom.crewColumnNames.addEventListener('scroll', this.onCrewColumnNamesScroll.bind(this));
    this.dom.crewColumnNames.addEventListener('mousewheel', this.onCrewColumnNamesMouseWheel.bind(this));
    this.dom.crewColumnNames.addEventListener('DOMMouseScroll', this.onCrewColumnNamesMouseWheel.bind(this));

    this.dom.bookingsColumnContent.addEventListener('click', this.onHRDayClicked.bind(this));
    this.dom.bookingsColumnContent.addEventListener('mousewheel', this.onBookingsMouseWheel.bind(this));
    this.dom.bookingsColumnContent.addEventListener('DOMMouseScroll', this.onBookingsMouseWheel.bind(this));
    this.dom.bookingsColumnContent.addEventListener('pointerdown', this.onBookingsPointerIn.bind(this));
    this.dom.bookingsColumnContent.addEventListener('pointermove', this.onBookingsPointerMove.bind(this));
    this.dom.bookingsColumnContent.addEventListener('pointerup', this.onBookingsPointerOut.bind(this));
    this.dom.bookingsColumnContent.addEventListener('pointerleave', this.onBookingsPointerOut.bind(this));

    //shift setup
    //this.dom.subDeptInput.addEventListener('onchange', this.shiftSetupValuesChanged.bind(this));
    this.dom.shiftSetupContent.addEventListener('onchange', this.shiftSetupValuesChanged.bind(this));
    this.dom.shiftSetupContent.addEventListener('keyup', this.shiftSetupTextChanged.bind(this));
    this.dom.shiftSetupContent.addEventListener('click', this.shiftSetupClick.bind(this));

  };

  async onHRDayClicked(event) {
    //get the items
    let dayItem = event.target.classInstance.dayItem;
    let hrItem = event.target.classInstance.hrItem;

    if (event.target.classInstance.isBusy) {
      alert(event.target.classInstance.disabledReason);
      return;
    };

    if (event.target.classInstance.disabled) {
      alert(event.target.classInstance.disabledReason);
      return;
    };

    //choose shift type
    if (dayItem && hrItem) {
      //shift setup and saving
      event.target.classInstance.setBusy("busy saving...");
      try {
        //calculate start time and end time
        let [sh, sm] = this.criteria.startTime.split(":");
        let [eh, em] = this.criteria.endTime.split(":");
        let sd = dayItem.data.clone();
        let ed = dayItem.data.clone();
        sd.startOf('day').set({
          hour: parseInt(sh),
          minute: parseInt(sm),
          second: 0
        });
        ed.startOf('day').set({
          hour: parseInt(eh),
          minute: parseInt(em),
          second: 0
        });
        //save the shift
        let data = await GenericShiftBO.createQuickShift({
          SystemID: this.criteria.systemID, ProductionAreaID: this.criteria.productionAreaID,
          ShiftTypeID: this.criteria.shiftTypeID, ShiftType: this.criteria.shiftType,
          HumanResourceID: hrItem.data.HumanResourceID, ResourceID: hrItem.data.ResourceID,
          HumanResource: hrItem.data.Firstname,
          DisciplineID: this.criteria.disciplineID, Discipline: this.criteria.discipline,
          StartDateTime: sd.format('DD MMMM YYYY HH:mm'), EndDateTime: ed.format('DD MMMM YYYY HH:mm'),
          ShiftTitle: this.criteria.shiftTitle 
        });
        //save succeeded
        OBMisc.Notifications.GritterSuccess("Saved", "", 250);
        event.target.classInstance.saveSucceeded();
      }
      catch (err) {
        //save failed
        OBMisc.Notifications.GritterError("Error", err, 1000);
        event.target.classInstance.saveFailed();
      }
    };
  };

  datesUpdated(event, picker) {
    this.criteria.startDate = picker.startDate.startOf('day');
    this.criteria.endDate = picker.endDate.endOf('day');
    this.drawContent();
  };

  onCrewColumnNamesScroll(event) {
    event.preventDefault();
    if (event.ctrlKey) { return };

    //direction moved
    let newScroll = event.srcElement.scrollTop;
    let direction = null,
      distanceY = (newScroll - this.positions.currentScroll);

    //console.log(event, this.lastEvent, this.positions.currentScroll, newScroll, distanceY);

    if (newScroll < this.positions.currentScroll) {
      //up
    } else if (newScroll > this.positions.currentScroll) {
      //down
    };
    this.positions.currentScroll = newScroll;
    //update
    //this.calculateYPosition(distanceY);
    //this.updateContentPositions(this.positions.currentX, this.positions.currentY);

    return;
    //this.dom.bookingsColumnContent.scrollTop = event.srcElement.scrollTop;
  };

  onCrewColumnNamesMouseWheel(event) {
    event.preventDefault();
    if (event.ctrlKey) { return };

    //distance moved (throttled by a factor of 5)
    let distanceY = (event.wheelDeltaY / 5);

    //update
    this.calculateYPosition(distanceY);
    this.updateContentPositions(this.positions.currentX, this.positions.currentY);
    this.dom.crewColumnNames.scrollTop = Math.abs(this.positions.currentY);
  };

  onBookingsMouseWheel(event) {
    event.preventDefault();
    if (event.ctrlKey) { return };

    //distance moved (throttled by a factor of 5)
    let distanceY = (event.wheelDeltaY / 5);

    //update
    this.calculateYPosition(distanceY);
    this.updateContentPositions(this.positions.currentX, this.positions.currentY);
    this.dom.crewColumnNames.scrollTop = Math.abs(this.positions.currentY);
  };

  onBookingsPointerIn(event) {
    this.eventStates.mousedDown = true;
    if (!event.ctrlKey) {
      this.positions.lastX = event.clientX;
      this.positions.lastY = event.clientY;
      this.positions.eventX = this.positions.lastX;
      this.positions.eventY = this.positions.lastY;
    }
    return;
  };

  onBookingsPointerMove(event) {
    if (event.ctrlKey && this.eventStates.mousedDown) {

    } else if (this.eventStates.mousedDown) {
      //update pointer positions
      this.positions.lastX = this.positions.eventX;
      this.positions.lastY = this.positions.eventY;
      this.positions.eventX = event.clientX;
      this.positions.eventY = event.clientY;

      //calculate distances moved
      let distanceX = 0;
      distanceX += Math.sqrt(Math.pow(this.positions.lastX - this.positions.eventX, 2));
      if (this.positions.eventX < this.positions.lastX) {
        //moved negative (left)
        distanceX = (-1 * distanceX);
      };
      this.calculateXPosition(distanceX);

      let distanceY = 0;
      distanceY += Math.sqrt(Math.pow(this.positions.lastY - this.positions.eventY, 2));
      if (this.positions.eventY < this.positions.lastY) {
        //moved negative (up)
        distanceY = (-1 * distanceY);
      };
      this.calculateYPosition(distanceY);

      //update positioning
      this.updateContentPositions();

    };
  };

  onBookingsPointerOut(event) {
    this.eventStates.mousedDown = false;
    if (!event.ctrlKey) {
      this.positions.lastX = this.positions.currentX;
      this.positions.lastY = this.positions.currentY;
    }
    return;
  };

  calculateYPosition(deltaY) {
    this.positions.currentY = (this.positions.currentY + deltaY); //current y position plus distance moved
    //prevent moving too low
    if (this.positions.currentY > 0) {
      this.positions.currentY = 0;
    };
    //prevent moving too high
    let remainingContentHeight = (this.positions.currentY + this.dimensions.bookingsColumnContentHeight),
      visibleContentHeight = (this.dimensions.bookingsColumnHeight - this.dimensions.bookingsColumnHeaderHeight);
    let maxTop = (-1 * (this.dimensions.bookingsColumnContentHeight - visibleContentHeight));
    if (remainingContentHeight <= visibleContentHeight) {
      this.positions.currentY = maxTop;
    };
  };

  calculateXPosition(deltaX) {
    this.positions.currentX = (this.positions.currentX + deltaX); //current x position plus distance moved
    //prevent moving too far right
    if (this.positions.currentX > 0) {
      this.positions.currentX = 0;
    };
    //prevent moving too far left
    let remainingContentWidth = (this.positions.currentX + this.dimensions.bookingsColumnContentWidth);
    let maxLeft = (-1 * (this.dimensions.bookingsColumnContentWidth - this.dimensions.bookingsColumnWidth));
    if (remainingContentWidth <= this.dimensions.bookingsColumnWidth) {
      this.positions.currentX = maxLeft;
    };
    //this.positions.lastX = this.positions.currentX;
  };

  updateContentPositions() {
    //bookings x position
    this.dom.bookingsColumnHeader.style.left = this.positions.currentX.toString() + 'px';
    this.dom.bookingsColumnContent.style.left = this.positions.currentX.toString() + 'px';
    //bookings y position
    this.dom.bookingsColumnContent.style.top = this.positions.currentY.toString() + 'px';
    this.dom.crewColumnNames.scrollTop = Math.abs(this.positions.currentY);
  };

  showShiftSetup(event) {
    //container dimensions
    let dimensions = this.dom.modal.getBoundingClientRect();

    //backdrop
    this.dom.shiftSetupBackdrop.style.display = 'block';
    this.dom.shiftSetupBackdrop.style.opacity = '0.5';
    this.dom.shiftSetupBackdrop.style.top = "0px"; //dimensions.top.toString() + 'px';
    this.dom.shiftSetupBackdrop.style.left = "0px"; //dimensions.left.toString() + 'px';
    this.dom.shiftSetupBackdrop.style.width = dimensions.width.toString() + 'px';
    this.dom.shiftSetupBackdrop.style.height = dimensions.height.toString() + 'px';

    //panel
    this.dom.shiftSetupContent.style.display = 'block';
    this.dom.shiftSetupContent.style.width = (dimensions.width / 2).toString() + 'px';
    this.dom.shiftSetupContent.style.height = (dimensions.height / 1.5).toString() + 'px';
    this.dom.shiftSetupContent.style.left = (0.20 * dimensions.width).toString() + 'px';
    this.dom.shiftSetupContent.style.top = (0.15 * dimensions.height).toString() + 'px';

    this.update();
  };

  getHRData() {
    return Singular.GetDataStatelessPromise("OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib", {
      SystemIDList: [this.criteria.systemID],
      ProductionAreaIDList: [this.criteria.productionAreaID],
      DisciplineIDList: [this.criteria.disciplineID],
      PageNo: 1, PageSize: 1000, SortAsc: true, SortColumn: "FirstName"
    });
  };

  getBookingsData() {
    let myResourceIDs = this.humanResourcesData.map(hr => { return hr.ResourceID });
    return Singular.GetDataStatelessPromise("OBLib.Resources.ResourceBookingList", {
      ResourceIDs: myResourceIDs,
      StartDate: this.criteria.startDate.toDate().format('dd MMM yyyy'),
      EndDate: this.criteria.endDate.toDate().format('dd MMM yyyy'),
      IgnoreUserAccess: true
    })
  };

  processSignalRChanges(stringifiedChanges) {
    //this will parse the data into a json object and pass it on to be processed
    let parsedChanges = JSON.parse(stringifiedChanges);
    let updatedBookings = parsedChanges.Data.UpdatedBookings;
    let addedBookings = parsedChanges.Data.AddedBookings;
    let removedBookingIDs = parsedChanges.Data.RemovedBookingIDs;
    console.log(updatedBookings, addedBookings, removedBookingIDs);
    //Process the updates
    updatedBookings.forEach((updBooking, indx) => {
      let bookingItem = this.hrBookingsElement.querySelector("#allocatorBookingID-" + updBooking.ResourceBookingID.toString());
      if (bookingItem) {
        bookingItem.classInstance.bookingData = updBooking;
        bookingItem.classInstance.updateBooking();
      };
    });
    //Process the additions
    addedBookings.forEach((addedBooking, indx) => {
      let bookingDay = moment(addedBooking.StartDateTimeBuffer || addedBooking.StartDateTime);
      let hrDayItem = this.hrDayItems.find(hrDay => {
        return ((hrDay.hrItem.data.ResourceID == addedBooking.DisplayResourceID) && hrDay.dayItem.data.isSame(bookingDay, 'day'));
      });
      if (hrDayItem) {
        hrDayItem.addBooking(addedBooking);
      };
    });
    //Process the deletes
    removedBookingIDs.forEach((resourceBookingID, indx) => {
      let bookingItem = this.hrDayItems.querySelector("#allocatorBookingID-" + resourceBookingID.toString());
      if (bookingItem) {
        let data = bookingItem.classInstance.hrDayItem.booking.bookingsData.find(bd => { return (bd.ResourceBookingID == resourceBookingID) });
        bookingItem.element.parentElement.removeChild(bookingItem.element);
        bookingItem.classInstance.hrDayItem.booking.bookingsData.pop(data);
        bookingItem.classInstance.hrDayItem.booking.bookingItems.pop(bookingItem.classInstance);
      };
    })
  };

  hide() {
    return new Promise((resolve, reject) => {
      try {
        this.dom.modalBackdrop.style.display = "none";
        this.dom.modal.style.display = "none";
        resolve(this);
      } catch (err) {
        reject(err);
      }
    });
  };

  update() {
    //update criteria
    this.criteria.systemID = this.dom.subDeptInput.actualValue;
    this.criteria.system = this.dom.subDeptInput.value;
    this.criteria.productionAreaID = this.dom.areaInput.actualValue;
    this.criteria.productionArea = this.dom.areaInput.value;
    this.criteria.disciplineID = this.dom.disciplineInput.actualValue;
    this.criteria.discipline = this.dom.disciplineInput.value;
    this.criteria.shiftTypeID = this.dom.shiftTypeInput.actualValue;
    this.criteria.shiftType = this.dom.shiftTypeInput.value;
    this.criteria.shiftTitle = this.dom.shiftTitleInput.actualValue;
    //updated enabled/disabled
    if (this.dom.subDeptInput.value == "") {
      this.dom.areaInput.setAttribute("disabled", "disabled");
      this.dom.disciplineInput.setAttribute("disabled", "disabled");
      this.dom.shiftTypeInput.setAttribute("disabled", "disabled");
    } else if (this.dom.areaInput.value == "") {
      this.dom.areaInput.removeAttribute("disabled");
      this.dom.disciplineInput.setAttribute("disabled", "disabled");
      this.dom.shiftTypeInput.setAttribute("disabled", "disabled");
    } else {
      this.dom.areaInput.removeAttribute("disabled");
      this.dom.disciplineInput.removeAttribute("disabled");
      this.dom.shiftTypeInput.removeAttribute("disabled");
    }
    //update required
    if (this.dom.subDeptInput.value == "") {
      this.dom.subDeptInput.parentNode.classList.add("has-error")
    } else {
      this.dom.subDeptInput.parentNode.classList.remove("has-error");
    };
    if (this.dom.areaInput.value == "") {
      this.dom.areaInput.parentNode.classList.add("has-error");
    } else {
      this.dom.areaInput.parentNode.classList.remove("has-error");
    };
    if (this.dom.disciplineInput.value == "") {
      this.dom.disciplineInput.parentNode.classList.add("has-error")
    } else {
      this.dom.disciplineInput.parentNode.classList.remove("has-error");
    };
    if (this.dom.shiftTypeInput.value == "") {
      this.dom.shiftTypeInput.parentNode.classList.add("has-error")
    } else {
      this.dom.shiftTypeInput.parentNode.classList.remove("has-error");
    };
    if (this.dom.shiftTitleInput.value == "") {
      this.dom.shiftTitleInput.parentNode.classList.add("has-error")
    } else {
      this.dom.shiftTitleInput.parentNode.classList.remove("has-error");
    };
    const invalidClasses = ["animated", "infinite", "pulse", "go"];
    if (this.dom.subDeptInput.value == "" || this.dom.areaInput.value == "" || this.dom.disciplineInput.value == "" || this.dom.shiftTypeInput.value == "") {
      //update done button
      this.dom.shiftSetupCompleteButton.setAttribute("disabled", "disabled");
      //update setup shift button
      this.dom.setupShiftButton.className = "btn btn-xs btn-danger animated infinite pulse go";
      //this.dom.setupShiftButton.classList.add(...invalidClasses);
    } else {
      //update done button
      this.dom.shiftSetupCompleteButton.removeAttribute("disabled");
      //update setup shift button
      this.dom.setupShiftButton.className = "btn btn-xs btn-default";
      //this.dom.setupShiftButton.classList.remove(...invalidClasses);
    };

  };

  shiftSetupValuesChanged(event) {
    let value = event.target.value;
    //update input values
    switch (event.target.id) {
      case "bookingsAssigner-subdept":
        this.dom.areaInput.actualValue = null;
        this.dom.areaInput.value = "";
        this.dom.disciplineInput.actualValue = null;
        this.dom.disciplineInput.value = "";
        this.dom.shiftTypeInput.actualValue = null;
        this.dom.shiftTypeInput.value = "";
        break;
      case "bookingsAssigner-area":
        this.dom.disciplineInput.actualValue = null;
        this.dom.disciplineInput.value = "";
        this.dom.shiftTypeInput.actualValue = null;
        this.dom.shiftTypeInput.value = "";
        break;
      case "bookingsAssigner-discipline":
        break;
      case "bookingsAssigner-shifttype":
        break;
      case "bookingsAssigner-shiftTitle":
        this.dom.shiftTitleInput.actualValue = this.dom.shiftTitleInput.value;
        break;
    };
    this.update();
  };

  shiftSetupTextChanged(event) {
    clearTimeout(OBMisc.keypressTimeout);
    OBMisc.keypressTimeout = window.setTimeout(() => {
      if (event.target.value == "") {
        event.target.actualValue = null;
        event.target.dispatchEvent(new Event('onchange', { bubbles: true }));
      };
      let isDropDown = event.target.hasAttribute("soberDD");
      event.target.businessObject = this;
      if (isDropDown) {
        soberDropDown.show(event);
      }
      else if (event.target.id == "bookingsAssigner-shiftTitle") {
        this.criteria.shiftTitle = event.target.value;
      };
    }, 250);
  };

  shiftSetupClick(event) {
    let isDisabled = event.target.hasAttribute("disabled");
    if (!isDisabled) {
      let isDropDown = event.target.hasAttribute("soberDD");
      event.target.businessObject = this;
      if (isDropDown) {
        soberDropDown.show(event);
      }
      else if (event.target.id == "bookingsAssigner-btnShiftSetupComplete") {
        this.refresh();
        this.hideShiftSetup();
      }
      else if (event.target.classList.contains("bookingsAssigner-closeIcon")) {
        this.hideShiftSetup();
      };
    };
  };

  hideShiftSetup() {
    return new Promise((resolve, reject) => {
      try {
        this.dom.shiftSetupBackdrop.style.display = "none";
        this.dom.shiftSetupContent.style.display = "none";
        resolve(this);
      } catch (err) {
        reject(err);
      }
    });
  };

};

Number.prototype.between = function (range, inclusive) {
  var min = Math.min.apply(Math, range),
    max = Math.max.apply(Math, range);
  return inclusive ? this >= min && this <= max : this > min && this < max;
};

//#endregion

//#region skills database

class skillsdatabase {

  //             optional parameter with a default value
  //constructor({containerID = "skillsdatabase"}) 
  constructor({ containerID = "skillsdatabase" }) {
    const me = this

    this.criteria = {
      disciplineID: 4,
      systemID: 1,
      productionareaID: 1,
      productiontypeID: 1
    }

    this.subdepts = ClientData.ROUserSystemList
    this.areas = ClientData.ROUserSystemAreaList

    this.positionsData = []
    this.humanresourcesData = []
    this.humanResourceSkillPositionsData = []

    this.positionElements = []
    this.humanresourceElements = []
    this.humanResourceSkillPositionItems = []

    this.dom = {}
    this.dom.container = document.getElementById(containerID)
    this.dom.loadingBar = document.createElement("div")
    this.dom.loadingBar.innerHTML = '<div class="cssload-thecube">'
      + '<div class="cssload-cube cssload-c1"></div>'
      + '<div class="cssload-cube cssload-c2"></div>'
      + '<div class="cssload-cube cssload-c4"></div>'
      + '<div class="cssload-cube cssload-c3"></div>'
    '</div>';
    this.dom.positionsContainer = null;
    this.dom.hrContainer = null;
    this.dom.skillPositionsContainer = null;

    this.positions = {
      //mouse cursor
      mouseX: 0,
      mouseY: 0,
      //mouse pointer
      currentX: null,
      currentY: null,
      lastX: null,
      lastY: null,
      distanceX: 0,
      distanceY: 0,
      //channel and time positions
      channelX: 0,
      channelY: null
    }

    this.dimensions = {
      containerWidth: screen.width,
      containerHeight: document.body.clientHeight - 85,
      positionTypeHeight: 32,
      positionTypeWidth: 150,
      hrHeight: 32,
      hrWidth: 200,
    }

    this.eventStates = {
      mousedDown: false
    }

  }

  showLoadingBar() {
    this.dom.container.style.visibility = 'hidden';
    this.dom.loadingBar.className = 'animated fadeIn fast go'
    this.dom.container.parentElement.insertBefore(this.dom.loadingBar, this.dom.container)
  }

  hideLoadingBar() {
    //hide loading bar
    this.dom.loadingBar.className = "animated fadeOut slowest go"
    //set a timeout to give the animation above some time to complete
    window.setTimeout(() => {
      //remove loading bar
      this.dom.container.parentElement.removeChild(this.dom.loadingBar)
      //show the layout
      this.dom.container.style.visibility = 'visible';
      this.dom.container.className = 'row animated fadeIn fastest go'
    }, 400)
  }

  refresh() {
    this.cleanup()
    this.getData()
  }

  getData() {

    this.showLoadingBar()
    this.dom.container.style.visibility = 'hidden'
    this.dom.container.className = 'row animated fadeOut fastest go'
    let sd = new Date().format('dd MMM yyyy')
    let ed = new Date().format('dd MMM yyyy')

    Singular.sequentialPromise(function* () {
      let positions = yield Singular.GetDataStatelessPromise("OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeList, OBLib",
        { SystemID: this.criteria.systemID, ProductionAreaID: this.criteria.productionareaID, DisciplineID: this.criteria.disciplineID });
      let people = yield Singular.GetDataStatelessPromise("OBLib.HR.ReadOnly.ROHumanResourceFindList, OBLib",
        {
          SystemIDList: [this.criteria.systemID], ProductionAreaIDList: [this.criteria.productionareaID], DisciplineIDList: [this.criteria.disciplineID],
          PageNo: 1, PageSize: 1000, SortAsc: true, SortColumn: "FirstName"
        });
      let hrids = people.map((item) => { return item.HumanResourceID })
      let posids = positions.map((item) => { return item.PositionID })
      let peoplePositions = yield Singular.GetDataStatelessPromise("OBLib.HR.HumanResourceSkillPositionSkillsDatabaseList, OBLib",
        {
          DisciplineID: this.criteria.disciplineID, HumanResourceIDs: hrids, ProductionTypeID: this.criteria.productiontypeID,
          PositionIDs: posids, ProductionAreaID: this.criteria.productionareaID
        });
      //
      [this.positionsData, this.humanresourcesData, this.humanResourceSkillPositionsData] = [positions, people, peoplePositions]

      //draw the UI
      this.draw()

      this.addEventHandlers()

      //hide loading bar after 1s
      setTimeout(() => {
        this.hideLoadingBar()
      }, 1000)

    }.bind(this))

  }

  cleanup() {
    while (this.dom.container.firstChild) {
      this.dom.container.removeChild(this.dom.container.firstChild)
    };
    [this.positionElements, this.humanresourceElements, this.humanResourceSkillPositionItems] = [[], [], []];
    [this.dom.hrContainer, this.dom.positionsContainer, this.dom.skillPositionsContainer, this.dom.settingsButton] = [null, null, null, null];
  }

  draw() {

    //1. start drawing
    this.dom.container.style.width = this.dimensions.containerWidth.toString() + 'px'
    this.dom.container.style.height = this.dimensions.containerHeight.toString() + 'px'
    this.dom.container.style.overflow = 'hidden'
    this.dom.container.style.position = 'absolute'

    //2. settings button
    this.dom.settingsButton = document.createElement('button')
    this.dom.settingsButton.id = 'settings-button'
    this.dom.settingsButton.className = 'btn btn-xs btn-primary'
    this.dom.settingsButton.innerHTML = '<i class="fa fa-gears"></i> Settings & Filters'
    this.dom.settingsButton.style.position = 'absolute'
    this.dom.settingsButton.style.top = '0px'
    this.dom.settingsButton.style.left = '0px'
    this.dom.settingsButton.style.width = this.dimensions.hrWidth.toString() + 'px'
    this.dom.settingsButton.style.height = this.dimensions.positionTypeHeight.toString() + 'px'
    this.dom.settingsButton.type = 'button'
    this.dom.container.appendChild(this.dom.settingsButton)

    //2. positions
    this.dom.positionsContainer = document.createElement('div')
    this.dom.positionsContainer.className = "positions"
    this.dom.positionsContainer.style.width = ((this.positionsData.length + 1) * this.dimensions.positionTypeWidth).toString() + 'px'
    this.dom.positionsContainer.style.height = this.dimensions.positionTypeHeight.toString() + 'px'
    //this.dom.positionsContainer.style.left = this.dimensions.hrWidth.toString() + 'px'
    this.dom.positionsContainer.style.marginLeft = this.dimensions.hrWidth.toString() + 'px'
    this.dom.positionsContainer.style.top = '0px'
    this.dom.container.appendChild(this.dom.positionsContainer)

    //3. create each positiontype element
    this.positionsData.forEach((pos, posIndx) => {
      this.createPositionTypeObject(pos, this.dom.positionsContainer)
    }, this)

    //4. people
    this.dom.hrContainer = document.createElement('div')
    this.dom.hrContainer.className = "crew"
    this.dom.hrContainer.style.width = this.dimensions.hrWidth.toString() + 'px'
    this.dom.hrContainer.style.height = (this.humanresourcesData.length * this.dimensions.hrHeight).toString() + 'px'
    this.dom.hrContainer.style.left = (0).toString() + 'px'
    //this.dom.hrContainer.style.top = this.dimensions.positionTypeHeight.toString() + 'px'
    this.dom.hrContainer.style.marginTop = this.dimensions.positionTypeHeight.toString() + 'px'
    this.dom.container.appendChild(this.dom.hrContainer)

    //5. create each hr element
    this.humanresourcesData.forEach((hr, hrIndx) => {
      this.createHRObject(hr, hrIndx, this.dom.hrContainer)
    }, this)

    //6. skill positions
    this.dom.skillPositionsContainer = document.createElement('div')
    this.dom.skillPositionsContainer.className = "skillpositions"
    this.dom.skillPositionsContainer.style.width = ((this.positionElements.length + 1) * this.dimensions.positionTypeWidth).toString() + 'px'
    this.dom.skillPositionsContainer.style.height = (this.humanresourceElements.length * this.dimensions.hrHeight).toString() + 'px'
    //this.dom.skillPositionsContainer.style.left = this.dimensions.hrWidth.toString() + 'px'
    this.dom.skillPositionsContainer.style.marginLeft = this.dimensions.hrWidth.toString() + 'px'
    //this.dom.skillPositionsContainer.style.top = this.dimensions.positionTypeHeight.toString() + 'px'
    this.dom.skillPositionsContainer.style.marginTop = this.dimensions.positionTypeHeight.toString() + 'px'
    this.dom.container.appendChild(this.dom.skillPositionsContainer)

    //7. create the elements for each person and person position
    this.humanresourceElements.forEach((hr, hrIndx) => {
      hr = this.createHRSkillRow(hr)
      hr = this.createHRSkillElements(hr)
    }, this)

  }

  createPositionTypeObject(positionData, parentElement) {
    let newPositionType = new positionTypeItem({ positionData, dimensions: { width: this.dimensions.positionTypeWidth, height: this.dimensions.positionTypeHeight } })
    parentElement.appendChild(newPositionType.element)
    this.positionElements.push(newPositionType)
  }

  createHRObject(hrData, hrIndx, parentElement) {
    let newHRItem = new humanResourceItem({ hrData, hrIndx, dimensions: { width: this.dimensions.hrWidth, height: this.dimensions.hrHeight } })
    parentElement.appendChild(newHRItem.element)
    this.humanresourceElements.push(newHRItem)
  }

  createHRSkillRow(hrItem) {
    //create the row for the person
    var calcheight = ((hrItem.index * (this.dimensions.hrHeight))); // - 1
    var hrskillrowElement = document.createElement('div')
    hrskillrowElement.className = 'hrskill-row'
    hrskillrowElement.style.top = (calcheight < 0 ? 0 : calcheight).toString() + 'px'
    hrskillrowElement.style.height = this.dimensions.hrHeight.toString() + 'px'
    hrskillrowElement.style.width = ((this.positionElements.length + 1) * this.dimensions.positionTypeWidth).toString() + 'px'
    hrItem.skillsRowElement = hrskillrowElement
    this.dom.skillPositionsContainer.appendChild(hrItem.skillsRowElement)
    return hrItem
  }

  createHRSkillElements(hrItem) {
    //get the persons skills
    let hrSkills = this.humanResourceSkillPositionsData.filter((skillposdata) => skillposdata.HumanResourceID == hrItem.data.HumanResourceID)
    //for each position that we have available (based on the page filters)
    let skillpos = [], skillposItem = null;
    this.positionElements.forEach((pos, posIndx) => {
      skillpos = hrSkills.filter((skillposdata) => skillposdata.PositionID == pos.data.PositionID)
      if (skillpos.length == 1) { skillpos = skillpos[0] } else { skillpos = null }
      skillposItem = new humanResourceSkillPositionItem({
        skillPositionData: skillpos,
        hrItem: hrItem,
        position: pos,
        dimensions: { width: this.dimensions.positionTypeWidth, height: this.dimensions.hrHeight }
      })
      hrItem.skillsRowElement.appendChild(skillposItem.element)
      this.humanResourceSkillPositionItems.push(skillposItem)
    }, this)
    return hrItem
  }

  addEventHandlers() {
    var me = this

    $(this.dom.container).off('mousewheel DOMMouseScroll')
    $(this.dom.skillPositionsContainer).off('pointerdown pointermove pointerup pointerleave')
    $(this.dom.container).off('click')

    $(this.dom.container).on('mousewheel DOMMouseScroll', function (event) {
      event.preventDefault()
      if (event.ctrlKey) { return }
      me.positions.distanceY = (-1 * event.originalEvent.deltaY)
      var movementY = (me.positions.channelY + me.positions.distanceY)
      me.positions.channelY = movementY
      if (me.positions.channelY > 0) { me.positions.channelY = 0 }
      //event.originalEvent.deltaX
      me.updatePosition(me.positions.channelX, me.positions.channelY)
      //deltaMode,deltaX,deltaY,deltaZ,detail
    })

    $(this.dom.skillPositionsContainer).on('pointerdown pointermove pointerup pointerleave', function (event) {
      event.preventDefault()
      me.positions.mouseX = event.clientX
      me.positions.mouseY = event.clientY

      if (event.type == 'pointerdown') {
        me.eventStates.mousedDown = true
        if (!event.ctrlKey) {
          //move the thing
          me.positions.lastX = event.clientX
          me.positions.lastY = event.clientY
          me.positions.currentX = event.clientX
          me.positions.currentY = event.clientY
        }
        return
      }

      if (event.type == 'pointerleave') {
        if (!event.ctrlKey) {
          //move the thing
          me.positions.lastX = me.positions.channelX
          me.positions.lastY = me.positions.channelY
          me.positions.currentX = me.positions.channelX
          me.positions.currentY = me.positions.channelY
        }
        return
      }

      if (event.type == 'pointerup') {
        me.eventStates.mousedDown = false
        if (!event.ctrlKey) {
          //move the thing
          me.positions.lastX = me.positions.channelX
          me.positions.lastY = me.positions.channelY
          me.positions.currentX = me.positions.channelX
          me.positions.currentY = me.positions.channelY
        }
        return
      }

      if (event.type == 'pointermove') {
        if (me.eventStates.mousedDown) {
          //move the thing

          //position data
          me.positions.lastX = me.positions.currentX
          me.positions.lastY = me.positions.currentY
          me.positions.currentX = event.clientX
          me.positions.currentY = event.clientY

          //calculate distances moved
          me.positions.distanceX = 0
          me.positions.distanceX += Math.sqrt(Math.pow(me.positions.lastX - me.positions.currentX, 2))
          if (me.positions.currentX < me.positions.lastX) {
            me.positions.distanceX = (-1 * me.positions.distanceX)
          }

          me.positions.distanceY = 0
          me.positions.distanceY += Math.sqrt(Math.pow(me.positions.lastY - me.positions.currentY, 2))
          if (me.positions.currentY < me.positions.lastY) {
            me.positions.distanceY = (-1 * me.positions.distanceY)
          }

          //determine new x of channel
          var moveX = (me.positions.channelX + me.positions.distanceX)
          if (moveX > 0) { moveX = 0 }

          //determine new y of channel
          var moveY = (me.positions.channelY + me.positions.distanceY)
          if (moveY > 0) { moveY = 0 }

          me.updatePosition(moveX, moveY)

        }
      }

    })

    $(this.dom.skillPositionsContainer).on('click', 'input.humanresourceskillposition-editor', function (event) {
      this.focus()
      this.select()
    })

    $(this.dom.skillPositionsContainer).on('change', 'input.humanresourceskillposition-editor', function (event) {
      this.businessObject.updateSkillPosition()
    })

    this.onSettingsButtonClicked = this.onSettingsButtonClicked.bind(this)
    this.dom.settingsButton.addEventListener('click', this.onSettingsButtonClicked)

  }

  updatePosition(newX, newY) {

    //if(newX < this.dimensions.hrWidth) {
    //  newX = this.dimensions.hrWidth
    //}
    this.positions.channelX = newX
    this.dom.positionsContainer.style.left = (this.positions.channelX).toString() + 'px'
    this.dom.skillPositionsContainer.style.left = (this.positions.channelX).toString() + 'px'

    //if(newY < this.dimensions.positionTypeHeight) {
    //  newY = this.dimensions.positionTypeHeight
    //}
    this.positions.channelY = newY
    this.dom.positionsContainer.style.top = "0px"
    this.dom.skillPositionsContainer.style.top = (this.positions.channelY).toString() + 'px'
    this.dom.hrContainer.style.top = (this.positions.channelY).toString() + 'px'

  }

  onSettingsButtonClicked() {

  }

}

class positionTypeItem {

  constructor({ positionData, dimensions = { width: 150, height: 32 } }) {
    this.data = positionData
    this.element = null
    this.dimensions = dimensions
    this.buildElement()
  }

  buildElement() {
    this.element = document.createElement('div')
    this.element.className += "position-item"
    this.element.innerHTML = this.data.Position
    this.element.style.width = this.dimensions.width.toString() + 'px'
    this.element.style.height = this.dimensions.height.toString() + 'px'
  }

}

class humanResourceItem {

  constructor({ hrData, hrIndx, dimensions = { width: 150, height: 32 } }) {
    this.data = hrData
    this.index = hrIndx
    this.element = null
    this.dimensions = dimensions
    this.buildElement()
  }

  buildElement() {
    this.element = document.createElement('div')
    this.element.className += "humanresource-item"
    this.element.style.top = (this.index * (this.dimensions.height)).toString() + 'px'
    this.element.style.left = '0px'
    //'<img class="hr-image" src="' + window.SiteInfo.InternalPhotosPath + 'anonymous-user-icon.png"></img> 
    this.element.innerHTML = '<span>' + this.data.Firstname + " " + this.data.Surname + '</span>'
    this.element.style.width = this.dimensions.width.toString() + 'px'
    this.element.style.height = this.dimensions.height.toString() + 'px'
    this.element.businessObject = this
  }

}

class humanResourceSkillPositionItem {

  constructor({ skillPositionData, hrItem, position, dimensions = { width: 150, height: 32 } }) {
    this.data = skillPositionData
    this.hrItem = hrItem
    this.position = position
    this.element = null
    this.editor = null
    this.busyIcon = null
    this.dimensions = dimensions
    this.buildElement()
  }

  buildElement() {
    //create the container element
    this.element = document.createElement('div')
    this.element.className = "humanresourceskillposition-item"
    this.element.style.height = this.dimensions.height.toString() + 'px'
    this.element.style.width = this.dimensions.width.toString() + 'px'

    //create the editor
    this.editor = document.createElement('input')
    this.editor.className = "humanresourceskillposition-editor editor-visible"
    this.element.appendChild(this.editor)

    //create a busy icon
    this.busyIcon = document.createElement('i')
    this.busyIcon.className = "fa fa-refresh fa-spin isNotBusy"
    this.busyIcon.style.display = 'none'
    this.element.appendChild(this.busyIcon)

    //if a skill position for this hr exists
    if (this.data) {
      this.editor.value = this.data.PositionLevel
      this.editor.title = this.data.LevelDescription
    }
    this.editor.businessObject = this
  }

  updateSkillPosition() {
    //update or create the business object
    if (this.data) {
      this.data.PositionLevel = this.editor.value
      if (this.data.StartDate == null) {
        this.data.StartDate = new Date().format('dd MMM yyyy')
      }
    }
    //else {
    //	let skillPos = new HumanResourceSkillPositionObject()
    //	let skillpos = skillPos.Serialise()
    //	skillpos.HumanResourceSkillPositionID = 0
    //	skillpos.HumanResourceSkillID = 0
    //	skillpos.ProductionTypeID = this.criteria.productiontypeID
    //	skillpos.ProductionType = ""//this.criteria.productiontypeID
    //	skillpos.PositionID = this.position.data.PositionID
    //	skillpos.StartDate = new Date()
    //	skillpos.PositionLevelID =  null
    //	skillpos.ProductionAreaID = this.criteria.productionAreaID
    //	skillpos.RoomTypeID = null
    //	skillpos.RoomID = null
    //	skillpos.PositionTypeID = this.position.data.PositionTypeID
    //	skillpos.PositionLevel = this.editor.value
    //	skillpos.LevelDescription = ""
    //	this.data = skillpos
    //}
    //save the business object
    this.editor.classList.add('editor-hidden')
    this.editor.style.display = 'none'
    this.busyIcon.classList.remove("isNotBusy")
    this.busyIcon.style.display = 'block'
    this.busyIcon.classList.add("isBusy")
    //save the position
    HumanResourceSkillPositionSkillsDatabaseBO.save(this.data, {
      onSuccess: (response) => {
        this.data = response.Data
        this.editor.classList.remove('editor-hidden')
        this.editor.style.display = 'block'
        this.busyIcon.classList.remove("isBusy")
        this.busyIcon.style.display = 'none'
        this.busyIcon.classList.add("isNotBusy")
      }
    })
  }

}

//#endregion

window.dropDowns = {
  bookingAssigner: {
    systemID: {
      listSource: "clientData", listNamespace: "ROUserSystemList", displayFields: ["System"], displayMember: "System", valueMember: "SystemID",
      setCriteria: bookingAssigner.getSystemCriteria, showOnFocus: true,
      onItemSelected: bookingAssigner.onSystemSelected
    },
    productionAreaID: {
      listSource: "clientData", listNamespace: "ROUserSystemAreaList", displayFields: ["ProductionArea"], displayMember: "ProductionArea", valueMember: "ProductionAreaID",
      filterList: (list, businessObject) => {
        return list.filter(area => { return ((businessObject.criteria.SystemID == null) || (list.SystemID == businessObject.criteria.systemID)) });
      },
      setCriteria: bookingAssigner.getProductionAreaCriteria, showOnFocus: true,
      onItemSelected: bookingAssigner.onProductionAreaSelected
    },
    disciplineID: {
      listSource: "server", listNamespace: "OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineListSelect",
      displayFields: ["Discipline"], displayMember: "Discipline", valueMember: "DisciplineID",
      setCriteria: bookingAssigner.getDisciplineCriteria, showOnFocus: true,
      onItemSelected: bookingAssigner.onDisciplineSelected
    },
    shiftTypeID: {
      listSource: "server", listNamespace: "OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList",
      displayFields: ["ShiftType"], displayMember: "ShiftType", valueMember: "ShiftTypeID",
      setCriteria: bookingAssigner.getShiftTypeCriteria, showOnFocus: true,
      onItemSelected: bookingAssigner.onShiftTypeSelected
    }
  }
}