﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="ErrorPage.aspx.vb" Inherits="NewOBWeb.ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<%--  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <h2>Error</h2>

  
    <div style="margin-bottom:20px;">
      <img src="Images/IconBug.png" alt="Error" style="vertical-align:middle; margin-right:20px;" />
    
      <%= LastError%>
    </div>
   

    <span style="font-family:Monospace; font-size:8pt"><%= LastErrorDetails%></span> 

</asp:Content>
