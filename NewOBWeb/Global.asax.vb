Imports System.Web.SessionState
Imports System.Web.Routing
Imports System.Web.Mvc
Imports System.Web.Http
Imports System.Web.Optimization
Imports System

Public Class Global_asax
  Inherits Singular.Web.ApplicationSettings(Of OBLib.Security.OBIdentity)

  Protected Overrides Sub SetupScripts(SM As Singular.Web.Scripts.ScriptManager)
    MyBase.SetupScripts(SM)

    'With SM.AddScriptGroup("Bootstrap", "../Scripts")
    '  .EnableCombineAndMinify(Singular.Web.Scripts.GetPath(Singular.Web.Scripts.ScriptLocation.Project), .AddScript("bootstrap.js", "bootstrap.min.js"))
    'End With

    'With SM.AddScriptGroup("Graph", Singular.Web.Scripts.GetPath(Singular.Web.Scripts.ScriptLocation.Project))
    '  .EnableCombineAndMinify(Singular.Web.Scripts.GetPath(Singular.Web.Scripts.ScriptLocation.Project), .AddScript("Graph.js", "Graph.min.js"))
    'End With

    'With SM.AddScriptGroup("Home", Singular.Web.Scripts.GetPath(Singular.Web.Scripts.ScriptLocation.Project))
    '  .EnableCombineAndMinify(Singular.Web.Scripts.GetPath(Singular.Web.Scripts.ScriptLocation.Project), .AddScript("Home.js", "Home.min.js"))
    'End With

  End Sub

  Overrides Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
    'Try
    '  ''  'Initialisation for Hubs for IM
    '  'Dim hubConfiguration As New Microsoft.AspNet.SignalR.HubConfiguration
    '  'hubConfiguration.EnableDetailedErrors = True
    '  'hubConfiguration.EnableJavaScriptProxies = True
    '  'hubConfiguration.EnableCrossDomain = True
    '  'RouteTable.Routes.MapHubs(hubConfiguration)
    'Catch ex As Exception
    'End Try
    'AreaRegistration.RegisterAllAreas()
    'WebApiConfig.Register(GlobalConfiguration.Configuration)
    'FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
    'RouteConfig.RegisterRoutes(RouteTable.Routes)
    'BundleConfig.RegisterBundles(BundleTable.Bundles)

    MyBase.Application_Start(sender, e)

    'Set the Common Data Reset Time
    'OBLib.CommonData.DefaultLifeTime = New TimeSpan(1, 0, 0)
    'Initialise Common Data Application Scope Lists
    OBLib.CommonData.GetCachedLists()
    Singular.Reporting.ListToXML_UseOldMethod = True
    Singular.Web.Data.JS.JSSerialiser.ProtectKeyProperties = False
    'Singular.Reporting.ListToXMLNodeName = "Table"
    Singular.CSLALib.ContextInfo.SetContextInfo = True

    RouteTable.Routes.Add(New Route("Api/GetImage", New Singular.Web.WebServices.StaticRouteHandler(Of OBLib.Images.ImgDataHandler)))
  End Sub

  Overrides Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
    MyBase.Session_Start(sender, e)
    'Initialise Common Data Session Scope Lists
    OBLib.CommonData.InitialiseSessionLists()
  End Sub

  Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)

    Dim Current = System.Web.HttpContext.Current
    If Current IsNot Nothing Then
      If (Not Singular.Debug.InDebugMode OrElse Current.Request.AppRelativeCurrentExecutionFilePath.EndsWith(".ashx")) AndAlso
          Not Current.Request.AppRelativeCurrentExecutionFilePath.EndsWith("ErrorPage.aspx") AndAlso
          Not Current.Request.AppRelativeCurrentExecutionFilePath.EndsWith(".svc") Then
        If System.Web.HttpContext.Current.Session IsNot Nothing Then
          Session("LastError") = Singular.Debug.RecurseExceptionMessage(Server.GetLastError).Replace(vbCrLf, "<br/>")
          Session("LastErrorLocation") = System.Web.HttpContext.Current.Request.Path
        End If
        If Not Server.GetLastError.ToString.IndexOf("FileDownloader.SendDocument Failed") >= 0 Then
          Response.Redirect("~/ErrorPage.aspx")
        End If
      End If
    End If

  End Sub

  Protected Overrides Sub ApplicationSetup()
    MyBase.ApplicationSetup()

    'Set the Common Data Reset Time
    OBLib.CommonData.DefaultLifeTime = New TimeSpan(1, 0, 0)
    'Initialise Common Data Application Scope Lists
    OBLib.CommonData.GetCachedLists()
    Singular.Reporting.ProjectReportHierarchy = New OBWebReports.OBWebReportHierarchy
    Singular.Web.Scripts.Settings.UseCDN = False
    'Singular.Web.Scripts.Settings.LibJQueryVersion = Singular.Web.Scripts.ScriptSettings.JQueryVersion.JQ_1_12_4
    'Singular.Web.Scripts.Settings.LibJQueryUIVersion = Singular.Web.Scripts.ScriptSettings.JQueryUIVersion.JQ_UI_1_12_1
    Singular.Web.Scripts.Settings.SupportECMA6 = True
    Singular.Web.Controls.UsesPagedGrid = True
    Singular.Web.Controls.DefaultDropDownType = Singular.DataAnnotations.DropDownWeb.SelectType.Combo
    Singular.Web.Controls.DefaultButtonStyle = Singular.Web.ButtonStyle.Bootstrap
    Singular.Security.PasswordPolicy.EncryptPassword = True
    Singular.Security.PasswordPolicy.EnforcePasswordPolicy = False
    Singular.CSLALib.ContextInfo.SetContextInfo = True

  End Sub

  Public Overrides Sub Application_BeginRequest(sender As Object, e As System.EventArgs)
    MyBase.Application_BeginRequest(sender, e)
    ValidateRequest()
  End Sub

  Private Sub ValidateRequest()

    'only if not logged in, and not browserupgrade.aspx and using IE less than 11
    If (Not (Request.Path.Contains("BrowserUpgrade.aspx") Or Request.Path.Contains("SignalRWindowsReceiver.aspx"))) AndAlso Request.Browser.Browser = "IE" AndAlso Request.Browser.MajorVersion < 10 Then
      Response.Redirect("~/BrowserUpgrade.aspx")
    End If

  End Sub

End Class
