﻿Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Csla
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.SystemManagement
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Timesheets.ReadOnly
Imports OBLib.Timesheets

Public Class SystemManagement
  Inherits OBPageBase(Of SystemManagementVM)
End Class

Public Class SystemManagementVM
  Inherits OBViewModelStateless(Of SystemManagementVM)

#Region " Properties "

  Public Shared CurrentDepartmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrentDepartmentID, "ID")
  <DropDownWeb(GetType(RODepartmentList), DisplayMember:="Department", ValueMember:="DepartmentID"),
  Required(ErrorMessage:="Department is required"),
  Display(Name:="Department"),
  SetExpression("SystemManagementPage.CurrentDepartmentIDSet(self)")>
  Public Property CurrentDepartmentID As Integer?
    Get
      Return GetProperty(CurrentDepartmentIDProperty)
    End Get
    Set(value As Integer?)
      SetProperty(CurrentDepartmentIDProperty, value)
    End Set
  End Property

  Public Shared CurrentSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrentSystemID, "ID")
  <DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID", FilterMethodName:="SystemManagementPage.filterSystems"),
  Required(ErrorMessage:="Sub-Dept is required"),
  Display(Name:="Sub-Dept"),
  SetExpression("SystemManagementPage.CurrentSystemIDSet(self)")>
  Public Property CurrentSystemID As Integer?
    Get
      Return GetProperty(CurrentSystemIDProperty)
    End Get
    Set(value As Integer?)
      SetProperty(CurrentSystemIDProperty, value)
    End Set
  End Property

  Public Shared CurrentProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrentProductionAreaID, "ID")
  <DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", FilterMethodName:="SystemManagementPage.filterProductionAreas"),
  Required(ErrorMessage:="Area is required"),
  Display(Name:="Area"),
  SetExpression("SystemManagementPage.CurrentProductionAreaIDSet(self)")>
  Public Property CurrentProductionAreaID As Integer?
    Get
      Return GetProperty(CurrentProductionAreaIDProperty)
    End Get
    Set(value As Integer?)
      SetProperty(CurrentProductionAreaIDProperty, value)
    End Set
  End Property

  Public Property ManageTimesheetRequirements As Boolean
  Public Property ManageYearlyRequirements As Boolean

  Public Property ROUserSystemList As OBLib.Maintenance.General.ReadOnly.ROUserSystemList

  Public Property CurrentSystemProductionAreaID As Integer?
  Public Property ROSystemProductionArea As OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionArea

  Public Property ROSystemYearList As ROSystemYearList
  Public Property ROSystemYearListCriteria As ROSystemYearList.Criteria
  Public Property ROSystemYearListManager As Singular.Web.Data.PagedDataManager(Of SystemManagementVM)
  Public Property CurrentSystemYear As SystemYear

  Public Property ROTimesheetRequirementPagedList As ROTimesheetRequirementPagedList
  Public Property ROTimesheetRequirementPagedListCriteria As ROTimesheetRequirementPagedList.Criteria
  Public Property ROTimesheetRequirementPagedListManager As Singular.Web.Data.PagedDataManager(Of SystemManagementVM)
  Public Property CurrentTimesheetRequirement As TimesheetRequirement
  Public Property FreelancerRateList As SystemProductionAreaFreelancerRateList
  Public Property SystemProductionArea As SystemProductionArea

#End Region

#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad

    'User Variables
    CurrentDepartmentID = OBLib.Security.Settings.CurrentUser.DepartmentID
    CurrentSystemID = OBLib.Security.Settings.CurrentUser.SystemID
    'CurrentProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    ManageTimesheetRequirements = False
    ManageYearlyRequirements = False

    'User Systems
    ROUserSystemList = OBLib.Maintenance.General.ReadOnly.ROUserSystemList.GetROUserSystemList(OBLib.Security.Settings.CurrentUser.UserID)

    'Years
    ROSystemYearList = New ROSystemYearList
    ROSystemYearListCriteria = New ROSystemYearList.Criteria
    ROSystemYearListManager = New Singular.Web.Data.PagedDataManager(Of SystemManagementVM)(Function(d) d.ROSystemYearList,
                                                                                            Function(d) d.ROSystemYearListCriteria,
                                                                                            "YearStartDate", 5, False)

    'Timesheet Requirements
    ROTimesheetRequirementPagedList = New ROTimesheetRequirementPagedList
    ROTimesheetRequirementPagedListCriteria = New ROTimesheetRequirementPagedList.Criteria
    ROTimesheetRequirementPagedListManager = New Singular.Web.Data.PagedDataManager(Of SystemManagementVM)(Function(d) d.ROTimesheetRequirementPagedList,
                                                                                                           Function(d) d.ROTimesheetRequirementPagedListCriteria,
                                                                                                           "StartDate", 10, True)

    'Client Side Lists
    ClientDataProvider.AddDataSource("RODepartmentList", OBLib.CommonData.Lists.RODepartmentList, False)
    ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
    ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROUserSystemList", OBLib.Security.Settings.CurrentUser.ROUserSystemList, False)
    ClientDataProvider.AddDataSource("ROUserSystemAreaList", OBLib.Security.Settings.CurrentUser.ROUserSystemAreaList, False)
    ClientDataProvider.AddDataSource("ROTeamNumberList", OBLib.CommonData.Lists.ROTeamNumberList, False)
    ClientDataProvider.AddDataSource("ROSystemTeamNumberList", OBLib.CommonData.Lists.ROSystemTeamNumberList, False)
    ClientDataProvider.AddDataSource("ROContractTypeList", OBLib.CommonData.Lists.ROContractTypeList, False)
    ClientDataProvider.AddDataSource("RODisciplineList", OBLib.CommonData.Lists.RODisciplineList, False)
    ClientDataProvider.AddDataSource("ROSystemProductionAreaList", OBLib.CommonData.Lists.ROSystemProductionAreaList, False)
    ClientDataProvider.AddDataSource("ROShiftAllowanceTypeList", OBLib.CommonData.Lists.ROShiftAllowanceTypeList, False)
    'ClientDataProvider.AddDataSource("ROPositionTypeList", OBLib.CommonData.Lists.ROPositionTypeList, False)
    'ClientDataProvider.AddDataSource("ROPositionList", OBLib.CommonData.Lists.ROPositionList, False)

    MessageFadeTime = 5000

  End Sub

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()
  End Sub

#End Region

#Region "Methods"

  '  Private Sub SaveYearlyRequirment(CommandArgs As Web.CommandArgs)
  '    Try
  '      Dim lst As New SystemYearList
  '      lst.Add(CurrentSystemYear)
  '      Dim sh As SaveHelper = TrySave(lst)
  '      If sh.Success Then
  '        lst = sh.SavedObject
  '        CurrentSystemYear = lst(0)
  '        lst = OBLib.Maintenance.SystemManagement.SystemYearList.GetSystemYearList(CurrentSystemYear.SystemYearID)
  '        CurrentSystemYear = lst(0)
  '        CommandArgs.ReturnData = New Singular.Web.Result(True)
  '      Else
  '        CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
  '      End If
  '    Catch ex As Exception
  '      CommandArgs.ReturnData = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
  '    End Try
  '  End Sub

#End Region

End Class