﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="SystemManagement.aspx.vb" Inherits="NewOBWeb.SystemManagement" %>

<%@ Import Namespace="OBLib.Maintenance.SystemManagement" %>

<%@ Import Namespace="OBLib.Timesheets" %>

<%@ Import Namespace="OBLib.Timesheets.ReadOnly" %>

<%@ Import Namespace="OBLib.Synergy" %>

<%@ Import Namespace="Singular.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance/ShiftPatterns.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/SystemManagement.js"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Maintenance.js"></script>
  <script type="text/javascript" src="../Scripts/Pages/SystemManagementPage.js"></script>
  <style type="text/css">
    .ValidationPopup {
      min-height: 131px;
      max-height: 150px;
      max-width: 500px;
    }

    .FadeHide {
      visibility: hidden;
      opacity: 0;
      transition: visibility 1s, opacity 1s;
    }

    .FadeDisplay {
      visibility: visible;
      opacity: 1;
      transition: visibility 1s, opacity 1s linear;
    }

    .add-vertical-scroll tbody td {
      width: 100%;
    }

    .add-vertical-scroll {
      display: block;
      max-height: 500px;
      overflow-y: scroll;
    }
  </style>
  <script type="text/javascript">
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      h.HTML.Heading1("System Mangement")
      'Toolbar
      With Helpers.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock(, , True)
            With .ContentTag
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d) ViewModel.CurrentDepartmentID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentDepartmentID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d) ViewModel.CurrentSystemID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentSystemID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d) ViewModel.CurrentProductionAreaID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.CurrentProductionAreaID, Singular.Web.BootstrapEnums.InputSize.Small, , )
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.With(Of SystemProductionArea)(Function(c) c.SystemProductionArea)
                    With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                                             Singular.Web.PostBackType.None, "SystemManagementPage.saveSystemProductionArea($data)", )
                      .Button.ID = "saveSPAButton"
                      .Button.AddClass("btn-block")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.css, Function(c) If(c.IsDirty, "flash slowest infinite", ""))
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Left)
            With .AddTab("YearlyRequirements", "fa-calendar", "SystemManagementPage.refreshYearlyRequirements()", "Year", , )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('YearlyRequirements', ViewModel)")
              .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('YearlyRequirements', ViewModel)")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 3, 2, 2)
                    With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                                   Singular.Web.PostBackType.None, "SystemManagementPage.refreshYearlyRequirements()", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 2, 2)
                    With .Helpers.Bootstrap.Button(, "New Year", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                  Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", "",
                                                  Singular.Web.PostBackType.None, "SystemManagementPage.newYearlyRequirement()")
                      .Button.AddClass("btn-block")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, )
                    .Helpers.Control(New NewOBWeb.Controls.ROSystemYearRequirementsTable(Of NewOBWeb.SystemManagementVM)())
                  End With
                End With
              End With
            End With
            With .AddTab("TimesheetRequirements", "fa-hourglass-2", "SystemManagementPage.refreshTimesheetRequirements()", "Timesheets", , )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Timesheets', ViewModel)")
              .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Timesheets', ViewModel)")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 3, 2, 2)
                    With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", ,
                                                   Singular.Web.PostBackType.None, "SystemManagementPage.refreshTimesheetRequirements()", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 2, 2)
                    With .Helpers.Bootstrap.Button(, "New Requirement", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-plus-circle", ,
                                                   Singular.Web.PostBackType.None, "SystemManagementPage.newTimesheetRequirement()", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)

                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.PagedGridFor(Of ROTimesheetRequirementPaged)("ViewModel.ROTimesheetRequirementPagedListManager", "ViewModel.ROTimesheetRequirementPagedList",
                                                                                      False, False, False, False, True, True, True, ,
                                                                                      Singular.Web.BootstrapEnums.PagerPosition.Bottom, , ,
                                                                                      True, )
                      With .FirstRow
                        With .AddColumn("")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.Button(, "Edit", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall,
                                                         , "fa-edit", , Singular.Web.PostBackType.None,
                                                         "SystemManagementPage.editTimesheetRequirement($data)", )
                            .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "ROTimesheetRequirementPagedBO.editButtonCss($data)")
                          End With
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.TimesheetRequirement)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.System)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.TeamName)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.StartDate)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.EndDate)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.RequiredHours)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.RequiredShifts)
                        End With
                        With .AddReadOnlyColumn(Function(d As ROTimesheetRequirementPaged) d.TotalTimesheetHours)
                        End With
                        With .AddColumn("Timesheets")
                          .Style.Width = "70px"
                          With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Warning, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                         "fa-group", , Singular.Web.PostBackType.None,
                                                         , )
                            .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "$data.TotalTimesheets()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                        With .AddColumn("Status")
                          .Style.Width = "50px"
                          With .Helpers.Span
                            .AddBinding(Singular.Web.KnockoutBindingString.text, "$data.ClosedDescriptionShort()")
                            .AddBinding(Singular.Web.KnockoutBindingString.title, "$data.ClosedDescriptionLong()")
                            .AddBinding(Singular.Web.KnockoutBindingString.css, "ROTimesheetRequirementPagedBO.statusButtonCss($data)")
                          End With
                        End With
                        With .AddColumn("")
                          .Style.Width = "50px"
                          With .Helpers.Bootstrap.Button(, "Run", Singular.Web.BootstrapEnums.Style.Info, ,
                                                         Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, ,
                                                         "fa-gears", , Singular.Web.PostBackType.None,
                                                         "ROTimesheetRequirementPagedBO.runTimesheetRequirement($data)", )
                            .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "ROTimesheetRequirementPagedBO.runButtonCss($data)")
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "!$data.IsClosed()")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .AddTab("FreelancerRates", "fa-bar-chart", "", "Freelancer Rates", , )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Rates', ViewModel)")
              .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Rates', ViewModel)")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate)("$data.SystemProductionAreaFreelancerRateList()", True, True, False, True, True, True, True, "SystemProductionAreaFreelancerRateList", , )
                        .AddClass("no-border hover list")
                        With .FirstRow
                          'With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.SystemProductionAreaID)
                          'End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.DisciplineID)
                            .Editor.Attributes("placeholder") = "All Disciplines"
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.EffectiveDate)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.WeekdayRate)
                            .Style.TextAlign = Singular.Web.TextAlign.right
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.WeekendRate)
                            .Style.TextAlign = Singular.Web.TextAlign.right
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaFreelancerRate) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With

              End With
            End With
            'spplit         
            With .AddTab("ShiftAllowances", "fa-credit-card-alt", , "Shift Allowances", , )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Allowances', ViewModel)")
              .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Allowances', ViewModel)")
              With .TabPane
                With .Helpers.Bootstrap.Row
                  With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                    With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting)("$data.SystemProductionAreaShiftAllowanceSettingList()", True, True, False, True, True, True, True, "SystemProductionAreaShiftAllowanceSettingList", , )
                      With .FirstRow
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.ShiftAllowanceTypeID)
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.EffectiveDate)
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn("Qualifying Start Time Start")
                          With .Helpers.TimeEditorFor(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.QualifyingStartTimeStart)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn("Qualifying Start Time End")
                          With .Helpers.TimeEditorFor(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.QualifyingStartTimeEnd)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn("Qualifying End Time Start")
                          With .Helpers.TimeEditorFor(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.QualifyingEndTimeStart)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn("Qualifying End Time End")
                          With .Helpers.TimeEditorFor(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.QualifyingEndTimeEnd)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.Ranking)
                          .Style.TextAlign = Singular.Web.TextAlign.right
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.AllowanceAmount)
                          .Style.TextAlign = Singular.Web.TextAlign.right
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.LoginName)
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSetting) d.ModifiedDateTime)
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .AddTab("ICR Rates", "fa-credit-card-alt", "", "ICR Rates", , )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('ICR Rates', ViewModel)")
              .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('ICR Rates', ViewModel)")
              With .TabPane
                With .Helpers.Bootstrap.PullRight
                  With .Helpers.Bootstrap.InputGroup(Singular.Web.BootstrapEnums.InputGroupSize.Small)
                    'With .Helpers.HTMLTag("span")
                    '  .AddClass("input-group-btn")
                    '  .Style.Width = "30px"
                    '  With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                    '                                 Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-double-left", , ,
                    '                                 "SmsPage.firstPage()")
                    '    .Button.AddClass("btn-flat btn-no-radius no-border-right")
                    '  End With
                    'End With
                    With .Helpers.HTMLTag("span")
                      .AddClass("input-group-btn")
                      .Style.Width = "30px"
                      With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                     Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-left", , ,
                                                     "SystemManagementPage.previousPageICRRates()")
                        .Button.AddClass("btn-flat btn-no-radius no-border-right")
                      End With
                    End With
                    'With .Helpers.HTMLTag("input")
                    '  .AddBinding(Singular.Web.KnockoutBindingString.value, "ViewModel.SmsSearchListManager().PageNo()")
                    '  .AddClass("form-control input-no-radius no-border-right text-center")
                    '  .Style.Width = "60px"
                    'End With
                    'With .Helpers.HTMLTag("input")
                    '  .AddBinding(Singular.Web.KnockoutBindingString.value, "' of ' + ViewModel.SmsSearchListManager().Pages().toString()")
                    '  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                    '  .AddClass("form-control input-no-radius no-border-right text-center")
                    '  .Style.Width = "60px"
                    'End With
                    With .Helpers.HTMLTag("span")
                      .AddClass("input-group-btn")
                      .Style.Width = "30px"
                      With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                                                     Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-right", , ,
                                                     "SystemManagementPage.nextPageICRRates()")
                        .Button.AddClass("btn-flat btn-no-radius no-border-right")
                      End With
                    End With
                    'With .Helpers.HTMLTag("span")
                    '  .AddClass("input-group-btn")
                    '  .Style.Width = "30px"
                    '  With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.DefaultStyle, ,
                    '                                 Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-angle-double-right", , ,
                    '                                 "SmsPage.lastPage()")
                    '    .Button.AddClass("btn-flat btn-no-radius")
                    '  End With
                    'End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                    With .Helpers.Bootstrap.Button(, "Add New", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                                                   "fa-plus", , Singular.Web.PostBackType.None, "SystemProductionAreaBO.newSystemAreaDisciplineShiftRate($data)")
                    End With
                    'With .Helpers.Bootstrap.Button(, "Add New", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.Small, ,
                    '                               "fa-plus", , Singular.Web.PostBackType.None, "SystemProductionAreaBO.newSystemAreaDisciplineShiftRate($data)")
                    'End With
                    With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate)("SystemProductionAreaBO.orderedSystemAreaDisciplineShiftRateList($data)", True, True, False, True, True, True, True, "SystemAreaDisciplineShiftRateList", , )
                      With .FirstRow
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.HumanResourceID)
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.DisciplineID)
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.EffectiveStartDate)
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.Rate)
                          .Style.TextAlign = Singular.Web.TextAlign.right
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.StartTime)
                          .Editor.Style.TextAlign = Singular.Web.TextAlign.center
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.EndTime)
                          .Editor.Style.TextAlign = Singular.Web.TextAlign.center
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.LoginName)
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.ModifiedDateTime)
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        With .AddColumn("Paid Hourly?")
                          .Style.Width = "100px"
                          .Style.TextAlign = Singular.Web.TextAlign.center
                          With .Helpers.BootstrapStateButton("", "SystemManagementPage.HourlyRateIndClick($data)", "SystemManagementPage.HourlyRateIndCss($data)", "SystemManagementPage.HourlyRateIndHtml($data)", False)
                            With .Button
                              .Style.Width = "100%"
                            End With
                          End With
                        End With
                        With .AddColumn("Weekend/day")
                          .Style.Width = "100px"
                          .Style.TextAlign = Singular.Web.TextAlign.center
                          With .Helpers.BootstrapStateButton("", "SystemManagementPage.WeekendIndClick($data)", "SystemManagementPage.WeekendIndCss($data)", "SystemManagementPage.WeekendIndHtml($data)", False)
                            With .Button
                              .Style.Width = "100%"
                            End With
                          End With
                        End With
                        With .AddColumn("Extra Shift")
                          .Style.Width = "100px"
                          .Style.TextAlign = Singular.Web.TextAlign.center
                          With .Helpers.BootstrapStateButton("", "SystemManagementPage.ExtraShiftIndClick($data)", "SystemManagementPage.ExtraShiftIndCss($data)", "SystemManagementPage.ExtraShiftIndHtml($data)", False)
                            With .Button
                              .Style.Width = "100%"
                            End With
                          End With
                        End With
                        With .AddColumn("Training")
                          .Style.Width = "100px"
                          .Style.TextAlign = Singular.Web.TextAlign.center
                          With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Maintenance.SystemManagement.SystemAreaDisciplineShiftRate) d.TrainingInd, , , , , , , )
                            With .Button
                              .Style.Width = "100%"
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .AddTab("AutomaticImports", "fa-skype", , "Automatic Imports", )
              .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('AutomaticImports', ViewModel)")
              .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('AutomaticImports', ViewModel)")
              With .TabPane
                With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.TableFor(Of AutomaticImport)("$data.AutomaticImportList()", True, True, True, True, True, True, True,
                                                                           "AutomaticImportList")
                        With .FirstRow
                          With .AddReadOnlyColumn(Function(d As AutomaticImport) d.AutomaticImportID)
                          End With
                          With .AddReadOnlyColumn(Function(d As AutomaticImport) d.SystemProductionAreaID)
                          End With
                          With .AddColumn(Function(d As AutomaticImport) d.StartDate)
                          End With
                          With .AddColumn(Function(d As AutomaticImport) d.EndDate)
                          End With
                          With .AddColumn(Function(d As AutomaticImport) d.SeriesNumber)
                          End With
                          With .AddColumn(Function(d As AutomaticImport) d.RoomID)
                          End With
                        End With
                        With .AddChildTable(Of AutomaticImportCrew)(Function(s As AutomaticImport) s.AutomaticImportCrewList, True, True, True, True, True, True, True, ,
                                                                    "AutomaticImportCrewList")
                          .Style.Width = "75%"
                          .Style.FloatRight()
                          With .FirstRow
                            With .AddColumn(Function(d As AutomaticImportCrew) d.DisciplineID)
                            End With
                            With .AddColumn(Function(d As AutomaticImportCrew) d.PositionID)
                            End With
                            With .AddColumn(Function(d As AutomaticImportCrew) d.PositionTypeID)
                            End With
                            With .AddColumn(Function(d As AutomaticImportCrew) d.HumanResourceID)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            If OBLib.Security.Settings.CurrentUserID = 1 Then
              With .AddTab("Disciplines", " fa-graduation-cap", "", "Disciplines", , )
                .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Disciplines', ViewModel)")
                .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('Disciplines', ViewModel)")
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaDiscipline)("$data.SystemProductionAreaDisciplineList()", True, True, False, True, True, True, True, "SystemProductionAreaDisciplineList", , )
                        With .FirstRow
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDiscipline) d.DisciplineID)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDiscipline) d.DisciplineOrder)
                            .Style.TextAlign = Singular.Web.TextAlign.right
                          End With
                          With .AddColumn("Position Type Required")
                            .Style.Width = "100px"
                            .Style.TextAlign = Singular.Web.TextAlign.center
                            With .Helpers.Bootstrap.StateButton(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDiscipline) d.PositionTypeRequired, , , , , , , )
                              With .Button
                                .Style.Width = "100%"
                              End With
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDiscipline) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDiscipline) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                        With .AddChildTable(Of SystemProductionAreaDisciplinePositionType)(Function(s As SystemProductionAreaDiscipline) s.SystemProductionAreaDisciplinePositionTypeList, True, True, True, False, True, True, True, , "SystemProductionAreaDisciplinePositionTypeList")
                          With .FirstRow
                            With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionType) d.PositionTypeID)
                            End With
                            With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionType) d.PositionID)
                            End With
                            With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionType) d.PositionOrder)
                            End With
                            With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionType) d.IncludeInBookingDescription)
                            End With
                            With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionType) d.PositionShortName)
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .AddTab("CallTimeSettings", "fa-phone-square", "", "Call Time Settings", , )
                .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('CallTimeSettings', ViewModel)")
                .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('CallTimeSettings', ViewModel)")
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting)("$data.SystemProductionAreaCallTimeSettingList()", True, True, False, True, True, True, True, "SystemProductionAreaCallTimeSettingList", , )
                        With .FirstRow
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting) d.EffectiveDate)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting) d.RoomID)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting) d.CallTimeMinutes)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting) d.WrapTimeMinutes)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaCallTimeSetting) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .AddTab("ChannelDefaults", "fa-television", "", "Channel Defaults", , )
                .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('ChannelDefaults', ViewModel)")
                .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('ChannelDefaults', ViewModel)")
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaChannelDefault)("$data.SystemProductionAreaChannelDefaultList()", True, True, False, True, True, True, True, "SystemProductionAreaChannelDefaultList", , )
                        With .FirstRow
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaChannelDefault) d.ChannelID)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaChannelDefault) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaChannelDefault) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .AddTab("AreaSettings", "fa-gears", "", "Area Settings", , )
                .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('AreaSettings', ViewModel)")
                .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('AreaSettings', ViewModel)")
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting)("$data.SystemProductionAreaSettingList()", True, True, False, True, True, True, True, "SystemProductionAreaSettingList", , )
                        With .FirstRow
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.EffectiveDate)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.MaxHoursBeforeApprovalRequired)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.MaxHoursPerShift)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.MinShortfallBetweenShifts)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.MaxBookingGroupTimeMinutes)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.MaxConsecutiveDaysBooked)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaSetting) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .AddTab("AreaStatus", " fa-th", "", "Area Status", , )
                .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('AreaStatus', ViewModel)")
                .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('AreaStatus', ViewModel)")
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaStatus)("$data.SystemProductionAreaStatusList()", True, True, False, True, True, True, True, "SystemProductionAreaStatusList", , )
                        With .FirstRow
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaStatus) d.ProductionAreaStatusID)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaStatus) d.OrderNum)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaStatus) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaStatus) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .AddTab("TimelineTypes", "fa-clock-o", "", "Timeline Types", , )
                .TabLink.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('TimelineTypes', ViewModel)")
                .TabPane.AddBinding(Singular.Web.KnockoutBindingString.visible, "SystemManagementPage.canEdit('TimelineTypes', ViewModel)")
                With .TabPane
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Maintenance.SystemManagement.SystemProductionArea)("ViewModel.SystemProductionArea()")
                      With .Helpers.Bootstrap.TableFor(Of OBLib.Maintenance.SystemManagement.SystemProductionAreaTimelineType)("$data.SystemProductionAreaTimelineTypeList()", True, True, False, True, True, True, True, "SystemProductionAreaTimelineTypeList", , )
                        With .FirstRow
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaTimelineType) d.ProductionTimelineTypeID)
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaTimelineType) d.OrderNum)
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaTimelineType) d.LoginName)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                          With .AddColumn(Function(d As OBLib.Maintenance.SystemManagement.SystemProductionAreaTimelineType) d.ModifiedDateTime)
                            .Style.TextAlign = Singular.Web.TextAlign.center
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If
          End With
        End With
      End With

      h.Control(New NewOBWeb.Controls.EditYearRequirementModal(Of NewOBWeb.SystemManagementVM)())

      With h.Bootstrap.Dialog("CurrentTimesheetRequirement", "Timesheet Requirement", , "modal-lg", Singular.Web.BootstrapEnums.Style.Primary, , "fa-list-o", , )
        With .ContentDiv
          With .Helpers.With(Of TimesheetRequirement)("ViewModel.CurrentTimesheetRequirement()")
            With .Helpers.DivC("loading-custom")
              .AddBinding(KnockoutBindingString.visible, "$data.IsProcessing()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa-refresh fa-spin", "fa-4x")
            End With
          End With
        End With
        With .Body
          .AddClass("modal-background-gray")
          With .Helpers.With(Of TimesheetRequirement)("ViewModel.CurrentTimesheetRequirement()")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                With .Helpers.Bootstrap.FlatBlock("Requirement Criteria", , , , )
                  With .ContentTag
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.TimesheetRequirement).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetRequirement) d.TimesheetRequirement, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'TimesheetRequirement')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.SystemID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetRequirement) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'SystemID')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.StartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetRequirement) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'StartDate')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.EndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetRequirement) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'EndDate')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.RequiredHours).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetRequirement) d.RequiredHours, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'RequiredHours')")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.IsTeamBasedRequirment).Style.Width = "100%"
                          With .Helpers.Bootstrap.StateButtonNew(Function(d As TimesheetRequirement) d.IsTeamBasedRequirment, , , , , , , "btn-sm")
                            .Button.Style.Width = "50%"
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsTeamBasedRequirment()")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As TimesheetRequirement) d.SystemTeamNumberID).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As TimesheetRequirement) d.SystemTeamNumberID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'SystemTeamNumberID')")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsTeamBasedRequirment()")
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.FieldSet("Requirement Detail")
                          .Helpers.Bootstrap.Button(, "Add All", Singular.Web.BootstrapEnums.Style.Warning, ,
                                                    Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-gear", ,
                                                    Singular.Web.PostBackType.None, "TimesheetRequirementBO.addAll($data)", )
                          With .Helpers.Bootstrap.TableFor(Of TimesheetRequirementSetting)("$data.TimesheetRequirementSettingList()",
                                                                                           True, False, False, True, True, True, True,
                                                                                           "TimesheetRequirementSettingList")
                            With .FirstRow
                              With .AddColumn(Function(d As TimesheetRequirementSetting) d.ProductionAreaID)
                              End With
                              With .AddColumn(Function(d As TimesheetRequirementSetting) d.ContractTypeID)
                              End With
                              With .AddColumn("")
                                .HeaderStyle.Width = "50px"
                                .Style.Width = "50px"
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Danger, ,
                                                              Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-trash", ,
                                                              Singular.Web.PostBackType.None, "TimesheetRequirementBO.removeSetting($parent, $data)")
                                End With
                              End With
                              'With .AddColumn(Function(d As TimesheetRequirementSetting) d.DisciplineID)
                              'End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
                With .Helpers.Bootstrap.TabControl(, "nav-tabs", , Singular.Web.BootstrapEnums.TabAlignment.Top)
                  With .AddTab("TimesheetRequirementHR", "fa-group", "SystemManagementPage.refreshTimesheetRequirementHR($data)", "Human Resources", , )
                    With .TabPane
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                          With .Helpers.Bootstrap.TableFor(Of TimesheetRequirementHR)("$data.TimesheetRequirementHRList()", False, False, False, True, True, True, True)
                            .AddClass("hover list")
                            With .FirstRow
                              With .AddReadOnlyColumn(Function(d As TimesheetRequirementHR) d.HRName)
                              End With
                              With .AddReadOnlyColumn(Function(d As TimesheetRequirementHR) d.ContractType)
                              End With
                              With .AddReadOnlyColumn(Function(d As TimesheetRequirementHR) d.IsClosedDateTimeString)
                                .HeaderText = "Status"
                                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) False)
                                With .Helpers.Bootstrap.Button(, , Singular.Web.BootstrapEnums.Style.Custom, ,
                                                               Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "fa-check-square-o", ,
                                                               Singular.Web.PostBackType.None, )
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.css, "TimesheetRequirementHRBO.statusButtonCss($data)")
                                  .ButtonText.AddBinding(Singular.Web.KnockoutBindingString.html, "TimesheetRequirementHRBO.statusButtonText($data)")
                                  .Icon.IconContainer.AddBinding(Singular.Web.KnockoutBindingString.css, "TimesheetRequirementHRBO.statusIconCss($data)")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Footer
          With .Helpers.With(Of TimesheetRequirement)("ViewModel.CurrentTimesheetRequirement()")
            With .Helpers.Bootstrap.PullRight
              With .Helpers.Bootstrap.Button(, "Save", Singular.Web.BootstrapEnums.Style.Success, ,
                                             Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-floppy-o", ,
                                             Singular.Web.PostBackType.None, "SystemManagementPage.saveCurrentTimesheetMonth($data)", )
                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "TimesheetRequirementBO.CanEdit($data, 'SaveButton')")
              End With
            End With
          End With
        End With
      End With

    End Using%>
</asp:Content>
