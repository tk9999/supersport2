﻿Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.Productions.Vehicles
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports System.ComponentModel
Imports OBLib.Productions.Crew.ReadOnly
Imports OBLib.Helpers.Templates
Imports OBLib.Helpers.RuleHelper.ProductionSystemAreaRules
Imports OBLib.Productions.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.Specs.ReadOnly.Old
Imports OBLib.Productions.Schedules
Imports Singular
Imports Singular.Misc
Imports OBLib.Productions
Imports OBLib.HR.ReadOnly
Imports OBLib.Productions.Crew
Imports OBLib.Productions.Areas
Imports OBLib.Biometrics
Imports System


Public Class Production
  Inherits OBPageBase(Of ProductionVM)



End Class

Public Class ProductionVM
  Inherits OBViewModel(Of ProductionVM)

#Region " Properties "

  Public ReadOnly Property CurrentProduction As OBLib.Productions.Old.Production
    Get
      Return SessionData.CurrentProduction
    End Get
  End Property

  Public ReadOnly Property CanAddBeforeTimelineReady As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Add Production Human Resource before Timeline Finalised")
    End Get
  End Property

  Public ReadOnly Property CanUpdateBeforeTimelineReady As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Update Production Human Resource before Timeline Finalised")
    End Get
  End Property


  Public ReadOnly Property CanDeleteBeforeTimelineReady As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Delete Production Human Resource before Timeline Finalised")
    End Get
  End Property

  Public ReadOnly Property CanIgnoreProductionRules As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Ignore Production rules before Timeline Finalised")
    End Get
  End Property


  Public ReadOnly Property CanFinaliseTimelineInd As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Finalise Timeline ")
    End Get
  End Property

  Public ReadOnly Property CanUnFinaliseTimelineInd As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Unfinalise Timeline")
    End Get
  End Property

  Public ReadOnly Property IncludeBiometricDetails As Boolean
    Get
      If CurrentProduction IsNot Nothing Then
        If CurrentProduction.GetTxStartTime < Now Then
          Return True
        End If
      End If
      Return False
    End Get
  End Property


#Region " Main Details "

  Public ReadOnly Property SystemID As Integer?
    Get
      Return OBLib.Security.Settings.CurrentUser.SystemID
    End Get
  End Property

  Public ReadOnly Property UserTypeID As Integer?
    Get
      Return OBLib.Security.Settings.CurrentUser.UserTypeID
    End Get
  End Property

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property MainTabVisible As Boolean = True

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ProductionTypeFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property EventTypeFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property VenueFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROHumanResourceFilter As String = ""

  Private mROHumanResourceList As ROHumanResourceList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROHumanResourceList As ROHumanResourceList
    Get
      Return mROHumanResourceList
    End Get
  End Property

  Public Property ProductionHRPlannerGUID As Guid

  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public Property SpecFilter As String = ""

  <ClientOnly()>
  Public Property CurrentUserID As Integer = OBLib.Security.Settings.CurrentUserID

  <ClientOnly()>
  Public Property CurrentUserName As String = OBLib.Security.Settings.CurrentUser.LoginName

  Private mROProductionTypeList As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList
  <ClientOnly>
  Public ReadOnly Property ROProductionTypeList As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList
    Get
      Return mROProductionTypeList
    End Get
  End Property

  Private mROEventTypeList As OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList
  <ClientOnly>
  Public ReadOnly Property ROEventTypeList As OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList
    Get
      Return mROEventTypeList
    End Get
  End Property

  Private mROProductionVenueFullList As ROProductionVenueFullList
  <ClientOnly>
  Public ReadOnly Property ROProductionVenueFullList As ROProductionVenueFullList
    Get
      Return mROProductionVenueFullList
    End Get
  End Property

  Private mROProductionSpecRequirementList As ROProductionSpecRequirementListOld
  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public ReadOnly Property ROProductionSpecRequirementList As ROProductionSpecRequirementList
  '  Get
  '    Return mROProductionSpecRequirementList
  '  End Get
  'End Property

  Private mROOBProductionRequirementList As OBLib.Productions.ReadOnly.ROOBProductionRequirementList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROOBProductionRequirementList As OBLib.Productions.ReadOnly.ROOBProductionRequirementList
    Get
      Return mROOBProductionRequirementList
    End Get
  End Property

#End Region

#Region " Requirements "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property RequirementsTabVisible As Boolean = False

#End Region

#Region " Outsource Services "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property OutsourceServicesTabVisible As Boolean = False

#End Region

#Region " Production Vehicles "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ProductionVehiclesTabVisible As Boolean = False

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectVehicleVisible As Boolean = False

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property VehicleListVisible As Boolean = True

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ROVehicleFilter As String = ""

#End Region

#Region " Areas & Timelines "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectedPTID As Integer? = Nothing

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property TimelinesTabVisible As Boolean = False

  <ClientOnly()>
  Public Property ProductionTimelineClient As ProductionTimelineClient

#End Region

#Region " Production Crew "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ProductionCrewTabVisible As Boolean = False

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property PHRListVisible As Boolean = True

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectedHRVisible As Boolean = False

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectedPHRID As Integer? = Nothing

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectedPHRGuid As String = ""

  '<ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  'Public Property SearchResultsFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property PHRFilter As String = ""

  Public Property ROSkilledHRList As ROSkilledHRList
  Public Property ROSkilledHRListCriteria As ROSkilledHRList.Criteria
  Public Property ROSkilledHRListPagingInfo As Singular.Web.Data.PagedDataManager(Of ProductionVM)

#End Region

#Region " Schedules "

  '<ClientOnly()>
  'Public Property CurrentHRSchedule As OBLib.Productions.Schedules.HRProductionSchedule
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectedHRScheduleGuid As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SchedulesTabVisible As Boolean = False

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property HRListVisible As Boolean = True

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property HRTimelineSelectVisible As Boolean = False

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property HRSelectFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property TimelineSelectFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property HRListFilter As String = ""

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property CurrentHRScheduleVisible As Boolean = False

  <ClientOnlyNoData()>
  Public Property HRTimelineSelect As HRTimelineSelect

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property SelectedHRSGuid As String = ""

#End Region

#Region " Correspondence "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property CorrespondenceTabVisible As Boolean = False

#End Region

#Region " Production Comments "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property CommentSectionTabVisible As Boolean = False

#End Region

#Region " Production Audio "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property AudioSectionTabVisible As Boolean = False

#End Region

#Region " Travel "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property TravelTabVisible As Boolean = False

#End Region

#Region " Reports "

  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public Property ReportsTabVisible As Boolean = False

#End Region

  Public Property BulkEditDetails As OBLib.Helpers.Templates.ProductionHumanResourceBulkEditDetails = Nothing

  Public Property BulkEditMode As String = ""

  Public Property CurrentSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID
  Public Property CurrentProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

  'Public ReadOnly Property HasFinaliseCrewAccess
  '  Get
  '    Return Singular.Security.HasAccess("Productions", "Finalise Crew")
  '  End Get
  'End Property

  'Public ReadOnly Property HasPlanningFinalisedAccess
  '  Get
  '    Return Singular.Security.HasAccess("Productions", "Finalise Production")
  '  End Get
  'End Property

  'Public ReadOnly Property HasUnCancelProduction
  '  Get
  '    Return Singular.Security.HasAccess("Productions", "Un-cancel productions")
  '  End Get
  'End Property

  'Public ReadOnly Property HasCancelProduction
  '  Get
  '    Return Singular.Security.HasAccess("Productions", "Cancel Production")
  '  End Get
  'End Property

  Public Property CopyCrewProductionCrewList As ProductionCrewList
  Private mROProductionList As ROProductionList
  Public ReadOnly Property ROProductionList As ROProductionList
    Get
      Return mROProductionList
    End Get
  End Property
  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of ProductionVM)

  Public ReadOnly Property CanEditBuildUpForAllAreas As Boolean
    Get
      Return Singular.Security.HasAccess("Productions", "Can Edit Build-Up For All Areas")
    End Get
  End Property


#End Region


#Region " Data "

  Private mRODisciplineList As RODisciplineList
  <Browsable(False)>
  Public ReadOnly Property RODisciplineList As RODisciplineList
    Get
      Return mRODisciplineList
    End Get
  End Property

  Private mROPositionList As ROPositionList
  <Browsable(False)>
  Public ReadOnly Property ROPositionList As ROPositionList
    Get
      Return mROPositionList
    End Get
  End Property

  Private mROPositionTypeList As ROPositionTypeList
  <Browsable(False)>
  Public ReadOnly Property ROPositionTypeList As ROPositionTypeList
    Get
      Return mROPositionTypeList
    End Get
  End Property

  Private mROVehicleSelectList As ROVehicleOldList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ROVehicleSelectList As ROVehicleOldList
    Get
      Return mROVehicleSelectList
    End Get
  End Property

  Private mROProductionAreaList As ROProductionAreaList
  <Browsable(False)>
  Public ReadOnly Property ROProductionAreaList As ROProductionAreaList
    Get
      Return mROProductionAreaList
    End Get
  End Property

  Private mProductionVehicleList As ProductionVehicleList
  <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
  Public ReadOnly Property ProductionVehicleList As ProductionVehicleList
    Get
      Return mProductionVehicleList
    End Get
  End Property

  Private mROProductionAreaAllowedTimelineTypeList As ROProductionAreaAllowedTimelineTypeList
  <Browsable(False)>
  Public ReadOnly Property ROProductionAreaAllowedTimelineTypeList As ROProductionAreaAllowedTimelineTypeList
    Get
      Return mROProductionAreaAllowedTimelineTypeList
    End Get
  End Property

  Private mROSystemList As OBLib.Maintenance.ReadOnly.ROSystemList
  <Browsable(False)>
  Public ReadOnly Property ROSystemList As OBLib.Maintenance.ReadOnly.ROSystemList
    Get
      Return mROSystemList
    End Get
  End Property

  Private mROCrewTypeList As OBLib.Maintenance.General.ReadOnly.ROCrewTypeList
  <Browsable(False)>
  Public ReadOnly Property ROCrewTypeList As OBLib.Maintenance.General.ReadOnly.ROCrewTypeList
    Get
      Return mROCrewTypeList
    End Get
  End Property

  Private mROProductionDisciplineTimelineTypeList As OBLib.Productions.Areas.ReadOnly.ROProductionDisciplineTimelineTypeList
  <Browsable(False)>
  Public ReadOnly Property ROProductionDisciplineTimelineTypeList As OBLib.Productions.Areas.ReadOnly.ROProductionDisciplineTimelineTypeList
    Get
      Return mROProductionDisciplineTimelineTypeList
    End Get
  End Property

  Private mROOutsourceServiceTypeList As OBLib.Maintenance.General.ReadOnly.ROOutsourceServiceTypeList
  <Browsable(False)>
  Public ReadOnly Property ROOutsourceServiceTypeList As OBLib.Maintenance.General.ReadOnly.ROOutsourceServiceTypeList
    Get
      Return mROOutsourceServiceTypeList
    End Get
  End Property

  Private mROProductionCommentHeaderList As ROProductionCommentHeaderList
  <Browsable(False)>
  Public ReadOnly Property ROProductionCommentHeaderList As ROProductionCommentHeaderList
    Get
      Return mROProductionCommentHeaderList
    End Get
  End Property

  'Private mROProductionCommentList As ROProductionCommentList
  '<Browsable(False)>
  'Public ReadOnly Property ROProductionCommentList As ROProductionCommentList
  '  Get
  '    Return mROProductionCommentList
  '  End Get
  'End Property

  Public Property ROProductionCommentList As ROProductionCommentList

#Region " Biometrics "
  Public Property AccessMissingShiftList() As OBLib.Biometrics.AccessShiftList

  Public ReadOnly Property MissingShiftsCount As Integer
    Get
      If AccessMissingShiftList Is Nothing Then
        Return 0
      End If
      Return AccessMissingShiftList.Count
    End Get
  End Property

#End Region

#End Region


#Region " Overrides "

  Protected Overrides Sub Setup()
    MyBase.PreSetup()
    ValidationMode = Singular.Web.ValidationMode.OnLoad
    'MessageFadeTime = 1000
    If OBLib.Security.Settings.CurrentUser.ProductionAreaID = CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer) Then
      If Page.Request.QueryString("New") IsNot Nothing AndAlso Singular.Security.HasAccess("Productions", "Can Create New Production") Then
        'SessionData.CurrentProduction = New OBLib.Productions.Old.Production
        'SessionData.CurrentProduction.AddNewSystemArea(OBLib.Security.Settings.CurrentUser.SystemID, CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer))
        'SessionData.CurrentProduction.AddNewTravelRequisition(OBLib.Security.Settings.CurrentUser.SystemID)
      ElseIf Page.Request.QueryString("P") IsNot Nothing Then
        If IsNumeric(Page.Request.QueryString("P")) Then
          Dim ProductionID As Integer = Page.Request.QueryString("P")
          Dim pl As OBLib.Productions.Old.ProductionList = OBLib.Productions.Old.ProductionList.GetProductionList(ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID)
          If pl.Count = 1 Then
            SessionData.CurrentProduction = pl(0)

          End If
        End If
      End If
    End If

    If SessionData.CurrentProduction IsNot Nothing Then
      mRODisciplineList = OBLib.Maintenance.General.ReadOnly.RODisciplineList.GetRODisciplineList(SessionData.CurrentProduction.ProductionSystemAreaList(0).SystemID)
      mROPositionList = OBLib.Maintenance.General.ReadOnly.ROPositionList.GetROPositionList(SessionData.CurrentProduction.ProductionSystemAreaList(0).SystemID)
      mROPositionTypeList = OBLib.Maintenance.General.ReadOnly.ROPositionTypeList.GetROPositionTypeList(SessionData.CurrentProduction.ProductionTypeID)
      mROSystemList = OBLib.Maintenance.ReadOnly.ROSystemList.GetROSystemList
      mROProductionAreaList = OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList.GetROProductionAreaList()
      'mROAllowedAreaStatusList = OBLib.Maintenance.Productions.Areas.ReadOnly.ROAllowedAreaStatusList.GetROAllowedAreaStatusList(SessionData.CurrentProduction.ProductionSystemAreaList(0).SystemID, SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionAreaID, SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionAreaStatusID)
      mROProductionAreaAllowedTimelineTypeList = OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedTimelineTypeList.GetROProductionAreaAllowedTimelineTypeList()
      'mROPoductionRoomList = OBLib.Productions.Rooms.ReadOnly.ROProductionRoomList.GetROProductionRoomList(SessionData.CurrentProduction.ProductionID)
      mROCrewTypeList = OBLib.Maintenance.General.ReadOnly.ROCrewTypeList.GetROCrewTypeList
      mROProductionDisciplineTimelineTypeList = OBLib.Productions.Areas.ReadOnly.ROProductionDisciplineTimelineTypeList.GetROProductionDisciplineTimelineTypeList(SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionID, SessionData.CurrentProduction.ProductionSystemAreaList(0).SystemID, SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaID)
      mProductionVehicleList = New ProductionVehicleList
      mROProductionTypeList = New OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList 'OBLib.CommonData.Lists.ROProductionTypeList
      mROEventTypeList = New OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList 'OBLib.CommonData.Lists.ROEventTypeList
      mROProductionVenueFullList = New ROProductionVenueFullList 'OBLib.CommonData.Lists.ROProductionVenueFullList
      mROProductionSpecRequirementList = OBLib.Productions.Specs.ReadOnly.Old.ROProductionSpecRequirementListOld.GetROProductionSpecRequirementListOld(OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      mROProductionCommentHeaderList = OBLib.Productions.ReadOnly.ROProductionCommentHeaderList.GetROProductionCommentHeaderList 'New ROProductionCommentHeaderList '
      ROProductionCommentList = New ROProductionCommentList
      'ROProductionCommentList = OBLib.Productions.ReadOnly.ROProductionCommentList.GetROProductionCommentList

      ROSkilledHRList = New ROSkilledHRList
      ROSkilledHRListCriteria = New ROSkilledHRList.Criteria
      ROSkilledHRListPagingInfo = New Singular.Web.Data.PagedDataManager(Of ProductionVM)(Function(d) Me.ROSkilledHRList, Function(d) Me.ROSkilledHRListCriteria, "HumanResource", 25)
      'ROSkilledHRListCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
      'ROSkilledHRListCriteria.EndDate = Singular.Dates.DateMonthEnd(Now)
      ROSkilledHRListCriteria.SortColumn = "HumanResource"
      ROSkilledHRListCriteria.PageNo = 1
      ROSkilledHRListCriteria.PageSize = 25

      mROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
      ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
      ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of ProductionVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "TxStartDateTime", 20)
      If CurrentProduction.PlayStartDateTime IsNot Nothing _
        AndAlso CurrentProduction.PlayEndDateTime IsNot Nothing Then
        ROProductionListCriteria.TxDateFrom = CurrentProduction.PlayStartDateTime.Value.Date.AddDays(-3)
        ROProductionListCriteria.TxDateTo = CurrentProduction.PlayEndDateTime.Value.Date.AddDays(3)
      Else
        ROProductionListCriteria.TxDateFrom = Now.Date.AddDays(-7)
        ROProductionListCriteria.TxDateTo = Now.Date.AddDays(7)
      End If
      ROProductionListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
      ROProductionListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
      ROProductionListCriteria.PageNo = 1
      ROProductionListCriteria.PageSize = 20
      ROProductionListCriteria.SortAsc = True
      ROProductionListCriteria.SortColumn = "TxStartDateTime"
      CopyCrewProductionCrewList = New ProductionCrewList

      ClientDataProvider.AddDataSource("ROSystemList", OBLib.CommonData.Lists.ROSystemList, False)
      ClientDataProvider.AddDataSource("RODisciplineList", mRODisciplineList, False)
      ClientDataProvider.AddDataSource("ROPositionList", mROPositionList, False)
      ClientDataProvider.AddDataSource("ROPositionTypeList", mROPositionTypeList, False)
      ClientDataProvider.AddDataSource("ROProductionAreaList", OBLib.CommonData.Lists.ROProductionAreaList, False)
      ClientDataProvider.AddDataSource("ROProductionAreaAllowedStatusList", OBLib.CommonData.Lists.ROProductionAreaAllowedStatusList, False)
      ClientDataProvider.AddDataSource("ROProductionAreaAllowedTimelineTypeList", OBLib.CommonData.Lists.ROProductionAreaAllowedTimelineTypeList, False)
      ClientDataProvider.AddDataSource("ROCrewTypeList", mROCrewTypeList, False)
      ClientDataProvider.AddDataSource("ROOutsourceServiceTypeList", OBLib.CommonData.Lists.ROOutsourceServiceTypeList, False)
      ClientDataProvider.AddDataSource("ROEquipmentTypeList", OBLib.CommonData.Lists.ROEquipmentTypeList, False)
      ClientDataProvider.AddDataSource("ROEquipmentSubTypeList", OBLib.CommonData.Lists.ROEquipmentSubTypeList, False)
      ClientDataProvider.AddDataSource("ROSupplierList", OBLib.CommonData.Lists.ROSupplierList, False)
      ClientDataProvider.AddDataSource("RODocumentTypeList", OBLib.CommonData.Lists.RODocumentTypeList, False)
      ClientDataProvider.AddDataSource("ROProductionPettyCashTypeList", OBLib.CommonData.Lists.ROProductionPettyCashTypeList, False)
      ClientDataProvider.AddDataSource("ROProductionCommentHeaderList", mROProductionCommentHeaderList, False)
      ClientDataProvider.AddDataSource("ROVehicleList", OBLib.CommonData.Lists.ROVehicleList, False)
      ClientDataProvider.AddDataSource("ROProductionTimelineTypeList", OBLib.CommonData.Lists.ROProductionTimelineTypeList, False)
      ClientDataProvider.AddDataSource("ROProductionSpecRequirementList", mROProductionSpecRequirementList, True)
      ClientDataProvider.AddDataSource("AudioConfigurationList", OBLib.Maintenance.AudioConfigurationList.GetAudioConfigurationList, False)
      ClientDataProvider.AddDataSource("ROProductionTypeList", OBLib.CommonData.Lists.ROProductionTypeList, False)
      ClientDataProvider.AddDataSource("ROEventTypeList", OBLib.CommonData.Lists.ROEventTypeList, False)
      ClientDataProvider.AddDataSource("ROProductionVenueList", OBLib.CommonData.Lists.ROProductionVenueList, False)

      Dim mROSystemAreaShiftTypeList As OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList = OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList.GetROSystemAreaShiftTypeList(OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ClientDataProvider.AddDataSource("ROSystemAreaShiftTypeList", mROSystemAreaShiftTypeList, False)
      If CurrentProduction.ProductionSystemAreaList.Count > 0 Then
        ClientDataProvider.AddDataSource("HRProductionScheduleList", CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList, False)
      End If
      CheckScheduleTimeCorrect()

    Else
      'TODO: add error about redirecting
      Page.Response.Redirect("~/OutsideBroadcast/FindOB.aspx")
    End If

  End Sub

  Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
    MyBase.HandleCommand(Command, CommandArgs)

    Select Case Command

      Case "Save"
        'Check everything before saving
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = ""
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        'CheckAllVehicles()
        'Only save if still valid
        If SessionData.CurrentProduction.IsValid Then
          Dim pl As New OBLib.Productions.Old.ProductionList
          pl.Add(SessionData.CurrentProduction)
          Dim sh As SaveHelper = TrySave(pl)
          If Not sh.Success Then
            'Error
            AddMessage(Singular.Web.MessageType.Error, "Error During Save", sh.ErrorText)
          Else
            'Success
            SessionData.CurrentProduction = CType(sh.SavedObject, OBLib.Productions.Old.ProductionList)(0)
            SessionData.CurrentProduction.DoPostSave()
            Dim CancelledTravelNames As String = SessionData.CurrentProduction.CancelledTravelHRNames
            SessionData.CurrentProduction = OBLib.Productions.Old.ProductionList.GetProductionList(SessionData.CurrentProduction.ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.CommonData.Enums.ProductionArea.OB).FirstOrDefault()
            SessionData.CurrentProduction.CancelledTravelHRNames = CancelledTravelNames
          End If
        End If
        WebsiteHelpers.SendResourceBookingUpdateNotifications()
        CommandArgs.TransferModelToClient = True

      Case "ReloadProduction"
        SessionData.CurrentProduction = OBLib.Productions.Old.ProductionList.GetProductionList(SessionData.CurrentProduction.ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.CommonData.Enums.ProductionArea.OB).FirstOrDefault()

      Case "UnReconcile"
        SessionData.CurrentProduction.Reconciled = False
        SessionData.CurrentProduction.ReconciledBy = Nothing
        SessionData.CurrentProduction.ReconciledDate = Nothing
        SessionData.CurrentProduction.ReconciledSetDate = Nothing

        'Main Tab------------------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------
      Case "PopulateProductionSpec"
        CurrentProduction.ProductionSystemAreaList(0).ClearProductionSpec()
        CurrentProduction.ProductionSystemAreaList(0).PopulateProductionSpec(CommandArgs.ClientArgs.ProductionSpecRequirementID)
        CurrentProduction.ProductionSystemAreaList(0).CheckNotComplySpecReasonsValid()

      Case "SetupAutoServices"
        CurrentProduction.ProductionSystemAreaList(0).SetupAutoOutsourceServices()
        CurrentProduction.ProductionSystemAreaList(0).CheckNotComplySpecReasonsValid()


        'Requirements Tab----------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------
      Case "CheckSpecValid"
        CurrentProduction.ProductionSystemAreaList(0).CheckNotComplySpecReasonsValid()


        'Vehicles Tab--------------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------
      Case "AddVehicle"
        'Do not add HR automatically, this must be done via "Populate HR"
        CurrentProduction.ProductionVehicleList.AddNewVehicle(CurrentProduction, CommandArgs.ClientArgs.VehicleID)
        SessionData.CurrentProduction.CalculateProductionHR(False)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()

      Case "RemoveVehicle"
        SessionData.CurrentProduction.CalculateProductionHR(False)
        CurrentProduction.RemoveVehicle(CommandArgs.ClientArgs.VehicleID)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()

      Case "CheckVehiclesValid"
        CheckAllVehicles()


        'Areas & Timelines---------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------
      Case "AddNewTimeline"
        'check vehicle, check hr schedule
        SessionData.CurrentProduction.ProductionSystemAreaList(0).AddNewTimeline()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = ""
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).SetupAllUnSelectedTimelines()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()

      Case "RemoveTimeline"
        'check vehicle, check hr schedule
        SessionData.CurrentProduction.ProductionSystemAreaList(0).RemoveTimeline(CommandArgs.ClientArgs.Guid)
        SessionData.CurrentProduction.CalculateProductionHR(False)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = ""
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).SetupAllUnSelectedTimelines()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()

      Case "CheckTimelines"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = ""
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()

      Case "CheckSchedules"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = ""
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()
        'CommandArgs.TransferModelToClient = True

      Case "CheckScheduleSingle"
        Dim f As Object
        'SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckScheduleValidOld(CommandArgs.ClientArgs.HumanResourceID)

      Case "SelectStatus"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionAreaStatusID = CommandArgs.ClientArgs.ProductionAreaStatusID

      Case "CheckAll"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = ""
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ProductionSystemAreaClashes = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()
        CommandArgs.TransferModelToClient = True
        'Outsource Services--------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------


        'Production Crew-----------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------
      Case "AddNewProductionHumanResource"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).AddNewProductionHumanResource()
        'SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllClashes()

      Case "DeleteProductionHumanResource"
        Dim GuidString As String = CommandArgs.ClientArgs.Guid
        SessionData.CurrentProduction.ProductionSystemAreaList(0).DeleteProductionHumanResource(New Guid(GuidString))
        SessionData.CurrentProduction.CalculateProductionHR(False)
        'SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllClashes()
        CheckAllVehicles()

      Case "ClearHumanResourceID"
        Dim GuidString As String = CommandArgs.ClientArgs.Guid
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ClearHumanResourceID(New Guid(GuidString))
        SessionData.CurrentProduction.CalculateProductionHR(False)
        'SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllClashes()
        CheckAllVehicles()

      Case "ClearPrefHumanResourceID"
        Dim t As Object

      Case "SetHumanResourceID"
        Dim GuidString As String = CommandArgs.ClientArgs.Guid
        SessionData.CurrentProduction.ProductionSystemAreaList(0).SetHumanResourceID(New Guid(GuidString), CommandArgs.ClientArgs.OldHumanResourceID, CommandArgs.ClientArgs.NewHumanResourceID,
                                                                                     CommandArgs.ClientArgs.NewHumanResource, CommandArgs.ClientArgs.DisciplineID, CommandArgs.ClientArgs.PositionID)
        SessionData.CurrentProduction.CalculateProductionHR(False)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability(CommandArgs.ClientArgs.NewHumanResourceID)
        CheckAllVehicles()

      Case "PopulateHumanResources"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).PopulateHumanResources(CurrentProduction.ProductionSystemAreaList(0))
        SessionData.CurrentProduction.CalculateProductionHR(True)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).SetupAllUnSelectedTimelines()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        CheckAllVehicles()

        'HR Schedules-------------------------------------------------------------------------------
        '--------------------------------------------------------------------------------------------
      Case "GenerateSchedules"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).GenerateAllProductionSchedules()
        SessionData.CurrentProduction.CalculateProductionHR(True)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAllPreAndPostTravel()
        SessionData.CurrentProduction.ProductionSystemAreaList(0).SetupAllUnSelectedTimelines()
        CheckAllVehicles()

      Case "GetSchedule"
        Dim z As Object
        'Dim AreaSchedule As OBLib.Productions.Schedules.HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(CommandArgs.ClientArgs.HumanResourceID)
        'AreaSchedule.HRProductionScheduleDetailListClient.Clear()
        'AreaSchedule.CheckPreAndPostTravel()
        'AreaSchedule.SetupUnSelectedTimelines()
        'SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckScheduleValidOld(AreaSchedule.HumanResourceID)
        'AreaSchedule.HRProductionScheduleDetailList.ToList.ForEach(Sub(Item)
        '                                                             AreaSchedule.HRProductionScheduleDetailListClient.Add(Item)
        '                                                           End Sub)
      Case "AddScheduledItem"
        'CurrentHRSchedule.HRProductionScheduleDetailList.RemoveScheduledItem(CommandArgs.ClientArgs.Guid)

      Case "RemoveScheduledItem"
        Dim x As Object
        'CurrentHRSchedule.HRProductionScheduleDetailList.RemoveScheduledItem(CommandArgs.ClientArgs.Guid)
        'CurrentHRSchedule.CheckClashes()

      Case "CheckCurrentHRScheduleValid"
        Dim CurrentHRSchedule As OBLib.Productions.Schedules.HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(CommandArgs.ClientArgs.HumanResourceID)
        OBLib.Helpers.ProductionSchedules.CheckProductionAvailabilityOB(CurrentProduction.ProductionSystemAreaList(0), CurrentHRSchedule.HumanResourceID)

      Case "SelectTimeline"
        HRTimelineSelect = New HRTimelineSelect(SessionData.CurrentProduction.ProductionSystemAreaList(0), CommandArgs.ClientArgs.Mode, CommandArgs.ClientArgs.HumanResourceID)
        SessionData.CurrentProduction.CalculateProductionHR(True)

      Case "UpdateFromHRTimelineSelect"
        Dim y As Object
        'SessionData.CurrentProduction.ProductionSystemAreaList(0).UpdateFromHRTimelineSelect(HRTimelineSelect)

      Case "GenerateComments"
        CurrentProduction.AutoAddTemplateCommentItems()

        'Reports
      Case "Schedule"
        If SessionData.CurrentProduction IsNot Nothing Then
          Dim rpt As New OBWebReports.ProductionReports.ProductionReport
          rpt.ReportCriteria.ProductionID = SessionData.CurrentProduction.ProductionID
          Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          SendFile(rfi.FileName, rfi.FileBytes)
        End If

      Case "SignIn"
        If SessionData.CurrentProduction IsNot Nothing Then
          If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
            Dim rpt As New OBWebReports.ProductionReports.ProductionPAForm
            rpt.ReportCriteria.ProductionID = SessionData.CurrentProduction.ProductionID
            Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
            SendFile(rfi.FileName, rfi.FileBytes)

          ElseIf CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then
            Dim rpt As New OBWebReports.ProductionReports.CommentatorProductionPAForm
            rpt.ReportCriteria.ProductionID = SessionData.CurrentProduction.ProductionID
            Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
            SendFile(rfi.FileName, rfi.FileBytes)

          End If
        End If

      Case "SignOut"
        If SessionData.CurrentProduction IsNot Nothing Then
          Dim rpt As New OBWebReports.ProductionReports.ProductionDerigForm
          rpt.ReportCriteria.ProductionID = SessionData.CurrentProduction.ProductionID
          rpt.ReportCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
          Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          SendFile(rfi.FileName, rfi.FileBytes)
        End If

      Case "SnT"
        If SessionData.CurrentProduction IsNot Nothing Then
          Dim rpt As New OBWebReports.FinanceReports.ProductionSnTReport
          rpt.ReportCriteria.ProductionID = SessionData.CurrentProduction.ProductionID
          'rpt.ReportCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
          'rpt.ReportCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
          Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.ExcelData)
          SendFile(rfi.FileName, rfi.FileBytes)
        End If


      Case "Cost"
        If SessionData.CurrentProduction IsNot Nothing Then
          Dim rpt As New OBWebReports.FinanceReports.ProductionCostReport
          rpt.ReportCriteria.ProductionID = SessionData.CurrentProduction.ProductionID
          Dim rfi = rpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
          SendFile(rfi.FileName, rfi.FileBytes)
        End If

      Case "EditTravel"
        If CommandArgs.ClientArgs.ProductionID IsNot Nothing Then
          If IsNumeric(CommandArgs.ClientArgs.ProductionID) Then
            'Dim TravelRequisitionID As Integer = CommandArgs.ClientArgs.TravelRequisitionID
            ''OBLib.Security.Settings.CurrentUser.ProductionAreaID
            'Dim trl As OBLib.Travel.TravelRequisitionList = OBLib.Travel.TravelRequisitionList.GetTravelRequisitionList(TravelRequisitionID, Nothing, Nothing, Nothing, Nothing, True)
            'If trl.Count = 1 Then
            '  SessionData.CurrentTravelRequisition = trl(0)
            'End If
            CommandArgs.TransferModelToClient = True
            Dim str As String = "~/Productions/Travel/Travel.aspx?TRID=" + CommandArgs.ClientArgs.TravelRequisitionID.ToString
            CommandArgs.ReturnData = VirtualPathUtility.ToAbsolute(str)
          End If
        End If

      Case "ClearSchedules"
        SessionData.CurrentProduction.ProductionSystemAreaList(0).ClearSchedule()
        SessionData.CurrentProduction.CalculateProductionHR(True)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()

      Case "SetupBulkUpdate"
        BulkEditDetails = New OBLib.Helpers.Templates.ProductionHumanResourceBulkEditDetails(SessionData.CurrentProduction)
        BulkEditMode = "Update"

      Case "SetupBulkAdd"
        BulkEditDetails = New OBLib.Helpers.Templates.ProductionHumanResourceBulkEditDetails(SessionData.CurrentProduction)
        BulkEditMode = "Add"

      Case "SetupBulkDelete"
        BulkEditDetails = New OBLib.Helpers.Templates.ProductionHumanResourceBulkEditDetails(SessionData.CurrentProduction)
        BulkEditMode = "Delete"

      Case "CommitBulkUpdate"
        Dim hrl As List(Of HumanResourceItem) = BulkEditDetails.GetSelectedHR
        For Each HR As HumanResourceItem In hrl
          Dim Sched As HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HR.HumanResourceID)
          If Sched IsNot Nothing Then
            Dim pttl As List(Of ProductionTimelineItem) = BulkEditDetails.GetSelectedTimelines
            pttl.ForEach(Sub(pt)
                           Dim ExistingCSD As HRProductionScheduleDetail = Sched.HRProductionScheduleDetailList.GetItemByProductionTimeline(pt.ProductionTimeline)
                           If ExistingCSD IsNot Nothing Then
                             If ExistingCSD.CanEditScheduleCheck Then
                               ExistingCSD.StartDateTime = pt.StartDateTime
                               ExistingCSD.EndDateTime = pt.EndDateTime
                             End If
                           End If
                         End Sub)
          End If
        Next
        SessionData.CurrentProduction.CalculateProductionHR(False)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()

      Case "CommitBulkAdd"
        Dim hrl As List(Of HumanResourceItem) = BulkEditDetails.GetSelectedHR
        For Each HR As HumanResourceItem In hrl
          Dim Sched As HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HR.HumanResourceID)
          If Sched IsNot Nothing Then
            Dim pttl As List(Of ProductionTimelineItem) = BulkEditDetails.GetSelectedTimelines
            pttl.ForEach(Sub(pt)
                           Dim ExistingCSD As HRProductionScheduleDetail = Sched.HRProductionScheduleDetailList.GetItemByProductionTimeline(pt.ProductionTimeline)
                           If ExistingCSD Is Nothing Then
                             Dim tm As OBLib.Maintenance.General.ReadOnly.ROTimesheetMonthOld = OBLib.CommonData.Lists.ROTimesheetMonthListOld.GetItem(pt.ProductionTimeline.StartDateTime.Value.Date) 'mTimesheetDate.Date)
                             If tm IsNot Nothing AndAlso Not tm.ClosedInd Then
                               Sched.AddTimeline(pt.ProductionTimeline)
                             End If
                           End If
                         End Sub)
          End If
        Next
        SessionData.CurrentProduction.CalculateProductionHR(False)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()


      Case "CommitBulkDelete"
        Dim hrl As List(Of HumanResourceItem) = BulkEditDetails.GetSelectedHR
        For Each HR As HumanResourceItem In hrl
          Dim Sched As HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HR.HumanResourceID)
          If Sched IsNot Nothing Then
            Dim pttl As List(Of ProductionTimelineItem) = BulkEditDetails.GetSelectedTimelines
            pttl.ForEach(Sub(pt)
                           Dim ExistingCSD As HRProductionScheduleDetail = Sched.HRProductionScheduleDetailList.GetItemByProductionTimeline(pt.ProductionTimeline)
                           If ExistingCSD IsNot Nothing Then
                             If ExistingCSD.CanEditScheduleCheck Then
                               Sched.HRProductionScheduleDetailList.Remove(ExistingCSD)
                             End If
                           End If
                         End Sub)
          End If
        Next
        SessionData.CurrentProduction.CalculateProductionHR(False)
        SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckAvailability()

      Case "SubmitCopyCrew"
        For Each pc As ProductionCrew In CopyCrewProductionCrewList
          If pc.Selected Then
            Dim phr As ProductionHumanResource = CurrentProduction.ProductionSystemAreaList(0).AddNewProductionHumanResource()
            phr.DisciplineID = pc.DisciplineID
            phr.Discipline = pc.Discipline
            phr.PositionID = pc.PositionID
            phr.Position = pc.Position
            'phr.VehicleID = pc.VehicleID
            'phr.VehicleName = pc.VehicleName
            phr.HumanResourceID = pc.HumanResourceID
            phr.HumanResource = pc.HumanResource
            CurrentProduction.ProductionSystemAreaList(0).GenerateProductionSchedule(phr.HumanResourceID)
            CurrentProduction.ProductionSystemAreaList(0).CheckAvailability(phr.HumanResourceID)
          End If
        Next
        SessionData.CurrentProduction.CalculateProductionHR(False)
        CopyCrewProductionCrewList = New Crew.ProductionCrewList
      Case "CreateMissingShifts"

        AccessMissingShiftList.CheckAllRules()
        If AccessMissingShiftList.IsValid Then
          For Each accs As AccessShift In AccessMissingShiftList
            If accs.AccessShiftOptionInd Then

              Dim phr As ProductionHumanResource = CurrentProduction.ProductionSystemAreaList(0).ProductionHumanResourceList.GetItemByHumanResourceID(accs.HumanResourceID)
              If phr Is Nothing Then
                phr = CurrentProduction.ProductionSystemAreaList(0).AddNewProductionHumanResource()

                phr.DisciplineID = accs.DisciplineID
                phr.HumanResourceID = accs.HumanResourceID
                phr.HumanResource = accs.HumanResource
                CurrentProduction.ProductionSystemAreaList(0).GenerateProductionSchedule(phr.HumanResourceID)
              End If
              Dim pc = CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(accs.HumanResourceID)
              pc.AccessShiftList.AssignMissingShift(accs)
              accs.ProductionID = CurrentProduction.ProductionID

              accs.GetShiftFlagValues(pc.HRProductionScheduleDetailList.GetScheduleStartTimeForBiometrics(accs.StartDateTime.Date), pc.HRProductionScheduleDetailList.GetScheduleEndTimeForBiometrics(accs.StartDateTime.Date), OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast)
            End If
          Next
        Else
          AddMessage(Singular.Web.MessageType.Validation, "Missing shifts", "Please fix the errors and try again.")
        End If
      Case "LoadBiometricsMissingShifts"
        LoadBiometricsMissingShifts()
      Case "UpdateScheduleTimeFromTimelines"
        UpdateScheduleTimeFromTimelines(CommandArgs.ClientArgs.ProductionTimelineGuiD, CommandArgs.ClientArgs.StartTimeInd)
    End Select

  End Sub

#End Region


  Private Sub CheckAllVehicles()
    For Each PV As ProductionVehicle In CurrentProduction.ProductionVehicleList
      PV.CheckClash()
    Next
  End Sub


#Region " Custom Rule Methods "

  Public Function GetROVehicleList() As Singular.Web.Result
    Dim SD As DateTime = Now
    Dim ED As DateTime = Now
    If SessionData.CurrentProduction.PlayStartDateTime IsNot Nothing And SessionData.CurrentProduction.PlayEndDateTime IsNot Nothing Then
      SD = SessionData.CurrentProduction.PlayStartDateTime
      ED = SessionData.CurrentProduction.PlayEndDateTime
    End If
    mROVehicleSelectList = OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOldList.GetROVehicleOldList(SD, ED, SessionData.CurrentProduction.ProductionVenueID)
    Return New Singular.Web.Result(True) With {.Data = ROVehicleSelectList}
  End Function

  Public Function CheckTimelineValid(ProductionTimelineClient As ProductionTimelineClient) As Singular.Web.Result
    SessionData.CurrentProduction.ProductionSystemAreaList(0).UpdateTimeline(ProductionTimelineClient)
    Return New Singular.Web.Result(True) With {.Data = SessionData.CurrentProduction.ProductionSystemAreaList(0).CheckTimelineValid()}
  End Function

  Public Function CheckVehicleValid(VehicleID As Integer) As Singular.Web.Result
    Dim PV As ProductionVehicle = CurrentProduction.ProductionVehicleList.GetItemByVehicleID(VehicleID)
    PV.CheckClash()
    Return New Singular.Web.Result(True) With {.Data = PV.Clash}
  End Function

  Public Function UpdateHRSchedule(HRScheduleClient As HRProductionSchedule) As Singular.Web.Result
    Dim HRSchedule As HRProductionSchedule = CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HRScheduleClient.HumanResourceID)
    HRSchedule.UpdateFromClient(HRScheduleClient)
    HRSchedule.SetupUnSelectedTimelines()
    HRSchedule.CheckPreAndPostTravel()
    Return New Singular.Web.Result(True) With {.Data = HRSchedule}
  End Function

  Public Function CheckScheduleItemClash(HRProductionScheduleDetail As HRProductionScheduleDetail) As Singular.Web.Result
    Return New Singular.Web.Result(True) With {.Data = HRProductionScheduleDetail}
  End Function

  'Public Function CheckScheduleClash(HRScheduleClient As HRProductionSchedule) As Singular.Web.Result
  '  Dim HRSchedule As HRProductionSchedule = CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HRScheduleClient.HumanResourceID)
  '  HRSchedule.UpdateFromClient(HRScheduleClient)
  '  Return New Singular.Web.Result(True) With {.Data = HRSchedule}
  'End Function

  Public Function GetSchedule(HumanResourceID As Integer?) As Singular.Web.Result
    Dim HRSchedule As OBLib.Productions.Schedules.HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HumanResourceID)
    HRSchedule.HRProductionScheduleDetailListClient = HRSchedule.HRProductionScheduleDetailList
    HRSchedule.SetupUnSelectedTimelines()
    If IncludeBiometricDetails Then
      HRSchedule.PopulateMissingBiometricsShifts()
    End If
    Return New Singular.Web.Result(True) With {.Data = HRSchedule}
  End Function

  'Public Function UpdateAndSaveHRSchedule(HRScheduleClient As HRProductionSchedule) As Singular.Web.Result
  '  Dim OldSchedule As OBLib.Productions.Schedules.HRProductionSchedule = SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HRScheduleClient.HumanResourceID)
  '  OldSchedule.UpdateFromClient(HRScheduleClient)
  '  Dim NewSchedule As HRProductionSchedule = OldSchedule.SaveDirectly()
  '  SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.Remove(OldSchedule)
  '  SessionData.CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.Add(NewSchedule)
  '  NewSchedule.HRProductionScheduleDetailListClient = NewSchedule.HRProductionScheduleDetailList
  '  Return New Singular.Web.Result(True) With {.Data = NewSchedule}
  'End Function

  Private Sub UpdateScheduleTimeFromTimelines(TimelineGuid As String, StartTimeInd As Boolean)
    If TimelineGuid IsNot Nothing Then
      Dim Timeline = CurrentProduction.ProductionSystemAreaList(0).ProductionTimelineList.GetItemByGuid(New Guid(TimelineGuid))
      If Timeline IsNot Nothing Then
        For Each schedule As HRProductionSchedule In CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList
          For Each details As HRProductionScheduleDetail In schedule.HRProductionScheduleDetailList
            If details.ProductionTimelineGuid = TimelineGuid.ToString Then
              If StartTimeInd Then
                details.StartDateTime = Timeline.StartDateTime

                For Each accessshift As AccessShift In schedule.AccessShiftList
                  If accessshift.AccessShiftID = details.AccessShiftID Then
                    accessshift.ChangedShiftStartDateTime = schedule.HRProductionScheduleDetailList.GetScheduleStartTimeForBiometrics(details.TimesheetDate)
                    accessshift.ChangedShiftEndDateTime = schedule.HRProductionScheduleDetailList.GetScheduleEndTimeForBiometrics(details.TimesheetDate)

                    If details.StartDateTime = accessshift.ChangedShiftStartDateTime Then


                      accessshift.ShiftStartDateTime = details.OrigStartDateTime
                      accessshift.ChangedShiftStartDateTime = details.StartDateTime
                      ' Now recalc the Flag
                      accessshift.GetShiftFlagValues(accessshift.ChangedShiftStartDateTime, _
                                                     accessshift.ChangedShiftEndDateTime, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast)

                    End If
                  End If
                Next
              Else
                details.EndDateTime = Timeline.EndDateTime



                For Each accessshift As AccessShift In schedule.AccessShiftList
                  If accessshift.AccessShiftID = details.AccessShiftID Then
                    accessshift.ChangedShiftStartDateTime = schedule.HRProductionScheduleDetailList.GetScheduleStartTimeForBiometrics(details.TimesheetDate)
                    accessshift.ChangedShiftEndDateTime = schedule.HRProductionScheduleDetailList.GetScheduleEndTimeForBiometrics(details.TimesheetDate)

                    If details.EndDateTime = accessshift.ChangedShiftEndDateTime Then
                      accessshift.ShiftEndDateTime = details.OrigEndDateTime
                      accessshift.ChangedShiftEndDateTime = details.EndDateTime
                      ' Now recalc the Flag
                      accessshift.GetShiftFlagValues(accessshift.ChangedShiftStartDateTime, _
                                                     accessshift.ChangedShiftEndDateTime, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast)

                    End If
                  End If
                Next
              End If
            End If
          Next
        Next
      End If
    End If
  End Sub

#End Region


#Region " Biometrics "

  Public Function GetShiftFlagValues(AccessShiftID As Integer, StartDate As DateTime?, EndDate As DateTime?, SyncID As Integer) As Singular.Web.Result
    Dim FlagValues = AccessShiftList.GetShiftFlagValues(AccessShiftID, StartDate, EndDate, SyncID, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast)
    Return New Singular.Web.Result(True) With {.Data = FlagValues}
  End Function

  Public Sub LoadBiometricsMissingShifts()

    If IncludeBiometricDetails Then

      AccessMissingShiftList = OBLib.Biometrics.AccessShiftList.NewAccessShiftList

      Dim TravelToStartDate As DateTime? = Nothing
      Dim TravelToEndDate As DateTime? = Nothing
      If CurrentProduction.ProductionSystemAreaList(0).ProductionTimelineList.Count = 0 Then
        TravelToStartDate = CurrentProduction.PlayStartDateTime
        TravelToEndDate = CurrentProduction.PlayEndDateTime
      Else
        CurrentProduction.GetMinMaxTimesForVehicle(Nothing, Nothing, TravelToStartDate, TravelToEndDate)
      End If
      AccessMissingShiftList.LoadOBMissingShiftList(TravelToStartDate, TravelToEndDate)
    End If
  End Sub

  Public Sub CheckScheduleTimeCorrect()
    If CurrentProduction IsNot Nothing Then
      Dim Validtimes = OBLib.Biometrics.AccessShiftList.ScheduleTimeCorrect(CurrentProduction.ProductionID)

      CurrentProduction.ScheduleTimeValidity = Validtimes

    End If
  End Sub

  Public Function SaveHRSchedule(HumanResourceID As Integer) As Singular.Web.Result
    Dim HRSchedule As HRProductionSchedule = CurrentProduction.ProductionSystemAreaList(0).HRProductionScheduleList.GetItem(HumanResourceID)
    If HRSchedule IsNot Nothing Then
      If HRSchedule.IsValid Then
        HRSchedule.UpdateHRProductionSchedule()
      End If
    End If
    Return New Singular.Web.Result(True) With {.Data = HRSchedule}
  End Function
#End Region


End Class