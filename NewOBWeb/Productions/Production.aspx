﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
  CodeBehind="Production.aspx.vb" Inherits="NewOBWeb.Production" %>

<%@ Import Namespace="OBLib.Productions.Specs.Old" %>
<%@ Import Namespace="OBLib.Biometrics" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.Productions.ReadOnly" %>
<%@ Import Namespace="OBLib.Productions.Crew" %>
<%@ Import Namespace="OBLib.Productions" %>
<%@ Import Namespace="OBLib.Productions.Old" %>
<%@ Import Namespace="OBLib.Productions.Crew.ReadOnly" %>
<%@ Import Namespace="OBLib.Productions.Areas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
<%--  <link href="Dashboard.css" rel="stylesheet" type="text/css" />
  <link href="../Bootstrap/override/form-showcase.css" rel="stylesheet" type="text/css" />--%>
  <style type="text/css">
    div.ProductionCrewTab .scroll-content {
      overflow-y: scroll;
      height: 350px;
    }

    div.ProductionCrewTab tr.TBC td {
      background-color: #FF9900 !important;
      color: White;
      font-weight: bold;
    }

    .HRScheduleInvalid {
      background: #d9534f !important;
      color: White !important;
    }

    div.ProductionCrewTab tr.Invalid td {
      background-color: #d9534f !important;
      color: White;
      font-weight: bold;
    }

    div.RequirementList th, div.ProductionOutsourceServiceList th, div.ROVehicleList th, div.ProductionVehicleList th, div.ProductionTimelineList th, div.ProductionCrewTab th, div.ProductionCrewSchedule th, table.ROProductionTypeList th, table.ROEventTypeList th, table.ROProductionVenueList th, div.ProductionCommentSectionList th, div.CorrespondenceTab th, div.TravelRequisitionList th, div.ProductionAudioConfigurationList th {
      background-color: #5e5e5e;
      color: White;
    }

    #SelectROProductionSpec .modal-dialog {
      width: 800px;
    }

    .Msg-Validation {
      height: 140px !important;
      width: 100% !important;
      overflow-y: scroll !important;
    }

    .text-overflow {
      width: 200px;
      text-overflow: ellipses;
      color: red;
      font-weight: bold;
    }

    .VehicleClash {
      width: 200px;
      height: 20px !important;
      overflow-y: scroll;
    }

    div.HRProductionSchedule {
      padding: 4px;
      background-color: #E3DED9;
      margin: 0 0 5px 0;
      cursor: pointer;
      border-radius: 3px;
    }

    div.HRProductionScheduleDetailList .scroll-content {
      height: 200px !important;
      overflow-y: scroll !important;
    }

    .Msg div, .ValidationPopup {
      height: 140px !important;
      width: 100% !important;
      overflow-y: scroll !important;
    }
    /*    span.ImgValidation
    {
      float:right !important;
    }*/
    .PHRRow {
      padding: 2px;
      background-color: #E3DED9;
      margin: 0 0 6px 0;
      cursor: pointer;
      border-radius: 3px;
    }

    .form-control-sm {
      display: block !important;
      width: 100%; /*height: 34px;
      padding: 6px 12px;
      font-size: 14px;*/
      padding: 4px 8px !important;
      font-size: 11px !important;
      line-height: 1.428571429 !important;
      color: #555 !important;
      vertical-align: middle !important;
      background-color: #fff !important;
      background-image: none !important;
      border: 1px solid #ccc !important;
      border-radius: 4px !important;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
      box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
      -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
    }

    .form-control-xs {
      display: block !important;
      width: 100%; /*height: 34px;
      padding: 6px 12px;
      font-size: 14px;*/
      padding: 4px 8px !important;
      font-size: 11px !important;
      line-height: 1.428571429 !important;
      color: #555 !important;
      vertical-align: middle !important;
      background-color: #fff !important;
      background-image: none !important;
      border: 1px solid #ccc !important;
      border-radius: 4px !important;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
      box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
      -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
    }

    element.style {
    }

    .well-sm-phr {
      padding: 9px;
      border-radius: 3px;
    }

    .well-phr {
      min-height: 12px;
      padding: 6px;
      margin-bottom: 6px;
      background-color: #f5f5f5;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
      cursor: pointer;
    }

    .field-box {
      margin-top: 5px;
    }

    .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
      color: white !important;
      background-color: #428bca !important;
    }

    .navbar-default .navbar-nav > .active > a {
      background-image: none !important;
      background-image: none !important;
      background-repeat: none !important;
      filter: none !important;
      -webkit-box-shadow: inset 0 3px 9px rgba(0,0,0,0.075);
      box-shadow: inset 0 3px 9px rgba(0,0,0,0.075);
    }

    .well-xs {
      min-height: 12px;
      padding: 6px;
      margin-bottom: 6px;
      background-color: #f5f5f5;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
      cursor: pointer;
    }

    tr.TimesheetProcessed td {
      background: #96bf48;
      background: linear-gradient(to bottom, #a9d651 0%, #96bf48 100%);
      box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);
      border: 1px solid #99bd56;
      text-shadow: rgba(0, 0, 0, 0.24706) 0px 1px 0px;
      color: #fff;
    }

    div.well-phr .col-lg-12, div.well-phr .col-md-12, div.well-phr .col-sm-12 {
      padding-right: 5px !important;
      padding-left: 5px !important;
    }

    .ProductionCrewHumanResource {
      float: left;
      width: 160px;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .btn-custom {
      display: inline-block;
      padding: 3px 6px;
      margin-bottom: 0;
      font-size: 11px;
      font-weight: normal;
      line-height: 1.428571429;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      cursor: pointer;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 4px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      -o-user-select: none;
      user-select: none;
    }

    .modal-xl {
      width: 90%;
    }

    .modal-md {
      width: 70%;
    }

    .modal-sm {
      width: 40%;
    }

    .modal-dg {
      display: none;
    }

    .Msg div, .ValidationPopup {
      height: 110px !important;
    }

    .ValidationPopup {
      position: inherit !important;
    }

    .GroupButtonOverride span.input-group-addon {
      width: auto !important;
    }

    .hr-schedule-busy {
      width: 100%;
      text-align: center;
      vertical-align: middle;
    }

    .fa-10x {
      font-size: 10em;
    }

    #BulkModal td.LButtons, #BulkModal th.LButtons {
      width: 30px !important;
    }

    .ComboDropDown {
      z-index: 2001 !important;
    }

    .custom-error-box {
      background-color: #FBECEB;
      border-color: #c44;
      border-style: solid;
      border-width: 1px;
      height: 80px !important;
      width: 100%;
      overflow-y: scroll !important;
    }

    .GraphicsSuppliers {
      overflow: hidden;
      display: inline-block;
      text-overflow: ellipsis;
      white-space: nowrap;
    }

    .OBFacilitySuppliers {
      overflow: hidden;
      display: inline-block;
      text-overflow: ellipsis;
      white-space: nowrap;
    }

    .FlagsFont {
      color: red !important;
    }

    .InformationFlagsFont {
      color: blue !important;
    }

    .NoFlagsFont {
    }

    td.CellWait {
      background-image: url('../Singular/Images/LoadingSmall.gif');
      background-repeat: no-repeat;
      background-position: 3px 5px;
      color: #bbb;
    }
  </style>
  <script type="text/javascript" src="../Productions/Scripts/ProductionHelper.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Productions/Scripts/ProductionVM.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Productions/Scripts/ManualOBProduction.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Travel.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/External/thenBy.min.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript">

    Singular.OnPageLoad(function () {
      if (!window.siteHubManager) {
        window.siteHubManager = new SiteHubManager()
      }
    })

    $(window).unload(function () {
      if (ViewModel.CurrentProduction() && ViewModel.CurrentProduction().ProductionSystemAreaList().length == 1 && !ViewModel.CurrentProduction().ProductionSystemAreaList()[0].IsBeingEditedBySomeoneElse()) {
        window.siteHubManager.bookingOutOfEdit(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ResourceBookingID())
      }
    })

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% 
    Using h = Helpers

      With h.Div
        With .Helpers.HTMLTag("h1")
          .AddBinding(Singular.Web.KnockoutBindingString.html, "'This booking is opened by ' + ViewModel.CurrentProduction().ProductionSystemAreaList()[0].InEditByName()")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.CurrentProduction().ProductionSystemAreaList()[0].IsBeingEditedBySomeoneElse()")
        End With
      End With

      Dim Template As NewOBWeb.CustomControls.Productions.ProductionDashboard = New NewOBWeb.CustomControls.Productions.ProductionDashboard(ViewModel.CurrentProduction, "")
      h.Control(Template)

      ''-------------------------------------------Main--------------------------------------------
      'Dim ProductionMain As NewOBWeb.CustomControls.ProductionControls.ProductionMain = New NewOBWeb.CustomControls.ProductionControls.ProductionMain(ViewModel, ViewModel.CurrentProduction)
      'h.Control(ProductionMain)
      With Template.SaveButton
        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(vm) ViewModel.IsValid)
        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!ViewModel.CurrentProduction().ProductionSystemAreaList()[0].IsBeingEditedBySomeoneElse()")
      End With

      With Template.ToolbarContainer
        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!ViewModel.CurrentProduction().ProductionSystemAreaList()[0].IsBeingEditedBySomeoneElse()")
        With .Helpers.Toolbar()
          .Helpers.MessageHolder()
        End With
      End With

      With Template.Content
        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!ViewModel.CurrentProduction().ProductionSystemAreaList()[0].IsBeingEditedBySomeoneElse()")
        With .Helpers.DivC("row MainDetails well well-sm")
          With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(vm) ViewModel.CurrentProduction)
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.BootstrapDivColumn(6, 3, 1)
                .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.ProductionRefNo)
                With .Helpers.ReadOnlyFor(Function(p As OBLib.Productions.Old.Production) p.ProductionRefNo)
                  .AddClass("form-control-sm")
                  .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d) True)
                End With
              End With
              With .Helpers.BootstrapDivColumn(6, 3, 7)
                .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.ProductionDescription)
                With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.ProductionDescription)
                  .AddClass("form-control-sm")
                  .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d) True)
                End With
              End With
              If OBLib.Security.Settings.CurrentUserID = 1 Or OBLib.Security.Settings.CurrentUser.UserTypeID = 29 Or OBLib.Security.Settings.CurrentUserID = 152 Then ' /*Chevani*/  
                With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(cp As OBLib.Productions.Old.Production) cp.ProductionSystemAreaList)
                  With .Helpers.BootstrapDivColumn(6, 3, 2)
                    .Helpers.LabelFor(Function(p As ProductionSystemArea) p.ProductionAreaStatusID)
                    With .Helpers.EditorFor(Function(p As ProductionSystemArea) p.ProductionAreaStatusID)
                      .AddClass("form-control-sm")
                    End With
                  End With
                  With .Helpers.BootstrapDivColumn(6, 3, 2)
                    .Helpers.LabelFor(Function(p As ProductionSystemArea) p.StatusDate)
                    With .Helpers.EditorFor(Function(p As ProductionSystemArea) p.StatusDate)
                      .AddClass("form-control-sm")
                    End With
                  End With
                End With
              End If
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.BootstrapDivColumn(6, 3, 3)
                .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.ProductionTypeID)
                With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.ProductionTypeID)
                  .AddClass("form-control-sm")
                  .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditProductionType()")
                End With
              End With
              With .Helpers.BootstrapDivColumn(6, 3, 3)
                .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.EventTypeID)
                With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.EventTypeID)
                  .AddClass("form-control-sm")
                  .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditEventType()")
                End With
              End With

              With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(cp As OBLib.Productions.Old.Production) cp.ProductionSystemAreaList)
                With .Helpers.BootstrapDivColumn(6, 3, 3)
                  .Helpers.LabelFor(Function(p As ProductionSystemArea) p.ProductionSpecRequirementID)
                  With .Helpers.EditorFor(Function(p As ProductionSystemArea) p.ProductionSpecRequirementID)
                    .AddClass("form-control-sm")
                    .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d) ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Or Not ViewModel.CurrentProduction.CanEditSpec)
                  End With
                End With
                With .Helpers.BootstrapDivColumn(6, 3, 3)
                  .Helpers.LabelFor(Function(p As ProductionSystemArea) p.NotComplySpecReasons)
                  With .Helpers.EditorFor(Function(p As ProductionSystemArea) p.NotComplySpecReasons)
                    .AddClass("form-control-sm")
                    .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d) ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Or Not ViewModel.CurrentProduction.CanEditSpec)
                  End With
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.BootstrapDivColumn(4, 4, 4)
                .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.ProductionVenueID)
                With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.ProductionVenueID)
                  .AddClass("form-control-sm")
                  .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As OBLib.Productions.Old.Production) d.SystemAreaReconciled Or d.SystemAreaCancelled)
                End With
              End With
              With .Helpers.BootstrapDivColumn(4, 4, 4)
                With .Helpers.DivC("field-box")
                  With .Helpers.Div
                    .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.PlayStartDateTime)
                  End With
                  With .Helpers.BootstrapDivColumn(6, 7, 7)
                    With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.PlayStartDateTime)
                      .AddClass("form-control-sm")
                    End With
                  End With
                  With .Helpers.BootstrapDivColumn(6, 5, 5)
                    With .Helpers.TimeEditorFor(Function(p As OBLib.Productions.Old.Production) p.PlayStartDateTime)
                      .AddClass("form-control-sm")
                    End With
                  End With
                End With
              End With
              With .Helpers.BootstrapDivColumn(4, 4, 4)
                With .Helpers.DivC("field-box")
                  With .Helpers.Div
                    .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.PlayEndDateTime)
                  End With
                  With .Helpers.BootstrapDivColumn(6, 7, 7)
                    With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.PlayEndDateTime)
                      .AddClass("form-control-sm")
                    End With
                  End With
                  With .Helpers.BootstrapDivColumn(6, 5, 5)
                    With .Helpers.TimeEditorFor(Function(p As OBLib.Productions.Old.Production) p.PlayEndDateTime)
                      .AddClass("form-control-sm")
                    End With
                  End With
                End With
              End With
              'With .Helpers.BootstrapDivColumn(4, 3, 2)
              '  .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.VenueConfirmedDate)
              '  With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.VenueConfirmedDate)
              '    .AddClass("form-control")
              '    If ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Then
              '      .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
              '    End If
              '  End With
              'End With
            End With
            If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
              With .Helpers.DivC("row top-buffer-10")
                With .Helpers.Bootstrap.Column(4, 4, 4, 4)
                  .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.City)
                  With .Helpers.Div
                    .AddClass("form-control-sm")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "CityHTML($data)")
                    .Style.Height = "25px"
                  End With
                End With
                'With .Helpers.BootstrapDivColumn(4, 4, 4)
                '  .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.VisionViewInd)
                '  .AddBinding(Singular.Web.KnockoutBindingString.visible, "CanSeeVissionView()")
                '  With .Helpers.BootstrapStateButton("", "", "VisionViewIndCss($data)", "VisionViewIndHtml($data)", False)
                '    '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) False)
                '  End With
                'End With
                With .Helpers.BootstrapDivColumn(4, 4, 4)
                  .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.OBFacilitySuppliers)
                  With .Helpers.Div
                    .AddClass("form-control-sm GraphicsSuppliers")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "OBFacilitySuppliersHTML($data)")
                    .Style.Height = "25px"
                  End With
                End With

                With .Helpers.BootstrapDivColumn(4, 4, 4)
                  .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.GraphicsSuppliers)
                  With .Helpers.Div
                    .AddClass("form-control-sm GraphicsSuppliers")
                    .AddBinding(Singular.Web.KnockoutBindingString.html, "GraphicsSuppliersHTML($data)")
                    .Style.Height = "25px"
                  End With
                End With
              End With
            End If
            'With .Helpers.Div()
            '  .AddClass("row top-buffer-10")

            'End With
            'With .Helpers.DivC("")
            '  With .Helpers.BootstrapDivColumn(6, 6, 6)

            '  End With
            'End With
          End With
        End With

        With .Helpers.DivC("row top-buffer-10")
          With .Helpers.BootstrapTabControl("", "nav-pills nav-justified", "tablist")

            With .AddTab("Main", "fa fa-reorder", True, "")
              With .TabPane
                .AddClass("top-buffer-10")
                With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(vm) ViewModel.CurrentProduction)

                  With .Helpers.BootstrapDivColumn(12, 6, 6)
                    With .Helpers.DivC("row top-buffer-10")
                      With .Helpers.BootstrapDivColumn(8, 8, 8)
                        .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.SynergyGenRefNo)
                        With .Helpers.ReadOnlyFor(Function(p As OBLib.Productions.Old.Production) p.SynergyGenRefNo)
                          .AddClass("form-control")
                          .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As OBLib.Productions.Old.Production) d.SystemAreaReconciled Or d.SystemAreaCancelled)
                        End With
                      End With
                    End With
                    With .Helpers.DivC("row top-buffer-10")
                      With .Helpers.BootstrapDivColumn(8, 8, 8)
                        .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.Title)
                        With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.Title)
                          .AddClass("form-control")
                          .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As OBLib.Productions.Old.Production) d.SystemAreaReconciled Or d.SystemAreaCancelled)
                        End With
                      End With
                    End With

                    With .Helpers.DivC("row top-buffer-10")
                      With .Helpers.BootstrapDivColumn(8, 8, 8)
                        .Helpers.LabelFor(Function(p As OBLib.Productions.Old.Production) p.TeamsPlaying)
                        With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.TeamsPlaying)
                          .AddClass("form-control")
                          .AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As OBLib.Productions.Old.Production) d.SystemAreaReconciled Or d.SystemAreaCancelled)
                        End With
                      End With
                    End With


                    'With .Helpers.DivC("row top-buffer-10")
                    If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then

                      With .Helpers.DivC("row top-buffer-10")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.TimelineReadyInd)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                If ViewModel.CurrentProduction.CanFinaliseTimeline Then
                                  With .Helpers.BootstrapStateButton("", "TimelineFinalisedClick($data)", "TimelineFinalisedCss($data)", "TimelineFinalisedHtml($data)", False)
                                    '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanClickFinaliseTimeline($data)")
                                  End With
                                Else
                                  With .Helpers.Bootstrap.ROStateButton(Function(d As OBLib.Productions.Old.Production) d.TimelineReadyInd, , , , , , , "btn-none")

                                  End With
                                End If
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.TimelineReadyDateTime)
                                .AddClass("form-control")
                                .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanClickFinaliseTimeline($data)")
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.DivC("row top-buffer-10")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.CrewFinalised)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                With .Helpers.BootstrapStateButton("", "CrewFinalisedClick($data)", "CrewFinalisedCss($data)", "CrewFinalisedHtml($data)", False)
                                  If ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Then
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  Else
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanClickCrewFinalise($data)")
                                    'If Singular.Security.HasAccess("Productions", "Finalise Crew") Then
                                    '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                    'Else
                                    '  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                    'End If
                                  End If
                                End With
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.CrewPlanningFinalisedDate)
                                .AddClass("form-control")
                                If ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Then
                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                Else
                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanClickCrewFinalise($data)")
                                  'If Singular.Security.HasAccess("Productions", "Finalise Crew") Then
                                  '  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                  'Else
                                  '  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  'End If
                                End If
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.DivC("row top-buffer-10")

                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.PlanningFinalised)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                '.Helpers.BootstrapButton(, , "btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveHumanResource($data)")
                                With .Helpers.BootstrapStateButton("", "PlanningFinalisedClick($data)", "PlanningFinalisedCss($data)", "PlanningFinalisedHtml($data)", False)
                                  If ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Then
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  Else
                                    If Singular.Security.HasAccess("Productions", "Finalise Production") AndAlso ViewModel.CurrentProduction.CrewFinalised Then
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                    Else
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                    End If
                                  End If
                                End With
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.PlanningFinalisedDate)
                                .AddClass("form-control")
                                If ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Then
                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                Else
                                  If Singular.Security.HasAccess("Productions", "Finalise Production") AndAlso ViewModel.CurrentProduction.CrewFinalised Then
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                  Else
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End If
                                End If
                              End With
                            End With
                          End With
                        End With

                      End With


                      ' End With
                      With .Helpers.DivC("row top-buffer-10")
                        'With .Helpers.BootstrapDivColumn(12, 12, 12)
                        '.AddClass("top-buffer-15")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          'Reconcile
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.Reconciled)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                With .Helpers.BootstrapStateButton("", "ReconciledClick($data)", "ReconciledCss($data)", "ReconciledHtml($data)", False)
                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanClickReconcile($data)")
                                End With
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.ReconciledDate)
                                .AddClass("form-control")
                                .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanClickReconcile($data)")
                                .AddBinding(Singular.Web.KnockoutBindingString.click, "ReconciledDateClick($data)")
                              End With
                            End With
                          End With
                        End With
                        'End With
                      End With

                      With .Helpers.DivC("row top-buffer-10")
                        'With .Helpers.BootstrapDivColumn(12, 12, 12)
                        '.AddClass("top-buffer-15")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.Cancelled)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                '.Helpers.BootstrapButton(, , "btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveHumanResource($data)")
                                With .Helpers.BootstrapStateButton("", "CancelledClick($data)", "CancelledCss($data)", "CancelledHtml($data)", False)
                                  If ViewModel.CurrentProduction.Reconciled Then
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  ElseIf ViewModel.CurrentProduction.SystemAreaCancelled Then
                                    If Singular.Security.HasAccess("Productions", "Un-cancel productions") Then
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                    End If
                                  Else
                                    If Singular.Security.HasAccess("Productions", "Cancel Production") Then
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                    Else
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                    End If
                                  End If
                                End With
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.CancelledDate)
                                .AddClass("form-control")
                                If ViewModel.CurrentProduction.Reconciled Then
                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                ElseIf ViewModel.CurrentProduction.Cancelled Then
                                  If Singular.Security.HasAccess("Productions", "Un-cancel productions") Then
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                  End If
                                Else
                                  If Singular.Security.HasAccess("Productions", "Cancel Production") Then
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                  Else
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End If
                                End If
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.CancelledReason)
                            With .Helpers.Div
                              With .Helpers.EditorFor(Function(p As OBLib.Productions.Old.Production) p.CancelledReason)
                                .AddClass("form-control")
                                If ViewModel.CurrentProduction.Reconciled Then
                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                ElseIf ViewModel.CurrentProduction.SystemAreaCancelled Then
                                  If Singular.Security.HasAccess("Productions", "Un-cancel productions") Then
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                  End If
                                Else
                                  If Singular.Security.HasAccess("Productions", "Cancel Production") Then
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) True)
                                  Else
                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End If
                                End If
                              End With
                            End With
                          End With
                        End With
                        'End With
                      End With

                      With .Helpers.With(Of OBLib.Productions.Areas.ProductionSystemArea)(Function(d As OBLib.Productions.Old.Production) d.ProductionSystemAreaList(0))
                        With .Helpers.DivC("row top-buffer-10")
                          'With .Helpers.BootstrapDivColumn(12, 12, 12)
                          '.AddClass("top-buffer-15")
                          With .Helpers.BootstrapDivColumn(12, 12, 6)
                            '.AddClass("top-buffer-15")
                            'Reconcile
                            With .Helpers.DivC("field-box")
                              .Helpers.LabelFor(Function(phr As OBLib.Productions.Areas.ProductionSystemArea) phr.MealReimbursement)
                              With .Helpers.DivC("input-group")
                                With .Helpers.HTMLTag("span")
                                  .AddClass("input-group-btn")
                                  With .Helpers.Bootstrap.StateButtonNew(Function(d) d.MealReimbursement, "Include Meal Reimbersement", "Exclude Meal Reimbursement", , , , , "btn-md")

                                  End With
                                End With
                              End With
                            End With
                          End With
                          'End With
                        End With
                      End With

                    ElseIf OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then

                      With .Helpers.DivC("row top-buffer-10")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.CommentatorCrewFinalised)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                '.Helpers.BootstrapButton(, , "btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveHumanResource($data)")
                                .Helpers.BootstrapStateButton("", "CommentatorCrewFinalisedClick($data)", "CommentatorCrewFinalisedCss($data)", "CommentatorCrewFinalisedHtml($data)", False)
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.CommentatorCrewPlanningFinalisedDate)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.DivC("row top-buffer-10")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.CommentatorPlanningFinalised)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                '.Helpers.BootstrapButton(, , "btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveHumanResource($data)")
                                .Helpers.BootstrapStateButton("", "CommentatorPlanningFinalisedClick($data)", "CommentatorPlanningFinalisedCss($data)", "CommentatorPlanningFinalisedHtml($data)", False)
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.CommentatorPlanningFinalisedDate)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.DivC("row top-buffer-10")
                        With .Helpers.BootstrapDivColumn(12, 12, 6)
                          '.AddClass("top-buffer-15")
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As OBLib.Productions.Old.Production) phr.CommentatorReconciled)
                            With .Helpers.DivC("input-group")
                              With .Helpers.HTMLTag("span")
                                .AddClass("input-group-btn")
                                '.Helpers.BootstrapButton(, , "btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveHumanResource($data)")
                                With .Helpers.BootstrapStateButton("", "CommentatorReconciledClick($data)", "CommentatorReconciledCss($data)", "CommentatorReconciledHtml($data)", False)
                                  '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanUnReconcile($data)")
                                End With
                              End With
                              With .Helpers.EditorFor(Function(phr As OBLib.Productions.Old.Production) phr.CommentatorReconciledDate)
                                .AddClass("form-control")
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.With(Of OBLib.Productions.Areas.ProductionSystemArea)(Function(d As OBLib.Productions.Old.Production) d.ProductionSystemAreaList(0))
                        With .Helpers.DivC("row top-buffer-10")
                          'With .Helpers.BootstrapDivColumn(12, 12, 12)
                          '.AddClass("top-buffer-15")
                          With .Helpers.BootstrapDivColumn(12, 12, 6)
                            '.AddClass("top-buffer-15")
                            'Reconcile
                            With .Helpers.DivC("field-box")
                              .Helpers.LabelFor(Function(phr As OBLib.Productions.Areas.ProductionSystemArea) phr.MealReimbursement)
                              With .Helpers.DivC("input-group")
                                With .Helpers.HTMLTag("span")
                                  .AddClass("input-group-btn")
                                  With .Helpers.Bootstrap.StateButtonNew(Function(d) d.MealReimbursement, "Include Meal Reimbersement", "Exclude Meal Reimbursement", , , , , "btn-md")
                                  End With
                                End With
                              End With
                            End With
                          End With
                          'End With
                        End With
                      End With

                    End If

                    With .Helpers.Div
                      .Style.Height = "20"
                    End With

                    'End With

                  End With

                  With .Helpers.BootstrapDivColumn(2, 2, 2)
                    With .Helpers.DivC("row top-buffer-10")
                      With .Helpers.BootstrapDivColumn(4, 4, 4)
                        .Helpers.BootstrapButton(, "Production Planners", "btn-warning", "glyphicon-user", Singular.Web.PostBackType.None, False, , , "ViewProductionPlanners()")
                      End With
                    End With
                  End With



                End With
              End With
            End With

            'Requirements Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewRequirementsInd Then
              With .AddTab("Requirements", "fa fa-database", False, "SetRequirementsTabVisible()")
                .AddBinding(Singular.Web.KnockoutBindingString.visible, False)
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
                    With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(v As OBLib.Productions.Old.Production) v.ProductionSystemAreaList)
                      With .Helpers.DivC("row RequirementList")
                        .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.RequirementsTabVisible)
                        With .Helpers.BootstrapDivColumn(12, 12, 5)
                          .AddClass("EquipmentTypeList")
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Equipment")
                            End With
                            With .PanelBody
                              With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Or Not ViewModel.CurrentProduction.CanEditSpec))
                                With .Helpers.BootstrapTableFor(Of ProductionSpecRequirementEquipmentType)(Function(vm) vm.ProductionSpecRequirementEquipmentTypeList, False, False, "")
                                  With .FirstRow
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementEquipmentType) et.EquipmentTypeID)
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementEquipmentType) et.EquipmentSubTypeID)
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementEquipmentType) et.EquipmentQuantity)
                                  End With
                                End With
                              End With
                              With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                                .Helpers.DivC("row top-buffer-10")
                                With .Helpers.BootstrapTableFor(Of ProductionSpecRequirementEquipmentType)(Function(vm) vm.ProductionSpecRequirementEquipmentTypeList, False, False, "")
                                  With .FirstRow
                                    .AddColumn(Function(et As ProductionSpecRequirementEquipmentType) et.EquipmentTypeID)
                                    .AddColumn(Function(et As ProductionSpecRequirementEquipmentType) et.EquipmentSubTypeID)
                                    .AddColumn(Function(et As ProductionSpecRequirementEquipmentType) et.EquipmentQuantity)
                                    With .AddColumn("")
                                      .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveEquipment($data)")
                                    End With
                                  End With
                                  With .FooterRow
                                    With .AddColumn("")
                                      .ColSpan = 3
                                      With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddEquipment()")
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddRequirementEquipmentType()")
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 7)
                          .AddClass("PositionList")
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Positions")
                            End With
                            With .PanelBody
                              With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Or Not ViewModel.CurrentProduction.CanEditSpec))
                                With .Helpers.BootstrapTableFor(Of ProductionSpecRequirementPosition)(Function(vm) vm.ProductionSpecRequirementPositionList, False, False, "")
                                  With .FirstRow
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementPosition) et.DisciplineID)
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementPosition) et.PositionID)
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementPosition) et.EquipmentSubTypeID)
                                    .AddReadOnlyColumn(Function(et As ProductionSpecRequirementPosition) et.EquipmentQuantity)
                                  End With
                                End With
                              End With
                              With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                                With .Helpers.BootstrapTableFor(Of ProductionSpecRequirementPosition)(Function(vm) vm.ProductionSpecRequirementPositionList, False, False, "")
                                  With .FirstRow
                                    .AddColumn(Function(et As ProductionSpecRequirementPosition) et.DisciplineID)
                                    .AddColumn(Function(et As ProductionSpecRequirementPosition) et.PositionID)
                                    .AddColumn(Function(et As ProductionSpecRequirementPosition) et.EquipmentSubTypeID)
                                    .AddColumn(Function(et As ProductionSpecRequirementPosition) et.EquipmentQuantity)
                                    With .AddColumn("")
                                      .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemovePosition($data)")
                                    End With
                                  End With
                                  With .FooterRow
                                    With .AddColumn("")
                                      .ColSpan = 3
                                      With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddPosition()")
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddRequirementPosition()")
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Vehicles Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewVehiclesInd Then
              With .AddTab("Vehicles", "fa fa-truck", False, "SetProductionVehiclesTabVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
                    With .Helpers.DivC("row ProductionVehiclesTab")
                      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.ProductionVehiclesTabVisible)

                      With .Helpers.DivC("ProductionVehicleList")
                        .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.VehicleListVisible)

                        With .Helpers.BootstrapPanel
                          With .PanelHeading
                            .Helpers.HTML("Vehicles")
                          End With
                          With .PanelBody
                            With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Vehicles.ProductionVehicle)(Function(c) ViewModel.CurrentProduction.ProductionVehicleList, False, False, "")
                                With .FirstRow
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleName).AddClass("bold")
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleType).AddClass("bold")
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteRequestedDate)
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteReceivedDate)
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuotedAmount)
                                  '.AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.Clash)
                                End With
                              End With
                            End With
                            With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Vehicles.ProductionVehicle)(Function(c) ViewModel.CurrentProduction.ProductionVehicleList, False, False, "")
                                With .FirstRow
                                  If Singular.Security.HasAccess("Productions", "Edit Vehicles") Then
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleName).AddClass("bold")
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleType).AddClass("bold")
                                    With .AddColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteRequestedDate)
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.Supplied()")
                                    End With
                                    With .AddColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteReceivedDate)
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.Supplied()")
                                    End With
                                    With .AddColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuotedAmount)
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.Supplied()")
                                    End With
                                    With .AddColumn("")
                                      .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveVehicle($data)")
                                    End With
                                  Else
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleName).AddClass("bold")
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleType).AddClass("bold")
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteRequestedDate)
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteReceivedDate)
                                    .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuotedAmount)
                                  End If
                                End With
                                With .FooterRow
                                  With .AddColumn("")
                                    .ColSpan = 6
                                    If Singular.Security.HasAccess("Productions", "Edit Vehicles") Then
                                      With .Helpers.BootstrapButton("", "Add Vehicle", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, True, , , "SetupSelectVehicle()")
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.GetParent().IsSelfValid()")
                                      End With
                                    End If
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                        With .Helpers.DivC("ROVehicleList")
                          .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.SelectVehicleVisible)
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Production Vehicles")
                            End With
                            With .PanelBody
                              'Vehicle Select                  
                              .Helpers.BootstrapButton("", "Back", "btn-sm btn-danger", "glyphicon-arrow-left", Singular.Web.PostBackType.None, False, , , "BackToProductionVehicleList()")
                              .Helpers.BootstrapPageHeader(2, "Vehicle List", "")
                              With .Helpers.DivC("field-box")
                                With .Helpers.EditorFor(Function(p) ViewModel.ROVehicleFilter)
                                  .Attributes("placeholder") = "Filter..."
                                  .AddClass("form-control filter-field")
                                  .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                  .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROVehicleList}")
                                End With
                              End With
                              .Helpers.DivC("top-buffer-10")
                              With .Helpers.BootstrapTableFor(Of OBLib.Maintenance.Vehicles.ReadOnly.ROVehicle)(Function(c) ViewModel.ROVehicleSelectList, False, False, "$data.Visible()")
                                With .FirstRow
                                  With .AddColumn("")
                                    .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "AddVehicle($data)")
                                  End With
                                  .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicle) c.VehicleName).AddClass("bold")
                                  .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicle) c.VehicleType).AddClass("bold")
                                  .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicle) c.PreviousCityDist)
                                  .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicle) c.CurrCity)
                                  .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicle) c.NextCityDist)
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Timelines Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            With .AddTab("Timelines", "fa fa-clock-o", True, "SetTimelineTabsVisible()")
              With .TabPane
                .AddClass("top-buffer-10")
                With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
                  With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(v As OBLib.Productions.Old.Production) v.ProductionSystemAreaList)
                    With .Helpers.DivC("row ProductionTimelinesTab")
                      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.TimelinesTabVisible)
                      With .Helpers.DivC("ProductionTimelineList")
                        With .Helpers.BootstrapPanel("panel-primary")
                          With .PanelHeading
                            .Helpers.HTML("Timelines")
                          End With
                          With .PanelBody
                            With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapTableFor(Of ProductionTimeline)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionTimelineList, False, False, "$data.CanRender()") 'RenderTimeline($data)
                                With .FirstRow
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.TimelineDate)
                                    .Style.TextAlign = Singular.Web.TextAlign.center
                                    .Editor.Style.TextAlign = Singular.Web.TextAlign.center
                                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End With
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.StartDateTime)
                                    .Style.TextAlign = Singular.Web.TextAlign.center
                                    .Editor.Style.TextAlign = Singular.Web.TextAlign.center
                                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End With
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.EndDateTime)
                                    .Style.TextAlign = Singular.Web.TextAlign.center
                                    .Editor.Style.TextAlign = Singular.Web.TextAlign.center
                                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End With
                                  .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.VehicleID).Style.TextAlign = Singular.Web.TextAlign.left
                                  .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.CrewTypeID).Style.TextAlign = Singular.Web.TextAlign.left
                                  .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.Comments).Style.TextAlign = Singular.Web.TextAlign.left
                                End With
                              End With
                            End With
                            With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewTimeline()")
                                .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionTimeline()")
                              End With
                              .Helpers.DivC("top-buffer-10")
                              With .Helpers.Bootstrap.TableFor(Of ProductionTimeline)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionTimelineList, False, False, , , , , , "ProductionTimelineList", , "ProductionHelper.Production.Rules.AfterTimelineRemoved($data)") 'RenderTimeline($data)
                                With .FirstRow
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.ProductionTimelineTypeID)
                                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                    .HeaderStyle.Width = 220
                                    '.Editor.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As ProductionTimeline) d.SharedTimeline OrElse (d.ProductionTimelineTypeID = CInt(OBLib.CommonData.Enums.ProductionTimelineType.TempTimelineBookings) AndAlso Not d.IsNew))
                                    .Editor.AddClass("grid-control-dropdown")
                                  End With
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.TimelineDate)
                                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center

                                    With .Editor
                                      .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditTimeline($data)")
                                      '.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As ProductionTimeline) d.SharedTimeline Or d.ProductionTimelineTypeID Is Nothing OrElse _
                                      '                                                                                               (d.ProductionTimelineTypeID = CInt(OBLib.CommonData.Enums.ProductionTimelineType.TempTimelineBookings) AndAlso Not d.IsNew))
                                      .Style.TextAlign = Singular.Web.TextAlign.center
                                      .AddClass("grid-control")
                                    End With
                                  End With
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.StartDateTime)
                                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                                    With .Editor
                                      .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditTimeline($data)")
                                      '.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As ProductionTimeline) d.SharedTimeline Or d.TimelineDate Is Nothing OrElse _
                                      '                                                                                                 (d.ProductionTimelineTypeID = CInt(OBLib.CommonData.Enums.ProductionTimelineType.TempTimelineBookings) AndAlso Not d.IsNew))
                                      .Style.TextAlign = Singular.Web.TextAlign.center
                                      .AddClass("grid-control")
                                    End With
                                  End With
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.EndDateTime)
                                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.center
                                    With .Editor
                                      .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditTimeline($data)")
                                      '.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As ProductionTimeline) d.SharedTimeline Or d.TimelineDate Is Nothing OrElse _
                                      '                                                                                                (d.ProductionTimelineTypeID = CInt(OBLib.CommonData.Enums.ProductionTimelineType.TempTimelineBookings) AndAlso Not d.IsNew))
                                      .Style.TextAlign = Singular.Web.TextAlign.center
                                      .AddClass("grid-control")
                                    End With
                                  End With
                                  If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer) Then
                                    With .AddColumn(Function(pt As ProductionTimeline) pt.VehicleID)
                                      .Style.TextAlign = Singular.Web.TextAlign.left
                                      With .Editor
                                        .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditTimeline($data)")
                                        '.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As ProductionTimeline) d.SharedTimeline Or d.TimelineDate Is Nothing OrElse _
                                        '                                                                                              (d.ProductionTimelineTypeID = CInt(OBLib.CommonData.Enums.ProductionTimelineType.TempTimelineBookings) AndAlso Not d.IsNew))
                                        .AddClass("grid-control-dropdown")
                                      End With
                                    End With
                                  End If
                                  'With .AddColumn(Function(pt As ProductionTimeline) pt.CrewTypeID)
                                  '  .Style.TextAlign = Singular.Web.TextAlign.left
                                  'End With
                                  With .AddColumn(Function(pt As ProductionTimeline) pt.Comments)
                                    .HeaderStyle.TextAlign = Singular.Web.TextAlign.left
                                    With .Editor
                                      .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditTimeline($data)")
                                      '.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As ProductionTimeline) d.SharedTimeline Or d.TimelineDate Is Nothing OrElse _
                                      '                                                                                               (d.ProductionTimelineTypeID = CInt(OBLib.CommonData.Enums.ProductionTimelineType.TempTimelineBookings) AndAlso Not d.IsNew))
                                      .AddClass("grid-control")
                                    End With

                                  End With
                                  With .AddColumn("")
                                    With .Helpers.If(Function(d As ProductionTimeline) Not d.SharedTimeline)
                                      With .Helpers.BootstrapButton(, , "btn-xs btn-flat btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveTimeline($data)")
                                        .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanDeleteTimeline($data)")
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

            'Services Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewServicesInd Then
              With .AddTab("Services", "fa fa-usd", False, "SetOutsourceServicesTabVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
                    With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(v As OBLib.Productions.Old.Production) v.ProductionSystemAreaList)
                      With .Helpers.DivC("row ProductionOutsourceServiceList")
                        .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.OutsourceServicesTabVisible)
                        With .Helpers.BootstrapPanel
                          With .PanelHeading
                            .Helpers.HTML("Outsource Services")
                          End With
                          With .PanelBody
                            With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Services.ProductionOutsourceService)(Function(c As OBLib.Productions.Areas.ProductionSystemArea) c.ProductionOutsourceServiceList, False, False, "")
                                With .FirstRow
                                  With .AddReadOnlyColumn(Function(c) c.OutsourceServiceTypeID)
                                  End With
                                  .AddReadOnlyColumn(Function(c) c.SupplierID)
                                  .AddReadOnlyColumn(Function(c) c.Description)
                                  .AddReadOnlyColumn(Function(c) c.QuoteRequestedDate)
                                  .AddReadOnlyColumn(Function(c) c.QuoteReceivedDate)
                                  .AddReadOnlyColumn(Function(c) c.QuotedAmount)
                                  'With .AddColumn("Required")
                                  '  With .Helpers.BootstrapStateButton("", "", "NotRequiredIndCss($data)", "NotRequiredIndHtml($data)", False)
                                  '    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d As OBLib.Productions.Services.ProductionOutsourceService) False)
                                  '  End With
                                  'End With
                                  '.AddReadOnlyColumn(Function(c) c.NotRequiredReason)
                                  .AddReadOnlyColumn(Function(c) c.SAPOrderNo)
                                End With
                                With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceTimeline)(Function(e) e.ProductionOutsourceServiceTimelineList, False, False, "")
                                  .Style.Width = "70%"
                                  .AddClass("pull-right")
                                  With .FirstRow
                                    .AddReadOnlyColumn(Function(d As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) d.ServiceStartDateTime)
                                    .AddReadOnlyColumn(Function(d As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) d.ServiceEndDateTime)
                                    With .AddColumn("Breakfast?")
                                      With .Helpers.BootstrapStateButton("", "", "CaterBreakfastCss($data)", "CaterBreakfastHtml($data)", False)
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                    With .AddColumn("Lunch?")
                                      With .Helpers.BootstrapStateButton("", "", "CaterLunchCss($data)", "CaterLunchHtml($data)", False)
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                    With .AddColumn("Dinner?")
                                      With .Helpers.BootstrapStateButton("", "", "CaterDinnerCss($data)", "CaterDinnerHtml($data)", False)
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                  End With
                                End With
                                With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceDetail)(Function(d) d.ProductionOutsourceServiceDetailList, False, False, "")
                                  .Style.Width = "50%"
                                  .AddClass("pull-right")
                                  With .FirstRow
                                    .AddColumn(Function(d) d.ProductionOutsourceServiceDetail)
                                    With .AddColumn("Required?")
                                      With .Helpers.BootstrapStateButton("", "", "ServiceDetailRequiredIndCss($data)", "ServiceDetailRequiredIndHtml($data)", False)
                                        .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Services.ProductionOutsourceService)(Function(c As OBLib.Productions.Areas.ProductionSystemArea) c.ProductionOutsourceServiceList, False, True, "")
                                .RemoveButton.RemoveClass("btn-primary")
                                .RemoveButton.AddClass("btn-sm btn-danger")
                                With .FirstRow
                                  With .AddColumn(Function(c) c.OutsourceServiceTypeID)
                                  End With
                                  .AddColumn(Function(c) c.SupplierID)
                                  .AddColumn(Function(c) c.Description)
                                  .AddColumn(Function(c) c.QuoteRequestedDate)
                                  .AddColumn(Function(c) c.QuoteReceivedDate)
                                  .AddColumn(Function(c) c.QuotedAmount)
                                  'With .AddColumn("Required")
                                  '  .Helpers.BootstrapStateButton("", "NotRequiredIndClick($data)", "NotRequiredIndCss($data)", "NotRequiredIndHtml($data)", False)
                                  'End With
                                  '.AddColumn(Function(c) c.NotRequiredReason)
                                  .AddColumn(Function(c) c.SAPOrderNo)
                                End With
                                With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceTimeline)(Function(e) e.ProductionOutsourceServiceTimelineList, True, True, "")
                                  .Style.Width = "70%"
                                  .AddClass("pull-right")
                                  .AddNewButton.AddClass("btn-sm")
                                  .RemoveButton.RemoveClass("btn-primary")
                                  .RemoveButton.AddClass("btn-sm btn-danger")
                                  With .FirstRow
                                    With .AddColumn("Service Starts")
                                      With .Helpers.EditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceStartDateTime)
                                        .Style.Width = 90
                                      End With
                                      With .Helpers.TimeEditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceStartDateTime)
                                        .Style.Width = 60
                                      End With
                                    End With
                                    With .AddColumn("Service Ends")
                                      With .Helpers.EditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceEndDateTime)
                                        .Style.Width = 90
                                      End With
                                      With .Helpers.TimeEditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceEndDateTime)
                                        .Style.Width = 60
                                      End With
                                    End With
                                    With .AddColumn("Breakfast?")
                                      With .Helpers.BootstrapStateButton("", "CaterBreakfastClick($data)", "CaterBreakfastCss($data)", "CaterBreakfastHtml($data)", False)
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "IsCatering($data)")
                                      End With
                                      'With .Helpers.BootstrapROStateButton(Function(d As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) d.CateringBreakfastInd,
                                      '          "btn-success", "glyphicon-check", "Yes",
                                      '          "btn-default", "glyphicon-minus", "No")
                                      'End With
                                    End With
                                    With .AddColumn("Lunch?")
                                      With .Helpers.BootstrapStateButton("", "CaterLunchClick($data)", "CaterLunchCss($data)", "CaterLunchHtml($data)", False)
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "IsCatering($data)")
                                      End With
                                    End With
                                    With .AddColumn("Dinner?")
                                      With .Helpers.BootstrapStateButton("", "CaterDinnerClick($data)", "CaterDinnerCss($data)", "CaterDinnerHtml($data)", False)
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "IsCatering($data)")
                                      End With
                                    End With
                                  End With
                                End With
                                With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceDetail)(Function(d) d.ProductionOutsourceServiceDetailList, True, True, "")
                                  .Style.Width = "50%"
                                  .AddClass("pull-right")
                                  .AddNewButton.AddClass("btn-sm")
                                  .RemoveButton.RemoveClass("btn-primary")
                                  .RemoveButton.AddClass("btn-sm btn-danger")
                                  With .FirstRow
                                    .AddColumn(Function(d) d.ProductionOutsourceServiceDetail)
                                    With .AddColumn("Required?")
                                      .Helpers.BootstrapStateButton("", "ServiceDetailRequiredIndClick($data)", "ServiceDetailRequiredIndCss($data)", "ServiceDetailRequiredIndHtml($data)", False)
                                    End With
                                  End With
                                End With
                                With .FooterRow
                                  With .AddColumn("")
                                    .ColSpan = 9
                                    With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewOutsourceService()")
                                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionOutsourceService()")
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Crew Tab----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewCrewInd Then
              With .AddTab("Crew", "fa fa-group", False, "SetProductionCrewTabsVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
                    With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(v As OBLib.Productions.Old.Production) v.ProductionSystemAreaList)
                      With .Helpers.DivC("row ProductionCrewTab")
                        .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.ProductionCrewTabVisible)
                        With .Helpers.If(Function(d As ProductionSystemArea) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled) Or d.CanEditHumanResources <> "" Or Not ViewModel.CurrentProduction.CanEditHumanResourcesAfterFinalised)
                          With .Helpers.DivC("field-box")
                            With .Helpers.EditorFor(Function(p) ViewModel.PHRFilter)
                              .Attributes("placeholder") = "Filter..."
                              .AddClass("form-control filter-field")
                              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                              .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterPHR}")
                            End With
                          End With
                          'Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionHumanResourceList
                          .Helpers.DivC("top-buffer-10")
                          With .Helpers.BootstrapTableFor(Of ProductionHumanResource)("ProductionHumanResourceListSorted()", False, False, "$data.Visible()")
                            With .FirstRow
                              .AddBinding(Singular.Web.KnockoutBindingString.css, "PHRCss($data)")
                              .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Discipline)
                              .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Position)
                              .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.PositionType)
                              .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.HumanResource)
                              .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.PrefHumanResource)
                              .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.VehicleName)
                            End With
                          End With
                        End With
                        With .Helpers.If(Function(d As ProductionSystemArea) (Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled)) AndAlso d.CanEditHumanResources = "")
                          '-------------------------------------------PHR List--------------------------------------------
                          With .Helpers.DivC("ROProductionHumanResourceList")
                            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.PHRListVisible)
                            With .Helpers.BootstrapPanel
                              With .PanelHeading
                                .Helpers.HTML("Crew")
                              End With
                              With .PanelBody
                                With .Helpers.Div
                                  '-------------------------------------------Heading-------------------------------------------
                                  With .Helpers.BootstrapButton(, "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddNewProductionHumanResource()")
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionHumanResource()")
                                  End With
                                  With .Helpers.BootstrapButton("PopulateHumanResources", "Populate HR", "btn-sm btn-default", "glyphicon-cog", Singular.Web.PostBackType.Ajax, False, , , )
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanPopulateHR()")
                                  End With
                                  If Singular.Security.HasAccess("Productions.Can Copy Crew") AndAlso ViewModel.CurrentSystemID = OBLib.CommonData.Enums.System.ProductionContent Then
                                    With .Helpers.Bootstrap.Button(, "Copy Crew", Singular.Web.BootstrapEnums.Style.DefaultStyle, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-copy", , Singular.Web.PostBackType.None, "SetupCopyCrew()")

                                    End With
                                  End If
                                End With
                                With .Helpers.DivC("field-box")
                                  With .Helpers.EditorFor(Function(p) ViewModel.PHRFilter)
                                    .Attributes("placeholder") = "Filter..."
                                    .AddClass("form-control filter-field")
                                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterPHR}")
                                  End With
                                End With
                                'PopulateHumanResources
                                .Helpers.DivC("top-buffer-10")
                                With .Helpers.DivC("table-responsive")
                                  With .Helpers.BootstrapTableFor(Of ProductionHumanResource)(Function(d As OBLib.Productions.Areas.ProductionSystemArea) d.ProductionHumanResourceList, False, False, "")
                                    With .FirstRow
                                      With .AddColumn(Function(d As ProductionHumanResource) d.DisciplineID)
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditPHRs($data)")
                                      End With
                                      With .AddColumn(Function(d As ProductionHumanResource) d.PositionID)
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditPHRs($data)")
                                      End With
                                      With .AddColumn(Function(d As ProductionHumanResource) d.PositionTypeID)
                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditPHRs($data)")
                                      End With
                                      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
                                        With .AddColumn(Function(d As ProductionHumanResource) d.VehicleID)
                                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditPHRs($data)")
                                        End With
                                      End If
                                      With .AddColumn("Human Resource")
                                        .Style.Width = "160px"
                                        .AddClass("GroupButtonOverride")

                                        With .Helpers.DivC("input-group")
                                          '.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As ProductionHumanResource) d.CoreCrewDisciplineInd)
                                          .AddBinding(Singular.Web.KnockoutBindingString.visible, "CanEditPHRs($data)")
                                          With .Helpers.HTMLTag("span")
                                            .AddBinding(Singular.Web.KnockoutBindingString.css, "GetHRCss($data)")
                                            .AddBinding(Singular.Web.KnockoutBindingString.click, "ShowPHRDetail($data)")

                                            With .Helpers.HTMLTag("i")
                                              .AddClass("fa fa-user")
                                            End With
                                            With .Helpers.HTMLTag("span")
                                              .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As ProductionHumanResource) d.HumanResource)
                                            End With
                                          End With
                                          With .Helpers.HTMLTag("span")
                                            .AddClass("input-group-btn")
                                            .Helpers.BootstrapButton(, , "btn-custom btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "ClearHumanResourceID($data)")
                                          End With
                                        End With
                                        With .Helpers.Div
                                          .AddBinding(Singular.Web.KnockoutBindingString.visible, "!CanEditPHRs($data)")
                                          With .Helpers.HTMLTag("span")
                                            .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As ProductionHumanResource) d.HumanResource)
                                          End With
                                        End With


                                      End With
                                      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
                                        With .AddColumn("Preferred HR")
                                          .Style.Width = "160px"
                                          .AddClass("GroupButtonOverride")
                                          With .Helpers.DivC("input-group")
                                            With .Helpers.HTMLTag("span")
                                              .AddBinding(Singular.Web.KnockoutBindingString.css, "GetPrefHRCss($data)")
                                              '.AddClass("btn-custom btn-primary ProductionCrewHumanResource")
                                              .AddBinding(Singular.Web.KnockoutBindingString.click, "ShowPHRDetail($data)")
                                              With .Helpers.HTMLTag("i")
                                                .AddClass("fa fa-user")
                                              End With
                                              With .Helpers.HTMLTag("span")
                                                .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As ProductionHumanResource) d.PrefHumanResource)
                                              End With
                                            End With
                                            With .Helpers.HTMLTag("span")
                                              .AddClass("input-group-btn")
                                              .Helpers.BootstrapButton(, , "btn-custom btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "ClearPrefHumanResourceID($data)")
                                            End With
                                          End With
                                        End With
                                      End If
                                      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
                                        With .AddColumn("Leader?")
                                          .Helpers.BootstrapStateButton("", "CrewLeaderClick($data)", "CrewLeaderCss($data)", "CrewLeaderHtml($data)", False)
                                        End With
                                        With .AddColumn("Relief?")
                                          .Helpers.BootstrapStateButton("", "ReliefCrewClick($data)", "ReliefCrewCss($data)", "ReliefCrewHtml($data)", False)
                                        End With
                                        With .AddColumn("Supplied?")
                                          .Helpers.BootstrapStateButton("", "SuppliedHumanResourceClick($data)", "SuppliedHumanResourceCss($data)", "SuppliedHumanResourceHtml($data)", False)
                                        End With
                                      End If
                                      With .AddColumn("TBC?")
                                        .Helpers.BootstrapStateButton("", "TBCClick($data)", "TBCCss($data)", "TBCHtml($data)", False)
                                      End With
                                      With .AddColumn("Training?")
                                        .Helpers.BootstrapStateButton("", "TrainingClick($data)", "TrainingCss($data)", "TrainingHtml($data)", False)
                                      End With
                                      With .AddColumn("")
                                        With .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "DeleteProductionHumanResource($data)")
                                          .Button.Style.Margin(, , , "10px")
                                          .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanDeleteProductionHumanResource()")
                                        End With
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Schedules Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewScheduleInd Then
              With .AddTab("Schedule", "fa fa-list", False, "SetProductionScheduleTabsVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
                    With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(v As OBLib.Productions.Old.Production) v.ProductionSystemAreaList)
                      With .Helpers.DivC("row ProductionCrewSchedule")
                        .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.SchedulesTabVisible)

                        With .Helpers.BootstrapPanel
                          .Panel.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(vm) ViewModel.HRListVisible)
                          With .PanelHeading
                            .Helpers.HTML("Crew")
                          End With
                          With .PanelBody
                            With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled Or Not ViewModel.CurrentProduction.CanEditCrewSchedulesAfterFinalised))
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Schedules.HRProductionSchedule)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.HRProductionScheduleList, False, False, "$data.Visible()")
                                .AddClass("ParentList")
                                With .FirstRow
                                  '.AddBinding(Singular.Web.KnockoutBindingString.css, "ScheduleDetailCss($data)")
                                  With .AddColumn("")
                                    .Helpers.BootstrapButton(, "View", "btn-xs btn-primary", "glyphicon-search", Singular.Web.PostBackType.None, False, , , "ViewReadOnlySchedule($data)")
                                    '"GetReadOnlySchedule($data)"
                                  End With
                                  With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.HumanResource)
                                    .AddClass("HumanResource bold")
                                  End With
                                  With .AddReadOnlyColumn(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.Disciplines)

                                  End With
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.CityCode)
                                  .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.Local)
                                End With
                                With .AddChildTable(Of OBLib.Productions.Schedules.HRProductionScheduleDetail)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.HRProductionScheduleDetailListClient, False, False, "")
                                  With .FirstRow
                                    With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.ProductionTimelineType)
                                    End With
                                    With .AddColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.StartDateTime)
                                      .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                    End With
                                    With .AddColumn("Start Time")
                                      With .Helpers.TimeEditorFor(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.StartDateTime)
                                        .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                    With .AddColumn("End Time")
                                      With .Helpers.TimeEditorFor(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.EndDateTime)
                                        .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                    With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.Detail)
                                    End With

                                    With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.InvoiceNo)
                                    End With
                                    'With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.Clash)
                                    'End With
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled) AndAlso d.CanEditHumanResources = "")
                              'HR Schedule List---------------------------------------------------------------------------------------------------------------------------------------------------
                              With .Helpers.DivC("HRProductionScheduleList top-buffer-10") 'row
                                With .Helpers.Div 'C("row")
                                  '"GenerateSchedules()"
                                  '-------------------------------------------Heading-------------------------------------------
                                  With .Helpers.Div
                                    .AddBinding(Singular.Web.KnockoutBindingString.visible, "CanEditCrewSchedulesAfterFinalised($data)")
                                    .Helpers.BootstrapButton("ClearSchedules", "Clear Schedules", "btn-sm btn-warning", "glyphicon-remove-sign", Singular.Web.PostBackType.Full, False, , , )
                                    .Helpers.BootstrapButton("GenerateSchedules", "Generate Schedules", "btn-sm btn-primary", "glyphicon-cog", Singular.Web.PostBackType.Full, False, , , )
                                    .Helpers.BootstrapButton(, "Bulk Update", "btn-sm btn-info", "glyphicon-edit", Singular.Web.PostBackType.None, False, , , "SetupBulkUpdate()")
                                    .Helpers.BootstrapButton(, "Bulk Add", "btn-sm btn-success", "glyphicon-plus-sign", Singular.Web.PostBackType.None, False, , , "SetupBulkAdd()")
                                    .Helpers.BootstrapButton(, "Bulk Delete", "btn-sm btn-danger", "glyphicon-remove-sign", Singular.Web.PostBackType.None, False, , , "SetupBulkDelete()")
                                    If ViewModel.SystemID = 1 AndAlso ViewModel.CurrentProductionAreaID = 1 Then
                                      .Helpers.BootstrapButton(, "Missing HR", "btn-sm btn-warning", "fa fa-reorder", Singular.Web.PostBackType.None, False, , , "ShowMissingShifts()")
                                    End If
                                    '.Helpers.BootstrapButton(, "Validate Schedule", "btn-sm btn-default", "glyphicon-refresh", Singular.Web.PostBackType.None, False, , , )
                                  End With

                                End With
                                .Helpers.DivC("top-buffer-10")
                                With .Helpers.DivC("field-box")
                                  With .Helpers.EditorFor(Function(p) ViewModel.HRListFilter)
                                    .Attributes("placeholder") = "Filter..."
                                    .AddClass("form-control filter-field")
                                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterHRList}")
                                  End With
                                End With
                                .Helpers.DivC("top-buffer-10")
                                With .Helpers.ForEachTemplate(Of OBLib.Productions.Schedules.HRProductionSchedule)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.HRProductionScheduleList)

                                  With .Helpers.BootstrapDivColumn(12, 12, 12)
                                    .AddBinding(Singular.Web.KnockoutBindingString.css, "HRProductionScheduleCss($data)")
                                    .AddBinding(Singular.Web.KnockoutBindingString.if, Function(d As OBLib.Productions.Schedules.HRProductionSchedule) d.Visible)
                                    .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As OBLib.Productions.Schedules.HRProductionSchedule) d.Visible)
                                    With .Helpers.BootstrapDivColumn(12, 2, 1)
                                      With .Helpers.BootstrapButton(, "", "btn-xs btn-danger", "glyphicon-exclamation-sign", Singular.Web.PostBackType.None, False, , , )
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsValid()")
                                      End With
                                      With .Helpers.BootstrapButton(, "Edit", "btn-xs btn-primary", "glyphicon-edit", Singular.Web.PostBackType.None, False, , , "ShowHRSchedule($data)")
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.ScheduleVisible() && !$data.IsBusyClient() && CanEditSchedule($data)")
                                      End With
                                      With .Helpers.BootstrapButton(, "Done", "btn-xs btn-primary", "glyphicon-check", Singular.Web.PostBackType.None, False, , , "HideHRSchedule($data)")
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.ScheduleVisible() && !$data.IsBusyClient()")
                                      End With
                                      With .Helpers.HTMLTag("span")
                                        .AddClass("btn btn-xs btn-warning")
                                        .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsBusyClient()")
                                        With .Helpers.HTMLTag("i")
                                          .AddClass("fa fa-cog fa-lg fa-spin")
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Helpers.HTML("Updating...")
                                        End With
                                      End With
                                    End With
                                    With .Helpers.BootstrapDivColumn(12, 2, 2)
                                      .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.HumanResource).AddClass("bold")
                                    End With
                                    With .Helpers.BootstrapDivColumn(12, 2, 2)
                                      .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.Disciplines).AddClass("bold")
                                    End With
                                    'With .Helpers.BootstrapDivColumn(12, 4, 1)
                                    '  With .Helpers.DivC("field-box")
                                    '    With .Helpers.Div
                                    '      .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.CrewType).AddClass("bold")
                                    '    End With
                                    '  End With
                                    'End With
                                    With .Helpers.BootstrapDivColumn(12, 2, 1)
                                      .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.CityCode).AddClass("bold")
                                    End With
                                    With .Helpers.BootstrapDivColumn(12, 2, 1)
                                      With .Helpers.BootstrapStateButton("", "", "HRLocalCss($data)", "HRLocalHtml($data)", False)
                                        .Button.AddClass("bold")
                                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                      End With
                                    End With
                                    If ViewModel.IncludeBiometricDetails Then
                                      With .Helpers.BootstrapDivColumn(12, 2, 1)
                                        .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.AccessFlagCountDescription)
                                        .AddBinding(KnockoutBindingString.css, "AccessFlagIDCSS($data)")
                                        .AddBinding(KnockoutBindingString.visible, Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.IsOBs)
                                      End With

                                      With .Helpers.BootstrapDivColumn(12, 2, 1)
                                        With .Helpers.BootstrapStateButton("RemoveHRProductionSchedule", "RemoveHRScheduleClick($data)", "RemoveHRScheduleCss($data)", "RemoveHRScheduleHTML($data)", False)
                                          With .Button
                                            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.AccessHRBookingList.Count > 0)
                                            .AddClass("bold")
                                            .AddBinding(KnockoutBindingString.visible, Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.IsOBs)

                                          End With
                                        End With
                                      End With

                                    End If
                                    With .Helpers.BootstrapDivColumn(12, 1, 1)
                                      With .Helpers.HTMLTag("span")
                                        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.HasPreAndPostTravel()")
                                        .AddClass("btn btn-xs btn-warning")
                                        With .Helpers.HTMLTag("span")
                                          With .Helpers.HTMLTag("i")
                                            .AddClass("fa fa-warning")
                                          End With
                                        End With
                                        With .Helpers.HTMLTag("span")
                                          .Helpers.HTML("no pre, post travel. Days away possible")
                                        End With
                                      End With
                                    End With
                                    With .Helpers.BootstrapDivColumn(12, 12, 12)
                                      .Helpers.DivC("top-buffer-10")
                                      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(d As OBLib.Productions.Schedules.HRProductionSchedule) d.ScheduleVisible)
                                      With .Helpers.BootstrapDivColumn(12, 12, 12)
                                        .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.IsBusyClient()")
                                        With .Helpers.DivC("hr-schedule-busy")
                                          With .Helpers.HTMLTag("div")
                                            With .Helpers.HTMLTag("i")
                                              .AddClass("fa fa-cog fa-spin fa-10x text-primary")
                                            End With
                                          End With
                                          With .Helpers.HTMLTag("div")
                                            With .Helpers.HTMLTag("h1")
                                              .AddClass("text-primary")
                                              .Helpers.HTML("Updating.....")
                                            End With
                                          End With
                                        End With
                                      End With
                                      With .Helpers.BootstrapDivColumn(12, 12, 7)
                                        With .Helpers.Bootstrap.Row
                                          .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsBusyClient()")
                                          .Helpers.DivC("top-buffer-10")
                                          With .Helpers.BootstrapTableFor(Of OBLib.Productions.Schedules.HRProductionScheduleDetail)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.HRProductionScheduleDetailListClient, False, False, "")
                                            With .FirstRow
                                              '.AddBinding(Singular.Web.KnockoutBindingString.css, "ScheduleDetailCss($data)")
                                              With .AddColumn("Select")
                                                With .Helpers.BootstrapStateButton("", "DetailSelectClick($data)", "DetailSelectCss($data)", "DetailSelectHtml($data)", False)
                                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.disable, "DisableHRScheduleDetailSelect($data)") 'Function(d As OBLib.Productions.Schedules.HRProductionScheduleDetail) (ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled))
                                                End With
                                              End With
                                              With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.ProductionTimelineType)
                                              End With
                                              With .AddColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.StartDateTime)
                                                .AddClass("Date")
                                                .Style.Width = 100
                                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditScheduleDetail($data, $element, 'ScheduleDate')")
                                              End With
                                              With .AddColumn("Start Time")
                                                With .Helpers.TimeEditorFor(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.StartDateTime)
                                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditScheduleDetail($data, $element, 'StartDateTime')")
                                                  .AddClass("Time")
                                                  .Style.Width = 40
                                                End With
                                                .Style.Width = 65
                                              End With
                                              With .AddColumn("End Time")
                                                With .Helpers.TimeEditorFor(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.EndDateTime)
                                                  .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditScheduleDetail($data, $element, 'EndDateTime')")
                                                  .AddClass("Time")
                                                  .Style.Width = 40
                                                End With
                                                .Style.Width = 65
                                              End With
                                              With .AddColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.Detail)
                                                .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditScheduleDetail($data, $element, 'Detail')")
                                              End With
                                              With .AddColumn("Timesheet?")
                                                With .Helpers.BootstrapStateButton("", "", "TimesheetIndCss($data)", "TimesheetIndHtml($data)", False)
                                                End With
                                              End With
                                              With .AddColumn("Invoice?")
                                                With .Helpers.BootstrapStateButton("", "", "InvoiceIndCss($data)", "InvoiceIndHtml($data)", False)
                                                End With
                                              End With
                                              'With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.Clash)
                                              '  .AddClass("Clash text-danger")
                                              'End With
                                            End With
                                          End With
                                          'If Not (ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled) Then
                                          With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                                            .Helpers.BootstrapButton(, "Delete Selected", "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "DeleteSelectedScheduleDetails($data)")
                                          End With
                                          'End If
                                        End With
                                        If ViewModel.IncludeBiometricDetails Then
                                          With .Helpers.Bootstrap.Row
                                            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(v As OBLib.Productions.Schedules.HRProductionSchedule) Not v.IsRemoved AndAlso v.IsOBs)
                                            .Helpers.LabelFor(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.AccessShiftList).AddClass("bold")
                                            With .Helpers.BootstrapTableFor(Of OBLib.Biometrics.AccessShift)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.AccessShiftList, False, False, "")
                                              With .FirstRow
                                                .AddReadOnlyColumn(Function(d As OBLib.Biometrics.AccessShift) d.ShiftDate, "Shift Date")

                                                With .AddColumn("Start time")
                                                  With .Helpers.TimeEditorFor(Function(d As OBLib.Biometrics.AccessShift) d.StartDateTime)
                                                    .AddClass("Time")
                                                    .Style.Width = 40
                                                  End With
                                                  .Style.Width = 65
                                                End With
                                                With .AddColumn("End time")
                                                  With .Helpers.TimeEditorFor(Function(d As OBLib.Biometrics.AccessShift) d.EndDateTime)
                                                    .AddClass("Time")
                                                    .Style.Width = 40
                                                  End With
                                                  .Style.Width = 65
                                                End With
                                                With .AddReadOnlyColumn(Function(d As OBLib.Biometrics.AccessShift) d.FlagDecription, "Flag")
                                                  .CellBindings.Add(Singular.Web.KnockoutBindingString.style, "$data.IsLoading() ? 'CellWait' : ''")
                                                  .Style.Width = 300
                                                End With
                                                .AddColumn(Function(d As OBLib.Biometrics.AccessShift) d.PlannedTimeChangedReason, "Flag reason")
                                              End With
                                            End With
                                          End With

                                          '--- Missing Biometrics Bookings
                                          With .Helpers.Bootstrap.Row
                                            'With .Helpers.LabelFor(
                                            .Helpers.LabelFor(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.AccessHRBookingList).AddClass("bold")
                                            .AddBinding(Singular.Web.KnockoutBindingString.visible, Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.IsRemoved)
                                            With .Helpers.BootstrapTableFor(Of OBLib.Biometrics.AccessHRBooking)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.AccessHRBookingList, False, False, "")
                                              With .FirstRow
                                                .AddReadOnlyColumn(Function(d) d.HRBookingType)
                                                .AddReadOnlyColumn(Function(d) d.Description)
                                                .AddReadOnlyColumn(Function(d) d.DriverInd)
                                                .AddReadOnlyColumn(Function(d) d.CoDriverInd)


                                                With .AddColumn("Cancel")
                                                  With .Helpers.BootstrapStateButton("", "HRCancelbtnClick($data)", "HRCancelbtnCss($data)", "HRCancelbtnHtml($data)", False)
                                                    With .Button
                                                      .Style.Width = 80
                                                    End With
                                                  End With
                                                  .Style.Width = 90
                                                End With
                                                With .AddColumn(Function(d) d.CancelledReason)

                                                End With
                                                With .AddColumn(Function(d) d.RentalCarReplacementHRID)
                                                  With .Editor
                                                    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) d.HRBookingTypeID = 3)
                                                  End With
                                                End With
                                              End With
                                            End With
                                          End With
                                        End If
                                      End With
                                      With .Helpers.BootstrapDivColumn(12, 12, 5)
                                        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!$data.IsBusyClient()")
                                        .Helpers.DivC("top-buffer-10")
                                        With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                                          With .Helpers.BootstrapTableFor(Of OBLib.Helpers.Templates.UnSelectedTimeline)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.UnSelectedProductionTimelineList, False, False, "$data.Visible()")
                                            With .FirstRow
                                              With .AddColumn("Select")
                                                With .Helpers.BootstrapStateButton("", "NewTimelineSelectClick($data)", "NewTimelineSelectCss($data)", "NewTimelineSelectHtml($data)", False)
                                                  '.Button.AddBinding(Singular.Web.KnockoutBindingString.disable, Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.TimesheetMonthClosedInd)
                                                  .Button.AddBinding(Singular.Web.KnockoutBindingString.disable, "DisableUnSelectedTimelineSelect($data)")
                                                End With
                                              End With
                                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.ProductionTimelineType)
                                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.StartDate)
                                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.StartDateTime)
                                              .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.EndDateTime)
                                            End With
                                          End With
                                          .Helpers.BootstrapButton("", "Add Seleted", "btn-xs btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddSelectedTimelines($data)")
                                        End With
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Comments Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewCommentsInd Then
              With .AddTab("Comments", "fa fa-comments-o", False, "SetProductionCommentSectionTabVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(d) ViewModel.CurrentProduction)
                    With .Helpers.DivC("row ProductionCommentSectionList")
                      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.CommentSectionTabVisible)

                      With .Helpers.BootstrapButton("GenerateComments", "Generate Comments", "btn-sm btn-primary", "", Singular.Web.PostBackType.Ajax, False, , , )
                        '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionCommentSection()")
                      End With

                      With .Helpers.DivC("")
                        .Style.Height = 15
                      End With

                      With .Helpers.BootstrapPanel
                        With .PanelHeading
                          .Helpers.HTML("Production Comments")
                        End With
                        With .PanelBody
                          With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                            With .Helpers.BootstrapTableFor(Of OBLib.Productions.ProductionCommentSection)(Function(c As OBLib.Productions.Old.Production) c.ProductionCommentSectionList, False, False, "")
                              With .FirstRow
                                .AddReadOnlyColumn(Function(c) c.ProductionCommentReportSectionID)
                              End With
                              ''''ProductionCommentHeaderList
                              With .AddChildTable(Of OBLib.Productions.ProductionCommentHeader)(Function(e) e.ProductionCommentHeaderList, False, False, "")
                                .Style.Width = "90%"
                                .AddClass("pull-right")
                                With .FirstRow
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.ProductionCommentHeader) d.ProductionCommentHeader)
                                End With
                                ''''ProductionCommentList
                                With .AddChildTable(Of OBLib.Productions.ProductionComment)(Function(d) d.ProductionCommentList, False, False, "")
                                  .Style.Width = "90%"
                                  .AddClass("pull-right")
                                  With .FirstRow
                                    .AddReadOnlyColumn(Function(d) d.ProductionComment)
                                    .AddReadOnlyColumn(Function(d) d.AdditionalComment)
                                  End With
                                End With
                              End With

                            End With
                          End With
                          With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                            With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewProductionCommentSection()")
                              '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionCommentSection()")
                            End With
                            .Helpers.DivC("top-buffer-10")
                            With .Helpers.BootstrapTableFor(Of OBLib.Productions.ProductionCommentSection)(Function(c As OBLib.Productions.Old.Production) c.ProductionCommentSectionList, False, True, "")
                              '.AddNewButton.AddClass("btn-sm")
                              .RemoveButton.RemoveClass("btn-primary")
                              .RemoveButton.AddClass("btn-sm btn-danger")
                              With .FirstRow
                                .AddColumn(Function(c) c.ProductionCommentReportSectionID)
                              End With
                              With .AddChildTable(Of OBLib.Productions.ProductionCommentHeader)(Function(e) e.ProductionCommentHeaderList, True, True, "")
                                .Style.Width = "90%"
                                .AddClass("pull-right")
                                .AddNewButton.AddClass("btn-sm")
                                .RemoveButton.RemoveClass("btn-primary")
                                .RemoveButton.AddClass("btn-sm btn-danger")
                                With .FirstRow
                                  .AddColumn(Function(d As OBLib.Productions.ProductionCommentHeader) d.ProductionCommentHeader)
                                End With
                                With .AddChildTable(Of OBLib.Productions.ProductionComment)(Function(d) d.ProductionCommentList, True, True, "")
                                  .Style.Width = "90%"
                                  .AddClass("pull-right")
                                  .Attributes("id") = "ProductionCommentsTable"
                                  .AddNewButton.AddClass("btn-sm")
                                  .RemoveButton.RemoveClass("btn-primary")
                                  .RemoveButton.AddClass("btn-sm btn-danger")
                                  .AddNewButton.AddBinding(Singular.Web.KnockoutBindingString.click, "AddNewPC($data)")
                                  .RemoveButton.AddBinding(Singular.Web.KnockoutBindingString.click, "RemovePC($data)")
                                  With .FirstRow
                                    .AddClass("comment-row")
                                    .AddColumn(Function(d) d.ProductionComment)
                                    .AddColumn(Function(d) d.AdditionalComment)
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Audio Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewAudioInd Then
              If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
                With .AddTab("Audio", "fa fa-headphones", False, "SetProductionAudioTabVisible()")
                  With .TabPane
                    .AddClass("top-buffer-10")
                    With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(d) ViewModel.CurrentProduction)
                      With .Helpers.DivC("row ProductionAudioConfigurationList")
                        .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.AudioSectionTabVisible)
                        With .Helpers.BootstrapPanel
                          With .PanelHeading
                            .Helpers.HTML("Production Audio")
                          End With
                          With .PanelBody
                            With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.ProductionAudioConfiguration)(Function(c As OBLib.Productions.Old.Production) c.ProductionAudioConfigurationList, False, False, "")
                                .RemoveClass("Grid")
                                .AddClass("table table-striped table-bordered table-hover table-condensed ProductionAudioConfigurationList ParentTable")
                                .FirstRow.AddReadOnlyColumn(Function(c) c.AudioConfigurationID)
                                With .AddChildTable(Of ProductionAudioConfigurationDetail)(Function(d) d.ProductionAudioConfigurationDetailList, False, False, "")
                                  .RemoveClass("Grid")
                                  .AddClass("table table-striped table-bordered table-hover table-condensed ProductionAudioConfigurationDetailList ChildTable")
                                  .FirstRow.AddReadOnlyColumn(Function(d) d.AudioConfigurationDetail)
                                End With
                              End With
                            End With
                            With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled))
                              With .Helpers.TableFor(Of ProductionAudioConfiguration)(Function(c) c.ProductionAudioConfigurationList, True, True)
                                .RemoveClass("Grid")
                                .AddClass("table table-striped table-bordered table-hover table-condensed ProductionAudioConfigurationList ParentTable")
                                .FirstRow.AddColumn(Function(c) c.AudioConfigurationID)
                                With .AddChildTable(Of ProductionAudioConfigurationDetail)(Function(d) d.ProductionAudioConfigurationDetailList, True, True)
                                  .RemoveClass("Grid")
                                  .AddClass("table table-striped table-bordered table-hover table-condensed ProductionAudioConfigurationDetailList ChildTable")
                                  .FirstRow.AddColumn(Function(d) d.AudioConfigurationDetail)
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End If
            End If

            'Correspondence Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewCorrespondenceInd Then
              With .AddTab("Correspondence", "fa fa-folder-open-o", False, "SetCorrespondenceTabVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(d) ViewModel.CurrentProduction)
                    With .Helpers.DivC("row CorrespondenceTab")
                      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.CorrespondenceTabVisible)

                      With .Helpers.BootstrapDivColumn(12, 12, 12)
                        With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Documents")
                            End With
                            With .PanelBody
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionDocument)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionDocumentList, False, False, "")
                                .Style.Width = "65%"
                                .AddClass("pull-right")
                                With .FirstRow
                                  '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.SystemID)
                                  '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.ProductionAreaID)
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.DocumentTypeID)
                                  With .AddColumn("")
                                    .Helpers.DocumentDownloader()
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Documents")
                            End With
                            With .PanelBody
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionDocument)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionDocumentList, False, True, "")
                                '.AddNewButton.AddClass("btn-sm")
                                .RemoveButton.RemoveClass("btn-primary")
                                .RemoveButton.AddClass("btn-xs btn-danger")
                                .Style.Width = "65%"
                                .AddClass("pull-right")
                                With .FirstRow
                                  '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.SystemID)
                                  '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.ProductionAreaID)
                                  .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.DocumentTypeID)
                                  With .AddColumn("")
                                    .Helpers.DocumentManager()
                                  End With
                                End With
                                With .FooterRow
                                  With .AddColumn("")
                                    .ColSpan = 3
                                    .Helpers.BootstrapButton(, "New Document", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, , , , "AddNewProductionDocument()")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.BootstrapDivColumn(12, 12, 12)
                        With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Comments")
                            End With
                            With .PanelBody
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionGeneralComment)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionGeneralCommentList, False, False, "")
                                .Style.Width = "65%"
                                .AddClass("pull-right")
                                With .FirstRow
                                  '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.SystemID)
                                  '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.ProductionAreaID)
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.CommentDate)
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.Comment)
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Comments")
                            End With
                            With .PanelBody
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionGeneralComment)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionGeneralCommentList, False, True, "")
                                .Style.Width = "65%"
                                .AddClass("pull-right")
                                '.AddNewButton.AddClass("btn-sm")
                                .RemoveButton.RemoveClass("btn-primary")
                                .RemoveButton.AddClass("btn-xs btn-danger")
                                With .FirstRow
                                  '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.SystemID)
                                  '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.ProductionAreaID)
                                  .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.CommentDate)
                                  .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.Comment)
                                End With
                                With .FooterRow
                                  With .AddColumn("")
                                    .ColSpan = 3
                                    .Helpers.BootstrapButton(, "New Document", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, , , , "AddNewProductionGeneralComment()")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With

                      With .Helpers.BootstrapDivColumn(12, 12, 12)
                        With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Petty Cash")
                            End With
                            With .PanelBody
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionPettyCash)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionPettyCashList, False, False, "")
                                .Style.Width = "65%"
                                .AddClass("pull-right")
                                With .FirstRow
                                  '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.SystemID)
                                  '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionAreaID)
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionPettyCashTypeID)
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Amount)
                                  .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Description)
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                          With .Helpers.BootstrapPanel
                            With .PanelHeading
                              .Helpers.HTML("Petty Cash")
                            End With
                            With .PanelBody
                              With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionPettyCash)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionPettyCashList, False, True, "")
                                .Style.Width = "65%"
                                .AddClass("pull-right")
                                '.AddNewButton.AddClass("btn-sm")
                                .RemoveButton.RemoveClass("btn-primary")
                                .RemoveButton.AddClass("btn-xs btn-danger")
                                With .FirstRow
                                  '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.SystemID)
                                  '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionAreaID)
                                  .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionPettyCashTypeID)
                                  .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Amount)
                                  .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Description)
                                End With
                                With .FooterRow
                                  With .AddColumn("")
                                    .ColSpan = 4
                                    .Helpers.BootstrapButton(, "New Document", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, , , , "AddNewProductionPettyCash()")
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If

            'Travel Tab------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            If ViewModel.CurrentProduction.CanViewTravelInd Then
              With .AddTab("Travel", "fa fa-road", False, "SetTravelRequisitionTabVisible()")
                With .TabPane
                  .AddClass("top-buffer-10")
                  With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(d) ViewModel.CurrentProduction)
                    With .Helpers.DivC("row TravelRequisitionList")
                      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.TravelTabVisible)
                      With .Helpers.BootstrapPanel
                        With .PanelHeading
                          .Helpers.HTML("Travel Requisition")
                        End With
                        With .PanelBody
                          With .Helpers.If(Function(d) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                            With .Helpers.BootstrapTableFor(Of OBLib.Travel.TravelRequisition)(Function(c As OBLib.Productions.Old.Production) c.TravelRequisitionList, False, False, "")
                              With .FirstRow
                                'With .AddColumn()
                                '  .Style.Width = "105px"
                                '  With .Helpers.LinkFor(Function(c) "../Productions/Travel/CrewMembers.aspx?TravelRequisitionID=" & c.TravelRequisitionID, , "", "")
                                '    .AddClass("btn btn-primary btn-xs")
                                '    With .Helpers.HTMLTag("span")
                                '      .AddClass("glyphicon glyphicon-search")
                                '    End With
                                '    .Helpers.HTML("View Travel")
                                '  End With
                                'End With
                                With .AddColumn("")
                                  .Style.Width = 105
                                  .Helpers.BootstrapButton(, "Edit Travel", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.Ajax, , , , "ProductionHelper.Travel.Methods.EditSelectedTravelReq($data)")
                                End With
                                .AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.VersionNo)
                                .AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.EventTitle)
                                .AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.System)
                                .AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.ProductionArea)
                                '.AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.CreatedByUser)
                              End With
                            End With
                          End With
                          With .Helpers.If(Function(d) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
                            .Helpers.DivC("top-buffer-10")
                            With .Helpers.BootstrapTableFor(Of OBLib.Travel.TravelRequisition)(Function(c As OBLib.Productions.Old.Production) c.TravelRequisitionList, False, False, "")
                              With .FirstRow
                                With .AddColumn("")
                                  .Style.Width = 105
                                  With .Helpers.BootstrapButton(, "Edit Travel", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.Ajax, , , , "ProductionHelper.Travel.Methods.EditSelectedTravelReq($data)")
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditTravel($data)")
                                  End With
                                End With
                                '.AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.ProductionDescription)
                                With .AddColumn(Function(c As OBLib.Travel.TravelRequisition) c.SystemID)
                                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditRequisitionSystemID($data)")
                                End With
                                With .AddColumn(Function(c As OBLib.Travel.TravelRequisition) c.ProductionAreaID)
                                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditRequisitionSystemID($data)")
                                End With
                                With .AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.VersionNo)

                                End With
                                If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
                                  With .AddColumn(Function(c As OBLib.Travel.TravelRequisition) c.CrewTypeID)
                                    .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanEditRequisitionCrewTypeID($data)")
                                  End With
                                End If
                                '.AddReadOnlyColumn(Function(c As OBLib.Travel.TravelRequisition) c.CreatedByUser)
                              End With
                              With .FooterRow
                                With .AddColumn("")
                                  .ColSpan = 5
                                  If (Singular.Security.HasAccess("Travel", "Can Add Travel Requisition") AndAlso Not Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent))) Or OBLib.Security.Settings.CurrentUserID = 152 Then
                                    .Helpers.BootstrapButton(, "New Requisition", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, , , , "AddNewTravelRequisition()")
                                  End If
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If
            With .AddTab("Reports", "fa fa-files-o", False, "SetReportsTabVisible()")
              With .TabPane
                .AddClass("top-buffer-10")
                With .Helpers.DivC("row ReportsTab")
                  .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.ReportsTabVisible)
                  With .Helpers.BootstrapPanel
                    With .PanelHeading
                      .Helpers.HTML("Reports")
                    End With
                    With .PanelBody
                      .Helpers.BootstrapButton("Schedule", "Schedule", "btn-sm btn-flat btn-default", "glyphicon-print", Singular.Web.PostBackType.Full, True)
                      .Helpers.BootstrapButton("SignIn", "PA Form Sign In", "btn-sm btn-flat btn-default", "glyphicon-print", Singular.Web.PostBackType.Full, True)
                      If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
                        .Helpers.BootstrapButton("SignOut", "PA Form Sign Out", "btn-sm btn-flat btn-default", "glyphicon-print", Singular.Web.PostBackType.Full, True)
                        .Helpers.BootstrapButton("SnT", "S&T Report", "btn-sm btn-flat btn-default", "glyphicon-print", Singular.Web.PostBackType.Full, True)
                        .Helpers.BootstrapButton("Cost", "Cost Report", "btn-sm btn-flat btn-default", "glyphicon-print", Singular.Web.PostBackType.Full, True)
                      End If
                    End With
                  End With
                End With
              End With
            End With

          End With
        End With

        With .Helpers.BootstrapDialog("HRSearch", "Search")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "CanViewSkillsModal()")
          .ModalDialogDiv.AddClass("modal-md")
          '.AddBinding(Singular.Web.KnockoutBindingString.dialog, Function(d) ViewModel.SelectedPHRGuid <> "")
          With .Body
            With .Helpers.BootstrapPanel
              With .PanelHeading
                .Helpers.HTML("Search")
              End With
              With .PanelBody
                With .Helpers.With(Of ProductionHumanResource)("SelectedPHR()")
                  With .Helpers.BootstrapDivColumn(12, 12, 12)
                    With .Helpers.BootstrapDivColumn(12, 3, 3)
                      With .Helpers.DivC("field-box")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.FilteredProductionTypeInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "FilteredProductionTypeClick($data)", "FilteredProductionTypeCss($data)", "FilteredProductionTypeHtml($data)", False)
                        End With
                      End With
                    End With
                    With .Helpers.BootstrapDivColumn(12, 3, 3)
                      With .Helpers.DivC("field-box")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.FilteredPositionInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "FilteredPositionClick($data)", "FilteredPositionCss($data)", "FilteredPositionHtml($data)", False)
                        End With
                      End With
                    End With
                    With .Helpers.BootstrapDivColumn(12, 3, 3)
                      With .Helpers.DivC("field-box")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.IncludedOffInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "IncludedOffClick($data)", "IncludedOffCss($data)", "IncludedOffHtml($data)", False)
                        End With
                      End With
                    End With
                    With .Helpers.BootstrapDivColumn(12, 3, 3)
                      .Helpers.BootstrapButton("", "Search", "btn-sm btn-primary", "glyphicon-search", Singular.Web.PostBackType.None, False, , , "GetQualifiedHR($data)")
                    End With
                  End With
                  .Helpers.BootstrapDivColumn(12, 12, 12).AddClass("top-buffer-10")
                End With
                With .Helpers.BootstrapDivColumn(12, 12, 12)
                  With .Helpers.EditorFor(Function(p) ViewModel.ROSkilledHRListCriteria.FilterName)
                    .Attributes("placeholder") = "Filter..."
                    .AddClass("form-control filter-field")
                    .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                    .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterQualifiedHR($data) }")
                  End With
                  .Helpers.DivC("top-buffer-10")
                  With .Helpers.DivC("scroll-content")
                    '.AddBinding(Singular.Web.KnockoutBindingString.if, Function(d As ProductionHumanResource) Not d.FetchingQualifiedHR)
                    With .Helpers.PagedGridFor(Of ROSkilledHR)(Function(d) ViewModel.ROSkilledHRListPagingInfo, Function(c) ViewModel.ROSkilledHRList, False, False)
                      With .BtnFirst
                        .AddClass("btn btn-xs btn-primary")
                        .Helpers.HTML("<i class='fa fa-angle-double-left'></i> First")
                      End With
                      With .BtnPrev
                        .AddClass("btn btn-xs btn-primary")
                        .Helpers.HTML("<i class='fa fa-angle-left'></i> Prev")
                      End With
                      With .BtnNext
                        .AddClass("btn btn-xs btn-primary")
                        .Helpers.HTML("Next <i class='fa fa-angle-right'></i>")
                      End With
                      With .BtnLast
                        .AddClass("btn btn-xs btn-primary")
                        .Helpers.HTML("Last <i class='fa fa-angle-double-right'></i>")
                      End With
                      With .FirstRow
                        With .AddColumn("")
                          .HeaderStyle.Width = "200px"
                          .Style.Width = "200px"
                          .Helpers.BootstrapButton(, "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetPrimaryHR($data)")
                          .Helpers.BootstrapButton(, "Preferred", "btn-xs btn-default", "glyphicon-thumbs-up", Singular.Web.PostBackType.None, False, , , "SetPreferredHR($data)")
                        End With
                        .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.HumanResource)
                        .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ContractType)
                        .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.PrimaryInd)
                        .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.CityCode)
                        '.AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ProductionCount)
                        .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.CrewScheduleHours)
                        .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.TimesheetHours)
                        If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
                          '.AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.DaysWorked)
                          .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ContractDays)
                        End If
                      End With
                    End With
                    'With .Helpers.BootstrapTableFor(Of ROSkilledHR)(Function(vm) vm.ROSkilledHRList, False, False, "$data.VisibleInd()")
                    '  With .FirstRow
                    '    With .AddColumn("")
                    '      .Helpers.BootstrapButton(, "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetPrimaryHR($data)")
                    '      .Helpers.BootstrapButton(, "Preferred", "btn-xs btn-default", "glyphicon-thumbs-up", Singular.Web.PostBackType.None, False, , , "SetPreferredHR($data)")
                    '    End With
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.HumanResource)
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ContractType)
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.PrimaryInd)
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.CityCode)
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ProductionCount)
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.CrewScheduleHours)
                    '    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.TimesheetHours)
                    '    If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
                    '      .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.DaysWorked)
                    '      .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ContractDays)
                    '    End If
                    '  End With
                    'End With
                  End With
                End With
              End With
            End With
          End With
        End With

        With .Helpers.BootstrapDialog("BulkModal", "Bulk Edit")
          '.AddBinding(Singular.Web.KnockoutBindingString.visible, "CanViewBulkModal()")
          .ModalDialogDiv.AddClass("modal-xl")
          With .Body
            .AddClass("row")
            With .Helpers.BootstrapDivColumn(12, 12, 12)
              With .Helpers.BootstrapDivColumn(6, 6, 6)
                With .Helpers.BootstrapPanel
                  With .PanelHeading
                    .Helpers.HTML("Human Resources")
                  End With
                  With .PanelBody
                    With .Helpers.BootstrapTableFor(Of OBLib.Helpers.Templates.DisciplineGroup)(Function(d) ViewModel.BulkEditDetails.DisciplineGroupList, False, False, "")
                      With .FirstRow
                        With .AddColumn("Updated?")
                          With .Helpers.BootstrapStateButton("", "ProductionHelper.BulkEdit.BulkEditDisciplineUpdateClick($data)", "ProductionHelper.BulkEdit.BulkEditDisciplineUpdateCss($data)", "ProductionHelper.BulkEdit.BulkEditDisciplineUpdateHtml($data)", False)
                          End With
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.DisciplineGroup) d.Discipline)

                        End With
                      End With
                      With .AddChildTable(Of OBLib.Helpers.Templates.HumanResourceItem)(Function(s As OBLib.Helpers.Templates.DisciplineGroup) s.HumanResources, False, False, "!$data.HiddenInd()")
                        With .FirstRow
                          With .AddColumn("Updated?")
                            With .Helpers.BootstrapStateButton("", "ProductionHelper.BulkEdit.BulkEditHRUpdateClick($data)", "ProductionHelper.BulkEdit.BulkEditHRUpdateCss($data)", "ProductionHelper.BulkEdit.BulkEditHRUpdateHtml($data)", False)
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.HumanResourceItem) d.HumanResource)

                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.HumanResourceItem) d.HasErrorsInd)

                          End With
                          'With .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.HumanResourceItem) d.Errors)

                          'End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.BootstrapDivColumn(6, 6, 6)
                With .Helpers.BootstrapPanel
                  With .PanelHeading
                    .Helpers.HTML("Timelines")
                  End With
                  With .PanelBody
                    With .Helpers.BootstrapTableFor(Of OBLib.Helpers.Templates.ProductionTimelineDay)(Function(d) ViewModel.BulkEditDetails.ProductionTimelineDayList, False, False, "")
                      With .FirstRow
                        With .AddColumn("Update?")
                          With .Helpers.BootstrapStateButton("", "ProductionHelper.BulkEdit.BulkEditTimelineDayUpdateClick($data)", "ProductionHelper.BulkEdit.BulkEditTimelineDayUpdateCss($data)", "ProductionHelper.BulkEdit.BulkEditTimelineDayUpdateHtml($data)", False)
                          End With
                        End With
                        With .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.ProductionTimelineDay) d.DayString)

                        End With
                      End With
                      With .AddChildTable(Of OBLib.Helpers.Templates.ProductionTimelineItem)(Function(s As OBLib.Helpers.Templates.ProductionTimelineDay) s.ProductionTimelineItems, False, False, "")
                        With .FirstRow
                          '.AddBinding(Singular.Web.KnockoutBindingString.if, "$data.HiddenInd()")
                          With .AddColumn("Update?")
                            With .Helpers.BootstrapStateButton("", "ProductionHelper.BulkEdit.BulkEditTimelineUpdateClick($data)", "ProductionHelper.BulkEdit.BulkEditTimelineUpdateCss($data)", "ProductionHelper.BulkEdit.BulkEditTimelineUpdateHtml($data)", False)
                            End With
                          End With
                          With .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.ProductionTimelineItem) d.TimelineType)

                          End With
                          With .AddColumn(Function(d As OBLib.Helpers.Templates.ProductionTimelineItem) d.StartDateTime)

                          End With
                          With .AddColumn("Start Time")
                            .Helpers.TimeEditorFor(Function(d As OBLib.Helpers.Templates.ProductionTimelineItem) d.StartDateTime)
                          End With
                          With .AddColumn("End Time")
                            .Helpers.TimeEditorFor(Function(d As OBLib.Helpers.Templates.ProductionTimelineItem) d.EndDateTime)
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

          End With
          With .Footer
            With .Helpers.Div
              .AddClass("pull-right")
              With .Helpers.BootstrapButton(, "Update", "btn-sm btn-info", "glyphicon-edit", Singular.Web.PostBackType.None, False, , , "CommitBulkUpdate()")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.BulkEditMode = "Update")
              End With
              With .Helpers.BootstrapButton(, "Add", "btn-sm btn-success", "glyphicon-plus-sign", Singular.Web.PostBackType.None, False, , , "CommitBulkAdd()")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.BulkEditMode = "Add")
              End With
              With .Helpers.BootstrapButton(, "Delete", "btn-sm btn-danger", "glyphicon-remove-sign", Singular.Web.PostBackType.None, False, , , "CommitBulkDelete()")
                .Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d) ViewModel.BulkEditMode = "Delete")
              End With
            End With
          End With
        End With

        'With .Helpers.BootstrapDialog("ProductionPlanners", "Production Planners")
        '  .ModalDialogDiv.AddClass("modal-sm")
        '  With .Body
        '    With .Helpers.BootstrapDivColumn(12, 12, 12, 12)
        '      .AddClass("custom-error-box-container")
        '      With .Helpers.DivC("custom-error-box")
        '        .AddBinding(Singular.Web.KnockoutBindingString.visible, "!IsProductionHRPlannerValid()")
        '        .AddBinding(Singular.Web.KnockoutBindingString.html, "GetProductionHRPlannerBrokenRulesHTML()")
        '      End With
        '    End With
        '    .AddClass("row")
        '    With .Helpers.BootstrapDivColumn(12, 12, 12)
        '      With .Helpers.BootstrapPanel
        '        With .PanelHeading
        '          .Helpers.HTML("Human Resources")
        '        End With
        '        With .PanelBody
        '          With .Helpers.With(Of OBLib.Productions.Old.Production)(Function(v) ViewModel.CurrentProduction)
        '            With .Helpers.ForEachTemplate(Of ProductionSystemArea)(Function(v As OBLib.Productions.Old.Production) v.ProductionSystemAreaList)
        '              With .Helpers.DivC("row ProductionPlanners")
        '                With .Helpers.If(Function(d As ProductionSystemArea) (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
        '                  With .Helpers.BootstrapTableFor(Of ProductionHRPlanner)(Function(c) c.ProductionHRPlannerList, False, False, "")
        '                    With .FirstRow
        '                      .AddReadOnlyColumn(Function(c As ProductionHRPlanner) c.HumanResourceID)
        '                      .AddReadOnlyColumn(Function(c As ProductionHRPlanner) c.DisciplineID)
        '                      .AddReadOnlyColumn(Function(c As ProductionHRPlanner) c.PlanningHours)
        '                    End With
        '                  End With
        '                End With
        '                With .Helpers.If(Function(d As ProductionSystemArea) Not (ViewModel.CurrentProduction.SystemAreaReconciled Or ViewModel.CurrentProduction.SystemAreaCancelled))
        '                  With .Helpers.BootstrapTableFor(Of ProductionHRPlanner)(Function(c) c.ProductionHRPlannerList, True, True, "")
        '                    With .FirstRow
        '                      '.AddColumn(Function(c As ProductionHRPlanner) c.HumanResourceID)
        '                      With .AddColumn("Human Resource")
        '                        .Style.Width = "120px"
        '                        .AddClass("GroupButtonOverride")
        '                        With .Helpers.DivC("input-group")
        '                          With .Helpers.HTMLTag("span")
        '                            .AddBinding(Singular.Web.KnockoutBindingString.css, "GetPlannerHRCss($data)")
        '                            .AddBinding(Singular.Web.KnockoutBindingString.click, "FindROHumanResource($data)")
        '                            With .Helpers.HTMLTag("i")
        '                              .AddClass("fa fa-user")
        '                            End With
        '                            With .Helpers.HTMLTag("span")
        '                              .AddBinding(Singular.Web.KnockoutBindingString.html, Function(d As ProductionHRPlanner) d.HumanResource)
        '                            End With
        '                          End With
        '                          With .Helpers.HTMLTag("span")
        '                            .AddClass("input-group-btn")
        '                            .Helpers.BootstrapButton(, , "btn-custom btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "ClearHumanResourceID($data)")
        '                          End With
        '                        End With
        '                      End With
        '                      .AddColumn(Function(c As ProductionHRPlanner) c.DisciplineID)
        '                      .AddColumn(Function(c As ProductionHRPlanner) c.PlanningHours)
        '                    End With
        '                  End With
        '                End With

        '              End With
        '            End With
        '          End With
        '        End With
        '      End With
        '    End With
        '  End With
        'End With

        'RO Human Resource Select
        With .Helpers.BootstrapDialog("SelectROHumanResource", "Select Manager")
          With .Body
            With .Helpers.DivC("field-box")
              With .Helpers.EditorFor(Function(p) ViewModel.ROHumanResourceFilter)
                .Attributes("placeholder") = "Filter..."
                .AddClass("form-control filter-field")
                .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROHumanResource}")
              End With
            End With
            .Helpers.HTML.Gap()
            With .Helpers.DivC("row ROHumanResourceList")
              With .Helpers.BootstrapTableFor(Of OBLib.HR.ReadOnly.ROHumanResource)(Function(d) ViewModel.ROHumanResourceList, False, False, "") '"$data.VisibleClient()")
                .AddClass("ROHumanResourceList")
                With .FirstRow
                  With .AddColumn("")
                    .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetManagerHumanResourceID($data)")
                  End With
                  .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResource) hr.Firstname)
                  .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResource) hr.PreferredName)
                  .AddReadOnlyColumn(Function(hr As OBLib.HR.ReadOnly.ROHumanResource) hr.Surname)
                End With
              End With
            End With
          End With
        End With

      End With

      With h.Bootstrap.Dialog("CopyCrew", "Copy Crew")
        .ModalDialogDiv.AddClass("modal-xl")
        With .Body
          .AddClass("row")
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 6, 3, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateFrom)
                    .AddClass("form-control input-sm")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.EditorFor(Function(d) d.ROProductionListCriteria.TxDateTo)
                    .AddClass("form-control input-sm")
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 2)
                With .Helpers.Bootstrap.FormControlFor(Function(d) ViewModel.ROProductionListCriteria.Keyword, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Attributes("placeholder") = "Search by Keyword"
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterCopyCrewProductions }")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 3, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button(, "Search", Singular.Web.BootstrapEnums.Style.Info, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-search", , Singular.Web.PostBackType.None, "FilterCopyCrewProductions()")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ViewModel.ROProductionListPagingManager,
                                                                    Function(vm) ViewModel.ROProductionList,
                                                                    False, False, True, False, True, True, False,
                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
                .AddClass("no-border hover list")
                .TableBodyClass = "no-border-y"
                .Pager.PagerListTag.ListTag.AddClass("pull-left")
                With .FirstRow
                  .AddBinding(Singular.Web.KnockoutBindingString.click, "CopyCrewProductionRowClicked($data)")
                  .AddClass("items")
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                    .AddClass("col-xs-3 col-sm-3 col-md-3 col-lg-4")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                  With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                    .AddClass("col-xs-1 col-sm-1 col-md-1 col-lg-1")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.Bootstrap.TableFor(Of ProductionCrew)(Function(d) ViewModel.CopyCrewProductionCrewList, False, False, False, False, True, True, False) '"$data.VisibleClient()")
              With .FirstRow
                With .AddColumn("Copy?")
                  .AddClass("col-sm-1 col-md-1 col-lg-1")
                  With .Helpers.BootstrapStateButton("", "CopyCrewMemberCycleState($data)", "CopyCrewMemberCss($data)", "CopyCrewMemberHtml($data)", False)
                  End With
                End With
                With .AddReadOnlyColumn(Function(hr As ProductionCrew) hr.Discipline)
                  .AddClass("col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(hr As ProductionCrew) hr.Position)
                  .AddClass("col-sm-1 col-md-1 col-lg-1")
                End With
                With .AddReadOnlyColumn(Function(hr As ProductionCrew) hr.HumanResource)
                  .AddClass("col-sm-1 col-md-1 col-lg-1")
                End With
              End With
            End With
          End With
        End With
        With .Footer
          If Singular.Security.HasAccess("Productions.Can Copy Crew") Then
            With .Helpers.Bootstrap.Button(, "Submit", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-copy", , Singular.Web.PostBackType.None, "SubmitCopyCrew()")

            End With
          End If
        End With
      End With

      ''Biometrics
      With h.Div()
        .AddClass("modal-dg big-text-mode black-text")
        .Attributes("id") = "MissingShiftModal"



        With .Helpers.Bootstrap.FlatBlock("Missing Shifts", True)
          .FlatBlockTag.AddClass("flat-block-paged")
          With .AboveContentTag
            With .Helpers.MessageHolder("Access Shift")
            End With
            With .Helpers.Bootstrap.Button("CreateMissingShifts", "Save / Create Shifts", BootstrapEnums.Style.Primary,
                   , BootstrapEnums.ButtonSize.Small, , "fa-save", , PostBackType.None)
              With .Button
                .AddBinding(KnockoutBindingString.click, "CreateSelectedMissingShifts()")
              End With
            End With


          End With
          With .ContentTag
            With .Helpers.DivC("table-responsive")
              With .Helpers.Bootstrap.TableFor(Of AccessShift)(Function(c) ViewModel.AccessMissingShiftList, False, False)

                .AddClass("no-border hover list")
                '.TableBodyClass = "no-border-y"

                With .FirstRow
                  With .AddColumn("Select")
                    .Style.Width = 90
                    .Helpers.BootstrapStateButton("", "SelectMissingShift($data)", "MissingShiftCss($data)", "MissingShiftHtml($data)", False)
                  End With


                  With .AddReadOnlyColumn(Function(d) d.HumanResource)
                    '.AddClass("col-xs-2")
                  End With
                  With .AddColumn(Function(d) d.ShiftStartDateTime, 100)
                    .Style.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddColumn("Start Time")
                    .Style.TextAlign = Singular.Web.TextAlign.center
                    .AddClass("Time")
                    With .Helpers.TimeEditorFor(Function(d) d.ShiftStartDateTime)
                      .Style.Width = 50

                    End With
                  End With
                  With .AddColumn(Function(d) d.ShiftEndDateTime, 100)

                    .Style.TextAlign = Singular.Web.TextAlign.center
                  End With
                  With .AddColumn("End Time")
                    .AddClass("Time")
                    .Style.TextAlign = Singular.Web.TextAlign.center
                    With .Helpers.TimeEditorFor(Function(c) c.ShiftEndDateTime)
                      .Style.Width = 50
                    End With
                  End With
                  With .AddColumn(Function(d) d.TotalShiftHours, 50, "Hours")
                  End With
                  'With .AddColumn("Shift")

                  '  '.Style.Width = 150
                  '  .Style.TextAlign = Singular.Web.TextAlign.center
                  '  .AddClass("btn-group")
                  '  'With .Helpers.BootstrapStateButton("", "HumanResourceShiftHelper.Methods.MissingShiftValid($data)", "HumanResourceShiftHelper.Methods.MissingShiftValidCss($data)", "HumanResourceShiftHelper.Methods.MissingShiftValidhtml()", False)
                  '  '  With .Button
                  '  '    .Style.Width = 70
                  '  '  End With
                  '  'End With
                  '  'With .Helpers.BootstrapStateButton("", "HumanResourceShiftHelper.Methods.MissingShiftRejected($data)", "HumanResourceShiftHelper.Methods.MissingShiftRejectedCss($data)", "HumanResourceShiftHelper.Methods.MissingShiftRejectedhtml()", False)
                  '  '  With .Button
                  '  '    .Style.Width = 70
                  '  '  End With
                  '  'End With

                  'End With

                  With .AddColumn(Function(d) d.DisciplineID)
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Using
  %>

  <script type="text/javascript">

    function VisionViewIndCss(Production) {
      if (Production.VisionViewInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
    };

    function VisionViewIndHtml(Production) {
      if (Production.VisionViewInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
    };

    function CanViewOBFacilitySuppliers(Production) {
      return Production.CanViewOBFacilitySuppliersInd();
    }

    function OBFacilitySuppliersHTML(Production) {
      return Production.OBFacilitySuppliers();
    }

    function GraphicsSuppliersHTML(Production) {
      return Production.GraphicsSuppliers();
    }
    function CityHTML(Production) {
      return Production.City();
    }

    sUtil = {
      getAbsoluteLeft: function (elem) {
        return elem.getBoundingClientRect().left;
      },
      getAbsoluteTop: function (elem) {
        return elem.getBoundingClientRect().top;
      },
      getTarget: function (event) {
        // code from http://www.quirksmode.org/js/events_properties.html
        if (!event) {
          event = window.event;
        }
        var target;
        if (event.target) {
          target = event.target;
        } else if (event.srcElement) {
          target = event.srcElement;
        }
        if (target.nodeType != undefined && target.nodeType == 3) {
          // defeat Safari bug
          target = target.parentNode;
        }
        return target;
      },
      getEventProperties: function (event) {
        var elem = sUtil.getTarget(event);
        var xPos = sUtil.getAbsoluteLeft(elem); //clientX - 
        var yPos = sUtil.getAbsoluteTop(elem); //clientY - 
        return {
          event: event,
          element: elem,
          x: xPos,
          y: yPos
        }
      }
    };

    var commentsDropDown = null,
      currentBO = null;

    function onCommentCellFocused(event) {
      var props = sUtil.getEventProperties(event);
      var elemPos = $(props.element).position();

      var yVal = (elemPos.top - 400).toString();
      var xVal = elemPos.left.toString();
      commentsDropDown.style.display = 'block';
      commentsDropDown.style.top = yVal + 'px';
      commentsDropDown.style.minWidth = '1234px';
      commentsDropDown.style.left = xVal + 'px'
      commentsDropDown.style.visibility = 'visible';
      currentBO = ko.dataFor(props.element);
    };

    function onCommentCellLostFocus(event) {
      var props = sUtil.getEventProperties(event);
      commentsDropDown.style.visibility = 'none';
    };

    function onEventOptionSelected(event) {
      var option = event.target;
      var busObj = option['business-object'];
      currentBO.ProductionComment(busObj.ProductionCommentHeader);
      commentsDropDown.style.visibility = 'hidden';
    };

    function filterDropDown(event) {
      var input = event.target;
      var busObj = ko.dataFor(input);

      for (var i = 0; i < commentsDropDown.children[0].children.length; i++) {
        var item = commentsDropDown.children[0].children[i];
        if (input.value == '') {
          //item.style.visibility = 'visible';
          item.style.display = 'block';
        } else {
          var childObj = item['business-object'];
          if (childObj.ProductionCommentHeader.indexOf(input.value) >= 0) {
            //item.style.visibility = 'visible';
            item.style.display = 'block';
          } else {
            //item.style.visibility = 'hidden';
            item.style.display = 'none';
          }
        }
      }
    };

    function createDropDown() {

      commentsDropDown = document.createElement('div');
      commentsDropDown.id = "CommentsDropDown"
      commentsDropDown.className = "ComboDropDown";
      commentsDropDownItemContainer = document.createElement('div');
      commentsDropDown.appendChild(commentsDropDownItemContainer);
      commentsDropDown.style.height = '450px';
      commentsDropDown.style.position = 'absolute';
      commentsDropDown.style.zIndex = '1000';
      commentsDropDown.style.visibility = 'hidden';
      commentsDropDown.style.overflowY = 'auto';
      commentsDropDown.style.display = 'block';

      //add options
      ClientData.ROProductionCommentHeaderList.Iterate(function (cmnt, cI) {
        var option = document.createElement('div');
        option.className = 'comment-option';
        option['business-object'] = cmnt;
        option.innerHTML = cmnt.ProductionCommentHeader;
        option.onclick = onEventOptionSelected;
        commentsDropDownItemContainer.appendChild(option);
      });

      $('#Comments').append(commentsDropDown);

    };

    Singular.OnPageLoad(function () {

      createDropDown();
      $('#Comments').on('focus', 'tr.comment-row > td:first-child > input', onCommentCellFocused);
      $('#Comments').on('focusout', 'tr.comment-row > td:first-child > input', onCommentCellLostFocus);
      $('#Comments').on('keyup', 'tr.comment-row > td:first-child > input', filterDropDown);
      //$('div.comment-option').on('click', onEventOptionSelected);

      //$('div.comment-option').on('click', function (event) {
      //  var option = event.target;
      //  var busObj = option['business-object'];
      //  currentBO.ProductionComment(busObj.ProductionCommentHeader);
      //});

    });

    //---------- Biometrics
    function BiometricslogsCss(HRProductionScheduleDetail) {

      if (HRProductionScheduleDetail.AccessFlagID() > 0) {
        return "btn btn-xs btn-danger";
      }
      else {
        return "btn btn-xs btn-success";
      }
    }
    function AccessFlagIDCSS(HRProductionSchedule) {

      if (HRProductionSchedule.AccessFlagCount() > 0) {
        if (HRProductionSchedule.IsAccessFlagInformational() == true || IsClientFlaginformaional(HRProductionSchedule) == true) {
          return "InformationFlagsFont";
        }
        return "FlagsFont";
      }
      else {
        return "NoFlagsFont";
      }
    }

    function IsClientFlaginformaional(ClientSchedule) {
      var MakeFlagInformationalWithReason = true

      ClientSchedule.AccessShiftList().Iterate(function (aShift, Index) {
        if (aShift.AccessFlagID() > 0 && aShift.PlannedTimeChangedReason() == "") {
          MakeFlagInformationalWithReason = false;
          return MakeFlagInformationalWithReason;
        }
      });
      return MakeFlagInformationalWithReason;
    }

    function RemoveHRScheduleCss(HRSchedule) {
      if (HRSchedule.IsRemoved()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }

    };
    function RemoveHRScheduleHTML(HRSchedule) {
      if (HRSchedule.IsRemoved()) {
        return "Removed"
      }
      return 'Remove'
    };
    function RemoveHRScheduleClick(HRSchedule) {
      HRSchedule.IsRemoved(!HRSchedule.IsRemoved());
    };

    function HRCancelbtnClick(AccessHRBooing) {
      AccessHRBooing.IsCancelled(!AccessHRBooing.IsCancelled());
    };
    function HRCancelbtnHtml(AccessHRBooing) {
      if (AccessHRBooing.IsCancelled()) { return "<span class='glyphicon glyphicon-check'></span> Cancelled" }
      else { return "<span class='glyphicon glyphicon-minus'></span> Cancel" }
    };

    function HRCancelbtnCss(AccessHRBooing) {
      if (AccessHRBooing.IsCancelled()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
    };

    function Accessshift(self) {

      self.IsLoading(true);
      self.SyncID(self.SyncID() + 1);

      ViewModel.CallServerMethod('GetShiftFlagValues', { AccessShiftID: self.AccessShiftID(), StartDate: self.ChangedShiftStartDateTime(), EndDate: self.ChangedShiftEndDateTime(), SyncID: self.SyncID() }, function (result) {

        if (self.SyncID() == result.Data.SyncID) {
          self.AccessFlagID(result.Data.AccessFlagID);
          self.FlagDecription(result.Data.FlagDescription);
          self.IsLoading(false);
        }
      });
    }
    function BiometricslogsHtml(HRProductionScheduleDetail) {

      if (HRProductionScheduleDetail.AccessFlagID() > 0) {
        return "flags";
      }
      else {
        return "logs";
      }
    }
    function CheckShiftType(Value, Rule, RuleArgs) {
      return;

    };
    function CheckDiscipline(Value, Rule, RuleArgs) {
      var AccessShiftOptionInd = RuleArgs.Object.AccessShiftOptionInd();
      if (!RuleArgs.IgnoreResults) {
        if (RuleArgs.Object.AccessShiftOptionInd() == true && (RuleArgs.Object.DisciplineID() == null || RuleArgs.Object.DisciplineID() == 0)) {
          RuleArgs.AddError("Discipline is required");
          return;
        }
      }
    };
    function CheckTimeChangedReason(Value, Rule, RuleArgs) {

      if (RuleArgs.Object.MustValidateAccessShift()) {
        if (!RuleArgs.Object.GetParent().IsRemoved()) {
          if (RuleArgs.Object.PlannedTimeChangedReason() == "" && RuleArgs.Object.AccessFlagID() > 0) {
            RuleArgs.AddError("Time Changed Reasons");
            return;
          }
        }
      }
    };
    function CheckCancelledReason(Value, Rule, RuleArgs) {


      if (RuleArgs.Object.CancelledReason() == "" && RuleArgs.Object.IsCancelled() == true) {
        RuleArgs.AddError("Cancel Reason is required");
        return;
      }
    };

    function FetchAccessTransactions(MonthAuthHumanResourceShift) {
      var StartTime = new Date(MonthAuthHumanResourceShift.StartDateTime());
      var EndTime = new Date(MonthAuthHumanResourceShift.EndDateTime());
      ViewModel.ROAccessTerminalShiftListCriteria().HumanResourceShiftID(MonthAuthHumanResourceShift.HumanResourceShiftID());
      ViewModel.ROAccessTerminalShiftListManager().Refresh();
      ViewModel.ROAccessShifFlagstList(MonthAuthHumanResourceShift.AccessFlagList());
      $("#ROAccesTransactionsModal").dialog({
        title: "Shift: " + StartTime.format("dd MMM yyyy HH:mm") + " - " + EndTime.format("dd MMM yyyy HH:mm"),
        closeText: "Close",
        height: 700,
        width: "80%",
        //modal: true,
        resizeable: true,
        open: function (event, ui) {


        },
        beforeClose: function (event, ui) {

        }
      })

    }
    function ShowMissingShifts() {

      $("#MissingShiftModal").dialog({
        title: "Missing Shift - " + ViewModel.MissingShiftsCount(),
        closeText: "Close",
        height: 700,
        width: "80%",
        resizeable: true,
        open: function (event, ui) {
          Singular.SendCommand('LoadBiometricsMissingShifts', {}, function (args) { })


        },
        beforeClose: function (event, ui) {

        }
      })
    }
    function GetAllowedDisciplines(List, Item) {
      var AllowedDisciplines = [];
      List.Iterate(function (Discipline, Index) {
        if (Discipline.SystemID == ViewModel.CurrentSystemID() && Discipline.ProductionAreaID == ViewModel.CurrentProductionAreaID()) {
          AllowedDisciplines.push(Discipline)
        } else {
          if (Item.DisciplineID() == Discipline.DisciplineID) {
            AllowedDisciplines.push(Discipline)
          }
        }
      })
      return AllowedDisciplines;
    };
    function CreateSelectedMissingShifts() {
      Singular.SendCommand('CreateMissingShifts', {}, function (args) {

        $("#MissingShiftModal").dialog('destroy');


      });
    };
    function SelectMissingShift(AccessShift) {
      AccessShift.AccessShiftOptionInd(!AccessShift.AccessShiftOptionInd());
    };
    function MissingShiftCss(MissingShift) {
      return (MissingShift.AccessShiftOptionInd() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
    };
    function MissingShiftHtml(MissingShift) {
      return (MissingShift.AccessShiftOptionInd() ? "<span class='glyphicon glyphicon-check'></span> Selected" : "<span class='glyphicon glyphicon-minus'></span> Select")
    }
  </script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions/Manual.js?v=50"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Productions/Production.js?v=50"></script>

</asp:Content>


<%--<div class="ComboDropDown" style="height: auto; display: block; top: 456.389px; overflow-y: auto; min-width: 1234px; left: 537.778px;">
  <div>
    <div>B Sky B Commentary position</div>
    <div class="Selected">Commentary positions</div>
    <div>Interview area</div>
    <div>Parking of OB horses</div>
    <div>Sky NZ Commentary position</div>
    <div>TMO position</div>
    <div>Accomodation</div>
    <div>Accomodation - Drivers</div>
    <div>Accomodation Rig crew and Ops crew (Jhb)</div>
    <div>Additional Information</div>
    <div>Afr. Commentary</div>
    <div>Afr. Commentary (Main Game)</div>
    <div>Afr. Commentary (U/19 &amp; U/ 21)</div>
    <div>Afr. Commentary (Vodacom Cup)</div>
    <div>Afrikaans commentary position</div>
    <div>ATM</div>
    <div>Broadacst</div>
    <div>Build Up Presenters</div>
    <div>Camera 3 position</div>
    <div>Camera Coordinator</div>
    <div>Cheetahs vs Lions </div>
    <div>Contacts:</div>
    <div>Crew Parking</div>
    <div>Currie Cup: Lions vs Sharks/U21 Lions vs Sharks</div>
    <div>Dress Code</div>
    <div>Eng. Commentary</div>
    <div>Eng. Commentary (Main Game)</div>
    <div>Eng. Commentary (U/19 &amp; U/21)</div>
    <div>Eng. Commentary (Vodacom Cup)</div>
    <div>English Commentary position</div>
    <div>EVS Coordinator</div>
    <div>Field Guest</div>
    <div>Field Guests</div>
    <div>Field Presenter</div>
    <div>Field Presenters</div>
    <div>Field Studio Guests</div>
    <div>Flash Interview Area</div>
    <div>Flash Interview Presenter</div>
    <div>ICR Telephone Numbers</div>
    <div>IsiXhosa Commentary</div>
    <div>Jimmy Jib</div>
    <div>Location Contact</div>
    <div>OB Tele-phone Numbers</div>
    <div>OB TelePhone Numbers</div>
    <div>On Site Contacts: SA Rugby</div>
    <div>OPS Crew Travel Arrangements</div>
    <div>Post Match Presenter</div>
    <div>Presenter</div>
    <div>Presenters</div>
    <div>RF Camera Positions</div>
    <div>Rig / Facility check Date</div>
    <div>Rig / Rehearsal Information</div>
    <div>Rig CrewTravel Arrangements</div>
    <div>Rig Date</div>
    <div>Rig Rehearse Date</div>
    <div>Ring Announcer</div>
    <div>Ring Anouncer</div>
    <div>Roving Presenter</div>
    <div>Roving Presenters</div>
    <div>SABC Commentary</div>
    <div>Side Line Commentary</div>
    <div>Side Line Commentary (Currie Cup)</div>
    <div>Side Line Field Presenter</div>
    <div>Sky Sports Commentary</div>
    <div>Sky Sports Side line Presentation area</div>
    <div>Sotho Commentary</div>
    <div>Special Arrangements</div>
    <div>Stats/graphics position</div>
    <div>Steady Cam</div>
    <div>Studio Field Guests</div>
    <div>Studio Field Presenter</div>
    <div>Studio Guests</div>
    <div>Studio Presenters</div>
    <div>Tape Stock</div>
    <div>Telephone Lines</div>
    <div>Telkom contact person</div>
    <div>Under 19: Lions vs Cheetahs </div>
    <div>Under 21: Lions vs Cheetahs </div>
    <div>Unilateral taker</div>
    <div>Vision Control Coordinator</div>
    <div>Xhosa Commentary</div>
    <div>Xhosa Commentary (Currie Cup)</div>
    <div>Xhosa Commentary - only 2nd game</div>
    <div>Zulu Commentary</div>
  </div>
</div>--%>
