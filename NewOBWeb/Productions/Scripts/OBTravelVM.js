﻿OBTravelVM = {
  getVMBrokenRulesHTML: function () {
    var ErrorString = ''
    if (ViewModel.IsValid() == false) {
      ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(ViewModel);
    }
    return ErrorString;
  },
  checkVMValidity: function () {
    var valid = true;
    //Singular.Validation.CheckRules(ViewModel);
    if (!ViewModel.IsValid()) {
      valid = false;
    }
    return valid;
  },
  pageHeaderDetails: function () {
    var heading = '';
    var ahtr = ViewModel.TravelRequisition();
    if (ahtr) {
      heading = '<h3>' + ahtr.ProductionDescription() + '</h3><br>'
    }
    else {
      heading = 'Ad Hoc Travel';
    }
    return heading;
  },
  FilterROCrewTypes: function (List, Item) {
    var Allowed = [];
    ClientData.ROCrewTypeList.Iterate(function (Item, Index) {
      if (Item.SystemID == ViewModel.CurrentSystemID()) {
        Allowed.push({ CrewTypeID: Item.CrewTypeID, TravelScreenDisplayName: Item.TravelScreenDisplayName });
      }
    });
    return Allowed;
  }, //FilterROCrewTypes
  Chauffeurs: {
    Methods: {
      // Chauffeurs
      RemoveChauffeur: function (Chauffeur) {
        if (Chauffeur.ChauffeurDriverHumanResourceList().length > 0 || Chauffeur.CancelledInd()) {
          var r = confirm("People are booked on this Chauffeur Drive, are you sure you want to remove it?");
          if (r == true)
            ViewModel.CurrentTravelRequisition().ChauffeurDriverList.Remove(Chauffeur);
        }
        else
          ViewModel.CurrentTravelRequisition().ChauffeurDriverList.Remove(Chauffeur);
      },
      IsChauffeurCancelled: function (Chauffeur) {
        Chauffeur.CancelledInd(!Chauffeur.CancelledInd());
        Chauffeur.CancelledReason('');
      }, //IsChauffeurCancelled
      IsChauffeurCancelledCss: function (Chauffeur) {
        if (Chauffeur.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //IsChauffeurCancelledCss
      IsChauffeurCancelledHtml: function (Chauffeur) {
        if (Chauffeur.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //IsChauffeurCancelledHtml
      ChauffeurMainPassengerValid: function () {
        var mainPassenCount = 0;
        var passengerCount = 0;
        ViewModel.BulkChauffeurPassengerList().Iterate(function (bcp, Idenx) {
          if (bcp.OnChauffeurCarInd() == true && bcp.CancelledInd() == false) {
            passengerCount++;
            if (bcp.MainPassengerInd() == true) {
              mainPassenCount++;
            }
          }
        });
        if (mainPassenCount == 0 && passengerCount > 0) {
          ViewModel.CurrentChauffeur().MainPassengerError('Main Passenger Required');
        }
        else {
          ViewModel.CurrentChauffeur().MainPassengerError('');
        }
      }, //ChauffeurMainPassengerValid
      OnChauffeurDriver: function (BulkChauffeurPassenger) {
        BulkChauffeurPassenger.OnChauffeurCarInd(!BulkChauffeurPassenger.OnChauffeurCarInd());
        TravelVM.Chauffeurs.Methods.ChauffeurMainPassengerValid();
      }, //OnChauffeurDriver
      OnCarCss: function (BulkChauffeurPassenger) {
        if (BulkChauffeurPassenger.OnChauffeurCarInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //OnCarCss
      OnCarHTML: function (BulkChauffeurPassenger) {
        if (BulkChauffeurPassenger.OnChauffeurCarInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span> On Car" } else { return "Select" }
      }, //OnCarHTML
      IsChauffeurHRValid: function () {
        var valid = true;
        if (ViewModel.CurrentChauffeur()) {
          if (!ViewModel.CurrentChauffeur().IsValid()) {
            valid = false
          }
        }
        ViewModel.BulkChauffeurPassengerList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            valid = false
          }
        });
        return valid;
      },//IsChauffeurHRValid
      IsChauffeurPassengerCancelled: function (BulkChauffeurPassenger) {
        if (BulkChauffeurPassenger.CancelledInd() == false) {
          return '';
        }
        else
          return BulkChauffeurPassenger.CancelledReason();
      }, //IsChauffeurPassengerCancelled
      GetChauffeurPassengersBrokenRulesHTML: function () {
        var ErrorString = ''
        if (ViewModel.CurrentChauffeur()) {
          if (!ViewModel.CurrentChauffeur().IsValid()) {
            ErrorString = ErrorString + "<ul><li><strong>Chauffeur Drive</strong><ul><li class=\"liVal\"><span class=\"liImg ImgValidation\"><label style=\"cursor: pointer;\">Main Passenger Required</label></span></li></ul></li></ul>";
          }
        }
        ViewModel.BulkChauffeurPassengerList().Iterate(function (brcp, Index) {
          if (brcp.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(brcp)
          }
        });
        return ErrorString;
      }, //GetChauffeurPassengersBrokenRulesHTML
      CancelledCDPassenger: function (BulkChauffeurPassenger) {
        BulkChauffeurPassenger.CancelledInd(!BulkChauffeurPassenger.CancelledInd());
        //var bccp = BulkChauffeurPassenger;
        //var cdhr = ViewModel.CurrentChauffeur().ChauffeurDriverHumanResourceList().Find("ChauffeurDriverHumanResourceID", bccp.ChauffeurDriverHumanResourceID());
        //cdhr.CancelledInd(bccp.CancelledInd());
        //cdhr.CancelledByUserID(null);
        //cdhr.CancelledDateTime(null);
        //cdhr.CancelledInd(bccp.CancelledInd());
        //cdhr.CancelledReason(bccp.CancelledReason());
        //if (BulkChauffeurPassenger.CancelledInd() == false) {
        //  cdhr.CancelledReason('');
        //}
        TravelVM.Chauffeurs.Methods.ChauffeurMainPassengerValid();
      }, //CancelledCDPassenger
      CancelledCDPassengerCss: function (BulkChauffeurPassenger) {
        if (BulkChauffeurPassenger.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //CancelledCDPassengerCss
      CancelledCDPassengerHtml: function (BulkChauffeurPassenger) {
        if (BulkChauffeurPassenger.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //CancelledCDPassengerHtml
      BookChauffeurHR: function (Chauffeur) {
        ViewModel.CurrentChauffeur(null);
        ViewModel.BulkChauffeurPassengerList.Set([]);
        ViewModel.CurrentChauffeur(Chauffeur);
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkChauffeurPassengerList, OBLib', {
          TravelRequisitionID: ViewModel.CurrentTravelRequisition().TravelRequisitionID(),
          ChauffeurDriverID: Chauffeur.ChauffeurDriverID(),
          PickUpDateTime: Chauffeur.PickUpDateTime(),
          DropOffDateTime: Chauffeur.DropOffDateTime()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.BulkChauffeurPassengerList);
            Singular.HideLoadingBar();
            $('#BulkChauffeurPassengers').modal();
          }
        });
      }, //BookChauffeurHR
      MainPassengerInd: function (BulkChauffeurDriverPassenger) {
        BulkChauffeurDriverPassenger.MainPassengerInd(!BulkChauffeurDriverPassenger.MainPassengerInd());
        TravelVM.Chauffeurs.Methods.ChauffeurMainPassengerValid();
      },
      MainPassengerIndCss: function (BulkChauffeurDriverPassenger) {
        if (BulkChauffeurDriverPassenger.MainPassengerInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
      },
      MainPassengerIndHtml: function (BulkChauffeurDriverPassenger) {
        if (BulkChauffeurDriverPassenger.MainPassengerInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
      },
      GetChauffeurPassengerClashes: function (BulkChauffeurPassenger) {
        ViewModel.CurrentBulkChauffeurPassenger(null);
        ViewModel.CurrentBulkChauffeurPassenger(BulkChauffeurPassenger);
        $('#ChauffeurPassengerClashDetails').modal();
      }, //GetChauffeurPassengerClashes
    }
  },
  Flights: {
    Methods: {
      //SaAirports: function (List, Item) {
      //  var Allowed = [];
      //  ClientData.ROAirportList.Iterate(function (Item, Index) {
      //    if (ViewModel.CurrentSystemID() == 1) {
      //      if (Item.CountryID == 1 && Item.RedundantInd == false) {
      //        Allowed.push({ AirportID: Item.AirportID, Airport: Item.Airport });
      //      };
      //    }
      //    else if (ViewModel.CurrentSystemID() == 2) {
      //      Allowed.push({ AirportID: Item.AirportID, Airport: Item.Airport });
      //    }
      //  });
      //  return Allowed;
      //}, //SaAirports
      IsFlightCancelled: function (Flight) {
        Flight.CancelledInd(!Flight.CancelledInd());
        if (Flight.CancelledInd() == false) {
          Flight.CancelledReason('');
          Flight.CancelledByUserID(null);
          Flight.CancelledDateTime(null);
        }
      }, //IsFlightCancelled
      IsFlightCancelledCss: function (Flight) {
        if (Flight.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //IsFlightCancelledCss

      IsFlightCancelledHtml: function (Flight) {
        if (Flight.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //IsFlightCancelledHtml
      RemoveFlight: function (Flight) {
        if (Flight.FlightHumanResourceList().length > 0 || Flight.CancelledInd()) {
          var r = confirm("People are booked on this Flight, are you sure you want to remove it?");
          if (r == true)
            ViewModel.CurrentTravelRequisition().FlightList.Remove(Flight);
        }
        else
          ViewModel.CurrentTravelRequisition().FlightList.Remove(Flight);
      }, //RemoveFlight
      CancelledFlightPassengerCount: function (Flight) {
        Flight.FlightHumanResourceList().Iterate(function (FlightHumanResource, Index) {
          if (FlightHumanResource.CancelledInd() == true) {
            return false;
          }
        });
        return true;
      }, //CancelledFlightPassengerCount
      FlightDetails: function () {
        var flight = ViewModel.CurrentFlight();
        if (flight != null) {
          var DepartureDateTime = new Date(flight.DepartureDateTime());
          var ArrivalDateTime = new Date(flight.ArrivalDateTime());
          var DepartAirport = ClientData.ROAirportList.Find("AirportID", flight.AirportIDFrom());
          var ArrivalAirport = ClientData.ROAirportList.Find("AirportID", flight.AirportIDTo());
          return (DepartAirport.Airport + '/' + ArrivalAirport.Airport + ': ' + DepartureDateTime.format('dd-MMM-yy HH:mm') + ' - ' + ArrivalDateTime.format('dd-MMM-yy HH:mm'))
        }
        return ""
      }, //FlightDetails
      BookFlightPassengers: function (Flight) {
        ViewModel.CurrentFlight(null);
        ViewModel.BulkFlightPassengerList.Set([]);
        ViewModel.CurrentFlight(Flight);
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkFlightPassengerList, OBLib', {
          TravelRequisitionID: ViewModel.CurrentTravelRequisition().TravelRequisitionID(),
          FlightID: Flight.FlightID(),
          DepartureDateTime: Flight.DepartureDateTime(),
          ArrivalDateTime: Flight.ArrivalDateTime()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.BulkFlightPassengerList);
            Singular.HideLoadingBar();
            ViewModel.ShowAllInd(false);
            $('#BulkFlightPassengers').modal();
          }
        });
      }, //BookFlightPassengers
      OnFlight: function (BulkFlightPassenger) {
        BulkFlightPassenger.OnFlightInd(!BulkFlightPassenger.OnFlightInd()); // Determine if passenger on flight or not
      }, //OnFlight
      OnFlightCss: function (BulkFlightPassenger) {
        if (BulkFlightPassenger.OnFlightInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //OnFlightCss
      OnFlightHtml: function (BulkFlightPassenger) {
        if (BulkFlightPassenger.OnFlightInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span> On Flight" } else { return "Select" }
      }, //OnFlightHtml
      CancelledFlightPassenger: function (BulkFlightPassenger) {
        BulkFlightPassenger.CancelledInd(!BulkFlightPassenger.CancelledInd());
        //var bfp = BulkFlightPassenger;
        //var fhr = ViewModel.CurrentFlight().FlightHumanResourceList().Find("FlightHumanResourceID", bfp.FlightHumanResourceID());
        //fhr.CancelledInd(bfp.CancelledInd());
        //fhr.CancelledReason(bfp.CancelledReason());
      }, //CancelledFlightPassenger
      CancelledFlightPassengerCss: function (BulkFlightPassenger) {
        if (BulkFlightPassenger.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //CancelledFlightPassengerCss
      CancelledFlightPassengerHtml: function (BulkFlightPassenger) {
        if (BulkFlightPassenger.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //CancelledFlightPassengerHtml
      ReAddFlightPassenger: function (BulkFlightPassenger) {
        var count = 0;
        ViewModel.BulkFlightPassengerList().Iterate(function (bfp, Index) {
          if (bfp.HumanResourceID() == BulkFlightPassenger.HumanResourceID() && bfp.CancelledInd() == false) {
            count++;
          }
        });
        if (count < 1) {
          var bfp = ViewModel.BulkFlightPassengerList.AddNew();
          bfp.CancelledInd(false);
          bfp.CancelledReason('');
          bfp.HumanResourceID(BulkFlightPassenger.HumanResourceID());
          bfp.HumanResource(BulkFlightPassenger.HumanResource());
          bfp.CityCode(BulkFlightPassenger.CityCode());
          bfp.Disciplines(BulkFlightPassenger.Disciplines());
          bfp.OtherFlights(BulkFlightPassenger.OtherFlights());
          TravelVM.Flights.Methods.ReOrder();
        } //end if
        else {
          alert(BulkFlightPassenger.HumanResource() + ' has already been Re-Added');
        } //end else
      }, //ReAddFlightPassenger  
      ReOrder: function () {
        $('table.BulkFlightPassengerList th:nth-child(2)').trigger('click');
      }, //ReOrder
      FilterBulkFlightPassenger: function () {
        Singular.ShowLoadingBar();
        if ((!ViewModel.ROFlightHumanResourceFilter() || ViewModel.ROFlightHumanResourceFilter() == '') && (!ViewModel.ROFlightDisciplineFilter() || ViewModel.ROFlightDisciplineFilter() == '')) {
          ViewModel.BulkFlightPassengerList().Iterate(function (bfp, Index) {
            bfp.Visible(true);
          });
        } else {
          ViewModel.BulkFlightPassengerList().Iterate(function (bfp, Index) {
            if (!ViewModel.ROFlightHumanResourceFilter()) {
              if (bfp.Disciplines().toLowerCase().indexOf(ViewModel.ROFlightDisciplineFilter().toLowerCase()) >= 0) {
                bfp.Visible(true);
              } else {
                bfp.Visible(false);
              };
            } else if (!ViewModel.ROFlightDisciplineFilter()) {
              if (bfp.HumanResource().toLowerCase().indexOf(ViewModel.ROFlightHumanResourceFilter().toLowerCase()) >= 0) {
                bfp.Visible(true);
              } else {
                bfp.Visible(false);
              };
            } else if ((bfp.HumanResource().toLowerCase().indexOf(ViewModel.ROFlightHumanResourceFilter().toLowerCase()) >= 0) && (bfp.Disciplines().toLowerCase().indexOf(ViewModel.ROFlightDisciplineFilter().toLowerCase()) >= 0)) {
              bfp.Visible(true);
            } else {
              bfp.Visible(false);
            };
          });
        };
        Singular.HideLoadingBar();
      }, //FilterROHumanResource
      GetBulkFlightPassengerBrokenRulesHTML: function () {
        var ErrorString = ''
        ViewModel.BulkFlightPassengerList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(bfp)
          }
        });
        return ErrorString;
      }, //GetBulkFlightPassengerBrokenRulesHTML
      GetFlightPassengerClashes: function (BulkFlightPassenger) {
        ViewModel.CurrentBulkFlightPassenger(null);
        ViewModel.CurrentBulkFlightPassenger(BulkFlightPassenger);
        $('#FlightPassengerClashDetails').modal();
      } //GetFlightPassengerClashes
    }//Methods
  }, //Flights
  RentalCars: {
    Methods: {
      RemoveRentalCar: function (RentalCar) {
        if (RentalCar.RentalCarHumanResourceList().length > 0 || RentalCar.CancelledInd()) {
          var r = confirm("People are booked on this Rental Car, are you sure you want to remove it?");
          if (r == true)
            ViewModel.CurrentTravelRequisition().RentalCarList.Remove(RentalCar);
        }
        else
          ViewModel.CurrentTravelRequisition().RentalCarList.Remove(RentalCar);
      }, //RemoveRentalCar
      CancelledRentalCarPassengersCount: function (RentalCar) {
        RentalCar.RentalCarHumanResourceList().Iterate(function (RentalCarHumanResource, Index) {
          if (RentalCarHumanResource.CancelledInd() == true) {
            return false;
          }
        });
        return true;
      }, //CancelledRentalCarPassengersCount
      BookRentalCarPassengers: function (RentalCar) {
        ViewModel.CurrentRentalCar(null);
        ViewModel.BulkRentalCarPassengerList().Clear();
        ViewModel.CurrentRentalCar(RentalCar);
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkRentalCarPassengerList, OBLib', {
          TravelRequisitionID: ViewModel.CurrentTravelRequisition().TravelRequisitionID(),
          RentalCarID: RentalCar.RentalCarID(),
          PickupDateTime: RentalCar.PickupDateTime(),
          DropoffDateTime: RentalCar.DropoffDateTime(),
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.BulkRentalCarPassengerList);
            Singular.HideLoadingBar();
            ViewModel.ShowAllInd(true);
            $('#BulkRentalCarPassengers').modal();
          }
        });
      }, //BookRentalCarPassengers
      OnRentalCar: function (BulkRentalCarPassenger) {
        BulkRentalCarPassenger.OnRentalCarInd(!BulkRentalCarPassenger.OnRentalCarInd());
        DriverValid();
      }, //OnRentalCar
      OnRentalCss: function (BulkRentalCarPassenger) {
        if (BulkRentalCarPassenger.OnRentalCarInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //OnRentalCss
      OnRentalHTML: function (BulkRentalCarPassenger) {
        if (BulkRentalCarPassenger.OnRentalCarInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span> On Car" } else { return "Select" }
      }, //OnRentalHTML
      DriverValid: function () {
        var len = ViewModel.BulkRentalCarPassengerList().length;
        var BulkRentalCarPassengerList = ViewModel.BulkRentalCarPassengerList();
        var driverCount = 0;
        var passengerCount = 0;
        for (var i = 0; i < len; i++) {
          if (BulkRentalCarPassengerList[i].OnRentalCarInd() == true && BulkRentalCarPassengerList[i].CancelledInd() == false) {
            passengerCount++;
            if (BulkRentalCarPassengerList[i].DriverInd() == true) {
              driverCount++;
            }
          }
        }
        if (driverCount == 0 && passengerCount > 0) {
          ViewModel.CurrentRentalCar().RentalCarDriverError('Driver Required for Rental Car');
        }
        else {
          ViewModel.CurrentRentalCar().RentalCarDriverError('');
        }
      }, //DriverValid
      CancelledRCPassenger: function (BulkRentalCarPassenger) {
        BulkRentalCarPassenger.CancelledInd(!BulkRentalCarPassenger.CancelledInd());
        //var brcp = BulkRentalCarPassenger;
        //var rchr = ViewModel.CurrentRentalCar().RentalCarHumanResourceList().Find("RentalCarHumanResourceID", brcp.RentalCarHumanResourceID());
        //rchr.CancelledInd(brcp.CancelledInd());

        //var CarType = ClientData.ROCarTypeList.Find('CarTypeID', ViewModel.CurrentRentalCar().CarTypeID());
        //if (CarType != undefined) {
        //  var MaxNoOfOccupants = CarType.MaxNoOfOccupants;
        //  var passengerCount = ViewModel.CurrentRentalCar().RentalCarHumanResourceList().length;
        //  passengerCount--; //Cancelled User

        //  var RemainingSeats = '(' + (MaxNoOfOccupants - passengerCount) + ' seats left)';
        //  ViewModel.CurrentRentalCar().RemainingSeats(RemainingSeats);
        //}

        //if (BulkRentalCarPassenger.CancelledInd() == false) {
        //  rchr.CancelledReason('');
        //}
        DriverValid();
      }, //CancelledRCPassenger
      CancelledRCPassengerCss: function (BulkRentalCarPassenger) {
        if (BulkRentalCarPassenger.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //CancelledRCPassengerCss
      CancelledRCPassengerHtml: function (BulkRentalCarPassenger) {
        if (BulkRentalCarPassenger.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //CancelledRCPassengerHtml
      PassengerCountValid: function (BulkRentalCarPassenger) {
        if (ViewModel.CurrentRentalCar() != undefined) {
          var brcp = BulkRentalCarPassenger;
          var CurrentRentalCar = ViewModel.CurrentRentalCar;
          var len = ViewModel.BulkRentalCarPassengerList().length;
          var BulkRentalCarPassengerList = ViewModel.BulkRentalCarPassengerList();
          var count = 0;
          for (var i = 0; i < len; i++) {
            if (BulkRentalCarPassengerList[i].OnRentalCarInd() == true && BulkRentalCarPassengerList[i].CancelledInd() == false) {
              count++;
            }
          }
          var carType = ClientData.ROCarTypeList.Find("CarTypeID", CurrentRentalCar().CarTypeID());
          if (carType != undefined) {
            var maxOccupants = carType.MaxNoOfOccupants;
            if (count >= maxOccupants && brcp.OnRentalCarInd() == false) {
              return false;
            }
          }
        }
        return true;
      }, //PassengerCountValid
      GetBulkRentalCarPassengersBrokenRulesHTML: function () {
        var ErrorString = ''
        //        if (ViewModel.CurrentRentalCar()) {
        //          ErrorString = Singular.Validation.GetBrokenRulesHTML(ViewModel.CurrentRentalCar());
        //        }
        if (ViewModel.CurrentRentalCar()) {
          if (!ViewModel.CurrentRentalCar().IsValid()) {
            ErrorString = ErrorString + "<ul><li><strong>Rental Car</strong><ul><li class=\"liVal\"><span class=\"liImg ImgValidation\"><label style=\"cursor: pointer;\">Driver Required for Rental Car</label></span></li></ul></li></ul>";
          }
        }
        ViewModel.BulkRentalCarPassengerList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(bfp);
          }
        });
        return ErrorString;
      }, //GetBulkRentalCarPassengersBrokenRulesHTML
      FilterBulkRentalCarPassengers: function () {
        Singular.ShowLoadingBar();
        if ((!ViewModel.RORCHumanResourceFilter() || ViewModel.RORCHumanResourceFilter() == '') && (!ViewModel.RORCDisciplineFilter() || ViewModel.RORCDisciplineFilter() == '')) {
          ViewModel.BulkRentalCarPassengerList().Iterate(function (bfp, Index) {
            bfp.Visible(true);
          });
        } else {
          ViewModel.BulkRentalCarPassengerList().Iterate(function (bfp, Index) {
            if (!ViewModel.RORCHumanResourceFilter()) {
              if (bfp.Disciplines().toLowerCase().indexOf(ViewModel.RORCDisciplineFilter().toLowerCase()) >= 0) {
                bfp.Visible(true);
              } else {
                bfp.Visible(false);
              };
            } else if (!ViewModel.RORCDisciplineFilter()) {
              if (bfp.HumanResource().toLowerCase().indexOf(ViewModel.RORCHumanResourceFilter().toLowerCase()) >= 0) {
                bfp.Visible(true);
              } else {
                bfp.Visible(false);
              };
            } else if ((bfp.HumanResource().toLowerCase().indexOf(ViewModel.RORCHumanResourceFilter().toLowerCase()) >= 0) && (bfp.Disciplines().toLowerCase().indexOf(ViewModel.RORCDisciplineFilter().toLowerCase()) >= 0)) {
              bfp.Visible(true);
            } else {
              bfp.Visible(false);
            };
          });
        };
        Singular.HideLoadingBar();
      }, //FilterBulkRentalCarPassengers
      RentalCarDetails: function () {
        var rc = ViewModel.CurrentRentalCar();
        if (rc != null) {
          var PickupDateTime = new Date(rc.PickupDateTime());
          var DropoffDateTime = new Date(rc.DropoffDateTime());
          var AgentFrom = ClientData.RORentalCarAgentBranchList.Find("RentalCarAgentBranchID", rc.AgentBranchIDFrom());
          var AgentTo = ClientData.RORentalCarAgentBranchList.Find("RentalCarAgentBranchID", rc.AgentBranchIDTo());
          var CrewType = ClientData.ROCrewTypeList.Find("CrewTypeID", rc.CrewTypeID());
          var RentalCarAgentBranchFrom = "";
          if (AgentFrom != undefined) {
            RentalCarAgentBranchFrom = AgentFrom.RentalCarAgentBranch;
          }
          var RentalCarAgentBranchTo = "";
          if (AgentTo != undefined) {
            RentalCarAgentBranchTo = AgentTo.RentalCarAgentBranch;
          }
          var CrewTypeName = "";
          if (CrewType != undefined) {
            CrewTypeName = CrewType.CrewType;
          }
          return (RentalCarAgentBranchFrom + '/' + RentalCarAgentBranchTo + ': ' + PickupDateTime.format('dd-MMM-yy HH:mm') + ' - ' + DropoffDateTime.format('dd-MMM-yy HH:mm') + ' (' + CrewTypeName + ')')
        }
        return ""
      }, //RentalCarDetails
      GetRentalCarPassengerClashes: function (BulkRentalCarPassenger) {
        ViewModel.CurrentBulkRentalCarPassenger(null);
        ViewModel.CurrentBulkRentalCarPassenger(BulkRentalCarPassenger);
        $('#RentalCarPassengerClashDetails').modal();
      }, //GetRentalCarPassengerClashes
      OpenBulkRentalCarAdd: function () {
        ViewModel.BulkRentalCarAddDriverList().Clear();
        ViewModel.CurrentTravelRequisition().CrewMemberList().Iterate(function (cm, Index) {
          var newObj = ViewModel.BulkRentalCarAddDriverList.AddNew();
          newObj.HumanResourceID(cm.HumanResourceID());
          newObj.HumanResourceName(cm.HumanResource());
          newObj.DriverInd(false);
          newObj.Discipline(cm.Discipline());
        });
        var BulkRentalCar = ViewModel.BulkRentalCarList.AddNew();
        //ViewModel.BulkRentalCar(new BulkRentalCarObject());
        ViewModel.ShowAllInd(false);
        $('#BulkAddRentalCar').modal();
      }, //OpenBulkRentalCarAdd
      GetBulkRentalCarBrokenRulesHTML: function () {
        var ErrorString = ''
        ViewModel.BulkRentalCarList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(bfp);
          }
        });
        return ErrorString;
      }
    }//Methods
  }, //RentalCars
  Accommodation: {
    Methods: {
      IsAccommodationCancelled: function (Accommodation) {
        Accommodation.CancelledInd(!Accommodation.CancelledInd());
        if (Accommodation.CancelledInd() == false)
          Accommodation.CancelledReason('');
      }, //IsAccommodationCancelled
      IsAccommodationCancelledCss: function (Accommodation) {
        if (Accommodation.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //IsAccommodationCancelled
      IsAccommodationCancelledHtml: function (Accommodation) {
        if (Accommodation.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //IsAccommodationCancelledHtml
      RemoveAccommodation: function (Accommodation) {
        if (Accommodation.AccommodationHumanResourceList().length > 0 || Accommodation.CancelledInd()) {
          var r = confirm("Guests are booked for this Accommodation, are you sure you want to remove it?");
          if (r == true)
            ViewModel.CurrentTravelRequisition().AccommodationList.Remove(Accommodation);
        }
        else
          ViewModel.CurrentTravelRequisition().AccommodationList.Remove(Accommodation);
      }, //RemoveAccommodation
      CancelledAccommodationGuestCount: function (Accommodation) {
        Accommodation.AccommodationHumanResourceList().Iterate(function (AccommodationHumanResource, Index) {
          if (AccommodationHumanResource.CancelledInd() == true) {
            return false;
          }
        });
        return true;
      }, //CancelledRentalCarPassengersCount
      AccommodationDetails: function () {
        var accom = ViewModel.CurrentAccommodation();
        if (accom != null) {
          var CheckInDate = new Date(accom.CheckInDate());
          var CheckOutDate = new Date(accom.CheckOutDate());
          var AccomProvider = ClientData.ROAccommodationProviderList.Find("AccommodationProviderID", accom.AccommodationProviderID());
          if (AccomProvider != undefined) {
            return (AccomProvider.AccommodationProvider + ': ' + CheckInDate.format('dd-MMM-yy') + ' - ' + CheckOutDate.format('dd-MMM-yy'));
          }
        }
        return ""
      }, //AccommodationDetails
      FilterBulkAccommodationGuests: function () {
        Singular.ShowLoadingBar();
        if ((!ViewModel.ROAccommHumanResourceFilter() || ViewModel.ROAccommHumanResourceFilter() == '') && (!ViewModel.ROAccommDisciplineFilter() || ViewModel.ROAccommDisciplineFilter() == '')) {
          ViewModel.BulkAccommodationGuestList().Iterate(function (bag, Index) {
            bag.Visible(true);
          });
        } else {
          ViewModel.BulkAccommodationGuestList().Iterate(function (bag, Index) {
            if (!ViewModel.ROAccommHumanResourceFilter()) {
              if (bag.Disciplines().toLowerCase().indexOf(ViewModel.ROAccommDisciplineFilter().toLowerCase()) >= 0) {
                bag.Visible(true);
              } else {
                bag.Visible(false);
              };
            } else if (!ViewModel.ROAccommDisciplineFilter()) {
              if (bag.HumanResource().toLowerCase().indexOf(ViewModel.ROAccommHumanResourceFilter().toLowerCase()) >= 0) {
                bag.Visible(true);
              } else {
                bag.Visible(false);
              };
            } else if ((bag.HumanResource().toLowerCase().indexOf(ViewModel.ROAccommHumanResourceFilter().toLowerCase()) >= 0) && (bag.Disciplines().toLowerCase().indexOf(ViewModel.ROAccommDisciplineFilter().toLowerCase()) >= 0)) {
              bag.Visible(true);
            } else {
              bag.Visible(false);
            };
          });
        };
        Singular.HideLoadingBar();
      }, //FilterBulkAccommodationGuests
      OnAccommodation: function (BulkAccommodationGuest) {
        BulkAccommodationGuest.OnAccommodationInd(!BulkAccommodationGuest.OnAccommodationInd());
      }, //OnAccommodation
      OnAccommodationSupplier: function (BulkAccommodationSupplierGuest) {
        BulkAccommodationSupplierGuest.OnAccommodationInd(!BulkAccommodationSupplierGuest.OnAccommodationInd());
      }, //OnAccommodationSupplier
      OnAccommodationCss: function (obj) {
        if (obj.OnAccommodationInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //OnAccommodationCss
      OnAccommodationHtml: function (obj) {
        if (obj.OnAccommodationInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span> Booked" } else { return "Select" }
      }, //OnAccommodationHtml
      CancelledAccommGuest: function (BulkAccommodationGuest) {
        BulkAccommodationGuest.CancelledInd(!BulkAccommodationGuest.CancelledInd());
        var bag = BulkAccommodationGuest;
        var ahr = ViewModel.CurrentAccommodation().AccommodationHumanResourceList().Find("AccommodationHumanResourceID", bag.AccommodationHumanResourceID());
        ahr.CancelledInd(bag.CancelledInd());
        ahr.CancelledReason(bag.CancelledReason());
      }, //CancelledAccommGuest
      CancelledAccommGuestCss: function (BulkAccommodationGuest) {
        if (BulkAccommodationGuest.CancelledInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-primary' }
      }, //CancelledAccommGuestCss
      CancelledAccommGuestHtml: function (BulkAccommodationGuest) {
        if (BulkAccommodationGuest.CancelledInd()) { return "Cancelled" } else { return "Cancel" }
      }, //CancelledAccommGuestHtml
      BookAccommodationGuests: function (Accommodation) {
        ViewModel.CurrentAccommodation(null);
        ViewModel.BulkAccommodationGuestList.Set([]);
        ViewModel.CurrentAccommodation(Accommodation);
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkAccommodationGuestList, OBLib', {
          TravelRequisitionID: ViewModel.CurrentTravelRequisition().TravelRequisitionID(),
          AccommodationID: Accommodation.AccommodationID(),
          CheckInDate: Accommodation.CheckInDate(),
          CheckOutDate: Accommodation.CheckOutDate()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.BulkAccommodationGuestList);
            var AccProvider = ClientData.ROAccommodationProviderList.Find("AccommodationProviderID", Accommodation.AccommodationProviderID());
            ViewModel.BulkAccommodationGuestList().Iterate(function (bag, Index) {
              if (AccProvider != undefined) {
                if (bag.HumanResourceCityID() == AccProvider.CityID) {
                  bag.AccommInBaseCityInd(true);
                }
                else {
                  bag.AccommInBaseCityInd(false);
                }
              }
              Singular.Validation.CheckRules(bag);
            });
            Singular.HideLoadingBar();
            //ViewModel.ShowAllInd(false);
            $('#BulkAccommodationGuests').modal();
          }
        });
      }, //BookAccommodationGuests
      BookAccommodationSupplierGuests: function (Accommodation) {
        ViewModel.CurrentAccommodation(null);
        ViewModel.BulkAccommodationSupplierGuestList().Clear();
        ViewModel.CurrentAccommodation(Accommodation);
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkAccommodationSupplierGuestList, OBLib', {
          SupplierID: Accommodation.SupplierID(),
          AccommodationID: Accommodation.AccommodationID(),
          CheckInDate: Accommodation.CheckInDate(),
          CheckOutDate: Accommodation.CheckOutDate()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.BulkAccommodationSupplierGuestList);
            Singular.HideLoadingBar();
            ViewModel.ShowAllInd(false);
            $('#BulkAccommodationSupplierGuests').modal();
          }
        });
      }, //BookAccommodationSupplierGuests
      FilterBulkAccommodationSupplierGuests: function () {
        Singular.ShowLoadingBar();
        if ((!ViewModel.ROAccommSupplierHumanResourceFilter() || ViewModel.ROAccommSupplierHumanResourceFilter() == '')) {
          ViewModel.BulkAccommodationSupplierGuestList().Iterate(function (bag, Index) {
            bag.Visible(true);
          });
        } else {
          ViewModel.BulkAccommodationSupplierGuestList().Iterate(function (bag, Index) {
            if ((bag.SupplierHumanResource().toLowerCase().indexOf(ViewModel.ROAccommSupplierHumanResourceFilter().toLowerCase()) >= 0)) {
              bag.Visible(true);
            } else {
              bag.Visible(false);
            };
          });
        };
        Singular.HideLoadingBar();
      }, //FilterBulkAccommodationSupplierGuests
      GetAccommodationGuestClashes: function (BulkAccommodationGuest) {
        ViewModel.CurrentBulkAccommodationGuest(null);
        ViewModel.CurrentBulkAccommodationGuest(BulkAccommodationGuest);
        $('#AccommodationGuestClashDetails').modal();
      }, //GetAccommodationGuestClashes
      GetAccommodationSupplierGuestClashes: function (BulkAccommodationSupplierGuest) {
        ViewModel.CurrentBulkAccommodationSupplierGuest(null);
        ViewModel.CurrentBulkAccommodationSupplierGuest(BulkAccommodationSupplierGuest);
        $('#AccommodationSupplierGuestClashDetails').modal();
      }, //GetAccommodationSupplierGuestClashes
      GetBulkAccommodationGuestBrokenRulesHTML: function () {
        var ErrorString = ''
        ViewModel.BulkAccommodationGuestList().Iterate(function (bag, Index) {
          if (bag.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(bag)
          }
        });
        return ErrorString;
      }, //GetBulkAccommodationGuestBrokenRulesHTML
      ReAddAccommodationGuest: function (BulkAccommodationGuest) {
        var count = 0;
        ViewModel.BulkAccommodationGuestList().Iterate(function (bag, Index) {
          if (bag.HumanResourceID() == BulkAccommodationGuest.HumanResourceID() && bag.CancelledInd() == false) {
            count++;
          }
        });
        if (count < 1) {
          var bag = ViewModel.BulkAccommodationGuestList.AddNew();
          bag.HumanResourceID(BulkAccommodationGuest.HumanResourceID());
          bag.HumanResource(BulkAccommodationGuest.HumanResource());
          bag.CityCode(BulkAccommodationGuest.CityCode());
          bag.Disciplines(BulkAccommodationGuest.Disciplines());
          bag.OtherAccommodations(BulkAccommodationGuest.OtherAccommodations());
          bag.HumanResourceCityID(BulkAccommodationGuest.HumanResourceCityID());
          var AccProvider = ClientData.ROAccommodationProviderList.Find("AccommodationProviderID", ViewModel.CurrentAccommodation().AccommodationProviderID());
          if (AccProvider != undefined) {
            if (bag.HumanResourceCityID() == AccProvider.CityID) {
              bag.AccommInBaseCityInd(true);
            }
            else {
              bag.AccommInBaseCityInd(false);
            }
          }
          TravelVM.Accommodation.Methods.ReOrder();
        } //end if
        else {
          alert(BulkAccommodationGuest.HumanResource() + ' has already been Re-Added');
        } //end else
      }, //ReAddAccommodationGuest
      ReOrder: function () {
        $('table.BulkAccommodationGuestList th:nth-child(2)').trigger('click');
      } //ReOrder
    }//Methods
  }, //Accommodation
  SnT: {
    Methods: {
      GetHumanResourceSnTDetails: function (ProductionHumanResourceSnT) {
        ViewModel.CurrentProductionHRSnT(null);
        ViewModel.BulkHRSnTDetailList().Clear();
        ViewModel.CurrentProductionHRSnT(ProductionHumanResourceSnT);
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkHRSnTDetailList, OBLib', {
          TravelRequisitionID: ViewModel.CurrentTravelRequisition().TravelRequisitionID(),
          HumanResourceID: ViewModel.CurrentProductionHRSnT().HumanResourceID()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.BulkHRSnTDetailList);
            ViewModel.BulkHRSnTDetailList().Iterate(function (bfp, Index) {
              Singular.Validation.CheckRules(bfp);
            });
            Singular.HideLoadingBar();
            TravelVM.SnT.Methods.AddListToParent(ViewModel.BulkHRSnTDetailList());
            $(".Popovers").popover();
            ViewModel.ShowAllInd(false);
            $('#BulkHRSnTDetails').modal();
          }
        });
      }, //GetHumanResourceSnTDetails
      AddListToParent: function (List) {
        var len = List.length;
        var phrs = ViewModel.CurrentProductionHRSnT();
        var phrsdList = List;
        for (var i = 0; i < len; i++) {
          var child = ViewModel.CurrentProductionHRSnT().ProductionHumanResourceSnTDetailList().Find("GroupHumanResourceSnTID", phrsdList[i].GroupHumanResourceSnTID());
          if (child == undefined) {
            var phrsdNew = phrs.ProductionHumanResourceSnTDetailList.AddNew();
            phrsdNew.GroupHumanResourceSnTID(phrsdList[i].GroupHumanResourceSnTID());
            phrsdNew.HumanResourceID(phrsdList[i].HumanResourceID())
            if (phrsdList[i].GroupSnTID() == null) {
              phrsdNew.GroupSnTID(13); //Default to South Africa - Domestic
            } else {
              phrsdNew.GroupSnTID(phrsdList[i].GroupSnTID());
            }
            var gsnt = ClientData.ROGroupSnTList.Find("GroupSnTID", phrsdNew.GroupSnTID());
            if (phrsdList[i].CountryID() == null) {
              phrsdNew.CountryID(gsnt.DestinationCountryID);
            }
            else {
              phrsdNew.CountryID(phrsdList[i].CountryID());
            }
            if (phrsdList[i].CurrencyID() == null) {
              phrsdNew.CurrencyID(gsnt.CurrencyID);
            }
            else {
              phrsdNew.CurrencyID(phrsdList[i].CurrencyID());
            }
            //phrsdNew.CountryID(phrsdList[i].CountryID());
            //phrsdNew.CurrencyID(phrsdList[i].CurrencyID());
            phrsdNew.InternationalInd(phrsdList[i].InternationalInd());
            phrsdNew.SnTDay(phrsdList[i].SnTDay());
            phrsdNew.BreakfastProductionID(phrsdList[i].BreakfastProductionID());
            phrsdNew.BreakfastAmount(phrsdList[i].BreakfastAmount());
            phrsdNew.LunchProductionID(phrsdList[i].LunchProductionID());
            phrsdNew.LunchAmount(phrsdList[i].LunchAmount());
            phrsdNew.DinnerProductionID(phrsdList[i].DinnerProductionID());
            phrsdNew.DinnerAmount(phrsdList[i].DinnerAmount());
            phrsdNew.IncidentalProductionID(phrsdList[i].IncidentalProductionID());
            phrsdNew.IncidentalAmount(phrsdList[i].IncidentalAmount());
            phrsdNew.ConferenceProductionID(phrsdList[i].ConferenceProductionID());
            phrsdNew.ConferenceAmount(phrsdList[i].ConferenceAmount());
            phrsdNew.AccommodationInd(phrsdList[i].AccommodationInd());
            phrsdNew.Comments(phrsdList[i].Comments());
            phrsdNew.ChangedReason(phrsdList[i].ChangedReason());
            phrsdNew.BreakfastSystemID(phrsdList[i].BreakfastSystemID());
            phrsdNew.BreakfastProductionAreaID(phrsdList[i].BreakfastProductionAreaID());
            phrsdNew.LunchSystemID(phrsdList[i].LunchSystemID());
            phrsdNew.LunchProductionAreaID(phrsdList[i].LunchProductionAreaID());
            phrsdNew.DinnerSystemID(phrsdList[i].DinnerSystemID());
            phrsdNew.DinnerProductionAreaID(phrsdList[i].DinnerProductionAreaID());
            phrsdNew.IncidentalSystemID(phrsdList[i].IncidentalSystemID());
            phrsdNew.IncidentalProductionAreaID(phrsdList[i].IncidentalProductionAreaID());
            phrsdNew.ConferenceSystemID(phrsdList[i].ConferenceSystemID());
            phrsdNew.ConferenceProductionAreaID(phrsdList[i].ConferenceProductionAreaID());
            phrsdNew.IsNew(false);
            phrsdNew.IsSelfDirty(false);
          }
        }
      }, //AddListToParent
      SetOnBreakfast: function (ProductionHumanResourceSnTDetail) {
        var phrsd = ProductionHumanResourceSnTDetail;
        phrsd.BreakfastInd(!phrsd.BreakfastInd());
        var phrs = ViewModel.CurrentProductionHRSnT();
        var child = phrs.ProductionHumanResourceSnTDetailList().Find("GroupHumanResourceSnTID", phrsd.GroupHumanResourceSnTID());
        var gsnt = ClientData.ROGroupSnTList.Find("GroupSnTID", phrsd.GroupSnTID());
        if (phrsd.BreakfastInd()) {
          phrsd.BreakfastAmount(gsnt.BreakfastAmount);
          phrsd.BreakfastProductionID(ViewModel.CurrentTravelRequisition().ProductionID());
          phrsd.BreakfastSystemID(ViewModel.CurrentSystemID());
          phrsd.BreakfastProductionAreaID(ViewModel.CurrentProductionAreaID());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() + gsnt.BreakfastAmount);
        }
        else {
          phrs.TotalBreakfastAmount(phrs.TotalBreakfastAmount() - phrsd.BreakfastAmount());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() - phrsd.BreakfastAmount());
          phrsd.BreakfastAmount(0);
          phrsd.BreakfastProductionID(null);
          phrsd.BreakfastSystemID(null);
          phrsd.BreakfastProductionAreaID(null);
        }
        if (child == undefined) {
          var phrsdNew = phrs.ProductionHumanResourceSnTDetailList.AddNew();
          PopulateNewObject(phrsdNew, phrsd);
          phrs.TotalBreakfastAmount(phrs.TotalBreakfastAmount() + phrsd.BreakfastAmount());
        }
        else {
          if (phrsd.BreakfastInd()) {
            child.BreakfastProductionID(phrsd.BreakfastProductionID());
            child.BreakfastAmount(phrsd.BreakfastAmount());
            child.BreakfastSystemID(phrsd.BreakfastSystemID());
            child.BreakfastProductionAreaID(phrsd.BreakfastProductionAreaID());
            phrs.TotalBreakfastAmount(phrs.TotalBreakfastAmount() + phrsd.BreakfastAmount());
          }
          else {
            child.BreakfastProductionID(null);
            child.BreakfastAmount(0);
            child.BreakfastSystemID(null);
            child.BreakfastProductionAreaID(null);
          }
        }
      }, //SetOnBreakfast
      GetButtonState: function (ButtonState) {
        var defaultStyle = "";
        if (ButtonState) { defaultStyle = "btn btn-xs btn-success" }
        else { defaultStyle = "btn btn-xs btn-primary" }
        return defaultStyle;
      }, //GetButtonState
      GetBreakfastText: function (ProductionHumanResourceSnTDetail) {
        var html = "";
        var amount = ProductionHumanResourceSnTDetail.BreakfastAmount().toString();
        if (ProductionHumanResourceSnTDetail.BreakfastInd()) { html = "<span class=\"glyphicon glyphicon-ok\"></span>" + " " + amount }
        else { html = "Select" }
        return html;
      }, //GetBreakfastText
      SetOnLunch: function (ProductionHumanResourceSnTDetail) {
        var phrsd = ProductionHumanResourceSnTDetail;
        phrsd.LunchInd(!phrsd.LunchInd());
        var phrs = ViewModel.CurrentProductionHRSnT();
        var child = phrs.ProductionHumanResourceSnTDetailList().Find("GroupHumanResourceSnTID", phrsd.GroupHumanResourceSnTID());
        var gsnt = ClientData.ROGroupSnTList.Find("GroupSnTID", phrsd.GroupSnTID());
        if (phrsd.LunchInd()) {
          phrsd.LunchAmount(gsnt.LunchAmount);
          phrsd.LunchProductionID(ViewModel.CurrentTravelRequisition().ProductionID());
          phrsd.LunchSystemID(ViewModel.CurrentSystemID());
          phrsd.LunchProductionAreaID(ViewModel.CurrentProductionAreaID());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() + gsnt.LunchAmount);
        }
        else {
          phrs.TotalLunchAmount(phrs.TotalLunchAmount() - phrsd.LunchAmount());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() - phrsd.LunchAmount());
          phrsd.LunchAmount(0);
          phrsd.LunchProductionID(null);
          phrsd.LunchSystemID(null);
          phrsd.LunchProductionAreaID(null);
        }
        if (child == undefined) {
          var phrsdNew = phrs.ProductionHumanResourceSnTDetailList.AddNew();
          PopulateNewObject(phrsdNew, phrsd);
          phrs.TotalLunchAmount(phrs.TotalLunchAmount() + phrsd.LunchAmount());
        }
        else {
          if (phrsd.LunchInd()) {
            child.LunchProductionID(phrsd.LunchProductionID());
            child.LunchAmount(phrsd.LunchAmount());
            child.LunchSystemID(phrsd.LunchSystemID());
            child.LunchProductionAreaID(phrsd.LunchProductionAreaID());
            phrs.TotalLunchAmount(phrs.TotalLunchAmount() + phrsd.LunchAmount());
          }
          else {
            child.LunchProductionID(null);
            child.LunchAmount(0);
            child.LunchSystemID(null);
            child.LunchProductionAreaID(null);
          }
        }
      }, //SetOnLunch
      GetLunchText: function (ProductionHumanResourceSnTDetail) {
        var html = "";
        var amount = ProductionHumanResourceSnTDetail.LunchAmount().toString();
        if (ProductionHumanResourceSnTDetail.LunchInd()) { html = "<span class=\"glyphicon glyphicon-ok\"></span>" + " " + amount }
        else { html = "Select" }
        return html;
      }, //GetLunchText
      SetOnDinner: function (ProductionHumanResourceSnTDetail) {
        //Trying New Way
        var phrsd = ProductionHumanResourceSnTDetail;
        phrsd.DinnerInd(!phrsd.DinnerInd());
        var phrs = ViewModel.CurrentProductionHRSnT();
        var child = phrs.ProductionHumanResourceSnTDetailList().Find("GroupHumanResourceSnTID", phrsd.GroupHumanResourceSnTID());
        var gsnt = ClientData.ROGroupSnTList.Find("GroupSnTID", phrsd.GroupSnTID());
        if (phrsd.DinnerInd()) {
          phrsd.DinnerAmount(gsnt.DinnerAmount);
          phrsd.DinnerProductionID(ViewModel.CurrentTravelRequisition().ProductionID());
          phrsd.DinnerSystemID(ViewModel.CurrentSystemID());
          phrsd.DinnerProductionAreaID(ViewModel.CurrentProductionAreaID());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() + gsnt.DinnerAmount);
        }
        else {
          phrs.TotalDinnerAmount(phrs.TotalDinnerAmount() - phrsd.DinnerAmount());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() - phrsd.DinnerAmount());
          phrsd.DinnerAmount(0);
          phrsd.DinnerProductionID(null);
          phrsd.DinnerSystemID(null);
          phrsd.DinnerProductionAreaID(null);
        }
        if (child == undefined) {
          var phrsdNew = phrs.ProductionHumanResourceSnTDetailList.AddNew();
          PopulateNewObject(phrsdNew, phrsd);
          phrs.TotalDinnerAmount(phrs.TotalDinnerAmount() + phrsd.DinnerAmount());
        }
        else {
          if (phrsd.DinnerInd()) {
            child.DinnerProductionID(phrsd.DinnerProductionID());
            child.DinnerAmount(phrsd.DinnerAmount());
            child.DinnerSystemID(phrsd.DinnerSystemID());
            child.DinnerProductionAreaID(phrsd.DinnerProductionAreaID());
            phrs.TotalDinnerAmount(phrs.TotalDinnerAmount() + phrsd.DinnerAmount());
          }
          else {
            child.DinnerProductionID(null);
            child.DinnerAmount(0);
            child.DinnerSystemID(null);
            child.DinnerProductionAreaID(null);
          }
        }
      }, //SetOnDinner
      GetDinnerText: function (ProductionHumanResourceSnTDetail) {
        var html = "";
        var amount = ProductionHumanResourceSnTDetail.DinnerAmount().toString();
        if (ProductionHumanResourceSnTDetail.DinnerInd()) { html = "<span class=\"glyphicon glyphicon-ok\"></span>" + " " + amount }
        else { html = "Select" }
        return html;
      }, //GetDinnerText
      SetOnIncidental: function (ProductionHumanResourceSnTDetail) {
        var phrsd = ProductionHumanResourceSnTDetail;
        phrsd.IncidentalInd(!phrsd.IncidentalInd());
        var phrs = ViewModel.CurrentProductionHRSnT();
        var child = phrs.ProductionHumanResourceSnTDetailList().Find("GroupHumanResourceSnTID", phrsd.GroupHumanResourceSnTID());
        var gsnt = ClientData.ROGroupSnTList.Find("GroupSnTID", phrsd.GroupSnTID());
        if (phrsd.IncidentalInd()) {
          phrsd.IncidentalAmount(gsnt.IncidentalAmount);
          phrsd.IncidentalProductionID(ViewModel.CurrentTravelRequisition().ProductionID());
          phrsd.IncidentalSystemID(ViewModel.CurrentSystemID());
          phrsd.IncidentalProductionAreaID(ViewModel.CurrentProductionAreaID());
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() + gsnt.IncidentalAmount);
        }
        else {
          phrsd.TotalSnTAmount(phrsd.TotalSnTAmount() - phrsd.IncidentalAmount());
          phrsd.IncidentalAmount(0);
          phrsd.IncidentalProductionID(null);
          phrsd.IncidentalSystemID(null);
          phrsd.IncidentalProductionAreaID(null);
        }
        if (child == undefined) {
          var phrsdNew = phrs.ProductionHumanResourceSnTDetailList.AddNew();
          PopulateNewObject(phrsdNew, phrsd);
          phrs.TotalIncidentalAmount(phrs.TotalIncidentalAmount() + phrsd.IncidentalAmount());
        }
        else {
          if (phrsd.IncidentalInd()) {
            child.IncidentalProductionID(phrsd.IncidentalProductionID());
            child.IncidentalAmount(phrsd.IncidentalAmount());
            child.IncidentalSystemID(phrsd.IncidentalSystemID());
            child.IncidentalProductionAreaID(phrsd.IncidentalProductionAreaID());
            phrs.TotalIncidentalAmount(phrs.TotalIncidentalAmount() + phrsd.IncidentalAmount());
          }
          else {
            phrs.TotalIncidentalAmount(phrs.TotalIncidentalAmount() - phrsd.IncidentalAmount());
            child.IncidentalProductionID(null);
            child.IncidentalAmount(0);
            child.IncidentalSystemID(null);
            child.IncidentalProductionAreaID(null);
          }
        }
      }, //SetOnIncidental
      GetIncidentalText: function (ProductionHumanResourceSnTDetail) {
        var html = "";
        var amount = ProductionHumanResourceSnTDetail.IncidentalAmount().toString();
        if (ProductionHumanResourceSnTDetail.IncidentalInd()) {
          html = "<span class=\"glyphicon glyphicon-ok\"></span>" + " " + amount
        }
        else { html = "Select" }
        return html;
      }, //GetIncidentalText
      WhatClicked: function (e) {
        $(".Popovers").popover();
      },
      SetupOtherProductionDetails: function (RefNo, Title, SystemID, ProductionAreaID) {
        var System = ClientData.ROSystemList.Find('SystemID', SystemID);
        var ProductionArea = ClientData.ROProductionAreaList.Find('ProductionAreaID', ProductionAreaID);
        var SystemName = '';
        var AreaName = '';
        if (System != undefined) {
          SystemName = System.System;
        }
        if (ProductionArea != undefined) {
          AreaName = ProductionArea.ProductionArea;
        }
        var str = "";
        str = '<div id="popOverBox"> <strong> Ref No:</strong> ' + RefNo + '<br><strong> Title:</strong>' + Title + '<br><strong>Sub-Dept:</strong> ' + SystemName + '<br><strong>Area:</strong> ' + AreaName + '</div>';
        //str = RefNo + ": " + Title + ' (' + SystemName + ' - ' + AreaName + ')';
        return str;
      }, //SetupOtherProductionDetails
      GetAdHocDetails: function (SystemID, ProductionAreaID, OtherBooking) {
        var Booking = '';
        var System = ClientData.ROSystemList.Find('SystemID', SystemID);
        var ProductionArea = ClientData.ROProductionAreaList.Find('ProductionAreaID', ProductionAreaID);
        var SystemName = '';
        var AreaName = '';
        if (System != undefined) {
          SystemName = System.System;
        }
        if (ProductionArea != undefined) {
          AreaName = ProductionArea.ProductionArea;
        }
        Booking = '<div id="popOverBox"> <strong> Title:</strong> ' + OtherBooking + '<br><strong>Sub-Dept:</strong> ' + SystemName + '<br><strong>Area:</strong> ' + AreaName + '</div>';
        return Booking;
      }, //GetDetails
      HRSnTDetails: function () {
        var phrs = ViewModel.CurrentProductionHRSnT();
        if (phrs != null) {
          return (phrs.HumanResource());
        }
        return ""
      }, //HRSnTDetails
      GetHRSnTDetailBrokenRulesHTML: function () {
        var ErrorString = ''
        ViewModel.BulkHRSnTDetailList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(bfp);
          }
        });
        return ErrorString;
      }, //GetHRSnTDetailBrokenRulesHTML
      FetchSnTDays: function () {
        Singular.ShowLoadingBar();
        Singular.GetDataStateless('OBLib.Productions.BulkHRSnTDetailList, OBLib', {
          HumanResourceID: ViewModel.CurrentProductionHRSnT().HumanResourceID(),
          StartDate: ViewModel.ROSnTStartDate(),
          EndDate: ViewModel.ROSnTEndDate()
        }, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.FetchedProductionHumanResourceSnTDetailList);
            Singular.HideLoadingBar();

            //Adding fetched S&T dates and adding them to list bound to VM
            var len = ViewModel.FetchedProductionHumanResourceSnTDetailList().length;
            var fphrsdList = ViewModel.FetchedProductionHumanResourceSnTDetailList();
            for (var i = 0; i < len; i++) {
              var phrsd = ViewModel.BulkHRSnTDetailList().Find("SnTDay", ViewModel.FetchedProductionHumanResourceSnTDetailList()[i].SnTDay());
              //Only create new object if it doesn't already exist in list
              //if (phrsd == undefined) {
              var newItem = ViewModel.FetchedProductionHumanResourceSnTDetailList()[i];
              newItem.BreakfastProductionID(fphrsdList[i].BreakfastProductionID());
              newItem.BreakfastAdHocBookingID(fphrsdList[i].BreakfastAdHocBookingID());
              newItem.LunchProductionID(fphrsdList[i].LunchProductionID());
              newItem.LunchAdHocBookingID(fphrsdList[i].LunchAdHocBookingID());
              newItem.DinnerProductionID(fphrsdList[i].DinnerProductionID());
              newItem.DinnerAdHocBookingID(fphrsdList[i].DinnerAdHocBookingID());
              newItem.IncidentalProductionID(fphrsdList[i].IncidentalProductionID());
              newItem.IncidentalAdHocBookingID(fphrsdList[i].IncidentalAdHocBookingID());
              newItem.BreakfastInd(fphrsdList[i].BreakfastInd());
              newItem.LunchInd(fphrsdList[i].LunchInd());
              newItem.DinnerInd(fphrsdList[i].DinnerInd());
              newItem.IncidentalInd(fphrsdList[i].IncidentalInd());
              newItem.AdHocBooking(fphrsdList[i].AdHocBooking());
              ViewModel.BulkHRSnTDetailList.Add(newItem);
                //var newPHRSD = ViewModel.CurrentProductionHRSnT().ProductionHumanResourceSnTDetailList.AddNew();
                //newPHRSD.GroupHumanResourceSnTID(fphrsdList[i].GroupHumanResourceSnTID());
                //newPHRSD.HumanResourceID(fphrsdList[i].HumanResourceID());
                //newPHRSD.CountryID(fphrsdList[i].CountryID());
                //newPHRSD.CurrencyID(fphrsdList[i].CurrencyID());
                //if (fphrsdList[i].GroupSnTID() == null) {
                //  newPHRSD.GroupSnTID(13); //Default to South Africa - Domestic
                //}
                //else {
                //  newPHRSD.GroupSnTID(fphrsdList[i].GroupSnTID());
                //}
                //newPHRSD.InternationalInd(fphrsdList[i].InternationalInd());
                //newPHRSD.SnTDay(fphrsdList[i].SnTDay());
                //newPHRSD.BreakfastProductionID(fphrsdList[i].BreakfastProductionID());
                //newPHRSD.BreakfastAdHocBookingID(fphrsdList[i].BreakfastProductionID());
                //newPHRSD.BreakfastAmount(fphrsdList[i].BreakfastAmount());
                //newPHRSD.LunchProductionID(fphrsdList[i].LunchProductionID());
                //newPHRSD.LunchAdHocBookingID(fphrsdList[i].BreakfastProductionID());
                //newPHRSD.LunchAmount(fphrsdList[i].LunchAmount());
                //newPHRSD.DinnerProductionID(fphrsdList[i].DinnerProductionID());
                //newPHRSD.DinnerAdHocBookingID(fphrsdList[i].BreakfastProductionID());
                //newPHRSD.DinnerAmount(fphrsdList[i].DinnerAmount());
                //newPHRSD.IncidentalProductionID(fphrsdList[i].IncidentalProductionID());
                //newPHRSD.IncidentalAdHocBookingID(fphrsdList[i].BreakfastProductionID());
                //newPHRSD.IncidentalAmount(fphrsdList[i].IncidentalAmount());
                //newPHRSD.ConferenceProductionID(fphrsdList[i].ConferenceProductionID());
                //newPHRSD.ConferenceAmount(fphrsdList[i].ConferenceAmount());
                //newPHRSD.AccommodationInd(fphrsdList[i].AccommodationInd());
                //newPHRSD.Comments(fphrsdList[i].Comments());
                //newPHRSD.ChangedReason(fphrsdList[i].ChangedReason());
                //newPHRSD.TotalSnTAmount(fphrsdList[i].TotalSnTAmount());
                //newPHRSD.BreakfastInd(fphrsdList[i].BreakfastInd());
                //newPHRSD.LunchInd(fphrsdList[i].LunchInd());
                //newPHRSD.DinnerInd(fphrsdList[i].DinnerInd());
                //newPHRSD.IncidentalInd(fphrsdList[i].IncidentalInd());
                //newPHRSD.BreakfastRefNo(fphrsdList[i].BreakfastRefNo());
                //newPHRSD.BreakfastTitle(fphrsdList[i].BreakfastTitle());
                //newPHRSD.LunchRefNo(fphrsdList[i].LunchRefNo());
                //newPHRSD.LunchTitle(fphrsdList[i].LunchTitle());
                //newPHRSD.DinnerRefNo(fphrsdList[i].DinnerRefNo());
                //newPHRSD.DinnerTitle(fphrsdList[i].DinnerTitle());
                //newPHRSD.IncidentalRefNo(fphrsdList[i].IncidentalRefNo());
                //newPHRSD.IncidentalTitle(fphrsdList[i].IncidentalTitle());
              //}
            }
            TravelVM.SnT.Methods.AddListToParent(ViewModel.BulkHRSnTDetailList());
          }
        });
        ViewModel.FetchedProductionHumanResourceSnTDetailList().Clear();
      }, //FetchSnTDays
      CheckIfSnTAlreadyGiven: function (obj) {
        if (obj.BreakfastAdHocBookingID() != undefined || obj.BreakfastAdHocBookingID() != null)
          return true;
        if (obj.LunchAdHocBookingID() != undefined || obj.LunchAdHocBookingID() != null)
          return true;
        if (obj.DinnerAdHocBookingID() != undefined || obj.DinnerAdHocBookingID() != null)
          return true;
        if (obj.IncidentalAdHocBookingID() != undefined || obj.IncidentalAdHocBookingID() != null)
          return true;
        else
          return false;
      }, //CheckIfSnTAlreadyGiven
      OpenBulkEditSnT: function () {
        Singular.SendCommand('OpenBulkEditSnT', {},
        function (d) {
          $('#BulkEditSnT').modal();
        });
        //        ViewModel.BulkGroupPolicyDetailList().Clear();
        //        var BulkGroupPolicyDetail = ViewModel.BulkGroupPolicyDetailList.AddNew();
        //        BulkGroupPolicyDetail.BreakfastInd(false);
        //        BulkGroupPolicyDetail.LunchInd(false);
        //        BulkGroupPolicyDetail.DinnerInd(false);
        //        BulkGroupPolicyDetail.IncidentalInd(false);

        //        ViewModel.GroupSnTBulkCrewList().Clear();
        //        ViewModel.CurrentTravelRequisition().CrewMemberList().Iterate(function (cm, Index) {
        //          var newGSBC = ViewModel.GroupSnTBulkCrewList.AddNew();
        //          newGSBC.SnTInd(false);
        //          newGSBC.HumanResourceID(cm.HumanResourceID());
        //          newGSBC.HumanResource(cm.HumanResource());
        //          newGSBC.CityCode(cm.CityCode());
        //          newGSBC.Discipline(cm.Discipline());
        //        });

        //        ViewModel.SnTDayList().Clear();
        //        var StartDate = new Date(ViewModel.CurrentTravelRequisition().EarliestScheduleDate());
        //        var EndDate = new Date(ViewModel.CurrentTravelRequisition().LatestScheduleDate());
        //        StartDate.setDate(StartDate.getDate() - 7);
        //        EndDate.setDate(EndDate.getDate() + 7);

        //        for (var i = StartDate; i <= EndDate; i.setDate(i.getDate() + 1)) {
        //          var newSnTdat = ViewModel.SnTDayList.AddNew();
        //          newSnTdat.SelectedInd(false);
        //          var day = new Date(i);
        //          newSnTdat.SnTDay(day);
        //        }
        //        ViewModel.ShowAllInd(false);
      }, //OpenBulkEditSnT
      SelectAll: function () {
        ViewModel.GroupSnTBulkCrewList().Iterate(function (item, index) {
          if (item.Visible()) {
            item.SnTInd(true);
          }
        });
      }, //SelectAll
      ClearAll: function () {
        ViewModel.GroupSnTBulkCrewList().Iterate(function (item, index) {
          item.SnTInd(false);
        });
      }, //ClearAll
      SelectedSnT: function (GroupSnTBulkCrew) {
        GroupSnTBulkCrew.SnTInd(!GroupSnTBulkCrew.SnTInd());
      }, //SelectedSnT
      SelectedSnTCss: function (GroupSnTBulkCrew) {
        if (GroupSnTBulkCrew.SnTInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //SelectedSnTCss
      SelectedSnTHtml: function (GroupSnTBulkCrew) {
        if (GroupSnTBulkCrew.SnTInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
      }, //SelectedSnTHtml
      FilterGroupSnTBulkCrew: function () {
        Singular.ShowLoadingBar();
        if ((!ViewModel.ROBulkSnTEditHumanResourceFilter() || ViewModel.ROBulkSnTEditHumanResourceFilter() == '') && (!ViewModel.ROBulkSnTEditDisciplineFilter() || ViewModel.ROBulkSnTEditDisciplineFilter() == '')) {
          ViewModel.GroupSnTBulkCrewList().Iterate(function (bag, Index) {
            bag.Visible(true);
          });
        } else {
          ViewModel.GroupSnTBulkCrewList().Iterate(function (bag, Index) {
            if (!ViewModel.ROBulkSnTEditHumanResourceFilter()) {
              if (bag.Discipline().toLowerCase().indexOf(ViewModel.ROBulkSnTEditDisciplineFilter().toLowerCase()) >= 0) {
                bag.Visible(true);
              } else {
                bag.Visible(false);
              };
            } else if (!ViewModel.ROBulkSnTEditDisciplineFilter()) {
              if (bag.HumanResource().toLowerCase().indexOf(ViewModel.ROBulkSnTEditHumanResourceFilter().toLowerCase()) >= 0) {
                bag.Visible(true);
              } else {
                bag.Visible(false);
              };
            } else if ((bag.HumanResource().toLowerCase().indexOf(ViewModel.ROBulkSnTEditHumanResourceFilter().toLowerCase()) >= 0) && (bag.Discipline().toLowerCase().indexOf(ViewModel.ROBulkSnTEditDisciplineFilter().toLowerCase()) >= 0)) {
              bag.Visible(true);
            } else {
              bag.Visible(false);
            };
          });
        };
        Singular.HideLoadingBar();
      }, //FilterGroupSnTBulkCrew
      SelectedSnTDay: function (SnTDay) {
        SnTDay.SelectedInd(!SnTDay.SelectedInd());
      }, //SelectedSnTDay
      SelectedSnTDayCss: function (SnTDay) {
        if (SnTDay.SelectedInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //SelectedSnTCss
      SelectedSnTDayHtml: function (SnTDay) {
        if (SnTDay.SelectedInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
      }, //SelectedSnTHtml
      BulkBreakfastInd: function () {
        ViewModel.BulkGroupPolicyDetailList()[0].BreakfastInd(!ViewModel.BulkGroupPolicyDetailList()[0].BreakfastInd());
        //ViewModel.BreakfastInd(!ViewModel.BreakfastInd());
      }, //BulkBreakfastInd
      BulkBreakfastIndCss: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].BreakfastInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //BulkBreakfastIndCss
      BulkBreakfastIndHtml: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].BreakfastInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
      }, //BulkBreakfastIndHtml
      BulkLunchInd: function () {
        ViewModel.BulkGroupPolicyDetailList()[0].LunchInd(!ViewModel.BulkGroupPolicyDetailList()[0].LunchInd());
        //ViewModel.LunchInd(!ViewModel.LunchInd());
      }, //BulkLunchInd
      BulkLunchIndCss: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].LunchInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //BulkLunchIndCss
      BulkLunchIndHtml: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].LunchInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
      }, //BulkLunchIndHtml
      BulkDinnerInd: function () {
        ViewModel.BulkGroupPolicyDetailList()[0].DinnerInd(!ViewModel.BulkGroupPolicyDetailList()[0].DinnerInd());
        //ViewModel.DinnerInd(!ViewModel.DinnerInd());
      }, //BulkDinnerInd
      BulkDinnerIndCss: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].DinnerInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //BulkDinnerIndCss
      BulkDinnerIndHtml: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].DinnerInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
      }, //BulkDinnerIndHtml
      BulkIncidentalInd: function () {
        ViewModel.BulkGroupPolicyDetailList()[0].IncidentalInd(!ViewModel.BulkGroupPolicyDetailList()[0].IncidentalInd());
        //ViewModel.IncidentalInd(!ViewModel.IncidentalInd());
      }, //BulkIncidentalInd
      BulkIncidentalIndCss: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].IncidentalInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
      }, //BulkIncidentalIndCss
      BulkIncidentalIndHtml: function () {
        if (ViewModel.BulkGroupPolicyDetailList()[0].IncidentalInd()) { return "<span class=\"glyphicon glyphicon-ok\"></span>" } else { return "<span class=\"glyphicon glyphicon-remove\"></span>" }
      }, //BulkIncidentalIndHtml
      GetBulkGroupPolicyDetailBrokenRulesHTML: function () {
        var ErrorString = ''
        ViewModel.BulkGroupPolicyDetailList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(bfp)
          }
        });
        return ErrorString;
      }, //GetBulkFlightPassengerBrokenRulesHTML
      CanGenerateBulkSnT: function () {
        var BGPDIsValid = true;
        ViewModel.BulkGroupPolicyDetailList().Iterate(function (bfp, Index) {
          if (bfp.IsValid() == false) {
            BGPDIsValid = false;
          }
        });
        return BGPDIsValid;
      } //CanGenerateBulkSnT

    }//Methods
  },//SnT
  SaAirports: function(List, Item) {
  var Allowed = [];
  ClientData.ROAirportList.Iterate(function (Item, Index) {
    if (ViewModel.CurrentSystemID() == 1) {
      if (Item.CountryID == 1 && Item.RedundantInd == false) {
        Allowed.push({ AirportID: Item.AirportID, Airport: Item.Airport });
      };
    }
    else if (ViewModel.CurrentSystemID() == 2) {
      Allowed.push({ AirportID: Item.AirportID, Airport: Item.Airport });
    }
  });
  return Allowed;
} //SaAirports
}//TravelVM
//Tabs
function SetCrewMembersTabVisible() { TravelVM.Tabs.CrewMembers.SetCrewMembersTabVisible() }
function SetFlightsTabVisible() { TravelVM.Tabs.Flights.SetFlightsTabVisible() }
function SetRentalCarsTabVisible() { TravelVM.Tabs.RentalCars.SetRentalCarsTabVisible() }
function SetAccommodationTabVisible() { TravelVM.Tabs.Accommodation.SetAccommodationTabVisible() }
function SetSnTTabVisible() { TravelVM.Tabs.SnT.SetSnTTabVisible() }
function SetTravelReqCommentsTabVisible() { TravelVM.Tabs.TravelReqComments.SetTravelReqCommentsTabVisible() }
function SetTravelAdvancesTabVisible() { TravelVM.Tabs.TravelAdvances.SetTravelAdvancesTabVisible() }
function SetChauffeursTabVisible() { TravelVM.Tabs.Chauffeurs.SetChauffeursTabVisible() }
function SetTravelReportsTabVisible() { TravelVM.Tabs.TravelReports.SetTravelReportsTabVisible() }
function HRClashDetails(Obj) {
  if (Obj != null) {
    return Obj.HumanResource();
  }
  return ""
};
function SupplierHRClashDetails(Obj) {
  if (Obj != null) {
    return Obj.SupplierHumanResource();
  }
  return ""
};
function BulkPassengerVisible(obj) {
  if (ViewModel.ShowAllInd()) {
    if (obj.Visible())
    { return true; }
    else
    { return false; }
  }
  else {
    if (obj.Visible() && !obj.ProductionInBaseCityInd()) {
      return true;
    }
    else {
      return false;
    }
  }
};
function ShowAllCrew() {
  ViewModel.ShowAllInd(!ViewModel.ShowAllInd());
};
function ShowAllCrewCss() {
  return "btn btn-default btn-warning";
};
function ShowAllCrewHtml() {
  if (ViewModel.ShowAllInd()) { return "<span class=\"glyphicon glyphicon-ok\">Yes</span>" } else { return "<span class=\"glyphicon glyphicon-remove\">No</span>" }
};
//Crew Members
function GetCrewMemberCancelledStateColor(CrewMember) {
  var defaultStyle = "";
  if (CrewMember.OnProductionInd() == false) { defaultStyle = "danger" }
  return defaultStyle;
};
function GetCancelledStateColor(CrewMemberDetail) {
  var defaultStyle = "";
  if (CrewMemberDetail.CancelledInd() == true) { defaultStyle = "danger" }
  return defaultStyle;
};
function ViewCrewMemberDetail(CrewMember) {
  ViewModel.CurrentCrewMember(null);
  ViewModel.CurrentCrewMember(CrewMember);
  ViewModel.ShowAllInd(false);
  $('#CrewMemberDetail').modal();
};
//Flights
function IsFlightCancelled(Flight) { TravelVM.Flights.Methods.IsFlightCancelled(Flight); };
function IsFlightCancelledCss(Flight) { return TravelVM.Flights.Methods.IsFlightCancelledCss(Flight); };
function IsFlightCancelledHtml(Flight) { return TravelVM.Flights.Methods.IsFlightCancelledHtml(Flight); };
function RemoveFlight(Flight) { TravelVM.Flights.Methods.RemoveFlight(Flight); };
function CancelledFlightPassengerCount(Flight) { return TravelVM.Flights.Methods.CancelledFlightPassengerCount(Flight); };
function FlightDetails() { return TravelVM.Flights.Methods.FlightDetails(); };
function BookFlightPassengers(Flight) { TravelVM.Flights.Methods.BookFlightPassengers(Flight); };
function OnFlight(BulkFlightPassenger) { TravelVM.Flights.Methods.OnFlight(BulkFlightPassenger); };
function OnFlightCss(BulkFlightPassenger) { return TravelVM.Flights.Methods.OnFlightCss(BulkFlightPassenger); };
function OnFlightHtml(BulkFlightPassenger) { return TravelVM.Flights.Methods.OnFlightHtml(BulkFlightPassenger); };
function CancelledFlightPassenger(BulkFlightPassenger) { TravelVM.Flights.Methods.CancelledFlightPassenger(BulkFlightPassenger); };
function CancelledFlightPassengerCss(BulkFlightPassenger) { return TravelVM.Flights.Methods.CancelledFlightPassengerCss(BulkFlightPassenger); };
function CancelledFlightPassengerHtml(BulkFlightPassenger) { return TravelVM.Flights.Methods.CancelledFlightPassengerHtml(BulkFlightPassenger); };
function ReAddFlightPassenger(BulkFlightPassenger) { TravelVM.Flights.Methods.ReAddFlightPassenger(BulkFlightPassenger); };
function FilterBulkFlightPassenger() { TravelVM.Flights.Methods.FilterBulkFlightPassenger(); };
function GetBulkFlightPassengerBrokenRulesHTML() { return TravelVM.Flights.Methods.GetBulkFlightPassengerBrokenRulesHTML(); };
function GetFlightPassengerClashes(BulkFlightPassenger) { TravelVM.Flights.Methods.GetFlightPassengerClashes(BulkFlightPassenger); };
//Rental Cars
function RemoveRentalCar(RentalCar) { TravelVM.RentalCars.Methods.RemoveRentalCar(RentalCar); };
function CancelledRentalCarPassengersCount(RentalCar) { return TravelVM.RentalCars.Methods.CancelledRentalCarPassengersCount(RentalCar); };
function BookRentalCarPassengers(RentalCar) { TravelVM.RentalCars.Methods.BookRentalCarPassengers(RentalCar); };
function OnRentalCar(BulkRentalCarPassenger) { TravelVM.RentalCars.Methods.OnRentalCar(BulkRentalCarPassenger); };
function OnRentalCss(BulkRentalCarPassenger) { return TravelVM.RentalCars.Methods.OnRentalCss(BulkRentalCarPassenger); };
function OnRentalHTML(BulkRentalCarPassenger) { return TravelVM.RentalCars.Methods.OnRentalHTML(BulkRentalCarPassenger); };
function DriverValid() { TravelVM.RentalCars.Methods.DriverValid(); };
function CancelledRCPassenger(BulkRentalCarPassenger) { TravelVM.RentalCars.Methods.CancelledRCPassenger(BulkRentalCarPassenger); };
function CancelledRCPassengerCss(BulkRentalCarPassenger) { return TravelVM.RentalCars.Methods.CancelledRCPassengerCss(BulkRentalCarPassenger); };
function CancelledRCPassengerHtml(BulkRentalCarPassenger) { return TravelVM.RentalCars.Methods.CancelledRCPassengerHtml(BulkRentalCarPassenger); };
function PassengerCountValid(BulkRentalCarPassenger) { return TravelVM.RentalCars.Methods.PassengerCountValid(BulkRentalCarPassenger); };
function GetBulkRentalCarPassengersBrokenRulesHTML() { return TravelVM.RentalCars.Methods.GetBulkRentalCarPassengersBrokenRulesHTML(); };
function FilterBulkRentalCarPassengers() { TravelVM.RentalCars.Methods.FilterBulkRentalCarPassengers(); };
function RentalCarDetails() { return TravelVM.RentalCars.Methods.RentalCarDetails(); };
function GetRentalCarPassengerClashes(BulkRentalCarPassenger) { TravelVM.RentalCars.Methods.GetRentalCarPassengerClashes(BulkRentalCarPassenger); };
function OpenBulkRentalCarAdd() { TravelVM.RentalCars.Methods.OpenBulkRentalCarAdd(); }
function SetRentalCarDriver(CarRentalCrew) {
  CarRentalCrew.DriverInd(!CarRentalCrew.DriverInd());
};
function SetRentalCarDriverCss(CarRentalCrew) {
  if (CarRentalCrew.DriverInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
};
function SetRentalCarDriverHtml(CarRentalCrew) {
  if (CarRentalCrew.DriverInd()) { return "Driver" } else { return "Select" }
};
function GetBulkRentalCarBrokenRulesHTML() { return TravelVM.RentalCars.Methods.GetBulkRentalCarBrokenRulesHTML(); };
function GenerateRentalCars() {
  ViewModel.BulkRentalCarAddDriverList().Iterate(function (cm, Index) {
    if (cm.DriverInd()) {
      var BulkRentalCar = ViewModel.BulkRentalCarList()[0];
      var newRentalCar = ViewModel.CurrentTravelRequisition().RentalCarList.AddNew();
      newRentalCar.RentalCarAgentID(BulkRentalCar.RentalCarAgentID());
      newRentalCar.CarTypeID(BulkRentalCar.CarTypeID());
      newRentalCar.PickupDateTime(new Date(BulkRentalCar.PickupDateTime()));
      newRentalCar.DropoffDateTime(new Date(BulkRentalCar.DropoffDateTime()));
      newRentalCar.RentalCarRefNo(BulkRentalCar.RentalCarRefNo());
      newRentalCar.AgentBranchIDFrom(BulkRentalCar.AgentBranchIDFrom());
      newRentalCar.AgentBranchIDTo(BulkRentalCar.AgentBranchIDTo());
      newRentalCar.CrewTypeID(BulkRentalCar.CrewTypeID());
      var newRCHumanResource = newRentalCar.RentalCarHumanResourceList.AddNew();
      newRCHumanResource.HumanResourceID(cm.HumanResourceID())
      newRCHumanResource.DriverInd(true);
      var passengerCount = newRentalCar.RentalCarHumanResourceList().length;
      var CarType = ClientData.ROCarTypeList.Find('CarTypeID', newRentalCar.CarTypeID());
      if (CarType != undefined) {
        var MaxNoOfOccupants = CarType.MaxNoOfOccupants;
        var RemainingSeats = '(' + (MaxNoOfOccupants - passengerCount) + ' seats left)';
        newRentalCar.RemainingSeats(RemainingSeats);
      }
    }
  });
  alert('Rental Cars Generated');
};
function RentalCarGenerateButtonVisible() {
  var BRCLCount = ViewModel.BulkRentalCarList().length;
  var BRC = ViewModel.BulkRentalCarList()[0];
  if (BRC != undefined) {
    var BRCValid = ViewModel.BulkRentalCarList()[0].IsValid();
    var DriverCount = 0;
    ViewModel.BulkRentalCarAddDriverList().Iterate(function (bfp, Index) {
      if (bfp.DriverInd() == true) {
        DriverCount++;
      }
    });
    if (BRCLCount > 0 && BRCValid && DriverCount > 0) {
      return true;
    }
  }
  return false;
} //RentalCarGenerateButtonVisible
//Accommodation
function IsAccommodationCancelled(Accommodation) { TravelVM.Accommodation.Methods.IsAccommodationCancelled(Accommodation); };
function IsAccommodationCancelledCss(Accommodation) { return TravelVM.Accommodation.Methods.IsAccommodationCancelledCss(Accommodation); };
function IsAccommodationCancelledHtml(Accommodation) { return TravelVM.Accommodation.Methods.IsAccommodationCancelledHtml(Accommodation); };
function RemoveAccommodation(Accommodation) { TravelVM.Accommodation.Methods.RemoveAccommodation(Accommodation); };
function CancelledAccommodationGuestCount(Accommodation) { return TravelVM.Accommodation.Methods.CancelledAccommodationGuestCount(Accommodation); };
function AccommodationDetails() { return TravelVM.Accommodation.Methods.AccommodationDetails(); };
function FilterBulkAccommodationGuests() { TravelVM.Accommodation.Methods.FilterBulkAccommodationGuests(); };
function OnAccommodation(BulkAccommodationGuest) { TravelVM.Accommodation.Methods.OnAccommodation(BulkAccommodationGuest); };
function OnAccommodationSupplier(BulkAccommodationSupplierGuest) { TravelVM.Accommodation.Methods.OnAccommodationSupplier(BulkAccommodationSupplierGuest); };
function OnAccommodationCss(obj) { return TravelVM.Accommodation.Methods.OnAccommodationCss(obj); };
function OnAccommodationHtml(obj) { return TravelVM.Accommodation.Methods.OnAccommodationHtml(obj); };
function CancelledAccommGuest(BulkAccommodationGuest) { TravelVM.Accommodation.Methods.CancelledAccommGuest(BulkAccommodationGuest); };
function CancelledAccommGuestCss(BulkAccommodationGuest) { return TravelVM.Accommodation.Methods.CancelledAccommGuestCss(BulkAccommodationGuest); };
function CancelledAccommGuestHtml(BulkAccommodationGuest) { return TravelVM.Accommodation.Methods.CancelledAccommGuestHtml(BulkAccommodationGuest); };
function BookAccommodationGuests(Accommodation) {
  if (Accommodation.SupplierID() == null) {
    TravelVM.Accommodation.Methods.BookAccommodationGuests(Accommodation);
  }
  else {
    TravelVM.Accommodation.Methods.BookAccommodationSupplierGuests(Accommodation);
  }
};
function FilterBulkAccommodationSupplierGuests() { TravelVM.Accommodation.Methods.FilterBulkAccommodationSupplierGuests(); };
function GetAccommodationGuestClashes(BulkAccommodationGuest) { TravelVM.Accommodation.Methods.GetAccommodationGuestClashes(BulkAccommodationGuest); };
function GetAccommodationSupplierGuestClashes(BulkAccommodationSupplierGuest) { TravelVM.Accommodation.Methods.GetAccommodationSupplierGuestClashes(BulkAccommodationSupplierGuest); };
function GetBulkAccommodationGuestBrokenRulesHTML() { return TravelVM.Accommodation.Methods.GetBulkAccommodationGuestBrokenRulesHTML(); };
function ReAddAccommodationGuest(BulkAccommodationGuest) { TravelVM.Accommodation.Methods.ReAddAccommodationGuest(BulkAccommodationGuest); };
//S&T
function GetHumanResourceSnTDetails(ProductionHumanResourceSnT) { TravelVM.SnT.Methods.GetHumanResourceSnTDetails(ProductionHumanResourceSnT); };
function SetOnBreakfast(ProductionHumanResourceSnTDetail) { TravelVM.SnT.Methods.SetOnBreakfast(ProductionHumanResourceSnTDetail); };
function GetButtonState(ButtonState) { return TravelVM.SnT.Methods.GetButtonState(ButtonState); };
function GetBreakfastText(ProductionHumanResourceSnTDetail) { return TravelVM.SnT.Methods.GetBreakfastText(ProductionHumanResourceSnTDetail); };
function SetOnLunch(ProductionHumanResourceSnTDetail) { TravelVM.SnT.Methods.SetOnLunch(ProductionHumanResourceSnTDetail); };
function GetLunchText(ProductionHumanResourceSnTDetail) { return TravelVM.SnT.Methods.GetLunchText(ProductionHumanResourceSnTDetail); };
function SetOnDinner(ProductionHumanResourceSnTDetail) { TravelVM.SnT.Methods.SetOnDinner(ProductionHumanResourceSnTDetail); };
function GetDinnerText(ProductionHumanResourceSnTDetail) { return TravelVM.SnT.Methods.GetDinnerText(ProductionHumanResourceSnTDetail); };
function SetOnIncidental(ProductionHumanResourceSnTDetail) { TravelVM.SnT.Methods.SetOnIncidental(ProductionHumanResourceSnTDetail); };
function GetIncidentalText(ProductionHumanResourceSnTDetail) { return TravelVM.SnT.Methods.GetIncidentalText(ProductionHumanResourceSnTDetail); };
function SetupOtherProductionDetails(RefNo, Title, SystemID, ProductionAreaID) { return TravelVM.SnT.Methods.SetupOtherProductionDetails(RefNo, Title, SystemID, ProductionAreaID); };
function HRSnTDetails() { return TravelVM.SnT.Methods.HRSnTDetails(); };
function GetHRSnTDetailBrokenRulesHTML() { return TravelVM.SnT.Methods.GetHRSnTDetailBrokenRulesHTML(); };
function FetchSnTDays() { TravelVM.SnT.Methods.FetchSnTDays(); };
function OpenBulkEditSnT() { TravelVM.SnT.Methods.OpenBulkEditSnT(); };
function SelectedSnT(GroupSnTBulkCrew) { TravelVM.SnT.Methods.SelectedSnT(GroupSnTBulkCrew); };
function SelectAll() { TravelVM.SnT.Methods.SelectAll(); };
function ClearAll() { TravelVM.SnT.Methods.ClearAll(); };
function SelectedSnTCss(GroupSnTBulkCrew) { return TravelVM.SnT.Methods.SelectedSnTCss(GroupSnTBulkCrew); };
function SelectedSnTHtml(GroupSnTBulkCrew) { return TravelVM.SnT.Methods.SelectedSnTHtml(GroupSnTBulkCrew); };
function FilterGroupSnTBulkCrew() { TravelVM.SnT.Methods.FilterGroupSnTBulkCrew(); };
function SelectedSnTDay(SnTDay) { TravelVM.SnT.Methods.SelectedSnTDay(SnTDay); };
function SelectedSnTDayCss(SnTDay) { return TravelVM.SnT.Methods.SelectedSnTDayCss(SnTDay); };
function SelectedSnTDayHtml(SnTDay) { return TravelVM.SnT.Methods.SelectedSnTDayHtml(SnTDay); };
function BulkBreakfastInd() { TravelVM.SnT.Methods.BulkBreakfastInd(); };
function BulkBreakfastIndCss() { return TravelVM.SnT.Methods.BulkBreakfastIndCss(); };
function BulkBreakfastIndHtml() { return TravelVM.SnT.Methods.BulkBreakfastIndHtml(); };
function BulkLunchInd() { TravelVM.SnT.Methods.BulkLunchInd(); };
function BulkLunchIndCss() { return TravelVM.SnT.Methods.BulkLunchIndCss(); };
function BulkLunchIndHtml() { return TravelVM.SnT.Methods.BulkLunchIndHtml(); };
function BulkDinnerInd() { TravelVM.SnT.Methods.BulkDinnerInd(); };
function BulkDinnerIndCss() { return TravelVM.SnT.Methods.BulkDinnerIndCss(); };
function BulkDinnerIndHtml() { return TravelVM.SnT.Methods.BulkDinnerIndHtml(); };
function BulkIncidentalInd() { TravelVM.SnT.Methods.BulkIncidentalInd(); };
function BulkIncidentalIndCss() { return TravelVM.SnT.Methods.BulkIncidentalIndCss(); };
function BulkIncidentalIndHtml() { return TravelVM.SnT.Methods.BulkIncidentalIndHtml(); };
function GetBulkGroupPolicyDetailBrokenRulesHTML() { return TravelVM.SnT.Methods.GetBulkGroupPolicyDetailBrokenRulesHTML(); };
function CanGenerateBulkSnT() { return TravelVM.SnT.Methods.CanGenerateBulkSnT(); };
//Chauffeurs==
function RemoveChauffeur(Chauffeur) { TravelVM.Chauffeurs.Methods.RemoveChauffeur(Chauffeur); };
function IsChauffeurPassengerCancelled(ChauffeurPassenger) { TravelVM.Chauffeurs.Methods.IsChauffeurPassengerCancelled(ChauffeurPassenger); };
function BookChauffeurHR(Chauffeur) { TravelVM.Chauffeurs.Methods.BookChauffeurHR(Chauffeur); };
function OnChauffeurDriver(BulkChauffeurDriverPassenger) { TravelVM.Chauffeurs.Methods.OnChauffeurDriver(BulkChauffeurDriverPassenger); };
function OnCarCss(BulkChauffeurDriverPassenger) { return TravelVM.Methods.OnCarCss(BulkChauffeurDriverPassenger); };
function OnCarHTML(BulkChauffeurDriverPassenger) { return TravelVM.Methods.OnCarHTML(BulkChauffeurDriverPassenger); };
function IsChauffeurCancelled(Chauffeur) { TravelVM.Chauffeurs.Methods.IsChauffeurCancelled(Chauffeur); };
function IsChauffeurCancelledCss(Chauffeur) { return TravelVM.Chauffeurs.Methods.IsChauffeurCancelledCss(Chauffeur); };
function IsChauffeurCancelledHtml(Chauffeur) { return TravelVM.Chauffeurs.Methods.IsChauffeurCancelledHtml(Chauffeur); };
function GetChauffeurPassengerClashes(BulkChauffeurDriverPassenger) { TravelVM.Chauffeurs.Methods.GetChauffeurPassengerClashes(BulkChauffeurDriverPassenger); };
function CancelledCDPassenger(BulkChauffeurDriverPassenger) { TravelVM.Chauffeurs.Methods.CancelledCDPassenger(BulkChauffeurDriverPassenger); };
function CancelledCDPassengerCss(BulkChauffeurDriverPassenger) { return Chauffeurs.Methods.CancelledCDPassengerCss(BulkChauffeurDriverPassenger); };
function CancelledCDPassengerHtml(BulkChauffeurDriverPassenger) { return Chauffeurs.Methods.CancelledCDPassengerHtml(BulkChauffeurDriverPassenger); };
//===========
function GenerateSnT() {
  Singular.SendCommand('GenerateSnT', {},
  function (d) {
    alert('S&T Generated');
    $('#BulkEditSnT').modal('hide');
    //    Singular.ShowMessage("S&T", "S&T Generated", 2, function () {
    //      $('#BulkEditSnT').modal('hide'); 
    //    });
  });
  //$('#BulkEditSnT').modal('hide'); 
};
function OnDriver(BulkRentalCarPassenger) {
  BulkRentalCarPassenger.DriverInd(!BulkRentalCarPassenger.DriverInd());
}
function OnCoDriver(BulkRentalCarPassenger) {
  BulkRentalCarPassenger.CoDriverInd(!BulkRentalCarPassenger.CoDriverInd());
}
function LicenceIndCss(BulkRentalCarPassenger) {
  if (BulkRentalCarPassenger.DriversLicenceInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
};
function LicenceIndHtml(BulkRentalCarPassenger) {
  if (BulkRentalCarPassenger.DriversLicenceInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
};
// driver
function DriverIndCss(BulkRentalCarPassenger) {
  if (BulkRentalCarPassenger.DriverInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
};
function DriverIndHtml(BulkRentalCarPassenger) {
  if (BulkRentalCarPassenger.DriverInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
};
//CoDriver
function CoDriverIndCss(BulkRentalCarPassenger) {
  if (BulkRentalCarPassenger.CoDriverInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
};
function CoDriverIndHtml(BulkRentalCarPassenger) {
  if (BulkRentalCarPassenger.CoDriverInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
};
function AutoCalculateSnT() {
  Singular.SendCommand("AutoCalculateSnT", {}, function (response) {
  });
};
function FindProductionHumanResourceSnTDetail(self) {
  var phrsd = self;
  var phrs = ViewModel.CurrentProductionHRSnT();
  var child = phrs.ProductionHumanResourceSnTDetailList().Find("GroupHumanResourceSnTID", phrsd.GroupHumanResourceSnTID());
  return child;
};
//Modal Events
$('#BulkFlightPassengers').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  var DirtyCount = 0;
  ViewModel.BulkFlightPassengerList().Iterate(function (bfp, Index) {
    if (bfp.IsValid() == false) {
      ErrorCount++;
    }
    if (bfp.IsDirty() == true) {
      DirtyCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else if (DirtyCount > 0) {
    //Singular.SendCommand('Save', {}, function () { });
    Singular.SendCommand('SaveFlights', {}, function () { });
    return true;
  }
})
$('#BulkRentalCarPassengers').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  ViewModel.BulkRentalCarPassengerList().Iterate(function (bfp, Index) {
    if (bfp.IsValid() == false) {
      ErrorCount++;
    }
  });
  if (!ViewModel.CurrentRentalCar().IsValid()) {
    ErrorCount++;
  }
  var DirtyCount = 0;
  ViewModel.BulkRentalCarPassengerList().Iterate(function (brcp, Index) {
    if (brcp.IsDirty() == true) {
      DirtyCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else if (DirtyCount > 0) {
    //Singular.SendCommand('Save', {}, function () { });
    Singular.SendCommand('SaveRentalCars', {}, function () { });
    return true;
  }
})
$('#BulkAccommodationGuests').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  var DirtyCount = 0;
  ViewModel.BulkAccommodationGuestList().Iterate(function (bag, Index) {
    if (bag.IsValid() == false) {
      ErrorCount++;
    }
    if (bag.IsDirty() == true) {
      DirtyCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else if (DirtyCount > 0) {
    Singular.SendCommand('SaveAccommodation', {}, function () { });
    //Singular.SendCommand('Save', {}, function () { });
    return true;
  }
})
$('#BulkAccommodationSupplierGuests').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  ViewModel.BulkAccommodationSupplierGuestList().Iterate(function (bag, Index) {
    if (bag.IsValid() == false) {
      ErrorCount++;
    }
  });
  var DirtyCount = 0;
  ViewModel.BulkAccommodationSupplierGuestList().Iterate(function (brcp, Index) {
    if (brcp.IsDirty() == true) {
      DirtyCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else if (DirtyCount > 0) {
    //Singular.SendCommand('Save', {}, function () { });
    Singular.SendCommand('SaveAccommodationSupplier', {}, function () { });
    return true;
  }
})
$('#BulkChauffeurPassengers').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  ViewModel.BulkChauffeurPassengerList().Iterate(function (bfp, Index) {
    if (bfp.IsValid() == false) {
      ErrorCount++;
    }
  });
  if (!ViewModel.CurrentChauffeur().IsValid()) {
    ErrorCount++;
  }
  var DirtyCount = 0;
  ViewModel.BulkChauffeurPassengerList().Iterate(function (brcp, Index) {
    if (brcp.IsDirty() == true) {
      DirtyCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else if (DirtyCount > 0) {
    Singular.SendCommand('SaveChauffeurPassengers', {}, function () { });
    //Singular.SendCommand('CancelledCDPassenger', {}, function () { });
    //Singular.SendCommand('Save', {}, function () { });
    return true;
  }
})
$('#BulkHRSnTDetails').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  ViewModel.BulkHRSnTDetailList().Iterate(function (bag, Index) {
    if (bag.IsValid() == false) {
      ErrorCount++;
    }
  });
  var DirtyCount = 0;
  ViewModel.BulkHRSnTDetailList().Iterate(function (brcp, Index) {
    if (brcp.IsDirty() == true) {
      DirtyCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else if (DirtyCount > 0) {
    Singular.SendCommand('Save', {}, function () { });
    return true;
  }
})
$('#BulkEditSnT').on('hide.bs.modal', function (e) {
  var ErrorCount = 0
  ViewModel.BulkGroupPolicyDetailList().Iterate(function (bag, Index) {
    if (bag.IsValid() == false) {
      ErrorCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    Singular.ShowMessage('Warning', 'Please Fix Errors', 2)
    alert('Please fix Errors');
    return false;
  }
  else {
    return true;
  }
})
$('#BulkAddRentalCar').on('hide.bs.modal', function (e) {
  ViewModel.BulkRentalCarList().Clear();
  var ErrorCount = 0
  ViewModel.BulkRentalCarList().Iterate(function (bag, Index) {
    if (bag.IsValid() == false) {
      ErrorCount++;
    }
  });
  if (ErrorCount > 0) {
    e.preventDefault();
    alert('Please fix Errors');
    return false;
  }
  else {
    Singular.SendCommand('Save', {}, function () { });
    return true;
  }
});


