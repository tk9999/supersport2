﻿ProductionHelper = {
	Production: {
		UI: {
			CrewFinalisedCss: function (Production) {
				if (Production.CrewFinalised()) { return 'btn btn-info' } else { return 'btn btn-default' }
			},
			CrewFinalisedHtml: function (Production) {
				if (Production.CrewFinalised()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			PlanningFinalisedCss: function (Production) {
				if (Production.PlanningFinalised()) { return 'btn btn-success' } else { return 'btn btn-default' }
			},
			PlanningFinalisedHtml: function (Production) {
				if (Production.PlanningFinalised()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			ReconciledCss: function (Production) {
				if (Production.Reconciled()) { return 'btn btn-warning' } else { return 'btn btn-default' }
			},
			ReconciledHtml: function (Production) {
				if (Production.Reconciled()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			CancelledCss: function (Production) {
				if (Production.Cancelled()) { return 'btn btn-danger' } else { return 'btn btn-default' }
			},
			CancelledHtml: function (Production) {
				if (Production.Cancelled()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			ReconciledCss: function (Production) {
				if (Production.Reconciled()) { return 'btn btn-warning' } else { return 'btn btn-default' }
			},
			ReconciledHtml: function (Production) {
				if (Production.Reconciled()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			CancelledCss: function (Production) {
				if (Production.Cancelled()) { return 'btn btn-danger' } else { return 'btn btn-default' }
			},
			CancelledHtml: function (Production) {
				if (Production.Cancelled()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			HDRequiredCss: function (Production) {
				if (Production.HDRequiredInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' }
			},
			HDRequiredHtml: function (Production) {
				if (Production.HDRequiredInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			}
		},
		Rules: {
			CheckCanReconcile: function (Production) {
				//if not reconciled
				var SystemID = ViewModel.CurrentProduction().ProductionSystemAreaList()[0].SystemID();
				if (Production.Reconciled() && SystemID == 1) {
					var CannotReconcileReason = "";

					//Access Rights
					var AllowedToReconcile = Production.CanReconcileAccess();

					if (!AllowedToReconcile) {
						CannotReconcileReason &= "You are not authorised to reconcile this production <br/>"
					}

					if (AllowedToReconcile) {
						//Has S&T Documents
						var SAndTDocCount = 0;
						Production.ProductionDocumentList().Iterate(function (Document, Index) {
							if (Document.DocumentTypeID() == 16) { SAndTDocCount += 1 }
						});
						if (SAndTDocCount == 0) {
							CannotReconcileReason += "S&T Document required: ";
						}
						//Has PA Forms
						var PAFormCount = 0;
						Production.ProductionDocumentList().Iterate(function (Document, Index) {
							if (Document.DocumentTypeID() == 6) { PAFormCount += 1 }
						});
						if (PAFormCount < 2) {
							CannotReconcileReason += "2 PA Forms (Sign-In and Sign-Out) required"
						}
					}
					Production.CannotReconcileReason(CannotReconcileReason);

				} else {
					//check if can un-reconile
					//this is done in the enable binding
					Production.CannotReconcileReason("");
				}
			},
			CanEditProductionType: function (Production) {
				if (ProductionHelper.CurrentArea().ProductionSpecRequirementID()) { return false } else { return true }
			},
			CanEditEventType: function (Production) {
				if (ProductionHelper.CurrentArea().ProductionSpecRequirementID()) { return false } else { return true }
			},
			CanClickReconcile: function (Production) {
				if (Production.Cancelled() || !Production.PlanningFinalised()) {
					return false;
				}
				else if (!Production.Reconciled()) {
					if (Production.CanReconcileAccess()) {
						return true;
					}
				} else {
					//Can Un Reconcile
					if (Production.CanUnReconcileAccess()) {
						return true;
					} else {
						//Can Un Reconcile Today
						var RecDate = new Date(Production.ReconciledSetDate());
						var Today = new Date();
						if (Production.CanUnReconcileTodayAccess() && Production.CurrentUserIsEventManager() && OBMisc.Dates.SameDay(RecDate, Today)) {
							return true;
						} else {
							return false;
						}
					}
				}
				return false;
			},
			CanClickCrewFinalise: function (Production) {
				if (Production.Cancelled() || !Production.TimelineReadyInd()) {
					return false;
				} else if (!Production.CrewFinalised()) {
					if (Production.CanFinaliseCrewAccess()) {
						return true;
					}
				} else {
					//Can Un Finalise Crew
					if (Production.CanUnFinaliseCrewAccess()) {
						return true;
					} else {
						return false;
					}
				}
				return false;
			},
			CheckTravelTimelines: function (Value, Rule, CtlError) { 
				var psa = CtlError.Object
				var travelTimelines = psa.ProductionTimelineList().filter(item => {  return ([42,43].indexOf(item.ProductionTimelineTypeID()) >= 0) })
				if (psa.ChecklTravelTimelineRule() && travelTimelines.length < 2) { 
					CtlError.AddError("Travel timelines are required")
				}
			},
			AfterTimelineRemoved: function(timeline) { 
				var psa = timeline.GetParent();
				Singular.Validation.CheckRules(psa)
			}
		},
		Events: {
			CrewPlanningFinalisedClicked: function (Production) {
				Production.CrewFinalised(!Production.CrewFinalised());
				if (Production.CrewFinalised()) {
					Production.CrewPlanningFinalisedDate(new Date())
					Production.CrewPlanningFinalisedBy(ViewModel.CurrentUserID())
					ProductionHelper.CurrentArea().ProductionAreaStatusID(2); //crew finalised
					if (!Production.CrewPlanningFirstFinalisedDate()) {
						Production.CrewPlanningFirstFinalisedDate(new Date());
					}
				} else {
					Production.CrewPlanningFinalisedDate(null)
					Production.CrewPlanningFinalisedBy(null)
					if (Production.Cancelled()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(5);
					} else if (Production.Reconciled()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(4);
					} else if (Production.PlanningFinalised()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(3);
					} else {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
					}
				}
			},
			PlanningFinalisedClicked: function (Production) {
				Production.PlanningFinalised(!Production.PlanningFinalised());
				if (Production.PlanningFinalised()) {
					Production.PlanningFinalisedDate(new Date())
					Production.PlanningFinalisedBy(ViewModel.CurrentUserID())
					ProductionHelper.CurrentArea().ProductionAreaStatusID(3); //planning finalised
				} else {
					Production.PlanningFinalisedDate(null)
					Production.PlanningFinalisedBy(null)
					if (Production.Cancelled()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(5);
					} else if (Production.Reconciled()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(4);
					} else if (Production.CrewFinalised()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(2);
					} else {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
					}
				}
			},
			ReconciledClicked: function (Production) {
				if (!Production.Reconciled()) {
					if (ViewModel.CurrentProduction().TravelRequisitionList().length > 0) { // If Travel Req attached, see if HR on flights
						var CostsAllocated = true;
						var ErrorString = '';
						Singular.GetDataStateless('OBLib.Productions.ReadOnly.ROOBProductionRequirementList, OBLib', {
							ProductionID: Production.ProductionID(),
							SystemID: ViewModel.CurrentSystemID()
						}, function (args) {
							if (args.Success) {
								KOFormatter.Deserialise(args.Data, ViewModel.ROOBProductionRequirementList);
								if (!ViewModel.ROOBProductionRequirementList()[0].FlightHRCostsValid()) {
									CostsAllocated = false;
									ErrorString = ViewModel.ROOBProductionRequirementList()[0].ErrorMessage();
								}
								if (CostsAllocated) { // Only if costs allocated you can reconcile
									Production.Reconciled(true);
									if (!Production.ReconciledDate()) {
										Production.ReconciledDate(new Date());
									}
									if (!Production.ReconciledSetDate()) {
										Production.ReconciledSetDate(new Date());
									}
									Production.ReconciledBy(ViewModel.CurrentUserID());
									ProductionHelper.CurrentArea().ProductionAreaStatusID(4); //reconciled
								} else {
									alert(ErrorString);
								}
								Singular.HideLoadingBar();
							}
						});
					}
					else { // No Travel Req to check, attempt to reconcile
						if (!Production.ReconciledDate()) {
							Production.ReconciledDate(new Date());
						}
						if (!Production.ReconciledSetDate()) {
							Production.ReconciledSetDate(new Date());
						}
						Production.ReconciledBy(ViewModel.CurrentUserID());
						ProductionHelper.CurrentArea().ProductionAreaStatusID(4); //reconciled
					}
				} else {
					Singular.ShowMessageQuestion("Un-Reconcile", "Are you sure you wish to un-reconcile this production?", function () {
						Singular.SendCommand("UnReconcile", {}, function () {
							//Production.ReconciledDate(null)
							//Production.ReconciledBy(null)
							//Production.ReconciledDate(null)
							if (Production.Cancelled()) {
								ProductionHelper.CurrentArea().ProductionAreaStatusID(5);
							} else if (Production.PlanningFinalised()) {
								ProductionHelper.CurrentArea().ProductionAreaStatusID(3);
							} else if (Production.CrewFinalised()) {
								ProductionHelper.CurrentArea().ProductionAreaStatusID(2);
							} else {
								ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
							}
							ProductionHelper.Production.Rules.CheckCanReconcile(Production);
						});
					})
				}
			},
			ReconciledDateClick: function (Production) {
				if (!Production.Reconciled()) {
					//ProductionHelper.Production.Events.ReconciledClicked(Production)
					Production.Reconciled(true)
					Production.ReconciledSetDate(new Date())
					Production.ReconciledBy(ViewModel.CurrentUserID())
					ProductionHelper.CurrentArea().ProductionAreaStatusID(4) //reconciled
				}
			},
			CancelledClicked: function (Production) {
				Production.Cancelled(!Production.Cancelled());
				if (Production.Cancelled()) {
					Production.CancelledDate(new Date())
					ProductionHelper.CurrentArea().ProductionAreaStatusID(5); //cancelled
				} else {
					Production.CancelledDate(null)
					//check the latest status
					if (Production.Reconciled()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(4);
					} else if (Production.PlanningFinalised()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(3);
					} else if (Production.CrewFinalised()) {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(2);
					} else {
						ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
					}
				}
			},
			HDRequiredClick: function (Production) {
				Production.HDRequiredInd(!Production.HDRequiredInd())
			},
			ProductionVenueIDSet: function (Production) {
				if (Production.ProductionVenueID() && Production.VenueConfirmedDate()) {
					Singular.SendCommand('SetupAutoServices', {}, function (d) {
						ProductionHelper.Production.Methods.SetDefaultProductionDescription(Production);
					})
				}
			},
			TimelineReadySet: function (Production) {
				if (Production.TimelineReadyInd()) {
					Production.TimelineReadyDateTime(new Date());
					Production.TimelineReadyByUserID(ViewModel.CurrentUserID());
				} else {
					Production.TimelineReadyDateTime(null);
					Production.TimelineReadyByUserID(null);
				}
			}
		},
		Methods: {
			FindROProductionSpec: function () {
				Singular.GetDataStateless('OBLib.Productions.Specs.ReadOnly.ROProductionSpecRequirementList, OBLib', {
					ProductionSpecRequirementID: null,
					ProductionID: null,
					ProductionTypeID: ViewModel.CurrentProduction().ProductionTypeID(),
					EventTypeID: ViewModel.CurrentProduction().EventTypeID(),
					SystemID: ViewModel.CurrentProduction().ProductionSystemAreaList()[0].SystemID(),
					ProductionAreaID: ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionAreaID()
				}, function (args) {
					if (args.Success) {
						KOFormatter.Deserialise(args.Data, ViewModel.ROProductionSpecRequirementList);
						$('#SelectROProductionSpec').modal();
						//            $("#SelectROProductionSpec").draggable({
						//              handle: ".modal-header"
						//            });
					}
				});
			},
			FilterROProductionSpec: function () {
				if (!ViewModel.SpecFilter() || ViewModel.SpecFilter() == '') {
					ViewModel.ROProductionSpecRequirementList().Iterate(function (ROProductionSpec, Index) {
						ROProductionSpec.VisibleClient(true);
					});
				} else {
					ViewModel.ROProductionSpecRequirementList().Iterate(function (ROProductionSpec, Index) {
						if (ROProductionSpec.ProductionSpecRequirementName().toLowerCase().indexOf(ViewModel.SpecFilter().toLowerCase()) >= 0) {
							ROProductionSpec.VisibleClient(true);
						} else {
							ROProductionSpec.VisibleClient(false);
						};
					});
				};
			},
			PopulateSpec: function (ROProductionSpec) {
				Singular.SendCommand("PopulateProductionSpec", { ProductionSpecRequirementID: ROProductionSpec.ProductionSpecRequirementID() }, function (d) {
					$('#SelectROProductionSpec').modal('hide')
				})
			},
			ClearProductionSpec: function () {
				ViewModel.CurrentProduction().ProductionSpecRequirementID(null);
				ViewModel.CurrentProduction().SpecName("");
			},
			FindROProductionVenue: function () {
				Singular.ShowLoadingBar();
				//ProductionTypeID: ViewModel.CurrentProduction().ProductionTypeID() 
				Singular.GetDataStateless('OBLib.Maintenance.General.ReadOnly.ROProductionVenueFullList, OBLib', {}, function (args) {
					if (args.Success) {
						KOFormatter.Deserialise(args.Data, ViewModel.ROProductionVenueFullList);
						Singular.HideLoadingBar();
						ProductionHelper.Production.Methods.FilterROProductionVenues();
						$('#SelectROProductionVenue').modal();
						//            $("#SelectROProductionVenue").draggable({
						//              handle: ".modal-header"
						//            });
					}
				});
			},
			FilterROProductionVenues: function () {
				if (!ViewModel.VenueFilter() || ViewModel.VenueFilter() == '') {
					ViewModel.ROProductionVenueFullList().Iterate(function (ROProductionVenue, Index) {
						ROProductionVenue.VisibleClient(true);
					});
				} else {
					ViewModel.ROProductionVenueFullList().Iterate(function (ROProductionVenue, Index) {
						if (ROProductionVenue.ProductionVenue().toLowerCase().indexOf(ViewModel.VenueFilter().toLowerCase()) >= 0) {
							ROProductionVenue.VisibleClient(true);
						} else {
							ROProductionVenue.VisibleClient(false);
						};
					});
				};
			},
			SetProductionVenueID: function (ROProductionVenue) {
				ViewModel.CurrentProduction().ProductionVenue(ROProductionVenue.ProductionVenue());
				ViewModel.CurrentProduction().ProductionVenueID(ROProductionVenue.ProductionVenueID());
			},
			SetVenueConfirmedDate: function (Production) { if (Production.ProductionVenueID()) { Production.VenueConfirmedDate(new Date()) } else { Production.VenueConfirmedDate(null) } },
			UpdateTx: function (Production) {
				if (Production.IsNew() && Production.PlayStartDateTime() && Production.PlayEndDateTime()) {
					var Area = ProductionHelper.CurrentArea();
					//Production Services - OB
					if (Area.SystemID() == 1 && Area.ProductionAreaID() == 1) {
						ProductionHelper.Production.Methods.UpdateTimlinePSOB(Production)

						//Production Content - OB
					} else if (Area.SystemID() == 2 && Area.ProductionAreaID() == 1) {
						ProductionHelper.Production.Methods.UpdateTimlinePCOB(Production)

						//
					}
				}
			},
			UpdateTimlinePSOB: function (Production) {

				Production.SuspendChanges = true

				var TimelineCount = ProductionHelper.CurrentArea().ProductionTimelineList().length;
				var Tx = null;
				//no tx yet added
				if (TimelineCount == 0) {

					//Setup Tx
					Tx = ProductionHelper.CurrentArea().ProductionTimelineList.AddNew()
					Tx.SuspendChanges = true
					Tx.ProductionID(ProductionHelper.CurrentArea().ProductionID())
					Tx.ProductionTimelineTypeID(11)
					Tx.ProductionTimelineType("Transmission")
					Tx.StartDateTime(new Date(Production.PlayStartDateTime()))
					Tx.EndDateTime(new Date(Production.PlayEndDateTime()))
					Tx.SuspendChanges = false

					//          //Add temp time before
					//          var TimeBefore = ProductionHelper.CurrentArea().ProductionTimelineList.AddNew()
					//          TimeBefore.ProductionID(ProductionHelper.CurrentArea().ProductionID())
					//          TimeBefore.ProductionTimelineTypeID(41)
					//          TimeBefore.ProductionTimelineType("Temp Timeline - Bookings")
					//          TimeBefore.StartDateTime()
					//          TimeBefore.EndDateTime(new Date(Production.PlayStartDateTime()))

					//          //Add temp time after
					//          var TimeAfter = ProductionHelper.CurrentArea().ProductionTimelineList.AddNew()
					//          TimeAfter.ProductionID(ProductionHelper.CurrentArea().ProductionID())
					//          TimeAfter.ProductionTimelineTypeID(41)
					//          TimeAfter.ProductionTimelineType("Temp Timeline - Bookings")
					//          TimeAfter.StartDateTime(new Date(Production.PlayEndDateTime()))
					//          TimeAfter.EndDateTime()

					//Tx already added
				} else if (TimelineCount == 1) {

					//Update Tx
					Tx = ProductionHelper.CurrentArea().ProductionTimelineList()[0]
					Tx.SuspendChanges = true
					Tx.StartDateTime(new Date(Production.PlayStartDateTime()))
					Tx.EndDateTime(new Date(Production.PlayEndDateTime()))
					Tx.SuspendChanges = false

					//Add temp time before


					//Add temp time after



					//Tx and Temp timelines already added
				} else if (TimelineCount > 1) {

					var TempTimelines = ProductionHelper.CurrentArea().ProductionTimelineList().Filter('ProductionTimelineTypeID', 11)

					//Update Tx
					Tx = ProductionHelper.CurrentArea().ProductionTimelineList().Find('ProductionTimelineTypeID', 11)
					Tx.SuspendChanges = true
					Tx.StartDateTime(new Date(Production.PlayStartDateTime()))
					Tx.EndDateTime(new Date(Production.PlayEndDateTime()))
					Tx.SuspendChanges = false

					//          //Update temp time before
					//          var TimeBefore = TempTimelines[0]
					//          TimeBefore.ProductionID(ProductionHelper.CurrentArea().ProductionID())
					//          TimeBefore.ProductionTimelineTypeID(41)
					//          TimeBefore.ProductionTimelineType("Temp Timeline - Bookings")
					//          TimeBefore.StartDateTime()
					//          TimeBefore.EndDateTime(new Date(Production.PlayStartDateTime()))

					//          //Update temp time after
					//          var TimeAfter = TempTimelines[1]
					//          TimeAfter.ProductionID(ProductionHelper.CurrentArea().ProductionID())
					//          TimeAfter.ProductionTimelineTypeID(41)
					//          TimeAfter.ProductionTimelineType("Temp Timeline - Bookings")
					//          TimeAfter.StartDateTime(new Date(Production.PlayEndDateTime()))
					//          TimeAfter.EndDateTime()

				}

				Production.SuspendChanges = false
			},
			UpdateTimlinePCOB: function () {

			},
			FetchROProductionVenueList: function (CallBack) {
				Singular.GetDataStateless('OBLib.Maintenance.ReadOnly.ROProductionVenueList, OBLib', {}, function (args) {
					if (args.Success) {
						KOFormatter.Deserialise(args.Data, ViewModel.ROProductionVenueList);
						CallBack
					}
				});
			},
			SetDefaultProductionDescription: function (Production) {
				var desc = '';
				if (Production.ProductionTypeID()) {
					var pt = ClientData.ROProductionTypeList.Find('ProductionTypeID', Production.ProductionTypeID())
					desc += pt.ProductionType + ' ';
				}
				if (Production.EventTypeID()) {
					var et = ClientData.ROEventTypeList.Find('EventTypeID', Production.EventTypeID())
					desc += '(' + et.EventType + ') ';
				}
				if (Production.ProductionVenueID()) {
					var pv = ClientData.ROProductionVenueList.Find('ProductionVenueID', Production.ProductionVenueID())
					desc += '(' + pv.ProductionVenue + ') ';
				}
				var times = '';
				Production.ProductionSystemAreaList().Iterate(function (Area, Index) {
					Area.ProductionTimelineList().Iterate(function (Timeline, Index) {
						if (Timeline.IsValid() && Timeline.ProductionTimelineTypeID() == 11) {
							var sd = new Date(Timeline.StartDateTime());
							if (times.length == 0) {
								times += moment(sd).format('HH:mm');
							} else {
								times += ',' + moment(sd).format('HH:mm');
							}
						}
					});
				});
				desc += ' (' + times + ')';
				Production.ProductionDescription(desc);
			}
		}
	},
	Spec: {
		Rules: {
			EquipmentTypeMeetsSpecRequirement: function (ProductionSpecRequirementEquipmentType) {
				var self = ProductionSpecRequirementEquipmentType;
				if (self.IsValid() && self.Quantity != 0 && ViewModel.CurrentProduction()) {
					ProductionHelper.ProductionSystemAreas.Methods.CheckSpecValidServer();
				}
			},
			PositionMeetsSpecRequirement: function (ProductionSpecRequirementPosition) {
				var self = ProductionSpecRequirementPosition;
				if (self.IsValid() && self.Quantity != 0 && ViewModel.CurrentProduction()) {
					ProductionHelper.ProductionSystemAreas.Methods.CheckSpecValidServer();
				}
			},
			CanAddProductionEquipment: function () {
				if (ViewModel.CurrentProduction().ProductionTypeID() && ViewModel.CurrentProduction().ProductionVenueID()) {
					return true
				} else {
					return false
				}
			},
			CanAddRequirementPosition: function () {
				if (ViewModel.CurrentProduction().ProductionTypeID()) {
					return true
				} else {
					return false
				}
			},
			CanAddRequirementEquipmentType: function () {
				if (ViewModel.CurrentProduction().ProductionTypeID()) {
					return true
				} else {
					return false
				}
			}
		},
		Methods: {
			AddEquipment: function () {
				var Area = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
				var NewItem = Area.ProductionSpecRequirementEquipmentTypeList.AddNew()
				NewItem.ProductionID(Area.ProductionID())
				NewItem.ProductionAreaID(Area.ProductionAreaID())
				NewItem.SystemID(Area.SystemID())
			},
			AddPosition: function () {
				var Area = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
				var NewItem = Area.ProductionSpecRequirementPositionList.AddNew()
				NewItem.ProductionID(Area.ProductionID())
				NewItem.ProductionAreaID(Area.ProductionAreaID())
				NewItem.SystemID(Area.SystemID())
			},
			RemoveEquipment: function (Equipment) {
				var Area = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
				Area.ProductionSpecRequirementEquipmentTypeList.Remove(Equipment);
			},
			RemovePosition: function (Position) {
				var Area = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
				Area.ProductionSpecRequirementPositionList.Remove(Position);
			}
		}
	},
	CurrentArea: function () {
		return ViewModel.CurrentProduction().ProductionSystemAreaList()[0]
	},
	CurrentSystemID: function () {
		return this.CurrentArea().SystemID()
	},
	CurrentProductionAreaID: function () {
		return this.CurrentArea().ProductionAreaID()
	},
	OutsourceServices: {
		UI: {
			NotRequiredIndCss: function (Service) {
				if (Service.NotRequiredInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-success' }
			},
			NotRequiredIndHtml: function (Service) {
				if (Service.NotRequiredInd()) { return "<span class='glyphicon glyphicon-minus'></span> No" } else { return "<span class='glyphicon glyphicon-check'></span> Yes" }
			},
			CaterBreakfastCss: function (ServiceTimeline) {
				if (ServiceTimeline.CateringBreakfastInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			CaterBreakfastHtml: function (ServiceTimeline) {
				if (ServiceTimeline.CateringBreakfastInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			CaterLunchCss: function (ServiceTimeline) {
				if (ServiceTimeline.CateringLunchInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			CaterLunchHtml: function (ServiceTimeline) {
				if (ServiceTimeline.CateringLunchInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			CaterDinnerCss: function (ServiceTimeline) {
				if (ServiceTimeline.CateringDinnerInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			CaterDinnerHtml: function (ServiceTimeline) {
				if (ServiceTimeline.CateringDinnerInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			ServiceDetailRequiredIndCss: function (ServiceDetail) {
				if (ServiceDetail.RequiredInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-danger' }
			},
			ServiceDetailRequiredIndHtml: function (ServiceDetail) {
				if (ServiceDetail.RequiredInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			}
		},
		Rules: {
			CanAddProductionOutsourceService: function () {
				if (ViewModel.CurrentProduction().ProductionTypeID() && ViewModel.CurrentProduction().ProductionVenueID()) {
					return true
				} else {
					return false
				}
			},
			OutsourceServiceTypeIDValid: function (Value, Rule, CtlError) {
				var ProductionOutsourceService = CtlError.Object;
				var ROOutsourceServiceType = ClientData.ROOutsourceServiceTypeList.Find("OutsourceServiceTypeID", ProductionOutsourceService.OutsourceServiceTypeID());
				if (ROOutsourceServiceType) {
					var TimelineCount = (ProductionOutsourceService.ProductionOutsourceServiceTimelineList() ? ProductionOutsourceService.ProductionOutsourceServiceTimelineList().length : 0)
					if (!ProductionOutsourceService.NotRequiredInd() && ROOutsourceServiceType.ServiceTimesRequiredInd && TimelineCount == 0) {
						if (ProductionOutsourceService.GetParent().GetParent().PlanningFinalised()) {
							CtlError.AddError("Please capture a timeline for this Outsource Service")
						} else {
							CtlError.AddWarning("Please capture a timeline for this Outsource Service")
						}
					}
				}
			},
			NotRequiredIndValid: function (Value, Rule, CtlError) {
				var ProductionOutsourceService = CtlError.Object;
				var ROOutsourceServiceType = ClientData.ROOutsourceServiceTypeList.Find("OutsourceServiceTypeID", ProductionOutsourceService.OutsourceServiceTypeID());
				if (ROOutsourceServiceType) {
					if (ROOutsourceServiceType.AlwaysRequiredInd && ProductionOutsourceService.NotRequiredInd && ProductionOutsourceService.NotRequiredReason().trim().length == 0) {
						CtlError.AddError("A reason is required for this outsource service not being required")
					}
				}
			},
			QuotedAmountValid: function (Value, Rule, CtlError) {
				var OSS = CtlError.Object;
				var SystemArea = OSS.GetParent();
				var Production = SystemArea.GetParent();
				if (SystemArea.SystemID() == 1 && SystemArea.ProductionAreaID() == 1 && OSS.QuotedAmount() <= 0 && Production.Reconciled()) {
					CtlError.AddError("Amount must be more than 0");
				}
			}
		},
		Methods: {
			AddNewOutsourceService: function () {
				var NewItem = ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionOutsourceServiceList.AddNew();
				NewItem.SystemID(ProductionHelper.CurrentArea().SystemID());
				NewItem.ProductionID(ProductionHelper.CurrentArea().ProductionID());
				if (NewItem.ProductionAreaID() != undefined) {
					NewItem.ProductionAreaID(ProductionHelper.CurrentArea().ProductionAreaID());
				}
			},
			NotRequiredIndClick: function (Service) {
				Service.NotRequiredInd(!Service.NotRequiredInd());
			},
			CaterBreakfastClick: function (ServiceTimeline) {
				ServiceTimeline.CateringBreakfastInd(!ServiceTimeline.CateringBreakfastInd());
			},
			CaterLunchClick: function (ServiceTimeline) {
				ServiceTimeline.CateringLunchInd(!ServiceTimeline.CateringLunchInd());
			},
			CaterDinnerClick: function (ServiceTimeline) {
				ServiceTimeline.CateringDinnerInd(!ServiceTimeline.CateringDinnerInd());
			},
			ServiceDetailRequiredIndClick: function (ServiceDetail) {
				ServiceDetail.RequiredInd(!ServiceDetail.RequiredInd());
			},
			IsCatering: function (ServiceTimeline) {
				return (ServiceTimeline.GetParent().OutsourceServiceTypeID() == 1)
			}
		},
		Timelines: {
			Rules: {
				CateringValid: function (Value, Rule, CtlError) {
					var ProductionOutsourceServiceTimeline = CtlError.Object;
					var ProductionOutsourceService = ProductionOutsourceServiceTimeline.GetParent();
					var ROOutsourceServiceType = ClientData.ROOutsourceServiceTypeList.Find("OutsourceServiceTypeID", ProductionOutsourceService.OutsourceServiceTypeID());
					if (ROOutsourceServiceType) {
						if (ROOutsourceServiceType.OutsourceServiceTypeID == 1 && !ProductionOutsourceServiceTimeline.CateringBreakfastInd() && !ProductionOutsourceServiceTimeline.CateringLunchInd() && !ProductionOutsourceServiceTimeline.CateringDinnerInd()) {
							CtlError.AddError("Please select what meals are being catered for")
						}
					}
				},
				StartEndDatesValid: function (Value, Rule, CtlError) {
					var ProductionOutsourceServiceTimeline = CtlError.Object;
					var ProductionOutsourceService = ProductionOutsourceServiceTimeline.GetParent();
					var ROOutsourceServiceType = ClientData.ROOutsourceServiceTypeList.Find("OutsourceServiceTypeID", ProductionOutsourceService.OutsourceServiceTypeID());
					if (!ProductionOutsourceService.NotRequiredInd() && ProductionOutsourceServiceTimeline.ServiceStartDateTime() && ProductionOutsourceServiceTimeline.ServiceEndDateTime()) {
						var sd = new Date(ProductionOutsourceServiceTimeline.ServiceStartDateTime());
						var ed = new Date(ProductionOutsourceServiceTimeline.ServiceEndDateTime());
						if (ed < sd) {
							CtlError.AddError("Start date and time must be before end date and time")
						}
					}
				}
			}
		}
	},
	SelectedPHR: function () {
		if (ViewModel.CurrentProduction() != null) {
			return ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHumanResourceList().Find('Guid', ViewModel.SelectedPHRGuid())
		}
	},
	Vehicles: {
		Methods: {
			SetupSelectVehicle: function () {
				Singular.ShowLoadingBar()
				ViewModel.CallServerMethod('GetROVehicleList', {}, function (args) {
					if (args.Success) {
						KOFormatter.Deserialise(args.Data, ViewModel.ROVehicleSelectList)
						ViewModel.ROVehicleSelectList().Iterate(function (ROVehicle, Index) {
							ViewModel.CurrentProduction().ProductionVehicleList().Iterate(function (ProductionVehicle, Index) {
								if (ROVehicle.VehicleID() == ProductionVehicle.VehicleID()) {
									ROVehicle.OnCurrentProduction(true);
									ROVehicle.Visible(false);
								} else {
									ROVehicle.OnCurrentProduction(false);
									ROVehicle.Visible(true);
								};
							});
						});
						ViewModel.SelectVehicleVisible(true);
						ViewModel.VehicleListVisible(false);
						Singular.HideLoadingBar()
					}
				})
			},
			BackToProductionVehicleList: function () {
				ViewModel.SelectVehicleVisible(false);
				ViewModel.VehicleListVisible(true);
			},
			FilterROVehicleList: function () {
				Singular.ShowLoadingBar();
				if (!ViewModel.ROVehicleFilter() || ViewModel.ROVehicleFilter() == '') {
					ViewModel.ROVehicleSelectList().Iterate(function (ROVehicle, Index) {
						if (ROVehicle.OnCurrentProduction()) {
							ROVehicle.Visible(false);
						} else {
							ROVehicle.Visible(true);
						}
					});
				} else {
					ViewModel.ROVehicleSelectList().Iterate(function (ROVehicle, Index) {
						var FilterString = ViewModel.ROVehicleFilter().toLowerCase();
						var VehicleNameMatchInd = (ROVehicle.VehicleName().indexOf(FilterString) >= 0 ? true : false);
						var VehicleTypeMatchInd = (ROVehicle.VehicleType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
						if (VehicleNameMatchInd || VehicleTypeMatchInd && !ROVehicle.OnCurrentProduction()) {
							ROVehicle.Visible(true);
						} else {
							ROVehicle.Visible(false);
						}
					});
				};
				Singular.HideLoadingBar();
			},
			RemoveVehicle: function (ProductionVehicle) {
				Singular.SendCommand('RemoveVehicle', { VehicleID: ProductionVehicle.VehicleID() }, function (d) { })
			},
			AddVehicle: function (ROVehicle) {
				Singular.SendCommand('AddVehicle', { VehicleID: ROVehicle.VehicleID() }, function (d) {
					SetProductionVehiclesTabVisible()
					ViewModel.SelectVehicleVisible(false)
					ViewModel.VehicleListVisible(true)
					//ViewModel.ROVehicleList().Clear()
				})
			},
			GetVehicleDropDown: function (List, Item) {
				var VehicleList = [];
				var CurrentVehicleIDs = [];
				ViewModel.CurrentProduction().ProductionVehicleList().Iterate(function (ProductionVehicle, Index) {
					CurrentVehicleIDs.push(ProductionVehicle.VehicleID())
				});
				ProductionHelper.CurrentArea().ProductionTimelineList().Iterate(function (ProductionTimeline, Index) {
					if (ProductionTimeline.VehicleID() && CurrentVehicleIDs.indexOf(ProductionTimeline.VehicleID()) < 0) {
						CurrentVehicleIDs.push(ProductionTimeline.VehicleID())
					}
				});
				CurrentVehicleIDs.Iterate(function (VehicleID, Index) {
					var ROV = ClientData.ROVehicleList.Find('VehicleID', VehicleID);
					VehicleList.push(ROV);
				});
				return VehicleList;
			}
		}
	},
	ProductionSystemAreas: {
		GetAllowedTimelineTypes: function (ROProductionAreaAllowedTimelineTypeList, ProductionAreaID, SystemID, ProductionTimeline) {
			var Allowed = [];
			if (ProductionTimeline.SharedTimeline()) {
				var Timeline = ClientData.ROProductionTimelineTypeList.Find('ProductionTimelineTypeID', ProductionTimeline.ProductionTimelineTypeID());
				Allowed.push(Timeline);
			} else {
				ROProductionAreaAllowedTimelineTypeList.Iterate(function (Item, Index) {
					if (Item.ProductionAreaID == ProductionAreaID && Item.SystemID == SystemID) {
						Allowed.push({ ProductionTimelineTypeID: Item.ProductionTimelineTypeID, ProductionTimelineType: Item.ProductionTimelineType });
					};
				});
			}
			return Allowed;
		},
		AddNewTimeline: function (ProductionSystemArea) {
			Singular.SendCommand('AddNewTimeline', {}, function (d) { });
		},
		RemoveTimeline: function (ProductionSystemArea, ProductionTimeline) {
			Singular.SendCommand('RemoveTimeline', { Guid: ProductionTimeline.Guid() }, function (d) { });
		},
		UI: {
			ProductionAreaStatusCss: function (ProductionSystemArea) {
				switch (ProductionSystemArea.ProductionAreaStatusID()) {
					case 1:
						return 'btn btn-md btn-primary';
						break;
					case 2:
						return 'btn btn-md btn-success';
						break;
					case 3:
						return 'btn btn-md btn-success';
						break;
					case 4:
						return 'btn btn-md btn-warning';
						break;
					case 5:
						return 'btn btn-md btn-danger';
						break;
					default:
						return 'btn btn-md btn-default';
						break
				};
			},
			ProductionAreaStatusHtml: function (ProductionSystemArea) {
				return ProductionSystemArea.ProductionAreaStatus();
			},
			ProductionAreaStatusClick: function (ProductionSystemArea) {

			}
		},
		Events: {
			ProductionSpecRequirementIDSet: function (ProductionSystemArea) {
				ProductionSystemArea.GetParent().ProductionSpecRequirementID(ProductionSystemArea.ProductionSpecRequirementID());
				ProductionSystemArea.GetParent().NotComplySpecReasons(ProductionSystemArea.NotComplySpecReasons());
				if (ProductionSystemArea.ProductionSpecRequirementID()) {
					ProductionHelper.ProductionSystemAreas.Methods.PopulateProductionSpec(ProductionSystemArea.ProductionSpecRequirementID())
				} else {
					ProductionHelper.ProductionSystemAreas.Methods.ClearProductionSpec()
				}
			},
			ProductionStatusIDChanged: function (ProductionSystemArea) {
				//set the status set date field
				//set the status set by field
				if (ProductionSystemArea.ProductionAreaStatusID()) {
					ProductionSystemArea.StatusDate(new Date());
					ProductionSystemArea.StatusSetByUserID(ProductionSystemArea.CurrentUserID());
					ProductionSystemArea.StatusSetByName(ProductionSystemArea.CurrentUserName());
				} else {
					ProductionSystemArea.StatusDate(null);
					ProductionSystemArea.StatusSetByUserID(null);
					ProductionSystemArea.StatusSetByName("");
				}
				//update field
				//SPHelper.Timeline.UI.UpdateSelectedScheduleItem(ProductionSystemArea)
			},
			NotComplySpecReasonsSet: function (ProductionSystemArea) {
				ProductionSystemArea.GetParent().ProductionSpecRequirementID(ProductionSystemArea.ProductionSpecRequirementID());
				ProductionSystemArea.GetParent().NotComplySpecReasons(ProductionSystemArea.NotComplySpecReasons());
				ProductionHelper.ProductionSystemAreas.Methods.CheckSpecValid(ProductionSystemArea);
			}
		},
		Methods: {
			GetAllowedStatuses: function (List, Item) {
				var AllowedStatuses = [];
				List.Iterate(function (Status, Index) {
					if (Status.SystemID == Item.SystemID() && Status.ProductionAreaID == Item.ProductionAreaID()) {
						AllowedStatuses.push(Status)
					}
				})
				return AllowedStatuses.sort(function (a, b) { return a.OrderNo - b.OrderNo });
			},
			FilterProductionSpecRequirementList: function (List, SystemArea) {
				var AllowedSpecs = [];
				var Production = SystemArea.GetParent();
				if (SystemArea.SystemID() == 1) {
					List.Iterate(function (Spec, Index) {
						if (Spec.SystemID == SystemArea.SystemID()
                && Spec.ProductionAreaID == SystemArea.ProductionAreaID()
                && (Spec.ProductionTypeID == Production.ProductionTypeID() || !Production.ProductionTypeID())
                && (Spec.EventTypeID == Production.EventTypeID() || !Production.EventTypeID())) {
							AllowedSpecs.push(Spec);
						}
					})
				} else {
					List.Iterate(function (Spec, Index) {
						if (Spec.SystemID == SystemArea.SystemID()
                && Spec.ProductionAreaID == SystemArea.ProductionAreaID()) {
							AllowedSpecs.push(Spec);
						}
					})
				}

				return AllowedSpecs;
			},
			PopulateProductionSpec: function (SelectedSpecID) {
				Singular.SendCommand("PopulateProductionSpec", { ProductionSpecRequirementID: SelectedSpecID }, function (d) {
				})
			},
			ClearProductionSpec: function () { },
			CheckSpecValid: function (ProductionSystemArea) {
				if (ProductionSystemArea.ProductionSpecRequirementID()) {
					ProductionHelper.ProductionSystemAreas.Methods.CheckSpecValidServer()
				}
			},
			SpecRequirementValid: function (Value, Rule, CtlError) {
				var CurrentProduction = CtlError.Object;
				var SystemArea = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
				if (SystemArea.SystemID() == 1) {
					if (CurrentProduction.SpecNotCompliant() && CurrentProduction.NotComplySpecReasons().trim().length == 0) {
						CtlError.AddError("Reason required. Production spec does not match the template.")
					}
				}
			},
			CheckSpecValidServer: function () {
				var SystemArea = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
				if (SystemArea.SystemID() == 1) {
					Singular.SendCommand('CheckSpecValid', {}, function (d) { })
				}
			},
			UpdateOldStatuses: function (ProductionSystemArea) {
				if (ProductionSystemArea.SystemID() == 1) {
					//Production Services
					//set to new
					if (ProductionSystemArea.ProductionAreaStatusID() == 1) {
						if (ProductionSystemArea.GetParent().CrewFinalised()) {
							//undo CF
							Production.CrewFinalised(false);
							if (Production.CrewFinalised()) {
								Production.CrewPlanningFinalisedDate(new Date())
								Production.CrewPlanningFinalisedBy(ViewModel.CurrentUserID())
							} else {
								Production.CrewPlanningFinalisedDate(null)
								Production.CrewPlanningFinalisedBy(null)
							}
							//undo PF
							Production.PlanningFinalised(!Production.PlanningFinalised());
							if (Production.PlanningFinalised()) {
								Production.PlanningFinalisedDate(new Date())
								Production.PlanningFinalisedBy(ViewModel.CurrentUserID())
							} else {
								Production.PlanningFinalisedDate(null)
								Production.PlanningFinalisedBy(null)
							}
							//undo Rec
							Production.PlanningFinalised(!Production.PlanningFinalised());
							if (Production.PlanningFinalised()) {
								Production.PlanningFinalisedDate(new Date())
								Production.PlanningFinalisedBy(ViewModel.CurrentUserID())
							} else {
								Production.PlanningFinalisedDate(null)
								Production.PlanningFinalisedBy(null)
							}
							//undo Cancelled
							Production.PlanningFinalised(!Production.PlanningFinalised());
							if (Production.PlanningFinalised()) {
								Production.PlanningFinalisedDate(new Date())
								Production.PlanningFinalisedBy(ViewModel.CurrentUserID())
							} else {
								Production.PlanningFinalisedDate(null)
								Production.PlanningFinalisedBy(null)
							}
						}

					}
					//set to crew finalised
					if (ProductionSystemArea.ProductionAreaStatusID() == 1) {

					}
					//set to planning finalised
					if (ProductionSystemArea.ProductionAreaStatusID() == 1) {

					}
					//set to reconciled
					if (ProductionSystemArea.ProductionAreaStatusID() == 1) {

					}
					//set to cancelled
					if (ProductionSystemArea.ProductionAreaStatusID() == 1) {

					}
				} else {
					//Production Content
				}
			},
			SetStatusFromOldStatusIndicators: function (StatusID) {
				ProductionHelper.CurrentArea().ProductionAreaStatusID(StatusID);
			}
		}
	},
	ProductionTimelines: {
		Events: {
			ProductionTimelineTypeIDSet: function (ProductionTimeline) {
				ProductionHelper.ProductionTimelines.Methods.SetProductionTimelineType(ProductionTimeline);
				//set defaults
				ProductionHelper.ProductionTimelines.Methods.SetDefaultTimes(ProductionTimeline);
				//check the rules
				if (ProductionTimeline.StartDateTime() && ProductionTimeline.EndDateTime() && ProductionTimeline.TimelineDate() && ProductionTimeline.ProductionTimelineTypeID()) {
					var AllDatesValid = ProductionHelper.ProductionTimelines.Rules.AllDatesValid(ProductionTimeline);
					if (AllDatesValid) {
						ProductionHelper.ProductionTimelines.Rules.CheckAll(ProductionTimeline);
					}
				}
			},
			TimelineDateSet: function (ProductionTimeline) {
				if (ProductionTimeline.IsNew() && ProductionTimeline.TimelineDate() && !ProductionTimeline.StartDateTime() && !ProductionTimeline.EndDateTime()) {
					//do nothing if new, has timeline date, but does not hace start or end
				} else if (ProductionTimeline.TimelineDate() && ProductionTimeline.StartDateTime() && ProductionTimeline.EndDateTime()) {
					//otherwise if it has a timeline date, start and end, make sure the days are the same for OB's
					var AllDatesValid = ProductionHelper.ProductionTimelines.Rules.AllDatesValid(ProductionTimeline);
					if (AllDatesValid) {
						//make sure on same day
						ProductionTimeline.SuspendChanges = true;
						var t = new Date(ProductionTimeline.TimelineDate());
						var s = new Date(ProductionTimeline.StartDateTime());
						var e = new Date(ProductionTimeline.EndDateTime());
						var SDSame = moment(s).isSame(t, 'day');
						var EDSame = moment(e).isSame(t, 'day');
						if (!SDSame) {
							var sdh = moment(s).hours();
							var sdm = moment(s).minutes();
							var newsd = t;
							newsd.setHours(sdh);
							newsd.setMinutes(sdm);
							ProductionTimeline.StartDateTime(newsd);
						}
						if (!EDSame) {
							var edh = moment(e).hours();
							var edm = moment(e).minutes();
							var newed = t;
							newed.setHours(edh);
							newed.setMinutes(edm);
							ProductionTimeline.EndDateTime(newed);
						}
						ProductionTimeline.SuspendChanges = false;
					}
				} else {
					//don't have everything
				}
				//check the rules
				if (ProductionTimeline.StartDateTime() && ProductionTimeline.EndDateTime() && ProductionTimeline.TimelineDate() && ProductionTimeline.ProductionTimelineTypeID()) {
					var AllDatesValid = ProductionHelper.ProductionTimelines.Rules.AllDatesValid(ProductionTimeline);
					if (AllDatesValid) {
						ProductionHelper.ProductionTimelines.Rules.CheckAll(ProductionTimeline);
					}
				}
			},
			StartTimeSet: function (ProductionTimeline) {
				ProductionTimeline.SuspendChanges = true;
				var t = new Date(ProductionTimeline.TimelineDate());
				var s = new Date(ProductionTimeline.StartDateTime());
				var SDSame = moment(s).isSame(t, 'day');
				if (!SDSame) {
					var sdh = moment(s).hours();
					var sdm = moment(s).minutes();
					var newsd = t;
					newsd.setHours(sdh);
					newsd.setMinutes(sdm);
					ProductionTimeline.StartDateTime(newsd);
				}
				ProductionTimeline.SuspendChanges = false;
				//check the rules
				var AllDatesValid = false
				if (ProductionTimeline.StartDateTime() && ProductionTimeline.EndDateTime() && ProductionTimeline.TimelineDate() && ProductionTimeline.ProductionTimelineTypeID()) {
					AllDatesValid = ProductionHelper.ProductionTimelines.Rules.AllDatesValid(ProductionTimeline);
				}
				ProductionHelper.Production.Methods.SetDefaultProductionDescription(ProductionTimeline.GetParent().GetParent())

				Singular.SendCommand('UpdateScheduleTimeFromTimelines', {
					ProductionTimelineGuiD: ProductionTimeline.Guid(),
					StartTimeInd: true
				}, function (d) {
					Singular.HideLoadingBar()
					if (AllDatesValid) {
						ProductionHelper.ProductionTimelines.Rules.CheckAll(ProductionTimeline);
					}
				});
			},
			EndTimeSet: function (ProductionTimeline) {
				ProductionTimeline.SuspendChanges = true;
				var t = new Date(ProductionTimeline.TimelineDate());
				var e = new Date(ProductionTimeline.EndDateTime());
				var EDSame = moment(e).isSame(t, 'day');
				if (!EDSame) {
					var edh = moment(e).hours();
					var edm = moment(e).minutes();
					var newed = t;
					newed.setHours(edh);
					newed.setMinutes(edm);
					ProductionTimeline.EndDateTime(newed);
				}
				ProductionTimeline.SuspendChanges = false;
				//check the rules
				var AllDatesValid = false
				if (ProductionTimeline.StartDateTime() && ProductionTimeline.EndDateTime() && ProductionTimeline.TimelineDate() && ProductionTimeline.ProductionTimelineTypeID()) {
					AllDatesValid = ProductionHelper.ProductionTimelines.Rules.AllDatesValid(ProductionTimeline);
				}
				ProductionHelper.Production.Methods.SetDefaultProductionDescription(ProductionTimeline.GetParent().GetParent())

				Singular.SendCommand('UpdateScheduleTimeFromTimelines', {
					ProductionTimelineGuiD: ProductionTimeline.Guid(),
					StartTimeInd: false
				}, function (d) {
					Singular.HideLoadingBar()
					if (AllDatesValid) {
						ProductionHelper.ProductionTimelines.Rules.CheckAll(ProductionTimeline);
					}
				})
			},
			VehicleIDSet: function (ProductionTimeline) {
				if (ProductionTimeline.StartDateTime() && ProductionTimeline.EndDateTime() && ProductionTimeline.TimelineDate() && ProductionTimeline.ProductionTimelineTypeID()) {
					var AllDatesValid = ProductionHelper.ProductionTimelines.Rules.AllDatesValid(ProductionTimeline);
					if (AllDatesValid) {
						ProductionHelper.ProductionTimelines.Rules.CheckAll(ProductionTimeline);
					}
				}
			}
		},
		Rules: {
			AllDatesValid: function (ProductionTimeline) {
				var t = new Date(ProductionTimeline.TimelineDate());
				var s = new Date(ProductionTimeline.StartDateTime());
				var e = new Date(ProductionTimeline.EndDateTime());
				var tdv = moment(t).isValid();
				var sdv = moment(s).isValid();
				var edv = moment(e).isValid();
				return (tdv && sdv && edv)
			},
			CheckTimelineValid: function (ProductionTimeline, Callback) {
				var TimelineTypeSet = (ProductionTimeline.ProductionTimelineTypeID() ? true : false)
				var StartTimeSet = (ProductionTimeline.StartDateTime() ? true : false)
				var EndTimeSet = (ProductionTimeline.EndDateTime() ? true : false)
				if (TimelineTypeSet && StartTimeSet && EndTimeSet) {
					Singular.SendCommand('CheckTimelines', {}, function (d) {
						Callback
					});
					//          var SameDay = OBMisc.Dates.SameDay(ProductionTimeline.StartDateTime(), ProductionTimeline.EndDateTime())
					//          if (SameDay) {
					//            ProductionTimeline.TimelineClashes("")
					//            var obj = new ProductionSystemAreaRules_ProductionTimelineClientObject();
					//            obj.ProductionTimeline(self);
					//            ViewModel.CallServerMethod('CheckTimelineValid', { ProductionTimelineClient: KOFormatter.Serialise(obj) }, function (WebResult) {
					//              if (WebResult.Data == '') {
					//                ProductionTimeline.TimelineClashes('')
					//              } else {
					//                ProductionTimeline.TimelineClashes(WebResult.Data)
					//              };
					//              Callback
					//            })
					//          }
				}
			},
			CheckVehicleValid: function (ProductionTimeline, Callback) {
				if (ProductionTimeline.IsValid) {
					var ExcludedTimelineTypes = [2, 3, 5, 7, 8, 12, 13, 20, 22, 25, 26, 28, 29, 32, 33, 34, 35, 36, 37, 41, 42, 43, 46, 47, 48, 49];
					var Tx = [11];
					if (ExcludedTimelineTypes.indexOf(ProductionTimeline.ProductionTimelineTypeID()) < 0 && Tx.indexOf(ProductionTimeline.ProductionTimelineTypeID()) >= 0) {
						Singular.SendCommand('CheckVehiclesValid', {}, function (d) {
							Callback
						})
					}
				}
			},
			CheckScheduleValid: function (Callback) {
				Singular.SendCommand('CheckSchedules', {}, function (response) {
					Callback
				});
			},
			CheckAll: function (ProductionTimeline, Callback) {
				//Singular.ShowLoadingBar();
				Singular.SendCommand('CheckAll', {}, function (response) {
					var r = response;
					var Area = ProductionTimeline.GetParent();
					Area.ProductionTimelineList().Iterate(function (Item, Index) {
						Singular.Validation.CheckRules(Item);
					});
					Singular.HideLoadingBar();
				});
				//        ProductionHelper.ProductionTimelines.Rules.CheckTimelineValid(ProductionTimeline, function () {
				//          ProductionHelper.ProductionTimelines.Rules.CheckScheduleValid(ProductionTimeline, function () {
				//            ProductionHelper.ProductionTimelines.Rules.CheckVehicleValid(ProductionTimeline, function () {

				//            });
				//          });
				//        });
			}
		},
		Methods: {
			GetTimeline: function (TimelineGuid) {
				return ProductionHelper.CurrentArea().ProductionTimelineList().Find('Guid', TimelineGuid)
			},
			AddNewTimeline: function () {
				//      Dim pt As ProductionTimeline = Me.ProductionTimelineList.AddNew()
				//      pt.ProductionID = Me.ProductionID
				//      pt.ProductionSystemAreaID = ProductionSystemAreaID
				//      pt.StartDateTime = Me.GetParent.PlayStartDateTime
				//      pt.EndDateTime = Me.GetParent.PlayEndDateTime
				Singular.SendCommand('AddNewTimeline', {}, function (d) { });
			},
			RemoveTimeline: function (ProductionTimeline) {
				Singular.SendCommand('RemoveTimeline', { Guid: ProductionTimeline.Guid() }, function (d) { });
			},
			SetupStartDateTimeAndEndDateTime: function (ProductionTimeline) {
				ProductionTimeline.StartDateTime(new Date(ProductionTimeline.TimelineDate()));
				ProductionTimeline.EndDateTime(new Date(ProductionTimeline.TimelineDate()));
			},
			CheckDatesAfterSet: function (ProductionTimeline) {
				ProductionTimeline.SuspendChanges = true;
				var t = new Date(ProductionTimeline.TimelineDate());
				var s = new Date(ProductionTimeline.StartDateTime());
				var e = new Date(ProductionTimeline.EndDateTime());
				var SDSame = moment(s).isSame(t, 'day');
				var EDSame = moment(e).isSame(t, 'day');
				//if the start date is not the same as timeline date
				//this should never be allowed
				if (!SDSame) {
					var sdh = moment(s).hours();
					var sdm = moment(s).minutes();
					var newsd = t;
					newsd.setHours(sdh);
					newsd.setMinutes(sdm);
					ProductionTimeline.StartDateTime(newsd);
				}
				//if the end date is the same as timeline date
				if (EDSame) {
					//StudioProductionTimeline.OvernightInd(false);
				} else {
					//end date is not on the same day as timeline date
					s = new Date(StudioProductionTimeline.StartDateTime());
					if (moment(e).isAfter(s, 'minute')) {
						StudioProductionTimeline.OvernightInd(true);
					}
				}
				StudioProductionTimeline.SuspendChanges = false;
			},
			SetProductionTimelineType: function (ProductionTimeline) {
				ClientData.ROProductionAreaAllowedTimelineTypeList.Iterate(function (Item, Index) {
					if (Item.ProductionTimelineTypeID == ProductionTimeline.ProductionTimelineTypeID()) {
						ProductionTimeline.ProductionTimelineType(Item.ProductionTimelineType);
					};
				});
			},
			SetDefaultTimes: function (ProductionTimeline) {
				var Area = ProductionTimeline.GetParent();
				var Production = Area.GetParent();
				if (Area.SystemID() == 2 && ProductionTimeline.ProductionTimelineTypeID()) {
					if (ProductionTimeline.ProductionTimelineTypeID()) {
						var DefaultIDs = [47, 48, 50];
						if (DefaultIDs.indexOf(ProductionTimeline.ProductionTimelineTypeID()) >= 0) {
							ProductionTimeline.TimelineDate(new Date(Production.PlayStartDateTime()));
						}
					}
				}
			}
		}
	},
	ProductionHumanResources: {
		Rules: {
			CanAddProductionHumanResource: function () {
				if (ViewModel.CurrentProduction().ProductionTypeID() && ViewModel.CurrentProduction().ProductionVenueID() && ViewModel.CurrentProduction().CanEditHumanResourcesAfterFinalised()) {
					return true
				} else {
					return false
				}
			},
			PositionRequired: function (phr) {
				var Discipline = ClientData.RODisciplineList.Find("DisciplineID", phr.DisciplineID());
				if (Discipline) {
					if (Discipline.PositionRequiredInd) {
						if (!phr.PositionID()) {
							return true;
						}
					}
				}
				return false;
			},
			HumanResourceRequired: function (CtlError) {
				var phr = CtlError.Object;
				if (phr.PositionID()) {
					var Position = ClientData.ROPositionList.Find("PositionID", phr.PositionID());
					if (Position.UnMannedInd && phr.HumanResourceID()) {
						CtlError.AddError("A human resource is not required for this position");
					}
					else if (!Position.UnMannedInd && !phr.HumanResourceID()) {
						if (phr.GetParent().GetParent().PlanningFinalised()) {
							CtlError.AddError("A human resource is required for this position")
						} else {
							CtlError.AddWarning("A human resource is required for this position")
						}
					}
				}
				if (phr.HumanResourceID()) {
					var ph = phr.GetParent().HRProductionScheduleList().Find("HumanResourceID", phr.HumanResourceID());
					if (!ph) {
						CtlError.AddError(phr.HumanResource() + ' does not have a schedule')
					}
				}
			}
		},
		Events: {
			CrewLeaderClick: function (ProductionHumanResource) {
				ProductionHumanResource.CrewLeaderInd(!ProductionHumanResource.CrewLeaderInd());
			},
			ReliefCrewClick: function (ProductionHumanResource) {
				ProductionHumanResource.ReliefCrewInd(!ProductionHumanResource.ReliefCrewInd());
			},
			TBCClick: function (ProductionHumanResource) {
				ProductionHumanResource.TBCInd(!ProductionHumanResource.TBCInd());
			},
			TrainingClick: function (ProductionHumanResource) {
				ProductionHumanResource.TrainingInd(!ProductionHumanResource.TrainingInd());
			},
			SuppliedHumanResourceClick: function (ProductionHumanResource) {
				ProductionHumanResource.SuppliedHumanResourceInd(!ProductionHumanResource.SuppliedHumanResourceInd());
			},
			FilteredProductionTypeClick: function (ProductionHumanResource) {
				ProductionHumanResource.FilteredProductionTypeInd(!ProductionHumanResource.FilteredProductionTypeInd());
			},
			FilteredPositionClick: function (ProductionHumanResource) {
				ProductionHumanResource.FilteredPositionInd(!ProductionHumanResource.FilteredPositionInd());
			},
			IncludedOffClick: function (ProductionHumanResource) {
				ProductionHumanResource.IncludedOffInd(!ProductionHumanResource.IncludedOffInd());
			}
		},
		Methods: {
			GetSortedList: function () {
				return ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHumanResourceList().sort(function (phr1, phr2) {
					//          var p1 = phr1.OrderNo().toString() + '.' + phr1.Priority().toString();
					//          var p2 = phr2.OrderNo().toString() + '.' + phr2.Priority().toString();
					//          var phr1SO = parseFloat(p1).toFixed(2);
					//          var phr2SO = parseFloat(p2).toFixed(2)
					//          return (phr1SO - phr2SO)
				})
			},
			SortList: function (ProductionSystemArea) {
				var so = firstBy(function (phr1, phr2) {
					return phr1.OrderNo() - phr2.OrderNo();
				}).thenBy(function (phr1, phr2) {
					return phr1.Priority() - phr2.Priority();
				}).thenBy(function (phr1, phr2) {
					return phr1.HumanResource() - phr2.HumanResource();
				});
				console.log(so);
				ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHumanResourceList.sort(so);
			},
			SetSelectedPHR: function (ProductionHumanResource) {
				ViewModel.SelectedPHRGuid(ProductionHumanResource.Guid());
				GetQualifiedHR(ProductionHelper.SelectedPHR());
				//ViewModel.PHRListVisible(false);
				//ViewModel.SelectedHRVisible(true);
			},
			BackToPHRList: function () {
				ViewModel.SelectedPHRID(null);
				ViewModel.PHRListVisible(true);
				ViewModel.SelectedHRVisible(false);
				//ViewModel.ROSkilledHRList().Clear()
			},
			GetQualifiedHR: function (ProductionHumanResource) {

				window.PHR = null;
				window.PHR = ProductionHumanResource;
				ViewModel.ROSkilledHRListCriteria().FilterName('');
				$('#HRSearch').modal();
				//var DistinctHRIDS = ViewModel.CurrentProductionSystemArea().ProductionHumanResourceList().Distinct('HumanResourceID', true);
				ProductionHumanResource.FetchingQualifiedHR(true);

				//ExcludeHRIDs: DistinctHRIDS,
				ViewModel.ROSkilledHRListCriteria().ProductionID(ViewModel.CurrentProduction().ProductionID());
				ViewModel.ROSkilledHRListCriteria().SystemID(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].SystemID());
				ViewModel.ROSkilledHRListCriteria().ProductionAreaID(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionAreaID());
				ViewModel.ROSkilledHRListCriteria().DisciplineID(ProductionHumanResource.DisciplineID());
				ViewModel.ROSkilledHRListCriteria().FilteredProductionTypeInd(ProductionHumanResource.FilteredProductionTypeInd());
				ViewModel.ROSkilledHRListCriteria().ProductionTypeID(ViewModel.CurrentProduction().ProductionTypeID());
				ViewModel.ROSkilledHRListCriteria().FilteredPositionInd(ProductionHumanResource.FilteredPositionInd());
				ViewModel.ROSkilledHRListCriteria().PositionID(ProductionHumanResource.PositionID());
				ViewModel.ROSkilledHRListCriteria().IncludedOffInd(ProductionHumanResource.IncludedOffInd());
				ViewModel.ROSkilledHRListCriteria().TxStartDate(ViewModel.CurrentProduction().PlayStartDateTime());
				ViewModel.ROSkilledHRListCriteria().TxEndDate(ViewModel.CurrentProduction().PlayEndDateTime());
				ViewModel.ROSkilledHRListCriteria().PositionLevelID(null);
				ViewModel.ROSkilledHRListCriteria().VehicleID(ProductionHumanResource.VehicleID());
				ViewModel.ROSkilledHRListCriteria().RoomID(ProductionHumanResource.RoomID());
				ViewModel.ROSkilledHRListPagingInfo().Refresh();
				//ProductionHumanResource.ROSkilledHRList(ViewModel.ROSkilledHRList());
				ProductionHumanResource.FetchingQualifiedHR(false);

				//        Singular.GetDataStateless('OBLib.Productions.Crew.ReadOnly.ROSkilledHRList, OBLib', {
				//          //ExcludeHRIDs: DistinctHRIDS,
				//          ProductionID: ViewModel.CurrentProduction().ProductionID(),
				//          SystemID: ViewModel.CurrentProduction().ProductionSystemAreaList()[0].SystemID(),
				//          ProductionAreaID: ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionAreaID(),
				//          DisciplineID: ProductionHumanResource.DisciplineID(),
				//          FilteredProductionTypeInd: ProductionHumanResource.FilteredProductionTypeInd(),
				//          ProductionTypeID: ViewModel.CurrentProduction().ProductionTypeID(),
				//          FilteredPositionInd: ProductionHumanResource.FilteredPositionInd(),
				//          PositionID: ProductionHumanResource.PositionID(),
				//          IncludedOffInd: ProductionHumanResource.IncludedOffInd(),
				//          TxStartDate: ViewModel.CurrentProduction().PlayStartDateTime(),
				//          TxEndDate: ViewModel.CurrentProduction().PlayEndDateTime(),
				//          PositionLevelID: null,
				//          VehicleID: ProductionHumanResource.VehicleID(),
				//          RoomID: ProductionHumanResource.RoomID()
				//        }, function (args) {
				//          if (args.Success) {
				//            KOFormatter.Deserialise(args.Data, ProductionHumanResource.ROSkilledHRList);
				//            ProductionHumanResource.FetchingQualifiedHR(false);
				//            ProductionHelper.ProductionHumanResources.Methods.FilterQualifiedHR(ProductionHumanResource);
				//            //if (ProductionHelper.CurrentArea().SystemID() == 2) {
				//            $('#HRSearch').modal();
				//            //}
				//          }
				//        });

			},
			FilterQualifiedHR: function (ProductionHumanResource) {
				ViewModel.ROSkilledHRListPagingInfo().Refresh();
				//Singular.ShowLoadingBar();
				//if (!ProductionHumanResource.SearchResultsFilter() || ProductionHumanResource.SearchResultsFilter() == '') {
				//  ProductionHumanResource.ROSkilledHRList().Iterate(function (ROSkilledHR, Index) {
				//    ROSkilledHR.VisibleInd(true);
				//  });
				//} else {
				//  ProductionHumanResource.ROSkilledHRList().Iterate(function (ROSkilledHR, Index) {
				//    var FilterString = ProductionHumanResource.SearchResultsFilter().toLowerCase();
				//    var HRMatchInd = (ROSkilledHR.HumanResource().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
				//    var CTMatchInd = (ROSkilledHR.ContractType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
				//    var CityMatchInd = (ROSkilledHR.CityCode().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
				//    var PTMatchCount = 0;
				//    var PosMatchCount = 0;
				//    var LevelMatchCount = 0;
				//    if (ROSkilledHR.ROSkilledHRPositionList().length > 0) {
				//      ROSkilledHR.ROSkilledHRPositionList().Iterate(function (ROHRPosition, Index) {
				//        PTMatchCount = (ROHRPosition.ProductionType().toLowerCase().indexOf(FilterString) >= 0 ? PTMatchCount += 1 : PTMatchCount += 0);
				//        PosMatchCount = (ROHRPosition.Position().toLowerCase().indexOf(FilterString) >= 0 ? PosMatchCount += 1 : PosMatchCount += 0);
				//        LevelMatchCount = (ROHRPosition.PositionLevel().toString().toLowerCase().indexOf(FilterString) >= 0 ? LevelMatchCount += 1 : LevelMatchCount += 0);
				//      })
				//    }
				//    if (HRMatchInd || CTMatchInd || CityMatchInd || PTMatchCount > 0 || PosMatchCount > 0 || LevelMatchCount > 0) {
				//      ROSkilledHR.VisibleInd(true);
				//    } else {
				//      ROSkilledHR.VisibleInd(false);
				//    }
				//  });
				//};
				//Singular.HideLoadingBar();
			},
			SetPrimaryHR: function (ROSkilledHR) {
				var ProductionHumanResource = ProductionHelper.CurrentArea().ProductionHumanResourceList().Filter('Guid', window.PHR.Guid())[0];
				//        var ProductionHumanResource = ROSkilledHR.GetParent();
				//        var Copy = {};
				//        Copy.PositionTypeID = ProductionHumanResource.PositionTypeID();
				//        Copy.PreferredHumanResourceID = ProductionHumanResource.PreferredHumanResourceID();
				//        Copy.SuppliedHumanResourceInd = ProductionHumanResource.SuppliedHumanResourceInd();
				//        Copy.SelectionReason = ProductionHumanResource.SelectionReason();
				//        Copy.CancelledDate = ProductionHumanResource.CancelledDate();
				//        Copy.CancelledReason = ProductionHumanResource.CancelledReason();
				//        Copy.CrewLeaderInd = ProductionHumanResource.CrewLeaderInd();
				//        Copy.TrainingInd = ProductionHumanResource.TrainingInd();
				//        Copy.TBCInd = ProductionHumanResource.TBCInd();
				//        Copy.VehicleID = ProductionHumanResource.VehicleID();
				//        Copy.Comments = ProductionHumanResource.Comments();
				//        Copy.ReliefCrewInd = ProductionHumanResource.ReliefCrewInd();
				//        Copy.RoomID = ProductionHumanResource.RoomID();
				//        Copy.PrefHumanResource = ProductionHumanResource.PrefHumanResource();
				var phrl = ProductionHelper.CurrentArea().ProductionHumanResourceList().Filter('HumanResourceID', ROSkilledHR.HumanResourceID());
				if (phrl.length > 0) {
					Singular.SendCommand("SetHumanResourceID", {
						Guid: ProductionHumanResource.Guid(),
						OldHumanResourceID: ProductionHumanResource.HumanResourceID(),
						NewHumanResourceID: ROSkilledHR.HumanResourceID(),
						NewHumanResource: ROSkilledHR.HumanResource(),
						DisciplineID: ProductionHumanResource.DisciplineID(),
						PositionID: ProductionHumanResource.PositionID()
					}, function (response) {
						//            ProductionHumanResource.PositionTypeID(Copy.PositionTypeID);
						//            ProductionHumanResource.PreferredHumanResourceID(Copy.PreferredHumanResourceID);
						//            ProductionHumanResource.SuppliedHumanResourceInd(Copy.SuppliedHumanResourceInd);
						//            ProductionHumanResource.SelectionReason(Copy.SelectionReason);
						//            ProductionHumanResource.CancelledDate(Copy.CancelledDate);
						//            ProductionHumanResource.CancelledReason(Copy.CancelledReason);
						//            ProductionHumanResource.CrewLeaderInd(Copy.CrewLeaderInd);
						//            ProductionHumanResource.TrainingInd(Copy.TrainingInd);
						//            ProductionHumanResource.TBCInd(Copy.TBCInd);
						//            ProductionHumanResource.VehicleID(Copy.VehicleID);
						//            ProductionHumanResource.Comments(Copy.Comments);
						//            ProductionHumanResource.ReliefCrewInd(Copy.ReliefCrewInd);
						//            ProductionHumanResource.RoomID(Copy.RoomID);
						//            ProductionHumanResource.PrefHumanResource(Copy.PrefHumanResource);            
						$('#HRSearch').modal('hide');
					});
				} else {
					Singular.SendCommand("SetHumanResourceID", {
						Guid: ProductionHumanResource.Guid(),
						OldHumanResourceID: ProductionHumanResource.HumanResourceID(),
						NewHumanResourceID: ROSkilledHR.HumanResourceID(),
						NewHumanResource: ROSkilledHR.HumanResource(),
						DisciplineID: ProductionHumanResource.DisciplineID(),
						PositionID: ProductionHumanResource.PositionID()
					}, function (response) {
						//            ProductionHumanResource.PositionTypeID(Copy.PositionTypeID);
						//            ProductionHumanResource.PreferredHumanResourceID(Copy.PreferredHumanResourceID);
						//            ProductionHumanResource.SuppliedHumanResourceInd(Copy.SuppliedHumanResourceInd);
						//            ProductionHumanResource.SelectionReason(Copy.SelectionReason);
						//            ProductionHumanResource.CancelledDate(Copy.CancelledDate);
						//            ProductionHumanResource.CancelledReason(Copy.CancelledReason);
						//            ProductionHumanResource.CrewLeaderInd(Copy.CrewLeaderInd);
						//            ProductionHumanResource.TrainingInd(Copy.TrainingInd);
						//            ProductionHumanResource.TBCInd(Copy.TBCInd);
						//            ProductionHumanResource.VehicleID(Copy.VehicleID);
						//            ProductionHumanResource.Comments(Copy.Comments);
						//            ProductionHumanResource.ReliefCrewInd(Copy.ReliefCrewInd);
						//            ProductionHumanResource.RoomID(Copy.RoomID);
						//            ProductionHumanResource.PrefHumanResource(Copy.PrefHumanResource);
						$('#HRSearch').modal('hide');
					});
				}
			},
			SetPreferredHR: function (ROSkilledHR) {
				var ProductionHumanResource = ProductionHelper.CurrentArea().ProductionHumanResourceList().Filter('Guid', window.PHR.Guid())[0];
				ProductionHumanResource.PreferredHumanResourceID(ROSkilledHR.HumanResourceID());
				ProductionHumanResource.PrefHumanResource(ROSkilledHR.HumanResource());

				//ROSkilledHR.GetParent().PreferredHumanResourceID(ROSkilledHR.HumanResourceID());
				//ROSkilledHR.GetParent().PrefHumanResource(ROSkilledHR.HumanResource());        
				$('#HRSearch').modal('hide');
				//}
			},
			FilterPHR: function () {
				Singular.ShowLoadingBar();
				if (!ViewModel.PHRFilter() || ViewModel.PHRFilter() == '') {
					ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHumanResourceList().Iterate(function (ProductionHumanResource, Index) {
						ProductionHumanResource.Visible(true);
					});
				} else {
					ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHumanResourceList().Iterate(function (ProductionHumanResource, Index) {
						var FilterString = ViewModel.PHRFilter().toLowerCase();
						var DiscMatchInd = (ProductionHumanResource.Discipline().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
						var PosMatchInd = (ProductionHumanResource.Position().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
						var VehMatchInd = (ProductionHumanResource.VehicleName().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
						var HRMatchInd = (ProductionHumanResource.HumanResource().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
						if (DiscMatchInd || PosMatchInd || VehMatchInd || HRMatchInd) {
							ProductionHumanResource.Visible(true);
						} else {
							ProductionHumanResource.Visible(false);
						}
					});
				};
				Singular.HideLoadingBar();
			},
			ReplaceResource: function () {
				ViewModel.QualifiedHRVisible(true);
				ViewModel.ManageScheduleVisible(false);
				GetQualifiedHR(SelectedPHR());
			},
			ClearHumanResourceID: function (ProductionHumanResource) {
				Singular.SendCommand('ClearHumanResourceID', { Guid: ProductionHumanResource.Guid() }, function (d) { })
			},
			ClearPrefHumanResourceID: function (ProductionHumanResource) {
				ProductionHumanResource.PrefHumanResource("");
				ProductionHumanResource.PreferredHumanResourceID(null);
				//Singular.SendCommand('ClearPrefHumanResourceID', { Guid: ProductionHumanResource.Guid() }, function (d) { })
			},
			DeleteProductionHumanResource: function (ProductionHumanResource) {
				Singular.ShowMessageQuestion("Delete Resource", "Are you sure you wish to delete this resource?", function () {
					Singular.SendCommand('DeleteProductionHumanResource', { Guid: ProductionHumanResource.Guid() }, function (d) { })
				})
			},
			AddNewProductionHumanResource: function () {
				Singular.SendCommand('AddNewProductionHumanResource', {}, function (d) { })
			},
			EditProductionHumanResource: function (ProductionHumanResource) {
				ProductionHumanResource.DetailVisible(!ProductionHumanResource.DetailVisible());
			},
			ShowPHRDetail: function (ProductionHumanResource) {
				this.SetSelectedPHR(ProductionHumanResource);
			}
		},
		UI: {
			CrewLeaderCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.CrewLeaderInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			ReliefCrewCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.ReliefCrewInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			TBCCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.TBCInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			TrainingCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.TrainingInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			SuppliedHumanResourceCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.SuppliedHumanResourceInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-default' }
			},
			FilteredProductionTypeCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.FilteredProductionTypeInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-danger' }
			},
			FilteredPositionCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.FilteredPositionInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-danger' }
			},
			IncludedOffCss: function (ProductionHumanResource) {
				if (ProductionHumanResource.IncludedOffInd()) { return 'btn btn-xs btn-danger' } else { return 'btn btn-xs btn-default' }
			},
			CrewLeaderHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.CrewLeaderInd()) { return "<span class='glyphicon glyphicon-check'></span> Leader" } else { return "<span class='glyphicon glyphicon-minus'></span> Leader" }
			},
			ReliefCrewHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.ReliefCrewInd()) { return "<span class='glyphicon glyphicon-check'></span> Relief" } else { return "<span class='glyphicon glyphicon-minus'></span> Relief" }
			},
			TBCHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.TBCInd()) { return "<span class='glyphicon glyphicon-check'></span> TBC" } else { return "<span class='glyphicon glyphicon-minus'></span> TBC" }
			},
			TrainingHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.TrainingInd()) { return "<span class='glyphicon glyphicon-check'></span> Training" } else { return "<span class='glyphicon glyphicon-minus'></span> Training" }
			},
			SuppliedHumanResourceHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.SuppliedHumanResourceInd()) { return "<span class='glyphicon glyphicon-check'></span> Supp/Ext" } else { return "<span class='glyphicon glyphicon-minus'></span> Supp/Ext" }
			},
			FilteredProductionTypeHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.FilteredProductionTypeInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			FilteredPositionHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.FilteredPositionInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			IncludedOffHtml: function (ProductionHumanResource) {
				if (ProductionHumanResource.IncludedOffInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
			},
			PHRCss: function (ProductionHumanResource) {
				if (!ProductionHumanResource.IsValid()) {
					return 'Invalid'
				} else if (ProductionHumanResource.TBCInd()) {
					return 'TBC'
				} else if (ProductionHumanResource.CancelledReason() != '') {
					return 'Cancelled'
				}
			}
		}
	},
	Schedules: {
		Properties: {
			CurrentHRSchedule: function () {
				return ViewModel.CurrentProduction().ProductionSystemAreaList()[0].HRProductionScheduleList().Find('Guid', ViewModel.SelectedHRScheduleGuid())
			}
		},
		UI: {
			HRLocalCss: function (HRProductionSchedule) { return (HRProductionSchedule.Local() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default') },
			HRLocalHtml: function (HRProductionSchedule) { return (HRProductionSchedule.Local() ? "<span class='glyphicon glyphicon-check'></span> Local" : "<span class='glyphicon glyphicon-minus'></span> Not Local") },
			DetailSelectCss: function (HRProductionScheduleDetail) {
				return (HRProductionScheduleDetail.Selected() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
			},
			DetailSelectHtml: function (HRProductionScheduleDetail) {
				return (HRProductionScheduleDetail.Selected() ? "<span class='glyphicon glyphicon-check'></span> Selected" : "<span class='glyphicon glyphicon-minus'></span> Select")
			},
			NewTimelineSelectCss: function (UnSelectedTimeline) {
				return (UnSelectedTimeline.Selected() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
			},
			NewTimelineSelectHtml: function (UnSelectedTimeline) {
				return (UnSelectedTimeline.Selected() ? "<span class='glyphicon glyphicon-check'></span> Selected" : "<span class='glyphicon glyphicon-minus'></span> Select")
			},
			ScheduleDetailCss: function (HRProductionScheduleDetail) {
				var CssClasses = '';
				if (HRProductionScheduleDetail.CrewTimesheetID()) {
					CssClasses = 'TimesheetProcessed'
				} else {
					CssClasses = ''
				}
				return CssClasses;
			}
		},
		Methods: {
			GetUnSelectedTimelineList: function (HRProductionSchedule) {
				UnSelectedTimelineList = [];
				ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionTimelineList().Iterate(function (ProductionTimeline, PTIndex) {
					var ScheduleDetail = HRProductionSchedule.HRProductionScheduleDetailListClient().Find('ProductionTimelineGuid', ProductionTimeline.Guid());
					if (!ScheduleDetail) {
						ProductionTimeline.HRProductionSchedule = HRProductionSchedule
						UnSelectedTimelineList.push(ProductionTimeline)
					}
				});
				return UnSelectedTimelineList
			},
			BackFromCurrentSchedule: function () {
				CurrentHRSchedule().HRProductionScheduleDetailList().Iterate(function (Item, Index) {
					Item.Visible(false);
				});
				ViewModel.SelectedHRScheduleGuid(null);
				ViewModel.HRListVisible(true);
				ViewModel.CurrentHRScheduleVisible(false);
			},
			GetScheduleFromServer: function (HRProductionSchedule, Callback) {
				Singular.ShowLoadingBar();
				ViewModel.CallServerMethod('GetSchedule', { HumanResourceID: HRProductionSchedule.HumanResourceID() }, function (args) {
					if (args.Success) {
						KOFormatter.Deserialise(args.Data, HRProductionSchedule);
						Singular.Validation.CheckRules(HRProductionSchedule);
						HRProductionSchedule.ScheduleVisible(true);
						Singular.HideLoadingBar();
						Callback(HRProductionSchedule);
					}
				})
			},
			UpdateClientScheduleWithServer: function (OldObject, NewObject, Callback) {
				var UpdatedHRSchedule = NewObject.Data;
				OldObject.AllClashes("");
				var f = new KOFormatterObject();
				f.IncludeChildren = true;
				f.IncludeClean = true;
				f.IncludeCleanInArray = true;
				f.IncludeCleanProperties = true;
				f.IncludeIsNew = true;
				f.Deserialise(UpdatedHRSchedule, OldObject)
				Singular.Validation.CheckRules(OldObject);
				var AllErrors = "";
				OldObject.HRProductionScheduleDetailListClient().Iterate(function (DetailItem, Index) {
					if (DetailItem.Clash().length > 0) {
						AllErrors += DetailItem.Clash() + ", <br /> ";
					}
				});
				OldObject.AllClashes(AllErrors);
				Callback(OldObject);
				//Singular.HideLoadingBar();
			},
			UpdateSchedule: function (HRProductionSchedule, Callback) {
				var f = new KOFormatterObject();
				f.IncludeChildren = true;
				f.IncludeClean = true;
				f.IncludeCleanInArray = true;
				f.IncludeCleanProperties = true;
				f.IncludeIsNew = true;
				ViewModel.CallServerMethod('UpdateHRSchedule', { HRScheduleClient: f.Serialise(HRProductionSchedule) }, function (args) {
					ProductionHelper.Schedules.Methods.UpdateClientScheduleWithServer(HRProductionSchedule, args, Callback);
				})
			},
			SaveSchedule: function (HRProductionSchedule) {
				Singular.ShowLoadingBar();
				ViewModel.CallServerMethod('UpdateAndSaveHRSchedule', { HRScheduleClient: KOFormatter.Serialise(HRProductionSchedule) }, function (args) {
					if (args.Success) {
						HRProductionSchedule.ScheduleVisible(false)
						HRProductionSchedule.SaveFailedVisible(false)
						HRProductionSchedule.SaveSucceededVisible(true)
					} else {
						HRProductionSchedule.ScheduleVisible(true)
						HRProductionSchedule.SaveFailedVisible(true)
						HRProductionSchedule.SaveSucceededVisible(false)
						ProductionHelper.Schedules.Methods.UpdateClientScheduleWithServer(HRProductionSchedule, args);
					}
					Singular.HideLoadingBar()
				})
			},
			RemoveScheduleItem: function (HRProductionScheduleDetail) {
				var HRProductionSchedule = HRProductionScheduleDetail.GetParent();
				HRProductionScheduleDetail.Visible(false);
				HRProductionScheduleDetail.Deleted(true);
				ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule, function (HRProductionSchedule) {
					HRProductionSchedule.ScheduleVisible(true);
					Singular.Validation.CheckRules(HRProductionSchedule);
				});
			},
			GenerateSchedules: function () {
				Singular.SendCommand("GenerateSchedules", { SendModel: true }, function (d) {
					ViewModel.HRListVisible(true)
					ViewModel.HRTimelineSelectVisible(false);
					ProductionHelper.Schedules.Methods.SetClashFieldsAfterGenerating();
				})
			},
			AddItemFromTimeline: function (HRProductionSchedule, UnSelectedTimeline) {
				var NewItem = HRProductionSchedule.HRProductionScheduleDetailListClient.AddNew();
				NewItem.JustAdded = true;
				NewItem.ProductionTimelineGuid(UnSelectedTimeline.ProductionTimelineGuid());
				NewItem.ProductionTimelineTypeID(UnSelectedTimeline.ProductionTimelineTypeID());
				NewItem.ProductionTimelineType(UnSelectedTimeline.ProductionTimelineType());
				NewItem.StartDateTime(new Date(UnSelectedTimeline.StartDateTime()));
				NewItem.EndDateTime(new Date(UnSelectedTimeline.EndDateTime()));
				NewItem.JustAdded = false;
				NewItem.Added(true);
				NewItem.Updated(false);
				NewItem.Deleted(false);
				NewItem.Visible(true);
				//Singular.Validation.CheckRules(NewItem);
			},
			CheckScheduleItemClash: function (HRProductionScheduleDetail, Element) {
				var HRProductionSchedule = HRProductionScheduleDetail.GetParent();
				ViewModel.CallServerMethod('CheckScheduleClash', { HRScheduleClient: KOFormatter.Serialise(HRProductionSchedule) }, function (args) {
					var UpdatedHRProductionScheduleDetail = args.Data;
				})
			},
			SetAllClashesField: function () {
				ProductionHelper.CurrentArea.HRProductionScheduleList().Iterate(function (HRSchedule, Index) {
					HRSchedule.HRProductionScheduleListClient().Iterate(function (Detail, Index) {
						var Current = HRSchedule.AllClashes();
						var Updated = Current + Detail.Clash() + ', <br/>';
						HRSchedule.AllClashes(Updated);
					});
				});
			},
			SetClashFieldsAfterGenerating: function () {
				ProductionHelper.CurrentArea().HRProductionScheduleList().Iterate(function (HRSchedule, Index) {
					HRSchedule.AllClashes(HRSchedule.AllClashes());
				});
			},
			DetailSelectClick: function (HRProductionScheduleDetail) {
				HRProductionScheduleDetail.Selected(!HRProductionScheduleDetail.Selected())
				HRProductionScheduleDetail.Deleted(HRProductionScheduleDetail.Selected())
				console.log(HRProductionScheduleDetail)
			},
			DeleteSelectedScheduleDetails: function (HRProductionSchedule) {
				HRProductionSchedule.IsBusyClient(true);
				//var toRemove = [];
				HRProductionSchedule.HRProductionScheduleDetailListClient().Iterate(function (item) {
					if (item.Selected()) {
						item.Deleted(true)
					}
				})
				ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule, function (HRProductionSchedule) {
					HRProductionSchedule.ScheduleVisible(true);
					HRProductionSchedule.IsBusyClient(false);
				})
			},
			AddSelectedTimeline: function (HRProductionSchedule, TimelineGuid) {
				var SelectedTimeline = ProductionHelper.ProductionTimelines.Methods.GetTimeline(TimelineGuid);
				ProductionHelper.Schedules.Methods.AddItemFromTimeline(HRProductionSchedule, SelectedTimeline);
			},
			NewTimelineSelectClick: function (UnSelectedTimeline) {
				UnSelectedTimeline.Selected(!UnSelectedTimeline.Selected())
			},
			AddSelectedTimelines: function (HRProductionSchedule) {
				HRProductionSchedule.IsBusyClient(true);
				HRProductionSchedule.UnSelectedProductionTimelineList().Iterate(function (Item, Index) {
					if (Item.Selected()) {
						ProductionHelper.Schedules.Methods.AddItemFromTimeline(HRProductionSchedule, Item);
						Item.Visible(false);
						HRProductionSchedule.IsBusyClient(false);
					}
				})
				ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule, function (HRProductionSchedule) {
					HRProductionSchedule.ScheduleVisible(true);
				})
				Singular.Validation.CheckRules(HRProductionSchedule);
			},
			ShowHRSchedule: function (HRProductionSchedule) {
				ProductionHelper.Schedules.Methods.GetScheduleFromServer(HRProductionSchedule, function (HRProductionSchedule) {

				});
			},
			HideHRSchedule: function (HRProductionSchedule) {
				HRProductionSchedule.IsBusyClient(true);
				ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule, function (HRProductionSchedule) {
					HRProductionSchedule.ScheduleVisible(false);
					HRProductionSchedule.IsBusyClient(false);
					//Singular.SendCommand('CheckScheduleSingle', { HumanResourceID: HRProductionSchedule.HumanResourceID }, function (response) { });
					//ViewModel.CallServerMethod('SaveHRSchedule', { HumanResourceID: HRProductionSchedule.HumanResourceID() }, function (args) { });

				});
			},
			BiometricTimes: function (HRProductionSchedule, self) {
				HRProductionSchedule.AccessShiftList().Iterate(function (accessshift, Index) {
					if (accessshift.AccessShiftID() == self.AccessShiftID()) {
						accessshift.ShiftStartDateTime(new Date(self.OrigStartDateTime()));
						accessshift.ChangedShiftStartDateTime(new Date(self.StartDateTime()));

						accessshift.ShiftEndDateTime(new Date(self.OrigEndDateTime()));
						accessshift.ChangedShiftEndDateTime(new Date(self.EndDateTime()));

					}
				})
			}

		},
		Events: {
			StartDateTimeSet: function (self) {
				var HRProductionSchedule = self.GetParent();
				if (!self.JustAdded) {
					//if (self.IsValid()) {  } //&& !self.IsClientNew()
					self.Updated(true)
					HRProductionSchedule.IsBusyClient(true);
					HRProductionSchedule.ScheduleVisible(false);
					ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule, function (HRProductionSchedule) {
						HRProductionSchedule.ScheduleVisible(true);
						HRProductionSchedule.IsBusyClient(false);
					})
					if (self.BioStartDateTime()) {
						ProductionHelper.Schedules.Methods.BiometricTimes(HRProductionSchedule, self)
					}
				}
			},
			EndDateTimeSet: function (self) {
				var HRProductionSchedule = self.GetParent();
				if (!self.JustAdded) {
					if (!OBMisc.Dates.SameDay(self.StartDateTime(), self.EndDateTime())) {
						self.SuspendChanges = true
						//End Time
						var ED = new Date(self.EndDateTime());
						var Hours = ED.getHours();
						var Minutes = ED.getMinutes();
						//Start Time
						var SD = new Date(self.StartDateTime());
						var Year = SD.getFullYear();
						var Month = SD.getMonth();
						var Day = SD.getDate();
						//New End Date
						var NewEndDate = new Date(Date.UTC(Year, Month, Day, Hours, Minutes, 0));
						self.EndDateTime(NewEndDate);
						self.SuspendChanges = false
					};
					//if (self.IsValid()) { self.Updated(true) } //&& !self.IsClientNew()
					self.Updated(true)
					HRProductionSchedule.IsBusyClient(true);
					HRProductionSchedule.ScheduleVisible(false);
					ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule, function (HRProductionSchedule) {
						HRProductionSchedule.ScheduleVisible(true);
						HRProductionSchedule.IsBusyClient(false);
					})

					if (self.BioEndDateTime()) {
						ProductionHelper.Schedules.Methods.BiometricTimes(HRProductionSchedule, self)
					}
				}
			}

		},
		Rules: {
			StartDateTimeValid: function (Value, Rule, CtlError) {
				var self = CtlError.Object;
				if (self.StartDateTime() && self.EndDateTime()) {
					var SDT = new Date(self.StartDateTime());
					var EDT = new Date(self.EndDateTime());
					if (SDT != NaN && EDT != NaN) {
						var ST = new Date(self.StartDateTime()).getTime();
						var ET = new Date(self.EndDateTime()).getTime();
						if (!(ST <= ET)) {
							CtlError.AddError("Start Time must be before End Time");
						}
					};
					if (!OBMisc.Dates.SameDay(self.StartDateTime(), self.EndDateTime())) {
						CtlError.AddError("Start and End Date must be the same");
					}
				} else {
					CtlError.AddError("Start Time and End Time required");
				}
			},
			EndDateTimeValid: function (Value, Rule, CtlError) {
				var self = CtlError.Object;
				if (self.StartDateTime() && self.EndDateTime()) {
					var SDT = new Date(self.StartDateTime());
					var EDT = new Date(self.EndDateTime());
					if (SDT != NaN && EDT != NaN) {
						var ST = new Date(self.StartDateTime()).getTime();
						var ET = new Date(self.EndDateTime()).getTime();
						if (!(ST <= ET)) {
							CtlError.AddError("Start Time must be before End Time");
						}
					};
					if (!OBMisc.Dates.SameDay(self.StartDateTime(), self.EndDateTime())) {
						CtlError.AddError("Start and End Date must be the same");
					};
				} else {
					CtlError.AddError("Start Time and End Time required");
				}
			}
		}
	},
	Documents: {
		Methods: {
			AddNewProductionDocument: function () {
				var NewItem = ViewModel.CurrentProduction().ProductionDocumentList.AddNew();
				NewItem.SystemID(ProductionHelper.CurrentArea().SystemID());
				NewItem.ParentID(ProductionHelper.CurrentArea().ProductionID());
				NewItem.ProductionAreaID(ProductionHelper.CurrentArea().ProductionAreaID());
			},
			DocumentTypeIDSet: function (ProductionDocument) {
				var Production = ProductionDocument.GetParent();
				if (Production) {
					if ((ProductionDocument.DocumentTypeID() == 16 || ProductionDocument.DocumentTypeID() == 6)) {
						ProductionHelper.Production.Rules.CheckCanReconcile(Production)
					}
				}
			}
		}
	},
	GeneralComments: {
		Methods: {
			AddNewProductionGeneralComment: function () {
				var NewItem = ViewModel.CurrentProduction().ProductionGeneralCommentList.AddNew();
				NewItem.SystemID(ProductionHelper.CurrentArea().SystemID());
				NewItem.ProductionID(ProductionHelper.CurrentArea().ProductionID());
				NewItem.ProductionAreaID(ProductionHelper.CurrentArea().ProductionAreaID());
			}
		}
	},
	PettyCash: {
		Methods: {
			AddNewProductionPettyCash: function () {
				var NewItem = ViewModel.CurrentProduction().ProductionPettyCashList.AddNew();
				NewItem.SystemID(ProductionHelper.CurrentArea().SystemID());
				NewItem.ProductionID(ProductionHelper.CurrentArea().ProductionID());
				NewItem.ProductionAreaID(ProductionHelper.CurrentArea().ProductionAreaID());
			}
		}
	},
	Audio: {
		Methods: {
			SetAudioConfigDetails: function (obj) {
				//var pt = ClientData.ROProductionTypeList.Find('ProductionTypeID', Production.ProductionTypeID())
				var AudioConfigLen = ClientData.AudioConfigurationList.length;
				if (AudioConfigLen > 0) {
					var AudioConfig = ClientData.AudioConfigurationList.Find("AudioConfigurationID", obj.AudioConfigurationID());

					if (AudioConfig != undefined) {
						var acd = AudioConfig.AudioConfigurationDetailList;
						var len = AudioConfig.AudioConfigurationDetailList.length;
						obj.ProductionAudioConfigurationDetailList().Clear();
						for (var i = 0; i < len; i++) {
							var newPACD = obj.ProductionAudioConfigurationDetailList.AddNew();
							newPACD.AudioConfigurationDetail(acd[i].AudioConfigurationDetail);
						} //for
					} //if
				} //if
			} //SetAudioConfigDetails
		}//Methods
	},
	Travel: {
		Methods: {
			EditSelectedTravelReq: function (obj) {
				if (ViewModel.CurrentProduction().ProductionSystemAreaList()[0].SystemID() == 1) {
					var message = ViewModel.CurrentProduction().CanOpenTravelMessage();
					if (message != '') {
						Singular.ShowMessage('Travel', message, 2)
					}
					else {
						window.location.href = window.SiteInfo.SitePath + "/Travel/Travel.aspx?TRID=" + obj.TravelRequisitionID().toString();
						//Singular.SendCommand('EditTravel', { TravelRequisitionID: obj.TravelRequisitionID(), ProductionID: obj.ProductionID() }, function (d) {
						//	window.location.href = d;
						//});
					}
				}
				else {
					window.location.href = window.SiteInfo.SitePath + "/Travel/Travel.aspx?TRID=" + obj.TravelRequisitionID().toString();
					//Singular.SendCommand('EditTravel', { TravelRequisitionID: obj.TravelRequisitionID(), ProductionID: obj.ProductionID() }, function (d) {
					//	window.location.href = d;
					//});
				}
			}
		}
	},
	BulkEdit: {

		BulkEditDisciplineUpdateClick: function (BulkEditDiscipline) {
			BulkEditDiscipline.UpdateInd(!BulkEditDiscipline.UpdateInd());
			BulkEditDiscipline.HumanResources().Iterate(function (HR, Index) {
				HR.UpdateInd(BulkEditDiscipline.UpdateInd());
			});
		},
		BulkEditDisciplineUpdateCss: function (BulkEditDiscipline) {
			return (BulkEditDiscipline.UpdateInd() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
		},
		BulkEditDisciplineUpdateHtml: function (BulkEditDiscipline) {
			return (BulkEditDiscipline.UpdateInd() ? "<span class='glyphicon glyphicon-check'></span> Yes" : "<span class='glyphicon glyphicon-minus'></span> No")
		},
		BulkEditHRUpdateClick: function (BulkEditHR) {
			BulkEditHR.UpdateInd(!BulkEditHR.UpdateInd())
		},
		BulkEditHRUpdateCss: function (BulkEditHR) {
			return (BulkEditHR.UpdateInd() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
		},
		BulkEditHRUpdateHtml: function (BulkEditHR) {
			return (BulkEditHR.UpdateInd() ? "<span class='glyphicon glyphicon-check'></span> Yes" : "<span class='glyphicon glyphicon-minus'></span> No")
		},

		BulkEditTimelineDayUpdateClick: function (BulkEditTimelineDay) {
			BulkEditTimelineDay.UpdateInd(!BulkEditTimelineDay.UpdateInd())
			BulkEditTimelineDay.ProductionTimelineItems().Iterate(function (Timeline, Index) {
				Timeline.UpdateInd(BulkEditTimelineDay.UpdateInd());
			});
		},
		BulkEditTimelineDayUpdateCss: function (BulkEditTimelineDay) {
			return (BulkEditTimelineDay.UpdateInd() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
		},
		BulkEditTimelineDayUpdateHtml: function (BulkEditTimelineDay) {
			return (BulkEditTimelineDay.UpdateInd() ? "<span class='glyphicon glyphicon-check'></span> Yes" : "<span class='glyphicon glyphicon-minus'></span> No")
		},
		BulkEditTimelineUpdateClick: function (BulkEditTimeline) {
			BulkEditTimeline.UpdateInd(!BulkEditTimeline.UpdateInd())
		},
		BulkEditTimelineUpdateCss: function (BulkEditTimeline) {
			return (BulkEditTimeline.UpdateInd() ? 'btn btn-xs btn-primary' : 'btn btn-xs btn-default')
		},
		BulkEditTimelineUpdateHtml: function (BulkEditTimeline) {
			return (BulkEditTimeline.UpdateInd() ? "<span class='glyphicon glyphicon-check'></span> Yes" : "<span class='glyphicon glyphicon-minus'></span> No")
		}

	}
}