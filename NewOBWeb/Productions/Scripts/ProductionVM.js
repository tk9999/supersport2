﻿Singular.OnPageLoad(() => {
  console.log('load');
  if (ViewModel.CurrentProduction()) {
    if (ViewModel.CurrentProduction().CancelledTravelHRNames().length > 0) {
      console.log(ViewModel.CurrentProduction().CancelledTravelHRNames());
      Singular.ShowMessage("Travel Cancelled", ViewModel.CurrentProduction().CancelledTravelHRNames(), 1, function () {
        ViewModel.CurrentProduction().CancelledTravelNames("");
      });
    };
  };
});

ProductionVM = {
  Tabs: {
    SetMainTab: function () {
      if (ViewModel.CurrentProduction()) {
        if (ViewModel.CurrentProduction().CurrentUserIsEventManager()) {
          this.Timelines.SetTimelineTabsVisible();
          $('a[href="#Timelines"]').trigger('click');
        }
      } else {
        this.Main.SetMainTabVisible();
      }
    },
    Main: {
      SetMainTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#MainTab').addClass('active');
        ViewModel.MainTabVisible(true)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      },
      FindROProductionType: function () {
        FetchROProductionTypeList(this.SetupProductionTypeModal())
      },
      FetchROProductionTypeList: function (CallBack) {
        Singular.GetDataStateless('OBLib.Maintenance.ReadOnly.ROProductionTypeList, OBLib', {}, function (args) {
          if (args.Success) {
            KOFormatter.Deserialise(args.Data, ViewModel.ROProductionTypeList);
            CallBack
          }
        })
      },
      SetupProductionTypeModal: function () {
        $('#SelectROProductionType').modal();
        $("#SelectROProductionType").draggable({
          handle: ".modal-header"
        })
      },
      FilterROProductionTypes: function () {
        if (!ViewModel.ProductionTypeFilter() || ViewModel.ProductionTypeFilter() == '') {
          ViewModel.ROProductionTypeList().Iterate(function (ROProductionType, Index) {
            ROProductionType.VisibleClient(true);
          });
        } else {
          ViewModel.ROProductionTypeList().Iterate(function (ROProductionType, Index) {
            if (ROProductionType.ProductionType().toLowerCase().indexOf(ViewModel.ProductionTypeFilter().toLowerCase()) >= 0) {
              ROProductionType.VisibleClient(true);
            } else {
              ROProductionType.VisibleClient(false);
            };
          });
        }
      },
      SetProductionTypeID: function (ROProductionType) {
        ViewModel.CurrentProduction().ProductionTypeID(ROProductionType.ProductionTypeID())
        ViewModel.CurrentProduction().ProductionType(ROProductionType.ProductionType())
      }
    },
    Requirements: {
      SetRequirementsTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#RequirementTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(true)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    OutsourceServices: {
      SetOutsourceServicesTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#OutsourceServiceTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(true)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    ProductionVehicles: {
      SetProductionVehiclesTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#ProductionVehicleTab').addClass('active');
        //ProductionHelper.ModelManager.SetProductionVehiclesTabVisible(ViewModel) 
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(true)
        ViewModel.VehicleListVisible(true)
        ViewModel.SelectVehicleVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    Timelines: {
      SetTimelineTabsVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#TimelineTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(true)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    ProductionHumanResources: {
      SetProductionCrewTabsVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#ProductionCrewTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(true)
        ViewModel.PHRListVisible(true)
        ViewModel.SelectedHRVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    ProductionSchedules: {
      SetProductionScheduleTabsVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#ProductionScheduleTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(true)
        ViewModel.HRListVisible(true)
        ViewModel.HRTimelineSelectVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    TravelRequisitions: {
      SetTravelRequisitionTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#TravelRequisitionTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.HRListVisible(false)
        ViewModel.HRTimelineSelectVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(true)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    Correspondence: {
      SetCorrespondenceTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#CorrespondenceTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.HRListVisible(false)
        ViewModel.HRTimelineSelectVisible(false)
        ViewModel.CorrespondenceTabVisible(true)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    ProductionComments: {
      SetProductionCommentSectionTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#ProductionCommentTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.HRListVisible(false)
        ViewModel.HRTimelineSelectVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(true)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(false)
      }
    },
    ProductionAudio: {
      SetProductionAudioTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#ProductionAudioTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.HRListVisible(false)
        ViewModel.HRTimelineSelectVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(false)
        ViewModel.AudioSectionTabVisible(true)
      }
    },
    ProductionReports: {
      SetReportsTabVisible: function () {
        $('.MenuTab').removeClass('active');
        $('#ReportsTab').addClass('active');
        ViewModel.MainTabVisible(false)
        ViewModel.RequirementsTabVisible(false)
        ViewModel.OutsourceServicesTabVisible(false)
        ViewModel.ProductionVehiclesTabVisible(false)
        ViewModel.TimelinesTabVisible(false)
        ViewModel.ProductionCrewTabVisible(false)
        ViewModel.SchedulesTabVisible(false)
        ViewModel.HRListVisible(false)
        ViewModel.HRTimelineSelectVisible(false)
        ViewModel.CorrespondenceTabVisible(false)
        ViewModel.CommentSectionTabVisible(false)
        ViewModel.TravelTabVisible(false)
        ViewModel.ReportsTabVisible(true)
        ViewModel.AudioSectionTabVisible(false)
      }
    }
  }
}

function SetMainTabVisible() { ProductionVM.Tabs.Main.SetMainTabVisible() }
function SetRequirementsTabVisible() { ProductionVM.Tabs.Requirements.SetRequirementsTabVisible() }
function SetOutsourceServicesTabVisible() { ProductionVM.Tabs.OutsourceServices.SetOutsourceServicesTabVisible() }
function SetProductionVehiclesTabVisible() { ProductionVM.Tabs.ProductionVehicles.SetProductionVehiclesTabVisible() }
function SetTimelineTabsVisible() { ProductionVM.Tabs.Timelines.SetTimelineTabsVisible() }
function SetProductionCrewTabsVisible() { ProductionVM.Tabs.ProductionHumanResources.SetProductionCrewTabsVisible() }
function SetProductionScheduleTabsVisible() { ProductionVM.Tabs.ProductionSchedules.SetProductionScheduleTabsVisible() }
function SetCorrespondenceTabVisible() { ProductionVM.Tabs.Correspondence.SetCorrespondenceTabVisible() }
function SetTravelRequisitionTabVisible() { ProductionVM.Tabs.TravelRequisitions.SetTravelRequisitionTabVisible() }
function SetProductionCommentSectionTabVisible() { ProductionVM.Tabs.ProductionComments.SetProductionCommentSectionTabVisible() }
function SetReportsTabVisible() { ProductionVM.Tabs.ProductionReports.SetReportsTabVisible() }
function SetProductionAudioTabVisible() { ProductionVM.Tabs.ProductionAudio.SetProductionAudioTabVisible() }

//Top
function FilterProductionSpecRequirementList(List, Item) {
  return ProductionHelper.ProductionSystemAreas.Methods.FilterProductionSpecRequirementList(List, Item);
}
function GetAllowedStatuses(List, Item) {
  return ProductionHelper.ProductionSystemAreas.Methods.GetAllowedStatuses(List, Item);
}


//Main Tab-------------------------------------------------------------------------------------
function CanEditProductionType() { return ProductionHelper.Production.Rules.CanEditProductionType() }
function CanEditEventType() { return ProductionHelper.Production.Rules.CanEditEventType() }
//Production Spec Select
function FindROProductionSpec() { ProductionHelper.Production.Methods.FindROProductionSpec() }
function FilterROProductionSpec() { ProductionHelper.Production.Methods.FilterROProductionSpec() }
function PopulateSpec(ROProductionSpec) { ProductionHelper.Production.Methods.PopulateSpec(ROProductionSpec) }
function ClearProductionSpec() { ProductionHelper.Production.Methods.ClearProductionSpec() }
//Production Venue Select
function FindROProductionVenue() { ProductionHelper.Production.Methods.FindROProductionVenue() }
function FilterROProductionVenues() { ProductionHelper.Production.Methods.FilterROProductionVenues() }
function SetProductionVenueID(ROProductionVenue) { ProductionHelper.Production.Methods.SetProductionVenueID(ROProductionVenue) }
function HDRequiredClick(Production) { ProductionHelper.Production.Events.HDRequiredClick(Production) }
function HDRequiredCss(Production) { return ProductionHelper.Production.UI.HDRequiredCss(Production) }
function HDRequiredHtml(Production) { return ProductionHelper.Production.UI.HDRequiredHtml(Production) }
//Statuses
function CrewFinalisedClick(Production) { ProductionHelper.Production.Events.CrewPlanningFinalisedClicked(Production) }
function CrewFinalisedCss(Production) { return ProductionHelper.Production.UI.CrewFinalisedCss(Production) }
function CrewFinalisedHtml(Production) { return ProductionHelper.Production.UI.CrewFinalisedHtml(Production) }
function CanClickCrewFinalise(Production) { return ProductionHelper.Production.Rules.CanClickCrewFinalise(Production) }
function PlanningFinalisedClick(Production) { ProductionHelper.Production.Events.PlanningFinalisedClicked(Production) }
function PlanningFinalisedCss(Production) { return ProductionHelper.Production.UI.PlanningFinalisedCss(Production) }
function PlanningFinalisedHtml(Production) { return ProductionHelper.Production.UI.PlanningFinalisedHtml(Production) }
function CanClickReconcile(Production) { return ProductionHelper.Production.Rules.CanClickReconcile(Production) }
function ReconciledClick(Production) { ProductionHelper.Production.Events.ReconciledClicked(Production) }
function ReconciledDateClick(Production) { ProductionHelper.Production.Events.ReconciledDateClick(Production) }
function ReconciledCss(Production) { return ProductionHelper.Production.UI.ReconciledCss(Production) }
function ReconciledHtml(Production) { return ProductionHelper.Production.UI.ReconciledHtml(Production) }
function CancelledClick(Production) { ProductionHelper.Production.Events.CancelledClicked(Production) }
function CancelledCss(Production) { return ProductionHelper.Production.UI.CancelledCss(Production) }
function CancelledHtml(Production) { return ProductionHelper.Production.UI.CancelledHtml(Production) }

function CommentatorCrewFinalisedClick(Production) {
  Production.CommentatorCrewFinalised(!Production.CommentatorCrewFinalised());
  if (Production.CommentatorCrewFinalised()) {
    Production.CommentatorCrewPlanningFinalisedDate(new Date())
    Production.CommentatorCrewPlanningFinalisedBy(ViewModel.CurrentUserID())
    ProductionHelper.CurrentArea().ProductionAreaStatusID(2); //crew finaliseds
  }
  else {
    Production.CommentatorCrewPlanningFinalisedBy(null)
    Production.CommentatorCrewPlanningFinalisedDate(null)
    if (Production.Cancelled()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(5);
    } else if (Production.CommentatorReconciled()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(4);
    } else if (Production.CommentatorPlanningFinalised()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(3);
    } else {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
    }
  }
}
function CommentatorCrewFinalisedCss(Production) {
  if (Production.CommentatorCrewFinalised()) { return 'btn btn-primary' } else { return 'btn btn-default' }
}
function CommentatorCrewFinalisedHtml(Production) {
  if (Production.CommentatorCrewFinalised()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
}
function CommentatorPlanningFinalisedClick(Production) {
  Production.CommentatorPlanningFinalised(!Production.CommentatorPlanningFinalised());
  if (Production.CommentatorPlanningFinalised()) {
    Production.CommentatorPlanningFinalisedDate(new Date());
    Production.CommentatorPlanningFinalisedBy(ViewModel.CurrentUserID());
    ProductionHelper.CurrentArea().ProductionAreaStatusID(3); //planning finalised
  }
  else {
    Production.CommentatorPlanningFinalisedDate(null);
    Production.CommentatorPlanningFinalisedBy(null);
    if (Production.Cancelled()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(5);
    } else if (Production.CommentatorReconciled()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(4);
    } else if (Production.CommentatorCrewFinalised()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(2);
    } else {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
    }
  }
}
function CommentatorPlanningFinalisedCss(Production) {
  if (Production.CommentatorPlanningFinalised()) { return 'btn btn-success' } else { return 'btn btn-default' }
}
function CommentatorPlanningFinalisedHtml(Production) {
  if (Production.CommentatorPlanningFinalised()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
}
function CommentatorReconciledClick(Production) {
  Production.CommentatorReconciled(!Production.CommentatorReconciled());
  if (Production.CommentatorReconciled()) {
    Production.CommentatorReconciledDate(new Date());
    Production.CommentatorReconciledBy(ViewModel.CurrentUserID());
    ProductionHelper.CurrentArea().ProductionAreaStatusID(4);  //reconciled
  }
  else {
    Production.CommentatorReconciledDate(null);
    Production.CommentatorReconciledBy(null);
    if (Production.Cancelled()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(5);
    } else if (Production.CommentatorPlanningFinalised()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(3);
    } else if (Production.CommentatorCrewFinalised()) {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(2);
    } else {
      ProductionHelper.CurrentArea().ProductionAreaStatusID(1);
    }
  }
}

function TimelineFinalisedCss(Production) {
  if (Production.TimelineReadyInd()) { return 'btn btn-success' } else { return 'btn btn-default' }
}
function TimelineFinalisedHtml(Production) {
  if (Production.TimelineReadyInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
}
function TimelineFinalisedClick(Production) {
  Production.TimelineReadyInd(!Production.TimelineReadyInd());
}
function CommentatorReconciledCss(Production) {
  if (Production.CommentatorReconciled()) { return 'btn btn-warning' } else { return 'btn btn-default' }
}
function CommentatorReconciledHtml(Production) {
  if (Production.CommentatorReconciled()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" }
}

function CanClickFinaliseTimeline(Production) {
  if (ViewModel.CurrentSystemID() == 1 && ViewModel.CanFinaliseTimelineInd() && ViewModel.CanUnFinaliseTimelineInd()) {
    return true;
  }
  return false;
}


function DisableCrewScheduleBulkButtons() {
  if (ViewModel.CurrentSystemID() != 1) {
    return true;
  }
  return ViewModel.CurrentProduction().IsTimelineReady();
  //else {
  //  return !ViewModel.CurrentProduction().IsTimelineReady();
  //}
  //if (!) {
  //  return false;
  //}
  //else {
  //    return true;
  //}
};

function CanEditCrewSchedulesAfterFinalised(obj) {

  if (ViewModel.CurrentSystemID() == 1) {
    return DisableCrewScheduleBulkButtons();
  }
  else {
    if (obj.GetParent().CanEditCrewSchedulesAfterFinalised()) { return true } else { return false }
    return false;
  }
}

//Production Type Select
function FindROProductionType() { ProductionVM.Tabs.Main.FindROProductionType() }
function FilterROProductionTypes() { ProductionVM.Tabs.Main.FilterROProductionTypes() };
function SetProductionTypeID(ROProductionType) { ProductionVM.Tabs.Main.SetProductionTypeID(ROProductionType) };
//Event Type Select
function FindROEventType() {
  FetchROEventTypeList(SetupEventTypeModal());
};
function SetupEventTypeModal() {
  FilterROEventTypes();
  $('#SelectROEventType').modal();
  $("#SelectROEventType").draggable({
    handle: ".modal-header"
  });
};
function FetchROEventTypeList(CallBack) {
  Singular.GetDataStateless('OBLib.Maintenance.ReadOnly.ROEventTypeList, OBLib', {}, function (args) {
    if (args.Success) {
      KOFormatter.Deserialise(args.Data, ViewModel.ROEventTypeList);
      CallBack
    }
  });
};
function FilterROEventTypes() {
  ViewModel.ROEventTypeList().Iterate(function (ROEventType, Index) { ROEventType.VisibleClient(false) });
  //first filter by production type id
  var ProductionTypeID = ViewModel.CurrentProduction().ProductionTypeID();
  ViewModel.ROEventTypeList().Iterate(function (ROEventType, Index) {
    //if a production type has been specified
    if (ProductionTypeID) {
      //filter for the same production type
      if (ProductionTypeID == ROEventType.ProductionTypeID()) {
        //if a filter string has not been typed in
        if (!ViewModel.EventTypeFilter() || ViewModel.EventTypeFilter() == '') {
          //set to visible
          ROEventType.VisibleClient(true);
        } else {
          //check if there is a match with what has been typed in
          if (ROEventType.EventType().toLowerCase().indexOf(ViewModel.EventTypeFilter().toLowerCase()) >= 0) {
            //if it matches, make it visible
            ROEventType.VisibleClient(true);
          } else {
            //otherwise hide it
            ROEventType.VisibleClient(false);
          };
        }
      } else {
        //hide if not from the same production type
        ROEventType.VisibleClient(false);
      }
    } else {
      //otherwise show all
      ROEventType.VisibleClient(true);
    };
  });

};
function SetEventTypeID(ROEventType) {
  ViewModel.CurrentProduction().EventTypeID(ROEventType.EventTypeID());
  ViewModel.CurrentProduction().EventType(ROEventType.EventType());
  if (!ViewModel.CurrentProduction().ProductionTypeID() || ROEventType.ProductionTypeID() != ViewModel.CurrentProduction().ProductionTypeID()) {
    var ROProductionType = ROProductionTypeList.Find('ProductionTypeID', ROEventType.ProductionTypeID());
    if (ROProductionType) {
      SetProductionTypeID(ROProductionType);
    }
  };
};

function FilterPositionTypeList(List, Item) {
  var Allowed = [];
  ClientData.ROPositionTypeList.Iterate(function (ROPositionType, Index) {
    if (ROPositionType.DisciplineID == Item.DisciplineID()) {
      Allowed.push({ PositionTypeID: ROPositionType.PositionTypeID, PositionType: ROPositionType.PositionType });
    };
  });
  return Allowed;
};

//Equipment and Disciplines-------------------------------------------------------------------------------------
function CanAddProductionEquipment() { return ProductionHelper.Spec.Rules.CanAddProductionEquipment() }
function CanAddRequirementPosition() { return ProductionHelper.Spec.Rules.CanAddRequirementPosition() }
function CanAddRequirementEquipmentType() { return ProductionHelper.Spec.Rules.CanAddRequirementEquipmentType() }
function AddEquipment() { ProductionHelper.Spec.Methods.AddEquipment() }
function AddPosition() { ProductionHelper.Spec.Methods.AddPosition() }
function RemoveEquipment(Equipment) { ProductionHelper.Spec.Methods.RemoveEquipment(Equipment) }
function RemovePosition(Position) { ProductionHelper.Spec.Methods.RemovePosition(Position) }


//Services------------------------------------------------------------------------------------------
function CanAddProductionOutsourceService() { return ProductionHelper.OutsourceServices.Rules.CanAddProductionOutsourceService() }
function AddNewOutsourceService() { ProductionHelper.OutsourceServices.Methods.AddNewOutsourceService() }
function NotRequiredIndClick(Service) { ProductionHelper.OutsourceServices.Methods.NotRequiredIndClick(Service) }
function NotRequiredIndCss(Service) { return ProductionHelper.OutsourceServices.UI.NotRequiredIndCss(Service) }
function NotRequiredIndHtml(Service) { return ProductionHelper.OutsourceServices.UI.NotRequiredIndHtml(Service) }
function CaterBreakfastClick(ServiceTimeline) { ProductionHelper.OutsourceServices.Methods.CaterBreakfastClick(ServiceTimeline) }
function CaterBreakfastCss(ServiceTimeline) { return ProductionHelper.OutsourceServices.UI.CaterBreakfastCss(ServiceTimeline) }
function CaterBreakfastHtml(ServiceTimeline) { return ProductionHelper.OutsourceServices.UI.CaterBreakfastHtml(ServiceTimeline) }
function CaterLunchClick(ServiceTimeline) { ProductionHelper.OutsourceServices.Methods.CaterLunchClick(ServiceTimeline) }
function CaterLunchCss(ServiceTimeline) { return ProductionHelper.OutsourceServices.UI.CaterLunchCss(ServiceTimeline) }
function CaterLunchHtml(ServiceTimeline) { return ProductionHelper.OutsourceServices.UI.CaterLunchHtml(ServiceTimeline) }
function CaterDinnerClick(ServiceTimeline) { ProductionHelper.OutsourceServices.Methods.CaterDinnerClick(ServiceTimeline) }
function CaterDinnerCss(ServiceTimeline) { return ProductionHelper.OutsourceServices.UI.CaterDinnerCss(ServiceTimeline) }
function CaterDinnerHtml(ServiceTimeline) { return ProductionHelper.OutsourceServices.UI.CaterDinnerHtml(ServiceTimeline) }
function ServiceDetailRequiredIndClick(ServiceDetail) { ProductionHelper.OutsourceServices.Methods.ServiceDetailRequiredIndClick(ServiceDetail) }
function ServiceDetailRequiredIndCss(ServiceDetail) { return ProductionHelper.OutsourceServices.UI.ServiceDetailRequiredIndCss(ServiceDetail) }
function ServiceDetailRequiredIndHtml(ServiceDetail) { return ProductionHelper.OutsourceServices.UI.ServiceDetailRequiredIndHtml(ServiceDetail) }
function IsCatering(ServiceTimeline) { return ProductionHelper.OutsourceServices.Methods.IsCatering(ServiceTimeline) }


//Production Vehicles------------------------------------------------------------------------------------------
function SetupSelectVehicle() { ProductionHelper.Vehicles.Methods.SetupSelectVehicle() }
function AddVehicle(ROVehicle) { ProductionHelper.Vehicles.Methods.AddVehicle(ROVehicle) }
function RemoveVehicle(ProductionVehicle) { ProductionHelper.Vehicles.Methods.RemoveVehicle(ProductionVehicle) }
function BackToProductionVehicleList() { ProductionHelper.Vehicles.Methods.BackToProductionVehicleList() }
function FilterROVehicleList() { ProductionHelper.Vehicles.Methods.FilterROVehicleList() }


//Timelines----------------------------------------------------------------------------------------------------
function AddNewTimeline() { ProductionHelper.ProductionTimelines.Methods.AddNewTimeline() }
function RemoveTimeline(ProductionTimeline) { ProductionHelper.ProductionTimelines.Methods.RemoveTimeline(ProductionTimeline) }
function GetVehicleDropDown(List, Item) { return ProductionHelper.Vehicles.Methods.GetVehicleDropDown(List, Item) }


//Production System Area & Timelines------------------------------------------------------------------------------------------
function CanAddProductionTimeline() {
  if (ViewModel.CurrentProduction().ProductionTypeID() && ViewModel.CurrentProduction().ProductionVenueID()) {
    return true
  } else {
    return false
  }
}

function SelectedTimeline() {
  return ViewModel.CurrentProductionSystemArea().ProductionTimelineList().Find('ProductionTimelineID', ViewModel.SelectedPTID());
}

function SetSelectedTimeline(ProductionTimeline) {
  ViewModel.SelectedPTID(ProductionTimeline.ProductionTimelineID())
};

function ProductionAreaStatusClick(ProductionSystemArea) { }

function ProductionAreaStatusCss(ProductionSystemArea) {
  switch (ProductionSystemArea.ProductionAreaStatusID()) {
    case 1:
      return 'btn btn-xs btn-primary';
      break;
    case 2:
      return 'btn btn-xs btn-success';
      break;
    case 3:
      return 'btn btn-xs btn-success';
      break;
    case 4:
      return 'btn btn-xs btn-warning';
      break;
    case 5:
      return 'btn btn-xs btn-danger';
      break;
    default:
      return 'btn btn-xs btn-default';
      break
  };
};

function ProductionAreaStatusHtml(ProductionSystemArea) {
  return ProductionSystemArea.ProductionAreaStatus()
}

function EditProductionArea(ProductionSystemArea) { Singular.SendROCommand('EditProductionSystemArea', { ProductionSystemAreaID: ProductionSystemArea.ProductionSystemAreaID() }) }

//Not added to Production Helpers
function SetupAddProductionSystemArea() {
  var n = new Templates_SystemAreaTemplateObject();
  n.SystemID(null);
  n.ProductionAreaID(null);
  ViewModel.NewSystemArea(n);
};

function SendNewProductionSystemAreaRequest() {
  Singular.SendCommand('AddNewProductionSystemArea', { SystemID: ViewModel.NewSystemArea().SystemID(), ProductionAreaID: ViewModel.NewSystemArea().ProductionAreaID() }, function (d) { });
};

function EditProductionSystemArea(ProductionSystemArea) {
  Singular.SendCommand('SetCurrentProductionSystemArea', { ProductionSystemAreaID: ProductionSystemArea.ProductionSystemAreaID() }, function (d) {
    ViewModel.SystemAreaListVisible(false);
    ViewModel.CurrentProductionSystemAreaVisible(true);
  });
};

function BackToAreaList() {
  ViewModel.CurrentProductionSystemAreaVisible(false);
  ViewModel.SystemAreaListVisible(true);
};

function CurrentAreaDescription() {
  if (ViewModel.CurrentProductionSystemArea()) {
    if (ViewModel.CurrentProductionSystemArea().ProductionArea()) {
      return ViewModel.CurrentProductionSystemArea().ProductionArea();
    };
  };
  return 'Current Area';
};

function ProductionAreaStatusClick(ProductionSystemArea) {
  ViewModel.StatusesVisible(true);
  ViewModel.TimelinesVisible(false);
};

function ProductionAreaStatusCss(ProductionSystemArea) {
  return ProductionHelper.ProductionSystemAreas.UI.ProductionAreaStatusCss(ProductionSystemArea);
};

function ProductionAreaStatusHtml(ProductionSystemArea) {
  return ProductionHelper.ProductionSystemAreas.UI.ProductionAreaStatusHtml(ProductionSystemArea);
};

function FilterAllowedTimelineTypes(List, Item) {
  //console.log(Item);
  var psa = ViewModel.CurrentProduction().ProductionSystemAreaList()[0];
  if (psa) {
    return ProductionHelper.ProductionSystemAreas.GetAllowedTimelineTypes(ClientData.ROProductionAreaAllowedTimelineTypeList,
      psa.ProductionAreaID(),
      psa.SystemID(),
      Item)
  } else {
    return [];
  }
};

function AllowedStatuses() {
  return ProductionHelper.ProductionSystemAreas.GetAllowedTimelineTypes(ClientData.ROProductionAreaAllowedTimelineTypeList,
    ViewModel.CurrentProductionSystemArea().ProductionAreaID(),
    ViewModel.CurrentProductionSystemArea().SystemID())
};

function SelectStatus(ROAllowedAreaStatus) {
  console.log('SelectStatus start');
  Singular.SendCommand('SelectStatus', {
    ProductionAreaStatusID: ROAllowedAreaStatus.NxtProductionAreaStatusID(),
    ProductionAreaStatus: ROAllowedAreaStatus.NextStatus()
  }, function (d) {
    ViewModel.TimelinesVisible(true);
    ViewModel.StatusesVisible(false);
  });
  console.log('SelectStatus start');
};


//Production Human Resources------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
function ProductionHumanResourceListSorted() { return ProductionHelper.ProductionHumanResources.Methods.GetSortedList() }
function CanAddProductionHumanResource() {
  if (ViewModel.CurrentSystemID() == 1 && !ViewModel.CurrentProduction().IsTimelineReady()) {
    return false;
  }
  return ProductionHelper.ProductionHumanResources.Rules.CanAddProductionHumanResource()
};
function CanPopulateHR() {
  //if (ViewModel.CurrentSystemID() == 1 && !ViewModel.CurrentProduction().TimelineReadyInd()) {
  //  return false;
  //}
  return ProductionHelper.ProductionHumanResources.Rules.CanAddProductionHumanResource()
};


function CanUpdateProductionHumanResource(ProductionHumanResource) {
  if (ViewModel.CurrentSystemID() != 1) {
    return true;
  }
  if (ViewModel.CurrentSystemID() == 1 && !ViewModel.CurrentProduction().IsTimelineReady()) {
    if (ProductionHumanResource.CoreCrewInd()) {
      return true;
    }
  }
  return false;
}

function CanDeleteProductionHumanResource() {
  if (ViewModel.CurrentSystemID() != 1) {
    return true;
  }
  if (ViewModel.CurrentSystemID() == 1) {
    return true;
  }
  return false;
  //&& !ViewModel.CurrentProduction().IsTimelineReady()
};

function CanSeeVissionView() {

  if (ViewModel.CurrentSystemID() == 2) {
    return true;
  }
  return false;
};


function SelectedPHR() { return ProductionHelper.SelectedPHR() };
function SetSelectedPHR(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.SetSelectedPHR(ProductionHumanResource) };
function BackToPHRList() { ProductionHelper.ProductionHumanResources.Methods.BackToPHRList() };
function GetQualifiedHR(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.GetQualifiedHR(ProductionHumanResource) };
function FilterQualifiedHR(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.FilterQualifiedHR(ProductionHumanResource) };
function SetPrimaryHR(ROSkilledHR) { ProductionHelper.ProductionHumanResources.Methods.SetPrimaryHR(ROSkilledHR) }
function SetPreferredHR(ROSkilledHR) { ProductionHelper.ProductionHumanResources.Methods.SetPreferredHR(ROSkilledHR) };
function FilterPHR() { ProductionHelper.ProductionHumanResources.Methods.FilterPHR() };
function ReplaceResource() { ProductionHelper.ProductionHumanResources.Methods.ReplaceResource() }
function RemoveHumanResource(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.RemoveHumanResource(ProductionHumanResource) };
function DeleteProductionHumanResource(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.DeleteProductionHumanResource(ProductionHumanResource) };
function AddNewProductionHumanResource() { ProductionHelper.ProductionHumanResources.Methods.AddNewProductionHumanResource() }
function EditProductionHumanResource(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.EditProductionHumanResource(ProductionHumanResource) }
function ShowPHRDetail(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.ShowPHRDetail(ProductionHumanResource) }

function CrewLeaderClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.CrewLeaderClick(ProductionHumanResource) };
function ReliefCrewClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.ReliefCrewClick(ProductionHumanResource) };
function TBCClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.TBCClick(ProductionHumanResource) };
function TrainingClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.TrainingClick(ProductionHumanResource) };
function SuppliedHumanResourceClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.SuppliedHumanResourceClick(ProductionHumanResource) };
function FilteredProductionTypeClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.FilteredProductionTypeClick(ProductionHumanResource) };
function FilteredPositionClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.FilteredPositionClick(ProductionHumanResource) };
function IncludedOffClick(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Events.IncludedOffClick(ProductionHumanResource) };
function CrewLeaderCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.CrewLeaderCss(ProductionHumanResource) };
function ReliefCrewCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.ReliefCrewCss(ProductionHumanResource) };
function TBCCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.TBCCss(ProductionHumanResource) };
function TrainingCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.TrainingCss(ProductionHumanResource) };
function SuppliedHumanResourceCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.SuppliedHumanResourceCss(ProductionHumanResource) };
function FilteredProductionTypeCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.FilteredProductionTypeCss(ProductionHumanResource) };
function FilteredPositionCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.FilteredPositionCss(ProductionHumanResource) };
function IncludedOffCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.IncludedOffCss(ProductionHumanResource) };
function CrewLeaderHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.CrewLeaderHtml(ProductionHumanResource) };
function ReliefCrewHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.ReliefCrewHtml(ProductionHumanResource) };
function TBCHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.TBCHtml(ProductionHumanResource) };
function TrainingHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.TrainingHtml(ProductionHumanResource) };
function SuppliedHumanResourceHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.SuppliedHumanResourceHtml(ProductionHumanResource) };
function FilteredProductionTypeHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.FilteredProductionTypeHtml(ProductionHumanResource) };
function FilteredPositionHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.FilteredPositionHtml(ProductionHumanResource) };
function IncludedOffHtml(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.IncludedOffHtml(ProductionHumanResource) };
function PHRCss(ProductionHumanResource) { return ProductionHelper.ProductionHumanResources.UI.PHRCss(ProductionHumanResource) };
function ClearHumanResourceID(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.ClearHumanResourceID(ProductionHumanResource) }
function ClearPrefHumanResourceID(ProductionHumanResource) { ProductionHelper.ProductionHumanResources.Methods.ClearPrefHumanResourceID(ProductionHumanResource) }


//Schedules-----------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
function CurrentHRSchedule() { return ProductionHelper.Schedules.Properties.CurrentHRSchedule() }
function GetScheduleFromServer(HRProductionSchedule) { ProductionHelper.Schedules.Methods.GetScheduleFromServer(HRProductionSchedule) }
function BackFromCurrentSchedule() { ProductionHelper.Schedules.UI.BackFromCurrentSchedule() }
function SaveSchedule(HRProductionSchedule) { ProductionHelper.Schedules.Methods.SaveSchedule(HRProductionSchedule) }
function UpdateHRSchedule(HRProductionSchedule) { ProductionHelper.Schedules.Methods.UpdateSchedule(HRProductionSchedule) }
function GenerateSchedules() { ProductionHelper.Schedules.Methods.GenerateSchedules() }
function RemoveScheduleItem(HRProductionScheduleDetail) { ProductionHelper.Schedules.Methods.RemoveScheduleItem(HRProductionScheduleDetail) }
function SetupUnSelectedTimelineList(HRProductionSchedule) { return ProductionHelper.Schedules.Methods.SetupUnSelectedTimelineList(HRProductionSchedule) }
function GetUnSelectedTimelineList(HRProductionSchedule) { return ProductionHelper.Schedules.Methods.GetUnSelectedTimelineList(HRProductionSchedule) }
function AddItemFromTimeline(UnSelectedTimeline) { return ProductionHelper.Schedules.Methods.AddItemFromTimeline(UnSelectedTimeline) }
function ShowHRSchedule(HRProductionSchedule) { ProductionHelper.Schedules.Methods.ShowHRSchedule(HRProductionSchedule); }
function HRProductionScheduleCss(HRProductionSchedule) {
  var css = '';
  if (HRProductionSchedule.IsValid()) {
    css = "well well-xs HRScheduleSummarised"
  } else {
    css = "well well-xs HRScheduleSummarised"
  }
  //HRScheduleInvalid
  return css;
}

function FilterProductionTimelines(List, Item) {
  var Allowed = [];
  List.Iterate(function (CurrItem, Index) {
    var ItemToCheck = Item.AreaScheduleDetailClientSideList().Find('ProductionTimelineID', CurrItem.ProductionTimelineID);
    if (!ItemToCheck) {
      Allowed.push(CurrItem);
    }
  });
  return Allowed;
};
function SetAllowedProductionTimelines(AreaSchedule) {
  ViewModel.ROAllowedTimelines([]);
  ViewModel.ROProductionTimelineList().Iterate(function (CurrItem, Index) {
    CurrItem.Visible(false);
    var ItemToCheck = AreaSchedule.AreaScheduleDetailClientSideList().Find('ProductionTimelineID', CurrItem.ProductionTimelineID());
    if (!ItemToCheck) {
      CurrItem.Visible(true);
    }
  });
};
function SelectNewTimeline(AreaSchedule) {
  SetAllowedProductionTimelines(AreaSchedule);
  $('#SelectNewTimeline').modal();
};
function AddTimeline(ROProductionTimeline) {
  //console.log(ROProductionTimeline);
};

function HRLocalCss(HRProductionSchedule) { return ProductionHelper.Schedules.UI.HRLocalCss(HRProductionSchedule) }
function HRLocalHtml(HRProductionSchedule) { return ProductionHelper.Schedules.UI.HRLocalHtml(HRProductionSchedule) }
function DetailSelectClick(HRProductionScheduleDetail) { ProductionHelper.Schedules.Methods.DetailSelectClick(HRProductionScheduleDetail) }
function DetailSelectCss(HRProductionScheduleDetail) { return ProductionHelper.Schedules.UI.DetailSelectCss(HRProductionScheduleDetail) }
function DetailSelectHtml(HRProductionScheduleDetail) { return ProductionHelper.Schedules.UI.DetailSelectHtml(HRProductionScheduleDetail) }
function DeleteSelectedScheduleDetails(HRProductionSchedule) { ProductionHelper.Schedules.Methods.DeleteSelectedScheduleDetails(HRProductionSchedule) }
function NewTimelineSelectClick(UnSelectedTimeline) { ProductionHelper.Schedules.Methods.NewTimelineSelectClick(UnSelectedTimeline) }
function NewTimelineSelectCss(UnSelectedTimeline) { return ProductionHelper.Schedules.UI.NewTimelineSelectCss(UnSelectedTimeline) }
function NewTimelineSelectHtml(UnSelectedTimeline) { return ProductionHelper.Schedules.UI.NewTimelineSelectHtml(UnSelectedTimeline) }
function AddSelectedTimelines(HRProductionSchedule) { ProductionHelper.Schedules.Methods.AddSelectedTimelines(HRProductionSchedule) }
function ScheduleDetailCss(HRProductionScheduleDetail) { return ProductionHelper.Schedules.UI.ScheduleDetailCss(HRProductionScheduleDetail) }
function HideHRSchedule(HRProductionSchedule) { ProductionHelper.Schedules.Methods.HideHRSchedule(HRProductionSchedule); }

function SetupBulkUpdate() {
  Singular.SendCommand('SetupBulkUpdate', { Mode: "Update" }, function (response) {
    $('#BulkModal').modal();
  })
}
function CommitBulkUpdate() {
  Singular.SendCommand('CommitBulkUpdate', { Mode: "Update" }, function (response) {
    $('#BulkModal').modal('hide');
  })
}
function SetupBulkAdd() {
  Singular.SendCommand('SetupBulkAdd', { Mode: "Add" }, function (response) {
    $('#BulkModal').modal();
  })
}
function CommitBulkAdd() {
  Singular.SendCommand('CommitBulkAdd', { Mode: "Add" }, function (response) {
    $('#BulkModal').modal('hide');
  })
}
function SetupBulkDelete() {
  Singular.SendCommand('SetupBulkDelete', { Mode: "Delete" }, function (response) {
    $('#BulkModal').modal();
  })
}
function CommitBulkDelete() {
  Singular.SendCommand('CommitBulkDelete', { Mode: "Delete" }, function (response) {
    $('#BulkModal').modal('hide');
  })
}

function DisableUnSelectedTimelineSelect(UnSelectedTimeline) {
  if (UnSelectedTimeline.TimesheetMonthClosedInd() && ViewModel.SystemID() == 1) {
    return true;
  }
  else if (!ViewModel.CurrentProduction().CanEditCrewSchedulesAfterFinalised()) {
    return true;
  }
  else {
    return false;
  }
};

function DisableHRScheduleDetailSelect(HRProductionScheduleDetail) {
  if (ViewModel.CurrentProduction().SystemAreaReconciled() || ViewModel.CurrentProduction().SystemAreaCancelled()) {
    return true;
  }

  var CanEdit = CanEditScheduleDetail(HRProductionScheduleDetail, '', 'Select')

  if (CanEdit) {
    return false;
  }
  else {
    return true;
  }
};

function CanEditScheduleDetail(HRProductionScheduleDetail, Element, Field) {
  var HRProductionSchedule = HRProductionScheduleDetail.GetParent();
  var ProductionSystemArea = HRProductionSchedule.GetParent()
  var Production = ProductionSystemArea.GetParent();

  var HRContractTypeID = HRProductionSchedule.ContractTypeID();

  if (HRProductionScheduleDetail.TimesheetInd()) {
    if (Production.SystemAreaReconciled() || !Production.CanEditCrewSchedulesAfterFinalised()) {
      return false;
    } //Production.Reconciled()
    else {
      if (HRProductionSchedule.IsBusyClient()) { return false; }
      else if (Field != 'ScheduleDate') {
        if (HRContractTypeID == 6) {
          if (HRProductionScheduleDetail.TimesheetInd() && HRProductionScheduleDetail.InvoiceDetailInd()) { return false; }
          else { return true; }
        }
        else {
          if (!HRProductionScheduleDetail.TimesheetMonthClosedInd()) { return true; }
          else {
            if (ViewModel.SystemID() == 1) {
              return false;
            } else {
              return true;
            }
          }
        }
      }
      else {
        return false;
      }
    }
  } //HRProductionScheduleDetail.TimesheetInd()
  else {
    if (HRProductionSchedule.IsBusyClient() || !Production.CanEditCrewSchedulesAfterFinalised()) {
      return false;
    }
    if (Field != 'ScheduleDate') {
      return true;
    } else {
      return false;
    }
  }

  //  if (HRProductionScheduleDetail.CrewTimesheetID()) {
  //    if (Production.Reconciled()) {
  //      return false;
  //    } else {
  //      if (HRProductionSchedule.IsBusyClient()) {
  //        return false;
  //      }
  //      if (Field != 'ScheduleDate') {
  //        return true;
  //      } else {
  //        return false;
  //      }
  //    }

  //  } else {
  //    if (HRProductionSchedule.IsBusyClient()) {
  //      return false;
  //    }
  //    if (Field != 'ScheduleDate') {
  //      return true;
  //    } else {
  //      return false;
  //    }
  //  }
  //Function(d As OBLib.Productions.Schedules.HRProductionScheduleDetail) d.CrewTimesheetID Is Nothing
}

function ViewReadOnlySchedule(HRProductionSchedule) {
  ProductionHelper.Schedules.Methods.GetScheduleFromServer(HRProductionSchedule, function (ReturnedSchedule) {
    ReturnedSchedule.Expanded(true);
  })
}

function TimesheetIndCss(HRProductionScheduleDetail) {
  if (HRProductionScheduleDetail.TimesheetInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
}

function TimesheetIndHtml(HRProductionScheduleDetail) {
  if (HRProductionScheduleDetail.TimesheetInd()) { return ("<span class=\"glyphicon glyphicon-ok\"></span> " + HRProductionScheduleDetail.TimesheetMonth()) } else { return "<span class=\"glyphicon glyphicon-minus\"></span>" }
}

function InvoiceIndCss(HRProductionScheduleDetail) {
  if (HRProductionScheduleDetail.InvoiceDetailInd()) { return 'btn btn-xs btn-success' } else { return 'btn btn-xs btn-primary' }
}

function InvoiceIndHtml(HRProductionScheduleDetail) {
  if (HRProductionScheduleDetail.InvoiceDetailInd()) { return ("<span class=\"glyphicon glyphicon-ok\"></span> " + HRProductionScheduleDetail.InvoiceNo()) } else { return "<span class=\"glyphicon glyphicon-minus\"></span>" }
}


//Correspondence-----------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
function AddNewProductionDocument() { ProductionHelper.Documents.Methods.AddNewProductionDocument() }
function AddNewProductionGeneralComment() { ProductionHelper.GeneralComments.Methods.AddNewProductionGeneralComment() }
function AddNewProductionPettyCash() { ProductionHelper.PettyCash.Methods.AddNewProductionPettyCash() }


//Production Comment Section------------------------------------------------------------------------------------------
//    function CanAddProductionCommentSection() {
//      if (ViewModel.CurrentProduction().ProductionTypeID() && ViewModel.CurrentProduction().ProductionVenueID()) {
//        return true
//      } else {
//        return false
//      }
//    };

function AddNewProductionCommentSection() {
  var NewItem = ViewModel.CurrentProduction().ProductionCommentSectionList.AddNew();
  NewItem.ProductionID(ViewModel.CurrentProduction().ProductionID());
};

function CommentHeadFilter(List, Item) {
  var Allowed = [];

  for (var i = 0; i < List.length; i++) {
    if (Item.GetParent().ProductionCommentReportSectionID() == List[i].ProductionCommentReportSectionID) {
      Allowed.push({ ProductionCommentHeader: List[i].ProductionCommentHeader });
    }
  }
  return Allowed;
};

function AddNewPC(obj) {
  var NewItem = obj.ProductionCommentList.AddNew();
  NewItem.ProductionCommentHeader(obj.ProductionCommentHeader());
};

function RemovePC(obj) {
  var pc = obj;
  obj.GetParent().ProductionCommentList.Remove(pc)
};


//Travel
function AddNewTravelRequisition() {
  var NewItem = ViewModel.CurrentProduction().TravelRequisitionList.AddNew();
  NewItem.EventTitle(ViewModel.CurrentProduction().ProductionDescription());
  NewItem.VersionNo(1);
  NewItem.ProductionID(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionID());
  NewItem.SystemID(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].SystemID());
  NewItem.ProductionAreaID(ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionAreaID());
  NewItem.CrewTypeID(null);
  //NewItem.CreatedByUser(ViewModel.CurrentUserName());
  //var versionNo = ViewModel.CurrentProduction().TravelRequisitionList()
};

function EditSelectedTravelReq(obj) { ProductionHelper.Travel.Methods.EditSelectedTravelReq(obj) }

$(document).ready(function () {
  //ProductionHelper = new ProductionHelper();
  ProductionVM.Tabs.SetMainTab();
  ViewModel.PHRListVisible(false);
  ViewModel.SelectedHRVisible(false);
  ViewModel.SelectedPHRID(null);
  ViewModel.SelectedPHRGuid('');
  ViewModel.PHRFilter('');
  ViewModel.HRListVisible(false);
  ViewModel.HRTimelineSelectVisible(false);
  ViewModel.ROVehicleFilter('');
  ViewModel.SelectedHRScheduleGuid('');
  if (ViewModel.CurrentProduction()) {
    ProductionHelper.ProductionHumanResources.Methods.SortList(ViewModel.CurrentProduction().ProductionSystemAreaList()[0]);
  }
});


function CanViewSkillsModal() {
  if (ProductionHelper.SelectedPHR()) {
    return true;
  } else {
    return false;
  }
};

function GetHRCss(ProductionHumanResource) {
  var style = "btn-custom ProductionCrewHumanResource";
  if (ProductionHumanResource.HumanResourceID()) {
    if (ProductionHumanResource.Clash.length == 0) {
      style = style + " btn-primary";
    } else {
      style = style + " btn-danger";
    }
  } else {
    style = style + " btn-default";
  }
  return style;
}

function GetPrefHRCss(ProductionHumanResource) {
  if (ProductionHumanResource.PreferredHumanResourceID()) {
    return "btn-custom btn-warning ProductionCrewHumanResource";
  } else {
    return "btn-custom btn-default ProductionCrewHumanResource";
  }
}

function CanEditTravel(TravelRequisition) {
  if (TravelRequisition.IsNew() || TravelRequisition.IsDirty()) {
    return false;
  } else if (ViewModel.CurrentSystemID() == 2) {
    return true;
  }
  else if (!TravelRequisition.GetParent().CrewFinalised()) {
    return false
  } else {
    return true;
  }
}

function CanEditRequisitionSystemID(TravelRequisition) {
  return TravelRequisition.IsNew();
}

function CanEditRequisitionCrewTypeID(TravelRequisition) {
  return TravelRequisition.IsNew();
}

function SetupCopyCrew() {
  $("#CopyCrew").modal();
};

function FilterCopyCrewProductions() {
  window.setTimeout(function () { ViewModel.ROProductionListPagingManager().Refresh(); }, 300);
}

function CopyCrewMemberCycleState(ProductionCrew) {
  ProductionCrew.Selected(!ProductionCrew.Selected());
}

function CopyCrewMemberCss(ProductionCrew) {
  if (ProductionCrew.Selected()) {
    return "btn btn-xs btn-primary";
  } else {
    return "btn btn-xs btn-default";
  }
}

function CopyCrewMemberHtml(ProductionCrew) {
  if (ProductionCrew.Selected()) {
    return "<span class='glyphicon glyphicon-check'></span> Yes";
  } else {
    return "<span class='glyphicon glyphicon-minus'></span> No";
  }
}

function CopyCrewProductionRowClicked(ROProduction) {
  if (ROProduction.ProductionSystemAreaID()) {
    Singular.GetDataStateless("OBLib.Productions.Crew.ProductionCrewList, OBLib", { ProductionSystemAreaID: ROProduction.ProductionSystemAreaID() }, function (response) {
      ViewModel.CopyCrewProductionCrewList.Set([]);
      ViewModel.CopyCrewProductionCrewList.Set(response.Data);
    });
  }
};

function SubmitCopyCrew() {
  Singular.SendCommand("SubmitCopyCrew", {}, function (response) {
    $("#CopyCrew").modal('hide');
    ProductionHelper.Methods.SortList();
  });
};

//NO LONGER USED------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
function AddItems() {
  Singular.SendCommand("SelectTimeline", { Mode: "Add Items" }, function (d) {
    ViewModel.HRListVisible(false);
    ViewModel.HRTimelineSelectVisible(true);
  })
};

function UpdateItems() {
  //{ SendModel: false, TextArgument: Args }
  Singular.SendCommand("SelectTimeline", { Mode: "Update Items" }, function (d) {
    ViewModel.HRListVisible(false);
    ViewModel.HRTimelineSelectVisible(true);
  })
};

function DeleteItems() {
  Singular.SendCommand("SelectTimeline", { Mode: "Delete Items" }, function (d) {
    ViewModel.HRListVisible(false);
    ViewModel.HRTimelineSelectVisible(true);
  })
};

function UpdateFromHRTimelineSelect(HRTimelineSelect) {
  Singular.SendCommand('UpdateFromHRTimelineSelect', {}, function (d) {
    ViewModel.HRListVisible(true);
    ViewModel.HRTimelineSelectVisible(false);
  })
};

function BackFromHRTimelineSelect() {
  ViewModel.HRListVisible(true);
  ViewModel.HRTimelineSelectVisible(false);
};

function HRSelect(HRSelect) { HRSelect.SelectInd(!HRSelect.SelectInd()) }

function HRSelectCss(HRSelect) { if (HRSelect.SelectInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' } }

function HRSelectHtml(HRSelect) { if (HRSelect.SelectInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" } }

function TimelineSelect(TimelineSelect) { TimelineSelect.SelectInd(!TimelineSelect.SelectInd()) }

function TimelineSelectCss(TimelineSelect) { if (TimelineSelect.SelectInd()) { return 'btn btn-xs btn-primary' } else { return 'btn btn-xs btn-default' } }

function TimelineSelectHtml(TimelineSelect) { if (TimelineSelect.SelectInd()) { return "<span class='glyphicon glyphicon-check'></span> Yes" } else { return "<span class='glyphicon glyphicon-minus'></span> No" } }

function FilterHRSelect() {
  Singular.ShowLoadingBar();
  if (!ViewModel.HRSelectFilter() || ViewModel.HRSelectFilter() == '') {
    ViewModel.HRTimelineSelect().HRSelectList().Iterate(function (HRSelect, Index) {
      HRSelect.Visible(true);
    });
  } else {
    ViewModel.HRTimelineSelect().HRSelectList().Iterate(function (HRSelect, Index) {
      var FilterString = ViewModel.HRSelectFilter().toLowerCase();
      var HRMatchInd = (HRSelect.HumanResource().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
      var ClashMatchInd = (HRSelect.Clashes().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
      if (HRMatchInd || ClashMatchInd) {
        HRSelect.Visible(true);
      } else {
        HRSelect.Visible(false);
      }
    });
  };
  Singular.HideLoadingBar();
};

function FilterTimelineSelect() {
  Singular.ShowLoadingBar();
  if (!ViewModel.HRSelectFilter() || ViewModel.HRSelectFilter() == '') {
    ViewModel.HRTimelineSelect().TimelineSelectList().Iterate(function (TimelineSelect, Index) {
      TimelineSelect.Visible(true);
    });
  } else {
    ViewModel.HRTimelineSelect().TimelineSelectList().Iterate(function (TimelineSelect, Index) {
      var FilterString = ViewModel.TimelineSelectFilter().toLowerCase();
      var ProductionTimelineTypeMatchInd = (TimelineSelect.ProductionTimelineType().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
      if (ProductionTimelineTypeMatchInd) {
        TimelineSelect.Visible(true);
      } else {
        TimelineSelect.Visible(false);
      }
    });
  };
  Singular.HideLoadingBar();
};

function FilterHRList() {
  Singular.ShowLoadingBar();
  if (!ViewModel.HRListFilter() || ViewModel.HRListFilter() == '') {
    ViewModel.CurrentProduction().ProductionSystemAreaList()[0].HRProductionScheduleList().Iterate(function (HR, Index) {
      HR.Visible(true);
    });
  } else {
    ViewModel.CurrentProduction().ProductionSystemAreaList()[0].HRProductionScheduleList().Iterate(function (HR, Index) {
      var FilterString = ViewModel.HRListFilter().toLowerCase();
      var HumanResourceMatchInd = (HR.HumanResource().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
      var CityCodeMatchInd = (HR.CityCode().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
      var AllClashesMatchInd = (HR.AllClashes().toLowerCase().indexOf(FilterString) >= 0 ? true : false);
      if (HumanResourceMatchInd || CityCodeMatchInd || AllClashesMatchInd) {
        HR.Visible(true);
      } else {
        HR.Visible(false);
      }
    });
  };
  Singular.HideLoadingBar();
};

function ViewProductionPlanners() {
  $('#ProductionPlanners').modal();
};

function FindROHumanResource(ProductionHRPlanner) {
  if (ProductionHRPlanner != undefined) {
    ViewModel.ProductionHRPlannerGUID(ProductionHRPlanner.Guid())
  }
  ViewModel.ROHumanResourceList.Set([]);
  ViewModel.ROHumanResourceFilter("");
  $('#SelectROHumanResource').modal();
};

function FilterROHumanResource() {
  if (ViewModel.ROHumanResourceFilter().length >= 3) {
    Singular.ShowLoadingBar();
    Singular.GetDataStateless('OBLib.HR.ReadOnly.ROHumanResourceList, OBLib', {
      FilterName: ViewModel.ROHumanResourceFilter(),
      SystemID: ViewModel.CurrentSystemID(),
      ProductionAreaID: ViewModel.CurrentProductionAreaID()
    }, function (args) {
      if (args.Success) {
        KOFormatter.Deserialise(args.Data, ViewModel.ROHumanResourceList);
        Singular.HideLoadingBar();
        $('#SelectROHumanResource').modal();
      }
    });
  }
};

function SetManagerHumanResourceID(ROHumanResource) {
  ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHRPlannerList().Iterate(function (Item, Index) {
    if (Item.Guid() == ViewModel.ProductionHRPlannerGUID()) {
      Item.HumanResourceID(ROHumanResource.HumanResourceID());
      Item.HumanResource(ROHumanResource.Firstname() + ' ' + ROHumanResource.Surname());
    }
  });
  $('#SelectROHumanResource').modal('hide');
};

function GetPlannerHRCss(obj) {
  if (obj.HumanResourceID()) {
    return "btn-custom btn-primary ProductionCrewHumanResource";
  } else {
    return "btn-custom btn-default ProductionCrewHumanResource";
  }
};

function ClearHumanResourceID(obj) {
  Singular.SendCommand("ClearHumanResourceID", { guid: obj.Guid() }, function (response) { });
};

function GetProductionHRPlannerBrokenRulesHTML() {
  var ErrorString = '';
  var ProdHRPlannerList = ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHRPlannerList()
  //if (ViewModel.CurrentProduction()) {
  ProdHRPlannerList.Iterate(function (Item, Index) {
    Singular.Validation.CheckRules(Item);
    if (Item.IsValid() == false) {
      ErrorString = ErrorString + Singular.Validation.GetBrokenRulesHTML(Item);
    }
  });
  //}
  return ErrorString;
};

function IsProductionHRPlannerValid() {
  var valid = true;
  var ProdHRPlannerList = ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionHRPlannerList()

  ProdHRPlannerList.Iterate(function (Item, Index) {
    Singular.Validation.CheckRules(Item);
    if (Item.IsValid() == false) {
      valid = false
    }
  });
  return valid;
};

function CanEditTimeline(ProductionTimeline) {
  if (ProductionTimeline.SharedTimeline()) {
    if (ProductionTimeline.ProductionTimelineTypeID() == 23 || ProductionTimeline.ProductionTimelineTypeID() == 11) {
      return ViewModel.CanEditBuildUpForAllAreas();
    } else {
      return false;
    }
  };
  if (!ProductionTimeline.ProductionTimelineTypeID()) {
    return false;
  };
  if (ProductionTimeline.ProductionTimelineTypeID() == 41 && !(ProductionTimeline.IsNew() || ProductionTimeline.IsClientNew())) {
    return false;
  };
  return true;
};

function CanDeleteTimeline() {
  return true;
};

function CanEditSchedule(HRProductionSchedule) {
  if (ViewModel.CurrentSystemID() != 1) {
    return true;
  }
  if (ViewModel.CurrentSystemID() == 1) {
    if (ViewModel.CurrentProduction().IsTimelineReady()) {
      //if (!ViewModel.CurrentProduction().IsDirty()) {
      return true;
      //}
    } else if (!ViewModel.CurrentProduction().IsTimelineReady()) {
      if (HRProductionSchedule.CoreCrewDisciplineInd()) {
        return true;
      }
    }
  }
  // && !ViewModel.CurrentProduction().CanFinaliseTimeline()
  return false

}

function CanEditPHRs(phr) {
  if (ViewModel.CurrentSystemID() != 1) {
    return true;
  }
  if (ViewModel.CurrentSystemID() == 1) {
    if (ViewModel.CurrentProduction().IsTimelineReady()) {
      //if (!ViewModel.CurrentProduction().IsDirty()) {
      return true;
      //}
    } else if (!ViewModel.CurrentProduction().IsTimelineReady()) {
      var coreDisc = [1, 2, 5, 6, 7, 11]
      var indx = coreDisc.indexOf(phr.DisciplineID());
      if (indx >= 0) {
        return true
      }
    }
  }
  return false

}

DocumentTypeIDSet = function (ProductionDocument) {
  var Production = ProductionDocument.GetParent();
  if (Production) {
    if ((ProductionDocument.DocumentTypeID() == 16 || ProductionDocument.DocumentTypeID() == 6)) {
      ProductionHelper.Production.Rules.CheckCanReconcile(Production)
    }
  }
}
