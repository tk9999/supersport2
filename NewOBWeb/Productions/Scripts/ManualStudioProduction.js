﻿ManualStudioProduction = {
  TitleValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.Title().trim().length == 0) {
      CtlError.AddError("Title is required")
    }
  },
  TeamsPlayingValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.TeamsPlaying().trim().length == 0) {
      CtlError.AddError("Teams Playing is required")
    }
  },
  ProductionTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionTypeID()) { CtlError.AddError("Genre is required") };
  },
  ProductionTypeValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.ProductionType().trim().length == 0) { CtlError.AddError("Genre is required") };
  },
  EventTypeIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.EvenTypeID()) { CtlError.AddError("Series is required") };
  },
  EventTypeValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.EventType().trim().length == 0) { CtlError.AddError("Series is required") };
  },
  ProductionVenueIDValid: function (Value, Rule, CtlError) {
    if (!CtlError.Object.ProductionVenueID()) { CtlError.AddError("Production Venue is required") };
  },
  ProductionVenueValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.ProductionVenue().trim().length == 0) { CtlError.AddError("Production Venue is required") };
  },
  LiveStartTimeValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.LiveStartDateTime();
    var leti = CtlError.Object.LiveEndDateTime();
    if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live Start Time must be before Live End Time");
      }
    } else {
      if (!lst && CtlError.Object.ProductionAreaID() == 1) {
        CtlError.AddError("Live Start Time required");
      }
    }
  },
  LiveEndTimeValid: function (Value, Rule, CtlError) {
    var lst = CtlError.Object.LiveStartDateTime();
    var leti = CtlError.Object.LiveEndDateTime();
    if (lst && leti) {
      if (OBMisc.Dates.IsAfter(lst, leti)) {
        CtlError.AddError("Live End Time must be after Live Start Time");
      }
    } else {
      if (!leti && CtlError.Object.ProductionAreaID() == 1) {
        CtlError.AddError("Live End Time required");
      }
    }
  },
  SpecValid: function (Value, Rule, CtlError) {
    if (CtlError.Object.Spec().trim().length == 0) { CtlError.AddError("Spec is required") };
  },
  VehicleValid: function (Value, Rule, CtlError) {
    //if (CtlError.Object.Vehicle().trim().length == 0) { CtlError.AddError("Vehicle is required") };
  },

  CallTimeValid: function (Value, Rule, CtlError) {
    var ct = CtlError.Object.CallTime();
    var rsst = CtlError.Object.OnAirStartTime();
    if (ct && rsst) {
      if (OBMisc.Dates.IsAfter(ct, rsst)) {
        CtlError.AddError("Call Time must be before On Air Start Time");
      }
    }
  },
  OnAirStartTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.OnAirStartTime();
    var oaet = CtlError.Object.OnAirEndTime();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      }
    } else {
      if (!oast) {
        CtlError.AddError("On Air Start Time is required");
      }
    }
  },
  OnAirEndTimeValid: function (Value, Rule, CtlError) {
    var oast = CtlError.Object.OnAirStartTime();
    var oaet = CtlError.Object.OnAirEndTime();
    if (oast && oaet) {
      if (OBMisc.Dates.IsAfter(oast, oaet)) {
        CtlError.AddError("On Air Start Time must be before On Air End Time");
      }
    } else {
      if (!oaet) {
        CtlError.AddError("On Air End Time is required");
      }
    }
  },
  WrapTimeValid: function (Value, Rule, CtlError) {
    var oaet = CtlError.Object.OnAirEndTime();
    var wrt = CtlError.Object.WrapTime();
    if (oaet && wrt) {
      if (OBMisc.Dates.IsAfter(oaet, wrt)) {
        CtlError.AddError("On Air End Time must be before Wrap End Time");
      }
    }
  },
  RoomIDValid: function (Value, Rule, CtlError) {
    var ManualStudioProduction = CtlError.Object;
    if (ManualStudioProduction.ProductionAreaID() == 2) {
      if (!ManualStudioProduction.RoomID()) {
        CtlError.AddError('Room is required');
      } else {
        if (ManualStudioProduction.OnAirStartTime() && ManualStudioProduction.OnAirEndTime()) {
          alert("check room available");
          //var f = new KOFormatterObject();
          //ViewModel.CallServerMethod('CheckRoomAvailable', { ImportedEvent: f.Serialise(ImportedEvent) }, function (WebResult) {
          //  CtlError.AddError(WebResult.Data);
          //});
        } else {
          CtlError.AddError('On Air Start and End times are required');
        }
      }
    }
  },
  RoomIDSet: function (self) {
    if (self.RoomID() && self.OnAirStartTime() && self.OnAirEndTime()) {
      ManualStudioProduction.GetCallTimesForRoom(self);
      ManualStudioProduction.GetProductionSpecForRoom(self);
    } else {
      self.CallTime(null);
      self.WrapTime(null);
    }
  },
  OnAirStartTimeSet: function (self) {
    if (self.RoomID() && self.OnAirStartTime() && self.OnAirEndTime()) {
      ManualStudioProduction.GetCallTimesForRoom(self);
      ManualStudioProduction.GetProductionSpecForRoom(self);
    } else {
      self.CallTime(null);
      self.WrapTime(null);
    }
  },
  OnAirEndTimeSet: function (self) {
    if (self.RoomID() && self.OnAirStartTime() && self.OnAirEndTime()) {
      ManualStudioProduction.GetCallTimesForRoom(self);
      ManualStudioProduction.GetProductionSpecForRoom(self);
    } else {
      self.CallTime(null);
      self.WrapTime(null);
    }
  },
  CanSelectRoom: function (ManualStudioProduction) {
    var oast = ManualStudioProduction.OnAirStartTime();
    var oaet = ManualStudioProduction.OnAirEndTime();
    if (oast && oaet) {
      return true;
    } else {
      return false;
    }
  },
  CanEditCallTime: function (ManualStudioProduction) {
    var oast = ManualStudioProduction.OnAirStartTime();
    var oaet = ManualStudioProduction.OnAirEndTime();
    if (oast && oaet && ManualStudioProduction.RoomID() && ManualStudioProduction.ProductionSpecRequirementID()) {
      return true;
    } else {
      return false;
    }
  },
  CanEditWrapTime: function (ManualStudioProduction) {
    var oast = ManualStudioProduction.OnAirStartTime();
    var oaet = ManualStudioProduction.OnAirEndTime();
    if (oast && oaet && ManualStudioProduction.RoomID() && ManualStudioProduction.ProductionSpecRequirementID()) {
      return true;
    } else {
      return false;
    }
  },
  GetCallTimesForRoom: function (ManualStudioProduction) {
    ViewModel.CallServerMethod('GetCalculatedTimesForRoom', {
      DefaultRoomID: ManualStudioProduction.RoomID(),
      DefaultOnAirStartDateTime: ManualStudioProduction.OnAirStartTime(),
      DefaultOnAirEndDateTime: ManualStudioProduction.OnAirEndTime()
    }, function (args) {
      if (args.Data) {
        ManualStudioProduction.CallTime(new Date(args.Data.CallTimeStart));
        ManualStudioProduction.WrapTime(new Date(args.Data.WrapTimeEnd));
      }
    });
  },
  GetProductionSpecForRoom: function (ManualStudioProduction) {
    ViewModel.CallServerMethod('GetProductionSpecRequirementForRoom', {
      RoomID: ManualStudioProduction.RoomID(),
      OnAirStartTime: ManualStudioProduction.OnAirStartTime()
    }, function (args) {
      if (args.Data) {
        ManualStudioProduction.ProductionSpecRequirementID(args.Data);
      }
    });
  },

  ProductionTypeClicked: function () {
    ViewModel.ProductionTypePagingInfo().Refresh();
    ViewModel.SelectProductionType(true);
    //ViewModel.SelectEventType(false);
    ViewModel.SelectProductionVenue(false);
  },
  ProductionTypeFilterChanged: function () {
    ViewModel.ProductionTypePagingInfo().Refresh();
  },
  ProductionTypeSelected: function (ManualStudioProduction, ROProductionType, Element) {
    ManualStudioProduction.ProductionTypeID(ROProductionType.ProductionTypeID());
    ManualStudioProduction.ProductionType(ROProductionType.ProductionType());
    ViewModel.EventTypeListCriteria().ProductionTypeID(ROProductionType.ProductionTypeID());
    ManualStudioProduction.EventTypeID(null);
    ManualStudioProduction.EventType("");
  },
  EventTypeClicked: function (ManualStudioProduction) {
    ViewModel.EventTypeListCriteria().ProductionTypeID(ManualStudioProduction.ProductionTypeID());
    ViewModel.EventTypePagingInfo().Refresh();
    //ViewModel.SelectProductionType(false);
    //ViewModel.SelectEventType(true);
    //ViewModel.SelectProductionVenue(false);
  },
  EventTypeFilterChanged: function () {
    ViewModel.EventTypePagingInfo().Refresh();
  },
  EventTypeSelected: function (ManualStudioProduction, ROEventType, Element) {
    ManualStudioProduction.EventTypeID(ROEventType.EventTypeID());
    ManualStudioProduction.EventType(ROEventType.EventType());
  },
  ProductionVenueClicked: function () {
    ViewModel.ProductionVenuePagingInfo().Refresh();
    //ViewModel.SelectProductionType(false);
    //ViewModel.SelectEventType(false);
    //ViewModel.SelectProductionVenue(true);
  },
  ProductionVenueFilterChanged: function () {
    ViewModel.ProductionVenuePagingInfo().Refresh();
  },
  ProductionVenueSelected: function (ManualStudioProduction, ROProductionVenue, Element) {
    ManualStudioProduction.ProductionVenueID(ROProductionVenue.ProductionVenueID());
    ManualStudioProduction.ProductionVenue(ROProductionVenue.ProductionVenue());
  },

}