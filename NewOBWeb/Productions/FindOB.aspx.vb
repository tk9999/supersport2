﻿Imports OBLib.Productions.ReadOnly
Imports Singular.DataAnnotations

Public Class FindOB
  Inherits OBPageBase(Of FindOBVM)

End Class

Public Class FindOBVM
  Inherits OBViewModelStateless(Of FindOBVM)

  <InitialDataOnly()>
  Public Property ROProductionList As ROProductionList

  Public Property ProductionServicesInd As Boolean = False
  Public Property ProductionContentInd As Boolean = False

  Public Property OutsideBroadcastInd As Boolean = False
  Public Property StudioInd As Boolean = False

  Public Property OBVan As Boolean = False
  Public Property Externalvan As Boolean = False

  Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
  Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of FindOBVM)

  Public Property ProductionDocumentList As OBLib.Productions.Correspondence.ProductionDocumentList

  Public ReadOnly Property CurrentSystemID() As Integer
    Get
      Return OBLib.Security.Settings.CurrentUser.SystemID
    End Get
  End Property

  Protected Overrides Sub Setup()
    MyBase.PreSetup()

    ROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
    ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
    ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of FindOBVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "TxStartDateTime", 20)
    ROProductionListCriteria.TxDateFrom = Now.Date.AddDays(-7)
    ROProductionListCriteria.TxDateTo = Now.Date.AddMonths(1)
    ROProductionListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
    ROProductionListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    ROProductionListCriteria.PageNo = 1
    ROProductionListCriteria.PageSize = 20
    ROProductionListCriteria.SortAsc = True
    ROProductionListCriteria.SortColumn = "TxStartDateTime"

    If OBLib.Security.Settings.CurrentUser.IsEventManager Then
      ROProductionListCriteria.TxDateFrom = Singular.Dates.DateMonthStart(Now).AddDays(-7)
      ROProductionListCriteria.TxDateTo = Singular.Dates.DateMonthEnd(Now.AddMonths(1))
      ROProductionListCriteria.EventManagerID = OBLib.Security.Settings.CurrentUser.HumanResourceID
      ROProductionListCriteria.EventManager = OBLib.Security.Settings.CurrentUser.LoginName
      ROProductionList = OBLib.Productions.[ReadOnly].ROProductionList.GetROProductionList(ROProductionListCriteria.ProductionTypeID, ROProductionListCriteria.EventTypeID, ROProductionListCriteria.ProductionVenueID,
                                                                                            Nothing, Nothing, Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID,
                                                                                            ROProductionListCriteria.TxDateFrom, ROProductionListCriteria.TxDateTo, ROProductionListCriteria.EventManagerID,
                                                                                            ROProductionListCriteria.ProductionRefNo, Nothing, OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                                            ROProductionListCriteria.PageNo, ROProductionListCriteria.PageSize,
                                                                                            ROProductionListCriteria.SortAsc, ROProductionListCriteria.SortColumn)
    End If

    ClientDataProvider.AddDataSource("RODocumentTypeList", OBLib.CommonData.Lists.RODocumentTypeList, False)

  End Sub

  'Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
  '  MyBase.HandleCommand(Command, CommandArgs)

  '  Select Case Command
  '    Case "FetchDocumentList"
  '      Dim ProductionID As Integer? = CommandArgs.ClientArgs.ProductionID
  '      ProductionDocumentList = OBLib.Productions.Correspondence.ProductionDocumentList.GetProductionDocumentList(ProductionID, "Productions", ROProductionListCriteria.SystemID, ROProductionListCriteria.ProductionAreaID)
  '  End Select

  'End Sub

  <Singular.Web.WebCallable(LoggedInOnly:=True)>
  Private Shared Function SaveDocuments(ProductionDocumentList As OBLib.Productions.Correspondence.ProductionDocumentList) As Singular.Web.Result
    Dim sh As Singular.SaveHelper = ProductionDocumentList.TrySave()
    If sh.Success Then
      Return New Singular.Web.Result(True) With {.Data = ProductionDocumentList}
    Else
      Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
    End If
  End Function

End Class
