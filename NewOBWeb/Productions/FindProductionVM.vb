﻿Imports OBLib.Productions
Imports OBLib.Productions.Old
Imports OBLib.Productions.ReadOnly
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports Singular
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old
Imports System

Namespace Productions

  Public Class FindProductionVM
    Inherits OBViewModel(Of FindProductionVM)

#Region " Properties "

    Private mROProductionList As ROProductionList
    Public ReadOnly Property ROProductionList As ROProductionList
      Get
        Return mROProductionList
      End Get
    End Property
    '    <ClientOnly(ClientOnly.DataOptionType.SendDataToClient)>

    Public Property CurrentROProduction As OBLib.Productions.ReadOnly.ROProduction

    Public Property ROProductionListCriteria As OBLib.Productions.ReadOnly.ROProductionList.Criteria
    Public Property ROProductionListPagingManager As Singular.Web.Data.PagedDataManager(Of FindProductionVM)

    'Search Criteria
    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), DropDownType:=DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), DropDownType:=DataAnnotations.DropDownWeb.SelectType.Combo, ThisFilterMember:="ProductionTypeID")>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionVenueOldList), DropDownType:=DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared EventManagerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventManagerID, "Event Manager", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Event Manager", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROEventManagerList), DisplayMember:="PreferredFirstSurname", ValueMember:="HumanResourceID", DropDownType:=DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property EventManagerID() As Integer?
      Get
        Return GetProperty(EventManagerIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventManagerIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets and sets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref No", Description:="")>
    Public Property ProductionRefNo() As Integer?
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionRefNoProperty, Value)
      End Set
    End Property

    Public Shared TxDateFromProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDateFrom, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public Property TxDateFrom As DateTime?
      Get
        Return GetProperty(TxDateFromProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TxDateFromProperty, Value)
      End Set
    End Property

    Public Shared TxDateToProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDateTo, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property TxDateTo As DateTime?
      Get
        Return GetProperty(TxDateToProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TxDateToProperty, Value)
      End Set
    End Property



#End Region

#Region " Overrides "

    Protected Overrides Sub Setup()
      MyBase.PreSetup()

      mROProductionList = New OBLib.Productions.ReadOnly.ROProductionList
      ROProductionListCriteria = New OBLib.Productions.ReadOnly.ROProductionList.Criteria
      ROProductionListPagingManager = New Singular.Web.Data.PagedDataManager(Of FindProductionVM)(Function(d) Me.ROProductionList, Function(d) Me.ROProductionListCriteria, "TxStartDateTime", 50)
      'ROProductionListCriteria.PlayStartDateTime = Now.Date.AddDays(-62)
      'ROProductionListCriteria.PlayEndDateTime = Now.Date.AddDays(62)
      ROProductionListCriteria.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
      ROProductionListCriteria.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
      ROProductionListCriteria.PageNo = 1
      ROProductionListCriteria.PageSize = 10
      ROProductionListCriteria.SortAsc = True
      ROProductionListCriteria.SortColumn = "TxStartDateTime"

      If OBLib.Security.Settings.CurrentUser.IsEventManager Then
        TxDateFrom = Singular.Dates.DateMonthStart(Now).AddDays(-7)
        TxDateTo = Singular.Dates.DateMonthEnd(Now.AddMonths(1))
        EventManagerID = OBLib.Security.Settings.CurrentUser.HumanResourceID
        mROProductionList = [ReadOnly].ROProductionList.GetROProductionList(ProductionTypeID, EventTypeID, ProductionVenueID, Nothing, Nothing, Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID, _
                                                                            TxDateFrom, TxDateTo, EventManagerID, ProductionRefNo, Nothing, OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                            ROProductionListCriteria.PageNo, ROProductionListCriteria.PageSize, ROProductionListCriteria.SortAsc, ROProductionListCriteria.SortColumn)
      End If

    End Sub

    Protected Overrides Sub HandleCommand(Command As String, CommandArgs As Singular.Web.CommandArgs)
      MyBase.HandleCommand(Command, CommandArgs)

      Select Case Command
        Case "Find"
          mROProductionList = [ReadOnly].ROProductionList.GetROProductionList(ProductionTypeID, EventTypeID, ProductionVenueID, Nothing, Nothing, Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID, _
                                                                              TxDateFrom, TxDateTo, EventManagerID, ProductionRefNo, Nothing, OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                              ROProductionListCriteria.PageNo, ROProductionListCriteria.PageSize, ROProductionListCriteria.SortAsc, ROProductionListCriteria.SortColumn)
        Case "EditProduction"
          If CommandArgs.ClientArgs.ProductionID IsNot Nothing Then
            If IsNumeric(CommandArgs.ClientArgs.ProductionID) Then
              Dim ProductionID As Integer = CommandArgs.ClientArgs.ProductionID
              CreateProductionChildLists(ProductionID)
              'OBLib.Security.Settings.CurrentUser.ProductionAreaID
              Dim pl As OBLib.Productions.Old.ProductionList = OBLib.Productions.Old.ProductionList.GetProductionList(ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, 1) '1 = OB
              If pl.Count = 1 Then
                SessionData.CurrentProduction = pl(0)
              End If
              CommandArgs.ReturnData = VirtualPathUtility.ToAbsolute("~/Productions/Production.aspx")
            End If
          End If
      End Select

    End Sub

#End Region

#Region " Methods "

    Private Sub CreateProductionChildLists(ProductionID As Integer)

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdCreateProductionChildLists",
                  New String() {
                               "@ProductionID",
                               "@SystemID",
                               "@UserID",
                               "@ProductionAreaID"
                               },
                  New Object() {
                               ProductionID,
                               OBLib.Security.Settings.CurrentUser.SystemID,
                               OBLib.Security.Settings.CurrentUser.UserID,
                               OBLib.Security.Settings.CurrentUser.ProductionAreaID
                               })
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd.CommandTimeout = 0
      cmd = cmd.Execute()

    End Sub

#End Region

  End Class

End Namespace
