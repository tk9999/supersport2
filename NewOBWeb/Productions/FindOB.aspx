﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.Master" CodeBehind="FindOB.aspx.vb" Inherits="NewOBWeb.FindOB" %>
<%@ Import Namespace="OBLib.Maintenance.Productions.ReadOnly" %>
<%@ Import Namespace="Singular.Web" %>
<%@ Import Namespace="OBLib.Productions.ReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <style type="text/css">
    .CrewFinalised {
      background-color: #5cb85c !important;
      color: white;
    }

    .Reconciled {
      background-color: #eea236 !important;
    }

    .Cancelled {
      background-color: #d2322d !important;
    }

    tr.CrewFinalised td {
      background-color: #5cb85c !important;
      color: white;
    }

    tr.Reconciled td {
      background-color: #eea236 !important;
      color: white;
    }

    tr.Cancelled td {
      background-color: #d2322d !important;
      color: white;
    }

    tr.VisionView td {
      background-color: #BFF1F1 !important;
    }

    .GraphicsSuppliers {
      text-overflow: ellipsis;
    }

    #ProductionDocuments {
      display: none;
    }

    td.avatar {
      text-align: center;
    }

    table.white {
      background: #fff;
    }

    /*#pcont {
      width: calc(100% - 95px);
    }

    .table-responsive > .table-bordered {
      border: solid 1px #ddd;
    }*/

    img.editor-image {
      width: 30px;
      height: 30px;
    }

    .modal-background-gray {
      background-color: #f1f1f1 !important;
    }
  </style>
  <script type="text/javascript" src="../Scripts/Pages/FindOB.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/ProductionsOld.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/HumanResources.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
  <script type="text/javascript" src="../Scripts/BusinessObjects/Resources.js?v="<%= OBLib.OBMisc.VersionNo %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <% Using h = Helpers

      h.HTML.Heading1("Find Production")

      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.FlatBlock("Criteria", False, True, , , )
            With .ContentTag
              With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionList.Criteria)("ViewModel.ROProductionListCriteria()")
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.InputSize.ExtraSmall, , "Ref Num")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 6, 3, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.TxDateFrom, Singular.Web.BootstrapEnums.InputSize.ExtraSmall, , "Tx Start Date")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 6, 3, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.TxDateTo, Singular.Web.BootstrapEnums.InputSize.ExtraSmall, , "Tx End Date")

                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.EventManagerID, Singular.Web.BootstrapEnums.InputSize.ExtraSmall, , "Event Manager")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Production Type")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.EventTypeID, Singular.Web.BootstrapEnums.InputSize.Small, , "Event Type")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionVenueID, Singular.Web.BootstrapEnums.InputSize.Small, , "Venue")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 6, 6, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, Singular.Web.BootstrapEnums.InputSize.Small, , "Keyword")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
                    With .Helpers.Div
                      With .Helpers.Bootstrap.ButtonGroup
                        If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                          .Helpers.Bootstrap.StateButtonNew(Function(d) d.ProductionServicesInd,
                                                            "Production Services", "Production Services", , , , "fa-hand-o-up", )
                        End If
                        If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                          .Helpers.Bootstrap.StateButtonNew(Function(d) d.ProductionContentInd,
                                                            "Production Content", "Production Content", , , , "fa-hand-o-up", )
                        End If
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    .Helpers.Bootstrap.LabelDisplay("Areas")
                    With .Helpers.Div
                      With .Helpers.Bootstrap.ButtonGroup
                        If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                          .Helpers.Bootstrap.StateButtonNew(Function(d) d.OutsideBroadcastInd,
                                                            "OB", "OB", , , , , )
                        End If
                        If Singular.Security.HasAccess("Production Area", "Studios") Then
                          .Helpers.Bootstrap.StateButtonNew(Function(d) d.StudioInd,
                                                            "Studio", "Studio", , , , "fa-hand-o-up", )
                        End If
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    .Helpers.Bootstrap.LabelDisplay("Vans")
                    With .Helpers.Div
                      With .Helpers.Bootstrap.ButtonGroup
                        .Helpers.Bootstrap.StateButtonNew(Function(d) d.OBVan,
                                                          "OB", "OB", , , , "fa-hand-o-up", )
                        .AddBinding(Singular.Web.KnockoutBindingString.click, "UpdateCriteria()")
                        .Helpers.Bootstrap.StateButtonNew(Function(d) d.Externalvan,
                                                          "External", "External", , , , "fa-hand-o-up", )
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-refresh", , Singular.Web.PostBackType.None, "Refresh()")
                        .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(c) ViewModel.IsValid)
                      End With
                      With .Helpers.Bootstrap.Button(, "Clear", Singular.Web.BootstrapEnums.Style.Warning, , Singular.Web.BootstrapEnums.ButtonSize.Small, , "fa-eraser", , Singular.Web.PostBackType.None, "ClearCriteria()")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With


      With h.Bootstrap.Row
        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(vm) ViewModel.ROProductionListPagingManager,
                                                            Function(vm) ViewModel.ROProductionList,
                                                            False, False, False, True, True, True, True,
                                                                                    , Singular.Web.BootstrapEnums.PagerPosition.Bottom)
            '.Pager.PagerListTag.ListTag.AddClass("pull-left")
            .AddClass("white")
            With .FirstRow
              .AddClass("items")
              .AddBinding(Singular.Web.KnockoutBindingString.css, "GetProductionStatColour($data)")
              With .AddColumn("")
                .AddClass("col-tiny avatar")
                .AddBinding(Singular.Web.KnockoutBindingString.title, "ROProductionBO.getTitle($data)")
                With .Helpers.HTMLTag("img")
                  .AddBinding(Singular.Web.KnockoutBindingString.src, "ROProductionBO.editorImagePath($data)")
                  .AddBinding(Singular.Web.KnockoutBindingString.visible, "!ROProductionBO.canEditProduction($data)")
                  .AddBinding(Singular.Web.KnockoutBindingString.title, "ROProductionBO.getTitle($data)")
                  .AddBinding(Singular.Web.KnockoutBindingString.click, "ROProductionBO.openProduction($data)")
                  .AddClass("editor-image")
                End With
                With .Helpers.Bootstrap.Anchor(, , , , Singular.Web.LinkTargetType._blank, "fa-pencil", )
                  .AnchorTag.AddClass("btn btn-sm btn-primary")
                  .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.click, "ROProductionBO.openProduction($data)")
                  .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.enable, "ROProductionBO.canEditProduction($data)")
                  .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.visible, "ROProductionBO.canEditProduction($data)")
                End With
              End With
              With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                .AddClass("col-tiny")
              End With
              With .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.FullProductionDescription()")
              End With
              With .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionVenue)
                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.FullVenueName()")
              End With
              With .AddReadOnlyColumn(Function(c) c.City)
              End With
              With .AddReadOnlyColumn(Function(c) c.VehicleName)
                .FieldDisplay.AddBinding(Singular.Web.KnockoutBindingString.title, "$data.FullVehicleName()")
              End With
              With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartsString)
              End With
              With .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndsString)
              End With
              With .AddReadOnlyColumn(Function(c) c.EventManager)
              End With
              With .AddColumn("")
                With .Helpers.HTMLTag("span")
                  .AddBinding(KnockoutBindingString.css, "ROProductionBO.CreatedTypeCss($data,$element)")
                  .AddBinding(KnockoutBindingString.html, "ROProductionBO.CreatedTypeHTML($data,$element)")
                End With
                With .Helpers.HTMLTag("span")
                  .AddClass("btn btn-xs btn-primary")
                  .Helpers.HTML("Placeholder")
                  .AddBinding(KnockoutBindingString.visible, "$data.PlaceHolderInd()")
                End With
              End With
              If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
                With .AddReadOnlyColumn(Function(c) c.GraphicSuppliers)
                End With
              End If
              If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer) Then
                With .AddColumn("")
                  With .Helpers.Bootstrap.Anchor(, , , "Post Prod Rpt", Singular.Web.LinkTargetType._blank, "fa-pencil", )
                    .AnchorTag.AddClass("btn btn-xs btn-primary")
                    .AnchorTag.AddBinding(Singular.Web.KnockoutBindingString.href, "GetPostProdRptHref($data)")
                  End With
                End With
              End If
              'With .AddReadOnlyColumn(Function(c) c.CreatedByUser)
              'End With
              'With .AddReadOnlyColumn(Function(c) c.CreatedDateString)
              'End With
              If Singular.Security.HasAccess("Productions", "Add Invoices From Find Production Screen") Then
                With .AddColumn("")
                  With .Helpers.Bootstrap.Button(, "Documents", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.ExtraSmall, , "fa-file", , PostBackType.None, "AddDocuments($data)")
                  End With
                End With
              End If
            End With
          End With
        End With
      End With

      If Singular.Security.HasAccess("Productions", "Add Invoices From Find Production Screen") Then
        With h.Bootstrap.Dialog("ProductionDocuments", "Documents",, "modal-xs", BootstrapEnums.Style.Info,, "fa-report",,)
          With .Body
            .AddClass("modal-background-gray")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FlatBlock("Documents", False, True)
                  With .ContentTag
                    With .Helpers.Bootstrap.TableFor(Of OBLib.Productions.Correspondence.ProductionDocument)(Function(vm) vm.ProductionDocumentList,
                                                                                                             True, True, False, True, True, True, True,
                                                                                                             "ProductionDocumentList", "AfterDocumentAdded")
                      With .FirstRow
                        .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.DocumentTypeID)
                        With .AddColumn("")
                          .Helpers.DocumentManager()
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Footer
            With .Helpers.Bootstrap.Button(, "Save", BootstrapEnums.Style.Success,
                                                                     , BootstrapEnums.ButtonSize.Small, ,
                               "fa-floppy-o", , PostBackType.None, "SaveDocuments()")
            End With
          End With
        End With
      End If

    End Using%>

</asp:Content>
