﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
  CodeBehind="Find.aspx.vb" Inherits="NewOBWeb.Productions.Find" %>

<%@ Import Namespace="OBLib.Productions.ReadOnly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <%= Singular.Web.CSSFile.RenderLibraryStyles%>
  <%= Singular.Web.CSSFile.RenderStyleTag("~/Styles/Site.css")%>
  <style type="text/css">
    .CrewFinalised {
      background-color: #89E894 !important;
    }

    .Reconciled {
      background-color: #fdfd96 !important;
    }

    .Cancelled {
      background-color: #fd96fd !important;
    }

    tr.CrewFinalised td {
      background-color: #89E894 !important;
    }

    tr.Reconciled td {
      background-color: #fdfd96 !important;
    }

    tr.VisionView td {
      background-color: #996633 !important;
    }

    tr.Cancelled td {
      background-color: #fd96fd !important;
    }
  </style>
  <link href="../Bootstrap/override/form-showcase.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <% Using h = Helpers

      With h.DivC("page-header")
        With .Helpers.HTMLTag("h1")
          .Helpers.HTML("Find Production")
          .Helpers.HTML("<small>enter search criteria</small>")
          '<h1>Example page header <small>Subtext for header</small></h1>
        End With
      End With
      
      With h.DivC("CustomContainer")
        
        With .Helpers.DivC("Criteria well well-lg row")
          
          With .Helpers.BootstrapDivColumn(2, 2, 2, 2)
            With .Helpers.DivC("row")
              With .Helpers.LabelFor(Function(d) d.ProductionTypeID)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.ProductionTypeID)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.LabelFor(Function(d) d.ProductionRefNo)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.ProductionRefNo)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              
            End With
          End With
        
          With .Helpers.BootstrapDivColumn(2, 2, 2, 2)
            .AddClass("col-sm-offset-1 col-md-offset-1 col-lg-offset-1")
            With .Helpers.DivC("row")
              With .Helpers.LabelFor(Function(d) d.EventTypeID)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.EventTypeID)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.LabelFor(Function(d) d.TxDateFrom)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.TxDateFrom)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-20")
              'With .Helpers.Div
              '  With .Helpers.Button(Singular.Web.DefinedButtonType.Find)
              '    .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) d.IsValid)
              '  End With
              'End With
            End With
          End With
        
          With .Helpers.BootstrapDivColumn(2, 2, 2, 2)
            .AddClass("col-sm-offset-1 col-md-offset-1 col-lg-offset-1")
            With .Helpers.DivC("row")
              With .Helpers.LabelFor(Function(d) d.ProductionVenueID)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.ProductionVenueID)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.LabelFor(Function(d) d.TxDateTo)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.TxDateTo)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.Div
              
              End With
            End With
          End With
        
          With .Helpers.BootstrapDivColumn(2, 2, 2, 2)
            .AddClass("col-sm-offset-1 col-md-offset-1 col-lg-offset-1")
            With .Helpers.DivC("row")
              With .Helpers.LabelFor(Function(d) d.EventManagerID)
              End With
              With .Helpers.Div
                With .Helpers.EditorFor(Function(d) d.EventManagerID)
                  .AddClass("form-control")
                End With
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.Div
              
              End With
            End With
            With .Helpers.DivC("row top-buffer-10")
              With .Helpers.Div
              
              End With
            End With
          End With
          
        End With
        
        With .Helpers.DivC("row")
          With .Helpers.Div
            With .Helpers.BootstrapButton(, "Clear", "btn btn-sm btn-warning", "fa fa-eraser", Singular.Web.PostBackType.None, False, , , "ClearCriteria()")
              
            End With
            With .Helpers.BootstrapButton("Find", "Find", "btn btn-sm btn-primary", "fa fa-search", Singular.Web.PostBackType.Full, False, , , )
              .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(d) d.IsValid)
            End With
          End With
        End With

        With .Helpers.DivC("row")
          .Style.Height = 10
        End With
        
        With .Helpers.DivC("row")
          With .Helpers.DivC("panel panel-primary")
          
            With .Helpers.DivC("panel-heading")
              .Helpers.HTML("Results")
            End With
          
            With .Helpers.DivC("panel-body")
              With .Helpers.Bootstrap.PagedGridFor(Of ROProduction)(Function(d) ViewModel.ROProductionListPagingManager, Function(d) d.ROProductionList, False, False,
                                                                    False, False, False, True, False,,Singular.Web.BootstrapEnums.PagerPosition.Top)          
                With .FirstRow
                  With .AddColumn("")
                    .Style.Width = 50
                    With .Helpers.BootstrapLinkFor(, , , "Edit", Singular.Web.LinkTargetType._blank, "glyphicon-pencil")
                      .Link.AddClass("btn btn-sm btn-primary")
                      .Link.AddBinding(Singular.Web.KnockoutBindingString.href, "GetHref($data)")
                    End With
                  End With
                  .AddBinding(Singular.Web.KnockoutBindingString.css, "GetProductionStatColour($data)")
                  .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionRefNo)
                  .AddReadOnlyColumn(Function(c) c.ProductionDescription)
                  .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionType)
                  .AddReadOnlyColumn(Function(c As ROProduction) c.EventType)
                  .AddReadOnlyColumn(Function(c As ROProduction) c.ProductionVenue)
                  .AddReadOnlyColumn(Function(c As ROProduction) c.PlayStartDateTime)
                  .AddReadOnlyColumn(Function(c As ROProduction) c.PlayEndDateTime)
                  .AddReadOnlyColumn(Function(c) c.EventManager)
                  .AddReadOnlyColumn(Function(c) c.SynergyGenRefNo)
                End With
              End With
            End With
              
          End With
        End With
        
        
      End With
      
    End Using%>
  <script type="text/javascript">

    function ClearCriteria() {
      ViewModel.ProductionTypeID(null);
      ViewModel.EventTypeID(null);
      ViewModel.ProductionVenueID(null);
      ViewModel.TxDateFrom(null);
      ViewModel.TxDateTo(null);
      ViewModel.EventManagerID(null);
      ViewModel.ProductionRefNo("");
    }

    function GetHref(ROPRoduction) {
      return Singular.RootPath + "/Productions/Production.aspx?P=" + ROPRoduction.ProductionID().toString();
      //return Singular.RootPath + "/OutsideBroadcast/ProductionContent/OBPCProduction.aspx?P=" + ROPRoduction.ProductionID().toString();
    };

    function GetProductionStatColour(ROProduction) {
      var defaultStyle = "";
      switch (ROProduction.ProductionAreaStatusID()) {
        case 2:
          defaultStyle = 'CrewFinalised';
          break;
        case 3:
          defaultStyle = 'CrewFinalised';
          break;
        case 4:
          defaultStyle = 'Reconciled';
          break;
        case 5:
          defaultStyle = 'Cancelled';
          break;
      }
      if (ROPRoduction.VisionViewInd()) {
        defaultStyle = 'VisionView';
      }
        return defaultStyle;
    };

    function RefreshROProductionList() {

    }

  </script>
</asp:Content>
