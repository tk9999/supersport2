﻿Imports OBLib.Productions.Areas
Imports OBLib.Productions.Crew.ReadOnly

Namespace CustomControls

  Public Class ProductionControls

    Public Class ProductionMain
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        '-------------------------------------------Main--------------------------------------------
        

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionRequirements
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        '-------------------------------------------Requirements--------------------------------------------
        With Helpers.DivC("row RequirementList")
          .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.RequirementsTabVisible)
          .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.RequirementsTabVisible)
          With .Helpers.BootstrapDivColumn(12, 12, 5)
            .AddClass("EquipmentTypeList")
            With .Helpers.BootstrapPanel
              With .PanelHeading
                .Helpers.HTML("Equipment")
              End With
              With .PanelBody
                If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                  'With .Helpers.BootstrapTableFor(Of OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType)(Function(vm) ViewModel.CurrentProduction.ProductionSpecRequirementEquipmentTypeList, False, False, "")
                  '  With .FirstRow
                  '    '.AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.SystemID)
                  '    '.AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.ProductionAreaID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.EquipmentTypeID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.EquipmentSubTypeID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.EquipmentQuantity)
                  '    'With .AddColumn("")
                  '    '  .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveEquipment($data)")
                  '    'End With
                  '  End With
                  'End With
                Else
                  With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddEquipment()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddRequirementEquipmentType()")
                  End With
                  .Helpers.DivC("row top-buffer-10")
                  'With .Helpers.BootstrapTableFor(Of OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType)(Function(vm) ViewModel.CurrentProduction.ProductionSpecRequirementEquipmentTypeList, False, False, "")
                  '  With .FirstRow
                  '    '.AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.SystemID)
                  '    '.AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.ProductionAreaID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.EquipmentTypeID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.EquipmentSubTypeID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType) et.EquipmentQuantity)
                  '    With .AddColumn("")
                  '      .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveEquipment($data)")
                  '    End With
                  '  End With
                  'End With
                End If
              End With
            End With
          End With
          With .Helpers.BootstrapDivColumn(12, 12, 7)
            .AddClass("PositionList")
            With .Helpers.BootstrapPanel
              With .PanelHeading
                .Helpers.HTML("Positions")
              End With
              With .PanelBody
                If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                  'With .Helpers.BootstrapTableFor(Of OBLib.Productions.Specs.ProductionSpecRequirementPosition)(Function(vm) ViewModel.CurrentProduction.ProductionSpecRequirementPositionList, False, False, "")
                  '  With .FirstRow
                  '    '.AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.SystemID)
                  '    '.AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.ProductionAreaID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.DisciplineID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.PositionID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.EquipmentSubTypeID)
                  '    .AddReadOnlyColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.EquipmentQuantity)
                  '  End With
                  'End With
                Else
                  With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddPosition()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddRequirementPosition()")
                  End With
                  .Helpers.DivC("row top-buffer-10")
                  'With .Helpers.BootstrapTableFor(Of OBLib.Productions.Specs.ProductionSpecRequirementPosition)(Function(vm) ViewModel.CurrentProduction.ProductionSpecRequirementPositionList, False, False, "")
                  '  With .FirstRow
                  '    '.AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.SystemID)
                  '    '.AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.ProductionAreaID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.DisciplineID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.PositionID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.EquipmentSubTypeID)
                  '    .AddColumn(Function(et As OBLib.Productions.Specs.ProductionSpecRequirementPosition) et.EquipmentQuantity)
                  '    With .AddColumn("")
                  '      .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemovePosition($data)")
                  '    End With
                  '  End With
                  'End With
                End If
              End With
            End With
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionOutsourceServices
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.ForEachTemplate(Of OBLib.Productions.Areas.ProductionSystemArea)(Function(vm) ViewModel.CurrentProduction.ProductionSystemAreaList)
          '-------------------------------------------Outsourced Services--------------------------------------------
          With .Helpers.DivC("row ProductionOutsourceServiceList")
            .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.OutsourceServicesTabVisible)
            .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.OutsourceServicesTabVisible)
            With .Helpers.BootstrapPanel
              With .PanelHeading
                .Helpers.HTML("Outsource Services")
              End With
              With .PanelBody
                If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Services.ProductionOutsourceService)(Function(c As OBLib.Productions.Areas.ProductionSystemArea) c.ProductionOutsourceServiceList, False, False, "")
                    With .FirstRow
                      With .AddReadOnlyColumn(Function(c) c.OutsourceServiceTypeID)
                      End With
                      .AddReadOnlyColumn(Function(c) c.SupplierID)
                      .AddReadOnlyColumn(Function(c) c.Description)
                      .AddReadOnlyColumn(Function(c) c.QuoteRequestedDate)
                      .AddReadOnlyColumn(Function(c) c.QuoteReceivedDate)
                      .AddReadOnlyColumn(Function(c) c.QuotedAmount)
                      With .AddColumn("Required")
                        With .Helpers.BootstrapStateButton("", "", "NotRequiredIndCss($data)", "NotRequiredIndHtml($data)", False)
                          .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d As OBLib.Productions.Services.ProductionOutsourceService) False)
                        End With
                      End With
                      .AddReadOnlyColumn(Function(c) c.NotRequiredReason)
                      .AddReadOnlyColumn(Function(c) c.SAPOrderNo)
                    End With
                    With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceTimeline)(Function(e) e.ProductionOutsourceServiceTimelineList, False, False, "")
                      .Style.Width = "70%"
                      .AddClass("pull-right")
                      With .FirstRow
                        .AddReadOnlyColumn(Function(d As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) d.ServiceStartDateTime)
                        .AddReadOnlyColumn(Function(d As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) d.ServiceEndDateTime)
                        With .AddColumn("Breakfast?")
                          With .Helpers.BootstrapStateButton("", "", "CaterBreakfastCss($data)", "CaterBreakfastHtml($data)", False)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .AddColumn("Lunch?")
                          With .Helpers.BootstrapStateButton("", "", "CaterLunchCss($data)", "CaterLunchHtml($data)", False)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                        With .AddColumn("Dinner?")
                          With .Helpers.BootstrapStateButton("", "", "CaterDinnerCss($data)", "CaterDinnerHtml($data)", False)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                    End With
                    With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceDetail)(Function(d) d.ProductionOutsourceServiceDetailList, False, False, "")
                      .Style.Width = "50%"
                      .AddClass("pull-right")
                      With .FirstRow
                        .AddColumn(Function(d) d.ProductionOutsourceServiceDetail)
                        With .AddColumn("Required?")
                          With .Helpers.BootstrapStateButton("", "", "ServiceDetailRequiredIndCss($data)", "ServiceDetailRequiredIndHtml($data)", False)
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                        End With
                      End With
                    End With
                  End With
                Else
                  With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewOutsourceService()")
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionOutsourceService()")
                  End With
                  .Helpers.DivC("top-buffer-10")
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Services.ProductionOutsourceService)(Function(c As OBLib.Productions.Areas.ProductionSystemArea) c.ProductionOutsourceServiceList, False, True, "")
                    .RemoveButton.RemoveClass("btn-primary")
                    .RemoveButton.AddClass("btn-sm btn-danger")
                    With .FirstRow
                      With .AddColumn(Function(c) c.OutsourceServiceTypeID)
                      End With
                      .AddColumn(Function(c) c.SupplierID)
                      .AddColumn(Function(c) c.Description)
                      .AddColumn(Function(c) c.QuoteRequestedDate)
                      .AddColumn(Function(c) c.QuoteReceivedDate)
                      .AddColumn(Function(c) c.QuotedAmount)
                      With .AddColumn("Required")
                        .Helpers.BootstrapStateButton("", "NotRequiredIndClick($data)", "NotRequiredIndCss($data)", "NotRequiredIndHtml($data)", False)
                      End With
                      .AddColumn(Function(c) c.NotRequiredReason)
                      .AddColumn(Function(c) c.SAPOrderNo)
                    End With
                    With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceTimeline)(Function(e) e.ProductionOutsourceServiceTimelineList, True, True, "")
                      .Style.Width = "70%"
                      .AddClass("pull-right")
                      .AddNewButton.AddClass("btn-sm")
                      .RemoveButton.RemoveClass("btn-primary")
                      .RemoveButton.AddClass("btn-sm btn-danger")
                      With .FirstRow
                        With .AddColumn("Service Starts")
                          With .Helpers.EditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceStartDateTime)
                            .Style.Width = 90
                          End With
                          With .Helpers.TimeEditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceStartDateTime)
                            .Style.Width = 60
                          End With
                        End With
                        With .AddColumn("Service Ends")
                          With .Helpers.EditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceEndDateTime)
                            .Style.Width = 90
                          End With
                          With .Helpers.TimeEditorFor(Function(e As OBLib.Productions.Services.ProductionOutsourceServiceTimeline) e.ServiceEndDateTime)
                            .Style.Width = 60
                          End With
                        End With
                        With .AddColumn("Breakfast?")
                          With .Helpers.BootstrapStateButton("", "CaterBreakfastClick($data)", "CaterBreakfastCss($data)", "CaterBreakfastHtml($data)", False)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "IsCatering($data)")
                          End With
                        End With
                        With .AddColumn("Lunch?")
                          With .Helpers.BootstrapStateButton("", "CaterLunchClick($data)", "CaterLunchCss($data)", "CaterLunchHtml($data)", False)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "IsCatering($data)")
                          End With
                        End With
                        With .AddColumn("Dinner?")
                          With .Helpers.BootstrapStateButton("", "CaterDinnerClick($data)", "CaterDinnerCss($data)", "CaterDinnerHtml($data)", False)
                            .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "IsCatering($data)")
                          End With
                        End With
                      End With
                    End With
                    With .AddChildTable(Of OBLib.Productions.Services.ProductionOutsourceServiceDetail)(Function(d) d.ProductionOutsourceServiceDetailList, True, True, "")
                      .Style.Width = "50%"
                      .AddClass("pull-right")
                      .AddNewButton.AddClass("btn-sm")
                      .RemoveButton.RemoveClass("btn-primary")
                      .RemoveButton.AddClass("btn-sm btn-danger")
                      With .FirstRow
                        .AddColumn(Function(d) d.ProductionOutsourceServiceDetail)
                        With .AddColumn("Required?")
                          .Helpers.BootstrapStateButton("", "ServiceDetailRequiredIndClick($data)", "ServiceDetailRequiredIndCss($data)", "ServiceDetailRequiredIndHtml($data)", False)
                        End With
                      End With
                    End With
                  End With
                End If
              End With
            End With
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionVehicles
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        '-------------------------------------------Production Vehicles--------------------------------------------
        With Helpers.DivC("row ProductionVehiclesTab")
          .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.ProductionVehiclesTabVisible)
          .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.ProductionVehiclesTabVisible)

          With .Helpers.DivC("ProductionVehicleList")
            .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.VehicleListVisible)

            With .Helpers.BootstrapPanel
              With .PanelHeading
                .Helpers.HTML("Production Vehicles")
              End With
              With .PanelBody
                If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Vehicles.ProductionVehicle)(Function(c) ViewModel.CurrentProduction.ProductionVehicleList, False, False, "")
                    With .FirstRow
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleName).AddClass("bold")
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleType).AddClass("bold")
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteRequestedDate)
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteReceivedDate)
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuotedAmount)
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.Clash)
                    End With
                  End With
                Else
                  If Singular.Security.HasAccess("Productions", "Edit Vehicles") Then
                    .Helpers.BootstrapButton("", "Add Vehicle", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, True, , , "SetupSelectVehicle()")
                  End If
                  .Helpers.DivC("top-buffer-10")
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Vehicles.ProductionVehicle)(Function(c) ViewModel.CurrentProduction.ProductionVehicleList, False, False, "")
                    With .FirstRow
                      If Singular.Security.HasAccess("Productions", "Edit Vehicles") Then
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleName).AddClass("bold")
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleType).AddClass("bold")
                        With .AddColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteRequestedDate)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.Supplied()")
                        End With
                        With .AddColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteReceivedDate)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.Supplied()")
                        End With
                        With .AddColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuotedAmount)
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.enable, "$data.Supplied()")
                        End With
                        With .AddColumn("")
                          .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveVehicle($data)")
                        End With
                      Else
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleName).AddClass("bold")
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.VehicleType).AddClass("bold")
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteRequestedDate)
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuoteReceivedDate)
                        .AddReadOnlyColumn(Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.QuotedAmount)
                      End If
                      With .AddColumn("Clash")
                        .AddClass("VehicleClash")
                        With .Helpers.HTMLTag("div")
                          .AddBinding(Singular.Web.KnockoutBindingString.html, Function(c As OBLib.Productions.Vehicles.ProductionVehicle) c.Clash)
                          .AddClass("VehicleClash text-overflow")
                        End With
                      End With
                    End With
                  End With
                End If
              End With
            End With
          End With
          If Not (ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled) Then
            With .Helpers.DivC("ROVehicleList")
              .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.SelectVehicleVisible)
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Production Vehicles")
                End With
                With .PanelBody
                  'Vehicle Select                  
                  .Helpers.BootstrapButton("", "Back", "btn-sm btn-danger", "glyphicon-arrow-left", Singular.Web.PostBackType.None, False, , , "BackToProductionVehicleList()")
                  .Helpers.BootstrapPageHeader(2, "Vehicle List", "")
                  With .Helpers.DivC("field-box")
                    With .Helpers.EditorFor(Function(p) ViewModel.ROVehicleFilter)
                      .Attributes("placeholder") = "Filter..."
                      .AddClass("form-control filter-field")
                      .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                      .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterROVehicleList}")
                    End With
                  End With
                  .Helpers.DivC("top-buffer-10")
                  With .Helpers.BootstrapTableFor(Of OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOld)(Function(c) ViewModel.ROVehicleSelectList, False, False, "$data.Visible()")
                    With .FirstRow
                      With .AddColumn("")
                        .Helpers.BootstrapButton("", "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "AddVehicle($data)")
                      End With
                      .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOld) c.VehicleName).AddClass("bold")
                      .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOld) c.VehicleType).AddClass("bold")
                      .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOld) c.PreviousCityDist)
                      .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOld) c.CurrCity)
                      .AddReadOnlyColumn(Function(c As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOld) c.NextCityDist)
                    End With
                  End With
                End With
              End With
            End With
          End If
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionTimelines
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.ForEachTemplate(Of OBLib.Productions.Areas.ProductionSystemArea)(Function(vm) ViewModel.CurrentProduction.ProductionSystemAreaList)
          '-------------------------------------------Timelines--------------------------------------------
          With .Helpers.DivC("row ProductionTimelinesTab")
            .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.TimelinesTabVisible)
            .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.TimelinesTabVisible)

            'With .Helpers.DivC("row")
            '  With .Helpers.BootstrapDivColumn(12, 6, 3)
            '    With .Helpers.DivC("field-box")
            '      .Helpers.LabelFor(Function(sa As ProductionSystemArea) sa.SystemID)
            '      With .Helpers.EditorFor(Function(sa As ProductionSystemArea) sa.SystemID)
            '        .AddClass("form-control")
            '        .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
            '      End With
            '    End With
            '  End With
            '  With .Helpers.BootstrapDivColumn(12, 6, 3)
            '    With .Helpers.DivC("field-box")
            '      .Helpers.LabelFor(Function(sa As ProductionSystemArea) sa.ProductionAreaID)
            '      With .Helpers.EditorFor(Function(sa As ProductionSystemArea) sa.ProductionAreaID)
            '        .AddClass("form-control")
            '        .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
            '      End With
            '    End With
            '  End With
            '  With .Helpers.BootstrapDivColumn(12, 6, 3)
            '    'With .Helpers.DivC("field-box")
            '    '  .Helpers.LabelFor(Function(sa As ProductionSystemArea) sa.ProductionAreaStatus)
            '    '  With .Helpers.Div
            '    '    With .Helpers.BootstrapStateButton("", "ProductionAreaStatusClick($data)", "ProductionAreaStatusCss($data)", "ProductionAreaStatusHtml($data)", False)
            '    '      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(vm) ViewModel.IsValid)
            '    '    End With
            '    '  End With
            '    'End With
            '  End With
            '  With .Helpers.BootstrapDivColumn(12, 6, 3)
            '    'With .Helpers.DivC("field-box")
            '    '  .Helpers.LabelFor(Function(sa As ProductionSystemArea) sa.StatusDate)
            '    '  With .Helpers.EditorFor(Function(sa As ProductionSystemArea) sa.StatusDate)
            '    '    .AddClass("form-control")
            '    '    .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
            '    '  End With
            '    'End With
            '  End With
            'End With

            'With .Helpers.BootstrapDivColumn(12, 12, 12)
            With .Helpers.DivC("ProductionTimelineList")
              With .Helpers.BootstrapPanel("panel-primary")
                With .PanelHeading
                  .Helpers.HTML("Timelines")
                End With
                With .PanelBody
                  If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                    With .Helpers.BootstrapTableFor(Of ProductionTimeline)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionTimelineList, False, False, "$data.CanRender()") 'RenderTimeline($data)
                      With .FirstRow
                        'With .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.ProductionTimelineTypeID)
                        '  .Style.TextAlign = Singular.Web.TextAlign.left
                        '  .Style.Width = 220
                        'End With
                        .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.StartDateTime).Style.TextAlign = Singular.Web.TextAlign.center
                        .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.EndDateTime).Style.TextAlign = Singular.Web.TextAlign.center
                        '.AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.VehicleID).Style.TextAlign = Singular.Web.TextAlign.left
                        .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.CrewTypeID).Style.TextAlign = Singular.Web.TextAlign.left
                        .AddReadOnlyColumn(Function(pt As ProductionTimeline) pt.Comments).Style.TextAlign = Singular.Web.TextAlign.left
                      End With
                    End With
                  Else
                    With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewTimeline()")
                      .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionTimeline()")
                    End With
                    .Helpers.DivC("top-buffer-10")
                    With .Helpers.BootstrapTableFor(Of ProductionTimeline)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionTimelineList, False, False, "$data.CanRender()") 'RenderTimeline($data)
                      With .FirstRow
                        With .AddColumn(Function(pt As ProductionTimeline) pt.ProductionTimelineTypeID)
                          .Style.TextAlign = Singular.Web.TextAlign.left
                          .Style.Width = 220
                        End With
                        .AddColumn(Function(pt As ProductionTimeline) pt.StartDateTime).Style.TextAlign = Singular.Web.TextAlign.center
                        With .AddColumn("Start Time")
                          .Helpers.TimeEditorFor(Function(pt As ProductionTimeline) pt.StartDateTime).Style.TextAlign = Singular.Web.TextAlign.center
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        '.AddColumn(Function(pt As ProductionTimeline) pt.EndDateTime).Style.TextAlign = Singular.Web.TextAlign.center
                        With .AddColumn("End Time")
                          .Helpers.TimeEditorFor(Function(pt As ProductionTimeline) pt.EndDateTime).Style.TextAlign = Singular.Web.TextAlign.center
                          .Style.TextAlign = Singular.Web.TextAlign.center
                        End With
                        .AddColumn(Function(pt As ProductionTimeline) pt.VehicleID).Style.TextAlign = Singular.Web.TextAlign.left
                        .AddColumn(Function(pt As ProductionTimeline) pt.CrewTypeID).Style.TextAlign = Singular.Web.TextAlign.left
                        .AddColumn(Function(pt As ProductionTimeline) pt.Comments).Style.TextAlign = Singular.Web.TextAlign.left
                        With .AddColumn("")
                          .Helpers.BootstrapButton(, , "btn-xs btn-flat btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "RemoveTimeline($data)")
                        End With
                      End With
                    End With
                  End If
                End With
              End With
            End With

            'End With

            'With .Helpers.BootstrapDivColumn(12, 6, 3)
            '  .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.StatusesVisible)
            '  With .Helpers.BootstrapPanel("panel-primary")
            '    With .PanelHeading
            '      .Helpers.HTML("Move to")
            '    End With
            '    With .PanelBody
            '      With .Helpers.BootstrapTableFor(Of ROAllowedAreaStatus)(Function(vm) ViewModel.ROAllowedAreaStatusList, False, False, "")
            '        With .FirstRow
            '          With .AddColumn("")
            '            With .Helpers.BootstrapButton(, "Select", "btn-xs btn-flat btn-primary", "", Singular.Web.PostBackType.Ajax, False, , , "SelectStatus($data)")
            '              '.Button.AddBinding(Singular.Web.KnockoutBindingString.visible, Function(d As ROAllowedAreaStatus) d.CanSelectStatus)
            '            End With
            '          End With
            '          .AddReadOnlyColumn(Function(pt As ROAllowedAreaStatus) pt.NextStatus)
            '          '.AddReadOnlyColumn(Function(pt As ROProductionAreaAllowedStatus) pt.OrderNo)
            '          .AddReadOnlyColumn(Function(pt As ROAllowedAreaStatus) pt.BackwardInd)
            '          '.AddReadOnlyColumn(Function(pt As ROAllowedAreaStatus) pt.CannotSelectStatusReason)
            '        End With
            '      End With
            '    End With
            '  End With
            'End With

            'With .Helpers.BootstrapDivColumn(3, 3, 3)
            '  .AddClass("well")
            '  With .Helpers.With(Of ProductionTimeline)(Function(vm) ViewModel.NewTimeline)
            '    With .Helpers.DivC("field-box top-buffer-10")
            '      .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.ProductionTimelineTypeID)
            '      .Helpers.EditorFor(Function(pt As ProductionTimeline) pt.ProductionTimelineTypeID).AddClass("form-control")
            '    End With
            '    With .Helpers.DivC("field-box top-buffer-10")
            '      .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.StartDateTime)
            '      .Helpers.EditorFor(Function(pt As ProductionTimeline) pt.StartDateTime).AddClass("form-control")
            '      .Helpers.TimeEditorFor(Function(pt As ProductionTimeline) pt.StartDateTime).AddClass("form-control time")
            '    End With
            '    'With .Helpers.DivC("field-box")
            '    '  .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.StartDateTime)
            '    '  .Helpers.TimeEditorFor(Function(pt As ProductionTimeline) pt.StartDateTime).AddClass("form-control")
            '    'End With
            '    With .Helpers.DivC("field-box top-buffer-10")
            '      .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.EndDateTime)
            '      .Helpers.EditorFor(Function(pt As ProductionTimeline) pt.EndDateTime).AddClass("form-control")
            '      .Helpers.TimeEditorFor(Function(pt As ProductionTimeline) pt.EndDateTime).AddClass("form-control time")
            '    End With
            '    'With .Helpers.DivC("field-box")
            '    '  .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.EndDateTime)
            '    '  .Helpers.TimeEditorFor(Function(pt As ProductionTimeline) pt.EndDateTime).AddClass("form-control")
            '    'End With
            '    With .Helpers.DivC("field-box top-buffer-10")
            '      .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.VehicleID)
            '      .Helpers.EditorFor(Function(pt As ProductionTimeline) pt.VehicleID).AddClass("form-control")
            '    End With
            '    With .Helpers.DivC("field-box top-buffer-10")
            '      .Helpers.LabelFor(Function(pt As ProductionTimeline) pt.Comments)
            '      .Helpers.EditorFor(Function(pt As ProductionTimeline) pt.Comments).AddClass("form-control")
            '    End With
            '  End With
            '  .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewTimeline()")
            'End With
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionHumanResources
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.ForEachTemplate(Of OBLib.Productions.Areas.ProductionSystemArea)(Function(vm) Production.ProductionSystemAreaList)
          '-------------------------------------------Crew--------------------------------------------
          With .Helpers.DivC("row ProductionCrewTab")
            .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.ProductionCrewTabVisible)
            .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.ProductionCrewTabVisible)

            If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
              With .Helpers.DivC("field-box")
                With .Helpers.EditorFor(Function(p) ViewModel.PHRFilter)
                  .Attributes("placeholder") = "Filter..."
                  .AddClass("form-control filter-field")
                  .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                  .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterPHR}")
                End With
              End With
              'Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionHumanResourceList
              .Helpers.DivC("top-buffer-10")
              With .Helpers.BootstrapTableFor(Of ProductionHumanResource)("ProductionHumanResourceListSorted()", False, False, "$data.Visible()")
                With .FirstRow
                  .AddBinding(Singular.Web.KnockoutBindingString.css, "PHRCss($data)")
                  .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Discipline)
                  .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Position)
                  .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.PositionType)
                  .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.HumanResource)
                  .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.PrefHumanResource)
                  .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.VehicleName)
                End With
              End With
            Else
              '-------------------------------------------PHR List--------------------------------------------
              With .Helpers.DivC("ROProductionHumanResourceList")
                .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.PHRListVisible)
                With .Helpers.BootstrapPanel
                  With .PanelHeading
                    .Helpers.HTML("Crew")
                  End With
                  With .PanelBody
                    With .Helpers.Div
                      '-------------------------------------------Heading-------------------------------------------
                      With .Helpers.BootstrapButton(, "Add", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddNewProductionHumanResource()")
                        .AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionHumanResource()")
                      End With
                      .Helpers.BootstrapButton("PopulateHumanResources", "Populate HR", "btn-sm btn-default", "glyphicon-cog", Singular.Web.PostBackType.Ajax, False, , , )
                    End With
                    'PopulateHumanResources
                    .Helpers.DivC("top-buffer-10")
                    With .Helpers.DivC("field-box")
                      With .Helpers.EditorFor(Function(p) ViewModel.PHRFilter)
                        .Attributes("placeholder") = "Filter..."
                        .AddClass("form-control filter-field")
                        .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                        .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterPHR}")
                      End With
                    End With
                    .Helpers.DivC("top-buffer-10")
                    With .Helpers.BootstrapTableFor(Of ProductionHumanResource)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.ProductionHumanResourceList, False, False, "$data.Visible()")
                      With .FirstRow
                        .AddBinding(Singular.Web.KnockoutBindingString.css, "PHRCss($data)")
                        '----Edit-------------------------------------------
                        With .AddColumn("")
                          With .Helpers.If(Function(d As ProductionHumanResource) d.CanEditPHR)
                            .Helpers.BootstrapButton(, "Edit", "btn-xs btn-primary", "glyphicon-pencil", Singular.Web.PostBackType.None, False, , , "SetSelectedPHR($data)")
                          End With
                          With .Helpers.If(Function(d As ProductionHumanResource) Not d.CanEditPHR)
                            .Helpers.ReadOnlyFor(Function(d As ProductionHumanResource) d.CanEditPHRReason, Singular.Web.FieldTagType.span)
                          End With
                        End With
                        .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Discipline)
                        .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Position)
                        .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.PositionType)
                        .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.HumanResource)
                        .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.PrefHumanResource)
                        .AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.VehicleName)
                        With .AddColumn("")
                          .Helpers.BootstrapButton(, , "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, , , , "DeleteProductionHumanResource($data)")
                        End With
                        '.AddReadOnlyColumn(Function(psa As ProductionHumanResource) psa.Room)
                      End With
                    End With
                  End With
                End With
              End With

              '-------------------------------------------Selected PHR--------------------------------------------
              With .Helpers.DivC("row CurrentProductionHumanResource well")
                .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.SelectedHRVisible)
                With .Helpers.With(Of ProductionHumanResource)("SelectedPHR()")
                  With .Helpers.BootstrapDivColumn(12, 12, 4)
                    With .Helpers.DivC("row")
                      .Helpers.BootstrapButton(, "Back", "btn-sm btn-danger", , Singular.Web.PostBackType.None, False, , , "BackToPHRList()")
                    End With
                    'General Details
                    With .Helpers.BootstrapDivColumn(12, 12, 9)
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.HumanResource)
                        With .Helpers.DivC("input-group")
                          With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.HumanResource)
                            .AddClass("form-control")
                            .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                          End With
                          With .Helpers.HTMLTag("span")
                            .AddClass("input-group-btn")
                            .Helpers.BootstrapButton(, , "btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "ClearHumanResourceID($data)")
                          End With
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.DisciplineID)
                        With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.DisciplineID)
                          .AddClass("form-control")
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.PositionID)
                        With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.PositionID)
                          .AddClass("form-control")
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.PositionTypeID)
                        With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.PositionTypeID)
                          .AddClass("form-control")
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.PrefHumanResource)
                        With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.PrefHumanResource)
                          .AddClass("form-control")
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.VehicleID)
                        With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.VehicleID)
                          .AddClass("form-control")
                        End With
                        With .Helpers.DivC("field-box top-buffer-10")
                          'TODO: when typing in cancelled reason, just set cancelled by and date, so add a set expression
                          .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.SelectionReason)
                          With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.SelectionReason)
                            .AddClass("form-control")
                          End With
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        'TODO: when typing in cancelled reason, just set cancelled by and date, so add a set expression
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.CancelledReason)
                        With .Helpers.EditorFor(Function(phr As ProductionHumanResource) phr.CancelledReason)
                          .AddClass("form-control")
                        End With
                      End With
                    End With
                    With .Helpers.BootstrapDivColumn(12, 12, 3)
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.CrewLeaderInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "CrewLeaderClick($data)", "CrewLeaderCss($data)", "CrewLeaderHtml($data)", False)
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.ReliefCrewInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "ReliefCrewClick($data)", "ReliefCrewCss($data)", "ReliefCrewHtml($data)", False)
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.TBCInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "TBCClick($data)", "TBCCss($data)", "TBCHtml($data)", False)
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.TrainingInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "TrainingClick($data)", "TrainingCss($data)", "TrainingHtml($data)", False)
                        End With
                      End With
                      With .Helpers.DivC("field-box top-buffer-10")
                        .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.SuppliedHumanResourceInd)
                        With .Helpers.Div
                          .Helpers.BootstrapStateButton("", "SuppliedHumanResourceClick($data)", "SuppliedHumanResourceCss($data)", "SuppliedHumanResourceHtml($data)", False)
                        End With
                      End With
                    End With
                  End With
                  'Qualified HR---------------------------------------------------------------------------------------------------------------------------------------------------
                  With .Helpers.BootstrapDivColumn(12, 12, 8)
                    With .Helpers.DivC("row ReplaceResource  top-buffer-10")
                      With .Helpers.DivC("row SearchCriteria")
                        With .Helpers.BootstrapDivColumn(12, 12, 3)
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.FilteredProductionTypeInd)
                            With .Helpers.Div
                              .Helpers.BootstrapStateButton("", "FilteredProductionTypeClick($data)", "FilteredProductionTypeCss($data)", "FilteredProductionTypeHtml($data)", False)
                            End With
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 3)
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.FilteredPositionInd)
                            With .Helpers.Div
                              .Helpers.BootstrapStateButton("", "FilteredPositionClick($data)", "FilteredPositionCss($data)", "FilteredPositionHtml($data)", False)
                            End With
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 3)
                          With .Helpers.DivC("field-box")
                            .Helpers.LabelFor(Function(phr As ProductionHumanResource) phr.IncludedOffInd)
                            With .Helpers.Div
                              .Helpers.BootstrapStateButton("", "IncludedOffClick($data)", "IncludedOffCss($data)", "IncludedOffHtml($data)", False)
                            End With
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 3)
                          .Helpers.BootstrapButton("", "Search", "btn-sm btn-primary", "glyphicon-search", Singular.Web.PostBackType.None, False, , , "GetQualifiedHR($data)")
                        End With
                      End With
                      With .Helpers.DivC("row SearchResults top-buffer-10")
                        With .Helpers.BootstrapPanel
                          With .PanelHeading
                            .Helpers.HTML("Search Results")
                          End With
                          With .PanelBody
                            With .Helpers.EditorFor(Function(p) p.SearchResultsFilter)
                              .Attributes("placeholder") = "Filter..."
                              .AddClass("form-control filter-field")
                              .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                              .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterQualifiedHR }")
                            End With
                            With .Helpers.DivC("scroll-content")
                              With .Helpers.BootstrapTableFor(Of ROSkilledHR)(Function(vm) vm.ROSkilledHRList, False, False, "$data.VisibleInd()")
                                With .FirstRow
                                  With .AddColumn("")
                                    .Helpers.BootstrapButton(, "Select", "btn-xs btn-primary", "glyphicon-hand-up", Singular.Web.PostBackType.None, False, , , "SetPrimaryHR($data)")
                                    .Helpers.BootstrapButton(, "Preferred", "btn-xs btn-default", "glyphicon-thumbs-up", Singular.Web.PostBackType.None, False, , , "SetPreferredHR($data)")
                                  End With
                                  .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.HumanResource)
                                  .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ContractType)
                                  .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.PrimaryInd)
                                  .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.CityCode)
                                  '.AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ProductionCount)
                                  .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.CrewScheduleHours)
                                  .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.TimesheetHours)
                                  If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
                                    '.AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.DaysWorked)
                                    .AddReadOnlyColumn(Function(sk As ROSkilledHR) sk.ContractDays)
                                  End If
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End If
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionHRSchedules
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.ForEachTemplate(Of OBLib.Productions.Areas.ProductionSystemArea)(Function(vm) Production.ProductionSystemAreaList)
          With .Helpers.DivC("row ProductionCrewSchedule")
            .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.SchedulesTabVisible)
            .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.SchedulesTabVisible)

            With .Helpers.BootstrapPanel
              .Panel.AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.HRListVisible)
              With .PanelHeading
                .Helpers.HTML("Crew")
              End With
              With .PanelBody
                If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Schedules.HRProductionSchedule)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.HRProductionScheduleList, False, False, "$data.Visible()")
                    .AddClass("ParentList")
                    With .FirstRow
                      With .AddColumn("")
                        .Style.Width = "15%"
                        .Helpers.BootstrapButton(, "View", "btn-xs btn-primary", "glyphicon-search", Singular.Web.PostBackType.None, False, , , )
                        '"GetReadOnlySchedule($data)"
                      End With
                      With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.HumanResource)
                        .AddClass("HumanResource bold")
                        .Style.Width = "15%"
                      End With
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.CityCode)
                      .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.Local)
                      'With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionSchedule) c.AllClashes)
                      '  .AddClass("Clash text-danger")
                      '  .Style.Width = "40%"
                      'End With
                    End With
                  End With
                Else
                  'HR Schedule List---------------------------------------------------------------------------------------------------------------------------------------------------
                  With .Helpers.DivC("HRProductionScheduleList top-buffer-10") 'row
                    With .Helpers.Div 'C("row")
                      '"GenerateSchedules()"
                      '-------------------------------------------Heading-------------------------------------------
                      .Helpers.BootstrapButton("GenerateSchedules", "Generate Schedules", "btn-sm btn-default", "glyphicon-cog", Singular.Web.PostBackType.Full, False, , , )
                      '.Helpers.BootstrapButton(, "Add Items", "btn-sm btn-default", "glyphicon-plus-sign", Singular.Web.PostBackType.None, False, , , "AddItems()")
                      '.Helpers.BootstrapButton(, "Update Items", "btn-sm btn-default", "glyphicon-edit", Singular.Web.PostBackType.None, False, , , "UpdateItems()")
                      '.Helpers.BootstrapButton(, "Delete Items", "btn-sm btn-default", "glyphicon-remove-sign", Singular.Web.PostBackType.None, False, , , "DeleteItems()")
                      '.Helpers.BootstrapButton(, "Validate Schedule", "btn-sm btn-default", "glyphicon-refresh", Singular.Web.PostBackType.None, False, , , )
                    End With
                    .Helpers.DivC("top-buffer-10")
                    With .Helpers.DivC("field-box")
                      With .Helpers.EditorFor(Function(p) ViewModel.HRListFilter)
                        .Attributes("placeholder") = "Filter..."
                        .AddClass("form-control filter-field")
                        .AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                        .AddBinding(Singular.Web.KnockoutBindingString.event, "{ keyup: FilterHRList}")
                      End With
                    End With
                    .Helpers.DivC("top-buffer-10")
                    'With .Helpers.BootstrapDivColumn(12, 12, 8)

                    'End With
                    'With .Helpers.BootstrapDivColumn(12, 12, 4)

                    'End With
                    With .Helpers.Div
                      With .Helpers.BootstrapDivColumn(12, 12, 12)
                        With .Helpers.BootstrapDivColumn(12, 12, 1)

                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 3)
                          With .Helpers.HTMLTag("span")
                            .AddClass("bold")
                            .Helpers.HTML("Human Resource")
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 2)
                          With .Helpers.HTMLTag("span")
                            .AddClass("bold")
                            .Helpers.HTML("Disciplines")
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 1)
                          With .Helpers.HTMLTag("span")
                            .AddClass("bold")
                            .Helpers.HTML("Crew Type")
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 1)
                          With .Helpers.HTMLTag("span")
                            .AddClass("bold")
                            .Helpers.HTML("City")
                          End With
                        End With
                        With .Helpers.BootstrapDivColumn(12, 12, 1)
                          With .Helpers.HTMLTag("span")
                            .AddClass("bold")
                            .Helpers.HTML("Local?")
                          End With
                        End With
                        'With .Helpers.BootstrapDivColumn(12, 12, 1)
                        '  With .Helpers.HTMLTag("span")
                        '    .AddClass("bold")
                        '    .Helpers.HTML("Clashes")
                        '  End With
                        'End With
                      End With
                    End With
                    With .Helpers.Div
                      With .Helpers.ForEachTemplate(Of OBLib.Productions.Schedules.HRProductionSchedule)(Function(vm As OBLib.Productions.Areas.ProductionSystemArea) vm.HRProductionScheduleList)
                        With .Helpers.DivC("HRProductionScheduleContainer")
                          .AddBinding(Singular.Web.KnockoutBindingString.if, Function(d As OBLib.Productions.Schedules.HRProductionSchedule) d.Visible)
                          With .Helpers.BootstrapDivColumn(12, 12, 12)
                            .AddClass("HRProductionSchedule")
                            With .Helpers.BootstrapDivColumn(12, 12, 1)
                              'With .Helpers.BootstrapButton(, "Saved Successfully", "btn-xs btn-success", "glyphicon-floppy-saved", Singular.Web.PostBackType.None, False, , , )
                              '  .Button.AddBinding(Singular.Web.KnockoutBindingString.visibleA, "$data.SaveSucceededVisible()")
                              'End With
                              'With .Helpers.BootstrapButton(, "Saved Failed", "btn-xs btn-danger", "glyphicon-floppy-remove", Singular.Web.PostBackType.None, False, , , )
                              '  .Button.AddBinding(Singular.Web.KnockoutBindingString.visibleA, "$data.SaveFailedVisible()")
                              'End With
                              .Helpers.BootstrapButton(, "Edit", "btn-primary", "glyphicon-edit", Singular.Web.PostBackType.None, False, , , "GetScheduleFromServer($data)")
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 3)
                              With .Helpers.DivC("field-box")
                                With .Helpers.Div
                                  With .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.HumanResource)
                                    .AddClass("bold")
                                  End With
                                End With
                              End With
                              '.Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.HumanResource)
                              'With .Helpers.DivC("field-box")
                              '  With .Helpers.DivC("input-group")
                              '    With .Helpers.HTMLTag("span")
                              '      .AddClass("input-group-btn")
                              '    End With
                              '    With .Helpers.EditorFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.HumanResource)
                              '      .AddClass("form-control bold")
                              '      .AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                              '    End With
                              '  End With
                              'End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 2)
                              With .Helpers.DivC("field-box")
                                With .Helpers.Div
                                  .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.Disciplines).AddClass("bold")
                                End With
                              End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 1)
                              With .Helpers.DivC("field-box")
                                With .Helpers.Div
                                  .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.CrewType).AddClass("bold")
                                End With
                              End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 1)
                              With .Helpers.DivC("field-box")
                                With .Helpers.Div
                                  .Helpers.ReadOnlyFor(Function(phr As OBLib.Productions.Schedules.HRProductionSchedule) phr.CityCode).AddClass("bold")
                                End With
                              End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 1)
                              With .Helpers.DivC("field-box")
                                With .Helpers.Div
                                  With .Helpers.BootstrapStateButton("", "", "HRLocalCss($data)", "HRLocalHtml($data)", False)
                                    .Button.AddClass("bold")
                                    .Button.AddBinding(Singular.Web.KnockoutBindingString.enable, Function(d) False)
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 1)
                            End With
                          End With
                          With .Helpers.BootstrapDivColumn(12, 12, 12)
                            .AddClass("HRProductionScheduleDetailList")
                            .AddBinding(Singular.Web.KnockoutBindingString.if, Function(d As OBLib.Productions.Schedules.HRProductionSchedule) d.ScheduleVisible)
                            '.Style.Width = "95%"
                            '.AddClass("pull-right")
                            With .Helpers.BootstrapDivColumn(12, 12, 12)
                              .AddClass("top-buffer-10")
                              With .Helpers.BootstrapDivColumn(12, 12, 7)
                                .Helpers.BootstrapButton(, "Save", "btn-xs btn-success", "glyphicon-floppy-disk", Singular.Web.PostBackType.None, False, , , "SaveSchedule($data)")
                                '.Helpers.BootstrapButton(, "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "SetupUnSelectedTimelineList($data)")
                                '.Helpers.BootstrapButton(, "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "")
                                .Helpers.BootstrapButton(, "Delete Selected", "btn-xs btn-danger", "glyphicon-trash", Singular.Web.PostBackType.None, False, , , "DeleteSelectedScheduleDetails($data)")
                              End With
                              With .Helpers.BootstrapDivColumn(12, 12, 5)
                                .Helpers.BootstrapButton("", "Add Seleted", "btn-xs btn-primary", "glyphicon-plus", Singular.Web.PostBackType.None, False, , , "AddSelectedTimelines($data)")
                              End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 7)
                              With .Helpers.DivC("well well-xs")
                                With .Helpers.BootstrapTableFor(Of OBLib.Productions.Schedules.HRProductionScheduleDetail)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.HRProductionScheduleDetailListClient, False, False, "")
                                  With .FirstRow
                                    With .AddColumn("Select")
                                      .Helpers.BootstrapStateButton("", "DetailSelectClick($data)", "DetailSelectCss($data)", "DetailSelectHtml($data)", False)
                                    End With
                                    With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.ProductionTimelineType)
                                      '.Style.Width = "15%"
                                    End With
                                    With .AddColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.StartDateTime)
                                      .AddClass("Date")
                                      '.Style.Width = "10%"
                                    End With
                                    With .AddColumn("Start Time")
                                      .Helpers.TimeEditorFor(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.StartDateTime)
                                      .AddClass("Time")
                                      '.Style.Width = "10%"
                                    End With
                                    With .AddColumn("End Time")
                                      .Helpers.TimeEditorFor(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.EndDateTime)
                                      .AddClass("Time")
                                      '.Style.Width = "10%"
                                    End With
                                    With .AddColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.Detail)
                                      '.Style.Width = "20%"
                                    End With
                                    With .AddReadOnlyColumn(Function(c As OBLib.Productions.Schedules.HRProductionScheduleDetail) c.Clash)
                                      .AddClass("Clash text-danger")
                                      '.Style.Width = "35%"
                                    End With
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.BootstrapDivColumn(12, 12, 5)
                              With .Helpers.Div 'C("scroll-content")
                                With .Helpers.BootstrapTableFor(Of OBLib.Helpers.Templates.UnSelectedTimeline)(Function(v As OBLib.Productions.Schedules.HRProductionSchedule) v.UnSelectedProductionTimelineList, False, False, "$data.Visible()")
                                  With .FirstRow
                                    With .AddColumn("Select")
                                      .Helpers.BootstrapStateButton("", "NewTimelineSelectClick($data)", "NewTimelineSelectCss($data)", "NewTimelineSelectHtml($data)", False)
                                    End With
                                    .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.ProductionTimelineType)
                                    .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.StartDate)
                                    .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.StartDateTime)
                                    .AddReadOnlyColumn(Function(d As OBLib.Helpers.Templates.UnSelectedTimeline) d.EndDateTime)
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End If
              End With
            End With
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    Public Class ProductionComments
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        '-------------------------------------------Production Comment Section--------------------------------------------
        With Helpers.DivC("row ProductionCommentSectionList")
          .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.CommentSectionTabVisible)
          .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.CommentSectionTabVisible)
          With .Helpers.BootstrapPanel
            With .PanelHeading
              .Helpers.HTML("Production Comments")
            End With
            With .PanelBody
              If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
                With .Helpers.BootstrapTableFor(Of OBLib.Productions.ProductionCommentSection)(Function(c As OBLib.Productions.Old.Production) c.ProductionCommentSectionList, False, False, "")
                  With .FirstRow
                    .AddReadOnlyColumn(Function(c) c.ProductionCommentReportSectionID)
                  End With
                  ''''ProductionCommentHeaderList
                  With .AddChildTable(Of OBLib.Productions.ProductionCommentHeader)(Function(e) e.ProductionCommentHeaderList, False, False, "")
                    .Style.Width = "90%"
                    .AddClass("pull-right")
                    With .FirstRow
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.ProductionCommentHeader) d.ProductionCommentHeader)
                    End With
                    ''''ProductionCommentList
                    With .AddChildTable(Of OBLib.Productions.ProductionComment)(Function(d) d.ProductionCommentList, False, False, "")
                      .Style.Width = "90%"
                      .AddClass("pull-right")
                      With .FirstRow
                        .AddReadOnlyColumn(Function(d) d.ProductionComment)
                        .AddReadOnlyColumn(Function(d) d.AdditionalComment)
                      End With
                    End With
                  End With

                End With
              Else
                With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewProductionCommentSection()")
                  '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionCommentSection()")
                End With
                .Helpers.DivC("top-buffer-10")
                With .Helpers.BootstrapTableFor(Of OBLib.Productions.ProductionCommentSection)(Function(c As OBLib.Productions.Old.Production) c.ProductionCommentSectionList, False, True, "")
                  '.AddNewButton.AddClass("btn-sm")
                  .RemoveButton.RemoveClass("btn-primary")
                  .RemoveButton.AddClass("btn-sm btn-danger")
                  With .FirstRow
                    .AddColumn(Function(c) c.ProductionCommentReportSectionID)
                  End With
                  With .AddChildTable(Of OBLib.Productions.ProductionCommentHeader)(Function(e) e.ProductionCommentHeaderList, True, True, "")
                    .Style.Width = "90%"
                    .AddClass("pull-right")
                    .AddNewButton.AddClass("btn-sm")
                    .RemoveButton.RemoveClass("btn-primary")
                    .RemoveButton.AddClass("btn-sm btn-danger")
                    With .FirstRow
                      .AddColumn(Function(d As OBLib.Productions.ProductionCommentHeader) d.ProductionCommentHeader)
                    End With

                    With .AddChildTable(Of OBLib.Productions.ProductionComment)(Function(d) d.ProductionCommentList, True, True, "")
                      .Style.Width = "90%"
                      .AddClass("pull-right")
                      .AddNewButton.AddClass("btn-sm")
                      .RemoveButton.RemoveClass("btn-primary")
                      .RemoveButton.AddClass("btn-sm btn-danger")
                      .AddNewButton.AddBinding(Singular.Web.KnockoutBindingString.click, "AddNewPC($data)")
                      .RemoveButton.AddBinding(Singular.Web.KnockoutBindingString.click, "RemovePC($data)")
                      With .FirstRow
                        .AddColumn(Function(d) d.ProductionComment)
                        .AddColumn(Function(d) d.AdditionalComment)
                      End With
                    End With
                  End With

                End With
              End If
            End With
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    'Public Class TravelRequisitions
    '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

    '  Private ViewModel As ProductionVM
    '  Private Production As OBLib.Productions.Old.Production

    '  Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
    '    Me.ViewModel = ViewModel
    '    Me.Production = Production
    '  End Sub

    '  Protected Overrides Sub Setup()
    '    MyBase.Setup()

    '    '-------------------------------------------Travel Requisition--------------------------------------------
    '    With Helpers.DivC("row TravelRequisitionList")
    '      .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.TravelTabVisible)
    '      .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.TravelTabVisible)
    '      With .Helpers.BootstrapPanel
    '        With .PanelHeading
    '          .Helpers.HTML("Travel Requisition")
    '        End With
    '        With .PanelBody
    '          If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
    '            With .Helpers.BootstrapTableFor(Of OBLib.Productions.TravelRequisition)(Function(c As OBLib.Productions.Old.Production) c.TravelRequisitionList, False, False, "")
    '              With .FirstRow
    '                With .AddColumn()
    '                  .Style.Width = "105px"
    '                  With .Helpers.LinkFor(Function(c) "../Productions/Travel/CrewMembers.aspx?TravelRequisitionID=" & c.TravelRequisitionID, , "", "")
    '                    .AddClass("btn btn-primary btn-xs")
    '                    With .Helpers.HTMLTag("span")
    '                      .AddClass("glyphicon glyphicon-search")
    '                    End With
    '                    .Helpers.HTML("View Travel")
    '                  End With
    '                End With
    '                .AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.ProductionDescription)
    '                .AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.System)
    '                .AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.CreatedByUser)
    '              End With
    '            End With
    '          Else
    '            With .Helpers.BootstrapButton("", "Add New", "btn-sm btn-primary", "glyphicon-plus", Singular.Web.PostBackType.Ajax, False, , , "AddNewTravelRequisition()")
    '              '.Button.AddBinding(Singular.Web.KnockoutBindingString.enable, "CanAddProductionCommentSection()")
    '            End With
    '            .Helpers.DivC("top-buffer-10")
    '            With .Helpers.BootstrapTableFor(Of OBLib.Productions.TravelRequisition)(Function(c As OBLib.Productions.Old.Production) c.TravelRequisitionList, False, True, "")
    '              '.AddNewButton.AddClass("btn-sm")
    '              .RemoveButton.RemoveClass("btn-primary")
    '              .RemoveButton.AddClass("btn-sm btn-danger")
    '              With .FirstRow
    '                With .AddColumn()
    '                  .Style.Width = "105px"
    '                  With .Helpers.LinkFor(Function(c) "../Productions/Travel/CrewMembers.aspx?TravelRequisitionID=" & c.TravelRequisitionID, , "", "")
    '                    .AddClass("btn btn-primary btn-xs")
    '                    With .Helpers.HTMLTag("span")
    '                      .AddClass("glyphicon glyphicon-search")
    '                    End With
    '                    .Helpers.HTML("View Travel")
    '                  End With
    '                End With
    '                .AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.ProductionDescription)
    '                .AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.System)
    '                .AddReadOnlyColumn(Function(c As OBLib.Productions.TravelRequisition) c.CreatedByUser)
    '              End With
    '            End With
    '          End If
    '        End With
    '      End With
    '    End With

    '  End Sub

    '  Protected Overrides Sub Render()
    '    MyBase.Render()
    '    RenderChildren()
    '  End Sub

    'End Class

    Public Class ProductionCorrespondence
      Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

      Private ViewModel As ProductionVM
      Private Production As OBLib.Productions.Old.Production

      Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
        Me.ViewModel = ViewModel
        Me.Production = Production
      End Sub

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.DivC("row CorrespondenceTab")
          .AddBinding(Singular.Web.KnockoutBindingString.if, Function(vm) ViewModel.CorrespondenceTabVisible)
          .AddBinding(Singular.Web.KnockoutBindingString.visibleA, Function(vm) ViewModel.CorrespondenceTabVisible)

          With .Helpers.BootstrapDivColumn(12, 12, 12)
            If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Documents")
                End With
                With .PanelBody
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionDocument)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionDocumentList, False, False, "")
                    With .FirstRow
                      '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.SystemID)
                      '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.ProductionAreaID)
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.DocumentTypeID)
                      With .AddColumn("")
                        .Helpers.DocumentDownloader()
                      End With
                    End With
                  End With
                End With
              End With
            Else
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Documents")
                End With
                With .PanelBody
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionDocument)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionDocumentList, True, True, "")
                    .AddNewButton.AddClass("btn-sm")
                    .RemoveButton.RemoveClass("btn-primary")
                    .RemoveButton.AddClass("btn-sm btn-danger")
                    With .FirstRow
                      '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.SystemID)
                      '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.ProductionAreaID)
                      .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionDocument) d.DocumentTypeID)
                      With .AddColumn("")
                        .Helpers.DocumentManager()
                      End With
                    End With
                  End With
                End With
              End With
            End If
          End With

          With .Helpers.BootstrapDivColumn(12, 12, 12)
            If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Comments")
                End With
                With .PanelBody
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionGeneralComment)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionGeneralCommentList, False, False, "")
                    With .FirstRow
                      '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.SystemID)
                      '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.ProductionAreaID)
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.CommentDate)
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.Comment)
                    End With
                  End With
                End With
              End With
            Else
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Comments")
                End With
                With .PanelBody
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionGeneralComment)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionGeneralCommentList, True, True, "")
                    .AddNewButton.AddClass("btn-sm")
                    .RemoveButton.RemoveClass("btn-primary")
                    .RemoveButton.AddClass("btn-sm btn-danger")
                    With .FirstRow
                      '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.SystemID)
                      '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.ProductionAreaID)
                      .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.CommentDate)
                      .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionGeneralComment) d.Comment)
                    End With
                  End With
                End With
              End With
            End If
          End With

          With .Helpers.BootstrapDivColumn(12, 12, 12)
            If ViewModel.CurrentProduction.Reconciled Or ViewModel.CurrentProduction.Cancelled Then
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Petty Cash")
                End With
                With .PanelBody
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionPettyCash)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionPettyCashList, False, False, "")
                    With .FirstRow
                      '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.SystemID)
                      '.AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionAreaID)
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionPettyCashTypeID)
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Amount)
                      .AddReadOnlyColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Description)
                    End With
                  End With
                End With
              End With
            Else
              With .Helpers.BootstrapPanel
                With .PanelHeading
                  .Helpers.HTML("Petty Cash")
                End With
                With .PanelBody
                  With .Helpers.BootstrapTableFor(Of OBLib.Productions.Correspondence.ProductionPettyCash)(Function(vm As OBLib.Productions.Old.Production) vm.ProductionPettyCashList, True, True, "")
                    .AddNewButton.AddClass("btn-sm")
                    .RemoveButton.RemoveClass("btn-primary")
                    .RemoveButton.AddClass("btn-sm btn-danger")
                    With .FirstRow
                      '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.SystemID)
                      '.AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionAreaID)
                      .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.ProductionPettyCashTypeID)
                      .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Amount)
                      .AddColumn(Function(d As OBLib.Productions.Correspondence.ProductionPettyCash) d.Description)
                    End With
                  End With
                End With
              End With
            End If
          End With
        End With

      End Sub

      Protected Overrides Sub Render()
        MyBase.Render()
        RenderChildren()
      End Sub

    End Class

    'Public Class ProductionOutsourceServices
    '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

    '  Private ViewModel As ProductionVM
    '  Private Production As OBLib.Productions.Old.Production

    '  Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
    '    Me.ViewModel = ViewModel
    '    Me.Production = Production
    '  End Sub

    '  Protected Overrides Sub Setup()
    '    MyBase.Setup()

    '  End Sub

    '  Protected Overrides Sub Render()
    '    MyBase.Render()
    '    RenderChildren()
    '  End Sub

    'End Class

    'Public Class ProductionOutsourceServices
    '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

    '  Private ViewModel As ProductionVM
    '  Private Production As OBLib.Productions.Old.Production

    '  Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
    '    Me.ViewModel = ViewModel
    '    Me.Production = Production
    '  End Sub

    '  Protected Overrides Sub Setup()
    '    MyBase.Setup()

    '  End Sub

    '  Protected Overrides Sub Render()
    '    MyBase.Render()
    '    RenderChildren()
    '  End Sub

    'End Class

    'Public Class ProductionOutsourceServices
    '  Inherits Singular.Web.Controls.HelperControls.HelperBase(Of OBLib.Productions.Old.Production)

    '  Private ViewModel As ProductionVM
    '  Private Production As OBLib.Productions.Old.Production

    '  Public Sub New(ViewModel As ProductionVM, Production As OBLib.Productions.Old.Production)
    '    Me.ViewModel = ViewModel
    '    Me.Production = Production
    '  End Sub

    '  Protected Overrides Sub Setup()
    '    MyBase.Setup()

    '  End Sub

    '  Protected Overrides Sub Render()
    '    MyBase.Render()
    '    RenderChildren()
    '  End Sub

    'End Class

  End Class

End Namespace

