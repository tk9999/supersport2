﻿
<ServiceContract()>
Public Interface ISoberWcfService

  <OperationContract()>
  Function GetOpenMonthMealReimbursements(ByVal CloseMonth As Boolean) As List(Of MealReimbursement)

  <OperationContract()>
  Function GetSatOpsBookings(StartDate As Date?, EndDate As Date?, GenRef As Int64?, Key As String) As List(Of ROEquipmentScheduleICR)

  <OperationContract()>
  Function GetSatOpsBooking(FeedID As Integer?, Key As String) As ROEquipmentScheduleICR

  <OperationContract()>
  Function GetAudioSettings(Key As String) As List(Of ROAudioSetting)

  <OperationContract()>
  Function GetTurnAroundPoints(Key As String) As List(Of ROTurnAroundPoint)

  <OperationContract()>
  Function GetFeedTypes(Key As String) As List(Of ROFeedType)

  <OperationContract()>
  Function GetStatuses(Key As String) As List(Of ROStatus)

  <OperationContract()>
  Function GetSatOpsEquipment(Key As String) As List(Of ROSatOpsEquipment)

  <OperationContract()>
  Sub EscalateFeed(Key As String, FeedID As Integer, ByUsername As String)

  <OperationContract()>
  Function GetSatOpsBookingsByFeedIDList(CSVFeedIDs As String, Key As String) As List(Of ROEquipmentScheduleICR)

  '<OperationContract()>
  'Function EscalateFeed2(Key As String, FeedID As Integer, ByUsername As String) As String()

End Interface

#Region " Meals "

<DataContract()>
Public Class MealReimbursement

  <DataMember()>
  Public Property FromModule() As String

  <DataMember()>
  Public Property HumanResourceID() As Integer

  <DataMember()>
  Public Property FirstName() As String

  <DataMember()>
  Public Property Surname() As String

  <DataMember()>
  Public Property EmployeeCode() As String

  <DataMember()>
  Public Property IDNo() As String

  <DataMember()>
  Public Property PeriodStart As DateTime

  <DataMember()>
  Public Property PeriodEnd As DateTime

  <DataMember()>
  Public Property AuthorisedHoursWorked() As Decimal

  <DataMember()>
  Public Property NumberOfMealVouchers() As Integer

  <DataMember()>
  Public Property NumberOfSpecialMealVouchers() As Integer

  <DataMember()>
  Public Property AmountPayable() As Decimal

  <DataMember()>
  Public Property SubDepartment() As String

  <DataMember()>
  Public Property Area() As String

End Class

#End Region

#Region " ICR Integration "

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROEquipmentScheduleICR

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property Debtor As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property DebtorID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EndDateTime As DateTime?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EndDateTimeBuffer As DateTime?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentName As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentScheduleComments As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentScheduleID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentScheduleTitle As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentScheduleType As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentScheduleTypeID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentServiceID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedList As List(Of ROESICRFeed)

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property StartDateTime As DateTime?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property StartDateTimeBuffer As DateTime?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property StatusID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property StatusName As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeed

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentScheduleID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedDescription As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedType As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedTypeID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedDefaultAudioList As List(Of ROESICRFeedDefaultAudio)

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedIngestInstructionList As List(Of ROESICRFeedIngestInstruction)

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedPathList As List(Of ROESICRFeedPath)

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedProductionList As List(Of ROESICRFeedProduction)

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedTurnAroundPointList As List(Of ROESICRFeedTurnAroundPoint)

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedProduction

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedProductionID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property KickOffTime As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ProcessingErrorText As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public ProductionID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ProductionVenue As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedProductionAudioList As List(Of ROESICRFeedProductionAudio)

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property SynergyGenRefNo As Int64?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property Title As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedDefaultAudio

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedDefaultAudioSettingID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property AudioSettingID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property AudioSetting As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ChannelNumber As Integer

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedIngestInstruction

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedIngestInstructionID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public FeedID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property IngestInstruction As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedPath

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedPathID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property PathComments As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property RoomScheduleID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property Room As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property RoomScheduleTitle As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property SynergyGenRefNo As Int64?

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedProductionAudio

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedProductionAudioSettingID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedProductionID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property AudioSettingID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property AudioSetting As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ChannelNumber As Integer

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedTurnAroundPoint

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedTurnAroundPointID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPointID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPoint As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPointOrder As Integer

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROESICRFeedTurnAroundPointContactList As List(Of ROESICRFeedTurnAroundPointContact)

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROESICRFeedTurnAroundPointContact

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedTurnAroundPointContactID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedTurnAroundPointID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPointContactID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ContactName As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ContactNumber As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROTurnAroundPoint

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPointID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPoint As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property Country As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property City As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ROTurnAroundPointContactList As List(Of ROTurnAroundPointContact)

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROTurnAroundPointContact

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPointID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property TurnAroundPointContactID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ContactName As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ContactNumber As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROAudioSetting

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property AudioSettingID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property AudioSetting As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property IsOld As Boolean

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROFeedType

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedTypeID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property FeedType As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROStatus

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property StatusID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property StatusName As String

End Class

<System.Runtime.Serialization.DataContractAttribute()> _
Partial Public Class ROSatOpsEquipment

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentDescription As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentTypeID As Integer?

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property EquipmentType As String

  '<System.Runtime.Serialization.DataMemberAttribute()> _
  'Public Property EquipmentSubType As String

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ActiveInd As Boolean

  <System.Runtime.Serialization.DataMemberAttribute()> _
  Public Property ParentEquipmentID As Integer?

End Class

#End Region
