﻿Imports System.IdentityModel.Policy
Imports System.Security.Principal

Public Class AuthorizationPolicy
  Implements IAuthorizationPolicy

  Private _ID As Guid = Guid.NewGuid()

  Public ReadOnly Property Id As String Implements System.IdentityModel.Policy.IAuthorizationComponent.Id
    Get
      Return _ID.ToString()
    End Get
  End Property

  Public Function Evaluate(evaluationContext As System.IdentityModel.Policy.EvaluationContext, ByRef state As Object) As Boolean Implements System.IdentityModel.Policy.IAuthorizationPolicy.Evaluate

    ' get the authenticated client identity
    Dim client = GetClientIdentity(evaluationContext)
    ' set the custom principal
    evaluationContext.Properties("Principal") = New CustomPrincipal(client)
    Return True

  End Function

  Private Function GetClientIdentity(evaluationContext As EvaluationContext) As IIdentity

    Dim obj As Object = Nothing
    If Not evaluationContext.Properties.TryGetValue("Identities", obj) Then
      ' Throw New Exception("No Identity found")
      Return Nothing
    Else

      Dim identities As IList(Of IIdentity) = obj
      If (identities Is Nothing OrElse identities.Count <= 0) Then
        Throw New Exception("No Identity found")
      End If
      Return identities(0)

    End If

  End Function

  Public ReadOnly Property Issuer As System.IdentityModel.Claims.ClaimSet Implements System.IdentityModel.Policy.IAuthorizationPolicy.Issuer
    Get
      Throw New NotImplementedException
    End Get
  End Property

End Class
