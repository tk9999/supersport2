﻿Imports System.IdentityModel.Selectors
Imports System.IdentityModel.Tokens

Public Class UserAuthentication
  Inherits UserNamePasswordValidator

  Public Overrides Sub Validate(userName As String, password As String)

    If ValidateCredentials(userName, password) Then
      ' all good
      ' Console.WriteLine("Authentic User")
    Else
      Throw New FaultException("Invalid username or password")
    End If

  End Sub

  Public Shared Function ValidateCredentials(userName As String, password As String) As Boolean

    If (userName = "SSFinance" AndAlso password = "$uper$portF!n@nce") Then
      Return True
    ElseIf (userName = "SSICR" AndAlso password = "!CR!nt3rgr@t!0n") Then
      Return False
    Else
      Return False
    End If

  End Function

End Class
