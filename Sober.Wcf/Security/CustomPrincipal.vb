﻿Imports System.Security.Principal

Public Class CustomPrincipal
  Implements IPrincipal

  Public Function IsInRole(role As String) As Boolean Implements System.Security.Principal.IPrincipal.IsInRole

    Return mRoles.Contains(role)

  End Function

  Private mIdentity As IIdentity

  Public Sub New(client As IIdentity)

    mIdentity = client

    If mIdentity IsNot Nothing Then
      If (Me.Identity.Name = "SSFinance") Then
        mRoles = {"Meal Reimbursements"}
      Else
        mRoles = {"USER"}
      End If
    End If

  End Sub

  Private mRoles As String()

  Public ReadOnly Property Identity As IIdentity Implements IPrincipal.Identity
    Get
      Return mIdentity
    End Get
  End Property

End Class
