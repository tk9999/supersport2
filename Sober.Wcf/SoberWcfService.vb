﻿Imports OBLib.MealReimbursements
Imports System.Security.Permissions
Imports Singular.Web
Imports System.ServiceModel.Channels


Public Class SoberWcfService
  Implements ISoberWcfService

  Public Sub New()

    ' setup the connection
    Singular.Settings.SetConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings("OBSql").ConnectionString)

  End Sub

  <PrincipalPermission(SecurityAction.Demand, Role:="Meal Reimbursements")>
  Public Function GetOpenMonthMealReimbursements(CloseMonth As Boolean) As List(Of MealReimbursement) Implements ISoberWcfService.GetOpenMonthMealReimbursements

    Dim MonthList = OBLib.NSWTimesheets.MRMonthList.GetMRMonthList()

    Dim OpenMonth = MonthList.GetOpenMonth()

    Dim CanCloseFromDate = CDate(OpenMonth.MonthEndDate).AddDays(2)

    If CloseMonth AndAlso Now.Date < CanCloseFromDate Then
      Throw New FaultException(String.Format("Cannot close {0} before {1}", CDate(OpenMonth.MonthEndDate).ToString("MMM-yy"), CanCloseFromDate.ToString("dd-MMM-yy")))
    End If

    ' load the open months data
    Dim list = ROMealReimbursementList.GetROMealReimbursementList(CloseMonth)

    Dim ReturnList = list.Select(Function(romr)

                                   Dim mr As New MealReimbursement()
                                   Singular.Reflection.CopyCommonProperties(romr, mr)
                                   Return mr

                                 End Function).ToList()

    Return ReturnList

  End Function

  Public Function GetSatOpsBookings(StartDate As Date?, EndDate As Date?, GenRef As Long?, Key As String) As List(Of ROEquipmentScheduleICR) Implements ISoberWcfService.GetSatOpsBookings


    Dim messageProperty As RemoteEndpointMessageProperty = Nothing

    Try
      messageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    Catch ex As Exception
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, "Unknown", "GetSatOpsBookings - Set Message Property", "Start Date: " & StartDate.Value.ToString("dd MMM yyyy") & ", End Date: " & EndDate.Value.ToString("dd MMM yyyy"))
    End Try

    Try
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsBookings - Start", "Start Date: " & StartDate.Value.ToString("dd MMM yyyy") & ", End Date: " & EndDate.Value.ToString("dd MMM yyyy"))
      Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
      If IsValidKey Then
        Dim List As OBLib.SatOps.Integration.ROEquipmentScheduleICRList = OBLib.SatOps.Integration.ROEquipmentScheduleICRList.GetROEquipmentScheduleICRList(StartDate, EndDate,
                                                                                                                                                            GenRef, Nothing)
        Dim Temp As New List(Of ROEquipmentScheduleICR)
        List.ToList.ForEach(Sub(bkng)

                              'Equipment Schedule
                              Dim newItem As New Sober.Wcf.ROEquipmentScheduleICR
                              Singular.Reflection.CopyCommonProperties(bkng, newItem, False)

                              'Feed
                              For Each fd As OBLib.SatOps.Integration.ROESICRFeed In bkng.ROESICRFeedList
                                Dim newFeed As New Sober.Wcf.ROESICRFeed
                                newFeed.ROESICRFeedDefaultAudioList = New List(Of Sober.Wcf.ROESICRFeedDefaultAudio)
                                newFeed.ROESICRFeedIngestInstructionList = New List(Of Sober.Wcf.ROESICRFeedIngestInstruction)
                                newFeed.ROESICRFeedProductionList = New List(Of Sober.Wcf.ROESICRFeedProduction)
                                newFeed.ROESICRFeedPathList = New List(Of Sober.Wcf.ROESICRFeedPath)
                                newFeed.ROESICRFeedTurnAroundPointList = New List(Of Sober.Wcf.ROESICRFeedTurnAroundPoint)
                                Singular.Reflection.CopyCommonProperties(fd, newFeed, False)
                                newItem.ROESICRFeedList = New List(Of Sober.Wcf.ROESICRFeed)
                                newItem.ROESICRFeedList.Add(newFeed)
                                'Feed Audio
                                newFeed.ROESICRFeedDefaultAudioList.Clear()
                                For Each fda As OBLib.SatOps.Integration.ROESICRFeedDefaultAudio In fd.ROESICRFeedDefaultAudioList
                                  Dim newFeedAudio As New Sober.Wcf.ROESICRFeedDefaultAudio
                                  Singular.Reflection.CopyCommonProperties(fda, newFeedAudio, False)
                                  newFeed.ROESICRFeedDefaultAudioList.Add(newFeedAudio)
                                Next
                                'Feed TurnAround
                                newFeed.ROESICRFeedTurnAroundPointList.Clear()
                                For Each fdt As OBLib.SatOps.Integration.ROESICRFeedTurnAroundPoint In fd.ROESICRFeedTurnAroundPointList
                                  Dim newFeedTap As New Sober.Wcf.ROESICRFeedTurnAroundPoint
                                  Singular.Reflection.CopyCommonProperties(fdt, newFeedTap, False)
                                  newFeed.ROESICRFeedTurnAroundPointList.Add(newFeedTap)
                                  newFeedTap.ROESICRFeedTurnAroundPointContactList = New List(Of ROESICRFeedTurnAroundPointContact)
                                  For Each fdtc As OBLib.SatOps.Integration.ROESICRFeedTurnAroundPointContact In fdt.ROESICRFeedTurnAroundPointContactList
                                    Dim newFeedTapC As New Sober.Wcf.ROESICRFeedTurnAroundPointContact
                                    Singular.Reflection.CopyCommonProperties(fdtc, newFeedTapC, False)
                                    newFeedTap.ROESICRFeedTurnAroundPointContactList.Add(newFeedTapC)
                                  Next
                                Next
                                'Feed Production
                                newFeed.ROESICRFeedProductionList.Clear()
                                For Each fdPrd As OBLib.SatOps.Integration.ROESICRFeedProduction In fd.ROESICRFeedProductionList
                                  Dim newFeedPrd As New Sober.Wcf.ROESICRFeedProduction
                                  newFeedPrd.ROESICRFeedProductionAudioList = New List(Of Sober.Wcf.ROESICRFeedProductionAudio)
                                  Singular.Reflection.CopyCommonProperties(fdPrd, newFeedPrd, False)
                                  newFeed.ROESICRFeedProductionList.Add(newFeedPrd)
                                  'Feed Production Audio
                                  newFeedPrd.ROESICRFeedProductionAudioList.Clear()
                                  For Each fPRDda As OBLib.SatOps.Integration.ROESICRFeedProductionAudio In fdPrd.ROESICRFeedProductionAudioList
                                    Dim newFeedProdAudio As New Sober.Wcf.ROESICRFeedProductionAudio
                                    Singular.Reflection.CopyCommonProperties(fPRDda, newFeedProdAudio, False)
                                    newFeedPrd.ROESICRFeedProductionAudioList.Add(newFeedProdAudio)
                                  Next
                                Next
                                'Feed Ingest Instruction
                                newFeed.ROESICRFeedIngestInstructionList.Clear()
                                For Each fii As OBLib.SatOps.Integration.ROESICRFeedIngestInstruction In fd.ROESICRFeedIngestInstructionList
                                  Dim newFeedII As New Sober.Wcf.ROESICRFeedIngestInstruction
                                  Singular.Reflection.CopyCommonProperties(fii, newFeedII, False)
                                  newFeed.ROESICRFeedIngestInstructionList.Add(newFeedII)
                                Next
                                'Feed Path
                                newFeed.ROESICRFeedPathList.Clear()
                                For Each fpa As OBLib.SatOps.Integration.ROESICRFeedPath In fd.ROESICRFeedPathList
                                  Dim newFeedPA As New Sober.Wcf.ROESICRFeedPath
                                  Singular.Reflection.CopyCommonProperties(fpa, newFeedPA, False)
                                  newFeed.ROESICRFeedPathList.Add(newFeedPA)
                                Next
                              Next

                              Temp.Add(newItem)

                            End Sub)
        OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsBookings - End", "Start Date: " & StartDate.Value.ToString("dd MMM yyyy") & ", End Date: " & EndDate.Value.ToString("dd MMM yyyy"))
        Return Temp
      End If
    Catch ex As Exception
      OBLib.Helpers.ErrorHelpers.LogClientError("ICR", "ICRDashboardIntegration", "GetSatOpsBookings", ex.Message _
                                                & vbCrLf & vbCrLf & _
                                                "SD: " & IIf(StartDate Is Nothing, "No Start Date", StartDate.ToString) _
                                                & vbCrLf & vbCrLf & _
                                                "ED: " & IIf(EndDate Is Nothing, "No End Date", EndDate.ToString) _
                                                & vbCrLf & vbCrLf & _
                                                "GR: " & IIf(GenRef Is Nothing, "No GenRef", GenRef.ToString) _
                                                & vbCrLf & vbCrLf & _
                                                "Key: " & IIf(Key Is Nothing, "No Key", Key.ToString))
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsBookings - Failed", "Start Date: " & StartDate.Value.ToString("dd MMM yyyy") & ", End Date: " & EndDate.Value.ToString("dd MMM yyyy"))
      Return Nothing
    End Try

    Return Nothing

  End Function

  Public Function GetSatOpsBookingsByFeedIDList(CSVFeedIDs As String, Key As String) As List(Of ROEquipmentScheduleICR) Implements ISoberWcfService.GetSatOpsBookingsByFeedIDList

    Try
      Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
      If IsValidKey Then

        Dim FeedIDs As List(Of Integer) = CSVFeedIDs.Split(","c).Select(Function(d) CType(d, Integer)).ToList
        Dim List As OBLib.SatOps.Integration.ROEquipmentScheduleICRList = OBLib.SatOps.Integration.ROEquipmentScheduleICRList.GetROEquipmentScheduleICRList(FeedIDs)
        Dim Temp As New List(Of ROEquipmentScheduleICR)
        List.ToList.ForEach(Sub(bkng)

                              'Equipment Schedule
                              Dim newItem As New Sober.Wcf.ROEquipmentScheduleICR
                              Singular.Reflection.CopyCommonProperties(bkng, newItem, False)

                              'Feed
                              For Each fd As OBLib.SatOps.Integration.ROESICRFeed In bkng.ROESICRFeedList
                                Dim newFeed As New Sober.Wcf.ROESICRFeed
                                newFeed.ROESICRFeedDefaultAudioList = New List(Of Sober.Wcf.ROESICRFeedDefaultAudio)
                                newFeed.ROESICRFeedIngestInstructionList = New List(Of Sober.Wcf.ROESICRFeedIngestInstruction)
                                newFeed.ROESICRFeedProductionList = New List(Of Sober.Wcf.ROESICRFeedProduction)
                                newFeed.ROESICRFeedPathList = New List(Of Sober.Wcf.ROESICRFeedPath)
                                newFeed.ROESICRFeedTurnAroundPointList = New List(Of Sober.Wcf.ROESICRFeedTurnAroundPoint)
                                Singular.Reflection.CopyCommonProperties(fd, newFeed, False)
                                newItem.ROESICRFeedList = New List(Of Sober.Wcf.ROESICRFeed)
                                newItem.ROESICRFeedList.Add(newFeed)
                                'Feed Audio
                                newFeed.ROESICRFeedDefaultAudioList.Clear()
                                For Each fda As OBLib.SatOps.Integration.ROESICRFeedDefaultAudio In fd.ROESICRFeedDefaultAudioList
                                  Dim newFeedAudio As New Sober.Wcf.ROESICRFeedDefaultAudio
                                  Singular.Reflection.CopyCommonProperties(fda, newFeedAudio, False)
                                  newFeed.ROESICRFeedDefaultAudioList.Add(newFeedAudio)
                                Next
                                'Feed TurnAround
                                newFeed.ROESICRFeedTurnAroundPointList.Clear()
                                For Each fdt As OBLib.SatOps.Integration.ROESICRFeedTurnAroundPoint In fd.ROESICRFeedTurnAroundPointList
                                  Dim newFeedTap As New Sober.Wcf.ROESICRFeedTurnAroundPoint
                                  Singular.Reflection.CopyCommonProperties(fdt, newFeedTap, False)
                                  newFeed.ROESICRFeedTurnAroundPointList.Add(newFeedTap)
                                  newFeedTap.ROESICRFeedTurnAroundPointContactList = New List(Of ROESICRFeedTurnAroundPointContact)
                                  For Each fdtc As OBLib.SatOps.Integration.ROESICRFeedTurnAroundPointContact In fdt.ROESICRFeedTurnAroundPointContactList
                                    Dim newFeedTapC As New Sober.Wcf.ROESICRFeedTurnAroundPointContact
                                    Singular.Reflection.CopyCommonProperties(fdtc, newFeedTapC, False)
                                    newFeedTap.ROESICRFeedTurnAroundPointContactList.Add(newFeedTapC)
                                  Next
                                Next
                                'Feed Production
                                newFeed.ROESICRFeedProductionList.Clear()
                                For Each fdPrd As OBLib.SatOps.Integration.ROESICRFeedProduction In fd.ROESICRFeedProductionList
                                  Dim newFeedPrd As New Sober.Wcf.ROESICRFeedProduction
                                  newFeedPrd.ROESICRFeedProductionAudioList = New List(Of Sober.Wcf.ROESICRFeedProductionAudio)
                                  Singular.Reflection.CopyCommonProperties(fdPrd, newFeedPrd, False)
                                  newFeed.ROESICRFeedProductionList.Add(newFeedPrd)
                                  'Feed Production Audio
                                  newFeedPrd.ROESICRFeedProductionAudioList.Clear()
                                  For Each fPRDda As OBLib.SatOps.Integration.ROESICRFeedProductionAudio In fdPrd.ROESICRFeedProductionAudioList
                                    Dim newFeedProdAudio As New Sober.Wcf.ROESICRFeedProductionAudio
                                    Singular.Reflection.CopyCommonProperties(fPRDda, newFeedProdAudio, False)
                                    newFeedPrd.ROESICRFeedProductionAudioList.Add(newFeedProdAudio)
                                  Next
                                Next
                                'Feed Ingest Instruction
                                newFeed.ROESICRFeedIngestInstructionList.Clear()
                                For Each fii As OBLib.SatOps.Integration.ROESICRFeedIngestInstruction In fd.ROESICRFeedIngestInstructionList
                                  Dim newFeedII As New Sober.Wcf.ROESICRFeedIngestInstruction
                                  Singular.Reflection.CopyCommonProperties(fii, newFeedII, False)
                                  newFeed.ROESICRFeedIngestInstructionList.Add(newFeedII)
                                Next
                                'Feed Path
                                newFeed.ROESICRFeedPathList.Clear()
                                For Each fpa As OBLib.SatOps.Integration.ROESICRFeedPath In fd.ROESICRFeedPathList
                                  Dim newFeedPA As New Sober.Wcf.ROESICRFeedPath
                                  Singular.Reflection.CopyCommonProperties(fpa, newFeedPA, False)
                                  newFeed.ROESICRFeedPathList.Add(newFeedPA)
                                Next
                              Next

                              Temp.Add(newItem)

                            End Sub)

        Return Temp
      End If
    Catch ex As Exception
      OBLib.Helpers.ErrorHelpers.LogClientError("ICR", "ICRDashboardIntegration", "GetSatOpsBookingsByFeedIDList", ex.Message _
                                                & vbCrLf & vbCrLf & _
                                                "FeedIDs: " & CSVFeedIDs.ToString)
      Return Nothing
    End Try

    Return Nothing

  End Function

  Public Function GetSatOpsBooking(FeedID As Integer?, Key As String) As ROEquipmentScheduleICR Implements ISoberWcfService.GetSatOpsBooking

    Dim messageProperty As RemoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    Try
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsBooking - Start", "Feed: " & FeedID.Value.ToString)
      Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
      If IsValidKey Then
        Dim List As OBLib.SatOps.Integration.ROEquipmentScheduleICRList = OBLib.SatOps.Integration.ROEquipmentScheduleICRList.GetROEquipmentScheduleICRList(Nothing, Nothing, Nothing, FeedID)
        Dim Temp As New List(Of ROEquipmentScheduleICR)
        List.ToList.ForEach(Sub(bkng)

                              'Equipment Schedule
                              Dim newItem As New Sober.Wcf.ROEquipmentScheduleICR
                              Singular.Reflection.CopyCommonProperties(bkng, newItem, False)

                              'Feed
                              For Each fd As OBLib.SatOps.Integration.ROESICRFeed In bkng.ROESICRFeedList
                                Dim newFeed As New Sober.Wcf.ROESICRFeed
                                newFeed.ROESICRFeedDefaultAudioList = New List(Of Sober.Wcf.ROESICRFeedDefaultAudio)
                                newFeed.ROESICRFeedIngestInstructionList = New List(Of Sober.Wcf.ROESICRFeedIngestInstruction)
                                newFeed.ROESICRFeedProductionList = New List(Of Sober.Wcf.ROESICRFeedProduction)
                                newFeed.ROESICRFeedPathList = New List(Of Sober.Wcf.ROESICRFeedPath)
                                newFeed.ROESICRFeedTurnAroundPointList = New List(Of Sober.Wcf.ROESICRFeedTurnAroundPoint)
                                Singular.Reflection.CopyCommonProperties(fd, newFeed, False)
                                newItem.ROESICRFeedList = New List(Of Sober.Wcf.ROESICRFeed)
                                newItem.ROESICRFeedList.Add(newFeed)
                                'Feed Audio
                                newFeed.ROESICRFeedDefaultAudioList.Clear()
                                For Each fda As OBLib.SatOps.Integration.ROESICRFeedDefaultAudio In fd.ROESICRFeedDefaultAudioList
                                  Dim newFeedAudio As New Sober.Wcf.ROESICRFeedDefaultAudio
                                  Singular.Reflection.CopyCommonProperties(fda, newFeedAudio, False)
                                  newFeed.ROESICRFeedDefaultAudioList.Add(newFeedAudio)
                                Next
                                'Feed TurnAround
                                newFeed.ROESICRFeedTurnAroundPointList.Clear()
                                For Each fdt As OBLib.SatOps.Integration.ROESICRFeedTurnAroundPoint In fd.ROESICRFeedTurnAroundPointList
                                  Dim newFeedTap As New Sober.Wcf.ROESICRFeedTurnAroundPoint
                                  Singular.Reflection.CopyCommonProperties(fdt, newFeedTap, False)
                                  newFeed.ROESICRFeedTurnAroundPointList.Add(newFeedTap)
                                  newFeedTap.ROESICRFeedTurnAroundPointContactList = New List(Of ROESICRFeedTurnAroundPointContact)
                                  For Each fdtc As OBLib.SatOps.Integration.ROESICRFeedTurnAroundPointContact In fdt.ROESICRFeedTurnAroundPointContactList
                                    Dim newFeedTapC As New Sober.Wcf.ROESICRFeedTurnAroundPointContact
                                    Singular.Reflection.CopyCommonProperties(fdtc, newFeedTapC, False)
                                    newFeedTap.ROESICRFeedTurnAroundPointContactList.Add(newFeedTapC)
                                  Next
                                Next
                                'Feed Production
                                newFeed.ROESICRFeedProductionList.Clear()
                                For Each fdPrd As OBLib.SatOps.Integration.ROESICRFeedProduction In fd.ROESICRFeedProductionList
                                  Dim newFeedPrd As New Sober.Wcf.ROESICRFeedProduction
                                  newFeedPrd.ROESICRFeedProductionAudioList = New List(Of Sober.Wcf.ROESICRFeedProductionAudio)
                                  Singular.Reflection.CopyCommonProperties(fdPrd, newFeedPrd, False)
                                  newFeed.ROESICRFeedProductionList.Add(newFeedPrd)
                                  'Feed Production Audio
                                  newFeedPrd.ROESICRFeedProductionAudioList.Clear()
                                  For Each fPRDda As OBLib.SatOps.Integration.ROESICRFeedProductionAudio In fdPrd.ROESICRFeedProductionAudioList
                                    Dim newFeedProdAudio As New Sober.Wcf.ROESICRFeedProductionAudio
                                    Singular.Reflection.CopyCommonProperties(fPRDda, newFeedProdAudio, False)
                                    newFeedPrd.ROESICRFeedProductionAudioList.Add(newFeedProdAudio)
                                  Next
                                Next
                                'Feed Ingest Instruction
                                newFeed.ROESICRFeedIngestInstructionList.Clear()
                                For Each fii As OBLib.SatOps.Integration.ROESICRFeedIngestInstruction In fd.ROESICRFeedIngestInstructionList
                                  Dim newFeedII As New Sober.Wcf.ROESICRFeedIngestInstruction
                                  Singular.Reflection.CopyCommonProperties(fii, newFeedII, False)
                                  newFeed.ROESICRFeedIngestInstructionList.Add(newFeedII)
                                Next
                                'Feed Path
                                newFeed.ROESICRFeedPathList.Clear()
                                For Each fpa As OBLib.SatOps.Integration.ROESICRFeedPath In fd.ROESICRFeedPathList
                                  Dim newFeedPA As New Sober.Wcf.ROESICRFeedPath
                                  Singular.Reflection.CopyCommonProperties(fpa, newFeedPA, False)
                                  newFeed.ROESICRFeedPathList.Add(newFeedPA)
                                Next
                              Next

                              Temp.Add(newItem)

                            End Sub)
        OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsBooking - End", "Feed: " & FeedID.Value.ToString)
        Return Temp(0)
      End If
    Catch ex As Exception
      OBLib.Helpers.ErrorHelpers.LogClientError("ICRDashboardIntegration", "ICRDashboardIntegration", "GetSatOpsBookings", ex.Message)
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsBooking - Failed", "Feed: " & FeedID.Value.ToString)
      Return Nothing
    End Try

    Return Nothing

  End Function

  Public Function GetAudioSettings(Key As String) As List(Of ROAudioSetting) Implements ISoberWcfService.GetAudioSettings

    Dim messageProperty As RemoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetAudioSettings - Start", "")
    Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
    If IsValidKey Then
      Dim ROAudioList As New List(Of ROAudioSetting)
      Dim ROAudioSettingList As OBLib.Maintenance.SatOps.ReadOnly.ROAudioSettingList = OBLib.Maintenance.SatOps.ReadOnly.ROAudioSettingList.GetROAudioSettingList(True)
      For Each roas As OBLib.Maintenance.SatOps.ReadOnly.ROAudioSetting In ROAudioSettingList
        Dim newAS As New Sober.Wcf.ROAudioSetting
        Singular.Reflection.CopyCommonProperties(roas, newAS, False)
        newAS.IsOld = roas.OldInd
        ROAudioList.Add(newAS)
      Next
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetAudioSettings - End", "")
      Return ROAudioList
    Else
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetAudioSettings - Failed", "")
    End If

    Return Nothing

  End Function

  Public Function GetTurnAroundPoints(Key As String) As List(Of ROTurnAroundPoint) Implements ISoberWcfService.GetTurnAroundPoints

    Dim messageProperty As RemoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetTurnAroundPoints - Start", "")
    Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
    If IsValidKey Then
      Dim ROTurnaroundPointList As OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPointList = OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPointList.GetROTurnAroundPointList
      Dim ROTAPList As New List(Of ROTurnAroundPoint)
      For Each roas As OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPoint In ROTurnaroundPointList
        Dim newAS As New Sober.Wcf.ROTurnAroundPoint
        Singular.Reflection.CopyCommonProperties(roas, newAS, False)
        ROTAPList.Add(newAS)
        For Each roasc As OBLib.Maintenance.SatOps.ReadOnly.ROTurnAroundPointContact In OBLib.CommonData.Lists.ROTurnAroundPointContactList
          If Singular.Misc.CompareSafe(roasc.TurnAroundPointID, roas.TurnAroundPointID) Then
            Dim newASC As New Sober.Wcf.ROTurnAroundPointContact
            Singular.Reflection.CopyCommonProperties(roasc, newASC, False)
            newAS.ROTurnAroundPointContactList = New List(Of Sober.Wcf.ROTurnAroundPointContact)
            newAS.ROTurnAroundPointContactList.Add(newASC)
          End If
        Next
      Next
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetTurnAroundPoints - Start", "")
      Return ROTAPList
    Else
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetTurnAroundPoints - Failed", "")
    End If

    Return Nothing

  End Function

  Public Function GetFeedTypes(Key As String) As List(Of ROFeedType) Implements ISoberWcfService.GetFeedTypes

    Dim messageProperty As RemoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetFeedTypes - Start", "")
    Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
    If IsValidKey Then
      Dim ROFeedTypeList As OBLib.Maintenance.SatOps.ReadOnly.ROFeedTypeList = OBLib.Maintenance.SatOps.ReadOnly.ROFeedTypeList.GetROFeedTypeList
      Dim ROTAPList As New List(Of ROFeedType)
      For Each roas As OBLib.Maintenance.SatOps.ReadOnly.ROFeedType In ROFeedTypeList
        Dim newAS As New Sober.Wcf.ROFeedType
        Singular.Reflection.CopyCommonProperties(roas, newAS, False)
        ROTAPList.Add(newAS)
      Next
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetFeedTypes - End", "")
      Return ROTAPList
    Else
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetFeedTypes - Failed", "")
    End If

    Return Nothing

  End Function

  Public Function GetStatuses(Key As String) As List(Of ROStatus) Implements ISoberWcfService.GetStatuses

    Dim messageProperty As RemoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetStatuses - Start", "")
    Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
    If IsValidKey Then
      Dim ROProductionAreaStatusList As OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaStatusList = OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaStatusList.GetROProductionAreaStatusList
      Dim ROTAPList As New List(Of ROStatus)
      For Each roas As OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaStatus In ROProductionAreaStatusList
        Dim newAS As New Sober.Wcf.ROStatus
        newAS.StatusID = roas.ProductionAreaStatusID
        newAS.StatusName = roas.ProductionAreaStatus
        'Singular.Reflection.CopyCommonProperties(roas, newAS, False)
        ROTAPList.Add(newAS)
      Next
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetStatuses - End", "")
      Return ROTAPList
    Else
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetStatuses - Failed", "")
    End If

    Return Nothing

  End Function

  Public Function GetSatOpsEquipment(Key As String) As List(Of ROSatOpsEquipment) Implements ISoberWcfService.GetSatOpsEquipment

    Dim messageProperty As RemoteEndpointMessageProperty = OperationContext.Current.IncomingMessageProperties(RemoteEndpointMessageProperty.Name)
    OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsEquipment - Start", "")
    Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
    If IsValidKey Then
      Dim ROSatOpsEquipmentList As OBLib.Equipment.ReadOnly.ROSatOpsEquipmentList = OBLib.Equipment.ReadOnly.ROSatOpsEquipmentList.GetROSatOpsEquipmentList
      Dim ROTAPList As New List(Of ROSatOpsEquipment)
      For Each roas As OBLib.Equipment.ReadOnly.ROSatOpsEquipment In ROSatOpsEquipmentList
        Dim newAS As New Sober.Wcf.ROSatOpsEquipment
        Singular.Reflection.CopyCommonProperties(roas, newAS, False)
        ROTAPList.Add(newAS)
      Next
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsEquipment - End", "")
      Return ROTAPList
    Else
      OBLib.OBMisc.LogWebServiceCall("Sober.wcf", Key, messageProperty.Address.ToString, "GetSatOpsEquipment - Failed", "")
    End If

    Return Nothing

  End Function

  Public Sub EscalateFeed(Key As String, FeedID As Integer, ByUsername As String) Implements ISoberWcfService.EscalateFeed

    Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
    If IsValidKey Then
      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEscalateFeed",
                                              New String() {"@FeedID", "@Username"},
                                              New Object() {FeedID, ByUsername})
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute
      OBLib.SoberHubClient.SendFeedEscalations()
    End If

  End Sub

  'Public Function EscalateFeed2(Key As String, FeedID As Integer, ByUsername As String) As String() Implements ISoberWcfService.EscalateFeed2
  '  Dim IsValidKey As Boolean = OBLib.CommonData.Lists.MiscList.IsValidIntegrationKey("ICRDashboardIntegrationPullKey", Key)
  '  If IsValidKey Then
  '    Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEscalateFeed",
  '                                            New String() {"@FeedID", "@Username"},
  '                                            New Object() {FeedID, ByUsername})
  '    cmdProc.UseTransaction = True
  '    cmdProc = cmdProc.Execute
  '    Return OBLib.SoberHubClient.SendFeedEscalations()
  '  End If
  '  Return New String() {"", ""}
  'End Function

End Class
