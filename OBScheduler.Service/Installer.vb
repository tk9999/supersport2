﻿Imports System.ServiceProcess
Imports System.ComponentModel

Namespace Installer
  <RunInstaller(True)> Public Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    Private serviceInstaller As ServiceInstaller
    Private processInstaller As ServiceProcessInstaller
    Private Const ServiceName As String = "SOBERMSScheduler"

    Public Sub New()
      ' Setup Base Class
      MyBase.New()

      'Instanciate Installers
      processInstaller = New ServiceProcessInstaller()
      serviceInstaller = New ServiceInstaller()

      'Configure Installers
      processInstaller.Account = ServiceAccount.NetworkService
      serviceInstaller.StartType = ServiceStartMode.Automatic
      serviceInstaller.ServiceName = ServiceName
      serviceInstaller.Description = "SOBERMSScheduler"
      'serviceInstaller.ServicesDependedOn = {"MSSQLSERVER"}

      'Add them to the list of installers to install
      Installers.Add(serviceInstaller)
      Installers.Add(processInstaller)
    End Sub

  End Class

End Namespace