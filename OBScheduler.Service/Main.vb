﻿Imports OBScheduler.Service.Schedules
Imports Singular

Public Class Main
  Inherits Singular.Service.ServiceBase

  Protected Overrides Sub AddServerPrograms()

    Singular.Reporting.ListToXML_UseOldMethod = True

    'Settings
    Dim msc As OBLib.Maintenance.Misc = OBLib.Maintenance.MiscList.GetMiscList().FirstOrDefault
    Dim MailCred As Singular.Emails.SingularMailSettings.MailCredential = New Singular.Emails.SingularMailSettings.MailCredential With {.FromServer = msc.MailServer,
                                                                                                                                        .FriendlyFrom = msc.MailFriendlyFrom,
                                                                                                                                        .FromAccount = msc.MailFromAccount,
                                                                                                                                        .FromAddress = msc.MailFromAddress,
                                                                                                                                        .FromPassword = msc.MailFromPassword}
    Dim SmSCred As Singular.SmsSending.ClickatellSettings = New Singular.SmsSending.ClickatellSettings With {.ApiID = msc.SmSApiID,
                                                                                                             .Password = msc.SMSPassword,
                                                                                                             .UserName = msc.SMSUsername}
    'Me.AddProgram(New StudioShiftTimesScheduler())
    Me.AddProgram(New SynergyScheduler())
    Me.AddProgram(New EmailScheduler(MailCred))
    Me.AddProgram(New SmsScheduleCustom(13, SmSCred))
    Me.AddProgram(New HourlyChecks())
    Me.AddProgram(New DailyChecks())
    Me.AddProgram(New DeleteCopiedBookings())
    Me.AddProgram(New SynergySchedulerNightly())
    Me.AddProgram(New InvoiceChecks())
    Me.AddProgram(New PlayoutChecks())
    Me.AddProgram(New ProductionServicesStudioChecks())
    Me.AddProgram(New PlayoutWeeklyChecks())
    Me.AddProgram(New HRContractChecks())
    Me.AddProgram(New TimesheetDaysScheduler())
  End Sub

  'Public Overrides ReadOnly Property EventLog As EventLog
  '  Get
  '    Return System.Diagnostics.EventLog.
  '  End Get
  'End Property

  Public Overrides ReadOnly Property EventSource As String
    Get
      Return "SOBERMSScheduler"
    End Get
  End Property

  Protected Overrides Sub PreSetup()
    MyBase.PreSetup()
  End Sub

  Protected Overrides ReadOnly Property ServiceDescription As String
    Get
      Return "SOBERMSScheduler"
    End Get
  End Property

  Protected Overrides ReadOnly Property VersionNo As String
    Get
      Return "1.0.0.0.0"
    End Get
  End Property

  'Dim MailCred As Singular.Emails.SingularMailSettings.MailCredential = New Singular.Emails.SingularMailSettings.MailCredential With {.FromServer = "10.100.8.34",
  '                                                                                                                              .FriendlyFrom = "SOBERMS",
  '                                                                                                                              .FromAccount = "",
  '                                                                                                                              .FromAddress = "sober@supersport.com",
  '                                                                                                                              .FromPassword = ""}

  'Dim smsCred As Singular.SmsSending.ClickatellSettings = New Singular.SmsSending.ClickatellSettings With {.ApiID = 3542381,
  '                                                                                                         .Password = "singular7j",
  '                                                                                                         .UserName = "soberms"}

End Class
