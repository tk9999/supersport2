﻿Imports OBLib

Namespace Schedules

  Public Class EmailScheduler
    Inherits Singular.Service.EmailScheduleBase

    Public Overrides ReadOnly Property ServerProgramTypeID As Integer
      Get
        Return 2
      End Get
    End Property

    Public Sub New(MailCred As Singular.Emails.SingularMailSettings.MailCredential)
      MyBase.New(2, MailCred, False)
      Singular.Emails.MailSettings.DefaultEmailBodyType = Singular.Emails.SingularMailSettings.EmailBodyType.CustomHTMLOnly
    End Sub

    'Protected Overrides Sub TimeUp()
    '  MyBase.TimeUp()
    'End Sub

    Sub Test()
      MyBase.TimeUp()
    End Sub

  End Class

End Namespace
