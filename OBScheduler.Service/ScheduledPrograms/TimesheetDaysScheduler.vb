﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class TimesheetDaysScheduler
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(27, "Timesheet Days Scheduler")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Timesheet Days Scheduler started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Timesheet Days Scheduler stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Timesheet Day Scheduler running for " & Now.ToString("dd-MMM-yy"))
        Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdTimesheetsGenerateDays")
        cmdProc.UseTransaction = True
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.None
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("Timesheet Day Scheduler", "Timesheet Day Scheduler", "Timeup", ex.Message & " - " & ex.StackTrace)
        AddProgress("Timesheet Day Scheduler failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
