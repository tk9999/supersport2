﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class SynergySchedulerNightly
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(17, "Synergy Scheduler Nightly")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Synergy Scheduler Nightly started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Synergy Scheduler Nightly stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Dim StartDate As DateTime = Singular.Dates.DateMonthStart(Now)
      Dim EndDate As DateTime = Singular.Dates.DateMonthEnd(StartDate.AddMonths(2))
      Dim StartDate2 As DateTime = Singular.Dates.DateMonthStart(EndDate.AddDays(1))
      Dim EndDate2 As DateTime = Singular.Dates.DateMonthEnd(StartDate2.AddYears(1))

      'Try
      AddProgress("Importing Events from " & StartDate2.ToString("dd-MMM-yy") & " to " & EndDate2.ToString("dd-MMM-yy"))
      'Scheduled Events--------------------------------------------------------------------------------------------------------------------------
      Dim mSynergyImport As OBLib.Synergy.Importer.[New].SynergyImport
      mSynergyImport = OBLib.Synergy.Importer.[New].SynergyImporter.ScheduledImport(StartDate2, EndDate2)
      AddProgress("Synergy Scheduler Completed for period" & StartDate2.ToString("dd-MMM-yy") & " to " & EndDate2.ToString("dd-MMM-yy"))
      If mSynergyImport IsNot Nothing AndAlso mSynergyImport.ChangeCount > 0 Then
        'Dim rptChanges As New OBWebReports.SynergyReports.SynergyChangesWithBookingInfo
        'rptChanges.ReportCriteria.StartDateTime = Now
        'rptChanges.ReportCriteria.EndDateTime = Now
        'rptChanges.ReportCriteria.SynergyImportID = mSynergyImport.SynergyImportID
        'Dim stream As System.IO.MemoryStream = rptChanges.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        'If stream.Length > 0 Then
        '  mSynergyImport.SendChangesEmail(stream.ToArray)
        'End If
      ElseIf mSynergyImport IsNot Nothing AndAlso mSynergyImport.ChangeCount = 0 Then
        AddProgress("Synergy Scheduler Completed for period" & StartDate2.ToString("dd-MMM-yy") & " to " & EndDate2.ToString("dd-MMM-yy") & " has detected 0 changes")
      End If

    End Sub

  End Class

End Namespace
