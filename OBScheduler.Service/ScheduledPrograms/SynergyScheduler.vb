﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class SynergyScheduler
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(9, "Synergy Scheduler")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Synergy Scheduler started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Synergy Scheduler stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Dim StartDate As DateTime = Singular.Dates.DateMonthStart(Now)
      Dim EndDate As DateTime = Singular.Dates.DateMonthEnd(StartDate.AddMonths(2))

      'Try
      AddProgress("Importing Events from " & StartDate.ToString("dd-MMM-yy") & " to " & EndDate.ToString("dd-MMM-yy"))
      'Scheduled Events--------------------------------------------------------------------------------------------------------------------------
      Dim mSynergyImport As OBLib.Synergy.Importer.[New].SynergyImport
      mSynergyImport = OBLib.Synergy.Importer.[New].SynergyImporter.ScheduledImport(StartDate, EndDate)
      AddProgress("Synergy Scheduler Completed for period" & StartDate.ToString("dd-MMM-yy") & " to " & EndDate.ToString("dd-MMM-yy"))
      If mSynergyImport IsNot Nothing AndAlso mSynergyImport.ChangeCount > 0 Then
        Dim rptChanges As New OBWebReports.SynergyReports.SynergyChangesWithBookingInfo
        rptChanges.ReportCriteria.StartDateTime = Now
        rptChanges.ReportCriteria.EndDateTime = Now
        rptChanges.ReportCriteria.SynergyImportID = mSynergyImport.SynergyImportID
        Dim stream As System.IO.MemoryStream = rptChanges.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        If stream.Length > 0 Then
          mSynergyImport.SendChangesEmail(stream.ToArray)
        End If
      ElseIf mSynergyImport IsNot Nothing AndAlso mSynergyImport.ChangeCount = 0 Then
        AddProgress("Synergy Scheduler Completed for period" & StartDate.ToString("dd-MMM-yy") & " to " & EndDate.ToString("dd-MMM-yy") & " has detected 0 changes")
      End If
      'Catch ex As Exception
      '  OBLib.Helpers.ErrorHelpers.LogClientError("Scheduler", "Service", "Timeup", ex.Message & " - " & ex.StackTrace)
      '  AddProgress("Synergy Scheduler failed: " & ex.Message)
      'End Try

    End Sub

  End Class

End Namespace
