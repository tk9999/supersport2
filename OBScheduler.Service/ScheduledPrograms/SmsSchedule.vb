﻿
Imports Singular
Imports Singular.Service
Imports OBLib.Notifications.SmS

Namespace Schedules

  Public Class SmsScheduleCustom
    Inherits ScheduleProgramBase

    Private Shared pSmsCred As Singular.SmsSending.ClickatellSettings

    Public Sub New(ByVal DatabaseID As Integer, ByVal ClickATellCredentials As Singular.SmsSending.ClickatellSettings)
      MyBase.New(DatabaseID, "SMS Scheduler")
      LogFile.LoggingEnabled = False
      Singular.SmsSending.SmsSender.Settings = ClickATellCredentials
      ClickATellCredentials.SessionTimeMinutes = 1
      ' WriteProgress("Credentials - New: " & ClickATellCredentials.ApiID.ToString & ", " & ClickATellCredentials.UserName.ToString & ", " & ClickATellCredentials.Password.ToString)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean

      Try
        SmsSending.SmsSender.SMSProvider = SMSProvider

        Select Case SMSProvider
          Case SmsSending.SMSProviderType.Clickatell
            Dim msc As OBLib.Maintenance.Misc = OBLib.Maintenance.MiscList.GetMiscList().FirstOrDefault
            Dim smsCred As Singular.SmsSending.ClickatellSettings = New Singular.SmsSending.ClickatellSettings With {.ApiID = msc.SmSApiID,
                                                                                                                     .Password = msc.SMSPassword,
                                                                                                                     .UserName = msc.SMSUsername,
                                                                                                                     .CallbackStatusDetailLevel = Singular.SmsSending.ClickatellCallbackStatusDetailLevel.IntermediateFinalAndErrorStatuses}

            Singular.SmsSending.SmsSender.Settings = smsCred
            pSmsCred = smsCred
        End Select
        WriteProgress("SMS Sender started.")
        Return True
      Catch ex As Exception
        WriteProgress("SMS Sender - " & ex.Message)
        Return False
      End Try

    End Function

    Protected Overrides Function StopSchedule() As Boolean
      WriteProgress("SMS Sender stopped.")
      Return True
    End Function

    Protected Overrides Sub TimeUp()

      'Just in case
      If pSmsCred Is Nothing Then
        Dim msc As OBLib.Maintenance.Misc = OBLib.Maintenance.MiscList.GetMiscList().FirstOrDefault
        Dim smsCred As Singular.SmsSending.ClickatellSettings = New Singular.SmsSending.ClickatellSettings With {.ApiID = msc.SmSApiID,
                                                                                                                       .Password = msc.SMSPassword,
                                                                                                                       .UserName = msc.SMSUsername,
                                                                                                                       .CallbackStatusDetailLevel = Singular.SmsSending.ClickatellCallbackStatusDetailLevel.IntermediateFinalAndErrorStatuses}

        smsCred.SessionTimeMinutes = 1
        Singular.SmsSending.SmsSender.Settings = smsCred
        pSmsCred = smsCred
      End If

      Dim mWebService As New Singular.SmsSending.PushServerWS
      Dim mSessionMessage As String = mWebService.auth(pSmsCred.ApiID, pSmsCred.UserName, pSmsCred.Password)
      Dim mSessionID As String = mSessionMessage.Substring(mSessionMessage.IndexOf(":") + 1).Trim

      WriteProgress("Auth Test: " & mSessionMessage)
      WriteProgress("Credentials: " & "ApiID: " & pSmsCred.ApiID.ToString & ", UserName: " & pSmsCred.UserName & ", Password: " & pSmsCred.Password)

      WriteProgress("Started sending Smses")
      Try
        Dim SmsList As OBLib.Notifications.Sms.SmsUnsentList
        SmsList = OBLib.Notifications.Sms.SmsUnsentList.GetSmsUnsentList
        If SmsList.Count > 0 Then
          For Each sms As OBLib.Notifications.Sms.SmsUnsent In SmsList
            sms.SendInternalSober(mSessionID, pSmsCred.ApiID, pSmsCred.UserName, pSmsCred.Password, Me.From, Singular.SmsSending.ClickatellCallbackStatusDetailLevel.IntermediateFinalAndErrorStatuses, 1)
          Next
          SmsList.Save()
          Dim fCount As Integer = SmsList.FailedCount
          Dim count As Integer = SmsList.Count - fCount
          If fCount > 0 Then
            WriteProgress(count & " Smses Sent. " & fCount & " Smses failed to Send.")
          Else
            WriteProgress(count & " Smses Sent.")
          End If
        Else
          WriteProgress("No Smses to Send")
        End If
      Catch ex As Exception
        WriteProgress("Error Sending Smses: " & ex.Message)
      End Try

    End Sub

    Protected Overridable ReadOnly Property AppID As Integer
      Get
        Return 0 'Singular.SmsSending.SmsSender.Settings.AppID
      End Get
    End Property

    Protected Overridable ReadOnly Property UserName As String
      Get
        Return Singular.SmsSending.SmsSender.Settings.UserName
      End Get
    End Property

    Protected Overridable ReadOnly Property Password As String
      Get
        Return Singular.SmsSending.SmsSender.Settings.Password
      End Get
    End Property

    Public Overridable ReadOnly Property SMSProvider As Singular.SmsSending.SMSProviderType
      Get
        Return SmsSending.SMSProviderType.Clickatell
      End Get
    End Property

    ''' <summary>
    ''' Required Only for SMS Warehouse.
    ''' </summary>
    Protected Overridable ReadOnly Property SenderID As String
      Get
        Return "SS"
      End Get
    End Property

    Protected Overridable ReadOnly Property MO As Integer
      Get
        Return 1
      End Get
    End Property

    Protected Overridable ReadOnly Property From As String
      Get
        Return ""
      End Get
    End Property

    Public Sub Test()
      Me.TimeUp()
    End Sub

  End Class

  'Public Class SmsSchedule
  '  Inherits Singular.Service.SmsScheduleBase

  '  Public Sub New(ByVal ClickATellCredentials As Singular.SmsSending.ClickatellSettings)
  '    MyBase.New(13, ClickATellCredentials)
  '  End Sub

  '  Protected Overrides Sub TimeUp()

  '    Dim SMSList As SmsSending.SmsList

  '    Try
  '      SMSList = SmsSending.SmsList.GetUnsentSmsList

  '      If SMSList.Count > 0 Then
  '        For Each sms As SmsSending.Sms In SMSList
  '          sms.SendSober()
  '        Next

  '        SMSList.Save()
  '        Dim fCount As Integer = SMSList.FailedCount
  '        Dim count As Integer = SMSList.Count - fCount
  '        If fCount > 0 Then
  '          WriteProgress(count & " Smses Sent. " & fCount & " Smses failed to Send.")
  '        Else
  '          WriteProgress(count & " Smses Sent.")
  '        End If
  '      Else
  '        WriteProgress("No Smses to Send")
  '      End If

  '    Catch ex As Exception
  '      WriteProgress("Error Sending Smses: " & ex.Message)
  '    End Try

  '  End Sub

  '  Public Sub Test()
  '    Me.TimeUp()
  '  End Sub

  'End Class

End Namespace
