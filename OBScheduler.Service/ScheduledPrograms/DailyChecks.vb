﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class DailyChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(15, "Daily Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Daily Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Daily Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Running daily checks for " & Now.Date.ToString("dd MMM yyyy"))
        Dim cpr As Singular.CommandProc = New Singular.CommandProc("[CmdProcs].[cmdCheckDaily]")
        'cpr.CommandTimeout = 10
        cpr = cpr.Execute()
        AddProgress("Running daily checks for " & Now.Date.ToString("dd MMM yyyy") & " has been completed")
      Catch ex As Exception
        AddProgress("Running daily checks failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
