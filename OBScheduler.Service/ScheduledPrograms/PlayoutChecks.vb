﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class PlayoutChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(22, "Playout Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Playout Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Playout Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      AddProgress("Playout checks for " & Now.Date.ToString("dd MMM yyyy HH:mm") & " has started")
      Try
        Dim rptPlayoutChecks As New OBWebReports.PlayoutOperationsReports.PlayoutChecksReport
        rptPlayoutChecks.ReportCriteria.StartDate = Now
        rptPlayoutChecks.ReportCriteria.RunDataFixes = False
        Dim stream As System.IO.MemoryStream = rptPlayoutChecks.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        If stream IsNot Nothing AndAlso stream.Length > 0 Then
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEmailPlayoutChecks")
          cmdProc.Parameters.AddWithValue("@ReportBytes", stream.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID", 0).Direction = ParameterDirection.Output
          cmdProc.UseTransaction = True
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
          cmdProc = cmdProc.Execute(0)
          Dim dr As DataRow = cmdProc.DataRow
          Dim emailID As Object = dr(0)
          Dim emailAttachmentID As Object = dr(1)
          Dim attachmentDocumentID As Object = dr(2)
          Dim x As Object = Nothing
        Else
          AddProgress("Playout checks: nothing to report")
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("Scheduler", "Invoice Checks", "Timeup", ex.Message & " - " & ex.StackTrace)
        AddProgress("Playout checks failed: " & ex.Message)
      Finally
        'Dim cpr As Singular.CommandProc = New Singular.CommandProc("[RptProcs].[rptPlayoutChecks]", New String() {"@Date"}, New Object() {Now})
        'cpr = cpr.Execute()
        AddProgress("Playout checks for " & Now.Date.ToString("dd MMM yyyy HH:mm") & " has been completed")
      End Try

    End Sub

  End Class

End Namespace
