﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class HRContractChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(26, "HR Contract Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("HR Contract Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("HR Contract Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Running hr contract checks for " & Now.Date.ToString("dd MMM yyyy HH:mm"))
        Dim cpr As Singular.CommandProc = New Singular.CommandProc("[CmdProcs].[cmdCheckHumanResourceContracts]")
        cpr.CommandTimeout = 0
        cpr = cpr.Execute()
        AddProgress("Running hr contract checks for " & Now.Date.ToString("dd MMM yyyy HH:mm") & " has been completed")
      Catch ex As Exception
        AddProgress("Running hr contract checks failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
