﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class InvoiceChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(18, "Invoice Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Invoice Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Invoice Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      AddProgress("Checking Invoices started")
      Try
        Dim rptInvoiceChecks As New OBWebReports.FinanceReports.InvoiceChecksProductionServices
        rptInvoiceChecks.ReportCriteria.PaymentRunID = Nothing
        rptInvoiceChecks.ReportCriteria.GetCurrentPaymentRun = True
        Dim stream As System.IO.MemoryStream = rptInvoiceChecks.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        If stream.Length > 0 Then
          Dim cmdProc As New Singular.CommandProc("cmdProcs.cmdEmailInvoiceChecks")
          cmdProc.Parameters.AddWithValue("@ReportBytes", stream.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID", 0).Direction = ParameterDirection.Output
          cmdProc.UseTransaction = True
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
          cmdProc = cmdProc.Execute(0)
          Dim dr As DataRow = cmdProc.DataRow
          Dim emailID As Object = dr(0)
          Dim emailAttachmentID As Object = dr(1)
          Dim attachmentDocumentID As Object = dr(2)
          Dim x As Object = Nothing
        Else
          AddProgress("Checking Invoices: nothing to report")
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("Scheduler", "Invoice Checks", "Timeup", ex.Message & " - " & ex.StackTrace)
        AddProgress("Checking Invoices failed: " & ex.Message)
      Finally
        AddProgress("Checking Invoices completed")
      End Try

    End Sub

  End Class

End Namespace
