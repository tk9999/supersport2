﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class SnapShotScheduler
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(12, "Snapshot Scheduler")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Snapshot Scheduler started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Snapshot Scheduler stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Capturing Snapshots for " & Now.Date.ToString("dd MMM yyyy"))
        Dim cpr As Singular.CommandProc = New Singular.CommandProc("[CmdProcs].[cmdCreateSnapShots]")
        'cpr.CommandTimeout = 10
        cpr = cpr.Execute()
        AddProgress("Snapshots for " & Now.Date.ToString("dd MMM yyyy") & " have been captured")
      Catch ex As Exception
        AddProgress("Snapshot capturing failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
