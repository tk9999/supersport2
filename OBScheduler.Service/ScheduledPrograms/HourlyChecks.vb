﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class HourlyChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(16, "Hourly Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Hourly Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Hourly Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Running hourly checks for " & Now.Date.ToString("dd MMM yyyy HH:mm"))
        Dim cpr As Singular.CommandProc = New Singular.CommandProc("[CmdProcs].[cmdCheckHourly]")
        'cpr.CommandTimeout = 10
        cpr = cpr.Execute()
        AddProgress("Running hourly checks for " & Now.Date.ToString("dd MMM yyyy HH:mm") & " has been completed")
      Catch ex As Exception
        AddProgress("Running hourly checks failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
