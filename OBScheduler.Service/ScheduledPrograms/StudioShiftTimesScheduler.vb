﻿Imports OBLib

Namespace Schedules

  Public Class StudioShiftTimesScheduler
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(10, "Studio Shift Times Scheduler")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Studio Shift Times Scheduler started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Studio Shift Times Scheduler stopped")
      Return True
    End Function

    Protected Overrides Sub TimeUp()

      Dim EndDate As DateTime = Now.Date.AddDays(-1)

      Try

        'Production Services - Studio-----------------------------------------------------------------------------------
        AddProgress("Creating/Updating Production Services (Studio) Unauthorised Shifts until date: " & EndDate.ToString("dd-MMM-yy"))
        Dim cmdPSST As New Singular.CommandProc("[CmdProcs].[cmdStudioShiftTimesScheduler]",
                                                 New String() {"SystemID", "ProductionAreaID", "ToDate"},
                                                 New Object() {OBLib.CommonData.Enums.System.ProductionServices,
                                                               OBLib.CommonData.Enums.ProductionArea.Studio,
                                                              EndDate})
        cmdPSST = cmdPSST.Execute()
        AddProgress("Production Services (Studio) Unauthorised Shifts updated " & EndDate.ToString("dd-MMM-yy"))

        'Production Content - Studio-----------------------------------------------------------------------------------
        AddProgress("Creating/Updating Production Content (Studio) Unauthorised Shifts until date: " & EndDate.ToString("dd-MMM-yy"))
        Dim cmdPCST As New Singular.CommandProc("[CmdProcs].[cmdStudioShiftScheduler]",
                                                 New String() {"SystemID", "ProductionAreaID", "ToDate"},
                                                 New Object() {OBLib.CommonData.Enums.System.ProductionContent,
                                                               OBLib.CommonData.Enums.ProductionArea.Studio,
                                                               EndDate})
        cmdPCST = cmdPCST.Execute()
        AddProgress("Production Content (Studio) Unauthorised Shifts updated " & EndDate.ToString("dd-MMM-yy"))

      Catch ex As Exception
        AddProgress("Studio Shift Times Scheduler failed: " & ex.Message)
      End Try

    End Sub

    Sub Test()
      TimeUp()
    End Sub

  End Class

End Namespace
