﻿Imports OBLib

Namespace Schedules

  Public Class DeveloperChecksProgram
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New("Developer Checks", 9)
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Developer Checks")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Developer Checks")
      Return True
    End Function

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Developer Checks Started")
        Dim cprocs As New Singular.CommandProc("[CmdProcs].[cmdDeveloperChecks]")
        cprocs = cprocs.Execute()
      Catch ex As Exception
        AddProgress("Developer Checks Failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
