﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class ProductionServicesStudioChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(23, "Production Services (Studio) Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Production Services (Studio) Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Production Services (Studio) Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      AddProgress("Production Services (Studio) checks has started")
      Try
        Dim rptProductionServicesStudioChecks As New OBWebReports.StudioReports.ServicesStudioDataChecksReport
        rptProductionServicesStudioChecks.ReportCriteria.StartDate = Singular.Dates.DateMonthStart(Now)
        rptProductionServicesStudioChecks.ReportCriteria.EndDate = Singular.Dates.DateMonthEnd(rptProductionServicesStudioChecks.ReportCriteria.StartDate)
        rptProductionServicesStudioChecks.ReportCriteria.SystemIDs.Add(1)
        rptProductionServicesStudioChecks.ReportCriteria.ProductionAreaIDs.Add(2)
        AddProgress("Production Services (Studio) checks for " & rptProductionServicesStudioChecks.ReportCriteria.StartDate.Value.ToString("dd MMM yyyy") & " has started")
        Dim stream As System.IO.MemoryStream = rptProductionServicesStudioChecks.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        If stream.Length > 0 Then
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEmailProductionServicesStudioChecks")
          cmdProc.Parameters.AddWithValue("@ReportBytes", stream.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID", 0).Direction = ParameterDirection.Output
          cmdProc.UseTransaction = True
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
          cmdProc = cmdProc.Execute(0)
          Dim dr As DataRow = cmdProc.DataRow
          Dim emailID As Object = dr(0)
          Dim emailAttachmentID As Object = dr(1)
          Dim attachmentDocumentID As Object = dr(2)
          Dim x As Object = Nothing
        Else
          AddProgress("Production Services (Studio) checks: nothing to report")
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("Scheduler", "Production Services (Studio) Checks", "Timeup", ex.Message & " - " & ex.StackTrace)
        AddProgress("Production Services (Studio) checks failed: " & ex.Message)
      Finally
        AddProgress("Production Services (Studio) checks has been completed")
      End Try

    End Sub

  End Class

End Namespace
