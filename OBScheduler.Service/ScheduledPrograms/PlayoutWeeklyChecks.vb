﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class PlayoutWeeklyChecks
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(24, "Playout Weekly Checks")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Playout Weekly Checks started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Playout Weekly Checks stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      AddProgress("Playout Weekly checks for " & Now.Date.ToString("dd MMM yyyy HH:mm") & " has started")
      Try
        'ISP Hours Report
        'current month
        Dim rptPlayoutDailyChecksCurrentMonth As New OBWebReports.PlayoutOperationsReports.PlayoutISPHoursSummary
        rptPlayoutDailyChecksCurrentMonth.ReportCriteria.StartDate = Singular.Dates.DateMonthStart(Now())
        rptPlayoutDailyChecksCurrentMonth.ReportCriteria.EndDate = Singular.Dates.DateMonthEnd(Now())
        rptPlayoutDailyChecksCurrentMonth.ReportCriteria.SystemID = OBLib.CommonData.Enums.System.PlayoutOperations
        Dim stream As System.IO.MemoryStream = rptPlayoutDailyChecksCurrentMonth.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        'next month
        Dim rptPlayoutDailyChecksNextMonth As New OBWebReports.PlayoutOperationsReports.PlayoutISPHoursSummary
        rptPlayoutDailyChecksNextMonth.ReportCriteria.StartDate = Singular.Dates.DateMonthStart(Now()).AddMonths(1)
        rptPlayoutDailyChecksNextMonth.ReportCriteria.EndDate = Singular.Dates.DateMonthEnd(Now()).AddMonths(1)
        rptPlayoutDailyChecksNextMonth.ReportCriteria.SystemID = OBLib.CommonData.Enums.System.PlayoutOperations
        Dim stream2 As System.IO.MemoryStream = rptPlayoutDailyChecksNextMonth.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)

        'MCR Projection Report
        Dim ASRAStartDateThisMonth As Date
        Dim ASRAEndDateThisMonth As Date
        Dim ASRAStartDateNextMonth As Date
        Dim ASRAEndDateNextMonth As Date

        If Now.Date.Day <= 19 Then
          ASRAStartDateThisMonth = New Date(Now.AddMonths(-1).Year, Now.AddMonths(-1).Month, 20)
          ASRAEndDateThisMonth = New Date(ASRAStartDateThisMonth.AddMonths(1).Year, ASRAStartDateThisMonth.AddMonths(1).Month, 19)
          ASRAStartDateNextMonth = ASRAEndDateThisMonth.AddDays(1)
          ASRAEndDateNextMonth = ASRAStartDateNextMonth.AddMonths(1).AddDays(-1)
        Else
          ASRAStartDateThisMonth = New Date(Now.Year, Now.Month, 20)
          ASRAEndDateThisMonth = New Date(ASRAStartDateThisMonth.AddMonths(1).Year, ASRAStartDateThisMonth.AddMonths(1).Month, 19)
          ASRAStartDateNextMonth = ASRAEndDateThisMonth.AddDays(1)
          ASRAEndDateNextMonth = ASRAStartDateNextMonth.AddMonths(1).AddDays(-1)
        End If

        'current month
        Dim rptPlayoutMCRChecksCurrentMonth As New OBWebReports.PlayoutOperationsReports.PlayoutOperationsMCRShiftWeekly
        rptPlayoutMCRChecksCurrentMonth.ReportCriteria.StartDate = ASRAStartDateThisMonth
        rptPlayoutMCRChecksCurrentMonth.ReportCriteria.EndDate = ASRAEndDateThisMonth
        rptPlayoutMCRChecksCurrentMonth.ReportCriteria.SystemIDs = New Integer() {CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer)}.ToList
        rptPlayoutMCRChecksCurrentMonth.ReportCriteria.ProductionAreaIDs = New Integer() {CType(OBLib.CommonData.Enums.ProductionArea.MCRRandburg, Integer), CType(OBLib.CommonData.Enums.ProductionArea.MCRSamrand, Integer)}.ToList
        Dim stream3 As System.IO.MemoryStream = rptPlayoutMCRChecksCurrentMonth.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)
        'next month
        Dim rptPlayoutMCRChecksNextMonth As New OBWebReports.PlayoutOperationsReports.PlayoutOperationsMCRShiftWeekly
        rptPlayoutMCRChecksNextMonth.ReportCriteria.StartDate = ASRAStartDateNextMonth
        rptPlayoutMCRChecksNextMonth.ReportCriteria.EndDate = ASRAEndDateNextMonth
        rptPlayoutMCRChecksNextMonth.ReportCriteria.SystemIDs = New Integer() {CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer)}.ToList
        rptPlayoutMCRChecksNextMonth.ReportCriteria.ProductionAreaIDs = New Integer() {CType(OBLib.CommonData.Enums.ProductionArea.MCRRandburg, Integer), CType(OBLib.CommonData.Enums.ProductionArea.MCRSamrand, Integer)}.ToList
        Dim stream4 As System.IO.MemoryStream = rptPlayoutMCRChecksNextMonth.GetAsDocument(Singular.Reporting.ReportDocumentType.ExcelData)

        If stream IsNot Nothing AndAlso stream2 IsNot Nothing AndAlso stream.Length > 0 AndAlso stream2.Length > 0 AndAlso
            stream3 IsNot Nothing AndAlso stream4 IsNot Nothing AndAlso stream3.Length > 0 AndAlso stream4.Length > 0 Then
          Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEmailPlayoutWeeklyChecks")
          cmdProc.Parameters.AddWithValue("@EmailID", 0).Direction = ParameterDirection.Output
          'ISP Current Month
          cmdProc.Parameters.AddWithValue("@ReportBytes", stream.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@Month", rptPlayoutDailyChecksCurrentMonth.ReportCriteria.StartDate.Value.ToString("MMM yy"))
          'ISP Next Month
          cmdProc.Parameters.AddWithValue("@ReportBytes2", stream2.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID2", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID2", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@Month2", rptPlayoutDailyChecksNextMonth.ReportCriteria.StartDate.Value.ToString("MMM yy"))
          'MCR Current Month
          cmdProc.Parameters.AddWithValue("@ReportBytes3", stream3.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID3", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID3", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@Month3", rptPlayoutMCRChecksCurrentMonth.ReportCriteria.StartDate.Value.ToString("MMM yy") + " - " + rptPlayoutMCRChecksCurrentMonth.ReportCriteria.EndDate.Value.ToString("MMM yy"))
          'MCR Next Month
          cmdProc.Parameters.AddWithValue("@ReportBytes4", stream4.ToArray).SqlType = SqlDbType.VarBinary
          cmdProc.Parameters.AddWithValue("@EmailAttachmentID4", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@AttachmentDocumentID4", 0).Direction = ParameterDirection.Output
          cmdProc.Parameters.AddWithValue("@Month4", rptPlayoutMCRChecksNextMonth.ReportCriteria.StartDate.Value.ToString("MMM yy") + " - " + rptPlayoutMCRChecksNextMonth.ReportCriteria.EndDate.Value.ToString("MMM yy"))
          cmdProc.UseTransaction = True
          cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
          cmdProc = cmdProc.Execute(0)
        Else
          AddProgress("Playout weekly checks: nothing to report")
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("Scheduler", "Playout Weekly Checks", "Timeup", ex.Message & " - " & ex.StackTrace)
        AddProgress("Playout weekly checks failed: " & ex.Message)
      Finally
        'Dim cpr As Singular.CommandProc = New Singular.CommandProc("[RptProcs].[rptPlayoutChecks]", New String() {"@Date"}, New Object() {Now})
        'cpr = cpr.Execute()
        AddProgress("Playout weekly checks for " & Now.Date.ToString("dd MMM yyyy HH:mm") & " has been completed")
      End Try

    End Sub

  End Class

End Namespace
