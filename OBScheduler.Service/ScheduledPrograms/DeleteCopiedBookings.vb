﻿Imports OBLib
Imports OBLib.SynergyWebService

Namespace Schedules

  Public Class DeleteCopiedBookings
    Inherits Singular.Service.ScheduleProgramBase

    Public Sub New()
      MyBase.New(14, "Delete Copied Bookings")
    End Sub

    Private Sub AddProgress(ByVal Text As String)
      WriteProgress(Text)
    End Sub

    Protected Overrides Function StartSchedule() As Boolean
      AddProgress("Delete Copied Bookings Scheduler started")
      Return True
    End Function

    Protected Overrides Function StopSchedule() As Boolean
      AddProgress("Delete Copied Bookings Scheduler stopped")
      Return True
    End Function

    Public Sub Test()
      TimeUp()
    End Sub

    Protected Overrides Sub TimeUp()

      Try
        AddProgress("Removing untouched copied bookings before " & Now.Date.ToString("dd MMM yyyy"))
        Dim cpr As Singular.CommandProc = New Singular.CommandProc("[CmdProcs].[cmdCreateSnapShots]")
        'cpr.CommandTimeout = 10
        cpr = cpr.Execute()
        AddProgress("Untouched copied bookings before " & Now.Date.ToString("dd MMM yyyy") & " have been removed")
      Catch ex As Exception
        AddProgress("Removing untouched copied bookings failed: " & ex.Message)
      End Try

    End Sub

  End Class

End Namespace
