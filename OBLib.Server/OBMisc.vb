﻿Imports System.Reflection
Imports System.ComponentModel
Imports System.Configuration

Public Class OBMisc

  'Private Shared mVersionNumberSetting As String

  Public Shared ReadOnly Property VersionNo As String
    Get
      Dim mVersionNumberSetting As String = System.Configuration.ConfigurationManager.AppSettings("VersionNumber")
      If mVersionNumberSetting Is Nothing OrElse mVersionNumberSetting = "" Then
        mVersionNumberSetting = "133"
      End If
      Return mVersionNumberSetting
    End Get
  End Property

  Public Shared Function IntegerListToXML(ByVal IntList As List(Of Integer)) As String

    If IntList Is Nothing Then
      Return ""
    ElseIf IntList.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = IntList.ConvertAll(Of String)(Function(d) d.ToString)
      Return StringArrayToXML(StringList.ToArray)
    End If

  End Function

  Public Shared Function Int64ListToXML(ByVal IntList As List(Of Int64)) As String

    If IntList Is Nothing Then
      Return ""
    ElseIf IntList.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = IntList.ConvertAll(Of String)(Function(d) d.ToString)
      Return StringArrayToXML(StringList.ToArray)
    End If

  End Function

  Public Shared Function NullableIntegerListToXML(ByVal IntList As List(Of Integer?)) As String

    If IntList Is Nothing Then
      Return ""
    ElseIf IntList.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = IntList.ConvertAll(Of String)(Function(d) d.Value.ToString)
      Return StringArrayToXML(StringList.ToArray)
    End If

  End Function

  Public Shared Function IntegerArrayToXML(ByVal IntList As Integer?()) As String

    If IntList Is Nothing Then
      Return ""
    ElseIf IntList.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = IntList.ToList.ConvertAll(Of String)(Function(d) d.Value.ToString)
      Return StringArrayToXML(StringList.ToArray)
    End If

  End Function

  Public Shared Function IntArrayToXML(ByVal IntList As Integer()) As String

    If IntList Is Nothing Then
      Return ""
    ElseIf IntList.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = IntList.ToList.ConvertAll(Of String)(Function(d) d.ToString)
      Return StringArrayToXML(StringList.ToArray)
    End If

  End Function

  Public Shared Function StringArrayToXML(ByVal StrArray() As String) As String

    If StrArray.Length = 0 Then
      Return ""
    Else
      Try
        Dim ds As New DataSet("DataSet")
        ds.Tables.Add("Table")
        ds.Tables("Table").Columns.Add("ID")
        ds.Tables("Table").Columns("ID").ColumnMapping = MappingType.Attribute

        For J As Integer = 0 To StrArray.Length - 1
          ds.Tables("Table").Rows.Add(New Object() {StrArray(J)})
        Next

        Dim w As New IO.StringWriter
        ds.WriteXml(w)
        Return (w.ToString)

      Catch ex As Exception
        Return String.Empty
      End Try
    End If

  End Function

  Public Shared Function StringListToXML(ByVal StrArray As List(Of String)) As String

    If StrArray.Count = 0 Then
      Return ""
    Else
      Try
        Dim ds As New DataSet("DataSet")
        ds.Tables.Add("Table")
        ds.Tables("Table").Columns.Add("ID")
        ds.Tables("Table").Columns("ID").ColumnMapping = MappingType.Attribute

        For J As Integer = 0 To StrArray.Count - 1
          ds.Tables("Table").Rows.Add(New Object() {StrArray(J)})
        Next

        Dim w As New IO.StringWriter
        ds.WriteXml(w)
        Return (w.ToString)

      Catch ex As Exception
        Return String.Empty
      End Try
    End If

  End Function

  Public Shared Function GetIDsFromXML(XMLString As String) As List(Of Integer)

    Dim LinqXml = System.Xml.Linq.XElement.Load(New IO.StringReader(XMLString))

    For Each element In LinqXml.Nodes
      Dim x As String = ""
    Next

    Return LinqXml.Nodes.Select(Function(c) CInt(CType(c, XElement).Attribute("ID").Value())).ToList

  End Function

  Public Shared Function GetMonthDescription(MonthID As Integer) As String

    Select Case MonthID
      Case 1
        Return "January"
      Case 2
        Return "February"
      Case 3
        Return "March"
      Case 4
        Return "April"
      Case 5
        Return "May"
      Case 6
        Return "June"
      Case 7
        Return "July"
      Case 8
        Return "August"
      Case 9
        Return "September"
      Case 10
        Return "October"
      Case 11
        Return "November"
      Case 12
        Return "December"
    End Select

    Return ""

  End Function

  Public Shared Function ResetCommonData(DataName As String) As Singular.Web.Result

    Try
      OBLib.CommonData.Refresh(DataName)
      Return New Singular.Web.Result(True)
    Catch ex As Exception
      Return New Singular.Web.Result(False, ex.Message)
    End Try

  End Function

  Public Shared Function GetEnumDescription(ByVal value As Object) As String

    Dim Info As FieldInfo = value.GetType.GetField(value.ToString)

    ' If the value is not valid for the enumeration then return '(None)'.
    If Info Is Nothing Then Return "Enum Description Not Located."

    Dim Attributes As DescriptionAttribute() = DirectCast(Info.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

    If Attributes.Length > 0 Then
      Return Attributes(0).Description
    Else
      Return Singular.Strings.Readable(value.ToString.Replace("_", ""))
    End If

  End Function

  Public Shared Function DateListToXML(ByVal DateList As List(Of DateTime)) As String

    If DateList Is Nothing Then
      Return ""
    ElseIf DateList.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = DateList.ConvertAll(Of String)(Function(d) d.ToString("dd MMM yyyy"))
      Return StringArrayDateToXML(StringList.ToArray)
    End If

  End Function

  Public Shared Function StringArrayDateToXML(ByVal StrArray() As String) As String

    Try

      Dim ds As New DataSet("DataSet")
      ds.Tables.Add("Table")
      ds.Tables("Table").Columns.Add("Day")
      ds.Tables("Table").Columns("Day").ColumnMapping = MappingType.Attribute

      For J As Integer = 0 To StrArray.Length - 1
        ds.Tables("Table").Rows.Add(New Object() {StrArray(J)})
      Next

      Dim w As New IO.StringWriter
      ds.WriteXml(w)
      Return (w.ToString)

    Catch ex As Exception
      Return String.Empty
    End Try

  End Function

  Shared Function IsSendingSmses() As Boolean

    Dim cmd As New Singular.CommandProc("CmdProcs.cmdSMSIsSendingSmses")
    cmd.FetchType = Singular.CommandProc.FetchTypes.DataObject
    cmd = cmd.Execute
    Dim IsBusy = cmd.DataObject
    Return IsBusy

  End Function

  Shared Sub SetIsSendingSmses(IsBusy As Boolean)

    Dim cmd As New Singular.CommandProc("CmdProcs.cmdSetIsSendingSmses", "@IsBusy", IsBusy)
    cmd.FetchType = Singular.CommandProc.FetchTypes.DataObject
    cmd = cmd.Execute

  End Sub

  Public Shared Function LogClientError(User As String, Page As String, Method As String, ErrorMessage As String) As Singular.Web.Result

    Try
      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdInsClientError]",
                                    New String() {"@User", "@Page", "@Method", "@Error"},
                                    New Object() {User, Page, Method, ErrorMessage})
      cmd = cmd.Execute
    Catch ex As Exception
      Return New Singular.Web.Result(False)
    End Try
    Return New Singular.Web.Result(True)

  End Function

  Public Shared Function LogICRServiceFeedback(User As String, FeedID As Integer, MethodName As String, Feedback As String) As Singular.Web.Result

    Try
      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdLogICRServiceFeedback]",
                                          New String() {"@User", "@FeedID", "@MethodName", "@Feedback"},
                                          New Object() {User, FeedID, MethodName, Feedback})
      cmd = cmd.Execute
    Catch ex As Exception
      Return New Singular.Web.Result(False)
    End Try
    Return New Singular.Web.Result(True)

  End Function

  Public Shared Function LogWebServiceCall(WebService As String, Key As String, User As String, MethodName As String, Parameters As String) As Singular.Web.Result

    Try
      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdLogWebServiceCall]",
                                          New String() {"@WebService", "@Key", "@User", "@MethodName", "@Parameters"},
                                          New Object() {WebService, Key, User, MethodName, Parameters})
      cmd = cmd.Execute
    Catch ex As Exception
      Return New Singular.Web.Result(False)
    End Try
    Return New Singular.Web.Result(True)

  End Function

  Public Shared Function CreateDataTable(Of T)(list As IEnumerable(Of T)) As DataTable
    Dim tp As Type = GetType(T)
    Dim allProperties = tp.GetProperties()
    Dim dataTable As DataTable = New DataTable()
    Dim propToInclude As New List(Of PropertyInfo)

    For Each propInfo As PropertyInfo In allProperties
      If propInfo.Name <> "ExtensionData" AndAlso propInfo.Name <> "_CreatorDetails" Then
        dataTable.Columns.Add(New DataColumn(propInfo.Name, propInfo.PropertyType))
        propToInclude.Add(propInfo)
      End If
    Next

    For Each entity As T In list
      Dim values(propToInclude.Count - 1) As Object '= {}
      For i As Integer = 0 To propToInclude.Count - 1 Step 1
        'If Singular.Reflection.IsBrowsable(propToInclude(i)) Then
        values(i) = propToInclude(i).GetValue(entity)
        'End If
      Next
      dataTable.Rows.Add(values)
    Next

    Return dataTable
  End Function

  Shared Function DBNullNothing(value As Object) As Object

    If IsDBNull(value) Then
      Return Nothing
    Else
      Return value
    End If

  End Function

End Class
