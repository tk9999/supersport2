﻿Imports System.Web
Imports System.Web.UI.HtmlControls
Imports Singular.Misc
Imports System.Text
Imports System.IO
Imports System.Web.UI
Imports OBLib.Maintenance.Company
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports Csla
Imports OBLib.Security
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old
Imports OBLib.Productions.Old

Public Class Emails

  Public Const LabelWarningStyle As String = "background-color: #f0ad4e;display: inline;padding: .2em .6em .3em;font-size: 75%;font-weight: 700;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25em;"
  Public Const LabelPrimaryStyle As String = "background-color: #428bca;display: inline;padding: .2em .6em .3em;font-size: 75%;font-weight: 700;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25em;"
  Public Const LabelInfoStyle As String = "background-color: #428bca;display: inline;padding: .2em .6em .3em;font-size: 75%;font-weight: 700;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25em;"
  Public Const LabelSuccessStyle As String = "background-color: #5bc0de;display: inline;padding: .2em .6em .3em;font-size: 75%;font-weight: 700;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25em;"
  Public Const LabelDangerStyle As String = "background-color: #d9534f;display: inline;padding: .2em .6em .3em;font-size: 75%;font-weight: 700;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25em;"

#Region " HTML Table "

  Private Class CustomHTMLTable

    Public Property Table As HtmlGenericControl
    Public Property TableName As HtmlGenericControl
    Public Property Header As HtmlGenericControl
    Public Property Body As HtmlGenericControl
    Public Property Footer As HtmlGenericControl
    Private mTableFontStyle As String = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:12px;border-collapse:collapse;"

    Private Sub AddHeaders(ColumnNames() As String)

      Dim Index As Integer = 1
      ColumnNames.ToList.ForEach(Sub(HeaderText)

                                   Dim HeaderCell As New HtmlGenericControl("th")
                                   'HeaderCell.InnerText = HeaderText
                                   If Index = 1 Then
                                     CreateCell(Header, HeaderCell, HeaderText, "Left", "Left", True, True)
                                   ElseIf Index = ColumnNames.Count Then
                                     CreateCell(Header, HeaderCell, HeaderText, "Left", "Right", True, True)
                                   Else
                                     CreateCell(Header, HeaderCell, HeaderText, "Left", "", True, True)
                                   End If
                                   Index += 1

                                 End Sub)

    End Sub

    Private Sub PopulateTableName(TableName As HtmlGenericControl, TableHeaderName As String)

      Dim TableNameCell As New HtmlGenericControl("th")
      TableNameCell.InnerHtml = TableHeaderName
      TableNameCell.Attributes("Style") = "font-weight: bold;"
      TableNameCell.Attributes("align") = "Left"
      AddBlankTableRow(TableNameCell)
      TableName.Controls.Add(TableNameCell)

      'CreateCell(TableName, TableNameCell, TableHeaderName, "Left", "", True, )

    End Sub

    Public Sub SetupTableAttributes(Attribute As String, Value As String)

      Table.Attributes(Attribute) = Value

    End Sub

    Public Sub New(ColumnNames() As String, PopulateBody As Action(Of HtmlGenericControl), TableHeaderName As String)

      Table = New HtmlGenericControl("table")
      TableName = New HtmlGenericControl("thead")
      Header = New HtmlGenericControl("thead")
      Body = New HtmlGenericControl("tbody")
      Footer = New HtmlGenericControl("tfoot")

      Table.Controls.Add(TableName)
      PopulateTableName(TableName, TableHeaderName)
      Table.Controls.Add(Header)
      Table.Controls.Add(Body)
      Table.Controls.Add(Footer)

      AddHeaders(ColumnNames)

      Table.Attributes("Style") = mTableFontStyle
      PopulateBody(Body)

    End Sub

  End Class

  Private Shared Sub CreateCell(ByRef Row As HtmlGenericControl, ByRef Cell As HtmlGenericControl, InnerHtml As String, CellAlignment As String, Optional Border As String = "", Optional BoldInd As Boolean = False, Optional HeaderInd As Boolean = False)

    Cell.InnerHtml = InnerHtml
    Cell.Attributes("align") = CellAlignment
    Dim BorderLine As String = If(Border = "", "", "border-" & Border & "-style:solid;")
    Dim Bold As String = If(BoldInd, "font-weight: bold;", "")
    Cell.Attributes("Style") = "padding: 4px 20px 4px 10px;" & _
                                "margin-right: 20px;" & _
                                BorderLine & _
                                "border-bottom-style:solid;" & _
                                "border-top-style:solid;" & _
                                "border-width:1px;" & _
                                Bold

    If HeaderInd Then
      Cell.Style.Add("color", "White")
      Cell.Style.Add("border-color", "Black")
      Cell.Attributes("bgcolor") = "#2F3232"
    End If
    Row.Controls.Add(Cell)

  End Sub

  Private Shared Sub CreateCellNoBorder(ByRef Row As HtmlGenericControl, ByRef Cell As HtmlGenericControl, InnerHtml As String, CellAlignment As String)

    Cell.InnerHtml = InnerHtml
    Cell.Attributes("align") = CellAlignment
    Cell.Attributes("Style") = "padding: 4px 20px 4px 10px;" & _
                                "margin-right: 20px;"
    Row.Controls.Add(Cell)

  End Sub

  Private Shared Sub CreateEmptyRow(ByRef Row As HtmlGenericControl, ByRef Cell As HtmlGenericControl)

    Cell.InnerHtml = ""
    Cell.Attributes("bgcolor") = "#FFFFFF"
    Cell.Attributes("valign") = "top"
    Cell.Attributes("align") = "left"
    Cell.Attributes("width") = "660"
    Cell.Attributes("height") = "21"
    Cell.Attributes("style") = "font-size:0;line-height:0"
    Row.Controls.Add(Cell)

  End Sub

  Private Shared Sub AddTableToParentTable(ByRef ParentTable As CustomHTMLTable, ByRef Table As CustomHTMLTable)

    Dim Row As New HtmlGenericControl("tr")
    ParentTable.Body.Controls.Add(Row)
    Dim RowCell As New HtmlGenericControl("td")
    Row.Controls.Add(RowCell)
    Dim EmptyRow As New HtmlGenericControl("tr")
    CreateEmptyRow(ParentTable.Body, EmptyRow)
    RowCell.Controls.Add(Table.Table)

  End Sub

  Private Shared Sub PopulateHeaderTable(ByRef Body As HtmlGenericControl)

    'Table Row 1
    Dim TableRow1 As New HtmlGenericControl("tr")
    Body.Controls.Add(TableRow1)

    Dim TableRow1Cell As New HtmlGenericControl("td")
    TableRow1.Controls.Add(TableRow1Cell)
    TableRow1Cell.Attributes("valign") = "top"
    TableRow1Cell.Attributes("align") = "left"
    TableRow1Cell.Attributes("width") = "680"
    TableRow1Cell.Attributes("height") = "20"
    TableRow1Cell.Attributes("style") = "font-size:0;line-height:0"

    'Table Row 2
    Dim TableRow2 As New HtmlGenericControl("tr")
    Body.Controls.Add(TableRow2)

    Dim TableRow2Cell As New HtmlGenericControl("td")
    TableRow2.Controls.Add(TableRow2Cell)
    TableRow2Cell.Attributes("valign") = "top"
    TableRow2Cell.Attributes("align") = "left"
    TableRow2Cell.Attributes("width") = "680"

    Dim TableRow2CellTable As New HtmlGenericControl("table")
    TableRow2Cell.Controls.Add(TableRow2CellTable)
    TableRow2CellTable.Attributes("cellpadding") = "0"
    TableRow2CellTable.Attributes("cellspacing") = "0"
    TableRow2CellTable.Attributes("border") = "0"

    Dim TableRow2CellTableBody As New HtmlGenericControl("tbody")
    TableRow2CellTable.Controls.Add(TableRow2CellTableBody)

    Dim TableRow2CellTableBodyRow As New HtmlGenericControl("tr")
    TableRow2CellTableBody.Controls.Add(TableRow2CellTableBodyRow)


    Dim TableRow2CellTableBodyRowCell1 As New HtmlGenericControl("td")
    TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell1)
    TableRow2CellTableBodyRowCell1.Attributes("valign") = "top"
    TableRow2CellTableBodyRowCell1.Attributes("align") = "left"
    TableRow2CellTableBodyRowCell1.Attributes("width") = "10"
    TableRow2CellTableBodyRowCell1.Attributes("style") = "font-size:0;line-height:0"


    Dim TableRow2CellTableBodyRowCell2 As New HtmlGenericControl("td")
    TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell2)
    TableRow2CellTableBodyRowCell2.Attributes("valign") = "middle"
    'TableRow2CellTableBodyRowCell2.Attributes("align") = "left"
    TableRow2CellTableBodyRowCell2.Attributes("width") = "257"
    TableRow2CellTableBodyRowCell2.Attributes("style") = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:20px;color:#FFFFFF"
    TableRow2CellTableBodyRowCell2.InnerText = "SOBER MS"


    'Dim TableRow2CellTableBodyRowCell2Link As New HtmlGenericControl("a")
    'TableRow2CellTableBodyRowCell2.Controls.Add(TableRow2CellTableBodyRowCell2Link)
    'TableRow2CellTableBodyRowCell2Link.Attributes("href") = "http://bit.ly/1dFFP45"
    'TableRow2CellTableBodyRowCell2Link.Attributes("target") = "_blank"

    'Dim TableRow2CellTableBodyRowCell2LinkImg As New HtmlGenericControl("img")
    'TableRow2CellTableBodyRowCell2Link.Controls.Add(TableRow2CellTableBodyRowCell2LinkImg)
    'TableRow2CellTableBodyRowCell2LinkImg.Attributes("src") = "https://ci4.googleusercontent.com/proxy/k6E6yrM-RjdwVDpX67OKGWDGDHm7PxmKMmRipBmEY84uSKq47w6CkgPnHkLBJ2ir50cQg7tNRweIMQcrAgM29QBFXm3kMrMqWxVkD2Hdb3VmxGrX2nkuQQ=s0-d-e1-ft#http://www.wantitall.co.za/emails/newsletters/superbalist/logo.png"
    'TableRow2CellTableBodyRowCell2LinkImg.Attributes("alt") = "Superbalist"
    'TableRow2CellTableBodyRowCell2LinkImg.Attributes("width") = "257"
    'TableRow2CellTableBodyRowCell2LinkImg.Attributes("height") = "27"
    'TableRow2CellTableBodyRowCell2LinkImg.Attributes("style") = "display:block;border:none"

    ''''''''''''''Not using ATM''''''''''''''
    'Dim TableRow2CellTableBodyRowCell3 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell3)
    'TableRow2CellTableBodyRowCell3.Attributes("valign") = "middle"
    'TableRow2CellTableBodyRowCell3.Attributes("align") = "center"
    'TableRow2CellTableBodyRowCell3.Attributes("width") = "345"
    'TableRow2CellTableBodyRowCell3.Attributes("style") = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:13px;color:#3cd062"

    'Dim TableRow2CellTableBodyRowCell4 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell4)
    'TableRow2CellTableBodyRowCell4.Attributes("valign") = "middle"
    'TableRow2CellTableBodyRowCell4.Attributes("align") = "right"
    'TableRow2CellTableBodyRowCell4.Attributes("width") = "58"
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'Dim TableRow2CellTableBodyRowCell4Link As New HtmlGenericControl("a")
    'TableRow2CellTableBodyRowCell4.Controls.Add(TableRow2CellTableBodyRowCell4Link)
    'TableRow2CellTableBodyRowCell4Link.Attributes("href") = "http://bit.ly/1dFFP45"
    'TableRow2CellTableBodyRowCell4Link.Attributes("target") = "_blank"

    'Dim TableRow2CellTableBodyRowCell4LinkImg As New HtmlGenericControl("img")
    'TableRow2CellTableBodyRowCell4Link.Controls.Add(TableRow2CellTableBodyRowCell4LinkImg)
    'TableRow2CellTableBodyRowCell4LinkImg.Attributes("src") = "https://ci5.googleusercontent.com/proxy/wqvkUAZOw2oCmd_hJ4cLXtu2xeUt7wllWn65s3anYGF5wfj25lrwUxN0qdFSqU2OGQVgY09TkmE_OFAGxbk5NAZgmjUrvJXoxr1Tbqz4lpJsqC_kwP12ex7nr_GMrMjT=s0-d-e1-ft#http://www.wantitall.co.za/emails/newsletters/superbalist/btn-view-all.png"
    'TableRow2CellTableBodyRowCell4LinkImg.Attributes("alt") = "View All"
    'TableRow2CellTableBodyRowCell4LinkImg.Attributes("width") = "58"
    'TableRow2CellTableBodyRowCell4LinkImg.Attributes("height") = "14"
    'TableRow2CellTableBodyRowCell4LinkImg.Attributes("style") = "display:block;border:none"

    Dim TableRow2CellTableBodyRowCell5 As New HtmlGenericControl("td")
    TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell5)
    TableRow2CellTableBodyRowCell5.Attributes("valign") = "top"
    TableRow2CellTableBodyRowCell5.Attributes("align") = "left"
    TableRow2CellTableBodyRowCell5.Attributes("width") = "10"
    TableRow2CellTableBodyRowCell5.Attributes("style") = "font-size:0;line-height:0"

    'Table Row 3
    Dim TableRow3 As New HtmlGenericControl("tr")
    Body.Controls.Add(TableRow3)
    TableRow3.Attributes("valign") = "top"
    TableRow3.Attributes("align") = "left"
    TableRow3.Attributes("width") = "10"
    TableRow3.Attributes("style") = "font-size:0;line-height:0"

    'Dim EmptyRow As New HtmlGenericControl("tr")
    'CreateEmptyRow(Table, EmptyRow)

    'Table.Attributes("width") = "100%"
    'Table.Attributes("cellpadding") = "0"
    'Table.Attributes("cellspacing") = "0"
    'Table.Attributes("border") = "0"
    'Table.Attributes("bgcolor") = "#2F3232"

    'Dim TableBody As New HtmlGenericControl("tbody")
    'Table.Controls.Add(TableBody)

    ''Table Row 1
    'Dim TableRow1 As New HtmlGenericControl("tr")
    'TableBody.Controls.Add(TableRow1)

    'Dim TableRow1Cell As New HtmlGenericControl("td")
    'TableRow1.Controls.Add(TableRow1Cell)
    'TableRow1Cell.Attributes("valign") = "top"
    'TableRow1Cell.Attributes("align") = "left"
    'TableRow1Cell.Attributes("width") = "680"
    'TableRow1Cell.Attributes("height") = "20"
    'TableRow1Cell.Attributes("style") = "font-size:0;line-height:0"

    ''Table Row 2
    'Dim TableRow2 As New HtmlGenericControl("tr")
    'TableBody.Controls.Add(TableRow2)

    'Dim TableRow2Cell As New HtmlGenericControl("td")
    'TableRow2.Controls.Add(TableRow2Cell)
    'TableRow2Cell.Attributes("valign") = "top"
    'TableRow2Cell.Attributes("align") = "center"
    'TableRow2Cell.Attributes("width") = "680"

    'Dim TableRow2CellTable As New HtmlGenericControl("table")
    'TableRow2Cell.Controls.Add(TableRow2CellTable)
    'TableRow2CellTable.Attributes("cellpadding") = "0"
    'TableRow2CellTable.Attributes("cellspacing") = "0"
    'TableRow2CellTable.Attributes("border") = "0"

    'Dim TableRow2CellTableBody As New HtmlGenericControl("tbody")
    'TableRow2CellTable.Controls.Add(TableRow2CellTableBody)

    'Dim TableRow2CellTableBodyRow As New HtmlGenericControl("tr")
    'TableRow2CellTableBody.Controls.Add(TableRow2CellTableBodyRow)

    'Dim TableRow2CellTableBodyRowCell1 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell1)
    'TableRow2CellTableBodyRowCell1.Attributes("valign") = "top"
    'TableRow2CellTableBodyRowCell1.Attributes("align") = "left"
    'TableRow2CellTableBodyRowCell1.Attributes("width") = "10"
    'TableRow2CellTableBodyRowCell1.Attributes("style") = "font-size:0;line-height:0"

    'Dim TableRow2CellTableBodyRowCell2 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell2)
    'TableRow2CellTableBodyRowCell2.Attributes("valign") = "middle"
    'TableRow2CellTableBodyRowCell2.Attributes("align") = "left"
    'TableRow2CellTableBodyRowCell2.Attributes("width") = "257"
    'TableRow2CellTableBodyRowCell2.Attributes("style") = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:16px;color:#FFFFFF"
    'TableRow2CellTableBodyRowCell2.InnerText = "SOBER MS"


    ''Dim TableRow2CellTableBodyRowCell2Link As New HtmlGenericControl("a")
    ''TableRow2CellTableBodyRowCell2.Controls.Add(TableRow2CellTableBodyRowCell2Link)
    ''TableRow2CellTableBodyRowCell2Link.Attributes("href") = "http://bit.ly/1dFFP45"
    ''TableRow2CellTableBodyRowCell2Link.Attributes("target") = "_blank"

    ''Dim TableRow2CellTableBodyRowCell2LinkImg As New HtmlGenericControl("img")
    ''TableRow2CellTableBodyRowCell2Link.Controls.Add(TableRow2CellTableBodyRowCell2LinkImg)
    ''TableRow2CellTableBodyRowCell2LinkImg.Attributes("src") = "https://ci4.googleusercontent.com/proxy/k6E6yrM-RjdwVDpX67OKGWDGDHm7PxmKMmRipBmEY84uSKq47w6CkgPnHkLBJ2ir50cQg7tNRweIMQcrAgM29QBFXm3kMrMqWxVkD2Hdb3VmxGrX2nkuQQ=s0-d-e1-ft#http://www.wantitall.co.za/emails/newsletters/superbalist/logo.png"
    ''TableRow2CellTableBodyRowCell2LinkImg.Attributes("alt") = "Superbalist"
    ''TableRow2CellTableBodyRowCell2LinkImg.Attributes("width") = "257"
    ''TableRow2CellTableBodyRowCell2LinkImg.Attributes("height") = "27"
    ''TableRow2CellTableBodyRowCell2LinkImg.Attributes("style") = "display:block;border:none"


    'Dim TableRow2CellTableBodyRowCell3 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell3)
    'TableRow2CellTableBodyRowCell3.Attributes("valign") = "middle"
    'TableRow2CellTableBodyRowCell3.Attributes("align") = "center"
    'TableRow2CellTableBodyRowCell3.Attributes("width") = "345"
    'TableRow2CellTableBodyRowCell3.Attributes("style") = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:13px;color:#3cd062"

    'Dim TableRow2CellTableBodyRowCell4 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell4)
    'TableRow2CellTableBodyRowCell4.Attributes("valign") = "middle"
    'TableRow2CellTableBodyRowCell4.Attributes("align") = "right"
    'TableRow2CellTableBodyRowCell4.Attributes("width") = "58"

    ''Dim TableRow2CellTableBodyRowCell4Link As New HtmlGenericControl("a")
    ''TableRow2CellTableBodyRowCell4.Controls.Add(TableRow2CellTableBodyRowCell4Link)
    ''TableRow2CellTableBodyRowCell4Link.Attributes("href") = "http://bit.ly/1dFFP45"
    ''TableRow2CellTableBodyRowCell4Link.Attributes("target") = "_blank"

    ''Dim TableRow2CellTableBodyRowCell4LinkImg As New HtmlGenericControl("img")
    ''TableRow2CellTableBodyRowCell4Link.Controls.Add(TableRow2CellTableBodyRowCell4LinkImg)
    ''TableRow2CellTableBodyRowCell4LinkImg.Attributes("src") = "https://ci5.googleusercontent.com/proxy/wqvkUAZOw2oCmd_hJ4cLXtu2xeUt7wllWn65s3anYGF5wfj25lrwUxN0qdFSqU2OGQVgY09TkmE_OFAGxbk5NAZgmjUrvJXoxr1Tbqz4lpJsqC_kwP12ex7nr_GMrMjT=s0-d-e1-ft#http://www.wantitall.co.za/emails/newsletters/superbalist/btn-view-all.png"
    ''TableRow2CellTableBodyRowCell4LinkImg.Attributes("alt") = "View All"
    ''TableRow2CellTableBodyRowCell4LinkImg.Attributes("width") = "58"
    ''TableRow2CellTableBodyRowCell4LinkImg.Attributes("height") = "14"
    ''TableRow2CellTableBodyRowCell4LinkImg.Attributes("style") = "display:block;border:none"

    'Dim TableRow2CellTableBodyRowCell5 As New HtmlGenericControl("td")
    'TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell5)
    'TableRow2CellTableBodyRowCell5.Attributes("valign") = "top"
    'TableRow2CellTableBodyRowCell5.Attributes("align") = "left"
    'TableRow2CellTableBodyRowCell5.Attributes("width") = "10"
    'TableRow2CellTableBodyRowCell5.Attributes("style") = "font-size:0;line-height:0"

    ''Table Row 3
    'Dim TableRow3 As New HtmlGenericControl("tr")
    'TableBody.Controls.Add(TableRow3)
    'TableRow3.Attributes("valign") = "top"
    'TableRow3.Attributes("align") = "left"
    'TableRow3.Attributes("width") = "10"
    'TableRow3.Attributes("style") = "font-size:0;line-height:0"

    ''Dim EmptyRow As New HtmlGenericControl("tr")
    ''CreateEmptyRow(Table, EmptyRow)

  End Sub

  Private Shared Sub AddBlankTableRow(ByRef HtmlGenericControl As HtmlGenericControl)

    Dim BlankRow As New HtmlGenericControl("tr")
    CreateEmptyRow(HtmlGenericControl, BlankRow)

  End Sub

  Private Shared Sub AddTextIntoCell(ByRef HtmlGenericControl As HtmlGenericControl, InnerHtml As String)

    Dim MessageRow As New HtmlGenericControl("tr")
    HtmlGenericControl.Controls.Add(MessageRow)
    Dim MessageRowCell As New HtmlGenericControl("td")
    MessageRowCell.InnerHtml = InnerHtml
    MessageRow.Controls.Add(MessageRowCell)

  End Sub

  Private Shared Function BuildEmailRecipients(UserList As ROUserList, EmailAddressList As List(Of String))

    Dim EmailAddress As String = ""

    For Each User As ROUser In UserList
      If EmailAddress = "" Then
        EmailAddress = User.EmailAddress
      ElseIf User.EmailAddress <> "" Then
        EmailAddress += "; " & User.EmailAddress
      End If
    Next

    EmailAddressList.ForEach(Sub(z)
                               If EmailAddress = "" Then
                                 EmailAddress += z
                               Else
                                 EmailAddress += ";" & z
                               End If
                             End Sub)

    Return EmailAddress

  End Function

  Private Shared Sub AddEmailRecipientsToList(ByVal System As OBLib.Maintenance.Company.System, ByRef ToUserList As ROUserList, ByRef CCUserList As ROUserList, _
                                                  ByRef ToEmailAddressList As List(Of String), ByRef CCEmailAddressList As List(Of String), SystemEmailsType As CommonData.Enums.SystemEmailTypes)


    For Each SystemEmail As OBLib.Maintenance.Company.SystemEmail In System.SystemEmailList.Where(Function(d) CompareSafe(d.SystemEmailTypeID, CType(SystemEmailsType, Integer)))

      For Each SystemEmailRecipient As OBLib.Maintenance.Company.SystemEmailRecipient In SystemEmail.SystemEmailRecipientList
        'Recipients mapped to a user
        If Not IsNullNothing(SystemEmailRecipient.UserID) Then
          If Not SystemEmailRecipient.CCInd Then
            Dim cu = ToUserList.GetItem(SystemEmailRecipient.UserID) 'Check if the user is already in the list
            If cu Is Nothing Then
              ToUserList.Add(SystemEmailRecipient.User)
            End If
          Else
            Dim cu = ToUserList.GetItem(SystemEmailRecipient.UserID)
            If cu Is Nothing Then
              CCUserList.Add(SystemEmailRecipient.User)
            End If
          End If

          'Recipients not users in SOBER
        Else
          If Not SystemEmailRecipient.CCInd Then
            ToEmailAddressList.Add(SystemEmailRecipient.EmailAddress)
          Else
            CCEmailAddressList.Add(SystemEmailRecipient.EmailAddress)
          End If

        End If
      Next

    Next

  End Sub

  Public Shared Function DataTableToHTMLTable(ByVal inTable As DataTable, ByVal TableAttributes As String, ByVal HeaderFormatting As String(), RowFormatting As String, ByVal CellFormatting As String()) As String
    Dim dString As New StringBuilder
    dString.Append("<table " & TableAttributes & " >")
    dString.Append(GetHeader(inTable, HeaderFormatting))
    dString.Append(GetBody(inTable, RowFormatting, CellFormatting))
    dString.Append("</table>")
    Return dString.ToString
  End Function

  Private Shared Function GetHeader(ByVal dTable As DataTable, ByVal HeaderFormatting As String()) As String
    Dim dString As New StringBuilder

    dString.Append("<thead><tr>")
    For Each dColumn As DataColumn In dTable.Columns
      dString.AppendFormat("<th " & HeaderFormatting(dTable.Columns.IndexOf(dColumn)).ToString & " >{0}</th>", dColumn.ColumnName)
    Next
    dString.Append("</tr></thead>")

    Return dString.ToString
  End Function

  Private Shared Function GetBody(ByVal dTable As DataTable, RowFormatting As String, ByVal CellFormatting As String()) As String
    Dim dString As New StringBuilder

    dString.Append("<tbody>")
    For Each dRow As DataRow In dTable.Rows
      dString.Append("<tr " & RowFormatting & ">")
      For dCount As Integer = 0 To dTable.Columns.Count - 1
        dString.AppendFormat("<td " & CellFormatting(dCount).ToString & ">{0}</td>", dRow(dCount))
      Next
      dString.Append("</tr>")
    Next
    dString.Append("</tbody>")

    Return dString.ToString()

  End Function

#End Region

  Public Class ForgotPasswordEmail

    Public Shared Sub CreateEmail(ByVal u As ROUser, ByVal NewPassword As String, Url As String)

      Dim el = Singular.Emails.EmailList.NewEmailList
      Dim e = el.AddNew
      e.DateToSend = Now
      e.ToEmailAddress = u.EmailAddress
      e.Subject = "New Password"

      e.Body = "Hi " + u.FirstName & vbCrLf & _
               "Your new password is: " + NewPassword & vbCrLf & _
               "Please login with your new password at the following link: " & vbCrLf & _
               System.Web.HttpUtility.UrlDecode(Url).ToString

      e.FriendlyFrom = "SOBER MS"
      e.FromEmailAddress = "noreply@soberms.com"
      el.Save()

    End Sub

  End Class

  Public Class UnReconciledProductions

    Public Property Email As Singular.Emails.Email

    Public Function UnReconciledProductionsEmail(Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String, SystemID As Integer) As Singular.Emails.Email

      Dim ProductionDetailTable As CustomHTMLTable = Nothing
      BuildProductionDetailTable(ProductionDetailTable, Production, User, EventManagers)

      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()
      SetupUnReconciledProductionsEmail(em, Production, User, EventManagers, SystemID, ProductionDetailTable)
      Return em

    End Function

    Private Sub SetupUnReconciledProductionsEmail(ByRef Email As Singular.Emails.Email, Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String, SystemID As Integer, ProductionDetailTable As CustomHTMLTable)

      Dim ToEmailAddress As String = ""
      Dim CCEmailAddress As String = ""
      'Dim SystemList As List(Of OBLib.Maintenance.Company.System)
      'If SystemID <> 0 Then
      '  SystemList = CommonData.Lists.CompanyList.GetSystem(SystemID).Where(Function(d) CompareSafe(d.SystemID, SystemID)).ToList
      'Else
      '  SystemList = CommonData.Lists.SystemList.ToList
      'End If
      Dim System As OBLib.Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(SystemID)
      Dim ToUserList As ROUserList = ROUserList.NewROUserList
      Dim CCUserList As ROUserList = ROUserList.NewROUserList
      Dim ToEmailAddressList As List(Of String) = New List(Of String)
      Dim CCEmailAddressList As List(Of String) = New List(Of String)

      'SystemList.ToList.ForEach(Sub(z)
      AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.UnReconciledProduction)
      'End Sub)

      ToEmailAddress = BuildEmailRecipients(ToUserList, ToEmailAddressList)
      CCEmailAddress = BuildEmailRecipients(CCUserList, CCEmailAddressList)

      Dim EventType As String = If(IsNullNothing(CommonData.Lists.ROEventTypeList.GetItem(Production.EventTypeID)), "", CommonData.Lists.ROEventTypeList.GetItem(Production.EventTypeID).EventType)

      With Email
        .Subject = "Un-Reconciled Production: " & Production.ProductionRefNo & " " & EventType
        .Body = UnReconciledProductionsHTML(Production, User, EventManagers, ProductionDetailTable)
        .ToEmailAddress = ToEmailAddress
        .CCEmailAddresses = CCEmailAddress
        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
        .DateToSend = Now
      End With

    End Sub

    Private Function UnReconciledProductionsHTML(Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String, ProductionDetailTable As CustomHTMLTable)

      'Parent Table
      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                      Sub()
                                                      End Sub,
                                                      "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)


      'Email Body Message
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateEmailBodyMessage(Body)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)
      AddTableToParentTable(ParentTable, ProductionDetailTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
      ParentTable.Table.RenderControl(htwParent)

      Return "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Un-Reconciled Production</title>" & _
                "</head>" & _
                "<body>" & _
                    swParent.ToString & _
                "</body>" & _
              "</html>"

    End Function

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl)

      'Dim Discipline As String = CommonData.Lists.DisciplineList.GetItem(DisciplineID).Discipline
      'Dim User
      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day"
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      'Dim MessageRow1 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow1)
      'Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      'MessageRow1Cell1.InnerHtml = "Ref No.: " & Production.ProductionRefNo
      'MessageRow1.Controls.Add(MessageRow1Cell1)

      'Dim MessageRow2 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow2)
      'Dim MessageRow2Cell1 As New HtmlGenericControl("td")
      'MessageRow2Cell1.InnerHtml = "Name: " & Production.ProductionDescription
      'MessageRow2.Controls.Add(MessageRow2Cell1)

      'Dim MessageRow3 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow3)
      'Dim MessageRow3Cell1 As New HtmlGenericControl("td")
      'MessageRow3Cell1.InnerHtml = "Un-reconciled By: " & User.FullName
      'MessageRow3.Controls.Add(MessageRow3Cell1)

      'Dim MessageRow4 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow4)
      'Dim MessageRow4Cell1 As New HtmlGenericControl("td")
      'MessageRow4Cell1.InnerHtml = "Un-reconciled Date: " & DateTime.Now.ToString("dd-MM-yy HH:mm")
      'MessageRow4.Controls.Add(MessageRow4Cell1)

      'Dim MessageRow5 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow5)
      'Dim MessageRow5Cell1 As New HtmlGenericControl("td")
      'MessageRow5Cell1.InnerHtml = "Event Manager(s): " & EventManagers
      'MessageRow5.Controls.Add(MessageRow5Cell1)

      'AddBlankTableRow(Body)

    End Sub

    Private Sub BuildProductionDetailTable(ByRef ProductionDetailTable As CustomHTMLTable, Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String)

      ProductionDetailTable = New CustomHTMLTable(New String() {}, _
                                                                    Sub(Body)

                                                                      'Ref No
                                                                      Dim HumanResourceRow1 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow1)

                                                                      Dim NameCell1 As New HtmlGenericControl("td")
                                                                      Dim DetailCell1 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow1, NameCell1, "Ref No.:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow1, DetailCell1, Production.ProductionRefNo, "Left", "Right", )

                                                                      'Name
                                                                      Dim HumanResourceRow2 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow2)

                                                                      Dim NameCell2 As New HtmlGenericControl("td")
                                                                      Dim DetailCell2 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow2, NameCell2, "Name:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow2, DetailCell2, Production.ProductionDescription, "Left", "Right", )

                                                                      'User
                                                                      Dim HumanResourceRow3 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow3)

                                                                      Dim NameCell3 As New HtmlGenericControl("td")
                                                                      Dim DetailCell3 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow3, NameCell3, "Un-reconciled By:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow3, DetailCell3, User.FullName, "Left", "Right", )


                                                                      'Date
                                                                      Dim HumanResourceRow4 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow4)

                                                                      Dim NameCell4 As New HtmlGenericControl("td")
                                                                      Dim DetailCell4 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow4, NameCell4, "Un-reconciled Date:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow4, DetailCell4, DateTime.Now.ToString("dd-MM-yy HH:mm"), "Left", "Right", )

                                                                      'Event Manager(s)
                                                                      Dim HumanResourceRow5 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow5)

                                                                      Dim NameCell5 As New HtmlGenericControl("td")
                                                                      Dim DetailCell5 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow5, NameCell5, "Event Manager(s):", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow5, DetailCell5, EventManagers, "Left", "Right", )

                                                                    End Sub,
                                                                    "Production Details")

    End Sub

    Public Sub New(Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, SystemID As Integer, EventManagers As String)
      Me.Email = UnReconciledProductionsEmail(Production, User, EventManagers, SystemID)
    End Sub

  End Class

  Public Class RemovedProductionHumanResources

    Private mHTML As String = ""
    Public ReadOnly Property HTML As String
      Get
        Return mHTML
      End Get
    End Property

    Private mEmail As Singular.Emails.Email
    Public ReadOnly Property Email As Singular.Emails.Email
      Get
        Return mEmail
      End Get
    End Property

    Private mRORemovedHumanResourceList As EmailBusinessObjects.RORemovedHumanResourceList = Nothing
    Private mTableFontStyle As String = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:12px;border-collapse:collapse;"

    Private Function AddTable(ColumnNames() As String) As HtmlGenericControl

      Dim Table As New HtmlGenericControl("table")

      Dim TableHeader As New HtmlGenericControl("thead")
      Table.Controls.Add(TableHeader)

      Dim HeaderRow As New HtmlGenericControl("tr")
      TableHeader.Controls.Add(HeaderRow)

      ColumnNames.ToList.ForEach(Sub(HeaderText)

                                   Dim HeaderCell As New HtmlGenericControl("th")
                                   HeaderCell.InnerText = HeaderText
                                   HeaderRow.Controls.Add(HeaderCell)

                                 End Sub)

      Dim TableBody As New HtmlGenericControl("tbody")
      Table.Controls.Add(TableBody)

      Return Table

    End Function

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, ProductionDescription As String)

      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day"
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      Dim MessageRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow1)
      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      MessageRow1Cell1.InnerHtml = "Human Resource(s) have been removed from Production: " & ProductionDescription
      MessageRow1.Controls.Add(MessageRow1Cell1)

      AddBlankTableRow(Body)

      Dim MessageRow2 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow2)
      Dim MessageRow2Cell2 As New HtmlGenericControl("td")
      MessageRow2Cell2.InnerHtml = "Please see below the following Travel booked for the removed Human Resource(s)"
      MessageRow2.Controls.Add(MessageRow2Cell2)

      AddBlankTableRow(Body)

    End Sub

    Private Sub PopulateHeaderTable(ByRef Body As HtmlGenericControl)

      'Table Row 1
      Dim TableRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(TableRow1)

      Dim TableRow1Cell As New HtmlGenericControl("td")
      TableRow1.Controls.Add(TableRow1Cell)
      TableRow1Cell.Attributes("valign") = "top"
      TableRow1Cell.Attributes("align") = "left"
      TableRow1Cell.Attributes("width") = "680"
      TableRow1Cell.Attributes("height") = "20"
      TableRow1Cell.Attributes("style") = "font-size:0;line-height:0"

      'Table Row 2
      Dim TableRow2 As New HtmlGenericControl("tr")
      Body.Controls.Add(TableRow2)

      Dim TableRow2Cell As New HtmlGenericControl("td")
      TableRow2.Controls.Add(TableRow2Cell)
      TableRow2Cell.Attributes("valign") = "top"
      TableRow2Cell.Attributes("align") = "left"
      TableRow2Cell.Attributes("width") = "680"

      Dim TableRow2CellTable As New HtmlGenericControl("table")
      TableRow2Cell.Controls.Add(TableRow2CellTable)
      TableRow2CellTable.Attributes("cellpadding") = "0"
      TableRow2CellTable.Attributes("cellspacing") = "0"
      TableRow2CellTable.Attributes("border") = "0"

      Dim TableRow2CellTableBody As New HtmlGenericControl("tbody")
      TableRow2CellTable.Controls.Add(TableRow2CellTableBody)

      Dim TableRow2CellTableBodyRow As New HtmlGenericControl("tr")
      TableRow2CellTableBody.Controls.Add(TableRow2CellTableBodyRow)


      Dim TableRow2CellTableBodyRowCell1 As New HtmlGenericControl("td")
      TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell1)
      TableRow2CellTableBodyRowCell1.Attributes("valign") = "top"
      TableRow2CellTableBodyRowCell1.Attributes("align") = "left"
      TableRow2CellTableBodyRowCell1.Attributes("width") = "10"
      TableRow2CellTableBodyRowCell1.Attributes("style") = "font-size:0;line-height:0"


      Dim TableRow2CellTableBodyRowCell2 As New HtmlGenericControl("td")
      TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell2)
      TableRow2CellTableBodyRowCell2.Attributes("valign") = "middle"
      TableRow2CellTableBodyRowCell2.Attributes("width") = "257"
      TableRow2CellTableBodyRowCell2.Attributes("style") = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:20px;color:#FFFFFF"
      TableRow2CellTableBodyRowCell2.InnerText = "SOBER MS"

      Dim TableRow2CellTableBodyRowCell5 As New HtmlGenericControl("td")
      TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell5)
      TableRow2CellTableBodyRowCell5.Attributes("valign") = "top"
      TableRow2CellTableBodyRowCell5.Attributes("align") = "left"
      TableRow2CellTableBodyRowCell5.Attributes("width") = "10"
      TableRow2CellTableBodyRowCell5.Attributes("style") = "font-size:0;line-height:0"

      'Table Row 3
      Dim TableRow3 As New HtmlGenericControl("tr")
      Body.Controls.Add(TableRow3)
      TableRow3.Attributes("valign") = "top"
      TableRow3.Attributes("align") = "left"
      TableRow3.Attributes("width") = "10"
      TableRow3.Attributes("style") = "font-size:0;line-height:0"

    End Sub

    Private Sub PopulateSummary(ByRef Body As HtmlGenericControl)

      For Each RORemovedHumanResource As EmailBusinessObjects.RORemovedHumanResource In mRORemovedHumanResourceList
        Dim HumanResourceRow As New HtmlGenericControl("tr")
        Body.Controls.Add(HumanResourceRow)

        Dim EmptyCell As New HtmlGenericControl("th")
        Dim NameCell As New HtmlGenericControl("td")
        Dim FlightsCell As New HtmlGenericControl("td")
        Dim RentalCarsCell As New HtmlGenericControl("td")
        Dim AccommodationCell As New HtmlGenericControl("td")
        Dim SnTCell As New HtmlGenericControl("td")

        CreateCell(HumanResourceRow, NameCell, RORemovedHumanResource.HumanResource, "Left", "Left", )
        CreateCell(HumanResourceRow, FlightsCell, RORemovedHumanResource.Flights, "center", , )
        CreateCell(HumanResourceRow, RentalCarsCell, RORemovedHumanResource.RentalCars, "center", , )
        CreateCell(HumanResourceRow, AccommodationCell, RORemovedHumanResource.Accommodation, "center", , )
        CreateCell(HumanResourceRow, SnTCell, RORemovedHumanResource.SnT, "center", "Right", )
      Next

    End Sub

    Private Sub PopulateFlightDetails(ByRef Body As HtmlGenericControl)

      For Each RORemovedHumanResource As EmailBusinessObjects.RORemovedHumanResource In mRORemovedHumanResourceList
        For Each RORemainingFlight As EmailBusinessObjects.RORemainingFlight In RORemovedHumanResource.RORemainingFlightList

          Dim HumanResourceRow As New HtmlGenericControl("tr")
          Body.Controls.Add(HumanResourceRow)

          Dim NameCell As New HtmlGenericControl("td")
          Dim FlightNoCell As New HtmlGenericControl("td")
          Dim TravelTypeCell As New HtmlGenericControl("td")
          Dim AirportFromCell As New HtmlGenericControl("td")
          Dim FlightStartDateCell As New HtmlGenericControl("td")
          Dim AirportToCell As New HtmlGenericControl("td")
          Dim FlightEndDateCell As New HtmlGenericControl("td")

          CreateCell(HumanResourceRow, NameCell, RORemainingFlight.HumanResource, "Left", "Left", )
          CreateCell(HumanResourceRow, FlightNoCell, RORemainingFlight.FlightNo, "Left", , )
          CreateCell(HumanResourceRow, TravelTypeCell, RORemainingFlight.TravelType, "Left", , )
          CreateCell(HumanResourceRow, AirportFromCell, RORemainingFlight.AirportFrom, "Left", , )
          CreateCell(HumanResourceRow, FlightStartDateCell, RORemainingFlight.FlightStartDate, "Left", , )
          CreateCell(HumanResourceRow, AirportToCell, RORemainingFlight.AirportTo, "Left", , )
          CreateCell(HumanResourceRow, FlightEndDateCell, RORemainingFlight.FlightEndDate, "Left", "Right", )

        Next
      Next

    End Sub

    Private Sub PopulateAccommodationDetails(ByRef Body As HtmlGenericControl)

      For Each RORemovedHumanResource As EmailBusinessObjects.RORemovedHumanResource In mRORemovedHumanResourceList
        For Each RORemainingAccommodation As EmailBusinessObjects.RORemainingAccommodation In RORemovedHumanResource.RORemainingAccommodationList

          Dim HumanResourceRow As New HtmlGenericControl("tr")
          Body.Controls.Add(HumanResourceRow)

          Dim NameCell As New HtmlGenericControl("td")
          Dim RefNoCell As New HtmlGenericControl("td")
          Dim ProviderCell As New HtmlGenericControl("td")
          Dim CityCell As New HtmlGenericControl("td")
          Dim CheckInCell As New HtmlGenericControl("td")
          Dim CheckOutCell As New HtmlGenericControl("td")

          CreateCell(HumanResourceRow, NameCell, RORemainingAccommodation.HumanResource, "Left", "Left", )
          CreateCell(HumanResourceRow, RefNoCell, RORemainingAccommodation.AccommodationNumber, "Left", , )
          CreateCell(HumanResourceRow, ProviderCell, RORemainingAccommodation.AccommodationProvider, "Left", , )
          CreateCell(HumanResourceRow, CityCell, RORemainingAccommodation.AccommodationCity, "Left", , )
          CreateCell(HumanResourceRow, CheckInCell, RORemainingAccommodation.AccommodationStartDate, "Left", , )
          CreateCell(HumanResourceRow, CheckOutCell, RORemainingAccommodation.AccommodationEndDate, "Left", "Right", )

        Next
      Next

    End Sub

    Private Sub PopulateRentalCarDetails(ByRef Body As HtmlGenericControl)

      For Each RORemovedHumanResource As EmailBusinessObjects.RORemovedHumanResource In mRORemovedHumanResourceList
        For Each RORemainingRentalCar As EmailBusinessObjects.RORemainingRentalCar In RORemovedHumanResource.RORemainingRentalCarList

          Dim HumanResourceRow As New HtmlGenericControl("tr")
          Body.Controls.Add(HumanResourceRow)

          Dim NameCell As New HtmlGenericControl("td")
          Dim FromBranchCell As New HtmlGenericControl("td")
          Dim PickupTimeCell As New HtmlGenericControl("td")
          Dim ToBranchCell As New HtmlGenericControl("td")
          Dim DropoffTimeCell As New HtmlGenericControl("td")

          CreateCell(HumanResourceRow, NameCell, RORemainingRentalCar.HumanResource, "Left", "Left", )
          CreateCell(HumanResourceRow, FromBranchCell, RORemainingRentalCar.BranchFrom, "Left", , )
          CreateCell(HumanResourceRow, PickupTimeCell, RORemainingRentalCar.PickupDateTime, "Left", , )
          CreateCell(HumanResourceRow, ToBranchCell, RORemainingRentalCar.BranchTo, "Left", , )
          CreateCell(HumanResourceRow, DropoffTimeCell, RORemainingRentalCar.DropoffDateTime, "Left", "Right", )

        Next
      Next

    End Sub

    Private Sub PopulateSnTDetails(ByRef Body As HtmlGenericControl)

      For Each RORemovedHumanResource As EmailBusinessObjects.RORemovedHumanResource In mRORemovedHumanResourceList
        For Each RORemainingSnT As EmailBusinessObjects.RORemainingSnT In RORemovedHumanResource.RORemainingSnTList

          Dim HumanResourceRow As New HtmlGenericControl("tr")
          Body.Controls.Add(HumanResourceRow)

          Dim NameCell As New HtmlGenericControl("td")
          Dim DayCell As New HtmlGenericControl("td")
          Dim PolicyCell As New HtmlGenericControl("td")
          Dim BreakfastCell As New HtmlGenericControl("td")
          Dim LunchCell As New HtmlGenericControl("td")
          Dim DinnerCell As New HtmlGenericControl("td")
          Dim IncidentalCell As New HtmlGenericControl("td")

          CreateCell(HumanResourceRow, NameCell, RORemainingSnT.HumanResource, "Left", "Left", )
          CreateCell(HumanResourceRow, DayCell, RORemainingSnT.SnTDay, "Left", , )
          CreateCell(HumanResourceRow, PolicyCell, RORemainingSnT.Country & If(RORemainingSnT.InternationalInd, " - International", " - Domestic"), "Left", , )
          CreateCell(HumanResourceRow, BreakfastCell, RORemainingSnT.BreakfastAmount.ToString("N2"), "Left", , )
          CreateCell(HumanResourceRow, LunchCell, RORemainingSnT.LunchAmount.ToString("N2"), "Left", , )
          CreateCell(HumanResourceRow, DinnerCell, RORemainingSnT.DinnerAmount.ToString("N2"), "Left", , )
          CreateCell(HumanResourceRow, IncidentalCell, RORemainingSnT.IncidentalAmount.ToString("N2"), "Left", "Right", )

        Next
      Next

    End Sub

    Private Sub BuildHTML(ROProduction As Productions.ReadOnly.ROProduction)


      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub()
                                                          End Sub,
                                                          "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)

      'Email Body Message
      Dim Description As String = ""
      If ROProduction Is Nothing Then
      Else
        Description = ROProduction.ProductionDescription
      End If
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateEmailBodyMessage(Body, Description)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)

      'Summary
      Dim SummaryTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Name", "Flights", "Rental Cars", "Accommodation", "S&T"}, _
                                                          Sub(Body)
                                                            PopulateSummary(Body)
                                                          End Sub,
                                                          "Summary Details")

      AddTableToParentTable(ParentTable, SummaryTable)

      'Flights
      Dim FlightsTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Name", "Flight No.", "Type", "From", "Departing", "To", "Arriving"}, _
                                                         Sub(Body)
                                                           PopulateFlightDetails(Body)
                                                         End Sub,
                                                         "Flight Details")

      AddTableToParentTable(ParentTable, FlightsTable)

      'Rental Cars
      Dim RentalCarsTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Name", "From Branch", "Pickup Time", "To Branch", "Dropoff Time"}, _
                                                   Sub(Body)
                                                     PopulateRentalCarDetails(Body)
                                                   End Sub,
                                                   "Rental Car Details")

      AddTableToParentTable(ParentTable, RentalCarsTable)

      'Accommoodation
      Dim AccommodationTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Name", "Ref Num", "Provider", "City", "Check-In", "Check-Out"}, _
                                                   Sub(Body)
                                                     PopulateAccommodationDetails(Body)
                                                   End Sub,
                                                   "Accommodation Details")

      AddTableToParentTable(ParentTable, AccommodationTable)

      'S&T
      Dim SnTTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Name", "Day", "Policy", "Breakfast", "Lunch", "Dinner", "Incidental"}, _
                                                   Sub(Body)
                                                     PopulateSnTDetails(Body)
                                                   End Sub,
                                                   "S&T Details")

      AddTableToParentTable(ParentTable, SnTTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)


      ParentTable.Table.RenderControl(htwParent)

      mHTML = "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Human Resources Removed</title>" & _
                "</head>" & _
                "<body>" & _
                  swParent.ToString & _
                "</body>" & _
              "</html>"

    End Sub

    Private Sub AddHtmlGenericControl(TableSection As HtmlGenericControl, ControlToAdd As HtmlGenericControl)

      TableSection.Controls.Add(ControlToAdd)

    End Sub

    Private Sub SetupEmail(ROProduction As Productions.ReadOnly.ROProduction, SystemID As Integer)

      Dim PrimaryEmailAddress As String = ""
      Dim CCEmailAddress As String = ""
      Dim ToUserList As ROUserList = ROUserList.NewROUserList
      Dim CCUserList As ROUserList = ROUserList.NewROUserList
      Dim ToEmailAddressList As List(Of String) = New List(Of String)
      Dim CCEmailAddressList As List(Of String) = New List(Of String)

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdGetProductionEventManagers", _
                                          New String() {"ProductionID",
                                                        "SystemID"
                                                        }, _
                                          New Object() {ROProduction.ProductionID,
                                                        SystemID}
                                                        )

      cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute()

      For Each row As DataRow In cmd.Dataset.Tables(0).Rows
        ' EventManagersList.Add(New Helpers.ProductionManagers(row("ProductionID"), row("HumanResourceID"), row("EmailAddress")))
        If PrimaryEmailAddress = "" Then
          PrimaryEmailAddress += row("EmailAddress")
        Else
          PrimaryEmailAddress += "; " & row("EmailAddress")
        End If
      Next

      Dim System As Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(SystemID)

      AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.RemovedProductionHumanResource)

      PrimaryEmailAddress += If(PrimaryEmailAddress = "", "", ";") & BuildEmailRecipients(ToUserList, ToEmailAddressList)
      CCEmailAddress += If(CCEmailAddress = "", "", ";") & BuildEmailRecipients(CCUserList, CCEmailAddressList)

      mEmail = Singular.Emails.Email.NewEmail()
      mEmail.ToEmailAddress = PrimaryEmailAddress
      mEmail.CCEmailAddresses = CCEmailAddress
      mEmail.FromEmailAddress = "noreply@supersport.com"
      mEmail.Subject = "Removed Human Resources Email for " & ROProduction.ProductionType & " Production: " & ROProduction.ProductionRefNo & " on " & ROProduction.TxStartDateTime.Value.Date.ToString("dd-MMM-yy")
      mEmail.Body = mHTML

    End Sub

    Public Sub New(ProductionID As Integer, SystemID As Integer, RemovedHumanResourceIDXML As String, ProductionAreaID As Integer)

      mRORemovedHumanResourceList = EmailBusinessObjects.RORemovedHumanResourceList.GetRORemovedHumanResourceList(ProductionID, SystemID, RemovedHumanResourceIDXML, ProductionAreaID)
      Dim ROProduction As Productions.ReadOnly.ROProduction = OBLib.Productions.ReadOnly.ROProductionList.GetROProductionList(ProductionID).FirstOrDefault
      BuildHTML(ROProduction)
      SetupEmail(ROProduction, SystemID)

    End Sub

  End Class

  Public Class UsernamePasswordEmail

    Public Shared Function SendEmail(ByRef User As Security.User, LoginUrl As String, Optional CustomMessage As String = "") As Singular.Web.Result


      Try
        Dim pw As String = Singular.Encryption.DecryptString(User.GetPassword())
        Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdEmailUsernamePassword",
                                                 New String() {"@ToUserID", "@Url", "@CurrentUserID", "@Password", "@CustomMessage"},
                                                 New Object() {User.UserID,
                                                               LoginUrl,
                                                               OBLib.Security.Settings.CurrentUserID,
                                                               pw,
                                                               CustomMessage})
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
        cmdProc = cmdProc.Execute()
        Dim EmailID As Integer? = cmdProc.DataObject
        If IsNullNothing(EmailID) Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Email failed to generate"}
        Else
          Return New Singular.Web.Result(True)
        End If
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

      'Dim EL As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList()
      'Dim email As Singular.Emails.Email = EL.AddNew
      'email.ToEmailAddress = User.EmailAddress
      'email.FromEmailAddress = "sober@supersport.com"
      'email.Subject = "SOBERMS Login Details"
      'email.DateToSend = Now

      'email.Body = "Dear " & User.FirstName & " " & User.Surname & vbCrLf &
      '    vbCrLf &
      '    "Your username is: " & User.LoginName & vbCrLf &
      '    "and your password is: " & Singular.Encryption.DecryptString(User.GetPassword) & vbCrLf &
      '    "the url for SOBERMS is: " & "https://soberms.supersport.com/Account/Login.aspx"

      ''If CompareSafe(User.ContractTypeID, 6) Then
      ''  email.Body = email.Body & vbCrLf &
      ''    vbCrLf &
      ''    vbCrLf &
      ''    "Your timesheet is available online at the following address:" & vbCrLf &
      ''    "https://soberms.supersport.com/Account/Login.aspx" & vbCrLf & _
      ''    vbCrLf &
      ''    "You will need to change your password if you are logging in for the first time, or if your password does not meet the password requirements." & vbCrLf & _
      ''    vbCrLf &
      ''    "Please note that the website requires you to use one of the following Web Browsers in order to function correctly:" & vbCrLf &
      ''     "1. Google Chrome: " & "https://www.google.com/intl/en/chrome/browser/" & vbCrLf &
      ''     "2. Firefox: " & "http://www.mozilla.org/en-US/firefox/new/" & vbCrLf &
      ''     "3. Apple Safari: " & "https://www.apple.com/za/safari/" & vbCrLf &
      ''     "4. Internet Explorer 10 and above: " & "http://windows.microsoft.com/en-ZA/internet-explorer/download-ie"
      ''End If

      'email.Body = email.Body & vbCrLf &
      '    vbCrLf &
      '    "Regards" & vbCrLf &
      '    "SOBERMS"

      'EL.Save()

    End Function

  End Class

  Public Class ProductionDetailsChangedEmail

    Public Property RCEmail As Singular.Emails.Email

    Public Sub New(LoadedProductionTypeID As Integer?, ProductionTypeID As Integer?,
                   LoadedEventTypeID As Integer?, EventTypeID As Integer?,
                   LoadedProductionVenueID As Integer?, ProductionVenueID As Integer?,
                   LoadedTeamsPlaying As String, TeamsPlaying As String,
                   LoadedPlayStartDateTime As DateTime?, PlayStartDateTime As DateTime?,
                   LoadedPlayEndDateTime As DateTime?, PlayEndDateTime As DateTime?,
                   ChangedByUserID As Integer,
                   LoadedTitle As String, Title As String)

      RCEmail = New Singular.Emails.Email
      RCEmail.DateToSend = Now

      Dim EmailRecipients As String = ""
      Dim CCRecipients As String = ""

      For Each rosyse As ROSystemEmailRecipientsAll In CommonData.Lists.ROSystemEmailRecipientAllList
        If CompareSafe(rosyse.SystemEmailTypeID, CType(CommonData.Enums.SystemEmailTypes.ProductionDetailsChanged, Integer)) Then
          If Not rosyse.CCInd Then
            If rosyse.UserID IsNot Nothing Then
              EmailRecipients += rosyse.UserEmailAddress & ";"
            ElseIf rosyse.NoUserEmailAddress <> "" Then
              EmailRecipients += rosyse.NoUserEmailAddress & ";"
            End If
          Else
            If rosyse.UserID IsNot Nothing Then
              CCRecipients += rosyse.UserEmailAddress & ";"
            ElseIf rosyse.NoUserEmailAddress <> "" Then
              CCRecipients += rosyse.NoUserEmailAddress & ";"
            End If
          End If
        End If
      Next

      RCEmail.ToEmailAddress = EmailRecipients
      RCEmail.CCEmailAddresses = CCRecipients
      RCEmail.Subject = "Production Details Changed"

      Dim mHTML As String = ""
      mHTML = "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Production Details Changed</title>" & _
                "</head>" & _
                "<body>" & _
                  GetEmailBody(LoadedProductionTypeID, ProductionTypeID, _
                               LoadedEventTypeID, EventTypeID, _
                               LoadedProductionVenueID, ProductionVenueID, _
                               LoadedTeamsPlaying, TeamsPlaying, _
                               LoadedPlayStartDateTime, PlayStartDateTime, _
                               LoadedPlayEndDateTime, PlayEndDateTime, OBLib.Security.Settings.CurrentUserID, _
                               LoadedTitle, Title) & _
                "</body>" & _
              "</html>"

      RCEmail.Body = mHTML

      RCEmail.FriendlyFrom = "SOBER MS"
      RCEmail.FromEmailAddress = "noreply@soberms.com"

    End Sub

    Public Function GetEmailBody(LoadedProductionTypeID As Integer?, ProductionTypeID As Integer?,
                                 LoadedEventTypeID As Integer?, EventTypeID As Integer?,
                                 LoadedProductionVenueID As Integer?, ProductionVenueID As Integer?,
                                 LoadedTeamsPlaying As String, TeamsPlaying As String,
                                 LoadedPlayStartDateTime As DateTime?, PlayStartDateTime As DateTime?,
                                 LoadedPlayEndDateTime As DateTime?, PlayEndDateTime As DateTime?,
                                 ChangedByUserID As Integer,
                                 LoadedTitle As String, Title As String) As String

      Dim TableHeader As String = "<thead>" & GetTableHeader(LoadedProductionTypeID, ProductionTypeID, _
                                                             LoadedEventTypeID, EventTypeID, _
                                                             LoadedProductionVenueID, ProductionVenueID, _
                                                             LoadedTeamsPlaying, TeamsPlaying, _
                                                             LoadedPlayStartDateTime, PlayStartDateTime, _
                                                             LoadedPlayEndDateTime, PlayEndDateTime, OBLib.Security.Settings.CurrentUserID, _
                                                             LoadedTitle, Title) & "</thead>"

      Dim TableBody As String = "<tbody>" & GetTableBody(LoadedProductionTypeID, ProductionTypeID, _
                                                         LoadedEventTypeID, EventTypeID, _
                                                         LoadedProductionVenueID, ProductionVenueID, _
                                                         LoadedTeamsPlaying, TeamsPlaying, _
                                                         LoadedPlayStartDateTime, PlayStartDateTime, _
                                                         LoadedPlayEndDateTime, PlayEndDateTime, OBLib.Security.Settings.CurrentUserID, _
                                                         LoadedTitle, Title) & "</tbody>"

      Return "<table>" & TableHeader & TableBody & "</table>"

    End Function

    Public Function GetTableHeader(LoadedProductionTypeID As Integer?, ProductionTypeID As Integer?,
                                   LoadedEventTypeID As Integer?, EventTypeID As Integer?,
                                   LoadedProductionVenueID As Integer?, ProductionVenueID As Integer?,
                                   LoadedTeamsPlaying As String, TeamsPlaying As String,
                                   LoadedPlayStartDateTime As DateTime?, PlayStartDateTime As DateTime?,
                                   LoadedPlayEndDateTime As DateTime?, PlayEndDateTime As DateTime?,
                                   ChangedByUserID As Integer,
                                   LoadedTitle As String, Title As String) As String

      Return ""

    End Function

    Public Function GetTableBody(LoadedProductionTypeID As Integer?, ProductionTypeID As Integer?,
                                 LoadedEventTypeID As Integer?, EventTypeID As Integer?,
                                 LoadedProductionVenueID As Integer?, ProductionVenueID As Integer?,
                                 LoadedTeamsPlaying As String, TeamsPlaying As String,
                                 LoadedPlayStartDateTime As DateTime?, PlayStartDateTime As DateTime?,
                                 LoadedPlayEndDateTime As DateTime?, PlayEndDateTime As DateTime?,
                                 ChangedByUserID As Integer,
                                 LoadedTitle As String, Title As String) As String

      Dim OldProductionTypeString As String = ""
      Dim OldProductionType As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType = Nothing
      If LoadedProductionTypeID IsNot Nothing Then
        OldProductionType = CommonData.Lists.ROProductionTypeList.GetItem(LoadedProductionTypeID)
        OldProductionTypeString = OldProductionType.ProductionType
      End If
      Dim NewProductionType As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType = CommonData.Lists.ROProductionTypeList.GetItem(ProductionTypeID)

      Dim OldEventTypeString As String = ""
      Dim OldEventType As OBLib.Maintenance.Productions.ReadOnly.Old.ROEventType = Nothing
      If LoadedEventTypeID IsNot Nothing Then
        OldEventType = CommonData.Lists.ROEventTypeList.GetItem(LoadedEventTypeID)
        OldEventTypeString = OldEventType.EventType
      End If
      Dim NewEventType As OBLib.Maintenance.Productions.ReadOnly.Old.ROEventType = CommonData.Lists.ROEventTypeList.GetItem(EventTypeID)

      Dim OldProductionVenueString As String = ""
      Dim OldProductionVenue As ROProductionVenueOld = Nothing
      If Not IsNullNothing(LoadedProductionVenueID, True) Then
        OldProductionVenue = CommonData.Lists.ROProductionVenueList.GetItem(LoadedProductionVenueID)
        OldProductionVenueString = OldProductionVenue.ProductionVenue
      End If
      Dim NewProductionVenueString As String = ""
      Dim NewProductionVenue As ROProductionVenueOld = Nothing
      If Not IsNullNothing(ProductionVenueID, True) Then
        NewProductionVenue = CommonData.Lists.ROProductionVenueList.GetItem(ProductionVenueID)
        NewProductionVenueString = NewProductionVenue.ProductionVenue
      End If

      Dim ChangedBy As String = CommonData.Lists.ROUserList.GetItem(ChangedByUserID).FirstName & " " & CommonData.Lists.ROUserList.GetItem(ChangedByUserID).Surname

      Dim CB As String = "<tr>" + _
                            "<td>" + "Changed By" + "</td>" + _
                            "<td>" + ChangedBy + "</td>" + _
                            "<td>" + "</td>" + _
                         "</tr>"

      Dim PT As String = "<tr>" + _
                            "<td>" + "Production Type" + "</td>" + _
                            "<td>" + IIf(OldProductionTypeString Is Nothing, "", OldProductionTypeString) + "</td>" + _
                            "<td>" + IIf(NewProductionType.ProductionType Is Nothing, "", NewProductionType.ProductionType) + "</td>" + _
                         "</tr>"

      Dim ET As String = "<tr>" + _
                            "<td>" + "Event Type" + "</td>" + _
                            "<td>" + IIf(OldEventTypeString Is Nothing, "", OldEventTypeString) + "</td>" + _
                            "<td>" + IIf(NewEventType.EventType Is Nothing, "", NewEventType.EventType) + "</td>" + _
                         "</tr>"

      Dim PV As String = "<tr>" + _
                            "<td>" + "Production Venue" + "</td>" + _
                            "<td>" + IIf(OldProductionVenueString.Trim.Length = 0, "", OldProductionVenueString) + "</td>" + _
                            "<td>" + IIf(NewProductionVenueString.Trim.Length = 0, "", NewProductionVenueString) + "</td>" + _
                         "</tr>"

      Dim Ttl As String = "<tr>" + _
                            "<td>" + "Title" + "</td>" + _
                            "<td>" + IIf(LoadedTitle Is Nothing, "", LoadedTitle) + "</td>" + _
                            "<td>" + IIf(Title Is Nothing, "", Title) + "</td>" + _
                         "</tr>"

      Dim TP As String = "<tr>" + _
                            "<td>" + "Teams" + "</td>" + _
                            "<td>" + IIf(LoadedTeamsPlaying Is Nothing, "", LoadedTeamsPlaying) + "</td>" + _
                            "<td>" + IIf(TeamsPlaying Is Nothing, "", TeamsPlaying) + "</td>" + _
                         "</tr>"

      Dim OldStartDate As String = ""
      If LoadedPlayStartDateTime Is Nothing Then
        OldStartDate = ""
      Else
        OldStartDate = LoadedPlayStartDateTime.Value.ToString("dd-MMM-yy HH:mm")
      End If

      Dim NewStartDate As String = ""
      If PlayStartDateTime Is Nothing Then
        NewStartDate = ""
      Else
        NewStartDate = PlayStartDateTime.Value.ToString("dd-MMM-yy HH:mm")
      End If

      Dim SD As String = "<tr>" + _
                            "<td>" + "Kick-Off Start Date Time" + "</td>" + _
                            "<td>" + OldStartDate + "</td>" + _
                            "<td>" + NewStartDate + "</td>" + _
                         "</tr>"

      Dim OldEndDate As String = ""
      If LoadedPlayEndDateTime Is Nothing Then
        OldEndDate = ""
      Else
        OldEndDate = LoadedPlayEndDateTime.Value.ToString("dd-MMM-yy HH:mm")
      End If

      Dim NewEndDate As String = ""
      If PlayEndDateTime Is Nothing Then
        NewEndDate = ""
      Else
        NewEndDate = PlayEndDateTime.Value.ToString("dd-MMM-yy HH:mm")
      End If

      Dim ED As String = "<tr>" + _
                            "<td>" + "Kick-Off End Date Time" + "</td>" + _
                            "<td>" + OldEndDate + "</td>" + _
                            "<td>" + NewEndDate + "</td>" + _
                         "</tr>"

      Dim Bdy As String = CB & PT & ET & PV & Ttl & TP & SD & ED

      Return Bdy

    End Function

  End Class

  Public Class HumanResourceShiftAddedUpdated

    Public Property Email As Singular.Emails.Email

    'Done this way cos this email class can be called from 2 different objects
    Public Sub New(NewInd As Boolean, _
                   OrigHumanResourceID As Integer?, HumanResourceID As Integer, _
                   OrigHRName As String, HRName As String, _
                   OrigProductionAreaID As Integer?, ProductionAreaID As Integer, _
                   OrigStartDateTime As DateTime, StartDateTime As DateTime, _
                   OrigEndDateTime As DateTime, EndDateTime As DateTime, _
                   OrigExtraShiftInd As Boolean?, ExtraShiftInd As Boolean?, _
                   SupervisorAuthInd As Boolean?, SupervisorAuthBy As Integer?, SupervisorAuthDateTime As DateTime?, _
                   ManagerAuthInd As Boolean?, ManagerAuthBy As Integer?, ManagerAuthDateTime As DateTime?)

      Me.Email = HRShiftAddedAmendedEmail(NewInd, _
                                          OrigHumanResourceID, HumanResourceID, _
                                          OrigHRName, HRName, _
                                          OrigProductionAreaID, ProductionAreaID, _
                                          OrigStartDateTime, StartDateTime, _
                                          OrigEndDateTime, EndDateTime, _
                                          OrigExtraShiftInd, ExtraShiftInd, _
                                          SupervisorAuthInd, SupervisorAuthBy, SupervisorAuthDateTime, _
                                          ManagerAuthInd, ManagerAuthBy, ManagerAuthDateTime)

    End Sub

    Public Function HRShiftAddedAmendedEmail(NewInd As Boolean, _
                                             OrigHumanResourceID As Integer, HumanResourceID As Integer, _
                                             OrigHRName As String, HRName As String, _
                                             OrigProductionAreaID As Integer, ProductionAreaID As Integer, _
                                             OrigStartDateTime As DateTime, StartDateTime As DateTime, _
                                             OrigEndDateTime As DateTime, EndDateTime As DateTime, _
                                             OrigExtraShiftInd As Boolean?, ExtraShiftInd As Boolean?, _
                                             SupervisorAuthInd As Boolean?, SupervisorAuthBy As Integer?, SupervisorAuthDateTime As DateTime?, _
                                             ManagerAuthInd As Boolean?, ManagerAuthBy As Integer?, ManagerAuthDateTime As DateTime?)

      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()

      'Parent Table
      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                      Sub()
                                                      End Sub,
                                                      "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)

      AddTableToParentTable(ParentTable, HeaderBannerTable)

      'Email Body Message
      Dim EmailHeaderMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            'Dim HRName As String
                                                            'If NewInd Then
                                                            '  HRName = FirstPreferredSurname(Firstname, PreferredName, Surname)
                                                            'Else
                                                            '  HRName = FirstPreferredSurname(OriginalFirstname, OriginalPreferredName, OriginalSurname)
                                                            'End If
                                                            PopulateEmailBodyMessage(Body, HRName, NewInd)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailHeaderMessageTable)

      'Email Body Message
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {"", "Value"}, _
                                                          Sub(Body)
                                                            PopulateEmailBodyMessage(Body, NewInd, _
                                                                                           OrigHumanResourceID, HumanResourceID, _
                                                                                           OrigHRName, HRName, _
                                                                                           OrigProductionAreaID, ProductionAreaID, _
                                                                                           OrigStartDateTime, StartDateTime, _
                                                                                           OrigEndDateTime, EndDateTime, _
                                                                                           OrigExtraShiftInd, ExtraShiftInd, _
                                                                                           SupervisorAuthInd, SupervisorAuthBy, SupervisorAuthDateTime, _
                                                                                           ManagerAuthInd, ManagerAuthBy, ManagerAuthDateTime)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)

      SetupHumanResourceEmail(em, NewInd, ParentTable, HumanResourceID)

      Return em

    End Function

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, HRName As String, NewInd As Boolean)

      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day " & HRName
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      Dim MessageRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow1)
      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      MessageRow1Cell1.InnerHtml = If(NewInd, "A", "Your") & " shift has been " & If(NewInd, "Added", "Modified") & " by " & Security.Settings.CurrentUser.FullName & "."
      MessageRow1.Controls.Add(MessageRow1Cell1)

      AddBlankTableRow(Body)

    End Sub

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, NewInd As Boolean, _
                                                                           OrigHumanResourceID As Integer, HumanResourceID As Integer, _
                                                                           OrigHRName As String, HRName As String, _
                                                                           OrigProductionAreaID As Integer, ProductionAreaID As Integer, _
                                                                           OrigStartDateTime As DateTime, StartDateTime As DateTime, _
                                                                           OrigEndDateTime As DateTime, EndDateTime As DateTime, _
                                                                           OrigExtraShiftInd As Boolean?, ExtraShiftInd As Boolean?, _
                                                                           SupervisorAuthInd As Boolean?, SupervisorAuthBy As Integer?, SupervisorAuthDateTime As DateTime?, _
                                                                           ManagerAuthInd As Boolean?, ManagerAuthBy As Integer?, ManagerAuthDateTime As DateTime?)


      'Dim GreetingRow As New HtmlGenericControl("tr")
      'Body.Controls.Add(GreetingRow)
      'Dim GreetingRowCell As New HtmlGenericControl("td")
      'GreetingRowCell.InnerHtml = "Good Day " & HRName
      'GreetingRow.Controls.Add(GreetingRowCell)

      'AddBlankTableRow(Body)

      'Dim MessageRow1 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow1)
      'Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      'MessageRow1Cell1.InnerHtml = If(NewInd, "A", "Your") & " shift has been " & If(NewInd, "Added", "Modified") & " by " & Security.Settings.CurrentUser.FullName & "."
      'MessageRow1.Controls.Add(MessageRow1Cell1)

      'AddBlankTableRow(Body)

      Dim Row1 As New HtmlGenericControl("tr")
      Body.Controls.Add(Row1)

      Dim AttributeCell1 As New HtmlGenericControl("td")
      Dim OriginalValueCell1 As New HtmlGenericControl("td")
      Dim NewValueCell1 As New HtmlGenericControl("td")

      CreateCell(Row1, AttributeCell1, "Start Date", "Left", "Left", True, )
      CreateCell(Row1, NewValueCell1, StartDateTime.ToString("dd-MMM-yyyy HH:mm"), "Left", "Right", )

      'Dim MessageRow2 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow2)
      'Dim MessageRow2Cell1 As New HtmlGenericControl("td")
      'MessageRow2Cell1.InnerHtml = "Start Date: " & StartDateTime.ToString("dd-MMM-yyyy HH:mm")
      'MessageRow2.Controls.Add(MessageRow2Cell1)

      Dim Row2 As New HtmlGenericControl("tr")
      Body.Controls.Add(Row2)

      Dim AttributeCell2 As New HtmlGenericControl("td")
      Dim OriginalValueCell2 As New HtmlGenericControl("td")
      Dim NewValueCell2 As New HtmlGenericControl("td")

      CreateCell(Row2, AttributeCell2, "End Date", "Left", "Left", True, )
      CreateCell(Row2, NewValueCell2, EndDateTime.ToString("dd-MMM-yyyy HH:mm"), "Left", "Right", )

      'Dim MessageRow3 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow3)
      'Dim MessageRow3Cell1 As New HtmlGenericControl("td")
      'MessageRow3Cell1.InnerHtml = "End Date: " & EndDateTime.ToString("dd-MMM-yyyy HH:mm")
      'MessageRow3.Controls.Add(MessageRow3Cell1)

      Dim Row3 As New HtmlGenericControl("tr")
      Body.Controls.Add(Row3)

      Dim AttributeCell3 As New HtmlGenericControl("td")
      Dim OriginalValueCell3 As New HtmlGenericControl("td")
      Dim NewValueCell3 As New HtmlGenericControl("td")

      Dim ROPA As ROProductionArea = CommonData.Lists.ROProductionAreaList.GetItem(ProductionAreaID)
      CreateCell(Row3, AttributeCell3, "Location", "Left", "Left", True, )
      CreateCell(Row3, NewValueCell3, If(ROPA IsNot Nothing, ROPA.ProductionArea, ""), "Left", "Right", )

      'Dim MessageRow4 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow4)
      'Dim MessageRow4Cell1 As New HtmlGenericControl("td")
      'Dim ROPA As ROProductionArea = CommonData.Lists.ROProductionAreaList.GetItem(ProductionAreaID)
      'MessageRow4Cell1.InnerHtml = "Location: " & If(ROPA IsNot Nothing, ROPA.ProductionArea, "")
      'MessageRow4.Controls.Add(MessageRow4Cell1)

      Dim Row4 As New HtmlGenericControl("tr")
      Body.Controls.Add(Row4)

      Dim AttributeCell4 As New HtmlGenericControl("td")
      Dim OriginalValueCell4 As New HtmlGenericControl("td")
      Dim NewValueCell4 As New HtmlGenericControl("td")

      Dim Supervisor As Maintenance.General.ReadOnly.ROUser = If(SupervisorAuthBy IsNot Nothing, CommonData.Lists.ROUserList.GetItem(SupervisorAuthBy), Nothing)
      CreateCell(Row4, AttributeCell4, "Supervisor Auth", "Left", "Left", True, )
      If (SupervisorAuthInd IsNot Nothing) Then
        CreateCell(Row4, NewValueCell4, If(Supervisor IsNot Nothing, Supervisor.FirstSurname, ""), "Left", "Right", )
      Else
        CreateCell(Row4, NewValueCell4, "Pending", "Left", "Right", )
      End If


      'Dim MessageRow5 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow5)
      'Dim MessageRow5Cell1 As New HtmlGenericControl("td")
      'Dim Supervisor As Maintenance.General.ReadOnly.ROUser = If(SupervisorAuthBy IsNot Nothing, CommonData.Lists.ROUserList.GetItem(SupervisorAuthBy), Nothing)
      'If (SupervisorAuthInd IsNot Nothing) Then
      '  MessageRow5Cell1.InnerHtml = "Supervisor " & If(SupervisorAuthInd, "Authorised", "Rejected") & " By " & If(Supervisor IsNot Nothing, Supervisor.FirstSurname, "")
      'Else
      '  MessageRow5Cell1.InnerHtml = "Supervisor Authorisation Pending"
      'End If
      'MessageRow5.Controls.Add(MessageRow5Cell1)

      Dim Row5 As New HtmlGenericControl("tr")
      Body.Controls.Add(Row5)

      Dim AttributeCell5 As New HtmlGenericControl("td")
      Dim OriginalValueCell5 As New HtmlGenericControl("td")
      Dim NewValueCell5 As New HtmlGenericControl("td")

      Dim Manager As Maintenance.General.ReadOnly.ROUser = If(ManagerAuthBy IsNot Nothing, CommonData.Lists.ROUserList.GetItem(ManagerAuthBy), Nothing)
      CreateCell(Row5, AttributeCell5, "Manager Auth", "Left", "Left", True, )
      If (SupervisorAuthInd IsNot Nothing) Then
        CreateCell(Row5, NewValueCell5, If(Manager IsNot Nothing, Manager.FirstSurname, ""), "Left", "Right", )
      Else
        CreateCell(Row5, NewValueCell5, "Pending", "Left", "Right", )
      End If

      'Dim MessageRow6 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow6)
      'Dim MessageRow6Cell1 As New HtmlGenericControl("td")
      'Dim Manager As Maintenance.General.ReadOnly.ROUser = If(ManagerAuthBy IsNot Nothing, CommonData.Lists.ROUserList.GetItem(ManagerAuthBy), Nothing)
      'If (ManagerAuthInd IsNot Nothing) Then
      '  MessageRow6Cell1.InnerHtml = "Manager " & If(ManagerAuthInd, "Authorised", "Rejected") & " By " & If(Manager IsNot Nothing, Manager.FirstSurname, "")
      'Else
      '  MessageRow6Cell1.InnerHtml = "Manager Authorisation Pending"
      'End If
      'MessageRow6.Controls.Add(MessageRow6Cell1)

    End Sub

    Private Sub SetupHumanResourceEmail(ByRef Email As Singular.Emails.Email, IsNewInd As Boolean, ParentTable As CustomHTMLTable, HumanResourceID As Integer)


      Dim ToEmailAddress As String = ""
      Dim CCEmailAddress As String = ""
      'Dim SystemList As List(Of Maintenance.Core.System) = New List(Of Maintenance.Core.System)

      'For Each hrs As HumanResourceSystem In HumanResource.HumanResourceSystemList
      '  Dim Sys As Maintenance.Core.System = CommonData.Lists.SystemList.FindFast("SystemID", hrs.SystemID)
      '  If Sys IsNot Nothing Then
      '    SystemList.Add(Sys)
      '  End If
      'Next
      'If SystemID <> 0 Then
      '  SystemList = CommonData.Lists.SystemList.Where(Function(d) CompareSafe(d.SystemID, SystemID)).ToList
      'Else
      '  SystemList = CommonData.Lists.SystemList.ToList
      'End If
      Dim ToUserList As Security.UserList = Security.UserList.NewUserList
      Dim CCUserList As Security.UserList = Security.UserList.NewUserList
      Dim ToEmailAddressList As List(Of String) = New List(Of String)
      Dim CCEmailAddressList As List(Of String) = New List(Of String)

      'SystemList.ToList.ForEach(Sub(z)
      '                            AddEmailRecipientsToList(z, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailsTypes.HumanResourcesAddedAmended)
      '                          End Sub)

      Dim ROHR As HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID)

      ToEmailAddress = ROHR.EmailAddress
      'CCEmailAddress = BuildEmailRecipients(CCUserList, CCEmailAddressList)

      Dim Title As String = If(IsNewInd, "Added", "Modified")

      With Email
        .Subject = "Shift " & Title
        .Body = HumanResourceHTML(Title, ParentTable)
        .ToEmailAddress = ToEmailAddress
        .CCEmailAddresses = CCEmailAddress
        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
        .DateToSend = Now
      End With

    End Sub

    Private Function HumanResourceHTML(Title As String, ParentTable As CustomHTMLTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
      ParentTable.Table.RenderControl(htwParent)

      Return "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Human Resource " & Title & "</title>" & _
                "</head>" & _
                "<body>" & _
                  "<table>" & _
                    swParent.ToString() & _
                "</body>" & _
              "</html>"

    End Function

  End Class

  Public Class ChangedProductionHumanResources

    Private mHTML As String = ""
    Public ReadOnly Property HTML As String
      Get
        Return mHTML
      End Get
    End Property

    Private mEmail As Singular.Emails.Email
    Public ReadOnly Property Email As Singular.Emails.Email
      Get
        Return mEmail
      End Get
    End Property

    Private RecordChangeList As List(Of OBLib.Helpers.RecordChange) = Nothing
    Private mTableFontStyle As String = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:12px;border-collapse:collapse;"

    Private Function AddTable(ColumnNames() As String) As HtmlGenericControl

      Dim Table As New HtmlGenericControl("table")

      Dim TableHeader As New HtmlGenericControl("thead")
      Table.Controls.Add(TableHeader)

      Dim HeaderRow As New HtmlGenericControl("tr")
      TableHeader.Controls.Add(HeaderRow)

      ColumnNames.ToList.ForEach(Sub(HeaderText)

                                   Dim HeaderCell As New HtmlGenericControl("th")
                                   HeaderCell.InnerText = HeaderText
                                   HeaderRow.Controls.Add(HeaderCell)

                                 End Sub)

      Dim TableBody As New HtmlGenericControl("tbody")
      Table.Controls.Add(TableBody)



      'Public Sub WaitTask(T As Action)

      '  Dim TaskQueue As New List(Of Task)
      '  TaskQueue.Add(Task.Factory.StartNew(T))
      '  Task.WaitAll(TaskQueue.ToArray())
      '  TaskQueue.Clear()

      'End Sub

    End Function

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, Production As OBLib.Productions.Old.Production)

      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day"
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      Dim MessageRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow1)
      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      MessageRow1Cell1.InnerHtml = "Human Resource(s) have been changed on Production: " & Production.ProductionDescription & ", " & Production.GetTxTimes() & "."
      MessageRow1.Controls.Add(MessageRow1Cell1)

      AddBlankTableRow(Body)

      Dim MessageRow2 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow2)
      Dim MessageRow2Cell1 As New HtmlGenericControl("td")
      Dim CPFirstDate As DateTime? = Production.CrewPlanningFirstFinalisedDate
      MessageRow2Cell1.InnerHtml = "Original Crew Finalised Date: " & If(CPFirstDate Is Nothing, "", CDate(Production.CrewPlanningFirstFinalisedDate).ToString("dd-MMM-yy"))
      MessageRow2.Controls.Add(MessageRow2Cell1)

      AddBlankTableRow(Body)

    End Sub

    Private Sub PopulateHeaderTable(ByRef Body As HtmlGenericControl)

      'Table Row 1
      Dim TableRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(TableRow1)

      Dim TableRow1Cell As New HtmlGenericControl("td")
      TableRow1.Controls.Add(TableRow1Cell)
      TableRow1Cell.Attributes("valign") = "top"
      TableRow1Cell.Attributes("align") = "left"
      TableRow1Cell.Attributes("width") = "680"
      TableRow1Cell.Attributes("height") = "20"
      TableRow1Cell.Attributes("style") = "font-size:0;line-height:0"

      'Table Row 2
      Dim TableRow2 As New HtmlGenericControl("tr")
      Body.Controls.Add(TableRow2)

      Dim TableRow2Cell As New HtmlGenericControl("td")
      TableRow2.Controls.Add(TableRow2Cell)
      TableRow2Cell.Attributes("valign") = "top"
      TableRow2Cell.Attributes("align") = "left"
      TableRow2Cell.Attributes("width") = "680"

      Dim TableRow2CellTable As New HtmlGenericControl("table")
      TableRow2Cell.Controls.Add(TableRow2CellTable)
      TableRow2CellTable.Attributes("cellpadding") = "0"
      TableRow2CellTable.Attributes("cellspacing") = "0"
      TableRow2CellTable.Attributes("border") = "0"

      Dim TableRow2CellTableBody As New HtmlGenericControl("tbody")
      TableRow2CellTable.Controls.Add(TableRow2CellTableBody)

      Dim TableRow2CellTableBodyRow As New HtmlGenericControl("tr")
      TableRow2CellTableBody.Controls.Add(TableRow2CellTableBodyRow)


      Dim TableRow2CellTableBodyRowCell1 As New HtmlGenericControl("td")
      TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell1)
      TableRow2CellTableBodyRowCell1.Attributes("valign") = "top"
      TableRow2CellTableBodyRowCell1.Attributes("align") = "left"
      TableRow2CellTableBodyRowCell1.Attributes("width") = "10"
      TableRow2CellTableBodyRowCell1.Attributes("style") = "font-size:0;line-height:0"


      Dim TableRow2CellTableBodyRowCell2 As New HtmlGenericControl("td")
      TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell2)
      TableRow2CellTableBodyRowCell2.Attributes("valign") = "middle"
      'TableRow2CellTableBodyRowCell2.Attributes("align") = "left"
      TableRow2CellTableBodyRowCell2.Attributes("width") = "257"
      TableRow2CellTableBodyRowCell2.Attributes("style") = "font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:20px;color:#FFFFFF"
      TableRow2CellTableBodyRowCell2.InnerText = "SOBER MS"

      Dim TableRow2CellTableBodyRowCell5 As New HtmlGenericControl("td")
      TableRow2CellTableBodyRow.Controls.Add(TableRow2CellTableBodyRowCell5)
      TableRow2CellTableBodyRowCell5.Attributes("valign") = "top"
      TableRow2CellTableBodyRowCell5.Attributes("align") = "left"
      TableRow2CellTableBodyRowCell5.Attributes("width") = "10"
      TableRow2CellTableBodyRowCell5.Attributes("style") = "font-size:0;line-height:0"

      'Table Row 3
      Dim TableRow3 As New HtmlGenericControl("tr")
      Body.Controls.Add(TableRow3)
      TableRow3.Attributes("valign") = "top"
      TableRow3.Attributes("align") = "left"
      TableRow3.Attributes("width") = "10"
      TableRow3.Attributes("style") = "font-size:0;line-height:0"

    End Sub

    Private Function AddMainRow(RecordChange As OBLib.Helpers.RecordChange, Style As String) As HtmlGenericControl

      Dim tr As New HtmlGenericControl("tr")

      Dim ToStringCell As New HtmlGenericControl("td")
      CreateCellNoBorder(tr, ToStringCell, RecordChange.MainDescription, "left")

      Dim ActionCell As New HtmlGenericControl("td")
      CreateCellNoBorder(tr, ActionCell, "<span style='" & Style & "'>" & RecordChange.ChangeType & "</span>", "left")

      Dim DescriptionCell As New HtmlGenericControl("td")
      CreateCellNoBorder(tr, DescriptionCell, "", "left")

      Dim SubDescriptionCell As New HtmlGenericControl("td")
      CreateCellNoBorder(tr, SubDescriptionCell, "", "left")

      Dim ChangedByCell As New HtmlGenericControl("td")
      CreateCellNoBorder(tr, ChangedByCell, RecordChange.ChangedBy, "left")

      Return tr

    End Function

    Private Function AddSubRow(RecordChangeDetail As OBLib.Helpers.RecordChangeDetail) As HtmlGenericControl

      Dim DetailRow As New HtmlGenericControl("tr")

      Dim DummyCell1 As New HtmlGenericControl("td")
      CreateCell(DetailRow, DummyCell1, "", "left", , , False)
      Dim DummyCell2 As New HtmlGenericControl("td")
      CreateCell(DetailRow, DummyCell2, "", "left", , , False)

      Dim FieldCell As New HtmlGenericControl("td")
      CreateCell(DetailRow, FieldCell, RecordChangeDetail.FieldName, "left", , , False)

      Dim PreviousValueCell As New HtmlGenericControl("td")
      CreateCell(DetailRow, PreviousValueCell, RecordChangeDetail.PreviousValue, "left", , , False)

      Dim NewValueCell As New HtmlGenericControl("td")
      CreateCell(DetailRow, NewValueCell, RecordChangeDetail.NewValue, "left", , , False)

      Return DetailRow

    End Function

    Private Sub PopulateRecordChange(ByRef Body As HtmlGenericControl)

      For Each RecordChange As OBLib.Helpers.RecordChange In RecordChangeList

        If RecordChange.ChangeType = "Added" Then
          Dim tr As HtmlGenericControl = AddMainRow(RecordChange, LabelSuccessStyle)
          Body.Controls.Add(tr)

        ElseIf RecordChange.ChangeType = "Deleted" Then
          Dim tr As HtmlGenericControl = AddMainRow(RecordChange, LabelDangerStyle)
          Body.Controls.Add(tr)

        ElseIf RecordChange.ChangeType = "Updated" Then
          Dim tr As HtmlGenericControl = AddMainRow(RecordChange, LabelPrimaryStyle)
          Body.Controls.Add(tr)

        End If

        If RecordChange.RecordChangeDetails.Count > 0 And RecordChange.ChangeType = "Updated" Then

          Dim ContainerRow As New HtmlGenericControl("tr")

          Dim DummyCell As New HtmlGenericControl("td")
          CreateCellNoBorder(ContainerRow, DummyCell, "", "left")

          Dim TablecontainerCell As New HtmlGenericControl("td")
          CreateCellNoBorder(ContainerRow, TablecontainerCell, "", "left")
          TablecontainerCell.Attributes("colspan") = "4"

          Dim DetailsTable As New HtmlGenericControl("table")
          Dim TableHeader As New HtmlGenericControl("thead")
          Dim TableHeaderRow As New HtmlGenericControl("tr")
          TableHeader.Controls.Add(TableHeaderRow)
          Dim TableBody As New HtmlGenericControl("tbody")
          Dim TableFooter As New HtmlGenericControl("tfoot")

          TablecontainerCell.Controls.Add(DetailsTable)
          DetailsTable.Controls.Add(TableHeader)
          DetailsTable.Controls.Add(TableBody)
          DetailsTable.Controls.Add(TableFooter)

          'Detail Header
          Dim DetailDummyCell1 As New HtmlGenericControl("td")
          Dim DetailDummyCell2 As New HtmlGenericControl("td")
          TableHeaderRow.Controls.Add(DetailDummyCell1)
          TableHeaderRow.Controls.Add(DetailDummyCell2)

          Dim FieldCell As New HtmlGenericControl("td")
          CreateCell(TableHeaderRow, FieldCell, "Field", "left", , True, True)

          Dim PreviousValueCell As New HtmlGenericControl("td")
          CreateCell(TableHeaderRow, PreviousValueCell, "Previous Value", "left", , True, True)

          Dim NewValueCell As New HtmlGenericControl("td")
          CreateCell(TableHeaderRow, NewValueCell, "New Value", "left", , True, True)

          'Detail Body
          For Each RecordChangeDetail As OBLib.Helpers.RecordChangeDetail In RecordChange.RecordChangeDetails
            Dim Detail As HtmlGenericControl = AddSubRow(RecordChangeDetail)
            TableBody.Controls.Add(Detail)
          Next

          Body.Controls.Add(ContainerRow)

        End If

      Next

    End Sub

    Private Sub BuildHTML(Production As Production)


      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub()
                                                          End Sub,
                                                          "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)

      'Email Body Message
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateEmailBodyMessage(Body, Production)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)

      'Summary
      Dim SummaryTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Record", "Action Taken", "Detail", "", "Changed By"}, _
                                                          Sub(Body)
                                                            PopulateRecordChange(Body)
                                                          End Sub,
                                                          "Change Details")

      AddTableToParentTable(ParentTable, SummaryTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)


      ParentTable.Table.RenderControl(htwParent)

      mHTML = "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Human Resources Removed</title>" & _
                "</head>" & _
                "<body>" & _
                  swParent.ToString & _
                "</body>" & _
              "</html>"

    End Sub

    Private Sub AddHtmlGenericControl(TableSection As HtmlGenericControl, ControlToAdd As HtmlGenericControl)

      TableSection.Controls.Add(ControlToAdd)

    End Sub

    Private Sub SetupEmail(Production As Productions.Old.Production, SystemID As Integer)

      Dim PrimaryEmailAddress As String = ""
      Dim CCEmailAddress As String = ""
      Dim ToUserList As ROUserList = ROUserList.NewROUserList
      Dim CCUserList As ROUserList = ROUserList.NewROUserList
      Dim ToEmailAddressList As List(Of String) = New List(Of String)
      Dim CCEmailAddressList As List(Of String) = New List(Of String)

      For Each phr As OBLib.Productions.Areas.ProductionHumanResource In Production.ProductionSystemAreaList(0).ProductionHumanResourceList
        If (CompareSafe(phr.DisciplineID, CommonData.Enums.Discipline.EventManager) Or CompareSafe(phr.DisciplineID, CommonData.Enums.Discipline.EventManagerIntern)) _
            AndAlso Not IsNullNothing(phr.HumanResourceID) Then
          Dim hr As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID)
          If hr IsNot Nothing Then
            If PrimaryEmailAddress = "" Then
              PrimaryEmailAddress += hr.EmailAddress
            Else
              PrimaryEmailAddress += "; " & hr.EmailAddress
            End If
          End If
        End If
      Next

      Dim System As Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(OBLib.Security.Settings.CurrentUser.SystemID)
      AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.ChangedHumanResourcesAfterCrewFinalised)
      PrimaryEmailAddress += If(PrimaryEmailAddress = "", "", ";") & BuildEmailRecipients(ToUserList, ToEmailAddressList)
      CCEmailAddress += If(CCEmailAddress = "", "", ";") & BuildEmailRecipients(CCUserList, CCEmailAddressList)

      mEmail = Singular.Emails.Email.NewEmail()
      mEmail.ToEmailAddress = PrimaryEmailAddress
      mEmail.CCEmailAddresses = CCEmailAddress
      mEmail.FromEmailAddress = "noreply@supersport.com"
      mEmail.Subject = "Changed Human Resources for " & Production.ProductionDescription
      mEmail.Body = mHTML

    End Sub

    Public Sub New(Production As OBLib.Productions.Old.Production, SystemID As Integer)

      mEmail = Singular.Emails.Email.NewEmail
      Me.RecordChangeList = Production.ProductionSystemAreaList(0).RecordChangeList
      BuildHTML(Production)
      SetupEmail(Production, SystemID)

    End Sub

  End Class

  Public Class HumanResourceShiftAuthorised



  End Class

  Public Class UnCrewFinalisedProductions

    Public Property Email As Singular.Emails.Email

    Public Sub New(Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, SystemID As Integer, EventManagers As String)
      Me.Email = UnFinalisingCrewEmail(Production, User, EventManagers, SystemID)
    End Sub

    Public Function UnFinalisingCrewEmail(Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String, SystemID As Integer) As Singular.Emails.Email

      Dim ProductionDetailTable As CustomHTMLTable = Nothing
      BuildProductionDetailTable(ProductionDetailTable, Production, User, EventManagers)

      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()
      SetupUnReconciledProductionsEmail(em, Production, User, EventManagers, SystemID, ProductionDetailTable)
      Return em

    End Function

    Private Sub BuildProductionDetailTable(ByRef ProductionDetailTable As CustomHTMLTable, Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String)

      ProductionDetailTable = New CustomHTMLTable(New String() {}, _
                                                                    Sub(Body)

                                                                      'Ref No
                                                                      Dim HumanResourceRow1 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow1)

                                                                      Dim NameCell1 As New HtmlGenericControl("td")
                                                                      Dim DetailCell1 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow1, NameCell1, "Ref No.:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow1, DetailCell1, Production.ProductionRefNo, "Left", "Right", )

                                                                      'Name
                                                                      Dim HumanResourceRow2 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow2)

                                                                      Dim NameCell2 As New HtmlGenericControl("td")
                                                                      Dim DetailCell2 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow2, NameCell2, "Name:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow2, DetailCell2, Production.ProductionDescription, "Left", "Right", )

                                                                      'User
                                                                      Dim HumanResourceRow3 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow3)

                                                                      Dim NameCell3 As New HtmlGenericControl("td")
                                                                      Dim DetailCell3 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow3, NameCell3, "Un-finalised crew By:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow3, DetailCell3, User.FullName, "Left", "Right", )


                                                                      'Date
                                                                      Dim HumanResourceRow4 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow4)

                                                                      Dim NameCell4 As New HtmlGenericControl("td")
                                                                      Dim DetailCell4 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow4, NameCell4, "Un-finalised crew Date:", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow4, DetailCell4, DateTime.Now.ToString("dd-MM-yy HH:mm"), "Left", "Right", )

                                                                      'Event Manager(s)
                                                                      Dim HumanResourceRow5 As New HtmlGenericControl("tr")
                                                                      Body.Controls.Add(HumanResourceRow5)

                                                                      Dim NameCell5 As New HtmlGenericControl("td")
                                                                      Dim DetailCell5 As New HtmlGenericControl("td")

                                                                      CreateCell(HumanResourceRow5, NameCell5, "Event Manager(s):", "Left", "Left", True, )
                                                                      CreateCell(HumanResourceRow5, DetailCell5, EventManagers, "Left", "Right", )

                                                                    End Sub,
                                                                    "Production Details")

    End Sub

    Private Sub SetupUnReconciledProductionsEmail(ByRef Email As Singular.Emails.Email, Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String, SystemID As Integer, ProductionDetailTable As CustomHTMLTable)

      Dim ToEmailAddress As String = ""
      Dim CCEmailAddress As String = ""
      'Dim SystemList As List(Of OBLib.Maintenance.Company.System)
      'If SystemID <> 0 Then
      '  SystemList = CommonData.Lists.SystemList.Where(Function(d) CompareSafe(d.SystemID, SystemID)).ToList
      'Else
      '  SystemList = CommonData.Lists.SystemList.ToList
      'End If
      Dim System As OBLib.Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(SystemID)
      Dim ToUserList As ROUserList = ROUserList.NewROUserList
      Dim CCUserList As ROUserList = ROUserList.NewROUserList
      Dim ToEmailAddressList As List(Of String) = New List(Of String)
      Dim CCEmailAddressList As List(Of String) = New List(Of String)

      'SystemList.ToList.ForEach(Sub(z)
      AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.UnFinalisingCrew)
      'End Sub)

      ToEmailAddress = BuildEmailRecipients(ToUserList, ToEmailAddressList)
      CCEmailAddress = BuildEmailRecipients(CCUserList, CCEmailAddressList)

      Dim EventType As String = If(IsNullNothing(CommonData.Lists.ROEventTypeList.GetItem(Production.EventTypeID)), "", CommonData.Lists.ROEventTypeList.GetItem(Production.EventTypeID).EventType)

      With Email
        .Subject = "Un-Finalising Crew: " & Production.ProductionRefNo & " " & EventType
        '.HTMLBody = UnFinalisingCrewHTML(Production, User, EventManagers, ProductionDetailTable)
        .Body = UnFinalisingCrewHTML(Production, User, EventManagers, ProductionDetailTable)
        .ToEmailAddress = ToEmailAddress
        .CCEmailAddresses = CCEmailAddress
        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
        .DateToSend = Now
      End With

    End Sub

    Private Function UnFinalisingCrewHTML(Production As OBLib.Productions.Old.Production, User As OBLib.Security.User, EventManagers As String, ProductionDetailTable As CustomHTMLTable)

      'Parent Table
      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                      Sub()
                                                      End Sub,
                                                      "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)


      'Email Body Message
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateEmailBodyMessage(Body)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)
      AddTableToParentTable(ParentTable, ProductionDetailTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
      ParentTable.Table.RenderControl(htwParent)

      Return "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Un-Finalisng Crew</title>" & _
                "</head>" & _
                "<body>" & _
                    swParent.ToString & _
                "</body>" & _
              "</html>"

    End Function

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl)

      'Dim Discipline As String = CommonData.Lists.DisciplineList.GetItem(DisciplineID).Discipline
      'Dim User
      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day"
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      Dim MessageRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow1)
      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      MessageRow1Cell1.InnerHtml = "The following Production's status has been changed:"
      MessageRow1.Controls.Add(MessageRow1Cell1)

      AddBlankTableRow(Body)

      'Dim MessageRow2 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow2)
      'Dim MessageRow2Cell1 As New HtmlGenericControl("td")
      'MessageRow2Cell1.InnerHtml = "Name: " & Production.ProductionDescription
      'MessageRow2.Controls.Add(MessageRow2Cell1)

      'Dim MessageRow3 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow3)
      'Dim MessageRow3Cell1 As New HtmlGenericControl("td")
      'MessageRow3Cell1.InnerHtml = "Un-reconciled By: " & User.FullName
      'MessageRow3.Controls.Add(MessageRow3Cell1)

      'Dim MessageRow4 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow4)
      'Dim MessageRow4Cell1 As New HtmlGenericControl("td")
      'MessageRow4Cell1.InnerHtml = "Un-reconciled Date: " & DateTime.Now.ToString("dd-MM-yy HH:mm")
      'MessageRow4.Controls.Add(MessageRow4Cell1)

      'Dim MessageRow5 As New HtmlGenericControl("tr")
      'Body.Controls.Add(MessageRow5)
      'Dim MessageRow5Cell1 As New HtmlGenericControl("td")
      'MessageRow5Cell1.InnerHtml = "Event Manager(s): " & EventManagers
      'MessageRow5.Controls.Add(MessageRow5Cell1)

      'AddBlankTableRow(Body)

    End Sub

  End Class

  '#Region " ICR - Shifts "

  '  Public Class StaffDisputedShifts

  '    Public Property Email As Singular.Emails.Email

  '    Public Sub New(DisputedList As List(Of Shifts.ICR.Auth.HRStaffAcknowledgeDetail), StaffName As String)
  '      Me.Email = StaffDisputedShiftsEmail(DisputedList, StaffName)
  '    End Sub

  '    Public Function StaffDisputedShiftsEmail(DisputedList As List(Of Shifts.ICR.Auth.HRStaffAcknowledgeDetail), StaffName As String) As Singular.Emails.Email

  '      Dim StaffShiftDetailTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                          Sub()
  '                                                          End Sub,
  '                                                          "")
  '      'BuildProductionDetailTable(ProductionDetailTable, Production, User, EventManagers)

  '      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()
  '      SetupUnReconciledProductionsEmail(em, DisputedList, StaffName, StaffShiftDetailTable)
  '      Return em

  '    End Function

  '    Private Sub SetupUnReconciledProductionsEmail(ByRef Email As Singular.Emails.Email, DisputedList As List(Of Shifts.ICR.Auth.HRStaffAcknowledgeDetail), StaffName As String, StaffShiftDetailTable As CustomHTMLTable)

  '      Dim ToEmailAddress As String = ""
  '      Dim CCEmailAddress As String = ""
  '      'Dim SystemList As List(Of OBLib.Maintenance.Company.System)
  '      'If SystemID <> 0 Then
  '      '  SystemList = CommonData.Lists.SystemList.Where(Function(d) CompareSafe(d.SystemID, SystemID)).ToList
  '      'Else
  '      '  SystemList = CommonData.Lists.SystemList.ToList
  '      'End If
  '      Dim System As OBLib.Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(OBLib.Security.Settings.CurrentUser.SystemID)
  '      Dim ToUserList As ROUserList = ROUserList.NewROUserList
  '      Dim CCUserList As ROUserList = ROUserList.NewROUserList
  '      Dim ToEmailAddressList As List(Of String) = New List(Of String)
  '      Dim CCEmailAddressList As List(Of String) = New List(Of String)

  '      'SystemList.ToList.ForEach(Sub(z)
  '      AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.DisputedShifts)
  '      'End Sub)

  '      ToEmailAddress = BuildEmailRecipients(ToUserList, ToEmailAddressList)
  '      CCEmailAddress = BuildEmailRecipients(CCUserList, CCEmailAddressList)

  '      With Email
  '        .Subject = "Disputed Shifts by " & StaffName
  '        .Body = DisputedShiftDetailHTML(DisputedList, StaffName, StaffShiftDetailTable)
  '        .ToEmailAddress = ToEmailAddress
  '        .CCEmailAddresses = CCEmailAddress
  '        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
  '        .DateToSend = Now
  '      End With

  '    End Sub

  '    Private Function DisputedShiftDetailHTML(DisputedList As List(Of Shifts.ICR.Auth.HRStaffAcknowledgeDetail), StaffName As String, StaffShiftDetailTable As CustomHTMLTable) As String

  '      'Parent Table
  '      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                      Sub()
  '                                                      End Sub,
  '                                                      "")

  '      'Header Banner
  '      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                          Sub(Body)
  '                                                            PopulateHeaderTable(Body)
  '                                                          End Sub,
  '                                                          "")

  '      HeaderBannerTable.SetupTableAttributes("width", "100%")
  '      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
  '      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
  '      HeaderBannerTable.SetupTableAttributes("border", "0")
  '      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

  '      AddTableToParentTable(ParentTable, HeaderBannerTable)


  '      'Email Body Message
  '      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                          Sub(Body)
  '                                                            PopulateEmailBodyMessage(Body, StaffName)
  '                                                          End Sub,
  '                                                          "")

  '      AddTableToParentTable(ParentTable, EmailMessageTable)

  '      'Detail
  '      Dim DetailTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Shift Type", "Start Date", "End Date", "Extra Shift", "Dispute Reason"}, _
  '                                                          Sub(Body)
  '                                                            PopulateDisputedShifts(Body, DisputedList)
  '                                                          End Sub,
  '                                                          "Shift Details")

  '      AddTableToParentTable(ParentTable, DetailTable)

  '      AddTableToParentTable(ParentTable, StaffShiftDetailTable)

  '      Dim swParent As StringWriter = New StringWriter()
  '      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
  '      ParentTable.Table.RenderControl(htwParent)

  '      Return "<!DOCTYPE html>" & _
  '              "<html>" & _
  '                "<head>" & _
  '                  "<title>Disputed Shift Details</title>" & _
  '                "</head>" & _
  '                "<body>" & _
  '                    swParent.ToString & _
  '                "</body>" & _
  '              "</html>"

  '    End Function

  '    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, StaffName As String)

  '      'Dim Discipline As String = CommonData.Lists.DisciplineList.GetItem(DisciplineID).Discipline
  '      'Dim User
  '      Dim GreetingRow As New HtmlGenericControl("tr")
  '      Body.Controls.Add(GreetingRow)
  '      Dim GreetingRowCell As New HtmlGenericControl("td")
  '      GreetingRowCell.InnerHtml = "Good Day"
  '      GreetingRow.Controls.Add(GreetingRowCell)

  '      AddBlankTableRow(Body)

  '      Dim MessageRow1 As New HtmlGenericControl("tr")
  '      Body.Controls.Add(MessageRow1)
  '      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
  '      MessageRow1Cell1.InnerHtml = StaffName & " has disputed his/her shift(s). Please review the following shifts: "
  '      MessageRow1.Controls.Add(MessageRow1Cell1)

  '      'AddBlankTableRow(Body)

  '    End Sub

  '    Private Sub PopulateDisputedShifts(ByRef Body As HtmlGenericControl, DisputedList As List(Of Shifts.ICR.Auth.HRStaffAcknowledgeDetail))

  '      For Each shift As Shifts.ICR.Auth.HRStaffAcknowledgeDetail In DisputedList
  '        Dim ShiftDetailRow As New HtmlGenericControl("tr")
  '        Body.Controls.Add(ShiftDetailRow)

  '        Dim ShiftTypeCell As New HtmlGenericControl("td")
  '        Dim StartDateCell As New HtmlGenericControl("td")
  '        Dim EndDateCell As New HtmlGenericControl("td")
  '        Dim ExtraShiftCell As New HtmlGenericControl("td")
  '        Dim StaffDisputeReasonCell As New HtmlGenericControl("td")

  '        CreateCell(ShiftDetailRow, ShiftTypeCell, shift.ShiftType, "Left", "Left", )
  '        CreateCell(ShiftDetailRow, StartDateCell, CDate(shift.StartDateTime).ToString("dd-MMM-yy HH:mm"), "center", , )
  '        CreateCell(ShiftDetailRow, EndDateCell, CDate(shift.EndDateTime).ToString("dd-MMM-yy HH:mm"), "center", , )
  '        CreateCell(ShiftDetailRow, ExtraShiftCell, shift.ExtraShiftInd, "center", , )
  '        CreateCell(ShiftDetailRow, StaffDisputeReasonCell, shift.StaffDisputeReason, "center", "Right", )

  '      Next

  '    End Sub

  '  End Class

  '  Public Class StaffNotification
  '    Public Property Email As Singular.Emails.Email

  '    Public Sub New(Notes As String, HRs As List(Of OBLib.Helpers.MultiSelectROHumanResource))
  '      Me.Email = StaffNotificationEmail(Notes, HRs)
  '    End Sub

  '    Public Function StaffNotificationEmail(Notes As String, HRs As List(Of OBLib.Helpers.MultiSelectROHumanResource)) As Singular.Emails.Email

  '      Dim StaffShiftNotificationTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                          Sub()
  '                                                          End Sub,
  '                                                          "")

  '      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()
  '      SetupStaffNotificationEmail(em, Notes, HRs, StaffShiftNotificationTable)
  '      Return em

  '    End Function

  '    Private Sub SetupStaffNotificationEmail(ByRef Email As Singular.Emails.Email, Notes As String, HRs As List(Of OBLib.Helpers.MultiSelectROHumanResource), StaffShiftNotificationTable As CustomHTMLTable)

  '      Dim ToEmailAddress As String = ""
  '      Dim CCEmailAddress As String = ""

  '      Dim System As OBLib.Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(OBLib.Security.Settings.CurrentUser.SystemID)
  '      Dim ToUserList As ROUserList = ROUserList.NewROUserList
  '      Dim CCUserList As ROUserList = ROUserList.NewROUserList
  '      Dim ToEmailAddressList As List(Of String) = New List(Of String)
  '      Dim CCEmailAddressList As List(Of String) = New List(Of String)

  '      AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.ICRStaffNotification)

  '      HRs.ForEach(Sub(z)
  '                    If ToEmailAddress = "" Then
  '                      ToEmailAddress += z.EmailAddress
  '                    Else
  '                      ToEmailAddress += ";" & z.EmailAddress
  '                    End If
  '                  End Sub)

  '      CCEmailAddress = BuildEmailRecipients(CCUserList, CCEmailAddressList)

  '      With Email
  '        .Subject = "Shift Detail Notification"
  '        .Body = ShiftNotificationHTML(Notes, StaffShiftNotificationTable)
  '        .ToEmailAddress = ToEmailAddress
  '        .CCEmailAddresses = CCEmailAddress
  '        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
  '        .DateToSend = Now
  '      End With

  '    End Sub

  '    Private Function ShiftNotificationHTML(Notes As String, StaffShiftNotificationTable As CustomHTMLTable) As String

  '      'Parent Table
  '      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                      Sub()
  '                                                      End Sub,
  '                                                      "")

  '      'Header Banner
  '      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                          Sub(Body)
  '                                                            PopulateHeaderTable(Body)
  '                                                          End Sub,
  '                                                          "")

  '      HeaderBannerTable.SetupTableAttributes("width", "100%")
  '      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
  '      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
  '      HeaderBannerTable.SetupTableAttributes("border", "0")
  '      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

  '      AddTableToParentTable(ParentTable, HeaderBannerTable)


  '      'Email Body Message
  '      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
  '                                                          Sub(Body)
  '                                                            PopulateEmailBodyMessage(Body, Notes)
  '                                                          End Sub,
  '                                                          "")

  '      AddTableToParentTable(ParentTable, EmailMessageTable)

  '      ''Detail
  '      'Dim DetailTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Shift Type", "Start Date", "End Date", "Extra Shift", "Dispute Reason"}, _
  '      '                                                    Sub(Body)
  '      '                                                      PopulateDisputedShifts(Body, DisputedList)
  '      '                                                    End Sub,
  '      '                                                    "Shift Details")

  '      'AddTableToParentTable(ParentTable, DetailTable)

  '      AddTableToParentTable(ParentTable, StaffShiftNotificationTable)

  '      Dim swParent As StringWriter = New StringWriter()
  '      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
  '      ParentTable.Table.RenderControl(htwParent)

  '      Return "<!DOCTYPE html>" & _
  '              "<html>" & _
  '                "<head>" & _
  '                  "<title>Staff Shift Notification</title>" & _
  '                "</head>" & _
  '                "<body>" & _
  '                    swParent.ToString & _
  '                "</body>" & _
  '              "</html>"

  '    End Function

  '    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, Notes As String)

  '      'Dim Discipline As String = CommonData.Lists.DisciplineList.GetItem(DisciplineID).Discipline
  '      'Dim User
  '      Dim GreetingRow As New HtmlGenericControl("tr")
  '      Body.Controls.Add(GreetingRow)
  '      Dim GreetingRowCell As New HtmlGenericControl("td")
  '      GreetingRowCell.InnerHtml = "Good Day"
  '      GreetingRow.Controls.Add(GreetingRowCell)

  '      AddBlankTableRow(Body)

  '      Dim MessageRow1 As New HtmlGenericControl("tr")
  '      Body.Controls.Add(MessageRow1)
  '      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
  '      MessageRow1Cell1.InnerHtml = "Please note that your shift details have been modified. To view your shift details, logon to https://soberms.supersport.com."
  '      MessageRow1.Controls.Add(MessageRow1Cell1)

  '      AddBlankTableRow(Body)

  '      Dim MessageRow2 As New HtmlGenericControl("tr")
  '      Body.Controls.Add(MessageRow2)
  '      Dim MessageRow2Cell1 As New HtmlGenericControl("td")
  '      MessageRow2Cell1.InnerHtml = "Note: " & Notes
  '      MessageRow2.Controls.Add(MessageRow2Cell1)

  '      'AddBlankTableRow(Body)

  '    End Sub

  '  End Class

  '#End Region

  Public Class AnnualLeaveClash

    Public Property EmailList As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList

    Public Sub New(DataSet As DataSet, Parent As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, ProductionClashInd As Boolean, StatusID As Integer)

      If ProductionClashInd Then
        ProductionClashes(DataSet, Parent, StartDate, EndDate, StatusID)
      Else
        DisciplineClashes(DataSet, Parent, StartDate, EndDate)
      End If

    End Sub

    Private Function AnnualLeaveClashEmail(EmailUserList As ROUserList, CurrentUser As User, HumanResource As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, _
                                           Clashes As List(Of Helpers.ProductionClashDetails), StatusID As Integer) As Singular.Emails.Email

      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()
      SetupEmailAsAnnualLeaveClash(em, EmailUserList, CurrentUser, HumanResource, StartDate, EndDate, Clashes, StatusID)
      Return em

    End Function

    Private Function AnnualLeaveClashEmail(EmailUserList As ROUserList, CCUserList As ROUserList, CurrentUser As User, HumanResource As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, _
                                       Clashes As List(Of Helpers.OffPeriodDisciplineHRClashes)) As Singular.Emails.Email

      Dim em As Singular.Emails.Email = Singular.Emails.Email.NewEmail()
      SetupEmailAsAnnualLeaveClash(em, EmailUserList, CCUserList, CurrentUser, HumanResource, StartDate, EndDate, Clashes)
      Return em

    End Function

    Private Sub SetupEmailAsAnnualLeaveClash(ByRef Email As Singular.Emails.Email, EmailUserList As ROUserList, CurrentUser As User, _
                                            HumanResource As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, _
                                            Clashes As List(Of Helpers.ProductionClashDetails), StatusID As Integer)

      Dim PrimaryEmailAddress As String = ""

      'Add email addresses of the users that need to be notified
      For Each User As ROUser In EmailUserList
        If PrimaryEmailAddress = "" Then
          PrimaryEmailAddress = User.EmailAddress
        ElseIf User.EmailAddress <> "" Then
          PrimaryEmailAddress += "; " & User.EmailAddress
        End If
      Next


      With Email
        .Subject = "Annual Leave Clash with Production"
        .Body = AnnualLeaveClashHTML(CurrentUser, HumanResource, StatusID, StartDate, EndDate, Clashes)
        .ToEmailAddress = PrimaryEmailAddress
        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
        .DateToSend = Now
      End With

    End Sub

    Private Sub SetupEmailAsAnnualLeaveClash(ByRef Email As Singular.Emails.Email, EmailUserList As ROUserList, CCUserList As ROUserList, CurrentUser As User, _
                                            HumanResource As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, _
                                            Clashes As List(Of Helpers.OffPeriodDisciplineHRClashes))

      Dim PrimaryEmailAddress As String = ""
      Dim CcEmailAddress As String = ""

      'Add email addresses of the users that need to be notified
      For Each User As ROUser In EmailUserList
        If PrimaryEmailAddress = "" Then
          PrimaryEmailAddress = User.EmailAddress
        ElseIf User.EmailAddress <> "" Then
          PrimaryEmailAddress += "; " & User.EmailAddress
        End If
      Next

      'Add email addresses of the users that need to be cc
      For Each User As ROUser In CCUserList
        If CcEmailAddress = "" Then
          CcEmailAddress = User.EmailAddress
        ElseIf User.EmailAddress <> "" Then
          CcEmailAddress += "; " & User.EmailAddress
        End If
      Next

      With Email
        .Subject = "Annual Leave Clash within a Discipline"
        .Body = AnnualLeaveClashHTML(CurrentUser, HumanResource, StartDate, EndDate, Clashes)
        .ToEmailAddress = PrimaryEmailAddress
        .CCEmailAddresses = CcEmailAddress
        .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
        .DateToSend = Now
      End With

    End Sub

    Private Function AnnualLeaveClashHTML(CurrentUser As User, HumanResource As HR.HumanResource, StatusID As Integer, StartDate As SmartDate, EndDate As SmartDate, _
                                          Clashes As List(Of Helpers.ProductionClashDetails))


      'Parent Table
      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                      Sub()
                                                      End Sub,
                                                      "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)

      'Email Body Message
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateEmailBodyMessage(Body, CurrentUser.FullName, HumanResource.Firstname, StatusID, StartDate, EndDate)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)

      'Clashes
      Dim ClashTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)

                                                            For Each Clash As Helpers.ProductionClashDetails In Clashes

                                                              Dim ClashRow As New HtmlGenericControl("tr")
                                                              Body.Controls.Add(ClashRow)

                                                              Dim ClashCell As New HtmlGenericControl("td")
                                                              ClashCell.InnerHtml = Clash.ProductionDescription & " on " & Clash.TxDate.Value.ToString("dd-MMM-yy")
                                                              ClashRow.Controls.Add(ClashCell)

                                                            Next


                                                          End Sub,
                                                          "This clashes with: ")

      AddTableToParentTable(ParentTable, ClashTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
      ParentTable.Table.RenderControl(htwParent)

      Return "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Annual Leave Clash with Production(s)</title>" & _
                "</head>" & _
                "<body>" & _
                  swParent.ToString & _
                "</body>" & _
              "</html>"

    End Function

    Private Function AnnualLeaveClashHTML(CurrentUser As User, HumanResource As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, _
                                          Clashes As List(Of Helpers.OffPeriodDisciplineHRClashes))


      'Parent Table
      Dim ParentTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                      Sub()
                                                      End Sub,
                                                      "")

      'Header Banner
      Dim HeaderBannerTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateHeaderTable(Body)
                                                          End Sub,
                                                          "")

      HeaderBannerTable.SetupTableAttributes("width", "100%")
      HeaderBannerTable.SetupTableAttributes("cellpadding", "0")
      HeaderBannerTable.SetupTableAttributes("cellspacing", "0")
      HeaderBannerTable.SetupTableAttributes("border", "0")
      HeaderBannerTable.SetupTableAttributes("bgcolor", "#2F3232")

      AddTableToParentTable(ParentTable, HeaderBannerTable)

      'Email Body Message
      Dim EmailMessageTable As CustomHTMLTable = New CustomHTMLTable(New String() {}, _
                                                          Sub(Body)
                                                            PopulateDisciplineEmailBodyMessage(Body, CurrentUser.FullName, HumanResource.Firstname, StartDate, EndDate)
                                                          End Sub,
                                                          "")

      AddTableToParentTable(ParentTable, EmailMessageTable)

      'Clashes
      Dim ClashTable As CustomHTMLTable = New CustomHTMLTable(New String() {"Discipline", "HumanResource", "Off Reason", "Leave Detail", "Start Date", "End Date"}, _
                                                          Sub(Body)

                                                            For Each Clash As Helpers.OffPeriodDisciplineHRClashes In Clashes

                                                              Dim ClashRow As New HtmlGenericControl("tr")
                                                              Body.Controls.Add(ClashRow)

                                                              Dim DisciplineCell As New HtmlGenericControl("td")
                                                              DisciplineCell.InnerHtml = Clash.Discipline
                                                              ClashRow.Controls.Add(DisciplineCell)

                                                              Dim HRCell As New HtmlGenericControl("td")
                                                              HRCell.InnerHtml = Clash.HumanResource
                                                              ClashRow.Controls.Add(HRCell)

                                                              Dim OffReasonCell As New HtmlGenericControl("td")
                                                              OffReasonCell.InnerHtml = Clash.OffReason
                                                              ClashRow.Controls.Add(OffReasonCell)


                                                              Dim LeaveDetailCell As New HtmlGenericControl("td")

                                                              LeaveDetailCell.InnerHtml = Clash.LeaveDetail
                                                              ClashRow.Controls.Add(LeaveDetailCell)

                                                              Dim StartDateCell As New HtmlGenericControl("td")
                                                              StartDateCell.InnerHtml = Clash.StartDate.ToString("dd-MMM-yyyy")
                                                              ClashRow.Controls.Add(StartDateCell)

                                                              Dim EndDateCell As New HtmlGenericControl("td")
                                                              EndDateCell.InnerHtml = Clash.EndDate.ToString("dd-MMM-yyyy")
                                                              ClashRow.Controls.Add(EndDateCell)


                                                            Next


                                                          End Sub,
                                                          "This clashes with: ")

      AddTableToParentTable(ParentTable, ClashTable)

      Dim swParent As StringWriter = New StringWriter()
      Dim htwParent As HtmlTextWriter = New HtmlTextWriter(swParent)
      ParentTable.Table.RenderControl(htwParent)

      Return "<!DOCTYPE html>" & _
              "<html>" & _
                "<head>" & _
                  "<title>Annual Leave Clash within a Discipline</title>" & _
                "</head>" & _
                "<body>" & _
                  swParent.ToString & _
                "</body>" & _
              "</html>"

    End Function

    Private Sub PopulateEmailBodyMessage(ByRef Body As HtmlGenericControl, CurrentUserName As String, HRName As String, StatusID As Integer, StartDate As SmartDate, EndDate As SmartDate)

      Dim TitleRow As New HtmlGenericControl("tr")
      Body.Controls.Add(TitleRow)
      Dim TitleRowCell As New HtmlGenericControl("td")
      TitleRowCell.InnerHtml = "Annual Leave Clash with Production(s)"
      TitleRowCell.Attributes("Style") = "font-weight: bold;" & _
                                         "font-size:20px;"
      TitleRow.Controls.Add(TitleRowCell)

      AddBlankTableRow(Body)

      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day"
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      Dim MessageRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow1)

      Dim StatusToString As String = ""
      If StatusID = CommonData.Enums.OffPeriodAuthorisationType.Pending Then
        StatusToString = "loaded"
      ElseIf StatusID = CommonData.Enums.OffPeriodAuthorisationType.Authorised Then
        StatusToString = "authorised "
      Else
        StatusToString = "Rejected "
      End If

      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      MessageRow1Cell1.InnerHtml = CurrentUserName & " has " & StatusToString & " leave for " & HRName & " from " & StartDate.Date.ToString("dd-MMM-yy") & " to " & EndDate.Date.ToString("dd-MMM-yy")
      MessageRow1.Controls.Add(MessageRow1Cell1)

      AddBlankTableRow(Body)

    End Sub

    Private Sub PopulateDisciplineEmailBodyMessage(ByRef Body As HtmlGenericControl, CurrentUserName As String, HRName As String, StartDate As SmartDate, EndDate As SmartDate)

      Dim TitleRow As New HtmlGenericControl("tr")
      Body.Controls.Add(TitleRow)
      Dim TitleRowCell As New HtmlGenericControl("td")
      TitleRowCell.InnerHtml = "Annual Leave Clash within a Discipline"
      TitleRowCell.Attributes("Style") = "font-weight: bold;" & _
                                         "font-size:20px;"
      TitleRow.Controls.Add(TitleRowCell)

      AddBlankTableRow(Body)

      Dim GreetingRow As New HtmlGenericControl("tr")
      Body.Controls.Add(GreetingRow)
      Dim GreetingRowCell As New HtmlGenericControl("td")
      GreetingRowCell.InnerHtml = "Good Day"
      GreetingRow.Controls.Add(GreetingRowCell)

      AddBlankTableRow(Body)

      Dim MessageRow1 As New HtmlGenericControl("tr")
      Body.Controls.Add(MessageRow1)
      Dim MessageRow1Cell1 As New HtmlGenericControl("td")
      MessageRow1Cell1.InnerHtml = CurrentUserName & " has captured leave for " & HRName & " from " & StartDate.Date.ToString("dd-MMM-yy") & " to " & EndDate.Date.ToString("dd-MMM-yy")
      MessageRow1.Controls.Add(MessageRow1Cell1)

      AddBlankTableRow(Body)

    End Sub

    Private Sub ProductionClashes(DataSet As DataSet, Parent As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate, StatusID As Integer)


      'Getting all Production Clashes
      Dim ProductionIDs As New List(Of Helpers.ProductionClashDetails)
      For Each row As DataRow In DataSet.Tables(0).Rows
        ProductionIDs.Add(New Helpers.ProductionClashDetails(row("ProductionID"), row("ProductionDescription"), row("CrewFinalisedInd"), row("TxDate")))
      Next

      'Getting event managers for the clashed productions
      Dim EventManagerList As List(Of Helpers.ProductionManagers) = New List(Of Helpers.ProductionManagers)
      For Each row As DataRow In DataSet.Tables(1).Rows
        EventManagerList.Add(New Helpers.ProductionManagers(row("ProductionID"), row("EventManagerID")))
      Next

      Dim ROProdBeforeCFList As New List(Of Helpers.ProductionClashDetails)
      Dim ROProdAfterCFList As New List(Of Helpers.ProductionClashDetails)

      'Separate Productions into before Crew finalised and after crew finalised
      For Each pID As Helpers.ProductionClashDetails In ProductionIDs
        If Not pID.CrewFinalisedInd Then
          ROProdBeforeCFList.Add(pID)
        Else
          ROProdAfterCFList.Add(pID)
        End If
      Next

      'System we are loggin in as
      Dim System As Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(Settings.CurrentUser.SystemID)
      Dim ClashCCEmail As Maintenance.Company.ClashCCEmail = Nothing

      'If there are productions that have not finalised their crew...
      If ROProdBeforeCFList.Count > 0 Then

        ClashCCEmail = System.ClashCCEmailList.Where(Function(d) CompareSafe(d.ClashTypeID, CInt(CommonData.Enums.ClashType.LeaveBeforeCrewFinalised))).FirstOrDefault

        'User List that the emails must be sent to
        Dim BeforeCFUserList As New Maintenance.General.ReadOnly.ROUserList

        'Get all users that need to receive the email
        If Not IsNullNothing(ClashCCEmail) Then
          For Each ClashCCEmailUser As Maintenance.Company.ClashCCEmailUser In ClashCCEmail.ClashCCEmailUserList
            Dim u = CommonData.Lists.ROUserList.GetItem(ClashCCEmailUser.UserID)
            BeforeCFUserList.Add(u)
          Next
        End If

        'Build HTML Description String
        'Dim HtmlString As String = BuildHtmlDescriptionString(ROProdBeforeCFList)

        Me.EmailList.Add(AnnualLeaveClashEmail(BeforeCFUserList, Settings.CurrentUser, Parent, StartDate, EndDate, ROProdBeforeCFList, StatusID))

      End If

      'If there are productions that have finalised their crew...
      If ROProdAfterCFList.Count > 0 Then

        ClashCCEmail = System.ClashCCEmailList.Where(Function(d) CompareSafe(d.ClashTypeID, CInt(CommonData.Enums.ClashType.LeaveAfterCrewFinalised))).FirstOrDefault

        'User List that the emails must be sent to
        Dim AfterCFUserList As ROUserList = ROUserList.NewROUserList

        'Get all users that need to receive the email
        If ClashCCEmail IsNot Nothing Then
          For Each ClashCCEmailUser As Maintenance.Company.ClashCCEmailUser In ClashCCEmail.ClashCCEmailUserList
            Dim u = CommonData.Lists.ROUserList.GetItem(ClashCCEmailUser.UserID)
            AfterCFUserList.Add(u)
          Next

          'Build HTML Description String
          'Dim HtmlString As String = BuildHtmlDescriptionString(ROProdAfterCFList)

          'Loop through Event Managers for Clashing Productions. EM also needs to be informed of the clash
          EventManagerList.Where(Function(d) Not IsNullNothing(d.ManagerID)) _
                          .ToList.ForEach(Sub(z)
                                            Dim User As ROUser = CommonData.Lists.ROUserList.Where(Function(c) c.HumanResourceID = z.ManagerID).FirstOrDefault
                                            If Not IsNullNothing(User) Then
                                              If Not AfterCFUserList.Contains(User) Then
                                                AfterCFUserList.Add(User)
                                              End If
                                            End If
                                          End Sub)

          Me.EmailList.Add(AnnualLeaveClashEmail(AfterCFUserList, Settings.CurrentUser, Parent, StartDate, EndDate, ROProdAfterCFList, StatusID))
        End If


      End If


    End Sub

    Private Sub DisciplineClashes(DataSet As DataSet, Parent As HR.HumanResource, StartDate As SmartDate, EndDate As SmartDate)

      ''Getting all Discipline Clashes
      'Dim DisciplineClashes As New List(Of Helpers.OffPeriodDisciplineHRClashes)
      'For Each row As DataRow In DataSet.Tables(0).Rows
      '  DisciplineClashes.Add(New Helpers.OffPeriodDisciplineHRClashes(row("HumanResourceID"), _
      '                                                                 row("Discipline"), _
      '                                                                 row("HumanResource"), _
      '                                                                 row("OffReason"), _
      '                                                                 row("LeaveDetail"), _
      '                                                                 row("StartDate"), _
      '                                                                 row("EndDate")))
      'Next

      ''User List that the emails must be sent to
      'Dim ToUserList As ROUserList = ROUserList.NewROUserList
      'Dim CCUserList As ROUserList = ROUserList.NewROUserList
      'Dim ToEmailAddressList As List(Of String) = New List(Of String)
      'Dim CCEmailAddressList As List(Of String) = New List(Of String)

      'For Each HRSystem As HR.HumanResourceSystem In Parent.HumanResourceSystemList.Where(Function(c) c.SystemID <> 7)
      '  Dim System As Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(HRSystem.SystemID)
      '  AddEmailRecipientsToList(System, ToUserList, CCUserList, ToEmailAddressList, CCEmailAddressList, CommonData.Enums.SystemEmailTypes.DisciplineLeaveOverlaps)
      'Next

      'Me.EmailList.Add(AnnualLeaveClashEmail(ToUserList, CCUserList, Settings.CurrentUser, Parent, StartDate, EndDate, DisciplineClashes))

    End Sub


  End Class

End Class