﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemovedHumanResourceList
    Inherits SingularReadOnlyListBase(Of RORemovedHumanResourceList, RORemovedHumanResource)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As RORemovedHumanResource

      For Each child As RORemovedHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetRORemainingFlight(HumanResourceID As Integer) As RORemainingFlight

      Dim obj As RORemainingFlight = Nothing
      For Each parent As RORemovedHumanResource In Me
        obj = parent.RORemainingFlightList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRORemainingAccommodation(HumanResourceID As Integer) As RORemainingAccommodation

      Dim obj As RORemainingAccommodation = Nothing
      For Each parent As RORemovedHumanResource In Me
        obj = parent.RORemainingAccommodationList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRORemainingRentalCar(HumanResourceID As Integer) As RORemainingRentalCar

      Dim obj As RORemainingRentalCar = Nothing
      For Each parent As RORemovedHumanResource In Me
        obj = parent.RORemainingRentalCarList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRORemainingSnT(HumanResource As String) As RORemainingSnT

      Dim obj As RORemainingSnT = Nothing
      For Each parent As RORemovedHumanResource In Me
        obj = parent.RORemainingSnTList.GetItem(HumanResource)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria

      Public HumanResourceIDXML As String = ""
      Public ProductionID As Integer? = Nothing
      Public SystemID As Integer? = Nothing
      Public ProductionAreaID As Integer? = Nothing

      Public Sub New(HumanResourceIDXML As String, ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)

        Me.HumanResourceIDXML = HumanResourceIDXML
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORemovedHumanResourceList() As RORemovedHumanResourceList

      Return New RORemovedHumanResourceList()

    End Function

    Public Shared Function GetRORemovedHumanResourceList(ProductionID As Integer?, SystemID As Integer?, HumanResourceIDXML As String, ProductionAreaID As Integer?) As RORemovedHumanResourceList

      Return DataPortal.Fetch(Of RORemovedHumanResourceList)(New Criteria(HumanResourceIDXML, ProductionID, SystemID, ProductionAreaID))

    End Function

    Public Shared Sub BeginGetRORemovedHumanResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORemovedHumanResourceList)))

      Dim dp As New DataPortal(Of RORemovedHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORemovedHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of RORemovedHumanResourceList)))

      Dim dp As New DataPortal(Of RORemovedHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORemovedHumanResourceList() As RORemovedHumanResourceList

      Return DataPortal.Fetch(Of RORemovedHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORemovedHumanResource.GetRORemovedHumanResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As RORemovedHumanResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.RORemainingFlightList.RaiseListChangedEvents = False
          parent.RORemainingFlightList.Add(RORemainingFlight.GetRORemainingFlight(sdr))
          parent.RORemainingFlightList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.RORemainingAccommodationList.RaiseListChangedEvents = False
          parent.RORemainingAccommodationList.Add(RORemainingAccommodation.GetRORemainingAccommodation(sdr))
          parent.RORemainingAccommodationList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.RORemainingRentalCarList.RaiseListChangedEvents = False
          parent.RORemainingRentalCarList.Add(RORemainingRentalCar.GetRORemainingRentalCar(sdr))
          parent.RORemainingRentalCarList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RORemainingSnTList.RaiseListChangedEvents = False
          parent.RORemainingSnTList.Add(RORemainingSnT.GetRORemainingSnT(sdr))
          parent.RORemainingSnTList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getRORemovedHumanResourceList"
            cm.Parameters.AddWithValue("@HumanResourceIDXML", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceIDXML))
            cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace