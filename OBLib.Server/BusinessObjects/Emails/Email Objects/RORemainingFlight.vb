﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemainingFlight
    Inherits SingularReadOnlyBase(Of RORemainingFlight)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared FlightNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightNo, "Flight No")
    ''' <summary>
    ''' Gets the Flight No value
    ''' </summary>
    <Display(Name:="Flight No", Description:="")>
    Public ReadOnly Property FlightNo() As String
      Get
        Return GetProperty(FlightNoProperty)
      End Get
    End Property

    Public Shared TravelTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelType, "Travel Type")
    ''' <summary>
    ''' Gets the Travel Description value
    ''' </summary>
    <Display(Name:="Travel Description", Description:="")>
    Public ReadOnly Property TravelType() As String
      Get
        Return GetProperty(TravelTypeProperty)
      End Get
    End Property

    Public Shared AirportFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AirportFrom, "Airport From")
    ''' <summary>
    ''' Gets the Airport From value
    ''' </summary>
    <Display(Name:="Airport From", Description:="")>
    Public ReadOnly Property AirportFrom() As String
      Get
        Return GetProperty(AirportFromProperty)
      End Get
    End Property

    Public Shared FlightStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FlightStartDate, "Flight Start Date")
    ''' <summary>
    ''' Gets the Flight Start Date value
    ''' </summary>
    <Display(Name:="Flight Start Date", Description:="")>
    Public ReadOnly Property FlightStartDate As DateTime?
      Get
        Return GetProperty(FlightStartDateProperty)
      End Get
    End Property

    Public Shared AirportToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AirportTo, "Airport To")
    ''' <summary>
    ''' Gets the Airport To value
    ''' </summary>
    <Display(Name:="Airport To", Description:="")>
    Public ReadOnly Property AirportTo() As String
      Get
        Return GetProperty(AirportToProperty)
      End Get
    End Property

    Public Shared FlightEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FlightEndDate, "Flight End Date")
    ''' <summary>
    ''' Gets the Flight End Date value
    ''' </summary>
    <Display(Name:="Flight End Date", Description:="")>
    Public ReadOnly Property FlightEndDate As DateTime?
      Get
        Return GetProperty(FlightEndDateProperty)
      End Get
    End Property

    Public Shared FlightIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlightID, "Flight", Nothing)
    ''' <summary>
    ''' Gets the Flight value
    ''' </summary>
    <Display(Name:="Flight", Description:="")>
    Public ReadOnly Property FlightID() As Integer?
      Get
        Return GetProperty(FlightIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORemainingFlight(dr As SafeDataReader) As RORemainingFlight

      Dim r As New RORemainingFlight()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(FlightNoProperty, .GetString(2))
        LoadProperty(TravelTypeProperty, .GetString(3))
        LoadProperty(AirportFromProperty, .GetString(4))
        LoadProperty(FlightStartDateProperty, .GetValue(5))
        LoadProperty(AirportToProperty, .GetString(6))
        LoadProperty(FlightEndDateProperty, .GetValue(7))
        LoadProperty(FlightIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace