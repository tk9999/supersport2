﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemainingRentalCar
    Inherits SingularReadOnlyBase(Of RORemainingRentalCar)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared BranchFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BranchFrom, "Branch From")
    ''' <summary>
    ''' Gets the Branch From value
    ''' </summary>
    <Display(Name:="Branch From", Description:="")>
    Public ReadOnly Property BranchFrom() As String
      Get
        Return GetProperty(BranchFromProperty)
      End Get
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PickupDateTime, "Pickup Date Time")
    ''' <summary>
    ''' Gets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="Pickup Date Time", Description:="")>
    Public ReadOnly Property PickupDateTime As DateTime?
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
    End Property

    Public Shared BranchToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BranchTo, "Branch To")
    ''' <summary>
    ''' Gets the Branch To value
    ''' </summary>
    <Display(Name:="Branch To", Description:="")>
    Public ReadOnly Property BranchTo() As String
      Get
        Return GetProperty(BranchToProperty)
      End Get
    End Property

    Public Shared DropoffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DropoffDateTime, "Dropoff Date Time")
    ''' <summary>
    ''' Gets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="Dropoff Date Time", Description:="")>
    Public ReadOnly Property DropoffDateTime As DateTime?
      Get
        Return GetProperty(DropoffDateTimeProperty)
      End Get
    End Property

    Public Shared CityFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityFrom, "City From")
    ''' <summary>
    ''' Gets the City From value
    ''' </summary>
    <Display(Name:="City From", Description:="")>
    Public ReadOnly Property CityFrom() As String
      Get
        Return GetProperty(CityFromProperty)
      End Get
    End Property

    Public Shared CityToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityTo, "City To")
    ''' <summary>
    ''' Gets the City To value
    ''' </summary>
    <Display(Name:="City To", Description:="")>
    Public ReadOnly Property CityTo() As String
      Get
        Return GetProperty(CityToProperty)
      End Get
    End Property

    Public Shared DriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverInd, "Driver", False)
    ''' <summary>
    ''' Gets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
    Public ReadOnly Property DriverInd() As Boolean
      Get
        Return GetProperty(DriverIndProperty)
      End Get
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarID, "Rental Car", Nothing)
    ''' <summary>
    ''' Gets the Rental Car value
    ''' </summary>
    <Display(Name:="Rental Car", Description:="")>
    Public ReadOnly Property RentalCarID() As Integer?
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORemainingRentalCar(dr As SafeDataReader) As RORemainingRentalCar

      Dim r As New RORemainingRentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(BranchFromProperty, .GetString(2))
        LoadProperty(PickupDateTimeProperty, .GetValue(3))
        LoadProperty(BranchToProperty, .GetString(4))
        LoadProperty(DropoffDateTimeProperty, .GetValue(5))
        LoadProperty(CityFromProperty, .GetString(6))
        LoadProperty(CityToProperty, .GetString(7))
        LoadProperty(DriverIndProperty, .GetBoolean(8))
        LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace