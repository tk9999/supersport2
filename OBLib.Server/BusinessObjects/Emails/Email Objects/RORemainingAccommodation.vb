﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemainingAccommodation
    Inherits SingularReadOnlyBase(Of RORemainingAccommodation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared AccommodationNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationNumber, "Accommodation Number")
    ''' <summary>
    ''' Gets the Accommodation Number value
    ''' </summary>
    <Display(Name:="Accommodation Number", Description:="")>
    Public ReadOnly Property AccommodationNumber() As Integer
      Get
        Return GetProperty(AccommodationNumberProperty)
      End Get
    End Property

    Public Shared AccommodationProviderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationProvider, "Accommodation Provider")
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:="")>
    Public ReadOnly Property AccommodationProvider() As String
      Get
        Return GetProperty(AccommodationProviderProperty)
      End Get
    End Property

    Public Shared AccommodationCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationCity, "Accommodation City")
    ''' <summary>
    ''' Gets the Accommodation City value
    ''' </summary>
    <Display(Name:="Accommodation City", Description:="")>
    Public ReadOnly Property AccommodationCity() As String
      Get
        Return GetProperty(AccommodationCityProperty)
      End Get
    End Property

    Public Shared AccommodationStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AccommodationStartDate, "Accommodation Start Date")
    ''' <summary>
    ''' Gets the Accommodation Start Date value
    ''' </summary>
    <Display(Name:="Accommodation Start Date", Description:="")>
    Public ReadOnly Property AccommodationStartDate As DateTime?
      Get
        Return GetProperty(AccommodationStartDateProperty)
      End Get
    End Property

    Public Shared AccommodationEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AccommodationEndDate, "Accommodation End Date")
    ''' <summary>
    ''' Gets the Accommodation End Date value
    ''' </summary>
    <Display(Name:="Accommodation End Date", Description:="")>
    Public ReadOnly Property AccommodationEndDate As DateTime?
      Get
        Return GetProperty(AccommodationEndDateProperty)
      End Get
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationID, "Accommodation", Nothing)
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="")>
    Public ReadOnly Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORemainingAccommodation(dr As SafeDataReader) As RORemainingAccommodation

      Dim r As New RORemainingAccommodation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(AccommodationNumberProperty, .GetInt32(2))
        LoadProperty(AccommodationProviderProperty, .GetString(3))
        LoadProperty(AccommodationCityProperty, .GetString(4))
        LoadProperty(AccommodationStartDateProperty, .GetValue(5))
        LoadProperty(AccommodationEndDateProperty, .GetValue(6))
        LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace