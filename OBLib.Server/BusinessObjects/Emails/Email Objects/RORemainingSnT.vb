﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemainingSnT
    Inherits SingularReadOnlyBase(Of RORemainingSnT)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SnTDayProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTDay, "Sn T Day")
    ''' <summary>
    ''' Gets the Sn T Day value
    ''' </summary>
    <Display(Name:="Sn T Day", Description:="")>
    Public ReadOnly Property SnTDay As DateTime?
      Get
        Return GetProperty(SnTDayProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets the International value
    ''' </summary>
    <Display(Name:="International", Description:="")>
    Public ReadOnly Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
    End Property

    Public Shared BreakfastAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.BreakfastAmount, "Breakfast Amount")
    ''' <summary>
    ''' Gets the Breakfast Amount value
    ''' </summary>
    <Display(Name:="Breakfast Amount", Description:="")>
    Public ReadOnly Property BreakfastAmount() As Decimal
      Get
        Return GetProperty(BreakfastAmountProperty)
      End Get
    End Property

    Public Shared LunchAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.LunchAmount, "Lunch Amount")
    ''' <summary>
    ''' Gets the Lunch Amount value
    ''' </summary>
    <Display(Name:="Lunch Amount", Description:="")>
    Public ReadOnly Property LunchAmount() As Decimal
      Get
        Return GetProperty(LunchAmountProperty)
      End Get
    End Property

    Public Shared DinnerAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DinnerAmount, "Dinner Amount")
    ''' <summary>
    ''' Gets the Dinner Amount value
    ''' </summary>
    <Display(Name:="Dinner Amount", Description:="")>
    Public ReadOnly Property DinnerAmount() As Decimal
      Get
        Return GetProperty(DinnerAmountProperty)
      End Get
    End Property

    Public Shared IncidentalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.IncidentalAmount, "Incidental Amount")
    ''' <summary>
    ''' Gets the Incidental Amount value
    ''' </summary>
    <Display(Name:="Incidental Amount", Description:="")>
    Public ReadOnly Property IncidentalAmount() As Decimal
      Get
        Return GetProperty(IncidentalAmountProperty)
      End Get
    End Property

    Public Shared GroupHumanResourceSnTIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupHumanResourceSnTID, "Group Human Resource Sn T", Nothing)
    ''' <summary>
    ''' Gets the Group Human Resource Sn T value
    ''' </summary>
    <Display(Name:="Group Human Resource Sn T", Description:="")>
    Public ReadOnly Property GroupHumanResourceSnTID() As Integer?
      Get
        Return GetProperty(GroupHumanResourceSnTIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORemainingSnT(dr As SafeDataReader) As RORemainingSnT

      Dim r As New RORemainingSnT()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceProperty, .GetString(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SnTDayProperty, .GetValue(2))
        LoadProperty(CountryProperty, .GetString(3))
        LoadProperty(InternationalIndProperty, .GetBoolean(4))
        LoadProperty(BreakfastAmountProperty, .GetDecimal(5))
        LoadProperty(LunchAmountProperty, .GetDecimal(6))
        LoadProperty(DinnerAmountProperty, .GetDecimal(7))
        LoadProperty(IncidentalAmountProperty, .GetDecimal(8))
        LoadProperty(GroupHumanResourceSnTIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace