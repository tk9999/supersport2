﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemainingSnTList
    Inherits SingularReadOnlyListBase(Of RORemainingSnTList, RORemainingSnT)

#Region " Parent "

    <NotUndoable()> Private mParent As RORemovedHumanResource
#End Region

#Region " Business Methods "

    Public Function GetItem(HumanResource As String) As RORemainingSnT

      For Each child As RORemainingSnT In Me
        If child.HumanResource = HumanResource Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewRORemainingSnTList() As RORemainingSnTList

      Return New RORemainingSnTList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace