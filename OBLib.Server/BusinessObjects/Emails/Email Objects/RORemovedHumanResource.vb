﻿' Generated 02 Sep 2014 22:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace EmailBusinessObjects

  <Serializable()> _
  Public Class RORemovedHumanResource
    Inherits SingularReadOnlyBase(Of RORemovedHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared FlightsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Flights, "Flights")
    ''' <summary>
    ''' Gets the Flights value
    ''' </summary>
    <Display(Name:="Flights", Description:="")>
    Public ReadOnly Property Flights() As Integer
      Get
        Return GetProperty(FlightsProperty)
      End Get
    End Property

    Public Shared RentalCarsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCars, "Rental Cars")
    ''' <summary>
    ''' Gets the Rental Cars value
    ''' </summary>
    <Display(Name:="Rental Cars", Description:="")>
    Public ReadOnly Property RentalCars() As Integer
      Get
        Return GetProperty(RentalCarsProperty)
      End Get
    End Property

    Public Shared AccommodationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Accommodation, "Accommodation")
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="")>
    Public ReadOnly Property Accommodation() As Integer
      Get
        Return GetProperty(AccommodationProperty)
      End Get
    End Property

    Public Shared SnTProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SnT, "Sn T")
    ''' <summary>
    ''' Gets the Sn T value
    ''' </summary>
    <Display(Name:="Sn T", Description:="")>
    Public ReadOnly Property SnT() As Integer
      Get
        Return GetProperty(SnTProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RORemainingFlightListProperty As PropertyInfo(Of RORemainingFlightList) = RegisterProperty(Of RORemainingFlightList)(Function(c) c.RORemainingFlightList, "RO Remaining Flight List")

    Public ReadOnly Property RORemainingFlightList() As RORemainingFlightList
      Get
        If GetProperty(RORemainingFlightListProperty) Is Nothing Then
          LoadProperty(RORemainingFlightListProperty, EmailBusinessObjects.RORemainingFlightList.NewRORemainingFlightList())
        End If
        Return GetProperty(RORemainingFlightListProperty)
      End Get
    End Property

    Public Shared RORemainingAccommodationListProperty As PropertyInfo(Of RORemainingAccommodationList) = RegisterProperty(Of RORemainingAccommodationList)(Function(c) c.RORemainingAccommodationList, "RO Remaining Accommodation List")

    Public ReadOnly Property RORemainingAccommodationList() As RORemainingAccommodationList
      Get
        If GetProperty(RORemainingAccommodationListProperty) Is Nothing Then
          LoadProperty(RORemainingAccommodationListProperty, EmailBusinessObjects.RORemainingAccommodationList.NewRORemainingAccommodationList())
        End If
        Return GetProperty(RORemainingAccommodationListProperty)
      End Get
    End Property

    Public Shared RORemainingRentalCarListProperty As PropertyInfo(Of RORemainingRentalCarList) = RegisterProperty(Of RORemainingRentalCarList)(Function(c) c.RORemainingRentalCarList, "RO Remaining Rental Car List")

    Public ReadOnly Property RORemainingRentalCarList() As RORemainingRentalCarList
      Get
        If GetProperty(RORemainingRentalCarListProperty) Is Nothing Then
          LoadProperty(RORemainingRentalCarListProperty, EmailBusinessObjects.RORemainingRentalCarList.NewRORemainingRentalCarList())
        End If
        Return GetProperty(RORemainingRentalCarListProperty)
      End Get
    End Property

    Public Shared RORemainingSnTListProperty As PropertyInfo(Of RORemainingSnTList) = RegisterProperty(Of RORemainingSnTList)(Function(c) c.RORemainingSnTList, "RO Remaining Sn T List")

    Public ReadOnly Property RORemainingSnTList() As RORemainingSnTList
      Get
        If GetProperty(RORemainingSnTListProperty) Is Nothing Then
          LoadProperty(RORemainingSnTListProperty, EmailBusinessObjects.RORemainingSnTList.NewRORemainingSnTList())
        End If
        Return GetProperty(RORemainingSnTListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORemovedHumanResource(dr As SafeDataReader) As RORemovedHumanResource

      Dim r As New RORemovedHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(FlightsProperty, .GetInt32(2))
        LoadProperty(RentalCarsProperty, .GetInt32(3))
        LoadProperty(AccommodationProperty, .GetInt32(4))
        LoadProperty(SnTProperty, .GetInt32(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace