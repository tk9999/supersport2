﻿' Generated 09 May 2014 07:44 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Finance.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROISPTimesheetDetailList
    Inherits SingularReadOnlyListBase(Of ROISPTimesheetDetailList, ROISPTimesheetDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROISPTimesheetHeader
#End Region

#Region " Business Methods "

    Public Function GetItem(CrewTimesheetID As Integer) As ROISPTimesheetDetail

      For Each child As ROISPTimesheetDetail In Me
        If child.CrewTimesheetID = CrewTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROISPTimesheetDetailList() As ROISPTimesheetDetailList

      Return New ROISPTimesheetDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace