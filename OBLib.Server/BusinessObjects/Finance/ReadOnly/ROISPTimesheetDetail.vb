﻿' Generated 09 May 2014 07:44 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Finance.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROISPTimesheetDetail
    Inherits SingularReadOnlyBase(Of ROISPTimesheetDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTimesheetID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdhocCrewTimesheetID, "Adhoc Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Adhoc Crew Timesheet value
    ''' </summary>
    <Display(Name:="Adhoc Crew Timesheet", Description:="")>
    Public ReadOnly Property AdhocCrewTimesheetID() As Integer?
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared PublicHolidayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PublicHolidayInd, "Public Holiday", False)
    ''' <summary>
    ''' Gets the Public Holiday value
    ''' </summary>
    <Display(Name:="Public Holiday", Description:="")>
    Public ReadOnly Property PublicHolidayInd() As Boolean
      Get
        Return GetProperty(PublicHolidayIndProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared CalculatedStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedStartDateTime, "Calculated Start Date Time")
    ''' <summary>
    ''' Gets the Calculated Start Date Time value
    ''' </summary>
    <Display(Name:="Calculated Start Date Time", Description:="")>
    Public ReadOnly Property CalculatedStartDateTime As DateTime?
      Get
        Return GetProperty(CalculatedStartDateTimeProperty)
      End Get
    End Property

    Public Shared CalculatedEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedEndDateTime, "Calculated End Date Time")
    ''' <summary>
    ''' Gets the Calculated End Date Time value
    ''' </summary>
    <Display(Name:="Calculated End Date Time", Description:="")>
    Public ReadOnly Property CalculatedEndDateTime As DateTime?
      Get
        Return GetProperty(CalculatedEndDateTimeProperty)
      End Get
    End Property

    Public Shared CrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Crew Start Date Time", Description:="")>
    Public ReadOnly Property CrewStartDateTime As DateTime?
      Get
        Return GetProperty(CrewStartDateTimeProperty)
      End Get
    End Property

    Public Shared CrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Crew End Date Time", Description:="")>
    Public ReadOnly Property CrewEndDateTime As DateTime?
      Get
        Return GetProperty(CrewEndDateTimeProperty)
      End Get
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForDay, "Hours For Day")
    ''' <summary>
    ''' Gets the Hours For Day value
    ''' </summary>
    <Display(Name:="Hours For Day", Description:="")>
    Public ReadOnly Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
    End Property

    Public Shared OvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Overtime, "Overtime")
    ''' <summary>
    ''' Gets the Overtime value
    ''' </summary>
    <Display(Name:="Overtime", Description:="")>
    Public ReadOnly Property Overtime() As Decimal
      Get
        Return GetProperty(OvertimeProperty)
      End Get
    End Property

    Public Shared SnTProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SnT, "Sn T")
    ''' <summary>
    ''' Gets the Sn T value
    ''' </summary>
    <Display(Name:="Sn T", Description:="")>
    Public ReadOnly Property SnT() As Decimal
      Get
        Return GetProperty(SnTProperty)
      End Get
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public ReadOnly Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
    Public ReadOnly Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Description

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROISPTimesheetDetail(dr As SafeDataReader) As ROISPTimesheetDetail

      Dim r As New ROISPTimesheetDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CrewTimesheetIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AdhocCrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PublicHolidayIndProperty, .GetBoolean(3))
        LoadProperty(DescriptionProperty, .GetString(4))
        LoadProperty(CalculatedStartDateTimeProperty, .GetValue(5))
        LoadProperty(CalculatedEndDateTimeProperty, .GetValue(6))
        LoadProperty(CrewStartDateTimeProperty, .GetValue(7))
        LoadProperty(CrewEndDateTimeProperty, .GetValue(8))
        LoadProperty(HoursForDayProperty, .GetDecimal(9))
        LoadProperty(OvertimeProperty, .GetDecimal(10))
        LoadProperty(SnTProperty, .GetDecimal(11))
        LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(AuthorisedDateTimeProperty, .GetValue(13))
        LoadProperty(CreatedByProperty, .GetInt32(14))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(15))
        LoadProperty(ModifiedByProperty, .GetInt32(16))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(17))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace