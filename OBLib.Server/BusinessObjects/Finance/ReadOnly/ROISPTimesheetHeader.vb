﻿' Generated 09 May 2014 07:44 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Finance.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROISPTimesheetHeader
    Inherits SingularReadOnlyBase(Of ROISPTimesheetHeader)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
    Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property FilteredInd As Boolean

#End Region

#Region " Child Lists "

    Public Shared ROISPTimesheetDetailListProperty As PropertyInfo(Of ROISPTimesheetDetailList) = RegisterProperty(Of ROISPTimesheetDetailList)(Function(c) c.ROISPTimesheetDetailList, "ROISP Timesheet Detail List")

    Public ReadOnly Property ROISPTimesheetDetailList() As ROISPTimesheetDetailList
      Get
        If GetProperty(ROISPTimesheetDetailListProperty) Is Nothing Then
          LoadProperty(ROISPTimesheetDetailListProperty, Finance.Timesheets.ReadOnly.ROISPTimesheetDetailList.NewROISPTimesheetDetailList())
        End If
        Return GetProperty(ROISPTimesheetDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROISPTimesheetHeader(dr As SafeDataReader) As ROISPTimesheetHeader

      Dim r As New ROISPTimesheetHeader()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(FirstNameProperty, .GetString(2))
        LoadProperty(SurnameProperty, .GetString(3))
        LoadProperty(EmployeeCodeProperty, .GetString(4))
        LoadProperty(IDNoProperty, .GetString(5))
        LoadProperty(PreferredNameProperty, .GetString(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace