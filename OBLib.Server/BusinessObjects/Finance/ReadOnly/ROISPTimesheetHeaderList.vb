﻿' Generated 09 May 2014 07:44 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.Strings

Namespace Finance.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROISPTimesheetHeaderList
    Inherits SingularReadOnlyListBase(Of ROISPTimesheetHeaderList, ROISPTimesheetHeader)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROISPTimesheetHeader

      For Each child As ROISPTimesheetHeader In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetROISPTimesheetDetail(CrewTimesheetID As Integer) As ROISPTimesheetDetail

      Dim obj As ROISPTimesheetDetail = Nothing
      For Each parent As ROISPTimesheetHeader In Me
        obj = parent.ROISPTimesheetDetailList.GetItem(CrewTimesheetID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HRIDXML As String = ""
      Public Property StartDate As Date? = Nothing
      Public Property EndDate As Date? = Nothing

      Public Sub New(HRIDXML As String, StartDate As Date?, EndDate As Date?)

        Me.HRIDXML = HRIDXML
        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROISPTimesheetHeaderList() As ROISPTimesheetHeaderList

      Return New ROISPTimesheetHeaderList()

    End Function

    Public Shared Sub BeginGetROISPTimesheetHeaderList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROISPTimesheetHeaderList)))

      Dim dp As New DataPortal(Of ROISPTimesheetHeaderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROISPTimesheetHeaderList(CallBack As EventHandler(Of DataPortalResult(Of ROISPTimesheetHeaderList)))

      Dim dp As New DataPortal(Of ROISPTimesheetHeaderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROISPTimesheetHeaderList() As ROISPTimesheetHeaderList

      Return DataPortal.Fetch(Of ROISPTimesheetHeaderList)(New Criteria())

    End Function

    Public Shared Function GetROISPTimesheetHeaderList(HRIDXML As String, StartDate As Date?, EndDate As Date?) As ROISPTimesheetHeaderList

      Return DataPortal.Fetch(Of ROISPTimesheetHeaderList)(New Criteria(HRIDXML, StartDate, EndDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROISPTimesheetHeader.GetROISPTimesheetHeader(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROISPTimesheetHeader = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROISPTimesheetDetailList.RaiseListChangedEvents = False
          parent.ROISPTimesheetDetailList.Add(ROISPTimesheetDetail.GetROISPTimesheetDetail(sdr))
          parent.ROISPTimesheetDetailList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getROISPTimesheetHeaderList"
            cm.Parameters.AddWithValue("@HumanResourceIDs", MakeEmptyDBNull(crit.HRIDXML))
            cm.Parameters.AddWithValue("@StartDate", MakeEmptyDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", MakeEmptyDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace