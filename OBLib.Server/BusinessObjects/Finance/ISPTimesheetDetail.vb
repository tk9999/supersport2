﻿' Generated 09 May 2014 13:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Finance.Timesheets

  <Serializable()> _
  Public Class ISPTimesheetDetail
    Inherits SingularBusinessBase(Of ISPTimesheetDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTimesheetID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdhocCrewTimesheetID, "Adhoc Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Adhoc Crew Timesheet value
    ''' </summary>
    <Display(Name:="Adhoc Crew Timesheet", Description:="")>
    Public Property AdhocCrewTimesheetID() As Integer?
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdhocCrewTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared PublicHolidayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PublicHolidayInd, "Public Holiday", False)
    ''' <summary>
    ''' Gets and sets the Public Holiday value
    ''' </summary>
    <Display(Name:="Public Holiday", Description:="")>
    Public Property PublicHolidayInd() As Boolean
      Get
        Return GetProperty(PublicHolidayIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PublicHolidayIndProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared CalculatedStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedStartDateTime, "Calculated Start Date Time")
    ''' <summary>
    ''' Gets and sets the Calculated Start Date Time value
    ''' </summary>
    <Display(Name:="Calculated Start Date Time", Description:="")>
    Public Property CalculatedStartDateTime As DateTime?
      Get
        Return GetProperty(CalculatedStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CalculatedStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CalculatedEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedEndDateTime, "Calculated End Date Time")
    ''' <summary>
    ''' Gets and sets the Calculated End Date Time value
    ''' </summary>
    <Display(Name:="Calculated End Date Time", Description:="")>
    Public Property CalculatedEndDateTime As DateTime?
      Get
        Return GetProperty(CalculatedEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CalculatedEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets and sets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Crew Start Date Time", Description:="")>
    Public Property CrewStartDateTime As DateTime?
      Get
        Return GetProperty(CrewStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets and sets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Crew End Date Time", Description:="")>
    Public Property CrewEndDateTime As DateTime?
      Get
        Return GetProperty(CrewEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForDay, "Hours For Day")
    ''' <summary>
    ''' Gets and sets the Hours For Day value
    ''' </summary>
    <Display(Name:="Hours For Day", Description:="")>
    Public Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(HoursForDayProperty, Value)
      End Set
    End Property

    Public Shared OvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Overtime, "Overtime")
    ''' <summary>
    ''' Gets and sets the Overtime value
    ''' </summary>
    <Display(Name:="Overtime", Description:="")>
    Public Property Overtime() As Decimal
      Get
        Return GetProperty(OvertimeProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(OvertimeProperty, Value)
      End Set
    End Property

    Public Shared SnTProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SnT, "Sn T")
    ''' <summary>
    ''' Gets and sets the Sn T value
    ''' </summary>
    <Display(Name:="Sn T", Description:="")>
    Public Property SnT() As Decimal
      Get
        Return GetProperty(SnTProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(SnTProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ISPTimesheetHeader

      Return CType(CType(Me.Parent, ISPTimesheetDetailList).Parent, ISPTimesheetHeader)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ISP Timesheet Detail")
        Else
          Return String.Format("Blank {0}", "ISP Timesheet Detail")
        End If
      Else
        Return Me.Description
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewISPTimesheetDetail() method.

    End Sub

    Public Shared Function NewISPTimesheetDetail() As ISPTimesheetDetail

      Return DataPortal.CreateChild(Of ISPTimesheetDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetISPTimesheetDetail(dr As SafeDataReader) As ISPTimesheetDetail

      Dim i As New ISPTimesheetDetail()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewTimesheetIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AdhocCrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PublicHolidayIndProperty, .GetBoolean(3))
          LoadProperty(DescriptionProperty, .GetString(4))
          LoadProperty(CalculatedStartDateTimeProperty, .GetValue(5))
          LoadProperty(CalculatedEndDateTimeProperty, .GetValue(6))
          LoadProperty(CrewStartDateTimeProperty, .GetValue(7))
          LoadProperty(CrewEndDateTimeProperty, .GetValue(8))
          LoadProperty(HoursForDayProperty, .GetDecimal(9))
          LoadProperty(OvertimeProperty, .GetDecimal(10))
          LoadProperty(SnTProperty, .GetDecimal(11))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(13))
          LoadProperty(CreatedByProperty, .GetInt32(14))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(15))
          LoadProperty(ModifiedByProperty, .GetInt32(16))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(17))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insISPTimesheetDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updISPTimesheetDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewTimesheetID As SqlParameter = .Parameters.Add("@CrewTimesheetID", SqlDbType.Int)
          paramCrewTimesheetID.Value = GetProperty(CrewTimesheetIDProperty)
          If Me.IsNew Then
            paramCrewTimesheetID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", Me.GetParent().HumanResourceID)
          .Parameters.AddWithValue("@AdhocCrewTimesheetID", GetProperty(AdhocCrewTimesheetIDProperty))
          .Parameters.AddWithValue("@PublicHolidayInd", GetProperty(PublicHolidayIndProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          cm.Parameters.AddWithValue("@CalculatedStartDateTime", (New SmartDate(GetProperty(CalculatedStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@CalculatedEndDateTime", (New SmartDate(GetProperty(CalculatedEndDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@CrewStartDateTime", (New SmartDate(GetProperty(CrewStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@CrewEndDateTime", (New SmartDate(GetProperty(CrewEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@HoursForDay", GetProperty(HoursForDayProperty))
          .Parameters.AddWithValue("@Overtime", GetProperty(OvertimeProperty))
          .Parameters.AddWithValue("@SnT", GetProperty(SnTProperty))
          .Parameters.AddWithValue("@AuthorisedByUserID", GetProperty(AuthorisedByUserIDProperty))
          cm.Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewTimesheetIDProperty, paramCrewTimesheetID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delISPTimesheetDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CrewTimesheetID", GetProperty(CrewTimesheetIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace