﻿' Generated 09 May 2014 13:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.Strings

Namespace Finance.Timesheets

  <Serializable()> _
  Public Class ISPTimesheetHeaderList
    Inherits SingularBusinessListBase(Of ISPTimesheetHeaderList, ISPTimesheetHeader)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ISPTimesheetHeader

      For Each child As ISPTimesheetHeader In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetISPTimesheetDetail(CrewTimesheetID As Integer) As ISPTimesheetDetail

      Dim obj As ISPTimesheetDetail = Nothing
      For Each parent As ISPTimesheetHeader In Me
        obj = parent.ISPTimesheetDetailList.GetItem(CrewTimesheetID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HRIDXML As String = ""
      Public Property StartDate As Date? = Nothing
      Public Property EndDate As Date? = Nothing

      Public Sub New(HRIDXML As String, StartDate As Date?, EndDate As Date?)

        Me.HRIDXML = HRIDXML
        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewISPTimesheetHeaderList() As ISPTimesheetHeaderList

      Return New ISPTimesheetHeaderList()

    End Function

    Public Shared Sub BeginGetISPTimesheetHeaderList(CallBack As EventHandler(Of DataPortalResult(Of ISPTimesheetHeaderList)))

      Dim dp As New DataPortal(Of ISPTimesheetHeaderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetISPTimesheetHeaderList() As ISPTimesheetHeaderList

      Return DataPortal.Fetch(Of ISPTimesheetHeaderList)(New Criteria())

    End Function

    Public Shared Function GetISPTimesheetHeaderList(HRIDXML As String, StartDate As Date?, EndDate As Date?) As ISPTimesheetHeaderList

      Return DataPortal.Fetch(Of ISPTimesheetHeaderList)(New Criteria(HRIDXML, StartDate, EndDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ISPTimesheetHeader.GetISPTimesheetHeader(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ISPTimesheetHeader = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ISPTimesheetDetailList.RaiseListChangedEvents = False
          parent.ISPTimesheetDetailList.Add(ISPTimesheetDetail.GetISPTimesheetDetail(sdr))
          parent.ISPTimesheetDetailList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As ISPTimesheetHeader In Me
        child.CheckRules()
        For Each ISPTimesheetDetail As ISPTimesheetDetail In child.ISPTimesheetDetailList
          ISPTimesheetDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getROISPTimesheetHeaderList"
            cm.Parameters.AddWithValue("@HumanResourceIDs", MakeEmptyDBNull(crit.HRIDXML))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ISPTimesheetHeader In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ISPTimesheetHeader In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace