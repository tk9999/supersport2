﻿' Generated 09 May 2014 13:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Finance.Timesheets

  <Serializable()> _
  Public Class ISPTimesheetHeader
    Inherits SingularBusinessBase(Of ISPTimesheetHeader)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name")
    ''' <summary>
    ''' Gets and sets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
    Public Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FirstNameProperty, Value)
      End Set
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets and sets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SurnameProperty, Value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets and sets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmployeeCodeProperty, Value)
      End Set
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets and sets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IDNoProperty, Value)
      End Set
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets and sets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PreferredNameProperty, Value)
      End Set
    End Property

    <Display(Name:="Select?")>
    Public Property SelectedInd As Boolean

    <ClientOnly()>
    Public Property FilteredInd As Boolean

#End Region

#Region " Child Lists "

    Public Shared ISPTimesheetDetailListProperty As PropertyInfo(Of ISPTimesheetDetailList) = RegisterProperty(Of ISPTimesheetDetailList)(Function(c) c.ISPTimesheetDetailList, "ISP Timesheet Detail List")

    Public ReadOnly Property ISPTimesheetDetailList() As ISPTimesheetDetailList
      Get
        If GetProperty(ISPTimesheetDetailListProperty) Is Nothing Then
          LoadProperty(ISPTimesheetDetailListProperty, Finance.Timesheets.ISPTimesheetDetailList.NewISPTimesheetDetailList())
        End If
        Return GetProperty(ISPTimesheetDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ISP Timesheet Header")
        Else
          Return String.Format("Blank {0}", "ISP Timesheet Header")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewISPTimesheetHeader() method.

    End Sub

    Public Shared Function NewISPTimesheetHeader() As ISPTimesheetHeader

      Return DataPortal.CreateChild(Of ISPTimesheetHeader)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetISPTimesheetHeader(dr As SafeDataReader) As ISPTimesheetHeader

      Dim i As New ISPTimesheetHeader()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceProperty, .GetString(1))
          LoadProperty(FirstNameProperty, .GetString(2))
          LoadProperty(SurnameProperty, .GetString(3))
          LoadProperty(EmployeeCodeProperty, .GetString(4))
          LoadProperty(IDNoProperty, .GetString(5))
          LoadProperty(PreferredNameProperty, .GetString(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insISPTimesheetHeader"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updISPTimesheetHeader"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
          paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
          If Me.IsNew Then
            paramHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
          .Parameters.AddWithValue("@FirstName", GetProperty(FirstNameProperty))
          .Parameters.AddWithValue("@Surname", GetProperty(SurnameProperty))
          .Parameters.AddWithValue("@EmployeeCode", GetProperty(EmployeeCodeProperty))
          .Parameters.AddWithValue("@IDNo", GetProperty(IDNoProperty))
          .Parameters.AddWithValue("@PreferredName", GetProperty(PreferredNameProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
          End If
          ' update child objects
          If GetProperty(ISPTimesheetDetailListProperty) IsNot Nothing Then
            Me.ISPTimesheetDetailList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ISPTimesheetDetailListProperty) IsNot Nothing Then
          Me.ISPTimesheetDetailList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delISPTimesheetHeader"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace