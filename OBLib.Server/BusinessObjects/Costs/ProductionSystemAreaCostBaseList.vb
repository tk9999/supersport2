﻿' Generated 08 Nov 2016 05:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionSystemAreaCostBaseList
    Inherits OBBusinessListBase(Of ProductionSystemAreaCostBaseList, ProductionSystemAreaCostBase)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaCostID As Integer) As ProductionSystemAreaCostBase

      For Each child As ProductionSystemAreaCostBase In Me
        If child.ProductionSystemAreaCostID = ProductionSystemAreaCostID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production System Area Costs"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewProductionSystemAreaCostBaseList() As ProductionSystemAreaCostBaseList

      Return New ProductionSystemAreaCostBaseList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductionSystemAreaCostBaseList() As ProductionSystemAreaCostBaseList

      Return DataPortal.Fetch(Of ProductionSystemAreaCostBaseList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionSystemAreaCostBase.GetProductionSystemAreaCostBase(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionSystemAreaCostBaseList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace