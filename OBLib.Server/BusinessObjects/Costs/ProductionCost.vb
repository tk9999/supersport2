﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCost
    Inherits OBBusinessBase(Of ProductionCost)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
  Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets and sets the Production Ref No value
    ''' </summary>
    <Display(Name:="Production Ref No", Description:=""),
    StringLength(50, ErrorMessage:="Production Ref No cannot be more than 50 characters")>
  Public Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionRefNoProperty, Value)
      End Set
    End Property

    Public Shared OldTravelScreenIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OldTravelScreenInd, "Old Travel Screen", False)
    ''' <summary>
    ''' Gets and sets the Old Travel Screen value
    ''' </summary>
    <Display(Name:="Old Travel Screen", Description:=""),
    Required(ErrorMessage:="Old Travel Screen required")>
  Public Property OldTravelScreenInd() As Boolean
      Get
        Return GetProperty(OldTravelScreenIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OldTravelScreenIndProperty, Value)
      End Set
    End Property

    Public Shared SynergyGenRefNumProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNum, "Synergy Gen Ref Num", Nothing)
    ''' <summary>
    ''' Gets and sets the Synergy Gen Ref Num value
    ''' </summary>
    <Display(Name:="Synergy Gen Ref Num", Description:=""),
    Required(ErrorMessage:="Synergy Gen Ref Num required")>
    Public Property SynergyGenRefNum() As Int64?
      Get
        Return GetProperty(SynergyGenRefNumProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(SynergyGenRefNumProperty, Value)
      End Set
    End Property

    Public Shared TotalCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalCost, "Total Cost")
    <Display(Name:="Total Cost", Description:=""),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property TotalCost() As Decimal
      Get
        Return GetProperty(TotalCostProperty)
      End Get
      Set(value As Decimal)
        SetProperty(TotalCostProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionCostFlightListProperty As PropertyInfo(Of ProductionCostFlightList) = RegisterProperty(Of ProductionCostFlightList)(Function(c) c.ProductionCostFlightList, "Production Cost Flight List")

    Public ReadOnly Property ProductionCostFlightList() As ProductionCostFlightList
      Get
        If GetProperty(ProductionCostFlightListProperty) Is Nothing Then
          LoadProperty(ProductionCostFlightListProperty, Costs.ProductionCostFlightList.NewProductionCostFlightList())
        End If
        Return GetProperty(ProductionCostFlightListProperty)
      End Get
    End Property

    Public Shared ProductionCostCarRentalListProperty As PropertyInfo(Of ProductionCostCarRentalList) = RegisterProperty(Of ProductionCostCarRentalList)(Function(c) c.ProductionCostCarRentalList, "Production Cost Car Rental List")

    Public ReadOnly Property ProductionCostCarRentalList() As ProductionCostCarRentalList
      Get
        If GetProperty(ProductionCostCarRentalListProperty) Is Nothing Then
          LoadProperty(ProductionCostCarRentalListProperty, Costs.ProductionCostCarRentalList.NewProductionCostCarRentalList())
        End If
        Return GetProperty(ProductionCostCarRentalListProperty)
      End Get
    End Property

    Public Shared ProductionCostAccomodationListProperty As PropertyInfo(Of ProductionCostAccomodationList) = RegisterProperty(Of ProductionCostAccomodationList)(Function(c) c.ProductionCostAccomodationList, "Production Cost Accomodation List")

    Public ReadOnly Property ProductionCostAccomodationList() As ProductionCostAccomodationList
      Get
        If GetProperty(ProductionCostAccomodationListProperty) Is Nothing Then
          LoadProperty(ProductionCostAccomodationListProperty, Costs.ProductionCostAccomodationList.NewProductionCostAccomodationList())
        End If
        Return GetProperty(ProductionCostAccomodationListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost")
        Else
          Return String.Format("Blank {0}", "Production Cost")
        End If
      Else
        Return Me.Description
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Sub UpdateAllCosts()

      For Each pca As ProductionCostAccomodation In Me.ProductionCostAccomodationList
        pca.TotalCost = pca.GetTotalAccommodationCost()
      Next

      For Each pcf As ProductionCostFlight In Me.ProductionCostFlightList
        pcf.TotalTicketCost = pcf.GetTotalTicketCost()
      Next

      Dim AccommodationTotal As Decimal = 0
      For Each pca As ProductionCostAccomodation In Me.ProductionCostAccomodationList
        AccommodationTotal += pca.TotalCost
      Next

      Dim FlightTotal As Decimal = 0
      For Each pcf As ProductionCostFlight In Me.ProductionCostFlightList
        FlightTotal += pcf.TotalTicketCost
      Next

      Dim RentalCarTotal As Decimal = 0
      For Each pcr As ProductionCostCarRental In Me.ProductionCostCarRentalList
        RentalCarTotal += pcr.RentalCost
      Next

      Me.TotalCost = AccommodationTotal + FlightTotal + RentalCarTotal

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCost() method.

    End Sub

    Public Shared Function NewProductionCost() As ProductionCost

      Return DataPortal.CreateChild(Of ProductionCost)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCost(dr As SafeDataReader) As ProductionCost

      Dim p As New ProductionCost()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionIDProperty, .GetInt32(0))
          LoadProperty(DescriptionProperty, .GetString(1))
          LoadProperty(ProductionRefNoProperty, .GetString(2))
          LoadProperty(OldTravelScreenIndProperty, .GetBoolean(3))
          LoadProperty(SynergyGenRefNumProperty, Singular.Misc.ZeroNothing(.GetInt64(4)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()
      UpdateAllCosts()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insProductionCost"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCost"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionID As SqlParameter = .Parameters.Add("@ProductionID", SqlDbType.Int)
          paramProductionID.Value = GetProperty(ProductionIDProperty)
          If Me.IsNew Then
            paramProductionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@ProductionRefNo", GetProperty(ProductionRefNoProperty))
          .Parameters.AddWithValue("@OldTravelScreenInd", GetProperty(OldTravelScreenIndProperty))
          .Parameters.AddWithValue("@SynergyGenRefNum", Singular.Misc.NothingDBNull(GetProperty(SynergyGenRefNumProperty)))

          '.ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionIDProperty, paramProductionID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionCostFlightListProperty) IsNot Nothing Then
            Me.ProductionCostFlightList.Update()
          End If
          If GetProperty(ProductionCostCarRentalListProperty) IsNot Nothing Then
            Me.ProductionCostCarRentalList.Update()
          End If
          If GetProperty(ProductionCostAccomodationListProperty) IsNot Nothing Then
            Me.ProductionCostAccomodationList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionCostFlightListProperty) IsNot Nothing Then
          Me.ProductionCostFlightList.Update()
        End If
        If GetProperty(ProductionCostCarRentalListProperty) IsNot Nothing Then
          Me.ProductionCostCarRentalList.Update()
        End If
        If GetProperty(ProductionCostAccomodationListProperty) IsNot Nothing Then
          Me.ProductionCostAccomodationList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delProductionCost"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace