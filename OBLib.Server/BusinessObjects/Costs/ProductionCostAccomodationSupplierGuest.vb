﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostAccomodationSupplierGuest
    Inherits OBBusinessBase(Of ProductionCostAccomodationSupplierGuest)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationSupplierHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationSupplierHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property AccommodationSupplierHumanResourceID() As Integer?
      Get
        Return GetProperty(AccommodationSupplierHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccommodationSupplierHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationID, "Accommodation", Nothing)
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared SupplierHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierHumanResourceID, "Supplier Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Supplier Human Resource value
    ''' </summary>
    <Display(Name:="Supplier Human Resource", Description:="")>
  Public ReadOnly Property SupplierHumanResourceID() As Integer?
      Get
        Return GetProperty(SupplierHumanResourceIDProperty)
      End Get
    End Property

    Public Shared SupplierHumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupplierHumanResource, "Supplier Human Resource")
    ''' <summary>
    ''' Gets the Supplier Human Resource value
    ''' </summary>
    <Display(Name:="Supplier Human Resource", Description:=""),
    StringLength(50, ErrorMessage:="Supplier Human Resource cannot be more than 50 characters")>
  Public ReadOnly Property SupplierHumanResource() As String
      Get
        Return GetProperty(SupplierHumanResourceProperty)
      End Get
    End Property

    Public Shared RoomCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RoomCost, "Room Cost")
    ''' <summary>
    ''' Gets and sets the Room Cost value
    ''' </summary>
    <Display(Name:="Room Cost", Description:=""),
    Required(ErrorMessage:="Room Cost required")>
  Public Property RoomCost() As Decimal
      Get
        Return GetProperty(RoomCostProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RoomCostProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCostAccomodation

      Return CType(CType(Me.Parent, ProductionCostAccomodationSupplierGuestList).Parent, ProductionCostAccomodation)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationSupplierHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SupplierHumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Accomodation Supplier Guest")
        Else
          Return String.Format("Blank {0}", "Production Cost Accomodation Supplier Guest")
        End If
      Else
        Return Me.SupplierHumanResource
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostAccomodationSupplierGuest() method.

    End Sub

    Public Shared Function NewProductionCostAccomodationSupplierGuest() As ProductionCostAccomodationSupplierGuest

      Return DataPortal.CreateChild(Of ProductionCostAccomodationSupplierGuest)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostAccomodationSupplierGuest(dr As SafeDataReader) As ProductionCostAccomodationSupplierGuest

      Dim p As New ProductionCostAccomodationSupplierGuest()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationSupplierHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SupplierHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SupplierHumanResourceProperty, .GetString(3))
          LoadProperty(RoomCostProperty, .GetDecimal(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostAccomodationSupplierGuest"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostAccomodationSupplierGuest"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccommodationSupplierHumanResourceID As SqlParameter = .Parameters.Add("@AccommodationSupplierHumanResourceID", SqlDbType.Int)
          paramAccommodationSupplierHumanResourceID.Value = GetProperty(AccommodationSupplierHumanResourceIDProperty)
          If Me.IsNew Then
            paramAccommodationSupplierHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccommodationSupplierHumanResourceID", GetProperty(AccommodationSupplierHumanResourceIDProperty))
          '.Parameters.AddWithValue("@SupplierHumanResourceID", GetProperty(SupplierHumanResourceIDProperty))
          '.Parameters.AddWithValue("@SupplierHumanResource", GetProperty(SupplierHumanResourceProperty))
          .Parameters.AddWithValue("@RoomCost", GetProperty(RoomCostProperty))
          .Parameters.AddWithValue("@ModifiedBy", Singular.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccommodationSupplierHumanResourceIDProperty, paramAccommodationSupplierHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcs.delProductionCostAccomodationSupplierGuest"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@AccommodationSupplierHumanResourceID", GetProperty(AccommodationSupplierHumanResourceIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Public Sub SetCost(cost As Decimal)

      Me.RoomCost = cost

    End Sub

  End Class

End Namespace