﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostFlightPassenger
    Inherits OBBusinessBase(Of ProductionCostFlightPassenger)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FlightHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FlightHumanResourceID() As Integer?
      Get
        Return GetProperty(FlightHumanResourceIDProperty)
      End Get
    End Property

    Public Shared FlightIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlightID, "Flight", Nothing)
    ''' <summary>
    ''' Gets the Flight value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property FlightID() As Integer?
      Get
        Return GetProperty(FlightIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    StringLength(50, ErrorMessage:="Human Resource cannot be more than 50 characters")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared TicketPriceProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.TicketPrice, 0) _
                                                                    .AddSetExpression("ProductionCostFlightPassengerBO.TicketPriceSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Ticket Price value
    ''' </summary>
    <Display(Name:="Ticket Price", Description:=""),
    Required(ErrorMessage:="Ticket Price required"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property TicketPrice() As Decimal
      Get
        Return GetProperty(TicketPriceProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TicketPriceProperty, Value)
      End Set
    End Property

    Public Shared ServiceFeeProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.ServiceFee, 0) _
                                                                    .AddSetExpression("ProductionCostFlightPassengerBO.ServiceFeeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Service Fee value
    ''' </summary>
    <Display(Name:="Service Fee", Description:=""),
    Required(ErrorMessage:="Service Fee required"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property ServiceFee() As Decimal
      Get
        Return GetProperty(ServiceFeeProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ServiceFeeProperty, Value)
      End Set
    End Property

    Public Shared CancellationFeeProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.CancellationFee, 0) _
                                                                        .AddSetExpression("ProductionCostFlightPassengerBO.CancellationFeeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Cancellation Fee value
    ''' </summary>
    <Display(Name:="Cancellation Fee", Description:=""),
    Required(ErrorMessage:="Cancellation Fee required"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property CancellationFee() As Decimal
      Get
        Return GetProperty(CancellationFeeProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(CancellationFeeProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledByUserProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledByUser, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:=""),
    StringLength(50, ErrorMessage:="Cancelled By User cannot be more than 50 characters")>
    Public ReadOnly Property CancelledByUser() As String
      Get
        Return GetProperty(CancelledByUserProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:=""),
    StringLength(50, ErrorMessage:="Cancelled Reason cannot be more than 50 characters")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCostFlight

      Return CType(CType(Me.Parent, ProductionCostFlightPassengerList).Parent, ProductionCostFlight)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Flight Passenger")
        Else
          Return String.Format("Blank {0}", "Production Cost Flight Passenger")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

    Public Function GetTotalFlightPassengerCost() As Decimal
      Return TicketPrice + ServiceFee + CancellationFee
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostFlightPassenger() method.

    End Sub

    Public Shared Function NewProductionCostFlightPassenger() As ProductionCostFlightPassenger

      Return DataPortal.CreateChild(Of ProductionCostFlightPassenger)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostFlightPassenger(dr As SafeDataReader) As ProductionCostFlightPassenger

      Dim p As New ProductionCostFlightPassenger()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FlightHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(FlightIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceProperty, .GetString(2))
          LoadProperty(TicketPriceProperty, .GetDecimal(3))
          LoadProperty(ServiceFeeProperty, .GetDecimal(4))
          LoadProperty(CancellationFeeProperty, .GetDecimal(5))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CancelledByUserProperty, .GetString(7))
          LoadProperty(CancelledReasonProperty, .GetString(8))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostFlightPassenger"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostFlightPassenger"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          'Dim paramFlightHumanResourceID As SqlParameter = .Parameters.Add("@FlightHumanResourceID", SqlDbType.Int)
          'paramFlightHumanResourceID.Value = GetProperty(FlightHumanResourceIDProperty)
          'If Me.IsNew Then
          '  paramFlightHumanResourceID.Direction = ParameterDirection.Output
          'End If
          '.Parameters.AddWithValue("@FlightID", Me.GetParent().FlightID)

          .Parameters.AddWithValue("@FlightHumanResourceID", GetProperty(FlightHumanResourceIDProperty))

          .Parameters.AddWithValue("@TicketPrice", GetProperty(TicketPriceProperty))
          .Parameters.AddWithValue("@ServiceFee", GetProperty(ServiceFeeProperty))
          .Parameters.AddWithValue("@CancellationFee", GetProperty(CancellationFeeProperty))

          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(FlightHumanResourceIDProperty, paramFlightHumanResourceID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delProductionCostFlightPassenger"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FlightHumanResourceID", GetProperty(FlightHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace