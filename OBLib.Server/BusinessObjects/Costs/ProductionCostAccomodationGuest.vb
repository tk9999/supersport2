﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostAccomodationGuest
    Inherits OBBusinessBase(Of ProductionCostAccomodationGuest)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property AccommodationHumanResourceID() As Integer?
      Get
        Return GetProperty(AccommodationHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccommodationHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationID, "Accommodation", Nothing)
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    StringLength(50, ErrorMessage:="Human Resource cannot be more than 50 characters")>
  Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared RoomCostProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.RoomCost, 0) _
                                                                 .AddSetExpression("ProductionCostAccomodationGuestBO.RoomCostSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Room Cost value
    ''' </summary>
    <Display(Name:="Room Cost", Description:=""),
    Required(ErrorMessage:="Room Cost required"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property RoomCost() As Decimal
      Get
        Return GetProperty(RoomCostProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RoomCostProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCostAccomodation

      Return CType(CType(Me.Parent, ProductionCostAccomodationGuestList).Parent, ProductionCostAccomodation)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Accomodation Guest")
        Else
          Return String.Format("Blank {0}", "Production Cost Accomodation Guest")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostAccomodationGuest() method.

    End Sub

    Public Shared Function NewProductionCostAccomodationGuest() As ProductionCostAccomodationGuest

      Return DataPortal.CreateChild(Of ProductionCostAccomodationGuest)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostAccomodationGuest(dr As SafeDataReader) As ProductionCostAccomodationGuest

      Dim p As New ProductionCostAccomodationGuest()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceProperty, .GetString(3))
          LoadProperty(RoomCostProperty, .GetDecimal(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostAccomodationGuest"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostAccommodationGuest"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          'Dim paramAccommodationHumanResourceID As SqlParameter = .Parameters.Add("@AccommodationHumanResourceID", SqlDbType.Int)
          'paramAccommodationHumanResourceID.Value = GetProperty(AccommodationHumanResourceIDProperty)
          'If Me.IsNew Then
          '  paramAccommodationHumanResourceID.Direction = ParameterDirection.Output
          'End If
          .Parameters.AddWithValue("@AccommodationHumanResourceID", GetProperty(AccommodationHumanResourceIDProperty))
          '.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          '.Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
          .Parameters.AddWithValue("@RoomCost", GetProperty(RoomCostProperty))
          .Parameters.AddWithValue("@ModifiedBy", Singular.Settings.CurrentUserID)
          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(AccommodationHumanResourceIDProperty, paramAccommodationHumanResourceID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delProductionCostAccomodationGuest"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccommodationHumanResourceID", GetProperty(AccommodationHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Public Sub SetCost(cost As Decimal)

      Me.RoomCost = cost

    End Sub

  End Class

End Namespace