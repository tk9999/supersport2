﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostCarRental
    Inherits OBBusinessBase(Of ProductionCostCarRental)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property RentalCarID() As Integer?
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared RentalCarRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarRefNo, "Rental Car Ref No")
    ''' <summary>
    ''' Gets the Rental Car Ref No value
    ''' </summary>
    <Display(Name:="Rental Car Ref No", Description:=""),
    StringLength(50, ErrorMessage:="Rental Car Ref No cannot be more than 50 characters")>
  Public ReadOnly Property RentalCarRefNo() As String
      Get
        Return GetProperty(RentalCarRefNoProperty)
      End Get
    End Property

    Public Shared AgentBranchIDFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AgentBranchIDFrom, "Agent Branch ID From")
    ''' <summary>
    ''' Gets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="Agent Branch ID From", Description:="")>
  Public ReadOnly Property AgentBranchIDFrom() As Integer
      Get
        Return GetProperty(AgentBranchIDFromProperty)
      End Get
    End Property

    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription As String
      Get
        Return GetParent().Description
      End Get
    End Property

    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionNo As String
      Get
        Return GetParent().ProductionRefNo
      End Get
    End Property

    Public Shared AgentBranchFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AgentBranchFrom, "Agent Branch From")
    ''' <summary>
    ''' Gets the Agent Branch From value
    ''' </summary>
    <Display(Name:="Branch From", Description:=""),
    StringLength(50, ErrorMessage:="Agent Branch From cannot be more than 50 characters")>
    Public ReadOnly Property AgentBranchFrom() As String
      Get
        Return GetProperty(AgentBranchFromProperty)
      End Get
    End Property

    Public Shared AgentBranchIDToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AgentBranchIDTo, "Agent Branch ID To")
    ''' <summary>
    ''' Gets the Agent Branch ID To value
    ''' </summary>
    <Display(Name:="Agent Branch ID To", Description:="")>
  Public ReadOnly Property AgentBranchIDTo() As Integer
      Get
        Return GetProperty(AgentBranchIDToProperty)
      End Get
    End Property

    <Display(Name:="Gen Ref Number", Description:="")>
    Public ReadOnly Property GenRefNum As Object
      Get
        Return GetParent().SynergyGenRefNum
      End Get
    End Property

    Public Shared AgentBranchToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AgentBranchTo, "Agent Branch To")
    ''' <summary>
    ''' Gets the Agent Branch To value
    ''' </summary>
    <Display(Name:="Branch To", Description:=""),
    StringLength(50, ErrorMessage:="Agent Branch To cannot be more than 50 characters")>
    Public ReadOnly Property AgentBranchTo() As String
      Get
        Return GetProperty(AgentBranchToProperty)
      End Get
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PickupDateTime, "Pickup Date Time")
    ''' <summary>
    ''' Gets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="Pickup Date Time", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property PickupDateTime As DateTime?
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
    End Property

    Public Shared DropoffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DropoffDateTime, "Dropoff Date Time")
    ''' <summary>
    ''' Gets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="Dropoff Date Time", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property DropoffDateTime As DateTime?
      Get
        Return GetProperty(DropoffDateTimeProperty)
      End Get
    End Property

    Public Shared RentalCostProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.RentalCost, 0) _
                                                                   .AddSetExpression("ProductionCostCarRentalBO.RentalCostSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Rental Cost value
    ''' </summary>
    <Display(Name:="Rental Cost", Description:=""),
    Required(ErrorMessage:="Rental Cost required"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property RentalCost() As Decimal
      Get
        Return GetProperty(RentalCostProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RentalCostProperty, Value)
      End Set
    End Property

    Public Shared DriverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Driver, "Driver")
    ''' <summary>
    ''' Gets and sets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:=""),
    StringLength(50, ErrorMessage:="Driver cannot be more than 50 characters")>
    Public Property Driver() As String
      Get
        Return GetProperty(DriverProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DriverProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionCostCarRentalPassengerListProperty As PropertyInfo(Of ProductionCostCarRentalPassengerList) = RegisterProperty(Of ProductionCostCarRentalPassengerList)(Function(c) c.ProductionCostCarRentalPassengerList, "Production Cost Car Rental Passenger List")

    Public ReadOnly Property ProductionCostCarRentalPassengerList() As ProductionCostCarRentalPassengerList
      Get
        If GetProperty(ProductionCostCarRentalPassengerListProperty) Is Nothing Then
          LoadProperty(ProductionCostCarRentalPassengerListProperty, Costs.ProductionCostCarRentalPassengerList.NewProductionCostCarRentalPassengerList())
        End If
        Return GetProperty(ProductionCostCarRentalPassengerListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCost

      Return CType(CType(Me.Parent, ProductionCostCarRentalList).Parent, ProductionCost)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RentalCarRefNo.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Car Rental")
        Else
          Return String.Format("Blank {0}", "Production Cost Car Rental")
        End If
      Else
        Return Me.RentalCarRefNo
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostCarRental() method.

    End Sub

    Public Shared Function NewProductionCostCarRental() As ProductionCostCarRental

      Return DataPortal.CreateChild(Of ProductionCostCarRental)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostCarRental(dr As SafeDataReader) As ProductionCostCarRental

      Dim p As New ProductionCostCarRental()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RentalCarIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RentalCarRefNoProperty, .GetString(2))
          LoadProperty(AgentBranchIDFromProperty, .GetInt32(3))
          LoadProperty(AgentBranchFromProperty, .GetString(4))
          LoadProperty(AgentBranchIDToProperty, .GetInt32(5))
          LoadProperty(AgentBranchToProperty, .GetString(6))
          LoadProperty(PickupDateTimeProperty, .GetValue(7))
          LoadProperty(DropoffDateTimeProperty, .GetValue(8))
          LoadProperty(RentalCostProperty, .GetDecimal(9))
          LoadProperty(DriverProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostCarRental"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostCarRental"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          'Dim paramRentalCarID As SqlParameter = .Parameters.Add("@RentalCarID", SqlDbType.Int)
          'paramRentalCarID.Value = GetProperty(RentalCarIDProperty)
          'If Me.IsNew Then
          '  paramRentalCarID.Direction = ParameterDirection.Output
          'End If
          '.Parameters.AddWithValue("@ProductionID", Me.GetParent().ProductionID)
          '.Parameters.AddWithValue("@RentalCarRefNo", GetProperty(RentalCarRefNoProperty))
          '.Parameters.AddWithValue("@AgentBranchIDFrom", GetProperty(AgentBranchIDFromProperty))
          '.Parameters.AddWithValue("@AgentBranchFrom", GetProperty(AgentBranchFromProperty))
          '.Parameters.AddWithValue("@AgentBranchIDTo", GetProperty(AgentBranchIDToProperty))
          '.Parameters.AddWithValue("@AgentBranchTo", GetProperty(AgentBranchToProperty))
          'cm.Parameters.AddWithValue("@PickupDateTime", (New SmartDate(GetProperty(PickupDateTimeProperty))).DBValue)
          'cm.Parameters.AddWithValue("@DropoffDateTime", (New SmartDate(GetProperty(DropoffDateTimeProperty))).DBValue)

          .Parameters.AddWithValue("@RentalCarID", GetProperty(RentalCarIDProperty))
          .Parameters.AddWithValue("@RentalCost", GetProperty(RentalCostProperty))
          '.Parameters.AddWithValue("@Driver", GetProperty(DriverProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(RentalCarIDProperty, paramRentalCarID.Value)
          'End If
          ' update child objects
          If GetProperty(ProductionCostCarRentalPassengerListProperty) IsNot Nothing Then
            Me.ProductionCostCarRentalPassengerList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionCostCarRentalPassengerListProperty) IsNot Nothing Then
          Me.ProductionCostCarRentalPassengerList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcs.delProductionCostCarRental"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@RentalCarID", GetProperty(RentalCarIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace