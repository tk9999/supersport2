﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostAccomodation
    Inherits OBBusinessBase(Of ProductionCostAccomodation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccommodationIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared AccommodationProviderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationProviderID, "Accommodation Provider", Nothing)
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:="")>
  Public ReadOnly Property AccommodationProviderID() As Integer?
      Get
        Return GetProperty(AccommodationProviderIDProperty)
      End Get
    End Property

    Public Shared AccommodationProviderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationProvider, "Accommodation Provider")
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:=""),
    StringLength(50, ErrorMessage:="Accommodation Provider cannot be more than 50 characters")>
  Public ReadOnly Property AccommodationProvider() As String
      Get
        Return GetProperty(AccommodationProviderProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
  Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription As String
      Get
        Return GetParent().Description
      End Get
    End Property

    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionNo As String
      Get
        Return GetParent().ProductionRefNo
      End Get
    End Property

    <Display(Name:="Gen Ref Number", Description:="")>
    Public ReadOnly Property GenRefNum As Object
      Get
        Return GetParent().SynergyGenRefNum
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:=""),
    StringLength(50, ErrorMessage:="City cannot be more than 50 characters")>
  Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    StringLength(50, ErrorMessage:="Description cannot be more than 50 characters")>
  Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared CheckInDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckInDate, "Check In Date")
    ''' <summary>
    ''' Gets the Check In Date value
    ''' </summary>
    <Display(Name:="Check In Date", Description:="")>
    Public ReadOnly Property CheckInDate As DateTime?
      Get
        Return GetProperty(CheckInDateProperty)
      End Get
    End Property

    Public Shared CheckOutDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckOutDate, "Check Out Date")
    ''' <summary>
    ''' Gets the Check Out Date value
    ''' </summary>
    <Display(Name:="Check Out Date", Description:="")>
  Public ReadOnly Property CheckOutDate As DateTime?
      Get
        Return GetProperty(CheckOutDateProperty)
      End Get
    End Property

    Public mSupplierInd As Boolean = False

    <DisplayNameAttribute("Supplier?")>
    Public ReadOnly Property SupplierInd() As Boolean
      Get
        If ProductionCostAccomodationGuestList.Count > 0 Then
          mSupplierInd = True
          Return mSupplierInd
        Else
          mSupplierInd = False
          Return mSupplierInd
        End If
      End Get
    End Property

    Public Shared TotalCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalCost, "Total Cost")

    <DisplayNameAttribute("Total Cost"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property TotalCost() As Decimal
      Get
        Return GetProperty(TotalCostProperty)
      End Get
      Set(value As Decimal)
        SetProperty(TotalCostProperty, value)
      End Set
    End Property

    Public Shared RoomCostProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.RoomCost, 0) _
                                                                 .AddSetExpression("ProductionCostBO.RoomCostSet(self)", False)

    <Display(Name:="Room Cost"),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property RoomCost As Decimal
      Get
        Return GetProperty(RoomCostProperty)
      End Get
      Set(value As Decimal)
        SetProperty(RoomCostProperty, value)
        'UpdateCosts(value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionCostAccomodationGuestListProperty As PropertyInfo(Of ProductionCostAccomodationGuestList) = RegisterProperty(Of ProductionCostAccomodationGuestList)(Function(c) c.ProductionCostAccomodationGuestList, "Production Cost Accomodation Guest List")

    Public ReadOnly Property ProductionCostAccomodationGuestList() As ProductionCostAccomodationGuestList
      Get
        If GetProperty(ProductionCostAccomodationGuestListProperty) Is Nothing Then
          LoadProperty(ProductionCostAccomodationGuestListProperty, Costs.ProductionCostAccomodationGuestList.NewProductionCostAccomodationGuestList())
        End If
        Return GetProperty(ProductionCostAccomodationGuestListProperty)
      End Get
    End Property

    Public Shared ProductionCostAccomodationSupplierGuestListProperty As PropertyInfo(Of ProductionCostAccomodationSupplierGuestList) = RegisterProperty(Of ProductionCostAccomodationSupplierGuestList)(Function(c) c.ProductionCostAccomodationSupplierGuestList, "Production Cost Accomodation Supplier Guest List")

    Public ReadOnly Property ProductionCostAccomodationSupplierGuestList() As ProductionCostAccomodationSupplierGuestList
      Get
        If GetProperty(ProductionCostAccomodationSupplierGuestListProperty) Is Nothing Then
          LoadProperty(ProductionCostAccomodationSupplierGuestListProperty, Costs.ProductionCostAccomodationSupplierGuestList.NewProductionCostAccomodationSupplierGuestList())
        End If
        Return GetProperty(ProductionCostAccomodationSupplierGuestListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCost

      Return CType(CType(Me.Parent, ProductionCostAccomodationList).Parent, ProductionCost)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationIDProperty)

    End Function

    Public Sub UpdateTotalCost()
      PropertyHasChanged(TotalCostProperty)
    End Sub

    Public Function GetTotalAccommodationCost() As Decimal
      'If mSupplierInd Then
      'Return ProductionCostAccomodationSupplierGuestList.Sum(Function(d) d.RoomCost)
      ' Else
      Return ProductionCostAccomodationGuestList.Sum(Function(d) d.RoomCost)
      ' End If
    End Function

    Public Sub UpdateCosts(value As Decimal)

      Me.ProductionCostAccomodationGuestList.ToList.ForEach(Sub(pahr)
                                                              pahr.SetCost(value)
                                                            End Sub)

      Me.ProductionCostAccomodationSupplierGuestList.ToList.ForEach(Sub(pahr)
                                                                      pahr.SetCost(value)
                                                                    End Sub)


    End Sub

    Public Overrides Function ToString() As String

      If Me.AccommodationProvider.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Accomodation")
        Else
          Return String.Format("Blank {0}", "Production Cost Accomodation")
        End If
      Else
        Return Me.AccommodationProvider
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostAccomodation() method.

    End Sub

    Public Shared Function NewProductionCostAccomodation() As ProductionCostAccomodation

      Return DataPortal.CreateChild(Of ProductionCostAccomodation)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostAccomodation(dr As SafeDataReader) As ProductionCostAccomodation

      Dim p As New ProductionCostAccomodation()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AccommodationProviderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AccommodationProviderProperty, .GetString(3))
          LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CityProperty, .GetString(5))
          LoadProperty(DescriptionProperty, .GetString(6))
          LoadProperty(CheckInDateProperty, .GetValue(7))
          LoadProperty(CheckOutDateProperty, .GetValue(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostAccomodation"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostAccomodation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramAccommodationID As SqlParameter = .Parameters.Add("@AccommodationID", SqlDbType.Int)
      '    paramAccommodationID.Value = GetProperty(AccommodationIDProperty)
      '    If Me.IsNew Then
      '      paramAccommodationID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@ProductionID", Me.GetParent().ProductionID)
      '    .Parameters.AddWithValue("@AccommodationProviderID", GetProperty(AccommodationProviderIDProperty))
      '    .Parameters.AddWithValue("@AccommodationProvider", GetProperty(AccommodationProviderProperty))
      '    .Parameters.AddWithValue("@CityID", GetProperty(CityIDProperty))
      '    .Parameters.AddWithValue("@City", GetProperty(CityProperty))
      '    .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
      '    cm.Parameters.AddWithValue("@CheckInDate", (New SmartDate(GetProperty(CheckInDateProperty))).DBValue)
      '    cm.Parameters.AddWithValue("@CheckOutDate", (New SmartDate(GetProperty(CheckOutDateProperty))).DBValue)

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(AccommodationIDProperty, paramAccommodationID.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(ProductionCostAccomodationGuestListProperty) IsNot Nothing Then
      '      Me.ProductionCostAccomodationGuestList.Update()
      '    End If
      '    If GetProperty(ProductionCostAccomodationSupplierGuestListProperty) IsNot Nothing Then
      '      Me.ProductionCostAccomodationSupplierGuestList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      ' update child objects
      If GetProperty(ProductionCostAccomodationGuestListProperty) IsNot Nothing Then
        Me.ProductionCostAccomodationGuestList.Update()
      End If
      If GetProperty(ProductionCostAccomodationSupplierGuestListProperty) IsNot Nothing Then
        Me.ProductionCostAccomodationSupplierGuestList.Update()
      End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcs.delProductionCostAccomodation"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@AccommodationID", GetProperty(AccommodationIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace