﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostAccomodationList
    Inherits OBBusinessListBase(Of ProductionCostAccomodationList, ProductionCostAccomodation)

#Region " Business Methods "

    Public Function GetItem(AccommodationID As Integer) As ProductionCostAccomodation

      For Each child As ProductionCostAccomodation In Me
        If child.AccommodationID = AccommodationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetProductionCostAccomodationGuest(AccommodationHumanResourceID As Integer) As ProductionCostAccomodationGuest

      Dim obj As ProductionCostAccomodationGuest = Nothing
      For Each parent As ProductionCostAccomodation In Me
        obj = parent.ProductionCostAccomodationGuestList.GetItem(AccommodationHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionCostAccomodationSupplierGuest(AccommodationSupplierHumanResourceID As Integer) As ProductionCostAccomodationSupplierGuest

      Dim obj As ProductionCostAccomodationSupplierGuest = Nothing
      For Each parent As ProductionCostAccomodation In Me
        obj = parent.ProductionCostAccomodationSupplierGuestList.GetItem(AccommodationSupplierHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionCostAccomodationList() As ProductionCostAccomodationList

      Return New ProductionCostAccomodationList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionCostAccomodation In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionCostAccomodation In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace