﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostCarRentalPassenger
    Inherits OBBusinessBase(Of ProductionCostCarRentalPassenger)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property RentalCarHumanResourceID() As Integer?
      Get
        Return GetProperty(RentalCarHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarID, "Rental Car", Nothing)
    ''' <summary>
    ''' Gets the Rental Car value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property RentalCarID() As Integer?
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    StringLength(50, ErrorMessage:="Human Resource cannot be more than 50 characters")>
  Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
  Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverInd, "Driver", False)
    ''' <summary>
    ''' Gets and sets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:=""),
    Required(ErrorMessage:="Driver required")>
  Public Property DriverInd() As Boolean
      Get
        Return GetProperty(DriverIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DriverIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCostCarRental

      Return CType(CType(Me.Parent, ProductionCostCarRentalPassengerList).Parent, ProductionCostCarRental)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Car Rental Passenger")
        Else
          Return String.Format("Blank {0}", "Production Cost Car Rental Passenger")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostCarRentalPassenger() method.

    End Sub

    Public Shared Function NewProductionCostCarRentalPassenger() As ProductionCostCarRentalPassenger

      Return DataPortal.CreateChild(Of ProductionCostCarRentalPassenger)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostCarRentalPassenger(dr As SafeDataReader) As ProductionCostCarRentalPassenger

      Dim p As New ProductionCostCarRentalPassenger()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RentalCarHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceProperty, .GetString(2))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(DriverIndProperty, .GetBoolean(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostCarRentalPassenger"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostCarRentalPassenger"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramRentalCarHumanResourceID As SqlParameter = .Parameters.Add("@RentalCarHumanResourceID", SqlDbType.Int)
      '    paramRentalCarHumanResourceID.Value = GetProperty(RentalCarHumanResourceIDProperty)
      '    If Me.IsNew Then
      '      paramRentalCarHumanResourceID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@RentalCarID", Me.GetParent().RentalCarID)
      '    .Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
      '    .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      '    .Parameters.AddWithValue("@DriverInd", GetProperty(DriverIndProperty))

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(RentalCarHumanResourceIDProperty, paramRentalCarHumanResourceID.Value)
      '    End If
      '    ' update child objects
      '    ' mChildList.Update()
      '    MarkOld()
      '  End With
      'Else
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcs.delProductionCostCarRentalPassenger"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@RentalCarHumanResourceID", GetProperty(RentalCarHumanResourceIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace