﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostList
    Inherits OBBusinessListBase(Of ProductionCostList, ProductionCost)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ProductionCost

      For Each child As ProductionCost In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetProductionCostFlight(FlightID As Integer) As ProductionCostFlight

      Dim obj As ProductionCostFlight = Nothing
      For Each parent As ProductionCost In Me
        obj = parent.ProductionCostFlightList.GetItem(FlightID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionCostCarRental(RentalCarID As Integer) As ProductionCostCarRental

      Dim obj As ProductionCostCarRental = Nothing
      For Each parent As ProductionCost In Me
        obj = parent.ProductionCostCarRentalList.GetItem(RentalCarID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionCostAccomodation(AccommodationID As Integer) As ProductionCostAccomodation

      Dim obj As ProductionCostAccomodation = Nothing
      For Each parent As ProductionCost In Me
        obj = parent.ProductionCostAccomodationList.GetItem(AccommodationID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Object

      Public Sub New()


      End Sub

      Public Sub New(ProductionID As Object)
        Me.ProductionID = ProductionID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionCostList() As ProductionCostList

      Return New ProductionCostList()

    End Function

    Public Shared Sub BeginGetProductionCostList(CallBack As EventHandler(Of DataPortalResult(Of ProductionCostList)))

      Dim dp As New DataPortal(Of ProductionCostList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionCostList() As ProductionCostList

      Return DataPortal.Fetch(Of ProductionCostList)(New Criteria())

    End Function

    Public Shared Function GetProductionCostList(ByVal VehicleID As Object) As ProductionCostList

      Return DataPortal.Fetch(Of ProductionCostList)(New Criteria(VehicleID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionCost.GetProductionCost(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionCost = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionCostFlightList.RaiseListChangedEvents = False
          parent.ProductionCostFlightList.Add(ProductionCostFlight.GetProductionCostFlight(sdr))
          parent.ProductionCostFlightList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ProductionCostFlight = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FlightID <> sdr.GetInt32(1) Then
            parentChild = Me.GetProductionCostFlight(sdr.GetInt32(1))
          End If
          parentChild.ProductionCostFlightPassengerList.RaiseListChangedEvents = False
          parentChild.ProductionCostFlightPassengerList.Add(ProductionCostFlightPassenger.GetProductionCostFlightPassenger(sdr))
          parentChild.ProductionCostFlightPassengerList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionCostCarRentalList.RaiseListChangedEvents = False
          parent.ProductionCostCarRentalList.Add(ProductionCostCarRental.GetProductionCostCarRental(sdr))
          parent.ProductionCostCarRentalList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentProductionCostCarRentalChild As ProductionCostCarRental = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentProductionCostCarRentalChild Is Nothing OrElse parentProductionCostCarRentalChild.RentalCarID <> sdr.GetInt32(1) Then
            parentProductionCostCarRentalChild = Me.GetProductionCostCarRental(sdr.GetInt32(1))
          End If
          parentProductionCostCarRentalChild.ProductionCostCarRentalPassengerList.RaiseListChangedEvents = False
          parentProductionCostCarRentalChild.ProductionCostCarRentalPassengerList.Add(ProductionCostCarRentalPassenger.GetProductionCostCarRentalPassenger(sdr))
          parentProductionCostCarRentalChild.ProductionCostCarRentalPassengerList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionCostAccomodationList.RaiseListChangedEvents = False
          parent.ProductionCostAccomodationList.Add(ProductionCostAccomodation.GetProductionCostAccomodation(sdr))
          parent.ProductionCostAccomodationList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentProductionCostAccomodationChild As ProductionCostAccomodation = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentProductionCostAccomodationChild Is Nothing OrElse parentProductionCostAccomodationChild.AccommodationID <> sdr.GetInt32(1) Then
            parentProductionCostAccomodationChild = Me.GetProductionCostAccomodation(sdr.GetInt32(1))
          End If
          parentProductionCostAccomodationChild.ProductionCostAccomodationGuestList.RaiseListChangedEvents = False
          parentProductionCostAccomodationChild.ProductionCostAccomodationGuestList.Add(ProductionCostAccomodationGuest.GetProductionCostAccomodationGuest(sdr))
          parentProductionCostAccomodationChild.ProductionCostAccomodationGuestList.RaiseListChangedEvents = True
        End While
      End If
      If sdr.NextResult() Then
        While sdr.Read
          If parentProductionCostAccomodationChild Is Nothing OrElse parentProductionCostAccomodationChild.AccommodationID <> sdr.GetInt32(1) Then
            parentProductionCostAccomodationChild = Me.GetProductionCostAccomodation(sdr.GetInt32(1))
          End If
          parentProductionCostAccomodationChild.ProductionCostAccomodationSupplierGuestList.RaiseListChangedEvents = False
          parentProductionCostAccomodationChild.ProductionCostAccomodationSupplierGuestList.Add(ProductionCostAccomodationSupplierGuest.GetProductionCostAccomodationSupplierGuest(sdr))
          parentProductionCostAccomodationChild.ProductionCostAccomodationSupplierGuestList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As ProductionCost In Me
        child.UpdateAllCosts()
        child.CheckRules()
        For Each ProductionCostFlight As ProductionCostFlight In child.ProductionCostFlightList
          ProductionCostFlight.CheckRules()

          For Each ProductionCostFlightPassenger As ProductionCostFlightPassenger In ProductionCostFlight.ProductionCostFlightPassengerList
            ProductionCostFlightPassenger.CheckRules()
          Next
        Next
        For Each ProductionCostCarRental As ProductionCostCarRental In child.ProductionCostCarRentalList
          ProductionCostCarRental.CheckRules()

          For Each ProductionCostCarRentalPassenger As ProductionCostCarRentalPassenger In ProductionCostCarRental.ProductionCostCarRentalPassengerList
            ProductionCostCarRentalPassenger.CheckRules()
          Next
        Next
        For Each ProductionCostAccomodation As ProductionCostAccomodation In child.ProductionCostAccomodationList
          ProductionCostAccomodation.CheckRules()

          For Each ProductionCostAccomodationGuest As ProductionCostAccomodationGuest In ProductionCostAccomodation.ProductionCostAccomodationGuestList
            ProductionCostAccomodationGuest.CheckRules()
          Next
          For Each ProductionCostAccomodationSupplierGuest As ProductionCostAccomodationSupplierGuest In ProductionCostAccomodation.ProductionCostAccomodationSupplierGuestList
            ProductionCostAccomodationSupplierGuest.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getProductionCostList"
            cm.Parameters.AddWithValue("@ProductionIDs", Singular.Data.XML.StringToXML(crit.ProductionID.ToString))

            If OBLib.Security.Settings.CurrentUser IsNot Nothing Then
              cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
            End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionCost In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionCost In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace