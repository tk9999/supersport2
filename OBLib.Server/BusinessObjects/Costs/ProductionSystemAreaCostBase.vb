﻿' Generated 08 Nov 2016 05:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionSystemAreaCostBase
    Inherits OBBusinessBase(Of ProductionSystemAreaCostBase)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionSystemAreaCostBaseBO.ProductionSystemAreaCostBaseBOToString(self)")

    Public Shared ProductionSystemAreaCostIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaCostID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionSystemAreaCostID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaCostIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
  Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CostTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostTypeID, "Cost Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Type value
    ''' </summary>
    <Display(Name:="Cost Type", Description:=""),
    Required(ErrorMessage:="Cost Type required")>
  Public Property CostTypeID() As Integer?
      Get
        Return GetProperty(CostTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", Nothing)
    ''' <summary>
    ''' Gets and sets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:=""),
    Required(ErrorMessage:="Currency required")>
  Public Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CurrencyIDProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", CDec(0))
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Amount required")>
  Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaCostIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production System Area Cost Base")
        Else
          Return String.Format("Blank {0}", "Production System Area Cost Base")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSystemAreaCostBase() method.

    End Sub

    Public Shared Function NewProductionSystemAreaCostBase() As ProductionSystemAreaCostBase

      Return DataPortal.CreateChild(Of ProductionSystemAreaCostBase)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionSystemAreaCostBase(dr As SafeDataReader) As ProductionSystemAreaCostBase

      Dim p As New ProductionSystemAreaCostBase()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSystemAreaCostIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CostTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(AmountProperty, .GetDecimal(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionSystemAreaCostIDProperty)

      cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
      cm.Parameters.AddWithValue("@CostTypeID", GetProperty(CostTypeIDProperty))
      cm.Parameters.AddWithValue("@CurrencyID", GetProperty(CurrencyIDProperty))
      cm.Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionSystemAreaCostIDProperty, cm.Parameters("@ProductionSystemAreaCostID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionSystemAreaCostID", GetProperty(ProductionSystemAreaCostIDProperty))
    End Sub

#End Region

  End Class

End Namespace