﻿' Generated 04 Mar 2015 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Costs

  <Serializable()> _
  Public Class ProductionCostFlight
    Inherits OBBusinessBase(Of ProductionCostFlight)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared FlightIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property FlightID() As Integer?
      Get
        Return GetProperty(FlightIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FlightIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared AirportIDFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AirportIDFrom, "Airport ID From")
    ''' <summary>
    ''' Gets the Airport ID From value
    ''' </summary>
    <Display(Name:="Airport ID From", Description:="")>
  Public ReadOnly Property AirportIDFrom() As Integer
      Get
        Return GetProperty(AirportIDFromProperty)
      End Get
    End Property

    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetParent.ProductionRefNo
      End Get
    End Property

    Public Shared AirportFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AirportFrom, "Airport From")
    ''' <summary>
    ''' Gets the Airport From value
    ''' </summary>
    <Display(Name:="Airport From", Description:=""),
    StringLength(50, ErrorMessage:="Airport From cannot be more than 50 characters")>
  Public ReadOnly Property AirportFrom() As String
      Get
        Return GetProperty(AirportFromProperty)
      End Get
    End Property

    Public Shared AirportIDToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AirportIDTo, "Airport ID To")
    ''' <summary>
    ''' Gets the Airport ID To value
    ''' </summary>
    <Display(Name:="Airport ID To", Description:="")>
  Public ReadOnly Property AirportIDTo() As Integer
      Get
        Return GetProperty(AirportIDToProperty)
      End Get
    End Property

    Public Shared AirportToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AirportTo, "Airport To")
    ''' <summary>
    ''' Gets the Airport To value
    ''' </summary>
    <Display(Name:="Airport To", Description:=""),
    StringLength(50, ErrorMessage:="Airport To cannot be more than 50 characters")>
  Public ReadOnly Property AirportTo() As String
      Get
        Return GetProperty(AirportToProperty)
      End Get
    End Property

    Public Shared DepartureDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DepartureDateTime, "Departure Date Time")
    ''' <summary>
    ''' Gets the Departure Date Time value
    ''' </summary>
    <Display(Name:="Departure Date Time", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property DepartureDateTime As String
      Get
        Return CType(GetProperty(DepartureDateTimeProperty), DateTime).ToString("dd MMM yyyy hh:mm tt")
      End Get
    End Property

    Public Shared ArrivalDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ArrivalDateTime, "Arrival Date Time")
    ''' <summary>
    ''' Gets the Arrival Date Time value
    ''' </summary>
    <Display(Name:="Arrival Date Time", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property ArrivalDateTime As String
      Get
        Return CType(GetProperty(ArrivalDateTimeProperty), DateTime).ToString("dd MMM yyyy hh:mm tt")
      End Get
    End Property

    Public Shared FlightNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightNo, "Flight No")
    ''' <summary>
    ''' Gets the Flight No value
    ''' </summary>
    <Display(Name:="Flight No", Description:=""),
    StringLength(50, ErrorMessage:="Flight No cannot be more than 50 characters")>
    Public ReadOnly Property FlightNo() As String
      Get
        Return GetProperty(FlightNoProperty)
      End Get
    End Property

    Public Shared TravelDirectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelDirectionID, "Travel Direction", Nothing)
    ''' <summary>
    ''' Gets the Travel Direction value
    ''' </summary>
    <Display(Name:="Travel Direction", Description:="")>
    Public ReadOnly Property TravelDirectionID() As Integer?
      Get
        Return GetProperty(TravelDirectionIDProperty)
      End Get
    End Property

    Public Shared TravelDirectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelDirection, "Travel Direction")
    ''' <summary>
    ''' Gets the Travel Direction value
    ''' </summary>
    <Display(Name:="Travel Direction", Description:=""),
    StringLength(50, ErrorMessage:="Travel Direction cannot be more than 50 characters")>
    Public ReadOnly Property TravelDirection() As String
      Get
        Return GetProperty(TravelDirectionProperty)
      End Get
    End Property

    Public Shared FlightTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlightTypeID, "Flight Type", Nothing)
    ''' <summary>
    ''' Gets the Flight Type value
    ''' </summary>
    <Display(Name:="Flight Type", Description:="")>
    Public ReadOnly Property FlightTypeID() As Integer?
      Get
        Return GetProperty(FlightTypeIDProperty)
      End Get
    End Property

    Public Shared FlightTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightType, "Flight Type")
    ''' <summary>
    ''' Gets the Flight Type value
    ''' </summary>
    <Display(Name:="Flight Type", Description:=""),
    StringLength(50, ErrorMessage:="Flight Type cannot be more than 50 characters")>
    Public ReadOnly Property FlightType() As String
      Get
        Return GetProperty(FlightTypeProperty)
      End Get
    End Property

    Public Shared TotalTicketCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalTicketCost, "Total Ticket Cost")
    <Display(Name:="Total Ticket Cost", Description:=""),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property TotalTicketCost() As Decimal
      Get
        Return GetProperty(TotalTicketCostProperty)
        'Dim total As Decimal = ProductionCostFlightPassengerList.Sum(Function(d) d.TicketPrice)
        'Return total
      End Get
      Set(value As Decimal)
        'PropertyHasChanged("TotalTicketCost")
        SetProperty(TotalTicketCostProperty, value)
      End Set
    End Property

    Public Shared SingleTicketPriceProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.SingleTicketPrice, 0) _
                                                                          .AddSetExpression("ProductionCostBO.FlightSingleTicketPriceSet(self)", False)
    <Display(Name:="Single Ticket Price", Description:=""),
    Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property SingleTicketPrice() As Decimal
      Get
        Return GetProperty(SingleTicketPriceProperty)
        'Dim total As Decimal = ProductionCostFlightPassengerList.Sum(Function(d) d.TicketPrice)
        'Return total
      End Get
      Set(value As Decimal)
        'PropertyHasChanged("TotalTicketCost")
        SetProperty(SingleTicketPriceProperty, value)
      End Set
    End Property

    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription As String
      Get
        Return GetParent().Description
      End Get
    End Property

    <Display(Name:="Gen Ref Number", Description:="")>
    Public ReadOnly Property GenRefNum As Object
      Get
        Return GetParent().SynergyGenRefNum
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionCostFlightPassengerListProperty As PropertyInfo(Of ProductionCostFlightPassengerList) = RegisterProperty(Of ProductionCostFlightPassengerList)(Function(c) c.ProductionCostFlightPassengerList, "Production Cost Flight Passenger List")

    Public ReadOnly Property ProductionCostFlightPassengerList() As ProductionCostFlightPassengerList
      Get
        If GetProperty(ProductionCostFlightPassengerListProperty) Is Nothing Then
          LoadProperty(ProductionCostFlightPassengerListProperty, Costs.ProductionCostFlightPassengerList.NewProductionCostFlightPassengerList())
        End If
        Return GetProperty(ProductionCostFlightPassengerListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCost

      Return CType(CType(Me.Parent, ProductionCostFlightList).Parent, ProductionCost)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AirportFrom.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Cost Flight")
        Else
          Return String.Format("Blank {0}", "Production Cost Flight")
        End If
      Else
        Return Me.AirportFrom
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Function GetTotalTicketCost() As Decimal
      Return ProductionCostFlightPassengerList.Sum(Function(d) d.GetTotalFlightPassengerCost)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCostFlight() method.

    End Sub

    Public Shared Function NewProductionCostFlight() As ProductionCostFlight

      Return DataPortal.CreateChild(Of ProductionCostFlight)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCostFlight(dr As SafeDataReader) As ProductionCostFlight

      Dim p As New ProductionCostFlight()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FlightIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AirportIDFromProperty, .GetInt32(2))
          LoadProperty(AirportFromProperty, .GetString(3))
          LoadProperty(AirportIDToProperty, .GetInt32(4))
          LoadProperty(AirportToProperty, .GetString(5))
          LoadProperty(DepartureDateTimeProperty, .GetValue(6))
          LoadProperty(ArrivalDateTimeProperty, .GetValue(7))
          LoadProperty(FlightNoProperty, .GetString(8))
          LoadProperty(TravelDirectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(TravelDirectionProperty, .GetString(10))
          LoadProperty(FlightTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(FlightTypeProperty, .GetString(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcs.insProductionCostFlight"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionCostFlight"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramFlightID As SqlParameter = .Parameters.Add("@FlightID", SqlDbType.Int)
      '    paramFlightID.Value = GetProperty(FlightIDProperty)
      '    If Me.IsNew Then
      '      paramFlightID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@ProductionID", Me.GetParent().ProductionID)
      '    .Parameters.AddWithValue("@AirportIDFrom", GetProperty(AirportIDFromProperty))
      '    .Parameters.AddWithValue("@AirportFrom", GetProperty(AirportFromProperty))
      '    .Parameters.AddWithValue("@AirportIDTo", GetProperty(AirportIDToProperty))
      '    .Parameters.AddWithValue("@AirportTo", GetProperty(AirportToProperty))
      '    cm.Parameters.AddWithValue("@DepartureDateTime", (New SmartDate(GetProperty(DepartureDateTimeProperty))).DBValue)
      '    cm.Parameters.AddWithValue("@ArrivalDateTime", (New SmartDate(GetProperty(ArrivalDateTimeProperty))).DBValue)
      '    .Parameters.AddWithValue("@FlightNo", GetProperty(FlightNoProperty))
      '    .Parameters.AddWithValue("@TravelDirectionID", GetProperty(TravelDirectionIDProperty))
      '    .Parameters.AddWithValue("@TravelDirection", GetProperty(TravelDirectionProperty))
      '    .Parameters.AddWithValue("@FlightTypeID", GetProperty(FlightTypeIDProperty))
      '    .Parameters.AddWithValue("@FlightType", GetProperty(FlightTypeProperty))

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(FlightIDProperty, paramFlightID.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(ProductionCostFlightPassengerListProperty) IsNot Nothing Then
      '      Me.ProductionCostFlightPassengerList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      ' update child objects
      If GetProperty(ProductionCostFlightPassengerListProperty) IsNot Nothing Then
        Me.ProductionCostFlightPassengerList.Update()
      End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcs.delProductionCostFlight"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@FlightID", GetProperty(FlightIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace