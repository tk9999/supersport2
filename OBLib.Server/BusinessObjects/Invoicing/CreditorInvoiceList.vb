﻿' Generated 01 Apr 2015 10:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.New

  <Serializable()> _
  Public Class CreditorInvoiceList
    Inherits OBBusinessListBase(Of CreditorInvoiceList, CreditorInvoice)

#Region " Business Methods "

    Public Function GetItem(CreditorInvoiceID As Integer) As CreditorInvoice

      For Each child As CreditorInvoice In Me
        If child.CreditorInvoiceID = CreditorInvoiceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Creditor Invoices"

    End Function

    Public Function GetCreditorInvoiceDetail(CreditorInvoiceDetailID As Integer) As CreditorInvoiceDetail

      Dim obj As CreditorInvoiceDetail = Nothing
      For Each parent As CreditorInvoice In Me
        obj = parent.CreditorInvoiceDetailList.GetItem(CreditorInvoiceDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria

      Public Property CreditorInvoiceID As Integer? = Nothing
      Public Property CreditorInvoiceIDsXML As String

      Public Sub New()


      End Sub

      Public Sub New(CreditorInvoiceIDsXML As String)

        Me.CreditorInvoiceIDsXML = CreditorInvoiceIDsXML

      End Sub

      Public Sub New(CreditorInvoiceID As Integer?)

        Me.CreditorInvoiceID = CreditorInvoiceID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewCreditorInvoiceList() As CreditorInvoiceList

      Return New CreditorInvoiceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetCreditorInvoiceList(CreditorInvoiceID As Integer?) As CreditorInvoiceList

      Return DataPortal.Fetch(Of CreditorInvoiceList)(New Criteria(CreditorInvoiceID))

    End Function

    Public Shared Function GetCreditorInvoiceListXML(CreditorInvoiceIDsXML As String) As CreditorInvoiceList

      Return DataPortal.Fetch(Of CreditorInvoiceList)(New Criteria(CreditorInvoiceIDsXML))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(CreditorInvoice.GetCreditorInvoice(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As CreditorInvoice = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.CreditorInvoiceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.CreditorInvoiceDetailList.RaiseListChangedEvents = False
          parent.CreditorInvoiceDetailList.Add(CreditorInvoiceDetail.GetCreditorInvoiceDetail(sdr))
          parent.CreditorInvoiceDetailList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As CreditorInvoice In Me
        child.SetTotals()
        child.CheckRules()
        For Each CreditorInvoiceDetail In child.CreditorInvoiceDetailList
          CreditorInvoiceDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getCreditorInvoiceList"
            cm.Parameters.AddWithValue("@CreditorInvoiceID", NothingDBNull(crit.CreditorInvoiceID))
            cm.Parameters.AddWithValue("@CreditorInvoiceIDsXML", Strings.MakeEmptyDBNull(crit.CreditorInvoiceIDsXML))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CreditorInvoice In DeletedList
          Child.DeleteChildren()
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CreditorInvoice In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
