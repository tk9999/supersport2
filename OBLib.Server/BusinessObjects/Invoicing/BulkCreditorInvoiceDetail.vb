﻿' Generated 11 May 2015 13:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.New

  <Serializable()> _
  Public Class BulkCreditorInvoiceDetail
    Inherits SingularBusinessBase(Of BulkCreditorInvoiceDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorInvoiceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceDetailID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property CreditorInvoiceDetailID() As Integer
      Get
        Return GetProperty(CreditorInvoiceDetailIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CreditorInvoiceDetailIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared SystemInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemInvoiceNum, "System Invoice Num")
    ''' <summary>
    ''' Gets and sets the System Invoice Num value
    ''' </summary>
    <Display(Name:="System Invoice Num", Description:="")>
    Public Property SystemInvoiceNum() As String
      Get
        Return GetProperty(SystemInvoiceNumProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemInvoiceNumProperty, Value)
      End Set
    End Property

    Public Shared CreditorInvoiceDetailDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreditorInvoiceDetailDate, "Detail Date")
    ''' <summary>
    ''' Gets and sets the Creditor Invoice Detail Date value
    ''' </summary>
    <Display(Name:="Detail Date", Description:="")>
    Public Property CreditorInvoiceDetailDate As DateTime?
      Get
        Return GetProperty(CreditorInvoiceDetailDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CreditorInvoiceDetailDateProperty, Value)
      End Set
    End Property

    Public Shared DetailDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DetailDescription, "Detail Description")
    ''' <summary>
    ''' Gets and sets the Detail Description value
    ''' </summary>
    <Display(Name:="Detail Description", Description:="")>
    Public Property DetailDescription() As String
      Get
        Return GetProperty(DetailDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailDescriptionProperty, Value)
      End Set
    End Property

    Public Shared SystemCalculatedRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SystemCalculatedRate, "System Calculated Rate")
    ''' <summary>
    ''' Gets and sets the System Calculated Rate value
    ''' </summary>
    <Display(Name:="System Calculated Rate", Description:=""),
    Required(ErrorMessage:="System Calculated Rate required")>
    Public Property SystemCalculatedRate() As Decimal
      Get
        Return GetProperty(SystemCalculatedRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(SystemCalculatedRateProperty, Value)
      End Set
    End Property

    Public Shared RateOverrideProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.RateOverride, "Rate Override")
    ''' <summary>
    ''' Gets and sets the Rate Override value
    ''' </summary>
    <Display(Name:="Rate Override", Description:="")>
    Public Property RateOverride() As Decimal?
      Get
        Return GetProperty(RateOverrideProperty)
      End Get
      Set(ByVal Value As Decimal?)
        If Not CompareSafe(CreditorInvoiceDetailTypeID, CInt(OBLib.CommonData.Enums.CreditorInvoiceDetailType.SnT)) Then
          SetProperty(RateOverrideProperty, Value)
          PopulateRateOverrideProperties(Value)
        End If
      End Set
    End Property

    Public Shared RateOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RateOverrideReason, "Rate Override Reason")
    ''' <summary>
    ''' Gets and sets the Rate Override Reason value
    ''' </summary>
    <Display(Name:="Rate Override Reason", Description:="")>
    Public Property RateOverrideReason() As String
      Get
        Return GetProperty(RateOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RateOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared RateOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RateOverrideBy, "Rate Override By")
    ''' <summary>
    ''' Gets and sets the Rate Override By value
    ''' </summary>
    <Display(Name:="Rate Override By", Description:="")>
    Public ReadOnly Property RateOverrideBy() As Integer?
      Get
        Return GetProperty(RateOverrideByProperty)
      End Get
    End Property

    Public Shared RateOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RateOverrideDateTime, "Rate Override Date Time")
    ''' <summary>
    ''' Gets and sets the Rate Override Date Time value
    ''' </summary>
    <Display(Name:="Rate Override Date Time", Description:="")>
    Public ReadOnly Property RateOverrideDateTime As DateTime?
      Get
        Return GetProperty(RateOverrideDateTimeProperty)
      End Get
    End Property

    Public Shared AmountExclVATProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AmountExclVAT, "Amount Excl VAT")
    ''' <summary>
    ''' Gets and sets the Amount Excl VAT value
    ''' </summary>
    <Display(Name:="Amount Excl VAT", Description:=""),
    Required(ErrorMessage:="Amount Excl VAT required")>
    Public Property AmountExclVAT() As Decimal
      Get
        Return GetProperty(AmountExclVATProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountExclVATProperty, Value)
      End Set
    End Property

    Public Shared VATAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.VATAmount, "VAT Amount")
    ''' <summary>
    ''' Gets and sets the VAT Amount value
    ''' </summary>
    <Display(Name:="VAT Amount", Description:=""),
    Required(ErrorMessage:="VAT Amount required")>
    Public Property VATAmount() As Decimal
      Get
        Return GetProperty(VATAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(VATAmountProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount")
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Amount required")>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.AmountOverride, "Amount Override")
    ''' <summary>
    ''' Gets and sets the Amount Override value
    ''' </summary>
    <Display(Name:="Amount Override", Description:="")>
    Public Property AmountOverride() As Decimal?
      Get
        Return GetProperty(AmountOverrideProperty)
      End Get
      Set(ByVal Value As Decimal?)
        If Not CompareSafe(CreditorInvoiceDetailTypeID, CInt(OBLib.CommonData.Enums.CreditorInvoiceDetailType.SnT)) Then
          SetProperty(AmountOverrideProperty, Value)
          PopulateAmountOverrideProperties(Value)
        End If
      End Set
    End Property

    Public Shared AmountOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AmountOverrideReason, "Amount Override Reason")
    ''' <summary>
    ''' Gets and sets the Amount Override Reason value
    ''' </summary>
    <Display(Name:="Amount Override Reason", Description:="")>
    Public Property AmountOverrideReason() As String
      Get
        Return GetProperty(AmountOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AmountOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AmountOverrideBy, "Amount Override By")
    ''' <summary>
    ''' Gets and sets the Amount Override By value
    ''' </summary>
    <Display(Name:="Amount Override By", Description:="")>
    Public ReadOnly Property AmountOverrideBy() As Integer?
      Get
        Return GetProperty(AmountOverrideByProperty)
      End Get
    End Property

    Public Shared AmountOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AmountOverrideDateTime, "Amount Override Date Time")
    ''' <summary>
    ''' Gets and sets the Amount Override Date Time value
    ''' </summary>
    <Display(Name:="Amount Override Date Time", Description:="")>
    Public ReadOnly Property AmountOverrideDateTime As DateTime?
      Get
        Return GetProperty(AmountOverrideDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Rate", Description:="Rate")>
    Public ReadOnly Property DisplayRate() As Decimal
      Get
        If Singular.Misc.IsNullNothing(GetProperty(RateOverrideProperty)) Then
          Return GetProperty(SystemCalculatedRateProperty)
        Else
          Return GetProperty(RateOverrideProperty)
        End If
      End Get
    End Property

    <Display(Name:="Amount", Description:="Amount")>
    Public ReadOnly Property DisplayAmount() As Decimal
      Get
        If Singular.Misc.IsNullNothing(GetProperty(AmountOverrideProperty)) Then
          Return GetProperty(AmountProperty)
        Else
          Return GetProperty(AmountOverrideProperty)
        End If
      End Get
    End Property

    Public Shared SelectedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedInd, "Selected", False)
    ''' <summary>
    ''' Gets and sets the Selected Ind value
    ''' </summary>
    Public Property SelectedInd() As Boolean
      Get
        Return GetProperty(SelectedIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(SelectedIndProperty, value)
      End Set
    End Property

    Public Shared CreditorInvoiceDetailTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreditorInvoiceDetailTypeID, "Creditor Invoice Detail Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Creditor Invoice Detail Type value
    ''' </summary>
    <Display(Name:="Detail Type", Description:="Link to the Invoice Detail Types")>
    Public ReadOnly Property CreditorInvoiceDetailTypeID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceDetailTypeIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Bulk Creditor Invoice Detail")
        Else
          Return String.Format("Blank {0}", "Bulk Creditor Invoice Detail")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

    Private Sub PopulateRateOverrideProperties(value As Decimal?)

      If value IsNot Nothing Then
        SetProperty(RateOverrideByProperty, OBLib.Security.Settings.CurrentUserID)
        SetProperty(RateOverrideDateTimeProperty, Date.Now)
      Else
        SetProperty(RateOverrideByProperty, Nothing)
        SetProperty(RateOverrideDateTimeProperty, Nothing)
      End If

    End Sub

    Private Sub PopulateAmountOverrideProperties(value As Decimal?)

      If value IsNot Nothing Then
        SetProperty(AmountOverrideByProperty, OBLib.Security.Settings.CurrentUserID)
        SetProperty(AmountOverrideDateTimeProperty, Date.Now)
      Else
        SetProperty(AmountOverrideByProperty, Nothing)
        SetProperty(AmountOverrideDateTimeProperty, Nothing)
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewBulkCreditorInvoiceDetail() method.

    End Sub

    Public Shared Function NewBulkCreditorInvoiceDetail() As BulkCreditorInvoiceDetail

      Return DataPortal.CreateChild(Of BulkCreditorInvoiceDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetBulkCreditorInvoiceDetail(dr As SafeDataReader) As BulkCreditorInvoiceDetail

      Dim b As New BulkCreditorInvoiceDetail()
      b.Fetch(dr)
      Return b

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CreditorInvoiceDetailIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceProperty, .GetString(1))
          LoadProperty(SystemInvoiceNumProperty, .GetString(2))
          LoadProperty(CreditorInvoiceDetailDateProperty, .GetValue(3))
          LoadProperty(DetailDescriptionProperty, .GetString(4))
          LoadProperty(SystemCalculatedRateProperty, .GetDecimal(5))
          LoadProperty(RateOverrideProperty, .GetValue(6))
          LoadProperty(RateOverrideReasonProperty, .GetString(7))
          LoadProperty(RateOverrideByProperty, .GetValue(8))
          LoadProperty(RateOverrideDateTimeProperty, .GetValue(9))
          LoadProperty(AmountExclVATProperty, .GetDecimal(10))
          LoadProperty(VATAmountProperty, .GetDecimal(11))
          LoadProperty(AmountProperty, .GetDecimal(12))
          LoadProperty(AmountOverrideProperty, .GetValue(13))
          LoadProperty(AmountOverrideReasonProperty, .GetString(14))
          LoadProperty(AmountOverrideByProperty, .GetValue(15))
          LoadProperty(AmountOverrideDateTimeProperty, .GetValue(16))
          LoadProperty(CreditorInvoiceDetailTypeIDProperty, .GetInt32(17))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insBulkCreditorInvoiceDetail"

      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updBulkCreditorInvoiceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCreditorInvoiceDetailID As SqlParameter = .Parameters.Add("@CreditorInvoiceDetailID", SqlDbType.Int)
          paramCreditorInvoiceDetailID.Value = GetProperty(CreditorInvoiceDetailIDProperty)
          If Me.IsNew Then
            paramCreditorInvoiceDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@RateOverride", NothingDBNull(GetProperty(RateOverrideProperty)))
          .Parameters.AddWithValue("@RateOverrideReason", GetProperty(RateOverrideReasonProperty))
          .Parameters.AddWithValue("@RateOverrideBy", NothingDBNull(GetProperty(RateOverrideByProperty)))
          .Parameters.AddWithValue("@RateOverrideDateTime", (New SmartDate(GetProperty(RateOverrideDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AmountOverride", NothingDBNull(GetProperty(AmountOverrideProperty)))
          .Parameters.AddWithValue("@AmountOverrideReason", GetProperty(AmountOverrideReasonProperty))
          .Parameters.AddWithValue("@AmountOverrideBy", NothingDBNull(GetProperty(AmountOverrideByProperty)))
          .Parameters.AddWithValue("@AmountOverrideDateTime", (New SmartDate(GetProperty(AmountOverrideDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CreditorInvoiceDetailIDProperty, paramCreditorInvoiceDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delBulkCreditorInvoiceDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CreditorInvoiceDetailID", GetProperty(CreditorInvoiceDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
