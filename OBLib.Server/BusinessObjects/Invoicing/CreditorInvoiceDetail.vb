﻿' Generated 01 Apr 2015 10:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Invoicing.New

  <Serializable()> _
  Public Class CreditorInvoiceDetail
    Inherits OBBusinessBase(Of CreditorInvoiceDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return CreditorInvoiceDetailBO.CreditorInvoiceDetailToString(self)")

    Public Shared CreditorInvoiceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CreditorInvoiceDetailID() As Integer
      Get
        Return GetProperty(CreditorInvoiceDetailIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreditorInvoiceID, "Creditor Invoice", Nothing)
    ''' <summary>
    ''' Gets the Creditor Invoice value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreditorInvoiceID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CreditorInvoiceDetailTypeID, Nothing)
    'OBLib.CommonData.Enums.CreditorInvoiceDetailTypes.FreelanceWork
    ''' <summary>
    ''' Gets and sets the Creditor Invoice Detail Type value
    ''' </summary>
    <Display(Name:="Type"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCreditorInvoiceDetailTypeList)),
    Required(ErrorMessage:="Detail Type is required")>
    Public Property CreditorInvoiceDetailTypeID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceDetailTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CreditorInvoiceDetailTypeIDProperty, Value)
      End Set
    End Property
    ', FilterMethodName:="FilterROCreditorInvoiceDetailTypeList"

    Public Shared CreditorInvoiceDetailDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CreditorInvoiceDetailDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Creditor Invoice Detail Date value
    ''' </summary>
    <Display(Name:="Date"),
    Required(ErrorMessage:="Date is required")>
    Public Property CreditorInvoiceDetailDate As DateTime?
      Get
        Return GetProperty(CreditorInvoiceDetailDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CreditorInvoiceDetailDateProperty, Value)
      End Set
    End Property

    Public Shared DetailDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DetailDescription, "Detail Description", "")
    ''' <summary>
    ''' Gets and sets the Detail Description value
    ''' </summary>
    <Display(Name:="Description"),
    StringLength(500, ErrorMessage:="Description cannot be more than 500 characters"),
    Required(ErrorMessage:="Description Required", AllowEmptyStrings:=False)>
    Public Property DetailDescription() As String
      Get
        Return GetProperty(DetailDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailDescriptionProperty, Value)
      End Set
    End Property

    Public Shared VatTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.VatTypeID, OBLib.CommonData.Enums.VatType.Zero) _
                                                                  .AddSetExpression("CreditorInvoiceDetailBO.CalculateAmounts('VatTypeID', self)")
    ''' <summary>
    ''' Gets and sets the Vat Type value
    ''' </summary>
    <Display(Name:="Vat Type", Description:="Link to the VAT Type.  Can be Standard, Exempt (Zero), or Variable"),
    Required(ErrorMessage:="Vat Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROVATTypeList))>
    Public Property VatTypeID() As Integer?
      Get
        Return GetProperty(VatTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VatTypeIDProperty, Value)
      End Set
    End Property

    Public Shared AmountExclVATProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.AmountExclVAT, CDec(0.0)) _
                                                                      .AddSetExpression("CreditorInvoiceDetailBO.CalculateAmounts('AmountExclVAT', self)")
    ''' <summary>
    ''' Gets and sets the Amount Excl VAT value
    ''' </summary>
    <Display(Name:="Amnt Ex VAT", Description:="Amount Excluding VAT"),
    Required(ErrorMessage:="Amount Excl VAT required"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property AmountExclVAT() As Decimal
      Get
        Return GetProperty(AmountExclVATProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountExclVATProperty, Value)
      End Set
    End Property

    Public Shared VATAmountProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.VATAmount, CDec(0.0)) '_
    '.AddSetExpression("CreditorInvoiceDetailBO.CalculateAmounts('VAT', self)")
    ''' <summary>
    ''' Gets and sets the VAT Amount value
    ''' </summary>
    <Display(Name:="VAT", Description:="The amount of VAT applicable for the invoice."),
    Required(ErrorMessage:="VAT Amount required"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property VATAmount() As Decimal
      Get
        Return GetProperty(VATAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(VATAmountProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Amount, CDec(0.0)) '_
    '.AddSetExpression("CreditorInvoiceDetailBO.CalculateAmounts('Amount', self)")

    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="The amount of the invoice detail"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared AccountIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AccountID, Nothing)
    ''' <summary>
    ''' Gets and sets the Account value
    ''' </summary>
    <Display(Name:="Account", Description:="The account the invoice detail belongs to"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROAccountList)),
    Required(ErrorMessage:="Account Required")>
    Public Property AccountID() As Integer?
      Get
        Return GetProperty(AccountIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccountIDProperty, Value)
      End Set
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CostCentreID, Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="The cost centre the invoice detail belongs to"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCostCentreList)),
    Required(ErrorMessage:="Cost Centre Required")>
    Public Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostCentreIDProperty, Value)
      End Set
    End Property

    Public Shared SystemCalculatedRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SystemCalculatedRate, "System Calculated Rate", 0)
    ''' <summary>
    ''' Gets and sets the System Calculated Rate value
    ''' </summary>
    <Display(Name:="System Calculated Rate", Description:="The system calculated rate")>
    Public Property SystemCalculatedRate() As Decimal
      Get
        Return GetProperty(SystemCalculatedRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(SystemCalculatedRateProperty, Value)
      End Set
    End Property

    Public Shared RateOverrideProperty As PropertyInfo(Of Decimal?) = RegisterSProperty(Of Decimal?)(Function(c) c.RateOverride, Nothing)
    ''' <summary>
    ''' Gets and sets the Rate Override value
    ''' </summary>
    <Display(Name:="Rate Override", Description:="The rate with which to override the system calculated rate"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property RateOverride() As Decimal?
      Get
        Return GetProperty(RateOverrideProperty)
      End Get
      Set(ByVal Value As Decimal?)
        SetProperty(RateOverrideProperty, Value)
        PopulateRateOverrideProperties(Value)
      End Set
    End Property

    Public Shared RateOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RateOverrideBy, "Rate Override By", Nothing)
    ''' <summary>
    ''' Gets and sets the Rate Override By value
    ''' </summary>
    <Display(Name:="Rate Override By", Description:="The user who has overriden the system rate")>
    Public ReadOnly Property RateOverrideBy() As Integer?
      Get
        Return GetProperty(RateOverrideByProperty)
      End Get
    End Property

    Public Shared RateOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RateOverrideReason, "Rate Override Reason", "")
    ''' <summary>
    ''' Gets and sets the Rate Override Reason value
    ''' </summary>
    <Display(Name:="Rate Override Reason", Description:="The reason why the user overrode the system rate"),
    StringLength(250, ErrorMessage:="Rate Override Reason cannot be more than 250 characters")>
    Public Property RateOverrideReason() As String
      Get
        Return GetProperty(RateOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RateOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared RateOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RateOverrideDateTime, "Rate Override Date Time")
    ''' <summary>
    ''' Gets and sets the Rate Override Date Time value
    ''' </summary>
    <Display(Name:="Rate Override Date Time", Description:="The date and time at which the rate was overriden")>
    Public ReadOnly Property RateOverrideDateTime As DateTime?
      Get
        Return GetProperty(RateOverrideDateTimeProperty)
      End Get
    End Property

    Public Shared SupplierInvoiceDetailNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupplierInvoiceDetailNum, "Ref Num", "")
    ''' <summary>
    ''' Gets and sets the Supplier Invoice Detail Num value
    ''' </summary>
    <Display(Name:="Ref Num", Description:="Supplier Ref number"),
    StringLength(20, ErrorMessage:="Supplier Ref Num cannot be more than 20 characters")>
    Public Property SupplierInvoiceDetailNum() As String
      Get
        Return GetProperty(SupplierInvoiceDetailNumProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SupplierInvoiceDetailNumProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideProperty As PropertyInfo(Of Decimal?) = RegisterSProperty(Of Decimal?)(Function(c) c.AmountOverride, Nothing)
    ''' <summary>
    ''' Gets and sets the Amount Override value
    ''' </summary>
    <Display(Name:="Amount Override", Description:="The amount with which to override the system calculated amount"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property AmountOverride() As Decimal?
      Get
        Return GetProperty(AmountOverrideProperty)
      End Get
      Set(ByVal Value As Decimal?)
        SetProperty(AmountOverrideProperty, Value)
        PopulateAmountOverrideProperties(Value)
      End Set
    End Property

    Public Shared AmountOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AmountOverrideBy, "Amount Override By", Nothing)
    ''' <summary>
    ''' Gets and sets the Amount Override By value
    ''' </summary>
    <Display(Name:="Amount Override By", Description:="The user who has overriden the system rate")>
    Public ReadOnly Property AmountOverrideBy() As Integer?
      Get
        Return GetProperty(AmountOverrideByProperty)
      End Get
    End Property

    Public Shared AmountOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AmountOverrideReason, "Amount Override Reason", "")
    ''' <summary>
    ''' Gets and sets the Amount Override Reason value
    ''' </summary>
    <Display(Name:="Amount Override Reason", Description:="The reason why the user overrode the system rate"),
    StringLength(250, ErrorMessage:="Amount Override Reason cannot be more than 250 characters")>
    Public Property AmountOverrideReason() As String
      Get
        Return GetProperty(AmountOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AmountOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AmountOverrideDateTime, "Amount Override Date Time")
    ''' <summary>
    ''' Gets and sets the Amount Override Date Time value
    ''' </summary>
    <Display(Name:="Amount Override Date Time", Description:="The date and time at which the rate was overriden")>
    Public ReadOnly Property AmountOverrideDateTime As DateTime?
      Get
        Return GetProperty(AmountOverrideDateTimeProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="Discipline worked")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForDay, "Hours For Day", 0)
    ''' <summary>
    ''' Gets the Hours For Day value
    ''' </summary>
    <Display(Name:="Hours For Day", Description:="Hours For Day worked")>
    Public ReadOnly Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours", 0)
    ''' <summary>
    ''' Gets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime Hours", Description:="Overtime Hours")>
    Public ReadOnly Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
    End Property

    'Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Rate, "Rate", 0)
    ' ''' <summary>
    ' ''' Gets the Rate value
    ' ''' </summary>
    '<Display(Name:="Rate", Description:="Rate")>
    'Public ReadOnly Property Rate() As Decimal
    '  Get
    '    Return GetProperty(RateProperty)
    '  End Get
    'End Property

    Public Shared CreditorInvoiceDetailTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreditorInvoiceDetailType, "Discipline", "")
    ''' <summary>
    ''' Gets the Creditor Invoice Detail Type value
    ''' </summary>
    <Display(Name:="Detail Type", Description:="Detail Type")>
    Public ReadOnly Property CreditorInvoiceDetailType() As String
      Get
        Return GetProperty(CreditorInvoiceDetailTypeProperty)
      End Get
    End Property

    Public Shared CostCentreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CostCentre, "Cost Centre", "")
    ''' <summary>
    ''' Gets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="Cost Centre")>
    Public ReadOnly Property CostCentre() As String
      Get
        Return GetProperty(CostCentreProperty)
      End Get
    End Property

    Public Shared AccountProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Account, "Account", "")
    ''' <summary>
    ''' Gets the Account value
    ''' </summary>
    <Display(Name:="Account", Description:="Account")>
    Public ReadOnly Property Account() As String
      Get
        Return GetProperty(AccountProperty)
      End Get
    End Property

    Public Shared VatTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatType, "Vat Type", "")
    ''' <summary>
    ''' Gets the Vat Type value
    ''' </summary>
    <Display(Name:="Vat Type", Description:="Vat Type")>
    Public ReadOnly Property VatType() As String
      Get
        Return GetProperty(VatTypeProperty)
      End Get
    End Property

    <Display(Name:="Rate", Description:="Rate"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property DisplayRate() As Decimal
      Get
        If Singular.Misc.IsNullNothing(GetProperty(RateOverrideProperty)) Then
          Return GetProperty(SystemCalculatedRateProperty)
        Else
          Return GetProperty(RateOverrideProperty)
        End If
      End Get
    End Property

    <Display(Name:="Amount", Description:="Amount"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property DisplayAmount() As Decimal
      Get
        If Singular.Misc.IsNullNothing(GetProperty(AmountOverrideProperty)) Then
          Return GetProperty(AmountProperty)
        Else
          Return GetProperty(AmountOverrideProperty)
        End If
      End Get
    End Property

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetCategoryID, "Timesheet Category", Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:="Timesheet Category")>
    Public Property TimesheetCategoryID() As Integer?
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(TimesheetCategoryIDProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Area"),
    DropDownWeb(GetType(ROProductionAreaList)),
    Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    ''' <summary>
    ''' Gets the Vat Type value
    ''' </summary>
    <Display(Name:="Area")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As CreditorInvoice

      Return CType(CType(Me.Parent, CreditorInvoiceDetailList).Parent, CreditorInvoice)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.GetParent.HumanResource & " - " & Me.DetailDescription & " (" & Me.CreditorInvoiceDetailDate.Value.ToString("dd-MMM-yy") & ")"

      'If Me.DetailDescription.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Creditor Invoice Detail")
      '  Else
      '    Return String.Format("Blank {0}", "Creditor Invoice Detail")
      '  End If
      'Else
      '  Return Me.DetailDescription
      'End If

    End Function

    Private Sub PopulateRateOverrideProperties(value As Decimal?)

      If value IsNot Nothing Then
        SetProperty(RateOverrideByProperty, OBLib.Security.Settings.CurrentUserID)
        SetProperty(RateOverrideDateTimeProperty, Date.Now)
      Else
        SetProperty(RateOverrideByProperty, Nothing)
        SetProperty(RateOverrideDateTimeProperty, Nothing)
      End If

    End Sub

    Private Sub PopulateAmountOverrideProperties(value As Decimal?)

      If value IsNot Nothing Then
        SetProperty(AmountOverrideByProperty, OBLib.Security.Settings.CurrentUserID)
        SetProperty(AmountOverrideDateTimeProperty, Date.Now)
      Else
        SetProperty(AmountOverrideByProperty, Nothing)
        SetProperty(AmountOverrideDateTimeProperty, Nothing)
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RateOverrideReasonProperty)
        .ASyncBusyText = "Checking Rate Override Reason"
        .JavascriptRuleCode = "if(ViewModel.CurrentSystemID() == 1 && self.RateOverride() && self.RateOverrideReason().trim() == '') { CtlError.AddError('Rate Override Reason Required'); }"
        .ServerRuleFunction = Function(d)
                                If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
                                  If Not IsNullNothing(d.RateOverride) AndAlso d.RateOverrideReason.Trim = "" Then
                                    Return "Rate Override Reason Required"
                                  End If
                                End If
                                Return ""
                              End Function
        .AddTriggerProperties(RateOverrideProperty)
      End With

      With AddWebRule(AmountOverrideReasonProperty)
        .ASyncBusyText = "Checking Amount Override Reason"
        .JavascriptRuleCode = "if(ViewModel.CurrentSystemID() == 1 && self.AmountOverride() && self.AmountOverrideReason().trim() == '') { CtlError.AddError('Amount Override Reason Required'); }"
        .ServerRuleFunction = Function(d)
                                If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then
                                  If Not IsNullNothing(d.AmountOverride) AndAlso d.AmountOverrideReason.Trim = "" Then
                                    Return "Amount Override Reason Required"
                                  End If
                                End If
                                Return ""
                              End Function
        .AddTriggerProperties(AmountOverrideProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCreditorInvoiceDetail() method.

    End Sub

    Public Shared Function NewCreditorInvoiceDetail() As CreditorInvoiceDetail

      Return DataPortal.CreateChild(Of CreditorInvoiceDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCreditorInvoiceDetail(dr As SafeDataReader) As CreditorInvoiceDetail

      Dim c As New CreditorInvoiceDetail()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CreditorInvoiceDetailIDProperty, .GetInt32(0))
          LoadProperty(CreditorInvoiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CreditorInvoiceDetailTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreditorInvoiceDetailDateProperty, .GetValue(3))
          LoadProperty(DetailDescriptionProperty, .GetString(4))
          LoadProperty(VatTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(AmountExclVATProperty, .GetDecimal(6))
          LoadProperty(VATAmountProperty, .GetDecimal(7))
          LoadProperty(AmountProperty, .GetDecimal(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(AccountIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(HoursForDayProperty, .GetDecimal(15))
          LoadProperty(OvertimeHoursProperty, .GetDecimal(16))
          LoadProperty(DisciplineProperty, .GetString(17))
          LoadProperty(SystemCalculatedRateProperty, .GetDecimal(18))
          LoadProperty(RateOverrideProperty, .GetValue(19))
          LoadProperty(RateOverrideByProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(RateOverrideDateTimeProperty, .GetValue(21))
          LoadProperty(RateOverrideReasonProperty, .GetString(22))
          LoadProperty(AmountOverrideProperty, .GetValue(23))
          LoadProperty(AmountOverrideByProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(AmountOverrideDateTimeProperty, .GetValue(25))
          LoadProperty(AmountOverrideReasonProperty, .GetString(26))
          LoadProperty(TimesheetCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(CostCentreProperty, .GetString(28))
          LoadProperty(AccountProperty, .GetString(29))
          LoadProperty(VatTypeProperty, .GetString(30))
          LoadProperty(SupplierInvoiceDetailNumProperty, .GetString(31))
          LoadProperty(CreditorInvoiceDetailTypeProperty, .GetString(32))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(33)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCreditorInvoiceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCreditorInvoiceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCreditorInvoiceDetailID As SqlParameter = .Parameters.Add("@CreditorInvoiceDetailID", SqlDbType.Int)
          paramCreditorInvoiceDetailID.Value = GetProperty(CreditorInvoiceDetailIDProperty)
          If Me.IsNew Then
            paramCreditorInvoiceDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CreditorInvoiceID", Me.GetParent().CreditorInvoiceID)
          .Parameters.AddWithValue("@CreditorInvoiceDetailTypeID", Singular.Misc.NothingDBNull(GetProperty(CreditorInvoiceDetailTypeIDProperty)))
          .Parameters.AddWithValue("@CreditorInvoiceDetailDate", (New SmartDate(GetProperty(CreditorInvoiceDetailDateProperty))).DBValue)
          .Parameters.AddWithValue("@DetailDescription", GetProperty(DetailDescriptionProperty))
          .Parameters.AddWithValue("@VatTypeID", GetProperty(VatTypeIDProperty))
          .Parameters.AddWithValue("@AmountExclVAT", GetProperty(AmountExclVATProperty))
          .Parameters.AddWithValue("@VATAmount", GetProperty(VATAmountProperty))
          .Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@AccountID", Singular.Misc.NothingDBNull(GetProperty(AccountIDProperty)))
          .Parameters.AddWithValue("@CostCentreID", Singular.Misc.NothingDBNull(GetProperty(CostCentreIDProperty)))
          .Parameters.AddWithValue("@SystemCalculatedRate", GetProperty(SystemCalculatedRateProperty))
          .Parameters.AddWithValue("@RateOverride", NothingDBNull(GetProperty(RateOverrideProperty)))
          .Parameters.AddWithValue("@RateOverrideBy", Singular.Misc.NothingDBNull(GetProperty(RateOverrideByProperty)))
          .Parameters.AddWithValue("@RateOverrideReason", GetProperty(RateOverrideReasonProperty))
          .Parameters.AddWithValue("@RateOverrideDateTime", (New SmartDate(GetProperty(RateOverrideDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SupplierInvoiceDetailNum", GetProperty(SupplierInvoiceDetailNumProperty))
          .Parameters.AddWithValue("@AmountOverride", NothingDBNull(GetProperty(AmountOverrideProperty)))
          .Parameters.AddWithValue("@AmountOverrideBy", Singular.Misc.NothingDBNull(GetProperty(AmountOverrideByProperty)))
          .Parameters.AddWithValue("@AmountOverrideReason", GetProperty(AmountOverrideReasonProperty))
          .Parameters.AddWithValue("@AmountOverrideDateTime", (New SmartDate(GetProperty(AmountOverrideDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CreditorInvoiceDetailIDProperty, paramCreditorInvoiceDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCreditorInvoiceDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CreditorInvoiceDetailID", GetProperty(CreditorInvoiceDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
