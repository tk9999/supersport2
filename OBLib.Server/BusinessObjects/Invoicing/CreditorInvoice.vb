﻿' Generated 01 Apr 2015 10:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports Singular
Imports Singular.Misc
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Invoicing.New

  <Serializable()> _
  Public Class CreditorInvoice
    Inherits OBBusinessBase(Of CreditorInvoice)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return CreditorInvoiceBO.CreditorInvoiceToString(self)")

    Public Shared CreditorInvoiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CreditorInvoiceID() As Integer
      Get
        Return GetProperty(CreditorInvoiceIDProperty)
      End Get
    End Property

    Public Shared CreditorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreditorID, "Creditor", Nothing)
    ''' <summary>
    ''' Gets and sets the Creditor value
    ''' </summary>
    <Display(Name:="Creditor", Description:="Link to the Creditor")>
    Public Property CreditorID() As Integer?
      Get
        Return GetProperty(CreditorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CreditorIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Link to the Supplier")>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared TransactionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TransactionTypeID, OBLib.CommonData.Enums.TransactionType.Invoice)
    ''' <summary>
    ''' Gets and sets the Transaction Type value
    ''' </summary>
    <Display(Name:="Transaction Type", Description:="Link to the Transaction Types"),
    Required(ErrorMessage:="Transaction Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROTransactionTypeList))>
    Public Property TransactionTypeID() As Integer?
      Get
        Return GetProperty(TransactionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TransactionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared InvoiceDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.InvoiceDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Invoice Date value
    ''' </summary>
    <Display(Name:="Invoice Date", Description:="Date that the invoice was issued by the Creditor"),
    Required(ErrorMessage:="Invoice Date required")>
    Public Property InvoiceDate As DateTime?
      Get
        Return GetProperty(InvoiceDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(InvoiceDateProperty, Value)
      End Set
    End Property

    Public Shared RefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RefNo, "Ref No", "")
    ''' <summary>
    ''' Gets and sets the Ref No value
    ''' </summary>
    <Display(Name:="Supplier Ref Num"),
    StringLength(50, ErrorMessage:="Supplier Ref Num cannot be more than 50 characters")>
    Public Property RefNo() As String
      Get
        Return GetProperty(RefNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RefNoProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="Any additional notes about the invoice that the creditor supplied"),
    StringLength(255, ErrorMessage:="Comments cannot be more than 255 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing) _
                                                                         .AddSetExpression("CreditorInvoiceBO.HumanResourceIDSet(self)")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource"), Required(ErrorMessage:="Human Resource Required"),
    DropDownWeb("ClientData.ROHumanResourceList", DropDownColumns:={"PreferredFirstSurname", "EmployeeCode", "IDNo"}, DisplayMember:="PreferredFirstSurname", ValueMember:="HumanResourceID")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared AccountIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccountID, "Account", Nothing)
    ''' <summary>
    ''' Gets and sets the Account value
    ''' </summary>
    <Display(Name:="Account", Description:="The account the invoice belongs to"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROAccountList))>
    Public Property AccountID() As Integer?
      Get
        Return GetProperty(AccountIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccountIDProperty, Value)
      End Set
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostCentreID, "Cost Centre", Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="The cost centre the invoice belongs to"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCostCentreList))>
    Public Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostCentreIDProperty, Value)
      End Set
    End Property

    Public Shared CompletedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CompletedInd, "Completed", True)
    ''' <summary>
    ''' Gets and sets the Completed value
    ''' </summary>
    <Display(Name:="Completed", Description:="True if the invoice is completed"),
    Required(ErrorMessage:="Completed required")>
    Public Property CompletedInd() As Boolean
      Get
        Return GetProperty(CompletedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CompletedIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemInvoiceNum, "Sys Inv Num", "")
    ''' <summary>
    ''' Gets and sets the System Invoice Num value
    ''' </summary>
    <Display(Name:="Sys Inv Num", Description:="System generated invoice number"),
    StringLength(20, ErrorMessage:="System Invoice Num cannot be more than 20 characters")>
    Public Property SystemInvoiceNum() As String
      Get
        Return GetProperty(SystemInvoiceNumProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemInvoiceNumProperty, Value)
      End Set
    End Property

    Public Shared PaymentRunIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunID, "Payment Run", Nothing)
    ''' <summary>
    ''' Gets the Payment Run value
    ''' </summary>
    Public Property PaymentRunID() As Integer?
      Get
        Return GetProperty(PaymentRunIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PaymentRunIDProperty, value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system the invoice belongs to"),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupplierInvoiceNum, "Supplier Invoice Num", "")
    ''' <summary>
    ''' Gets and sets the Supplier Invoice Num value
    ''' </summary>
    <Display(Name:="Supplier Invoice Num", Description:="The supplier invoice number"),
    StringLength(20, ErrorMessage:="Supplier Invoice Num cannot be more than 20 characters")>
    Public Property SupplierInvoiceNum() As String
      Get
        Return GetProperty(SupplierInvoiceNumProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SupplierInvoiceNumProperty, Value)
      End Set
    End Property

    Public Shared PaymentRunIDOverrideProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunIDOverride, "Payment Run ID Override", Nothing)
    ''' <summary>
    ''' Gets and sets the Payment Run ID Override value
    ''' </summary>
    <Display(Name:="Payment Run ID Override", Description:="Overridable payment run")>
    Public Property PaymentRunIDOverride() As Integer?
      Get
        Return GetProperty(PaymentRunIDOverrideProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PaymentRunIDOverrideProperty, Value)
      End Set
    End Property

    Public Shared PaymentRunIDOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunIDOverrideBy, "Payment Run ID Override By", Nothing)
    ''' <summary>
    ''' Gets and sets the Payment Run ID Override By value
    ''' </summary>
    <Display(Name:="Payment Run ID Override By", Description:="User who overrode the payment run")>
    Public Property PaymentRunIDOverrideBy() As Integer?
      Get
        Return GetProperty(PaymentRunIDOverrideByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PaymentRunIDOverrideByProperty, Value)
      End Set
    End Property

    Public Shared PaymentRunIDOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PaymentRunIDOverrideReason, "Payment Run ID Override Reason", "")
    ''' <summary>
    ''' Gets and sets the Payment Run ID Override Reason value
    ''' </summary>
    <Display(Name:="Payment Run ID Override Reason", Description:="Reason for the payment run being overridden"),
    StringLength(255, ErrorMessage:="Payment Run ID Override Reason cannot be more than 255 characters")>
    Public Property PaymentRunIDOverrideReason() As String
      Get
        Return GetProperty(PaymentRunIDOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PaymentRunIDOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared PaymentRunIDOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PaymentRunIDOverrideDateTime, "Payment Run ID Override Date Time")
    ''' <summary>
    ''' Gets and sets the Payment Run ID Override Date Time value
    ''' </summary>
    <Display(Name:="Payment Run ID Override Date Time", Description:="Date and time the payment run was overridden")>
    Public Property PaymentRunIDOverrideDateTime As DateTime?
      Get
        Return GetProperty(PaymentRunIDOverrideDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PaymentRunIDOverrideDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human Resource Invoice")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared HumanResourceIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceIDNo, "Human Resource ID No", "")
    ''' <summary>
    ''' Gets the Human Resource ID No value
    ''' </summary>
    <Display(Name:="ID No.", Description:="Human Resource ID No")>
    Public ReadOnly Property HumanResourceIDNo() As String
      Get
        Return GetProperty(HumanResourceIDNoProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Human Resource Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Staff No.", Description:="Human Resource Employee Code")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    <Display(Name:="Total Tx & Travel", Description:="Total Tx & Travel"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property TotalAmountTxAndTravel() As Decimal
      Get
        Return CreditorInvoiceDetailList.Where(Function(d) CompareSafe(d.CreditorInvoiceDetailTypeID, CInt(CommonData.Enums.CreditorInvoiceDetailTypes.FreelanceWork))).Sum(Function(c) c.DisplayAmount) 'mTotalAmountTxAndTravel
      End Get
    End Property

    <Display(Name:="Total S&T", Description:="Total S&T"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property TotalSnT() As Decimal
      Get
        Return CreditorInvoiceDetailList.Where(Function(d) CompareSafe(d.CreditorInvoiceDetailTypeID, CInt(CommonData.Enums.CreditorInvoiceDetailTypes.SnT))).Sum(Function(c) c.Amount) 'mTotalSnT
      End Get
    End Property

    <Display(Name:="Amount Ex. Vat", Description:="Total Amount Ex. Vat"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property TotalAmountExVat() As Decimal
      Get
        Return CreditorInvoiceDetailList.Sum(Function(c) c.AmountExclVAT) 'mTotalSnT
      End Get
    End Property

    <Display(Name:="Vat", Description:="Total Vat"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property TotalVat() As Decimal
      Get
        Return CreditorInvoiceDetailList.Sum(Function(c) c.VATAmount) 'mTotalSnT
      End Get
    End Property

    <Display(Name:="Total", Description:="Total"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public ReadOnly Property Total() As Decimal
      Get
        Return CreditorInvoiceDetailList.Sum(Function(c) c.VATAmount + c.AmountExclVAT) 'mTotalSnT
      End Get
    End Property

    Public Shared AmountExclVATProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.AmountExclVAT, CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Amount Excl VAT value
    ''' </summary>
    <Display(Name:="Amnt Ex VAT"),
    AlwaysClean,
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property AmountExclVAT() As Decimal
      Get
        Return GetProperty(AmountExclVATProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountExclVATProperty, Value)
      End Set
    End Property

    Public Shared VATAmountProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.VATAmount, CDec(0.0))
    ''' <summary>
    ''' Gets and sets the VAT Amount value
    ''' </summary>
    <Display(Name:="VAT"),
    AlwaysClean,
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property VATAmount() As Decimal
      Get
        Return GetProperty(VATAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(VATAmountProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Amount, CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="The amount of the invoice detail"),
    AlwaysClean,
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared CreditorInvoiceDetailListProperty As PropertyInfo(Of CreditorInvoiceDetailList) = RegisterProperty(Of CreditorInvoiceDetailList)(Function(c) c.CreditorInvoiceDetailList, "Creditor Invoice Detail List")

    Public ReadOnly Property CreditorInvoiceDetailList() As CreditorInvoiceDetailList
      Get
        If GetProperty(CreditorInvoiceDetailListProperty) Is Nothing Then
          LoadProperty(CreditorInvoiceDetailListProperty, Invoicing.New.CreditorInvoiceDetailList.NewCreditorInvoiceDetailList())
        End If
        Return GetProperty(CreditorInvoiceDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As PaymentRun

      Return CType(CType(Me.Parent, CreditorInvoiceList).Parent, PaymentRun)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RefNo.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Creditor Invoice")
        Else
          Return String.Format("Blank {0}", "Creditor Invoice")
        End If
      Else
        Return Me.RefNo
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CreditorInvoiceDetails"}
      End Get
    End Property

    Sub SetTotals()
      LoadProperty(AmountExclVATProperty, Me.CreditorInvoiceDetailList.Sum(Function(d) d.AmountExclVAT))
      LoadProperty(VATAmountProperty, Me.CreditorInvoiceDetailList.Sum(Function(d) d.VATAmount))
      LoadProperty(AmountProperty, Me.CreditorInvoiceDetailList.Sum(Function(d) d.Amount))
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCreditorInvoice() method.

    End Sub

    Public Shared Function NewCreditorInvoice() As CreditorInvoice

      Return DataPortal.CreateChild(Of CreditorInvoice)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCreditorInvoice(dr As SafeDataReader) As CreditorInvoice

      Dim c As New CreditorInvoice()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CreditorInvoiceIDProperty, .GetInt32(0))
          LoadProperty(CreditorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(TransactionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(InvoiceDateProperty, .GetValue(4))
          LoadProperty(RefNoProperty, .GetString(5))
          LoadProperty(CommentsProperty, .GetString(6))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(AccountIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(CompletedIndProperty, .GetBoolean(10))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(SystemInvoiceNumProperty, .GetString(15))
          LoadProperty(PaymentRunIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(SystemIDProperty, .GetInt32(17))
          LoadProperty(SupplierInvoiceNumProperty, .GetString(18))
          LoadProperty(PaymentRunIDOverrideProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(PaymentRunIDOverrideByProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(PaymentRunIDOverrideReasonProperty, .GetString(21))
          LoadProperty(PaymentRunIDOverrideDateTimeProperty, .GetValue(22))
          LoadProperty(HumanResourceProperty, .GetString(23))
          LoadProperty(HumanResourceIDNoProperty, .GetString(24))
          LoadProperty(EmployeeCodeProperty, .GetString(25))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCreditorInvoice"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCreditorInvoice"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCreditorInvoiceID As SqlParameter = .Parameters.Add("@CreditorInvoiceID", SqlDbType.Int)
          paramCreditorInvoiceID.Value = GetProperty(CreditorInvoiceIDProperty)
          If Me.IsNew Then
            paramCreditorInvoiceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CreditorID", Singular.Misc.NothingDBNull(GetProperty(CreditorIDProperty)))
          .Parameters.AddWithValue("@SupplierID", Singular.Misc.NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@TransactionTypeID", GetProperty(TransactionTypeIDProperty))
          .Parameters.AddWithValue("@InvoiceDate", (New SmartDate(GetProperty(InvoiceDateProperty))).DBValue)
          .Parameters.AddWithValue("@RefNo", GetProperty(RefNoProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@AccountID", Singular.Misc.NothingDBNull(GetProperty(AccountIDProperty)))
          .Parameters.AddWithValue("@CostCentreID", Singular.Misc.NothingDBNull(GetProperty(CostCentreIDProperty)))
          .Parameters.AddWithValue("@CompletedInd", GetProperty(CompletedIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SystemInvoiceNum", GetProperty(SystemInvoiceNumProperty))
          .Parameters.AddWithValue("@PaymentRunID", GetProperty(PaymentRunIDProperty)) 'Me.GetParent().PaymentRunID)
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@SupplierInvoiceNum", GetProperty(SupplierInvoiceNumProperty))
          .Parameters.AddWithValue("@PaymentRunIDOverride", Singular.Misc.NothingDBNull(GetProperty(PaymentRunIDOverrideProperty)))
          .Parameters.AddWithValue("@PaymentRunIDOverrideBy", Singular.Misc.NothingDBNull(GetProperty(PaymentRunIDOverrideByProperty)))
          .Parameters.AddWithValue("@PaymentRunIDOverrideReason", GetProperty(PaymentRunIDOverrideReasonProperty))
          .Parameters.AddWithValue("@PaymentRunIDOverrideDateTime", (New SmartDate(GetProperty(PaymentRunIDOverrideDateTimeProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CreditorInvoiceIDProperty, paramCreditorInvoiceID.Value)
          End If
          ' update child objects
          If GetProperty(CreditorInvoiceDetailListProperty) IsNot Nothing Then
            Me.CreditorInvoiceDetailList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(CreditorInvoiceDetailListProperty) IsNot Nothing Then
          Me.CreditorInvoiceDetailList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCreditorInvoice"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CreditorInvoiceID", GetProperty(CreditorInvoiceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub DeleteChildren()
      For Each detail As CreditorInvoiceDetail In Me.CreditorInvoiceDetailList
        detail.DeleteSelf()
      Next
    End Sub

  End Class

End Namespace