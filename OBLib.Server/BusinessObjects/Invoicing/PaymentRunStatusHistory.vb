﻿' Generated 02 Apr 2015 15:48 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.New

  <Serializable()> _
  Public Class PaymentRunStatusHistory
    Inherits OBBusinessBase(Of PaymentRunStatusHistory)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PaymentRunStatusHistoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PaymentRunStatusHistoryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property PaymentRunStatusHistoryID() As Integer
      Get
        Return GetProperty(PaymentRunStatusHistoryIDProperty)
      End Get
    End Property

    Public Shared PaymentRunIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunID, "Payment Run", Nothing)
    ''' <summary>
    ''' Gets and sets the Payment Run value
    ''' </summary>
    <Display(Name:="Payment Run", Description:="Link to the parent"),
    Required(ErrorMessage:="Payment Run required")>
  Public Property PaymentRunID() As Integer?
      Get
        Return GetProperty(PaymentRunIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PaymentRunIDProperty, Value)
      End Set
    End Property

    Public Shared FromStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FromStatusID, "From Status", Nothing)
    ''' <summary>
    ''' Gets and sets the From Status value
    ''' </summary>
    <Display(Name:="From Status", Description:="Status moved from"),
    Required(ErrorMessage:="From Status required")>
  Public Property FromStatusID() As Integer?
      Get
        Return GetProperty(FromStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FromStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ToStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ToStatusID, "To Status", Nothing)
    ''' <summary>
    ''' Gets and sets the To Status value
    ''' </summary>
    <Display(Name:="To Status", Description:="Status moved to"),
    Required(ErrorMessage:="To Status required")>
  Public Property ToStatusID() As Integer?
      Get
        Return GetProperty(ToStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ToStatusIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PaymentRunStatusHistoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Payment Run Status History")
        Else
          Return String.Format("Blank {0}", "Payment Run Status History")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPaymentRunStatusHistory() method.

    End Sub

    Public Shared Function NewPaymentRunStatusHistory() As PaymentRunStatusHistory

      Return DataPortal.CreateChild(Of PaymentRunStatusHistory)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPaymentRunStatusHistory(dr As SafeDataReader) As PaymentRunStatusHistory

      Dim p As New PaymentRunStatusHistory()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PaymentRunStatusHistoryIDProperty, .GetInt32(0))
          LoadProperty(PaymentRunIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(FromStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ToStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPaymentRunStatusHistory"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPaymentRunStatusHistory"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPaymentRunStatusHistoryID As SqlParameter = .Parameters.Add("@PaymentRunStatusHistoryID", SqlDbType.Int)
          paramPaymentRunStatusHistoryID.Value = GetProperty(PaymentRunStatusHistoryIDProperty)
          If Me.IsNew Then
            paramPaymentRunStatusHistoryID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PaymentRunID", GetProperty(PaymentRunIDProperty))
          .Parameters.AddWithValue("@FromStatusID", GetProperty(FromStatusIDProperty))
          .Parameters.AddWithValue("@ToStatusID", GetProperty(ToStatusIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PaymentRunStatusHistoryIDProperty, paramPaymentRunStatusHistoryID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPaymentRunStatusHistory"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PaymentRunStatusHistoryID", GetProperty(PaymentRunStatusHistoryIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace