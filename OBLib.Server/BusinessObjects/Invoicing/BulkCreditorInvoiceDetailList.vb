﻿' Generated 11 May 2015 13:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.New

  <Serializable()> _
  Public Class BulkCreditorInvoiceDetailList
    Inherits SingularBusinessListBase(Of BulkCreditorInvoiceDetailList, BulkCreditorInvoiceDetail)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(CreditorInvoiceDetailID As Integer) As BulkCreditorInvoiceDetail

      For Each child As BulkCreditorInvoiceDetail In Me
        If child.CreditorInvoiceDetailID = CreditorInvoiceDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Property PaymentRunID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(PaymentRunID As Integer?)

        Me.PaymentRunID = PaymentRunID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewBulkCreditorInvoiceDetailList() As BulkCreditorInvoiceDetailList

      Return New BulkCreditorInvoiceDetailList()

    End Function

    Public Shared Sub BeginGetBulkCreditorInvoiceDetailList(CallBack As EventHandler(Of DataPortalResult(Of BulkCreditorInvoiceDetailList)))

      Dim dp As New DataPortal(Of BulkCreditorInvoiceDetailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetBulkCreditorInvoiceDetailList(PaymentRunID As Integer?) As BulkCreditorInvoiceDetailList

      Return DataPortal.Fetch(Of BulkCreditorInvoiceDetailList)(New Criteria(PaymentRunID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      While sdr.Read
        Me.Add(BulkCreditorInvoiceDetail.GetBulkCreditorInvoiceDetail(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getBulkCreditorInvoiceDetailList"
            cm.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(crit.PaymentRunID))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As BulkCreditorInvoiceDetail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As BulkCreditorInvoiceDetail In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace