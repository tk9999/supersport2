﻿' Generated 23 Jan 2016 18:51 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROInvoicePreviewList
    Inherits OBReadOnlyListBase(Of ROInvoicePreviewList, ROInvoicePreview)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROInvoicePreview

      For Each child As ROInvoicePreview In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROInvoicePreviewDetail(HumanResourceID As Integer) As ROInvoicePreviewDetail

      Dim obj As ROInvoicePreviewDetail = Nothing
      For Each parent As ROInvoicePreview In Me
        obj = parent.ROInvoicePreviewDetailList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property PaymentRunID As Integer? = Nothing

      Public Sub New(PaymentRunID As Integer?)
        Me.PaymentRunID = PaymentRunID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROInvoicePreviewList() As ROInvoicePreviewList

      Return New ROInvoicePreviewList()

    End Function

    Public Shared Sub BeginGetROInvoicePreviewList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROInvoicePreviewList)))

      Dim dp As New DataPortal(Of ROInvoicePreviewList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROInvoicePreviewList(CallBack As EventHandler(Of DataPortalResult(Of ROInvoicePreviewList)))

      Dim dp As New DataPortal(Of ROInvoicePreviewList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROInvoicePreviewList() As ROInvoicePreviewList

      Return DataPortal.Fetch(Of ROInvoicePreviewList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROInvoicePreview.GetROInvoicePreview(sdr))
      End While
      Me.IsReadOnly = True

      Me.RaiseListChangedEvents = True

      Dim parent As ROInvoicePreview = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ROInvoicePreviewDetailList.RaiseListChangedEvents = False
          parent.ROInvoicePreviewDetailList.Add(ROInvoicePreviewDetail.GetROInvoicePreviewDetail(sdr))
          parent.ROInvoicePreviewDetailList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROInvoicePreviewList]"
            cm.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(crit.PaymentRunID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace