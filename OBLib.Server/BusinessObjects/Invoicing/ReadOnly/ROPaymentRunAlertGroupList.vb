﻿' Generated 09 Feb 2016 09:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunAlertGroupList
    Inherits OBReadOnlyListBase(Of ROPaymentRunAlertGroupList, ROPaymentRunAlertGroup)

#Region " Business Methods "

    Public Function GetItem(AlertGroupID As Integer) As ROPaymentRunAlertGroup

      For Each child As ROPaymentRunAlertGroup In Me
        If child.AlertGroupID = AlertGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROPaymentRunAlertGroupDetail(AlertGroupDetailID As Integer) As ROPaymentRunAlertGroupDetail

      Dim obj As ROPaymentRunAlertGroupDetail = Nothing
      For Each parent As ROPaymentRunAlertGroup In Me
        obj = parent.ROPaymentRunAlertGroupDetailList.GetItem(AlertGroupDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property PaymentRunID As Integer?

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPaymentRunAlertGroupList() As ROPaymentRunAlertGroupList

      Return New ROPaymentRunAlertGroupList()

    End Function

    Public Shared Sub BeginGetROPaymentRunAlertGroupList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunAlertGroupList)))

      Dim dp As New DataPortal(Of ROPaymentRunAlertGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPaymentRunAlertGroupList(CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunAlertGroupList)))

      Dim dp As New DataPortal(Of ROPaymentRunAlertGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPaymentRunAlertGroupList() As ROPaymentRunAlertGroupList

      Return DataPortal.Fetch(Of ROPaymentRunAlertGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPaymentRunAlertGroup.GetROPaymentRunAlertGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROPaymentRunAlertGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AlertGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROPaymentRunAlertGroupDetailList.RaiseListChangedEvents = False
          parent.ROPaymentRunAlertGroupDetailList.Add(ROPaymentRunAlertGroupDetail.GetROPaymentRunAlertGroupDetail(sdr))
          parent.ROPaymentRunAlertGroupDetailList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPaymentRunAlertGroupList"
            cm.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(crit.PaymentRunID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace