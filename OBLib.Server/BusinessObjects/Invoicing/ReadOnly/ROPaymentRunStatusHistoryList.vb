﻿' Generated 20 Jun 2014 12:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunStatusHistoryList
    Inherits SingularReadOnlyListBase(Of ROPaymentRunStatusHistoryList, ROPaymentRunStatusHistory)

#Region " Business Methods "

    Public Function GetItem(PaymentRunStatusHistoryID As Integer) As ROPaymentRunStatusHistory

      For Each child As ROPaymentRunStatusHistory In Me
        If child.PaymentRunStatusHistoryID = PaymentRunStatusHistoryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Payment Run Status Historys"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPaymentRunStatusHistoryList() As ROPaymentRunStatusHistoryList

      Return New ROPaymentRunStatusHistoryList()

    End Function

    Public Shared Sub BeginGetROPaymentRunStatusHistoryList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunStatusHistoryList)))

      Dim dp As New DataPortal(Of ROPaymentRunStatusHistoryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPaymentRunStatusHistoryList(CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunStatusHistoryList)))

      Dim dp As New DataPortal(Of ROPaymentRunStatusHistoryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPaymentRunStatusHistoryList() As ROPaymentRunStatusHistoryList

      Return DataPortal.Fetch(Of ROPaymentRunStatusHistoryList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPaymentRunStatusHistory.GetROPaymentRunStatusHistory(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPaymentRunStatusHistoryList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace