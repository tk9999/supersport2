﻿' Generated 12 Jun 2014 14:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunSelect
    Inherits OBReadOnlyBase(Of ROPaymentRunSelect)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PaymentRunIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PaymentRunID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property PaymentRunID() As Integer
      Get
        Return GetProperty(PaymentRunIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:="The sub-department to which this payment run applies")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared MonthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Month, "Month", 0)
    ''' <summary>
    ''' Gets the Month value
    ''' </summary>
    <Display(Name:="Month", Description:="The month of the payment run"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.CommonData.Enums.Month))>
    Public ReadOnly Property Month() As Integer
      Get
        Return GetProperty(MonthProperty)
      End Get
    End Property

    Public Shared YearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Year, "Year", 0)
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Year", Description:="The year of the payment run")>
    Public ReadOnly Property Year() As Integer
      Get
        Return GetProperty(YearProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The start date of the payment run")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The end date of the payment run")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared PaymentRunStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunStatusID, "Payment Run Status", Nothing)
    ''' <summary>
    ''' Gets the Payment Run Status value
    ''' </summary>
    <Display(Name:="Payment Run Status", Description:="Current status for the payment run status")>
    Public ReadOnly Property PaymentRunStatusID() As Integer?
      Get
        Return GetProperty(PaymentRunStatusIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Month")>
    Public ReadOnly Property MonthString As String
      Get
        If Month > 0 AndAlso Year > 0 Then
          Return OBMisc.GetMonthDescription(Month)
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Month and Year")>
    Public ReadOnly Property MonthYearString As String
      Get
        Return MonthString & " " & Year.ToString
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System", "")
    <Display(Name:="Sub-Dept")>
    Public ReadOnly Property System As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared StatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Status, "Status", "")
    <Display(Name:="Status")>
    Public ReadOnly Property Status As String
      Get
        Return GetProperty(StatusProperty)
      End Get
    End Property

    Public Shared MonthNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MonthName, "Month", "")
    ''' <summary>
    ''' Gets the Month Name value
    ''' </summary>
    <Display(Name:="Month", Description:="Name of the Month")>
    Public ReadOnly Property MonthName() As String
      Get
        Return GetProperty(MonthNameProperty)
      End Get
    End Property

    Public Shared TotalInvoicesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalInvoices, "Total Invoices", 0)
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Total Invoices")>
    Public ReadOnly Property TotalInvoices() As Integer
      Get
        Return GetProperty(TotalInvoicesProperty)
      End Get
    End Property

    Public Shared TotalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalAmount, "Total Amount", 0)
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Total Amount")>
    Public ReadOnly Property TotalAmount() As Decimal
      Get
        Return GetProperty(TotalAmountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PaymentRunIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Month.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPaymentRunSelect(dr As SafeDataReader) As ROPaymentRunSelect

      Dim r As New ROPaymentRunSelect()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PaymentRunIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(MonthProperty, .GetInt32(2))
        LoadProperty(YearProperty, .GetInt32(3))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(EndDateProperty, .GetValue(5))
        LoadProperty(PaymentRunStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(SystemProperty, .GetString(11))
        LoadProperty(StatusProperty, .GetString(12))
        LoadProperty(MonthNameProperty, .GetString(13))
        LoadProperty(TotalInvoicesProperty, .GetInt32(14))
        LoadProperty(TotalAmountProperty, .GetDecimal(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace