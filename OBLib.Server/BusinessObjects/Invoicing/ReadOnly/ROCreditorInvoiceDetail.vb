﻿' Generated 14 Apr 2014 09:35 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROCreditorInvoiceDetail
    Inherits SingularReadOnlyBase(Of ROCreditorInvoiceDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorInvoiceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CreditorInvoiceDetailID() As Integer
      Get
        Return GetProperty(CreditorInvoiceDetailIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreditorInvoiceID, "Creditor Invoice", Nothing)
    ''' <summary>
    ''' Gets the Creditor Invoice value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreditorInvoiceID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreditorInvoiceDetailTypeID, "Creditor Invoice Detail Type", Nothing)
    ''' <summary>
    ''' Gets the Creditor Invoice Detail Type value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail Type", Description:="Link to the Invoice Detail Types")>
    Public ReadOnly Property CreditorInvoiceDetailTypeID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceDetailTypeIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreditorInvoiceDetailDate, "Creditor Invoice Detail Date")
    ''' <summary>
    ''' Gets the Creditor Invoice Detail Date value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail Date", Description:="")>
    Public ReadOnly Property CreditorInvoiceDetailDate As DateTime?
      Get
        Return GetProperty(CreditorInvoiceDetailDateProperty)
      End Get
    End Property

    Public Shared DetailDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DetailDescription, "Detail Description", "")
    ''' <summary>
    ''' Gets the Detail Description value
    ''' </summary>
    <Display(Name:="Detail Description", Description:="Details about the each line item on the invoice")>
    Public ReadOnly Property DetailDescription() As String
      Get
        Return GetProperty(DetailDescriptionProperty)
      End Get
    End Property

    Public Shared VatTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VatTypeID, "Vat Type", Nothing)
    ''' <summary>
    ''' Gets the Vat Type value
    ''' </summary>
    <Display(Name:="Vat Type", Description:="Link to the VAT Type.  Can be Standard, Exempt (Zero), or Variable")>
    Public ReadOnly Property VatTypeID() As Integer?
      Get
        Return GetProperty(VatTypeIDProperty)
      End Get
    End Property

    Public Shared AmountExclVATProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AmountExclVAT, "Amount Excl VAT", 0)
    ''' <summary>
    ''' Gets the Amount Excl VAT value
    ''' </summary>
    <Display(Name:="Amount Excl VAT", Description:="Amount Excluding VAT")>
    Public ReadOnly Property AmountExclVAT() As Decimal
      Get
        Return GetProperty(AmountExclVATProperty)
      End Get
    End Property

    Public Shared VATAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.VATAmount, "VAT Amount", 0)
    ''' <summary>
    ''' Gets the VAT Amount value
    ''' </summary>
    <Display(Name:="VAT Amount", Description:="The amount of VAT applicable for the invoice.")>
    Public ReadOnly Property VATAmount() As Decimal
      Get
        Return GetProperty(VATAmountProperty)
      End Get
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", 0)
    ''' <summary>
    ''' Gets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="")>
    Public ReadOnly Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemInvoiceNum, "System Invoice Num")
    ''' <summary>
    ''' Gets the System Invoice Num value
    ''' </summary>
    <Display(Name:="System Invoice Num", Description:="")>
    Public ReadOnly Property SystemInvoiceNum() As String
      Get
        Return GetProperty(SystemInvoiceNumProperty)
      End Get
    End Property

    Public Shared SupplierInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupplierInvoiceNum, "Supplier Invoice Num", "")
    ''' <summary>
    ''' Gets and sets the Supplier Invoice Num value
    ''' </summary>
    <Display(Name:="Supplier Invoice Num", Description:="The supplier invoice number")>
    Public ReadOnly Property SupplierInvoiceNum() As String
      Get
        Return GetProperty(SupplierInvoiceNumProperty)
      End Get
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.DetailDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCreditorInvoiceDetail(dr As SafeDataReader) As ROCreditorInvoiceDetail

      Dim r As New ROCreditorInvoiceDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CreditorInvoiceDetailIDProperty, .GetInt32(0))
        LoadProperty(CreditorInvoiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(CreditorInvoiceDetailTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CreditorInvoiceDetailDateProperty, .GetValue(3))
        LoadProperty(DetailDescriptionProperty, .GetString(4))
        LoadProperty(VatTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(AmountExclVATProperty, .GetDecimal(6))
        LoadProperty(VATAmountProperty, .GetDecimal(7))
        LoadProperty(AmountProperty, .GetDecimal(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace