﻿' Generated 20 Jun 2014 12:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunStatusHistory
    Inherits SingularReadOnlyBase(Of ROPaymentRunStatusHistory)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PaymentRunStatusHistoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PaymentRunStatusHistoryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PaymentRunStatusHistoryID() As Integer
      Get
        Return GetProperty(PaymentRunStatusHistoryIDProperty)
      End Get
    End Property

    Public Shared PaymentRunIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunID, "Payment Run", Nothing)
    ''' <summary>
    ''' Gets the Payment Run value
    ''' </summary>
    <Display(Name:="Payment Run", Description:="Link to the parent")>
    Public ReadOnly Property PaymentRunID() As Integer?
      Get
        Return GetProperty(PaymentRunIDProperty)
      End Get
    End Property

    Public Shared FromStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FromStatusID, "From Status", Nothing)
    ''' <summary>
    ''' Gets the From Status value
    ''' </summary>
    <Display(Name:="From Status", Description:="Status moved from")>
    Public ReadOnly Property FromStatusID() As Integer?
      Get
        Return GetProperty(FromStatusIDProperty)
      End Get
    End Property

    Public Shared ToStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ToStatusID, "To Status", Nothing)
    ''' <summary>
    ''' Gets the To Status value
    ''' </summary>
    <Display(Name:="To Status", Description:="Status moved to")>
    Public ReadOnly Property ToStatusID() As Integer?
      Get
        Return GetProperty(ToStatusIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PaymentRunStatusHistoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPaymentRunStatusHistory(dr As SafeDataReader) As ROPaymentRunStatusHistory

      Dim r As New ROPaymentRunStatusHistory()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PaymentRunStatusHistoryIDProperty, .GetInt32(0))
        LoadProperty(PaymentRunIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FromStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ToStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace