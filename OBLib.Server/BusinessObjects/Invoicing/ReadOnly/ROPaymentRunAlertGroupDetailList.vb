﻿' Generated 09 Feb 2016 09:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunAlertGroupDetailList
    Inherits OBReadOnlyListBase(Of ROPaymentRunAlertGroupDetailList, ROPaymentRunAlertGroupDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROPaymentRunAlertGroup
#End Region

#Region " Business Methods "

    Public Function GetItem(AlertGroupDetailID As Integer) As ROPaymentRunAlertGroupDetail

      For Each child As ROPaymentRunAlertGroupDetail In Me
        If child.AlertGroupDetailID = AlertGroupDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROPaymentRunAlertGroupDetailList() As ROPaymentRunAlertGroupDetailList

      Return New ROPaymentRunAlertGroupDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace