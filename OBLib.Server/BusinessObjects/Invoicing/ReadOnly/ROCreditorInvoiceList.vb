﻿' Generated 08 May 2015 10:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROCreditorInvoiceList
    Inherits OBReadOnlyListBase(Of ROCreditorInvoiceList, ROCreditorInvoice)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(CreditorInvoiceID As Integer) As ROCreditorInvoice

      For Each child As ROCreditorInvoice In Me
        If child.CreditorInvoiceID = CreditorInvoiceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FirstName, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="First Name", Description:="")>
      Public Property FirstName() As String
        Get
          Return ReadProperty(FirstNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(FirstNameProperty, Value)
        End Set
      End Property

      Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Surname, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Surname", Description:="")>
      Public Property Surname() As String
        Get
          Return ReadProperty(SurnameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SurnameProperty, Value)
        End Set
      End Property

      Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.PreferredName, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Pref Name", Description:="")>
      Public Property PreferredName() As String
        Get
          Return ReadProperty(PreferredNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PreferredNameProperty, Value)
        End Set
      End Property

      'Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ' ''' <summary>
      ' ''' Gets and sets the Keyword value
      ' ''' </summary>
      '<Display(Name:="Keyword", Description:="")>
      'Public Property Keyword() As String
      '  Get
      '    Return ReadProperty(KeywordProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(KeywordProperty, Value)
      '  End Set
      'End Property

      Public Property PaymentRunID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(PaymentRunID As Integer?)

        Me.PaymentRunID = PaymentRunID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCreditorInvoiceList() As ROCreditorInvoiceList

      Return New ROCreditorInvoiceList()

    End Function

    Public Shared Sub BeginGetROCreditorInvoiceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCreditorInvoiceList)))

      Dim dp As New DataPortal(Of ROCreditorInvoiceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCreditorInvoiceList(CallBack As EventHandler(Of DataPortalResult(Of ROCreditorInvoiceList)))

      Dim dp As New DataPortal(Of ROCreditorInvoiceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCreditorInvoiceList(PaymentRunID As Integer?) As ROCreditorInvoiceList

      Return DataPortal.Fetch(Of ROCreditorInvoiceList)(New Criteria(PaymentRunID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCreditorInvoice.GetROCreditorInvoice(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCreditorInvoiceList"
            cm.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(crit.PaymentRunID))
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.FirstName))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace