﻿' Generated 23 Jan 2016 18:51 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROInvoicePreview
    Inherits OBReadOnlyBase(Of ROInvoicePreview)

#Region " Properties and Methods "

#Region " Properties "

    Public Property IsExpanded As Boolean

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ParticipantNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ParticipantName, "Participant")
    ''' <summary>
    ''' Gets the Participant Name value
    ''' </summary>
    <Display(Name:="Participant", Description:="")>
    Public ReadOnly Property ParticipantName() As String
      Get
        Return GetProperty(ParticipantNameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared TotalHoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHoursForDay, "Hours")
    ''' <summary>
    ''' Gets the Total Hours For Day value
    ''' </summary>
    <Display(Name:="Hours", Description:="")>
    Public ReadOnly Property TotalHoursForDay() As Decimal
      Get
        Return GetProperty(TotalHoursForDayProperty)
      End Get
    End Property

    Public Shared TotalOvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalOvertime, "Additional")
    ''' <summary>
    ''' Gets the Total Overtime value
    ''' </summary>
    <Display(Name:="Additional", Description:="")>
    Public ReadOnly Property TotalOvertime() As Decimal
      Get
        Return GetProperty(TotalOvertimeProperty)
      End Get
    End Property

    Public Shared TotalTaxableAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalTaxableAmount, "Taxable Amount")
    ''' <summary>
    ''' Gets the Total Taxable Amount value
    ''' </summary>
    <Display(Name:="Taxable Amount", Description:="")>
    Public ReadOnly Property TotalTaxableAmount() As Decimal
      Get
        Return GetProperty(TotalTaxableAmountProperty)
      End Get
    End Property

    Public Shared TotalSnTProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalSnT, "S&T")
    ''' <summary>
    ''' Gets the Total Sn T value
    ''' </summary>
    <Display(Name:="S&T", Description:="")>
    Public ReadOnly Property TotalSnT() As Decimal
      Get
        Return GetProperty(TotalSnTProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public ReadOnly Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROInvoicePreviewDetailListProperty As PropertyInfo(Of ROInvoicePreviewDetailList) = RegisterProperty(Of ROInvoicePreviewDetailList)(Function(c) c.ROInvoicePreviewDetailList, "ROOB City HR Invoice Detail List")

    Public ReadOnly Property ROInvoicePreviewDetailList() As ROInvoicePreviewDetailList
      Get
        If GetProperty(ROInvoicePreviewDetailListProperty) Is Nothing Then
          LoadProperty(ROInvoicePreviewDetailListProperty, Invoicing.ReadOnly.ROInvoicePreviewDetailList.NewROInvoicePreviewDetailList())
        End If
        Return GetProperty(ROInvoicePreviewDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ParticipantName

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROInvoicePreview(dr As SafeDataReader) As ROInvoicePreview

      Dim r As New ROInvoicePreview()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ParticipantNameProperty, .GetString(1))
        LoadProperty(EmployeeCodeProperty, .GetString(2))
        LoadProperty(ContractTypeProperty, .GetString(3))
        LoadProperty(TotalHoursForDayProperty, .GetDecimal(4))
        LoadProperty(TotalOvertimeProperty, .GetDecimal(5))
        LoadProperty(TotalTaxableAmountProperty, .GetDecimal(6))
        LoadProperty(TotalSnTProperty, .GetDecimal(7))
        LoadProperty(EmailAddressProperty, .GetString(8))
        LoadProperty(IDNoProperty, .GetString(9))
        LoadProperty(RowNoProperty, .GetInt32(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace