﻿' Generated 09 Feb 2016 09:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunAlertGroup
    Inherits OBReadOnlyBase(Of ROPaymentRunAlertGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AlertGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AlertGroupID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AlertGroupID() As Integer
      Get
        Return GetProperty(AlertGroupIDProperty)
      End Get
    End Property

    Public Shared AlertCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlertCategory, "Alert Category")
    ''' <summary>
    ''' Gets the Alert Category value
    ''' </summary>
    <Display(Name:="Alert Category", Description:="")>
  Public ReadOnly Property AlertCategory() As String
      Get
        Return GetProperty(AlertCategoryProperty)
      End Get
    End Property

    Public Shared AlertIconProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlertIcon, "Alert Icon")
    ''' <summary>
    ''' Gets the Alert Category value
    ''' </summary>
    <Display(Name:="Alert Icon", Description:="")>
    Public ReadOnly Property AlertIcon() As String
      Get
        Return GetProperty(AlertIconProperty)
      End Get
    End Property

    Public Shared AlertTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlertTitle, "Alert Tile")
    ''' <summary>
    ''' Gets the Alert Tile value
    ''' </summary>
    <Display(Name:="Alert Title", Description:="")>
    Public ReadOnly Property AlertTitle() As String
      Get
        Return GetProperty(AlertTitleProperty)
      End Get
    End Property

    Public Shared AlertDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlertDescription, "Alert Description")
    ''' <summary>
    ''' Gets the Alert Description value
    ''' </summary>
    <Display(Name:="Alert Description", Description:="")>
  Public ReadOnly Property AlertDescription() As String
      Get
        Return GetProperty(AlertDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROPaymentRunAlertGroupDetailListProperty As PropertyInfo(Of ROPaymentRunAlertGroupDetailList) = RegisterProperty(Of ROPaymentRunAlertGroupDetailList)(Function(c) c.ROPaymentRunAlertGroupDetailList, "RO Payment Run Alert Group Detail List")

    Public ReadOnly Property ROPaymentRunAlertGroupDetailList() As ROPaymentRunAlertGroupDetailList
      Get
        If GetProperty(ROPaymentRunAlertGroupDetailListProperty) Is Nothing Then
          LoadProperty(ROPaymentRunAlertGroupDetailListProperty, Invoicing.ReadOnly.ROPaymentRunAlertGroupDetailList.NewROPaymentRunAlertGroupDetailList())
        End If
        Return GetProperty(ROPaymentRunAlertGroupDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AlertGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AlertCategory

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPaymentRunAlertGroup(dr As SafeDataReader) As ROPaymentRunAlertGroup

      Dim r As New ROPaymentRunAlertGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AlertGroupIDProperty, .GetInt32(0))
        LoadProperty(AlertCategoryProperty, .GetString(1))
        LoadProperty(AlertIconProperty, .GetString(2))
        LoadProperty(AlertTitleProperty, .GetString(3))
        LoadProperty(AlertDescriptionProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace