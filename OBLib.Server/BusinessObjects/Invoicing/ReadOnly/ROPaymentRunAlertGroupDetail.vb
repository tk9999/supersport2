﻿' Generated 09 Feb 2016 09:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunAlertGroupDetail
    Inherits OBReadOnlyBase(Of ROPaymentRunAlertGroupDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AlertGroupDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AlertGroupDetailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AlertGroupDetailID() As Integer
      Get
        Return GetProperty(AlertGroupDetailIDProperty)
      End Get
    End Property

    Public Shared AlertGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AlertGroupID, "Alert Group")
    ''' <summary>
    ''' Gets the Alert Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property AlertGroupID() As Integer
      Get
        Return GetProperty(AlertGroupIDProperty)
      End Get
    End Property

    Public Shared DetailTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DetailTitle, "Detail Title")
    ''' <summary>
    ''' Gets the Detail Title value
    ''' </summary>
    <Display(Name:="Detail Title", Description:="")>
  Public ReadOnly Property DetailTitle() As String
      Get
        Return GetProperty(DetailTitleProperty)
      End Get
    End Property

    Public Shared DetailDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DetailDescription, "Detail Description")
    ''' <summary>
    ''' Gets the Detail Description value
    ''' </summary>
    <Display(Name:="Detail Description", Description:="")>
  Public ReadOnly Property DetailDescription() As String
      Get
        Return GetProperty(DetailDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AlertGroupDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.DetailTitle

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPaymentRunAlertGroupDetail(dr As SafeDataReader) As ROPaymentRunAlertGroupDetail

      Dim r As New ROPaymentRunAlertGroupDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AlertGroupDetailIDProperty, .GetInt32(0))
        LoadProperty(AlertGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DetailTitleProperty, .GetString(2))
        LoadProperty(DetailDescriptionProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace