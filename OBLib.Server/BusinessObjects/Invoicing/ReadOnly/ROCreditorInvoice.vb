﻿' Generated 08 May 2015 10:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROCreditorInvoice
    Inherits OBReadOnlyBase(Of ROCreditorInvoice)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorInvoiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CreditorInvoiceID() As Integer
      Get
        Return GetProperty(CreditorInvoiceIDProperty)
      End Get
    End Property

    Public Shared CreditorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorID, "Creditor")
    ''' <summary>
    ''' Gets the Creditor value
    ''' </summary>
    <Display(Name:="Creditor", Description:="")>
    Public ReadOnly Property CreditorID() As Integer
      Get
        Return GetProperty(CreditorIDProperty)
      End Get
    End Property

    Public Shared TransactionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TransactionTypeID, "Transaction Type")
    ''' <summary>
    ''' Gets the Transaction Type value
    ''' </summary>
    <Display(Name:="Transaction Type", Description:="")>
    Public ReadOnly Property TransactionTypeID() As Integer
      Get
        Return GetProperty(TransactionTypeIDProperty)
      End Get
    End Property

    Public Shared InvoiceDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.InvoiceDate, "Invoice Date")
    ''' <summary>
    ''' Gets the Invoice Date value
    ''' </summary>
    <Display(Name:="Invoice Date", Description:="")>
    Public ReadOnly Property InvoiceDate As DateTime?
      Get
        Return GetProperty(InvoiceDateProperty)
      End Get
    End Property

    Public Shared RefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RefNo, "Ref No")
    ''' <summary>
    ''' Gets the Ref No value
    ''' </summary>
    <Display(Name:="Ref No", Description:="")>
    Public ReadOnly Property RefNo() As String
      Get
        Return GetProperty(RefNoProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared StaffNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StaffNo, "Staff No")
    ''' <summary>
    ''' Gets the Staff No value
    ''' </summary>
    <Display(Name:="Staff No", Description:="")>
    Public ReadOnly Property StaffNo() As String
      Get
        Return GetProperty(StaffNoProperty)
      End Get
    End Property

    Public Shared CompletedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CompletedInd, "Completed", False)
    ''' <summary>
    ''' Gets the Completed value
    ''' </summary>
    <Display(Name:="Completed", Description:="")>
    Public ReadOnly Property CompletedInd() As Boolean
      Get
        Return GetProperty(CompletedIndProperty)
      End Get
    End Property

    Public Shared SystemInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemInvoiceNum, "System Invoice Num")
    ''' <summary>
    ''' Gets the System Invoice Num value
    ''' </summary>
    <Display(Name:="System Invoice Num", Description:="")>
    Public ReadOnly Property SystemInvoiceNum() As String
      Get
        Return GetProperty(SystemInvoiceNumProperty)
      End Get
    End Property

    Public Shared PaymentRunIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PaymentRunID, "Payment Run")
    ''' <summary>
    ''' Gets the Payment Run value
    ''' </summary>
    <Display(Name:="Payment Run", Description:="")>
    Public ReadOnly Property PaymentRunID() As Integer
      Get
        Return GetProperty(PaymentRunIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared HumanResourceIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceIDNo, "Human Resource ID No.", "")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No.", Description:="Human Resource ID No.")>
    Public ReadOnly Property HumanResourceIDNo() As String
      Get
        Return GetProperty(HumanResourceIDNoProperty)
      End Get
    End Property

    Public Shared TransactionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TransactionType, "Transaction Type", "")
    ''' <summary>
    ''' Gets the Transaction Type value
    ''' </summary>
    <Display(Name:="Transaction Type", Description:="Transaction Type of the Invoice")>
    Public ReadOnly Property TransactionType() As String
      Get
        Return GetProperty(TransactionTypeProperty)
      End Get
    End Property

    Public Shared TotalTxAndTravelProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalTxAndTravel, "Total Tx & Travel", 0)
    ''' <summary>
    ''' Gets the Total Tx And Travel value
    ''' </summary>
    <Display(Name:="Total Tx & Travel", Description:="Total Tx And Travel Amount")>
    Public ReadOnly Property TotalTxAndTravel() As Decimal
      Get
        Return GetProperty(TotalTxAndTravelProperty)
      End Get
    End Property

    Public Shared TotalSnTProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalSnT, "Total S&T", 0)
    ''' <summary>
    ''' Gets the Total SnT value
    ''' </summary>
    <Display(Name:="Total S&T", Description:="Total S&T Amount")>
    Public ReadOnly Property TotalSnT() As Decimal
      Get
        Return GetProperty(TotalSnTProperty)
      End Get
    End Property

    Public Shared SupplierInvoiceNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupplierInvoiceNum, "Supplier Invoice Num", "")
    ''' <summary>
    ''' Gets and sets the Supplier Invoice Num value
    ''' </summary>
    <Display(Name:="Supplier Invoice Num", Description:="The supplier invoice number")>
    Public ReadOnly Property SupplierInvoiceNum() As String
      Get
        Return GetProperty(SupplierInvoiceNumProperty)
      End Get
    End Property

    Public Shared TotalAmountExclVATProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalAmountExclVAT, "Amnt Ex. VAT", 0)
    ''' <summary>
    ''' Gets the Total Amount value
    ''' </summary>
    <Display(Name:="Amnt Ex. VAT", Description:="Total Amnt Ex. VAT")>
    Public ReadOnly Property TotalAmountExclVAT() As Decimal
      Get
        Return GetProperty(TotalAmountExclVATProperty)
      End Get
    End Property

    Public Shared TotalVATAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalVATAmount, "VAT", 0)
    ''' <summary>
    ''' Gets the Total Amount value
    ''' </summary>
    <Display(Name:="VAT", Description:="Total Vat Amount")>
    Public ReadOnly Property TotalVATAmount() As Decimal
      Get
        Return GetProperty(TotalVATAmountProperty)
      End Get
    End Property

    Public Shared TotalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalAmount, "Total S&T", 0)
    ''' <summary>
    ''' Gets the Total Amount value
    ''' </summary>
    <Display(Name:="Total Amount", Description:="Total Amount")>
    Public ReadOnly Property TotalAmount() As Decimal
      Get
        Return GetProperty(TotalAmountProperty)
      End Get
    End Property

    Public Shared RowNumProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNum, "RowNum")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="RowNum", Description:="")>
    Public ReadOnly Property RowNum() As Integer
      Get
        Return GetProperty(RowNumProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RefNo

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCreditorInvoice(dr As SafeDataReader) As ROCreditorInvoice

      Dim r As New ROCreditorInvoice()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CreditorInvoiceIDProperty, .GetInt32(0))
        LoadProperty(CreditorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(TransactionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(InvoiceDateProperty, .GetValue(3))
        LoadProperty(RefNoProperty, .GetString(4))
        LoadProperty(CommentsProperty, .GetString(5))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(10))
        LoadProperty(StaffNoProperty, .GetString(11))
        LoadProperty(CompletedIndProperty, .GetBoolean(12))
        LoadProperty(SystemInvoiceNumProperty, .GetString(13))
        LoadProperty(PaymentRunIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(HumanResourceProperty, .GetString(16))
        LoadProperty(HumanResourceIDNoProperty, .GetString(17))
        LoadProperty(TransactionTypeProperty, .GetString(18))
        LoadProperty(SupplierInvoiceNumProperty, .GetString(19))
        LoadProperty(TotalTxAndTravelProperty, .GetDecimal(20))
        LoadProperty(TotalSnTProperty, .GetDecimal(21))
        LoadProperty(TotalAmountExclVATProperty, .GetDecimal(22))
        LoadProperty(TotalVATAmountProperty, .GetDecimal(23))
        LoadProperty(TotalAmountProperty, .GetDecimal(24))
        LoadProperty(RowNumProperty, .GetInt32(25))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace