﻿' Generated 12 Jun 2014 14:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunSelectList
    Inherits OBReadOnlyListBase(Of ROPaymentRunSelectList, ROPaymentRunSelect)

#Region " Business Methods "

    Public Function GetItem(PaymentRunID As Integer) As ROPaymentRunSelect

      For Each child As ROPaymentRunSelect In Me
        If child.PaymentRunID = PaymentRunID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetLatestPaymentRun(SystemID As Integer) As ROPaymentRunSelect

      Dim LastOpenRun As ROPaymentRunSelect = Me.Where(Function(d) d.PaymentRunStatusID = CommonData.Enums.PaymentRunStatus.Open).OrderBy(Function(d) d.StartDate).FirstOrDefault
      If LastOpenRun Is Nothing Then
        LastOpenRun = Me.OrderBy(Function(d) d.EndDate).LastOrDefault
      End If

      Return LastOpenRun

    End Function

    Public Overrides Function ToString() As String

      Return "Payment Runs"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="Payment Run"), PrimarySearchField>
      Public Property PaymentRun As String
      Public Property SystemID As Integer? = Nothing

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPaymentRunSelectList() As ROPaymentRunSelectList

      Return New ROPaymentRunSelectList()

    End Function

    Public Shared Sub BeginGetROPaymentRunSelectList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunSelectList)))

      Dim dp As New DataPortal(Of ROPaymentRunSelectList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPaymentRunSelectList(CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunSelectList)))

      Dim dp As New DataPortal(Of ROPaymentRunSelectList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPaymentRunSelectList() As ROPaymentRunSelectList

      Return DataPortal.Fetch(Of ROPaymentRunSelectList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPaymentRunSelect.GetROPaymentRunSelect(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPaymentRunListSelect"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@PaymentRun", NothingDBNull(crit.PaymentRun))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace