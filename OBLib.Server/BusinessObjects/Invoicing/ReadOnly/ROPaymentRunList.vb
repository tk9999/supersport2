﻿' Generated 12 Jun 2014 14:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunList
    Inherits OBReadOnlyListBase(Of ROPaymentRunList, ROPaymentRun)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(PaymentRunID As Integer) As ROPaymentRun

      For Each child As ROPaymentRun In Me
        If child.PaymentRunID = PaymentRunID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetLatestPaymentRun(SystemID As Integer) As ROPaymentRun

      Dim LastOpenRun As ROPaymentRun = Me.Where(Function(d) d.PaymentRunStatusID = CommonData.Enums.PaymentRunStatus.Open).OrderBy(Function(d) d.StartDate).FirstOrDefault
      If LastOpenRun Is Nothing Then
        LastOpenRun = Me.OrderBy(Function(d) d.EndDate).LastOrDefault
      End If

      Return LastOpenRun

    End Function

    Public Overrides Function ToString() As String

      Return "Payment Runs"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property PaymentRunID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property LatestForSystem As Boolean = False

      Public Sub New(SystemID As Integer?, LatestForSystem As Boolean)
        Me.SystemID = SystemID
        Me.PageNo = 1
        Me.PageSize = 1
        Me.LatestForSystem = LatestForSystem
      End Sub

      Public Sub New(PaymentRunID As Integer?)
        Me.PaymentRunID = PaymentRunID
        Me.PageNo = 1
        Me.PageSize = 1
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPaymentRunList() As ROPaymentRunList

      Return New ROPaymentRunList()

    End Function

    Public Shared Sub BeginGetROPaymentRunList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunList)))

      Dim dp As New DataPortal(Of ROPaymentRunList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPaymentRunList(CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunList)))

      Dim dp As New DataPortal(Of ROPaymentRunList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPaymentRunList() As ROPaymentRunList

      Return DataPortal.Fetch(Of ROPaymentRunList)(New Criteria())

    End Function

    Public Shared Function GetROLatestPaymentRunForSystem(SystemID As Integer?) As ROPaymentRunList

      Return DataPortal.Fetch(Of ROPaymentRunList)(New Criteria(SystemID, True))

    End Function

    Public Shared Function GetROPaymentRunList(PaymentRunID As Integer) As ROPaymentRunList

      Return DataPortal.Fetch(Of ROPaymentRunList)(New Criteria(PaymentRunID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPaymentRun.GetROPaymentRun(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPaymentRunList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(crit.PaymentRunID))
            cm.Parameters.AddWithValue("@LatestForSystem", crit.LatestForSystem)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace