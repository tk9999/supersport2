﻿' Generated 23 Jan 2016 18:51 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROInvoicePreviewDetail
    Inherits OBReadOnlyBase(Of ROInvoicePreviewDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AccountingDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AccountingDate, "Date")
    ''' <summary>
    ''' Gets the Accounting Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public ReadOnly Property AccountingDate As DateTime?
      Get
        Return GetProperty(AccountingDateProperty)
      End Get
    End Property

    <Display(Name:="Date", Description:="")>
    Public ReadOnly Property DateDisplay As String
      Get
        If AccountingDate IsNot Nothing Then
          Return AccountingDate.Value.ToString("ddd dd MMM yyyy")
        End If
        Return ""
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared CalculatedStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedStartDateTime, "Calculated Start Date Time")
    ''' <summary>
    ''' Gets the Calculated Start Date Time value
    ''' </summary>
    <Display(Name:="Calculated Start Date Time", Description:="")>
    Public ReadOnly Property CalculatedStartDateTime As DateTime?
      Get
        Return GetProperty(CalculatedStartDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property StartDateDisplay As String
      Get
        If CalculatedStartDateTime IsNot Nothing Then
          Return CalculatedStartDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared CalculatedEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedEndDateTime, "Calculated End Date Time")
    ''' <summary>
    ''' Gets the Calculated End Date Time value
    ''' </summary>
    <Display(Name:="Calculated End Date Time", Description:="")>
    Public ReadOnly Property CalculatedEndDateTime As DateTime?
      Get
        Return GetProperty(CalculatedEndDateTimeProperty)
      End Get
    End Property

    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property EndDateDisplay As String
      Get
        If CalculatedEndDateTime IsNot Nothing Then
          Return CalculatedEndDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetCategoryID, "Timesheet Category")
    ''' <summary>
    ''' Gets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:="")>
    Public ReadOnly Property TimesheetCategoryID() As Integer
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetCategory, "Category")
    ''' <summary>
    ''' Gets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Category", Description:="")>
    Public ReadOnly Property TimesheetCategory() As String
      Get
        Return GetProperty(TimesheetCategoryProperty)
      End Get
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForDay, "Hours")
    ''' <summary>
    ''' Gets the Hours For Day value
    ''' </summary>
    <Display(Name:="Hours", Description:="")>
    Public ReadOnly Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Add. Hours")
    ''' <summary>
    ''' Gets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Add. Hours", Description:="")>
    Public ReadOnly Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
    End Property

    Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Rate, "Rate")
    ''' <summary>
    ''' Gets the Rate value
    ''' </summary>
    <Display(Name:="Rate", Description:="")>
    Public ReadOnly Property Rate() As Decimal
      Get
        Return GetProperty(RateProperty)
      End Get
    End Property

    Public Shared HourlyRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HourlyRate, "Hourly Rate")
    ''' <summary>
    ''' Gets the Hourly Rate value
    ''' </summary>
    <Display(Name:="Hourly Rate", Description:="")>
    Public ReadOnly Property HourlyRate() As Decimal
      Get
        Return GetProperty(HourlyRateProperty)
      End Get
    End Property

    Public Shared TxIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TxInd, "Tx", False)
    ''' <summary>
    ''' Gets the Tx value
    ''' </summary>
    <Display(Name:="Tx", Description:="")>
    Public ReadOnly Property TxInd() As Boolean
      Get
        Return GetProperty(TxIndProperty)
      End Get
    End Property

    Public Shared TxAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TxAmount, "Tx Amount")
    ''' <summary>
    ''' Gets the Tx Amount value
    ''' </summary>
    <Display(Name:="Tx Amount", Description:="")>
    Public ReadOnly Property TxAmount() As Decimal
      Get
        Return GetProperty(TxAmountProperty)
      End Get
    End Property

    Public Shared OvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Overtime, "Additional")
    ''' <summary>
    ''' Gets the Overtime value
    ''' </summary>
    <Display(Name:="Additional", Description:="")>
    Public ReadOnly Property Overtime() As Decimal
      Get
        Return GetProperty(OvertimeProperty)
      End Get
    End Property

    Public Shared OTAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OTAmount, "Add. Amount")
    ''' <summary>
    ''' Gets the OT Amount value
    ''' </summary>
    <Display(Name:="Add. Amount", Description:="")>
    Public ReadOnly Property OTAmount() As Decimal
      Get
        Return GetProperty(OTAmountProperty)
      End Get
    End Property

    Public Shared RigIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RigInd, "Rig", False)
    ''' <summary>
    ''' Gets the Rig value
    ''' </summary>
    <Display(Name:="Rig", Description:="")>
    Public ReadOnly Property RigInd() As Boolean
      Get
        Return GetProperty(RigIndProperty)
      End Get
    End Property

    Public Shared RigAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RigAmount, "Rig Amount")
    ''' <summary>
    ''' Gets the Rig Amount value
    ''' </summary>
    <Display(Name:="Rig Amount", Description:="")>
    Public ReadOnly Property RigAmount() As Decimal
      Get
        Return GetProperty(RigAmountProperty)
      End Get
    End Property

    Public Shared TvlIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TvlInd, "Tvl", False)
    ''' <summary>
    ''' Gets the Tvl value
    ''' </summary>
    <Display(Name:="Tvl", Description:="")>
    Public ReadOnly Property TvlInd() As Boolean
      Get
        Return GetProperty(TvlIndProperty)
      End Get
    End Property

    Public Shared TvlAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TvlAmount, "Tvl Amount")
    ''' <summary>
    ''' Gets the Tvl Amount value
    ''' </summary>
    <Display(Name:="Tvl Amount", Description:="")>
    Public ReadOnly Property TvlAmount() As Decimal
      Get
        Return GetProperty(TvlAmountProperty)
      End Get
    End Property

    Public Shared DayAwayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DayAwayInd, "Day Away", False)
    ''' <summary>
    ''' Gets the Day Away value
    ''' </summary>
    <Display(Name:="Day Away", Description:="")>
    Public ReadOnly Property DayAwayInd() As Boolean
      Get
        Return GetProperty(DayAwayIndProperty)
      End Get
    End Property

    Public Shared DayAwayAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DayAwayAmount, "Day Away Amount")
    ''' <summary>
    ''' Gets the Day Away Amount value
    ''' </summary>
    <Display(Name:="Day Away Amount", Description:="")>
    Public ReadOnly Property DayAwayAmount() As Decimal
      Get
        Return GetProperty(DayAwayAmountProperty)
      End Get
    End Property

    Public Shared AdHocIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AdHocInd, "Ad Hoc", False)
    ''' <summary>
    ''' Gets the Ad Hoc value
    ''' </summary>
    <Display(Name:="Ad Hoc", Description:="")>
    Public ReadOnly Property AdHocInd() As Boolean
      Get
        Return GetProperty(AdHocIndProperty)
      End Get
    End Property

    Public Shared AdHocAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AdHocAmount, "AdHoc Amount")
    ''' <summary>
    ''' Gets the Ad Hoc Amount value
    ''' </summary>
    <Display(Name:="AdHoc Amount", Description:="")>
    Public ReadOnly Property AdHocAmount() As Decimal
      Get
        Return GetProperty(AdHocAmountProperty)
      End Get
    End Property

    Public Shared TrvAdHocIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrvAdHocInd, "Trv Ad Hoc", False)
    ''' <summary>
    ''' Gets the Trv Ad Hoc value
    ''' </summary>
    <Display(Name:="Trv Ad Hoc", Description:="")>
    Public ReadOnly Property TrvAdHocInd() As Boolean
      Get
        Return GetProperty(TrvAdHocIndProperty)
      End Get
    End Property

    Public Shared TrvAdHocAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TrvAdHocAmount, "TrvAdHoc Amount")
    ''' <summary>
    ''' Gets the Trv Ad Hoc Amount value
    ''' </summary>
    <Display(Name:="TrvAdHoc Amount", Description:="")>
    Public ReadOnly Property TrvAdHocAmount() As Decimal
      Get
        Return GetProperty(TrvAdHocAmountProperty)
      End Get
    End Property

    Public Shared FacilityIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FacilityInd, "Facility", False)
    ''' <summary>
    ''' Gets the Facility value
    ''' </summary>
    <Display(Name:="Facility", Description:="")>
    Public ReadOnly Property FacilityInd() As Boolean
      Get
        Return GetProperty(FacilityIndProperty)
      End Get
    End Property

    Public Shared FacilityAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.FacilityAmount, "Fac. Amount")
    ''' <summary>
    ''' Gets the Facility Amount value
    ''' </summary>
    <Display(Name:="Fac. Amount", Description:="")>
    Public ReadOnly Property FacilityAmount() As Decimal
      Get
        Return GetProperty(FacilityAmountProperty)
      End Get
    End Property

    Public Shared TaxableTotalProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TaxableTotal, "Taxable")
    ''' <summary>
    ''' Gets the Taxable Total value
    ''' </summary>
    <Display(Name:="Taxable", Description:="")>
    Public ReadOnly Property TaxableTotal() As Decimal
      Get
        Return GetProperty(TaxableTotalProperty)
      End Get
    End Property

    Public Shared SnTProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SnT, "S&T")
    ''' <summary>
    ''' Gets the Sn T value
    ''' </summary>
    <Display(Name:="S&T", Description:="")>
    Public ReadOnly Property SnT() As Decimal
      Get
        Return GetProperty(SnTProperty)
      End Get
    End Property

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTimesheetID, "Crew Timesheet")
    ''' <summary>
    ''' Gets the Crew Timesheet value
    ''' </summary>
    <Display(Name:="Crew Timesheet", Description:="")>
    Public ReadOnly Property CrewTimesheetID() As Integer
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceDetailID, "Creditor Invoice Detail")
    ''' <summary>
    ''' Gets the Creditor Invoice Detail value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail", Description:="")>
    Public ReadOnly Property CreditorInvoiceDetailID() As Integer
      Get
        Return GetProperty(CreditorInvoiceDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "ProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Category value
    ''' </summary>
    <Display(Name:="ProductionAreaID")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    ''' <summary>
    ''' Gets the Vat Type value
    ''' </summary>
    <Display(Name:="Area")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Description

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROInvoicePreviewDetail(dr As SafeDataReader) As ROInvoicePreviewDetail

      Dim r As New ROInvoicePreviewDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(AccountingDateProperty, .GetValue(1))
        LoadProperty(DescriptionProperty, .GetString(2))
        LoadProperty(DisciplineProperty, .GetString(3))
        LoadProperty(CalculatedStartDateTimeProperty, .GetValue(4))
        LoadProperty(CalculatedEndDateTimeProperty, .GetValue(5))
        LoadProperty(TimesheetCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(TimesheetCategoryProperty, .GetString(7))
        LoadProperty(HoursForDayProperty, .GetDecimal(8))
        LoadProperty(OvertimeHoursProperty, .GetDecimal(9))
        LoadProperty(RateProperty, .GetDecimal(10))
        LoadProperty(HourlyRateProperty, .GetDecimal(11))
        LoadProperty(TxIndProperty, .GetBoolean(12))
        LoadProperty(TxAmountProperty, .GetDecimal(13))
        LoadProperty(OvertimeProperty, .GetDecimal(14))
        LoadProperty(OTAmountProperty, .GetDecimal(15))
        LoadProperty(RigIndProperty, .GetBoolean(16))
        LoadProperty(RigAmountProperty, .GetDecimal(17))
        LoadProperty(TvlIndProperty, .GetBoolean(18))
        LoadProperty(TvlAmountProperty, .GetDecimal(19))
        LoadProperty(DayAwayIndProperty, .GetBoolean(20))
        LoadProperty(DayAwayAmountProperty, .GetDecimal(21))
        LoadProperty(AdHocIndProperty, .GetBoolean(22))
        LoadProperty(AdHocAmountProperty, .GetDecimal(23))
        LoadProperty(TrvAdHocIndProperty, .GetBoolean(24))
        LoadProperty(TrvAdHocAmountProperty, .GetDecimal(25))
        LoadProperty(FacilityIndProperty, .GetBoolean(26))
        LoadProperty(FacilityAmountProperty, .GetDecimal(27))
        LoadProperty(TaxableTotalProperty, .GetDecimal(28))
        LoadProperty(SnTProperty, .GetDecimal(29))
        LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
        LoadProperty(CreditorInvoiceDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(31)))
        LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(32)))
        LoadProperty(ProductionAreaProperty, .GetString(33))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace