﻿' Generated 01 Apr 2015 10:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports Singular
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Invoicing.New

  <Serializable()> _
  Public Class PaymentRun
    Inherits OBBusinessBase(Of PaymentRun)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return PaymentRunBO.ToString(self)")

    Public Shared PaymentRunIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PaymentRunID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property PaymentRunID() As Integer
      Get
        Return GetProperty(PaymentRunIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:="The sub-department to which this payment run applies"),
    Required(ErrorMessage:="Sub-Department required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemList), UnselectedText:="Sub-Dept.")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemIDProperty, value)
      End Set
    End Property

    Public Shared MonthProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.Month, Nothing)
    ''' <summary>
    ''' Gets and sets the Month value
    ''' </summary>
    <Display(Name:="Month"),
    Required(ErrorMessage:="Month required"),
    DropDownWeb(GetType(Maintenance.ReadOnly.ROMonthList))>
    Public Property Month() As Integer?
      Get
        Return GetProperty(MonthProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(MonthProperty, Value)
      End Set
    End Property

    ',
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.CommonData.Enums.Month),
    '                                     Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData,
    '                                     UnselectedText:="Month")

    Public Shared YearProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.Year, Nothing)
    ''' <summary>
    ''' Gets and sets the Year value
    ''' </summary>
    <Display(Name:="Year", Description:="The year of the payment run"),
    Required(ErrorMessage:="Year required")>
    Public Property Year() As Integer?
      Get
        Return GetProperty(YearProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(YearProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The start date of the payment run"),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The end date of the payment run"),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared PaymentRunStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PaymentRunStatusID, "Status", Nothing)
    ''' <summary>
    ''' Gets and sets the Payment Run Status value
    ''' </summary>
    <Display(Name:="Status", Description:="Current status for the payment run status"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Invoicing.ReadOnly.ROPaymentRunStatusList), UnselectedText:="Select Status...")>
    Public Property PaymentRunStatusID() As Integer?
      Get
        Return GetProperty(PaymentRunStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PaymentRunStatusIDProperty, Value)
      End Set
    End Property

    Public Shared MonthNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MonthName, "Month", "")
    ''' <summary>
    ''' Gets the Month Name value
    ''' </summary>
    <Display(Name:="Month", Description:="Name of the Month")>
    Public ReadOnly Property MonthName() As String
      Get
        Return GetProperty(MonthNameProperty)
      End Get
    End Property

    <Display(Name:="Month")>
    Public ReadOnly Property MonthString As String
      Get
        If Month > 0 AndAlso Year > 0 Then
          Return OBMisc.GetMonthDescription(Month)
        End If
        Return ""
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System", "")
    <Display(Name:="Sub-Dept")>
    Public ReadOnly Property System As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Month")>
    Public ReadOnly Property MonthYearString As String
      Get
        Return MonthString & " " & Year.ToString
      End Get
    End Property

    Public Shared StatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Status, "Status", "")
    <Display(Name:="Status")>
    Public Property Status As String
      Get
        Return GetProperty(StatusProperty)
      End Get
      Set(value As String)
        SetProperty(StatusProperty, value)
      End Set
    End Property

    Public Shared TotalInvoicesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalInvoices, "Total Invoices", 0)
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Total Invoices")>
    Public Property TotalInvoices() As Integer
      Get
        Return GetProperty(TotalInvoicesProperty)
      End Get
      Set(value As Integer)
        SetProperty(TotalInvoicesProperty, value)
      End Set
    End Property

    Public Shared TotalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalAmount, "Total Amount", 0)
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Total Amount")>
    Public Property TotalAmount() As Decimal
      Get
        Return GetProperty(TotalAmountProperty)
      End Get
      Set(value As Decimal)
        SetProperty(TotalAmountProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared CreditorInvoiceListProperty As PropertyInfo(Of CreditorInvoiceList) = RegisterProperty(Of CreditorInvoiceList)(Function(c) c.CreditorInvoiceList, "Creditor Invoice List")

    Public ReadOnly Property CreditorInvoiceList() As CreditorInvoiceList
      Get
        If GetProperty(CreditorInvoiceListProperty) Is Nothing Then
          LoadProperty(CreditorInvoiceListProperty, Invoicing.New.CreditorInvoiceList.NewCreditorInvoiceList())
        End If
        Return GetProperty(CreditorInvoiceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PaymentRunIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Month.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Payment Run")
        Else
          Return String.Format("Blank {0}", "Payment Run")
        End If
      Else
        Return Me.Month.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CreditorInvoices"}
      End Get
    End Property

    Sub GenerateInvoices()

      If Not Singular.Misc.IsNullNothing(Me.PaymentRunID, True) Then
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdPaymentRunGenerateInvoices",
                   New String() {
                                  "@PaymentRunID",
                                  "@UserID"
                                },
                   New Object() {
                                  Me.PaymentRunID,
                                  OBLib.Security.Settings.CurrentUser.UserID
                                })
        cmd.FetchType = CommandProc.FetchTypes.DataSet
        cmd.CommandTimeout = 0
        cmd = cmd.Execute()
      Else
        Throw New Exception("Payment Run must have an ID before invoices can be generated. Please ensure that the Payment Run is saved")
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(YearProperty)
      '  .JavascriptRuleFunctionName = "PaymentRunBO.YearValid"
      '  .ServerRuleFunction = AddressOf YearValid
      'End With

      'With AddWebRule(MonthProperty)
      '  .JavascriptRuleFunctionName = "PaymentRunBO.MonthValid"
      '  .ServerRuleFunction = AddressOf MonthValid
      'End With

      'With AddWebRule(PaymentRunStatusIDProperty)
      '  .JavascriptRuleFunctionName = "PaymentRunBO.StatusValid"
      '  .ServerRuleFunction = AddressOf StatusValid
      'End With

      'With AddWebRule(StartDateProperty)
      '  .AddTriggerProperty(EndDateProperty)
      '  '.JavascriptRuleFunctionName = "PaymentRunBO.StatusValid"
      '  .ServerRuleFunction = AddressOf TimesValid
      'End With

    End Sub

    Public Shared Function YearValid(PR As PaymentRun) As String
      Dim ErrorString = ""
      If PR.Year = 0 Then
        ErrorString = "Year Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function MonthValid(PR As PaymentRun) As String
      Dim ErrorString = ""
      If PR.Month = 0 Then
        ErrorString = "Month Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function StatusValid(PR As PaymentRun) As String
      Dim ErrorString = ""
      Select Case PR.PaymentRunStatusID
        Case OBLib.CommonData.Enums.PaymentRunStatus.Closed,
             OBLib.CommonData.Enums.PaymentRunStatus.Completed,
             OBLib.CommonData.Enums.PaymentRunStatus.Open
        Case Else
          ErrorString = "Status Required"
      End Select
      Return ErrorString
    End Function

    Public Shared Function TimesValid(PR As PaymentRun) As String

      Dim ErrorMsg As String = ""
      Dim PaymentRunID As Integer? = Nothing

      If PR.StartDate IsNot Nothing AndAlso PR.EndDate IsNot Nothing Then
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdGetOverlappingPaymentRuns", _
                                         New String() {"PaymentRunID",
                                                       "StartDate",
                                                       "EndDate",
                                                       "SystemID"}, _
                                         New Object() {Singular.Misc.ZeroDBNull(PR.PaymentRunID),
                                                       PR.StartDate,
                                                       PR.EndDate,
                                                       PR.SystemID})
        cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmd.Execute()

        If cmd.Dataset IsNot Nothing Then
          If cmd.Dataset.Tables(0).Rows.Count > 0 Then

            For Each row As DataRow In cmd.Dataset.Tables(0).Rows

              If ErrorMsg = "" Then
                ErrorMsg += "Payment Run dates overlaps with: " & row("StartDate") & " - " & row("EndDate")
              Else
                ErrorMsg += vbCrLf & row("StartDate") & " - " & row("EndDate")
              End If

            Next

          End If
        End If

      End If

      Return ErrorMsg

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPaymentRun() method.

    End Sub

    Public Shared Function NewPaymentRun() As PaymentRun

      Return DataPortal.CreateChild(Of PaymentRun)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPaymentRun(dr As SafeDataReader) As PaymentRun

      Dim p As New PaymentRun()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PaymentRunIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(MonthProperty, .GetInt32(2))
          LoadProperty(YearProperty, .GetInt32(3))
          LoadProperty(StartDateProperty, .GetValue(4))
          LoadProperty(EndDateProperty, .GetValue(5))
          LoadProperty(PaymentRunStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(StatusProperty, .GetString(11))
          LoadProperty(TotalInvoicesProperty, .GetInt32(12))
          LoadProperty(TotalAmountProperty, .GetDecimal(13))
          LoadProperty(SystemProperty, .GetString(14))
          'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          'LoadProperty(ProductionAreaProperty, .GetString(16))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPaymentRun"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPaymentRun"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPaymentRunID As SqlParameter = .Parameters.Add("@PaymentRunID", SqlDbType.Int)
          paramPaymentRunID.Value = GetProperty(PaymentRunIDProperty)
          If Me.IsNew Then
            paramPaymentRunID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID) 'GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@Month", GetProperty(MonthProperty))
          .Parameters.AddWithValue("@Year", GetProperty(YearProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@PaymentRunStatusID", GetProperty(PaymentRunStatusIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID) 'GetProperty(ModifiedByProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PaymentRunIDProperty, paramPaymentRunID.Value)
            Dim ros As ROSystem = OBLib.CommonData.Lists.ROSystemList.GetItem(SystemID)
            If ros IsNot Nothing Then
              LoadProperty(SystemProperty, ros.System)
            End If
          End If
          ' update child objects
          If GetProperty(CreditorInvoiceListProperty) IsNot Nothing Then
            Me.CreditorInvoiceList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(CreditorInvoiceListProperty) IsNot Nothing Then
          Me.CreditorInvoiceList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPaymentRun"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PaymentRunID", GetProperty(PaymentRunIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace