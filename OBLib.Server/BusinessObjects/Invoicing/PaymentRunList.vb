﻿' Generated 01 Apr 2015 10:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Invoicing.New

  <Serializable()> _
  Public Class PaymentRunList
    Inherits OBBusinessListBase(Of PaymentRunList, PaymentRun)

#Region " Business Methods "

    Public Function GetItem(PaymentRunID As Integer) As PaymentRun

      For Each child As PaymentRun In Me
        If child.PaymentRunID = PaymentRunID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Payment Runs"

    End Function

    Public Function GetCreditorInvoice(CreditorInvoiceID As Integer) As CreditorInvoice

      Dim obj As CreditorInvoice = Nothing
      For Each parent As PaymentRun In Me
        obj = parent.CreditorInvoiceList.GetItem(CreditorInvoiceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property PaymentRunID As Integer? = Nothing
      Public Property ParentOnly As Boolean = True

      Public Sub New()


      End Sub

      Public Sub New(PaymentRunID As Integer?, ParentOnly As Boolean)

        Me.PaymentRunID = PaymentRunID
        Me.ParentOnly = ParentOnly

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewPaymentRunList() As PaymentRunList

      Return New PaymentRunList()

    End Function

    Public Shared Sub BeginGetPaymentRunList(CallBack As EventHandler(Of DataPortalResult(Of PaymentRunList)))

      Dim dp As New DataPortal(Of PaymentRunList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetPaymentRunList() As PaymentRunList

      Return DataPortal.Fetch(Of PaymentRunList)(New Criteria())

    End Function

    Public Shared Function GetPaymentRunList(PaymentRunID As Integer?, ParentOnly As Boolean) As PaymentRunList

      Return DataPortal.Fetch(Of PaymentRunList)(New Criteria(PaymentRunID, ParentOnly))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(PaymentRun.GetPaymentRun(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As PaymentRun = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.PaymentRunID <> sdr.GetInt32(16) Then
            parent = Me.GetItem(sdr.GetInt32(16))
          End If
          parent.CreditorInvoiceList.RaiseListChangedEvents = False
          parent.CreditorInvoiceList.Add(CreditorInvoice.GetCreditorInvoice(sdr))
          parent.CreditorInvoiceList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As CreditorInvoice = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.CreditorInvoiceID <> sdr.GetInt32(1) Then
            parentChild = Me.GetCreditorInvoice(sdr.GetInt32(1))
          End If
          parentChild.CreditorInvoiceDetailList.RaiseListChangedEvents = False
          parentChild.CreditorInvoiceDetailList.Add(CreditorInvoiceDetail.GetCreditorInvoiceDetail(sdr))
          parentChild.CreditorInvoiceDetailList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As PaymentRun In Me
        child.CheckRules()
        For Each CreditorInvoice As CreditorInvoice In child.CreditorInvoiceList
          CreditorInvoice.CheckRules()

          For Each CreditorInvoiceDetail As CreditorInvoiceDetail In CreditorInvoice.CreditorInvoiceDetailList
            CreditorInvoiceDetail.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getPaymentRunList"
            cm.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(crit.PaymentRunID))
            cm.Parameters.AddWithValue("@ParentOnly", crit.ParentOnly)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As PaymentRun In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As PaymentRun In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

    Shared Function GetPaymentRunList(p1 As Integer) As PaymentRun
      Throw New NotImplementedException
    End Function

  End Class

End Namespace