﻿' Generated 01 Apr 2015 10:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Invoicing.New

  <Serializable()> _
  Public Class CreditorInvoiceDetailList
    Inherits OBBusinessListBase(Of CreditorInvoiceDetailList, CreditorInvoiceDetail)

#Region " Business Methods "

    Public Function GetItem(CreditorInvoiceDetailID As Integer) As CreditorInvoiceDetail

      For Each child As CreditorInvoiceDetail In Me
        If child.CreditorInvoiceDetailID = CreditorInvoiceDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Creditor Invoice Details"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewCreditorInvoiceDetailList() As CreditorInvoiceDetailList

      Return New CreditorInvoiceDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CreditorInvoiceDetail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CreditorInvoiceDetail In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace