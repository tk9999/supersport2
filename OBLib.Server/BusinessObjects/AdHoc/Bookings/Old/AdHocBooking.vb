﻿' Generated 17 Oct 2014 09:09 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AdHoc.Old

  <Serializable()> _
  Public Class AdHocBooking
    Inherits OBBusinessBase(Of AdHocBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AdHocBookingTypeID, Nothing) _
                                                                            .AddSetExpression("AdHocBookingBOOld.AdHocBookingTypeIDSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Booking Type", Description:=""),
    Required(ErrorMessage:="Ad Hoc Booking Type required"),
    DropDownWeb(GetType(Maintenance.AdHoc.ReadOnly.ROSystemAreaAdHocBookingTypeList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="AdHocBookingBOOld.setAdHocBookingTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="AdHocBookingBOOld.triggerAdHocBookingTypeIDAutoPopulate",
                AfterFetchJS:="AdHocBookingBOOld.afterAdHocBookingTypeIDRefreshAjax",
                LookupMember:="AdHocBookingType", ValueMember:="AdHocBookingTypeID", DropDownColumns:={"AdHocBookingType"},
                OnItemSelectJSFunction:="AdHocBookingBOOld.onAdHocBookingTypeSelected", DropDownCssClass:="hr-dropdown")>
    Public Property AdHocBookingTypeID() As Integer?
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingTypeIDProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.AdHoc.ReadOnly.ROSystemAreaAdHocBookingTypeList), _
    '                                 Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel, _
    '                                 DisplayMember:="AdHocBookingType", _
    '                                 ValueMember:="AdHocBookingTypeID")

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AdHocBookingType, "")
    ''' <summary>
    ''' Gets and sets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Booking Type")>
    Public Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdHocBookingTypeProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaAdHocAdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaAdHocBookingTypeID, "System Area AdHoc Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the System Area Ad Hoc Booking Type value
    ''' </summary>>
    Public Property SystemAreaAdHocBookingTypeID() As Integer?
      Get
        Return GetProperty(SystemAreaAdHocAdHocBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaAdHocAdHocBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Title, "") _
                                                                      .AddSetExpression("AdHocBookingBOOld.TitleSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""), Required(ErrorMessage:="Title is required"),
    StringLength(50, ErrorMessage:="Title cannot be more than 50 characters")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Description, "") _
                                                                      .AddSetExpression("AdHocBookingBOOld.DescriptionSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required, DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required, DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Call Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime) _
                                                                      .AddSetExpression("AdHocBookingBOOld.StartDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime) _
                                                                      .AddSetExpression("AdHocBookingBOOld.EndDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostCentreID, "Cost Centre", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCostCentreList))>
    Public Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostCentreIDProperty, Value)
      End Set
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", OBLib.CommonData.Enums.Country.SouthAfrica)
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(CountryIDProperty, value)
      End Set
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LocationID, "Location", Nothing)
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property LocationID() As Integer?
      Get
        Return GetProperty(LocationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LocationIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
    ''' <summary>
    ''' Gets the Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref No", Description:="")>
    Public Property GenRefNo() As Int64?
      Get
        Return GetProperty(GenRefNoProperty)
      End Get
      Set(value As Int64?)
        SetProperty(GenRefNoProperty, value)
      End Set
    End Property

    Public Shared UsingDefaultGenRefReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.UsingDefaultGenRefReason, "")
    ''' <summary>
    ''' Gets and sets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Reason for using Default Gen Ref No.", Description:=""), StringLength(100, ErrorMessage:="Reason should not be longer than 100 characters")>
    Public Property UsingDefaultGenRefReason() As String
      Get
        Return GetProperty(UsingDefaultGenRefReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(UsingDefaultGenRefReasonProperty, Value)
      End Set
    End Property

    Public Shared RequiresTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTravel, "Requires Travel?", False)
    ''' <summary>
    ''' Gets and sets the Requires Travel value
    ''' </summary>
    <Display(Name:="Requires Travel?", Description:="")>
    Public Property RequiresTravel() As Boolean
      Get
        Return GetProperty(RequiresTravelProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiresTravelProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SystemArea, "")
    ''' <summary>
    ''' Gets and sets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Sub-Dept (Area)"),
    AlwaysClean>
    Public Property SystemArea() As String
      Get
        Return GetProperty(SystemAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemAreaProperty, Value)
      End Set
    End Property

    Public Shared CountryLocationProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CountryLocation, "")
    ''' <summary>
    ''' Gets and sets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Country (Location)"),
    AlwaysClean>
    Public Property CountryLocation() As String
      Get
        Return GetProperty(CountryLocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CountryLocationProperty, Value)
      End Set
    End Property

#End Region

#Region " Non-Database Properties"

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "AdHocBookingBOOld.ToString(self)")

    Public Shared ExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Expanded, "Expanded", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Expanded", Description:="")>
    Public Property Expanded() As Boolean
      Get
        Return GetProperty(ExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExpandedProperty, Value)
      End Set
    End Property

    Public Property RoomScheduleID As Integer?

    Public Shared CheckRulesIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CheckRulesInd, False) _
                                                                      .AddSetExpression("AdHocBookingBOOld.CheckRulesIndSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Creditor value
    ''' </summary>
    <Display(Name:="Completed?", Description:="")>
    Public Property CheckRulesInd() As Boolean
      Get
        Return GetProperty(CheckRulesIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CheckRulesIndProperty, Value)
      End Set
    End Property

    Public Shared DefaultGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.DefaultGenRefNo, "Default Gen Ref No", Nothing)
    <Display(Name:="Default Gen Ref No", Description:="")>
    Public Property DefaultGenRefNo() As Int64?
      Get
        Return GetProperty(DefaultGenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(DefaultGenRefNoProperty, Value)
      End Set
    End Property

    Public Shared HasValidGenRefProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.HasValidGenRef, "Has Valid Gen Ref?")
    Public Property HasValidGenRef() As Boolean?
      Get
        Return GetProperty(HasValidGenRefProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(HasValidGenRefProperty, Value)
      End Set
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "South Africa")
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CountryProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location", "")
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared AdHocBookingHumanResourceListProperty As PropertyInfo(Of AdHocBookingHumanResourceList) = RegisterProperty(Of AdHocBookingHumanResourceList)(Function(c) c.AdHocBookingHumanResourceList, "Team Discipline Human Resource List")
    Public ReadOnly Property AdHocBookingHumanResourceList() As AdHocBookingHumanResourceList
      Get
        If GetProperty(AdHocBookingHumanResourceListProperty) Is Nothing Then
          LoadProperty(AdHocBookingHumanResourceListProperty, AdHocBookingHumanResourceList.NewAdHocBookingHumanResourceList())
        End If
        Return GetProperty(AdHocBookingHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdHocBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Ad Hoc Booking")
        Else
          Return String.Format("Blank {0}", "Ad Hoc Booking")
        End If
      Else
        Return Me.Title
      End If

    End Function

    Public Sub NewAdhocBookingHumanResource()

      Dim ad As AdHocBookingHumanResource = Me.AdHocBookingHumanResourceList.AddNew()
      ad.StartDateTime = Me.StartDateTime
      ad.EndDateTime = Me.EndDateTime

    End Sub

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CrewScheduleDetails"}
      End Get
    End Property

    Public Sub CopyFrom(From As AdHocBooking)

      LoadProperty(AdHocBookingIDProperty, From.AdHocBookingID)
      LoadProperty(AdHocBookingTypeIDProperty, From.AdHocBookingTypeID)
      LoadProperty(SystemIDProperty, From.SystemID)
      LoadProperty(TitleProperty, From.Title)
      LoadProperty(DescriptionProperty, From.Description)
      LoadProperty(SystemIDProperty, From.SystemID)
      LoadProperty(ProductionAreaIDProperty, From.ProductionAreaID)
      LoadProperty(StartDateTimeProperty, From.StartDateTime)
      LoadProperty(EndDateTimeProperty, From.EndDateTime)
      LoadProperty(CreatedByProperty, From.CreatedBy)
      LoadProperty(CreatedDateTimeProperty, From.CreatedDateTime)
      LoadProperty(ModifiedByProperty, From.ModifiedBy)
      LoadProperty(ModifiedDateTimeProperty, From.ModifiedDateTime)
      LoadProperty(AdHocBookingHumanResourceListProperty, From.AdHocBookingHumanResourceList)
      MarkOld()

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EndDateTimeProperty,
                Function(d) d.EndDateTime < d.StartDateTime,
                Function(d) "Start date must be before end date")
        .AffectedProperties.Add(StartDateTimeProperty)
        .AddTriggerProperty(StartDateTimeProperty)
      End With

      'With AddWebRule(DescriptionProperty)
      '  .JavascriptRuleFunctionName = "AdHocBookingBO.DescriptionValid"
      '  .ServerRuleFunction = AddressOf DescriptionValid
      'End With

      'With AddWebRule(EndDateTimeProperty)
      '  '.JavascriptRuleFunctionName = "CheckSupervisorRejectedReason"
      '  .ServerRuleFunction = Function(ts)
      '                          If ts.EndDateTime < ts.StartDateTime Then
      '                            Return "Start date must be before end date"
      '                          End If
      '                          Return ""

      '                        End Function
      '  .AffectedProperties.Add(StartDateTimeProperty)
      '  .AddTriggerProperty(StartDateTimeProperty)
      'End With

    End Sub

    Public Shared Function DescriptionValid(AdHocBooking As AdHocBooking) As String

      If AdHocBooking.Description.Trim.Length = 0 Then
        Return "Description is required"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdHocBooking() method.
      Me.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
      Me.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    End Sub

    Public Shared Function NewAdHocBooking() As AdHocBooking

      Return DataPortal.CreateChild(Of AdHocBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAdHocBooking(dr As SafeDataReader) As AdHocBooking

      Dim a As New AdHocBooking()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AdHocBookingIDProperty, .GetInt32(0))
          LoadProperty(AdHocBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TitleProperty, .GetString(2))
          LoadProperty(DescriptionProperty, .GetString(3))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(StartDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeProperty, .GetValue(7))
          LoadProperty(CountryIDProperty, ZeroNothing(.GetInt32(8)))
          LoadProperty(RequiresTravelProperty, .GetBoolean(9))
          LoadProperty(GenRefNoProperty, .GetInt64(10))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(LocationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(UsingDefaultGenRefReasonProperty, .GetString(17))
          LoadProperty(CountryProperty, .GetString(18))
          LoadProperty(LocationProperty, .GetString(19))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(20))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(21))
          LoadProperty(SystemAreaProperty, .GetString(22))
          LoadProperty(AdHocBookingTypeProperty, .GetString(23))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAdHocBookingOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAdHocBookingOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAdHocBookingID As SqlParameter = .Parameters.Add("@AdHocBookingID", SqlDbType.Int)
          paramAdHocBookingID.Value = GetProperty(AdHocBookingIDProperty)
          If Me.IsNew Then
            paramAdHocBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AdHocBookingTypeID", GetProperty(AdHocBookingTypeIDProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@CountryID", NothingDBNull(GetProperty(CountryIDProperty)))
          .Parameters.AddWithValue("@LocationID", NothingDBNull(GetProperty(LocationIDProperty)))
          .Parameters.AddWithValue("@GenRefNo", Singular.Misc.NothingDBNull(GetProperty(GenRefNoProperty)))
          .Parameters.AddWithValue("@CostCentreID", NothingDBNull(GetProperty(CostCentreIDProperty)))
          .Parameters.AddWithValue("@RequiresTravel", GetProperty(RequiresTravelProperty))
          .Parameters.AddWithValue("@UsingDefaultGenRefReason", GetProperty(UsingDefaultGenRefReasonProperty))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AdHocBookingIDProperty, paramAdHocBookingID.Value)
          End If
          ' update child objects
          If GetProperty(AdHocBookingHumanResourceListProperty) IsNot Nothing Then
            Me.AdHocBookingHumanResourceList.Update()
          End If
          ' mChildList.Update()
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AdHocBookingHumanResourceListProperty) IsNot Nothing Then
          Me.AdHocBookingHumanResourceList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAdHocBookingOld"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AdHocBookingID", GetProperty(AdHocBookingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Function GetAttendeeByHRID(HumanResourceID As Integer?) As AdHocBookingHumanResource

      Dim obj As AdHocBookingHumanResource = Me.AdHocBookingHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, HumanResourceID)).FirstOrDefault
      Return obj

    End Function

    Public Sub AttendeeSelected(hrid As Integer?, hr As String, resID As Integer?)

      Dim att As AdHocBookingHumanResource = Me.AdHocBookingHumanResourceList.AddNew
      att.HumanResourceID = hrid
      att.HumanResource = hr
      att.ResourceID = resID
      att.StartDateTime = att.GetParent.StartDateTime
      att.EndDateTime = att.GetParent.EndDateTime
      att.SetResourceBookingDescription()
      att.CheckClash()

    End Sub

    Sub CheckAllClashes()

      Dim rbl As New List(Of Helpers.ResourceHelpers.ResourceBooking)
      For Each att As AdHocBookingHumanResource In Me.AdHocBookingHumanResourceList
        If att.ResourceID IsNot Nothing And att.StartDateTime IsNot Nothing And att.EndDateTime IsNot Nothing Then
          Dim rb As New Helpers.ResourceHelpers.ResourceBooking(att.Guid, att.ResourceID, att.ResourceBookingID, Nothing, att.StartDateTime, att.EndDateTime, Nothing,
                                                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
          rbl.Add(rb)
        End If
      Next

      OBLib.Helpers.ResourceHelpers.GetResourceClashes(rbl)

      For Each rb As Helpers.ResourceHelpers.ResourceBooking In rbl
        Dim att As AdHocBookingHumanResource = Me.AdHocBookingHumanResourceList.Where(Function(d) d.Guid = rb.ObjectGuid).FirstOrDefault
        If att IsNot Nothing Then
          rb.ClashesWith.ForEach(Sub(cls)
                                   att.ClashList.Add(cls)
                                 End Sub)
        End If
      Next

    End Sub

    Sub RemoveAttendee(guidToRemove As System.Guid)
      Dim Attendee As AdHocBookingHumanResource = Me.AdHocBookingHumanResourceList.Where(Function(d) d.Guid = guidToRemove).FirstOrDefault
      If Attendee IsNot Nothing Then
        Me.AdHocBookingHumanResourceList.Remove(Attendee)
      End If
      CheckAllClashes()
      CheckAllRules()
    End Sub

    Sub UpdateGenRef()
      If Me.AdHocBookingTypeID IsNot Nothing Then
        Dim setting As OBLib.Maintenance.AdHoc.ReadOnly.ROSystemAreaAdHocBookingType = OBLib.CommonData.Lists.ROSystemAreaAdHocBookingTypeList.GetItem(Me.SystemAreaAdHocBookingTypeID)
        If setting IsNot Nothing Then
          SetGenRef(setting.DefaultGenRefNo)
        End If
      End If
    End Sub

    Public Sub SetGenRef(GenRefNumber As Int64?)
      SetProperty(GenRefNoProperty, GenRefNumber)
    End Sub

  End Class

End Namespace