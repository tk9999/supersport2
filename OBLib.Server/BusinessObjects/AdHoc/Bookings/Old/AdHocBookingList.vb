﻿' Generated 17 Oct 2014 09:09 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc
Imports Singular

Namespace AdHoc.Old

  <Serializable()> _
  Public Class AdHocBookingList
    Inherits OBBusinessListBase(Of AdHocBookingList, AdHocBooking)

#Region " Business Methods "

    Public Function GetItem(AdHocBookingID As Integer) As AdHocBooking

      For Each child As AdHocBooking In Me
        If child.AdHocBookingID = AdHocBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Ad Hoc Bookings"

    End Function

    Public Function GetItemByGuid(Guid As String)

      'Get the timeline item which is to be removed
      Dim gid As Guid = New Guid(Guid)
      Dim AdHocBooking As AdHocBooking = Nothing
      For Each ad As AdHocBooking In Me
        If ad.Guid = gid Then
          AdHocBooking = ad
          Exit For
        End If
      Next

      Return AdHocBooking

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property AdHocBookingID As Integer? = Nothing
      Public Property AdHocBookingIDs As String = ""
      Public Property CurrentSystemID As Integer? = Nothing
      Public Property CurrentProductionAreaID As Integer? = Nothing
      Public Property AdHocBookingOnly As Boolean

      Public Sub New(AdHocBookingID As Integer?, AdHocBookingIDs As String,
                     CurrentSystemID As Integer?, CurrentProductionAreaID As Integer?,
                     AdHocBookingOnly As Boolean)
        Me.AdHocBookingID = AdHocBookingID
        Me.AdHocBookingIDs = AdHocBookingIDs
        Me.CurrentSystemID = CurrentSystemID
        Me.CurrentProductionAreaID = CurrentProductionAreaID
        Me.AdHocBookingOnly = AdHocBookingOnly
      End Sub

      Public Sub New(AdHocBookingID As Integer?, AdHocBookingOnly As Boolean)
        Me.AdHocBookingID = AdHocBookingID
        Me.CurrentSystemID = Security.Settings.CurrentUser.SystemID
        Me.AdHocBookingOnly = AdHocBookingOnly
      End Sub
      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAdHocBookingList() As AdHocBookingList

      Return New AdHocBookingList()

    End Function

    Public Shared Sub BeginGetAdHocBookingList(CallBack As EventHandler(Of DataPortalResult(Of AdHocBookingList)))

      Dim dp As New DataPortal(Of AdHocBookingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAdHocBookingList() As AdHocBookingList

      Return DataPortal.Fetch(Of AdHocBookingList)(New Criteria())

    End Function

    Public Shared Function GetAdHocBookingList(AdHocBookingIDs As String,
                                               CurrentSystemID As Integer?, CurrentProductionAreaID As Integer?,
                                               AdHocBookingOnly As Boolean) As AdHocBookingList

      Return DataPortal.Fetch(Of AdHocBookingList)(New Criteria(Nothing, AdHocBookingIDs, CurrentSystemID, CurrentProductionAreaID, AdHocBookingOnly))

    End Function

    Public Shared Function GetAdHocBookingList(AdHocBookingID As Integer?, AdHocBookingOnly As Boolean) As AdHocBookingList

      Return DataPortal.Fetch(Of AdHocBookingList)(New Criteria(AdHocBookingID, AdHocBookingOnly))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AdHocBooking.GetAdHocBooking(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AdHocBooking = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AdHocBookingID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AdHocBookingHumanResourceList.RaiseListChangedEvents = False
          parent.AdHocBookingHumanResourceList.Add(AdHocBookingHumanResource.GetAdHocBookingHumanResource(sdr))
          parent.AdHocBookingHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As AdHocBooking In Me
        child.CheckRules()
        For Each AdHocBookingHumanResource As AdHocBookingHumanResource In child.AdHocBookingHumanResourceList
          AdHocBookingHumanResource.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAdHocBookingListOld"
            cm.Parameters.AddWithValue("@SystemID", crit.CurrentSystemID)
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.CurrentProductionAreaID))
            cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(crit.AdHocBookingID))
            cm.Parameters.AddWithValue("@AdHocBookingIDs", Strings.MakeEmptyDBNull(crit.AdHocBookingIDs))
            cm.Parameters.AddWithValue("@AdHocBookingOnly", crit.AdHocBookingOnly)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AdHocBooking In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AdHocBooking In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace