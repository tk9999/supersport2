﻿' Generated 21 Oct 2014 13:16 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.ICR.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.PlayoutOps.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AdHoc.Old

  <Serializable()> _
  Public Class AdHocBookingHumanResource
    Inherits OBBusinessBase(Of AdHocBookingHumanResource)

#Region " Properties and Methods "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "AdHocBookingHumanResourceBO.ToString(self)")

#Region " Properties "

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewScheduleDetailID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public Property CrewScheduleDetailID() As Integer
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CrewScheduleDetailIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required"),
    DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceICRList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="AdHocBookingHumanResourceBO.setHumanResourceCriteriaBeforeRefresh",
                PreFindJSFunction:="AdHocBookingHumanResourceBO.triggerHumanResourceAutoPopulate",
                AfterFetchJS:="AdHocBookingHumanResourceBO.afterHumanResourceRefreshAjax",
                LookupMember:="HumanResource", ValueMember:="HumanResourceID", DropDownColumns:={"HumanResource"},
                OnItemSelectJSFunction:="AdHocBookingHumanResourceBO.onHumanResourceSelected", DropDownCssClass:="hr-dropdown"),
    SetExpression("AdHocBookingHumanResourceBO.HumanResourceIDSet(self)")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing) _
                                                                        .AddSetExpression("AdHocBookingHumanResourceBO.StartDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:=""),
    Required(ErrorMessage:="Start Date Time required")>
    Public Property StartDateTime() As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing) _
                                                                      .AddSetExpression("AdHocBookingHumanResourceBO.EndDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:=""),
    Required(ErrorMessage:="End Date Time required")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamID, "System Team", Nothing)
    ''' <summary>
    ''' Gets and sets the System Team ID value
    ''' </summary>
    <Display(Name:="System Team", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.TeamManagement.ReadOnly.ROSystemTeamList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property SystemTeamID() As Integer?
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamIDProperty, Value)
      End Set
    End Property

    Public ReadOnly Property UserSystemID As Integer?
      Get
        Return OBLib.Security.Settings.CurrentUser.SystemID
      End Get
    End Property

    Public Shared ClashProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clash, "Clash", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Clash", Description:="")>
    Public Property Clash() As String
      Get
        Return GetProperty(ClashProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashProperty, Value)
      End Set
    End Property

    Public Shared MealReimbursementIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MealReimbursementInd, "Meal Reimbursement ", True)
    ''' <summary>
    ''' Gets and sets the Meal Reimbursement value
    ''' </summary>
    <Display(Name:="Meal Reimbursement?", Description:="")>
    Public Property MealReimbursementInd() As Boolean
      Get
        Return GetProperty(MealReimbursementIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MealReimbursementIndProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="ResourceID", Description:="")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "ResourceBookingTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:="")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingHumanResourceClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingHumanResourceClashes, "AdHocBookingHumanResourceStartDateInvalid", "")
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="", Description:="")>
    Public Property AdHocBookingHumanResourceClashes() As String
      Get
        Return GetProperty(AdHocBookingHumanResourceClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdHocBookingHumanResourceClashesProperty, Value)
      End Set
    End Property

    Public Shared IsResourceBookingCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsResourceBookingCancelled, "IsResourceBookingCancelled ", False)
    ''' <summary>
    ''' Gets and sets the Meal Reimbursement value
    ''' </summary>
    <Display(Name:="IsResourceBookingCancelled?", Description:="")>
    Public Property IsResourceBookingCancelled() As Boolean
      Get
        Return GetProperty(IsResourceBookingCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsResourceBookingCancelledProperty, Value)
      End Set
    End Property

    Public Shared IsResourceBookingCancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsResourceBookingCancelledReason, "IsResourceBookingCancelledReason", "")
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="", Description:="")>
    Public Property IsResourceBookingCancelledReason() As String
      Get
        Return GetProperty(IsResourceBookingCancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsResourceBookingCancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingStatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingStatusCssClass, "ResourceBookingStatusCssClass", "")
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="", Description:="")>
    Public Property ResourceBookingStatusCssClass() As String
      Get
        Return GetProperty(ResourceBookingStatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingStatusCssClassProperty, Value)
      End Set
    End Property

    <Browsable(False)>
    Public ReadOnly Property ResourceBooking As Helpers.ResourceHelpers.ResourceBooking
      Get
        Return New Helpers.ResourceHelpers.ResourceBooking(Me.Guid, Me.ResourceID, Me.ResourceBookingID, Nothing, Me.StartDateTime, Me.EndDateTime, Nothing,
                                                           Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of String)) = RegisterProperty(Of List(Of String))(Function(c) c.ClashList, "Clash List")
    Public ReadOnly Property ClashList() As List(Of String)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of String))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParentList() As AdHocBookingHumanResourceList
      Return CType(Me.Parent, AdHocBookingHumanResourceList)
    End Function

    Public Function GetParent() As AdHocBooking

      If GetParentList() IsNot Nothing Then
        Return CType(GetParentList.Parent, AdHocBooking)
      End If
      Return Nothing

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CrewScheduleDetails", "ResourceBookings"}
      End Get
    End Property

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewScheduleDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CrewScheduleDetailID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Ad Hoc Booking Human Resource")
        Else
          Return String.Format("Blank {0}", "Ad Hoc Booking Human Resource")
        End If
      Else
        Return Me.CrewScheduleDetailID.ToString()
      End If

    End Function

    Public Sub SetResourceBookingDescription()

      Dim prnt As AdHocBooking = Me.GetParent
      If prnt IsNot Nothing AndAlso prnt.AdHocBookingTypeID IsNot Nothing Then
        Dim ahbt As OBLib.Maintenance.AdHoc.ReadOnly.ROAdHocBookingType = OBLib.CommonData.Lists.ROAdHocBookingTypeList.GetItem(prnt.AdHocBookingTypeID)
        If ahbt IsNot Nothing Then
          Me.ResourceBookingDescription = ahbt.AdHocBookingType & ": " & prnt.Title
        End If
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(EndDateTimeProperty,
      '                Function(d) d.EndDateTime < d.StartDateTime,
      '                Function(d) "Start date must be before end date")
      '  .AffectedProperties.Add(StartDateTimeProperty)
      '  .AddTriggerProperty(StartDateTimeProperty)
      'End With

      'With AddWebRule(SystemTeamIDProperty)
      '  .JavascriptRuleFunctionName = "AdHocBookingHumanResourceBO.TeamIDValid"
      '  .ServerRuleFunction = AddressOf TeamIDValid
      'End With

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "AdHocBookingHumanResourceBO.StartDateTimeValid"
        .ServerRuleFunction = AddressOf StartDateTimeValid
        .AddTriggerProperty(EndDateTimeProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
      End With

      'With AddWebRule(EndDateTimeProperty)
      '  .JavascriptRuleFunctionName = "AdHocBookingBO.EndDateTimeValid"
      '  .ServerRuleFunction = AddressOf EndDateTimeValid
      'End With

      'Check Already added
      With AddWebRule(HumanResourceIDProperty)
        .ServerRuleFunction = AddressOf AttendeeAlreadyAdded
        .JavascriptRuleFunctionName = "AdHocBookingHumanResourceBO.AttendeeAlreadyAdded"
        .Severity = Singular.Rules.RuleSeverity.Warning
        .AffectedProperties.Add(HumanResourceProperty)
        .AffectedProperties.Add(StartDateTimeProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
      End With

      With AddWebRule(ClashListProperty)
        .JavascriptRuleFunctionName = "AdHocBookingHumanResourceBO.HasClashesValid"
        .ServerRuleFunction = AddressOf HasClashesValid
        .Severity = Singular.Rules.RuleSeverity.Error
        .AffectedProperties.Add(HumanResourceProperty)
        .AffectedProperties.Add(StartDateTimeProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
      End With

    End Sub

    Public Function StartDateTimeValid(AdHocBookingHumanResource As AdHocBookingHumanResource) As String

      If AdHocBookingHumanResource.GetParent() IsNot Nothing Then
        If (AdHocBookingHumanResource.EndDateTime IsNot Nothing AndAlso AdHocBookingHumanResource.StartDateTime IsNot Nothing) AndAlso (AdHocBookingHumanResource.GetParent().StartDateTime > AdHocBookingHumanResource.StartDateTime OrElse AdHocBookingHumanResource.GetParent().EndDateTime < AdHocBookingHumanResource.StartDateTime) Then
          Return "Start Time is not within the AdHoc Booking times"
        End If
      End If

      If AdHocBookingHumanResource.GetParent() IsNot Nothing Then
        If (AdHocBookingHumanResource.EndDateTime IsNot Nothing AndAlso AdHocBookingHumanResource.StartDateTime IsNot Nothing) AndAlso (AdHocBookingHumanResource.EndDateTime < AdHocBookingHumanResource.StartDateTime) Then
          Return "Start Time must be before End Time"
        End If
      End If

      Return ""

    End Function

    Public Function EndDateTimeValid(AdHocBookingHumanResource As AdHocBookingHumanResource) As String

      If AdHocBookingHumanResource.GetParent() IsNot Nothing Then
        If AdHocBookingHumanResource.GetParent().StartDateTime > AdHocBookingHumanResource.EndDateTime OrElse AdHocBookingHumanResource.GetParent().EndDateTime < AdHocBookingHumanResource.EndDateTime Then
          Return "AdHoc Booking Human Resource End Date is not within the AdHoc Booking dates"
        End If
      End If
      Return ""

    End Function

    Public Function TeamIDValid(AdHocBookingHumanResource As AdHocBookingHumanResource) As String

      If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ICR, Integer) Then
        If AdHocBookingHumanResource.SystemTeamID Is Nothing Then
          Return "Team is required"
        End If
      End If

      Return ""

    End Function

    'Private Shared Function AdHocBookingHumanResourceExistsInParentList(AdHocBookingHumanResource As AdHocBookingHumanResource)
    '  For Each AdHocBookingHumanResourceNew As AdHocBookingHumanResource In AdHocBookingHumanResource.GetParent().AdHocBookingHumanResourceList
    '    If (AdHocBookingHumanResource.HumanResourceID = AdHocBookingHumanResourceNew.HumanResourceID AndAlso
    '        AdHocBookingHumanResourceNew.StartDateTime = AdHocBookingHumanResource.StartDateTime AndAlso
    '        AdHocBookingHumanResource.EndDateTime = AdHocBookingHumanResourceNew.EndDateTime AndAlso
    '        AdHocBookingHumanResourceNew.Guid <> AdHocBookingHumanResource.Guid) Then
    '      Return "That Booking Human Resource is already booked"
    '    End If
    '  Next
    '  Return ""
    'End Function

    'Public Shared Sub CheckClashingAdHocBookingHumanResource(AdHocBookingHumanResource As AdHocBookingHumanResource)

    '  If AdHocBookingHumanResource.GetParent() IsNot Nothing AndAlso AdHocBookingHumanResource.GetParent().CheckRulesInd Then
    '    If AdHocBookingHumanResource.HumanResourceID IsNot Nothing _
    '       AndAlso AdHocBookingHumanResource.StartDateTime IsNot Nothing _
    '       AndAlso AdHocBookingHumanResource.EndDateTime IsNot Nothing Then
    '      AdHocBookingHumanResource.AdHocBookingHumanResourceClashes = AdHocBookingHumanResourceExistsInParentList(AdHocBookingHumanResource)
    '      If AdHocBookingHumanResource.AdHocBookingHumanResourceClashes <> "" Then
    '        Exit Sub
    '      End If
    '      Dim cProc As New Singular.CommandProc("[cmdProcs].[cmdAdHocBookingHumanResourceClashes]")
    '      cProc.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(AdHocBookingHumanResource.AdHocBookingID))
    '      cProc.Parameters.AddWithValue("@CrewScheduleDetailID", NothingDBNull(AdHocBookingHumanResource.CrewScheduleDetailID))
    '      cProc.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(AdHocBookingHumanResource.HumanResourceID))
    '      cProc.Parameters.AddWithValue("@StartDateTime", NothingDBNull(AdHocBookingHumanResource.StartDateTime))
    '      cProc.Parameters.AddWithValue("@EndDateTime", NothingDBNull(AdHocBookingHumanResource.EndDateTime))
    '      cProc.CommandType = CommandType.StoredProcedure
    '      cProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
    '      cProc = cProc.Execute
    '      If cProc.DataObject <> "" Then
    '        AdHocBookingHumanResource.AdHocBookingHumanResourceClashes = cProc.DataObject
    '      Else
    '        AdHocBookingHumanResource.AdHocBookingHumanResourceClashes = ""
    '      End If

    '    End If
    '  End If
    'End Sub

    'Public Shared Function AdHocBookingHumanResourceClash(AdHocBookingHumanResource As AdHocBookingHumanResource) As String
    '  CheckClashingAdHocBookingHumanResource(AdHocBookingHumanResource)
    '  Return AdHocBookingHumanResource.AdHocBookingHumanResourceClashes
    'End Function

    Public Shared Function HasClashesValid(AdHocBookingHumanResource As AdHocBookingHumanResource) As String

      If AdHocBookingHumanResource.ClashList.Count > 0 Then
        Return AdHocBookingHumanResource.HumanResource & " has " & AdHocBookingHumanResource.ClashList.Count.ToString & " clashes with this booking"
      End If
      Return ""

    End Function

    Public Shared Function AttendeeAlreadyAdded(AdHocBookingHumanResource As AdHocBookingHumanResource) As String

      Dim AdHocBooking As AdHocBooking = AdHocBookingHumanResource.GetParent
      If AdHocBooking IsNot Nothing Then
        Dim OtherBookings As List(Of AdHocBookingHumanResource) = AdHocBooking.AdHocBookingHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, AdHocBookingHumanResource.HumanResourceID) _
                                                                                                                   AndAlso Not CompareSafe(d.Guid, AdHocBookingHumanResource.Guid)).ToList
        If OtherBookings.Count > 0 Then
          Return AdHocBookingHumanResource.HumanResource & " is already added to this booking"
        End If
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdHocBookingHumanResource() method.

    End Sub

    Public Shared Function NewAdHocBookingHumanResource() As AdHocBookingHumanResource

      Return DataPortal.CreateChild(Of AdHocBookingHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAdHocBookingHumanResource(dr As SafeDataReader) As AdHocBookingHumanResource

      Dim a As New AdHocBookingHumanResource()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewScheduleDetailIDProperty, .GetInt32(0))
          LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(StartDateTimeProperty, .GetValue(3))
          LoadProperty(EndDateTimeProperty, .GetValue(4))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(SystemTeamIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(HumanResourceProperty, .GetString(9))
          LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(ResourceBookingIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(ResourceBookingTypeIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(13))
          LoadProperty(IsResourceBookingCancelledProperty, .GetBoolean(14))
          LoadProperty(IsResourceBookingCancelledReasonProperty, .GetString(15))
          LoadProperty(ResourceBookingStatusCssClassProperty, .GetString(16))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAdHocBookingHumanResource"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAdHocBookingHumanResource"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewScheduleDetailID As SqlParameter = .Parameters.Add("@CrewScheduleDetailID", SqlDbType.Int)
          Dim paramHumanResourceShiftID As SqlParameter = .Parameters.Add("@HumanResourceShiftID", SqlDbType.Int)
          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramCrewScheduleDetailID.Value = GetProperty(CrewScheduleDetailIDProperty)
          paramHumanResourceShiftID.Value = NothingDBNull(GetProperty(HumanResourceShiftIDProperty))
          paramResourceBookingID.Value = NothingDBNull(GetProperty(ResourceBookingIDProperty))
          If Me.IsNew Then
            paramCrewScheduleDetailID.Direction = ParameterDirection.InputOutput
            paramHumanResourceShiftID.Direction = ParameterDirection.InputOutput
            paramResourceBookingID.Direction = ParameterDirection.InputOutput
          End If
          .Parameters.AddWithValue("@AdHocBookingID", GetParent.AdHocBookingID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@StartDateTime", GetProperty(StartDateTimeProperty))
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetParent.SystemID))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetParent.ProductionAreaID))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemTeamID", NothingDBNull(GetProperty(SystemTeamIDProperty)))
          .Parameters.AddWithValue("@MealReimbursementInd", GetProperty(MealReimbursementIndProperty))
          .Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
          .Parameters.AddWithValue("@AdHocBookingTypeID", NothingDBNull(GetParent.AdHocBookingTypeID))
          .Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
          .Parameters.AddWithValue("@IsResourceBookingCancelled", GetProperty(IsResourceBookingCancelledProperty))
          .Parameters.AddWithValue("@IsResourceBookingCancelledReason", GetProperty(IsResourceBookingCancelledReasonProperty))
          .Parameters.AddWithValue("@ResourceBookingStatusCssClass", GetProperty(ResourceBookingStatusCssClassProperty))
          '.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(GetProperty(ResourceBookingIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewScheduleDetailIDProperty, paramCrewScheduleDetailID.Value)
            If IsDBNull(paramHumanResourceShiftID.Value) Then
              LoadProperty(HumanResourceShiftIDProperty, Nothing)
            Else
              LoadProperty(HumanResourceShiftIDProperty, paramHumanResourceShiftID.Value)
            End If
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAdHocBookingHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(GetProperty(ResourceBookingIDProperty)))
        cm.Parameters.AddWithValue("@CrewScheduleDetailID", NothingDBNull(GetProperty(CrewScheduleDetailIDProperty)))
        cm.Parameters.AddWithValue("@HumanResourceShiftID", NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
        cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Public Function GetResourceBooking() As Helpers.ResourceHelpers.ResourceBooking
      Return New Helpers.ResourceHelpers.ResourceBooking(Me.Guid, Me.ResourceID, Me.ResourceBookingID, Nothing, Me.StartDateTime, Me.EndDateTime, Nothing,
                                                                Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    End Function

    Public Sub CheckClash()

      Dim rb As Helpers.ResourceHelpers.ResourceBooking = ResourceBooking
      Me.ClashList.Clear()
      Helpers.ResourceHelpers.GetSingleResourceClash(rb)
      rb.ClashesWith.ForEach(Sub(cls)
                               Me.ClashList.Add(cls)
                             End Sub)

    End Sub

  End Class

End Namespace