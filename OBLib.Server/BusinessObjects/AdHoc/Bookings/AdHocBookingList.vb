﻿' Generated 04 May 2016 11:45 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AdHoc

  <Serializable()> _
  Public Class AdHocBookingList
    Inherits OBBusinessListBase(Of AdHocBookingList, AdHocBooking)

#Region " Business Methods "

    Public Function GetItem(AdHocBookingID As Integer) As AdHocBooking

      For Each child As AdHocBooking In Me
        If child.AdHocBookingID = AdHocBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Ad Hoc Bookings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property AdHocBookingID As Integer?
      Public Property ProductionSystemAreaID As Integer?

      Public Sub New(AdHocBookingID As Integer?, ProductionSystemAreaID As Integer?)
        Me.AdHocBookingID = AdHocBookingID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewAdHocBookingList() As AdHocBookingList

      Return New AdHocBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetAdHocBookingList() As AdHocBookingList

      Return DataPortal.Fetch(Of AdHocBookingList)(New Criteria())

    End Function

    Public Shared Function GetAdHocBookingList(AdHocBookingID As Integer?, ProductionSystemAreaID As Integer?) As AdHocBookingList

      Return DataPortal.Fetch(Of AdHocBookingList)(New Criteria(AdHocBookingID, ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AdHocBooking.GetAdHocBooking(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAdHocBookingList"
            cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(crit.AdHocBookingID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace