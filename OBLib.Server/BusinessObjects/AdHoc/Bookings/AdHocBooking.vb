﻿' Generated 04 May 2016 11:45 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib.Maintenance.AdHoc.ReadOnly

Namespace AdHoc

  <Serializable()> _
  Public Class AdHocBooking
    Inherits OBBusinessBase(Of AdHocBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return AdHocBookingBO.AdHocBookingBOToString(self)")

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingTypeID, "Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Booking Type", Description:=""),
    Required(ErrorMessage:="Booking Type required"),
    DropDownWeb(GetType(ROSystemAreaAdHocBookingTypeList),
                 BeforeFetchJS:="AdHocBookingBO.setAdHocBookingTypeIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="AdHocBookingBO.triggerAdHocBookingTypeIDAutoPopulate",
                 AfterFetchJS:="AdHocBookingBO.afterAdHocBookingTypeIDRefreshAjax",
                 OnItemSelectJSFunction:="AdHocBookingBO.onAdHocBookingTypeIDSelected",
                 LookupMember:="AdHocBookingType", DisplayMember:="AdHocBookingType", ValueMember:="AdHocBookingTypeID",
                 DropDownCssClass:="circuit-dropdown", DropDownColumns:={"AdHocBookingType", "DefaultGenRefNo"}),
    SetExpression("AdHocBookingBO.AdHocBookingTypeIDSet(self)")>
    Public Property AdHocBookingTypeID() As Integer?
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingType, "Booking Type", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Booking Type", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdHocBookingTypeProperty, Value)
      End Set
    End Property

    '    DropDownWeb(GetType(ROSystemAreaAdHocBookingTypeList), FilterMethodName:="AdHocBookingBO.ROAdHocBookingTypeOptions", DisplayMember:="DisplayName", ValueMember:="AdHocBookingTypeID"),

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    StringLength(50, ErrorMessage:="Title cannot be more than 50 characters"),
    Required(AllowEmptyStrings:=False),
    SetExpression("AdHocBookingBO.TitleSet(self)")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Booking Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Booking Description", Description:=""),
    StringLength(250, ErrorMessage:="Description cannot be more than 250 characters"),
    Required(AllowEmptyStrings:=False),
    SetExpression("AdHocBookingBO.DescriptionSet(self)")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    SetExpression("AdHocBookingBO.SystemIDSet(self)"),
    Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
    SetExpression("AdHocBookingBO.ProductionAreaIDSet(self)"),
    Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date/Time", Description:=""),
    SetExpression("AdHocBookingBO.StartDateTimeSet(self)"),
    Required(ErrorMessage:="Start Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="ddd DD MMM YYYY HH:mm", ViewMode:="days")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date/Time", Description:=""),
    SetExpression("AdHocBookingBO.EndDateTimeSet(self)"),
    Required(ErrorMessage:="End Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="ddd DD MMM YYYY HH:mm", ViewMode:="days")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LocationID, "Location", Nothing)
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property LocationID() As Integer?
      Get
        Return GetProperty(LocationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LocationIDProperty, Value)
      End Set
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostCentreID, "Cost Centre", Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Company.ReadOnly.ROCostCentreList), DisplayMember:="CostCentre", ValueMember:="CostCentreID")>
    Public Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostCentreIDProperty, Value)
      End Set
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="Country where the Booking is to take place"),
    DropDownWeb(GetType(ROCountryList))>
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref", Nothing)
    ''' <summary>
    ''' Gets and sets the Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="Gen Ref No of a linked Production, will need to import from Synergy if valid Gen Ref No entered"),
    Required(ErrorMessage:="Gen Ref is required")>
    Public Property GenRefNo() As Int64?
      Get
        Return GetProperty(GenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(GenRefNoProperty, Value)
      End Set
    End Property

    Public Shared RequiresTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTravel, "Requires Travel", False)
    ''' <summary>
    ''' Gets and sets the Requires Travel value
    ''' </summary>
    <Display(Name:="Requires Travel", Description:="Does Booking Require Travel?"),
    Required(ErrorMessage:="Requires Travel required")>
    Public Property RequiresTravel() As Boolean
      Get
        Return GetProperty(RequiresTravelProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiresTravelProperty, Value)
      End Set
    End Property

    Public Shared UsingDefaultGenRefReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UsingDefaultGenRefReason, "Using Default Gen Ref Reason", "")
    ''' <summary>
    ''' Gets and sets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Using Default Gen Ref Reason", Description:="Reason for using a default Gen Ref for Production Type Travel"),
    StringLength(100, ErrorMessage:="Using Default Gen Ref Reason cannot be more than 100 characters")>
    Public Property UsingDefaultGenRefReason() As String
      Get
        Return GetProperty(UsingDefaultGenRefReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(UsingDefaultGenRefReasonProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="ddd DD MMM YYYY HH:mm", ViewMode:="days")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="ddd DD MMM YYYY HH:mm", ViewMode:="days")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared CreatedTravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreatedTravelRequisitionID, "CreatedTravelRequisitionID", Nothing)
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="CreatedTravelRequisitionID", Description:="")>
    Public Property CreatedTravelRequisitionID() As Integer?
      Get
        Return GetProperty(CreatedTravelRequisitionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CreatedTravelRequisitionIDProperty, Value)
      End Set
    End Property

    Public Shared UseDefaultGenRefProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UseDefaultGenRef, "UseDefaultGenRef", False)
    ''' <summary>
    ''' Gets and sets the Requires Travel value
    ''' </summary>
    <Display(Name:="UseDefaultGenRef")>
    Public Property UseDefaultGenRef() As Boolean
      Get
        Return GetProperty(UseDefaultGenRefProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UseDefaultGenRefProperty, Value)
      End Set
    End Property

    Public Shared ShowFindSynergyEventProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShowFindSynergyEvent, "ShowFindSynergyEvent", False)
    ''' <summary>
    ''' Gets and sets the Requires Travel value
    ''' </summary>
    <Display(Name:="ShowFindSynergyEvent")>
    Public Property ShowFindSynergyEvent() As Boolean
      Get
        Return GetProperty(ShowFindSynergyEventProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShowFindSynergyEventProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="ProductionSystemAreaID", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Non-Database Properties "

    Public Shared IsOwningAreaProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOwningArea, "IsOwningArea?", True)
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="IsOwningArea?", Description:="")>
    Public Overridable Property IsOwningArea() As Boolean
      Get
        Return GetProperty(IsOwningAreaProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsOwningAreaProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdHocBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Ad Hoc Booking")
        Else
          Return String.Format("Blank {0}", "Ad Hoc Booking")
        End If
      Else
        Return Me.Title
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(GenRefNoProperty)
        .AddTriggerProperty(UsingDefaultGenRefReasonProperty)
        .AffectedProperties.Add(UsingDefaultGenRefReasonProperty)
        .JavascriptRuleFunctionName = "AdHocBookingBO.GenRefNoValid"
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdHocBooking() method.

    End Sub

    Public Shared Function NewAdHocBooking() As AdHocBooking

      Return DataPortal.CreateChild(Of AdHocBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetAdHocBooking(dr As SafeDataReader) As AdHocBooking

      Dim a As New AdHocBooking()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AdHocBookingIDProperty, .GetInt32(0))
          LoadProperty(AdHocBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TitleProperty, .GetString(2))
          LoadProperty(DescriptionProperty, .GetString(3))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(StartDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeProperty, .GetValue(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(LocationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(GenRefNoProperty, .GetInt64(15))
          LoadProperty(RequiresTravelProperty, .GetBoolean(16))
          LoadProperty(UsingDefaultGenRefReasonProperty, .GetString(17))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(18))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(19))
          LoadProperty(IsOwningAreaProperty, .GetBoolean(20))
          LoadProperty(AdHocBookingTypeProperty, .GetString(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, AdHocBookingIDProperty)
      AddInputOutputParam(cm, CreatedTravelRequisitionIDProperty)
      AddInputOutputParam(cm, ProductionSystemAreaIDProperty)

      cm.Parameters.AddWithValue("@AdHocBookingTypeID", GetProperty(AdHocBookingTypeIDProperty))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
      cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@StartDateTime", Singular.Misc.NothingDBNull(StartDateTime))
      cm.Parameters.AddWithValue("@EndDateTime", Singular.Misc.NothingDBNull(EndDateTime))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@LocationID", Singular.Misc.NothingDBNull(GetProperty(LocationIDProperty)))
      cm.Parameters.AddWithValue("@CostCentreID", Singular.Misc.NothingDBNull(GetProperty(CostCentreIDProperty)))
      cm.Parameters.AddWithValue("@CountryID", Singular.Misc.NothingDBNull(GetProperty(CountryIDProperty)))
      cm.Parameters.AddWithValue("@GenRefNo", GetProperty(GenRefNoProperty))
      cm.Parameters.AddWithValue("@RequiresTravel", GetProperty(RequiresTravelProperty))
      cm.Parameters.AddWithValue("@UsingDefaultGenRefReason", GetProperty(UsingDefaultGenRefReasonProperty))
      cm.Parameters.AddWithValue("@EndDateTimeBuffer", Singular.Misc.NothingDBNull(EndDateTimeBuffer))
      cm.Parameters.AddWithValue("@StartDateTimeBuffer", Singular.Misc.NothingDBNull(StartDateTimeBuffer))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(AdHocBookingIDProperty, cm.Parameters("@AdHocBookingID").Value)
                 LoadProperty(ProductionSystemAreaIDProperty, cm.Parameters("@ProductionSystemAreaID").Value)
                 If Me.RequiresTravel Then
                   LoadProperty(CreatedTravelRequisitionIDProperty, Singular.Misc.ZeroNothing(cm.Parameters("@CreatedTravelRequisitionID").Value))
                 End If
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@AdHocBookingID", GetProperty(AdHocBookingIDProperty))
    End Sub

#End Region

  End Class

End Namespace