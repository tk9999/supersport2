﻿' Generated 11 Aug 2016 13:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Playout

  <Serializable()> _
  Public Class LiveEventHRCard
    Inherits OBBusinessBase(Of LiveEventHRCard)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return LiveEventHRCardBO.LiveEventHRCardBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared PictureNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PictureName, "Picture Name")
    ''' <summary>
    ''' Gets the Picture Name value
    ''' </summary>
    <Display(Name:="Picture Name", Description:="")>
    Public ReadOnly Property PictureName() As String
      Get
        Return GetProperty(PictureNameProperty)
      End Get
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:="")>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public ReadOnly Property CallTime As Date
      Get
        Return GetProperty(CallTimeProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property StartDateTime As Date
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public ReadOnly Property WrapTime As Date
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SiteID, "Site")
    ''' <summary>
    ''' Gets the Site value
    ''' </summary>
    <Display(Name:="Site", Description:="")>
    Public ReadOnly Property SiteID() As Integer
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared SiteNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SiteName, "Site Name")
    ''' <summary>
    ''' Gets the Site Name value
    ''' </summary>
    <Display(Name:="Site Name", Description:="")>
    Public ReadOnly Property SiteName() As String
      Get
        Return GetProperty(SiteNameProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared HasArrivedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasArrived, "Has Arrived", False)
    ''' <summary>
    ''' Gets the Has Arrived value
    ''' </summary>
    <Display(Name:="Has Arrived", Description:="")>
    Public ReadOnly Property HasArrived() As Boolean
      Get
        Return GetProperty(HasArrivedProperty)
      End Get
    End Property

    Public Shared ArrivedDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ArrivedDateTime, "Arrived Date Time")
    ''' <summary>
    ''' Gets the Arrived Date Time value
    ''' </summary>
    <Display(Name:="Arrived Date Time", Description:="")>
    Public ReadOnly Property ArrivedDateTime As Date
      Get
        Return GetProperty(ArrivedDateTimeProperty)
      End Get
    End Property

    Public Shared AccessShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessShiftID, "Access Shift")
    ''' <summary>
    ''' Gets the Access Shift value
    ''' </summary>
    <Display(Name:="Access Shift", Description:="")>
    Public ReadOnly Property AccessShiftID() As Integer
      Get
        Return GetProperty(AccessShiftIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomScheduleIDProperty, value)
      End Set
    End Property

    Public Shared ProductionAssistantProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAssistant, "Production Assistant")
    ''' <summary>
    ''' Gets the ProductionAssistant value
    ''' </summary>
    <Display(Name:="Production Assistant", Description:="")>
    Public ReadOnly Property ProductionAssistant() As String
      Get
        Return GetProperty(ProductionAssistantProperty)
      End Get
    End Property

    Public Shared ProducerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Producer, "Producer")
    ''' <summary>
    ''' Gets the Producer value
    ''' </summary>
    <Display(Name:="Producer", Description:="")>
    Public ReadOnly Property Producer() As String
      Get
        Return GetProperty(ProducerProperty)
      End Get
    End Property

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessFlagID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property AccessFlagID() As Integer?
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(AccessFlagIDProperty, value)
      End Set
    End Property

    Public Shared FlagDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlagDescription, "FlagDescription")
    ''' <summary>
    ''' Gets the Producer value
    ''' </summary>
    <Display(Name:="FlagDescription", Description:="")>
    Public Property FlagDescription() As String
      Get
        Return GetProperty(FlagDescriptionProperty)
      End Get
      Set(value As String)
        SetProperty(FlagDescriptionProperty, value)
      End Set
    End Property

    Public Shared IsShiftProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsShift, "IsShift", False)
    ''' <summary>
    ''' Gets the Has Arrived value
    ''' </summary>
    <Display(Name:="IsShift", Description:="")>
    Public Property IsShift() As Boolean
      Get
        Return GetProperty(IsShiftProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsShiftProperty, value)
      End Set
    End Property

    Public Shared CallTimePassedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CallTimePassed, "CallTimePassed", False)
    ''' <summary>
    ''' Gets the Has Arrived value
    ''' </summary>
    <Display(Name:="CallTimePassed", Description:="")>
    Public Property CallTimePassed() As Boolean
      Get
        Return GetProperty(CallTimePassedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(CallTimePassedProperty, value)
      End Set
    End Property

    Public Shared IsLeaveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLeave, "IsLeave", False)
    ''' <summary>
    ''' Gets the Has Arrived value
    ''' </summary>
    <Display(Name:="IsLeave", Description:="")>
    Public Property IsLeave() As Boolean
      Get
        Return GetProperty(IsLeaveProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsLeaveProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared LiveEventHRCardDisciplineListProperty As PropertyInfo(Of LiveEventHRCardDisciplineList) = RegisterProperty(Of LiveEventHRCardDisciplineList)(Function(c) c.LiveEventHRCardDisciplineList, "Live Event HR Card Discipline List")

    Public ReadOnly Property LiveEventHRCardDisciplineList() As LiveEventHRCardDisciplineList
      Get
        If GetProperty(LiveEventHRCardDisciplineListProperty) Is Nothing Then
          LoadProperty(LiveEventHRCardDisciplineListProperty, Playout.LiveEventHRCardDisciplineList.NewLiveEventHRCardDisciplineList())
        End If
        Return GetProperty(LiveEventHRCardDisciplineListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Firstname.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Live Event HR Card")
        Else
          Return String.Format("Blank {0}", "Live Event HR Card")
        End If
      Else
        Return Me.Firstname
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewLiveEventHRCard() method.

    End Sub

    Public Shared Function NewLiveEventHRCard() As LiveEventHRCard

      Return DataPortal.CreateChild(Of LiveEventHRCard)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetLiveEventHRCard(dr As SafeDataReader) As LiveEventHRCard

      Dim l As New LiveEventHRCard()
      l.Fetch(dr)
      Return l

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(FirstnameProperty, .GetString(1))
          LoadProperty(SurnameProperty, .GetString(2))
          LoadProperty(PreferredNameProperty, .GetString(3))
          LoadProperty(HRNameProperty, .GetString(4))
          LoadProperty(CellPhoneNumberProperty, .GetString(5))
          LoadProperty(AlternativeContactNumberProperty, .GetString(6))
          LoadProperty(PictureNameProperty, .GetString(7))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CallTimeProperty, .GetValue(9))
          LoadProperty(StartDateTimeProperty, .GetValue(10))
          LoadProperty(EndDateTimeProperty, .GetValue(11))
          LoadProperty(WrapTimeProperty, .GetValue(12))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(RoomProperty, .GetString(14))
          LoadProperty(SiteIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(SiteNameProperty, .GetString(16))
          LoadProperty(TitleProperty, .GetString(17))
          LoadProperty(HasArrivedProperty, .GetBoolean(18))
          LoadProperty(ArrivedDateTimeProperty, .GetValue(19))
          LoadProperty(AccessShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(ProductionAssistantProperty, .GetString(22))
          LoadProperty(ProducerProperty, .GetString(23))
          LoadProperty(AccessFlagIDProperty, ZeroNothing(.GetInt32(24)))
          LoadProperty(FlagDescriptionProperty, .GetString(25))
          LoadProperty(IsShiftProperty, .GetBoolean(26))
          'GroupORder = 27
          LoadProperty(CallTimePassedProperty, .GetBoolean(28))
          LoadProperty(IsLeaveProperty, .GetBoolean(29))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceIDProperty)

      cm.Parameters.AddWithValue("@Firstname", GetProperty(FirstnameProperty))
      cm.Parameters.AddWithValue("@Surname", GetProperty(SurnameProperty))
      cm.Parameters.AddWithValue("@PreferredName", GetProperty(PreferredNameProperty))
      cm.Parameters.AddWithValue("@HRName", GetProperty(HRNameProperty))
      cm.Parameters.AddWithValue("@CellPhoneNumber", GetProperty(CellPhoneNumberProperty))
      cm.Parameters.AddWithValue("@AlternativeContactNumber", GetProperty(AlternativeContactNumberProperty))
      cm.Parameters.AddWithValue("@PictureName", GetProperty(PictureNameProperty))
      cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
      cm.Parameters.AddWithValue("@CallTime", CallTime)
      cm.Parameters.AddWithValue("@StartDateTime", StartDateTime)
      cm.Parameters.AddWithValue("@EndDateTime", EndDateTime)
      cm.Parameters.AddWithValue("@WrapTime", WrapTime)
      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      cm.Parameters.AddWithValue("@SiteID", GetProperty(SiteIDProperty))
      cm.Parameters.AddWithValue("@SiteName", GetProperty(SiteNameProperty))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@HasArrived", GetProperty(HasArrivedProperty))
      cm.Parameters.AddWithValue("@ArrivedDateTime", ArrivedDateTime)
      cm.Parameters.AddWithValue("@AccessShiftID", GetProperty(AccessShiftIDProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceIDProperty, cm.Parameters("@HumanResourceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(LiveEventHRCardDisciplineListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace