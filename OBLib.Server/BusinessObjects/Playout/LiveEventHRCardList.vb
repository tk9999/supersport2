﻿' Generated 11 Aug 2016 13:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Playout

  <Serializable()> _
  Public Class LiveEventHRCardList
    Inherits OBBusinessListBase(Of LiveEventHRCardList, LiveEventHRCard)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As LiveEventHRCard

      For Each child As LiveEventHRCard In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Live Events HR Cards"

    End Function

    Public Function GetLiveEventHRCardDiscipline(RoomScheduleID As Integer) As LiveEventHRCardDiscipline

      Dim obj As LiveEventHRCardDiscipline = Nothing
      For Each parent As LiveEventHRCard In Me
        obj = parent.LiveEventHRCardDisciplineList.GetItem(RoomScheduleID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, Now)
      ''' <summary>
      ''' Gets and sets the Start Date Time value
      ''' </summary>
      <Display(Name:="Start Date"),
      SetExpression("LiveEventHRCardCriteriaBO.StartDateSet(self)", False)>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, Now)
      ''' <summary>
      ''' Gets and sets the End Date Time value
      ''' </summary>
      <Display(Name:="End Date"),
      SetExpression("LiveEventHRCardCriteriaBO.EndDateSet(self)", False)>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared SystemIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterProperty(Of List(Of Integer))(Function(c) c.SystemIDs, "", New List(Of Integer))
      ''' <summary>
      ''' Gets and sets the End Date Time value
      ''' </summary>
      Public Property SystemIDs As List(Of Integer)
        Get
          Return ReadProperty(SystemIDsProperty)
        End Get
        Set(ByVal Value As List(Of Integer))
          LoadProperty(SystemIDsProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterProperty(Of List(Of Integer))(Function(c) c.ProductionAreaIDs, "", New List(Of Integer))
      ''' <summary>
      ''' Gets and sets the End Date Time value
      ''' </summary>
      Public Property ProductionAreaIDs As List(Of Integer)
        Get
          Return ReadProperty(ProductionAreaIDsProperty)
        End Get
        Set(ByVal Value As List(Of Integer))
          LoadProperty(ProductionAreaIDsProperty, Value)
        End Set
      End Property

      Public Shared LastRefreshTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LastRefreshTime, "", "")
      ''' <summary>
      ''' Gets and sets the End Date Time value
      ''' </summary>
      Public Property LastRefreshTime As String
        Get
          Return ReadProperty(LastRefreshTimeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(LastRefreshTimeProperty, Value)
        End Set
      End Property


      Public Shared EventBasedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EventBasedInd, "Event Based", True)
      ''' <summary>
      ''' Gets and sets the Event Based Ind value
      ''' </summary>      
      <SetExpression("LiveEventHRCardCriteriaBO.EventBasedIndSet(self)")>
      Public Property EventBasedInd() As Boolean
        Get
          Return ReadProperty(EventBasedIndProperty)
        End Get
        Set(value As Boolean)
          LoadProperty(EventBasedIndProperty, value)
        End Set
      End Property

      Public Shared ShiftBasedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShiftBasedInd, "Shift Based", True)
      ''' <summary>
      ''' Gets and sets the Shift Based Ind value
      ''' </summary>      
      <SetExpression("LiveEventHRCardCriteriaBO.ShiftBasedIndSet(self)")>
      Public Property ShiftBasedInd() As Boolean
        Get
          Return ReadProperty(ShiftBasedIndProperty)
        End Get
        Set(value As Boolean)
          LoadProperty(ShiftBasedIndProperty, value)
        End Set
      End Property

      Public Shared LeaveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LeaveInd, "Leave", True)
      ''' <summary>
      ''' Gets and sets the Leave Ind value
      ''' </summary>
      <SetExpression("LiveEventHRCardCriteriaBO.LeaveIndSet(self)")>
      Public Property LeaveInd() As Boolean
        Get
          Return ReadProperty(LeaveIndProperty)
        End Get
        Set(value As Boolean)
          LoadProperty(LeaveIndProperty, value)
        End Set
      End Property

      Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room")
      ''' <summary>
      ''' Gets and sets the Room value
      ''' </summary>
      <Display(Name:="Room", Description:=""),
      Required(ErrorMessage:="Room required"),
      DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="LiveEventHRCardCriteriaBO.setRoomCriteriaBeforeRefresh",
                  PreFindJSFunction:="LiveEventHRCardCriteriaBO.triggerRoomAutoPopulate",
                  AfterFetchJS:="LiveEventHRCardCriteriaBO.afterRoomRefreshAjax",
                  LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                  OnItemSelectJSFunction:="LiveEventHRCardCriteriaBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
      SetExpression("LiveEventHRCardCriteriaBO.RoomIDSet(self)")>
      Public Property RoomID() As Integer?
        Get
          Return ReadProperty(RoomIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(RoomIDProperty, Value)
        End Set
      End Property

      Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
      ''' <summary>
      ''' Gets and sets the Debtor value
      ''' </summary>
      <Display(Name:="Room", Description:="")>
      Public Property Room() As String
        Get
          Return ReadProperty(RoomProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(RoomProperty, Value)
        End Set
      End Property

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewLiveEventHRCardList() As LiveEventHRCardList

      Return New LiveEventHRCardList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetLiveEventHRCardList() As LiveEventHRCardList

      Return DataPortal.Fetch(Of LiveEventHRCardList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(LiveEventHRCard.GetLiveEventHRCard(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As LiveEventHRCard = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomScheduleID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.LiveEventHRCardDisciplineList.RaiseListChangedEvents = False
          parent.LiveEventHRCardDisciplineList.Add(LiveEventHRCardDiscipline.GetLiveEventHRCardDiscipline(sdr))
          parent.LiveEventHRCardDisciplineList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As LiveEventHRCard In Me
        child.CheckRules()
        For Each LiveEventHRCardDiscipline As LiveEventHRCardDiscipline In child.LiveEventHRCardDisciplineList
          LiveEventHRCardDiscipline.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getLiveEventHRCardList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            cm.Parameters.AddWithValue("@RoomID", NothingDBNull(crit.RoomID))
            cm.Parameters.AddWithValue("@EventBasedInd", crit.EventBasedInd)
            cm.Parameters.AddWithValue("@ShiftBasedInd", crit.ShiftBasedInd)
            cm.Parameters.AddWithValue("@LeaveInd", crit.LeaveInd)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace