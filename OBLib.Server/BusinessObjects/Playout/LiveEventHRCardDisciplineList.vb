﻿' Generated 11 Aug 2016 13:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Playout

  <Serializable()> _
  Public Class LiveEventHRCardDisciplineList
    Inherits OBBusinessListBase(Of LiveEventHRCardDisciplineList, LiveEventHRCardDiscipline)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As LiveEventHRCardDiscipline

      For Each child As LiveEventHRCardDiscipline In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewLiveEventHRCardDisciplineList() As LiveEventHRCardDisciplineList

      Return New LiveEventHRCardDisciplineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace