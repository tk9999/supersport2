﻿' Generated 11 Aug 2016 13:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Playout

  <Serializable()> _
  Public Class LiveEventHRCardDiscipline
    Inherits OBBusinessBase(Of LiveEventHRCardDiscipline)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return LiveEventHRCardDisciplineBO.LiveEventHRCardDisciplineBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared FromSystemAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromSystemArea, "From System Area")
    ''' <summary>
    ''' Gets the From System Area value
    ''' </summary>
    <Display(Name:="From System Area", Description:="")>
    Public ReadOnly Property FromSystemArea() As String
      Get
        Return GetProperty(FromSystemAreaProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address")
    ''' <summary>
    ''' Gets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alternative Email Address", Description:="")>
    Public ReadOnly Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As LiveEventHRCard

      Return CType(CType(Me.Parent, LiveEventHRCardDisciplineList).Parent, LiveEventHRCard)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Discipline.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Live Event HR Card Discipline")
        Else
          Return String.Format("Blank {0}", "Live Event HR Card Discipline")
        End If
      Else
        Return Me.Discipline
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewLiveEventHRCardDiscipline() method.

    End Sub

    Public Shared Function NewLiveEventHRCardDiscipline() As LiveEventHRCardDiscipline

      Return DataPortal.CreateChild(Of LiveEventHRCardDiscipline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetLiveEventHRCardDiscipline(dr As SafeDataReader) As LiveEventHRCardDiscipline

      Dim l As New LiveEventHRCardDiscipline()
      l.Fetch(dr)
      Return l

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineProperty, .GetString(2))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(HRNameProperty, .GetString(4))
          LoadProperty(SystemProperty, .GetString(5))
          LoadProperty(ProductionAreaProperty, .GetString(6))
          LoadProperty(FromSystemAreaProperty, .GetString(7))
          LoadProperty(CellPhoneNumberProperty, .GetString(8))
          LoadProperty(AlternativeContactNumberProperty, .GetString(9))
          LoadProperty(EmailAddressProperty, .GetString(10))
          LoadProperty(AlternativeEmailAddressProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, RoomScheduleIDProperty)

      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      cm.Parameters.AddWithValue("@HumanResourceID", Me.GetParent().HumanResourceID)
      cm.Parameters.AddWithValue("@HRName", GetProperty(HRNameProperty))
      cm.Parameters.AddWithValue("@System", GetProperty(SystemProperty))
      cm.Parameters.AddWithValue("@ProductionArea", GetProperty(ProductionAreaProperty))
      cm.Parameters.AddWithValue("@FromSystemArea", GetProperty(FromSystemAreaProperty))
      cm.Parameters.AddWithValue("@CellPhoneNumber", GetProperty(CellPhoneNumberProperty))
      cm.Parameters.AddWithValue("@AlternativeContactNumber", GetProperty(AlternativeContactNumberProperty))
      cm.Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
      cm.Parameters.AddWithValue("@AlternativeEmailAddress", GetProperty(AlternativeEmailAddressProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(RoomScheduleIDProperty, cm.Parameters("@RoomScheduleID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
    End Sub

#End Region

  End Class

End Namespace