﻿' Generated 08 Jun 2015 18:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class RONotificationGroupUserList
    Inherits SingularReadOnlyListBase(Of RONotificationGroupUserList, RONotificationGroupUser)

#Region " Parent "

    <NotUndoable()> Private mParent As RONotificationGroup
#End Region

#Region " Business Methods "

    Public Function GetItem(NotificationGroupUserID As Integer) As RONotificationGroupUser

      For Each child As RONotificationGroupUser In Me
        If child.NotificationGroupUserID = NotificationGroupUserID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Notification Group Users"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewRONotificationGroupUserList() As RONotificationGroupUserList

      Return New RONotificationGroupUserList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace