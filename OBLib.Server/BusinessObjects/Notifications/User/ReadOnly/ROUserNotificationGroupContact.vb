﻿' Generated 16 Sep 2015 09:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class ROUserNotificationGroupContact
    Inherits OBReadOnlyBase(Of ROUserNotificationGroupContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NotificationGroupContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupContactID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key,
    Required(ErrorMessage:="ID required")>
    Public ReadOnly Property NotificationGroupContactID() As Integer
      Get
        Return GetProperty(NotificationGroupContactIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupID, "Notification Group")
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Notification Group", Description:=""),
    Required(ErrorMessage:="Notification Group required")>
    Public ReadOnly Property NotificationGroupID() As Integer
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactName, "Contact Name")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="")>
    Public ReadOnly Property NotificationGroupContactName() As String
      Get
        Return GetProperty(NotificationGroupContactNameProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactNumber, "Contact Number")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:="")>
    Public ReadOnly Property NotificationGroupContactNumber() As String
      Get
        Return GetProperty(NotificationGroupContactNumberProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactEmailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactEmail, "Contact Email")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Email value
    ''' </summary>
    <Display(Name:="Contact Email", Description:="")>
    Public ReadOnly Property NotificationGroupContactEmail() As String
      Get
        Return GetProperty(NotificationGroupContactEmailProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NotificationGroupContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.NotificationGroupContactName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserNotificationGroupContact(dr As SafeDataReader) As ROUserNotificationGroupContact

      Dim u As New ROUserNotificationGroupContact()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(NotificationGroupContactIDProperty, .GetInt32(0))
        LoadProperty(NotificationGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(NotificationGroupContactNameProperty, .GetString(2))
        LoadProperty(NotificationGroupContactNumberProperty, .GetString(3))
        LoadProperty(NotificationGroupContactEmailProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace