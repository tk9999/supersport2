﻿' Generated 11 Nov 2016 12:17 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUNSummaryNotificationList
    Inherits OBReadOnlyListBase(Of ROUNSummaryNotificationList, ROUNSummaryNotification)

#Region " Parent "

    <NotUndoable()> Private mParent As ROUNSummary
#End Region

#Region " Business Methods "

    Public Function GetItem(UserNotificationID As Integer) As ROUNSummaryNotification

      For Each child As ROUNSummaryNotification In Me
        If child.UserNotificationID = UserNotificationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROUNSummaryNotificationList() As ROUNSummaryNotificationList

      Return New ROUNSummaryNotificationList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace