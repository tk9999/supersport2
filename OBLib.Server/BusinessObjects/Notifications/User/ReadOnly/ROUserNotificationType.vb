﻿' Generated 02 Feb 2016 09:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class ROUserNotificationType
    Inherits OBReadOnlyBase(Of ROUserNotificationType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserNotificationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserNotificationTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property UserNotificationTypeID() As Integer
      Get
        Return GetProperty(UserNotificationTypeIDProperty)
      End Get
    End Property

    Public Shared UserNotificationTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UserNotificationType, "User Notification Type", "")
    ''' <summary>
    ''' Gets the User Notification Type value
    ''' </summary>
    <Display(Name:="User Notification Type", Description:="")>
  Public ReadOnly Property UserNotificationType() As String
      Get
        Return GetProperty(UserNotificationTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserNotificationTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserNotificationType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserNotificationType(dr As SafeDataReader) As ROUserNotificationType

      Dim r As New ROUserNotificationType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserNotificationTypeIDProperty, .GetInt32(0))
        LoadProperty(UserNotificationTypeProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace