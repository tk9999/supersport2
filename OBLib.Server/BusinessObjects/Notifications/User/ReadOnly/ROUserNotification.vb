﻿' Generated 02 Feb 2016 09:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUserNotification
    Inherits OBReadOnlyBase(Of ROUserNotification)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserNotificationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserNotificationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property UserNotificationID() As Integer
      Get
        Return GetProperty(UserNotificationIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public ReadOnly Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared NotificationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationID, "Notification", 0)
    ''' <summary>
    ''' Gets the User Notification Type value
    ''' </summary>
    <Display(Name:="Notification", Description:="")>
    Public ReadOnly Property NotificationID() As Integer
      Get
        Return GetProperty(NotificationIDProperty)
      End Get
    End Property

    Public Shared ReadDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ReadDateTime, Nothing)
    ''' <summary>
    ''' Gets the User Notification Type value
    ''' </summary>
    <Display(Name:="ReadDateTime", Description:="")>
    Public ReadOnly Property ReadDateTime() As DateTime?
      Get
        Return GetProperty(ReadDateTimeProperty)
      End Get
    End Property

    Public Shared NotificationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationTypeID, "Notification Type", 0)
    ''' <summary>
    ''' Gets the User Notification Type value
    ''' </summary>
    <Display(Name:="Notification Type", Description:="")>
    Public ReadOnly Property NotificationTypeID() As Integer
      Get
        Return GetProperty(NotificationTypeIDProperty)
      End Get
    End Property

    Public Shared NotificationTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationType, "Notification Type", "")
    ''' <summary>
    ''' Gets the Notification Title value
    ''' </summary>
    <Display(Name:="Notification Type", Description:="")>
    Public ReadOnly Property NotificationType() As String
      Get
        Return GetProperty(NotificationTypeProperty)
      End Get
    End Property

    Public Shared NotificationTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationTitle, "Notification Title", "")
    ''' <summary>
    ''' Gets the Notification Title value
    ''' </summary>
    <Display(Name:="Notification Title", Description:="")>
    Public ReadOnly Property NotificationTitle() As String
      Get
        Return GetProperty(NotificationTitleProperty)
      End Get
    End Property

    Public Shared NotificationTextShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationTextShort, "Notification Text Short", "")
    ''' <summary>
    ''' Gets the Notification Text Short value
    ''' </summary>
    <Display(Name:="Notification Text Short", Description:="")>
    Public ReadOnly Property NotificationTextShort() As String
      Get
        Return GetProperty(NotificationTextShortProperty)
      End Get
    End Property

    Public Shared NotificationTextLongProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationTextLong, "Notification Text Long", "")
    ''' <summary>
    ''' Gets the Notification Text Long value
    ''' </summary>
    <Display(Name:="Notification Text Long", Description:="")>
    Public ReadOnly Property NotificationTextLong() As String
      Get
        Return GetProperty(NotificationTextLongProperty)
      End Get
    End Property

    Public Shared ShowGritterProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShowGritter, "Show Gritter", False)
    ''' <summary>
    ''' Gets the Show Gritter value
    ''' </summary>
    <Display(Name:="Show Gritter", Description:="")>
    Public ReadOnly Property ShowGritter() As Boolean
      Get
        Return GetProperty(ShowGritterProperty)
      End Get
    End Property

    Public Shared GritterStickyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.GritterSticky, "Gritter Sticky", False)
    ''' <summary>
    ''' Gets the Gritter Sticky value
    ''' </summary>
    <Display(Name:="Gritter Sticky", Description:="")>
    Public ReadOnly Property GritterSticky() As Boolean
      Get
        Return GetProperty(GritterStickyProperty)
      End Get
    End Property

    Public Shared GritterClassNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GritterClassName, "Gritter Class Name", "clean")
    ''' <summary>
    ''' Gets the Gritter Class Name value
    ''' </summary>
    <Display(Name:="Gritter Class Name", Description:="")>
    Public ReadOnly Property GritterClassName() As String
      Get
        Return GetProperty(GritterClassNameProperty)
      End Get
    End Property

    Public Shared GritterFadeTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GritterFadeTime, "Gritter Fade Time", 1000)
    ''' <summary>
    ''' Gets the Gritter Fade Time value
    ''' </summary>
    <Display(Name:="Gritter Fade Time", Description:="")>
    Public ReadOnly Property GritterFadeTime() As Integer
      Get
        Return GetProperty(GritterFadeTimeProperty)
      End Get
    End Property

    Public Shared GritterPositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GritterPosition, "Gritter Position", "top-right")
    ''' <summary>
    ''' Gets the Gritter Position value
    ''' </summary>
    <Display(Name:="Gritter Position", Description:="")>
    Public ReadOnly Property GritterPosition() As String
      Get
        Return GetProperty(GritterPositionProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CreatedDateTime, Nothing)
    ''' <summary>
    ''' Gets the User Notification Type value
    ''' </summary>
    <Display(Name:="CreatedDateTime", Description:="")>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedDateTimeString, "Created Date Time", "")
    ''' <summary>
    ''' Gets the Gritter Position value
    ''' </summary>
    <Display(Name:="Created Date Time", Description:="")>
    Public ReadOnly Property CreatedDateTimeString() As String
      Get
        Return GetProperty(CreatedDateTimeStringProperty)
      End Get
    End Property

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SynergyImportID, "SynergyImportID", Nothing)
    ''' <summary>
    ''' Gets the Gritter Fade Time value
    ''' </summary>
    <Display(Name:="SynergyImportID", Description:="")>
    Public ReadOnly Property SynergyImportID() As Integer?
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
    End Property

    Public Shared HasFileProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasFile, "HasFile", False)
    ''' <summary>
    ''' Gets the Gritter Position value
    ''' </summary>
    <Display(Name:="HasFile", Description:="")>
    Public ReadOnly Property HasFile() As Boolean
      Get
        Return GetProperty(HasFileProperty)
      End Get
    End Property

    Public Shared FileTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FileType, "FileType", "")
    ''' <summary>
    ''' Gets the Gritter Position value
    ''' </summary>
    <Display(Name:="FileType", Description:="")>
    Public ReadOnly Property FileType() As String
      Get
        Return GetProperty(FileTypeProperty)
      End Get
    End Property

    Public Shared FileIconProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FileIcon, "FileIcon", "")
    ''' <summary>
    ''' Gets the Gritter Position value
    ''' </summary>
    <Display(Name:="FileIcon", Description:="")>
    Public ReadOnly Property FileIcon() As String
      Get
        Return GetProperty(FileIconProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserNotificationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.NotificationTitle

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserNotification(dr As SafeDataReader) As ROUserNotification

      Dim r As New ROUserNotification()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserNotificationIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(NotificationIDProperty, .GetInt32(2))
        LoadProperty(ReadDateTimeProperty, .GetValue(3))
        LoadProperty(NotificationTypeIDProperty, .GetInt32(4))
        LoadProperty(NotificationTypeProperty, .GetString(5))
        LoadProperty(NotificationTitleProperty, .GetString(6))
        LoadProperty(NotificationTextShortProperty, .GetString(7))
        LoadProperty(NotificationTextLongProperty, .GetString(8))
        LoadProperty(ShowGritterProperty, .GetBoolean(9))
        LoadProperty(GritterStickyProperty, .GetBoolean(10))
        LoadProperty(GritterClassNameProperty, .GetString(11))
        LoadProperty(GritterFadeTimeProperty, .GetInt32(12))
        LoadProperty(GritterPositionProperty, .GetString(13))
        LoadProperty(CreatedDateTimeProperty, .GetValue(14))
        LoadProperty(CreatedDateTimeStringProperty, .GetString(15))
        LoadProperty(SynergyImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        'Rnk = 17
        LoadProperty(HasFileProperty, .GetBoolean(18))
        LoadProperty(FileTypeProperty, .GetString(19))
        LoadProperty(FileIconProperty, .GetString(20))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace