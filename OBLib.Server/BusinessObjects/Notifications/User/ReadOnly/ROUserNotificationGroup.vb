﻿' Generated 16 Sep 2015 07:55 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class ROUserNotificationGroup
    Inherits OBReadOnlyBase(Of ROUserNotificationGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserNotificationGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserNotificationGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserNotificationGroupID() As Integer
      Get
        Return GetProperty(UserNotificationGroupIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public ReadOnly Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "Notification Group", Nothing)
    ''' <summary>
    ''' Gets the Notification Group value
    ''' </summary>
    <Display(Name:="Notification Group", Description:="")>
    Public ReadOnly Property NotificationGroupID() As Integer?
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroup, "Notification Group", "")
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Notification Group", Description:="")>
    Public ReadOnly Property NotificationGroup() As String
      Get
        Return GetProperty(NotificationGroupProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROUserNotificationGroupContactListProperty As PropertyInfo(Of ROUserNotificationGroupContactList) = RegisterProperty(Of ROUserNotificationGroupContactList)(Function(c) c.ROUserNotificationGroupContactList, "Notification Group Contact List")
    Public ReadOnly Property ROUserNotificationGroupContactList() As ROUserNotificationGroupContactList
      Get
        If GetProperty(ROUserNotificationGroupContactListProperty) Is Nothing Then
          LoadProperty(ROUserNotificationGroupContactListProperty, Notifications.ReadOnly.ROUserNotificationGroupContactList.NewROUserNotificationGroupContactList())
        End If
        Return GetProperty(ROUserNotificationGroupContactListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserNotificationGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserNotificationGroupID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserNotificationGroup(dr As SafeDataReader) As ROUserNotificationGroup

      Dim r As New ROUserNotificationGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserNotificationGroupIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(NotificationGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(NotificationGroupProperty, .GetString(3))
        LoadProperty(IsSelectedProperty, .GetBoolean(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace