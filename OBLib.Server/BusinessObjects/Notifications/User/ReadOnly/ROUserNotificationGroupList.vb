﻿' Generated 16 Sep 2015 07:55 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class ROUserNotificationGroupList
    Inherits OBReadOnlyListBase(Of ROUserNotificationGroupList, ROUserNotificationGroup)

#Region " Business Methods "

    Public Function GetItem(UserNotificationGroupID As Integer) As ROUserNotificationGroup

      For Each child As ROUserNotificationGroup In Me
        If child.UserNotificationGroupID = UserNotificationGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByNotificationGroupID(NotificationGroupID As Integer) As ROUserNotificationGroup

      For Each child As ROUserNotificationGroup In Me
        If child.NotificationGroupID = NotificationGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "User Notification Groups"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer? = Nothing
      Public Property NotificationGroupID As Integer? = Nothing

      Public Sub New(UserID As Integer?, NotificationGroupID As Integer?)
        Me.UserID = UserID
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserNotificationGroupList() As ROUserNotificationGroupList

      Return New ROUserNotificationGroupList()

    End Function

    Public Shared Sub BeginGetROUserNotificationGroupList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserNotificationGroupList)))

      Dim dp As New DataPortal(Of ROUserNotificationGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROUserNotificationGroupList(CallBack As EventHandler(Of DataPortalResult(Of ROUserNotificationGroupList)))

      Dim dp As New DataPortal(Of ROUserNotificationGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserNotificationGroupList(UserID As Integer?, NotificationGroupID As Integer?) As ROUserNotificationGroupList

      Return DataPortal.Fetch(Of ROUserNotificationGroupList)(New Criteria(UserID, NotificationGroupID))

    End Function

    Public Shared Function GetROUserNotificationGroupList() As ROUserNotificationGroupList

      Return DataPortal.Fetch(Of ROUserNotificationGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserNotificationGroup.GetROUserNotificationGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROUserNotificationGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.NotificationGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByNotificationGroupID(sdr.GetInt32(1))
          End If
          parent.ROUserNotificationGroupContactList.RaiseListChangedEvents = False
          parent.ROUserNotificationGroupContactList.Add(ROUserNotificationGroupContact.GetROUserNotificationGroupContact(sdr))
          parent.ROUserNotificationGroupContactList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserNotificationGroupList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace