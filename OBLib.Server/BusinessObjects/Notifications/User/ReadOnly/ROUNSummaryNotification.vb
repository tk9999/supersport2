﻿' Generated 11 Nov 2016 12:17 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUNSummaryNotification
    Inherits OBReadOnlyBase(Of ROUNSummaryNotification)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROUNSummaryNotificationBO.ROUNSummaryNotificationBOToString(self)")

    Public Shared UserNotificationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserNotificationID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserNotificationID() As Integer
      Get
        Return GetProperty(UserNotificationIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "User")
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared NotificationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationID, "Notification")
    ''' <summary>
    ''' Gets the Notification value
    ''' </summary>
    <Display(Name:="Notification", Description:="")>
    Public ReadOnly Property NotificationID() As Integer
      Get
        Return GetProperty(NotificationIDProperty)
      End Get
    End Property

    Public Shared ReadDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ReadDateTime, "Read Date Time")
    ''' <summary>
    ''' Gets the Read Date Time value
    ''' </summary>
    <Display(Name:="Read Date Time", Description:="")>
    Public ReadOnly Property ReadDateTime As Date
      Get
        Return GetProperty(ReadDateTimeProperty)
      End Get
    End Property

    Public Shared NotificationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationTypeID, "Notification Type")
    ''' <summary>
    ''' Gets the Notification Type value
    ''' </summary>
    <Display(Name:="Notification Type", Description:="")>
    Public ReadOnly Property NotificationTypeID() As Integer
      Get
        Return GetProperty(NotificationTypeIDProperty)
      End Get
    End Property

    Public Shared NotificationTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationType, "Notification Type")
    ''' <summary>
    ''' Gets the Notification Type value
    ''' </summary>
    <Display(Name:="Notification Type", Description:="")>
    Public ReadOnly Property NotificationType() As String
      Get
        Return GetProperty(NotificationTypeProperty)
      End Get
    End Property

    Public Shared NotificationTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationTitle, "Notification Title")
    ''' <summary>
    ''' Gets the Notification Title value
    ''' </summary>
    <Display(Name:="Notification Title", Description:="")>
    Public ReadOnly Property NotificationTitle() As String
      Get
        Return GetProperty(NotificationTitleProperty)
      End Get
    End Property

    Public Shared NotificationTextShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationTextShort, "Notification Text Short")
    ''' <summary>
    ''' Gets the Notification Text Short value
    ''' </summary>
    <Display(Name:="Notification Text Short", Description:="")>
    Public ReadOnly Property NotificationTextShort() As String
      Get
        Return GetProperty(NotificationTextShortProperty)
      End Get
    End Property

    Public Shared NotificationTextLongProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationTextLong, "Notification Text Long")
    ''' <summary>
    ''' Gets the Notification Text Long value
    ''' </summary>
    <Display(Name:="Notification Text Long", Description:="")>
    Public ReadOnly Property NotificationTextLong() As String
      Get
        Return GetProperty(NotificationTextLongProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared UnRnkProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UnRnk, "Un Rnk")
    ''' <summary>
    ''' Gets the Un Rnk value
    ''' </summary>
    <Display(Name:="Un Rnk", Description:="")>
    Public ReadOnly Property UnRnk() As Integer
      Get
        Return GetProperty(UnRnkProperty)
      End Get
    End Property

    <Display(Name:="Ago", Description:="")>
    Public ReadOnly Property Ago() As String
      Get
        Dim MinutesAgo As Integer = Now.Subtract(Me.CreatedDateTime).TotalMinutes
        Dim HoursAgo As Integer = Now.Subtract(Me.CreatedDateTime).TotalHours
        If MinutesAgo < 60 Then
          Return MinutesAgo.ToString & " minutes ago"
        ElseIf MinutesAgo > 60 Then
          Return HoursAgo.ToString & " minutes ago"
        End If
        Return "minutes ago"
      End Get
    End Property

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SynergyImportID, "SynergyImportID", Nothing)
    ''' <summary>
    ''' Gets the Notification Type value
    ''' </summary>
    <Display(Name:="SynergyImportID", Description:="")>
    Public ReadOnly Property SynergyImportID() As Integer?
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserNotificationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.NotificationType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROUNSummaryNotification(dr As SafeDataReader) As ROUNSummaryNotification

      Dim r As New ROUNSummaryNotification()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserNotificationIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(NotificationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ReadDateTimeProperty, .GetValue(3))
        LoadProperty(NotificationTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(NotificationTypeProperty, .GetString(5))
        LoadProperty(NotificationTitleProperty, .GetString(6))
        LoadProperty(NotificationTextShortProperty, .GetString(7))
        LoadProperty(NotificationTextLongProperty, .GetString(8))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
        LoadProperty(UnRnkProperty, .GetInt32(10))
        LoadProperty(SynergyImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
      End With

    End Sub

#End Region

  End Class

End Namespace