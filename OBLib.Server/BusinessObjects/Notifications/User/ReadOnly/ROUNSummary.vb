﻿' Generated 11 Nov 2016 12:17 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUNSummary
    Inherits OBReadOnlyBase(Of ROUNSummary)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROUNSummaryBO.ROUNSummaryBOToString(self)")

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared TotalUnReadCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalUnReadCount, "Total Un Read Count")
    ''' <summary>
    ''' Gets the Total Un Read Count value
    ''' </summary>
    <Display(Name:="Total Un Read Count", Description:="")>
    Public ReadOnly Property TotalUnReadCount() As Integer
      Get
        Return GetProperty(TotalUnReadCountProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROUNSummaryNotificationListProperty As PropertyInfo(Of ROUNSummaryNotificationList) = RegisterProperty(Of ROUNSummaryNotificationList)(Function(c) c.ROUNSummaryNotificationList, "ROUN Summary Notification List")

    Public ReadOnly Property ROUNSummaryNotificationList() As ROUNSummaryNotificationList
      Get
        If GetProperty(ROUNSummaryNotificationListProperty) Is Nothing Then
          LoadProperty(ROUNSummaryNotificationListProperty, Users.ReadOnly.ROUNSummaryNotificationList.NewROUNSummaryNotificationList())
        End If
        Return GetProperty(ROUNSummaryNotificationListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserID.ToString()

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROUNSummary(dr As SafeDataReader) As ROUNSummary

      Dim r As New ROUNSummary()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserIDProperty, .GetInt32(0))
        LoadProperty(TotalUnReadCountProperty, .GetInt32(1))
      End With

    End Sub

#End Region

  End Class

End Namespace