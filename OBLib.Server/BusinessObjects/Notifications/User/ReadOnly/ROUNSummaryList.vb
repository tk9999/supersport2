﻿' Generated 11 Nov 2016 12:17 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUNSummaryList
    Inherits OBReadOnlyListBase(Of ROUNSummaryList, ROUNSummary)

#Region " Business Methods "

    Public Function GetItem(UserID As Integer) As ROUNSummary

      For Each child As ROUNSummary In Me
        If child.UserID = UserID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROUNSummaryNotification(UserNotificationID As Integer) As ROUNSummaryNotification

      Dim obj As ROUNSummaryNotification = Nothing
      For Each parent As ROUNSummary In Me
        obj = parent.ROUNSummaryNotificationList.GetItem(UserNotificationID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer?
      Public Property UserIDs As List(Of Integer)
      Public Property SynergyImportID As Integer?

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROUNSummaryList() As ROUNSummaryList

      Return New ROUNSummaryList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROUNSummaryList() As ROUNSummaryList

      Return DataPortal.Fetch(Of ROUNSummaryList)(New Criteria())

    End Function

    Public Shared Function GetROUNSummaryList(Criteria As Criteria) As ROUNSummaryList

      Return DataPortal.Fetch(Of ROUNSummaryList)(Criteria)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUNSummary.GetROUNSummary(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROUNSummary = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.UserID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROUNSummaryNotificationList.RaiseListChangedEvents = False
          parent.ROUNSummaryNotificationList.Add(ROUNSummaryNotification.GetROUNSummaryNotification(sdr))
          parent.ROUNSummaryNotificationList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserNotificationListSummarised"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@UserIDs", Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.UserIDs)))
            cm.Parameters.AddWithValue("@SynergyImportID", NothingDBNull(crit.SynergyImportID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace