﻿' Generated 02 Feb 2016 09:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUserNotificationList
    Inherits OBReadOnlyListBase(Of ROUserNotificationList, ROUserNotification)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(UserNotificationID As Integer) As ROUserNotification

      For Each child As ROUserNotification In Me
        If child.UserNotificationID = UserNotificationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "User Notifications"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property IsProcessing As Boolean
      Public Property UserID As Integer?
      Public Property UserIDs As List(Of Integer)

      Public Shared StartDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.StartDate, "Start Date", Now)
      ''' <summary>
      ''' Gets and sets the Start Date Time value
      ''' </summary>
      <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required"),
      SetExpression("ROUserNotificationListCriteriaBO.StartDateSet(self)")>
      Public Property StartDate As Date?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As Date?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.EndDate, "End Date", Now)
      ''' <summary>
      ''' Gets and sets the Start Date Time value
      ''' </summary>
      <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required"),
      SetExpression("ROUserNotificationListCriteriaBO.EndDateSet(self)")>
      Public Property EndDate As Date?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As Date?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserNotificationList() As ROUserNotificationList

      Return New ROUserNotificationList()

    End Function

    Public Shared Sub BeginGetROUserNotificationList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserNotificationList)))

      Dim dp As New DataPortal(Of ROUserNotificationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserNotificationList(CallBack As EventHandler(Of DataPortalResult(Of ROUserNotificationList)))

      Dim dp As New DataPortal(Of ROUserNotificationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetUnsentNotifications() As ROUserNotificationList

      Return DataPortal.Fetch(Of ROUserNotificationList)(New Criteria())

    End Function

    Public Shared Function GetROUserNotificationList() As ROUserNotificationList

      Return DataPortal.Fetch(Of ROUserNotificationList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserNotification.GetROUserNotification(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserNotificationList"
            cm.Parameters.AddWithValue("@UserID", Singular.Misc.NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@UserIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.UserIDs)))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace