﻿' Generated 16 Sep 2015 09:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications

  <Serializable()> _
  Public Class UserNotificationGroupContact
    Inherits SingularBusinessBase(Of UserNotificationGroupContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NotificationGroupContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupContactID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property NotificationGroupContactID() As Integer
      Get
        Return GetProperty(NotificationGroupContactIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NotificationGroupContactIDProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupID, "Notification Group")
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Notification Group", Description:=""),
    Required(ErrorMessage:="Notification Group required")>
  Public Property NotificationGroupID() As Integer
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NotificationGroupIDProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactName, "Notification Group Contact Name")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Name value
    ''' </summary>
    <Display(Name:="Notification Group Contact Name", Description:="")>
  Public Property NotificationGroupContactName() As String
      Get
        Return GetProperty(NotificationGroupContactNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupContactNameProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactNumber, "Notification Group Contact Number")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Number value
    ''' </summary>
    <Display(Name:="Notification Group Contact Number", Description:="")>
  Public Property NotificationGroupContactNumber() As String
      Get
        Return GetProperty(NotificationGroupContactNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupContactNumberProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupContactEmailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactEmail, "Notification Group Contact Email")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Email value
    ''' </summary>
    <Display(Name:="Notification Group Contact Email", Description:="")>
  Public Property NotificationGroupContactEmail() As String
      Get
        Return GetProperty(NotificationGroupContactEmailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupContactEmailProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NotificationGroupContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.NotificationGroupContactName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "User Notification Group Contact")
        Else
          Return String.Format("Blank {0}", "User Notification Group Contact")
        End If
      Else
        Return Me.NotificationGroupContactName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewUserNotificationGroupContact() method.

    End Sub

    Public Shared Function NewUserNotificationGroupContact() As UserNotificationGroupContact

      Return DataPortal.CreateChild(Of UserNotificationGroupContact)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetUserNotificationGroupContact(dr As SafeDataReader) As UserNotificationGroupContact

      Dim u As New UserNotificationGroupContact()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NotificationGroupContactIDProperty, .GetInt32(0))
          LoadProperty(NotificationGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(NotificationGroupContactNameProperty, .GetString(2))
          LoadProperty(NotificationGroupContactNumberProperty, .GetString(3))
          LoadProperty(NotificationGroupContactEmailProperty, .GetString(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insUserNotificationGroupContact"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updUserNotificationGroupContact"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNotificationGroupContactID As SqlParameter = .Parameters.Add("@NotificationGroupContactID", SqlDbType.Int)
          paramNotificationGroupContactID.Value = GetProperty(NotificationGroupContactIDProperty)
          If Me.IsNew Then
            paramNotificationGroupContactID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@NotificationGroupID", GetProperty(NotificationGroupIDProperty))
          .Parameters.AddWithValue("@NotificationGroupContactName", GetProperty(NotificationGroupContactNameProperty))
          .Parameters.AddWithValue("@NotificationGroupContactNumber", GetProperty(NotificationGroupContactNumberProperty))
          .Parameters.AddWithValue("@NotificationGroupContactEmail", GetProperty(NotificationGroupContactEmailProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(NotificationGroupContactIDProperty, paramNotificationGroupContactID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delUserNotificationGroupContact"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NotificationGroupContactID", GetProperty(NotificationGroupContactIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace