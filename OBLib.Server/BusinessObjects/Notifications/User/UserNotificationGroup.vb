﻿' Generated 11 Sep 2015 14:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications

  <Serializable()> _
  Public Class UserNotificationGroup
    Inherits OBBusinessBase(Of UserNotificationGroup)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Is Expanded", False)
    ' ''' <summary>
    ' ''' Gets and sets the Notification Group value
    ' ''' </summary>
    '<Display(Name:="Is Expanded")>
    'Public Property IsExpanded() As Boolean
    '  Get
    '    Return GetProperty(IsExpandedProperty)
    '  End Get
    '  Set(ByVal Value As Boolean)
    '    SetProperty(IsExpandedProperty, Value)
    '  End Set
    'End Property

    Public Shared UserNotificationGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserNotificationGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserNotificationGroupID() As Integer
      Get
        Return GetProperty(UserNotificationGroupIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets and sets the User value
    ''' </summary>
    <Display(Name:="User", Description:=""),
    Required(ErrorMessage:="User required")>
    Public Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(UserIDProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "Notification Group", Nothing)
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Notification Group", Description:=""),
    Required(ErrorMessage:="Notification Group required")>
    Public Property NotificationGroupID() As Integer?
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NotificationGroupIDProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroup, "Notification Group", "")
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Notification Group", Description:=""),
    StringLength(100, ErrorMessage:="Notification Group cannot be more than 100 characters")>
    Public ReadOnly Property NotificationGroup() As String
      Get
        Return GetProperty(NotificationGroupProperty)
      End Get
      'Set(ByVal Value As String)
      '  SetProperty(NotificationGroupProperty, Value)
      'End Set
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared UserNotificationGroupContactListProperty As PropertyInfo(Of UserNotificationGroupContactList) = RegisterProperty(Of UserNotificationGroupContactList)(Function(c) c.UserNotificationGroupContactList, "Notification Group Contact List")
    '    Public ReadOnly Property UserNotificationGroupContactList() As UserNotificationGroupContactList
    '      Get
    '        If GetProperty(UserNotificationGroupContactListProperty) Is Nothing Then
    '          LoadProperty(UserNotificationGroupContactListProperty, Notifications.UserNotificationGroupContactList.NewUserNotificationGroupContactList())
    '        End If
    '        Return GetProperty(UserNotificationGroupContactListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserNotificationGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.UserNotificationGroupID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "User Notification Group")
        Else
          Return String.Format("Blank {0}", "User Notification Group")
        End If
      Else
        Return Me.UserNotificationGroupID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

        Protected Overrides Sub OnCreate()

            'This is called when a new object is created
            'Set any variables here, not in the constructor or NewUserNotificationGroup() method.

        End Sub

    Public Shared Function NewUserNotificationGroup() As UserNotificationGroup

      Return DataPortal.CreateChild(Of UserNotificationGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetUserNotificationGroup(dr As SafeDataReader) As UserNotificationGroup

      Dim u As New UserNotificationGroup()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(UserNotificationGroupIDProperty, .GetInt32(0))
          LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(NotificationGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(NotificationGroupProperty, .GetString(3))
          LoadProperty(IsSelectedProperty, .GetBoolean(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insUserNotificationGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updUserNotificationGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramUserNotificationGroupID As SqlParameter = .Parameters.Add("@UserNotificationGroupID", SqlDbType.Int)
          paramUserNotificationGroupID.Value = GetProperty(UserNotificationGroupIDProperty)
          If Me.IsNew Then
            paramUserNotificationGroupID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@UserID", GetProperty(UserIDProperty))
          .Parameters.AddWithValue("@NotificationGroupID", GetProperty(NotificationGroupIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(UserNotificationGroupIDProperty, paramUserNotificationGroupID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delUserNotificationGroup"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@UserNotificationGroupID", GetProperty(UserNotificationGroupIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace