﻿' Generated 16 Sep 2015 09:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications

  <Serializable()> _
  Public Class UserNotificationGroupContactList
    Inherits SingularBusinessListBase(Of UserNotificationGroupContactList, UserNotificationGroupContact)

#Region " Business Methods "

    Public Function GetItem(NotificationGroupContactID As Integer) As UserNotificationGroupContact

      For Each child As UserNotificationGroupContact In Me
        If child.NotificationGroupContactID = NotificationGroupContactID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "NotificationGroupID", Nothing)
      ''' <summary>
      ''' Gets and sets the User value
      ''' </summary>
      <Display(Name:="NotificationGroupID", Description:="")>
      Public Property NotificationGroupID() As Integer?
        Get
          Return ReadProperty(NotificationGroupIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(NotificationGroupIDProperty, Value)
        End Set
      End Property

      Public Sub New(NotificationGroupID As Integer?)
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewUserNotificationGroupContactList() As UserNotificationGroupContactList

      Return New UserNotificationGroupContactList()

    End Function

    Public Shared Sub BeginGetUserNotificationGroupContactList(CallBack As EventHandler(Of DataPortalResult(Of UserNotificationGroupContactList)))

      Dim dp As New DataPortal(Of UserNotificationGroupContactList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetUserNotificationGroupContactList() As UserNotificationGroupContactList

      Return DataPortal.Fetch(Of UserNotificationGroupContactList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(UserNotificationGroupContact.GetUserNotificationGroupContact(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getUserNotificationGroupContactList"
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As UserNotificationGroupContact In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As UserNotificationGroupContact In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace