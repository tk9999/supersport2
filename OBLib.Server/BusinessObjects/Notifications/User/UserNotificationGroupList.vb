﻿' Generated 11 Sep 2015 14:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications

  <Serializable()> _
  Public Class UserNotificationGroupList
    Inherits OBBusinessListBase(Of UserNotificationGroupList, UserNotificationGroup)

#Region " Business Methods "

    Public Function GetItem(UserNotificationGroupID As Integer) As UserNotificationGroup

      For Each child As UserNotificationGroup In Me
        If child.UserNotificationGroupID = UserNotificationGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "User Notification Groups"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
      ''' <summary>
      ''' Gets and sets the User value
      ''' </summary>
      <Display(Name:="User", Description:="")>
      Public Property UserID() As Integer?
        Get
          Return ReadProperty(UserIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(UserIDProperty, Value)
        End Set
      End Property

      Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "Notification Group", Nothing)
      ''' <summary>
      ''' Gets and sets the Notification Group value
      ''' </summary>
      <Display(Name:="Notification Group", Description:="")>
      Public Property NotificationGroupID() As Integer?
        Get
          Return ReadProperty(NotificationGroupIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(NotificationGroupIDProperty, Value)
        End Set
      End Property

      Public Sub New(UserID As Integer?, NotificationGroupID As Integer?)
        Me.UserID = UserID
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewUserNotificationGroupList() As UserNotificationGroupList

      Return New UserNotificationGroupList()

    End Function

    Public Shared Sub BeginGetUserNotificationGroupList(CallBack As EventHandler(Of DataPortalResult(Of UserNotificationGroupList)))

      Dim dp As New DataPortal(Of UserNotificationGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetUserNotificationGroupList(UserID As Integer?, NotificationGroupID As Integer?) As UserNotificationGroupList

      Return DataPortal.Fetch(Of UserNotificationGroupList)(New Criteria(UserID, NotificationGroupID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(UserNotificationGroup.GetUserNotificationGroup(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getUserNotificationGroupList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As UserNotificationGroup In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As UserNotificationGroup In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace