﻿' Generated 21 Sep 2015 10:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email

  <Serializable()> _
  Public Class EmailBatchTemplate
    Inherits SingularBusinessBase(Of EmailBatchTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailBatchTemplateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchTemplateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EmailBatchTemplateID() As Integer
      Get
        Return GetProperty(EmailBatchTemplateIDProperty)
      End Get
    End Property

    Public Shared EmailBatchTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailBatchTemplate, "Email Batch Template", "")
    ''' <summary>
    ''' Gets and sets the Email Batch Template value
    ''' </summary>
    <Display(Name:="Email Batch Template", Description:=""),
    StringLength(100, ErrorMessage:="Email Batch Template cannot be more than 100 characters")>
    Public Property EmailBatchTemplate() As String
      Get
        Return GetProperty(EmailBatchTemplateProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailBatchTemplateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailBatchTemplateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EmailBatchTemplate.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Email Batch Template")
        Else
          Return String.Format("Blank {0}", "Email Batch Template")
        End If
      Else
        Return Me.EmailBatchTemplate
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEmailBatchTemplate() method.

    End Sub

    Public Shared Function NewEmailBatchTemplate() As EmailBatchTemplate

      Return DataPortal.CreateChild(Of EmailBatchTemplate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEmailBatchTemplate(dr As SafeDataReader) As EmailBatchTemplate

      Dim e As New EmailBatchTemplate()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EmailBatchTemplateIDProperty, .GetInt32(0))
          LoadProperty(EmailBatchTemplateProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEmailBatchTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEmailBatchTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEmailBatchTemplateID As SqlParameter = .Parameters.Add("@EmailBatchTemplateID", SqlDbType.Int)
          paramEmailBatchTemplateID.Value = GetProperty(EmailBatchTemplateIDProperty)
          If Me.IsNew Then
            paramEmailBatchTemplateID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EmailBatchTemplate", GetProperty(EmailBatchTemplateProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EmailBatchTemplateIDProperty, paramEmailBatchTemplateID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEmailBatchTemplate"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EmailBatchTemplateID", GetProperty(EmailBatchTemplateIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace