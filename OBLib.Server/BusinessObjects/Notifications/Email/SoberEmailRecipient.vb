﻿' Generated 12 Oct 2015 16:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Notifications.Email.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email

  <Serializable()> _
  Public Class SoberEmailRecipient
    Inherits OBBusinessBase(Of SoberEmailRecipient)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailRecipientID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EmailRecipientID() As Integer
      Get
        Return GetProperty(EmailRecipientIDProperty)
      End Get
    End Property

    Public Shared EmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property EmailID() As Integer
      Get
        Return GetProperty(EmailIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(EmailIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Recipient Addresses", "")
    ''' <summary>
    ''' Gets and sets the To Email Address value
    ''' </summary>
    <Display(Name:="Recipient Addresses", Description:="Email Address to which email should be sent"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="To Email Address is required")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared IsCCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCC, "IsCC", False)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="IsCC", Description:="Tick indicates that this email will be ignored")>
    Public Property IsCC() As Boolean
      Get
        Return GetProperty(IsCCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCCProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailAddressProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EmailAddress.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sober Email")
        Else
          Return String.Format("Blank {0}", "Sober Email")
        End If
      Else
        Return Me.EmailAddress
      End If

    End Function

    Public Function GetParent() As SoberEmail
      Return CType(CType(Me.Parent, SoberEmailRecipientList).Parent, SoberEmail)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSoberEmailRecipient() method.

    End Sub

    Public Shared Function NewSoberEmailRecipient() As SoberEmailRecipient

      Return DataPortal.CreateChild(Of SoberEmailRecipient)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSoberEmailRecipient(dr As SafeDataReader) As SoberEmailRecipient

      Dim s As New SoberEmailRecipient()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EmailRecipientIDProperty, .GetInt32(0))
          LoadProperty(EmailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EmailAddressProperty, .GetString(3))
          LoadProperty(IsCCProperty, .GetBoolean(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSoberEmailRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSoberEmailRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          Dim paramEmailRecipientID As SqlParameter = .Parameters.Add("@EmailRecipientID", SqlDbType.Int)
          paramEmailRecipientID.Value = GetProperty(EmailRecipientIDProperty)
          If Me.IsNew Then
            paramEmailRecipientID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EmailID", Me.GetParent.EmailID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@IsCC", GetProperty(IsCCProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(EmailRecipientIDProperty, paramEmailRecipientID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSoberEmailRecipient"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EmailID", GetProperty(EmailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace