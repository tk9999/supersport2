﻿' Generated 12 Oct 2015 16:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email

  <Serializable()> _
  Public Class SoberEmailList
    Inherits OBBusinessListBase(Of SoberEmailList, SoberEmail)

#Region " Business Methods "

    Public Function GetItem(EmailID As Integer) As SoberEmail

      For Each child As SoberEmail In Me
        If child.EmailID = EmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Sub RemoveClean(Email As SoberEmail)
      Me.Remove(Email)
      DeletedList.Remove(Email)
    End Sub

    Public Overrides Function ToString() As String

      Return "Emails"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property EmailIDsXml As String = ""
      Public Property FetchAttachmentData As Boolean = False

      Public Sub New(EmailIDsXml As String, FetchAttachmentData As Boolean)
        Me.EmailIDsXml = EmailIDsXml
        Me.FetchAttachmentData = FetchAttachmentData
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSoberEmailList() As SoberEmailList

      Return New SoberEmailList()

    End Function

    Public Shared Sub BeginGetSoberEmailList(CallBack As EventHandler(Of DataPortalResult(Of SoberEmailList)))

      Dim dp As New DataPortal(Of SoberEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSoberEmailList() As SoberEmailList

      Return DataPortal.Fetch(Of SoberEmailList)(New Criteria())

    End Function

    Public Shared Function GetSoberEmailList(EmailIDs As List(Of Integer), FetchAttachmentData As Boolean) As SoberEmailList

      Dim EmailIDsXml As String = OBLib.OBMisc.IntegerListToXML(EmailIDs)

      Return DataPortal.Fetch(Of SoberEmailList)(New Criteria(EmailIDsXml, FetchAttachmentData))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SoberEmail.GetSoberEmail(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SoberEmail = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.EmailID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SoberEmailRecipientList.RaiseListChangedEvents = False
          parent.SoberEmailRecipientList.Add(SoberEmailRecipient.GetSoberEmailRecipient(sdr))
          parent.SoberEmailRecipientList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.EmailID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SoberEmailAttachmentList.RaiseListChangedEvents = False
          parent.SoberEmailAttachmentList.Add(SoberEmailAttachment.GetSoberEmailAttachment(sdr))
          parent.SoberEmailAttachmentList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SoberEmail In Me
        child.CheckRules()
        For Each emRecip As SoberEmailRecipient In child.SoberEmailRecipientList
          emRecip.CheckRules()
        Next
        For Each emAtt As SoberEmailAttachment In child.SoberEmailAttachmentList
          emAtt.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSoberEmailList"
            cm.Parameters.AddWithValue("@EmailIDsXML", Strings.MakeEmptyDBNull(crit.EmailIDsXml))
            cm.Parameters.AddWithValue("@FetchAttachmentData", crit.FetchAttachmentData)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SoberEmail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SoberEmail In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace