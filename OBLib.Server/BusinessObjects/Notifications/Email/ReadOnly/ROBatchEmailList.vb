﻿' Generated 23 Sep 2015 05:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROBatchEmailList
    Inherits Singular.Documents.DocumentProviderListBase(Of ROBatchEmailList, ROBatchEmail)
    'Inherits SingularReadOnlyListBase(Of ROBatchEmailList, ROBatchEmail)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(EmailID As Integer) As ROBatchEmail

      For Each child As ROBatchEmail In Me
        If child.EmailID = EmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property EmailBatchID As Integer? = Nothing

      Public Sub New(EmailBatchID As Integer?)
        Me.EmailBatchID = EmailBatchID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROBatchEmailList() As ROBatchEmailList

      Return New ROBatchEmailList()

    End Function

    Public Shared Sub BeginGetROBatchEmailList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROBatchEmailList)))

      Dim dp As New DataPortal(Of ROBatchEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROBatchEmailList(CallBack As EventHandler(Of DataPortalResult(Of ROBatchEmailList)))

      Dim dp As New DataPortal(Of ROBatchEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROBatchEmailList() As ROBatchEmailList

      Return DataPortal.Fetch(Of ROBatchEmailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      'Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROBatchEmail.GetROBatchEmail(sdr))
      End While
      'Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROEmailListBatch]"
            cm.Parameters.AddWithValue("@EmailBatchID", NothingDBNull(crit.EmailBatchID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace