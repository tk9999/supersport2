﻿' Generated 13 Nov 2015 07:53 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROEmailSearch
    Inherits OBReadOnlyBase(Of ROEmailSearch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsBatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsBatch, "ID", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property IsBatch() As Boolean
      Get
        Return GetProperty(IsBatchProperty)
      End Get
    End Property

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchID, "Email Batch")
    ''' <summary>
    ''' Gets the Email Batch value
    ''' </summary>
    <Display(Name:="Email Batch", Description:="")>
    Public ReadOnly Property EmailBatchID() As Integer
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
    End Property

    Public Shared EmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailID, "Email")
    ''' <summary>
    ''' Gets the Email value
    ''' </summary>
    <Display(Name:="Email", Description:="")>
    Public ReadOnly Property EmailID() As Integer
      Get
        Return GetProperty(EmailIDProperty)
      End Get
    End Property

    Public Shared TempIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TempID, "TempID")
    ''' <summary>
    ''' Gets the Brief Description value
    ''' </summary>
    <Key>
    Public ReadOnly Property TempID() As String
      Get
        Return GetProperty(TempIDProperty)
      End Get
    End Property

    Public Shared BriefDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BriefDescription, "Brief Description")
    ''' <summary>
    ''' Gets the Brief Description value
    ''' </summary>
    <Display(Name:="Brief Description", Description:="")>
    Public ReadOnly Property BriefDescription() As String
      Get
        Return GetProperty(BriefDescriptionProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared RecipientCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RecipientCount, "Recipients")
    ''' <summary>
    ''' Gets the Recipient Count value
    ''' </summary>
    <Display(Name:="Recipients", Description:="")>
    Public ReadOnly Property RecipientCount() As Integer
      Get
        Return GetProperty(RecipientCountProperty)
      End Get
    End Property

    Public Shared CCCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CCCount, "CC")
    ''' <summary>
    ''' Gets the CC Count value
    ''' </summary>
    <Display(Name:="CC", Description:="")>
    Public ReadOnly Property CCCount() As Integer
      Get
        Return GetProperty(CCCountProperty)
      End Get
    End Property

    Public Shared SentCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SentCount, "Sent")
    ''' <summary>
    ''' Gets the Sent Count value
    ''' </summary>
    <Display(Name:="Sent", Description:="")>
    Public ReadOnly Property SentCount() As Integer
      Get
        Return GetProperty(SentCountProperty)
      End Get
    End Property

    Public Shared NotSentCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotSentCount, "Not Sent")
    ''' <summary>
    ''' Gets the Not Sent Count value
    ''' </summary>
    <Display(Name:="Not Sent", Description:="")>
    Public ReadOnly Property NotSentCount() As Integer
      Get
        Return GetProperty(NotSentCountProperty)
      End Get
    End Property

    Public Shared SentDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SentDescription, "")
    ''' <summary>
    ''' Gets the Sent Description value
    ''' </summary>
    <Display(Name:="", Description:="")>
    Public ReadOnly Property SentDescription() As String
      Get
        Return GetProperty(SentDescriptionProperty)
      End Get
    End Property

    Public Shared RecipientsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Recipients, "Recipients")
    ''' <summary>
    ''' Gets the Recipients value
    ''' </summary>
    <Display(Name:="Recipients", Description:="")>
    Public ReadOnly Property Recipients() As String
      Get
        Return GetProperty(RecipientsProperty)
      End Get
    End Property

    Public Shared CCRecipientsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CCRecipients, "CC Recipients")
    ''' <summary>
    ''' Gets the CC Recipients value
    ''' </summary>
    <Display(Name:="CC Recipients", Description:="")>
    Public ReadOnly Property CCRecipients() As String
      Get
        Return GetProperty(CCRecipientsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedBy, "Created By", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedBy() As String
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared MessagePreviewProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MessagePreview, "Message Preview")
    ''' <summary>
    ''' Gets the Message Preview value
    ''' </summary>
    <Display(Name:="Message Preview", Description:="")>
    Public ReadOnly Property MessagePreview() As String
      Get
        Return GetProperty(MessagePreviewProperty)
      End Get
    End Property

    Public Shared AttachmentCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AttachmentCount, "Attachments")
    ''' <summary>
    ''' Gets the Message Preview value
    ''' </summary>
    <Display(Name:="Attachments", Description:="")>
    Public ReadOnly Property AttachmentCount() As Integer
      Get
        Return GetProperty(AttachmentCountProperty)
      End Get
    End Property

    Public Shared FailedToSendCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FailedToSendCount, "Failed")
    ''' <summary>
    ''' Gets the Message Preview value
    ''' </summary>
    <Display(Name:="Failed", Description:="")>
    Public ReadOnly Property FailedToSendCount() As Integer
      Get
        Return GetProperty(FailedToSendCountProperty)
      End Get
    End Property

    Public Shared AllSentProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllSent, "All Sent", False)
    ''' <summary>
    ''' Gets the All Sent value
    ''' </summary>
    <Display(Name:="All Sent", Description:="")>
    Public ReadOnly Property AllSent() As Boolean
      Get
        Return GetProperty(AllSentProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDate, "Created Date", CType(Nothing, DateTime?))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime?
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    <Display(Name:="Created Date")>
    Public ReadOnly Property CreatedDateString As String
      Get
        Return Me.CreatedDate.Value.ToString("ddd dd MMM yy HH:mm")
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TempIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BriefDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEmailSearch(dr As SafeDataReader) As ROEmailSearch

      Dim r As New ROEmailSearch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(IsBatchProperty, .GetBoolean(0))
        LoadProperty(EmailBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EmailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(BriefDescriptionProperty, .GetString(3))
        LoadProperty(DescriptionProperty, .GetString(4))
        LoadProperty(RecipientCountProperty, .GetInt32(5))
        LoadProperty(CCCountProperty, .GetInt32(6))
        LoadProperty(SentCountProperty, .GetInt32(7))
        LoadProperty(NotSentCountProperty, .GetInt32(8))
        LoadProperty(SentDescriptionProperty, .GetString(9))
        LoadProperty(RecipientsProperty, .GetString(10))
        LoadProperty(CCRecipientsProperty, .GetString(11))
        LoadProperty(CreatedByProperty, .GetString(12))
        LoadProperty(MessagePreviewProperty, .GetString(13))
        LoadProperty(CreatedDateProperty, .GetValue(14))
        LoadProperty(AttachmentCountProperty, .GetInt32(15))
        LoadProperty(FailedToSendCountProperty, .GetInt32(16))
        LoadProperty(AllSentProperty, .GetBoolean(17))
        If IsBatch Then
          LoadProperty(TempIDProperty, EmailBatchID.ToString & ";" & EmailID.ToString)
        Else
          LoadProperty(TempIDProperty, "0;" & EmailID.ToString)
        End If
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace