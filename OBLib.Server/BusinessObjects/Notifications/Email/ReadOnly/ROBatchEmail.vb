﻿' Generated 23 Sep 2015 05:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROBatchEmail
    Inherits Singular.Documents.DocumentProviderBase(Of ROBatchEmail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsSelectedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsSelected, False)
    ''' <summary>
    ''' Gets the IsProcessing value
    ''' </summary>
    ''' 
    <AlwaysClean>
    Public Property IsSelected() As Boolean
      Get
        Return GetProperty(IsSelectedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSelectedProperty, value)
      End Set
    End Property

    <Display(Name:="IsBatch", Description:="Body of email")>
    Public ReadOnly Property IsBatch() As Boolean
      Get
        Return EmailBatchID IsNot Nothing
      End Get
    End Property

    Public Shared EmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EmailID() As Integer
      Get
        Return GetProperty(EmailIDProperty)
      End Get
    End Property

    Public Shared ToEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ToEmailAddress, "To Email Address")
    ''' <summary>
    ''' Gets the To Email Address value
    ''' </summary>
    <Display(Name:="To Email Address", Description:="")>
    Public ReadOnly Property ToEmailAddress() As String
      Get
        Return GetProperty(ToEmailAddressProperty)
      End Get
    End Property

    Public Shared FromEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromEmailAddress, "From Email Address")
    ''' <summary>
    ''' Gets the From Email Address value
    ''' </summary>
    <Display(Name:="From Email Address", Description:="")>
    Public ReadOnly Property FromEmailAddress() As String
      Get
        Return GetProperty(FromEmailAddressProperty)
      End Get
    End Property

    Public Shared FriendlyFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FriendlyFrom, "Friendly From")
    ''' <summary>
    ''' Gets the Friendly From value
    ''' </summary>
    <Display(Name:="Friendly From", Description:="")>
    Public ReadOnly Property FriendlyFrom() As String
      Get
        Return GetProperty(FriendlyFromProperty)
      End Get
    End Property

    Public Shared SubjectProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Subject, "Subject")
    ''' <summary>
    ''' Gets the Subject value
    ''' </summary>
    <Display(Name:="Subject", Description:="")>
    Public ReadOnly Property Subject() As String
      Get
        Return GetProperty(SubjectProperty)
      End Get
    End Property

    Public Shared BodyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Body, "Body")
    ''' <summary>
    ''' Gets the Body value
    ''' </summary>
    <Display(Name:="Body", Description:="")>
    Public ReadOnly Property Body() As String
      Get
        Return GetProperty(BodyProperty)
      End Get
    End Property

    Public Shared CCEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CCEmailAddresses, "CC Email Addresses")
    ''' <summary>
    ''' Gets the CC Email Addresses value
    ''' </summary>
    <Display(Name:="CC Email Addresses", Description:="")>
    Public ReadOnly Property CCEmailAddresses() As String
      Get
        Return GetProperty(CCEmailAddressesProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As String
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DateToSend, "Date To Send")
    ''' <summary>
    ''' Gets the Date To Send value
    ''' </summary>
    <Display(Name:="Date To Send", Description:="")>
    Public ReadOnly Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SentDate, "Sent Date")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="")>
    Public ReadOnly Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error")
    ''' <summary>
    ''' Gets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:="")>
    Public ReadOnly Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", False)
    ''' <summary>
    ''' Gets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="")>
    Public ReadOnly Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
    End Property

    Public Shared HTMLBodyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HTMLBody, "HTML Body")
    ''' <summary>
    ''' Gets the HTML Body value
    ''' </summary>
    <Display(Name:="HTML Body", Description:="")>
    Public ReadOnly Property HTMLBody() As String
      Get
        Return GetProperty(HTMLBodyProperty)
      End Get
    End Property

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailBatchID, "Email Batch", Nothing)
    ''' <summary>
    ''' Gets the Email Batch value
    ''' </summary>
    <Display(Name:="Email Batch", Description:="")>
    Public ReadOnly Property EmailBatchID() As Integer?
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
    End Property

    Public Shared SystemGeneratedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemGenerated, "System Generated", False)
    ''' <summary>
    ''' Gets the System Generated value
    ''' </summary>
    <Display(Name:="System Generated", Description:="")>
    Public ReadOnly Property SystemGenerated() As Boolean
      Get
        Return GetProperty(SystemGeneratedProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public ReadOnly Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public Shared EmailAttachmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailAttachmentID, "Email Attachment")
    ''' <summary>
    ''' Gets the Email Attachment value
    ''' </summary>
    <Display(Name:="Email Attachment", Description:="")>
    Public ReadOnly Property EmailAttachmentID() As Integer
      Get
        Return GetProperty(EmailAttachmentIDProperty)
      End Get
    End Property

    Public Shared AttachmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AttachmentName, "Attachment Name")
    ''' <summary>
    ''' Gets the Attachment Name value
    ''' </summary>
    <Display(Name:="Attachment Name", Description:="")>
    Public ReadOnly Property AttachmentName() As String
      Get
        Return GetProperty(AttachmentNameProperty)
      End Get
    End Property

    Public Shared AddressOfAttachmentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressOfAttachment, "Address Of Attachment")
    ''' <summary>
    ''' Gets the Address Of Attachment value
    ''' </summary>
    <Display(Name:="Address Of Attachment", Description:="")>
    Public ReadOnly Property AddressOfAttachment() As String
      Get
        Return GetProperty(AddressOfAttachmentProperty)
      End Get
    End Property

    Public Shared AttachmentDataProperty As PropertyInfo(Of Byte()) = RegisterProperty(Of Byte())(Function(c) c.AttachmentData, "AttachmentData")
    <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property AttachmentData() As Byte()
      Get
        Return GetProperty(AttachmentDataProperty)
      End Get
    End Property

    Public Shared SendStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SendStatus, "Send Status", "")
    ''' <Summary>
    ''' Gets and sets the CC Email Addresses value
    ''' </Summary>
    <Display(Name:="Send Status")> _
    Public ReadOnly Property SendStatus() As String
      Get
        If SentDate Is Nothing Then
          If NotSentError = "" Then
            Return "Pending"
          Else
            Return "Failed to Send"
          End If
        Else
          If NotSentError = "" Then
            Return "Sent"
          Else
            Return "Pending"
          End If
        End If
      End Get
    End Property

    <Display(Name:="Document", Description:="Document", AutoGenerateField:=True),
   Singular.DataAnnotations.DocumentField(GetType(OBLib.Notifications.Email.ReadOnly.ROBatchEmail), "DocumentName"), Browsable(True)> _
    Public Overrides Property DocumentID As Integer? 'Implements Singular.Documents.IDocumentProvider.DocumentID
      Get
        Return GetProperty(DocumentIDProperty)
      End Get
      Set(ByVal value As Integer?)
        SetProperty(DocumentIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ToEmailAddress

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROBatchEmail(dr As SafeDataReader) As ROBatchEmail

      Dim r As New ROBatchEmail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailIDProperty, .GetInt32(0))
        LoadProperty(ToEmailAddressProperty, .GetString(1))
        LoadProperty(FromEmailAddressProperty, .GetString(2))
        LoadProperty(FriendlyFromProperty, .GetString(3))
        LoadProperty(SubjectProperty, .GetString(4))
        LoadProperty(BodyProperty, .GetString(5))
        LoadProperty(CCEmailAddressesProperty, .GetString(6))
        LoadProperty(CreatedByProperty, .GetString(7))
        LoadProperty(CreatedDateProperty, .GetDateTime(8))
        LoadProperty(DateToSendProperty, .GetValue(9))
        LoadProperty(SentDateProperty, .GetValue(10))
        LoadProperty(NotSentErrorProperty, .GetString(11))
        LoadProperty(IgnoreProperty, .GetBoolean(12))
        LoadProperty(HTMLBodyProperty, .GetString(13))
        LoadProperty(EmailBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(SystemGeneratedProperty, .GetBoolean(15))
        LoadProperty(RowNoProperty, .GetInt32(16))
        LoadProperty(EmailAttachmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(AttachmentNameProperty, .GetString(18))
        LoadProperty(AddressOfAttachmentProperty, .GetString(19))
        LoadProperty(AttachmentDataProperty, .GetValue(20))
        LoadProperty(DocumentNameProperty, AttachmentName)
        LoadProperty(DocumentIDProperty, ZeroNothing(.GetInt32(21)))
      End With

    End Sub

#End If

#End Region

#End Region

    Protected Overrides Sub CallSaveDocument()

    End Sub

    Public Overrides Sub DeleteSelf()

    End Sub

    Public Overrides Sub Insert()

    End Sub

    Public Overrides Sub Update()

    End Sub

  End Class

End Namespace