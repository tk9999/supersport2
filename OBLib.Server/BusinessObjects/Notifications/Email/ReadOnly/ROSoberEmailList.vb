﻿' Generated 12 Oct 2015 16:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROSoberEmailList
    Inherits OBReadOnlyListBase(Of ROSoberEmailList, ROSoberEmail)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(EmailID As Integer) As ROSoberEmail

      For Each child As ROSoberEmail In Me
        If child.EmailID = EmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Emails"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EmailAddress, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="To Email Address", Description:="")>
      Public Property EmailAddress() As String
        Get
          Return ReadProperty(EmailAddressProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(EmailAddressProperty, Value)
        End Set
      End Property

      Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BatchName, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Batch Name", Description:="")>
      Public Property BatchName() As String
        Get
          Return ReadProperty(BatchNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(BatchNameProperty, Value)
        End Set
      End Property

      Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CreatedByName, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Created By", Description:="")>
      Public Property CreatedByName() As String
        Get
          Return ReadProperty(CreatedByNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(CreatedByNameProperty, Value)
        End Set
      End Property

      Public Shared CreatedDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CreatedDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Created Date", Description:="")>
      Public Property CreatedDate() As DateTime?
        Get
          Return ReadProperty(CreatedDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(CreatedDateProperty, Value)
        End Set
      End Property

      Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EmailBatchID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Batch Name", Description:="")>
      Public Property EmailBatchID() As Integer?
        Get
          Return ReadProperty(EmailBatchIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(EmailBatchIDProperty, Value)
        End Set
      End Property

      Public Shared HasBeenDeliveredProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.HasBeenDelivered, False)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Delivered", Description:="")>
      Public Property HasBeenDelivered() As Boolean?
        Get
          Return ReadProperty(HasBeenDeliveredProperty)
        End Get
        Set(ByVal Value As Boolean?)
          LoadProperty(HasBeenDeliveredProperty, Value)
        End Set
      End Property

      Public Shared HasNotBeenDeliveredProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.HasNotBeenDelivered, False)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Not Delivered", Description:="")>
      Public Property HasNotBeenDelivered() As Boolean?
        Get
          Return ReadProperty(HasNotBeenDeliveredProperty)
        End Get
        Set(ByVal Value As Boolean?)
          LoadProperty(HasNotBeenDeliveredProperty, Value)
        End Set
      End Property

      Public Sub New(EmailAddress As String, EmailBatchID As Integer?, _
                     BatchName As String)

        Me.EmailAddress = EmailAddress
        Me.EmailBatchID = EmailBatchID
        Me.BatchName = BatchName

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSoberEmailList() As ROSoberEmailList

      Return New ROSoberEmailList()

    End Function

    Public Shared Sub BeginGetROSoberEmailList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSoberEmailList)))

      Dim dp As New DataPortal(Of ROSoberEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSoberEmailList(CallBack As EventHandler(Of DataPortalResult(Of ROSoberEmailList)))

      Dim dp As New DataPortal(Of ROSoberEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSoberEmailList() As ROSoberEmailList

      Return DataPortal.Fetch(Of ROSoberEmailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSoberEmail.GetROSoberEmail(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSoberEmailList"
            'cm.Parameters.AddWithValue("@RecipientName", Singular.Strings.MakeEmptyDBNull(crit.RecipientName))
            cm.Parameters.AddWithValue("@EmailAddress", Singular.Strings.MakeEmptyDBNull(crit.EmailAddress))
            cm.Parameters.AddWithValue("@EmailBatchID", Singular.Misc.NothingDBNull(crit.EmailBatchID))
            cm.Parameters.AddWithValue("@BatchName", Singular.Strings.MakeEmptyDBNull(crit.BatchName))
            cm.Parameters.AddWithValue("@CreatedByName", Singular.Strings.MakeEmptyDBNull(crit.CreatedByName))
            cm.Parameters.AddWithValue("@CreatedDate", NothingDBNull(crit.CreatedDate))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace