﻿' Generated 13 Nov 2015 07:53 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROEmailSearchList
    Inherits OBReadOnlyListBase(Of ROEmailSearchList, ROEmailSearch)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(IsBatch As Boolean) As ROEmailSearch

      For Each child As ROEmailSearch In Me
        If child.IsBatch = IsBatch Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="Created Start Date"), SetExpression("ROEmailSearchListCriteriaBO.CreatedStartDateSet(self)")>
      Public Property CreatedStartDate As DateTime? = Now.AddDays(-7)

      <Display(Name:="Created End Date"), SetExpression("ROEmailSearchListCriteriaBO.CreatedEndDateSet(self)")>
      Public Property CreatedEndDate As DateTime? = Now

      <Display(Name:="Created By Me?"), SetExpression("ROEmailSearchListCriteriaBO.CreatedByMeSet(self)")>
      Public Property CreatedByMe As Boolean = True

      <Display(Name:="Created By Someone Else"), SetExpression("ROEmailSearchListCriteriaBO.CreatedByOtherSet(self)", False, 250), TextField(False, False, False, 1)>
      Public Property CreatedByOther As String = ""
      '

      Public Property SystemGenerated As Boolean? = False

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEmailSearchList() As ROEmailSearchList

      Return New ROEmailSearchList()

    End Function

    Public Shared Sub BeginGetROEmailSearchList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEmailSearchList)))

      Dim dp As New DataPortal(Of ROEmailSearchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEmailSearchList(CallBack As EventHandler(Of DataPortalResult(Of ROEmailSearchList)))

      Dim dp As New DataPortal(Of ROEmailSearchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEmailSearchList() As ROEmailSearchList

      Return DataPortal.Fetch(Of ROEmailSearchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEmailSearch.GetROEmailSearch(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEmailSearchList"
            cm.Parameters.AddWithValue("@CreatedStartDate", NothingDBNull(crit.CreatedStartDate))
            cm.Parameters.AddWithValue("@CreatedEndDate", NothingDBNull(crit.CreatedEndDate))
            cm.Parameters.AddWithValue("@SystemGenerated", NothingDBNull(crit.SystemGenerated))
            cm.Parameters.AddWithValue("@CreatedByMe", NothingDBNull(crit.CreatedByMe))
            cm.Parameters.AddWithValue("@CreatedByOther", Strings.MakeEmptyDBNull(crit.CreatedByOther))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace