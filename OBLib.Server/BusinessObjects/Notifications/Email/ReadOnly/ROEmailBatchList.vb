﻿' Generated 21 Sep 2015 10:55 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROEmailBatchList
    Inherits SingularReadOnlyListBase(Of ROEmailBatchList, ROEmailBatch)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(EmailBatchID As Integer) As ROEmailBatch

      For Each child As ROEmailBatch In Me
        If child.EmailBatchID = EmailBatchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Email Batches"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property CreatedBy As Integer? = Nothing

      Public Sub New(CreatedBy As Integer?)
        Me.CreatedBy = CreatedBy
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEmailBatchList() As ROEmailBatchList

      Return New ROEmailBatchList()

    End Function

    Public Shared Sub BeginGetROEmailBatchList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEmailBatchList)))

      Dim dp As New DataPortal(Of ROEmailBatchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEmailBatchList(CallBack As EventHandler(Of DataPortalResult(Of ROEmailBatchList)))

      Dim dp As New DataPortal(Of ROEmailBatchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEmailBatchList() As ROEmailBatchList

      Return DataPortal.Fetch(Of ROEmailBatchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEmailBatch.GetROEmailBatch(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEmailBatchList"
            cm.Parameters.AddWithValue("@CreatedBy", NothingDBNull(crit.CreatedBy))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace