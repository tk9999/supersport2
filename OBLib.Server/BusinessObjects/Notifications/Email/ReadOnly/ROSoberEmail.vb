﻿' Generated 12 Oct 2015 16:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Notifications.Email.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROSoberEmail
    Inherits OBReadOnlyBase(Of ROSoberEmail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property EmailID() As Integer
      Get
        Return GetProperty(EmailIDProperty)
      End Get
    End Property

    Public Shared ToEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ToEmailAddress, "To Email Address", "")
    ''' <summary>
    ''' Gets the To Email Address value
    ''' </summary>
    <Display(Name:="To Email Address", Description:="Email Address to which email should be sent")>
    Public ReadOnly Property ToEmailAddress() As String
      Get
        Return GetProperty(ToEmailAddressProperty)
      End Get
    End Property

    Public Shared ShortAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortAddress, "To Addresses", "")
    ''' <summary>
    ''' Gets the To Email Address value
    ''' </summary>
    <Display(Name:="To Addresses", Description:="Email Address to which email should be sent")>
    Public ReadOnly Property ShortAddress() As String
      Get
        Return GetProperty(ShortAddressProperty)
      End Get
    End Property

    Public Shared FromEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromEmailAddress, "From Email Address", "noreply@supersport.co.za")
    ''' <summary>
    ''' Gets the From Email Address value
    ''' </summary>
    <Display(Name:="From Email Address", Description:="Email address of the sender")>
    Public ReadOnly Property FromEmailAddress() As String
      Get
        Return GetProperty(FromEmailAddressProperty)
      End Get
    End Property

    Public Shared FriendlyFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FriendlyFrom, "Friendly From", "")
    ''' <summary>
    ''' Gets the Friendly From value
    ''' </summary>
    <Display(Name:="Friendly From", Description:="Friendly name of sender")>
    Public ReadOnly Property FriendlyFrom() As String
      Get
        Return GetProperty(FriendlyFromProperty)
      End Get
    End Property

    Public Shared SubjectProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Subject, "Subject", "")
    ''' <summary>
    ''' Gets the Subject value
    ''' </summary>
    <Display(Name:="Subject", Description:="Subject of Email")>
    Public ReadOnly Property Subject() As String
      Get
        Return GetProperty(SubjectProperty)
      End Get
    End Property

    Public Shared BodyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Body, "Body", "")
    ''' <summary>
    ''' Gets the Body value
    ''' </summary>
    <Display(Name:="Body", Description:="Body of email")>
    Public ReadOnly Property Body() As String
      Get
        Return GetProperty(BodyProperty)
      End Get
    End Property

    <Display(Name:="Short Message", Description:="Body of email")>
    Public ReadOnly Property ShortMessage() As String
      Get
        If Body.Length > 100 Then
          Return Body.Substring(0, 97) & "..."
        Else
          Return Body
        End If
      End Get
    End Property

    <Display(Name:="IsBatch", Description:="Body of email")>
    Public ReadOnly Property IsBatch() As Boolean
      Get
        Return EmailBatchID IsNot Nothing
      End Get
    End Property

    Public Shared CCEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CCEmailAddresses, "CC Addresses", "")
    ''' <summary>
    ''' Gets the CC Email Addresses value
    ''' </summary>
    <Display(Name:="CC Addresses", Description:="Any other employees that email should be sent to")>
    Public ReadOnly Property CCEmailAddresses() As String
      Get
        Return GetProperty(CCEmailAddressesProperty)
      End Get
    End Property

    Public Shared ShortCCAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortShortCCAddress, "CC Addresses", "")
    ''' <summary>
    ''' Gets the To Email Address value
    ''' </summary>
    <Display(Name:="CC Addresses", Description:="Email Address to which email should be sent")>
    Public ReadOnly Property ShortShortCCAddress() As String
      Get
        Return GetProperty(ShortCCAddressProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedBy, "Created By", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedBy() As String
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    <Display(Name:="Created Date")>
    Public ReadOnly Property CreatedDateString As String
      Get
        Return Me.CreatedDate.ToString("ddd dd MMM yy HH:mm")
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DateToSend, "Date To Send")
    ''' <summary>
    ''' Gets the Date To Send value
    ''' </summary>
    <Display(Name:="Date To Send", Description:="Date to send the email (will be 5 minutes after this date)")>
    Public ReadOnly Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SentDate, "Sent Date")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="Date on which email was sent")>
    Public ReadOnly Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
    End Property

    Public Shared SentDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SentDateString, "Sent Date")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="Date on which email was sent")>
    Public ReadOnly Property SentDateString As String
      Get
        If SentDate Is Nothing Then
          Return ""
        Else
          Return SentDate.Value.ToString("dd MMM yy HH:mm")
        End If
      End Get
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error", "")
    ''' <summary>
    ''' Gets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:="Any errors encountered during sending")>
    Public ReadOnly Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", False)
    ''' <summary>
    ''' Gets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="Tick indicates that this email will be ignored")>
    Public ReadOnly Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
    End Property

    Public Shared HTMLBodyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HTMLBody, "Message", "")
    ''' <summary>
    ''' Gets the HTML Body value
    ''' </summary>
    <Display(Name:="Message", Description:="")>
    Public ReadOnly Property HTMLBody() As String
      Get
        Return GetProperty(HTMLBodyProperty)
      End Get
    End Property

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailBatchID, "Email Batch", Nothing)
    ''' <summary>
    ''' Gets the Email Batch value
    ''' </summary>
    <Display(Name:="Email Batch", Description:="")>
    Public ReadOnly Property EmailBatchID() As Integer?
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
    End Property

    Public Shared SystemGeneratedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemGenerated, "System Generated", True)
    ''' <summary>
    ''' Gets the System Generated value
    ''' </summary>
    <Display(Name:="System Generated", Description:="")>
    Public ReadOnly Property SystemGenerated() As Boolean
      Get
        Return GetProperty(SystemGeneratedProperty)
      End Get
    End Property

    Public Shared EmailTemplateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailTemplateTypeID, "Email Template Type", Nothing)
    ''' <summary>
    ''' Gets the Email Template Type value
    ''' </summary>
    <Display(Name:="Email Template Type", Description:=""),
    DropDownWeb(GetType(ROEmailTemplateTypeList))>
    Public ReadOnly Property EmailTemplateTypeID() As Integer?
      Get
        Return GetProperty(EmailTemplateTypeIDProperty)
      End Get
    End Property

    Public Shared SendStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SendStatus, "Send Status", "")
    ''' <Summary>
    ''' Gets and sets the CC Email Addresses value
    ''' </Summary>
    <Display(Name:="Send Status")> _
    Public ReadOnly Property SendStatus() As String
      Get
        If SentDate Is Nothing Then
          If NotSentError = "" Then
            Return "Pending"
          Else
            Return "Failed to Send"
          End If
        Else
          If NotSentError = "" Then
            Return "Sent"
          Else
            Return "Unknown"
          End If
        End If
      End Get
    End Property

    Public Shared AttachmentCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AttachmentCount, "Attachments", 0)
    ''' <summary>
    ''' Gets the Email Batch value
    ''' </summary>
    <Display(Name:="Attachments", Description:="")>
    Public ReadOnly Property AttachmentCount() As Integer
      Get
        Return GetProperty(AttachmentCountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ToEmailAddress

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSoberEmail(dr As SafeDataReader) As ROSoberEmail

      Dim r As New ROSoberEmail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailIDProperty, .GetInt32(0))
        LoadProperty(ToEmailAddressProperty, .GetString(1))
        LoadProperty(FromEmailAddressProperty, .GetString(2))
        LoadProperty(FriendlyFromProperty, .GetString(3))
        LoadProperty(SubjectProperty, .GetString(4))
        LoadProperty(BodyProperty, .GetString(5))
        LoadProperty(CCEmailAddressesProperty, .GetString(6))
        LoadProperty(CreatedByProperty, .GetString(7))
        LoadProperty(CreatedDateProperty, .GetSmartDate(8))
        LoadProperty(DateToSendProperty, .GetValue(9))
        LoadProperty(SentDateProperty, .GetValue(10))
        LoadProperty(NotSentErrorProperty, .GetString(11))
        LoadProperty(IgnoreProperty, .GetBoolean(12))
        LoadProperty(HTMLBodyProperty, .GetString(13))
        LoadProperty(EmailBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(SystemGeneratedProperty, .GetBoolean(15))
        LoadProperty(EmailTemplateTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(AttachmentCountProperty, .GetInt32(17))
      End With

      SetShortAddress()

    End Sub

    Public Sub SetShortAddress()
      If ToEmailAddress.Length > 100 Then
        LoadProperty(ShortAddressProperty, ToEmailAddress.Substring(0, 97) & "...")
      Else
        LoadProperty(ShortAddressProperty, ToEmailAddress)
      End If
    End Sub

    Public Sub SetShortCCAddress()
      If CCEmailAddresses.Length > 100 Then
        LoadProperty(ShortCCAddressProperty, CCEmailAddresses.Substring(0, 97) & "...")
      Else
        LoadProperty(ShortCCAddressProperty, CCEmailAddresses)
      End If
    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace