﻿' Generated 21 Sep 2015 10:55 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROEmailBatchTemplate
    Inherits SingularReadOnlyBase(Of ROEmailBatchTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailBatchTemplateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchTemplateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EmailBatchTemplateID() As Integer
      Get
        Return GetProperty(EmailBatchTemplateIDProperty)
      End Get
    End Property

    Public Shared EmailBatchTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailBatchTemplate, "Email Batch Template", "")
    ''' <summary>
    ''' Gets the Email Batch Template value
    ''' </summary>
    <Display(Name:="Email Batch Template", Description:="")>
  Public ReadOnly Property EmailBatchTemplate() As String
      Get
        Return GetProperty(EmailBatchTemplateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailBatchTemplateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EmailBatchTemplate

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEmailBatchTemplate(dr As SafeDataReader) As ROEmailBatchTemplate

      Dim r As New ROEmailBatchTemplate()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailBatchTemplateIDProperty, .GetInt32(0))
        LoadProperty(EmailBatchTemplateProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace