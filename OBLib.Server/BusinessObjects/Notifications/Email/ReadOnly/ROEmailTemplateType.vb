﻿' Generated 12 Oct 2015 16:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROEmailTemplateType
    Inherits OBReadOnlyBase(Of ROEmailTemplateType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailTemplateTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailTemplateTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EmailTemplateTypeID() As Integer
      Get
        Return GetProperty(EmailTemplateTypeIDProperty)
      End Get
    End Property

    Public Shared EmailTemplateTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailTemplateType, "Email Template Type", "")
    ''' <summary>
    ''' Gets the Email Template Type value
    ''' </summary>
    <Display(Name:="Email Template Type", Description:="")>
  Public ReadOnly Property EmailTemplateType() As String
      Get
        Return GetProperty(EmailTemplateTypeProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public Shared MessageTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MessageTemplate, "Message Template", "")
    ''' <summary>
    ''' Gets the Message Template value
    ''' </summary>
    <Display(Name:="Message Template", Description:="")>
  Public ReadOnly Property MessageTemplate() As String
      Get
        Return GetProperty(MessageTemplateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailTemplateTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EmailTemplateType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEmailTemplateType(dr As SafeDataReader) As ROEmailTemplateType

      Dim r As New ROEmailTemplateType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailTemplateTypeIDProperty, .GetInt32(0))
        LoadProperty(EmailTemplateTypeProperty, .GetString(1))
        LoadProperty(SystemIndProperty, .GetBoolean(2))
        LoadProperty(MessageTemplateProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace