﻿' Generated 21 Sep 2015 10:55 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email.ReadOnly

  <Serializable()> _
  Public Class ROEmailBatch
    Inherits SingularReadOnlyBase(Of ROEmailBatch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EmailBatchID() As Integer
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name", "")
    ''' <summary>
    ''' Gets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:="")>
  Public ReadOnly Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
    End Property

    Public Shared BatchDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchDescription, "Batch Description", "")
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:="")>
  Public ReadOnly Property BatchDescription() As String
      Get
        Return GetProperty(BatchDescriptionProperty)
      End Get
    End Property

    Public Shared EmailBatchTemplateIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailBatchTemplateID, "Email Batch Template", Nothing)
    ''' <summary>
    ''' Gets the Email Batch Template value
    ''' </summary>
    <Display(Name:="Email Batch Template", Description:="")>
  Public ReadOnly Property EmailBatchTemplateID() As Integer?
      Get
        Return GetProperty(EmailBatchTemplateIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
  Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
  Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailBatchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BatchName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEmailBatch(dr As SafeDataReader) As ROEmailBatch

      Dim r As New ROEmailBatch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailBatchIDProperty, .GetInt32(0))
        LoadProperty(BatchNameProperty, .GetString(1))
        LoadProperty(BatchDescriptionProperty, .GetString(2))
        LoadProperty(EmailBatchTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(EndDateProperty, .GetValue(5))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetValue(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetValue(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace