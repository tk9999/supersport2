﻿' Generated 31 Mar 2016 08:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email

  <Serializable()> _
  Public Class EmailBatchRecipient
    Inherits OBBusinessBase(Of EmailBatchRecipient)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailBatchRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchRecipientID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EmailBatchRecipientID() As Integer
      Get
        Return GetProperty(EmailBatchRecipientIDProperty)
      End Get
    End Property

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchID, "Email Batch", 0)
    ''' <summary>
    ''' Gets and sets the Email Batch value
    ''' </summary>
    <Display(Name:="Email Batch", Description:=""),
    Required(ErrorMessage:="Email Batch required")>
    Public Property EmailBatchID() As Integer
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EmailBatchIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:=""),
    StringLength(200, ErrorMessage:="Email Address cannot be more than 200 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RecipientName, "Recipient", "")
    ''' <summary>
    ''' Gets and sets the Recipient Name value
    ''' </summary>
    <Display(Name:="Recipient", Description:="")>
    Public Property RecipientName() As String
      Get
        Return GetProperty(RecipientNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RecipientNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailBatchRecipientIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EmailAddress.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Email Batch Recipient")
        Else
          Return String.Format("Blank {0}", "Email Batch Recipient")
        End If
      Else
        Return Me.EmailAddress
      End If

    End Function

    Public Function GetParent() As EmailBatch

      Return CType(CType(Me.Parent, EmailBatchRecipientList).Parent, EmailBatch)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEmailBatchRecipient() method.

    End Sub

    Public Shared Function NewEmailBatchRecipient() As EmailBatchRecipient

      Return DataPortal.CreateChild(Of EmailBatchRecipient)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEmailBatchRecipient(dr As SafeDataReader) As EmailBatchRecipient

      Dim e As New EmailBatchRecipient()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EmailBatchRecipientIDProperty, .GetInt32(0))
          LoadProperty(EmailBatchIDProperty, .GetInt32(1))
          LoadProperty(HumanResourceIDProperty, .GetInt32(2))
          LoadProperty(EmailAddressProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEmailBatchRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEmailBatchRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEmailBatchRecipientID As SqlParameter = .Parameters.Add("@EmailBatchRecipientID", SqlDbType.Int)
          paramEmailBatchRecipientID.Value = GetProperty(EmailBatchRecipientIDProperty)
          If Me.IsNew Then
            paramEmailBatchRecipientID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EmailBatchID", Me.GetParent.EmailBatchID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EmailBatchRecipientIDProperty, paramEmailBatchRecipientID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEmailBatchRecipient"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EmailBatchRecipientID", GetProperty(EmailBatchRecipientIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace