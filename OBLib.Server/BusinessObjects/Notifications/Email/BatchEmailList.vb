﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

#If SILVERLIGHT Then
#Else
Imports System.Data.SqlClient
#End If

Namespace Notifications.Email

  <Serializable()> _
  Public Class BatchEmailList
    Inherits OBBusinessListBase(Of BatchEmailList, BatchEmail)

#Region " Business Methods "

    Private Function GetEmailAttachmentWithDocumentID(DocumentID As Integer) As BatchEmailAttachment
      For Each child As BatchEmail In Me
        For Each att In child.BatchEmailAttachmentList
          If att.DocumentID = DocumentID Then
            Return att
          End If
        Next
      Next
      Return Nothing
    End Function

    Public Function GetItem(EmailID As Integer) As BatchEmail

      For Each child As BatchEmail In Me
        If child.EmailID = EmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Emails"

    End Function

    Private mSentCount As Integer = 0
    Public ReadOnly Property SentCount() As Integer
      Get
        Return mSentCount
      End Get
    End Property

    Private mFailedCount As Integer = 0
    Public ReadOnly Property FailedCount() As Integer
      Get
        Return mFailedCount
      End Get
    End Property

#If SILVERLIGHT Then
#Else

    'Public Sub SendEmails()
    '  SendEmails(New List(Of SingularMailSettings.MailCredential)({MailSettings.DefaultCredential}))
    'End Sub

    'Public Sub SendEmails(Credentials As List(Of SingularMailSettings.MailCredential))
    '  SendEmailsInternal(Credentials, False)
    'End Sub

    'Public Sub SendEmailsFast()
    '  SendEmailsFast(New List(Of SingularMailSettings.MailCredential)({MailSettings.DefaultCredential}))
    'End Sub

    'Public Sub SendEmailsFast(Credentials As List(Of SingularMailSettings.MailCredential))
    '  SyncLock Me

    '    Dim SendInvoker As Action(Of List(Of SingularMailSettings.MailCredential), Boolean) = AddressOf SendEmailsInternal
    '    SendInvoker.BeginInvoke(Credentials, True, Sub(Result As System.Runtime.Remoting.Messaging.AsyncResult)

    '                                                 Dim CalledMethod As Action(Of List(Of SingularMailSettings.MailCredential), Boolean) = Result.AsyncDelegate

    '                                                 Try
    '                                                   CalledMethod.EndInvoke(Result)
    '                                                 Catch ex As Exception

    '                                                 End Try

    '                                               End Sub, Nothing)

    '    'Wait until they have all sent
    '    System.Threading.Monitor.Wait(Me, 300000) 'timeout of 5 minutes
    '  End SyncLock
    'End Sub

    'Private Sub SendEmailsInternal(Credentials As List(Of SingularMailSettings.MailCredential), Async As Boolean)
    '  mSentCount = 0
    '  mFailedCount = 0

    '  For Each em As BatchEmail In Me

    '    Dim c As SingularMailSettings.MailCredential = Nothing

    '    'Get the Correct Credential for this From Address.
    '    For Each cred As SingularMailSettings.MailCredential In Credentials
    '      If cred.FromAddress = em.FromEmailAddress Then
    '        c = cred
    '        Exit For
    '      End If
    '    Next
    '    'If there is no match, then use the default credential.
    '    If c Is Nothing Then
    '      c = Credentials(0)
    '    End If


    '    If [Async] Then
    '      em.Send(c, AddressOf EmailSendComplete)
    '    Else
    '      'Synchronous
    '      If em.Send(c) Then
    '        mSentCount += 1
    '      Else
    '        mFailedCount += 1
    '      End If
    '    End If
    '  Next

    'End Sub

    'Private Sub EmailSendComplete(sender As Object, e As System.ComponentModel.AsyncCompletedEventArgs)

    '  Dim em As Email = CType(e.UserState, SingularMail).OriginalEmail
    '  If em.HandleSendComplete(e.Error) Then
    '    mSentCount += 1
    '  Else
    '    mFailedCount += 1
    '  End If

    '  If mSentCount + mFailedCount = Me.Count Then
    '    'Allow the SendEmailsFast method to continue once all emails are sent.
    '    SyncLock (Me)
    '      System.Threading.Monitor.Pulse(Me)
    '    End SyncLock

    '  End If

    'End Sub

#End If



#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared EmailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailID, "EmailID", DirectCast(Nothing, Integer?))

      <Display(Name:="EmailID")> _
      Public Property EmailID() As Integer?
        Get
          Return ReadProperty(EmailIDProperty)
        End Get
        Set(ByVal value As Integer?)
          LoadProperty(EmailIDProperty, value)
        End Set
      End Property

      Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailBatchID, "EmailBatchID", DirectCast(Nothing, Integer?))

      <Display(Name:="EmailID")> _
      Public Property EmailBatchID() As Integer?
        Get
          Return ReadProperty(EmailBatchIDProperty)
        End Get
        Set(ByVal value As Integer?)
          LoadProperty(EmailBatchIDProperty, value)
        End Set
      End Property

      Public Shared SentIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.SentInd, "SentInd", DirectCast(Nothing, Boolean?))

      <Display(Name:="SentInd")> _
      Public Property SentInd() As Boolean?
        Get
          Return ReadProperty(SentIndProperty)
        End Get
        Set(ByVal value As Boolean?)
          LoadProperty(SentIndProperty, value)
        End Set
      End Property

      Public Shared FetchAttachmentDataProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FetchAttachmentData, "FetchAttachmentData", False)

      <Display(Name:="FetchAttachmentData")> _
      Public Property FetchAttachmentData() As Boolean
        Get
          Return ReadProperty(FetchAttachmentDataProperty)
        End Get
        Set(ByVal value As Boolean)
          LoadProperty(FetchAttachmentDataProperty, value)
        End Set
      End Property

      Public Property EmailBatchIDs As String = ""

      Public Sub New(EmailBatchIDs As String, FetchAttachmentData As Boolean)
        Me.EmailBatchIDs = EmailBatchIDs
        Me.FetchAttachmentData = FetchAttachmentData
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEmailList() As BatchEmailList

      Return New BatchEmailList()

    End Function

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

Public Shared Sub BeginGetEmailList(ByVal CallBack As EventHandler(Of DataPortalResult(Of EmailList)))

Dim dp As New DataPortal(Of EmailList)
AddHandler dp.FetchCompleted, CallBack
dp.BeginFetch(New Criteria)

End Sub

Public Sub New()

' require use of MobileFormatter

End Sub

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEmailList() As BatchEmailList

      Return DataPortal.Fetch(Of BatchEmailList)(New Criteria)

    End Function

    Public Shared Function GetEmailList(SentInd As Boolean) As BatchEmailList

      Return DataPortal.Fetch(Of BatchEmailList)(New Criteria With {.SentInd = SentInd})

    End Function

    Public Shared Function GetEmailList(EmailID As Integer) As BatchEmailList

      Return DataPortal.Fetch(Of BatchEmailList)(New Criteria With {.EmailID = EmailID})

    End Function

    Public Shared Function GetBatchEmailList(EmailBatchID As Integer, FetchAttachmentData As Boolean) As BatchEmailList

      Return DataPortal.Fetch(Of BatchEmailList)(New Criteria With {.EmailBatchID = EmailBatchID, .FetchAttachmentData = FetchAttachmentData})

    End Function

    Public Shared Function GetBatchEmailList(EmailBatchIDs As List(Of Integer), FetchAttachmentData As Boolean) As BatchEmailList

      Dim xml As String = OBMisc.IntegerListToXML(EmailBatchIDs)

      Return DataPortal.Fetch(Of BatchEmailList)(New Criteria(xml, FetchAttachmentData))

    End Function

    Public Sub New()

      ' require use of factory methods

    End Sub

    Private Sub Fetch(ByVal sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(BatchEmail.GetEmail(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As BatchEmail = Nothing
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.EmailID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.BatchEmailAttachmentList.RaiseListChangedEvents = False
          parent.BatchEmailAttachmentList.Add(BatchEmailAttachment.GetEmailAttachment(sdr))
          parent.BatchEmailAttachmentList.RaiseListChangedEvents = True
        End While
      End If

      Dim parent2 As BatchEmailAttachment = Nothing
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent2) OrElse parent2.DocumentID <> sdr.GetInt32(0) Then
            parent2 = Me.GetEmailAttachmentWithDocumentID(sdr.GetInt32(0))
          End If
          Dim Document As Singular.Documents.Document = Singular.Documents.Document.GetDocument(sdr)
          parent2.LoadDoucment(Document)
        End While
      End If

      For Each child As BatchEmail In Me
        child.CheckRules()
        For Each EmailAttachment As BatchEmailAttachment In child.BatchEmailAttachmentList
          EmailAttachment.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(ByVal criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEmailListBatch"
            cm.Parameters.AddWithValue("@SentInd", Singular.Misc.NothingDBNull(crit.SentInd))
            cm.Parameters.AddWithValue("@EmailID", Singular.Misc.NothingDBNull(crit.EmailID))
            cm.Parameters.AddWithValue("@EmailBatchID", Singular.Misc.NothingDBNull(crit.EmailBatchID))
            cm.Parameters.AddWithValue("@FetchAttachmentData", Singular.Misc.NothingDBNull(crit.FetchAttachmentData))
            cm.Parameters.AddWithValue("@EmailBatchIDs", Singular.Strings.MakeEmptyDBNull(crit.EmailBatchIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Private Function CreateBatchEmailTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("Guid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EmailID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ToEmailAddress", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("FromEmailAddress", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("FriendlyFrom", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Subject", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Body", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SendStatus", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CCEmailAddresses", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ModifiedBy", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DateToSend", GetType(DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SentDate", GetType(DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("NotSentError", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Ignore", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SendOnInsert", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EmailBatchID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SystemGenerated", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DocumentName", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EmailAttachment", GetType(Byte())).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each em As BatchEmail In Me
        Dim row As DataRow = RBTable.NewRow
        row("Guid") = em.Guid
        row("EmailID") = em.EmailID
        row("ToEmailAddress") = em.ToEmailAddress
        row("FromEmailAddress") = em.FromEmailAddress
        row("FriendlyFrom") = em.FriendlyFrom
        row("Subject") = em.Subject
        row("Body") = em.Body
        row("SendStatus") = em.SendStatus
        row("CCEmailAddresses") = em.CCEmailAddresses
        row("ModifiedBy") = OBLib.Security.Settings.CurrentUserID
        row("DateToSend") = Singular.Misc.NothingDBNull(em.DateToSend)
        row("SentDate") = Singular.Misc.NothingDBNull(em.SentDate)
        row("NotSentError") = em.NotSentError
        row("Ignore") = em.Ignore
        row("SendOnInsert") = em.SendOnInsert
        row("EmailBatchID") = em.GetParent().EmailBatchID
        row("SystemGenerated") = em.SystemGenerated
        row("DocumentName") = em.DocumentName
        row("EmailAttachment") = em.EmailAttachment.AttachmentData
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Sub UpdateBulk()

      Dim dt As DataTable = CreateBatchEmailTableParameter()
      Dim cm As New Singular.CommandProc("UpdProcsWeb.updBatchEmailBulk")
      cm.Parameters.AddWithValue("@BatchEmail", dt, SqlDbType.Structured)
      cm.UseTransaction = True
      cm.Execute()

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ''Loop through emails and send if neccessary
        'For Each Child As BatchEmail In Me
        '  If Child.SendOnInsert AndAlso Child.SentDate Is Nothing Then
        '    Child.Send()
        '  End If
        'Next

        ' Loop through each deleted child object and call its Update() method
        For Each Child As BatchEmail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        Me.UpdateBulk()
        'For Each Child As BatchEmail In Me
        '  If Child.IsNew Then
        '    Child.Insert()
        '  Else
        '    Child.Update()
        '  End If
        'Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class


End Namespace