﻿' Generated 21 Sep 2015 10:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Notifications.Email.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Security

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Notifications.SmS.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly

Namespace Notifications.Email

  <Serializable()> _
  Public Class EmailBatch
    Inherits SingularBusinessBase(Of EmailBatch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailBatchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EmailBatchID() As Integer
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name", "")
    ''' <summary>
    ''' Gets and sets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Batch Name is required"),
    StringLength(50, ErrorMessage:="Batch Name cannot be more than 50 characters")>
    Public Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BatchNameProperty, Value)
      End Set
    End Property

    Public Shared BatchDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchDescription, "Batch Description", "")
    ''' <summary>
    ''' Gets and sets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Batch Description is required"),
    StringLength(150, ErrorMessage:="Batch Description cannot be more than 150 characters")>
    Public Property BatchDescription() As String
      Get
        Return GetProperty(BatchDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BatchDescriptionProperty, Value)
      End Set
    End Property

    Public Shared EmailBatchTemplateIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EmailBatchTemplateID, Nothing) _
                                                                              .AddSetExpression("EmailBatchBO.EmailBatchTemplateIDSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Email Batch Template value
    ''' </summary>
    <Display(Name:="Template", Description:=""),
    Required(ErrorMessage:="Template required"),
    DropDownWeb(GetType(OBLib.Notifications.Email.ReadOnly.ROEmailBatchTemplateList), Source:=DropDownWeb.SourceType.CommonData)>
    Public Property EmailBatchTemplateID() As Integer?
      Get
        Return GetProperty(EmailBatchTemplateIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EmailBatchTemplateIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date is required")>
    Public Property StartDate() As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(StartDateProperty, value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date is required")>
    Public Property EndDate() As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(EndDateProperty, value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(UserSystemList), ValueMember:="SystemID", DisplayMember:="SystemName")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemIDProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROSystemAllowedAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(ROProductionAreaAllowedDisciplineList), FilterMethodName:="EmailBatchBO.FilteredDisciplineList", DisplayMember:="Discipline", ValueMember:="DisciplineID")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(DisciplineIDProperty, value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Contract Type", Description:=""),
    DropDownWeb(GetType(Maintenance.HR.ReadOnly.ROContractTypeList))>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ContractTypeIDProperty, value)
      End Set
    End Property

    Public Shared SelectRecipientsManuallyProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.SelectRecipientsManually, False)
    ''' <summary>
    ''' Gets and sets the Batch Description value
    ''' </summary>
    <Display(Name:="Select Recipients Manually")>
    Public Property SelectRecipientsManually() As Boolean
      Get
        Return GetProperty(SelectRecipientsManuallyProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectRecipientsManuallyProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailBatchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.BatchName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Email Batch")
        Else
          Return String.Format("Blank {0}", "Email Batch")
        End If
      Else
        Return Me.BatchName
      End If

    End Function

#End Region

#End Region

#Region " Child Lists "

    Public Shared EmailBatchRecipientListProperty As PropertyInfo(Of EmailBatchRecipientList) = RegisterProperty(Of EmailBatchRecipientList)(Function(c) c.EmailBatchRecipientList, "Email Batch Recipient List")
    Public ReadOnly Property EmailBatchRecipientList() As EmailBatchRecipientList
      Get
        If GetProperty(EmailBatchRecipientListProperty) Is Nothing Then
          LoadProperty(EmailBatchRecipientListProperty, Notifications.Email.EmailBatchRecipientList.NewEmailBatchRecipientList())
        End If
        Return GetProperty(EmailBatchRecipientListProperty)
      End Get
    End Property

    Public Shared BatchEmailListProperty As PropertyInfo(Of BatchEmailList) = RegisterProperty(Of BatchEmailList)(Function(c) c.BatchEmailList, "Email List")
    Public ReadOnly Property BatchEmailList() As BatchEmailList
      Get
        If GetProperty(BatchEmailListProperty) Is Nothing Then
          LoadProperty(BatchEmailListProperty, Notifications.Email.BatchEmailList.NewEmailList())
        End If
        Return GetProperty(BatchEmailListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(SystemIDProperty)
        .ServerRuleFunction = AddressOf SystemIDValid
        .JavascriptRuleFunctionName = "EmailBatchBO.SystemIDValid"
        .AddTriggerProperties({SelectRecipientsManuallyProperty})
      End With

    End Sub

    Public Shared Function SystemIDValid(Instance As EmailBatch) As String

      If Not Instance.SelectRecipientsManually And IsNullNothing(Instance.SystemID) Then
        Return "Sub-Dept is required"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEmailBatch() method.

    End Sub

    Public Shared Function NewEmailBatch() As EmailBatch

      Return DataPortal.CreateChild(Of EmailBatch)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEmailBatch(dr As SafeDataReader) As EmailBatch

      Dim e As New EmailBatch()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EmailBatchIDProperty, .GetInt32(0))
          LoadProperty(BatchNameProperty, .GetString(1))
          LoadProperty(BatchDescriptionProperty, .GetString(2))
          LoadProperty(EmailBatchTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(StartDateProperty, .GetValue(4))
          LoadProperty(EndDateProperty, .GetValue(5))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetValue(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(12))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(SelectRecipientsManuallyProperty, .GetBoolean(14))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEmailBatch"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEmailBatch"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEmailBatchID As SqlParameter = .Parameters.Add("@EmailBatchID", SqlDbType.Int)
          paramEmailBatchID.Value = GetProperty(EmailBatchIDProperty)
          If Me.IsNew Then
            paramEmailBatchID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@BatchName", GetProperty(BatchNameProperty))
          .Parameters.AddWithValue("@BatchDescription", GetProperty(BatchDescriptionProperty))
          .Parameters.AddWithValue("@EmailBatchTemplateID", GetProperty(EmailBatchTemplateIDProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ContractTypeID", Singular.Misc.NothingDBNull(GetProperty(ContractTypeIDProperty)))
          .Parameters.AddWithValue("@SelectRecipientsManually", GetProperty(SelectRecipientsManuallyProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EmailBatchIDProperty, paramEmailBatchID.Value)
          End If
          ' update child objects
          Me.EmailBatchRecipientList.Update()
          Me.BatchEmailList.Update()
          MarkOld()
        End With
      Else
        Me.EmailBatchRecipientList.Update()
        Me.BatchEmailList.Update()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEmailBatch"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EmailBatchID", GetProperty(EmailBatchIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace