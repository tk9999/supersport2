﻿' Generated 21 Sep 2015 10:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email

  <Serializable()> _
  Public Class EmailBatchList
    Inherits SingularBusinessListBase(Of EmailBatchList, EmailBatch)

#Region " Business Methods "

    Public Function GetItem(EmailBatchID As Integer) As EmailBatch

      For Each child As EmailBatch In Me
        If child.EmailBatchID = EmailBatchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetEmail(EmailID As Integer) As Email.BatchEmail

      For Each child As EmailBatch In Me
        For Each email As BatchEmail In child.BatchEmailList
          If email.EmailID = EmailID Then
            Return email
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Function GetEmailAttachment(EmailAttachmentID As Integer) As Email.BatchEmailAttachment

      For Each child As EmailBatch In Me
        For Each email As BatchEmail In child.BatchEmailList
          For Each emailAtt As BatchEmailAttachment In email.BatchEmailAttachmentList
            If emailAtt.EmailAttachmentID = EmailAttachmentID Then
              Return emailAtt
            End If
          Next
        Next
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Email Batches"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property EmailBatchID As Integer?

      Public Sub New(EmailBatchID As Integer?)
        Me.EmailBatchID = EmailBatchID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEmailBatchList() As EmailBatchList

      Return New EmailBatchList()

    End Function

    Public Shared Sub BeginGetEmailBatchList(CallBack As EventHandler(Of DataPortalResult(Of EmailBatchList)))

      Dim dp As New DataPortal(Of EmailBatchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEmailBatchList() As EmailBatchList

      Return DataPortal.Fetch(Of EmailBatchList)(New Criteria())

    End Function

    Public Shared Function GetEmailBatchList(EmailBatchID As Integer?) As EmailBatchList

      Return DataPortal.Fetch(Of EmailBatchList)(New Criteria(EmailBatchID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(EmailBatch.GetEmailBatch(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentBatch As Email.EmailBatch = Nothing
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parentBatch) OrElse parentBatch.EmailBatchID <> sdr.GetInt32(14) Then
            parentBatch = Me.GetItem(sdr.GetInt32(14))
          End If
          parentBatch.BatchEmailList.RaiseListChangedEvents = False
          parentBatch.BatchEmailList.Add(BatchEmail.GetEmail(sdr))
          parentBatch.BatchEmailList.RaiseListChangedEvents = True
        End While
      End If

      Dim parent As BatchEmail = Nothing
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.EmailID <> sdr.GetInt32(1) Then
            parent = Me.GetEmail(sdr.GetInt32(1))
          End If
          parent.BatchEmailAttachmentList.RaiseListChangedEvents = False
          parent.BatchEmailAttachmentList.Add(BatchEmailAttachment.GetEmailAttachment(sdr))
          parent.BatchEmailAttachmentList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parentBatch) OrElse parentBatch.EmailBatchID <> sdr.GetInt32(1) Then
            parentBatch = Me.GetItem(sdr.GetInt32(1))
          End If
          parentBatch.EmailBatchRecipientList.RaiseListChangedEvents = False
          parentBatch.EmailBatchRecipientList.Add(EmailBatchRecipient.GetEmailBatchRecipient(sdr))
          parentBatch.EmailBatchRecipientList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As EmailBatch In Me
        child.CheckAllRules()
        For Each BatchEmail As Email.BatchEmail In child.BatchEmailList
          BatchEmail.CheckAllRules()
          For Each EmailAttachment As BatchEmailAttachment In BatchEmail.BatchEmailAttachmentList
            EmailAttachment.CheckAllRules()
          Next
        Next
        For Each EmailBatchRecipient As EmailBatchRecipient In child.EmailBatchRecipientList
          EmailBatchRecipient.CheckAllRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEmailBatchList"
            cm.Parameters.AddWithValue("@EmailBatchID", NothingDBNull(crit.EmailBatchID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EmailBatch In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EmailBatch In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace