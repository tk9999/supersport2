﻿' Generated 12 Oct 2015 16:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Notifications.Email.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email

  <Serializable()> _
  Public Class SoberEmail
    Inherits OBBusinessBase(Of SoberEmail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EmailID() As Integer
      Get
        Return GetProperty(EmailIDProperty)
      End Get
    End Property

    <Display(Name:="IsBatch", Description:="Body of email")>
    Public ReadOnly Property IsBatch() As Boolean
      Get
        Return EmailBatchID IsNot Nothing
      End Get
    End Property

    Public Shared ToEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ToEmailAddress, "Recipient Addresses", "")
    ''' <summary>
    ''' Gets and sets the To Email Address value
    ''' </summary>
    <Display(Name:="Recipient Addresses", Description:="Email Address to which email should be sent"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="To Email Address is required")>
    Public Property ToEmailAddress() As String
      Get
        Return GetProperty(ToEmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ToEmailAddressProperty, Value)
      End Set
    End Property

    Public Shared FromEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromEmailAddress, "From Email Address", "noreply@supersport.co.za")
    ''' <summary>
    ''' Gets and sets the From Email Address value
    ''' </summary>
    <Display(Name:="From Email Address", Description:="Email address of the sender"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="From Email Address is required"),
    StringLength(100, ErrorMessage:="From Email Address cannot be more than 100 characters")>
    Public Property FromEmailAddress() As String
      Get
        Return GetProperty(FromEmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FromEmailAddressProperty, Value)
      End Set
    End Property

    Public Shared FriendlyFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FriendlyFrom, "Friendly From", "SOBER MS")
    ''' <summary>
    ''' Gets and sets the Friendly From value
    ''' </summary>
    <Display(Name:="Friendly From", Description:="Friendly name of sender"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Friendly From is required"),
    StringLength(50, ErrorMessage:="Friendly From cannot be more than 50 characters")>
    Public Property FriendlyFrom() As String
      Get
        Return GetProperty(FriendlyFromProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FriendlyFromProperty, Value)
      End Set
    End Property

    Public Shared SubjectProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Subject, "Subject", "")
    ''' <summary>
    ''' Gets and sets the Subject value
    ''' </summary>
    <Display(Name:="Subject", Description:="Subject of Email"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Subject is required"),
    StringLength(255, ErrorMessage:="Subject cannot be more than 255 characters")>
    Public Property Subject() As String
      Get
        Return GetProperty(SubjectProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SubjectProperty, Value)
      End Set
    End Property

    Public Shared BodyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Body, "Message", "")
    ''' <summary>
    ''' Gets and sets the Body value
    ''' </summary>
    <Display(Name:="Message", Description:="Message"),
    RichTextField()>
    Public Property Body() As String
      Get
        Return GetProperty(BodyProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BodyProperty, Value)
      End Set
    End Property

    Public Shared CCEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CCEmailAddresses, "CC Addresses", "")
    ''' <summary>
    ''' Gets and sets the CC Email Addresses value
    ''' </summary>
    <Display(Name:="CC Addresses", Description:="Any other employees that email should be sent to")>
    Public Property CCEmailAddresses() As String
      Get
        Return GetProperty(CCEmailAddressesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CCEmailAddressesProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedBy, "Created By", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As String
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DateToSend, "Date To Send")
    ''' <summary>
    ''' Gets and sets the Date To Send value
    ''' </summary>
    <Display(Name:="Date To Send", Description:="Date to send the email (will be 5 minutes after this date)")>
    Public Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DateToSendProperty, Value)
      End Set
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SentDate, "Sent Date")
    ''' <summary>
    ''' Gets and sets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="Date on which email was sent")>
    Public Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SentDateProperty, Value)
      End Set
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error", "")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:="Any errors encountered during sending"),
    StringLength(1024, ErrorMessage:="Not Sent Error cannot be more than 1024 characters")>
    Public Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotSentErrorProperty, Value)
      End Set
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", False)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="Tick indicates that this email will be ignored")>
    Public Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreProperty, Value)
      End Set
    End Property

    Public Shared HTMLBodyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HTMLBody, "Message", "")
    ''' <summary>
    ''' Gets and sets the HTML Body value
    ''' </summary>
    <Display(Name:="Message", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Message is required"),
    TextField(True, False, False, 6)>
    Public Property HTMLBody() As String
      Get
        Return GetProperty(HTMLBodyProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HTMLBodyProperty, Value)
      End Set
    End Property

    Public Shared EmailBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailBatchID, "Email Batch", Nothing)
    ''' <summary>
    ''' Gets and sets the Email Batch value
    ''' </summary>
    <Display(Name:="Email Batch", Description:="")>
    Public Property EmailBatchID() As Integer?
      Get
        Return GetProperty(EmailBatchIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EmailBatchIDProperty, Value)
      End Set
    End Property

    Public Shared SystemGeneratedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemGenerated, "System Generated", True)
    ''' <summary>
    ''' Gets and sets the System Generated value
    ''' </summary>
    <Display(Name:="System Generated", Description:="")>
    Public Property SystemGenerated() As Boolean
      Get
        Return GetProperty(SystemGeneratedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemGeneratedProperty, Value)
      End Set
    End Property

    Public Shared EmailTemplateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EmailTemplateTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Email Template Type value
    ''' </summary>
    <Display(Name:="Template", Description:=""),
    DropDownWeb(GetType(ROEmailTemplateTypeList)),
    SetExpression("SoberEmailBO.EmailTemplateTypeIDSet(self)")>
    Public Property EmailTemplateTypeID() As Integer?
      Get
        Return GetProperty(EmailTemplateTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EmailTemplateTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SendStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SendStatus, "Send Status", "")
    ''' <Summary>
    ''' Gets and sets the CC Email Addresses value
    ''' </Summary>
    <Display(Name:="Send Status")> _
    Public ReadOnly Property SendStatus() As String
      Get
        If SentDate Is Nothing Then
          If NotSentError = "" Then
            Return "Pending"
          Else
            Return "Failed to Send"
          End If
        Else
          If NotSentError = "" Then
            Return "Sent"
          Else
            Return "Unknown"
          End If
        End If
      End Get
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SoberEmailBO.getToString(self)")

#End Region

#Region " Child Lists "

    Public Shared SoberEmailRecipientListProperty As PropertyInfo(Of SoberEmailRecipientList) = RegisterProperty(Of SoberEmailRecipientList)(Function(c) c.SoberEmailRecipientList, "Sms Recipient List")
    Public ReadOnly Property SoberEmailRecipientList() As SoberEmailRecipientList
      Get
        If GetProperty(SoberEmailRecipientListProperty) Is Nothing Then
          LoadProperty(SoberEmailRecipientListProperty, Notifications.Email.SoberEmailRecipientList.NewSoberEmailRecipientList())
        End If
        Return GetProperty(SoberEmailRecipientListProperty)
      End Get
    End Property

    Public Shared SoberEmailAttachmentListProperty As PropertyInfo(Of SoberEmailAttachmentList) = RegisterProperty(Of SoberEmailAttachmentList)(Function(c) c.SoberEmailAttachmentList, "Sms Recipient List")
    Public ReadOnly Property SoberEmailAttachmentList() As SoberEmailAttachmentList
      Get
        If GetProperty(SoberEmailAttachmentListProperty) Is Nothing Then
          LoadProperty(SoberEmailAttachmentListProperty, Notifications.Email.SoberEmailAttachmentList.NewSoberEmailAttachmentList())
        End If
        Return GetProperty(SoberEmailAttachmentListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Subject.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sober Email")
        Else
          Return String.Format("Blank {0}", "Sober Email")
        End If
      Else
        Return Me.Subject
      End If

    End Function

    Public Function GenerateEmailListFromTemplate() As SoberEmailList

      Dim GeneratedList As New SoberEmailList
      Dim Recipients As List(Of String) = ToEmailAddress.Split(";").ToList
      If Recipients.Count > 0 Then
        For Each smsRecip As String In Recipients
          Dim NewEmail As New SoberEmail
          NewEmail.Body = Me.Body.Replace("{RecipientName}", smsRecip)
          NewEmail.HTMLBody = Me.HTMLBody.Replace("{RecipientName}", smsRecip)
          NewEmail.DateToSend = Me.DateToSend
          NewEmail.Ignore = Me.Ignore
          NewEmail.EmailTemplateTypeID = Me.EmailTemplateTypeID
          NewEmail.ToEmailAddress = smsRecip
          NewEmail.SystemGenerated = False
          NewEmail.Subject = Me.Subject
          GeneratedList.Add(NewEmail)
        Next
      End If
      Return GeneratedList

    End Function

    Sub PrepareForSaveAndSend()
      If Me.Ignore Then
        Me.Ignore = False
        'For Each recip As OBLib.Notifications.SmS.SmsRecipient In Me.SmsRecipientList
        '  If recip.SentDate Is Nothing Or recip.StatusCode = "" Then
        '    recip.SentDate = Now
        '  End If
        'Next
      End If
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSoberEmail() method.

    End Sub

    Public Shared Function NewSoberEmail() As SoberEmail

      Return DataPortal.CreateChild(Of SoberEmail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSoberEmail(dr As SafeDataReader) As SoberEmail

      Dim s As New SoberEmail()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EmailIDProperty, .GetInt32(0))
          LoadProperty(ToEmailAddressProperty, .GetString(1))
          LoadProperty(FromEmailAddressProperty, .GetString(2))
          LoadProperty(FriendlyFromProperty, .GetString(3))
          LoadProperty(SubjectProperty, .GetString(4))
          LoadProperty(BodyProperty, .GetString(5))
          LoadProperty(CCEmailAddressesProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetString(7))
          LoadProperty(CreatedDateProperty, .GetSmartDate(8))
          LoadProperty(DateToSendProperty, .GetValue(9))
          LoadProperty(SentDateProperty, .GetValue(10))
          LoadProperty(NotSentErrorProperty, .GetString(11))
          LoadProperty(IgnoreProperty, .GetBoolean(12))
          LoadProperty(HTMLBodyProperty, .GetString(13))
          LoadProperty(EmailBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(SystemGeneratedProperty, .GetBoolean(15))
          LoadProperty(EmailTemplateTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSoberEmail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSoberEmail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure
          Dim paramEmailID As SqlParameter = .Parameters.Add("@EmailID", SqlDbType.Int)
          paramEmailID.Value = GetProperty(EmailIDProperty)
          If Me.IsNew Then
            paramEmailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ToEmailAddress", GetProperty(ToEmailAddressProperty))
          .Parameters.AddWithValue("@FromEmailAddress", GetProperty(FromEmailAddressProperty))
          .Parameters.AddWithValue("@FriendlyFrom", GetProperty(FriendlyFromProperty))
          .Parameters.AddWithValue("@Subject", GetProperty(SubjectProperty))
          .Parameters.AddWithValue("@Body", GetProperty(BodyProperty))
          .Parameters.AddWithValue("@CCEmailAddresses", GetProperty(CCEmailAddressesProperty))
          .Parameters.AddWithValue("@CreatedBy", OBLib.Security.Settings.CurrentUser.LoginName)
          .Parameters.AddWithValue("@DateToSend", (New SmartDate(GetProperty(DateToSendProperty))).DBValue)
          .Parameters.AddWithValue("@SentDate", (New SmartDate(GetProperty(SentDateProperty))).DBValue)
          .Parameters.AddWithValue("@NotSentError", GetProperty(NotSentErrorProperty))
          .Parameters.AddWithValue("@Ignore", GetProperty(IgnoreProperty))
          .Parameters.AddWithValue("@HTMLBody", GetProperty(HTMLBodyProperty))
          .Parameters.AddWithValue("@EmailBatchID", Singular.Misc.NothingDBNull(GetProperty(EmailBatchIDProperty)))
          .Parameters.AddWithValue("@SystemGenerated", GetProperty(SystemGeneratedProperty))
          .Parameters.AddWithValue("@EmailTemplateTypeID", Singular.Misc.NothingDBNull(GetProperty(EmailTemplateTypeIDProperty)))
          .Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
          .CommandTimeout = 0
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EmailIDProperty, paramEmailID.Value)
          End If
          ' update child objects
          SoberEmailRecipientList.Update()
          SoberEmailAttachmentList.Update()
          MarkOld()
        End With
      Else
        SoberEmailRecipientList.Update()
        SoberEmailAttachmentList.Update()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSoberEmail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EmailID", GetProperty(EmailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace