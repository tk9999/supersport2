﻿' Generated 12 Oct 2015 16:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Email

  <Serializable()> _
  Public Class EmailTemplateType
    Inherits OBBusinessBase(Of EmailTemplateType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailTemplateTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailTemplateTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EmailTemplateTypeID() As Integer
      Get
        Return GetProperty(EmailTemplateTypeIDProperty)
      End Get
    End Property

    Public Shared EmailTemplateTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailTemplateType, "Email Template Type", "")
    ''' <summary>
    ''' Gets the Email Template Type value
    ''' </summary>
    <Display(Name:="Email Template Type", Description:=""),
    StringLength(50, ErrorMessage:="Email Template Type cannot be more than 50 characters")>
  Public ReadOnly Property EmailTemplateType() As String
      Get
        Return GetProperty(EmailTemplateTypeProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public Shared MessageTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MessageTemplate, "Message Template", "")
    ''' <summary>
    ''' Gets the Message Template value
    ''' </summary>
    <Display(Name:="Message Template", Description:="")>
  Public ReadOnly Property MessageTemplate() As String
      Get
        Return GetProperty(MessageTemplateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailTemplateTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EmailTemplateType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Email Template Type")
        Else
          Return String.Format("Blank {0}", "Email Template Type")
        End If
      Else
        Return Me.EmailTemplateType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEmailTemplateType() method.

    End Sub

    Public Shared Function NewEmailTemplateType() As EmailTemplateType

      Return DataPortal.CreateChild(Of EmailTemplateType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEmailTemplateType(dr As SafeDataReader) As EmailTemplateType

      Dim e As New EmailTemplateType()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EmailTemplateTypeIDProperty, .GetInt32(0))
          LoadProperty(EmailTemplateTypeProperty, .GetString(1))
          LoadProperty(SystemIndProperty, .GetBoolean(2))
          LoadProperty(MessageTemplateProperty, .GetString(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEmailTemplateType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEmailTemplateType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEmailTemplateTypeID As SqlParameter = .Parameters.Add("@EmailTemplateTypeID", SqlDbType.Int)
          paramEmailTemplateTypeID.Value = GetProperty(EmailTemplateTypeIDProperty)
          If Me.IsNew Then
            paramEmailTemplateTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EmailTemplateType", GetProperty(EmailTemplateTypeProperty))
          .Parameters.AddWithValue("@SystemInd", GetProperty(SystemIndProperty))
          .Parameters.AddWithValue("@MessageTemplate", GetProperty(MessageTemplateProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EmailTemplateTypeIDProperty, paramEmailTemplateTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEmailTemplateType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EmailTemplateTypeID", GetProperty(EmailTemplateTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace