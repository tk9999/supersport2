﻿' Generated 12 Oct 2015 16:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.Email

  <Serializable()> _
  Public Class SoberEmailRecipientList
    Inherits OBBusinessListBase(Of SoberEmailRecipientList, SoberEmailRecipient)

#Region " Business Methods "

    Public Function GetItem(EmailID As Integer) As SoberEmailRecipient

      For Each child As SoberEmailRecipient In Me
        If child.EmailID = EmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Sub RemoveClean(Email As SoberEmailRecipient)
      Me.Remove(Email)
      DeletedList.Remove(Email)
    End Sub

    Public Overrides Function ToString() As String

      Return "Emails"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property EmailIDsXml As String = ""

      Public Sub New(EmailIDsXml As String)
        Me.EmailIDsXml = EmailIDsXml
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSoberEmailRecipientList() As SoberEmailRecipientList

      Return New SoberEmailRecipientList()

    End Function

    Public Shared Sub BeginGetSoberEmailRecipientList(CallBack As EventHandler(Of DataPortalResult(Of SoberEmailRecipientList)))

      Dim dp As New DataPortal(Of SoberEmailRecipientList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSoberEmailRecipientList() As SoberEmailRecipientList

      Return DataPortal.Fetch(Of SoberEmailRecipientList)(New Criteria())

    End Function

    Public Shared Function GetSoberEmailRecipientList(EmailIDs As List(Of Integer)) As SoberEmailRecipientList

      Dim EmailIDsXml As String = OBLib.OBMisc.IntegerListToXML(EmailIDs)

      Return DataPortal.Fetch(Of SoberEmailRecipientList)(New Criteria(EmailIDsXml))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SoberEmailRecipient.GetSoberEmailRecipient(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSoberEmailRecipientList"
            cm.Parameters.AddWithValue("@EmailIDsXML", Strings.MakeEmptyDBNull(crit.EmailIDsXml))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SoberEmailRecipient In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SoberEmailRecipient In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace