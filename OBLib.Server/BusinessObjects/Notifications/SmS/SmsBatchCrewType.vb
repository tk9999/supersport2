﻿' Generated 02 Sep 2016 10:22 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchCrewType
    Inherits OBBusinessBase(Of SmsBatchCrewType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchCrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchCrewTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SmsBatchCrewTypeID() As Integer
      Get
        Return GetProperty(SmsBatchCrewTypeIDProperty)
      End Get
    End Property

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchID, "Sms Batch", 0)
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Sms Batch", Description:=""),
    Required(ErrorMessage:="Sms Batch required")>
    Public Property SmsBatchID() As Integer
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsBatchIDProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "CrewTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property CrewTypeID As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CrewTypeProperty, Value)
      End Set
    End Property

    Public Shared CallTimeBaseProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CallTimeBase, "Call Time @Base", "")
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Call Time @Base", Description:="")>
    Public Property CallTimeBase() As String
      Get
        Return GetProperty(CallTimeBaseProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CallTimeBaseProperty, Value)
      End Set
    End Property

    Public Shared CallTimeVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CallTimeVenue, "Call Time @Venue", "")
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Call Time @Venue", Description:="")>
    Public Property CallTimeVenue() As String
      Get
        Return GetProperty(CallTimeVenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CallTimeVenueProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchCrewTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SmsBatchCrewTypeID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Batch Date")
        Else
          Return String.Format("Blank {0}", "Sms Batch Date")
        End If
      Else
        Return Me.SmsBatchCrewTypeID.ToString()
      End If

    End Function

    Public Function GetParent() As SmsBatch
      Return CType(CType(Me.Parent, SmsBatchCrewTypeList).Parent, SmsBatch)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsBatchCrewType() method.

    End Sub

    Public Shared Function NewSmsBatchCrewType() As SmsBatchCrewType

      Return DataPortal.CreateChild(Of SmsBatchCrewType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSmsBatchCrewType(dr As SafeDataReader) As SmsBatchCrewType

      Dim s As New SmsBatchCrewType()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsBatchCrewTypeIDProperty, .GetInt32(0))
          LoadProperty(SmsBatchIDProperty, .GetInt32(1))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CrewTypeProperty, .GetString(3))
          LoadProperty(CallTimeBaseProperty, .GetString(4))
          LoadProperty(CallTimeVenueProperty, .GetString(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SmsBatchCrewTypeIDProperty)

      cm.Parameters.AddWithValue("@SmsBatchID", Me.GetParent.SmsBatchID)
      cm.Parameters.AddWithValue("@CrewTypeID", GetProperty(CrewTypeIDProperty))
      cm.Parameters.AddWithValue("@CallTimeBase", GetProperty(CallTimeBaseProperty))
      cm.Parameters.AddWithValue("@CallTimeVenue", GetProperty(CallTimeVenueProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SmsBatchCrewTypeIDProperty, cm.Parameters("@SmsBatchCrewTypeID").Value)
               End If
             End Sub

    End Function

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsBatchCrewType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsBatchCrewType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SmsBatchCrewTypeID", GetProperty(SmsBatchCrewTypeIDProperty))
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsBatchCrewType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsBatchCrewTypeID", GetProperty(SmsBatchCrewTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

#End Region

  End Class

End Namespace