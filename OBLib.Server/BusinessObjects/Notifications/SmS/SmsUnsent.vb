﻿' Generated 02 Apr 2016 16:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.SmsSending

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.SmS

  <Serializable()>
  Public Class SmsUnsent
    Inherits OBBusinessBase(Of SmsUnsent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsRecipientID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property SmsRecipientID() As Integer
      Get
        Return GetProperty(SmsRecipientIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsRecipientIDProperty, Value)
      End Set
    End Property

    Public Shared CellNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellNo, "Cell No")
    ''' <summary>
    ''' Gets and sets the Cell No value
    ''' </summary>
    <Display(Name:="Cell No", Description:="")>
    Public Property CellNo() As String
      Get
        Return GetProperty(CellNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellNoProperty, Value)
      End Set
    End Property

    Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RecipientName, "Recipient Name")
    ''' <summary>
    ''' Gets and sets the Recipient Name value
    ''' </summary>
    <Display(Name:="Recipient Name", Description:="")>
    Public Property RecipientName() As String
      Get
        Return GetProperty(RecipientNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RecipientNameProperty, Value)
      End Set
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.SentDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="")>
    Public Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SentDateProperty, Value)
      End Set
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status Code")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Status Code", Description:="")>
    Public Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCodeProperty, Value)
      End Set
    End Property

    Public Shared StatusDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDescription, "Status Description")
    ''' <summary>
    ''' Gets and sets the Status Description value
    ''' </summary>
    <Display(Name:="Status Description", Description:="")>
    Public Property StatusDescription() As String
      Get
        Return GetProperty(StatusDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusDescriptionProperty, Value)
      End Set
    End Property

    Public Shared MessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Message, "Message")
    ''' <summary>
    ''' Gets and sets the Message value
    ''' </summary>
    <Display(Name:="Message", Description:="")>
    Public Property Message() As String
      Get
        Return GetProperty(MessageProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MessageProperty, Value)
      End Set
    End Property

    Public Shared SmsesRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsesRequired, "Smses Required")
    ''' <summary>
    ''' Gets and sets the Smses Required value
    ''' </summary>
    <Display(Name:="Smses Required", Description:=""),
    Required(ErrorMessage:="Smses Required required")>
    Public Property SmsesRequired() As Integer
      Get
        Return GetProperty(SmsesRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsesRequiredProperty, Value)
      End Set
    End Property

    Public Shared SmSBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmSBatchID, "Sm S Batch")
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Sm S Batch", Description:=""),
    Required(ErrorMessage:="Sm S Batch required")>
    Public Property SmSBatchID() As Integer
      Get
        Return GetProperty(SmSBatchIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmSBatchIDProperty, Value)
      End Set
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error", "")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:=""),
    StringLength(1024, ErrorMessage:="Not Sent Error cannot be more than 1024 characters")>
    Public Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotSentErrorProperty, Value)
      End Set
    End Property

    Public Shared RemainingRetriesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RemainingRetries, "Sm S Batch", 3)
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Sm S Batch", Description:="")>
    Public Property RemainingRetries() As Integer
      Get
        Return GetProperty(RemainingRetriesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RemainingRetriesProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsRecipientIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CellNo.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Unsent")
        Else
          Return String.Format("Blank {0}", "Sms Unsent")
        End If
      Else
        Return Me.CellNo
      End If

    End Function

    Private mBatchID As String = ""
    Private mFrom As String = ""
    Private mOnComplete As Action
    Friend mHasComplete As Boolean = False
    Private Shared mSmsQueue As New Queue
    Private Shared mSendingCount As Integer = 0

    'Public Sub SendSober(Optional ByVal From As String = "", Optional BatchID As String = "", Optional OnComplete As Action = Nothing)

    '  mBatchID = BatchID
    '  mFrom = [From]
    '  mOnComplete = OnComplete

    '  If OnComplete Is Nothing Then
    '    'Send syncronously
    '    SendInternalSober(Me)
    '  Else
    '    SyncLock mSmsQueue
    '      mSmsQueue.Enqueue(Me)
    '    End SyncLock
    '    CheckSendQueue()
    '  End If

    'End Sub

    Private Shared Sub SendInternal(SmsRecipient As SmsUnsent)

      Try

        Dim Result As SmsResult
        'Check if there is a batch id.
        If SmsRecipient.mBatchID = "" Then
          Result = SmsSender.SendSms(SmsRecipient.CellNo, SmsRecipient.Message)
        Else
          Result = ClickatellSender.QuickSend(SmsRecipient.mBatchID, SmsRecipient.CellNo)
        End If

        If Result.Sent Then
          SmsRecipient.SentDate = Now()
        Else
          SmsRecipient.NotSentError = Result.ErrorMessage
        End If

      Catch ex As Exception
        SmsRecipient.NotSentError = Singular.Debug.RecurseExceptionMessage(ex)
      End Try

    End Sub

    Public Function SendSmsWithMessageID(SessionID As String, ApiID As Integer, Username As String, Password As String, From As String, CallbackStatusDetailLevel As SmsSending.ClickatellCallbackStatusDetailLevel,
                                         Optional Escalate As Integer = Nothing) As SmsResult

      Dim mWebService = New PushServerWS
      Dim partsCount As Integer = 0
      If Message.Length > 160 Then
        ' we need to concatenate
        If (Message.Length / 153) > 10 Then
          Return New SmsResult(False, "A message cannot be longer than " & (153 * 10) & " Characters")
        Else
          partsCount = (Message.Length \ 153) + 1
        End If
      End If

      'Dim mSessionMessage As String = mWebService.auth(ApiID, Username, Password)
      'If Me.RemainingRetries = 0 Then
      '  mWebService.delmsg(SessionID, ApiID, Username, Password, "", SmsRecipientID.ToString)
      'Else
      Dim Result As String() = mWebService.sendmsg(SessionID, ApiID, Username, Password, {CellNo}, "", Me.Message, partsCount, Nothing, CallbackStatusDetailLevel, Nothing, Nothing, Nothing, Nothing, Escalate, Nothing, SmsRecipientID.ToString, Nothing, Nothing, Nothing, Nothing, Nothing)
      If Result.Length > 0 AndAlso Result(0).StartsWith("ID") Then
        Return New SmsResult(True, "")
      Else
        Return New SmsResult(False, If(Result.Length = 0, "Unknown", Result(0)))
      End If
      'End If

    End Function

    Public Sub SendInternalSober(SessionID As String, ApiID As Integer, Username As String, Password As String, From As String, CallbackStatusDetailLevel As SmsSending.ClickatellCallbackStatusDetailLevel,
                                         Optional Escalate As Integer? = Nothing)

      Dim Result As SmsResult
      Result = Me.SendSmsWithMessageID(SessionID, ApiID, Username, Password, From, CallbackStatusDetailLevel, Escalate)
      If Result.Sent Then
        Me.SentDate = Now()
      End If

      If Me.NotSentError <> "" Then
        'remove spaces and tabs
        Me.CellNo = Me.CellNo.Replace(" ", "")
        Me.CellNo = Me.CellNo.Replace(vbTab, "")
        'retry
        Me.SendSmsWithMessageID(SessionID, ApiID, Username, Password, From, CallbackStatusDetailLevel, Escalate)
        If Me.NotSentError <> "" Then
          OBLib.OBMisc.LogClientError("SmsUnsent", "SendInternalSober", "Send Failed", Me.NotSentError)
        End If
        'If Me.NotSentError.Contains("Invalid Destination Address") Then
        '  ''If it still errors
        '  'If Me.NotSentError <> "" Then
        '  '  'modify the number to have the SA country code in front
        '  '  If Me.CellNo.StartsWith("0") Then
        '  '    Me.CellNo = Me.CellNo.Substring(1, Me.CellNo.Length - 1)
        '  '    Me.CellNo = "27" & Me.CellNo
        '  '  End If
        '  '  Me.SendSmsWithMessageID(SessionID, ApiID, Username, Password, From, CallbackStatusDetailLevel)
        '  '  'If it still errors
        '  '  If Me.NotSentError <> "" Then
        '  '    OBLib.OBMisc.LogClientError("SmsUnsent", "SendInternalSober", "Invalid Destination Address", Me.NotSentError)
        '  '  End If
        '  'End If
        'ElseIf Me.NotSentError.Contains("Authentication failed") Then
        '  'If it still errors
        '  If Me.NotSentError <> "" Then
        '    OBLib.OBMisc.LogClientError("SmsUnsent", "SendInternalSober", "Authentication Failed", Me.NotSentError)
        '  End If
        'End If
      End If

    End Sub

    Private Shared Sub CheckSendQueue()

      'Send asyncronously, but only 10 threads at a time.
      SyncLock mSmsQueue
        If mSendingCount >= 10 OrElse mSmsQueue.Count = 0 Then
          Exit Sub
        End If

        mSendingCount += 1
        Dim smsr As SmsUnsent = mSmsQueue.Dequeue

        Dim AsyncMethod As Action(Of SmsUnsent) = AddressOf SendInternal
        AsyncMethod.BeginInvoke(smsr,
          Sub(args)

            smsr.mHasComplete = True
            smsr.mOnComplete()

            SyncLock mSmsQueue
              mSendingCount -= 1
              CheckSendQueue()
            End SyncLock

          End Sub, Nothing)

      End SyncLock

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsUnsent() method.

    End Sub

    Public Shared Function NewSmsUnsent() As SmsUnsent

      Return DataPortal.CreateChild(Of SmsUnsent)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSmsUnsent(dr As SafeDataReader) As SmsUnsent

      Dim s As New SmsUnsent()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsRecipientIDProperty, .GetInt32(0))
          LoadProperty(CellNoProperty, .GetString(1))
          LoadProperty(RecipientNameProperty, .GetString(2))
          LoadProperty(SentDateProperty, .GetValue(3))
          LoadProperty(StatusCodeProperty, .GetString(4))
          LoadProperty(StatusDescriptionProperty, .GetString(5))
          LoadProperty(MessageProperty, .GetString(6))
          LoadProperty(SmsesRequiredProperty, .GetInt32(7))
          LoadProperty(SmSBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(NotSentErrorProperty, .GetString(9))
          LoadProperty(RemainingRetriesProperty, .GetInt32(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsUnsent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsUnsent"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsRecipientID As SqlParameter = .Parameters.Add("@SmsRecipientID", SqlDbType.Int)
          paramSmsRecipientID.Value = GetProperty(SmsRecipientIDProperty)
          If Me.IsNew Then
            paramSmsRecipientID.Direction = ParameterDirection.Output
          End If
          '.Parameters.AddWithValue("@CellNo", GetProperty(CellNoProperty))
          '.Parameters.AddWithValue("@RecipientName", GetProperty(RecipientNameProperty))
          .Parameters.AddWithValue("@SentDate", (New SmartDate(GetProperty(SentDateProperty))).DBValue)
          '.Parameters.AddWithValue("@StatusCode", GetProperty(StatusCodeProperty))
          '.Parameters.AddWithValue("@StatusDescription", GetProperty(StatusDescriptionProperty))
          '.Parameters.AddWithValue("@Message", GetProperty(MessageProperty))
          '.Parameters.AddWithValue("@SmsesRequired", GetProperty(SmsesRequiredProperty))
          '.Parameters.AddWithValue("@SmSBatchID", GetProperty(SmSBatchIDProperty))
          .Parameters.AddWithValue("@NotSentError", GetProperty(NotSentErrorProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsRecipientIDProperty, paramSmsRecipientID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsUnsent"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsRecipientID", GetProperty(SmsRecipientIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace