﻿' Generated 12 Jun 2015 07:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.SmsSending
Imports Singular.DataAnnotations
Imports OBLib.Notifications.SmS.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.SmS

  <Serializable()> _
  Public Class Sms
    Inherits SingularBusinessBase(Of Sms)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SmsID() As Integer
      Get
        Return GetProperty(SmsIDProperty)
      End Get
    End Property

    Public Shared MessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Message, "Message", "")
    ''' <summary>
    ''' Gets and sets the Message value
    ''' </summary>
    <Display(Name:="Message", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Message is required"),
    StringLength(1024, ErrorMessage:="Message cannot be more than 459 characters"),
    Singular.DataAnnotations.TextField(True, True, True),
    SetExpression("SmsBO.MessageSet(self)")>
    Public Property Message() As String
      Get
        Return GetProperty(MessageProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MessageProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDate, "Created Date", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime?
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.DateToSend, Nothing)
    ''' <summary>
    ''' Gets and sets the Date To Send value
    ''' </summary>
    <Display(Name:="Date and Time to send"),
    Singular.DataAnnotations.DateAndTimeField>
    Public Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DateToSendProperty, Value)
      End Set
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", True)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="")>
    Public Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreProperty, Value)
      End Set
    End Property

    Public Shared SmsesRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsesRequired, "Smses", 1)
    ''' <summary>
    ''' Gets and sets the Smses Required value
    ''' </summary>
    <Display(Name:="Smses", Description:="")>
    Public Property SmsesRequired() As Integer
      Get
        Return GetProperty(SmsesRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsesRequiredProperty, Value)
      End Set
    End Property

    Public Shared SmSBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmSBatchID, "Sm S Batch", Nothing)
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="SmS Batch", Description:="")>
    Public Property SmSBatchID() As Integer?
      Get
        Return GetProperty(SmSBatchIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SmSBatchIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SmsTemplateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SmsTemplateTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Message Template", Description:=""),
    DropDownWeb(GetType(ROSmsTemplateTypeList), UnselectedText:="Message Template to use..."),
    SetExpression("SmsBO.SmsTemplateTypeIDSet(self)")>
    Public Property SmsTemplateTypeID() As Integer?
      Get
        Return GetProperty(SmsTemplateTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SmsTemplateTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SystemGeneratedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemGenerated, "SystemGenerated", False)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="SystemGenerated", Description:="")>
    Public Property SystemGenerated() As Boolean
      Get
        Return GetProperty(SystemGeneratedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemGeneratedProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SmsRecipientListProperty As PropertyInfo(Of SmsRecipientList) = RegisterProperty(Of SmsRecipientList)(Function(c) c.SmsRecipientList, "Sms Recipient List")

    Public ReadOnly Property SmsRecipientList() As SmsRecipientList
      Get
        If GetProperty(SmsRecipientListProperty) Is Nothing Then
          LoadProperty(SmsRecipientListProperty, Notifications.SmS.SmsRecipientList.NewSmsRecipientList())
        End If
        Return GetProperty(SmsRecipientListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Message.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms")
        Else
          Return String.Format("Blank {0}", "Sms")
        End If
      Else
        Return Me.Message
      End If

    End Function

    Public Shared Sub SendImmediate(SmsID As Integer)

      Dim sl As SmsList = Notifications.SmS.SmsList.GetSmsList(SmsID)
      If sl.Count = 1 Then
        sl(0).SendAndSave()
      End If

    End Sub

    ''' <summary>
    ''' Sends the email and saves it asyncronously. If the send fails, it will be saved with the not sent error.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SendAndSave()
      Send(, Sub()

               Try
                 Dim sl As SmsList = SmsList.NewSmsList
                 sl.Add(Me)
                 sl.Save()
               Catch ex As Exception
               End Try

             End Sub)
    End Sub

    Public Sub Send(Optional ByVal From As String = "", Optional OnComplete As Action = Nothing)
      Dim BatchID As String = ""

      If Me.SmsRecipientList.Count > 1 AndAlso SmsSender.SMSProvider = SMSProviderType.Clickatell Then
        'Start a batch because you can send the message once, and then just specify each number to send to after that.
        BatchID = ClickatellSender.StartBatch(Message)
      End If

      'This method will check if the Batch ID is filled in.
      For Each smsr As SmsRecipient In Me.SmsRecipientList
        smsr.Send([From], BatchID, OnComplete)
      Next
    End Sub

    ''' <summary>
    ''' Sends to all the recipients at once, this way you will not be able to tell which numbers fail.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SendAll()
      If SmsSender.SendSms(GetNumbers, Message).Sent Then
        For Each sr As SmsRecipient In Me.SmsRecipientList
          sr.SentDate = Now
        Next
      End If
    End Sub

    Public Sub AddRecipient(CellNo As String, Optional RecipientName As String = "")
      Dim smsr As SmsRecipient = SmsRecipient.NewSmsRecipient()
      smsr.CellNo = CellNo
      smsr.RecipientName = RecipientName
      SmsRecipientList.Add(smsr)
    End Sub

    Public Function GetRecipentsAndNumbersAsString() As String

      Dim sReturn As String = ""
      For Each Recipient As String In Me.GetRecipentsAndNumbers
        If sReturn <> "" Then
          sReturn &= "; "
        End If

        sReturn &= Recipient
      Next
      Return sReturn

    End Function

    Public Function GetRecipentsAndNumbers() As String()

      Dim sReturn(SmsRecipientList.Count - 1) As String

      For i As Integer = 0 To sReturn.Length - 1
        sReturn(i) = SmsRecipientList(i).RecipientName & ": " & SmsRecipientList(i).CellNo
      Next

      Return sReturn

    End Function

    Public Function GetNumbers() As String()

      Dim sReturn(SmsRecipientList.Count - 1) As String

      For i As Integer = 0 To sReturn.Length - 1
        sReturn(i) = SmsRecipientList(i).CellNo
      Next

      Return sReturn

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SmsRecipients"}
      End Get
    End Property

    Sub PrepareForSaveAndSend()
      If Me.Ignore Then
        Me.Ignore = False
        For Each recip As OBLib.Notifications.SmS.SmsRecipient In Me.SmsRecipientList
          If recip.SentDate Is Nothing Or recip.StatusCode = "" Then
            recip.SentDate = Now
          End If
        Next
      End If
    End Sub

    Public Function GenerateSmsListFromTemplate() As SmsList

      Dim GeneratedList As New SmsList
      For Each smsRecip As SmsRecipient In Me.SmsRecipientList
        Dim NewSms As New Sms
        Dim NewSmsRecip As SmsRecipient = smsRecip.Clone()
        NewSms.Message = Me.Message
        NewSms.Message = NewSms.Message.Replace("{RecipientFirstName}", smsRecip.RecipientName)
        NewSms.Message = NewSms.Message.Replace("{RecipientName}", smsRecip.RecipientName)
        NewSms.DateToSend = Me.DateToSend
        NewSms.Ignore = Me.Ignore
        NewSms.SmsesRequired = Me.SmsesRequired
        NewSms.SmsTemplateTypeID = Me.SmsTemplateTypeID
        If NewSms.DateToSend IsNot Nothing Then
          Me.Ignore = False
          NewSms.Ignore = False
          NewSmsRecip.Ignore = False
        End If
        NewSmsRecip.DateToSend = NewSms.DateToSend
        NewSms.SmsRecipientList.Add(NewSmsRecip)
        GeneratedList.Add(NewSms)
      Next
      Return GeneratedList

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSms() method.

    End Sub

    Public Shared Function NewSms() As Sms

      Return DataPortal.CreateChild(Of Sms)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSms(dr As SafeDataReader) As Sms

      Dim s As New Sms()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsIDProperty, .GetInt32(0))
          LoadProperty(MessageProperty, .GetString(1))
          LoadProperty(CreatedDateProperty, .GetValue(2))
          LoadProperty(DateToSendProperty, .GetValue(3))
          LoadProperty(IgnoreProperty, .GetBoolean(4))
          LoadProperty(SmsesRequiredProperty, .GetInt32(5))
          LoadProperty(SmSBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(9))
          LoadProperty(SmsTemplateTypeIDProperty, ZeroNothing(.GetInt32(10)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSms"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSms"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsID As SqlParameter = .Parameters.Add("@SmsID", SqlDbType.Int)
          paramSmsID.Value = GetProperty(SmsIDProperty)
          If Me.IsNew Then
            paramSmsID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Message", GetProperty(MessageProperty))
          .Parameters.AddWithValue("@DateToSend", (New SmartDate(GetProperty(DateToSendProperty))).DBValue)
          .Parameters.AddWithValue("@Ignore", GetProperty(IgnoreProperty))
          .Parameters.AddWithValue("@SmsesRequired", GetProperty(SmsesRequiredProperty))
          .Parameters.AddWithValue("@SmSBatchID", Singular.Misc.NothingDBNull(GetProperty(SmSBatchIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SmsTemplateTypeID", NothingDBNull(GetProperty(SmsTemplateTypeIDProperty)))
          .Parameters.AddWithValue("@SystemGenerated", GetProperty(SystemGeneratedProperty))
          .CommandTimeout = 120
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsIDProperty, paramSmsID.Value)
          End If
          ' update child objects
          If GetProperty(SmsRecipientListProperty) IsNot Nothing Then
            Me.SmsRecipientList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(SmsRecipientListProperty) IsNot Nothing Then
          Me.SmsRecipientList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSms"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsID", GetProperty(SmsIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace