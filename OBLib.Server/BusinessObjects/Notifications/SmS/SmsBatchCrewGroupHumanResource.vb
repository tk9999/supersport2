﻿' Generated 11 May 2017 07:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchCrewGroupHumanResource
    Inherits OBBusinessBase(Of SmsBatchCrewGroupHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SmsBatchCrewGroupHumanResourceBO.SmsBatchCrewGroupHumanResourceBOToString(self)")

    Public Shared SmsBatchCrewGroupHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchCrewGroupHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property SmsBatchCrewGroupHumanResourceID() As Integer
      Get
        Return GetProperty(SmsBatchCrewGroupHumanResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SmsBatchCrewGroupHumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared SmsBatchCrewGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmsBatchCrewGroupID, "Sms Batch Crew Group", Nothing)
    ''' <summary>
    ''' Gets the Sms Batch Crew Group value
    ''' </summary>
    Public Property SmsBatchCrewGroupID() As Integer?
      Get
        Return GetProperty(SmsBatchCrewGroupIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SmsBatchCrewGroupIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the HRName value
    ''' </summary>
    <Display(Name:="Crew Type", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRNameProperty, Value)
      End Set
    End Property

    Public Shared ImageFileNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageFileName, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the ImageFileName value
    ''' </summary>
    <Display(Name:="Crew Type", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property ImageFileName() As String
      Get
        Return GetProperty(ImageFileNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImageFileNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SmsBatchCrewGroup

      Return CType(CType(Me.Parent, SmsBatchCrewGroupHumanResourceList).Parent, SmsBatchCrewGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchCrewGroupHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Batch Crew Group Human Resource")
        Else
          Return String.Format("Blank {0}", "Sms Batch Crew Group Human Resource")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsBatchCrewGroupHumanResource() method.

    End Sub

    Public Shared Function NewSmsBatchCrewGroupHumanResource() As SmsBatchCrewGroupHumanResource

      Return DataPortal.CreateChild(Of SmsBatchCrewGroupHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSmsBatchCrewGroupHumanResource(dr As SafeDataReader) As SmsBatchCrewGroupHumanResource

      Dim s As New SmsBatchCrewGroupHumanResource()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsBatchCrewGroupHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(SmsBatchCrewGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(HRNameProperty, .GetString(7))
          LoadProperty(ImageFileNameProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SmsBatchCrewGroupHumanResourceIDProperty)

      cm.Parameters.AddWithValue("@SmsBatchCrewGroupID", Me.GetParent().SmsBatchCrewGroupID)
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SmsBatchCrewGroupHumanResourceIDProperty, cm.Parameters("@SmsBatchCrewGroupHumanResourceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SmsBatchCrewGroupHumanResourceID", GetProperty(SmsBatchCrewGroupHumanResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace