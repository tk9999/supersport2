﻿' Generated 15 Sep 2015 12:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Web

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatchFind
    Inherits SingularReadOnlyBase(Of ROSmsBatchFind)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property SmsBatchID() As Integer
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name", "")
    ''' <summary>
    ''' Gets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:="")>
    Public ReadOnly Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
    End Property

    Public Shared BatchDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchDescription, "Batch Description", "")
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:="")>
    Public ReadOnly Property BatchDescription() As String
      Get
        Return GetProperty(BatchDescriptionProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared TotalSmsesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalSmses, "Smses", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Smses")>
    Public ReadOnly Property TotalSmses() As Integer?
      Get
        Return GetProperty(TotalSmsesProperty)
      End Get
    End Property

    Public Shared DeliveredCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DeliveredCount, "Delivered", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Delivered")>
    Public ReadOnly Property DeliveredCount() As Integer?
      Get
        Return GetProperty(DeliveredCountProperty)
      End Get
    End Property

    Public Shared RecipientCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RecipientCount, "Recipients", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Recipients")>
    Public ReadOnly Property RecipientCount() As Integer?
      Get
        Return GetProperty(RecipientCountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BatchName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSmsBatchFind(dr As SafeDataReader) As ROSmsBatchFind

      Dim r As New ROSmsBatchFind()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SmsBatchIDProperty, .GetInt32(0))
        LoadProperty(BatchNameProperty, .GetString(1))
        LoadProperty(BatchDescriptionProperty, .GetString(2))
        LoadProperty(CreatedByNameProperty, .GetString(3))
        LoadProperty(TotalSmsesProperty, .GetInt32(4))
        LoadProperty(DeliveredCountProperty, .GetInt32(5))
        LoadProperty(RecipientCountProperty, .GetInt32(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace