﻿' Generated 10 Oct 2015 18:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsTemplateTypeList
    Inherits OBReadOnlyListBase(Of ROSmsTemplateTypeList, ROSmsTemplateType)

#Region " Business Methods "

    Public Function GetItem(SmsTemplateTypeID As Integer) As ROSmsTemplateType

      For Each child As ROSmsTemplateType In Me
        If child.SmsTemplateTypeID = SmsTemplateTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Sms Template Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSmsTemplateTypeList() As ROSmsTemplateTypeList

      Return New ROSmsTemplateTypeList()

    End Function

    Public Shared Sub BeginGetROSmsTemplateTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSmsTemplateTypeList)))

      Dim dp As New DataPortal(Of ROSmsTemplateTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSmsTemplateTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROSmsTemplateTypeList)))

      Dim dp As New DataPortal(Of ROSmsTemplateTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSmsTemplateTypeList() As ROSmsTemplateTypeList

      Return DataPortal.Fetch(Of ROSmsTemplateTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSmsTemplateType.GetROSmsTemplateType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSmsTemplateTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace