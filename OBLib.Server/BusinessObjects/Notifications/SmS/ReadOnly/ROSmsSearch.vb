﻿' Generated 18 Sep 2015 06:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.SmS.ReadOnly

  <Serializable()> _
  Public Class ROSmsSearch
    Inherits OBReadOnlyBase(Of ROSmsSearch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SmsID() As Integer
      Get
        Return GetProperty(SmsIDProperty)
      End Get
    End Property

    Public Shared ShortMessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortMessage, "Message")
    ''' <summary>
    ''' Gets the Message value
    ''' </summary>
    <Display(Name:="Message", Description:="")>
    Public ReadOnly Property ShortMessage() As String
      Get
        Return GetProperty(ShortMessageProperty)
      End Get
    End Property

    Public Shared FullMessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FullMessage, "Message")
    ''' <summary>
    ''' Gets the Message value
    ''' </summary>
    <Display(Name:="Message", Description:="")>
    Public ReadOnly Property FullMessage() As String
      Get
        Return GetProperty(FullMessageProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    Public ReadOnly Property CreatedDate() As DateTime
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DateToSend, "Date To Send")
    ''' <summary>
    ''' Gets the Date To Send value
    ''' </summary>
    <Display(Name:="Date To Send", Description:="")>
    Public ReadOnly Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", False)
    ''' <summary>
    ''' Gets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="")>
    Public ReadOnly Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
    End Property

    Public Shared SmsesRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsesRequired, "Smses Required")
    ''' <summary>
    ''' Gets the Smses Required value
    ''' </summary>
    <Display(Name:="Smses Required", Description:="")>
    Public ReadOnly Property SmsesRequired() As Integer
      Get
        Return GetProperty(SmsesRequiredProperty)
      End Get
    End Property

    Public Shared SmsRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsRecipientID, "Sms Recipient")
    ''' <summary>
    ''' Gets the Sms Recipient value
    ''' </summary>
    <Display(Name:="Sms Recipient", Description:="")>
    Public ReadOnly Property SmsRecipientID() As Integer
      Get
        Return GetProperty(SmsRecipientIDProperty)
      End Get
    End Property

    Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RecipientName, "Recipient Name")
    ''' <summary>
    ''' Gets the Recipient Name value
    ''' </summary>
    <Display(Name:="Recipient Name", Description:="")>
    Public ReadOnly Property RecipientName() As String
      Get
        Return GetProperty(RecipientNameProperty)
      End Get
    End Property

    Public Shared CellNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellNo, "Cell No")
    ''' <summary>
    ''' Gets the Cell No value
    ''' </summary>
    <Display(Name:="Cell No", Description:="")>
    Public ReadOnly Property CellNo() As String
      Get
        Return GetProperty(CellNoProperty)
      End Get
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SentDate, "Sent Date")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="")>
    Public ReadOnly Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error")
    ''' <summary>
    ''' Gets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:="")>
    Public ReadOnly Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
    End Property

    Public Shared SmSBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmSBatchID, "Sm S Batch")
    ''' <summary>
    ''' Gets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Sm S Batch", Description:="")>
    Public ReadOnly Property SmSBatchID() As Integer
      Get
        Return GetProperty(SmSBatchIDProperty)
      End Get
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name")
    ''' <summary>
    ''' Gets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:="")>
    Public ReadOnly Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status Code")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Status Code", Description:="")>
    Public ReadOnly Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
    End Property

    Public Shared StatusDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDescription, "Status Description")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Status Description", Description:="")>
    Public ReadOnly Property StatusDescription() As String
      Get
        Return GetProperty(StatusDescriptionProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row Num")
    ''' <summary>
    ''' Gets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Row Num", Description:="")>
    Public ReadOnly Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public Shared SmsTemplateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmsTemplateTypeID, "Sms Template", Nothing)
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Sms Template", Description:="")>
    Public ReadOnly Property SmsTemplateTypeID() As Integer?
      Get
        Return GetProperty(SmsTemplateTypeIDProperty)
      End Get
    End Property

    Public Shared RecipientCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RecipientCount, "Recipient Count")
    ''' <summary>
    ''' Gets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Recipient Count", Description:="")>
    Public ReadOnly Property RecipientCount() As Integer
      Get
        Return GetProperty(RecipientCountProperty)
      End Get
    End Property

    Public Shared StatusDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusDateTime, "Status Date Time")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="Status Date Time", Description:="")>
    Public ReadOnly Property StatusDateTime As DateTime?
      Get
        Return GetProperty(StatusDateTimeProperty)
      End Get
    End Property

    Public Shared StatusDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDateString, "Status Date")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Status Date", Description:="")>
    Public ReadOnly Property StatusDateString() As String
      Get
        Return GetProperty(StatusDateStringProperty)
      End Get
    End Property

    Public Shared DateToSendStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DateToSendString, "Send Date")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Send Date")>
    Public ReadOnly Property DateToSendString() As String
      Get
        Return GetProperty(DateToSendStringProperty)
      End Get
    End Property

    Public Shared ButtonCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ButtonCss, "ButtonCss")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="ButtonCss")>
    Public ReadOnly Property ButtonCss() As String
      Get
        Return GetProperty(ButtonCssProperty)
      End Get
    End Property

    Public Shared IconCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IconCss, "IconCss")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="IconCss")>
    Public ReadOnly Property IconCss() As String
      Get
        Return GetProperty(IconCssProperty)
      End Get
    End Property

    Public Shared CreatedDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedDateString, "Created Date")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Created Date", Description:="")>
    Public ReadOnly Property CreatedDateString() As String
      Get
        Return GetProperty(CreatedDateStringProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ShortMessage

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSmsSearch(dr As SafeDataReader) As ROSmsSearch

      Dim r As New ROSmsSearch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SmsIDProperty, .GetInt32(0))
        LoadProperty(ShortMessageProperty, .GetString(1))
        LoadProperty(FullMessageProperty, .GetString(2))
        LoadProperty(CreatedDateProperty, .GetDateTime(3))
        LoadProperty(DateToSendProperty, .GetValue(4))
        LoadProperty(IgnoreProperty, .GetBoolean(5))
        LoadProperty(SmsesRequiredProperty, .GetInt32(6))
        LoadProperty(SmsRecipientIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(RecipientNameProperty, .GetString(8))
        LoadProperty(CellNoProperty, .GetString(9))
        LoadProperty(SentDateProperty, .GetValue(10))
        LoadProperty(NotSentErrorProperty, .GetString(11))
        LoadProperty(SmSBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(BatchNameProperty, .GetString(13))
        LoadProperty(StatusCodeProperty, .GetString(14))
        LoadProperty(StatusDescriptionProperty, .GetString(15))
        LoadProperty(CreatedByNameProperty, .GetString(16))
        LoadProperty(RowNoProperty, .GetInt32(17))
        LoadProperty(SmsTemplateTypeIDProperty, ZeroNothing(.GetInt32(18)))
        LoadProperty(RecipientCountProperty, .GetInt32(19))
        LoadProperty(StatusDateTimeProperty, .GetValue(20))
        LoadProperty(StatusDateStringProperty, .GetString(21))
        LoadProperty(DateToSendStringProperty, .GetString(22))
        LoadProperty(ButtonCssProperty, .GetString(23))
        LoadProperty(IconCssProperty, .GetString(24))
        LoadProperty(CreatedDateStringProperty, .GetString(25))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace