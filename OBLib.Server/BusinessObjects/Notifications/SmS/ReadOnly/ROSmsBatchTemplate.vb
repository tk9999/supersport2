﻿' Generated 17 Sep 2015 16:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.SmS.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatchTemplate
    Inherits SingularReadOnlyBase(Of ROSmsBatchTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchTemplateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchTemplateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SmsBatchTemplateID() As Integer
      Get
        Return GetProperty(SmsBatchTemplateIDProperty)
      End Get
    End Property

    Public Shared SmsBatchTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SmsBatchTemplate, "Sms Batch Template", "")
    ''' <summary>
    ''' Gets the Sms Batch Template value
    ''' </summary>
    <Display(Name:="Sms Batch Template", Description:="")>
    Public ReadOnly Property SmsBatchTemplate() As String
      Get
        Return GetProperty(SmsBatchTemplateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchTemplateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SmsBatchTemplate

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSmsBatchTemplate(dr As SafeDataReader) As ROSmsBatchTemplate

      Dim r As New ROSmsBatchTemplate()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SmsBatchTemplateIDProperty, .GetInt32(0))
        LoadProperty(SmsBatchTemplateProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace