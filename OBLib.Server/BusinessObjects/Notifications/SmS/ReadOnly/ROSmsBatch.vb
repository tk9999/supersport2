﻿' Generated 15 Sep 2015 12:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Web

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatch
    Inherits SingularReadOnlyBase(Of ROSmsBatch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SmsBatchID() As Integer
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name", "")
    ''' <summary>
    ''' Gets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:="")>
  Public ReadOnly Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
    End Property

    Public Shared BatchDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchDescription, "Batch Description", "")
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:="")>
  Public ReadOnly Property BatchDescription() As String
      Get
        Return GetProperty(BatchDescriptionProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared DeliveredToHandsetCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DeliveredToHandsetCount, "Delivered", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Delivered")>
    Public ReadOnly Property DeliveredToHandsetCount() As Integer?
      Get
        Return GetProperty(DeliveredToHandsetCountProperty)
      End Get
    End Property

    Public Shared NotDeliveredToHandsetCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotDeliveredToHandsetCount, "Not Delivered", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Not Delivered")>
    Public ReadOnly Property NotDeliveredToHandsetCount() As Integer?
      Get
        Return GetProperty(NotDeliveredToHandsetCountProperty)
      End Get
    End Property

    Public Shared TotalRecipientsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalRecipients, "Total Recipients", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Total Recipients")>
    Public ReadOnly Property TotalRecipients() As Integer?
      Get
        Return GetProperty(TotalRecipientsProperty)
      End Get
    End Property

    <Display(Name:="Fully Sent")>
    Public ReadOnly Property FullySent As Boolean
      Get
        If TotalRecipients = DeliveredToHandsetCount Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    <Display(Name:="Status")>
    Public ReadOnly Property StatusText As String
      Get
        If TotalRecipients = DeliveredToHandsetCount AndAlso TotalRecipients > 0 Then
          Return TotalRecipients.ToString & " smses delivered"
        Else
          Return NotDeliveredToHandsetCount.ToString & " smses pending/failed delivery"
        End If
      End Get
    End Property

    Public Shared TotalSmsesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalSmses, "Smses", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Smses")>
    Public ReadOnly Property TotalSmses() As Integer?
      Get
        Return GetProperty(TotalSmsesProperty)
      End Get
    End Property

    Public Shared SentCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SentCount, "Sent", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Sent")>
    Public ReadOnly Property SentCount() As Integer?
      Get
        Return GetProperty(SentCountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BatchName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSmsBatch(dr As SafeDataReader) As ROSmsBatch

      Dim r As New ROSmsBatch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SmsBatchIDProperty, .GetInt32(0))
        LoadProperty(CreatedByProperty, .GetInt32(1))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(2))
        LoadProperty(BatchNameProperty, .GetString(3))
        LoadProperty(BatchDescriptionProperty, .GetString(4))
        LoadProperty(CreatedByNameProperty, .GetString(5))
        LoadProperty(DeliveredToHandsetCountProperty, .GetInt32(6))
        LoadProperty(NotDeliveredToHandsetCountProperty, .GetInt32(7))
        LoadProperty(TotalRecipientsProperty, .GetInt32(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace