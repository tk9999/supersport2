﻿' Generated 07 Sep 2016 12:10 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatchCrewType
    Inherits OBReadOnlyBase(Of ROSmsBatchCrewType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchCrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchCrewTypeID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property SmsBatchCrewTypeID() As Integer
      Get
        Return GetProperty(SmsBatchCrewTypeIDProperty)
      End Get
    End Property

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property SmsBatchID() As Integer
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "CrewTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public ReadOnly Property CrewTypeID As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
    End Property

    Public Shared CrewCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewCount, "Crew Count", 0)
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Crew Count", Description:="")>
    Public ReadOnly Property CrewCount() As Integer
      Get
        Return GetProperty(CrewCountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CrewType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSmsBatchCrewType(dr As SafeDataReader) As ROSmsBatchCrewType

      Dim r As New ROSmsBatchCrewType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SmsBatchCrewTypeIDProperty, .GetInt32(0))
        LoadProperty(SmsBatchIDProperty, .GetInt32(1))
        LoadProperty(CrewTypeIDProperty, .GetInt32(2))
        LoadProperty(CrewTypeProperty, .GetString(3))
        LoadProperty(CrewCountProperty, .GetInt32(4))
      End With

    End Sub

#End Region

  End Class

End Namespace