﻿' Generated 16 Nov 2016 09:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsSearchScenario
    Inherits OBReadOnlyBase(Of ROSmsSearchScenario)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSmsSearchScenarioBO.ROSmsSearchScenarioBOToString(self)")

    Public Shared ScenarioIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScenarioID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ScenarioID() As Integer
      Get
        Return GetProperty(ScenarioIDProperty)
      End Get
    End Property

    Public Shared ScenarioNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScenarioName, "Scenario Name")
    ''' <summary>
    ''' Gets the Scenario Name value
    ''' </summary>
    <Display(Name:="Scenario Name", Description:="")>
  Public ReadOnly Property ScenarioName() As String
      Get
        Return GetProperty(ScenarioNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ScenarioIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ScenarioName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSmsSearchScenario(dr As SafeDataReader) As ROSmsSearchScenario

      Dim r As New ROSmsSearchScenario()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ScenarioIDProperty, .GetInt32(0))
        LoadProperty(ScenarioNameProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace