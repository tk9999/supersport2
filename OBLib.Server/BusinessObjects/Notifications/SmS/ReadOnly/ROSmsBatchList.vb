﻿' Generated 15 Sep 2015 12:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Web
Imports Singular.Misc

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatchList
    Inherits SingularReadOnlyListBase(Of ROSmsBatchList, ROSmsBatch)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SmsBatchID As Integer) As ROSmsBatch

      For Each child As ROSmsBatch In Me
        If child.SmsBatchID = SmsBatchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Sms Batches"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property CreatedBy As Integer? = Nothing

      Public Sub New(CreatedBy As Integer?)
        Me.CreatedBy = CreatedBy
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSmsBatchList() As ROSmsBatchList

      Return New ROSmsBatchList()

    End Function

    Public Shared Sub BeginGetROSmsBatchList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSmsBatchList)))

      Dim dp As New DataPortal(Of ROSmsBatchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSmsBatchList(CallBack As EventHandler(Of DataPortalResult(Of ROSmsBatchList)))

      Dim dp As New DataPortal(Of ROSmsBatchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSmsBatchList() As ROSmsBatchList

      Return DataPortal.Fetch(Of ROSmsBatchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSmsBatch.GetROSmsBatch(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSmsBatchList"
            cm.Parameters.AddWithValue("@CreatedBy", NothingDBNull(crit.CreatedBy))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace