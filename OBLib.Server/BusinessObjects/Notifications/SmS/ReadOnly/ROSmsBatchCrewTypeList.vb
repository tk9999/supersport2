﻿' Generated 07 Sep 2016 12:10 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatchCrewTypeList
    Inherits OBReadOnlyListBase(Of ROSmsBatchCrewTypeList, ROSmsBatchCrewType)

#Region " Business Methods "

    Public Function GetItem(SmsBatchID As Integer) As ROSmsBatchCrewType

      For Each child As ROSmsBatchCrewType In Me
        If child.SmsBatchID = SmsBatchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SmsBatchID As Integer?

      <PrimarySearchField>
      Public Property CrewType As String

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSmsBatchCrewTypeList() As ROSmsBatchCrewTypeList

      Return New ROSmsBatchCrewTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSmsBatchCrewTypeList() As ROSmsBatchCrewTypeList

      Return DataPortal.Fetch(Of ROSmsBatchCrewTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSmsBatchCrewType.GetROSmsBatchCrewType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSmsBatchCrewTypeList"
            cm.Parameters.AddWithValue("@SmsBatchID", Singular.Misc.NothingDBNull(crit.SmsBatchID))
            cm.Parameters.AddWithValue("@CrewType", Singular.Strings.MakeEmptyDBNull(crit.CrewType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace