﻿' Generated 15 Sep 2015 12:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Web
Imports Singular.Misc

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsBatchFindList
    Inherits SingularReadOnlyListBase(Of ROSmsBatchFindList, ROSmsBatchFind)

#Region " Business Methods "

    Public Function GetItem(SmsBatchID As Integer) As ROSmsBatchFind

      For Each child As ROSmsBatchFind In Me
        If child.SmsBatchID = SmsBatchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Sms Batches"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Singular.SingularCriteriaBase(Of Criteria)

      Public Property CreatedBy As Integer? = Nothing
      <PrimarySearchField>
      Public Property BatchName As String = ""
      Public Property CreatedStartDate As DateTime?
      Public Property CreatedEndDate As DateTime?

      Public Sub New(CreatedBy As Integer?)
        Me.CreatedBy = CreatedBy
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSmsBatchFindList() As ROSmsBatchFindList

      Return New ROSmsBatchFindList()

    End Function

    Public Shared Sub BeginGetROSmsBatchFindList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSmsBatchFindList)))

      Dim dp As New DataPortal(Of ROSmsBatchFindList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSmsBatchFindList(CallBack As EventHandler(Of DataPortalResult(Of ROSmsBatchFindList)))

      Dim dp As New DataPortal(Of ROSmsBatchFindList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSmsBatchFindList() As ROSmsBatchFindList

      Return DataPortal.Fetch(Of ROSmsBatchFindList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSmsBatchFind.GetROSmsBatchFind(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSmsBatchListFind"
            cm.Parameters.AddWithValue("@CreatedBy", NothingDBNull(crit.CreatedBy))
            cm.Parameters.AddWithValue("@BatchName", Strings.MakeEmptyDBNull(crit.BatchName))
            cm.Parameters.AddWithValue("@CreatedStartDate", NothingDBNull(crit.CreatedStartDate))
            cm.Parameters.AddWithValue("@CreatedEndDate", NothingDBNull(crit.CreatedEndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace