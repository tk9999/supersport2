﻿' Generated 10 Oct 2015 18:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROSmsTemplateType
    Inherits OBReadOnlyBase(Of ROSmsTemplateType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsTemplateTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsTemplateTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SmsTemplateTypeID() As Integer
      Get
        Return GetProperty(SmsTemplateTypeIDProperty)
      End Get
    End Property

    Public Shared SmsTemplateTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SmsTemplateType, "Sms Template Type", "")
    ''' <summary>
    ''' Gets the Sms Template Type value
    ''' </summary>
    <Display(Name:="Sms Template Type", Description:="")>
  Public ReadOnly Property SmsTemplateType() As String
      Get
        Return GetProperty(SmsTemplateTypeProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", True)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public Shared MessageTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MessageTemplate, "Message Template", "")
    ''' <summary>
    ''' Gets the Message Template value
    ''' </summary>
    <Display(Name:="Message Template", Description:="")>
  Public ReadOnly Property MessageTemplate() As String
      Get
        Return GetProperty(MessageTemplateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsTemplateTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SmsTemplateType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSmsTemplateType(dr As SafeDataReader) As ROSmsTemplateType

      Dim r As New ROSmsTemplateType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SmsTemplateTypeIDProperty, .GetInt32(0))
        LoadProperty(SmsTemplateTypeProperty, .GetString(1))
        LoadProperty(SystemIndProperty, .GetBoolean(2))
        LoadProperty(MessageTemplateProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace