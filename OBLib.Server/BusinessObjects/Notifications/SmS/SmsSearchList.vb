﻿' Generated 28 Mar 2017 08:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Security
Imports OBLib.HR.ReadOnly
Imports Singular
Imports Singular.Misc
Imports OBLib.Notifications.Sms.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsSearchList
    Inherits OBBusinessListBase(Of SmsSearchList, SmsSearch)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SmsID As Integer) As SmsSearch

      For Each child As SmsSearch In Me
        If child.SmsID = SmsID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSmsSearchRecipient(SmsID As Integer) As SmsSearchRecipient

      Dim obj As SmsSearchRecipient = Nothing
      For Each parent As SmsSearch In Me
        obj = parent.SmsSearchRecipientList.GetItem(SmsID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      'Common Filters

      Public Shared ScenarioProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.Scenario, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Search By"), Required(ErrorMessage:="Search By is required"),
      DropDownWeb(GetType(OBLib.Notifications.Sms.ReadOnly.ROSmsSearchScenarioList), DisplayMember:="ScenarioName", ValueMember:="ScenarioID")>
      Public Property Scenario() As Integer?
        Get
          Return ReadProperty(ScenarioProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ScenarioProperty, Value)
        End Set
      End Property

      <Display(Name:="Created Start Date"), SetExpression("ROSmsSearchListCriteriaBO.CreatedStartDateSet(self)"),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.CreatedStartDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")>
      Public Property CreatedStartDate As DateTime? = Now '.AddDays(-7)

      <Display(Name:="Created End Date"), SetExpression("ROSmsSearchListCriteriaBO.CreatedEndDateSet(self)"),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.CreatedEndDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")>
      Public Property CreatedEndDate As DateTime? = Now

      Public Shared CreatedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CreatedByUserID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Created By"),
      SetExpression("ROSmsSearchListCriteriaBO.CreatedByUserIDSet(self)"),
      DropDownWeb(GetType(ROUserFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="ROSmsSearchListCriteriaBO.setCreatedByUserIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="ROSmsSearchListCriteriaBO.triggerCreatedByUserIDAutoPopulate",
                  AfterFetchJS:="ROSmsSearchListCriteriaBO.afterCreatedByUserIDRefreshAjax",
                  OnItemSelectJSFunction:="ROSmsSearchListCriteriaBO.onUserIDSelected",
                  LookupMember:="CreatedBy", DisplayMember:="FullName", ValueMember:="UserID",
                  DropDownColumns:={"FirstName", "Surname", "System", "ProductionArea"})>
      Public Property CreatedByUserID() As Integer?
        Get
          Return ReadProperty(CreatedByUserIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CreatedByUserIDProperty, Value)
        End Set
      End Property

      Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CreatedBy, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Created By", Description:="")>
      Public Property CreatedBy() As String
        Get
          Return ReadProperty(CreatedByProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(CreatedByProperty, Value)
        End Set
      End Property

      Public Shared RecipientHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.RecipientHumanResourceID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Recipient"),
      SetExpression("ROSmsSearchListCriteriaBO.RecipientHumanResourceIDSet(self)"),
      DropDownWeb(GetType(ROHumanResourceContactList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="ROSmsSearchListCriteriaBO.setRecipientHumanResourceIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="ROSmsSearchListCriteriaBO.triggerRecipientHumanResourceIDAutoPopulate",
                  AfterFetchJS:="ROSmsSearchListCriteriaBO.afterRecipientHumanResourceIDRefreshAjax",
                  OnItemSelectJSFunction:="ROSmsSearchListCriteriaBO.onRecipientHumanResourceIDSelected",
                  LookupMember:="RecipientName", DisplayMember:="PreferredFirstSurname", ValueMember:="HumanResourceID",
                  DropDownColumns:={"Firstname", "Surname", "CellPhoneNumber", "AlternativeContactNumber"})>
      Public Property RecipientHumanResourceID() As Integer?
        Get
          Return ReadProperty(RecipientHumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(RecipientHumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.RecipientName, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Recipient", Description:="")>
      Public Property RecipientName() As String
        Get
          Return ReadProperty(RecipientNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(RecipientNameProperty, Value)
        End Set
      End Property


      'Batch Filters (smses generated in a batch)
      Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SmsBatchID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Batch Name", Description:=""), SetExpression("ROSmsSearchListCriteriaBO.SmsBatchIDSet(self)"),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.SmsBatchIDValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required"),
      DropDownWeb(GetType(ROSmsBatchFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="ROSmsSearchListCriteriaBO.setSmsBatchIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="ROSmsSearchListCriteriaBO.triggerSmsBatchIDAutoPopulate",
                  AfterFetchJS:="ROSmsSearchListCriteriaBO.afterSmsBatchIDRefreshAjax",
                  OnItemSelectJSFunction:="ROSmsSearchListCriteriaBO.onSmsBatchIDSelected",
                  LookupMember:="BatchName", DisplayMember:="BatchName", ValueMember:="SmsBatchID",
                  DropDownColumns:={"BatchName", "BatchDescription", "TotalSmses", "DeliveredCount", "RecipientCount"})>
      Public Property SmsBatchID() As Integer?
        Get
          Return ReadProperty(SmsBatchIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SmsBatchIDProperty, Value)
        End Set
      End Property

      Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BatchName, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Batch Name")>
      Public Property BatchName() As String
        Get
          Return ReadProperty(BatchNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(BatchNameProperty, Value)
        End Set
      End Property

      Public Shared SmsBatchCrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SmsBatchCrewTypeID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Batch Name", Description:=""), SetExpression("ROSmsSearchListCriteriaBO.SmsBatchCrewTypeIDSet(self)"),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.CrewTypeIDValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required"),
      DropDownWeb(GetType(ROSmsBatchCrewTypeList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="ROSmsSearchListCriteriaBO.setSmsBatchCrewTypeIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="ROSmsSearchListCriteriaBO.triggerSmsBatchCrewTypeIDAutoPopulate",
                  AfterFetchJS:="ROSmsSearchListCriteriaBO.afterSmsBatchCrewTypeIDRefreshAjax",
                  OnItemSelectJSFunction:="ROSmsSearchListCriteriaBO.onSmsBatchCrewTypeIDSelected",
                  LookupMember:="CrewType", DisplayMember:="CrewType", ValueMember:="SmsBatchCrewTypeID",
                  DropDownColumns:={"CrewType", "CrewCount"})>
      Public Property SmsBatchCrewTypeID() As Integer?
        Get
          Return ReadProperty(SmsBatchCrewTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SmsBatchCrewTypeIDProperty, Value)
        End Set
      End Property

      Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CrewType, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Batch Name")>
      Public Property CrewType() As String
        Get
          Return ReadProperty(CrewTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(CrewTypeProperty, Value)
        End Set
      End Property


      'Area Filters (smses for a ProductionSystemArea)
      Public Shared SystemIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterSProperty(Of List(Of Integer))(Function(c) c.SystemIDs, New List(Of Integer))
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Event")>
      Public Property SystemIDs() As List(Of Integer)
        Get
          Return ReadProperty(SystemIDsProperty)
        End Get
        Set(ByVal Value As List(Of Integer))
          LoadProperty(SystemIDsProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterSProperty(Of List(Of Integer))(Function(c) c.ProductionAreaIDs, New List(Of Integer))
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Event")>
      Public Property ProductionAreaIDs() As List(Of Integer)
        Get
          Return ReadProperty(ProductionAreaIDsProperty)
        End Get
        Set(ByVal Value As List(Of Integer))
          LoadProperty(ProductionAreaIDsProperty, Value)
        End Set
      End Property

      Public Shared ProductionSystemAreaStartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ProductionSystemAreaStartDate, Nothing)
      ''' <summary>
      ''' Gets the Batch Description value
      ''' </summary>
      <Display(Name:="Event Start Date", Description:=""),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.ProductionSystemAreaStartDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")>
      Public Property ProductionSystemAreaStartDate() As DateTime?
        Get
          Return ReadProperty(ProductionSystemAreaStartDateProperty)
        End Get
        Set(value As DateTime?)
          LoadProperty(ProductionSystemAreaStartDateProperty, value)
        End Set
      End Property

      Public Shared ProductionSystemAreaEndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ProductionSystemAreaEndDate, Nothing)
      ''' <summary>
      ''' Gets the Batch Description value
      ''' </summary>
      <Display(Name:="Event End Date", Description:=""),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.ProductionSystemAreaEndDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")>
      Public Property ProductionSystemAreaEndDate() As DateTime?
        Get
          Return ReadProperty(ProductionSystemAreaEndDateProperty)
        End Get
        Set(value As DateTime?)
          LoadProperty(ProductionSystemAreaEndDateProperty, value)
        End Set
      End Property

      Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Event"),
      DropDownWeb(GetType(ROPSAListSms), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="ROSmsSearchListCriteriaBO.setProductionSystemAreaCriteriaBeforeRefresh",
                  PreFindJSFunction:="ROSmsSearchListCriteriaBO.triggerProductionSystemAreaAutoPopulate",
                  AfterFetchJS:="ROSmsSearchListCriteriaBO.afterProductionSystemAreaRefreshAjax",
                  OnItemSelectJSFunction:="ROSmsSearchListCriteriaBO.onProductionSystemAreaIDSelected",
                  LookupMember:="ProductionSystemAreaName", DisplayMember:="ProductionSystemAreaName", ValueMember:="ProductionSystemAreaID",
                  DropDownColumns:={"SubDept", "Area", "ProductionSystemAreaName", "MinSD", "MaxED", "Room"})>
      Public Property ProductionSystemAreaID() As Integer?
        Get
          Return ReadProperty(ProductionSystemAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSystemAreaIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionSystemAreaNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionSystemAreaName, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Event")>
      Public Property ProductionSystemAreaName() As String
        Get
          Return ReadProperty(ProductionSystemAreaNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionSystemAreaNameProperty, Value)
        End Set
      End Property


      'Day Filters (smses for a day)
      Public Shared StartDayProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.StartDay, Nothing)
      ''' <summary>
      ''' Gets the Batch Description value
      ''' </summary>
      <Display(Name:="Start Day", Description:=""),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.StartDayValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")>
      Public Property StartDay() As Date?
        Get
          Return ReadProperty(StartDayProperty)
        End Get
        Set(value As Date?)
          LoadProperty(StartDayProperty, value)
        End Set
      End Property

      Public Shared EndDayProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.EndDay, Nothing)
      ''' <summary>
      ''' Gets the Batch Description value
      ''' </summary>
      <Display(Name:="End Day", Description:=""),
      Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.EndDayValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")>
      Public Property EndDay() As Date?
        Get
          Return ReadProperty(EndDayProperty)
        End Get
        Set(value As Date?)
          LoadProperty(EndDayProperty, value)
        End Set
      End Property

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSmsSearchList() As SmsSearchList

      Return New SmsSearchList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSmsSearchList() As SmsSearchList

      Return DataPortal.Fetch(Of SmsSearchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      While sdr.Read
        Me.Add(SmsSearch.GetSmsSearch(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SmsSearch = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.SmsSearchRecipientList.RaiseListChangedEvents = False
          parent.SmsSearchRecipientList.Add(SmsSearchRecipient.GetSmsSearchRecipient(sdr))
          parent.SmsSearchRecipientList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SmsSearch In Me
        child.CheckRules()
        For Each SmsSearchRecipient As SmsSearchRecipient In child.SmsSearchRecipientList
          SmsSearchRecipient.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSmsSearchList"
            cm.Parameters.AddWithValue("@Scenario", Singular.Misc.NothingDBNull(crit.Scenario))
            cm.Parameters.AddWithValue("@SystemIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            cm.Parameters.AddWithValue("@CreatedStartDate", NothingDBNull(crit.CreatedStartDate))
            cm.Parameters.AddWithValue("@CreatedEndDate", NothingDBNull(crit.CreatedEndDate))
            cm.Parameters.AddWithValue("@CreatedByUserID", NothingDBNull(crit.CreatedByUserID))
            cm.Parameters.AddWithValue("@RecipientHumanResourceID", NothingDBNull(crit.RecipientHumanResourceID))
            cm.Parameters.AddWithValue("@SmSBatchID", Singular.Misc.NothingDBNull(crit.SmsBatchID))
            cm.Parameters.AddWithValue("@SmsBatchCrewTypeID", Singular.Misc.NothingDBNull(crit.SmsBatchCrewTypeID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaStartDate", NothingDBNull(crit.ProductionSystemAreaStartDate))
            cm.Parameters.AddWithValue("@ProductionSystemAreaEndDate", NothingDBNull(crit.ProductionSystemAreaEndDate))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@StartDay", NothingDBNull(crit.StartDay))
            cm.Parameters.AddWithValue("@EndDay", NothingDBNull(crit.EndDay))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace