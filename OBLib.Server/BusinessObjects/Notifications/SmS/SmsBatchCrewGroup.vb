﻿' Generated 11 May 2017 07:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchCrewGroup
    Inherits OBBusinessBase(Of SmsBatchCrewGroup)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SmsBatchCrewGroupBO.SmsBatchCrewGroupBOToString(self)")

    Public Shared SmsBatchCrewGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchCrewGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property SmsBatchCrewGroupID() As Integer
      Get
        Return GetProperty(SmsBatchCrewGroupIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsBatchCrewGroupIDProperty, Value)
      End Set
    End Property

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmsBatchID, "Sms Batch", Nothing)
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Sms Batch", Description:="")>
    Public Property SmsBatchID() As Integer?
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SmsBatchIDProperty, Value)
      End Set
    End Property

    Public Shared TemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Template, "Template", "")
    ''' <summary>
    ''' Gets and sets the Template value
    ''' </summary>
    <Display(Name:="Template", Description:=""),
    StringLength(1024, ErrorMessage:="Template cannot be more than 1024 characters"),
    Required(AllowEmptyStrings:=False), Singular.DataAnnotations.TextField(True, True, False, 12)>
    Public Property Template() As String
      Get
        Return GetProperty(TemplateProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TemplateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupName, "Group Name", "")
    ''' <summary>
    ''' Gets and sets the CrewType value
    ''' </summary>
    <Display(Name:="Group Name", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property GroupName() As String
      Get
        Return GetProperty(GroupNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GroupNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SmsBatchCrewGroupHumanResourceListProperty As PropertyInfo(Of SmsBatchCrewGroupHumanResourceList) = RegisterProperty(Of SmsBatchCrewGroupHumanResourceList)(Function(c) c.SmsBatchCrewGroupHumanResourceList, "Sms Batch Crew Group Human Resource List")

    Public ReadOnly Property SmsBatchCrewGroupHumanResourceList() As SmsBatchCrewGroupHumanResourceList
      Get
        If GetProperty(SmsBatchCrewGroupHumanResourceListProperty) Is Nothing Then
          LoadProperty(SmsBatchCrewGroupHumanResourceListProperty, Notifications.Sms.SmsBatchCrewGroupHumanResourceList.NewSmsBatchCrewGroupHumanResourceList())
        End If
        Return GetProperty(SmsBatchCrewGroupHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchCrewGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Template.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Batch Crew Group")
        Else
          Return String.Format("Blank {0}", "Sms Batch Crew Group")
        End If
      Else
        Return Me.Template
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SmsBatchCrewGroupHumanResources"}
      End Get
    End Property

    Public Function GetParent() As SmsBatch
      Return CType(CType(Me.Parent, SmsBatchCrewGroupList).Parent, SmsBatch)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsBatchCrewGroup() method.

    End Sub

    Public Shared Function NewSmsBatchCrewGroup() As SmsBatchCrewGroup

      Return DataPortal.CreateChild(Of SmsBatchCrewGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSmsBatchCrewGroup(dr As SafeDataReader) As SmsBatchCrewGroup

      Dim s As New SmsBatchCrewGroup()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsBatchCrewGroupIDProperty, .GetInt32(0))
          LoadProperty(SmsBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TemplateProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(GroupNameProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SmsBatchCrewGroupIDProperty)

      cm.Parameters.AddWithValue("@SmsBatchID", Me.GetParent.SmsBatchID)
      cm.Parameters.AddWithValue("@Template", GetProperty(TemplateProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", GetProperty(ModifiedByProperty))
      cm.Parameters.AddWithValue("@CrewTypeID", NothingDBNull(GetProperty(CrewTypeIDProperty)))
      cm.Parameters.AddWithValue("@GroupName", GetProperty(GroupNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SmsBatchCrewGroupIDProperty, cm.Parameters("@SmsBatchCrewGroupID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SmsBatchCrewGroupHumanResourceListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SmsBatchCrewGroupID", GetProperty(SmsBatchCrewGroupIDProperty))
    End Sub

#End Region

  End Class

End Namespace