﻿' Generated 02 Sep 2016 10:23 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchCrewTypeList
    Inherits OBBusinessListBase(Of SmsBatchCrewTypeList, SmsBatchCrewType)

#Region " Business Methods "

    Public Function GetItem(SmsBatchCrewTypeID As Integer) As SmsBatchCrewType

      For Each child As SmsBatchCrewType In Me
        If child.SmsBatchCrewTypeID = SmsBatchCrewTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Sms Batch Dates"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSmsBatchCrewTypeList() As SmsBatchCrewTypeList

      Return New SmsBatchCrewTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSmsBatchCrewTypeList() As SmsBatchCrewTypeList

      Return DataPortal.Fetch(Of SmsBatchCrewTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SmsBatchCrewType.GetSmsBatchCrewType(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub


    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSmsBatchCrewTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SmsBatchCrewType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SmsBatchCrewType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End Region

  End Class

End Namespace