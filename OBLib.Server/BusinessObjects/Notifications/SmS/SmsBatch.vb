﻿' Generated 11 Jun 2015 16:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Notifications.SmS.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Security

Namespace Notifications.SmS

  <Serializable()> _
  Public Class SmsBatch
    Inherits OBBusinessBase(Of SmsBatch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property SmsBatchID() As Integer
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name", "")
    ''' <summary>
    ''' Gets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:=""),
    Required(ErrorMessage:="Batch Name is required", AllowEmptyStrings:=False)>
    Public Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
      Set(value As String)
        SetProperty(BatchNameProperty, value)
      End Set
    End Property

    Public Shared BatchDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchDescription, "Batch Description", "")
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Batch Description", Description:=""),
    Required(ErrorMessage:="Batch Description is required", AllowEmptyStrings:=False)>
    Public Property BatchDescription() As String
      Get
        Return GetProperty(BatchDescriptionProperty)
      End Get
      Set(value As String)
        SetProperty(BatchDescriptionProperty, value)
      End Set
    End Property

    Public Shared SmsBatchTemplateIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SmsBatchTemplateID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Template", Description:=""),
    DropDownWeb(GetType(ROSmsBatchTemplateList)),
    SetExpression("SmsBatchBO.SmsBatchTemplateIDSet(self)", False)>
    Public Property SmsBatchTemplateID() As Integer?
      Get
        Return GetProperty(SmsBatchTemplateIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SmsBatchTemplateIDProperty, value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date is required")>
    Public Property StartDate() As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(StartDateProperty, value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date is required")>
    Public Property EndDate() As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(EndDateProperty, value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(UserSystemList), ValueMember:="SystemID", DisplayMember:="SystemName")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemIDProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROSystemAllowedAreaList), ThisFilterMember:="SystemID", ValueMember:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(ROProductionAreaAllowedDisciplineList), FilterMethodName:="SmsBatchBO.FilteredDisciplineList", DisplayMember:="Discipline", ValueMember:="DisciplineID")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(DisciplineIDProperty, value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing)
    ''' <summary>
    ''' Gets the ContractType value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="Contract Type"),
    DropDownWeb(GetType(ROContractTypeList))>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ContractTypeIDProperty, value)
      End Set
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SelectRecipientsManuallyProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.SelectRecipientsManually, False)
    ''' <summary>
    ''' Gets and sets the Batch Description value
    ''' </summary>
    <Display(Name:="Select Recipients Manually")>
    Public Property SelectRecipientsManually() As Boolean
      Get
        Return GetProperty(SelectRecipientsManuallyProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectRecipientsManuallyProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, Nothing)
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Event"),
    DropDownWeb(GetType(ROPSAListSms), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SmsBatchBO.setProductionSystemAreaCriteriaBeforeRefresh",
                PreFindJSFunction:="SmsBatchBO.triggerProductionSystemAreaAutoPopulate",
                AfterFetchJS:="SmsBatchBO.afterProductionSystemAreaRefreshAjax",
                OnItemSelectJSFunction:="SmsBatchBO.onProductionSystemAreaIDSelected",
                LookupMember:="ProductionSystemAreaName", DisplayMember:="ProductionSystemAreaName", ValueMember:="ProductionSystemAreaID",
                DropDownColumns:={"SubDept", "Area", "ProductionSystemAreaName", "MinSD", "MaxED", "Room"})>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return ReadProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionSystemAreaName, "")
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Event")>
    Public Property ProductionSystemAreaName() As String
      Get
        Return ReadProperty(ProductionSystemAreaNameProperty)
      End Get
      Set(ByVal Value As String)
        LoadProperty(ProductionSystemAreaNameProperty, Value)
      End Set
    End Property

    Public Shared AllDaysProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.AllDays, False)
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Select All Days"),
    SetExpression("SmsBatchBO.AllDaysSet(self)")>
    Public Property AllDays() As Boolean
      Get
        Return ReadProperty(AllDaysProperty)
      End Get
      Set(ByVal Value As Boolean)
        LoadProperty(AllDaysProperty, Value)
      End Set
    End Property

    Public Shared BatchSendDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.BatchSendDateTime, Nothing)
    ''' <summary>
    ''' Gets the Batch Description value
    ''' </summary>
    <Display(Name:="Send Time", Description:=""),
    Singular.DataAnnotations.DateAndTimeField(ShowClear:=True)>
    Public Property BatchSendDateTime() As DateTime?
      Get
        Return GetProperty(BatchSendDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(BatchSendDateTimeProperty, value)
      End Set
    End Property

    Public Shared CancellationsProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.Cancellations, Nothing)
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Cancellations?"),
    SetExpression("SmsBatchBO.CancellationsSet(self)")>
    Public Property Cancellations() As Boolean?
      Get
        Return ReadProperty(CancellationsProperty)
      End Get
      Set(ByVal Value As Boolean?)
        LoadProperty(CancellationsProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SmsBatchRecipientListProperty As PropertyInfo(Of SmsBatchRecipientList) = RegisterProperty(Of SmsBatchRecipientList)(Function(c) c.SmsBatchRecipientList, "Sms Batch Recipient List")
    Public ReadOnly Property SmsBatchRecipientList() As SmsBatchRecipientList
      Get
        If GetProperty(SmsBatchRecipientListProperty) Is Nothing Then
          LoadProperty(SmsBatchRecipientListProperty, Notifications.Sms.SmsBatchRecipientList.NewSmsBatchRecipientList())
        End If
        Return GetProperty(SmsBatchRecipientListProperty)
      End Get
    End Property

    Public Shared BatchSmsListProperty As PropertyInfo(Of BatchSmsList) = RegisterProperty(Of BatchSmsList)(Function(c) c.BatchSmsList, "Sms List")
    Public ReadOnly Property BatchSmsList() As BatchSmsList
      Get
        If GetProperty(BatchSmsListProperty) Is Nothing Then
          LoadProperty(BatchSmsListProperty, Notifications.Sms.BatchSmsList.NewBatchSmsList())
        End If
        Return GetProperty(BatchSmsListProperty)
      End Get
    End Property

    Public Shared SmsBatchDateListProperty As PropertyInfo(Of SmsBatchDateList) = RegisterProperty(Of SmsBatchDateList)(Function(c) c.SmsBatchDateList, "Sms Batch Dates")
    Public ReadOnly Property SmsBatchDateList() As SmsBatchDateList
      Get
        If GetProperty(SmsBatchDateListProperty) Is Nothing Then
          LoadProperty(SmsBatchDateListProperty, Notifications.SmS.SmsBatchDateList.NewSmsBatchDateList())
        End If
        Return GetProperty(SmsBatchDateListProperty)
      End Get
    End Property

    Public Shared SmsBatchCrewTypeListProperty As PropertyInfo(Of SmsBatchCrewTypeList) = RegisterProperty(Of SmsBatchCrewTypeList)(Function(c) c.SmsBatchCrewTypeList, "Sms Batch Dates")
    Public ReadOnly Property SmsBatchCrewTypeList() As SmsBatchCrewTypeList
      Get
        If GetProperty(SmsBatchCrewTypeListProperty) Is Nothing Then
          LoadProperty(SmsBatchCrewTypeListProperty, Notifications.SmS.SmsBatchCrewTypeList.NewSmsBatchCrewTypeList())
        End If
        Return GetProperty(SmsBatchCrewTypeListProperty)
      End Get
    End Property

    Public Shared SmsBatchCrewGroupListProperty As PropertyInfo(Of SmsBatchCrewGroupList) = RegisterProperty(Of SmsBatchCrewGroupList)(Function(c) c.SmsBatchCrewGroupList, "Sms Batch Dates")
    Public ReadOnly Property SmsBatchCrewGroupList() As SmsBatchCrewGroupList
      Get
        If GetProperty(SmsBatchCrewGroupListProperty) Is Nothing Then
          LoadProperty(SmsBatchCrewGroupListProperty, Notifications.SmS.SmsBatchCrewGroupList.NewSmsBatchCrewGroupList())
        End If
        Return GetProperty(SmsBatchCrewGroupListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Batch")
        Else
          Return String.Format("Blank {0}", "Sms Batch")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(SmsBatchTemplateIDProperty)
        .ServerRuleFunction = AddressOf SystemIDValid
        .JavascriptRuleFunctionName = "SmsBatchBO.SmsBatchTemplateIDValid"
        .AddTriggerProperties({SystemIDProperty, ProductionAreaIDProperty, StartDateProperty, EndDateProperty, DisciplineIDProperty, ContractTypeIDProperty, ProductionSystemAreaIDProperty})
        .AffectedProperties.AddRange({SystemIDProperty, ProductionAreaIDProperty, StartDateProperty, EndDateProperty, DisciplineIDProperty, ContractTypeIDProperty, ProductionSystemAreaIDProperty})
      End With

      'With AddWebRule(SystemIDProperty)
      '  .ServerRuleFunction = AddressOf SystemIDValid
      '  .JavascriptRuleFunctionName = "EmailBatchBO.SystemIDValid"
      '  .AddTriggerProperties({SelectRecipientsManuallyProperty})
      'End With

    End Sub

    Public Shared Function SystemIDValid(Instance As SmsBatch) As String

      If Not Instance.SelectRecipientsManually And IsNullNothing(Instance.SystemID) Then
        Return "Sub-Dept is required"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsBatch() method.

    End Sub

    Public Shared Function NewSmsBatch() As SmsBatch

      Return DataPortal.CreateChild(Of SmsBatch)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSmsBatch(dr As SafeDataReader) As SmsBatch

      Dim s As New SmsBatch()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsBatchIDProperty, .GetInt32(0))
          LoadProperty(CreatedByProperty, .GetInt32(1))
          LoadProperty(CreatedDateTimeProperty, .GetValue(2))
          LoadProperty(BatchNameProperty, .GetString(3))
          LoadProperty(BatchDescriptionProperty, .GetString(4))
          LoadProperty(SmsBatchTemplateIDProperty, ZeroNothing(.GetInt32(5)))
          LoadProperty(StartDateProperty, .GetValue(6))
          LoadProperty(EndDateProperty, .GetValue(7))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(8)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(DisciplineIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(ContractTypeIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(13))
          LoadProperty(SelectRecipientsManuallyProperty, .GetBoolean(14))
          LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(15)))
          LoadProperty(ProductionSystemAreaNameProperty, .GetString(16))
          LoadProperty(BatchSendDateTimeProperty, .GetValue(17))
          LoadProperty(CancellationsProperty, .GetBoolean(18))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsBatch"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsBatch"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsBatchID As SqlParameter = .Parameters.Add("@SmsBatchID", SqlDbType.Int)
          paramSmsBatchID.Value = GetProperty(SmsBatchIDProperty)
          If Me.IsNew Then
            paramSmsBatchID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CreatedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@BatchName", GetProperty(BatchNameProperty))
          .Parameters.AddWithValue("@BatchDescription", GetProperty(BatchDescriptionProperty))
          .Parameters.AddWithValue("@SmsBatchTemplateID", NothingDBNull(GetProperty(SmsBatchTemplateIDProperty)))
          .Parameters.AddWithValue("@StartDate", NothingDBNull(GetProperty(StartDateProperty)))
          .Parameters.AddWithValue("@EndDate", NothingDBNull(GetProperty(EndDateProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@ContractTypeID", NothingDBNull(GetProperty(ContractTypeIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SelectRecipientsManually", GetProperty(SelectRecipientsManuallyProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          .Parameters.AddWithValue("@BatchSendDateTime", NothingDBNull(GetProperty(BatchSendDateTimeProperty)))
          .Parameters.AddWithValue("@Cancellations", NothingDBNull(GetProperty(CancellationsProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsBatchIDProperty, paramSmsBatchID.Value)
          End If
          ' update child objects
          If GetProperty(SmsBatchRecipientListProperty) IsNot Nothing Then
            Me.SmsBatchRecipientList.Update()
          End If
          If GetProperty(BatchSmsListProperty) IsNot Nothing Then
            Me.BatchSmsList.Update()
          End If
          If GetProperty(SmsBatchDateListProperty) IsNot Nothing Then
            Me.SmsBatchDateList.Update()
          End If
          If GetProperty(SmsBatchCrewTypeListProperty) IsNot Nothing Then
            Me.SmsBatchCrewTypeList.Update()
          End If
          UpdateChild(GetProperty(SmsBatchCrewGroupListProperty))
          MarkOld()
        End With
      Else
        If GetProperty(SmsBatchRecipientListProperty) IsNot Nothing Then
          Me.SmsBatchRecipientList.Update()
        End If
        If GetProperty(BatchSmsListProperty) IsNot Nothing Then
          Me.BatchSmsList.Update()
        End If
        If GetProperty(SmsBatchDateListProperty) IsNot Nothing Then
          Me.SmsBatchDateList.Update()
        End If
        If GetProperty(SmsBatchCrewTypeListProperty) IsNot Nothing Then
          Me.SmsBatchCrewTypeList.Update()
        End If
        UpdateChild(GetProperty(SmsBatchCrewGroupListProperty))
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsBatch"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsBatchID", GetProperty(SmsBatchIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub GenerateSmses()

      If Not Me.IsNew AndAlso Not Me.IsDirty Then
        Select Case Me.SmsBatchTemplateID
          Case OBLib.CommonData.Enums.SmsBatchTemplate.Schedule_ProductionServices
            GenerateScheduleProductionServicesStudio()
          Case OBLib.CommonData.Enums.SmsBatchTemplate.Schedule_PlayoutOperations
            GenerateSchedulePlayoutOperations()
          Case OBLib.CommonData.Enums.SmsBatchTemplate.Schedule_OB
            GenerateScheduleOB()
          Case OBLib.CommonData.Enums.SmsBatchTemplate.Schedule_ICR
            GenerateScheduleICR()
          Case OBLib.CommonData.Enums.SmsBatchTemplate.Schedule_ContentStudio
            GenerateScheduleProductionContentStudio()
          Case OBLib.CommonData.Enums.SmsBatchTemplate.Schedule_Maximo
            GenerateScheduleMaximo()
        End Select
      Else
        Throw New Exception("Please ensure that the Sms Batch has been saved first")
      End If

    End Sub

    Private Sub GenerateScheduleProductionServicesStudio()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchProductionServicesStudio",
                                              New String() {"@SmsBatchID", "@CurrentUserID"},
                                              New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      cmdProc.CommandTimeout = 0
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute()

    End Sub

    Private Sub GenerateSchedulePlayoutOperations()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchPlayoutOperations",
                                              New String() {"@SmsBatchID", "@CurrentUserID"},
                                              New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      cmdProc.CommandTimeout = 0
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute()

    End Sub

    Private Sub GenerateScheduleOB()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchProductionServicesOBTemp",
                                          New String() {"@SmsBatchID", "@CurrentUserID"},
                                          New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      'Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchProductionServicesOB",
      '                                          New String() {"@SmsBatchID", "@CurrentUserID"},
      '                                          New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      cmdProc.CommandTimeout = 0
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute()

    End Sub

    Private Sub GenerateScheduleICR()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchICR",
                                                New String() {"@SmsBatchID", "@CurrentUserID"},
                                                New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      cmdProc.CommandTimeout = 0
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute()

    End Sub

    Private Sub GenerateScheduleProductionContentStudio()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchProductionContentStudio",
                                              New String() {"@SmsBatchID", "@CurrentUserID"},
                                              New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      cmdProc.CommandTimeout = 0
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute()

    End Sub

    Private Sub GenerateScheduleMaximo()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdSMSGenerateBatchMaximo",
                                              New String() {"@SmsBatchID", "@CurrentUserID"},
                                              New Object() {NothingDBNull(Me.SmsBatchID), OBLib.Security.Settings.CurrentUserID})
      cmdProc.CommandTimeout = 0
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute()

    End Sub

  End Class

End Namespace