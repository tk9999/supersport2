﻿' Generated 07 Sep 2016 12:10 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROPSADay
    Inherits OBReadOnlyBase(Of ROPSADay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared TimelineDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.TimelineDate, "Timeline Date")
    ''' <summary>
    ''' Gets the Timeline Date value
    ''' </summary>
    <Display(Name:="Timeline Date", Description:="")>
    Public ReadOnly Property TimelineDate() As DateTime
      Get
        Return GetProperty(TimelineDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TimelineDate

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROPSADay(dr As SafeDataReader) As ROPSADay

      Dim r As New ROPSADay()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(0))
        LoadProperty(TimelineDateProperty, .GetValue(1))
      End With

    End Sub

#End Region

  End Class

End Namespace