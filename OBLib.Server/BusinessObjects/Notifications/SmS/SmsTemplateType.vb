﻿' Generated 10 Oct 2015 18:51 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsTemplateType
    Inherits OBBusinessBase(Of SmsTemplateType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsTemplateTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsTemplateTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SmsTemplateTypeID() As Integer
      Get
        Return GetProperty(SmsTemplateTypeIDProperty)
      End Get
    End Property

    Public Shared SmsTemplateTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SmsTemplateType, "Sms Template Type", "")
    ''' <summary>
    ''' Gets and sets the Sms Template Type value
    ''' </summary>
    <Display(Name:="Sms Template Type", Description:=""),
    StringLength(50, ErrorMessage:="Sms Template Type cannot be more than 50 characters")>
  Public Property SmsTemplateType() As String
      Get
        Return GetProperty(SmsTemplateTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SmsTemplateTypeProperty, Value)
      End Set
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", True)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
  Public Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemIndProperty, Value)
      End Set
    End Property

    Public Shared MessageTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MessageTemplate, "Message Template", "")
    ''' <summary>
    ''' Gets and sets the Message Template value
    ''' </summary>
    <Display(Name:="Message Template", Description:="")>
  Public Property MessageTemplate() As String
      Get
        Return GetProperty(MessageTemplateProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MessageTemplateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsTemplateTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SmsTemplateType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Template Type")
        Else
          Return String.Format("Blank {0}", "Sms Template Type")
        End If
      Else
        Return Me.SmsTemplateType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsTemplateType() method.

    End Sub

    Public Shared Function NewSmsTemplateType() As SmsTemplateType

      Return DataPortal.CreateChild(Of SmsTemplateType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSmsTemplateType(dr As SafeDataReader) As SmsTemplateType

      Dim s As New SmsTemplateType()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsTemplateTypeIDProperty, .GetInt32(0))
          LoadProperty(SmsTemplateTypeProperty, .GetString(1))
          LoadProperty(SystemIndProperty, .GetBoolean(2))
          LoadProperty(MessageTemplateProperty, .GetString(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsTemplateType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsTemplateType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsTemplateTypeID As SqlParameter = .Parameters.Add("@SmsTemplateTypeID", SqlDbType.Int)
          paramSmsTemplateTypeID.Value = GetProperty(SmsTemplateTypeIDProperty)
          If Me.IsNew Then
            paramSmsTemplateTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SmsTemplateType", GetProperty(SmsTemplateTypeProperty))
          .Parameters.AddWithValue("@SystemInd", GetProperty(SystemIndProperty))
          .Parameters.AddWithValue("@MessageTemplate", GetProperty(MessageTemplateProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsTemplateTypeIDProperty, paramSmsTemplateTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsTemplateType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsTemplateTypeID", GetProperty(SmsTemplateTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace