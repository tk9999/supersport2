﻿' Generated 28 Mar 2017 08:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsSearchRecipient
    Inherits OBBusinessBase(Of SmsSearchRecipient)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SmsSearchRecipientBO.SmsSearchRecipientBOToString(self)")

    Public Shared SmsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SmsID() As Integer
      Get
        Return GetProperty(SmsIDProperty)
      End Get
    End Property

    Public Shared SmsRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsRecipientID, "Sms Recipient")
    ''' <summary>
    ''' Gets and sets the Sms Recipient value
    ''' </summary>
    <Display(Name:="Sms Recipient", Description:=""),
    Required(ErrorMessage:="Sms Recipient required")>
  Public Property SmsRecipientID() As Integer
      Get
        Return GetProperty(SmsRecipientIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsRecipientIDProperty, Value)
      End Set
    End Property

    Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RecipientName, "Recipient Name")
    ''' <summary>
    ''' Gets and sets the Recipient Name value
    ''' </summary>
    <Display(Name:="Recipient Name", Description:="")>
  Public Property RecipientName() As String
      Get
        Return GetProperty(RecipientNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RecipientNameProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
  Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CellNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellNo, "Cell No")
    ''' <summary>
    ''' Gets and sets the Cell No value
    ''' </summary>
    <Display(Name:="Cell No", Description:="")>
  Public Property CellNo() As String
      Get
        Return GetProperty(CellNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellNoProperty, Value)
      End Set
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.SentDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="")>
    Public Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SentDateProperty, Value)
      End Set
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:="")>
  Public Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotSentErrorProperty, Value)
      End Set
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status Code")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Status Code", Description:="")>
  Public Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCodeProperty, Value)
      End Set
    End Property

    Public Shared StatusDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDescription, "Status Description")
    ''' <summary>
    ''' Gets and sets the Status Description value
    ''' </summary>
    <Display(Name:="Status Description", Description:="")>
  Public Property StatusDescription() As String
      Get
        Return GetProperty(StatusDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StatusDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StatusDateTime, "Status Date Time")
    ''' <summary>
    ''' Gets and sets the Status Date Time value
    ''' </summary>
    <Display(Name:="Status Date Time", Description:=""),
    Required(ErrorMessage:="Status Date Time required")>
  Public Property StatusDateTime As Date
      Get
        Return GetProperty(StatusDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StatusDateTimeProperty, Value)
      End Set
    End Property

    Public Shared StatusDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDateString, "Status Date String")
    ''' <summary>
    ''' Gets and sets the Status Date String value
    ''' </summary>
    <Display(Name:="Status Date String", Description:="")>
  Public Property StatusDateString() As String
      Get
        Return GetProperty(StatusDateStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusDateStringProperty, Value)
      End Set
    End Property

    Public Shared ButtonCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ButtonCss, "Button Css")
    ''' <summary>
    ''' Gets and sets the Button Css value
    ''' </summary>
    <Display(Name:="Button Css", Description:="")>
  Public Property ButtonCss() As String
      Get
        Return GetProperty(ButtonCssProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ButtonCssProperty, Value)
      End Set
    End Property

    Public Shared IconCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IconCss, "Icon Css")
    ''' <summary>
    ''' Gets and sets the Icon Css value
    ''' </summary>
    <Display(Name:="Icon Css", Description:="")>
  Public Property IconCss() As String
      Get
        Return GetProperty(IconCssProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IconCssProperty, Value)
      End Set
    End Property

    Public Shared StateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.State, "State")
    ''' <summary>
    ''' Gets and sets the Icon Css value
    ''' </summary>
    <Display(Name:="State", Description:="")>
    Public Property State() As String
      Get
        Return GetProperty(StateProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StateProperty, Value)
      End Set
    End Property

    Public Shared ImageFileNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageFileName, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the ImageFileName value
    ''' </summary>
    <Display(Name:="Crew Type", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property ImageFileName() As String
      Get
        Return GetProperty(ImageFileNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImageFileNameProperty, Value)
      End Set
    End Property

    Public Shared RemainingRetriesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RemainingRetries, "Sm S Batch", 3)
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Sm S Batch", Description:="")>
    Public Property RemainingRetries() As Integer
      Get
        Return GetProperty(RemainingRetriesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RemainingRetriesProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SmsSearch

      Return CType(CType(Me.Parent, SmsSearchRecipientList).Parent, SmsSearch)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RecipientName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Search Recipient")
        Else
          Return String.Format("Blank {0}", "Sms Search Recipient")
        End If
      Else
        Return Me.RecipientName
      End If

    End Function

    Public Sub Retry()
      Dim proc As New Singular.CommandProc("CmdProcs.cmdSMSPrepareForSending",
                                           New String() {"@SmsBatchID", "@SmsIDsXml", "@CurrentUserID", "@SmsRecipientID"},
                                           New Object() {NothingDBNull(Nothing), NothingDBNull(Nothing), OBLib.Security.Settings.CurrentUserID, Me.SmsRecipientID})
      proc = proc.Execute()
      'Me.Ignore = False
      Me.SentDate = Nothing
      'Me.DateToSend = DateTime.Now
      Me.NotSentError = ""
      Me.StatusCode = ""
      Me.StatusDescription = "Pending"
      Me.RemainingRetries = 1
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsSearchRecipient() method.

    End Sub

    Public Shared Function NewSmsSearchRecipient() As SmsSearchRecipient

      Return DataPortal.CreateChild(Of SmsSearchRecipient)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSmsSearchRecipient(dr As SafeDataReader) As SmsSearchRecipient

      Dim s As New SmsSearchRecipient()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsIDProperty, .GetInt32(0))
          LoadProperty(SmsRecipientIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RecipientNameProperty, .GetString(2))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CellNoProperty, .GetString(4))
          LoadProperty(SentDateProperty, .GetValue(5))
          LoadProperty(NotSentErrorProperty, .GetString(6))
          LoadProperty(StatusCodeProperty, .GetString(7))
          LoadProperty(StatusDescriptionProperty, .GetString(8))
          LoadProperty(StatusDateTimeProperty, .GetValue(9))
          LoadProperty(StatusDateStringProperty, .GetString(10))
          LoadProperty(ButtonCssProperty, .GetString(11))
          LoadProperty(IconCssProperty, .GetString(12))
          LoadProperty(StateProperty, .GetString(13))
          LoadProperty(ImageFileNameProperty, .GetString(14))
          LoadProperty(RemainingRetriesProperty, .GetInt32(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SmsRecipientIDProperty)

      If Me.GetParent Is Nothing Then
        cm.Parameters.AddWithValue("@SmsID", GetProperty(SmsIDProperty))
      Else
        cm.Parameters.AddWithValue("@SmsID", Me.GetParent.SmsID)
      End If
      cm.Parameters.AddWithValue("@CellNo", GetProperty(CellNoProperty))
      cm.Parameters.AddWithValue("@RecipientName", GetProperty(RecipientNameProperty))
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@SentDate", SentDate)
      cm.Parameters.AddWithValue("@NotSentError", GetProperty(NotSentErrorProperty))
      cm.Parameters.AddWithValue("@StatusCode", GetProperty(StatusCodeProperty))
      cm.Parameters.AddWithValue("@StatusDescription", GetProperty(StatusDescriptionProperty))
      cm.Parameters.AddWithValue("@StatusDateTime", GetProperty(StatusDateTimeProperty))
      cm.Parameters.AddWithValue("@RemainingRetries", GetProperty(RemainingRetriesProperty))

      'cm.Parameters.AddWithValue("@StatusDateString", GetProperty(StatusDateStringProperty))
      'cm.Parameters.AddWithValue("@ButtonCss", GetProperty(ButtonCssProperty))
      'cm.Parameters.AddWithValue("@IconCss", GetProperty(IconCssProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SmsIDProperty, cm.Parameters("@SmsID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SmsID", GetProperty(SmsIDProperty))
    End Sub

#End Region

  End Class

End Namespace