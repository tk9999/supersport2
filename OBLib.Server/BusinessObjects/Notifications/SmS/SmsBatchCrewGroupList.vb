﻿' Generated 11 May 2017 07:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchCrewGroupList
    Inherits OBBusinessListBase(Of SmsBatchCrewGroupList, SmsBatchCrewGroup)

#Region " Business Methods "

    Public Function GetItem(SmsBatchCrewGroupID As Integer) As SmsBatchCrewGroup

      For Each child As SmsBatchCrewGroup In Me
        If child.SmsBatchCrewGroupID = SmsBatchCrewGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Sms Batch Crew Groups"

    End Function

    Public Function GetSmsBatchCrewGroupHumanResource(SmsBatchCrewGroupHumanResourceID As Integer) As SmsBatchCrewGroupHumanResource

      Dim obj As SmsBatchCrewGroupHumanResource = Nothing
      For Each parent As SmsBatchCrewGroup In Me
        obj = parent.SmsBatchCrewGroupHumanResourceList.GetItem(SmsBatchCrewGroupHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSmsBatchCrewGroupList() As SmsBatchCrewGroupList

      Return New SmsBatchCrewGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSmsBatchCrewGroupList() As SmsBatchCrewGroupList

      Return DataPortal.Fetch(Of SmsBatchCrewGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SmsBatchCrewGroup.GetSmsBatchCrewGroup(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SmsBatchCrewGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsBatchCrewGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SmsBatchCrewGroupHumanResourceList.RaiseListChangedEvents = False
          parent.SmsBatchCrewGroupHumanResourceList.Add(SmsBatchCrewGroupHumanResource.GetSmsBatchCrewGroupHumanResource(sdr))
          parent.SmsBatchCrewGroupHumanResourceList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As SmsBatchCrewGroup In Me
        child.CheckRules()
        For Each SmsBatchCrewGroupHumanResource As SmsBatchCrewGroupHumanResource In child.SmsBatchCrewGroupHumanResourceList
          SmsBatchCrewGroupHumanResource.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSmsBatchCrewGroupList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace