﻿' Generated 17 Sep 2015 16:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.SmS

  <Serializable()> _
  Public Class SmsBatchTemplate
    Inherits SingularBusinessBase(Of SmsBatchTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchTemplateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchTemplateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SmsBatchTemplateID() As Integer
      Get
        Return GetProperty(SmsBatchTemplateIDProperty)
      End Get
    End Property

    Public Shared SmsBatchTemplateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SmsBatchTemplate, "Sms Batch Template", "")
    ''' <summary>
    ''' Gets and sets the Sms Batch Template value
    ''' </summary>
    <Display(Name:="Sms Batch Template", Description:=""),
    StringLength(50, ErrorMessage:="Sms Batch Template cannot be more than 50 characters")>
    Public Property SmsBatchTemplate() As String
      Get
        Return GetProperty(SmsBatchTemplateProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SmsBatchTemplateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchTemplateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SmsBatchTemplate.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Batch Template")
        Else
          Return String.Format("Blank {0}", "Sms Batch Template")
        End If
      Else
        Return Me.SmsBatchTemplate
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsBatchTemplate() method.

    End Sub

    Public Shared Function NewSmsBatchTemplate() As SmsBatchTemplate

      Return DataPortal.CreateChild(Of SmsBatchTemplate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSmsBatchTemplate(dr As SafeDataReader) As SmsBatchTemplate

      Dim s As New SmsBatchTemplate()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsBatchTemplateIDProperty, .GetInt32(0))
          LoadProperty(SmsBatchTemplateProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsBatchTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsBatchTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsBatchTemplateID As SqlParameter = .Parameters.Add("@SmsBatchTemplateID", SqlDbType.Int)
          paramSmsBatchTemplateID.Value = GetProperty(SmsBatchTemplateIDProperty)
          If Me.IsNew Then
            paramSmsBatchTemplateID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SmsBatchTemplate", GetProperty(SmsBatchTemplateProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsBatchTemplateIDProperty, paramSmsBatchTemplateID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsBatchTemplate"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsBatchTemplateID", GetProperty(SmsBatchTemplateIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace