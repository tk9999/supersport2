﻿' Generated 11 Sep 2015 10:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.SmS

  <Serializable()> _
  Public Class SMSResponse
    Inherits SingularBusinessBase(Of SMSResponse)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SMSResponseIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SMSResponseID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SMSResponseID() As Integer
      Get
        Return GetProperty(SMSResponseIDProperty)
      End Get
    End Property

    Public Shared ApiIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ApiID, "Api", 0)
    ''' <summary>
    ''' Gets and sets the Api value
    ''' </summary>
    <Display(Name:="Api", Description:="")>
    Public Property ApiID() As Integer
      Get
        Return GetProperty(ApiIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ApiIDProperty, Value)
      End Set
    End Property

    Public Shared ApiMsgIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ApiMsgID, "Api Msg", "")
    ''' <summary>
    ''' Gets and sets the Api Msg value
    ''' </summary>
    <Display(Name:="Api Msg", Description:="")>
    Public Property ApiMsgID() As String
      Get
        Return GetProperty(ApiMsgIDProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ApiMsgIDProperty, Value)
      End Set
    End Property

    Public Shared ClientMsgIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClientMsgID, "Client Msg", "")
    ''' <summary>
    ''' Gets and sets the Client Msg value
    ''' </summary>
    <Display(Name:="Client Msg", Description:="")>
    Public Property ClientMsgID() As String
      Get
        Return GetProperty(ClientMsgIDProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClientMsgIDProperty, Value)
      End Set
    End Property

    Public Shared TimestampProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Timestamp, "Timestamp", "")
    ''' <summary>
    ''' Gets and sets the Timestamp value
    ''' </summary>
    <Display(Name:="Timestamp", Description:="")>
    Public Property Timestamp() As String
      Get
        Return GetProperty(TimestampProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimestampProperty, Value)
      End Set
    End Property

    Public Shared ToNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ToNumber, "To Number", "")
    ''' <summary>
    ''' Gets and sets the To Number value
    ''' </summary>
    <Display(Name:="To Number", Description:="")>
    Public Property ToNumber() As String
      Get
        Return GetProperty(ToNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ToNumberProperty, Value)
      End Set
    End Property

    Public Shared FromNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromNumber, "From Number", "")
    ''' <summary>
    ''' Gets and sets the From Number value
    ''' </summary>
    <Display(Name:="From Number", Description:="")>
    Public Property FromNumber() As String
      Get
        Return GetProperty(FromNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FromNumberProperty, Value)
      End Set
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status Code", "")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Status Code", Description:="")>
    Public Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCodeProperty, Value)
      End Set
    End Property

    Public Shared StatusDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDescription, "Status Description", "")
    ''' <summary>
    ''' Gets and sets the Status Description value
    ''' </summary>
    <Display(Name:="Status Description", Description:="")>
    Public Property StatusDescription() As String
      Get
        Return GetProperty(StatusDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusDescriptionProperty, Value)
      End Set
    End Property

    Public Shared ChargeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Charge, "Charge", "")
    ''' <summary>
    ''' Gets and sets the Charge value
    ''' </summary>
    <Display(Name:="Charge", Description:="")>
    Public Property Charge() As String
      Get
        Return GetProperty(ChargeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChargeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SMSResponseIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Timestamp.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "SMS Response")
        Else
          Return String.Format("Blank {0}", "SMS Response")
        End If
      Else
        Return Me.Timestamp
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSMSResponse() method.

    End Sub

    Public Shared Function NewSMSResponse() As SMSResponse

      Return DataPortal.CreateChild(Of SMSResponse)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Public Sub New(ApiID As Integer, ApiMsgID As String, ClientMsgID As String,
                   Timestamp As String, ToNumber As String, FromNumber As String,
                   StatusCode As String, StatusDescription As String,
                   Charge As String)
      Me.ApiID = ApiID
      Me.ApiMsgID = ApiMsgID
      Me.ClientMsgID = ClientMsgID
      Me.Timestamp = Timestamp
      Me.ToNumber = ToNumber
      Me.FromNumber = FromNumber
      Me.StatusCode = StatusCode
      Me.StatusDescription = StatusDescription
      Me.Charge = Charge

    End Sub

    Public Sub SetDescription()

      Select Case Me.StatusCode
        Case "001"
          Me.StatusDescription = "Message unknown"
        Case "002"
          Me.StatusDescription = "Message queued"
        Case "003"
          Me.StatusDescription = "Delivered to gateway"
        Case "004"
          Me.StatusDescription = "Received by recipient"
        Case "005"
          Me.StatusDescription = "Error with message"
        Case "006"
          Me.StatusDescription = "User cancelled message delivery"
        Case "007"
          Me.StatusDescription = "Error delivering message"
        Case "008"
          'Code 8 does not exist
          'Me.StatusDescription = "Routing error"
        Case "009"
          Me.StatusDescription = "Routing error"
        Case "010"
          Me.StatusDescription = "Message expired"
        Case "011"
          Me.StatusDescription = "Message scheduled for later delivery"
        Case "012"
          Me.StatusDescription = "Out of credit"
        Case "013"
          Me.StatusDescription = "Clickatell cancelled message delivery"
        Case "014"
          Me.StatusDescription = "Maximum MT limit exceeded"
      End Select

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSMSResponse(dr As SafeDataReader) As SMSResponse

      Dim s As New SMSResponse()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SMSResponseIDProperty, .GetInt32(0))
          LoadProperty(ApiIDProperty, .GetInt32(1))
          LoadProperty(ApiMsgIDProperty, .GetString(2))
          LoadProperty(ClientMsgIDProperty, .GetString(3))
          LoadProperty(TimestampProperty, .GetString(4))
          LoadProperty(ToNumberProperty, .GetString(5))
          LoadProperty(FromNumberProperty, .GetString(6))
          LoadProperty(StatusCodeProperty, .GetString(7))
          LoadProperty(StatusDescriptionProperty, .GetString(8))
          LoadProperty(ChargeProperty, .GetString(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSMSResponse"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSMSResponse"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSMSResponseID As SqlParameter = .Parameters.Add("@SMSResponseID", SqlDbType.Int)
          paramSMSResponseID.Value = GetProperty(SMSResponseIDProperty)
          If Me.IsNew Then
            paramSMSResponseID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ApiID", GetProperty(ApiIDProperty))
          .Parameters.AddWithValue("@ApiMsgID", GetProperty(ApiMsgIDProperty))
          .Parameters.AddWithValue("@ClientMsgID", GetProperty(ClientMsgIDProperty))
          .Parameters.AddWithValue("@Timestamp", GetProperty(TimestampProperty))
          .Parameters.AddWithValue("@ToNumber", GetProperty(ToNumberProperty))
          .Parameters.AddWithValue("@FromNumber", GetProperty(FromNumberProperty))
          .Parameters.AddWithValue("@StatusCode", GetProperty(StatusCodeProperty))
          .Parameters.AddWithValue("@StatusDescription", GetProperty(StatusDescriptionProperty))
          .Parameters.AddWithValue("@Charge", GetProperty(ChargeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SMSResponseIDProperty, paramSMSResponseID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSMSResponse"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SMSResponseID", GetProperty(SMSResponseIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace