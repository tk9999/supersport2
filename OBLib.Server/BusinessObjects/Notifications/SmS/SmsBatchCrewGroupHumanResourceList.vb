﻿' Generated 11 May 2017 07:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchCrewGroupHumanResourceList
    Inherits OBBusinessListBase(Of SmsBatchCrewGroupHumanResourceList, SmsBatchCrewGroupHumanResource)

#Region " Business Methods "

    Public Function GetItem(SmsBatchCrewGroupHumanResourceID As Integer) As SmsBatchCrewGroupHumanResource

      For Each child As SmsBatchCrewGroupHumanResource In Me
        If child.SmsBatchCrewGroupHumanResourceID = SmsBatchCrewGroupHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Sms Batch Crew Group Human Resources"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSmsBatchCrewGroupHumanResourceList() As SmsBatchCrewGroupHumanResourceList

      Return New SmsBatchCrewGroupHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace