﻿' Generated 12 Jun 2015 07:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.SmsSending

#If SILVERLIGHT Then
#Else
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Notifications.SmS

  <Serializable()> _
  Public Class SmsRecipient
    Inherits SingularBusinessBase(Of SmsRecipient)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsRecipientID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SmsRecipientID() As Integer
      Get
        Return GetProperty(SmsRecipientIDProperty)
      End Get
    End Property

    Public Shared SmsIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmsID, "Sms", Nothing)
    ''' <summary>
    ''' Gets the Sms value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SmsID() As Integer?
      Get
        Return GetProperty(SmsIDProperty)
      End Get
    End Property

    Public Shared CellNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellNo, "Cell No", "")
    ''' <summary>
    ''' Gets and sets the Cell No value
    ''' </summary>
    <Display(Name:="Cell No", Description:=""),
    StringLength(15, ErrorMessage:="Cell No cannot be more than 15 characters")>
    Public Property CellNo() As String
      Get
        Return GetProperty(CellNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellNoProperty, Value)
      End Set
    End Property

    Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RecipientName, "Recipient", "")
    ''' <summary>
    ''' Gets and sets the Recipient Name value
    ''' </summary>
    <Display(Name:="Recipient", Description:="")>
    Public Property RecipientName() As String
      Get
        Return GetProperty(RecipientNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RecipientNameProperty, Value)
      End Set
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SentDate, "Sent Date")
    ''' <summary>
    ''' Gets and sets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent Date", Description:="")>
    Public Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SentDateProperty, Value)
      End Set
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Not Sent Error", "")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Not Sent Error", Description:=""),
    StringLength(1024, ErrorMessage:="Not Sent Error cannot be more than 1024 characters")>
    Public Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotSentErrorProperty, Value)
      End Set
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status Code", "")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Status Code", Description:=""),
    StringLength(10, ErrorMessage:="Status Code cannot be more than 10 characters")>
    Public Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCodeProperty, Value)
      End Set
    End Property

    Public Shared StatusDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDescription, "Status Description", "")
    ''' <summary>
    ''' Gets and sets the Status Description value
    ''' </summary>
    <Display(Name:="Status Description", Description:="")>
    Public Property StatusDescription() As String
      Get
        Return GetProperty(StatusDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusDescriptionProperty, Value)
      End Set
    End Property

    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property StatusGroup As String
      Get
        Select Case Me.StatusCode
          Case "001"
            Return "Error"
          Case "002"
            Return "Pending"
          Case "003"
            Return "In Transit"
          Case "004"
            Return "Delivered"
          Case "005"
            Return "Error"
          Case "006"
            Return "Cancelled"
          Case "007"
            Return "Error"
          Case "008"
            'Code 8 does not exist
            'Return "Routing error"
          Case "009"
            Return "Error"
          Case "010"
            Return "Error"
          Case "011"
            Return "Pending"
          Case "012"
            Return "Error"
          Case "013"
            Return "Cancelled"
          Case "014"
            Return "Error"
        End Select
        Return "Pending"
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.DateToSend, Nothing)
    ''' <summary>
    ''' Gets and sets the Date To Send value
    ''' </summary>
    <Display(Name:="Date and Time to send")>
    Public Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DateToSendProperty, Value)
      End Set
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", True)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="")>
    Public Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "HumanResourceID", Nothing)
    ''' <summary>
    ''' Gets the Sms value
    ''' </summary>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Sms

      Return CType(CType(Me.Parent, SmsRecipientList).Parent, Sms)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsRecipientIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CellNo.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Recipient")
        Else
          Return String.Format("Blank {0}", "Sms Recipient")
        End If
      Else
        Return Me.CellNo
      End If

    End Function

#If SILVERLIGHT Then
#Else
    Private mBatchID As String = ""
    Private mFrom As String = ""
    Private mOnComplete As Action
    Friend mHasComplete As Boolean = False
    Private Shared mSmsQueue As New Queue
    Private Shared mSendingCount As Integer = 0
#End If


    Public Class SendEvent
      Public Property EventName As String
      Public Property EventTime As Date

      Public Overrides Function ToString() As String
        Return EventName & " " & EventTime.ToString("HH:mm:ss")
      End Function

      Public Sub New(EventName As String)
        Me.EventName = EventName
        Me.EventTime = Now
      End Sub
    End Class

    Public Sub Send(Optional ByVal From As String = "", Optional BatchID As String = "", Optional OnComplete As Action = Nothing)
#If Silverlight = False Then

      mBatchID = BatchID
      mFrom = [From]
      mOnComplete = OnComplete

      If OnComplete Is Nothing Then
        'Send syncronously
        SendInternal(Me)

      Else

        SyncLock mSmsQueue
          mSmsQueue.Enqueue(Me)
        End SyncLock
        CheckSendQueue()

      End If

#End If
    End Sub

    Public Sub SendSober(Optional ByVal From As String = "", Optional BatchID As String = "", Optional OnComplete As Action = Nothing)
#If Silverlight = False Then

      mBatchID = BatchID
      mFrom = [From]
      mOnComplete = OnComplete

      If OnComplete Is Nothing Then
        'Send syncronously
        SendInternalSober(Me)
      Else
        SyncLock mSmsQueue
          mSmsQueue.Enqueue(Me)
        End SyncLock
        CheckSendQueue()
      End If

#End If
    End Sub

    Private Shared Sub CheckSendQueue()
#If Silverlight = False Then

      'Send asyncronously, but only 10 threads at a time.
      SyncLock mSmsQueue
        If mSendingCount >= 10 OrElse mSmsQueue.Count = 0 Then
          Exit Sub
        End If

        mSendingCount += 1
        Dim smsr As SmsRecipient = mSmsQueue.Dequeue

        Dim AsyncMethod As Action(Of SmsRecipient) = AddressOf SendInternal
        AsyncMethod.BeginInvoke(smsr,
          Sub(args)

            smsr.mHasComplete = True
            smsr.mOnComplete()

            SyncLock mSmsQueue
              mSendingCount -= 1
              CheckSendQueue()
            End SyncLock

          End Sub, Nothing)

      End SyncLock

#End If
    End Sub

    Private Shared Sub SendInternal(SmsRecipient As SmsRecipient)
#If Silverlight = False Then

      Try

        Dim Result As SmsResult
        'Check if there is a batch id.
        If SmsRecipient.mBatchID = "" Then
          Result = SmsSender.SendSms(SmsRecipient.CellNo, CType(SmsRecipient.GetParent, Sms).Message)
        Else
          Result = ClickatellSender.QuickSend(SmsRecipient.mBatchID, SmsRecipient.CellNo)
        End If

        If Result.Sent Then
          SmsRecipient.SentDate = Now()
        Else
          SmsRecipient.NotSentError = Result.ErrorMessage
        End If

      Catch ex As Exception
        SmsRecipient.NotSentError = Singular.Debug.RecurseExceptionMessage(ex)
      End Try

#End If
    End Sub

    Private Shared Sub SendInternalSober(SmsRecipient As SmsRecipient)

      Try
        Dim Result As SmsResult
        'Check if there is a batch id.
        If SmsRecipient.mBatchID = "" Then
          'Result = SmsSending.ClickatellSender.SendSmsWithMessageID(SmsRecipient.CellNo, CType(SmsRecipient.GetParent, Sms).Message, SmsRecipient.SmsRecipientID) 'SmsSender.SendSmsSober(SmsRecipient.CellNo, CType(SmsRecipient.GetParent, Sms).Message, SmsRecipient.SmsRecipientID)
        Else
          Result = SmsSending.ClickatellSender.QuickSend(SmsRecipient.mBatchID, SmsRecipient.CellNo)
        End If
        'If sms is sent
        If Result.Sent Then
          SmsRecipient.SentDate = Now()
        Else
          SmsRecipient.NotSentError = Result.ErrorMessage
        End If
      Catch ex As Exception
        SmsRecipient.NotSentError = Singular.Debug.RecurseExceptionMessage(ex)
      End Try

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsRecipient() method.

    End Sub

    Public Shared Function NewSmsRecipient() As SmsRecipient

      Return DataPortal.CreateChild(Of SmsRecipient)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSmsRecipient(dr As SafeDataReader) As SmsRecipient

      Dim s As New SmsRecipient()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsRecipientIDProperty, .GetInt32(0))
          LoadProperty(SmsIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CellNoProperty, .GetString(2))
          LoadProperty(RecipientNameProperty, .GetString(3))
          LoadProperty(SentDateProperty, .GetValue(4))
          LoadProperty(NotSentErrorProperty, .GetString(5))
          LoadProperty(StatusCodeProperty, .GetString(6))
          LoadProperty(StatusDescriptionProperty, .GetString(7))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsRecipientID As SqlParameter = .Parameters.Add("@SmsRecipientID", SqlDbType.Int)
          paramSmsRecipientID.Value = GetProperty(SmsRecipientIDProperty)
          If Me.IsNew Then
            paramSmsRecipientID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SmsID", Me.GetParent().SmsID)
          .Parameters.AddWithValue("@CellNo", GetProperty(CellNoProperty))
          .Parameters.AddWithValue("@RecipientName", GetProperty(RecipientNameProperty))
          .Parameters.AddWithValue("@SentDate", (New SmartDate(GetProperty(SentDateProperty))).DBValue)
          .Parameters.AddWithValue("@NotSentError", GetProperty(NotSentErrorProperty))
          .Parameters.AddWithValue("@StatusCode", GetProperty(StatusCodeProperty))
          .Parameters.AddWithValue("@StatusDescription", GetProperty(StatusDescriptionProperty))
          .Parameters.AddWithValue("@DateToSend", NothingDBNull(Me.GetParent.DateToSend))
          .Parameters.AddWithValue("@Ignore", GetProperty(IgnoreProperty))
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsRecipientIDProperty, paramSmsRecipientID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsRecipient"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsRecipientID", GetProperty(SmsRecipientIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace