﻿' Generated 02 Sep 2016 10:22 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsBatchDate
    Inherits OBBusinessBase(Of SmsBatchDate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsBatchDateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchDateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SmsBatchDateID() As Integer
      Get
        Return GetProperty(SmsBatchDateIDProperty)
      End Get
    End Property

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsBatchID, "Sms Batch", 0)
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Sms Batch", Description:=""),
    Required(ErrorMessage:="Sms Batch required")>
  Public Property SmsBatchID() As Integer
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsBatchIDProperty, Value)
      End Set
    End Property

    Public Shared BatchDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.BatchDate, "Date")
    ''' <summary>
    ''' Gets and sets the Date value
    ''' </summary>
    <Display(Name:="Date", Description:=""),
    Required(ErrorMessage:="Date required")>
  Public Property BatchDate As Date
      Get
        Return GetProperty(BatchDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(BatchDateProperty, Value)
      End Set
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsBatchDateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SmsBatchDateID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Batch Date")
        Else
          Return String.Format("Blank {0}", "Sms Batch Date")
        End If
      Else
        Return Me.SmsBatchDateID.ToString()
      End If

    End Function


    Public Function GetParent() As SmsBatch
      Return CType(CType(Me.Parent, SmsBatchDateList).Parent, SmsBatch)
    End Function


#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsBatchDate() method.

    End Sub

    Public Shared Function NewSmsBatchDate() As SmsBatchDate

      Return DataPortal.CreateChild(Of SmsBatchDate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSmsBatchDate(dr As SafeDataReader) As SmsBatchDate

      Dim s As New SmsBatchDate()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsBatchDateIDProperty, .GetInt32(0))
          LoadProperty(SmsBatchIDProperty, .GetInt32(1))
          LoadProperty(BatchDateProperty, .GetValue(2))
          LoadProperty(IsSelectedProperty, .GetBoolean(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SmsBatchDateIDProperty)

      cm.Parameters.AddWithValue("@SmsBatchID", Me.GetParent.SmsBatchID)
      cm.Parameters.AddWithValue("@Date", GetProperty(BatchDateProperty))
      cm.Parameters.AddWithValue("@IsSelected", GetProperty(IsSelectedProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SmsBatchDateIDProperty, cm.Parameters("@SmsBatchDateID").Value)
               End If
             End Sub

    End Function

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSmsBatchDate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSmsBatchDate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SmsBatchDateID", GetProperty(SmsBatchDateIDProperty))
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSmsBatchDate"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsBatchDateID", GetProperty(SmsBatchDateIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

#End Region

  End Class

End Namespace