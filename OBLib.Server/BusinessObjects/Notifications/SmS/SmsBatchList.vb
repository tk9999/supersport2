﻿' Generated 11 Jun 2015 16:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.SmS

  <Serializable()> _
  Public Class SmsBatchList
    Inherits OBBusinessListBase(Of SmsBatchList, SmsBatch)

#Region " Business Methods "

    Public Function GetItem(SmsBatchID As Integer) As SmsBatch

      For Each child As SmsBatch In Me
        If child.SmsBatchID = SmsBatchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSmsBatchCrewGroup(SmsBatchCrewGroupID As Integer) As SmsBatchCrewGroup

      For Each child As SmsBatch In Me
        For Each grandchild As SmsBatchCrewGroup In child.SmsBatchCrewGroupList
          If grandchild.SmsBatchCrewGroupID = SmsBatchCrewGroupID Then
            Return grandchild
          End If
        Next
      Next

      Return Nothing

    End Function

    'Public Function GetSms(SmsID As Integer) As Sms

    '  For Each b As SmsBatch In Me
    '    For Each child As Sms In b.SmsList
    '      If child.SmsID = SmsID Then
    '        Return child
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Sms Batches"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property BatchID As Integer? = Nothing

      Public Sub New(BatchID As Integer?)
        Me.BatchID = BatchID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSmsBatchList() As SmsBatchList

      Return New SmsBatchList()

    End Function

    Public Shared Sub BeginGetSmsBatchList(CallBack As EventHandler(Of DataPortalResult(Of SmsBatchList)))

      Dim dp As New DataPortal(Of SmsBatchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSmsBatchList() As SmsBatchList

      Return DataPortal.Fetch(Of SmsBatchList)(New Criteria())

    End Function

    Public Shared Function GetSmsBatchList(BatchID As Integer?) As SmsBatchList

      Return DataPortal.Fetch(Of SmsBatchList)(New Criteria(BatchID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SmsBatch.GetSmsBatch(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SmsBatch = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsBatchID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SmsBatchRecipientList.RaiseListChangedEvents = False
          parent.SmsBatchRecipientList.Add(SmsBatchRecipient.GetSmsBatchRecipient(sdr))
          parent.SmsBatchRecipientList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsBatchID <> sdr.GetInt32(11) Then
            parent = Me.GetItem(sdr.GetInt32(11))
          End If
          parent.BatchSmsList.RaiseListChangedEvents = False
          parent.BatchSmsList.Add(BatchSms.GetBatchSms(sdr))
          parent.BatchSmsList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsBatchID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SmsBatchDateList.RaiseListChangedEvents = False
          parent.SmsBatchDateList.Add(SmsBatchDate.GetSmsBatchDate(sdr))
          parent.SmsBatchDateList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsBatchID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SmsBatchCrewTypeList.RaiseListChangedEvents = False
          parent.SmsBatchCrewTypeList.Add(SmsBatchCrewType.GetSmsBatchCrewType(sdr))
          parent.SmsBatchCrewTypeList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SmsBatchID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SmsBatchCrewGroupList.RaiseListChangedEvents = False
          parent.SmsBatchCrewGroupList.Add(SmsBatchCrewGroup.GetSmsBatchCrewGroup(sdr))
          parent.SmsBatchCrewGroupList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentCrewGroup As SmsBatchCrewGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentCrewGroup Is Nothing OrElse parentCrewGroup.SmsBatchCrewGroupID <> sdr.GetInt32(1) Then
            parentCrewGroup = Me.GetSmsBatchCrewGroup(sdr.GetInt32(1))
          End If
          parentCrewGroup.SmsBatchCrewGroupHumanResourceList.RaiseListChangedEvents = False
          parentCrewGroup.SmsBatchCrewGroupHumanResourceList.Add(SmsBatchCrewGroupHumanResource.GetSmsBatchCrewGroupHumanResource(sdr))
          parentCrewGroup.SmsBatchCrewGroupHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each batch As SmsBatch In Me
        batch.CheckRules()
        For Each smsBatchRecip As SmsBatchRecipient In batch.SmsBatchRecipientList
          smsBatchRecip.CheckRules()
        Next
        For Each smsBatchDate As SmsBatchDate In batch.SmsBatchDateList
          smsBatchDate.CheckRules()
        Next
        For Each batchSms As BatchSms In batch.BatchSmsList
          batchSms.CheckRules()
        Next
        For Each smsBatchCrewGroup As SmsBatchCrewGroup In batch.SmsBatchCrewGroupList
          smsBatchCrewGroup.CheckRules()
          For Each smsBatchCrewGroupHumanResource As SmsBatchCrewGroupHumanResource In smsBatchCrewGroup.SmsBatchCrewGroupHumanResourceList
            smsBatchCrewGroupHumanResource.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSmsBatchList"
            cm.Parameters.AddWithValue("@BatchID", NothingDBNull(crit.BatchID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SmsBatch In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SmsBatch In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace