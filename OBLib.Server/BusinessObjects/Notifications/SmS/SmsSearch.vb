﻿' Generated 28 Mar 2017 08:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsSearch
    Inherits OBBusinessBase(Of SmsSearch)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SmsSearchBO.SmsSearchBOToString(self)")

    Public Shared SmsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property SmsID() As Integer
      Get
        Return GetProperty(SmsIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsIDProperty, Value)
      End Set
    End Property

    Public Shared ShortMessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortMessage, "Short Message")
    ''' <summary>
    ''' Gets and sets the Short Message value
    ''' </summary>
    <Display(Name:="Short Message", Description:="")>
  Public Property ShortMessage() As String
      Get
        Return GetProperty(ShortMessageProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShortMessageProperty, Value)
      End Set
    End Property

    Public Shared FullMessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FullMessage, "Full Message")
    ''' <summary>
    ''' Gets and sets the Full Message value
    ''' </summary>
    <Display(Name:="Full Message", Description:=""),
    TextField(MultiLine:=True, WordWrap:=True)>
    Public Property FullMessage() As String
      Get
        Return GetProperty(FullMessageProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FullMessageProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDate() As DateTime
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.DateToSend, "Date To Send")
    ''' <summary>
    ''' Gets and sets the Date To Send value
    ''' </summary>
    <Display(Name:="Date To Send", Description:=""),
    Required(ErrorMessage:="Date To Send required")>
  Public Property DateToSend As Date
      Get
        Return GetProperty(DateToSendProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(DateToSendProperty, Value)
      End Set
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", False)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:=""),
    Required(ErrorMessage:="Ignore required")>
  Public Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreProperty, Value)
      End Set
    End Property

    Public Shared SmsesRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsesRequired, "Smses Required")
    ''' <summary>
    ''' Gets and sets the Smses Required value
    ''' </summary>
    <Display(Name:="Smses Required", Description:=""),
    Required(ErrorMessage:="Smses Required required")>
  Public Property SmsesRequired() As Integer
      Get
        Return GetProperty(SmsesRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsesRequiredProperty, Value)
      End Set
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By Name")
    ''' <summary>
    ''' Gets and sets the Created By Name value
    ''' </summary>
    <Display(Name:="Created By Name", Description:="")>
  Public Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreatedByNameProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedDateString, "Created Date String")
    ''' <summary>
    ''' Gets and sets the Created Date String value
    ''' </summary>
    <Display(Name:="Created Date String", Description:="")>
  Public Property CreatedDateString() As String
      Get
        Return GetProperty(CreatedDateStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreatedDateStringProperty, Value)
      End Set
    End Property

    Public Shared BatchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BatchName, "Batch Name")
    ''' <summary>
    ''' Gets and sets the Batch Name value
    ''' </summary>
    <Display(Name:="Batch Name", Description:="")>
  Public Property BatchName() As String
      Get
        Return GetProperty(BatchNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BatchNameProperty, Value)
      End Set
    End Property

    Public Shared DateToSend1Property As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.DateToSend1, "Date To Send 1")
    ''' <summary>
    ''' Gets and sets the Date To Send 1 value
    ''' </summary>
    <Display(Name:="Date To Send 1", Description:=""),
    Required(ErrorMessage:="Date To Send 1 required")>
  Public Property DateToSend1 As Date
      Get
        Return GetProperty(DateToSend1Property)
      End Get
      Set(ByVal Value As Date)
        SetProperty(DateToSend1Property, Value)
      End Set
    End Property

    Public Shared DateToSendStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DateToSendString, "Date To Send String")
    ''' <summary>
    ''' Gets and sets the Date To Send String value
    ''' </summary>
    <Display(Name:="Date To Send String", Description:="")>
  Public Property DateToSendString() As String
      Get
        Return GetProperty(DateToSendStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DateToSendStringProperty, Value)
      End Set
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets and sets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:=""),
    Required(ErrorMessage:="Row No required")>
  Public Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RowNoProperty, Value)
      End Set
    End Property

    Public Shared RecipientCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RecipientCount, "Recipient Count")
    ''' <summary>
    ''' Gets and sets the Recipient Count value
    ''' </summary>
    <Display(Name:="Recipient Count", Description:=""),
    Required(ErrorMessage:="Recipient Count required")>
  Public Property RecipientCount() As Integer
      Get
        Return GetProperty(RecipientCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RecipientCountProperty, Value)
      End Set
    End Property

    Public Shared InEditModeProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InEditMode, "InEditMode", False)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="InEditMode", Description:="")>
    Public Property InEditMode() As Boolean
      Get
        Return GetProperty(InEditModeProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InEditModeProperty, Value)
      End Set
    End Property

    Public Shared SentCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SentCount, "Sent Count")
    ''' <summary>
    ''' Gets and sets the Recipient Count value
    ''' </summary>
    <Display(Name:="Sent Count", Description:="")>
    Public Property SentCount() As Integer
      Get
        Return GetProperty(SentCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SentCountProperty, Value)
      End Set
    End Property

    Public Shared SentPercentageProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.SentPercentage, 0.0)
    ''' <summary>
    ''' Gets and sets the Recipient Count value
    ''' </summary>
    <Display(Name:="Sent Percentage", Description:="")>
    Public Property SentPercentage() As Decimal
      Get
        Return GetProperty(SentPercentageProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(SentPercentageProperty, Value)
      End Set
    End Property

    Public Shared ProgressBarCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProgressBarCss, "")
    ''' <summary>
    ''' Gets and sets the Date To Send String value
    ''' </summary>
    <Display(Name:="ProgressBarCss", Description:="")>
    Public Property ProgressBarCss() As String
      Get
        Return GetProperty(ProgressBarCssProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProgressBarCssProperty, Value)
      End Set
    End Property

    Public Shared SentPercentageStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SentPercentageString, "")
    ''' <summary>
    ''' Gets and sets the Date To Send String value
    ''' </summary>
    <Display(Name:="SentPercentageString", Description:="")>
    Public Property SentPercentageString() As String
      Get
        Return GetProperty(SentPercentageStringProperty) 'CType(SentPercentage, Integer).ToString() & "%"
      End Get
      Set(ByVal Value As String)
        SetProperty(SentPercentageStringProperty, Value)
      End Set
    End Property

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SmsBatchID, Nothing)
    ''' <summary>
    ''' Gets and sets the Recipient Count value
    ''' </summary>
    <Display(Name:="SmsBatchID", Description:="")>
    Public Property SmsBatchID() As Integer?
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SmsBatchIDProperty, Value)
      End Set
    End Property

    Public Shared InTransitCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.InTransitCount, "In Transit Count")
    ''' <summary>
    ''' Gets and sets the Recipient Count value
    ''' </summary>
    <Display(Name:="In Transit Count", Description:="")>
    Public Property InTransitCount() As Integer
      Get
        Return GetProperty(InTransitCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(InTransitCountProperty, Value)
      End Set
    End Property

    Public Shared FirstSentTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FirstSentTime, "First Sent Time")
    ''' <summary>
    ''' Gets and sets the Date To Send value
    ''' </summary>
    <Display(Name:="First Sent Time", Description:="")>
    Public Property FirstSentTime As DateTime?
      Get
        Return GetProperty(FirstSentTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FirstSentTimeProperty, Value)
      End Set
    End Property

    Public Shared FirstSentTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstSentTimeString, "Sent Time")
    ''' <summary>
    ''' Gets and sets the Created Date String value
    ''' </summary>
    <Display(Name:="Sent Time", Description:="")>
    Public Property FirstSentTimeString() As String
      Get
        Return GetProperty(FirstSentTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FirstSentTimeStringProperty, Value)
      End Set
    End Property


#End Region

#Region " Child Lists "

    Public Shared SmsSearchRecipientListProperty As PropertyInfo(Of SmsSearchRecipientList) = RegisterProperty(Of SmsSearchRecipientList)(Function(c) c.SmsSearchRecipientList, "Sms Search Recipient List")

    Public ReadOnly Property SmsSearchRecipientList() As SmsSearchRecipientList
      Get
        If GetProperty(SmsSearchRecipientListProperty) Is Nothing Then
          LoadProperty(SmsSearchRecipientListProperty, Notifications.Sms.SmsSearchRecipientList.NewSmsSearchRecipientList())
        End If
        Return GetProperty(SmsSearchRecipientListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ShortMessage.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sms Search")
        Else
          Return String.Format("Blank {0}", "Sms Search")
        End If
      Else
        Return Me.ShortMessage
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSmsSearch() method.

    End Sub

    Public Shared Function NewSmsSearch() As SmsSearch

      Return DataPortal.CreateChild(Of SmsSearch)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSmsSearch(dr As SafeDataReader) As SmsSearch

      Dim s As New SmsSearch()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsIDProperty, .GetInt32(0))
          LoadProperty(ShortMessageProperty, .GetString(1))
          LoadProperty(FullMessageProperty, .GetString(2))
          LoadProperty(CreatedDateProperty, .GetDateTime(3))
          LoadProperty(DateToSendProperty, .GetValue(4))
          LoadProperty(IgnoreProperty, .GetBoolean(5))
          LoadProperty(SmsesRequiredProperty, .GetInt32(6))
          LoadProperty(CreatedByNameProperty, .GetString(7))
          LoadProperty(CreatedDateStringProperty, .GetString(8))
          LoadProperty(BatchNameProperty, .GetString(9))
          LoadProperty(DateToSend1Property, .GetValue(10))
          LoadProperty(DateToSendStringProperty, .GetString(11))
          LoadProperty(RowNoProperty, .GetInt32(12))
          LoadProperty(RecipientCountProperty, .GetInt32(13))
          LoadProperty(SentCountProperty, .GetInt32(14))
          LoadProperty(SentPercentageProperty, .GetDecimal(15))
          LoadProperty(ProgressBarCssProperty, .GetString(16))
          LoadProperty(SentPercentageStringProperty, .GetString(17))
          LoadProperty(SmsBatchIDProperty, ZeroNothing(.GetInt32(18)))
          LoadProperty(InTransitCountProperty, .GetInt32(19))
          LoadProperty(FirstSentTimeProperty, .GetValue(20))
          LoadProperty(FirstSentTimeStringProperty, .GetString(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SmsIDProperty)

      'cm.Parameters.AddWithValue("@ShortMessage", GetProperty(ShortMessageProperty))
      cm.Parameters.AddWithValue("@Message", GetProperty(FullMessageProperty))
      cm.Parameters.AddWithValue("@DateToSend", DateToSend)
      cm.Parameters.AddWithValue("@Ignore", GetProperty(IgnoreProperty))
      cm.Parameters.AddWithValue("@SmsesRequired", GetProperty(SmsesRequiredProperty))
      cm.Parameters.AddWithValue("@SmsBatchID", NothingDBNull(GetProperty(SmsBatchIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      'cm.Parameters.AddWithValue("@CreatedByName", GetProperty(CreatedByNameProperty))
      'cm.Parameters.AddWithValue("@CreatedDateString", GetProperty(CreatedDateStringProperty))
      'cm.Parameters.AddWithValue("@BatchName", GetProperty(BatchNameProperty))
      'cm.Parameters.AddWithValue("@DateToSend1", DateToSend1)
      'cm.Parameters.AddWithValue("@DateToSendString", GetProperty(DateToSendStringProperty))
      'cm.Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))
      'cm.Parameters.AddWithValue("@RecipientCount", GetProperty(RecipientCountProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SmsIDProperty, cm.Parameters("@SmsID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SmsSearchRecipientListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SmsID", GetProperty(SmsIDProperty))
    End Sub

#End Region

  End Class

End Namespace