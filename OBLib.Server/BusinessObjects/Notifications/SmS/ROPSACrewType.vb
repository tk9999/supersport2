﻿' Generated 07 Sep 2016 12:10 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms.ReadOnly

  <Serializable()> _
  Public Class ROPSACrewType
    Inherits OBReadOnlyBase(Of ROPSACrewType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "CrewTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public ReadOnly Property CrewTypeID As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the Sms Batch value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CrewType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROPSACrewType(dr As SafeDataReader) As ROPSACrewType

      Dim r As New ROPSACrewType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CrewTypeIDProperty, .GetInt32(0))
        LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(1))
        LoadProperty(CrewTypeProperty, .GetString(2))
      End With

    End Sub

#End Region

  End Class

End Namespace