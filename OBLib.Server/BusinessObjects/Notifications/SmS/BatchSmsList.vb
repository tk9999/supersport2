﻿' Generated 11 Jun 2015 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.SmS

  <Serializable()> _
  Public Class BatchSmsList
    Inherits OBBusinessListBase(Of BatchSmsList, BatchSms)

#Region " Business Methods "

    Public Function GetItem(SmsID As Integer) As BatchSms

      For Each child As BatchSms In Me
        If child.SmsID = SmsID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property BatchID As Integer?

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewBatchSmsList() As BatchSmsList

      Return New BatchSmsList()

    End Function

    'Public Shared Function GetUnsentSmsList() As BatchSmsList
    '  Return CType(DataPortal.Fetch(Of BatchSmsList)(New Criteria() With {.UnsentInd = True}), BatchSmsList)
    'End Function

    Public Shared Sub BeginGetBatchSmsList(CallBack As EventHandler(Of DataPortalResult(Of BatchSmsList)))

      Dim dp As New DataPortal(Of BatchSmsList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetBatchSmsList() As BatchSmsList

      Return DataPortal.Fetch(Of BatchSmsList)(New Criteria())

    End Function

    'Public Shared Function GetBatchSmsList(SmsIDs As List(Of Integer)) As BatchSmsList

    '  Dim xml As String = OBMisc.IntegerListToXML(SmsIDs)

    '  Return DataPortal.Fetch(Of BatchSmsList)(New Criteria(xml))

    'End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(BatchSms.GetBatchSms(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getBatchSmsList"
            'cm.Parameters.AddWithValue("@NotSentInd", crit.UnsentInd)
            'cm.Parameters.AddWithValue("@SmsID", crit.SmsID)
            cm.Parameters.AddWithValue("@BatchID", NothingDBNull(crit.BatchID))
            'cm.Parameters.AddWithValue("@SmsIDsXml", Singular.Strings.MakeEmptyDBNull(crit.SmsIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As BatchSms In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As BatchSms In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace