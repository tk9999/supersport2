﻿' Generated 11 Jun 2015 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.SmS

  <Serializable()> _
  Public Class BatchSms
    Inherits OBBusinessBase(Of BatchSms)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SmsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property SmsID() As Integer
      Get
        Return GetProperty(SmsIDProperty)
      End Get
      'Set(ByVal Value As Integer)
      '  SetProperty(SmsIDProperty, Value)
      'End Set
    End Property

    Public Shared MessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Message, "Message")
    ''' <summary>
    ''' Gets and sets the Message value
    ''' </summary>
    <Display(Name:="Message", Description:="")>
    Public Property Message() As String
      Get
        Return GetProperty(MessageProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MessageProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDate, "Created Date", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime?
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared DateToSendProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DateToSend, "Date To Send")
    ''' <summary>
    ''' Gets and sets the Date To Send value
    ''' </summary>
    <Display(Name:="Date To Send", Description:=""),
    Required(ErrorMessage:="Date To Send required")>
    Public Property DateToSend As DateTime?
      Get
        Return GetProperty(DateToSendProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DateToSendProperty, Value)
      End Set
    End Property

    Public Shared IgnoreProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Ignore, "Ignore", False)
    ''' <summary>
    ''' Gets and sets the Ignore value
    ''' </summary>
    <Display(Name:="Ignore", Description:="")>
    Public Property Ignore() As Boolean
      Get
        Return GetProperty(IgnoreProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreProperty, Value)
      End Set
    End Property

    Public Shared SmsesRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsesRequired, "Smses Required")
    ''' <summary>
    ''' Gets and sets the Smses Required value
    ''' </summary>
    <Display(Name:="Smses Required", Description:="")>
    Public Property SmsesRequired() As Integer
      Get
        Return GetProperty(SmsesRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SmsesRequiredProperty, Value)
      End Set
    End Property

    Public Shared SmsRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SmsRecipientID, "Sms Recipient")
    ''' <summary>
    ''' Gets and sets the Sms Recipient value
    ''' </summary>
    <Display(Name:="Sms Recipient", Description:="", AutoGenerateField:=False),
    Browsable(True)>
    Public ReadOnly Property SmsRecipientID() As Integer
      Get
        Return GetProperty(SmsRecipientIDProperty)
      End Get
      'Set(ByVal Value As Integer)
      '  SetProperty(SmsRecipientIDProperty, Value)
      'End Set
    End Property

    Public Shared RecipientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RecipientName, "Recipient Name")
    ''' <summary>
    ''' Gets and sets the Recipient Name value
    ''' </summary>
    <Display(Name:="Recipient Name", Description:="")>
    Public Property RecipientName() As String
      Get
        Return GetProperty(RecipientNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RecipientNameProperty, Value)
      End Set
    End Property

    Public Shared CellNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellNo, "Cell No")
    ''' <summary>
    ''' Gets and sets the Cell No value
    ''' </summary>
    <Display(Name:="Cell No", Description:="")>
    Public Property CellNo() As String
      Get
        Return GetProperty(CellNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellNoProperty, Value)
      End Set
    End Property

    Public Shared SentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SentDate, "Sent")
    ''' <summary>
    ''' Gets and sets the Sent Date value
    ''' </summary>
    <Display(Name:="Sent", Description:="")>
    Public ReadOnly Property SentDate As DateTime?
      Get
        Return GetProperty(SentDateProperty)
      End Get
    End Property

    Public Shared NotSentErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotSentError, "Status")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property NotSentError() As String
      Get
        Return GetProperty(NotSentErrorProperty)
      End Get
    End Property

    Public Shared SmsBatchIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmsBatchID, "Sms Batch")
    ''' <summary>
    ''' Gets and sets the Sms Recipient value
    ''' </summary>
    <Display(Name:="Sms Batch", Description:="", AutoGenerateField:=False),
    Browsable(True)>
    Public ReadOnly Property SmsBatchID() As Integer?
      Get
        Return GetProperty(SmsBatchIDProperty)
      End Get
      'Set(ByVal Value As Integer)
      '  SetProperty(SmsRecipientIDProperty, Value)
      'End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SmsTemplateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SmsTemplateTypeID, "Sms Template", Nothing)
    ''' <summary>
    ''' Gets and sets the Sm S Batch value
    ''' </summary>
    <Display(Name:="Sms Template", Description:="")>
    Public Property SmsTemplateTypeID() As Integer?
      Get
        Return GetProperty(SmsTemplateTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SmsTemplateTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status Code")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Status Code", Description:="")>
    Public ReadOnly Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
    End Property

    Public Shared StatusDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusDescription, "Status")
    ''' <summary>
    ''' Gets and sets the Not Sent Error value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property StatusDescription() As String
      Get
        Return GetProperty(StatusDescriptionProperty)
      End Get
    End Property

    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property StatusGroup As String
      Get
        Select Case Me.StatusCode
          Case "001"
            Return "Error"
          Case "002"
            Return "Pending"
          Case "003"
            Return "In Transit"
          Case "004"
            Return "Delivered"
          Case "005"
            Return "Error"
          Case "006"
            Return "Cancelled"
          Case "007"
            Return "Error"
          Case "008"
            'Code 8 does not exist
            'Return "Routing error"
          Case "009"
            Return "Error"
          Case "010"
            Return "Error"
          Case "011"
            Return "Pending"
          Case "012"
            Return "Error"
          Case "013"
            Return "Cancelled"
          Case "014"
            Return "Error"
        End Select
        Return "Unknown"
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SmsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Message.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sm S Flat")
        Else
          Return String.Format("Blank {0}", "Sm S Flat")
        End If
      Else
        Return Me.Message
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewBatchSms() method.

    End Sub

    Public Shared Function NewBatchSms() As BatchSms

      Return DataPortal.CreateChild(Of BatchSms)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetBatchSms(dr As SafeDataReader) As BatchSms

      Dim s As New BatchSms()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SmsIDProperty, .GetInt32(0))
          LoadProperty(MessageProperty, .GetString(1))
          LoadProperty(CreatedDateProperty, .GetDateTime(2))
          LoadProperty(DateToSendProperty, .GetValue(3))
          LoadProperty(IgnoreProperty, .GetBoolean(4))
          LoadProperty(SmsesRequiredProperty, .GetInt32(5))
          LoadProperty(SmsRecipientIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(RecipientNameProperty, .GetString(7))
          LoadProperty(CellNoProperty, .GetString(8))
          LoadProperty(SentDateProperty, .GetValue(9))
          LoadProperty(NotSentErrorProperty, .GetString(10))
          LoadProperty(SmsBatchIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(CreatedByProperty, .GetInt32(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(14))
          LoadProperty(SmsTemplateTypeIDProperty, ZeroNothing(.GetInt32(15)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insBatchSms"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updBatchSms"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSmsID As SqlParameter = .Parameters.Add("@SmsID", SqlDbType.Int)
          paramSmsID.Value = GetProperty(SmsIDProperty)
          If Me.IsNew Then
            paramSmsID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Message", GetProperty(MessageProperty))
          .Parameters.AddWithValue("@DateToSend", (New SmartDate(GetProperty(DateToSendProperty))).DBValue)
          .Parameters.AddWithValue("@Ignore", GetProperty(IgnoreProperty))
          .Parameters.AddWithValue("@SmsesRequired", GetProperty(SmsesRequiredProperty))
          .Parameters.AddWithValue("@SmsRecipientID", GetProperty(SmsRecipientIDProperty))
          .Parameters.AddWithValue("@RecipientName", GetProperty(RecipientNameProperty))
          .Parameters.AddWithValue("@CellNo", GetProperty(CellNoProperty))
          .Parameters.AddWithValue("@SentDate", (New SmartDate(GetProperty(SentDateProperty))).DBValue)
          .Parameters.AddWithValue("@NotSentError", GetProperty(NotSentErrorProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SmsTemplateTypeID", NothingDBNull(GetProperty(SmsTemplateTypeIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SmsIDProperty, paramSmsID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delBatchSms"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SmsID", GetProperty(SmsIDProperty))
        cm.Parameters.AddWithValue("@SmsRecipientID", NothingDBNull(GetProperty(SmsRecipientIDProperty)))
        cm.Parameters.AddWithValue("@SmsBatchID", NothingDBNull(GetProperty(SmsBatchIDProperty)))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace