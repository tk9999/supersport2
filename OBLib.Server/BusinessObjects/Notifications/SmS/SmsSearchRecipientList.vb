﻿' Generated 28 Mar 2017 08:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Notifications.Sms

  <Serializable()> _
  Public Class SmsSearchRecipientList
    Inherits OBBusinessListBase(Of SmsSearchRecipientList, SmsSearchRecipient)

#Region " Business Methods "

    Public Function GetItem(SmsID As Integer) As SmsSearchRecipient

      For Each child As SmsSearchRecipient In Me
        If child.SmsID = SmsID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSmsSearchRecipientList() As SmsSearchRecipientList

      Return New SmsSearchRecipientList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace