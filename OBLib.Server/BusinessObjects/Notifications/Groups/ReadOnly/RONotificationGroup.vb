﻿' Generated 08 Jun 2015 18:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class RONotificationGroup
    Inherits SingularReadOnlyBase(Of RONotificationGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key>
    Public ReadOnly Property NotificationGroupID() As Integer
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroup, "Group Name", "")
    ''' <summary>
    ''' Gets the Notification Group value
    ''' </summary>
    <Display(Name:="Group Name", Description:="")>
    Public ReadOnly Property NotificationGroup() As String
      Get
        Return GetProperty(NotificationGroupProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HasAccessToGroupProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasAccessToGroup, "Has Access", False)
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Has Access")>
    Public ReadOnly Property HasAccessToGroup() As Boolean
      Get
        Return GetProperty(HasAccessToGroupProperty)
      End Get
    End Property

    Public Shared ContactCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContactCount, "Contacts", 0)
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Contacts")>
    Public ReadOnly Property ContactCount() As Integer
      Get
        Return GetProperty(ContactCountProperty)
      End Get
    End Property

    Public ReadOnly Property CurrentUserID() As Integer
      Get
        Return OBLib.Security.Settings.CurrentUserID
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NotificationGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.NotificationGroup

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRONotificationGroup(dr As SafeDataReader) As RONotificationGroup

      Dim r As New RONotificationGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(NotificationGroupIDProperty, .GetInt32(0))
        LoadProperty(NotificationGroupProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(HasAccessToGroupProperty, .GetBoolean(6))
        LoadProperty(ContactCountProperty, .GetInt32(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace