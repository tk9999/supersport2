﻿' Generated 08 Jun 2015 18:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class RONotificationGroupContactList
    Inherits SingularReadOnlyListBase(Of RONotificationGroupContactList, RONotificationGroupContact)

#Region " Parent "

    <NotUndoable()> Private mParent As RONotificationGroup
#End Region

#Region " Business Methods "

    Public Function GetItem(NotificationGroupContactID As Integer) As RONotificationGroupContact

      For Each child As RONotificationGroupContact In Me
        If child.NotificationGroupContactID = NotificationGroupContactID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Notification Group Contacts"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property NotificationGroupID As Integer? = Nothing

      Public Sub New(NotificationGroupID As Integer?)
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRONotificationGroupContactList() As RONotificationGroupContactList

      Return New RONotificationGroupContactList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRONotificationGroupContactList(NotificationGroupID As Integer?) As RONotificationGroupContactList

      Return DataPortal.Fetch(Of RONotificationGroupContactList)(New Criteria(NotificationGroupID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RONotificationGroupContact.GetRONotificationGroupContact(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRONotificationGroupContactList"
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace