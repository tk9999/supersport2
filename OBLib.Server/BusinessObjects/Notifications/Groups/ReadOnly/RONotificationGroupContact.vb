﻿' Generated 08 Jun 2015 18:43 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class RONotificationGroupContact
    Inherits SingularReadOnlyBase(Of RONotificationGroupContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NotificationGroupContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property NotificationGroupContactID() As Integer
      Get
        Return GetProperty(NotificationGroupContactIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "Notification Group", Nothing)
    ''' <summary>
    ''' Gets the Notification Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property NotificationGroupID() As Integer?
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets the Notification Group Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="''")>
    Public ReadOnly Property NotificationGroupContactName() As String
      Get
        Return GetProperty(NotificationGroupContactNameProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets the Notification Group Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:="''")>
    Public ReadOnly Property NotificationGroupContactNumber() As String
      Get
        Return GetProperty(NotificationGroupContactNumberProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactEmailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactEmail, "Contact Email", "")
    ''' <summary>
    ''' Gets the Notification Group Contact Email value
    ''' </summary>
    <Display(Name:="Contact Email", Description:="''")>
    Public ReadOnly Property NotificationGroupContactEmail() As String
      Get
        Return GetProperty(NotificationGroupContactEmailProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NotificationGroupContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.NotificationGroupContactName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRONotificationGroupContact(dr As SafeDataReader) As RONotificationGroupContact

      Dim r As New RONotificationGroupContact()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(NotificationGroupContactIDProperty, .GetInt32(0))
        LoadProperty(NotificationGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(NotificationGroupContactNameProperty, .GetString(2))
        LoadProperty(NotificationGroupContactNumberProperty, .GetString(3))
        LoadProperty(NotificationGroupContactEmailProperty, .GetString(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace