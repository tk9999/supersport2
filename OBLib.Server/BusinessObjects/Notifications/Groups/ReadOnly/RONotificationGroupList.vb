﻿' Generated 08 Jun 2015 18:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications.ReadOnly

  <Serializable()> _
  Public Class RONotificationGroupList
    Inherits SingularReadOnlyListBase(Of RONotificationGroupList, RONotificationGroup)

#Region " Business Methods "

    Public Function GetItem(NotificationGroupID As Integer) As RONotificationGroup

      For Each child As RONotificationGroup In Me
        If child.NotificationGroupID = NotificationGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Notification Groups"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property FilterForUserID As Integer? = Nothing
      Public Property CheckForUserID As Integer? = Nothing
      Public Property NotificationGroupID As Integer? = Nothing

      Public Sub New(FilterForUserID As Integer?, CheckForUserID As Integer?, NotificationGroupID As Integer?)
        Me.FilterForUserID = FilterForUserID
        Me.CheckForUserID = CheckForUserID
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRONotificationGroupList() As RONotificationGroupList

      Return New RONotificationGroupList()

    End Function

    Public Shared Sub BeginGetRONotificationGroupList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RONotificationGroupList)))

      Dim dp As New DataPortal(Of RONotificationGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRONotificationGroupList(CallBack As EventHandler(Of DataPortalResult(Of RONotificationGroupList)))

      Dim dp As New DataPortal(Of RONotificationGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRONotificationGroupList() As RONotificationGroupList

      Return DataPortal.Fetch(Of RONotificationGroupList)(New Criteria())

    End Function

    Public Shared Function GetRONotificationGroupList(FilterForUserID As Integer?, CheckForUserID As Integer?, NotificationGroupID As Integer?) As RONotificationGroupList

      Return DataPortal.Fetch(Of RONotificationGroupList)(New Criteria(FilterForUserID, CheckForUserID, NotificationGroupID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RONotificationGroup.GetRONotificationGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRONotificationGroupList"
            cm.Parameters.AddWithValue("@FilterForUserID", NothingDBNull(crit.FilterForUserID))
            cm.Parameters.AddWithValue("@CheckForUserID", NothingDBNull(crit.CheckForUserID))
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace