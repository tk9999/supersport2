﻿' Generated 08 Jun 2015 18:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications

  <Serializable()> _
  Public Class NotificationGroupContactList
    Inherits SingularBusinessListBase(Of NotificationGroupContactList, NotificationGroupContact)

#Region " Business Methods "

    Public Function GetItem(NotificationGroupContactID As Integer) As NotificationGroupContact

      For Each child As NotificationGroupContact In Me
        If child.NotificationGroupContactID = NotificationGroupContactID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Notification Group Contacts"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property NotificationGroupID As Integer? = Nothing
      Public Property NotificationGroupContactIDs As String = ""

      Public Sub New(NotificationGroupID As Integer?)
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New(NotificationGroupContactIDs As String)
        Me.NotificationGroupID = Nothing
        Me.NotificationGroupContactIDs = NotificationGroupContactIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewNotificationGroupContactList() As NotificationGroupContactList

      Return New NotificationGroupContactList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetNotificationGroupList() As NotificationGroupContactList

      Return DataPortal.Fetch(Of NotificationGroupContactList)(New Criteria())

    End Function

    Public Shared Function GetNotificationGroupContactList(NotificationGroupID As Integer?) As NotificationGroupContactList

      Return DataPortal.Fetch(Of NotificationGroupContactList)(New Criteria(NotificationGroupID))

    End Function

    Public Shared Function GetNotificationGroupContactList(NotificationGroupContactIDs As String) As NotificationGroupContactList

      Return DataPortal.Fetch(Of NotificationGroupContactList)(New Criteria(NotificationGroupContactIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(NotificationGroupContact.GetNotificationGroupContact(sdr))
      End While
      Me.RaiseListChangedEvents = True

      For Each child As NotificationGroupContact In Me
        child.CheckRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getNotificationGroupContactList"
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            cm.Parameters.AddWithValue("@NotificationGroupContactIDs", Strings.MakeEmptyDBNull(crit.NotificationGroupContactIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As NotificationGroupContact In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As NotificationGroupContact In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace