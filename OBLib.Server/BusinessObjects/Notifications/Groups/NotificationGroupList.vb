﻿' Generated 08 Jun 2015 18:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications

  <Serializable()> _
  Public Class NotificationGroupList
    Inherits OBBusinessListBase(Of NotificationGroupList, NotificationGroup)

#Region " Business Methods "

    Public Function GetItem(NotificationGroupID As Integer) As NotificationGroup

      For Each child As NotificationGroup In Me
        If child.NotificationGroupID = NotificationGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Notification Groups"

    End Function

    Public Function GetNotificationGroupContact(NotificationGroupContactID As Integer) As NotificationGroupContact

      Dim obj As NotificationGroupContact = Nothing
      For Each parent As NotificationGroup In Me
        obj = parent.NotificationGroupContactList.GetItem(NotificationGroupContactID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "UserID", Nothing)
      ''' <summary>
      ''' Gets and sets the User value
      ''' </summary>
      <Display(Name:="UserID", Description:="")>
      Public Property UserID() As Integer?
        Get
          Return ReadProperty(UserIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(UserIDProperty, Value)
        End Set
      End Property

      Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "NotificationGroupID", Nothing)
      ''' <summary>
      ''' Gets and sets the User value
      ''' </summary>
      <Display(Name:="NotificationGroupID", Description:="")>
      Public Property NotificationGroupID() As Integer?
        Get
          Return ReadProperty(NotificationGroupIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(NotificationGroupIDProperty, Value)
        End Set
      End Property

      Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.GroupName, "") _
                                                                   .AddSetExpression("NotificationGroupListBO.GroupNameCrtieriaSet(self)", False, 350)
      ''' <summary>
      ''' Gets and sets the User value
      ''' </summary>
      <Display(Name:="Group Name", Description:=""),
      TextField(False, False, False, 1)>
      Public Property GroupName() As String
        Get
          Return ReadProperty(GroupNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GroupNameProperty, Value)
        End Set
      End Property

      Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ContactName, "") _
                                                                   .AddSetExpression("NotificationGroupListBO.ContactNameCrtieriaSet(self)", False, 350)
      ''' <summary>
      ''' Gets and sets the User value
      ''' </summary>
      <Display(Name:="Contact Name", Description:=""),
      TextField(False, False, False, 1)>
      Public Property ContactName() As String
        Get
          Return ReadProperty(ContactNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ContactNameProperty, Value)
        End Set
      End Property

      Public Sub New(UserID As Integer?, GroupName As String, ContactName As String)
        Me.UserID = UserID
        Me.GroupName = GroupName
        Me.ContactName = ContactName
      End Sub

      Public Sub New(UserID As Integer?, NotificationGroupID As Integer?)
        Me.UserID = UserID
        Me.NotificationGroupID = NotificationGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewNotificationGroupList() As NotificationGroupList

      Return New NotificationGroupList()

    End Function

    Public Shared Sub BeginGetNotificationGroupList(CallBack As EventHandler(Of DataPortalResult(Of NotificationGroupList)))

      Dim dp As New DataPortal(Of NotificationGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetNotificationGroupList() As NotificationGroupList

      Return DataPortal.Fetch(Of NotificationGroupList)(New Criteria())

    End Function

    Public Shared Function GetNotificationGroupList(UserID As Integer?, NotificationGroupID As Integer?) As NotificationGroupList

      Return DataPortal.Fetch(Of NotificationGroupList)(New Criteria(UserID, NotificationGroupID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(NotificationGroup.GetNotificationGroup(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As NotificationGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.NotificationGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.NotificationGroupContactList.RaiseListChangedEvents = False
          parent.NotificationGroupContactList.Add(NotificationGroupContact.GetNotificationGroupContact(sdr))
          parent.NotificationGroupContactList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As NotificationGroup In Me
        child.CheckRules()
        For Each NotificationGroupContact As NotificationGroupContact In child.NotificationGroupContactList
          NotificationGroupContact.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getNotificationGroupList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            cm.Parameters.AddWithValue("@NotificationGroupID", NothingDBNull(crit.NotificationGroupID))
            cm.Parameters.AddWithValue("@GroupName", Strings.MakeEmptyDBNull(crit.GroupName))
            cm.Parameters.AddWithValue("@ContactName", Strings.MakeEmptyDBNull(crit.ContactName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As NotificationGroup In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As NotificationGroup In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace