﻿' Generated 08 Jun 2015 18:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Text.RegularExpressions

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications

  <Serializable()> _
  Public Class NotificationGroupContact
    Inherits SingularBusinessBase(Of NotificationGroupContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Selected, False) _
                                                                 .AddSetExpression("NotificationGroupContactBO.SelectedSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Select")>
    Public Property Selected() As Boolean
      Get
        Return GetProperty(SelectedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property NotificationGroupContactID() As Integer
      Get
        Return GetProperty(NotificationGroupContactIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotificationGroupID, "Notification Group", Nothing)
    ''' <summary>
    ''' Gets the Notification Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property NotificationGroupID() As Integer?
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactName, "Name", "")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Name value
    ''' </summary>
    <Display(Name:="Name", Description:="''"),
    StringLength(100, ErrorMessage:="Name cannot be more than 100 characters")>
    Public Property NotificationGroupContactName() As String
      Get
        Return GetProperty(NotificationGroupContactNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupContactNameProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Contact Name required"),

    Public Shared NotificationGroupContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactNumber, "Number", "")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Number value
    ''' </summary>
    <Display(Name:="Number"),
    StringLength(15, ErrorMessage:="Number cannot be more than 15 characters")>
    Public Property NotificationGroupContactNumber() As String
      Get
        Return GetProperty(NotificationGroupContactNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupContactNumberProperty, Value)
      End Set
    End Property
    '    Required(ErrorMessage:="Cell Number is required"),

    Public Shared NotificationGroupContactEmailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupContactEmail, "Email", "")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Email value
    ''' </summary>
    <Display(Name:="Email"),
    StringLength(50, ErrorMessage:="Email cannot be more than 50 characters")>
    Public Property NotificationGroupContactEmail() As String
      Get
        Return GetProperty(NotificationGroupContactEmailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupContactEmailProperty, Value)
      End Set
    End Property
    '    Required(ErrorMessage:="Email address is required"),

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property


    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Name", "")
    ''' <summary>
    ''' Gets and sets the Notification Group Contact Name value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="''")>
    Public Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, " HumanResource", Nothing)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property


    Public Shared ForSmsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ForSms, "Use to Sms?", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Use to Sms?", Description:="")>
    Public Property ForSms() As Boolean
      Get
        Return GetProperty(ForSmsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ForSmsProperty, Value)
      End Set
    End Property


    Public Shared ForEmailProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ForEmail, "Use to Email?", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Use to Email?", Description:="")>
    Public Property ForEmail() As Boolean
      Get
        Return GetProperty(ForEmailProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ForEmailProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As NotificationGroup

      Return CType(CType(Me.Parent, NotificationGroupContactList).Parent, NotificationGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NotificationGroupContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.NotificationGroupContactName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Notification Group Contact")
        Else
          Return String.Format("Blank {0}", "Notification Group Contact")
        End If
      Else
        Return Me.NotificationGroupContactName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(NotificationGroupContactEmailProperty)
        .JavascriptRuleFunctionName = "NotificationGroupContactBO.EmailAddressValid"
        .ServerRuleFunction = AddressOf CheckEmailValid
      End With

      With AddWebRule(NotificationGroupContactNumberProperty)
        .JavascriptRuleFunctionName = "NotificationGroupContactBO.ContactNumberValid"
        .ServerRuleFunction = AddressOf CheckContactNumberValid
      End With

    End Sub

    Public Shared Function CheckEmailValid(NotificationGroupContact As NotificationGroupContact) As String

      Dim ErrorDescription As String = "Please enter a valid email address"
      If NotificationGroupContact.ForEmail Then
        If Not IsNullNothing(NotificationGroupContact.NotificationGroupContactEmail) Then
          If Not Singular.Emails.ValidEmailAddress(NotificationGroupContact.NotificationGroupContactEmail) Then
            Return ErrorDescription
          Else
            Return ""
          End If
        End If
      End If
      Return ErrorDescription

    End Function

    Public Shared Function CheckContactNumberValid(NotificationGroupContact As NotificationGroupContact) As String

      Dim ErrorDescription As String = "Please enter a valid notification group contact number"
      If Not IsNullNothing(NotificationGroupContact.NotificationGroupContactNumber) Then
        If Not IsPhoneNumberValid(NotificationGroupContact.NotificationGroupContactNumber.Trim) Then
          Return ErrorDescription
        Else
          Return ""
        End If
      End If

    End Function

    Private Shared Function IsPhoneNumberValid(phoneNumber As String) As Boolean
      Dim result As String = ""
      Dim chars As Char() = phoneNumber.ToCharArray()
      For count = 0 To chars.GetLength(0) - 1
        Dim tempChar As Char = chars(count)
        If [Char].IsDigit(tempChar) Or "()+-., ".Contains(tempChar.ToString()) Then

          result += StripNonAlphaNumeric(tempChar)
        Else
          Return False
        End If

      Next

      Return result.Length = 10
    End Function

    Private Shared Function StripNonAlphaNumeric(value As String) As String
      Dim regex = New Regex("[^0-9a-zA-Z]", RegexOptions.None)
      Dim result As String = ""
      If regex.IsMatch(value) Then
        result = regex.Replace(value, "")
      Else
        result = value
      End If

      Return result
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNotificationGroupContact() method.

    End Sub

    Public Shared Function NewNotificationGroupContact() As NotificationGroupContact

      Return DataPortal.CreateChild(Of NotificationGroupContact)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNotificationGroupContact(dr As SafeDataReader) As NotificationGroupContact

      Dim n As New NotificationGroupContact()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NotificationGroupContactIDProperty, .GetInt32(0))
          LoadProperty(NotificationGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(NotificationGroupContactNameProperty, .GetString(2))
          LoadProperty(NotificationGroupContactNumberProperty, .GetString(3))
          LoadProperty(NotificationGroupContactEmailProperty, .GetString(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(HumanResourceIDProperty, .GetInt32(9))
          LoadProperty(HumanResourceNameProperty, .GetString(10))
          LoadProperty(ForSmsProperty, .GetBoolean(11))
          LoadProperty(ForEmailProperty, .GetBoolean(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNotificationGroupContact"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNotificationGroupContact"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNotificationGroupContactID As SqlParameter = .Parameters.Add("@NotificationGroupContactID", SqlDbType.Int)
          paramNotificationGroupContactID.Value = GetProperty(NotificationGroupContactIDProperty)
          If Me.IsNew Then
            paramNotificationGroupContactID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@NotificationGroupID", Me.GetParent().NotificationGroupID)
          .Parameters.AddWithValue("@NotificationGroupContactName", GetProperty(NotificationGroupContactNameProperty))
          .Parameters.AddWithValue("@NotificationGroupContactNumber", GetProperty(NotificationGroupContactNumberProperty))
          .Parameters.AddWithValue("@NotificationGroupContactEmail", GetProperty(NotificationGroupContactEmailProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(NotificationGroupContactIDProperty, paramNotificationGroupContactID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delNotificationGroupContact"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NotificationGroupContactID", GetProperty(NotificationGroupContactIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace