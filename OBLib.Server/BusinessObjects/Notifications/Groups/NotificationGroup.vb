﻿' Generated 08 Jun 2015 18:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Notifications

  <Serializable()> _
  Public Class NotificationGroup
    Inherits OBBusinessBase(Of NotificationGroup)

#Region " Properties and Methods "

    'Private mOriginalIsLinked As Boolean

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Is Expanded", False)
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Is Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared NotificationGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotificationGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property NotificationGroupID() As Integer
      Get
        Return GetProperty(NotificationGroupIDProperty)
      End Get
    End Property

    Public Shared NotificationGroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotificationGroupName, "Group Name", "")
    ''' <summary>
    ''' Gets and sets the Notification Group value
    ''' </summary>
    <Display(Name:="Group Name", Description:=""),
    StringLength(100, ErrorMessage:="Notification Group cannot be more than 100 characters"),
    Required(AllowEmptyStrings:=False)>
    Public Property NotificationGroupName() As String
      Get
        Return GetProperty(NotificationGroupNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotificationGroupNameProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared GroupDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupDisciplineID, "Auto-add contacts from which discipline?", Nothing)
    ''' <summary>
    ''' Gets the Group DisciplineID By value
    ''' </summary>
    ''' 
    <Display(Name:="Auto-add contacts from which discipline?"), DropDownWeb(GetType(RODisciplineList), DisplayMember:="Discipline", ValueMember:="DisciplineID")>
    Public Property GroupDisciplineID() As Integer?
      Get
        Return GetProperty(GroupDisciplineIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(GroupDisciplineIDProperty, value)
      End Set
    End Property

    Public Shared AutoPopulateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AutoPopulate, "Auto Populate Contacts?", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Auto Populate Contacts?", Description:="")>
    Public Property AutoPopulate() As Boolean
      Get
        Return GetProperty(AutoPopulateProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AutoPopulateProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(RODisciplineList))>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property


#End Region

#Region " Child Lists "

    Public Shared NotificationGroupContactListProperty As PropertyInfo(Of NotificationGroupContactList) = RegisterProperty(Of NotificationGroupContactList)(Function(c) c.NotificationGroupContactList, "Notification Group Contact List")
    Public ReadOnly Property NotificationGroupContactList() As NotificationGroupContactList
      Get
        If GetProperty(NotificationGroupContactListProperty) Is Nothing Then
          LoadProperty(NotificationGroupContactListProperty, Notifications.NotificationGroupContactList.NewNotificationGroupContactList())
        End If
        Return GetProperty(NotificationGroupContactListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NotificationGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.NotificationGroupName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Notification Group")
        Else
          Return String.Format("Blank {0}", "Notification Group")
        End If
      Else
        Return Me.NotificationGroupName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"NotificationGroupUsers", "NotificationGroupContacts"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(DisciplineIDProperty)
        .JavascriptRuleFunctionName = "NotificationGroupBO.DisciplineIDValid"
        .ServerRuleFunction = AddressOf DisciplineIDValid
        .AddTriggerProperty(AutoPopulateProperty)
        .AffectedProperties.Add(AutoPopulateProperty)
      End With

      With AddWebRule(SystemIDProperty)
        .JavascriptRuleFunctionName = "NotificationGroupBO.SystemIDValid"
        .ServerRuleFunction = AddressOf SystemIDValid
        .AddTriggerProperty(AutoPopulateProperty)
        .AffectedProperties.Add(AutoPopulateProperty)
      End With

      With AddWebRule(ProductionAreaIDProperty)
        .JavascriptRuleFunctionName = "NotificationGroupBO.ProductionAreaIDValid"
        .ServerRuleFunction = AddressOf ProductionAreaIDValid
        .AddTriggerProperty(AutoPopulateProperty)
        .AffectedProperties.Add(AutoPopulateProperty)
      End With

    End Sub

    Public Shared Function DisciplineIDValid(NotificationGroup As NotificationGroup) As String

      Dim ErrorDescription As String = ""
      If NotificationGroup.AutoPopulate AndAlso IsNullNothing(NotificationGroup.DisciplineID) Then
        ErrorDescription = "Discipline is required"
      End If
      Return ErrorDescription

    End Function

    Public Shared Function SystemIDValid(NotificationGroup As NotificationGroup) As String

      Dim ErrorDescription As String = ""
      If NotificationGroup.AutoPopulate AndAlso IsNullNothing(NotificationGroup.SystemID) Then
        ErrorDescription = "Sub-Dept is required"
      End If
      Return ErrorDescription

    End Function

    Public Shared Function ProductionAreaIDValid(NotificationGroup As NotificationGroup) As String

      Dim ErrorDescription As String = ""
      If NotificationGroup.AutoPopulate AndAlso IsNullNothing(NotificationGroup.ProductionAreaID) Then
        ErrorDescription = "Area is required"
      End If
      Return ErrorDescription

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNotificationGroup() method.

    End Sub

    Public Shared Function NewNotificationGroup() As NotificationGroup

      Return DataPortal.CreateChild(Of NotificationGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNotificationGroup(dr As SafeDataReader) As NotificationGroup

      Dim n As New NotificationGroup()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NotificationGroupIDProperty, .GetInt32(0))
          LoadProperty(NotificationGroupNameProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetValue(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(5))
          LoadProperty(AutoPopulateProperty, .GetBoolean(6))
          LoadProperty(DisciplineIDProperty, ZeroNothing(.GetInt32(7)))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(8)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(9)))
          'LoadProperty(IsLinkedProperty, .GetBoolean(6))
          'LoadProperty(OriginalIsLinkedProperty, .GetBoolean(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNotificationGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNotificationGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          Dim paramNotificationGroupID As SqlParameter = .Parameters.Add("@NotificationGroupID", SqlDbType.Int)
          paramNotificationGroupID.Value = GetProperty(NotificationGroupIDProperty)
          If Me.IsNew Then
            paramNotificationGroupID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@NotificationGroup", GetProperty(NotificationGroupNameProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          '.Parameters.AddWithValue("@IsLinked", GetProperty(IsLinkedProperty))
          '.Parameters.AddWithValue("@OriginalIsLinked", GetProperty(OriginalIsLinkedProperty))
          .Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
          '.Parameters.AddWithValue("@NewGroupDisciplineID", NothingDBNull(GetProperty(GroupDisciplineIDProperty))) 'NewGroupDisciplineIDProperty
          .Parameters.AddWithValue("@AutoPopulate", GetProperty(AutoPopulateProperty))
          .Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(NotificationGroupIDProperty, paramNotificationGroupID.Value)
          End If
          ' update child objects
          If GetProperty(NotificationGroupContactListProperty) IsNot Nothing Then
            Me.NotificationGroupContactList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(NotificationGroupContactListProperty) IsNot Nothing Then
          Me.NotificationGroupContactList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delNotificationGroup"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NotificationGroupID", GetProperty(NotificationGroupIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace