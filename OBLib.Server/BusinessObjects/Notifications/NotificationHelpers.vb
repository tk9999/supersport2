﻿Imports OBLib.Notifications
Imports Singular.SmsSending
Imports Singular.Emails
Imports OBLib.Notifications.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular
Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports Singular.Misc
Imports OBLib.Notifications.SmS.ReadOnly

Namespace Notifications

  'Public Class NotificationCriteria

  Public Class SmsGeneratorCriteria
    Inherits SingularCriteriaBase(Of SmsGeneratorCriteria)

    Public Shared SmSTemplateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SmSTemplateTypeID, Nothing)
    <Display(Name:="Template"),
    Required(ErrorMEssage:="Template is required"),
    DropDownWeb(GetType(ROSmsTemplateTypeList))>
    Public Overridable Property SmSTemplateTypeID As Integer?
      Get
        Return ReadProperty(SmSTemplateTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(SmSTemplateTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    <Display(Name:="Start Date"),
    Required(ErrorMEssage:="Start Date is required")>
    Public Overridable Property StartDate As DateTime?
      Get
        Return ReadProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        LoadProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    <Display(Name:="End Date"),
    Required(ErrorMEssage:="Start Date is required")>
    Public Overridable Property EndDate As DateTime?
      Get
        Return ReadProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        LoadProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
    <Display(Name:="Sub-Dept"),
    Required(ErrorMEssage:="Dept is required"),
    DropDownWeb(GetType(ROSystemList))>
    Public Overridable Property SystemID As Integer?
      Get
        Return ReadProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    <Display(Name:="Area"),
    Required(ErrorMEssage:="Area is required"),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Overridable Property ProductionAreaID As Integer?
      Get
        Return ReadProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing)
    <Display(Name:="Sub-Dept")>
    Public Overridable Property HumanResourceID As Integer?
      Get
        Return ReadProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.RoomID, Nothing)
    <Display(Name:="Room")>
    Public Overridable Property RoomID As Integer?
      Get
        Return ReadProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared CurrentUserIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CurrentUserID, Nothing)
    <Display(Name:="User")>
    Public Overridable Property CurrentUserID As Integer?
      Get
        Return ReadProperty(CurrentUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        LoadProperty(CurrentUserIDProperty, Value)
      End Set
    End Property

  End Class

  'End Class

  Public Class NotificationHelpers

    Public Shared Function GenerateSmsesProductionServicesStudio(nc As SmsGeneratorCriteria) As Integer?

      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdGenerateSMSProductionServicesStudio]",
                                          New String() {"@StartDate", "@EndDate", "@SystemID", "@ProductionAreaID", "@HumanResourceID",
                                                        "@RoomID", "@CurrentUserID"},
                                          New Object() {
                                                        nc.StartDate, nc.EndDate, nc.SystemID, nc.ProductionAreaID, NothingDBNull(nc.HumanResourceID),
                                                        NothingDBNull(nc.RoomID), nc.CurrentUserID
                                            })
      cmd.FetchType = CommandProc.FetchTypes.DataObject
      cmd = cmd.Execute
      Dim GeneratedBatchID As Integer? = cmd.DataObject()
      Return GeneratedBatchID

    End Function

  End Class

End Namespace
