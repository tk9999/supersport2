﻿' Generated 18 Feb 2015 14:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace VehiclesAndEquipment

  <Serializable()> _
  Public Class VehicleEquipmentList
    Inherits OBBusinessListBase(Of VehicleEquipmentList, VehicleEquipment)


#Region " Business Methods "

    Public Function GetItem(VehicleEquipmentID As Integer) As VehicleEquipment

      For Each child As VehicleEquipment In Me
        If child.VehicleEquipmentID = VehicleEquipmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Sub AddVehicleEquipment(EquipmentIDList As List(Of Integer))


      For Each EquipmentID In EquipmentIDList
        Me.Add(VehicleEquipment.NewVehicleEquipment(EquipmentID))

      Next
      Update()

    End Sub

    Public Sub DeleteVehicleEquipment(ByVal VehicleEquipmentIDList As List(Of Integer))

      For Each VehicleEquipmentID In VehicleEquipmentIDList
        Me.DBDeleteChild(VehicleEquipmentID)
      Next

    End Sub

    Public Overrides Function ToString() As String

      Return "Vehicle Equipments"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewVehicleEquipmentList() As VehicleEquipmentList

      Return New VehicleEquipmentList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As VehicleEquipment In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As VehicleEquipment In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Friend Sub DBDeleteChild(VehicleEquipmentID As Integer)
      Dim cmdProc As Singular.CommandProc = New Singular.CommandProc
      Using cmdProc
        Try
          cmdProc.CommandText = "DelProcsWeb.delVehicleEquipment"
          cmdProc.CommandType = CommandType.StoredProcedure
          cmdProc.Parameters.AddWithValue("@VehicleEquipmentID", VehicleEquipmentID)

          cmdProc = cmdProc.Execute


        Catch ex As Exception

          Throw ex

        End Try

      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace