﻿' Generated 18 Feb 2015 14:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace VehiclesAndEquipment

  <Serializable()> _
  Public Class VehicleService
    Inherits OBBusinessBase(Of VehicleService)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleServiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleServiceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property VehicleServiceID() As Integer
      Get
        Return GetProperty(VehicleServiceIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="The date the vehicle went in for it's service"),
    Required(ErrorMessage:="Start Date Time required")>
  Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="The date the vehicle's service was completed"),
    Required(ErrorMessage:="End Date Time required")>
  Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ServiceDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ServiceDescription, "Service Description", "")
    ''' <summary>
    ''' Gets and sets the Service Description value
    ''' </summary>
    <Display(Name:="Service Description", Description:=""),
    StringLength(50, ErrorMessage:="Service Description cannot be more than 50 characters"),
    Required(ErrorMessage:="Description required")>
    Public Property ServiceDescription() As String
      Get
        Return GetProperty(ServiceDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ServiceDescriptionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Vehicle

      Return CType(CType(Me.Parent, VehicleServiceList).Parent, Vehicle)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleServiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ServiceDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Vehicle Service")
        Else
          Return String.Format("Blank {0}", "Vehicle Service")
        End If
      Else
        Return Me.ServiceDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      ''  Me.AddMultiplePropertyRule(AddressOf Singular.CSLALib.Rules.CompareProperties, New Singular.CSLALib.Rules.Args.ComparePropertyArgs("StartDateTime", "EndDateTime", "<="), New String() {"StartDateTime", "EndDateTime"})

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVehicleService() method.

    End Sub

    Public Shared Function NewVehicleService() As VehicleService

      Return DataPortal.CreateChild(Of VehicleService)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVehicleService(dr As SafeDataReader) As VehicleService

      Dim v As New VehicleService()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VehicleServiceIDProperty, .GetInt32(0))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(StartDateTimeProperty, .GetValue(2))
          LoadProperty(EndDateTimeProperty, .GetValue(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ServiceDescriptionProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insVehicleService"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updVehicleService"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVehicleServiceID As SqlParameter = .Parameters.Add("@VehicleServiceID", SqlDbType.Int)
          paramVehicleServiceID.Value = GetProperty(VehicleServiceIDProperty)
          If Me.IsNew Then
            paramVehicleServiceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VehicleID", Me.GetParent().VehicleID)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ServiceDescription", GetProperty(ServiceDescriptionProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VehicleServiceIDProperty, paramVehicleServiceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delVehicleService"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VehicleServiceID", GetProperty(VehicleServiceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace