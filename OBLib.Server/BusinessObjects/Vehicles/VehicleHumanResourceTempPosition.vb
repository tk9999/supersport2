﻿' Generated 18 Feb 2015 14:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace VehiclesAndEquipment

  <Serializable()> _
  Public Class VehicleHumanResourceTempPosition
    Inherits OBBusinessBase(Of VehicleHumanResourceTempPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleHumanResourceTempPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleHumanResourceTempPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property VehicleHumanResourceTempPositionID() As Integer
      Get
        Return GetProperty(VehicleHumanResourceTempPositionIDProperty)
      End Get
    End Property

    Public Shared VehicleHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleHumanResourceID, "Vehicle Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Vehicle Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property VehicleHumanResourceID() As Integer?
      Get
        Return GetProperty(VehicleHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Temp Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Manager value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
  Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required")>
  Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As VehicleHumanResource

      If Me.Parent IsNot Nothing Then
        Return CType(CType(Me.Parent, VehicleHumanResourceTempPositionList).Parent, VehicleHumanResource)
      Else
        Return Nothing
      End If


    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleHumanResourceTempPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Vehicle Human Resource Temp Position")
        Else
          Return String.Format("Blank {0}", "Vehicle Human Resource Temp Position")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      AddWebRule(StartDateProperty, Function(c) c.StartDate = c.EndDate, Function(c) "Start Date must be less than end date")

      With AddWebRule(HumanResourceIDProperty)
        .ASyncBusyText = "Checking Rule ...."
        .ServerRuleFunction = Function(vhrtp)

                                If vhrtp IsNot Nothing And vhrtp.GetParent IsNot Nothing Then
                                  If Not Singular.Misc.IsNullNothing(vhrtp.GetParent.HumanResourceID, True) AndAlso Not Singular.Misc.IsNullNothing(vhrtp.GetParent.DisciplineID, True) AndAlso Not Singular.Misc.IsNullNothing(vhrtp.HumanResourceID, True) Then
                                    Dim hrs As HR.ReadOnly.ROHumanResourceSkillList = CommonData.Lists.ROHumanResourceSkillList
                                    If hrs IsNot Nothing And hrs.Count > 0 Then
                                      If hrs.Where(Function(c) c.HumanResourceID = vhrtp.HumanResourceID AndAlso c.DisciplineID = vhrtp.GetParent.DisciplineID).Count = 0 Then
                                        Return "This Temp Resource does not have the required skills to replace the Human Resource."
                                      End If
                                    Else
                                      Return "The Temp resource has no assigned skills."
                                    End If
                                  Else
                                    Return "Please ensure that the Human Resource(including their Discipline) and Temp Human Resource are selected"
                                  End If
                                Else
                                  Return "Temp Position is null or Human Resource (parent) is null"
                                End If

                                Return ""
                              End Function
      End With

      With AddWebRule(HumanResourceIDProperty)
        .ASyncBusyText = "Checking Rule ...."
        .ServerRuleFunction = Function(tp)
                                If tp.GetParent IsNot Nothing AndAlso Not Singular.Misc.IsNullNothing(tp.GetParent.HumanResourceID, True) Then
                                  If Not Singular.Misc.IsNullNothing(tp.HumanResourceID, True) Then
                                    If tp.HumanResourceID = tp.GetParent.HumanResourceID Then
                                      Return "The temp resource cannot be the same as the resource you wish to replace"
                                    ElseIf Not CommonData.Lists.ROHumanResourceList.GetItem(tp.HumanResourceID).ActiveInd AndAlso _
                                         (tp.StartDate.HasValue AndAlso CDate(tp.StartDate) >= Now.Date OrElse tp.EndDate.HasValue AndAlso CDate(tp.EndDate) >= Now.Date) Then
                                      Return "This temp resource is not an active human resource"
                                    End If
                                  End If
                                Else
                                  Return "Human Resource cannot be blank"
                                End If

                                Return ""
                              End Function
        .AffectedProperties.Add(StartDateProperty)
        .AddTriggerProperty(StartDateProperty)
        .AffectedProperties.Add(EndDateProperty)
        .AddTriggerProperty(EndDateProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVehicleHumanResourceTempPosition() method.

    End Sub

    Public Shared Function NewVehicleHumanResourceTempPosition() As VehicleHumanResourceTempPosition

      Return DataPortal.CreateChild(Of VehicleHumanResourceTempPosition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVehicleHumanResourceTempPosition(dr As SafeDataReader) As VehicleHumanResourceTempPosition

      Dim v As New VehicleHumanResourceTempPosition()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VehicleHumanResourceTempPositionIDProperty, .GetInt32(0))
          LoadProperty(VehicleHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, .GetInt32(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(HumanResourceProperty, .GetString(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insVehicleHumanResourceTempPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updVehicleHumanResourceTempPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVehicleHumanResourceTempPositionID As SqlParameter = .Parameters.Add("@VehicleHumanResourceTempPositionID", SqlDbType.Int)
          paramVehicleHumanResourceTempPositionID.Value = GetProperty(VehicleHumanResourceTempPositionIDProperty)
          If Me.IsNew Then
            paramVehicleHumanResourceTempPositionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VehicleHumanResourceID", Me.GetParent().VehicleHumanResourceID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VehicleHumanResourceTempPositionIDProperty, paramVehicleHumanResourceTempPositionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delVehicleHumanResourceTempPosition"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VehicleHumanResourceTempPositionID", GetProperty(VehicleHumanResourceTempPositionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace