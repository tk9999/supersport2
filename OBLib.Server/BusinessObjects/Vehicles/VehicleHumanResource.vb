﻿' Generated 18 Feb 2015 14:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace VehiclesAndEquipment

  <Serializable()> _
  Public Class VehicleHumanResource
    Inherits OBBusinessBase(Of VehicleHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property VehicleHumanResourceID() As Integer
      Get
        Return GetProperty(VehicleHumanResourceIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.DisciplineID, 0) 
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline of the human resource assigned to the vehicle"),
    Required(ErrorMessage:="Discipline required"),
    Singular.DataAnnotations.DropDownWeb(GetType(RODisciplineList))>
    Public Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing) 
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource assigned to the vehicle"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property


    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Manager value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared PreferenceOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PreferenceOrder, "Preference Order", 1)
    ''' <summary>
    ''' Gets and sets the Preference Order value
    ''' </summary>
    <Display(Name:="Preference Order", Description:="The preference order that will be given to human resources when a vehicle is selected for a production"),
    Required(ErrorMessage:="Preference Order required")>
    Public Property PreferenceOrder() As Integer
      Get
        Return GetProperty(PreferenceOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PreferenceOrderProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared InActiveHumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InActiveHumanResource, "InActive Human Resource", "")
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="", Description:=""), ClientOnly()>
    Public Property InActiveHumanResource() As String
      Get
        Return GetProperty(InActiveHumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(InActiveHumanResourceProperty, Value)
      End Set
    End Property

    Public Shared HRIsSkilledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HRIsSkilled, "", True)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public Property HRIsSkilled() As Boolean
      Get
        Return GetProperty(HRIsSkilledProperty)
      End Get
      Set(value As Boolean)
        SetProperty(HRIsSkilledProperty, value)
      End Set
    End Property

    Public Shared CheckRulesIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CheckRulesInd, False) _
                                                                  .AddSetExpression("VehicleHumanResourceBO.CheckRulesIndSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Creditor value
    ''' </summary>
    <Display(Name:="Validate object?", Description:="Can We validate the object yet?")>
    Public Property CheckRulesInd() As Boolean
      Get
        Return GetProperty(CheckRulesIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CheckRulesIndProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSkillValidProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceSkillValid, "HumanResourceSkillValid", "")
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="", Description:="")>
    Public Property HumanResourceSkillValid() As String
      Get
        Return GetProperty(HumanResourceSkillValidProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceSkillValidProperty, Value)
      End Set
    End Property


    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#Region " Child Lists "

    Public Shared VehicleHumanResourceTempPositionListProperty As PropertyInfo(Of VehicleHumanResourceTempPositionList) = RegisterProperty(Of VehicleHumanResourceTempPositionList)(Function(c) c.VehicleHumanResourceTempPositionList, "Vehicle Human Resource Temp Position List")

    '  <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property VehicleHumanResourceTempPositionList() As VehicleHumanResourceTempPositionList
      Get
        If GetProperty(VehicleHumanResourceTempPositionListProperty) Is Nothing Then
          LoadProperty(VehicleHumanResourceTempPositionListProperty, VehiclesAndEquipment.VehicleHumanResourceTempPositionList.NewVehicleHumanResourceTempPositionList())
        End If
        Return GetProperty(VehicleHumanResourceTempPositionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Vehicle

      Return CType(CType(Me.Parent, VehicleHumanResourceList).Parent, Vehicle)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Vehicle Human Resource")
        Else
          Return String.Format("Blank {0}", "Vehicle Human Resource")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"VehicleHumanResourceTempPositions"}
      End Get
    End Property

    Public Sub ResetCheckRules()

      LoadProperty(CheckRulesIndProperty, False)

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      '' ValidationRules.AddRule(AddressOf Singular.CSLALib.Rules.CompareValue, New Singular.CSLALib.Rules.Args.CompareValueArgs("PreferenceOrder", 1, ">="))

      'Check if the Human Resource is Active before adding to the Vehicle 
      With AddWebRule(HumanResourceIDProperty)
        .ServerRuleFunction = AddressOf CheckActiveHumanResource
      End With

      ''Human Resource Skill Validation
      'With AddWebRule(DisciplineIDProperty)
      '  '.ASyncBusyText = "Checking Skill Validation Rule..."
      '  .ServerRuleFunction = AddressOf CheckHumanResourceSkillValid
      '  '.AffectedProperties.Add(DisciplineIDProperty)
      '  '.AddTriggerProperty(DisciplineIDProperty)
      'End With

      With AddWebRule(HumanResourceIDProperty)
        .ASyncBusyText = "Checking Skill"
        .ServerRuleFunction = AddressOf CheckHumanResourceSkill
        .AffectedProperties.Add(DisciplineIDProperty)
        .AddTriggerProperty(DisciplineIDProperty)
      End With

    End Sub

    Private Shared Function CheckActiveHumanResource(VehicleHumanResource As VehicleHumanResource) As String
      If VehicleHumanResource IsNot Nothing Then
        If VehicleHumanResource.HumanResourceID IsNot Nothing Then
          Dim ROHumanResource As HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.Where(Function(c) c.HumanResourceID = VehicleHumanResource.HumanResourceID).FirstOrDefault
          If ROHumanResource IsNot Nothing Then
            If VehicleHumanResource.HumanResourceID <> 0 AndAlso Not ROHumanResource.ActiveInd Then
              Return "Only active human resources can be added to vehicles"
            End If
          End If
        End If
      End If
      Return ""
    End Function

    Private Shared Function CheckHumanResourceSkill(VehicleHumanResource As VehicleHumanResource) As String
      VehicleHumanResource.CheckRulesInd = False
      If VehicleHumanResource IsNot Nothing And Not Singular.Misc.IsNullNothing(VehicleHumanResource.HumanResourceID, True) And Not Singular.Misc.IsNullNothing(VehicleHumanResource.DisciplineID, True) Then
        Dim hrs As HR.ReadOnly.ROHumanResourceSkillList = CommonData.Lists.ROHumanResourceSkillList
        If hrs IsNot Nothing And hrs.Count > 0 Then
          If hrs.Where(Function(f) f.HumanResourceID = VehicleHumanResource.HumanResourceID AndAlso f.DisciplineID = VehicleHumanResource.DisciplineID AndAlso (Not f.EndDate.HasValue OrElse f.EndDate.Value >= Now)).Count = 0 Then
              Return "This human resource is not qualified to assume this role."
          Else
            VehicleHumanResource.HRIsSkilled = True
            Return ""
          End If
        Else
          Return "This Human Resource has no assigned skills."
          VehicleHumanResource.HRIsSkilled = False
        End If
      Else
        Return "Please ensure that both a human resource and skill are selected."
        VehicleHumanResource.HRIsSkilled = False
      End If
      Return ""
      'VehicleHumanResource.HRIsSkilled = True
    End Function

    Private Shared Function CheckHumanResourceSkillValid(VehicleHumanResource As VehicleHumanResource) As String
      If Not VehicleHumanResource.HRIsSkilled Then
        VehicleHumanResource.HumanResourceSkillValid = "This human resource is not qualified to assume this role."
        Return "This human resource is not qualified to assume this role."
      Else
        VehicleHumanResource.HumanResourceSkillValid = ""
      End If
      Return ""
    End Function

    'Protected Overrides Sub AddBusinessRules()

    '  MyBase.AddBusinessRules()
    '  '' ValidationRules.AddRule(AddressOf Singular.CSLALib.Rules.CompareValue, New Singular.CSLALib.Rules.Args.CompareValueArgs("PreferenceOrder", 1, ">="))

    '  'Check if the Human Resource is Active before adding to the Vehicle 
    '  With AddWebRule(HumanResourceIDProperty)
    '    .ServerRuleFunction = AddressOf CheckActiveHumanResource
    '  End With

    '  'Human Resource Skill Validation
    '  With AddWebRule(HumanResourceIDProperty)
    '    .ASyncBusyText = "Checking Skill"
    '    .ServerRuleFunction = AddressOf CheckHumanResourceSkill
    '    .AffectedProperties.Add(DisciplineIDProperty)
    '    .AddTriggerProperty(DisciplineIDProperty)
    '  End With

    'End Sub

    'Private Shared Function CheckActiveHumanResource(VehicleHumanResource As VehicleHumanResource) As String
    '  If VehicleHumanResource IsNot Nothing Then
    '    If VehicleHumanResource.HumanResourceID IsNot Nothing Then
    '      Dim ROHumanResource As HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.Where(Function(c) c.HumanResourceID = VehicleHumanResource.HumanResourceID).FirstOrDefault
    '      If ROHumanResource IsNot Nothing Then
    '        If VehicleHumanResource.HumanResourceID <> 0 AndAlso Not ROHumanResource.ActiveInd Then
    '          Return "Only active human resources can be added to vehicles"
    '        End If
    '      End If
    '    End If
    '  End If
    '  Return ""
    'End Function

    'Private Shared Function CheckHumanResourceSkill(VehicleHumanResource As VehicleHumanResource) As String
    '  If VehicleHumanResource IsNot Nothing And Not Singular.Misc.IsNullNothing(VehicleHumanResource.HumanResourceID, True) And Not Singular.Misc.IsNullNothing(VehicleHumanResource.DisciplineID, True) Then
    '    Dim hrs As HR.ReadOnly.ROHumanResourceSkillList = CommonData.Lists.ROHumanResourceSkillList
    '    If hrs IsNot Nothing And hrs.Count > 0 Then
    '      If hrs.Where(Function(f) f.HumanResourceID = VehicleHumanResource.HumanResourceID AndAlso f.DisciplineID = VehicleHumanResource.DisciplineID AndAlso (Not f.EndDate.HasValue OrElse f.EndDate.Value >= Now)).Count = 0 Then
    '        Return "This human resource is not qualified to assume this role."
    '      End If
    '    Else
    '      Return "This Human Resource has no assigned skills."

    '    End If
    '  Else
    '    Return "Please ensure that both a human resource and skill are selected."

    '  End If

    '  Return ""
    'End Function

#End Region


#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVehicleHumanResource() method.

    End Sub

    Public Shared Function NewVehicleHumanResource() As VehicleHumanResource

      Return DataPortal.CreateChild(Of VehicleHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVehicleHumanResource(dr As SafeDataReader) As VehicleHumanResource

      Dim v As New VehicleHumanResource()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VehicleHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, .GetInt32(2))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PreferenceOrderProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(HumanResourceProperty, .GetString(9))
          LoadProperty(HRIsSkilledProperty, .GetBoolean(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insVehicleHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updVehicleHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVehicleHumanResourceID As SqlParameter = .Parameters.Add("@VehicleHumanResourceID", SqlDbType.Int)
          paramVehicleHumanResourceID.Value = GetProperty(VehicleHumanResourceIDProperty)
          If Me.IsNew Then
            paramVehicleHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VehicleID", Me.GetParent().VehicleID)
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@PreferenceOrder", GetProperty(PreferenceOrderProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VehicleHumanResourceIDProperty, paramVehicleHumanResourceID.Value)
          End If
          ' update child objects
          If GetProperty(VehicleHumanResourceTempPositionListProperty) IsNot Nothing Then
            Me.VehicleHumanResourceTempPositionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(VehicleHumanResourceTempPositionListProperty) IsNot Nothing Then
          Me.VehicleHumanResourceTempPositionList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delVehicleHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VehicleHumanResourceID", GetProperty(VehicleHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace