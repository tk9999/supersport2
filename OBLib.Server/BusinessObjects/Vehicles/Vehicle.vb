﻿' Generated 18 Feb 2015 14:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance 
Imports OBLib.Maintenance.Vehicles.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace VehiclesAndEquipment

  <Serializable()> _
  Public Class Vehicle
    Inherits OBBusinessBase(Of Vehicle)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleTypeID, "Vehicle Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:=""),
    Required(ErrorMessage:="Vehicle Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROVehicleTypeList))>
    Public Property VehicleTypeID() As Integer?
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets and sets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="Name ofthe Vehicle"),
    Required(ErrorMessage:="Vehicle Name required"),
    StringLength(50, ErrorMessage:="Vehicle Name cannot be more than 50 characters")>
    Public Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VehicleNameProperty, Value)
      End Set
    End Property

    Public Shared VehicleDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleDescription, "Vehicle Description", "")
    ''' <summary>
    ''' Gets and sets the Vehicle Description value
    ''' </summary>
    <Display(Name:="Vehicle Description", Description:="Description of the Vehicle"),
    StringLength(100, ErrorMessage:="Vehicle Description cannot be more than 100 characters")>
    Public Property VehicleDescription() As String
      Get
        Return GetProperty(VehicleDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VehicleDescriptionProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Supplier that this Vehicle is source from. NULL implies the vehicle belongs to MIH"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSupplierList))>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared NoOfDriversProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfDrivers, "No Of Drivers", 1)
    ''' <summary>
    ''' Gets and sets the No Of Drivers value
    ''' </summary>
    <Display(Name:="No Of Drivers", Description:="The minimum number of drivers required for this vehicle"),
    Required(ErrorMessage:="No Of Drivers required")>
    Public Property NoOfDrivers() As Integer
      Get
        Return GetProperty(NoOfDriversProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NoOfDriversProperty, Value)
      End Set
    End Property

    Public Shared OvernightNoOfDriversProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OvernightNoOfDrivers, "Overnight No Of Drivers", 1)
    ''' <summary>
    ''' Gets and sets the Overnight No Of Drivers value
    ''' </summary>
    <Display(Name:="Overnight No Of Drivers", Description:="The minimum number of drivers required for this vehicle if the scheduled travel tim is overnight"),
    Required(ErrorMessage:="Overnight No Of Drivers required")>
    Public Property OvernightNoOfDrivers() As Integer
      Get
        Return GetProperty(OvernightNoOfDriversProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OvernightNoOfDriversProperty, Value)
      End Set
    End Property

    Public Shared HDCompatibleIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDCompatibleInd, "HD Compatible", False)
    ''' <summary>
    ''' Gets and sets the HD Compatible value
    ''' </summary>
    <Display(Name:="HD Compatible", Description:="Tick indicates that this vehicle can be used for HD productions"),
    Required(ErrorMessage:="HD Compatible required")>
    Public Property HDCompatibleInd() As Boolean
      Get
        Return GetProperty(HDCompatibleIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HDCompatibleIndProperty, Value)
      End Set
    End Property

    Public Shared DecommissionedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DecommissionedDate, "Decommissioned Date")
    ''' <summary>
    ''' Gets and sets the Decommissioned Date value
    ''' </summary>
    <Display(Name:="Decommissioned Date", Description:="This is the date that the vehicle must stop being used (it cannot be loaded on a production after this date)")>
    Public Property DecommissionedDate As DateTime?
      Get
        Return GetProperty(DecommissionedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DecommissionedDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property


#End Region

#Region " Child Lists "

    Public Shared VehicleEquipmentListProperty As PropertyInfo(Of VehicleEquipmentList) = RegisterProperty(Of VehicleEquipmentList)(Function(c) c.VehicleEquipmentList, "Vehicle Equipment List")

    '  <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property VehicleEquipmentList() As VehicleEquipmentList
      Get
        If GetProperty(VehicleEquipmentListProperty) Is Nothing Then
          LoadProperty(VehicleEquipmentListProperty, VehiclesAndEquipment.VehicleEquipmentList.NewVehicleEquipmentList())
        End If
        Return GetProperty(VehicleEquipmentListProperty)
      End Get
    End Property

    Public Shared VehicleHumanResourceListProperty As PropertyInfo(Of VehicleHumanResourceList) = RegisterProperty(Of VehicleHumanResourceList)(Function(c) c.VehicleHumanResourceList, "Vehicle Human Resource List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property VehicleHumanResourceList() As VehicleHumanResourceList
      Get
        If GetProperty(VehicleHumanResourceListProperty) Is Nothing Then
          LoadProperty(VehicleHumanResourceListProperty, VehiclesAndEquipment.VehicleHumanResourceList.NewVehicleHumanResourceList())
        End If
        Return GetProperty(VehicleHumanResourceListProperty)
      End Get
    End Property

    Public Shared VehicleServiceListProperty As PropertyInfo(Of VehicleServiceList) = RegisterProperty(Of VehicleServiceList)(Function(c) c.VehicleServiceList, "Vehicle Service List")

    ' <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property VehicleServiceList() As VehicleServiceList
      Get
        If GetProperty(VehicleServiceListProperty) Is Nothing Then
          LoadProperty(VehicleServiceListProperty, VehiclesAndEquipment.VehicleServiceList.NewVehicleServiceList())
        End If
        Return GetProperty(VehicleServiceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.VehicleName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Vehicle")
        Else
          Return String.Format("Blank {0}", "Vehicle")
        End If
      Else
        Return Me.VehicleName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"VehicleEquipment", "VehicleHumanResources", "VehicleServices"}
      End Get
    End Property

    Public Overrides ReadOnly Property IsValid As Boolean
      Get
        Return MyBase.IsValid AndAlso VehicleEquipmentList.IsValid AndAlso
          VehicleHumanResourceList.IsValid AndAlso VehicleServiceList.IsValid
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'ValidationRules.AddRule(AddressOf Singular.CSLALib.Rules.CompareValue, New Singular.CSLALib.Rules.Args.CompareValueArgs("OvernightNoOfDrivers", 0, ">"))
      'ValidationRules.AddRule(AddressOf Singular.CSLALib.Rules.CompareValue, New Singular.CSLALib.Rules.Args.CompareValueArgs("NoOfDrivers", 0, ">"))

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVehicle() method.

    End Sub

    Public Shared Function NewVehicle() As Vehicle

      Return DataPortal.CreateChild(Of Vehicle)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVehicle(dr As SafeDataReader) As Vehicle

      Dim v As New Vehicle()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VehicleIDProperty, .GetInt32(0))
          LoadProperty(VehicleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(VehicleNameProperty, .GetString(2))
          LoadProperty(VehicleDescriptionProperty, .GetString(3))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(NoOfDriversProperty, .GetInt32(5))
          LoadProperty(OvernightNoOfDriversProperty, .GetInt32(6))
          LoadProperty(HDCompatibleIndProperty, .GetBoolean(7))
          LoadProperty(DecommissionedDateProperty, .GetValue(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insVehicle"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updVehicle"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVehicleID As SqlParameter = .Parameters.Add("@VehicleID", SqlDbType.Int)
          paramVehicleID.Value = GetProperty(VehicleIDProperty)
          If Me.IsNew Then
            paramVehicleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VehicleTypeID", GetProperty(VehicleTypeIDProperty))
          .Parameters.AddWithValue("@VehicleName", GetProperty(VehicleNameProperty))
          .Parameters.AddWithValue("@VehicleDescription", GetProperty(VehicleDescriptionProperty))
          .Parameters.AddWithValue("@SupplierID", Singular.Misc.NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@NoOfDrivers", GetProperty(NoOfDriversProperty))
          .Parameters.AddWithValue("@OvernightNoOfDrivers", GetProperty(OvernightNoOfDriversProperty))
          .Parameters.AddWithValue("@HDCompatibleInd", GetProperty(HDCompatibleIndProperty))
          .Parameters.AddWithValue("@DecommissionedDate", (New SmartDate(GetProperty(DecommissionedDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VehicleIDProperty, paramVehicleID.Value)
          End If
          ' update child objects
          If GetProperty(VehicleEquipmentListProperty) IsNot Nothing Then
            Me.VehicleEquipmentList.Update()
          End If
          If GetProperty(VehicleHumanResourceListProperty) IsNot Nothing Then
            Me.VehicleHumanResourceList.Update()
          End If
          If GetProperty(VehicleServiceListProperty) IsNot Nothing Then
            Me.VehicleServiceList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(VehicleEquipmentListProperty) IsNot Nothing Then
          Me.VehicleEquipmentList.Update()
        End If
        If GetProperty(VehicleHumanResourceListProperty) IsNot Nothing Then
          Me.VehicleHumanResourceList.Update()
        End If
        If GetProperty(VehicleServiceListProperty) IsNot Nothing Then
          Me.VehicleServiceList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delVehicle"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace