﻿' Generated 22 Oct 2015 10:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleDetailByProductionList
    Inherits OBReadOnlyListBase(Of ROVehicleDetailByProductionList, ROVehicleDetailByProduction)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROVehicleDetailByProduction

      For Each child As ROVehicleDetailByProduction In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionIDsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionIDs, "")
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="ProductionIDs")>
      Public Property ProductionIDs As String
        Get
          Return ReadProperty(ProductionIDsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionIDsProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVehicleDetailByProductionList() As ROVehicleDetailByProductionList

      Return New ROVehicleDetailByProductionList()

    End Function

    Public Shared Sub BeginGetROVehicleDetailByProductionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVehicleDetailByProductionList)))

      Dim dp As New DataPortal(Of ROVehicleDetailByProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROVehicleDetailByProductionList(CallBack As EventHandler(Of DataPortalResult(Of ROVehicleDetailByProductionList)))

      Dim dp As New DataPortal(Of ROVehicleDetailByProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVehicleDetailByProductionList() As ROVehicleDetailByProductionList

      Return DataPortal.Fetch(Of ROVehicleDetailByProductionList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVehicleDetailByProduction.GetROVehicleDetailByProduction(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleDetailByProductionList"
            cm.Parameters.AddWithValue("@ProductionIDs", Strings.MakeEmptyDBNull(crit.ProductionIDs))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace