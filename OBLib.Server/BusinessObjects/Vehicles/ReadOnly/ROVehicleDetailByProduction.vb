﻿' Generated 22 Oct 2015 10:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleDetailByProduction
    Inherits OBReadOnlyBase(Of ROVehicleDetailByProduction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProductionNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionName, "Production Name")
    ''' <summary>
    ''' Gets the Production Name value
    ''' </summary>
    <Display(Name:="Production Name", Description:="")>
  Public ReadOnly Property ProductionName() As String
      Get
        Return GetProperty(ProductionNameProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle")
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
  Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
  Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleTypeID, "Vehicle Type")
    ''' <summary>
    ''' Gets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
  Public ReadOnly Property VehicleTypeID() As Integer
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type")
    ''' <summary>
    ''' Gets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
  Public ReadOnly Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
    End Property

    Public Shared IsOBVanProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOBVan, "Is OB Van", False)
    ''' <summary>
    ''' Gets the Is OB Van value
    ''' </summary>
    <Display(Name:="Is OB Van", Description:="")>
  Public ReadOnly Property IsOBVan() As Boolean
      Get
        Return GetProperty(IsOBVanProperty)
      End Get
    End Property

    Public Shared IsExternalVanProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExternalVan, "Is External Van", False)
    ''' <summary>
    ''' Gets the Is External Van value
    ''' </summary>
    <Display(Name:="Is External Van", Description:="")>
  Public ReadOnly Property IsExternalVan() As Boolean
      Get
        Return GetProperty(IsExternalVanProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleDetailByProduction(dr As SafeDataReader) As ROVehicleDetailByProduction

      Dim r As New ROVehicleDetailByProduction()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ProductionNameProperty, .GetString(1))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(VehicleNameProperty, .GetString(3))
        LoadProperty(VehicleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(VehicleTypeProperty, .GetString(5))
        LoadProperty(IsOBVanProperty, .GetBoolean(6))
        LoadProperty(IsExternalVanProperty, .GetBoolean(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace