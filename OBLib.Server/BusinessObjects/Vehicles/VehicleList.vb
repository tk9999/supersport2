﻿' Generated 18 Feb 2015 14:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace VehiclesAndEquipment

  <Serializable()> _
  Public Class VehicleList
    Inherits OBBusinessListBase(Of VehicleList, Vehicle)

#Region " Business Methods "

    Public Function GetItem(VehicleID As Integer) As Vehicle

      For Each child As Vehicle In Me
        If child.VehicleID = VehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicles"

    End Function

    Public Function GetVehicleEquipment(VehicleEquipmentID As Integer) As VehicleEquipment

      Dim obj As VehicleEquipment = Nothing
      For Each parent As Vehicle In Me
        obj = parent.VehicleEquipmentList.GetItem(VehicleEquipmentID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetVehicleHumanResource(VehicleHumanResourceID As Integer) As VehicleHumanResource

      Dim obj As VehicleHumanResource = Nothing
      For Each parent As Vehicle In Me
        obj = parent.VehicleHumanResourceList.GetItem(VehicleHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetVehicleService(VehicleServiceID As Integer) As VehicleService

      Dim obj As VehicleService = Nothing
      For Each parent As Vehicle In Me
        obj = parent.VehicleServiceList.GetItem(VehicleServiceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property VehicleID As Object

      Public Sub New()


      End Sub
      Public Sub New(VehicleID As Object)
        Me.VehicleID = VehicleID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewVehicleList() As VehicleList

      Return New VehicleList()

    End Function

    Public Shared Sub BeginGetVehicleList(CallBack As EventHandler(Of DataPortalResult(Of VehicleList)))

      Dim dp As New DataPortal(Of VehicleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetVehicleList() As VehicleList

      Return DataPortal.Fetch(Of VehicleList)(New Criteria())

    End Function

    Public Shared Function GetVehicleList(ByVal VehicleID As Object) As VehicleList

      Return DataPortal.Fetch(Of VehicleList)(New Criteria(VehicleID))

    End Function
     

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Vehicle.GetVehicle(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Vehicle = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.VehicleID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.VehicleEquipmentList.RaiseListChangedEvents = False
      '    parent.VehicleEquipmentList.Add(VehicleEquipment.GetVehicleEquipment(sdr))
      '    parent.VehicleEquipmentList.RaiseListChangedEvents = True
      '  End While
      'End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.VehicleID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.VehicleHumanResourceList.RaiseListChangedEvents = False
          parent.VehicleHumanResourceList.Add(VehicleHumanResource.GetVehicleHumanResource(sdr))
          parent.VehicleHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As VehicleHumanResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.VehicleHumanResourceID <> sdr.GetInt32(1) Then
            parentChild = Me.GetVehicleHumanResource(sdr.GetInt32(1))
          End If
          parentChild.VehicleHumanResourceTempPositionList.RaiseListChangedEvents = False
          parentChild.VehicleHumanResourceTempPositionList.Add(VehicleHumanResourceTempPosition.GetVehicleHumanResourceTempPosition(sdr))
          parentChild.VehicleHumanResourceTempPositionList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.VehicleID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.VehicleServiceList.RaiseListChangedEvents = False
          parent.VehicleServiceList.Add(VehicleService.GetVehicleService(sdr))
          parent.VehicleServiceList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As Vehicle In Me
        child.CheckRules()
        For Each VehicleEquipment As VehicleEquipment In child.VehicleEquipmentList
          VehicleEquipment.CheckRules()
        Next
        For Each VehicleHumanResource As VehicleHumanResource In child.VehicleHumanResourceList
          VehicleHumanResource.CheckAllRules()

          For Each VehicleHumanResourceTempPosition As VehicleHumanResourceTempPosition In VehicleHumanResource.VehicleHumanResourceTempPositionList
            VehicleHumanResourceTempPosition.CheckAllRules()
          Next
        Next
        For Each VehicleService As VehicleService In child.VehicleServiceList
          VehicleService.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getVehicleList"
            cm.Parameters.AddWithValue("@VehicleID", Singular.Misc.ZeroNothingDBNull(crit.VehicleID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Vehicle In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Vehicle In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
        OBLib.CommonData.Refresh("ROVehicleList")
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace