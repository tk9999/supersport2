﻿Imports Singular.CommonData.Enums
Imports Csla
Imports Csla.Serialization
Imports Singular
Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Equipment.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace Copying

  Public Class CopySetting
    Inherits OBBusinessBase(Of CopySetting)

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentID, Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Circuit"),
    DropDownWeb(GetType(ROCircuitAvailabilityList),
                BeforeFetchJS:="CopySettingBO.setCircuitCriteriaBeforeRefresh",
                PreFindJSFunction:="CopySettingBO.triggerCircuitAutoPopulate",
                AfterFetchJS:="CopySettingBO.afterCircuitRefreshAjax",
                OnItemSelectJSFunction:="CopySettingBO.onCircuitSelected",
                LookupMember:="EquipmentName", DisplayMember:="EquipmentName", ValueMember:="ResourceID",
                DropDownCssClass:="circuit-dropdown", DropDownColumns:={"EquipmentName", "IsAvailableString"}),
    SetExpression("CopySettingBO.EquipmentIDSet(self)"),
    Required(ErrorMessage:="Circuit Required")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Circuit")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Circuit", Description:="")>
    Public Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="ResourceBookingID"), Required(ErrorMessage:="Resource is required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="ResourceBookingID"), Required(ErrorMessage:="Resource Booking is required")>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "ResourceBookingTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="ResourceBookingTypeID")>
    Public Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Description", "")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Description"), Required(AllowEmptystrings:=False, ErrorMessage:="Description is required")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Start Buffer"), DateField(FormatString:="ddd dd MMM yyyy HH:mm")>
    Public Property StartDateTimeBuffer() As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Starts"), DateField(FormatString:="ddd dd MMM yyyy HH:mm")>
    Public Property StartDateTime() As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Ends"), DateField(FormatString:="ddd dd MMM yyyy HH:mm")>
    Public Property EndDateTime() As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="End Buffer"), DateField(FormatString:="ddd dd MMM yyyy HH:mm")>
    Public Property EndDateTimeBuffer() As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeBufferProperty)
        .AddTriggerProperties({StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AffectedProperties.AddRange({StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .ServerRuleFunction = AddressOf DatesValid
        .JavascriptRuleFunctionName = "CopySettingBO.DatesValid"
      End With

    End Sub

    Public Shared Function DatesValid(CopySetting As CopySetting) As String

      If CopySetting.StartDateTimeBuffer IsNot Nothing AndAlso CopySetting.StartDateTime IsNot Nothing _
         AndAlso CopySetting.EndDateTime IsNot Nothing AndAlso CopySetting.EndDateTimeBuffer IsNot Nothing Then
        If CopySetting.StartDateTimeBuffer.Value.CompareTo(CopySetting.StartDateTime.Value) > 0 _
            OrElse CopySetting.StartDateTime.Value.CompareTo(CopySetting.EndDateTime.Value) > 0 _
            OrElse CopySetting.EndDateTime.Value.CompareTo(CopySetting.EndDateTimeBuffer.Value) > 0 Then
          Return "times overlap/clash"
        Else
          Return ""
        End If
      Else
        Return "All times are required"
      End If

    End Function

#Region " Copy Properties "

    Public Shared CopyToDatesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CopyToDates, "Copy to Dates", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Copy to Dates")>
    Public Property CopyToDates() As Boolean
      Get
        Return GetProperty(CopyToDatesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CopyToDatesProperty, Value)
      End Set
    End Property

    'Public Shared SelectDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.SelectDate, Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the System value
    ' ''' </summary>
    '<Display(Name:="Repeat Until"),
    'Singular.DataAnnotations.DateField(AlwaysShow:=True, AutoChange:=Singular.DataAnnotations.AutoChangeType.None)>
    'Public Property SelectDate() As DateTime?
    '  Get
    '    Return GetProperty(SelectDateProperty)
    '  End Get
    '  Set(ByVal Value As DateTime?)
    '    SetProperty(SelectDateProperty, Value)
    '  End Set
    'End Property

    Public Shared DatesToCopyToProperty As PropertyInfo(Of List(Of DateTime)) = RegisterProperty(Of List(Of DateTime))(Function(c) c.DatesToCopyTo, "Copy to Dates", New List(Of DateTime))
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Copy to Dates")>
    Public Property DatesToCopyTo() As List(Of DateTime)
      Get
        Return GetProperty(DatesToCopyToProperty)
      End Get
      Set(ByVal Value As List(Of DateTime))
        SetProperty(DatesToCopyToProperty, Value)
      End Set
    End Property

#End Region

#Region " Weekly Repetition Properties "

    Public Shared RepeatBookingProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RepeatBooking, "Repeat Booking", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Repeat Booking")>
    Public Property RepeatBooking() As Boolean
      Get
        Return GetProperty(RepeatBookingProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RepeatBookingProperty, Value)
      End Set
    End Property

    Public Shared MondayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Monday, "Monday", False)
    Public Property Monday() As Boolean
      Get
        Return GetProperty(MondayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(MondayProperty, value)
      End Set
    End Property

    Public Shared TuesdayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Tuesday, "Tuesday", False)
    Public Property Tuesday() As Boolean
      Get
        Return GetProperty(TuesdayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(TuesdayProperty, value)
      End Set
    End Property

    Public Shared WednesdayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Wednesday, "Wednesday", False)
    Public Property Wednesday() As Boolean
      Get
        Return GetProperty(WednesdayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(WednesdayProperty, value)
      End Set
    End Property

    Public Shared ThursdayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Thursday, "Thursday", False)
    Public Property Thursday() As Boolean
      Get
        Return GetProperty(ThursdayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ThursdayProperty, value)
      End Set
    End Property

    Public Shared FridayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Friday, "Friday", False)
    Public Property Friday() As Boolean
      Get
        Return GetProperty(FridayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(FridayProperty, value)
      End Set
    End Property

    Public Shared SaturdayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Saturday, "Saturday", False)
    Public Property Saturday() As Boolean
      Get
        Return GetProperty(SaturdayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(SaturdayProperty, value)
      End Set
    End Property

    Public Shared SundayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Sunday, "Sunday", False)
    Public Property Sunday() As Boolean
      Get
        Return GetProperty(SundayProperty)
      End Get
      Set(value As Boolean)
        SetProperty(SundayProperty, value)
      End Set
    End Property

    Public Shared RepeatUntilProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.RepeatUntil, Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Repeat Until")>
    Public Property RepeatUntil() As DateTime?
      Get
        Return GetProperty(RepeatUntilProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(RepeatUntilProperty, Value)
      End Set
    End Property

#End Region

    Public Function Submit() As List(Of OBLib.Helpers.CopyResult)

      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdResourceSchedulerCopyEquipmentSchedule]",
                                          New String() {"@ResourceID", "@EquipmentID", "@ResourceBookingID", "@CurrentUserID", "DatesToCopyTo",
                                                        "@StartDateTimeBuffer", "@StartDateTime", "@EndDateTime", "@EndDateTimeBuffer"},
                                          New Object() {Me.ResourceID, NothingDBNull(Me.EquipmentID), Me.ResourceBookingID, OBLib.Security.Settings.CurrentUserID, OBMisc.DateListToXML(Me.DatesToCopyTo),
                                                        StartDateTimeBuffer, StartDateTime, EndDateTime, EndDateTimeBuffer})
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      Dim res As New List(Of OBLib.Helpers.CopyResult)
      For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
        res.Add(New OBLib.Helpers.CopyResult(dr))
      Next
      Return res

    End Function

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insRSResource"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "UpdProcsWeb.updRSResource"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramResourceID As SqlParameter = .Parameters.Add("@ResourceID", SqlDbType.Int)
      '    paramResourceID.Value = GetProperty(ResourceIDProperty)
      '    If Me.IsNew Then
      '      paramResourceID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@ResourceName", GetProperty(ResourceNameProperty))
      '    .Parameters.AddWithValue("@ResourceTypeID", GetProperty(ResourceTypeIDProperty))
      '    .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      '    .Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      '    .Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
      '    .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
      '    .Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
      '    '.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      '    .Parameters.AddWithValue("@ReqShifts", GetProperty(ReqShiftsProperty))
      '    .Parameters.AddWithValue("@ReqHours", GetProperty(ReqHoursProperty))
      '    .Parameters.AddWithValue("@TotalShifts", GetProperty(TotalShiftsProperty))
      '    .Parameters.AddWithValue("@TotalHours", GetProperty(TotalHoursProperty))
      '    .Parameters.AddWithValue("@OvertimeHours", GetProperty(OvertimeHoursProperty))
      '    .Parameters.AddWithValue("@ShortfallHours", GetProperty(ShortfallHoursProperty))
      '    .Parameters.AddWithValue("@PrevBalanceHours", GetProperty(PrevBalanceHoursProperty))

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(ResourceIDProperty, paramResourceID.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(RSResourceBookingListProperty) IsNot Nothing Then
      '      Me.RSResourceBookingList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      '  ' update child objects
      '  If GetProperty(RSResourceBookingListProperty) IsNot Nothing Then
      '    Me.RSResourceBookingList.Update()
      '  End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delRSResource"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      'If Me.IsNew Then Exit Sub

      'cm.ExecuteNonQuery()
      'MarkNew()

    End Sub

  End Class

End Namespace
