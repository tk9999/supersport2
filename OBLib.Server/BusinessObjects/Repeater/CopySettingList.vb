﻿Imports Singular.CommonData.Enums
Imports Csla
Imports Csla.Serialization
Imports Singular
Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations

Namespace Copying

  Public Class CopySettingList
    Inherits OBBusinessListBase(Of CopySettingList, CopySetting)

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CopySetting In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CopySetting In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

  End Class

End Namespace
