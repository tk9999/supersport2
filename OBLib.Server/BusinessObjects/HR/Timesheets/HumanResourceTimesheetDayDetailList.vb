﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetDayDetailList
    Inherits OBBusinessListBase(Of HumanResourceTimesheetDayDetailList, HumanResourceTimesheetDayDetail)

#Region " Business Methods "

    Public Function GetItem(HumanResourceTimesheetDayDetailID As Integer) As HumanResourceTimesheetDayDetail

      For Each child As HumanResourceTimesheetDayDetail In Me
        If child.HumanResourceTimesheetDayDetailID = HumanResourceTimesheetDayDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Timesheet Day Details"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewHumanResourceTimesheetDayDetailList() As HumanResourceTimesheetDayDetailList

      Return New HumanResourceTimesheetDayDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace