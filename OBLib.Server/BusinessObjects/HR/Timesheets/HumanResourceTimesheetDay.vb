﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetDay
    Inherits OBBusinessBase(Of HumanResourceTimesheetDay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceTimesheetDayBO.HumanResourceTimesheetDayBOToString(self)")

    Public Shared HumanResourceTimesheetDayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceTimesheetDayID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property HumanResourceTimesheetDayID() As Integer
      Get
        Return GetProperty(HumanResourceTimesheetDayIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceTimesheetID, "Human Resource Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Timesheet value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceTimesheetID() As Integer?
      Get
        Return GetProperty(HumanResourceTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceTimesheetDayCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceTimesheetDayCategoryID, "Day Category", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Timesheet Day Category value
    ''' </summary>
    <Display(Name:="Day Category", Description:=""),
    Required(ErrorMessage:="Day Category required")>
    Public Property HumanResourceTimesheetDayCategoryID() As Integer?
      Get
        Return GetProperty(HumanResourceTimesheetDayCategoryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceTimesheetDayCategoryIDProperty, Value)
      End Set
    End Property

    Public Shared TimesheetDayProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.TimesheetDay, "Day")
    ''' <summary>
    ''' Gets and sets the Timesheet Day value
    ''' </summary>
    <Display(Name:="Day", Description:=""),
    Required(ErrorMessage:="Day required"),
    DateField(FormatString:="ddd dd MMM yy")>
    Public Property TimesheetDay As Date
      Get
        Return GetProperty(TimesheetDayProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(TimesheetDayProperty, Value)
      End Set
    End Property

    Public Shared TimesheetDayDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetDayDescription, "Day Description", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Day Description value
    ''' </summary>
    <Display(Name:="Day Description", Description:=""),
    StringLength(1024, ErrorMessage:="Day Description cannot be more than 1024 characters")>
    Public Property TimesheetDayDescription() As String
      Get
        Return GetProperty(TimesheetDayDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimesheetDayDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeSystemProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartDateTimeSystem, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time System value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property StartDateTimeSystem As DateTime
      Get
        Return GetProperty(StartDateTimeSystemProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(StartDateTimeSystemProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeSystemProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndDateTimeSystem, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time System value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property EndDateTimeSystem As DateTime
      Get
        Return GetProperty(EndDateTimeSystemProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(EndDateTimeSystemProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeDisputeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeDispute, "Dispute Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Dispute value
    ''' </summary>
    <Display(Name:="Dispute Start Time ", Description:="")>
    Public Property StartDateTimeDispute As DateTime?
      Get
        Return GetProperty(StartDateTimeDisputeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeDisputeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeDisputeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeDispute, "Dispute End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Dispute value
    ''' </summary>
    <Display(Name:="Dispute End Time", Description:="")>
    Public Property EndDateTimeDispute As DateTime?
      Get
        Return GetProperty(EndDateTimeDisputeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeDisputeProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeManagerProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeManager, "Manager Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Manager value
    ''' </summary>
    <Display(Name:="Manager Start Time", Description:="")>
    Public Property StartDateTimeManager As DateTime?
      Get
        Return GetProperty(StartDateTimeManagerProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeManagerProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeManagerProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeManager, "Manager End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Manager value
    ''' </summary>
    <Display(Name:="Manager End Time", Description:="")>
    Public Property EndDateTimeManager As DateTime?
      Get
        Return GetProperty(EndDateTimeManagerProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeManagerProperty, Value)
      End Set
    End Property

    Public Shared NormalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.NormalHours, "Normal", CDec(0))
    ''' <summary>
    ''' Gets and sets the Normal Hours value
    ''' </summary>
    <Display(Name:="Normal", Description:=""),
    Required(ErrorMessage:="Normal required")>
    Public Property NormalHours() As Decimal
      Get
        Return GetProperty(NormalHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(NormalHoursProperty, Value)
      End Set
    End Property

    Public Shared WeekdayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekdayHours, "Weekday", CDec(0))
    ''' <summary>
    ''' Gets and sets the Weekday Hours value
    ''' </summary>
    <Display(Name:="Weekday", Description:=""),
    Required(ErrorMessage:="Weekday required")>
    Public Property WeekdayHours() As Decimal
      Get
        Return GetProperty(WeekdayHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(WeekdayHoursProperty, Value)
      End Set
    End Property

    Public Shared WeekendHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekendHours, "Weekend", CDec(0))
    ''' <summary>
    ''' Gets and sets the Weekend Hours value
    ''' </summary>
    <Display(Name:="Weekend", Description:=""),
    Required(ErrorMessage:="Weekend required")>
    Public Property WeekendHours() As Decimal
      Get
        Return GetProperty(WeekendHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(WeekendHoursProperty, Value)
      End Set
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime", CDec(0))
    ''' <summary>
    ''' Gets and sets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime", Description:=""),
    Required(ErrorMessage:="Overtime required")>
    Public Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(OvertimeHoursProperty, Value)
      End Set
    End Property

    Public Shared PublicHolidayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PublicHolidayHours, "PH.", CDec(0))
    ''' <summary>
    ''' Gets and sets the Public Holiday Hours value
    ''' </summary>
    <Display(Name:="PH.", Description:=""),
    Required(ErrorMessage:="PH. required")>
    Public Property PublicHolidayHours() As Decimal
      Get
        Return GetProperty(PublicHolidayHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(PublicHolidayHoursProperty, Value)
      End Set
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall", CDec(0))
    ''' <summary>
    ''' Gets and sets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall", Description:=""),
    Required(ErrorMessage:="Shortfall required")>
    Public Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShortfallHoursProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceTimesheetDayCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceTimesheetDayCategory, "Category", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Day Description value
    ''' </summary>
    <Display(Name:="Category", Description:=""),
    StringLength(1024, ErrorMessage:="Category cannot be more than 1024 characters")>
    Public Property HumanResourceTimesheetDayCategory() As String
      Get
        Return GetProperty(HumanResourceTimesheetDayCategoryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceTimesheetDayCategoryProperty, Value)
      End Set
    End Property

    Public Shared StartTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTimeString, "Start Time", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Day Description value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public Property StartTimeString() As String
      Get
        Return GetProperty(StartTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StartTimeStringProperty, Value)
      End Set
    End Property

    Public Shared EndTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndTimeString, "End Time", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Day Description value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public Property EndTimeString() As String
      Get
        Return GetProperty(EndTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EndTimeStringProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared HumanResourceTimesheetDayDetailListProperty As PropertyInfo(Of HumanResourceTimesheetDayDetailList) = RegisterProperty(Of HumanResourceTimesheetDayDetailList)(Function(c) c.HumanResourceTimesheetDayDetailList, "Human Resource Timesheet Day Detail List")

    Public ReadOnly Property HumanResourceTimesheetDayDetailList() As HumanResourceTimesheetDayDetailList
      Get
        If GetProperty(HumanResourceTimesheetDayDetailListProperty) Is Nothing Then
          LoadProperty(HumanResourceTimesheetDayDetailListProperty, HR.Timesheets.HumanResourceTimesheetDayDetailList.NewHumanResourceTimesheetDayDetailList())
        End If
        Return GetProperty(HumanResourceTimesheetDayDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResourceTimesheet

      Return CType(CType(Me.Parent, HumanResourceTimesheetDayList).Parent, HumanResourceTimesheet)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceTimesheetDayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TimesheetDayDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Timesheet Day")
        Else
          Return String.Format("Blank {0}", "Human Resource Timesheet Day")
        End If
      Else
        Return Me.TimesheetDayDescription
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"HumanResourceTimesheetDayDetail"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceTimesheetDay() method.

    End Sub

    Public Shared Function NewHumanResourceTimesheetDay() As HumanResourceTimesheetDay

      Return DataPortal.CreateChild(Of HumanResourceTimesheetDay)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetHumanResourceTimesheetDay(dr As SafeDataReader) As HumanResourceTimesheetDay

      Dim h As New HumanResourceTimesheetDay()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceTimesheetDayIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, .GetInt32(1))
          LoadProperty(HumanResourceTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceTimesheetDayCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(TimesheetDayProperty, .GetValue(4))
          LoadProperty(TimesheetDayDescriptionProperty, .GetString(5))
          LoadProperty(StartDateTimeSystemProperty, .GetValue(6))
          LoadProperty(EndDateTimeSystemProperty, .GetValue(7))
          LoadProperty(StartDateTimeDisputeProperty, .GetValue(8))
          LoadProperty(EndDateTimeDisputeProperty, .GetValue(9))
          LoadProperty(StartDateTimeManagerProperty, .GetValue(10))
          LoadProperty(EndDateTimeManagerProperty, .GetValue(11))
          LoadProperty(NormalHoursProperty, .GetDecimal(12))
          LoadProperty(WeekdayHoursProperty, .GetDecimal(13))
          LoadProperty(WeekendHoursProperty, .GetDecimal(14))
          LoadProperty(OvertimeHoursProperty, .GetDecimal(15))
          LoadProperty(PublicHolidayHoursProperty, .GetDecimal(16))
          LoadProperty(ShortfallHoursProperty, .GetDecimal(17))
          LoadProperty(CreatedByProperty, .GetInt32(18))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(19))
          LoadProperty(ModifiedByProperty, .GetInt32(20))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(21))
          LoadProperty(HumanResourceTimesheetDayCategoryProperty, .GetString(22))
          LoadProperty(StartTimeStringProperty, .GetString(23))
          LoadProperty(EndTimeStringProperty, .GetString(24))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceTimesheetDayIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@HumanResourceTimesheetID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceTimesheetIDProperty)))
      cm.Parameters.AddWithValue("@HumanResourceTimesheetDayCategoryID", GetProperty(HumanResourceTimesheetDayCategoryIDProperty))
      cm.Parameters.AddWithValue("@TimesheetDay", TimesheetDay)
      cm.Parameters.AddWithValue("@TimesheetDayDescription", GetProperty(TimesheetDayDescriptionProperty))
      cm.Parameters.AddWithValue("@StartDateTimeSystem", StartDateTimeSystem)
      cm.Parameters.AddWithValue("@EndDateTimeSystem", EndDateTimeSystem)
      cm.Parameters.AddWithValue("@StartDateTimeDispute", Singular.Misc.NothingDBNull(StartDateTimeDispute))
      cm.Parameters.AddWithValue("@EndDateTimeDispute", Singular.Misc.NothingDBNull(EndDateTimeDispute))
      cm.Parameters.AddWithValue("@StartDateTimeManager", Singular.Misc.NothingDBNull(StartDateTimeManager))
      cm.Parameters.AddWithValue("@EndDateTimeManager", Singular.Misc.NothingDBNull(EndDateTimeManager))
      cm.Parameters.AddWithValue("@NormalHours", GetProperty(NormalHoursProperty))
      cm.Parameters.AddWithValue("@WeekdayHours", GetProperty(WeekdayHoursProperty))
      cm.Parameters.AddWithValue("@WeekendHours", GetProperty(WeekendHoursProperty))
      cm.Parameters.AddWithValue("@OvertimeHours", GetProperty(OvertimeHoursProperty))
      cm.Parameters.AddWithValue("@PublicHolidayHours", GetProperty(PublicHolidayHoursProperty))
      cm.Parameters.AddWithValue("@ShortfallHours", GetProperty(ShortfallHoursProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceTimesheetDayIDProperty, cm.Parameters("@HumanResourceTimesheetDayID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(HumanResourceTimesheetDayDetailListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceTimesheetDayID", GetProperty(HumanResourceTimesheetDayIDProperty))
    End Sub

#End Region

  End Class

End Namespace