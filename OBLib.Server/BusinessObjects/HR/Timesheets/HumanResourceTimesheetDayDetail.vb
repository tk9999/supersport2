﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetDayDetail
    Inherits OBBusinessBase(Of HumanResourceTimesheetDayDetail)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceTimesheetDayDetailBO.HumanResourceTimesheetDayDetailBOToString(self)")

    Public Shared HumanResourceTimesheetDayDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceTimesheetDayDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property HumanResourceTimesheetDayDetailID() As Integer
      Get
        Return GetProperty(HumanResourceTimesheetDayDetailIDProperty)
      End Get
    End Property

    Public Shared HumanResourceTimesheetDayIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceTimesheetDayID, "Human Resource Timesheet Day", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Timesheet Day value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceTimesheetDayID() As Integer?
      Get
        Return GetProperty(HumanResourceTimesheetDayIDProperty)
      End Get
    End Property

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewScheduleDetailID, "Crew Schedule Detail", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Schedule Detail value
    ''' </summary>
    <Display(Name:="Crew Schedule Detail", Description:="")>
    Public Property CrewScheduleDetailID() As Integer?
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewScheduleDetailIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResourceTimesheetDay

      Return CType(CType(Me.Parent, HumanResourceTimesheetDayDetailList).Parent, HumanResourceTimesheetDay)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceTimesheetDayDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceTimesheetDayDetailID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Timesheet Day Detail")
        Else
          Return String.Format("Blank {0}", "Human Resource Timesheet Day Detail")
        End If
      Else
        Return Me.HumanResourceTimesheetDayDetailID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceTimesheetDayDetail() method.

    End Sub

    Public Shared Function NewHumanResourceTimesheetDayDetail() As HumanResourceTimesheetDayDetail

      Return DataPortal.CreateChild(Of HumanResourceTimesheetDayDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetHumanResourceTimesheetDayDetail(dr As SafeDataReader) As HumanResourceTimesheetDayDetail

      Dim h As New HumanResourceTimesheetDayDetail()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceTimesheetDayDetailIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceTimesheetDayIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CrewScheduleDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceTimesheetDayDetailIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceTimesheetDayID", Me.GetParent().HumanResourceTimesheetDayID)
      cm.Parameters.AddWithValue("@CrewScheduleDetailID", Singular.Misc.NothingDBNull(GetProperty(CrewScheduleDetailIDProperty)))
      cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceTimesheetDayDetailIDProperty, cm.Parameters("@HumanResourceTimesheetDayDetailID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceTimesheetDayDetailID", GetProperty(HumanResourceTimesheetDayDetailIDProperty))
    End Sub

#End Region

  End Class

End Namespace