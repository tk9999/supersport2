﻿' Generated 22 Jul 2016 12:10 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceTimesheetPagedList
    Inherits OBReadOnlyListBase(Of ROHumanResourceTimesheetPagedList, ROHumanResourceTimesheetPaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROHumanResourceTimesheetPagedList As ROHumanResourceTimesheetPagedList
    'Public Property ROHumanResourceTimesheetPagedListCriteria As ROHumanResourceTimesheetPagedList.Criteria
    'Public Property ROHumanResourceTimesheetPagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROHumanResourceTimesheetPagedList = New ROHumanResourceTimesheetPagedList
    'ROHumanResourceTimesheetPagedListCriteria = New ROHumanResourceTimesheetPagedList.Criteria
    'ROHumanResourceTimesheetPagedListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROHumanResourceTimesheetPagedList, Function(d) d.ROHumanResourceTimesheetPagedListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(HumanResourceTimesheetID As Integer) As ROHumanResourceTimesheetPaged

      For Each child As ROHumanResourceTimesheetPaged In Me
        If child.HumanResourceTimesheetID = HumanResourceTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Human Resource Timesheets"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property StartDate As Integer? = Nothing
      Public Property EndDate As Integer? = Nothing
      Public Property TimesheetRequirementID As Integer? = Nothing

      Public Sub New(HumanResourceID As Integer?, StartDate As Integer?, EndDate As Integer?, _
                     TimesheetRequirementID As Integer?)

        Me.HumanResourceID = HumanResourceID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.TimesheetRequirementID = TimesheetRequirementID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROHumanResourceTimesheetPagedList() As ROHumanResourceTimesheetPagedList

      Return New ROHumanResourceTimesheetPagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROHumanResourceTimesheetPagedList() As ROHumanResourceTimesheetPagedList

      Return DataPortal.Fetch(Of ROHumanResourceTimesheetPagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceTimesheetPaged.GetROHumanResourceTimesheetPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceTimesheetPagedList"
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@TimesheetRequirementID", Singular.Misc.NothingDBNull(crit.TimesheetRequirementID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace