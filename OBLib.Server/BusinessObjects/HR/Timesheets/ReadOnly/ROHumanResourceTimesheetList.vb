﻿' Generated 22 Jul 2016 12:12 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceTimesheetList
    Inherits OBReadOnlyListBase(Of ROHumanResourceTimesheetList, ROHumanResourceTimesheet)

#Region " Business Methods "

    Public Function GetItem(HumanResourceTimesheetID As Integer) As ROHumanResourceTimesheet

      For Each child As ROHumanResourceTimesheet In Me
        If child.HumanResourceTimesheetID = HumanResourceTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Timesheets"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property StartDate As Integer? = Nothing
      Public Property EndDate As Integer? = Nothing
      Public Property TimesheetRequirementID As Integer? = Nothing

      Public Sub New(HumanResourceID As Integer?, StartDate As Integer?, EndDate As Integer?, _
                     TimesheetRequirementID As Integer?)

        Me.HumanResourceID = HumanResourceID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.TimesheetRequirementID = TimesheetRequirementID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROHumanResourceTimesheetList() As ROHumanResourceTimesheetList

      Return New ROHumanResourceTimesheetList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROHumanResourceTimesheetList() As ROHumanResourceTimesheetList

      Return DataPortal.Fetch(Of ROHumanResourceTimesheetList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceTimesheet.GetROHumanResourceTimesheet(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceTimesheetList"
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@TimesheetRequirementID", Singular.Misc.NothingDBNull(crit.TimesheetRequirementID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace