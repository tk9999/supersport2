﻿' Generated 26 Aug 2016 14:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceTimesheetDayList
    Inherits OBReadOnlyListBase(Of ROHumanResourceTimesheetDayList, ROHumanResourceTimesheetDay)

#Region " Business Methods "

    Public Function GetItem(HumanResourceTimesheetDayID As Integer) As ROHumanResourceTimesheetDay

      For Each child As ROHumanResourceTimesheetDay In Me
        If child.HumanResourceTimesheetDayID = HumanResourceTimesheetDayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Timesheet Days"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property ClashHumanResourceTimesheetID As Integer? = Nothing
      Public Property ClashCheck As Boolean = False

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROHumanResourceTimesheetDayList() As ROHumanResourceTimesheetDayList

      Return New ROHumanResourceTimesheetDayList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROHumanResourceTimesheetDayList() As ROHumanResourceTimesheetDayList

      Return DataPortal.Fetch(Of ROHumanResourceTimesheetDayList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceTimesheetDay.GetROHumanResourceTimesheetDay(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceTimesheetDayList"
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ClashHumanResourceTimesheetID", Singular.Misc.NothingDBNull(crit.ClashHumanResourceTimesheetID))
            cm.Parameters.AddWithValue("@ClashCheck", crit.ClashCheck)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace