﻿' Generated 22 Jul 2016 12:10 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceTimesheetPaged
    Inherits OBReadOnlyBase(Of ROHumanResourceTimesheetPaged)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROHumanResourceTimesheetPagedBO.ROHumanResourceTimesheetPagedBOToString(self)")

    Public Shared HumanResourceTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceTimesheetID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceTimesheetID() As Integer
      Get
        Return GetProperty(HumanResourceTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDate As Date
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared StartingHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.StartingHours, "Balance/Carry Over Hrs", CDec(0.0))
    ''' <summary>
    ''' Gets the Starting Hours value
    ''' </summary>
    <Display(Name:="Balance/Carry Over Hrs", Description:="")>
    Public ReadOnly Property StartingHours() As Decimal
      Get
        Return GetProperty(StartingHoursProperty)
      End Get
    End Property

    Public Shared StartingShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartingShifts, "Balance/Carry Over Shifts", 0)
    ''' <summary>
    ''' Gets the Starting Shifts value
    ''' </summary>
    <Display(Name:="Balance/Carry Over Shifts", Description:="")>
    Public ReadOnly Property StartingShifts() As Integer
      Get
        Return GetProperty(StartingShiftsProperty)
      End Get
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Req Hours", CDec(0.0))
    ''' <summary>
    ''' Gets the Required Hours value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public ReadOnly Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
    End Property

    Public Shared RequiredShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredShifts, "Req Shifts", 0)
    ''' <summary>
    ''' Gets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:="")>
    Public ReadOnly Property RequiredShifts() As Integer
      Get
        Return GetProperty(RequiredShiftsProperty)
      End Get
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Hours", CDec(0.0))
    ''' <summary>
    ''' Gets the Total Hours value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
    Public ReadOnly Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
    End Property

    Public Shared TotalShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShifts, "Shifts", 0)
    ''' <summary>
    ''' Gets the Total Shifts value
    ''' </summary>
    <Display(Name:="Total Shifts", Description:="")>
    Public ReadOnly Property TotalShifts() As Integer
      Get
        Return GetProperty(TotalShiftsProperty)
      End Get
    End Property

    Public Shared TotalOvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalOvertime, "Overtime", CDec(0.0))
    ''' <summary>
    ''' Gets the Total Overtime value
    ''' </summary>
    <Display(Name:="Total Overtime", Description:="")>
    Public ReadOnly Property TotalOvertime() As Decimal
      Get
        Return GetProperty(TotalOvertimeProperty)
      End Get
    End Property

    Public Shared TotalShortfallProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalShortfall, "Shortfall", CDec(0.0))
    ''' <summary>
    ''' Gets the Total Shortfall value
    ''' </summary>
    <Display(Name:="Total Shortfall", Description:="")>
    Public ReadOnly Property TotalShortfall() As Decimal
      Get
        Return GetProperty(TotalShortfallProperty)
      End Get
    End Property

    Public Shared TotalPublicHolidayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalPublicHolidayHours, "P. Holiday", CDec(0.0))
    ''' <summary>
    ''' Gets the Total Public Holiday Hours value
    ''' </summary>
    <Display(Name:="Total Public Holiday Hours", Description:="")>
    Public ReadOnly Property TotalPublicHolidayHours() As Decimal
      Get
        Return GetProperty(TotalPublicHolidayHoursProperty)
      End Get
    End Property

    Public Shared TimesheetRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetRequirementID, "Timesheet Requirement", Nothing)
    ''' <summary>
    ''' Gets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Timesheet Requirement", Description:="")>
    Public ReadOnly Property TimesheetRequirementID() As Integer?
      Get
        Return GetProperty(TimesheetRequirementIDProperty)
      End Get
    End Property

    Public Shared ClosingHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ClosingHours, "Closing Hours", CDec(0))
    ''' <summary>
    ''' Gets the Closing Hours value
    ''' </summary>
    <Display(Name:="Closing Hours", Description:="")>
    Public ReadOnly Property ClosingHours() As Decimal
      Get
        Return GetProperty(ClosingHoursProperty)
      End Get
    End Property

    Public Shared ClosingShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClosingShifts, "Closing Shifts", 0)
    ''' <summary>
    ''' Gets the Closing Shifts value
    ''' </summary>
    <Display(Name:="Closing Shifts", Description:="")>
    Public ReadOnly Property ClosingShifts() As Integer
      Get
        Return GetProperty(ClosingShiftsProperty)
      End Get
    End Property

    Public Shared IsClosedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsClosed, "Is Closed", False)
    ''' <summary>
    ''' Gets the Is Closed value
    ''' </summary>
    <Display(Name:="Is Closed", Description:="")>
    Public ReadOnly Property IsClosed() As Boolean
      Get
        Return GetProperty(IsClosedProperty)
      End Get
    End Property

    Public Shared IsClosedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IsClosedByUserID, "Is Closed By User", Nothing)
    ''' <summary>
    ''' Gets the Is Closed By User value
    ''' </summary>
    <Display(Name:="Is Closed By User", Description:="")>
    Public ReadOnly Property IsClosedByUserID() As Integer?
      Get
        Return GetProperty(IsClosedByUserIDProperty)
      End Get
    End Property

    Public Shared IsClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.IsClosedDateTime, "Is Closed Date Time")
    ''' <summary>
    ''' Gets the Is Closed Date Time value
    ''' </summary>
    <Display(Name:="Is Closed Date Time", Description:="")>
    Public ReadOnly Property IsClosedDateTime As DateTime?
      Get
        Return GetProperty(IsClosedDateTimeProperty)
      End Get
    End Property

    Public Shared ClosedByUserProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClosedByUser, "Closed By", "")
    ''' <summary>
    ''' Gets the Closing Shifts value
    ''' </summary>
    <Display(Name:="Closed By", Description:="")>
    Public ReadOnly Property ClosedByUser() As String
      Get
        Return GetProperty(ClosedByUserProperty)
      End Get
    End Property

    Public Shared ClosedDateDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClosedDateDescription, "Closed Date", "")
    ''' <summary>
    ''' Gets the Closing Shifts value
    ''' </summary>
    <Display(Name:="Closed Date", Description:="")>
    Public ReadOnly Property ClosedDateDescription() As String
      Get
        Return GetProperty(ClosedDateDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResourceTimesheetID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROHumanResourceTimesheetPaged(dr As SafeDataReader) As ROHumanResourceTimesheetPaged

      Dim r As New ROHumanResourceTimesheetPaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceTimesheetIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(StartDateProperty, .GetValue(2))
        LoadProperty(EndDateProperty, .GetValue(3))
        LoadProperty(StartingHoursProperty, .GetDecimal(4))
        LoadProperty(StartingShiftsProperty, .GetInt32(5))
        LoadProperty(RequiredHoursProperty, .GetDecimal(6))
        LoadProperty(RequiredShiftsProperty, .GetInt32(7))
        LoadProperty(TotalHoursProperty, .GetDecimal(8))
        LoadProperty(TotalShiftsProperty, .GetInt32(9))
        LoadProperty(TotalOvertimeProperty, .GetDecimal(10))
        LoadProperty(TotalShortfallProperty, .GetDecimal(11))
        LoadProperty(TotalPublicHolidayHoursProperty, .GetDecimal(12))
        LoadProperty(TimesheetRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(ClosingHoursProperty, .GetDecimal(14))
        LoadProperty(ClosingShiftsProperty, .GetInt32(15))
        LoadProperty(IsClosedProperty, .GetBoolean(16))
        LoadProperty(IsClosedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(IsClosedDateTimeProperty, .GetValue(18))
        'RowNo = 19
        LoadProperty(ClosedByUserProperty, .GetString(20))
        LoadProperty(ClosedDateDescriptionProperty, .GetString(21))
      End With

    End Sub

#End Region

  End Class

End Namespace