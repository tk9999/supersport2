﻿' Generated 22 Jul 2016 08:55 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHRTimesheetRequirementStatList
    Inherits OBReadOnlyListBase(Of ROHRTimesheetRequirementStatList, ROHRTimesheetRequirementStat)

#Region " Business Methods "

    Public Function GetItem(ResourceID As Integer) As ROHRTimesheetRequirementStat

      For Each child As ROHRTimesheetRequirementStat In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property TimesheetRequirementID As Integer?
      Public Property AtDate As Date?

      Public Sub New()

      End Sub

      Public Sub New(HumanResourceID As Integer?,
                     TimesheetRequirementID As Integer?, AtDate As Date?)
        Me.HumanResourceID = HumanResourceID
        Me.TimesheetRequirementID = TimesheetRequirementID
        Me.AtDate = AtDate
      End Sub

    End Class

    Public Shared Function NewROHRTimesheetRequirementStatList() As ROHRTimesheetRequirementStatList

      Return New ROHRTimesheetRequirementStatList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROHRTimesheetRequirementStatList() As ROHRTimesheetRequirementStatList

      Return DataPortal.Fetch(Of ROHRTimesheetRequirementStatList)(New Criteria())

    End Function

    Public Shared Function GetROHRTimesheetRequirementStatList(HumanResourceID As Integer?,
                                                               TimesheetRequirementID As Integer?, AtDate As Date?) As ROHRTimesheetRequirementStatList

      Return DataPortal.Fetch(Of ROHRTimesheetRequirementStatList)(New Criteria(HumanResourceID,
                                                                                TimesheetRequirementID, AtDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHRTimesheetRequirementStat.GetROHRTimesheetRequirementStat(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceListTimesheetRequirementStats"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@TimesheetRequirementID", NothingDBNull(crit.TimesheetRequirementID))
            cm.Parameters.AddWithValue("@AtDate", NothingDBNull(crit.AtDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace