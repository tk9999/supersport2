﻿' Generated 26 Aug 2016 14:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceTimesheetDay
    Inherits OBReadOnlyBase(Of ROHumanResourceTimesheetDay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROHumanResourceTimesheetDayBO.ROHumanResourceTimesheetDayBOToString(self)")

    Public Shared HumanResourceTimesheetDayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceTimesheetDayID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceTimesheetDayID() As Integer
      Get
        Return GetProperty(HumanResourceTimesheetDayIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceTimesheetID, "Human Resource Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Timesheet value
    ''' </summary>
    <Display(Name:="Human Resource Timesheet", Description:="")>
    Public ReadOnly Property HumanResourceTimesheetID() As Integer?
      Get
        Return GetProperty(HumanResourceTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceTimesheetDayCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceTimesheetDayCategoryID, "Human Resource Timesheet Day Category", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Timesheet Day Category value
    ''' </summary>
    <Display(Name:="Human Resource Timesheet Day Category", Description:="")>
    Public ReadOnly Property HumanResourceTimesheetDayCategoryID() As Integer?
      Get
        Return GetProperty(HumanResourceTimesheetDayCategoryIDProperty)
      End Get
    End Property

    Public Shared TimesheetDayProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.TimesheetDay, "Timesheet Day")
    ''' <summary>
    ''' Gets the Timesheet Day value
    ''' </summary>
    <Display(Name:="Timesheet Day", Description:="")>
    Public ReadOnly Property TimesheetDay As Date
      Get
        Return GetProperty(TimesheetDayProperty)
      End Get
    End Property

    Public Shared TimesheetDayDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetDayDescription, "Timesheet Day Description", "")
    ''' <summary>
    ''' Gets the Timesheet Day Description value
    ''' </summary>
    <Display(Name:="Timesheet Day Description", Description:="")>
    Public ReadOnly Property TimesheetDayDescription() As String
      Get
        Return GetProperty(TimesheetDayDescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeSystemProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTimeSystem, "Start Date Time System")
    ''' <summary>
    ''' Gets the Start Date Time System value
    ''' </summary>
    <Display(Name:="Start Date Time System", Description:="")>
    Public ReadOnly Property StartDateTimeSystem As Date
      Get
        Return GetProperty(StartDateTimeSystemProperty)
      End Get
    End Property

    Public Shared EndDateTimeSystemProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTimeSystem, "End Date Time System")
    ''' <summary>
    ''' Gets the End Date Time System value
    ''' </summary>
    <Display(Name:="End Date Time System", Description:="")>
    Public ReadOnly Property EndDateTimeSystem As Date
      Get
        Return GetProperty(EndDateTimeSystemProperty)
      End Get
    End Property

    Public Shared StartDateTimeDisputeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeDispute, "Start Date Time Dispute")
    ''' <summary>
    ''' Gets the Start Date Time Dispute value
    ''' </summary>
    <Display(Name:="Start Date Time Dispute", Description:="")>
    Public ReadOnly Property StartDateTimeDispute As DateTime?
      Get
        Return GetProperty(StartDateTimeDisputeProperty)
      End Get
    End Property

    Public Shared EndDateTimeDisputeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeDispute, "End Date Time Dispute")
    ''' <summary>
    ''' Gets the End Date Time Dispute value
    ''' </summary>
    <Display(Name:="End Date Time Dispute", Description:="")>
    Public ReadOnly Property EndDateTimeDispute As DateTime?
      Get
        Return GetProperty(EndDateTimeDisputeProperty)
      End Get
    End Property

    Public Shared StartDateTimeManagerProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeManager, "Start Date Time Manager")
    ''' <summary>
    ''' Gets the Start Date Time Manager value
    ''' </summary>
    <Display(Name:="Start Date Time Manager", Description:="")>
    Public ReadOnly Property StartDateTimeManager As DateTime?
      Get
        Return GetProperty(StartDateTimeManagerProperty)
      End Get
    End Property

    Public Shared EndDateTimeManagerProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeManager, "End Date Time Manager")
    ''' <summary>
    ''' Gets the End Date Time Manager value
    ''' </summary>
    <Display(Name:="End Date Time Manager", Description:="")>
    Public ReadOnly Property EndDateTimeManager As DateTime?
      Get
        Return GetProperty(EndDateTimeManagerProperty)
      End Get
    End Property

    Public Shared NormalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.NormalHours, "Normal Hours", CDec(0))
    ''' <summary>
    ''' Gets the Normal Hours value
    ''' </summary>
    <Display(Name:="Normal Hours", Description:="")>
    Public ReadOnly Property NormalHours() As Decimal
      Get
        Return GetProperty(NormalHoursProperty)
      End Get
    End Property

    Public Shared WeekdayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekdayHours, "Weekday Hours", CDec(0))
    ''' <summary>
    ''' Gets the Weekday Hours value
    ''' </summary>
    <Display(Name:="Weekday Hours", Description:="")>
    Public ReadOnly Property WeekdayHours() As Decimal
      Get
        Return GetProperty(WeekdayHoursProperty)
      End Get
    End Property

    Public Shared WeekendHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekendHours, "Weekend Hours", CDec(0))
    ''' <summary>
    ''' Gets the Weekend Hours value
    ''' </summary>
    <Display(Name:="Weekend Hours", Description:="")>
    Public ReadOnly Property WeekendHours() As Decimal
      Get
        Return GetProperty(WeekendHoursProperty)
      End Get
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours", CDec(0))
    ''' <summary>
    ''' Gets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime Hours", Description:="")>
    Public ReadOnly Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
    End Property

    Public Shared PublicHolidayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PublicHolidayHours, "Public Holiday Hours", CDec(0))
    ''' <summary>
    ''' Gets the Public Holiday Hours value
    ''' </summary>
    <Display(Name:="Public Holiday Hours", Description:="")>
    Public ReadOnly Property PublicHolidayHours() As Decimal
      Get
        Return GetProperty(PublicHolidayHoursProperty)
      End Get
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours", CDec(0))
    ''' <summary>
    ''' Gets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall Hours", Description:="")>
    Public ReadOnly Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceTimesheetDayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TimesheetDayDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROHumanResourceTimesheetDay(dr As SafeDataReader) As ROHumanResourceTimesheetDay

      Dim r As New ROHumanResourceTimesheetDay()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceTimesheetDayIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, .GetInt32(1))
        LoadProperty(HumanResourceTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(HumanResourceTimesheetDayCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(TimesheetDayProperty, .GetValue(4))
        LoadProperty(TimesheetDayDescriptionProperty, .GetString(5))
        LoadProperty(StartDateTimeSystemProperty, .GetValue(6))
        LoadProperty(EndDateTimeSystemProperty, .GetValue(7))
        LoadProperty(StartDateTimeDisputeProperty, .GetValue(8))
        LoadProperty(EndDateTimeDisputeProperty, .GetValue(9))
        LoadProperty(StartDateTimeManagerProperty, .GetValue(10))
        LoadProperty(EndDateTimeManagerProperty, .GetValue(11))
        LoadProperty(NormalHoursProperty, .GetDecimal(12))
        LoadProperty(WeekdayHoursProperty, .GetDecimal(13))
        LoadProperty(WeekendHoursProperty, .GetDecimal(14))
        LoadProperty(OvertimeHoursProperty, .GetDecimal(15))
        LoadProperty(PublicHolidayHoursProperty, .GetDecimal(16))
        LoadProperty(ShortfallHoursProperty, .GetDecimal(17))
        LoadProperty(CreatedByProperty, .GetInt32(18))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(19))
        LoadProperty(ModifiedByProperty, .GetInt32(20))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(21))
      End With

    End Sub

#End Region

  End Class

End Namespace