﻿' Generated 22 Jul 2016 08:55 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHRTimesheetRequirementStat
    Inherits OBReadOnlyBase(Of ROHRTimesheetRequirementStat)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROHRTimesheetRequirementStatBO.ROHRTimesheetRequirementStatBOToString(self)")

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared TotalShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShifts, "Total Shifts")
    ''' <summary>
    ''' Gets the Total Shifts value
    ''' </summary>
    <Display(Name:="Total Shifts", Description:="")>
  Public ReadOnly Property TotalShifts() As Integer
      Get
        Return GetProperty(TotalShiftsProperty)
      End Get
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Total Hours")
    ''' <summary>
    ''' Gets the Total Hours value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
  Public ReadOnly Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
    End Property

    Public Shared RequiredShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredShifts, "Required Shifts")
    ''' <summary>
    ''' Gets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:="")>
  Public ReadOnly Property RequiredShifts() As Integer
      Get
        Return GetProperty(RequiredShiftsProperty)
      End Get
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredHours, "Required Hours")
    ''' <summary>
    ''' Gets the Required Hours value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
  Public ReadOnly Property RequiredHours() As Integer
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
    End Property

    Public Shared StartingBalanceShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartingBalanceShifts, "Starting Balance Shifts")
    ''' <summary>
    ''' Gets the Starting Balance Shifts value
    ''' </summary>
    <Display(Name:="Starting Balance Shifts", Description:="")>
  Public ReadOnly Property StartingBalanceShifts() As Integer
      Get
        Return GetProperty(StartingBalanceShiftsProperty)
      End Get
    End Property

    Public Shared StartingBalanceHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.StartingBalanceHours, "Starting Balance Hours")
    ''' <summary>
    ''' Gets the Starting Balance Hours value
    ''' </summary>
    <Display(Name:="Starting Balance Hours", Description:="")>
  Public ReadOnly Property StartingBalanceHours() As Decimal
      Get
        Return GetProperty(StartingBalanceHoursProperty)
      End Get
    End Property

    Public Shared TargetHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TargetHours, "Target Hours")
    ''' <summary>
    ''' Gets the Target Hours value
    ''' </summary>
    <Display(Name:="Target Hours", Description:="")>
  Public ReadOnly Property TargetHours() As Decimal
      Get
        Return GetProperty(TargetHoursProperty)
      End Get
    End Property

    Public Shared TargetShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TargetShifts, "Target Shifts")
    ''' <summary>
    ''' Gets the Target Shifts value
    ''' </summary>
    <Display(Name:="Target Shifts", Description:="")>
  Public ReadOnly Property TargetShifts() As Integer
      Get
        Return GetProperty(TargetShiftsProperty)
      End Get
    End Property

    Public Shared UtilPercProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.UtilPerc, "Util Perc")
    ''' <summary>
    ''' Gets the Util Perc value
    ''' </summary>
    <Display(Name:="Util Perc", Description:="")>
  Public ReadOnly Property UtilPerc() As Decimal
      Get
        Return GetProperty(UtilPercProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TotalHours.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROHRTimesheetRequirementStat(dr As SafeDataReader) As ROHRTimesheetRequirementStat

      Dim r As New ROHRTimesheetRequirementStat()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(TotalShiftsProperty, .GetInt32(2))
        LoadProperty(TotalHoursProperty, .GetDecimal(3))
        LoadProperty(RequiredShiftsProperty, .GetInt32(4))
        LoadProperty(RequiredHoursProperty, .GetInt32(5))
        LoadProperty(StartingBalanceShiftsProperty, .GetInt32(6))
        LoadProperty(StartingBalanceHoursProperty, .GetDecimal(7))
        LoadProperty(TargetHoursProperty, .GetDecimal(8))
        LoadProperty(TargetShiftsProperty, .GetInt32(9))
        LoadProperty(UtilPercProperty, .GetDecimal(10))
      End With

    End Sub

#End Region

  End Class

End Namespace