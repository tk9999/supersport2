﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetDayCategoryList
    Inherits OBBusinessListBase(Of HumanResourceTimesheetDayCategoryList, HumanResourceTimesheetDayCategory)

#Region " Business Methods "

    Public Function GetItem(HumanResourceTimesheetDayCategoryID As Integer) As HumanResourceTimesheetDayCategory

      For Each child As HumanResourceTimesheetDayCategory In Me
        If child.HumanResourceTimesheetDayCategoryID = HumanResourceTimesheetDayCategoryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Timesheet Day Categorys"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewHumanResourceTimesheetDayCategoryList() As HumanResourceTimesheetDayCategoryList

      Return New HumanResourceTimesheetDayCategoryList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetHumanResourceTimesheetDayCategoryList() As HumanResourceTimesheetDayCategoryList

      Return DataPortal.Fetch(Of HumanResourceTimesheetDayCategoryList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceTimesheetDayCategory.GetHumanResourceTimesheetDayCategory(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceTimesheetDayCategoryList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace