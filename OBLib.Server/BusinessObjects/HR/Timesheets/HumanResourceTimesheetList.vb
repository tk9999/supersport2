﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetList
    Inherits OBBusinessListBase(Of HumanResourceTimesheetList, HumanResourceTimesheet)

#Region " Business Methods "

    Public Function GetItem(HumanResourceTimesheetID As Integer) As HumanResourceTimesheet

      For Each child As HumanResourceTimesheet In Me
        If child.HumanResourceTimesheetID = HumanResourceTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Timesheets"

    End Function

    Public Function GetHumanResourceTimesheetDay(HumanResourceTimesheetDayID As Integer) As HumanResourceTimesheetDay

      Dim obj As HumanResourceTimesheetDay = Nothing
      For Each parent As HumanResourceTimesheet In Me
        obj = parent.HumanResourceTimesheetDayList.GetItem(HumanResourceTimesheetDayID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceTimesheetID As Integer?
      Public Property XmlIDs As String

      Public Sub New(HumanResourceTimesheetID As Integer?)
        Me.HumanResourceTimesheetID = HumanResourceTimesheetID
      End Sub

      Public Sub New(XmlIDs As String)
        Me.XmlIDs = XmlIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewHumanResourceTimesheetList() As HumanResourceTimesheetList

      Return New HumanResourceTimesheetList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetHumanResourceTimesheetList() As HumanResourceTimesheetList

      Return DataPortal.Fetch(Of HumanResourceTimesheetList)(New Criteria())

    End Function

    Public Shared Function GetHumanResourceTimesheetList(HumanResourceTimesheetID As Integer?) As HumanResourceTimesheetList

      Return DataPortal.Fetch(Of HumanResourceTimesheetList)(New Criteria(HumanResourceTimesheetID))

    End Function

    Public Shared Function GetHumanResourceTimesheetList(XmlIDs As String) As HumanResourceTimesheetList

      Return DataPortal.Fetch(Of HumanResourceTimesheetList)(New Criteria(XmlIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceTimesheet.GetHumanResourceTimesheet(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As HumanResourceTimesheet = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceTimesheetID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.HumanResourceTimesheetDayList.RaiseListChangedEvents = False
          parent.HumanResourceTimesheetDayList.Add(HumanResourceTimesheetDay.GetHumanResourceTimesheetDay(sdr))
          parent.HumanResourceTimesheetDayList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As HumanResourceTimesheetDay = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.HumanResourceTimesheetDayID <> sdr.GetInt32(1) Then
            parentChild = Me.GetHumanResourceTimesheetDay(sdr.GetInt32(1))
          End If
          parentChild.HumanResourceTimesheetDayDetailList.RaiseListChangedEvents = False
          parentChild.HumanResourceTimesheetDayDetailList.Add(HumanResourceTimesheetDayDetail.GetHumanResourceTimesheetDayDetail(sdr))
          parentChild.HumanResourceTimesheetDayDetailList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As HumanResourceTimesheet In Me
        child.CheckRules()
        For Each HumanResourceTimesheetDay As HumanResourceTimesheetDay In child.HumanResourceTimesheetDayList
          HumanResourceTimesheetDay.CheckRules()

          For Each HumanResourceTimesheetDayDetail As HumanResourceTimesheetDayDetail In HumanResourceTimesheetDay.HumanResourceTimesheetDayDetailList
            HumanResourceTimesheetDayDetail.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceTimesheetList"
            cm.Parameters.AddWithValue("@HumanResourceTimesheetiD", NothingDBNull(crit.HumanResourceTimesheetID))
            cm.Parameters.AddWithValue("@HumanResourceTimesheetiDs", NothingDBNull(crit.XmlIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace