﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Timesheets.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheet
    Inherits OBBusinessBase(Of HumanResourceTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceTimesheetBO.HumanResourceTimesheetBOToString(self)")

    Public Shared HumanResourceTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceTimesheetID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceTimesheetID() As Integer
      Get
        Return GetProperty(HumanResourceTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required"),
    SetExpression("HumanResourceTimesheetBO.StartDateSet(self)")>
    Public Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required"),
    SetExpression("HumanResourceTimesheetBO.EndDateSet(self)")>
    Public Property EndDate As Date
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared StartingHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.StartingHours, "Starting Hours", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Starting Hours value
    ''' </summary>
    <Display(Name:="Starting Hours", Description:=""),
    Required(ErrorMessage:="Starting Hours required")>
    Public Property StartingHours() As Decimal
      Get
        Return GetProperty(StartingHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(StartingHoursProperty, Value)
      End Set
    End Property

    Public Shared StartingShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartingShifts, "Starting Shifts", 0)
    ''' <summary>
    ''' Gets and sets the Starting Shifts value
    ''' </summary>
    <Display(Name:="Starting Shifts", Description:=""),
    Required(ErrorMessage:="Starting Shifts required")>
    Public Property StartingShifts() As Integer
      Get
        Return GetProperty(StartingShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartingShiftsProperty, Value)
      End Set
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Required Hours", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Required Hours value
    ''' </summary>
    <Display(Name:="Required Hours", Description:=""),
    Required(ErrorMessage:="Required Hours required")>
    Public Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursProperty, Value)
      End Set
    End Property

    Public Shared RequiredShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredShifts, "Required Shifts", 0)
    ''' <summary>
    ''' Gets and sets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:=""),
    Required(ErrorMessage:="Required Shifts required")>
    Public Property RequiredShifts() As Integer
      Get
        Return GetProperty(RequiredShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RequiredShiftsProperty, Value)
      End Set
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Total Hours", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Total Hours value
    ''' </summary>
    <Display(Name:="Total Hours", Description:=""),
    Required(ErrorMessage:="Total Hours required")>
    Public Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalHoursProperty, Value)
      End Set
    End Property

    Public Shared TotalShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShifts, "Total Shifts", 0)
    ''' <summary>
    ''' Gets and sets the Total Shifts value
    ''' </summary>
    <Display(Name:="Total Shifts", Description:=""),
    Required(ErrorMessage:="Total Shifts required")>
    Public Property TotalShifts() As Integer
      Get
        Return GetProperty(TotalShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TotalShiftsProperty, Value)
      End Set
    End Property

    Public Shared TotalOvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalOvertime, "Total Overtime", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Total Overtime value
    ''' </summary>
    <Display(Name:="Total Overtime", Description:=""),
    Required(ErrorMessage:="Total Overtime required")>
    Public Property TotalOvertime() As Decimal
      Get
        Return GetProperty(TotalOvertimeProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalOvertimeProperty, Value)
      End Set
    End Property

    Public Shared TotalShortfallProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalShortfall, "Total Shortfall", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Total Shortfall value
    ''' </summary>
    <Display(Name:="Total Shortfall", Description:=""),
    Required(ErrorMessage:="Total Shortfall required")>
    Public Property TotalShortfall() As Decimal
      Get
        Return GetProperty(TotalShortfallProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalShortfallProperty, Value)
      End Set
    End Property

    Public Shared TotalPublicHolidayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalPublicHolidayHours, "Total Public Holiday Hours", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Total Public Holiday Hours value
    ''' </summary>
    <Display(Name:="Total Public Holiday Hours", Description:=""),
    Required(ErrorMessage:="Total Public Holiday Hours required")>
    Public Property TotalPublicHolidayHours() As Decimal
      Get
        Return GetProperty(TotalPublicHolidayHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalPublicHolidayHoursProperty, Value)
      End Set
    End Property

    Public Shared TimesheetRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetRequirementID, "Requirement", Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Requirement", Description:=""),
    DropDownWeb(GetType(ROTimesheetRequirementList),
                BeforeFetchJS:="HumanResourceTimesheetBO.setTimesheetRequirementIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceTimesheetBO.triggerTimesheetRequirementIDAutoPopulate",
                AfterFetchJS:="HumanResourceTimesheetBO.afterTimesheetRequirementIDRefreshAjax",
                OnItemSelectJSFunction:="HumanResourceTimesheetBO.onTimesheetRequirementIDSelected",
                LookupMember:="TimesheetRequirement", DisplayMember:="TimesheetRequirement", ValueMember:="TimesheetRequirementID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"TimesheetRequirement", "System", "TeamName", "ClosedDescriptionShort"}),
    SetExpression("HumanResourceTimesheetBO.TimesheetRequirementIDSet(self)")>
    Public Property TimesheetRequirementID() As Integer?
      Get
        Return GetProperty(TimesheetRequirementIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TimesheetRequirementIDProperty, Value)
      End Set
    End Property

    Public Shared TimesheetRequirementProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetRequirement, "Requirement", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Requirement", Description:="")>
    Public Property TimesheetRequirement() As String
      Get
        Return GetProperty(TimesheetRequirementProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimesheetRequirementProperty, Value)
      End Set
    End Property

    Public Shared ClosingHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ClosingHours, "Closing Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the Closing Hours value
    ''' </summary>
    <Display(Name:="Closing Hours", Description:=""),
    Required(ErrorMessage:="Closing Hours required")>
    Public Property ClosingHours() As Decimal
      Get
        Return GetProperty(ClosingHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ClosingHoursProperty, Value)
      End Set
    End Property

    Public Shared ClosingShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClosingShifts, "Closing Shifts", 0)
    ''' <summary>
    ''' Gets and sets the Closing Shifts value
    ''' </summary>
    <Display(Name:="Closing Shifts", Description:=""),
    Required(ErrorMessage:="Closing Shifts required")>
    Public Property ClosingShifts() As Integer
      Get
        Return GetProperty(ClosingShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ClosingShiftsProperty, Value)
      End Set
    End Property

    Public Shared IsClosedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsClosed, "Is Closed", False)
    ''' <summary>
    ''' Gets and sets the Is Closed value
    ''' </summary>
    <Display(Name:="Is Closed", Description:=""),
    Required(ErrorMessage:="Is Closed required")>
    Public Property IsClosed() As Boolean
      Get
        Return GetProperty(IsClosedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsClosedProperty, Value)
      End Set
    End Property

    Public Shared IsClosedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IsClosedByUserID, "Is Closed By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Closed By User value
    ''' </summary>
    <Display(Name:="Is Closed By User", Description:="")>
    Public Property IsClosedByUserID() As Integer?
      Get
        Return GetProperty(IsClosedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IsClosedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared IsClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.IsClosedDateTime, "Is Closed Date Time")
    ''' <summary>
    ''' Gets and sets the Is Closed Date Time value
    ''' </summary>
    <Display(Name:="Is Closed Date Time", Description:="")>
    Public Property IsClosedDateTime As DateTime?
      Get
        Return GetProperty(IsClosedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(IsClosedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ClashingDaysCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashingDaysCount, "ClashingDaysCount", 0)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="ClashingDaysCount", Description:="")>
    Public Property ClashingDaysCount() As Integer
      Get
        Return GetProperty(ClashingDaysCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ClashingDaysCountProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared HumanResourceTimesheetDayListProperty As PropertyInfo(Of HumanResourceTimesheetDayList) = RegisterProperty(Of HumanResourceTimesheetDayList)(Function(c) c.HumanResourceTimesheetDayList, "Human Resource Timesheet Day List")

    Public ReadOnly Property HumanResourceTimesheetDayList() As HumanResourceTimesheetDayList
      Get
        If GetProperty(HumanResourceTimesheetDayListProperty) Is Nothing Then
          LoadProperty(HumanResourceTimesheetDayListProperty, HR.Timesheets.HumanResourceTimesheetDayList.NewHumanResourceTimesheetDayList())
        End If
        Return GetProperty(HumanResourceTimesheetDayListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceTimesheetID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Timesheet")
        Else
          Return String.Format("Blank {0}", "Human Resource Timesheet")
        End If
      Else
        Return Me.HumanResourceTimesheetID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"HumanResourceTimesheetDays"}
      End Get
    End Property

    'Public Function GetClashes() As Integer

    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ClashingDaysCountProperty)
        .JavascriptRuleFunctionName = "HumanResourceTimesheetBO.ClashingDaysCountValid"
        .ServerRuleFunction = AddressOf ClashingDaysCountValid
      End With

    End Sub

    Public Shared Function ClashingDaysCountValid(HumanResourceTimesheet As HumanResourceTimesheet) As String

      If HumanResourceTimesheet.ClashingDaysCount > 0 Then
        Return "This timesheet has days which overlap with another timesheet"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceTimesheet() method.

    End Sub

    Public Shared Function NewHumanResourceTimesheet() As HumanResourceTimesheet

      Return DataPortal.CreateChild(Of HumanResourceTimesheet)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetHumanResourceTimesheet(dr As SafeDataReader) As HumanResourceTimesheet

      Dim h As New HumanResourceTimesheet()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceTimesheetIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(StartDateProperty, .GetValue(2))
          LoadProperty(EndDateProperty, .GetValue(3))
          LoadProperty(StartingHoursProperty, .GetDecimal(4))
          LoadProperty(StartingShiftsProperty, .GetInt32(5))
          LoadProperty(RequiredHoursProperty, .GetDecimal(6))
          LoadProperty(RequiredShiftsProperty, .GetInt32(7))
          LoadProperty(TotalHoursProperty, .GetDecimal(8))
          LoadProperty(TotalShiftsProperty, .GetInt32(9))
          LoadProperty(TotalOvertimeProperty, .GetDecimal(10))
          LoadProperty(TotalShortfallProperty, .GetDecimal(11))
          LoadProperty(TotalPublicHolidayHoursProperty, .GetDecimal(12))
          LoadProperty(TimesheetRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ClosingHoursProperty, .GetDecimal(14))
          LoadProperty(ClosingShiftsProperty, .GetInt32(15))
          LoadProperty(IsClosedProperty, .GetBoolean(16))
          LoadProperty(IsClosedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(IsClosedDateTimeProperty, .GetValue(18))
          LoadProperty(HumanResourceProperty, .GetString(19))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceTimesheetIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@StartDate", StartDate)
      cm.Parameters.AddWithValue("@EndDate", EndDate)
      cm.Parameters.AddWithValue("@StartingHours", GetProperty(StartingHoursProperty))
      cm.Parameters.AddWithValue("@StartingShifts", GetProperty(StartingShiftsProperty))
      cm.Parameters.AddWithValue("@RequiredHours", GetProperty(RequiredHoursProperty))
      cm.Parameters.AddWithValue("@RequiredShifts", GetProperty(RequiredShiftsProperty))
      cm.Parameters.AddWithValue("@TotalHours", GetProperty(TotalHoursProperty))
      cm.Parameters.AddWithValue("@TotalShifts", GetProperty(TotalShiftsProperty))
      cm.Parameters.AddWithValue("@TotalOvertime", GetProperty(TotalOvertimeProperty))
      cm.Parameters.AddWithValue("@TotalShortfall", GetProperty(TotalShortfallProperty))
      cm.Parameters.AddWithValue("@TotalPublicHolidayHours", GetProperty(TotalPublicHolidayHoursProperty))
      cm.Parameters.AddWithValue("@TimesheetRequirementID", Singular.Misc.NothingDBNull(GetProperty(TimesheetRequirementIDProperty)))
      cm.Parameters.AddWithValue("@ClosingHours", GetProperty(ClosingHoursProperty))
      cm.Parameters.AddWithValue("@ClosingShifts", GetProperty(ClosingShiftsProperty))
      cm.Parameters.AddWithValue("@IsClosed", GetProperty(IsClosedProperty))
      cm.Parameters.AddWithValue("@IsClosedByUserID", Singular.Misc.NothingDBNull(GetProperty(IsClosedByUserIDProperty)))
      cm.Parameters.AddWithValue("@IsClosedDateTime", Singular.Misc.NothingDBNull(IsClosedDateTime))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceTimesheetIDProperty, cm.Parameters("@HumanResourceTimesheetID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(HumanResourceTimesheetDayListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceTimesheetID", GetProperty(HumanResourceTimesheetIDProperty))
    End Sub

#End Region

  End Class

End Namespace