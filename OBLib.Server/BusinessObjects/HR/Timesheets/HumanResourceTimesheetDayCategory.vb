﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetDayCategory
    Inherits OBBusinessBase(Of HumanResourceTimesheetDayCategory)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceTimesheetDayCategoryBO.HumanResourceTimesheetDayCategoryBOToString(self)")

    Public Shared HumanResourceTimesheetDayCategoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceTimesheetDayCategoryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceTimesheetDayCategoryID() As Integer
      Get
        Return GetProperty(HumanResourceTimesheetDayCategoryIDProperty)
      End Get
    End Property

    Public Shared HumanResourceTimesheetDayCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceTimesheetDayCategory, "Human Resource Timesheet Day Category", "")
    ''' <summary>
    ''' Gets and sets the Human Resource Timesheet Day Category value
    ''' </summary>
    <Display(Name:="Human Resource Timesheet Day Category", Description:=""),
    StringLength(150, ErrorMessage:="Human Resource Timesheet Day Category cannot be more than 150 characters")>
    Public Property HumanResourceTimesheetDayCategory() As String
      Get
        Return GetProperty(HumanResourceTimesheetDayCategoryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceTimesheetDayCategoryProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceTimesheetDayCategoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceTimesheetDayCategory.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Timesheet Day Category")
        Else
          Return String.Format("Blank {0}", "Human Resource Timesheet Day Category")
        End If
      Else
        Return Me.HumanResourceTimesheetDayCategory
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceTimesheetDayCategory() method.

    End Sub

    Public Shared Function NewHumanResourceTimesheetDayCategory() As HumanResourceTimesheetDayCategory

      Return DataPortal.CreateChild(Of HumanResourceTimesheetDayCategory)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetHumanResourceTimesheetDayCategory(dr As SafeDataReader) As HumanResourceTimesheetDayCategory

      Dim h As New HumanResourceTimesheetDayCategory()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceTimesheetDayCategoryIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceTimesheetDayCategoryProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceTimesheetDayCategoryIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceTimesheetDayCategory", GetProperty(HumanResourceTimesheetDayCategoryProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceTimesheetDayCategoryIDProperty, cm.Parameters("@HumanResourceTimesheetDayCategoryID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceTimesheetDayCategoryID", GetProperty(HumanResourceTimesheetDayCategoryIDProperty))
    End Sub

#End Region

  End Class

End Namespace