﻿' Generated 22 Jul 2016 13:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace HR.Timesheets

  <Serializable()> _
  Public Class HumanResourceTimesheetDayList
    Inherits OBBusinessListBase(Of HumanResourceTimesheetDayList, HumanResourceTimesheetDay)

#Region " Business Methods "

    Public Function GetItem(HumanResourceTimesheetDayID As Integer) As HumanResourceTimesheetDay

      For Each child As HumanResourceTimesheetDay In Me
        If child.HumanResourceTimesheetDayID = HumanResourceTimesheetDayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Timesheet Days"

    End Function

    Public Function GetHumanResourceTimesheetDayDetail(HumanResourceTimesheetDayDetailID As Integer) As HumanResourceTimesheetDayDetail

      Dim obj As HumanResourceTimesheetDayDetail = Nothing
      For Each parent As HumanResourceTimesheetDay In Me
        obj = parent.HumanResourceTimesheetDayDetailList.GetItem(HumanResourceTimesheetDayDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewHumanResourceTimesheetDayList() As HumanResourceTimesheetDayList

      Return New HumanResourceTimesheetDayList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace