﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class HumanResourceOffPeriod
    Inherits OBBusinessBase(Of HumanResourceOffPeriod)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceOffPeriodBO.HumanResourceOffPeriodBOToString(self)")

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceOffPeriodID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceOffPeriodID() As Integer
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared OffReasonIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OffReasonID, Nothing) _
                                                                     .AddSetExpression("HumanResourceOffPeriodBO.OffReasonIDSet(self)")
    ''' <summary>
    ''' Gets and sets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="The reason the human resource was off"),
    Required(ErrorMessage:="Off Reason required"),
    DropDownWeb(GetType(ROSystemProductionAreaOffReasonList),
                BeforeFetchJS:="HumanResourceOffPeriodBO.setOffReasonIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceOffPeriodBO.triggerOffReasonIDAutoPopulate",
                AfterFetchJS:="HumanResourceOffPeriodBO.afterOffReasonIDRefreshAjax",
                OnItemSelectJSFunction:="HumanResourceOffPeriodBO.onOffReasonIDSelected",
                LookupMember:="OffReason", DisplayMember:="OffReason", ValueMember:="OffReasonID",
                DropDownCssClass:="hr-offreason", DropDownColumns:={"OffReason"})>
    Public Property OffReasonID() As Integer?
      Get
        Return GetProperty(OffReasonIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OffReasonIDProperty, Value)
      End Set
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.OffReason, "")
    ''' <summary>
    ''' Gets and sets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason")>
    Public Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OffReasonProperty, Value)
      End Set
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Detail", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="Description of the off period"),
    StringLength(200, ErrorMessage:="Detail cannot be more than 200 characters")>
    Public Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing) _
                                                                      .AddSetExpression("HumanResourceOffPeriodBO.StartDateSet(self)")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The start date for the off period"),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing) _
                                                                      .AddSetExpression("HumanResourceOffPeriodBO.EndDateSet(self)")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The end date for the off period"),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.AuthorisedID, CInt(CommonData.Enums.OffPeriodAuthorisationType.Pending)) _
                                                                      .AddSetExpression("HumanResourceOffPeriodBO.AuthorisedIDSet(self)")
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Authorised", Description:="Pending, Authorised or Rejected"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.CommonData.Enums.OffPeriodAuthorisationType), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property AuthorisedID() As Integer
      Get
        Return GetProperty(AuthorisedIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AuthorisedIDProperty, Value)
        SetAuthorisedBy(Value)
      End Set
    End Property

    Public Shared AuthorisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedBy, "Authorised By", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By value
    ''' </summary>
    <Display(Name:="Authorised By", Description:="Person who authorised the leave")>
    Public ReadOnly Property AuthorisedBy() As Integer?
      Get
        Return GetProperty(AuthorisedByProperty)
      End Get
    End Property

    Public Shared AuthorisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDate, "Authorised Date")
    ''' <summary>
    ''' Gets and sets the Authorised Date value
    ''' </summary>
    <Display(Name:="Authorised Date", Description:="Date and time the leave was authorised")>
    Public ReadOnly Property AuthorisedDate As DateTime?
      Get
        Return GetProperty(AuthorisedDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared AuthorisedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AuthorisedByName, "Authorised By Name", "")
    ''' <summary>
    ''' Gets the Authorised By Name value
    ''' </summary>
    <Display(Name:="Authorised By", Description:="Authorised By Name")>
    Public Property AuthorisedByName() As String
      Get
        Return GetProperty(AuthorisedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(AuthorisedByNameProperty, value)
      End Set
    End Property

    <Browsable(False)>
    Public Property OriginalStartDate As DateTime?
    <Browsable(False)>
    Public Property OriginalEndDate As DateTime?
    <Browsable(False)>
    Public Property OriginalAuthoriseID As Integer?

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
    <Display(Name:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    <Display(Name:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsFreelancer, False)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public ReadOnly Property IsFreelancer() As Boolean
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceIDProperty, value)
      End Set
    End Property

    Public Shared CanAuthoriseLeaveProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CanAuthoriseLeave, False)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public ReadOnly Property CanAuthoriseLeave() As Boolean
      Get
        Return GetProperty(CanAuthoriseLeaveProperty)
      End Get
    End Property

    Public Shared AuthorisedDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AuthorisedDateString, "Authorised Date", "")
    ''' <summary>
    ''' Gets the Authorised By Name value
    ''' </summary>
    <Display(Name:="Authorised Date", Description:="Authorised Date")>
    Public Property AuthorisedDateString() As String
      Get
        Return GetProperty(AuthorisedDateStringProperty)
      End Get
      Set(value As String)
        SetProperty(AuthorisedDateStringProperty, value)
      End Set
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "ManagerHumanResourceID", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ManagerHumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceBookingIDProperty, value)
      End Set
    End Property

    'Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Booking Description", "")
    ' ''' <summary>
    ' ''' Gets and sets the Resource Booking Description value
    ' ''' </summary>
    '<Display(Name:="Booking Description", Description:=""),
    'StringLength(500, ErrorMessage:="Booking Description cannot be more than 500 characters"),
    'Required(AllowEmptyStrings:=False, ErrorMessage:="Booking Description is required")>
    'Public Property ResourceBookingDescription() As String
    '  Get
    '    Return GetProperty(ResourceBookingDescriptionProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(ResourceBookingDescriptionProperty, Value)
    '  End Set
    'End Property

    'Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "StatusCssClass", "")
    ' ''' <summary>
    ' ''' Gets and sets the Can Move value
    ' ''' </summary>
    '<Display(Name:="StatusCssClass", Description:="")>
    'Public Property StatusCssClass() As String
    '  Get
    '    Return GetProperty(StatusCssClassProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(StatusCssClassProperty, Value)
    '  End Set
    'End Property

    'Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IgnoreClashes, False)
    ' ''' <summary>
    ' ''' Gets and sets the Can Move value
    ' ''' </summary>
    '<Display(Name:="IgnoreClashes", Description:="")>
    'Public Property IgnoreClashes() As Boolean
    '  Get
    '    Return GetProperty(IgnoreClashesProperty)
    '  End Get
    '  Set(ByVal Value As Boolean)
    '    SetProperty(IgnoreClashesProperty, Value)
    '  End Set
    'End Property

    'Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "IgnoreClashesReason", "")
    ' ''' <summary>
    ' ''' Gets and sets the Can Move value
    ' ''' </summary>
    '<Display(Name:="StatusCssClass", Description:="")>
    'Public Property IgnoreClashesReason() As String
    '  Get
    '    Return GetProperty(IgnoreClashesReasonProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(IgnoreClashesReasonProperty, Value)
    '  End Set
    'End Property

    Public Shared IsForNegativeHoursProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsForNegativeHours, False)
    ''' <summary>
    ''' Gets and sets the Is For Negative Hours value
    ''' </summary>
    <Display(Name:="Negative Hours Replacement?", Description:="")>
    Public Property IsForNegativeHours() As Boolean
      Get
        Return GetProperty(IsForNegativeHoursProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsForNegativeHoursProperty, Value)
      End Set
    End Property

    Public Shared SetupCompletedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.SetupCompleted, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="SetupCompleted", Description:="")>
    Public Property SetupCompleted() As Boolean
      Get
        Return GetProperty(SetupCompletedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SetupCompletedProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared HumanResourceOffPeriodDayListProperty As PropertyInfo(Of HumanResourceOffPeriodDayList) = RegisterProperty(Of HumanResourceOffPeriodDayList)(Function(c) c.HumanResourceOffPeriodDayList, "OffPeriod Booking List")
    Public ReadOnly Property HumanResourceOffPeriodDayList() As HumanResourceOffPeriodDayList
      Get
        If GetProperty(HumanResourceOffPeriodDayListProperty) Is Nothing Then
          LoadProperty(HumanResourceOffPeriodDayListProperty, New HumanResourceOffPeriodDayList)
        End If
        Return GetProperty(HumanResourceOffPeriodDayListProperty)
      End Get
    End Property

    Public Shared ROOtherDisciplinesOnLeaveListProperty As PropertyInfo(Of ROOtherDisciplinesOnLeaveList) = RegisterProperty(Of ROOtherDisciplinesOnLeaveList)(Function(c) c.ROOtherDisciplinesOnLeaveList, "OffPeriod Booking List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROOtherDisciplinesOnLeaveList() As ROOtherDisciplinesOnLeaveList
      Get
        If GetProperty(ROOtherDisciplinesOnLeaveListProperty) Is Nothing Then
          LoadProperty(ROOtherDisciplinesOnLeaveListProperty, New ROOtherDisciplinesOnLeaveList)
        End If
        Return GetProperty(ROOtherDisciplinesOnLeaveListProperty)
      End Get
    End Property

    Public Shared ROOffPeriodClashListProperty As PropertyInfo(Of ROOffPeriodClashList) = RegisterProperty(Of ROOffPeriodClashList)(Function(c) c.ROOffPeriodClashList, "OffPeriod Booking List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROOffPeriodClashList() As ROOffPeriodClashList
      Get
        If GetProperty(ROOffPeriodClashListProperty) Is Nothing Then
          LoadProperty(ROOffPeriodClashListProperty, New ROOffPeriodClashList)
        End If
        Return GetProperty(ROOffPeriodClashListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public ReadOnly Property IsAuthorsied As Boolean
      Get
        If CompareSafe(Me.AuthorisedID, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Authorised, Integer)) Then
          Return True
        End If
        Return False
      End Get
    End Property

    'Public Function GetParent() As HumanResource

    '  Return CType(CType(Me.Parent, HumanResourceOffPeriodList).Parent, HumanResource)

    'End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceOffPeriodIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Detail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Off Period")
        Else
          Return String.Format("Blank {0}", "Human Resource Off Period")
        End If
      Else
        Return Me.Detail
      End If

    End Function

    Private Sub SetAuthorisedBy(Value As Integer)

      If CompareSafe(Value, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Authorised, Integer)) Then
        SetProperty(AuthorisedByProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(AuthorisedDateProperty, Now)
      ElseIf CompareSafe(Value, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Pending, Integer)) Then
        SetProperty(AuthorisedByProperty, Nothing)
        SetProperty(AuthorisedDateProperty, Nothing)
      ElseIf CompareSafe(Value, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Rejected, Integer)) Then
        SetProperty(AuthorisedByProperty, Nothing)
        SetProperty(AuthorisedDateProperty, Nothing)
      End If

    End Sub

    'Private Sub CheckOffPeriodClashes()

    '  'Getting ProductionIDs with which the person's leave is clashing with
    '  Dim cmd As New Singular.CommandProc("CmdProcs.cmdGetOffPeriodClashingProductions", _
    '                                      New String() {"HumanResourceID",
    '                                                    "StartDateTime",
    '                                                    "EndDateTime"
    '                                                    }, _
    '                                      New Object() {GetParent.HumanResourceID,
    '                                                    StartDate,
    '                                                    EndDate}
    '                                                    )

    '  cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '  cmd = cmd.Execute()

    '  'If there is a clash... 
    '  If cmd.Dataset IsNot Nothing Then
    '    If cmd.Dataset.Tables(0).Rows.Count > 0 Then

    '      Dim OffPeriodClash As Emails.AnnualLeaveClash = New Emails.AnnualLeaveClash(cmd.Dataset, GetParent, StartDate, EndDate, True, AuthorisedID)
    '      For Each Email As Singular.Emails.Email In OffPeriodClash.EmailList
    '        Me.EmailList.Add(Email)
    '      Next

    '    End If
    '  End If

    '  'Getting Human Resources with which the person's leave is clashing with in the same discipline
    '  Dim cmdD = New Singular.CommandProc("CmdProcs.[cmdGetOffPeriodSameDisciplineHRClashes]", _
    '                                      New String() {"HumanResourceID",
    '                                                    "StartDateTime",
    '                                                    "EndDateTime"
    '                                                    }, _
    '                                      New Object() {GetParent.HumanResourceID,
    '                                                    StartDate,
    '                                                    EndDate}
    '                                                    )

    '  cmdD.CommandType = CommandType.StoredProcedure
    '  cmdD.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '  cmdD = cmdD.Execute()

    '  'If there is a clash...
    '  If cmdD.Dataset IsNot Nothing Then
    '    If cmdD.Dataset.Tables(0).Rows.Count > 0 Then

    '      'Need to count the number of recipients who should get this email - If none, don't bother (makes saving timeout everytime you build the email for mulitple leave records)
    '      Dim count As Integer = 0
    '      For Each HRSystem As HR.HumanResourceSystem In GetParent.HumanResourceSystemList.Where(Function(c) c.SystemID <> 7)
    '        Dim System As Maintenance.Company.System = CommonData.Lists.CompanyList.GetSystem(HRSystem.SystemID)
    '        count += System.SystemEmailList.Where(Function(d) CompareSafe(d.SystemEmailTypeID, CType(CommonData.Enums.SystemEmailTypes.DisciplineLeaveOverlaps, Integer))).Count
    '      Next

    '      If count > 0 Then
    '        Dim OffPeriodClash As Emails.AnnualLeaveClash = New Emails.AnnualLeaveClash(cmdD.Dataset, GetParent, StartDate, EndDate, False, AuthorisedID)
    '        For Each Email As Singular.Emails.Email In OffPeriodClash.EmailList
    '          Me.EmailList.Add(Email)
    '        Next
    '      End If

    '    End If
    '  End If

    'End Sub

#End Region

#End Region

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ResourceBookings", "CrewScheduleDetails"}
      End Get
    End Property

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(DetailProperty)
        .JavascriptRuleFunctionName = "HumanResourceOffPeriodBO.OffReasonDetailRequired"
        .ServerRuleFunction = AddressOf OffReasonDetailRequired
      End With

      With AddWebRule(OffReasonIDProperty)
        .AddTriggerProperties(StartDateProperty, EndDateProperty)
        .JavascriptRuleFunctionName = "HumanResourceOffPeriodBO.OffReasonIDValid"
        .ServerRuleFunction = AddressOf OffReasonIDValid
      End With

      'With AddWebRule(StartDateProperty, Singular.Rules.RuleSeverity.Warning)
      '  .AddTriggerProperties(EndDateProperty, AuthorisedIDProperty)
      '  .ASyncBusyText = "Checking Off Reason..."
      '  .ServerRuleFunction = AddressOf CheckAvailabilty
      'End With

      'With AddWebRule(StartDateProperty, Singular.Rules.RuleSeverity.Warning)
      '  .AddTriggerProperties(EndDateProperty, AuthorisedIDProperty)
      '  .ASyncBusyText = "Checking Availabilty Within Discipline..."
      '  .ServerRuleFunction = AddressOf CheckAvailabiltyWithinDiscipline
      'End With

    End Sub

    Public Shared Function OffReasonDetailRequired(hrop As HumanResourceOffPeriod) As String
      Dim ErrorString = ""
      If hrop.OffReasonID > 0 Then
        Dim offReason = CommonData.Lists.ROOffReasonList.GetItem(hrop.OffReasonID)
        If offReason IsNot Nothing Then
          If offReason.DetailRequiredInd AndAlso hrop.Detail.Trim = "" Then
            ErrorString = "Detail required for selected Off Reason"
          End If
        End If
      End If
      Return ErrorString
    End Function

    Public Shared Function OffReasonIDValid(hrop As HumanResourceOffPeriod) As String
      Dim ErrorString = ""
      If hrop.IsNew Then
        Dim offReason = CommonData.Lists.ROOffReasonList.GetItem(hrop.OffReasonID)
        If hrop.IsFreelancer AndAlso Not offReason.FreelancerInd Then
          ErrorString = "Selected Off Reason only applicable to Human Resources that are not Freelancers"
        ElseIf Not hrop.IsFreelancer AndAlso offReason.FreelancerInd Then
          ErrorString = "Selected Off Reason only applicable to Human Resources that are Freelancers"
        End If
      End If
      Return ErrorString
    End Function

    'Private Shared Function CheckAvailabilty(hrop As HumanResourceOffPeriod) As String

    '  If Not IsNullNothing(hrop.Parent) Then
    '    If hrop.StartDate IsNot Nothing AndAlso hrop.EndDate IsNot Nothing Then
    '      If Not (Not hrop.IsNew AndAlso Not hrop.IsDirty) Then 'Don't check old records that are not dirty

    '        If CompareSafe(hrop.AuthorisedID, CType(CommonData.Enums.OffPeriodAuthorisationType.Authorised, Integer)) Then

    '          Dim cmd As New Singular.CommandProc("CmdProcs.cmdCheckProductionOffPeriodClashes", _
    '                                                New String() {"HumanResourceID",
    '                                                              "StartDateTime",
    '                                                              "EndDateTime"}, _
    '                                                New Object() {hrop.GetParent.HumanResourceID,
    '                                                              hrop.StartDate.Value,
    '                                                              hrop.EndDate.Value})
    '          cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '          cmd = cmd.Execute()

    '          If cmd.Dataset.Tables(0).Rows.Count > 0 Then
    '            Dim ErrorMessage As String = ""
    '            For Each row As DataRow In cmd.Dataset.Tables(0).Rows
    '              If ErrorMessage = "" Then
    '                ErrorMessage &= row("Title")
    '              Else
    '                ErrorMessage &= vbCrLf & row("Title")
    '              End If
    '            Next

    '            If ErrorMessage.Trim.Length = 0 Then
    '              ErrorMessage = ""
    '            End If

    '            Return ErrorMessage
    '          End If
    '        End If
    '      End If
    '    End If
    '  End If

    '  Return ""

    'End Function

    'Private Shared Function CheckAvailabiltyWithinDiscipline(hrop As HumanResourceOffPeriod) As String

    '  If OBLib.Security.Settings.CurrentUser.SystemID = CommonData.Enums.System.ProductionContent AndAlso _
    '     OBLib.Security.Settings.CurrentUser.ProductionAreaID = CommonData.Enums.ProductionArea.OB Then
    '    If Not IsNullNothing(hrop.Parent) Then
    '      If hrop.StartDate IsNot Nothing AndAlso hrop.EndDate IsNot Nothing Then
    '        If hrop.IsDirty Then 'Don't check old records that are not dirty

    '          If CompareSafe(hrop.AuthorisedID, CType(CommonData.Enums.OffPeriodAuthorisationType.Authorised, Integer)) Then

    '            Dim cmd As New Singular.CommandProc("CmdProcs.cmdGetOffPeriodSameDisciplineHRClashes", _
    '                                                  New String() {"HumanResourceID",
    '                                                                "StartDateTime",
    '                                                                "EndDateTime"}, _
    '                                                  New Object() {hrop.GetParent.HumanResourceID,
    '                                                                hrop.StartDate.Value,
    '                                                                hrop.EndDate.Value})
    '            cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '            cmd = cmd.Execute()

    '            If cmd.Dataset.Tables(0).Rows.Count > 0 Then
    '              Dim ErrorMessage As String = "Clashes Within Discipline(s): "
    '              For Each row As DataRow In cmd.Dataset.Tables(0).Rows
    '                If ErrorMessage = "" Then
    '                  ErrorMessage &= row("Discipline") & ": " & row("HumanResource")
    '                Else
    '                  ErrorMessage &= vbCrLf & row("Discipline") & ": " & row("HumanResource")
    '                End If
    '              Next

    '              If ErrorMessage.Trim.Length = 0 Then
    '                ErrorMessage = ""
    '              End If

    '              Return ErrorMessage
    '            End If
    '          End If
    '        End If
    '      End If
    '    End If
    '  End If

    '  Return ""

    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceOffPeriod() method.

    End Sub

    Public Shared Function NewHumanResourceOffPeriod() As HumanResourceOffPeriod

      Return DataPortal.CreateChild(Of HumanResourceOffPeriod)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceOffPeriod(dr As SafeDataReader) As HumanResourceOffPeriod

      Dim h As New HumanResourceOffPeriod()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceOffPeriodIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(OffReasonIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DetailProperty, .GetString(3))
          LoadProperty(StartDateProperty, .GetValue(4))
          LoadProperty(EndDateProperty, .GetValue(5))
          LoadProperty(AuthorisedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(AuthorisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(AuthorisedDateProperty, .GetValue(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(AuthorisedByNameProperty, .GetString(13))
          LoadProperty(SystemIDProperty, .GetInt32(14))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(15))
          LoadProperty(IsFreelancerProperty, .GetBoolean(16))
          LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(CanAuthoriseLeaveProperty, .GetBoolean(18))
          LoadProperty(ResourceBookingIDProperty, .GetInt32(19))
          'LoadProperty(ResourceBookingDescriptionProperty, .GetString(20))
          'LoadProperty(StatusCssClassProperty, .GetString(21))
          'LoadProperty(IgnoreClashesProperty, .GetBoolean(22))
          'LoadProperty(IgnoreClashesReasonProperty, .GetString(23))
          LoadProperty(OffReasonProperty, .GetString(24))
          LoadProperty(IsForNegativeHoursProperty, .GetBoolean(25))
          LoadProperty(SetupCompletedProperty, True)
        End With
      End Using

      If AuthorisedDate IsNot Nothing Then
        AuthorisedDateString = AuthorisedDate.Value.ToString("ddd dd MMM yyyy HH:mm")
      End If

      OriginalStartDate = StartDate
      OriginalEndDate = EndDate
      OriginalAuthoriseID = AuthorisedID

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceOffPeriod"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceOffPeriod"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceOffPeriodID As SqlParameter = .Parameters.Add("@HumanResourceOffPeriodID", SqlDbType.Int)
          paramHumanResourceOffPeriodID.Value = GetProperty(HumanResourceOffPeriodIDProperty)
          If Me.IsNew Then
            paramHumanResourceOffPeriodID.Direction = ParameterDirection.Output
          End If
          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", Me.HumanResourceID)
          .Parameters.AddWithValue("@OffReasonID", GetProperty(OffReasonIDProperty))
          .Parameters.AddWithValue("@Detail", GetProperty(DetailProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@AuthorisedID", GetProperty(AuthorisedIDProperty))
          .Parameters.AddWithValue("@AuthorisedBy", NothingDBNull(GetProperty(AuthorisedByProperty)))
          .Parameters.AddWithValue("@AuthorisedDate", (New SmartDate(GetProperty(AuthorisedDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@CanAuthoriseLeave", False) 'Me.GetParent.CanAuthoriseLeave
          .Parameters.AddWithValue("@ResourceID", Me.ResourceID)
          '.Parameters.AddWithValue("@OffReason", GetProperty(OffReasonProperty))
          .Parameters.AddWithValue("@IsForNegativeHours", Me.IsForNegativeHours)
          '.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
          '.Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
          '.Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
          '.Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceOffPeriodIDProperty, paramHumanResourceOffPeriodID.Value)
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
            'CheckOffPeriodClashes()
            'ElseIf Not Singular.Misc.CompareSafe(OriginalStartDate, StartDate) OrElse Not Singular.Misc.CompareSafe(OriginalEndDate, EndDate) OrElse Not Singular.Misc.CompareSafe(OriginalAuthoriseID, AuthorisedID) Then
            '  CheckOffPeriodClashes()
            '  'Need to change original Dates to new saved date, otherwise Original dates will be the same from when it was fetched from the DB
            '  OriginalStartDate = StartDate
            '  OriginalEndDate = EndDate
            '  OriginalAuthoriseID = AuthorisedID
          End If

          Me.HumanResourceOffPeriodDayList.Update()
          '' update child objects
          'Me.EmailList.Save()
          '' Need to clear the list otherwise Email List with same data will be sent again
          'Me.EmailList.Clear()
          MarkOld()
        End With
      Else
        Me.HumanResourceOffPeriodDayList.Update()
      End If

    End Sub

    Friend Sub DeleteSelf()

      'For Each rb As OffPeriodBooking In Me.OffPeriodBookingList
      '  rb.DeleteSelf()
      'Next

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceOffPeriod"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", GetProperty(HumanResourceOffPeriodIDProperty))
        cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Private Sub CheckAvailabilty()
      Throw New NotImplementedException
    End Sub

    Public Sub UnAuthorise()
      SetProperty(AuthorisedByProperty, Nothing)
      SetProperty(AuthorisedByNameProperty, "")
      SetProperty(AuthorisedDateProperty, Nothing)
      Me.AuthorisedID = OBLib.CommonData.Enums.OffPeriodAuthorisationType.Pending
      SetProperty(AuthorisedDateStringProperty, "")
    End Sub

    Sub GenerateOffPeriod()

      Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdHumanResourceOffPeriodGenerateLeaveDays",
                                              New String() {"@OffReasonID", "@StartDateTime", "@EndDateTime", "@SystemID", "@ProductionAreaID", "@SetupCompleted"},
                                              New Object() {NothingDBNull(Me.OffReasonID),
                                                            NothingDBNull(Me.StartDate),
                                                            NothingDBNull(Me.EndDate),
                                                            NothingDBNull(Me.SystemID),
                                                            NothingDBNull(Me.ProductionAreaID),
                                                            Me.SetupCompleted})
      cmdProc.FetchType = CommandProc.FetchTypes.DataSet
      cmdProc = cmdProc.Execute
      Me.HumanResourceOffPeriodDayList.Clear()

      'Booking
      Dim bookingRow As DataRow = cmdProc.Dataset.Tables(0).Rows(0)
      Me.StartDate = bookingRow(0)
      Me.EndDate = bookingRow(1)
      'Days
      Dim daysTable As DataTable = cmdProc.Dataset.Tables(1)
      For Each dr As DataRow In daysTable.Rows
        Dim newDay As HumanResourceOffPeriodDay = Me.HumanResourceOffPeriodDayList.AddNew
        newDay.TimesheetDate = dr(0)
        newDay.StartDateTime = dr(1)
        newDay.EndDateTime = dr(2)
      Next

    End Sub

  End Class

End Namespace