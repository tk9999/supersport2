﻿' Generated 06 Oct 2016 13:30 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.PersonnelManager

  <Serializable()> _
  Public Class PMDate
    Inherits OBBusinessBase(Of PMDate)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return PMDateBO.PMDateBOToString(self)")

    Public Shared DateOfMonthProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.DateOfMonth, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property DateOfMonth As Date
      Get
        Return GetProperty(DateOfMonthProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(DateOfMonthProperty, Value)
      End Set
    End Property

    Public Shared YearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Year, "Year")
    ''' <summary>
    ''' Gets and sets the Year value
    ''' </summary>
    <Display(Name:="Year", Description:="")>
    Public Property Year() As Integer
      Get
        Return GetProperty(YearProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(YearProperty, Value)
      End Set
    End Property

    Public Shared MonthNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MonthNo, "Month No")
    ''' <summary>
    ''' Gets and sets the Month No value
    ''' </summary>
    <Display(Name:="Month No", Description:="")>
    Public Property MonthNo() As Integer
      Get
        Return GetProperty(MonthNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MonthNoProperty, Value)
      End Set
    End Property

    Public Shared WeekNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekNo, "Week No")
    ''' <summary>
    ''' Gets and sets the Week No value
    ''' </summary>
    <Display(Name:="Week No", Description:="")>
    Public Property WeekNo() As Integer
      Get
        Return GetProperty(WeekNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WeekNoProperty, Value)
      End Set
    End Property

    Public Shared MonthDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MonthDay, "Month Day")
    ''' <summary>
    ''' Gets and sets the Month Day value
    ''' </summary>
    <Display(Name:="Month Day", Description:="")>
    Public Property MonthDay() As Integer
      Get
        Return GetProperty(MonthDayProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MonthDayProperty, Value)
      End Set
    End Property

    Public Shared WeekDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekDay, "Week Day")
    ''' <summary>
    ''' Gets and sets the Week Day value
    ''' </summary>
    <Display(Name:="Week Day", Description:="")>
    Public Property WeekDay() As Integer
      Get
        Return GetProperty(WeekDayProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WeekDayProperty, Value)
      End Set
    End Property

    Public Shared WeekDayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WeekDayName, "Week Day Name")
    ''' <summary>
    ''' Gets and sets the Week Day Name value
    ''' </summary>
    <Display(Name:="Week Day Name", Description:="")>
    Public Property WeekDayName() As String
      Get
        Return GetProperty(WeekDayNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(WeekDayNameProperty, Value)
      End Set
    End Property

    Public Shared WeekDayNameShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WeekDayNameShort, "Week Day Name Short")
    ''' <summary>
    ''' Gets and sets the Week Day Name Short value
    ''' </summary>
    <Display(Name:="Week Day Name Short", Description:="")>
    Public Property WeekDayNameShort() As String
      Get
        Return GetProperty(WeekDayNameShortProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(WeekDayNameShortProperty, Value)
      End Set
    End Property

    Public Shared IsWeekendProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsWeekend, "Is Weekend", False)
    ''' <summary>
    ''' Gets and sets the Is Weekend value
    ''' </summary>
    <Display(Name:="Is Weekend", Description:="")>
    Public Property IsWeekend() As Boolean
      Get
        Return GetProperty(IsWeekendProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsWeekendProperty, Value)
      End Set
    End Property

    Public Shared IsPublicHolidayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsPublicHoliday, "Is Public Holiday", False)
    ''' <summary>
    ''' Gets and sets the Is Weekend value
    ''' </summary>
    <Display(Name:="Is Public Holiday", Description:="")>
    Public Property IsPublicHoliday() As Boolean
      Get
        Return GetProperty(IsPublicHolidayProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsPublicHolidayProperty, Value)
      End Set
    End Property

    Public Shared IsTodayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsToday, "Is Today", False)
    ''' <summary>
    ''' Gets and sets the Is Weekend value
    ''' </summary>
    <Display(Name:="Is Today", Description:="")>
    Public Property IsToday() As Boolean
      Get
        Return GetProperty(IsTodayProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTodayProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DateOfMonthProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.WeekDayName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "PM Date")
        Else
          Return String.Format("Blank {0}", "PM Date")
        End If
      Else
        Return Me.WeekDayName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPMDate() method.

    End Sub

    Public Shared Function NewPMDate() As PMDate

      Return DataPortal.CreateChild(Of PMDate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetPMDate(dr As SafeDataReader) As PMDate

      Dim p As New PMDate()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DateOfMonthProperty, .GetValue(0))
          LoadProperty(YearProperty, .GetInt32(1))
          LoadProperty(MonthNoProperty, .GetInt32(2))
          LoadProperty(WeekNoProperty, .GetInt32(3))
          LoadProperty(MonthDayProperty, .GetInt32(4))
          LoadProperty(WeekDayProperty, .GetInt32(5))
          LoadProperty(WeekDayNameProperty, .GetString(6))
          LoadProperty(WeekDayNameShortProperty, .GetString(7))
          LoadProperty(IsWeekendProperty, .GetBoolean(8))
          LoadProperty(IsPublicHolidayProperty, .GetBoolean(9))
          LoadProperty(IsTodayProperty, .GetBoolean(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'cm.Parameters.AddWithValue("@Date", Date)
      '      cm.Parameters.AddWithValue("@Year", GetProperty(YearProperty))
      '      cm.Parameters.AddWithValue("@MonthNo", GetProperty(MonthNoProperty))
      '      cm.Parameters.AddWithValue("@WeekNo", GetProperty(WeekNoProperty))
      '      cm.Parameters.AddWithValue("@MonthDay", GetProperty(MonthDayProperty))
      '      cm.Parameters.AddWithValue("@WeekDay", GetProperty(WeekDayProperty))
      '      cm.Parameters.AddWithValue("@WeekDayName", GetProperty(WeekDayNameProperty))
      '      cm.Parameters.AddWithValue("@WeekDayNameShort", GetProperty(WeekDayNameShortProperty))
      '      cm.Parameters.AddWithValue("@IsWeekend", GetProperty(IsWeekendProperty))

      '      Return Sub()
      '               'Post Save
      '               If Me.IsNew Then
      '                 LoadProperty(DateProperty, cm.Parameters("@Date").Value)
      '               End If
      '             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@Date", GetProperty(DateProperty))
    End Sub

#End Region

  End Class

End Namespace