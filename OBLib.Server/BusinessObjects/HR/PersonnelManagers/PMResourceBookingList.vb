﻿' Generated 06 Oct 2016 13:04 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.PersonnelManager

  <Serializable()> _
  Public Class PMResourceBookingList
    Inherits OBBusinessListBase(Of PMResourceBookingList, PMResourceBooking)

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As PMResourceBooking

      For Each child As PMResourceBooking In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewPMResourceBookingList() As PMResourceBookingList

      Return New PMResourceBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace