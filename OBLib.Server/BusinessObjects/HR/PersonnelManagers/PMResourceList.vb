﻿' Generated 06 Oct 2016 13:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.PersonnelManager

    <Serializable()> _
    Public Class PMResourceList
        Inherits OBBusinessListBase(Of PMResourceList, PMResource)

#Region " Business Methods "

        Public Function GetItem(ResourceID As Integer) As PMResource

            For Each child As PMResource In Me
                If child.ResourceID = ResourceID Then
                    Return child
                End If
            Next
            Return Nothing

        End Function

        Public Overrides Function ToString() As String

            Return "S"

        End Function

        Public Function GetPMResourceBooking(ResourceBookingID As Integer) As PMResourceBooking

            Dim obj As PMResourceBooking = Nothing
            For Each parent As PMResource In Me
                obj = parent.PMResourceBookingList.GetItem(ResourceBookingID)
                If obj IsNot Nothing Then
                    Return obj
                End If
            Next
            Return Nothing

        End Function

#End Region

#Region " Data Access "

        <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
        Public Class Criteria
            Inherits CriteriaBase(Of Criteria)

            Public Property OnlyBookedInd As Boolean = False
            Public Property StartDate As DateTime? = Nothing
            Public Property EndDate As DateTime? = Nothing
            Public Property SystemIDs As List(Of Integer)
            Public Property ProductionAreaIDs As List(Of Integer)
            Public Property ContractTypeIDs As List(Of Integer)
            Public Property SystemTeamNumberIDs As List(Of Integer)
            Public Property DisciplineIDs As List(Of Integer)
            Public Property HumanResourceIDs As List(Of Integer)

            Public Sub New()

            End Sub

        End Class

        Public Shared Function NewPMResourceList() As PMResourceList

            Return New PMResourceList()

        End Function

        Public Sub New()

            ' must have parameter-less constructor

        End Sub

        Public Shared Function GetPMResourceList() As PMResourceList

            Return DataPortal.Fetch(Of PMResourceList)(New Criteria())

        End Function

        Private Sub Fetch(sdr As SafeDataReader)

            Me.RaiseListChangedEvents = False
            While sdr.Read
                Me.Add(PMResource.GetPMResource(sdr))
            End While
            Me.RaiseListChangedEvents = True

            Dim parent As PMResource = Nothing
            If sdr.NextResult() Then
                While sdr.Read
                    If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
                        parent = Me.GetItem(sdr.GetInt32(1))
                    End If
                    parent.PMResourceBookingList.RaiseListChangedEvents = False
                    parent.PMResourceBookingList.Add(PMResourceBooking.GetPMResourceBooking(sdr))
                    parent.PMResourceBookingList.RaiseListChangedEvents = True
                End While
            End If

            'For Each child As PMResource In Me
            '  child.CheckRules()
            '  For Each PMResourceBooking As PMResourceBooking In child.PMResourceBookingList
            '    PMResourceBooking.CheckRules()
            '  Next
            'Next

        End Sub

        Protected Overrides Sub DataPortal_Fetch(criteria As Object)

            Dim crit As Criteria = criteria
            Using cn As New SqlConnection(Singular.Settings.ConnectionString)
                cn.Open()
                Try
                    Using cm As SqlCommand = cn.CreateCommand
                        cm.CommandType = CommandType.StoredProcedure
                        cm.CommandText = "GetProcsWeb.getPersonnelManagerResourceList"
                        cm.Parameters.AddWithValue("@OnlyBookedInd", crit.OnlyBookedInd)
                        cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
                        cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
                        cm.Parameters.AddWithValue("@SystemIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemIDs)))
                        cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
                        cm.Parameters.AddWithValue("@ContractTypeIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ContractTypeIDs)))
                        cm.Parameters.AddWithValue("@DisciplineIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.DisciplineIDs)))
                        cm.Parameters.AddWithValue("@SystemTeamNumberIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemTeamNumberIDs)))
                        cm.Parameters.AddWithValue("@ResourceIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.HumanResourceIDs)))
                        Using sdr As New SafeDataReader(cm.ExecuteReader)
                            Fetch(sdr)
                        End Using
                    End Using
                Finally
                    cn.Close()
                End Try
            End Using

        End Sub

#End Region

    End Class

End Namespace