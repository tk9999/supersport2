﻿' Generated 06 Oct 2016 13:30 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.PersonnelManager

  <Serializable()> _
  Public Class PMDateList
    Inherits OBBusinessListBase(Of PMDateList, PMDate)

#Region " Business Methods "

    Public Function GetItem(DateOfMonth As Date) As PMDate

      For Each child As PMDate In Me
        If child.DateOfMonth = DateOfMonth Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDate As DateTime?
      Public Property EndDate As DateTime?

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewPMDateList() As PMDateList

      Return New PMDateList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetPMDateList() As PMDateList

      Return DataPortal.Fetch(Of PMDateList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(PMDate.GetPMDate(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getPersonnelManagerDateList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace