﻿' Generated 06 Oct 2016 13:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.PersonnelManager

  <Serializable()> _
  Public Class PMResource
    Inherits OBBusinessBase(Of PMResource)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return PMResourceBO.PMResourceBOToString(self)")

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:="")>
    Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeID, "Resource Type")
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public Property ResourceTypeID() As Integer
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    'Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ' ''' <summary>
    ' ''' Gets and sets the Human Resource value
    ' ''' </summary>
    '<Display(Name:="Human Resource", Description:="")>
    'Public Property HumanResourceID() As Integer
    '  Get
    '    Return GetProperty(HumanResourceIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(HumanResourceIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ' ''' <summary>
    ' ''' Gets and sets the Room value
    ' ''' </summary>
    '<Display(Name:="Room", Description:="")>
    'Public Property RoomID() As Integer
    '  Get
    '    Return GetProperty(RoomIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(RoomIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle")
    ' ''' <summary>
    ' ''' Gets and sets the Vehicle value
    ' ''' </summary>
    '<Display(Name:="Vehicle", Description:="")>
    'Public Property VehicleID() As Integer
    '  Get
    '    Return GetProperty(VehicleIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(VehicleIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "Equipment")
    ' ''' <summary>
    ' ''' Gets and sets the Equipment value
    ' ''' </summary>
    '<Display(Name:="Equipment", Description:="")>
    'Public Property EquipmentID() As Integer
    '  Get
    '    Return GetProperty(EquipmentIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(EquipmentIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "Channel")
    ' ''' <summary>
    ' ''' Gets and sets the Channel value
    ' ''' </summary>
    '<Display(Name:="Channel", Description:="")>
    'Public Property ChannelID() As Integer
    '  Get
    '    Return GetProperty(ChannelIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(ChannelIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ' ''' <summary>
    ' ''' Gets the Created By value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property CreatedBy() As Integer
    '  Get
    '    Return GetProperty(CreatedByProperty)
    '  End Get
    'End Property

    'Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ' ''' <summary>
    ' ''' Gets the Created Date Time value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property CreatedDateTime() As DateTime
    '  Get
    '    Return GetProperty(CreatedDateTimeProperty)
    '  End Get
    'End Property

    'Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ' ''' <summary>
    ' ''' Gets the Modified By value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ModifiedBy() As Integer
    '  Get
    '    Return GetProperty(ModifiedByProperty)
    '  End Get
    'End Property

    'Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ' ''' <summary>
    ' ''' Gets the Modified Date Time value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ModifiedDateTime() As DateTime
    '  Get
    '    Return GetProperty(ModifiedDateTimeProperty)
    '  End Get
    'End Property

    'Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CustomResourceID, "Custom Resource")
    ' ''' <summary>
    ' ''' Gets and sets the Custom Resource value
    ' ''' </summary>
    '<Display(Name:="Custom Resource", Description:="")>
    'Public Property CustomResourceID() As Integer
    '  Get
    '    Return GetProperty(CustomResourceIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(CustomResourceIDProperty, Value)
    '  End Set
    'End Property

#End Region

#Region " Child Lists "

    Public Shared PMResourceBookingListProperty As PropertyInfo(Of PMResourceBookingList) = RegisterProperty(Of PMResourceBookingList)(Function(c) c.PMResourceBookingList, "PM Resource Booking List")

    Public ReadOnly Property PMResourceBookingList() As PMResourceBookingList
      Get
        If GetProperty(PMResourceBookingListProperty) Is Nothing Then
          LoadProperty(PMResourceBookingListProperty, HR.PersonnelManager.PMResourceBookingList.NewPMResourceBookingList())
        End If
        Return GetProperty(PMResourceBookingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "PM Resource")
        Else
          Return String.Format("Blank {0}", "PM Resource")
        End If
      Else
        Return Me.ResourceName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPMResource() method.

    End Sub

    Public Shared Function NewPMResource() As PMResource

      Return DataPortal.CreateChild(Of PMResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetPMResource(dr As SafeDataReader) As PMResource

      Dim p As New PMResource()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceIDProperty, .GetInt32(0))
          LoadProperty(ResourceNameProperty, .GetString(1))
          LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          'LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          'LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          'LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          'LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          'LoadProperty(CreatedByProperty, .GetInt32(8))
          'LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
          'LoadProperty(ModifiedByProperty, .GetInt32(10))
          'LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
          'LoadProperty(CustomResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'AddPrimaryKeyParam(cm, ResourceIDProperty)

      'cm.Parameters.AddWithValue("@ResourceName", GetProperty(ResourceNameProperty))
      'cm.Parameters.AddWithValue("@ResourceTypeID", GetProperty(ResourceTypeIDProperty))
      ''cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      ''cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      ''cm.Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
      ''cm.Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
      ''cm.Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
      ''cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      ''cm.Parameters.AddWithValue("@CustomResourceID", GetProperty(CustomResourceIDProperty))

      'Return Sub()
      '         'Post Save
      '         If Me.IsNew Then
      '           LoadProperty(ResourceIDProperty, cm.Parameters("@ResourceID").Value)
      '         End If
      '       End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      'UpdateChild(GetProperty(PMResourceBookingListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      ' cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace