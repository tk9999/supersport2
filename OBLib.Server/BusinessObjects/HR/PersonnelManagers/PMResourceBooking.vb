﻿' Generated 06 Oct 2016 13:04 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.PersonnelManager

  <Serializable()> _
  Public Class PMResourceBooking
    Inherits OBBusinessBase(Of PMResourceBooking)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return PMResourceBookingBO.PMResourceBookingBOToString(self)")

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceIDProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type")
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:="")>
    Public Property ResourceBookingTypeID() As Integer
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:="")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
    Public Property StartDateTimeBuffer As Date
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public Property StartDateTime As Date
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
    Public Property EndDateTimeBuffer As Date
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedBy, "Is Being Edited By")
    ''' <summary>
    ''' Gets and sets the Is Being Edited By value
    ''' </summary>
    <Display(Name:="Is Being Edited By", Description:="")>
    Public Property IsBeingEditedBy() As String
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsBeingEditedByProperty, Value)
      End Set
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.InEditDateTime, "In Edit Date Time")
    ''' <summary>
    ''' Gets and sets the In Edit Date Time value
    ''' </summary>
    <Display(Name:="In Edit Date Time", Description:="")>
    Public Property InEditDateTime As Date
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(InEditDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftID, "Human Resource Shift")
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period")
    ''' <summary>
    ''' Gets and sets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="")>
    Public Property HumanResourceOffPeriodID() As Integer
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment")
    ''' <summary>
    ''' Gets and sets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
    Public Property HumanResourceSecondmentID() As Integer
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceSecondmentIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "Production HR")
    ''' <summary>
    ''' Gets and sets the Production HR value
    ''' </summary>
    <Display(Name:="Production HR", Description:="")>
    Public Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class")
    ''' <summary>
    ''' Gets and sets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css Class", Description:="")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IgnoreClashes, "Ignore Clashes", False)
    ''' <summary>
    ''' Gets and sets the Ignore Clashes value
    ''' </summary>
    <Display(Name:="Ignore Clashes", Description:="")>
    Public Property IgnoreClashes() As Boolean
      Get
        Return GetProperty(IgnoreClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreClashesProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "Ignore Clashes Reason")
    ''' <summary>
    ''' Gets and sets the Ignore Clashes Reason value
    ''' </summary>
    <Display(Name:="Ignore Clashes Reason", Description:="")>
    Public Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IgnoreClashesReasonProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared IsLockedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLocked, "Is Locked", False)
    ''' <summary>
    ''' Gets and sets the Is Locked value
    ''' </summary>
    <Display(Name:="Is Locked", Description:="")>
    Public Property IsLocked() As Boolean
      Get
        Return GetProperty(IsLockedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsLockedProperty, Value)
      End Set
    End Property

    Public Shared IsLockedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsLockedReason, "Is Locked Reason")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Is Locked Reason", Description:="")>
    Public Property IsLockedReason() As String
      Get
        Return GetProperty(IsLockedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsLockedReasonProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:="")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleID, "Equipment Schedule")
    ''' <summary>
    ''' Gets and sets the Equipment Schedule value
    ''' </summary>
    <Display(Name:="Equipment Schedule", Description:="")>
    Public Property EquipmentScheduleID() As Integer
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ParentResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentResourceBookingID, "Parent Resource Booking")
    ''' <summary>
    ''' Gets and sets the Parent Resource Booking value
    ''' </summary>
    <Display(Name:="Parent Resource Booking", Description:="")>
    Public Property ParentResourceBookingID() As Integer
      Get
        Return GetProperty(ParentResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ParentResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content")
    ''' <summary>
    ''' Gets and sets the Popover Content value
    ''' </summary>
    <Display(Name:="Popover Content", Description:="")>
    Public Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PopoverContentProperty, Value)
      End Set
    End Property

    Public Shared DisplayInBackgroundProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DisplayInBackground, "Display In Background", False)
    ''' <summary>
    ''' Gets and sets the Display In Background value
    ''' </summary>
    <Display(Name:="Display In Background", Description:="")>
    Public Property DisplayInBackground() As Boolean
      Get
        Return GetProperty(DisplayInBackgroundProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DisplayInBackgroundProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As PMResource

      Return CType(CType(Me.Parent, PMResourceBookingList).Parent, PMResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "PM Resource Booking")
        Else
          Return String.Format("Blank {0}", "PM Resource Booking")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPMResourceBooking() method.

    End Sub

    Public Shared Function NewPMResourceBooking() As PMResourceBooking

      Return DataPortal.CreateChild(Of PMResourceBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetPMResourceBooking(dr As SafeDataReader) As PMResourceBooking

      Dim p As New PMResourceBooking()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
          LoadProperty(IsBeingEditedByProperty, .GetString(8))
          LoadProperty(InEditDateTimeProperty, .GetValue(9))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(IsCancelledProperty, .GetBoolean(11))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(StatusCssClassProperty, .GetString(15))
          LoadProperty(IgnoreClashesProperty, .GetBoolean(16))
          LoadProperty(IgnoreClashesReasonProperty, .GetString(17))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(IsLockedProperty, .GetBoolean(19))
          LoadProperty(IsLockedReasonProperty, .GetString(20))
          LoadProperty(IsTBCProperty, .GetBoolean(21))
          LoadProperty(EquipmentScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(ParentResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(PopoverContentProperty, .GetString(25))
          LoadProperty(DisplayInBackgroundProperty, .GetBoolean(26))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'AddPrimaryKeyParam(cm, ResourceBookingIDProperty)

      'cm.Parameters.AddWithValue("@ResourceID", Me.GetParent().ResourceID)
      'cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
      'cm.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      'cm.Parameters.AddWithValue("@StartDateTimeBuffer", StartDateTimeBuffer)
      'cm.Parameters.AddWithValue("@StartDateTime", StartDateTime)
      'cm.Parameters.AddWithValue("@EndDateTime", EndDateTime)
      'cm.Parameters.AddWithValue("@EndDateTimeBuffer", EndDateTimeBuffer)
      'cm.Parameters.AddWithValue("@IsBeingEditedBy", GetProperty(IsBeingEditedByProperty))
      'cm.Parameters.AddWithValue("@InEditDateTime", InEditDateTime)
      'cm.Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
      'cm.Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      'cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", GetProperty(HumanResourceOffPeriodIDProperty))
      'cm.Parameters.AddWithValue("@HumanResourceSecondmentID", GetProperty(HumanResourceSecondmentIDProperty))
      'cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
      'cm.Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
      'cm.Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
      'cm.Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
      'cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
      'cm.Parameters.AddWithValue("@IsLocked", GetProperty(IsLockedProperty))
      'cm.Parameters.AddWithValue("@IsLockedReason", GetProperty(IsLockedReasonProperty))
      'cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      'cm.Parameters.AddWithValue("@EquipmentScheduleID", GetProperty(EquipmentScheduleIDProperty))
      'cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
      'cm.Parameters.AddWithValue("@ParentResourceBookingID", GetProperty(ParentResourceBookingIDProperty))
      'cm.Parameters.AddWithValue("@PopoverContent", GetProperty(PopoverContentProperty))
      'cm.Parameters.AddWithValue("@DisplayInBackground", GetProperty(DisplayInBackgroundProperty))

      'Return Sub()
      '         'Post Save
      '         If Me.IsNew Then
      '           LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
      '         End If
      '       End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
    End Sub

#End Region

  End Class

End Namespace