﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

<Serializable>
Public Class HumanResourceOffPeriodDayList
  Inherits OBBusinessListBase(Of HumanResourceOffPeriodDayList, HumanResourceOffPeriodDay)

#Region " Business Methods "

  Public Overrides Function ToString() As String

    Return "s"

  End Function

  'Public Property TBCSaveResults As List(Of Singular.Web.Result)

#End Region

#Region " Data Access "

  <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
  Public Class Criteria
    Inherits CriteriaBase(Of Criteria)

    Public Property StartDate As Date?
    Public Property EndDate As Date?
    Public Property HumanResourceID As Integer?
    Public Property HumanResourceIDs As String

    Public Sub New(HumanResourceID As Integer?, HumanResourceIDs As String,
                   StartDate As Date?, EndDate As Date?)
      Me.HumanResourceID = HumanResourceID
      Me.HumanResourceIDs = HumanResourceIDs
      Me.StartDate = StartDate
      Me.EndDate = EndDate
    End Sub

    Public Sub New()


    End Sub

  End Class

#Region " Common "

  Public Shared Function NewHumanResourceOffPeriodDayList() As HumanResourceOffPeriodDayList

    Return New HumanResourceOffPeriodDayList()

  End Function

  Public Shared Sub BeginGetHumanResourceOffPeriodDayList(CallBack As EventHandler(Of DataPortalResult(Of HumanResourceOffPeriodDayList)))

    Dim dp As New DataPortal(Of HumanResourceOffPeriodDayList)()
    AddHandler dp.FetchCompleted, CallBack
    dp.BeginFetch(New Criteria())

  End Sub

  Public Sub New()

    ' must have parameter-less constructor

  End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

  Public Shared Function GetHumanResourceOffPeriodDayList() As HumanResourceOffPeriodDayList

    Return DataPortal.Fetch(Of HumanResourceOffPeriodDayList)(New Criteria())

  End Function

  Private Sub Fetch(sdr As SafeDataReader)

    Me.RaiseListChangedEvents = False
    While sdr.Read
      Me.Add(HumanResourceOffPeriodDay.GetHumanResourceOffPeriodDay(sdr))
    End While
    Me.RaiseListChangedEvents = True

  End Sub

  Protected Overrides Sub DataPortal_Fetch(criteria As Object)

    Dim crit As Criteria = criteria
    Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      cn.Open()
      Try
        Using cm As SqlCommand = cn.CreateCommand
          cm.CommandType = CommandType.StoredProcedure
          cm.CommandText = "GetProcsWeb.getHumanResourceOffPeriodDayList"
          cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
          cm.Parameters.AddWithValue("@HumanResourceIDs", Strings.MakeEmptyDBNull(crit.HumanResourceIDs))
          cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
          cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
          Using sdr As New SafeDataReader(cm.ExecuteReader)
            Fetch(sdr)
          End Using
        End Using
      Finally
        cn.Close()
      End Try
    End Using

  End Sub

  Friend Sub Update()

    Me.RaiseListChangedEvents = False
    Try
      ' Loop through each deleted child object and call its Update() method
      For Each Child As HumanResourceOffPeriodDay In DeletedList
        Child.DeleteSelf()
      Next

      ' Then clear the list of deleted objects because they are truly gone now.
      DeletedList.Clear()

      ' Loop through each non-deleted child object and call its Update() method
      For Each Child As HumanResourceOffPeriodDay In Me
        If Child.IsNew Then
          Child.Insert()
        Else
          Child.Update()
        End If
        'Dim wr As Singular.Web.Result
        'If Child.IsAvailable Then
        '  'We expect a success notification
        '  wr = New Singular.Web.Result(True) With {.ErrorText = "", .Data = New With {.Available = True, .NotAvailable = False}}
        'ElseIf Not Child.IsAvailable Then
        '  'We expect a warning notification
        '  wr = New Singular.Web.Result(True) With {.ErrorText = ""}
        'End If
      Next
    Finally
      Me.RaiseListChangedEvents = True
    End Try

  End Sub

  Protected Overrides Sub DataPortal_Update()

    UpdateTransactional(AddressOf Update)

  End Sub

#End If

#End Region

#End Region

End Class