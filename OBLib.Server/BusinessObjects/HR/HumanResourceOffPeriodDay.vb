﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

<Serializable>
Public Class HumanResourceOffPeriodDay
  Inherits OBBusinessBase(Of HumanResourceOffPeriodDay)

#Region " Properties "

  Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewScheduleDetailID, "ID", 0)
  ''' <summary>
  ''' Gets the ID value
  ''' </summary>
  <Key,
  Display(Name:="ID")>
  Public ReadOnly Property CrewScheduleDetailID() As Integer
    Get
      Return GetProperty(CrewScheduleDetailIDProperty)
    End Get
  End Property

  Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ResourceBookingID, Nothing)
  ''' <summary>
  ''' Gets and sets the ID value
  ''' </summary>
  Public Property ResourceBookingID() As Integer?
    Get
      Return GetProperty(ResourceBookingIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(ResourceBookingIDProperty, Value)
    End Set
  End Property

  Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
  ''' <summary>
  ''' Gets and sets the Human Resource Off Period value
  ''' </summary>
  <Display(Name:="Human Resource Off Period", Description:="the off period (leave) that the scheduled item is linked to")>
  Public Property HumanResourceOffPeriodID() As Integer?
    Get
      Return GetProperty(HumanResourceOffPeriodIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(HumanResourceOffPeriodIDProperty, Value)
    End Set
  End Property

  Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Date")
  ''' <summary>
  ''' Gets and sets the Timesheet Date value
  ''' </summary>
  <Display(Name:="Date", Description:=""),
  Required(ErrorMessage:="Date required"),
  DateField(FormatString:="ddd dd MMM yyyy")>
  Public Property TimesheetDate As DateTime?
    Get
      Return GetProperty(TimesheetDateProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(TimesheetDateProperty, Value)
    End Set
  End Property

  Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
  ''' <summary>
  ''' Gets and sets the Start Date Time value
  ''' </summary>
  <Display(Name:="Start Time"),
  Required(ErrorMessage:="Start Time required"),
  SetExpression("HumanResourceOffPeriodDayBO.StartDateTimeSet(self)"),
  TimeField(TimeFormat:=TimeFormats.ShortTime)>
  Public Property StartDateTime As DateTime?
    Get
      Return GetProperty(StartDateTimeProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(StartDateTimeProperty, Value)
    End Set
  End Property

  Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
  ''' <summary>
  ''' Gets and sets the End Date Time value
  ''' </summary>
  <Display(Name:="End Time"),
  Required(ErrorMessage:="End Time required"),
  SetExpression("HumanResourceOffPeriodDayBO.EndDateTimeSet(self)"),
  TimeField(TimeFormat:=TimeFormats.ShortTime)>
  Public Property EndDateTime As DateTime?
    Get
      Return GetProperty(EndDateTimeProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(EndDateTimeProperty, Value)
    End Set
  End Property

  Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTimesheetID, "Crew Timesheet", Nothing)
  ''' <summary>
  ''' Gets and sets the Crew Timesheet value
  ''' </summary>
  <Display(Name:="Crew Timesheet", Description:="The timesheet record that will be created for this schedule detail one the production has finished")>
  Public Property CrewTimesheetID() As Integer?
    Get
      Return GetProperty(CrewTimesheetIDProperty)
    End Get
    Set(ByVal Value As Integer?)
      SetProperty(CrewTimesheetIDProperty, Value)
    End Set
  End Property

#End Region

#Region " Methods "

  Public Function GetParent() As HumanResourceOffPeriod
    Return CType(CType(Me.Parent, HumanResourceOffPeriodDayList).Parent, HumanResourceOffPeriod)
  End Function

  Protected Overrides Function GetIdValue() As Object

    Return GetProperty(CrewScheduleDetailIDProperty)

  End Function

  Public Overrides Function ToString() As String

    If Me.TimesheetDate IsNot Nothing Then
      Return Me.TimesheetDate.Value.ToShortDateString()
    Else
      Return "Leave Day"
    End If

  End Function

#End Region

#Region " Validation Rules "

  Protected Overrides Sub AddBusinessRules()
    MyBase.AddBusinessRules()

  End Sub

#End Region

#Region " Common "

  Protected Overrides Sub OnCreate()

    'This is called when a new object is created
    'Set any variables here, not in the constructor or NewCrewScheduleDetailBase() method.

  End Sub

  Public Shared Function NewHumanResourceOffPeriodDay() As HumanResourceOffPeriodDay

    Return DataPortal.CreateChild(Of HumanResourceOffPeriodDay)()

  End Function

  Public Sub New()

    MarkAsChild()

  End Sub

#End Region

#Region " .Net Data Access "

  Friend Shared Function GetHumanResourceOffPeriodDay(dr As SafeDataReader) As HumanResourceOffPeriodDay

    Dim p As New HumanResourceOffPeriodDay()
    p.Fetch(dr)
    Return p

  End Function

  Protected Sub Fetch(sdr As SafeDataReader)

    Using BypassPropertyChecks
      With sdr
        LoadProperty(CrewScheduleDetailIDProperty, .GetInt32(0))
        LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(TimesheetDateProperty, .GetValue(3))
        LoadProperty(StartDateTimeProperty, .GetValue(4))
        LoadProperty(EndDateTimeProperty, .GetValue(5))
        LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        'LoadProperty(CreatedByProperty, .GetInt32(12))
        'LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
        'LoadProperty(ModifiedByProperty, .GetInt32(14))
        'LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))
        'LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        'LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
      End With
    End Using

    MarkAsChild()
    MarkOld()
    BusinessRules.CheckRules()

  End Sub

  Friend Sub Insert()

    ' if we're not dirty then don't update the database
    Using cm As SqlCommand = New SqlCommand
      cm.CommandText = "InsProcsWeb.insHumanResourceOffPeriodDay"
      DoInsertUpdateChild(cm)
    End Using

  End Sub

  Friend Sub Update()

    ' if we're not dirty then don't update the database
    Using cm As SqlCommand = New SqlCommand
      cm.CommandText = "UpdProcsWeb.updHumanResourceOffPeriodDay"
      DoInsertUpdateChild(cm)
    End Using

  End Sub

  Protected Overrides Sub InsertUpdate(cm As SqlCommand)

    If Me.IsSelfDirty Then

      With cm
        .CommandType = CommandType.StoredProcedure

        Dim paramCrewScheduleDetailID As SqlParameter = .Parameters.Add("@CrewScheduleDetailID", SqlDbType.Int)
        paramCrewScheduleDetailID.Value = GetProperty(CrewScheduleDetailIDProperty)
        If Me.IsNew Then
          paramCrewScheduleDetailID.Direction = ParameterDirection.Output
        End If
        .Parameters.AddWithValue("@ResourceBookingID", Me.GetParent.ResourceBookingID)
        .Parameters.AddWithValue("@HumanResourceOffPeriodID", Me.GetParent.HumanResourceOffPeriodID)
        .Parameters.AddWithValue("@TimesheetDate", (New SmartDate(GetProperty(TimesheetDateProperty))).DBValue)
        .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
        .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
        .Parameters.AddWithValue("@CrewTimesheetID", Singular.Misc.NothingDBNull(GetProperty(CrewTimesheetIDProperty)))
        .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
        .Parameters.AddWithValue("@HumanResourceID", Me.GetParent.HumanResourceID)
        .Parameters.AddWithValue("@ProductionAreaID", Me.GetParent.ProductionAreaID)
        .Parameters.AddWithValue("@SystemID", Me.GetParent.SystemID)
        .ExecuteNonQuery()

        If Me.IsNew Then
          LoadProperty(CrewScheduleDetailIDProperty, paramCrewScheduleDetailID.Value)
        End If
        MarkOld()
      End With
    Else
    End If

  End Sub

  Friend Sub DeleteSelf()

    ' if we're not dirty then don't update the database
    If Me.IsNew Then Exit Sub

    Using cm As SqlCommand = New SqlCommand
      cm.CommandText = "DelProcsWeb.delHumanResourceOffPeriodDay"
      cm.CommandType = CommandType.StoredProcedure
      cm.Parameters.AddWithValue("@CrewScheduleDetailID", GetProperty(CrewScheduleDetailIDProperty))
      DoDeleteChild(cm)
    End Using

  End Sub

  Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

    If Me.IsNew Then Exit Sub

    cm.ExecuteNonQuery()
    MarkNew()

  End Sub

#End Region

End Class
