﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class HumanResourceUnAvailabilityTemplate
    Inherits OBBusinessBase(Of HumanResourceUnAvailabilityTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceUnAvailabilityTemplateBO.HumanResourceUnAvailabilityTemplateBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The start time for the off period"),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("HumanResourceUnAvailabilityTemplateBO.StartDateSet(self)"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Time", Description:="The end time for the off period"),
    Required(ErrorMessage:="End Time required"),
    SetExpression("HumanResourceUnAvailabilityTemplateBO.EndDateSet(self)"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Reason", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Reason", Description:="Description of the off period"),
    StringLength(200, ErrorMessage:="Reason cannot be more than 200 characters"),
    Required(AllowEmptyStrings:=False),
    SetExpression("HumanResourceUnAvailabilityTemplateBO.DetailSet(self)")>
    Public Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailProperty, Value)
      End Set
    End Property

    Public Shared UnAvailableDaysProperty As PropertyInfo(Of List(Of String)) = RegisterProperty(Of List(Of String))(Function(c) c.UnAvailableDays, "UnAvailableDays", New List(Of String))
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    Public Property UnAvailableDays() As List(Of String)
      Get
        Return GetProperty(UnAvailableDaysProperty)
      End Get
      Set(ByVal Value As List(Of String))
        SetProperty(UnAvailableDaysProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Sub ReplaceList(NewList As HumanResourceUnAvailabilityList)
      SetProperty(HumanResourceUnAvailabilityListProperty, NewList)
    End Sub

#End Region

#End Region

#Region " Child Lists "

    Public Shared HumanResourceUnAvailabilityListProperty As PropertyInfo(Of HumanResourceUnAvailabilityList) = RegisterProperty(Of HumanResourceUnAvailabilityList)(Function(c) c.HumanResourceUnAvailabilityList, "HumanResourceUnAvailabilityList")
    Public ReadOnly Property HumanResourceUnAvailabilityList() As HumanResourceUnAvailabilityList
      Get
        If GetProperty(HumanResourceUnAvailabilityListProperty) Is Nothing Then
          LoadProperty(HumanResourceUnAvailabilityListProperty, HR.HumanResourceUnAvailabilityList.NewHumanResourceUnAvailabilityList())
        End If
        Return GetProperty(HumanResourceUnAvailabilityListProperty)
      End Get
    End Property

#End Region

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ResourceBookings", "CrewScheduleDetails"}
      End Get
    End Property

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceUnAvailabilityTemplate() method.

    End Sub

    Public Shared Function NewHumanResourceUnAvailabilityTemplate() As HumanResourceUnAvailabilityTemplate

      Return DataPortal.CreateChild(Of HumanResourceUnAvailabilityTemplate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceUnAvailabilityTemplate(dr As SafeDataReader) As HumanResourceUnAvailabilityTemplate

      Dim h As New HumanResourceUnAvailabilityTemplate()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(0)))
          LoadProperty(DetailProperty, .GetString(1))
          LoadProperty(StartDateTimeProperty, .GetValue(2))
          LoadProperty(EndDateTimeProperty, .GetValue(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceUnAvailabilityTemplate"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceUnAvailabilityTemplate"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then
      '  With cm
      '    .CommandType = CommandType.StoredProcedure
      '    .Parameters.AddWithValue("@HumanResourceID", Me.HumanResourceID)
      '    .Parameters.AddWithValue("@Detail", GetProperty(DetailProperty))
      '    .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
      '    .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
      '    .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      '    .ExecuteNonQuery()
      '    If Me.IsNew Then
      '      LoadProperty(HumanResourceOffPeriodIDProperty, cm.Parameters("@HumanResourceOffPeriodID").Value)
      '      LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
      '    End If
      '    MarkOld()
      '  End With
      'End If

    End Sub

    Friend Sub DeleteSelf()

      'If Me.IsNew Then Exit Sub
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delHumanResourceUnAvailabilityTemplate"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", GetProperty(HumanResourceOffPeriodIDProperty))
      '  cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

    Protected Overrides Sub SaveChildren()
      UpdateChild(GetProperty(HumanResourceUnAvailabilityListProperty))
    End Sub

#End Region

#End Region

  End Class

End Namespace