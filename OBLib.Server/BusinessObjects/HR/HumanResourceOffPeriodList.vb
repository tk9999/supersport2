﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class HumanResourceOffPeriodList
    Inherits OBBusinessListBase(Of HumanResourceOffPeriodList, HumanResourceOffPeriod)

#Region " Business Methods "

    Public Function GetItem(HumanResourceOffPeriodID As Integer) As HumanResourceOffPeriod

      For Each child As HumanResourceOffPeriod In Me
        If child.HumanResourceOffPeriodID = HumanResourceOffPeriodID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetOffPeriodBooking(ResourceBookingID As Integer) As OffPeriodBooking

    '  For Each child As HumanResourceOffPeriod In Me
    '    For Each b As OffPeriodBooking In child.OffPeriodBookingList
    '      If b.ResourceBookingID = ResourceBookingID Then
    '        Return b
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Off Periods"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property HumanResourceOffPeriodID As Integer?
      Public Property HumanResourceOffPeriodIDs As String = ""

      Public Sub New(HumanResourceOffPeriodIDs As String)
        Me.HumanResourceOffPeriodIDs = HumanResourceOffPeriodIDs
      End Sub

      Public Sub New(HumanResourceOffPeriodID As Integer?)
        Me.HumanResourceOffPeriodID = HumanResourceOffPeriodID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHumanResourceOffPeriodList() As HumanResourceOffPeriodList

      Return New HumanResourceOffPeriodList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHumanResourceOffPeriodList() As HumanResourceOffPeriodList

      Return DataPortal.Fetch(Of HumanResourceOffPeriodList)(New Criteria())

    End Function

    Public Shared Function GetHumanResourceOffPeriodList(HumanResourceOffPeriodID As Integer?) As HumanResourceOffPeriodList

      Return DataPortal.Fetch(Of HumanResourceOffPeriodList)(New Criteria(HumanResourceOffPeriodID))

    End Function

    Public Shared Function GetHumanResourceOffPeriodList(HumanResourceOffPeriodIDSXML As String) As HumanResourceOffPeriodList

      Return DataPortal.Fetch(Of HumanResourceOffPeriodList)(New Criteria(HumanResourceOffPeriodIDSXML))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceOffPeriod.GetHumanResourceOffPeriod(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentOffPeriod As HumanResourceOffPeriod = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentOffPeriod Is Nothing OrElse parentOffPeriod.HumanResourceOffPeriodID <> sdr.GetInt32(17) Then
      '      parentOffPeriod = Me.GetItem(sdr.GetInt32(17))
      '    End If
      '    If parentOffPeriod IsNot Nothing Then
      '      parentOffPeriod.OffPeriodBookingList.RaiseListChangedEvents = False
      '      parentOffPeriod.OffPeriodBookingList.Add(OffPeriodBooking.GetOffPeriodBooking(sdr))
      '      parentOffPeriod.OffPeriodBookingList.RaiseListChangedEvents = True
      '    End If
      '  End While
      'End If

      'Dim parentOffPeriodBooking As OffPeriodBooking = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentOffPeriod Is Nothing OrElse parentOffPeriod.HumanResourceOffPeriodID <> sdr.GetInt32(2) Then
            parentOffPeriod = Me.GetItem(sdr.GetInt32(2))
          End If
          If parentOffPeriod IsNot Nothing Then
            parentOffPeriod.HumanResourceOffPeriodDayList.RaiseListChangedEvents = False
            parentOffPeriod.HumanResourceOffPeriodDayList.Add(HumanResourceOffPeriodDay.GetHumanResourceOffPeriodDay(sdr))
            parentOffPeriod.HumanResourceOffPeriodDayList.RaiseListChangedEvents = True
          End If
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceOffPeriodList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            cm.Parameters.AddWithValue("@UserHumanResourceID", NothingDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", NothingDBNull(crit.HumanResourceOffPeriodID))
            cm.Parameters.AddWithValue("@HumanResourceOffPeriodIDs", Strings.MakeEmptyDBNull(crit.HumanResourceOffPeriodIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResourceOffPeriod In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResourceOffPeriod In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End Region

#End Region

  End Class

End Namespace