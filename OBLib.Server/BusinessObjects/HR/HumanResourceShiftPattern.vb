﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceShiftPattern
    Inherits OBBusinessBase(Of HumanResourceShiftPattern)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftPatternID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceShiftPatternID() As Integer
      Get
        Return GetProperty(HumanResourceShiftPatternIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ShiftPatternIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftPatternID, "Shift Pattern", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Pattern value
    ''' </summary>
    <Display(Name:="Shift Pattern", Description:="The shift pattern that this human resource is assigned to"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROShiftPatternList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ShiftPatternID() As Integer?
      Get
        Return GetProperty(ShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftPatternIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The date that this shift pattern is effective for an employee. Must always be at the start of the month (No date dropdown, only a month selection)"),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The end date of the shift pattern")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternID, "System Area Shift Pattern", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Pattern value
    ''' </summary>
    <Display(Name:="System Area Shift Pattern", Description:="System Area Shift Pattern")>
    Public Property SystemAreaShiftPatternID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaShiftPatternIDProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemAreaShiftPatternName, "System Area Shift Pattern Name", "")
    ''' <summary>
    ''' Gets and sets the Shift Pattern value
    ''' </summary>
    <Display(Name:="Shift Pattern Name", Description:="System Area Shift Pattern Name"),
    Required(ErrorMessage:="Shift Pattern required")>
    Public Property SystemAreaShiftPatternName() As String
      Get
        Return GetProperty(SystemAreaShiftPatternNameProperty)
      End Get
      Set(value As String)
        SetProperty(SystemAreaShiftPatternNameProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResource

      Return CType(CType(Me.Parent, HumanResourceShiftPatternList).Parent, HumanResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Shift Pattern")
        Else
          Return String.Format("Blank {0}", "Human Resource Shift Pattern")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceShiftPattern() method.

    End Sub

    Public Shared Function NewHumanResourceShiftPattern() As HumanResourceShiftPattern

      Return DataPortal.CreateChild(Of HumanResourceShiftPattern)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceShiftPattern(dr As SafeDataReader) As HumanResourceShiftPattern

      Dim h As New HumanResourceShiftPattern()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceShiftPatternIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(EndDateProperty, .GetValue(8))
          LoadProperty(SystemAreaShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(SystemAreaShiftPatternNameProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceShiftPattern"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceShiftPattern"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceShiftPatternID As SqlParameter = .Parameters.Add("@HumanResourceShiftPatternID", SqlDbType.Int)
          paramHumanResourceShiftPatternID.Value = GetProperty(HumanResourceShiftPatternIDProperty)
          If Me.IsNew Then
            paramHumanResourceShiftPatternID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", Me.GetParent().HumanResourceID)
          .Parameters.AddWithValue("@ShiftPatternID", NothingDBNull(GetProperty(ShiftPatternIDProperty)))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@SystemAreaShiftPatternID", NothingDBNull(GetProperty(SystemAreaShiftPatternIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceShiftPatternIDProperty, paramHumanResourceShiftPatternID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceShiftPattern"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceShiftPatternID", GetProperty(HumanResourceShiftPatternIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace