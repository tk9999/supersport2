﻿' Generated 24 Mar 2015 08:58 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSecondmentList
    Inherits OBBusinessListBase(Of HumanResourceSecondmentList, HumanResourceSecondment)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSecondmentID As Integer) As HumanResourceSecondment

      For Each child As HumanResourceSecondment In Me
        If child.HumanResourceSecondmentID = HumanResourceSecondmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSecondmentBooking(ResourceBookingID As Integer) As SecondmentBooking

      For Each child As HumanResourceSecondment In Me
        For Each b As SecondmentBooking In child.SecondmentBookingList
          If b.ResourceBookingID = ResourceBookingID Then
            Return b
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Secondments"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property HumanResourceSecondmentID As Integer?
      Public Property HumanResourceSecondmentIDs As String = ""

      Public Sub New(HumanResourceSecondmentIDs As String)
        Me.HumanResourceSecondmentIDs = HumanResourceSecondmentIDs
      End Sub

      Public Sub New(HumanResourceSecondmentID As Integer?)
        Me.HumanResourceSecondmentID = HumanResourceSecondmentID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHumanResourceSecondmentList() As HumanResourceSecondmentList

      Return New HumanResourceSecondmentList()

    End Function

    Public Shared Sub BeginGetHumanResourceSecondmentList(CallBack As EventHandler(Of DataPortalResult(Of HumanResourceSecondmentList)))

      Dim dp As New DataPortal(Of HumanResourceSecondmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHumanResourceSecondmentList() As HumanResourceSecondmentList

      Return DataPortal.Fetch(Of HumanResourceSecondmentList)(New Criteria())

    End Function

    Public Shared Function GetHumanResourceSecondmentList(HumanResourceSecondmentID As Integer?) As HumanResourceSecondmentList

      Return DataPortal.Fetch(Of HumanResourceSecondmentList)(New Criteria(HumanResourceSecondmentID))

    End Function

    Public Shared Function GetHumanResourceSecondmentList(HumanResourceSecondmentIDSXML As String) As HumanResourceSecondmentList

      Return DataPortal.Fetch(Of HumanResourceSecondmentList)(New Criteria(HumanResourceSecondmentIDSXML))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceSecondment.GetHumanResourceSecondment(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentSecondment As HumanResourceSecondment = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSecondment Is Nothing OrElse parentSecondment.HumanResourceSecondmentID <> sdr.GetInt32(18) Then
            parentSecondment = Me.GetItem(sdr.GetInt32(18))
          End If
          parentSecondment.SecondmentBookingList.RaiseListChangedEvents = False
          parentSecondment.SecondmentBookingList.Add(SecondmentBooking.GetSecondmentBooking(sdr))
          parentSecondment.SecondmentBookingList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentSecondmentBooking As SecondmentBooking = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSecondmentBooking Is Nothing OrElse parentSecondmentBooking.ResourceBookingID <> sdr.GetInt32(26) Then
            parentSecondmentBooking = Me.GetSecondmentBooking(sdr.GetInt32(26))
          End If
          parentSecondmentBooking.CrewScheduleDetailList.RaiseListChangedEvents = False
          parentSecondmentBooking.CrewScheduleDetailList.Add(CrewScheduleDetail.GetCrewScheduleDetail(sdr))
          parentSecondmentBooking.CrewScheduleDetailList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceSecondmentList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceSecondmentID", NothingDBNull(crit.HumanResourceSecondmentID))
            cm.Parameters.AddWithValue("@HumanResourceSecondmentIDs", Strings.MakeEmptyDBNull(crit.HumanResourceSecondmentIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResourceSecondment In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResourceSecondment In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace