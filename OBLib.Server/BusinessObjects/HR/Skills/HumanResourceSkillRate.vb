﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSkillRate
    Inherits OBBusinessBase(Of HumanResourceSkillRate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceSkillRateBO.HumanResourceSkillRateBOToString(self)")

    Public Shared HumanResourceSkillRateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSkillRateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceSkillRateID() As Integer
      Get
        Return GetProperty(HumanResourceSkillRateIDProperty)
      End Get
    End Property

    Public Shared HumanResourceSkillIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSkillID, "Human Resource Skill", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Skill value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceSkillID() As Integer?
      Get
        Return GetProperty(HumanResourceSkillIDProperty)
      End Get
    End Property

    Public Shared RateDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RateDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Rate Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="The date the this record will be effective from"),
    Required(ErrorMessage:="Effective Date required")>
    Public Property RateDate As DateTime?
      Get
        Return GetProperty(RateDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(RateDateProperty, Value)
      End Set
    End Property

    Public Shared RatePerDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RatePerDay, "Daily Rate", CDec(0))
    ''' <summary>
    ''' Gets and sets the Rate Per Day value
    ''' </summary>
    <Display(Name:="Daily Rate", Description:="The rate that this human resource charges for the specific skill"),
    Required(ErrorMessage:="Daily Rate required"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property RatePerDay() As Decimal
      Get
        Return GetProperty(RatePerDayProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RatePerDayProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="Comments on the current rate")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Skill value
    ''' </summary>
    <Display(Name:="Area"), DropDownWeb(GetType(ROProductionAreaList)), Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResourceSkill

      Return CType(CType(Me.Parent, HumanResourceSkillRateList).Parent, HumanResourceSkill)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSkillRateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Skill Rate")
        Else
          Return String.Format("Blank {0}", "Human Resource Skill Rate")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RateDateProperty)
        .JavascriptRuleFunctionName = "HumanResourceSkillRateBO.HRSkillRateListUnique"
        .ServerRuleFunction = AddressOf HRSkillRateListUnique
      End With

      With AddWebRule(RatePerDayProperty)
        .JavascriptRuleFunctionName = "HumanResourceSkillRateBO.RatePerDayValid"
        .ServerRuleFunction = AddressOf RatePerDayValid
      End With

    End Sub

    Public Shared Function HRSkillRateListUnique(HRSR As HumanResourceSkillRate) As String
      Dim ErrorString = ""
      If HRSR.Parent IsNot Nothing Then
        Dim hrsrList = HRSR.GetParent().HumanResourceSkillRateList
        Dim len = hrsrList.Count
        For i = 0 To len - 1
          If HRSR IsNot hrsrList.ElementAt(i) Then
            Dim RateDate1 = hrsrList.ElementAt(i).RateDate
            Dim RateDate2 = HRSR.RateDate
            If RateDate1.Value = RateDate2.Value Then
              ErrorString = "Skill Rates must be unique by Rate Date"
            End If
          End If
        Next
      End If
      Return ErrorString
    End Function

    Public Shared Function RatePerDayValid(HRSR As HumanResourceSkillRate) As String
      Dim ErrorString = ""
      If HRSR IsNot Nothing Then
        If HRSR.RatePerDay < 0 Then
          ErrorString = "Rate per day cannot be less than zero"
        End If
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceSkillRate() method.

    End Sub

    Public Shared Function NewHumanResourceSkillRate() As HumanResourceSkillRate

      Return DataPortal.CreateChild(Of HumanResourceSkillRate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceSkillRate(dr As SafeDataReader) As HumanResourceSkillRate

      Dim h As New HumanResourceSkillRate()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceSkillRateIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceSkillIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RateDateProperty, .GetValue(2))
          LoadProperty(RatePerDayProperty, .GetDecimal(3))
          LoadProperty(CommentsProperty, .GetString(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(9)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceSkillRate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceSkillRate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSkillRateID As SqlParameter = .Parameters.Add("@HumanResourceSkillRateID", SqlDbType.Int)
          paramHumanResourceSkillRateID.Value = GetProperty(HumanResourceSkillRateIDProperty)
          If Me.IsNew Then
            paramHumanResourceSkillRateID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceSkillID", Me.GetParent().HumanResourceSkillID)
          .Parameters.AddWithValue("@RateDate", (New SmartDate(GetProperty(RateDateProperty))).DBValue)
          .Parameters.AddWithValue("@RatePerDay", GetProperty(RatePerDayProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(HumanResourceSkillRateIDProperty, paramHumanResourceSkillRateID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceSkillRate"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceSkillRateID", GetProperty(HumanResourceSkillRateIDProperty))
        cm.Parameters.AddWithValue("@CurrentUserIsHRManager", Me.GetParent.CurrentUserIsHRManagerServer)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace