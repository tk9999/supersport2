﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSkillRateList
    Inherits OBBusinessListBase(Of HumanResourceSkillRateList, HumanResourceSkillRate)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSkillRateID As Integer) As HumanResourceSkillRate

      For Each child As HumanResourceSkillRate In Me
        If child.HumanResourceSkillRateID = HumanResourceSkillRateID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Skill Rates"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True, Roles:={"HR Manager.Is HR Manager"})> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceSkillID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(HumanResourceID As Integer?, HumanResourceSkillID As Integer?)
        Me.HumanResourceID = HumanResourceID
        Me.HumanResourceSkillID = HumanResourceSkillID
      End Sub

      Public Sub New(HumanResourceSkillID As Integer?)
        Me.HumanResourceSkillID = HumanResourceSkillID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHumanResourceSkillRateList() As HumanResourceSkillRateList

      Return New HumanResourceSkillRateList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHumanResourceSkillList(HumanResourceSkillIDs As String) As HumanResourceSkillList

      Return DataPortal.Fetch(Of HumanResourceSkillList)(New Criteria(HumanResourceSkillIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceSkillRate.GetHumanResourceSkillRate(sdr))
      End While
      Me.RaiseListChangedEvents = True

      For Each child As HumanResourceSkillRate In Me
        child.CheckRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceSkillRateList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceSkillID", NothingDBNull(crit.HumanResourceSkillID))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResourceSkillRate In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResourceSkillRate In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace