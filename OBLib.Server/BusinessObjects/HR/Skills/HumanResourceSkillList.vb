﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSkillList
    Inherits OBBusinessListBase(Of HumanResourceSkillList, HumanResourceSkill)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSkillID As Integer) As HumanResourceSkill

      For Each child As HumanResourceSkill In Me
        If child.HumanResourceSkillID = HumanResourceSkillID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Skills"

    End Function

    Public Function GetHumanResourceSkillPosition(HumanResourceSkillPositionID As Integer) As HumanResourceSkillPosition

      Dim obj As HumanResourceSkillPosition = Nothing
      For Each parent As HumanResourceSkill In Me
        obj = parent.HumanResourceSkillPositionList.GetItem(HumanResourceSkillPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetHumanResourceSkillRate(HumanResourceSkillRateID As Integer) As HumanResourceSkillRate

      Dim obj As HumanResourceSkillRate = Nothing
      For Each parent As HumanResourceSkill In Me
        obj = parent.HumanResourceSkillRateList.GetItem(HumanResourceSkillRateID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property HumanResourceSkillID As Integer? = Nothing
      Public Property HumanResourceSkillIDs As String = ""

      Public Sub New()

      End Sub

      Public Sub New(HumanResourceID As Integer?, HumanResourceSkillID As Integer?)
        Me.HumanResourceID = HumanResourceID
        Me.HumanResourceSkillID = HumanResourceSkillID
      End Sub

      Public Sub New(HumanResourceSkillIDs As String)
        Me.HumanResourceSkillIDs = HumanResourceSkillIDs
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHumanResourceSkillList() As HumanResourceSkillList

      Return New HumanResourceSkillList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHumanResourceSkillList(HumanResourceID As Integer?, HumanResourceSkillID As Integer?) As HumanResourceSkillList

      Return DataPortal.Fetch(Of HumanResourceSkillList)(New Criteria(HumanResourceID, HumanResourceSkillID))

    End Function

    Public Shared Function GetHumanResourceSkillList(HumanResourceSkillIDs As String) As HumanResourceSkillList

      Return DataPortal.Fetch(Of HumanResourceSkillList)(New Criteria(HumanResourceSkillIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceSkill.GetHumanResourceSkill(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentChild As HumanResourceSkill = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.HumanResourceSkillID <> sdr.GetInt32(1) Then
            parentChild = Me.GetItem(sdr.GetInt32(1))
          End If
          parentChild.HumanResourceSkillPositionList.RaiseListChangedEvents = False
          parentChild.HumanResourceSkillPositionList.Add(HumanResourceSkillPosition.GetHumanResourceSkillPosition(sdr))
          parentChild.HumanResourceSkillPositionList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.HumanResourceSkillID <> sdr.GetInt32(1) Then
            parentChild = Me.GetItem(sdr.GetInt32(1))
          End If
          parentChild.HumanResourceSkillRateList.RaiseListChangedEvents = False
          parentChild.HumanResourceSkillRateList.Add(HumanResourceSkillRate.GetHumanResourceSkillRate(sdr))
          parentChild.HumanResourceSkillRateList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As HumanResourceSkill In Me
        child.CheckRules()
        For Each HumanResourceSkillPosition As HumanResourceSkillPosition In child.HumanResourceSkillPositionList
          HumanResourceSkillPosition.CheckRules()
        Next
        For Each HumanResourceSkillRate As HumanResourceSkillRate In child.HumanResourceSkillRateList
          HumanResourceSkillRate.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceSkillList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceSkillID", NothingDBNull(crit.HumanResourceSkillID))
            cm.Parameters.AddWithValue("@HumanResourceSkillIDs", Strings.MakeEmptyDBNull(crit.HumanResourceSkillIDs))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResourceSkill In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResourceSkill In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace