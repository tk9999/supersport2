﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSkill
    Inherits OBBusinessBase(Of HumanResourceSkill)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceSkillBO.HumanResourceSkillBOToString(self)")

    Public Shared HumanResourceSkillIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSkillID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceSkillID() As Integer
      Get
        Return GetProperty(HumanResourceSkillIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    ''' 
    <Required>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="The type of crew that this human resource can be included in a production")>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline that this human resource is skill in"),
    Required(ErrorMessage:="Discipline required"),
    Singular.DataAnnotations.DropDownWeb(GetType(RODisciplineList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared SkillLevelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SkillLevelID, "Skill Level", Nothing)
    ''' <summary>
    ''' Gets and sets the Skill Level value
    ''' </summary>
    <Display(Name:="Skill Level", Description:="The skill level of the human resource. Used when allocating human resource to productions"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSkillLevelList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property SkillLevelID() As Integer?
      Get
        Return GetProperty(SkillLevelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SkillLevelIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Date the person started in this discipline. Used for skill level experience."),
    Required(ErrorMessage:="Start Date required"),
    Singular.DataAnnotations.DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="Last date on which Human Resource can be booked for this skill"),
    Singular.DataAnnotations.DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Is Primary Skill?", False)
    ''' <summary>
    ''' Gets and sets the Primary value
    ''' </summary>
    <Display(Name:="Is Primary Skill?", Description:="Tick indicates the this discipline is the primary one for the HR"),
    Required(ErrorMessage:="Primary required"),
    SetExpression("HumanResourceSkillBO.PrimaryDisciplineSet(self)")>
    Public Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PrimaryIndProperty, Value)
      End Set
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets and sets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader", Description:="Tick indicates that this human resource is skilled to be a crew leader on a production for this discipline"),
    Required(ErrorMessage:="Crew Leader required")>
    Public Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CrewLeaderIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Booking Count", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property BookingCount() As Integer?
      Get
        Return GetProperty(BookingCountProperty)
      End Get
    End Property

    <Browsable(False)>
    Public ReadOnly Property Discipline As String
      Get
        If Me.DisciplineID IsNot Nothing Then
          Dim disc As RODiscipline = OBLib.CommonData.Lists.RODisciplineList.GetItem(Me.DisciplineID)
          If disc IsNot Nothing Then
            Return disc.Discipline
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared PrimaryDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PrimaryDisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline")>
    Public ReadOnly Property PrimaryDisciplineID() As Integer?
      Get
        Return GetProperty(PrimaryDisciplineIDProperty)
      End Get
    End Property

    Public Shared CanChangePrimaryIndicatorProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanChangePrimaryIndicator, "CanChangePrimaryIndicator", False)
    ''' <summary>
    ''' Gets and sets the Crew Leader value
    ''' </summary>
    <Display(Name:="CanChangePrimaryIndicator")>
    Public Property CanChangePrimaryIndicator() As Boolean
      Get
        Return GetProperty(CanChangePrimaryIndicatorProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanChangePrimaryIndicatorProperty, Value)
      End Set
    End Property

    Public Shared CurrentUserIsHRManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CurrentUserIsHRManager, "CurrentUserIsHRManager", False)
    ''' <summary>
    ''' Gets and sets the Crew Leader value
    ''' </summary>
    <Display(Name:="CurrentUserIsHRManager")>
    Public Property CurrentUserIsHRManager() As Boolean
      Get
        Return GetProperty(CurrentUserIsHRManagerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CurrentUserIsHRManagerProperty, Value)
      End Set
    End Property

    Public Shared CurrentUserIsHRManagerServerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CurrentUserIsHRManagerServer, "CurrentUserIsHRManagerServer", False)
    ''' <summary>
    ''' Gets and sets the Crew Leader value
    ''' </summary>
    <Display(Name:="CurrentUserIsHRManager"), Browsable(False)>
    Public ReadOnly Property CurrentUserIsHRManagerServer() As Boolean
      Get
        Return GetProperty(CurrentUserIsHRManagerServerProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared HumanResourceSkillPositionListProperty As PropertyInfo(Of HumanResourceSkillPositionList) = RegisterProperty(Of HumanResourceSkillPositionList)(Function(c) c.HumanResourceSkillPositionList, "Human Resource Skill Position List")
    Public ReadOnly Property HumanResourceSkillPositionList() As HumanResourceSkillPositionList
      Get
        If GetProperty(HumanResourceSkillPositionListProperty) Is Nothing Then
          LoadProperty(HumanResourceSkillPositionListProperty, HR.HumanResourceSkillPositionList.NewHumanResourceSkillPositionList())
        End If
        Return GetProperty(HumanResourceSkillPositionListProperty)
      End Get
    End Property

    Public Shared HumanResourceSkillRateListProperty As PropertyInfo(Of HumanResourceSkillRateList) = RegisterProperty(Of HumanResourceSkillRateList)(Function(c) c.HumanResourceSkillRateList, "Human Resource Skill Rate List")
    Public ReadOnly Property HumanResourceSkillRateList() As HumanResourceSkillRateList
      Get
        If GetProperty(HumanResourceSkillRateListProperty) Is Nothing Then
          LoadProperty(HumanResourceSkillRateListProperty, HR.HumanResourceSkillRateList.NewHumanResourceSkillRateList())
        End If
        Return GetProperty(HumanResourceSkillRateListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResource

      Return CType(CType(Me.Parent, HumanResourceSkillList).Parent, HumanResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSkillIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Skill")
        Else
          Return String.Format("Blank {0}", "Human Resource Skill")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"HumanResourceSkillPositions", "HumanResourceSkillRates"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(PrimaryIndProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceSkillBO.PrimaryDisciplineValid"
      '  .ServerRuleFunction = AddressOf PrimaryDisciplineValid
      'End With

      With AddWebRule(DisciplineIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceSkillBO.CheckDiscipline"
        .ServerRuleFunction = AddressOf CheckDiscipline
      End With

      'With AddWebRule(DisciplineIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HumanResourceSkillBO.HRSkillPositionListUnique
      'End With

      'With AddWebRule(DisciplineIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HumanResourceSkillBO.HRSkillRateListUnique
      'End With

    End Sub

    Public Shared Function PrimaryDisciplineValid(hrs As HumanResourceSkill) As String
      Dim ErrorString = ""
      'If hrs IsNot Nothing AndAlso hrs.Parent IsNot Nothing Then
      '  Dim HRSkillList = hrs.GetParent().HumanResourceSkillList
      '  Dim length = HRSkillList.Count
      '  Dim count = 0
      '  If length > 0 Then
      '    For i = 0 To length - 1
      '      If HRSkillList.ElementAt(i).PrimaryInd Then
      '        count += 1
      '      End If
      '    Next
      '  End If
      '  If count <> 1 Then
      '    ErrorString = "At Least 1 Primary Skill is Required"
      '  End If
      'End If
      Return ErrorString
    End Function

    Public Shared Function CheckDiscipline(hrs As HumanResourceSkill) As String
      Dim ErrorString = ""
      'If hrs IsNot Nothing AndAlso hrs.DisciplineID IsNot Nothing Then
      '  Dim d = CommonData.Lists.RODisciplineList.GetItem(hrs.DisciplineID)
      '  If d IsNot Nothing Then
      '    If d.PositionRequiredInd Then
      '      Dim SkillPosCount = hrs.HumanResourceSkillPositionList.Count
      '      If SkillPosCount = 0 Then
      '        ErrorString = "At least one Skill Position required."
      '      End If
      '    End If
      '  End If
      'End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceSkill() method.

    End Sub

    Public Shared Function NewHumanResourceSkill() As HumanResourceSkill

      Return DataPortal.CreateChild(Of HumanResourceSkill)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceSkill(dr As SafeDataReader) As HumanResourceSkill

      Dim h As New HumanResourceSkill()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceSkillIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SkillLevelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(StartDateProperty, .GetValue(5))
          LoadProperty(EndDateProperty, .GetValue(6))
          LoadProperty(PrimaryIndProperty, .GetBoolean(7))
          LoadProperty(CrewLeaderIndProperty, .GetBoolean(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(PrimaryDisciplineIDProperty, ZeroNothing(.GetInt32(13)))
          LoadProperty(CurrentUserIsHRManagerProperty, .GetBoolean(14))
          LoadProperty(BookingCountProperty, .GetInt32(15))
          LoadProperty(CanChangePrimaryIndicatorProperty, .GetBoolean(16))
          LoadProperty(CurrentUserIsHRManagerServerProperty, CurrentUserIsHRManager)
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceSkill"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceSkill"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSkillID As SqlParameter = .Parameters.Add("@HumanResourceSkillID", SqlDbType.Int)
          paramHumanResourceSkillID.Value = GetProperty(HumanResourceSkillIDProperty)
          If Me.IsNew Then
            paramHumanResourceSkillID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(GetProperty(CrewTypeIDProperty)))
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@SkillLevelID", Singular.Misc.NothingDBNull(GetProperty(SkillLevelIDProperty)))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@PrimaryInd", GetProperty(PrimaryIndProperty))
          .Parameters.AddWithValue("@CrewLeaderInd", GetProperty(CrewLeaderIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceSkillIDProperty, paramHumanResourceSkillID.Value)
          End If
          ' update child objects
          If GetProperty(HumanResourceSkillPositionListProperty) IsNot Nothing Then
            Me.HumanResourceSkillPositionList.Update()
          End If
          If GetProperty(HumanResourceSkillRateListProperty) IsNot Nothing Then
            Me.HumanResourceSkillRateList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(HumanResourceSkillPositionListProperty) IsNot Nothing Then
          Me.HumanResourceSkillPositionList.Update()
        End If
        If GetProperty(HumanResourceSkillRateListProperty) IsNot Nothing Then
          Me.HumanResourceSkillRateList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceSkill"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceSkillID", GetProperty(HumanResourceSkillIDProperty))
        cm.Parameters.AddWithValue("@CurrentUserIsHRManager", GetProperty(CurrentUserIsHRManagerServerProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace