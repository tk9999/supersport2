﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSkillPosition
    Inherits OBBusinessBase(Of HumanResourceSkillPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceSkillPositionBO.ToString(self)")

    Public Shared HumanResourceSkillPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSkillPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public Property HumanResourceSkillPositionID() As Integer
      Get
        Return GetProperty(HumanResourceSkillPositionIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(HumanResourceSkillPositionIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceSkillIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSkillID, "Human Resource Skill", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Skill value
    ''' </summary>
    Public Property HumanResourceSkillID() As Integer?
      Get
        Return GetProperty(HumanResourceSkillIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceSkillIDProperty, value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type"),
    DropDownWeb(GetType(ROSkillProductionTypeList), DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PositionID, Nothing) _
                                                                    .AddSetExpression("HumanResourceSkillPositionBO.PositionIDSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="The camera position that this human resource is skilled in"),
    Required,
    Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel, FilterMethodName:="HumanResourceSkillPositionBO.FilterPositionsByDiscipline")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Date the person started in this position. Used for skill level experience."),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PositionLevelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionLevelID, "Level", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Level value
    ''' </summary>
    <Display(Name:="Level"),
    DropDownWeb(GetType(ROPositionLevelList))>
    Public Property PositionLevelID() As Integer?
      Get
        Return GetProperty(PositionLevelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionLevelIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", CInt(CommonData.Enums.ProductionArea.OB))
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomTypeID, "Room Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:="The type of room that the heading is linked to"),
     DropDownWeb(GetType(RORoomTypeList), DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property RoomTypeID() As Integer?
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="The room that the heading is linked to "),
     DropDownWeb(GetType(RORoomList), DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type"),
    DropDownWeb(GetType(ROPositionTypeList), DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property
    ', FilterMethodName:="HumanResourceSkillPositionBO.FilterByDPA"

#End Region

#Region " Methods "

    Public Function GetParentList() As HumanResourceSkillPositionList

      If Me.Parent Is Nothing Then
        Return Nothing
      Else
        Return CType(Me.Parent, HumanResourceSkillPositionList)
      End If

    End Function

    Public Function GetParent() As HumanResourceSkill

      If GetParentList() IsNot Nothing Then
        Return CType(CType(Me.Parent, HumanResourceSkillPositionList).Parent, HumanResourceSkill)
      End If
      Return Nothing

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSkillPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Skill Position")
        Else
          Return String.Format("Blank {0}", "Human Resource Skill Position")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(PositionIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceSkillPositionBO.PositionIDValid"
        .ServerRuleFunction = AddressOf PositionIDValid
        .AddTriggerProperties({PositionTypeIDProperty, ProductionAreaIDProperty, ProductionTypeIDProperty, RoomTypeIDProperty, RoomIDProperty})
        .AffectedProperties.Add(PositionTypeIDProperty)
        .AffectedProperties.Add(ProductionAreaIDProperty)
        .AffectedProperties.Add(ProductionTypeIDProperty)
        .AffectedProperties.Add(RoomTypeIDProperty)
        .AffectedProperties.Add(RoomIDProperty)
      End With

      'With AddWebRule(ProductionTypeIDProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceSkillPositionBO.HRSkillPositionListUnique"
      '  .ServerRuleFunction = AddressOf HRSkillPositionTypeListUnique
      'End With

      'With AddWebRule(PositionTypeIDProperty)
      '  .AddTriggerProperty(ProductionAreaIDProperty)
      '  .AffectedProperties.Add(ProductionAreaIDProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceSkillPositionBO.CheckPositionType"
      '  .ServerRuleFunction = AddressOf CheckPositionType
      'End With

      'With AddWebRule(ProductionTypeIDProperty)
      '  .AddTriggerProperty(ProductionAreaIDProperty)
      '  .AffectedProperties.Add(ProductionAreaIDProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceSkillPositionBO.CheckProductionType"
      '  .ServerRuleFunction = AddressOf CheckProductionType
      'End With

      'With AddWebRule(RoomTypeIDProperty)
      '  .AddTriggerProperty(ProductionAreaIDProperty)
      '  .AffectedProperties.Add(ProductionAreaIDProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceSkillPositionBO.CheckRoomType"
      '  .ServerRuleFunction = AddressOf CheckRoomType
      'End With

      'With AddWebRule(RoomIDProperty)
      '  .AddTriggerProperty(ProductionAreaIDProperty)
      '  .AffectedProperties.Add(ProductionAreaIDProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceSkillPositionBO.CheckRoom"
      '  .ServerRuleFunction = AddressOf CheckRoom
      'End With

    End Sub

    Public Shared Function PositionIDValid(HumanResourceSkillPosition As HumanResourceSkillPosition) As String

      Dim ErrorMessage As String = ""
      If HumanResourceSkillPosition.PositionID Is Nothing Then
        ErrorMessage = "Position is required"
        'Else
        '  If HumanResourceSkillPosition.GetParent IsNot Nothing Then
        '    Dim settings As List(Of OBLib.HR.ReadOnly.ROSkillsDatabaseSetting) = OBLib.CommonData.Lists.ROSKillsDatabaseSettingList.Where(Function(d) d.DisciplineID = HumanResourceSkillPosition.GetParent.DisciplineID And d.ProductionAreaID = HumanResourceSkillPosition.ProductionAreaID).ToList
        '    If settings.Count = 1 Then
        '      If settings(0).PositionTypeRequired AndAlso HumanResourceSkillPosition.PositionTypeID Is Nothing Then
        '        ErrorMessage &= vbCrLf & vbCrLf & "Position Type is required"
        '      End If
        '      If settings(0).ProductionTypeRequired AndAlso HumanResourceSkillPosition.ProductionTypeID Is Nothing Then
        '        ErrorMessage &= vbCrLf & vbCrLf & "Production Type is required"
        '      End If
        '      If settings(0).RoomTypeRequired AndAlso HumanResourceSkillPosition.RoomTypeID Is Nothing Then
        '        ErrorMessage &= vbCrLf & vbCrLf & "Room Type is required"
        '      End If
        '      If settings(0).RoomRequired AndAlso HumanResourceSkillPosition.RoomID Is Nothing Then
        '        ErrorMessage &= vbCrLf & vbCrLf & "Room is required"
        '      End If
        '    End If
        '  End If
      End If
      Return ErrorMessage

    End Function


    '    Public Shared Function GetCorrectAreaCriteria(HRSP As HumanResourceSkillPosition) As RODisciplineProductionAreaCriteria
    '      Dim crit As New RODisciplineProductionAreaCriteria
    '      Dim temp As New List(Of RODisciplineProductionAreaCriteria)
    '      Dim lst = CommonData.Lists.RODisciplineProductionAreaCriteriaList
    '      For n = 0 To lst.Count - 1
    '        'Find records with correct Discipline
    '        If HRSP.Parent IsNot Nothing Then
    '          If (lst.ElementAtOrDefault(n).DisciplineID = HRSP.GetParent().DisciplineID) Then
    '            temp.Add(lst.ElementAtOrDefault(n))
    '          End If
    '        End If
    '      Next
    '      For i = 0 To temp.Count - 1
    '        'Find records of a Discipline for correct Production Area
    '        If (temp.ElementAtOrDefault(i).ProductionAreaID = HRSP.ProductionAreaID) Then
    '          crit = temp.ElementAtOrDefault(i)
    '          'Found correct criteria, no need to continue
    '          GoTo theEnd
    '        End If
    '      Next

    'theEnd:

    '      Return crit

    '    End Function

    'Public Shared Function HRSkillPositionTypeListUnique(HRSP As HumanResourceSkillPosition) As String
    '  Dim ErrorString = ""
    '  'Dim hrsplist As New HumanResourceSkillPositionList
    '  'Dim len = 0
    '  'If HRSP.Parent IsNot Nothing Then
    '  '  hrsplist = HRSP.GetParent().HumanResourceSkillPositionList
    '  '  len = hrsplist.Count
    '  '  For i = 0 To len - 1
    '  '    If HRSP IsNot hrsplist.ElementAtOrDefault(i) Then
    '  '      Dim ptID1 = hrsplist.ElementAtOrDefault(i).ProductionTypeID()
    '  '      Dim pID1 = hrsplist.ElementAtOrDefault(i).PositionID()
    '  '      Dim ptID2 = HRSP.ProductionTypeID()
    '  '      Dim pID2 = HRSP.PositionID()
    '  '      Dim pSystemID1 = hrsplist.ElementAtOrDefault(i).GetParent().GetParent().SystemID
    '  '      Dim pAreaID1 = hrsplist.ElementAtOrDefault(i).ProductionAreaID
    '  '      Dim pSystemID2 = HRSP.GetParent().GetParent().SystemID
    '  '      Dim pAreaID2 = HRSP.ProductionAreaID
    '  '      If pID1 Is Nothing AndAlso pID2 Is Nothing Then
    '  '        pID1 = 0
    '  '        pID2 = 0
    '  '      End If
    '  '      If (ptID1 = ptID2 AndAlso pID1 = pID2 AndAlso pSystemID1 = pSystemID2 AndAlso pAreaID1 = pAreaID2) Then
    '  '        ErrorString = "Skills must be unique by Production Type, Position, System ID and Production Area ID"
    '  '      End If
    '  '    End If
    '  '  Next
    '  'End If
    '  Return ErrorString
    'End Function

    'Public Shared Function CheckProductionType(HRSP As HumanResourceSkillPosition) As String
    '  Dim ErrorString As String = ""
    '  'If HRSP.GetParent IsNot Nothing Then
    '  '  If HRSP.GetParent.GetParent IsNot Nothing Then
    '  '    If HRSP.GetParent.GetParent.WorksForOBCity AndAlso HRSP.ProductionTypeID Is Nothing Then
    '  '      ErrorString = "Production Type is Required"
    '  '    End If
    '  '  End If
    '  'End If
    '  ''If HRSP IsNot Nothing Then
    '  ''  Dim crit = GetCorrectAreaCriteria(HRSP)
    '  ''  If crit IsNot Nothing Then
    '  ''    If crit.ProductionTypeRequiredInd AndAlso HRSP.ProductionTypeID Is Nothing Then
    '  ''      ErrorString = "Production Type Required"
    '  ''    End If
    '  ''  End If
    '  ''End If
    '  Return ErrorString
    'End Function

    'Public Shared Function CheckPositionType(HRSP As HumanResourceSkillPosition) As String
    '  Dim ErrorString As String = ""
    '  'If HRSP IsNot Nothing Then
    '  '  Dim crit = GetCorrectAreaCriteria(HRSP)
    '  '  If crit IsNot Nothing Then
    '  '    If crit.PositionTypeRequiredInd AndAlso HRSP.PositionTypeID Is Nothing Then
    '  '      ErrorString = "Position Type Required"
    '  '    End If
    '  '  End If
    '  'End If
    '  Return ErrorString
    'End Function

    'Public Shared Function CheckRoomType(HRSP As HumanResourceSkillPosition) As String
    '  Dim ErrorString As String = ""
    '  'If HRSP IsNot Nothing Then
    '  '  Dim crit = GetCorrectAreaCriteria(HRSP)
    '  '  If crit IsNot Nothing Then
    '  '    If crit.RoomTypeRequiredInd AndAlso HRSP.RoomTypeID Is Nothing Then
    '  '      ErrorString = "Room Type Required"
    '  '    End If
    '  '  End If
    '  'End If
    '  Return ErrorString
    'End Function

    'Public Shared Function CheckRoom(HRSP As HumanResourceSkillPosition) As String
    '  Dim ErrorString As String = ""
    '  'If HRSP IsNot Nothing Then
    '  '  Dim crit = GetCorrectAreaCriteria(HRSP)
    '  '  If crit IsNot Nothing Then
    '  '    If crit.RoomRequiredInd AndAlso HRSP.RoomID Is Nothing Then
    '  '      ErrorString = "Room Required"
    '  '    End If
    '  '  End If
    '  'End If
    '  Return ErrorString
    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceSkillPosition() method.

    End Sub

    Public Shared Function NewHumanResourceSkillPosition() As HumanResourceSkillPosition

      Return DataPortal.CreateChild(Of HumanResourceSkillPosition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceSkillPosition(dr As SafeDataReader) As HumanResourceSkillPosition

      Dim h As New HumanResourceSkillPosition()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceSkillPositionIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceSkillIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(StartDateProperty, .GetValue(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(PositionLevelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ProductionTypeProperty, .GetString(14))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceSkillPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceSkillPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSkillPositionID As SqlParameter = .Parameters.Add("@HumanResourceSkillPositionID", SqlDbType.Int)
          paramHumanResourceSkillPositionID.Value = GetProperty(HumanResourceSkillPositionIDProperty)
          If Me.IsNew Then
            paramHumanResourceSkillPositionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceSkillID", Me.GetParent().HumanResourceSkillID)
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@PositionID", NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@PositionLevelID", Singular.Misc.NothingDBNull(GetProperty(PositionLevelIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@PositionTypeID", NothingDBNull(GetProperty(PositionTypeIDProperty)))
          .Parameters.AddWithValue("@RoomTypeID", NothingDBNull(GetProperty(RoomTypeIDProperty)))
          .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceSkillPositionIDProperty, paramHumanResourceSkillPositionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceSkillPosition"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceSkillPositionID", GetProperty(HumanResourceSkillPositionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace