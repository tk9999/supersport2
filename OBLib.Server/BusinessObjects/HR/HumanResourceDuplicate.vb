﻿' Generated 15 Aug 2014 13:41 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceDuplicate
    Inherits OBBusinessBase(Of HumanResourceDuplicate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceDuplicateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceDuplicateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceDuplicateID() As Integer
      Get
        Return GetProperty(HumanResourceDuplicateIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Current Human Resource"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DuplicateHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DuplicateHumanResourceID, "Duplicate Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Duplicate Human Resource value
    ''' </summary>
    <Display(Name:="Duplicate Human Resource", Description:="Duplicated Human Resource"),
    Required(ErrorMessage:="Duplicate Human Resource required")>
    Public Property DuplicateHumanResourceID() As Integer?
      Get
        Return GetProperty(DuplicateHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DuplicateHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared MatchTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MatchTypeID, "Match Type", 0)
    ''' <summary>
    ''' Gets and sets the Match Type value
    ''' </summary>
    <Display(Name:="Match Type", Description:="Match Type for the duplicate"),
    Required(ErrorMessage:="Match Type required")>
    Public Property MatchTypeID() As Integer
      Get
        Return GetProperty(MatchTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MatchTypeIDProperty, Value)
      End Set
    End Property

    Public Shared MatchTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MatchType, "Match Type", "")
    ''' <summary>
    ''' Gets and sets the Match Type value
    ''' </summary>
    <Display(Name:="Match Type", Description:="Match type name"),
    StringLength(50, ErrorMessage:="Match Type cannot be more than 50 characters")>
    Public Property MatchType() As String
      Get
        Return GetProperty(MatchTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MatchTypeProperty, Value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets and sets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="Employee code of the Human Resource tha is already in DB"),
    StringLength(10, ErrorMessage:="Employee Code cannot be more than 10 characters")>
    Public Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmployeeCodeProperty, Value)
      End Set
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets and sets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="ID No. of the Human Resource tha is already in DB"),
    StringLength(15, ErrorMessage:="ID No cannot be more than 15 characters")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IDNoProperty, Value)
      End Set
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets and sets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="Firstname of the Human Resource tha is already in DB"),
    StringLength(50, ErrorMessage:="First Name cannot be more than 50 characters")>
    Public Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FirstNameProperty, Value)
      End Set
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets and sets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="Surname of the Human Resource tha is already in DB"),
    StringLength(50, ErrorMessage:="Surname cannot be more than 50 characters")>
    Public Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SurnameProperty, Value)
      End Set
    End Property

    Public Shared SystemsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Systems, "Systems", "")
    ''' <summary>
    ''' Gets and sets the Systems value
    ''' </summary>
    <Display(Name:="Systems", Description:="System of the Human Resource that is already in DB"),
    StringLength(50, ErrorMessage:="Systems cannot be more than 50 characters")>
    Public Property Systems() As String
      Get
        Return GetProperty(SystemsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemsProperty, Value)
      End Set
    End Property

    Public Shared OrdinalProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Ordinal, "Ordinal", 0)
    ''' <summary>
    ''' Gets and sets the Ordinal value
    ''' </summary>
    <Display(Name:="Ordinal", Description:="Ordinal number"),
    Required(ErrorMessage:="Ordinal required")>
    Public Property Ordinal() As Integer
      Get
        Return GetProperty(OrdinalProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrdinalProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceDuplicateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.MatchType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Duplicate")
        Else
          Return String.Format("Blank {0}", "Human Resource Duplicate")
        End If
      Else
        Return Me.MatchType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceDuplicate() method.

    End Sub

    Public Shared Function NewHumanResourceDuplicate() As HumanResourceDuplicate

      Return DataPortal.CreateChild(Of HumanResourceDuplicate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceDuplicate(dr As SafeDataReader) As HumanResourceDuplicate

      Dim h As New HumanResourceDuplicate()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceDuplicateIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DuplicateHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(MatchTypeIDProperty, .GetInt32(3))
          LoadProperty(MatchTypeProperty, .GetString(4))
          LoadProperty(EmployeeCodeProperty, .GetString(5))
          LoadProperty(IDNoProperty, .GetString(6))
          LoadProperty(FirstNameProperty, .GetString(7))
          LoadProperty(SurnameProperty, .GetString(8))
          LoadProperty(SystemsProperty, .GetString(9))
          LoadProperty(OrdinalProperty, .GetInt32(10))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceDuplicate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceDuplicate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceDuplicateID As SqlParameter = .Parameters.Add("@HumanResourceDuplicateID", SqlDbType.Int)
          paramHumanResourceDuplicateID.Value = GetProperty(HumanResourceDuplicateIDProperty)
          If Me.IsNew Then
            paramHumanResourceDuplicateID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@DuplicateHumanResourceID", GetProperty(DuplicateHumanResourceIDProperty))
          .Parameters.AddWithValue("@MatchTypeID", GetProperty(MatchTypeIDProperty))
          .Parameters.AddWithValue("@MatchType", GetProperty(MatchTypeProperty))
          .Parameters.AddWithValue("@EmployeeCode", GetProperty(EmployeeCodeProperty))
          .Parameters.AddWithValue("@IDNo", GetProperty(IDNoProperty))
          .Parameters.AddWithValue("@FirstName", GetProperty(FirstNameProperty))
          .Parameters.AddWithValue("@Surname", GetProperty(SurnameProperty))
          .Parameters.AddWithValue("@Systems", GetProperty(SystemsProperty))
          .Parameters.AddWithValue("@Ordinal", GetProperty(OrdinalProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceDuplicateIDProperty, paramHumanResourceDuplicateID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceDuplicate"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceDuplicateID", GetProperty(HumanResourceDuplicateIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace