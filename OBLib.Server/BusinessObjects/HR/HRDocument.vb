﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HRDocument
    Inherits Singular.Documents.DocumentProviderBase(Of HRDocument)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsSelectedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsSelected, False)
    ''' <summary>
    ''' Gets the IsProcessing value
    ''' </summary>
    ''' 
    Public Property IsSelected() As Boolean
      Get
        Return GetProperty(IsSelectedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSelectedProperty, value)
      End Set
    End Property

    Public Shared HRDocumentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRDocumentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HRDocumentID() As Integer
      Get
        Return GetProperty(HRDocumentIDProperty)
      End Get
    End Property

    Public Shared HRDocumentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HRDocumentTypeID, "HR Document Type", Nothing)
    ''' <summary>
    ''' Gets and sets the HR Document Type value
    ''' </summary>
    <Display(Name:="Document Type", Description:="Document type"),
    Required(ErrorMessage:="Document Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROHRDocumentTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property HRDocumentTypeID() As Integer?
      Get
        Return GetProperty(HRDocumentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HRDocumentTypeIDProperty, Value)
      End Set
    End Property

    'Public Shared HRDocumentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRDocumentName, "HR Document Name", "")
    ' ''' <summary>
    ' ''' Gets and sets the HR Document Name value
    ' ''' </summary>
    '<Display(Name:="HR Document Name", Description:="Document name"),
    'StringLength(100, ErrorMessage:="HR Document Name cannot be more than 100 characters")>
    'Public Property HRDocumentName() As String
    '  Get
    '    Return GetProperty(HRDocumentNameProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(HRDocumentNameProperty, Value)
    '  End Set
    'End Property

    'Public Shared HRDocumentProperty As PropertyInfo(Of Byte()) = RegisterProperty(Of Byte())(Function(c) c.HRDocument, "HR Document", New Byte())
    ' ''' <summary>
    ' ''' Gets and sets the HR Document value
    ' ''' </summary>
    '<Display(Name:="HR Document", Description:="Document of the human resource"),
    'Required(ErrorMessage:="HR Document required")>
    'Public Property HRDocument() As Byte()
    '  Get
    '    Return GetProperty(HRDocumentProperty)
    '  End Get
    '  Set(ByVal Value As Byte())
    '    SetProperty(HRDocumentProperty, Value)
    '  End Set
    'End Property

    Public Shared ParentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentID, "Parent", 0)
    ''' <summary>
    ''' Gets and sets the Parent value
    ''' </summary>
    <Display(Name:="Parent", Description:="The human resource which the document belongs to"),
    Required(ErrorMessage:="Parent required")>
    Public Property ParentID() As Integer
      Get
        Return GetProperty(ParentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ParentIDProperty, Value)
      End Set
    End Property

    Public Shared ParentTableProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ParentTable, "Parent Table", "HumanResources")
    ''' <summary>
    ''' Gets and sets the Parent Table value
    ''' </summary>
    <Display(Name:="Parent Table", Description:="Parent table name"),
    StringLength(50, ErrorMessage:="Parent Table cannot be more than 50 characters")>
    Public Property ParentTable() As String
      Get
        Return GetProperty(ParentTableProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ParentTableProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="System for whcih the document belongs to")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    '<DisplayNameAttribute("Size")> _
    'Public ReadOnly Property DocumentSize() As String
    '  Get
    '    If GetProperty(DocumentProperty) Is Nothing Then
    '      Return "0 KB"
    '    Else
    '      Return (GetProperty(DocumentProperty).Length / 1024.0).ToString("#,##0.00 KB")
    '    End If
    '  End Get
    'End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResource

      Return CType(CType(Me.Parent, HRDocumentList).Parent, HumanResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HRDocumentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.DocumentName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "HR Document")
        Else
          Return String.Format("Blank {0}", "HR Document")
        End If
      Else
        Return Me.DocumentName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHRDocument() method.

    End Sub

    Public Shared Function NewHRDocument() As HRDocument

      Return DataPortal.CreateChild(Of HRDocument)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Protected Overrides Sub CallSaveDocument()
      Me.SaveDocument()
    End Sub

    Friend Shared Function GetHRDocument(dr As SafeDataReader) As HRDocument

      Dim h As New HRDocument()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HRDocumentIDProperty, .GetInt32(0))
          LoadProperty(HRDocumentTypeIDProperty, .GetInt32(1))
          LoadProperty(DocumentNameProperty, .GetString(2))
          LoadProperty(DocumentProperty, .GetValue(3))
          LoadProperty(ParentIDProperty, .GetInt32(4))
          LoadProperty(ParentTableProperty, .GetString(5))
          LoadProperty(SystemIDProperty, .GetInt32(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(DocumentIDProperty, .GetInt32(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Overrides Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHRDocument"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Overrides Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHRDocument"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      Dim MustSaveBinary As Boolean = False
      If Document IsNot Nothing AndAlso Document.IsDirty Then
        MustSaveBinary = True
      End If

      If Me.IsSelfDirty Or (Document IsNot Nothing AndAlso Document.IsDirty) Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHRDocumentID As SqlParameter = .Parameters.Add("@HRDocumentID", SqlDbType.Int)
          paramHRDocumentID.Value = GetProperty(HRDocumentIDProperty)
          If Me.IsNew Then
            paramHRDocumentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HRDocumentTypeID", GetProperty(HRDocumentTypeIDProperty))
          .Parameters.AddWithValue("@HRDocumentName", GetProperty(DocumentNameProperty))
          '.Parameters.AddWithValue("@HRDocument", GetProperty(HRDocumentProperty))
          If MustSaveBinary Then
            .Parameters.AddWithValue("@HRDocument", Document.Document)
          Else
            .Parameters.AddWithValue("@HRDocument", NothingDBNull(Nothing))
          End If
          If Me.GetParent() IsNot Nothing Then
            .Parameters.AddWithValue("@ParentID", Me.GetParent().HumanResourceID)
          Else
            If Me.ParentID = 0 Then
              Throw New Exception("ParentID was nor provided")
            Else
              .Parameters.AddWithValue("@ParentID", GetProperty(ParentIDProperty))
            End If
          End If
          .Parameters.AddWithValue("@ParentTable", GetProperty(ParentTableProperty)) '"HumanResources")
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HRDocumentIDProperty, paramHRDocumentID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Overrides Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHRDocument"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HRDocumentID", GetProperty(HRDocumentIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace