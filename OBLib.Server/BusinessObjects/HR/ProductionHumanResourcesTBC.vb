﻿' Generated 03 Mar 2014 11:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class ProductionHumanResourcesTBC
    Inherits OBBusinessBase(Of ProductionHumanResourcesTBC)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key(),
    Required(ErrorMessage:="ID required")>
    Public Property ProductionHumanResourceID() As Int32
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Int32)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description", "")
    ''' <summary>
    ''' Gets and sets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionDescriptionProperty, Value)
      End Set
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.TBCInd, False) _
                                                               .AddSetExpression("ProductionHumanResourceTBCBO.TBCSet(self)", False)
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:=""),
    Required(ErrorMessage:="TBC required")>
    Public Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TBCIndProperty, Value)
      End Set
    End Property

    Public Shared IsAvailableProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.IsAvailable, CType(Nothing, Boolean?)) _
                                                                     .AddSetExpression("ProductionHumanResourceTBCBO.IsAvailableSet(self)", False)
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="Available?", Description:="")>
    Public Property IsAvailable() As Boolean?
      Get
        Return GetProperty(IsAvailableProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(IsAvailableProperty, Value)
      End Set
    End Property

    Public Shared TxDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDate, "Tx Date")
    ''' <summary>
    ''' Gets and sets the Tx Date value
    ''' </summary>
    <Display(Name:="Tx Date", Description:=""),
    Required(ErrorMessage:="Tx Date required")>
    Public Property TxDate As DateTime?
      Get
        Return GetProperty(TxDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TxDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResource

      Return CType(CType(Me.Parent, ProductionHumanResourcesTBCList).Parent, HumanResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Human Resources TBC")
        Else
          Return String.Format("Blank {0}", "Production Human Resources TBC")
        End If
      Else
        Return Me.ProductionDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHumanResourcesTBC() method.

    End Sub

    Public Shared Function NewProductionHumanResourcesTBC() As ProductionHumanResourcesTBC

      Return DataPortal.CreateChild(Of ProductionHumanResourcesTBC)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionHumanResourcesTBC(dr As SafeDataReader) As ProductionHumanResourcesTBC

      Dim p As New ProductionHumanResourcesTBC()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionDescriptionProperty, .GetString(3))
          LoadProperty(TBCIndProperty, .GetBoolean(4))
          LoadProperty(TxDateProperty, .GetValue(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionHumanResourcesTBC"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionHumanResourcesTBC"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionHumanResourceID As SqlParameter = .Parameters.Add("@ProductionHumanResourceID", SqlDbType.Int)
          paramProductionHumanResourceID.Value = GetProperty(ProductionHumanResourceIDProperty)
          If Me.IsNew Then
            paramProductionHumanResourceID.Direction = ParameterDirection.Output
          End If
          '.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          '.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          '.Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
          .Parameters.AddWithValue("@TBCInd", GetProperty(TBCIndProperty))
          .Parameters.AddWithValue("@IsAvailable", NothingDBNull(GetProperty(IsAvailableProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          '.Parameters.AddWithValue("@TxDate", (New SmartDate(GetProperty(TxDateProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionHumanResourceIDProperty, paramProductionHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delProductionHumanResourcesTBC"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionHumanResourceID", GetProperty(ProductionHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace