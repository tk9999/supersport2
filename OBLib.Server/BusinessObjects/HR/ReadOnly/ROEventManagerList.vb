﻿' Generated 29 Jul 2014 08:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROEventManagerList
    Inherits OBReadOnlyListBase(Of ROEventManagerList, ROEventManager)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROEventManager

      For Each child As ROEventManager In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property EventManager As String = ""
      Public Property AtDate As DateTime? = Nothing

      Public Sub New(AtDate As DateTime?)
        Me.AtDate = AtDate
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEventManagerList() As ROEventManagerList

      Return New ROEventManagerList()

    End Function

    Public Shared Sub BeginGetROEventManagerList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEventManagerList)))

      Dim dp As New DataPortal(Of ROEventManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEventManagerList(CallBack As EventHandler(Of DataPortalResult(Of ROEventManagerList)))

      Dim dp As New DataPortal(Of ROEventManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEventManagerList() As ROEventManagerList

      Return DataPortal.Fetch(Of ROEventManagerList)(New Criteria())

    End Function

    Public Shared Function GetROEventManagerList(AtDate As DateTime?) As ROEventManagerList

      Return DataPortal.Fetch(Of ROEventManagerList)(New Criteria(AtDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEventManager.GetROEventManager(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEventManagerList"
            cm.Parameters.AddWithValue("@AtDate", NothingDBNull(crit.AtDate))
            cm.Parameters.AddWithValue("@EventManager", Singular.Strings.MakeEmptyDBNull(crit.EventManager))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace