﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceFullList
    Inherits SingularReadOnlyListBase(Of ROHumanResourceFullList, ROHumanResourceFull)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourceFull

      For Each child As ROHumanResourceFull In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resources"

    End Function

    Public Function GetROHumanResourceShiftPattern(HumanResourceShiftPatternID As Integer) As ROHumanResourceShiftPattern

      Dim obj As ROHumanResourceShiftPattern = Nothing
      For Each parent As ROHumanResourceFull In Me
        obj = parent.ROHumanResourceShiftPatternList.GetItem(HumanResourceShiftPatternID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetROHumanResourceSkill(HumanResourceSkillID As Integer) As ROHumanResourceSkill

    '  Dim obj As ROHumanResourceSkill = Nothing
    '  For Each parent As ROHumanResourceFull In Me
    '    obj = parent.ROHumanResourceSkillList.GetItem(HumanResourceSkillID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetROHumanResourceOffPeriod(HumanResourceOffPeriodID As Integer) As ROHumanResourceOffPeriod

    '  Dim obj As ROHumanResourceOffPeriod = Nothing
    '  For Each parent As ROHumanResourceFull In Me
    '    obj = parent.ROHumanResourceOffPeriodList.GetItem(HumanResourceOffPeriodID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(HumanResourceID As Integer?)

        Me.HumanResourceID = HumanResourceID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceFullList() As ROHumanResourceFullList

      Return New ROHumanResourceFullList()

    End Function

    Public Shared Sub BeginGetROHumanResourceFullList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceFullList)))

      Dim dp As New DataPortal(Of ROHumanResourceFullList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourceFullList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceFullList)))

      Dim dp As New DataPortal(Of ROHumanResourceFullList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceFullList() As ROHumanResourceFullList

      Return DataPortal.Fetch(Of ROHumanResourceFullList)(New Criteria())

    End Function

    Public Shared Function GetROHumanResourceFullList(HumanResourceID As Integer?) As ROHumanResourceFullList

      Return DataPortal.Fetch(Of ROHumanResourceFullList)(New Criteria(HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceFull.GetROHumanResourceFull(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROHumanResourceFull = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROHumanResourceShiftPatternList.RaiseListChangedEvents = False
          parent.ROHumanResourceShiftPatternList.Add(ROHumanResourceShiftPattern.GetROHumanResourceShiftPattern(sdr))
          parent.ROHumanResourceShiftPatternList.RaiseListChangedEvents = True
        End While
      End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ROHumanResourceSkillList.RaiseListChangedEvents = False
      '    parent.ROHumanResourceSkillList.Add(ROHumanResourceSkill.GetROHumanResourceSkill(sdr))
      '    parent.ROHumanResourceSkillList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As ROHumanResourceSkill = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.HumanResourceSkillID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetROHumanResourceSkill(sdr.GetInt32(1))
      '    End If
      '    parentChild.ROHumanResourceSkillPositionList.RaiseListChangedEvents = False
      '    parentChild.ROHumanResourceSkillPositionList.Add(ROHumanResourceSkillPosition.GetROHumanResourceSkillPosition(sdr))
      '    parentChild.ROHumanResourceSkillPositionList.RaiseListChangedEvents = True
      '  End While
      'End If
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.HumanResourceSkillID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetROHumanResourceSkill(sdr.GetInt32(1))
      '    End If
      '    parentChild.ROHumanResourceSkillRateList.RaiseListChangedEvents = False
      '    parentChild.ROHumanResourceSkillRateList.Add(ROHumanResourceSkillRate.GetROHumanResourceSkillRate(sdr))
      '    parentChild.ROHumanResourceSkillRateList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ROHumanResourceOffPeriodList.RaiseListChangedEvents = False
      '    parent.ROHumanResourceOffPeriodList.Add(ROHumanResourceOffPeriod.GetROHumanResourceOffPeriod(sdr))
      '    parent.ROHumanResourceOffPeriodList.RaiseListChangedEvents = True
      '  End While
      'End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROHumanResourceSystemList.RaiseListChangedEvents = False
          parent.ROHumanResourceSystemList.Add(ROHumanResourceSystem.GetROHumanResourceSystem(sdr))
          parent.ROHumanResourceSystemList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceFullList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace