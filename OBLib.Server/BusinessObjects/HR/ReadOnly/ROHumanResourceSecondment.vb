﻿' Generated 24 Mar 2015 08:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSecondment
    Inherits OBReadOnlyBase(Of ROHumanResourceSecondment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSecondmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key>
    Public ReadOnly Property HumanResourceSecondmentID() As Integer
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The link to the Human Resource")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="The country to which the human resource was seconded to")>
    Public ReadOnly Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor", Nothing)
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="The debtor to which the employee was seconded to")>
    Public ReadOnly Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostCentreID, "Cost Centre", Nothing)
    ''' <summary>
    ''' Gets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="The cost centre to which the human resource was seconded to")>
    Public ReadOnly Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
    End Property

    Public Shared FromDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FromDate, "From Date")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="From Date", Description:="The date and time at which the secondment begins")>
    Public ReadOnly Property FromDate As DateTime?
      Get
        Return GetProperty(FromDateProperty)
      End Get
    End Property

    Public Shared ToDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ToDate, "To Date")
    ''' <summary>
    ''' Gets the To Date value
    ''' </summary>
    <Display(Name:="To Date", Description:="The date and time at which the secondment ends")>
    Public ReadOnly Property ToDate As DateTime?
      Get
        Return GetProperty(ToDateProperty)
      End Get
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="The user who authorised the secondment")>
    Public ReadOnly Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="Date the secondment was authorised")>
    Public ReadOnly Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
    End Property

    Public Shared ConfirmedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ConfirmedByUserID, "Confirmed By User", Nothing)
    ''' <summary>
    ''' Gets the Confirmed By User value
    ''' </summary>
    <Display(Name:="Confirmed By User", Description:="The user who confirmed the secondment")>
    Public ReadOnly Property ConfirmedByUserID() As Integer?
      Get
        Return GetProperty(ConfirmedByUserIDProperty)
      End Get
    End Property

    Public Shared ConfirmedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ConfirmedDateTime, "Confirmed Date Time")
    ''' <summary>
    ''' Gets the Confirmed Date Time value
    ''' </summary>
    <Display(Name:="Confirmed Date Time", Description:="Date the secondment was confirmed")>
    Public ReadOnly Property ConfirmedDateTime As DateTime?
      Get
        Return GetProperty(ConfirmedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDate, "Created Date", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime?
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Description, "")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="Description")>
    Public ReadOnly Property Description As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Country, "")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="Country")>
    Public ReadOnly Property Country As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared FromDateDisplayProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FromDateDisplay, "")
    <Display(Name:="From Date")>
    Public ReadOnly Property FromDateDisplay As String
      Get
        If FromDate IsNot Nothing Then
          LoadProperty(FromDateDisplayProperty, FromDate.Value.ToString("ddd dd MMM yyyy"))
        Else
          LoadProperty(FromDateDisplayProperty, "")
        End If
        Return GetProperty(FromDateDisplayProperty)
      End Get
    End Property

    Public Shared ToDateDisplayProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ToDateDisplay, "")
    <Display(Name:="To Date")>
    Public ReadOnly Property ToDateDisplay As String
      Get
        If ToDate IsNot Nothing Then
          LoadProperty(ToDateDisplayProperty, ToDate.Value.ToString("ddd dd MMM yyyy"))
        Else
          LoadProperty(ToDateDisplayProperty, "")
        End If
        Return GetProperty(ToDateDisplayProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CreatedByName, "")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByName As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared CreatedDateDisplayProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CreatedDateDisplay, "")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="Created Date")>
    Public ReadOnly Property CreatedDateDisplay As String
      Get
        If CreatedDate IsNot Nothing Then
          Return CreatedDate.Value.ToString("ddd dd MMM yyyy HH:mm")
        Else
          Return ""
        End If
      End Get
    End Property

    Public Shared AuthorisedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AuthorisedByName, "")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="Auth. By")>
    Public ReadOnly Property AuthorisedByName As String
      Get
        Return GetProperty(AuthorisedByNameProperty)
      End Get
    End Property

    Public Shared ConfirmedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ConfirmedByName, "")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="Conf. By")>
    Public ReadOnly Property ConfirmedByName As String
      Get
        Return GetProperty(ConfirmedByNameProperty)
      End Get
    End Property

    Public Shared IsAuthorisedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsAuthorised, False)
    ''' <summary>
    ''' Gets and sets the Second Name value
    ''' </summary>
    <Display(Name:="Auth. Status", Description:="Authorised to be seconded?")>
    Public ReadOnly Property IsAuthorised() As Boolean
      Get
        Return GetProperty(IsAuthorisedProperty)
      End Get
    End Property

    Public Shared IsConfirmedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsConfirmed, False)
    ''' <summary>
    ''' Gets and sets the Second Name value
    ''' </summary>
    <Display(Name:="Conf. Status", Description:="Confirmed to be seconded?")>
    Public ReadOnly Property IsConfirmed() As Boolean
      Get
        Return GetProperty(IsConfirmedProperty)
      End Get
    End Property

    Public ReadOnly Property AuthorisedDetails As String
      Get
        If AuthorisedByUserID IsNot Nothing Then
          Return AuthorisedByName & IIf(AuthorisedDateTime Is Nothing, "", " on " & AuthorisedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm"))
        End If
        Return "Awaiting Auth"
      End Get
    End Property

    <Display(Name:="Conf. Details")>
    Public ReadOnly Property ConfirmedDetails As String
      Get
        If ConfirmedByUserID IsNot Nothing Then
          Return ConfirmedByName & IIf(ConfirmedDateTime Is Nothing, "", " on " & ConfirmedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm"))
        End If
        Return "Awaiting Conf"
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSecondmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDate.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceSecondment(dr As SafeDataReader) As ROHumanResourceSecondment

      Dim r As New ROHumanResourceSecondment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceSecondmentIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(FromDateProperty, .GetValue(5))
        LoadProperty(ToDateProperty, .GetValue(6))
        LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(AuthorisedDateTimeProperty, .GetValue(8))
        LoadProperty(ConfirmedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        LoadProperty(ConfirmedDateTimeProperty, .GetValue(10))
        LoadProperty(CreatedByProperty, .GetInt32(11))
        LoadProperty(CreatedDateProperty, .GetValue(12))
        LoadProperty(ModifiedByProperty, .GetInt32(13))
        LoadProperty(ModifiedDateTimeProperty, .GetValue(14))
        LoadProperty(DescriptionProperty, .GetString(15))
        LoadProperty(CountryProperty, .GetString(16))
        LoadProperty(CreatedByNameProperty, .GetString(17))
        LoadProperty(AuthorisedByNameProperty, .GetString(18))
        LoadProperty(ConfirmedByNameProperty, .GetString(19))
      End With
      If AuthorisedByUserID IsNot Nothing Then
        LoadProperty(IsAuthorisedProperty, True)
      End If
      If ConfirmedByUserID IsNot Nothing Then
        LoadProperty(IsConfirmedProperty, True)
      End If

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace