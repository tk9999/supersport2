﻿' Generated 11 Sep 2016 22:54 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROPlayoutOperatorAvailability
    Inherits OBReadOnlyBase(Of ROPlayoutOperatorAvailability)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROPlayoutOperatorAvailabilityBO.ROPlayoutOperatorAvailabilityBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    'Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ' ''' <summary>
    ' ''' Gets the Email Address value
    ' ''' </summary>
    '<Display(Name:="Email Address", Description:="")>
    'Public ReadOnly Property EmailAddress() As String
    '  Get
    '    Return GetProperty(EmailAddressProperty)
    '  End Get
    'End Property

    'Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address")
    ' ''' <summary>
    ' ''' Gets the Alternative Email Address value
    ' ''' </summary>
    '<Display(Name:="Alternative Email Address", Description:="")>
    'Public ReadOnly Property AlternativeEmailAddress() As String
    '  Get
    '    Return GetProperty(AlternativeEmailAddressProperty)
    '  End Get
    'End Property

    Public Shared IsAvailableProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAvailable, "Is Available", False)
    ''' <summary>
    ''' Gets the Is Available value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailable() As Boolean
      Get
        Return GetProperty(IsAvailableProperty)
      End Get
    End Property

    Public Shared IsAvailableStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsAvailableString, "Is Available String")
    ''' <summary>
    ''' Gets the Is Available String value
    ''' </summary>
    <Display(Name:="Is Available String", Description:="")>
    Public ReadOnly Property IsAvailableString() As String
      Get
        Return GetProperty(IsAvailableStringProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift")
    ''' <summary>
    ''' Gets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public ReadOnly Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared ShiftTimesStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftTimesString, "Shift Times")
    ''' <summary>
    ''' Gets the Shift Times String value
    ''' </summary>
    <Display(Name:="Shift Times", Description:="")>
    Public ReadOnly Property ShiftTimesString() As String
      Get
        Return GetProperty(ShiftTimesStringProperty)
      End Get
    End Property

    Public Shared StartTimeDifferenceProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.StartTimeDifference, "Start Time Difference")
    ''' <summary>
    ''' Gets the Start Time Difference value
    ''' </summary>
    <Display(Name:="Start Time Difference", Description:="")>
    Public ReadOnly Property StartTimeDifference() As Decimal
      Get
        Return GetProperty(StartTimeDifferenceProperty)
      End Get
    End Property

    Public Shared EndTimeDifferenceProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.EndTimeDifference, "End Time Difference")
    ''' <summary>
    ''' Gets the End Time Difference value
    ''' </summary>
    <Display(Name:="End Time Difference", Description:="")>
    Public ReadOnly Property EndTimeDifference() As Decimal
      Get
        Return GetProperty(EndTimeDifferenceProperty)
      End Get
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Hours")
    ''' <summary>
    ''' Gets the Total Hours value
    ''' </summary>
    <Display(Name:="Hours", Description:="")>
    Public ReadOnly Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
    End Property

    Public Shared CurrentShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.CurrentShiftDuration, "Current Duration")
    ''' <summary>
    ''' Gets the Current Shift Duration value
    ''' </summary>
    <Display(Name:="Current Duration", Description:="")>
    Public ReadOnly Property CurrentShiftDuration() As Decimal
      Get
        Return GetProperty(CurrentShiftDurationProperty)
      End Get
    End Property

    Public Shared NewDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.NewDuration, "New Duration")
    ''' <summary>
    ''' Gets the New Duration value
    ''' </summary>
    <Display(Name:="New Duration", Description:="")>
    Public ReadOnly Property NewDuration() As Decimal
      Get
        Return GetProperty(NewDurationProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public ReadOnly Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared RoomScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleTitle, "Title")
    ''' <summary>
    ''' Gets the Room Schedule Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property RoomScheduleTitle() As String
      Get
        Return GetProperty(RoomScheduleTitleProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared RoomChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomChannels, "Room Channels")
    ''' <summary>
    ''' Gets the Room Channels value
    ''' </summary>
    <Display(Name:="Room Channels", Description:="")>
    Public ReadOnly Property RoomChannels() As String
      Get
        Return GetProperty(RoomChannelsProperty)
      End Get
    End Property

    Public Shared NextHumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NextHumanResourceShiftID, "Next Human Resource Shift")
    ''' <summary>
    ''' Gets the Next Human Resource Shift value
    ''' </summary>
    <Display(Name:="Next Human Resource Shift", Description:="")>
    Public ReadOnly Property NextHumanResourceShiftID() As Integer?
      Get
        Return GetProperty(NextHumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared StartDateTime1Property As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime1, "Start Date Time 1")
    ''' <summary>
    ''' Gets the Start Date Time 1 value
    ''' </summary>
    <Display(Name:="Start Date Time 1", Description:="")>
    Public ReadOnly Property StartDateTime1 As DateTime?
      Get
        Return GetProperty(StartDateTime1Property)
      End Get
    End Property

    Public Shared EndDateTime1Property As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime1, "End Date Time 1")
    ''' <summary>
    ''' Gets the End Date Time 1 value
    ''' </summary>
    <Display(Name:="End Date Time 1", Description:="")>
    Public ReadOnly Property EndDateTime1 As DateTime?
      Get
        Return GetProperty(EndDateTime1Property)
      End Get
    End Property

    Public Shared ShiftTypeID1Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID1, "Shift Type ID 1")
    ''' <summary>
    ''' Gets the Shift Type ID 1 value
    ''' </summary>
    <Display(Name:="Shift Type ID 1", Description:="")>
    Public ReadOnly Property ShiftTypeID1() As Integer?
      Get
        Return GetProperty(ShiftTypeID1Property)
      End Get
    End Property

    Public Shared ShiftType1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType1, "Shift Type 1")
    ''' <summary>
    ''' Gets the Shift Type 1 value
    ''' </summary>
    <Display(Name:="Shift Type 1", Description:="")>
    Public ReadOnly Property ShiftType1() As String
      Get
        Return GetProperty(ShiftType1Property)
      End Get
    End Property

    Public Shared NextShiftProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextShift, "Next Shift")
    ''' <summary>
    ''' Gets the Next Shift value
    ''' </summary>
    <Display(Name:="Next Shift", Description:="")>
    Public ReadOnly Property NextShift() As String
      Get
        Return GetProperty(NextShiftProperty)
      End Get
    End Property

    Public Shared TimeToNextShiftProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TimeToNextShift, "Next Shift")
    ''' <summary>
    ''' Gets the Time To Next Shift value
    ''' </summary>
    <Display(Name:="Next Shift", Description:="")>
    Public ReadOnly Property TimeToNextShift() As Decimal
      Get
        Return GetProperty(TimeToNextShiftProperty)
      End Get
    End Property

    Public Shared TimeToNextShiftStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimeToNextShiftString, "Time To Next Shift String")
    ''' <summary>
    ''' Gets the Time To Next Shift String value
    ''' </summary>
    <Display(Name:="Time To Next Shift String", Description:="")>
    Public ReadOnly Property TimeToNextShiftString() As String
      Get
        Return GetProperty(TimeToNextShiftStringProperty)
      End Get
    End Property

    Public Shared ShortfallWarningProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShortfallWarning, "Shortfall")
    ''' <summary>
    ''' Gets the Shortfall Warning value
    ''' </summary>
    <Display(Name:="Shortfall", Description:="")>
    Public ReadOnly Property ShortfallWarning() As Boolean
      Get
        Return GetProperty(ShortfallWarningProperty)
      End Get
    End Property

    Public Shared ShiftHoursWarningProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShiftHoursWarning, "Duration")
    ''' <summary>
    ''' Gets the Shift Hours Warning value
    ''' </summary>
    <Display(Name:="Duration", Description:="")>
    Public ReadOnly Property ShiftHoursWarning() As Boolean
      Get
        Return GetProperty(ShiftHoursWarningProperty)
      End Get
    End Property

    Public Shared TimesheetHoursWarningProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TimesheetHoursWarning, "Timesheet")
    ''' <summary>
    ''' Gets the Timesheet Hours Warning value
    ''' </summary>
    <Display(Name:="Timesheet", Description:="")>
    Public ReadOnly Property TimesheetHoursWarning() As Boolean
      Get
        Return GetProperty(TimesheetHoursWarningProperty)
      End Get
    End Property

    Public Shared TotalHoursStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TotalHoursString, "Total Hours")
    ''' <summary>
    ''' Gets the Time To Next Shift String value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
    Public ReadOnly Property TotalHoursString() As String
      Get
        Return GetProperty(TotalHoursStringProperty)
      End Get
    End Property

    Public Shared ShiftDurationStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftDurationString, "Shift Dur.")
    ''' <summary>
    ''' Gets the Time To Next Shift String value
    ''' </summary>
    <Display(Name:="Shift Dur.", Description:="")>
    Public ReadOnly Property ShiftDurationString() As String
      Get
        Return GetProperty(ShiftDurationStringProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HRName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROPlayoutOperatorAvailability(dr As SafeDataReader) As ROPlayoutOperatorAvailability

      Dim r As New ROPlayoutOperatorAvailability()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HRNameProperty, .GetString(2))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ContractTypeProperty, .GetString(4))
        'LoadProperty(EmailAddressProperty, .GetString(5))
        'LoadProperty(AlternativeEmailAddressProperty, .GetString(6))
        LoadProperty(IsAvailableProperty, .GetBoolean(5))
        LoadProperty(IsAvailableStringProperty, .GetString(6))
        LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(ShiftTypeProperty, .GetString(9))
        LoadProperty(StartDateTimeProperty, .GetValue(10))
        LoadProperty(EndDateTimeProperty, .GetValue(11))
        LoadProperty(ShiftTimesStringProperty, .GetString(12))
        LoadProperty(StartTimeDifferenceProperty, .GetDecimal(13))
        LoadProperty(EndTimeDifferenceProperty, .GetDecimal(14))
        LoadProperty(TotalHoursProperty, .GetDecimal(15))
        LoadProperty(CurrentShiftDurationProperty, .GetDecimal(16))
        LoadProperty(NewDurationProperty, .GetDecimal(17))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(RoomProperty, .GetString(20))
        LoadProperty(RoomScheduleTitleProperty, .GetString(21))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        LoadProperty(RoomChannelsProperty, .GetString(23))
        LoadProperty(NextHumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
        LoadProperty(StartDateTime1Property, .GetValue(25))
        LoadProperty(EndDateTime1Property, .GetValue(26))
        LoadProperty(ShiftTypeID1Property, Singular.Misc.ZeroNothing(.GetInt32(27)))
        LoadProperty(ShiftType1Property, .GetString(28))
        LoadProperty(NextShiftProperty, .GetString(29))
        LoadProperty(TimeToNextShiftProperty, .GetDecimal(30))
        LoadProperty(TimeToNextShiftStringProperty, .GetString(31))
        LoadProperty(ShortfallWarningProperty, .GetBoolean(32))
        LoadProperty(ShiftHoursWarningProperty, .GetBoolean(33))
        LoadProperty(TimesheetHoursWarningProperty, .GetBoolean(34))
        LoadProperty(TotalHoursStringProperty, .GetString(35))
        LoadProperty(ShiftDurationStringProperty, .GetString(36))
      End With

    End Sub

#End Region

  End Class

End Namespace