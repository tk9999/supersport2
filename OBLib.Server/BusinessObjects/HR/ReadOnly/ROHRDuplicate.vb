﻿' Generated 13 Aug 2014 10:07 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class ROHRDuplicate
    Inherits OBBusinessBase(Of ROHRDuplicate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared OHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public Property OHumanResourceID() As Integer
      Get
        Return GetProperty(OHumanResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(OHumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared MatchTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MatchTypeID, "Match Type", Nothing)
    ''' <summary>
    ''' Gets the Match Type value
    ''' </summary>
    <Display(Name:="Match Type", Description:="")>
    Public Property MatchTypeID() As Integer?
      Get
        Return GetProperty(MatchTypeIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(MatchTypeIDProperty, value)
      End Set
    End Property

    Public Shared MatchTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MatchType, "Match Type", "")
    ''' <summary>
    ''' Gets the Match Type value
    ''' </summary>
    <Display(Name:="Match Type", Description:="")>
    Public Property MatchType() As String
      Get
        Return GetProperty(MatchTypeProperty)
      End Get
      Set(value As String)
        SetProperty(MatchTypeProperty, value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(value As String)
        SetProperty(IDNoProperty, value)
      End Set
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
    Public Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
      Set(value As String)
        SetProperty(FirstNameProperty, value)
      End Set
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
      Set(value As String)
        SetProperty(SurnameProperty, value)
      End Set
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System", "")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
      Set(value As String)
        SetProperty(SystemProperty, value)
      End Set
    End Property

    Public Shared OrdinalProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Ordinal, "Ordinal", 0)
    ''' <summary>
    ''' Gets the Ordinal value
    ''' </summary>
    <Display(Name:="Ordinal", Description:="")>
    Public Property Ordinal() As Integer
      Get
        Return GetProperty(OrdinalProperty)
      End Get
      Set(value As Integer)
        SetProperty(OrdinalProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area", "")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(value As String)
        SetProperty(ProductionAreaProperty, value)
      End Set
    End Property

    Public Shared LinkProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Link, "Link", False)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Link"), SetExpression("ROHRDuplicateBO.LinkSet(self)")>
    Public Property Link() As Boolean
      Get
        Return GetProperty(LinkProperty)
      End Get
      Set(value As Boolean)
        SetProperty(LinkProperty, value)
      End Set
    End Property

    Public Shared IsDuplicateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsDuplicate, "IsDuplicate", False)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="IsDuplicate"), SetExpression("ROHRDuplicateBO.IsDuplicateSet(self)")>
    Public Property IsDuplicate() As Boolean
      Get
        Return GetProperty(IsDuplicateProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsDuplicateProperty, value)
      End Set
    End Property

    Public Shared OptionSelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OptionSelected, "IgnoreDuplicate", False)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="OptionSelected"), SetExpression("ROHRDuplicateBO.OptionSelectedSet(self)")>
    Public Property OptionSelected() As Boolean
      Get
        Return GetProperty(OptionSelectedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(OptionSelectedProperty, value)
      End Set
    End Property

    Public Shared IsNotSameHRProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsNotSameHR, "IsNotSameHR", False)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="IsNotSameHR"), SetExpression("ROHRDuplicateBO.IsNotSameHRSet(self)")>
    Public Property IsNotSameHR() As Boolean
      Get
        Return GetProperty(IsNotSameHRProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsNotSameHRProperty, value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Link to Sub-Dept"),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserSystemList), DisplayMember:="System", valueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemIDProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Link to Area"),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList), DisplayMember:="ProductionArea", valueMember:="ProductionAreaID", ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared CurrentHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrentHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property CurrentHumanResourceID() As Integer
      Get
        Return GetProperty(CurrentHumanResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(CurrentHumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Bookings", 0)
    ''' <summary>
    ''' Gets the Ordinal value
    ''' </summary>
    <Display(Name:="Bookings", Description:="")>
    Public Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(BookingCountProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.MatchType

    End Function

#End Region

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(LinkProperty)
        .ServerRuleFunction = AddressOf LinkValid
        .AddTriggerProperties({SystemIDProperty, ProductionAreaIDProperty})
        .AffectedProperties.AddRange({SystemIDProperty, ProductionAreaIDProperty})
        .JavascriptRuleFunctionName = "ROHRDuplicateBO.LinkValid"
      End With

      With AddWebRule(OptionSelectedProperty)
        .ServerRuleFunction = AddressOf OptionSelectedValid
        .AddTriggerProperties({LinkProperty, IsDuplicateProperty, IsNotSameHRProperty})
        .JavascriptRuleFunctionName = "ROHRDuplicateBO.IgnoreDuplicateValid"
      End With

    End Sub

    Public Shared Function LinkValid(ROHRDuplicate As ROHRDuplicate) As String

      If ROHRDuplicate.Link Then
        If (ROHRDuplicate.SystemID Is Nothing Or ROHRDuplicate.ProductionAreaID Is Nothing) AndAlso Not ROHRDuplicate.IsDuplicate Then
          Return "Sub-Dept and Area area are required for linking"
        End If
      End If
      Return ""

    End Function

    Public Shared Function OptionSelectedValid(ROHRDuplicate As ROHRDuplicate) As String

      If Not ROHRDuplicate.OptionSelected Then
        Return "Please select an action"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceBankingDetail() method.

    End Sub

    Public Shared Function NewROHRDuplicate() As ROHRDuplicate

      Return DataPortal.CreateChild(Of ROHRDuplicate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function NewROHRDuplicate(OHumanResourceID As Integer, MatchTypeID As Integer, MatchType As String,
                                        EmployeeCode As String, IDNo As String, FirstName As String, Surname As String,
                                        System As String, Ordinal As Integer) As ROHRDuplicate

      Return New ROHRDuplicate(OHumanResourceID, MatchTypeID, MatchType,
                               EmployeeCode, IDNo, FirstName, Surname, System, Ordinal)

    End Function

    Friend Shared Function GetROHRDuplicate(dr As SafeDataReader) As ROHRDuplicate

      Dim r As New ROHRDuplicate()
      r.Fetch(dr)
      Return r

      Return Nothing

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(OHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(MatchTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(MatchTypeProperty, .GetString(2))
        LoadProperty(EmployeeCodeProperty, .GetString(3))
        LoadProperty(IDNoProperty, .GetString(4))
        LoadProperty(FirstNameProperty, .GetString(5))
        LoadProperty(SurnameProperty, .GetString(6))
        LoadProperty(SystemProperty, .GetString(7))
        LoadProperty(OrdinalProperty, .GetInt32(8))
        LoadProperty(ProductionAreaProperty, .GetString(9))
        LoadProperty(CurrentHumanResourceIDProperty, .GetInt32(10))
        LoadProperty(BookingCountProperty, .GetInt32(11))
      End With

    End Sub

    Private Sub New(OHumanResourceID As Integer, MatchTypeID As Integer, MatchType As String,
                EmployeeCode As String, IDNo As String, FirstName As String, Surname As String,
                System As String, Ordinal As Integer)

      SetProperty(OHumanResourceIDProperty, OHumanResourceID)
      SetProperty(MatchTypeIDProperty, MatchTypeID)
      SetProperty(MatchTypeProperty, MatchType)
      SetProperty(EmployeeCodeProperty, EmployeeCode)
      SetProperty(IDNoProperty, IDNo)
      SetProperty(FirstNameProperty, FirstName)
      SetProperty(SurnameProperty, Surname)
      SetProperty(SystemProperty, System)
      SetProperty(OrdinalProperty, Ordinal)

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insHumanResourceOffPeriod"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updROHRDuplicate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          .Parameters.AddWithValue("@OHumanResourceID", OHumanResourceID)
          .Parameters.AddWithValue("@MatchTypeID", MatchTypeID)
          .Parameters.AddWithValue("@EmployeeCode", EmployeeCode)
          .Parameters.AddWithValue("@IDNo", IDNo)
          .Parameters.AddWithValue("@FirstName", FirstName)
          .Parameters.AddWithValue("@Surname", Surname)
          .Parameters.AddWithValue("@System", System)
          .Parameters.AddWithValue("@ProductionArea", ProductionArea)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemID", NothingDBNull(SystemID))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(ProductionAreaID))
          .Parameters.AddWithValue("@Ordinal", Ordinal)
          .Parameters.AddWithValue("@Link", Link)
          .Parameters.AddWithValue("@IsDuplicate", IsDuplicate)
          .Parameters.AddWithValue("@OptionSelected", OptionSelected)
          .Parameters.AddWithValue("@IsNotSameHR", IsNotSameHR)
          .Parameters.AddWithValue("@CurrentHumanResourceID", CurrentHumanResourceID)
          .ExecuteNonQuery()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delHumanResourceOffPeriod"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", GetProperty(HumanResourceOffPeriodIDProperty))
      '  cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace