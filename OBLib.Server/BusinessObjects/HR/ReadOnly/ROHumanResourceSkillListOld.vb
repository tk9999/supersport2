﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSkillListOld
    Inherits OBReadOnlyListBase(Of ROHumanResourceSkillListOld, ROHumanResourceSkillOld)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSkillID As Integer) As ROHumanResourceSkillOld

      For Each child As ROHumanResourceSkillOld In Me
        If child.HumanResourceSkillID = HumanResourceSkillID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Skills"

    End Function

    'Public Function GetROHumanResourceSkillPosition(HumanResourceSkillPositionID As Integer) As ROHumanResourceSkillPosition

    '  Dim obj As ROHumanResourceSkillOld = Nothing
    '  For Each parent As ROHumanResourceSkillOld In Me
    '    obj = parent.ROHumanResourceSkillPositionList.GetItem(HumanResourceSkillPositionID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetROHumanResourceSkillRate(HumanResourceSkillRateID As Integer) As ROHumanResourceSkillRate

    '  Dim obj As ROHumanResourceSkillRate = Nothing
    '  For Each parent As ROHumanResourceSkill In Me
    '    obj = parent.ROHumanResourceSkillRateList.GetItem(HumanResourceSkillRateID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    Function GetPrimaryDiscipline() As ROHumanResourceSkillOld

      Return Me.Where(Function(d) d.PrimaryInd).FirstOrDefault

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Singular.SingularCriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceSkillListOld() As ROHumanResourceSkillListOld

      Return New ROHumanResourceSkillListOld()

    End Function

    Public Shared Sub BeginGetROResourceScheduleClashList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSkillListOld)))

      Dim dp As New DataPortal(Of ROHumanResourceSkillListOld)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROResourceScheduleClashList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSkillListOld)))

      Dim dp As New DataPortal(Of ROHumanResourceSkillListOld)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceSkillListOld() As ROHumanResourceSkillListOld

      Return DataPortal.Fetch(Of ROHumanResourceSkillListOld)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceSkillOld.GetROHumanResourceSkillOld(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROHumanResourceSkillListOld]"
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace