﻿' Generated 21 Sep 2015 09:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSwapList
    Inherits OBReadOnlyListBase(Of ROHumanResourceSwapList, ROHumanResourceSwap)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceSwapID As Integer) As ROHumanResourceSwap

      For Each child As ROHumanResourceSwap In Me
        If child.HumanResourceSwapID = HumanResourceSwapID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Swaps"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ForHumanResourceID As Integer? = Nothing
      Public Property StartDate As Date? = Nothing
      Public Property EndDate As Date? = Nothing
      Public Property ApproverUserID As Integer? = Nothing

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceSwapList() As ROHumanResourceSwapList

      Return New ROHumanResourceSwapList()

    End Function

    Public Shared Sub BeginGetROHumanResourceSwapList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSwapList)))

      Dim dp As New DataPortal(Of ROHumanResourceSwapList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourceSwapList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSwapList)))

      Dim dp As New DataPortal(Of ROHumanResourceSwapList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceSwapList() As ROHumanResourceSwapList

      Return DataPortal.Fetch(Of ROHumanResourceSwapList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceSwap.GetROHumanResourceSwap(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceSwapList"
            cm.Parameters.AddWithValue("@ForHumanResourceID", Singular.Misc.NothingDBNull(crit.ForHumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ApproverUserID", Singular.Misc.NothingDBNull(crit.ApproverUserID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace