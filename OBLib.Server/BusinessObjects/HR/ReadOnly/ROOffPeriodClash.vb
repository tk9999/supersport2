﻿' Generated 09 Mar 2016 08:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROOffPeriodClash
    Inherits OBReadOnlyBase(Of ROOffPeriodClash)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key, Display(Name:="Booking")>
    Public ReadOnly Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Call Time")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time"), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time"), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time"), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "Wrap Time")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time"), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingDescriptionProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceBookingDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROOffPeriodClash(dr As SafeDataReader) As ROOffPeriodClash

      Dim r As New ROOffPeriodClash()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceBookingDescriptionProperty, .GetString(0))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(1))
        LoadProperty(StartDateTimeProperty, .GetValue(2))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(3))
        LoadProperty(EndDateTimeProperty, .GetValue(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace