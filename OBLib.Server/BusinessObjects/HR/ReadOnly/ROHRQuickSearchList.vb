﻿' Generated 09 Nov 2014 11:52 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHRQuickSearchList
    Inherits SingularReadOnlyListBase(Of ROHRQuickSearchList, ROHRQuickSearch)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHRQuickSearch

      For Each child As ROHRQuickSearch In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemAreaShiftPatternID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property HumanResource As String = ""
      Public Property SelectedHRIDs As String = ""

      Public Sub New(SystemAreaShiftPatternID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?,
                     StartDate As DateTime?, EndDate As DateTime?, HumanResource As String)

        Me.SystemAreaShiftPatternID = SystemAreaShiftPatternID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.HumanResource = HumanResource

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHRQuickSearchList() As ROHRQuickSearchList

      Return New ROHRQuickSearchList()

    End Function

    Public Shared Sub BeginGetROHRQuickSearchList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHRQuickSearchList)))

      Dim dp As New DataPortal(Of ROHRQuickSearchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHRQuickSearchList(CallBack As EventHandler(Of DataPortalResult(Of ROHRQuickSearchList)))

      Dim dp As New DataPortal(Of ROHRQuickSearchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHRQuickSearchList() As ROHRQuickSearchList

      Return DataPortal.Fetch(Of ROHRQuickSearchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHRQuickSearch.GetROHRQuickSearch(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHRQuickSearchList"
            cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", NothingDBNull(crit.SystemAreaShiftPatternID))
            cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID) 'NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID) 'NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@HumanResource", Strings.MakeEmptyDBNull(crit.HumanResource))
            If crit.SelectedHRIDs <> "" Then
              Dim SelectedIds As String() = crit.SelectedHRIDs.Split(",")
              cm.Parameters.AddWithValue("@SelectedHRIDs", OBLib.Helpers.MiscHelper.StringArrayToXML(SelectedIds))
            Else
              cm.Parameters.AddWithValue("@SelectedHRIDs", Strings.MakeEmptyDBNull(crit.SelectedHRIDs))
            End If
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace