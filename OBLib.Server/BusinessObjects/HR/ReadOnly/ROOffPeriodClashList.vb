﻿' Generated 09 Mar 2016 08:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROOffPeriodClashList
    Inherits OBReadOnlyListBase(Of ROOffPeriodClashList, ROOffPeriodClash)

#Region " Business Methods "

    Public Function GetItem(ResourceBookingDescription As String) As ROOffPeriodClash

      For Each child As ROOffPeriodClash In Me
        If child.ResourceBookingDescription = ResourceBookingDescription Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property HumanResourceOffPeriodID As Integer?
      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROOffPeriodClashList() As ROOffPeriodClashList

      Return New ROOffPeriodClashList()

    End Function

    Public Shared Sub BeginGetROOffPeriodClashList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROOffPeriodClashList)))

      Dim dp As New DataPortal(Of ROOffPeriodClashList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROOffPeriodClashList(CallBack As EventHandler(Of DataPortalResult(Of ROOffPeriodClashList)))

      Dim dp As New DataPortal(Of ROOffPeriodClashList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROOffPeriodClashList() As ROOffPeriodClashList

      Return DataPortal.Fetch(Of ROOffPeriodClashList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROOffPeriodClash.GetROOffPeriodClash(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROOffPeriodClashList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", NothingDBNull(crit.HumanResourceOffPeriodID))
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace