﻿' Generated 09 Mar 2016 03:13 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROOtherDisciplinesOnLeave
    Inherits OBReadOnlyBase(Of ROOtherDisciplinesOnLeave)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffReason, "Off Reason")
    ''' <summary>
    ''' Gets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="")>
  Public ReadOnly Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
    End Property

    Public Shared LeaveDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LeaveDetail, "Leave Detail")
    ''' <summary>
    ''' Gets the Leave Detail value
    ''' </summary>
    <Display(Name:="Leave Detail", Description:="")>
  Public ReadOnly Property LeaveDetail() As String
      Get
        Return GetProperty(LeaveDetailProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date"), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date"), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Discipline

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROOtherDisciplinesOnLeave(dr As SafeDataReader) As ROOtherDisciplinesOnLeave

      Dim r As New ROOtherDisciplinesOnLeave()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(DisciplineProperty, .GetString(1))
        LoadProperty(HumanResourceProperty, .GetString(2))
        LoadProperty(OffReasonProperty, .GetString(3))
        LoadProperty(LeaveDetailProperty, .GetString(4))
        LoadProperty(StartDateProperty, .GetValue(5))
        LoadProperty(EndDateProperty, .GetValue(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace