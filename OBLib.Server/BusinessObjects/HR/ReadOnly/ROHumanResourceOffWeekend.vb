﻿' Generated 17 Apr 2015 12:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceOffWeekend
    Inherits SingularReadOnlyBase(Of ROHumanResourceOffWeekend)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared WeekendStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WeekendStartDate, "Weekend Start Date")
    ''' <summary>
    ''' Gets the Weekend Start Date value
    ''' </summary>
    <Display(Name:="Weekend Start Date", Description:="")>
    Public ReadOnly Property WeekendStartDate As DateTime?
      Get
        Return GetProperty(WeekendStartDateProperty)
      End Get
    End Property

    Public Shared WeekendEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WeekendEndDate, "Weekend End Date")
    ''' <summary>
    ''' Gets the Weekend End Date value
    ''' </summary>
    <Display(Name:="Weekend End Date", Description:="")>
    Public ReadOnly Property WeekendEndDate As DateTime?
      Get
        Return GetProperty(WeekendEndDateProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared NameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Name, "Name")
    ''' <summary>
    ''' Gets the Name value
    ''' </summary>
    <Display(Name:="Name", Description:="")>
    Public ReadOnly Property Name() As String
      Get
        Return GetProperty(NameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared ReportingColourProperty As PropertyInfo(Of Drawing.Color) = RegisterProperty(Of Drawing.Color)(Function(c) c.ReportingColour, "Reporting Colour")
    ''' <summary>
    ''' Gets the Reporting Colour value
    ''' </summary>
    <Display(Name:="Reporting Colour", Description:="")>
    Public ReadOnly Property ReportingColour() As Drawing.Color
      Get
        Return GetProperty(ReportingColourProperty)
      End Get
    End Property

    Public Shared ShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftPatternID, "Shift Pattern")
    ''' <summary>
    ''' Gets the Shift Pattern value
    ''' </summary>
    <Display(Name:="Shift Pattern", Description:="")>
    Public ReadOnly Property ShiftPatternID() As Integer
      Get
        Return GetProperty(ShiftPatternIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceOffWeekend(dr As SafeDataReader) As ROHumanResourceOffWeekend

      Dim r As New ROHumanResourceOffWeekend()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(WeekendStartDateProperty, .GetValue(1))
        LoadProperty(WeekendEndDateProperty, .GetValue(2))
        LoadProperty(FirstnameProperty, .GetString(3))
        LoadProperty(PreferredNameProperty, .GetString(4))
        LoadProperty(NameProperty, .GetString(5))
        LoadProperty(SurnameProperty, .GetString(6))
        If Not String.IsNullOrEmpty(.GetString(7)) Then
          LoadProperty(ReportingColourProperty, Drawing.Color.FromName(.GetString(7)))
        End If

        LoadProperty(ShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace