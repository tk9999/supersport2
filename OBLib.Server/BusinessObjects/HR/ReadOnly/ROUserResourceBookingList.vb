﻿' Generated 20 Jun 2015 07:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.PersonnelManager.ReadOnly

  <Serializable()> _
  Public Class ROUserResourceBookingList
    Inherits SingularReadOnlyListBase(Of ROUserResourceBookingList, ROUserResourceBooking)

#Region " Parent "

    <NotUndoable()> Private mParent As ROUserResource
#End Region

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As ROUserResourceBooking

      For Each child As ROUserResourceBooking In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROUserResourceBookingList() As ROUserResourceBookingList

      Return New ROUserResourceBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace