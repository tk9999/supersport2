﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceOffPeriodList
    Inherits OBReadOnlyListBase(Of ROHumanResourceOffPeriodList, ROHumanResourceOffPeriod)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceOffPeriodID As Integer) As ROHumanResourceOffPeriod

      For Each child As ROHumanResourceOffPeriod In Me
        If child.HumanResourceOffPeriodID = HumanResourceOffPeriodID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Off Periods"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceOffPeriodList() As ROHumanResourceOffPeriodList

      Return New ROHumanResourceOffPeriodList()

    End Function

    Public Shared Sub BeginGetROHumanResourceOffPeriodList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceOffPeriodList)))

      Dim dp As New DataPortal(Of ROHumanResourceOffPeriodList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROHumanResourceOffPeriodList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceOffPeriodList)))

      Dim dp As New DataPortal(Of ROHumanResourceOffPeriodList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceOffPeriodList() As ROHumanResourceOffPeriodList

      Return DataPortal.Fetch(Of ROHumanResourceOffPeriodList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceOffPeriod.GetROHumanResourceOffPeriod(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceOffPeriodList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUser.UserID))
            cm.Parameters.AddWithValue("@UserHumanResourceID", NothingDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

#End Region

  End Class

End Namespace