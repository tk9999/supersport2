﻿' Generated 14 Apr 2014 14:00 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

    <Serializable()> _
    Public Class ROHumanResourcePersonnelManager
        Inherits OBReadOnlyBase(Of ROHumanResourcePersonnelManager)

#Region " Properties and Methods "

#Region " Properties "

        Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
        ''' <summary>
        ''' Gets the ID value
        ''' </summary>
        <Display(AutoGenerateField:=False), Key()>
        Public ReadOnly Property HumanResourceID() As Integer
            Get
                Return GetProperty(HumanResourceIDProperty)
            End Get
        End Property

        Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname", "")
        ''' <summary>
        ''' Gets the Firstname value
        ''' </summary>
        <Display(Name:="First Name", Description:="The human resource's first name")>
        Public ReadOnly Property Firstname() As String
            Get
                Return GetProperty(FirstnameProperty)
            End Get
        End Property

        Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name", "")
        ''' <summary>
        ''' Gets the Second Name value
        ''' </summary>
        <Display(Name:="Second Name", Description:="The human resource's second name")>
        Public ReadOnly Property SecondName() As String
            Get
                Return GetProperty(SecondNameProperty)
            End Get
        End Property

        Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
        ''' <summary>
        ''' Gets the Preferred Name value
        ''' </summary>
        <Display(Name:="Preferred Name", Description:="The human resource's preferred name")>
        Public ReadOnly Property PreferredName() As String
            Get
                Return GetProperty(PreferredNameProperty)
            End Get
        End Property

        Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
        ''' <summary>
        ''' Gets the Surname value
        ''' </summary>
        <Display(Name:="Surname", Description:="The human resource's surname")>
        Public ReadOnly Property Surname() As String
            Get
                Return GetProperty(SurnameProperty)
            End Get
        End Property

        Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
        ''' <summary>
        ''' Gets the Manager Human Resource ID 2 value
        ''' </summary>
        ''' 
        <Display(Name:="ResourceID", Description:="A 2nd manager that is allowed to edit Human Resource Skill Rates")>
        Public ReadOnly Property ResourceID() As Integer?
            Get
                Return GetProperty(ResourceIDProperty)
            End Get
        End Property

        Public Shared SelectedIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.SelectedInd, False)
        ''' <summary>
        ''' Gets the Outside Broadcast Ind value
        ''' </summary>
        Public Property SelectedInd() As Boolean
            Get
                Return GetProperty(SelectedIndProperty)
            End Get
            Set(value As Boolean)
                LoadProperty(SelectedIndProperty, value)
            End Set
        End Property

#End Region

#Region " Methods "

        Protected Overrides Function GetIdValue() As Object

            Return GetProperty(HumanResourceIDProperty)

        End Function

        Public Overrides Function ToString() As String

            Return Me.Firstname

        End Function

        Public Shared Function GetROHumanResourcePersonnelManager(dr As SafeDataReader) As ROHumanResourcePersonnelManager

            Dim r As New ROHumanResourcePersonnelManager()
            r.Fetch(dr)
            Return r

        End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

        Friend Shared Function GetROHumanResource(dr As SafeDataReader) As ROHumanResourcePersonnelManager

            Dim r As New ROHumanResourcePersonnelManager()
            r.Fetch(dr)
            Return r

        End Function

        Protected Sub Fetch(sdr As SafeDataReader)

            With sdr
                LoadProperty(HumanResourceIDProperty, .GetInt32(0))
                LoadProperty(FirstnameProperty, .GetString(1))
                LoadProperty(SecondNameProperty, .GetString(2))
                LoadProperty(PreferredNameProperty, .GetString(3))
                LoadProperty(SurnameProperty, .GetString(4))
                LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(5)))
                LoadProperty(SelectedIndProperty, .GetBoolean(6))
            End With
        End Sub

#End If

#End Region

#End Region

    End Class

End Namespace