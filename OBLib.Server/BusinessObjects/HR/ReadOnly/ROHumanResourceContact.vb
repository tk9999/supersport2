﻿' Generated 18 Mar 2016 10:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Text.RegularExpressions

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceContact
    Inherits OBReadOnlyBase(Of ROHumanResourceContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared PreferredFirstSurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredFirstSurname, "Preferred First Surname")
    ''' <summary>
    ''' Gets the Preferred First Surname value
    ''' </summary>
    <Display(Name:="Preferred First Surname", Description:="")>
    Public ReadOnly Property PreferredFirstSurname() As String
      Get
        Return GetProperty(PreferredFirstSurnameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address")
    ''' <summary>
    ''' Gets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alternative Email Address", Description:="")>
    Public ReadOnly Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
    End Property

    Public Shared PhoneEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhoneEmailAddress, "Phone Email Address")
    ''' <summary>
    ''' Gets the Phone Email Address value
    ''' </summary>
    <Display(Name:="Phone Email Address", Description:="")>
    Public ReadOnly Property PhoneEmailAddress() As String
      Get
        Return GetProperty(PhoneEmailAddressProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public ReadOnly Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public ReadOnly Property HasValidEmailAddressPrimary As Boolean
      Get
        Return Singular.Emails.ValidEmailAddress(EmailAddress)
      End Get
    End Property

    Public ReadOnly Property HasValidEmailAddressSecondary As Boolean
      Get
        Return Singular.Emails.ValidEmailAddress(AlternativeEmailAddress)
      End Get
    End Property

    Public ReadOnly Property HasValidMobileNumberPrimary As Boolean
      Get
        Return CheckPhoneNumber(CellPhoneNumber)
      End Get
    End Property

    Public ReadOnly Property HasValidMobileNumberSecondary As Boolean
      Get
        Return CheckPhoneNumber(AlternativeContactNumber)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PreferredFirstSurname

    End Function

    Public Shared Function CheckPhoneNumber(ByVal strPhoneNumber As String) As Boolean
      Dim MatchPhoneNumberPattern As String = "^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"
      If strPhoneNumber IsNot Nothing Then
        Return Regex.IsMatch(strPhoneNumber, MatchPhoneNumberPattern)
      Else
        Return False
      End If
    End Function

    Public Shared Function CheckEmailAddress(ByVal emailAddress As String) As Boolean
      Return Singular.Emails.ValidEmailAddress(emailAddress)
    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceContact(dr As SafeDataReader) As ROHumanResourceContact

      Dim r As New ROHumanResourceContact()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(FirstnameProperty, .GetString(1))
        LoadProperty(SecondNameProperty, .GetString(2))
        LoadProperty(PreferredNameProperty, .GetString(3))
        LoadProperty(SurnameProperty, .GetString(4))
        LoadProperty(PreferredFirstSurnameProperty, .GetString(5))
        LoadProperty(EmployeeCodeProperty, .GetString(6))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(ContractTypeProperty, .GetString(8))
        LoadProperty(EmailAddressProperty, .GetString(9))
        LoadProperty(AlternativeEmailAddressProperty, .GetString(10))
        LoadProperty(PhoneEmailAddressProperty, .GetString(11))
        LoadProperty(CellPhoneNumberProperty, .GetString(12))
        LoadProperty(AlternativeContactNumberProperty, .GetString(13))
        LoadProperty(IDNoProperty, .GetString(14))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(CityProperty, .GetString(16))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(SupplierProperty, .GetString(18))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(RowNoProperty, .GetInt32(20))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace