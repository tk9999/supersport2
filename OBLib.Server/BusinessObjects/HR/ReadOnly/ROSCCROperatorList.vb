﻿' Generated 16 Apr 2016 13:38 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROSCCROperatorList
    Inherits OBReadOnlyListBase(Of ROSCCROperatorList, ROSCCROperator)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROSCCROperator

      For Each child As ROSCCROperator In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property AtDate As DateTime? = Nothing

      Public Sub New(AtDate As DateTime?)
        Me.AtDate = AtDate
      End Sub

    End Class

    Public Shared Function NewROSCCROperatorList() As ROSCCROperatorList

      Return New ROSCCROperatorList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSCCROperatorList(AtDate As DateTime) As ROSCCROperatorList

      Return DataPortal.Fetch(Of ROSCCROperatorList)(New Criteria(AtDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSCCROperator.GetROSCCROperator(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROHumanResourceListSCCROperators]"
            cm.Parameters.AddWithValue("@AtDate", NothingDBNull(crit.AtDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace