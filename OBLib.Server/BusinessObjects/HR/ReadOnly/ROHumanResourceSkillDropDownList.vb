﻿' Generated 19 May 2016 11:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSkillDropDownList
    Inherits OBReadOnlyListBase(Of ROHumanResourceSkillDropDownList, ROHumanResourceSkillDropDown)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSkillID As Integer) As ROHumanResourceSkillDropDown

      For Each child As ROHumanResourceSkillDropDown In Me
        If child.HumanResourceSkillID = HumanResourceSkillID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property Discipline As String = ""

      Public Property HumanResourceID As Integer?
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROHumanResourceSkillDropDownList() As ROHumanResourceSkillDropDownList

      Return New ROHumanResourceSkillDropDownList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROHumanResourceSkillDropDownList() As ROHumanResourceSkillDropDownList

      Return DataPortal.Fetch(Of ROHumanResourceSkillDropDownList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceSkillDropDown.GetROHumanResourceSkillDropDown(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceSkillListDropDown"
            cm.Parameters.AddWithValue("@Discipline", Strings.MakeEmptyDBNull(crit.Discipline))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace