﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

    <Serializable()> _
    Public Class ROHumanResourceSkillPositionListOld
    Inherits SingularReadOnlyListBase(Of ROHumanResourceSkillPositionListOld, ROHumanResourceSkillPositionOld)

#Region " Parent "

    <NotUndoable()> Private mParent As ROHumanResourceSkillPositionOld
#End Region

#Region " Business Methods "

    Public Function GetItem(HumanResourceSkillPositionID As Integer) As ROHumanResourceSkillPositionOld

      For Each child As ROHumanResourceSkillPositionOld In Me
        If child.HumanResourceSkillPositionID = HumanResourceSkillPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

        Public Overrides Function ToString() As String

            Return "Human Resource Skill Positions"

        End Function

#End Region

#Region " Data Access "

#Region " Common "

        Public Shared Function NewROHumanResourceSkillPositionListOld() As ROHumanResourceSkillPositionListOld

            Return New ROHumanResourceSkillPositionListOld()

        End Function

        Public Sub New()

            ' must have parameter-less constructor

        End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

    End Class

End Namespace