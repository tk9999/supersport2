﻿' Generated 21 Feb 2016 00:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()>
  Public Class ROManagerList
    Inherits OBReadOnlyListBase(Of ROManagerList, ROManager)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROManager

      For Each child As ROManager In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property HumanResourceName As String

      Public Property ExcludeManagerHumanResourceIDs As New List(Of Integer)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROManagerList() As ROManagerList

      Return New ROManagerList()

    End Function

    Public Shared Sub BeginGetROManagerList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROManagerList)))

      Dim dp As New DataPortal(Of ROManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROManagerList(CallBack As EventHandler(Of DataPortalResult(Of ROManagerList)))

      Dim dp As New DataPortal(Of ROManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROManagerList() As ROManagerList

      Return DataPortal.Fetch(Of ROManagerList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROManager.GetROManager(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROHumanResourceListManagers]"
            cm.Parameters.AddWithValue("@HumanResource", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceName))
            cm.Parameters.AddWithValue("@ExcludeManagerHumanResourceIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ExcludeManagerHumanResourceIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace