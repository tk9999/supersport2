﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.General.ReadOnly

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceOffPeriod
    Inherits OBReadOnlyBase(Of ROHumanResourceOffPeriod)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceOffPeriodID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceOffPeriodID() As Integer
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared OffReasonIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OffReasonID, "Off Reason", Nothing)
    ''' <summary>
    ''' Gets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="The reason the human resource was off")>
    Public ReadOnly Property OffReasonID() As Integer?
      Get
        Return GetProperty(OffReasonIDProperty)
      End Get
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Detail", "")
    ''' <summary>
    ''' Gets the Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="Description of the off period")>
    Public ReadOnly Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The start date for the off period")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The end date for the off period")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared AuthorisedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisedID, "Authorised", 0)
    ''' <summary>
    ''' Gets the Authorised value
    ''' </summary>
    <Display(Name:="Authorised", Description:="")>
    Public ReadOnly Property AuthorisedID() As Integer
      Get
        Return GetProperty(AuthorisedIDProperty)
      End Get
    End Property

    Public Shared AuthorisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedBy, "Authorised By", Nothing)
    ''' <summary>
    ''' Gets the Authorised By value
    ''' </summary>
    <Display(Name:="Authorised By", Description:="")>
    Public ReadOnly Property AuthorisedBy() As Integer?
      Get
        Return GetProperty(AuthorisedByProperty)
      End Get
    End Property

    Public Shared AuthorisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDate, "Authorised Date")
    ''' <summary>
    ''' Gets the Authorised Date value
    ''' </summary>
    <Display(Name:="Authorised Date", Description:="")>
    Public ReadOnly Property AuthorisedDate As DateTime?
      Get
        Return GetProperty(AuthorisedDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.OffReason, "")
    <Display(Name:="Off Reason")>
    Public ReadOnly Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
    End Property

    Public Shared AuthStatusProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AuthStatus, "")
    <Display(Name:="Status")>
    Public ReadOnly Property AuthStatus() As String
      Get
        Return GetProperty(AuthStatusProperty)
      End Get
    End Property

    Public Shared AuthorisedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AuthorisedByName, "")
    <Display(Name:="Authorised By")>
    Public ReadOnly Property AuthorisedByName() As String
      Get
        Return GetProperty(AuthorisedByNameProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CreatedByName, "")
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    <Display(Name:="Start Date")>
    Public ReadOnly Property StartDateDisplay() As String
      Get
        Return StartDate.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    <Display(Name:="End Date")>
    Public ReadOnly Property EndDateDisplay() As String
      Get
        Return EndDate.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    <Display(Name:="Auth Details")>
    Public ReadOnly Property AuthDetails() As String
      Get
        If AuthorisedDate IsNot Nothing Then
          Return AuthorisedByName & " on " & AuthorisedDate.Value.ToString("ddd dd MMM yyyy HH:mm")
        End If
        Return AuthorisedByName
      End Get
    End Property

    <Display(Name:="Create Details")>
    Public ReadOnly Property CreateDetails() As String
      Get
        Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    Public ReadOnly Property IsPending As Boolean
      Get
        Return CompareSafe(Me.AuthorisedID, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Pending, Integer))
      End Get
    End Property

    Public ReadOnly Property IsRejected As Boolean
      Get
        Return CompareSafe(Me.AuthorisedID, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Rejected, Integer))
      End Get
    End Property

    Public ReadOnly Property IsAuthorised As Boolean
      Get
        Return CompareSafe(Me.AuthorisedID, CType(OBLib.CommonData.Enums.OffPeriodAuthorisationType.Authorised, Integer))
      End Get
    End Property

    Public Shared CanAuthLeaveProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CanAuthLeave, False)
    <Display(Name:="CanAuthLeave")>
    Public ReadOnly Property CanAuthLeave() As Boolean
      Get
        Return GetProperty(CanAuthLeaveProperty)
      End Get
    End Property

    Public Shared CanEditPendingLeaveProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CanEditPendingLeave, False)
    <Display(Name:="CanEditOffPeriod")>
    Public ReadOnly Property CanEditPendingLeave() As Boolean
      Get
        Return GetProperty(CanEditPendingLeaveProperty)
      End Get
    End Property

    Public Shared CanEditOffPeriodProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CanEditOffPeriod, False)
    <Display(Name:="CanEditOffPeriod")>
    Public ReadOnly Property CanEditOffPeriod() As Boolean
      Get
        Return GetProperty(CanEditOffPeriodProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceOffPeriodIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Detail

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceOffPeriod(dr As SafeDataReader) As ROHumanResourceOffPeriod

      Dim r As New ROHumanResourceOffPeriod()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceOffPeriodIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(OffReasonIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DetailProperty, .GetString(3))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(EndDateProperty, .GetValue(5))
        LoadProperty(AuthorisedIDProperty, .GetInt32(6))
        LoadProperty(AuthorisedByProperty, .GetInt32(7))
        LoadProperty(AuthorisedDateProperty, .GetValue(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetValue(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetValue(12))
        LoadProperty(OffReasonProperty, .GetString(13))
        LoadProperty(AuthStatusProperty, .GetString(14))
        LoadProperty(AuthorisedByNameProperty, .GetString(15))
        LoadProperty(CreatedByNameProperty, .GetString(16))
        'RowNo = 17
        LoadProperty(CanAuthLeaveProperty, .GetBoolean(18))
        LoadProperty(CanEditPendingLeaveProperty, .GetBoolean(19))
        LoadProperty(CanEditOffPeriodProperty, .GetBoolean(20))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace