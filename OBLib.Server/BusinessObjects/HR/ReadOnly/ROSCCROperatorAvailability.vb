﻿' Generated 16 Apr 2016 13:38 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROSCCROperatorAvailability
    Inherits OBReadOnlyBase(Of ROSCCROperatorAvailability)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSCCROperatorAvailabilityBO.ROSCCROperatorAvailabilityBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address")
    ''' <summary>
    ''' Gets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alternative Email Address", Description:="")>
    Public ReadOnly Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared RaceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RaceID, "Race")
    ''' <summary>
    ''' Gets the Race value
    ''' </summary>
    <Display(Name:="Race", Description:="")>
    Public ReadOnly Property RaceID() As Integer
      Get
        Return GetProperty(RaceIDProperty)
      End Get
    End Property

    Public Shared GenderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GenderID, "Gender")
    ''' <summary>
    ''' Gets the Gender value
    ''' </summary>
    <Display(Name:="Gender", Description:="")>
    Public ReadOnly Property GenderID() As Integer
      Get
        Return GetProperty(GenderIDProperty)
      End Get
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Active", False)
    ''' <summary>
    ''' Gets the Active value
    ''' </summary>
    <Display(Name:="Active", Description:="")>
    Public ReadOnly Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
    End Property

    Public Shared InactiveReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InactiveReason, "Inactive Reason")
    ''' <summary>
    ''' Gets the Inactive Reason value
    ''' </summary>
    <Display(Name:="Inactive Reason", Description:="")>
    Public ReadOnly Property InactiveReason() As String
      Get
        Return GetProperty(InactiveReasonProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource")
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
    Public ReadOnly Property ManagerHumanResourceID() As Integer
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared NationalityCountyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NationalityCountyID, "Nationality County")
    ''' <summary>
    ''' Gets the Nationality County value
    ''' </summary>
    <Display(Name:="Nationality County", Description:="")>
    Public ReadOnly Property NationalityCountyID() As Integer
      Get
        Return GetProperty(NationalityCountyIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared IsShiftBasedDisciplineProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsShiftBasedDiscipline, "Shift Based", False)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Shift Based", Description:="")>
    Public ReadOnly Property IsShiftBasedDiscipline() As Boolean
      Get
        Return GetProperty(IsShiftBasedDisciplineProperty)
      End Get
    End Property

    Public Shared IsAvailableProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAvailable, "Is Available", False)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailable() As Boolean
      Get
        Return GetProperty(IsAvailableProperty)
      End Get
    End Property

    Public Shared IsAvailableStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsAvailableString, "Is Available", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailableString() As String
      Get
        Return GetProperty(IsAvailableStringProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public ReadOnly Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property StartDateTime() As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property EndDateTime() As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared ShiftStartStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftStartString, "Start Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property ShiftStartString() As String
      Get
        Return GetProperty(ShiftStartStringProperty)
      End Get
    End Property

    Public Shared ShiftEndStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftEndString, "End Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property ShiftEndString() As String
      Get
        Return GetProperty(ShiftEndStringProperty)
      End Get
    End Property

    Public Shared ShiftStartEndStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftStartEndString, "End Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property ShiftStartEndString() As String
      Get
        Return GetProperty(ShiftStartEndStringProperty)
      End Get
    End Property

    Public Shared IsOnShiftProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOnShift, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public ReadOnly Property IsOnShift() As Boolean
      Get
        Return GetProperty(IsOnShiftProperty)
      End Get
    End Property

    Public Shared StartTimeDeficitProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.StartTimeDeficit, 0.0)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Start Deficit", Description:="")>
    Public ReadOnly Property StartTimeDeficit() As Decimal
      Get
        Return GetProperty(StartTimeDeficitProperty)
      End Get
    End Property

    Public Shared EndTimeDeficitProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.EndTimeDeficit, 0.0)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="End Deficit", Description:="")>
    Public ReadOnly Property EndTimeDeficit() As Decimal
      Get
        Return GetProperty(EndTimeDeficitProperty)
      End Get
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.TotalHours, 0.0)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
    Public ReadOnly Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
    End Property

    Public Shared CurrentShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.CurrentShiftDuration, 0.0)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Curr Dur.", Description:="")>
    Public ReadOnly Property CurrentShiftDuration() As Decimal
      Get
        Return GetProperty(CurrentShiftDurationProperty)
      End Get
    End Property

    Public Shared ExpectedShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.ExpectedShiftDuration, 0.0)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="New Dur.", Description:="")>
    Public ReadOnly Property ExpectedShiftDuration() As Decimal
      Get
        Return GetProperty(ExpectedShiftDurationProperty)
      End Get
    End Property

    Public Shared NextHumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NextHumanResourceShiftID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public ReadOnly Property NextHumanResourceShiftID() As Integer?
      Get
        Return GetProperty(NextHumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared NextStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NextStartDateTime, "Start Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property NextStartDateTime() As DateTime?
      Get
        Return GetProperty(NextStartDateTimeProperty)
      End Get
    End Property

    Public Shared NextEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NextEndDateTime, "End Time")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property NextEndDateTime() As DateTime?
      Get
        Return GetProperty(NextEndDateTimeProperty)
      End Get
    End Property

    Public Shared NextShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NextShiftTypeID, "Shift Type")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property NextShiftTypeID() As Integer?
      Get
        Return GetProperty(NextShiftTypeIDProperty)
      End Get
    End Property

    Public Shared NextShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextShiftType, "Shift Type")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property NextShiftType() As String
      Get
        Return GetProperty(NextShiftTypeProperty)
      End Get
    End Property

    Public Shared NextShiftProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextShift, "Next Shift")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Next Shift", Description:="")>
    Public ReadOnly Property NextShift() As String
      Get
        Return GetProperty(NextShiftProperty)
      End Get
    End Property

    Public Shared TimeToNextShiftProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.TimeToNextShift, 0.0)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Time To Next Shift", Description:="")>
    Public ReadOnly Property TimeToNextShift() As Decimal
      Get
        Return GetProperty(TimeToNextShiftProperty)
      End Get
    End Property

    Public Shared TimetoNextShiftStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimetoNextShiftString, "Next Shift")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="DownTime", Description:="")>
    Public ReadOnly Property TimetoNextShiftString() As String
      Get
        Return GetProperty(TimetoNextShiftStringProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSCCROperatorAvailability(dr As SafeDataReader) As ROSCCROperatorAvailability

      Dim r As New ROSCCROperatorAvailability()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(FirstnameProperty, .GetString(1))
        LoadProperty(SecondNameProperty, .GetString(2))
        LoadProperty(PreferredNameProperty, .GetString(3))
        LoadProperty(SurnameProperty, .GetString(4))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(EmailAddressProperty, .GetString(6))
        LoadProperty(AlternativeEmailAddressProperty, .GetString(7))
        LoadProperty(CellPhoneNumberProperty, .GetString(8))
        LoadProperty(AlternativeContactNumberProperty, .GetString(9))
        LoadProperty(IDNoProperty, .GetString(10))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(RaceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(GenderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(ActiveIndProperty, .GetBoolean(14))
        LoadProperty(InactiveReasonProperty, .GetString(15))
        LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(EmployeeCodeProperty, .GetString(17))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(NationalityCountyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(ContractTypeProperty, .GetString(20))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(HRNameProperty, .GetString(22))
        LoadProperty(IsShiftBasedDisciplineProperty, .GetBoolean(23))
        LoadProperty(IsAvailableProperty, .GetBoolean(24))
        LoadProperty(IsAvailableStringProperty, .GetString(25))
        LoadProperty(HumanResourceShiftIDProperty, ZeroNothing(.GetInt32(26)))
        LoadProperty(ShiftTypeIDProperty, ZeroNothing(.GetInt32(27)))
        LoadProperty(ShiftTypeProperty, .GetString(28))
        LoadProperty(StartDateTimeProperty, .GetValue(29))
        LoadProperty(EndDateTimeProperty, .GetValue(30))
        LoadProperty(ShiftStartStringProperty, .GetString(31))
        LoadProperty(ShiftEndStringProperty, .GetString(32))
        LoadProperty(StartTimeDeficitProperty, .GetDecimal(33))
        LoadProperty(EndTimeDeficitProperty, .GetDecimal(34))
        LoadProperty(TotalHoursProperty, .GetDecimal(35))

        LoadProperty(CurrentShiftDurationProperty, .GetDecimal(36))
        LoadProperty(ExpectedShiftDurationProperty, .GetDecimal(37))
        LoadProperty(NextHumanResourceShiftIDProperty, ZeroNothing(.GetInt32(38)))
        LoadProperty(StartDateTimeProperty, .GetValue(39))
        LoadProperty(EndDateTimeProperty, .GetValue(40))
        LoadProperty(ShiftTypeIDProperty, ZeroNothing(.GetInt32(41)))
        LoadProperty(NextShiftTypeProperty, .GetString(42))
        LoadProperty(NextShiftProperty, .GetString(43))
        LoadProperty(TimeToNextShiftProperty, .GetDecimal(44))
        LoadProperty(TimetoNextShiftStringProperty, .GetString(45))
      End With
      If HumanResourceShiftID Is Nothing Then
        LoadProperty(IsOnShiftProperty, False)
      Else
        LoadProperty(IsOnShiftProperty, True)
      End If
      If ShiftEndString <> "" And ShiftEndString <> ":" Then
        LoadProperty(ShiftStartEndStringProperty, ShiftStartString & " - " & ShiftEndString)
      End If

    End Sub

#End Region

  End Class

End Namespace