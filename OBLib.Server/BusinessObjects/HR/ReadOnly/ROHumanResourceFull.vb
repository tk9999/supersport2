﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceFull
    Inherits SingularReadOnlyBase(Of ROHumanResourceFull)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname", "")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="The human resource's first name")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name", "")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="The human resource's second name")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="The human resource's preferred name")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="The human resource's surname")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="True indicates that this person is a contractor and not an employee"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROContractTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="The human resource's email address")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address", "")
    ''' <summary>
    ''' Gets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alternative Email Address", Description:="An alternative email address for the human resource")>
    Public ReadOnly Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
    End Property

    Public Shared PhoneEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhoneEmailAddress, "Phone Email Address", "")
    ''' <summary>
    ''' Gets the Phone Email Address value
    ''' </summary>
    <Display(Name:="Phone Email Address", Description:="This is the email address that will be used to send scheduling information to crew. The crew must always be able to access this address from their phone")>
    Public ReadOnly Property PhoneEmailAddress() As String
      Get
        Return GetProperty(PhoneEmailAddressProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number", "")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="The human resource's cell phone number")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number", "")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="An alternative contact number for the human resource")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="The human resource's unique ID number")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="The city that this HR is based. Used to calculate travel for events"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCityList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared RaceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RaceID, "Race", Nothing)
    ''' <summary>
    ''' Gets the Race value
    ''' </summary>
    <Display(Name:="Race", Description:="The human resource's race"),
    Singular.DataAnnotations.DropDownWeb(GetType(RORaceList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property RaceID() As Integer?
      Get
        Return GetProperty(RaceIDProperty)
      End Get
    End Property

    Public Shared GenderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GenderID, "Gender", Nothing)
    ''' <summary>
    ''' Gets the Gender value
    ''' </summary>
    <Display(Name:="Gender", Description:="The human resource's gender"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROGenderList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property GenderID() As Integer?
      Get
        Return GetProperty(GenderIDProperty)
      End Get
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Active", True)
    ''' <summary>
    ''' Gets the Active value
    ''' </summary>
    <Display(Name:="Active", Description:="Tick indicates that this human resource is currently active and can be used for productions")>
    Public ReadOnly Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
    End Property

    Public Shared InactiveReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InactiveReason, "Inactive Reason", "")
    ''' <summary>
    ''' Gets the Inactive Reason value
    ''' </summary>
    <Display(Name:="Inactive Reason", Description:="If a Human Resource is marked as Not Active then an InactiveReason is required")>
    Public ReadOnly Property InactiveReason() As String
      Get
        Return GetProperty(InactiveReasonProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
    Public ReadOnly Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="The employee code from payroll")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared ReportingColourProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportingColour, "Reporting Colour", "")
    ''' <summary>
    ''' Gets the Reporting Colour value
    ''' </summary>
    <Display(Name:="Reporting Colour", Description:="The colour to use on the Board Report for Event Managers")>
    Public ReadOnly Property ReportingColour() As String
      Get
        Return GetProperty(ReportingColourProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PhotoPathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhotoPath, "Photo Path", "")
    ''' <summary>
    ''' Gets the Photo Path value
    ''' </summary>
    <Display(Name:="Photo Path", Description:="")>
    Public ReadOnly Property PhotoPath() As String
      Get
        Return GetProperty(PhotoPathProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared PantSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PantSize, "Pant Size", "")
    ''' <summary>
    ''' Gets the Pant Size value
    ''' </summary>
    <Display(Name:="Pant Size", Description:="")>
    Public ReadOnly Property PantSize() As String
      Get
        Return GetProperty(PantSizeProperty)
      End Get
    End Property

    Public Shared ShirtSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShirtSize, "Shirt Size", "")
    ''' <summary>
    ''' Gets the Shirt Size value
    ''' </summary>
    <Display(Name:="Shirt Size", Description:="")>
    Public ReadOnly Property ShirtSize() As String
      Get
        Return GetProperty(ShirtSizeProperty)
      End Get
    End Property

    Public Shared JacketSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.JacketSize, "Jacket Size", "")
    ''' <summary>
    ''' Gets the Jacket Size value
    ''' </summary>
    <Display(Name:="Jacket Size", Description:="")>
    Public ReadOnly Property JacketSize() As String
      Get
        Return GetProperty(JacketSizeProperty)
      End Get
    End Property

    Public Shared SAAVoyagerNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SAAVoyagerNo, "SAA Voyager No", "")
    ''' <summary>
    ''' Gets the SAA Voyager No value
    ''' </summary>
    <Display(Name:="SAA Voyager No", Description:="")>
    Public ReadOnly Property SAAVoyagerNo() As String
      Get
        Return GetProperty(SAAVoyagerNoProperty)
      End Get
    End Property

    Public Shared SAAVoyagerTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SAAVoyagerTypeID, "SAA Voyager Type", Nothing)
    ''' <summary>
    ''' Gets the SAA Voyager Type value
    ''' </summary>
    <Display(Name:="SAA Voyager Type", Description:="")>
    Public ReadOnly Property SAAVoyagerTypeID() As Integer?
      Get
        Return GetProperty(SAAVoyagerTypeIDProperty)
      End Get
    End Property

    Public Shared BAVoyagerNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BAVoyagerNo, "BA Voyager No", "")
    ''' <summary>
    ''' Gets the BA Voyager No value
    ''' </summary>
    <Display(Name:="BA Voyager No", Description:="")>
    Public ReadOnly Property BAVoyagerNo() As String
      Get
        Return GetProperty(BAVoyagerNoProperty)
      End Get
    End Property

    Public Shared BAVoyagerTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BAVoyagerTypeID, "BA Voyager Type", Nothing)
    ''' <summary>
    ''' Gets the BA Voyager Type value
    ''' </summary>
    <Display(Name:="BA Voyager Type", Description:="")>
    Public ReadOnly Property BAVoyagerTypeID() As Integer?
      Get
        Return GetProperty(BAVoyagerTypeIDProperty)
      End Get
    End Property

    Public Shared PayHalfMonthIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PayHalfMonthInd, "Pay Half Month", False)
    ''' <summary>
    ''' Gets the Pay Half Month value
    ''' </summary>
    <Display(Name:="Pay Half Month", Description:="")>
    Public ReadOnly Property PayHalfMonthInd() As Boolean
      Get
        Return GetProperty(PayHalfMonthIndProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier", 0)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Employed by a supplier"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSupplierList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared NationalityCountyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NationalityCountyID, "Nationality County", Nothing)
    ''' <summary>
    ''' Gets the Nationality County value
    ''' </summary>
    <Display(Name:="Nationality County", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCountryList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property NationalityCountyID() As Integer?
      Get
        Return GetProperty(NationalityCountyIDProperty)
      End Get
    End Property

    Public Shared ContractDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractDays, "Contract Days", 0)
    ''' <summary>
    ''' Gets the Contract Days value
    ''' </summary>
    <Display(Name:="Contract Days", Description:="")>
    Public ReadOnly Property ContractDays() As Integer
      Get
        Return GetProperty(ContractDaysProperty)
      End Get
    End Property

    Public Shared AddressR1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR1, "Address R 1", "")
    ''' <summary>
    ''' Gets the Address R 1 value
    ''' </summary>
    <Display(Name:="Address R 1", Description:="Residential Address line 1")>
    Public ReadOnly Property AddressR1() As String
      Get
        Return GetProperty(AddressR1Property)
      End Get
    End Property

    Public Shared AddressR2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR2, "Address R 2", "")
    ''' <summary>
    ''' Gets the Address R 2 value
    ''' </summary>
    <Display(Name:="Address R 2", Description:="Residential Address line 2")>
    Public ReadOnly Property AddressR2() As String
      Get
        Return GetProperty(AddressR2Property)
      End Get
    End Property

    Public Shared AddressR3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR3, "Address R 3", "")
    ''' <summary>
    ''' Gets the Address R 3 value
    ''' </summary>
    <Display(Name:="Address R 3", Description:="Residential Address line 3")>
    Public ReadOnly Property AddressR3() As String
      Get
        Return GetProperty(AddressR3Property)
      End Get
    End Property

    Public Shared AddressR4Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR4, "Address R 4", "")
    ''' <summary>
    ''' Gets the Address R 4 value
    ''' </summary>
    <Display(Name:="Address R 4", Description:="Residential Address line 4")>
    Public ReadOnly Property AddressR4() As String
      Get
        Return GetProperty(AddressR4Property)
      End Get
    End Property

    Public Shared PostalCodeRProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalCodeR, "Postal Code R", "")
    ''' <summary>
    ''' Gets the Postal Code R value
    ''' </summary>
    <Display(Name:="Postal Code R", Description:="Residential Address Postal Code")>
    Public ReadOnly Property PostalCodeR() As String
      Get
        Return GetProperty(PostalCodeRProperty)
      End Get
    End Property

    Public Shared AddressP1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP1, "Address P 1", "")
    ''' <summary>
    ''' Gets the Address P 1 value
    ''' </summary>
    <Display(Name:="Address P 1", Description:="Postal Address line 1")>
    Public ReadOnly Property AddressP1() As String
      Get
        Return GetProperty(AddressP1Property)
      End Get
    End Property

    Public Shared AddressP2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP2, "Address P 2", "")
    ''' <summary>
    ''' Gets the Address P 2 value
    ''' </summary>
    <Display(Name:="Address P 2", Description:="Postal Address line 2")>
    Public ReadOnly Property AddressP2() As String
      Get
        Return GetProperty(AddressP2Property)
      End Get
    End Property

    Public Shared AddressP3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP3, "Address P 3", "")
    ''' <summary>
    ''' Gets the Address P 3 value
    ''' </summary>
    <Display(Name:="Address P 3", Description:="Postal Address line 3")>
    Public ReadOnly Property AddressP3() As String
      Get
        Return GetProperty(AddressP3Property)
      End Get
    End Property

    Public Shared AddressP4Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP4, "Address P 4", "")
    ''' <summary>
    ''' Gets the Address P 4 value
    ''' </summary>
    <Display(Name:="Address P 4", Description:="Postal Address line 4")>
    Public ReadOnly Property AddressP4() As String
      Get
        Return GetProperty(AddressP4Property)
      End Get
    End Property

    Public Shared PostalCodePProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalCodeP, "Postal Code P", "")
    ''' <summary>
    ''' Gets the Postal Code P value
    ''' </summary>
    <Display(Name:="Postal Code P", Description:="Postal Address Postal Code")>
    Public ReadOnly Property PostalCodeP() As String
      Get
        Return GetProperty(PostalCodePProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceID2Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID2, "Manager Human Resource ID 2", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource ID 2 value
    ''' </summary>
    <Display(Name:="Manager Human Resource ID 2", Description:="A 2nd manager that is allowed to edit Human Resource Skill Rates")>
    Public ReadOnly Property ManagerHumanResourceID2() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceID2Property)
      End Get
    End Property

    Public Shared TaxNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TaxNumber, "Tax Number", "")
    ''' <summary>
    ''' Gets the Tax Number value
    ''' </summary>
    <Display(Name:="Tax Number", Description:="")>
    Public ReadOnly Property TaxNumber() As String
      Get
        Return GetProperty(TaxNumberProperty)
      End Get
    End Property

    Public Shared InvoiceSupplierIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InvoiceSupplierInd, "Invoice Supplier", False)
    ''' <summary>
    ''' Gets the Invoice Supplier value
    ''' </summary>
    <Display(Name:="Invoice Supplier", Description:="")>
    Public ReadOnly Property InvoiceSupplierInd() As Boolean
      Get
        Return GetProperty(InvoiceSupplierIndProperty)
      End Get
    End Property

    <DisplayNameAttribute("Human Resource")> _
    Public ReadOnly Property PreferredFirstSurname() As String 'switched around to make auto complete to work based on preferred name
      Get
        'Return mFirstname & IIf(mPreferredName.Trim = "", "", " (" & mPreferredName & ")") & " " & mSurname
        '" (" & mPreferredName & ")"
        Return IIf(PreferredName.Trim.Length = 0, Firstname, PreferredName & " (" & Firstname & ")") & " " & Surname
      End Get
    End Property

    Public Shared ManagerHumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerHumanResourceName, "Manager Human Resource Name", "")
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager", Description:="The person managing the human resource")>
    Public ReadOnly Property ManagerHumanResourceName() As String
      Get
        Return GetProperty(ManagerHumanResourceNameProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROHumanResourceShiftPatternListProperty As PropertyInfo(Of ROHumanResourceShiftPatternList) = RegisterProperty(Of ROHumanResourceShiftPatternList)(Function(c) c.ROHumanResourceShiftPatternList, "RO Human Resource Shift Pattern List")
    Public ReadOnly Property ROHumanResourceShiftPatternList() As ROHumanResourceShiftPatternList
      Get
        If GetProperty(ROHumanResourceShiftPatternListProperty) Is Nothing Then
          LoadProperty(ROHumanResourceShiftPatternListProperty, HR.ReadOnly.ROHumanResourceShiftPatternList.NewROHumanResourceShiftPatternList())
        End If
        Return GetProperty(ROHumanResourceShiftPatternListProperty)
      End Get
    End Property

    Public Shared ROHumanResourceSkillListOldProperty As PropertyInfo(Of ROHumanResourceSkillListOld) = RegisterProperty(Of ROHumanResourceSkillListOld)(Function(c) c.ROHumanResourceSkillListOld, "RO Human Resource Skill List")
    Public ReadOnly Property ROHumanResourceSkillListOld() As ROHumanResourceSkillListOld
      Get
        If GetProperty(ROHumanResourceSkillListOldProperty) Is Nothing Then
          LoadProperty(ROHumanResourceSkillListOldProperty, HR.ReadOnly.ROHumanResourceSkillListOld.NewROHumanResourceSkillListOld())
        End If
        Return GetProperty(ROHumanResourceSkillListOldProperty)
      End Get
    End Property

    'Public Shared ROHumanResourceOffPeriodListProperty As PropertyInfo(Of ROHumanResourceOffPeriodList) = RegisterProperty(Of ROHumanResourceOffPeriodList)(Function(c) c.ROHumanResourceOffPeriodList, "RO Human Resource Off Period List")
    'Public ReadOnly Property ROHumanResourceOffPeriodList() As ROHumanResourceOffPeriodList
    '  Get
    '    If GetProperty(ROHumanResourceOffPeriodListProperty) Is Nothing Then
    '      LoadProperty(ROHumanResourceOffPeriodListProperty, HR.ReadOnly.ROHumanResourceOffPeriodList.NewROHumanResourceOffPeriodList())
    '    End If
    '    Return GetProperty(ROHumanResourceOffPeriodListProperty)
    '  End Get
    'End Property

    Public Shared ROHumanResourceSystemListProperty As PropertyInfo(Of ROHumanResourceSystemList) = RegisterProperty(Of ROHumanResourceSystemList)(Function(c) c.ROHumanResourceSystemList, "RO Human Resource Off Period List")
    Public ReadOnly Property ROHumanResourceSystemList() As ROHumanResourceSystemList
      Get
        If GetProperty(ROHumanResourceSystemListProperty) Is Nothing Then
          LoadProperty(ROHumanResourceSystemListProperty, HR.ReadOnly.ROHumanResourceSystemList.NewROHumanResourceSystemList())
        End If
        Return GetProperty(ROHumanResourceSystemListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

    Public Function IsSkilled(ByVal ProductionTypeID As Object, ByVal DisciplineID As Integer, ByVal PositionID As Object, ByVal AtDate As Date) As Boolean

      For Each hrs As ROHumanResourceSkillOld In ROHumanResourceSkillListOld
        If AtDate >= hrs.StartDate AndAlso _
          (Not hrs.EndDate.HasValue OrElse AtDate <= CDate(hrs.EndDate)) AndAlso _
          Singular.Misc.CompareSafe(DisciplineID, hrs.DisciplineID) Then

          'we now know that this person is skilled in the discipline at the AtDate

          Dim d As Maintenance.General.ReadOnly.RODiscipline = CommonData.Lists.RODisciplineList.GetItem(DisciplineID)
          If Not d.PositionRequiredInd Then
            'no position is required and this person is skilled in the discipline
            Return True
          ElseIf Singular.Misc.IsNullNothing(ProductionTypeID) AndAlso Singular.Misc.IsNullNothing(PositionID) Then
            'you are obviously only wanting to check to see if the person is skilled in the discipline and don't care about position or production type
            Return True
          End If
          For Each hrsp As ROHumanResourceSkillPositionOld In hrs.ROHumanResourceSkillPositionListOld
            'has the person acquired the skill after the AtDate
            'are we testing for ProductionTypeID? Yes? Are they skilled for this production type
            'are we testing for PositionID? Yes? Are they skilled for this position
            If CDate(hrsp.StartDate) <= AtDate AndAlso _
              (Singular.Misc.IsNullNothing(ProductionTypeID) OrElse Not Singular.Misc.IsNullNothing(ProductionTypeID) AndAlso hrsp.ProductionTypeID = ProductionTypeID) AndAlso _
              (Singular.Misc.IsNullNothing(PositionID) OrElse Not Singular.Misc.IsNullNothing(PositionID) AndAlso hrsp.PositionID = PositionID) Then
              Return True
            End If
          Next
        End If
      Next

      Return False

    End Function

    'Public Function IsEventManager(ByVal AtDate As Date) As Boolean

    '  Dim ROHRSkill As List(Of ROHumanResourceSkill) = (From Skill In Me.ROHumanResourceSkillList
    '                                                    Where AtDate >= Skill.StartDate AndAlso _
    '                                                    (Not Skill.EndDate.HasValue OrElse AtDate <= CDate(Skill.EndDate)) AndAlso _
    '                                                    (Singular.Misc.CompareSafe(CType(CommonData.Enums.Discipline.EventManager, Integer), Skill.DisciplineID) _
    '                                                     Or Singular.Misc.CompareSafe(CType(CommonData.Enums.Discipline.EventManagerIntern, Integer), Skill.DisciplineID))).ToList

    '  If ROHRSkill.Count > 0 Then
    '    Return True
    '  Else
    '    Return False
    '  End If

    'End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceFull(dr As SafeDataReader) As ROHumanResourceFull

      Dim r As New ROHumanResourceFull()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(FirstnameProperty, .GetString(1))
        LoadProperty(SecondNameProperty, .GetString(2))
        LoadProperty(PreferredNameProperty, .GetString(3))
        LoadProperty(SurnameProperty, .GetString(4))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(EmailAddressProperty, .GetString(6))
        LoadProperty(AlternativeEmailAddressProperty, .GetString(7))
        LoadProperty(PhoneEmailAddressProperty, .GetString(8))
        LoadProperty(CellPhoneNumberProperty, .GetString(9))
        LoadProperty(AlternativeContactNumberProperty, .GetString(10))
        LoadProperty(IDNoProperty, .GetString(11))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(RaceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(GenderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(ActiveIndProperty, .GetBoolean(15))
        LoadProperty(InactiveReasonProperty, .GetString(16))
        LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(EmployeeCodeProperty, .GetString(18))
        LoadProperty(ReportingColourProperty, .GetString(19))
        LoadProperty(CreatedByProperty, .GetInt32(20))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(21))
        LoadProperty(ModifiedByProperty, .GetInt32(22))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(23))
        LoadProperty(PhotoPathProperty, .GetString(24))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
        LoadProperty(PantSizeProperty, .GetString(26))
        LoadProperty(ShirtSizeProperty, .GetString(27))
        LoadProperty(JacketSizeProperty, .GetString(28))
        LoadProperty(SAAVoyagerNoProperty, .GetString(29))
        LoadProperty(SAAVoyagerTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
        LoadProperty(BAVoyagerNoProperty, .GetString(31))
        LoadProperty(BAVoyagerTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(32)))
        LoadProperty(PayHalfMonthIndProperty, .GetBoolean(33))
        LoadProperty(SupplierIDProperty, .GetInt32(34))
        LoadProperty(NationalityCountyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(35)))
        LoadProperty(ContractDaysProperty, .GetInt32(36))
        LoadProperty(AddressR1Property, .GetString(37))
        LoadProperty(AddressR2Property, .GetString(38))
        LoadProperty(AddressR3Property, .GetString(39))
        LoadProperty(AddressR4Property, .GetString(40))
        LoadProperty(PostalCodeRProperty, .GetString(41))
        LoadProperty(AddressP1Property, .GetString(42))
        LoadProperty(AddressP2Property, .GetString(43))
        LoadProperty(AddressP3Property, .GetString(44))
        LoadProperty(AddressP4Property, .GetString(45))
        LoadProperty(PostalCodePProperty, .GetString(46))
        LoadProperty(ManagerHumanResourceID2Property, Singular.Misc.ZeroNothing(.GetInt32(47)))
        LoadProperty(TaxNumberProperty, .GetString(48))
        LoadProperty(InvoiceSupplierIndProperty, .GetBoolean(49))
        LoadProperty(ManagerHumanResourceNameProperty, .GetString(50))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace