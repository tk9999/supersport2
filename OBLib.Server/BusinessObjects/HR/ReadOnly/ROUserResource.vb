﻿' Generated 20 Jun 2015 07:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.PersonnelManager.ReadOnly

  <Serializable()> _
  Public Class ROUserResource
    Inherits OBReadOnlyBase(Of ROUserResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name")
    ''' <summary>
    ''' Gets the Resource Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:="")>
    Public ReadOnly Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared RowNumProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNum, "Row Num")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Row Num", Description:="")>
    Public ReadOnly Property RowNum() As Integer
      Get
        Return GetProperty(RowNumProperty)
      End Get
    End Property

    Public Shared EstShiftsProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.EstShifts, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property EstShifts() As Decimal
      Get
        Return GetProperty(EstShiftsProperty)
      End Get
    End Property

    Public Shared EstHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.EstHours, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property EstHours() As Decimal
      Get
        Return GetProperty(EstHoursProperty)
      End Get
    End Property

    Public Shared TShiftsProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TShifts, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property TShifts() As Decimal
      Get
        Return GetProperty(TShiftsProperty)
      End Get
    End Property

    Public Shared THoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.THours, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property THours() As Decimal
      Get
        Return GetProperty(THoursProperty)
      End Get
    End Property

    Public Shared CanSwapWithCurrentUserProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.CanSwapWithCurrentUser, False)
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="CanSwapWithCurrentUser", Description:="")>
    Public ReadOnly Property CanSwapWithCurrentUser() As Boolean
      Get
        Return GetProperty(CanSwapWithCurrentUserProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROUserResourceBookingListProperty As PropertyInfo(Of ROUserResourceBookingList) = RegisterProperty(Of ROUserResourceBookingList)(Function(c) c.ROUserResourceBookingList, "ROPM Resource Booking List")

    Public ReadOnly Property ROUserResourceBookingList() As ROUserResourceBookingList
      Get
        If GetProperty(ROUserResourceBookingListProperty) Is Nothing Then
          LoadProperty(ROUserResourceBookingListProperty, OBLib.HR.PersonnelManager.ReadOnly.ROUserResourceBookingList.NewROUserResourceBookingList())
        End If
        Return GetProperty(ROUserResourceBookingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceName

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserResource(dr As SafeDataReader) As ROUserResource

      Dim r As New ROUserResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceNameProperty, .GetString(1))
        LoadProperty(ResourceTypeIDProperty, .GetInt32(2))
        LoadProperty(HumanResourceIDProperty, ZeroNothing(.GetInt32(3)))
        LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(4)))
        LoadProperty(VehicleIDProperty, ZeroNothing(.GetInt32(5)))
        LoadProperty(EquipmentIDProperty, ZeroNothing(.GetInt32(6)))
        LoadProperty(ChannelIDProperty, ZeroNothing(.GetInt32(7)))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetValue(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetValue(11))
        LoadProperty(ContractTypeIDProperty, ZeroNothing(.GetInt32(12)))
        LoadProperty(ContractTypeProperty, .GetString(13))
        LoadProperty(RowNumProperty, .GetInt32(14))
        LoadProperty(EstShiftsProperty, .GetDecimal(15))
        LoadProperty(EstHoursProperty, .GetDecimal(16))
        LoadProperty(TShiftsProperty, .GetDecimal(17))
        LoadProperty(THoursProperty, .GetDecimal(18))
        LoadProperty(CanSwapWithCurrentUserProperty, .GetBoolean(19))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace