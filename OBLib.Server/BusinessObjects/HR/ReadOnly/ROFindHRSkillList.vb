﻿' Generated 22 Aug 2014 07:57 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROFindHRSkillList
    Inherits OBReadOnlyListBase(Of ROFindHRSkillList, ROFindHRSkill)

#Region " Business Methods "

    Public Function GetItem(Discipline As String) As ROFindHRSkill

      For Each child As ROFindHRSkill In Me
        If child.Discipline = Discipline Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(HumanResourceID As Integer?)

        Me.HumanResourceID = HumanResourceID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROFindHRSkillList() As ROFindHRSkillList

      Return New ROFindHRSkillList()

    End Function

    Public Shared Sub BeginGetROFindHRSkillList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROFindHRSkillList)))

      Dim dp As New DataPortal(Of ROFindHRSkillList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROFindHRSkillList(CallBack As EventHandler(Of DataPortalResult(Of ROFindHRSkillList)))

      Dim dp As New DataPortal(Of ROFindHRSkillList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROFindHRSkillList(HumanResourceID As Integer?) As ROFindHRSkillList

      Return DataPortal.Fetch(Of ROFindHRSkillList)(New Criteria(HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFindHRSkill.GetROFindHRSkill(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFindHRSkillList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace