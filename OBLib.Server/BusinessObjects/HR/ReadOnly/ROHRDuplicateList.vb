﻿' Generated 13 Aug 2014 10:07 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class ROHRDuplicateList
    Inherits OBBusinessListBase(Of ROHRDuplicateList, ROHRDuplicate)

#Region " Business Methods "

    Public Function GetItem(OHumanResourceID As Integer) As ROHRDuplicate

      For Each child As ROHRDuplicate In Me
        If child.OHumanResourceID = OHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property MatchTypeID As Integer? = Nothing
      Public Property Firstname As String = ""
      Public Property Surname As String = ""
      Public Property IDNo As String = ""
      Public Property EmployeeCode As String = ""

      Public Sub New()


      End Sub

      Public Sub New(HumanResourceID As Integer, MatchTypeID As Integer, Firstname As String, Surname As String, IDNo As String, EmployeeCode As String)

        Me.HumanResourceID = HumanResourceID
        Me.MatchTypeID = MatchTypeID
        Me.Firstname = Firstname
        Me.Surname = Surname
        Me.IDNo = IDNo
        Me.EmployeeCode = EmployeeCode

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHRDuplicateList() As ROHRDuplicateList

      Return New ROHRDuplicateList()

    End Function

    Public Shared Sub BeginGetROHRDuplicateList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHRDuplicateList)))

      Dim dp As New DataPortal(Of ROHRDuplicateList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHRDuplicateList(CallBack As EventHandler(Of DataPortalResult(Of ROHRDuplicateList)))

      Dim dp As New DataPortal(Of ROHRDuplicateList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHRDuplicateList(HumanResourceID As Integer?, MatchTypeID As Integer?, Firstname As String, Surname As String, IDNo As String, EmployeeCode As String) As ROHRDuplicateList

      Return DataPortal.Fetch(Of ROHRDuplicateList)(New Criteria(HumanResourceID, MatchTypeID, Firstname, Surname, IDNo, EmployeeCode))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      'Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHRDuplicate.GetROHRDuplicate(sdr))
      End While
      'Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHRDuplicateList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@MatchTypeID", NothingDBNull(crit.MatchTypeID))
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.Firstname))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@IDNo", Singular.Strings.MakeEmptyDBNull(crit.IDNo))
            cm.Parameters.AddWithValue("@EmployeeCode", Singular.Strings.MakeEmptyDBNull(crit.EmployeeCode))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ROHRDuplicate In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ROHRDuplicate In Me
          'If Child.IsNew Then
          '  Child.Insert()
          'Else
          Child.Update()
          ' End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace