﻿' Generated 18 Mar 2016 10:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceContactList
    Inherits OBReadOnlyListBase(Of ROHumanResourceContactList, ROHumanResourceContact)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourceContact

      For Each child As ROHumanResourceContact In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "Firstname", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Firstname"), TextField(False, False, True), PrimarySearchField>
      Public Overridable Property FirstName() As String
        Get
          Return ReadProperty(FirstNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(FirstNameProperty, Value)
        End Set
      End Property

      Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Surname"), TextField(False, False, True)>
      Public Overridable Property Surname() As String
        Get
          Return ReadProperty(SurnameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SurnameProperty, Value)
        End Set
      End Property

      Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Preferred Name"), TextField(False, False, True)>
      Public Overridable Property PreferredName() As String
        Get
          Return ReadProperty(PreferredNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PreferredNameProperty, Value)
        End Set
      End Property

      Public Property DisciplineIDs As List(Of Integer)
      Public Property ContractTypeIDs As List(Of Integer)
      Public Property SystemIDs As List(Of Integer)
      Public Property ProductionAreaIDs As List(Of Integer)

      Public Property KeyWord As String = ""

      Public Sub New(DisciplineIDs As List(Of Integer), ContractTypeIDs As List(Of Integer),
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)
        Me.DisciplineIDs = DisciplineIDs
        Me.ContractTypeIDs = ContractTypeIDs
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      'Public Sub New(FilterName As String)
      '  Me.FilterName = FilterName
      'End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceContactList() As ROHumanResourceContactList

      Return New ROHumanResourceContactList()

    End Function

    Public Shared Sub BeginGetROHumanResourceContactList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceContactList)))

      Dim dp As New DataPortal(Of ROHumanResourceContactList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourceContactList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceContactList)))

      Dim dp As New DataPortal(Of ROHumanResourceContactList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceContactList() As ROHumanResourceContactList

      Return DataPortal.Fetch(Of ROHumanResourceContactList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceContact.GetROHumanResourceContact(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceContactList"
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.FirstName))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@DisciplineIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.DisciplineIDs)))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ContractTypeIDs)))
            cm.Parameters.AddWithValue("@SystemIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace