﻿' Generated 20 Jun 2015 07:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.PersonnelManager.ReadOnly

  <Serializable()> _
  Public Class ROUserResourceList
    Inherits OBReadOnlyListBase(Of ROUserResourceList, ROUserResource)

    Public Property TotalRecords As Integer = 0
    Public Property TotalPages As Integer = 0

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROUserResource

      For Each child As ROUserResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

    Public Function GetROUserResourceBooking(ResourceBookingID As Integer) As ROUserResourceBooking

      Dim obj As ROUserResourceBooking = Nothing
      For Each parent As ROUserResource In Me
        obj = parent.ROUserResourceBookingList.GetItem(ResourceBookingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:="")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="End Date", Description:="")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Type value
      ''' </summary>
      <Display(Name:="Discipline", Description:="")>
      Public Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property
      'Singular.DataAnnotations.DropDownWeb(GetType(RODisciplineList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Discipline", ValueMember:="DisciplineID", DisplayMember:="Discipline")

      'Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ' ''' <summary>
      ' ''' Gets and sets the Production Type value
      ' ''' </summary>
      '<Display(Name:="Sub-Dept", Description:="")>
      'Public Property SystemID() As Integer?
      '  Get
      '    Return ReadProperty(SystemIDProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    LoadProperty(SystemIDProperty, Value)
      '  End Set
      'End Property
      ''Singular.DataAnnotations.DropDownWeb(GetType(ROSystemList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, ValueMember:="SystemID", DisplayMember:="System")

      'Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ' ''' <summary>
      ' ''' Gets and sets the Production Type value
      ' ''' </summary>
      '<Display(Name:="Area", Description:="")>
      'Public Property ProductionAreaID() As Integer?
      '  Get
      '    Return ReadProperty(ProductionAreaIDProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    LoadProperty(ProductionAreaIDProperty, Value)
      '  End Set
      'End Property
      ''Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)

      Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Type value
      ''' </summary>
      <Display(Name:="Position", Description:="")>
      Public Property PositionID() As Integer?
        Get
          Return ReadProperty(PositionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(PositionIDProperty, Value)
        End Set
      End Property

      Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "")
      ''' <summary>
      ''' Gets and sets the Resource Name value
      ''' </summary>
      <Display(Name:="Human Resource Filter", Description:="")>
      Public Property FirstName() As String
        Get
          Return ReadProperty(FirstNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(FirstNameProperty, Value)
        End Set
      End Property
      'Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)

      Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "")
      ''' <summary>
      ''' Gets and sets the Resource Name value
      ''' </summary>
      <Display(Name:="Surname", Description:="")>
      Public Property Surname() As String
        Get
          Return ReadProperty(SurnameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SurnameProperty, Value)
        End Set
      End Property
      'Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)

      Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "")
      ''' <summary>
      ''' Gets and sets the Resource Name value
      ''' </summary>
      <Display(Name:="Preferred Name", Description:="")>
      Public Property PreferredName() As String
        Get
          Return ReadProperty(PreferredNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PreferredNameProperty, Value)
        End Set
      End Property

      Public Shared PageNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PageNo, "PageNo", 1)
      ''' <summary>
      ''' Gets and sets the Production Type value
      ''' </summary>
      <Display(Name:="PageNo", Description:="")>
      Public Property PageNo() As Integer
        Get
          Return ReadProperty(PageNoProperty)
        End Get
        Set(ByVal Value As Integer)
          LoadProperty(PageNoProperty, Value)
        End Set
      End Property

      Public Shared PageSizeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PageSize, "PageSize", 25)
      ''' <summary>
      ''' Gets and sets the Production Type value
      ''' </summary>
      <Display(Name:="PageSize", Description:="")>
      Public Property PageSize() As Integer
        Get
          Return ReadProperty(PageSizeProperty)
        End Get
        Set(ByVal Value As Integer)
          LoadProperty(PageSizeProperty, Value)
        End Set
      End Property

      Public Sub New(StartDate As DateTime?,
                     EndDate As DateTime?,
                     DisciplineID As Integer?,
                      PositionID As Integer?,
                     FirstName As String,
                     Surname As String,
                     PreferredName As String,
                     PageNo As Integer,
                     PageSize As Integer)

        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.DisciplineID = DisciplineID
        Me.PositionID = PositionID
        'Me.SystemID = SystemID
        'Me.ProductionAreaID = ProductionAreaID
        Me.FirstName = FirstName
        Me.Surname = Surname
        Me.PreferredName = PreferredName
        Me.PageNo = PageNo
        Me.PageSize = PageSize

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserResourceList() As ROUserResourceList

      Return New ROUserResourceList()

    End Function

    Public Shared Sub BeginGetROUserResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserResourceList)))

      Dim dp As New DataPortal(Of ROUserResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROUserResourceList)))

      Dim dp As New DataPortal(Of ROUserResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserResourceList(StartDate As DateTime?,
                                               EndDate As DateTime?,
                                               DisciplineID As Integer?,
                                               PositionID As Integer?,
                                               FirstName As String,
                                               Surname As String,
                                               PreferredName As String,
                                               PageNo As Integer,
                                               PageSize As Integer) As ROUserResourceList

      Return DataPortal.Fetch(Of ROUserResourceList)(New Criteria(StartDate, EndDate,
                                                                DisciplineID, PositionID,
                                                                FirstName, Surname, PreferredName,
                                                                PageNo, PageSize))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False

      sdr.Read()
      TotalRecords = sdr.GetInt32(0)
      TotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      While sdr.Read
        Me.Add(ROUserResource.GetROUserResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROUserResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(10) Then
            parent = Me.GetItem(sdr.GetInt32(10))
          End If
          parent.ROUserResourceBookingList.RaiseListChangedEvents = False
          parent.ROUserResourceBookingList.Add(ROUserResourceBooking.GetROUserResourceBooking(sdr))
          parent.ROUserResourceBookingList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROPersonnelManagerResourceBookingList]"
            cm.Parameters.AddWithValue("@UserHumanResourceID", NothingDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", New Csla.SmartDate(crit.StartDate).DBValue)
            cm.Parameters.AddWithValue("@EndDate", New Csla.SmartDate(crit.EndDate).DBValue)
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@PositionID", NothingDBNull(crit.PositionID))
            cm.Parameters.AddWithValue("@FirstName", Strings.MakeEmptyDBNull(crit.FirstName))
            cm.Parameters.AddWithValue("@Surname", Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@PreferredName", Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@PageNo", crit.PageNo)
            cm.Parameters.AddWithValue("@PageSize", crit.PageSize)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace