﻿' Generated 14 Apr 2014 14:00 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourcePagedList
    Inherits SingularReadOnlyListBase(Of ROHumanResourcePagedList, ROHumanResourcePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourcePaged

      For Each child As ROHumanResourcePaged In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      'Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      <Browsable(False)>
      Public Property HumanResourceID As Integer? = Nothing

      Public Overridable Property AtDate As DateTime? = Nothing

      <Display(Name:="Firstname"),
     SetExpression("ROHumanResourcePagedListCriteriaBO.FirstnameSet(self)", , 250), TextField(, , , , )>
      Public Overridable Property Firstname As String = ""

      <Display(Name:="Second Name"),
     SetExpression("ROHumanResourcePagedListCriteriaBO.SecondNameSet(self)", , 250), TextField(, , , , )>
      Public Overridable Property SecondName As String = ""

      <Display(Name:="Pref. Name"),
     SetExpression("ROHumanResourcePagedListCriteriaBO.PreferredNameSet(self)", , 250), TextField(, , , , )>
      Public Overridable Property PreferredName As String = ""

      <Display(Name:="Surname"),
     SetExpression("ROHumanResourcePagedListCriteriaBO.SurnameSet(self)", , 250), TextField(, , , , )>
      Public Overridable Property Surname As String = ""

      <Display(Name:="Keyword"),
      SetExpression("ROHumanResourcePagedListCriteriaBO.KeywordSet(self)", , 250), TextField(, , , , )>
      Public Overridable Property Keyword As String = ""

      Public Overridable Property IDNo As String = ""
      Public Overridable Property EmployeeCode As String = ""
      'Public Property ShowMyAreaOnly As Boolean = True
      Public Overridable Property ManagerInd As Boolean = False
      Public Overridable Property ManagersInd As Boolean = False

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Discipline", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList)),
     SetExpression("ROHumanResourcePagedListCriteriaBO.DisciplineIDSet(self)")>
      Public Overridable Property DisciplineID As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared KeywordUserProfileProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.KeywordUserProfile) _
                                                                            .AddSetExpression("ROHumanResourcePagedListBO.Criteria.KeywordUserProfileSet(self)", False, 250)

      Public Overridable Property KeywordUserProfile As String
        Get
          Return ReadProperty(KeywordUserProfileProperty)
        End Get
        Set(value As String)
          LoadProperty(KeywordUserProfileProperty, value)
        End Set
      End Property

      Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle")
      <Display(Name:="Vehicle", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Vehicles.ReadOnly.ROVehicleOldList))>
      Public Overridable Property VehicleID As Integer?
        Get
          Return ReadProperty(VehicleIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(VehicleIDProperty, Value)
        End Set
      End Property

      Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="City", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList))>
      Public Overridable Property CityID As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "ContractType")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Contract Type", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.HR.ReadOnly.ROContractTypeList)),
     SetExpression("ROHumanResourcePagedListCriteriaBO.ContractTypeIDSet(self)")>
      Public Overridable Property ContractTypeID As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Position", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList))>
      Public Overridable Property PositionID As Integer?
        Get
          Return ReadProperty(PositionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Overridable Property SystemID As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property
      'OBLib.Security.Settings.CurrentUser.SystemID

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:="")>
      Public Overridable Property ProductionAreaID As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property
      'OBLib.Security.Settings.CurrentUser.ProductionAreaID



      <Display(AutoGenerateField:=False), Browsable(True), DefaultValue(CType(Nothing, Boolean))>
      Public Overridable Property FreelancerInd As Boolean?

      <Display(Name:="Firstname"),
     SetExpression("ROHumanResourcePagedListCriteriaBO.SystemIDsXmlSet(self)")>
      Public Property SystemIDsXml As String

      <Display(Name:="Firstname"),
     SetExpression("ROHumanResourcePagedListCriteriaBO.ProductionAreaIDsXmlSet(self)")>
      Public Property ProductionAreaIDsXml As String

      Public Sub New(DisciplineID As Integer?, AtDate As DateTime?)
        Me.DisciplineID = DisciplineID
        Me.AtDate = AtDate
      End Sub

      Public Sub New(Keyword As String)
        Me.Keyword = Keyword
      End Sub

      Public Sub New(HumanResourceID As Integer)
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

      Public Sub New(ByVal ContractTypeID As Integer?, ByVal CityID As Integer?, ByVal EmployeeCode As String, _
               ByVal IDNo As String, ByVal Surname As String, ByVal PreferredName As String, ByVal SecondName As String, _
               ByVal Firstname As String, ByVal DisciplineID As Integer?, ByVal PositionID As Integer?, _
               ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?, ByVal Keyword As String)

        Me.Firstname = Firstname
        Me.CityID = CityID
        Me.ContractTypeID = ContractTypeID
        Me.EmployeeCode = EmployeeCode
        Me.IDNo = IDNo
        Me.Surname = Surname
        Me.PreferredName = PreferredName
        Me.SecondName = SecondName
        Me.DisciplineID = DisciplineID
        Me.PositionID = PositionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.Keyword = Keyword

      End Sub

    End Class

    '<Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    'Public Class ReportCriteria
    '  Inherits Criteria

    '  <System.ComponentModel.DataAnnotations.Display(Order:=1)>
    '  Public Overrides Property Keyword As String = ""

    '  <Display(Name:="Discipline", Description:="", Order:=7),
    ' Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList))>
    '  Public Overrides Property DisciplineID() As Integer?
    '    Get
    '      Return ReadProperty(DisciplineIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      LoadProperty(DisciplineIDProperty, Value)
    '    End Set
    '  End Property

    '  <Browsable(False)>
    '  Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

    '  <Browsable(False)>
    '  Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    '  <Display(AutoGenerateField:=False), Browsable(True), DefaultValue(CType(Nothing, Boolean))>
    '  Public Overrides Property FreelancerInd As Boolean?

    'End Class

    '<Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    'Public Class MangersReportCriteria
    '  Inherits Criteria

    '  <Browsable(False)>
    '  Public Overrides Property Firstname As String = ""

    '  <Browsable(False)>
    '  Public Overrides Property SecondName As String = ""

    '  <Browsable(False)>
    '  Public Overrides Property PreferredName As String = ""

    '  <Browsable(False)>
    '  Public Overrides Property Surname As String = ""

    '  <Browsable(False)>
    '  Public Overrides Property IDNo As String = ""

    '  <DropDownWeb(GetType(RODisciplineList))>
    '  Public Overrides Property DisciplineID() As Integer? '= OBLib.CommonData.Enums.Discipline.UnitSupervisor

    '  <Browsable(False)>
    '  Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

    '  <Browsable(False)>
    '  Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    '  <Browsable(False)>
    '  Public Overrides Property AtDate As DateTime? = Nothing

    '  Public Overrides Property ManagerInd As Boolean = True

    '  <Display(Name:="City", Description:="", Order:=8),
    '  Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList)), Browsable(False)>
    '  Public Overrides Property CityID() As Integer?
    '    Get
    '      Return ReadProperty(CityIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      LoadProperty(CityIDProperty, Value)
    '    End Set
    '  End Property

    '  <Browsable(False)>
    '  Public Overrides Property ContractTypeID() As Integer?

    '  <Browsable(False)>
    '  Public Overrides Property PositionID() As Integer?

    'End Class

#Region " Common "

    Public Shared Function NewROHumanResourcePagedList() As ROHumanResourcePagedList

      Return New ROHumanResourcePagedList()

    End Function

    Public Shared Sub BeginGetROHumanResourcePagedList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourcePagedList)))

      Dim dp As New DataPortal(Of ROHumanResourcePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourcePagedList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourcePagedList)))

      Dim dp As New DataPortal(Of ROHumanResourcePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserSystemList() As ROUserSystemList

      Return DataPortal.Fetch(Of ROUserSystemList)(New Criteria())

    End Function

    Public Shared Function GetROUserSystemList(SystemID As Integer?, AtDate As DateTime?) As ROUserSystemList

      Return DataPortal.Fetch(Of ROUserSystemList)(New Criteria(SystemID, AtDate))

    End Function

    Public Shared Function GetROHumanResourceListPaged() As ROHumanResourcePagedList

      Return DataPortal.Fetch(Of ROHumanResourcePagedList)(New Criteria())

    End Function

    Public Shared Function GetROHumanResourceListPaged(DisciplineID As Integer?, AtDate As DateTime?) As ROHumanResourcePagedList

      Return DataPortal.Fetch(Of ROHumanResourcePagedList)(New Criteria(DisciplineID, AtDate))

    End Function

    Public Shared Function GetROHumanResourceListPaged(ByVal ContractTypeID As Integer?, ByVal CityID As Integer?, ByVal EmployeeCode As String, _
                                              ByVal IDNo As String, ByVal Surname As String, ByVal PreferredName As String, ByVal SecondName As String, _
                                              ByVal Firstname As String, ByVal DisciplineID As Integer?, ByVal PositionID As Integer?, _
                                              ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?, ByVal Keyword As String) As ROHumanResourcePagedList

      Return DataPortal.Fetch(Of ROHumanResourcePagedList)(New Criteria(ContractTypeID, CityID, EmployeeCode, IDNo, Surname, PreferredName, SecondName, Firstname, DisciplineID, PositionID, SystemID, ProductionAreaID, Keyword))

    End Function

    Public Shared Function GetROHumanResourceListPaged(Keyword As String) As ROHumanResourcePagedList

      Return DataPortal.Fetch(Of ROHumanResourcePagedList)(New Criteria(Keyword))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourcePaged.GetROHumanResourcePaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      'If crit.ShowMyAreaOnly Then
      '    crit.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
      'Else
      '    crit.SystemID = Nothing
      'End If
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceListPaged"
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@IsManagerInd", NothingDBNull(crit.ManagerInd))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@VehicleID", NothingDBNull(crit.VehicleID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@FreelancerInd", Singular.Misc.NothingDBNull(crit.FreelancerInd))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@ContractTypeID", NothingDBNull(crit.ContractTypeID))
            cm.Parameters.AddWithValue("@ManagersInd", crit.ManagersInd)
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.Firstname))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@SystemIDsXML", Strings.MakeEmptyDBNull(crit.SystemIDsXml))
            cm.Parameters.AddWithValue("@ProductionAreaIDsXML", Strings.MakeEmptyDBNull(crit.ProductionAreaIDsXml))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)

            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using
      'Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      '    cn.Open()
      '    Try
      '        Using cm As SqlCommand = cn.CreateCommand
      '            cm.CommandType = CommandType.StoredProcedure
      '            cm.CommandText = "GetProcsWeb.getROUserSystemList"
      '            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
      '            Using sdr As New SafeDataReader(cm.ExecuteReader)
      '                Fetch(sdr)

      '            End Using
      '        End Using
      '    Finally
      '        cn.Close()
      '    End Try
      'End Using
      'Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      '    cn.Open()
      '    Try
      '        Using cm As SqlCommand = cn.CreateCommand
      '            cm.CommandType = CommandType.StoredProcedure
      '            cm.CommandText = "GetProcsWeb.getROUserSystemAreaList"
      '            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
      '            Using sdr As New SafeDataReader(cm.ExecuteReader)
      '                Fetch(sdr)

      '            End Using
      '        End Using
      '    Finally
      '        cn.Close()
      '    End Try
      'End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace