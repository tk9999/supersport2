﻿' Generated 16 Apr 2016 13:38 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceAvailabilityList
    Inherits OBReadOnlyListBase(Of ROHumanResourceAvailabilityList, ROHumanResourceAvailability)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourceAvailability

      For Each child As ROHumanResourceAvailability In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property HumanResource As String

      Public Property StartDateTime As DateTime? = Nothing
      Public Property EndDateTime As DateTime? = Nothing
      Public Property DisciplineID As Integer? = Nothing
      Public Property PositionID As Integer? = Nothing
      Public Property PositionTypeID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property CallTime As DateTime? = Nothing
      Public Property WrapTime As DateTime? = Nothing

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROHumanResourceAvailabilityList() As ROHumanResourceAvailabilityList

      Return New ROHumanResourceAvailabilityList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceAvailability.GetROHumanResourceAvailability(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROHumanResourceListHumanResourceAvailability]"
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@PositionID", NothingDBNull(crit.PositionID))
            cm.Parameters.AddWithValue("@PositionTypeID", NothingDBNull(crit.PositionTypeID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@HumanResource", Strings.MakeEmptyDBNull(crit.HumanResource))
            cm.Parameters.AddWithValue("@CallTime", NothingDBNull(crit.CallTime))
            cm.Parameters.AddWithValue("@WrapTime", NothingDBNull(crit.WrapTime))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace