﻿' Generated 14 Apr 2014 14:00 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations
Imports Singular.Reporting

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceList
    Inherits OBReadOnlyListBase(Of ROHumanResourceList, ROHumanResource)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResource

      For Each child As ROHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Browsable(False)>
      Public Property HumanResourceID As Integer? = Nothing

      Public Overridable Property AtDate As DateTime? = Nothing

      Public Overridable Property Firstname As String = ""
      Public Overridable Property SecondName As String = ""
      Public Overridable Property PreferredName As String = ""
      Public Overridable Property Surname As String = ""
      Public Overridable Property IDNo As String = ""
      Public Overridable Property EmployeeCode As String = ""
      Public Property FetchAll As Boolean = False
      Public Property DisciplineIDs As String
      Public Property ProductionTypeIDs As String
      Public Property ContractTypeIDs As String

      <Browsable(False)>
      Public Overridable Property ManagerInd As Boolean = False

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Discipline", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList))>
      Public Overridable Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="City", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList))>
      Public Overridable Property CityID() As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "ContractType")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Contract Type", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.HR.ReadOnly.ROContractTypeList))>
      Public Overridable Property ContractTypeID() As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Position", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList))>
      Public Overridable Property PositionID() As Integer?
        Get
          Return ReadProperty(PositionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Overridable Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:="")>
      Public Overridable Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Overridable Property FilterName As String = ""

      <Display(AutoGenerateField:=False), Browsable(True), DefaultValue(CType(Nothing, Boolean))>
      Public Overridable Property FreelancerInd As Boolean?

      Public Sub New(DisciplineID As Integer?, AtDate As DateTime?)
        Me.DisciplineID = DisciplineID
        Me.AtDate = AtDate
      End Sub

      Public Sub New(DisciplineID As Integer?, AtDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.DisciplineID = DisciplineID
        Me.AtDate = AtDate
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(FilterName As String)
        Me.FilterName = FilterName
      End Sub

      Public Sub New(HumanResourceID As Integer)
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New(HumanResourceID As Integer?)
        Me.HumanResourceID = HumanResourceID
        Me.SystemID = Nothing
        Me.ProductionAreaID = Nothing
      End Sub

      Public Sub New()


      End Sub

      Public Sub New(ByVal ContractTypeID As Integer?, ByVal CityID As Integer?, ByVal EmployeeCode As String, _
               ByVal IDNo As String, ByVal Surname As String, ByVal PreferredName As String, ByVal SecondName As String, _
               ByVal Firstname As String, ByVal DisciplineID As Integer?, ByVal PositionID As Integer?, _
               ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?, ByVal DisciplineIDs As String, ByVal ProductionTypeIDs As String, ByVal ContractTypeIDs As String)

        Me.Firstname = Firstname
        Me.CityID = CityID
        Me.ContractTypeID = ContractTypeID
        Me.EmployeeCode = EmployeeCode
        Me.IDNo = IDNo
        Me.Surname = Surname
        Me.PreferredName = PreferredName
        Me.SecondName = SecondName
        Me.DisciplineID = DisciplineID
        Me.PositionID = PositionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.DisciplineIDs = DisciplineIDs
        Me.ProductionTypeIDs = ProductionTypeIDs
        Me.ContractTypeIDs = ContractTypeIDs
      End Sub

      Public Sub New(ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

      Public Sub New(FetchAll As Boolean)
        Me.FetchAll = FetchAll
      End Sub

    End Class

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class ReportCriteria
      Inherits Criteria

      <System.ComponentModel.DataAnnotations.Display(Order:=1)>
      Public Overrides Property Firstname As String = ""

      <System.ComponentModel.DataAnnotations.Display(Order:=2)>
      Public Overrides Property SecondName As String = ""

      <System.ComponentModel.DataAnnotations.Display(Order:=3)>
      Public Overrides Property PreferredName As String = ""

      <System.ComponentModel.DataAnnotations.Display(Order:=4)>
      Public Overrides Property Surname As String = ""

      <System.ComponentModel.DataAnnotations.Display(Order:=5)>
      Public Overrides Property IDNo As String = ""

      <System.ComponentModel.DataAnnotations.Display(Order:=6)>
      Public Overridable Property EmployeeCode As String = ""

      <Display(Name:="Discipline", Description:="", Order:=7),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList))>
      Public Overrides Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      <Display(Name:="City", Description:="", Order:=8),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList))>
      Public Overrides Property CityID() As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      <Display(Name:="Contract Type", Description:="", Order:=9),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.HR.ReadOnly.ROContractTypeList))>
      Public Overrides Property ContractTypeID() As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      <Display(Name:="Position", Description:="", Order:=10),
      Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList))>
      Public Overrides Property PositionID() As Integer?
        Get
          Return ReadProperty(PositionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      <Browsable(False)>
      Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

      <Browsable(False)>
      Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

      <Browsable(False)>
      Public Overrides Property FilterName As String = ""

      <Browsable(False)>
      Public Overridable Property AtDate As DateTime? = Nothing

      <Display(AutoGenerateField:=False), Browsable(True), DefaultValue(CType(Nothing, Boolean))>
      Public Overrides Property FreelancerInd As Boolean?

    End Class

    Public Class FreelancerAndISPCriteria
      Inherits ReportCriteria

      <Display(Name:="Contract Type", Description:="", Order:=9),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.HR.ReadOnly.ROContractTypeList),
                                           Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData,
                                           DataSourceFilterMember:="FreelancerInd",
                                           ThisFilterMember:="FreelancerInd")>
      Public Overrides Property ContractTypeID() As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      <Display(AutoGenerateField:=False), Browsable(True), DefaultValue(True)>
      Public Overrides Property FreelancerInd As Boolean? = True

    End Class

    Public Class DisciplineReportCriteria
      Inherits Criteria

      <Browsable(False)>
      Public Overrides Property Firstname As String = ""

      <Browsable(False)>
      Public Overrides Property SecondName As String = ""

      <Browsable(False)>
      Public Overrides Property PreferredName As String = ""

      <Browsable(False)>
      Public Overrides Property Surname As String = ""

      <Browsable(False)>
      Public Overrides Property IDNo As String = ""

      <Browsable(False)>
      Public Overridable Property EmployeeCode As String = ""

      <Browsable(False)>
      Public Overrides Property DisciplineID() As Integer? = OBLib.CommonData.Enums.Discipline.UnitSupervisor

      <Browsable(False)>
      Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

      <Browsable(False)>
      Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

      <Browsable(False)>
      Public Overrides Property FilterName As String = ""

      <Browsable(False)>
      Public Overridable Property AtDate As DateTime? = Nothing

      <Display(Name:="City", Description:="", Order:=8),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList)), Browsable(False)>
      Public Overrides Property CityID() As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      <Browsable(False)>
      Public Overrides Property ContractTypeID() As Integer?

      <Browsable(False)>
      Public Overrides Property PositionID() As Integer?

    End Class

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class EventManagerReportCriteria
      Inherits DisciplineReportCriteria

      <Browsable(False)>
      Public Overrides Property DisciplineID() As Integer? = OBLib.CommonData.Enums.Discipline.EventManager

    End Class

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class UnitSupervisorReportCriteria
      Inherits DisciplineReportCriteria

      <Browsable(False)>
      Public Overrides Property DisciplineID() As Integer? = OBLib.CommonData.Enums.Discipline.UnitSupervisor

    End Class

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class MangersReportCriteria
      Inherits Criteria

      <Browsable(False)>
      Public Overrides Property Firstname As String = ""

      <Browsable(False)>
      Public Overrides Property SecondName As String = ""

      <Browsable(False)>
      Public Overrides Property PreferredName As String = ""

      <Browsable(False)>
      Public Overrides Property Surname As String = ""

      <Browsable(False)>
      Public Overrides Property IDNo As String = ""

      <Browsable(False)>
      Public Overridable Property EmployeeCode As String = ""

      <Browsable(False)>
      Public Overrides Property DisciplineID() As Integer? '= OBLib.CommonData.Enums.Discipline.UnitSupervisor

      <Browsable(False)>
      Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

      <Browsable(False)>
      Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

      <Browsable(False)>
      Public Overrides Property FilterName As String = ""

      <Browsable(False)>
      Public Overrides Property AtDate As DateTime? = Nothing

      <Browsable(False)>
      Public Overrides Property ManagerInd As Boolean = True

      <Display(Name:="City", Description:="", Order:=8),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList)), Browsable(False)>
      Public Overrides Property CityID() As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      <Browsable(False)>
      Public Overrides Property ContractTypeID() As Integer?

      <Browsable(False)>
      Public Overrides Property PositionID() As Integer?

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceList() As ROHumanResourceList

      Return New ROHumanResourceList()

    End Function

    Public Shared Sub BeginGetROHumanResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceList)))

      Dim dp As New DataPortal(Of ROHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceList)))

      Dim dp As New DataPortal(Of ROHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceList() As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria())

    End Function

    Public Shared Function GetROHumanResourceList(DisciplineID As Integer?, AtDate As DateTime?) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(DisciplineID, AtDate))

    End Function

    Public Shared Function GetROHumanResourceList(DisciplineID As Integer?, AtDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(DisciplineID, AtDate, SystemID, ProductionAreaID))

    End Function

    Public Shared Function GetROHumanResourceList(ByVal ContractTypeID As Integer?, ByVal CityID As Integer?, ByVal EmployeeCode As String, _
                                                  ByVal IDNo As String, ByVal Surname As String, ByVal PreferredName As String, ByVal SecondName As String, _
                                                  ByVal Firstname As String, ByVal DisciplineID As Integer?, ByVal PositionID As Integer?, _
                                                  ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?, ByVal DisciplineIDs As String, ByVal ProductionTypeIDs As String,
                                                  ByVal ContractTypeIDs As String) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(ContractTypeID, CityID, EmployeeCode, IDNo, Surname, PreferredName, SecondName, Firstname, DisciplineID, PositionID, SystemID, ProductionAreaID, DisciplineIDs, ProductionTypeIDs, ContractTypeIDs))

    End Function

    Public Shared Function GetROHumanResourceList(ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Public Shared Function GetROHumanResourceList(FilterName As String) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(FilterName))

    End Function

    Public Shared Function GetROHumanResourceList(HumanResourceID As Integer) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(HumanResourceID))

    End Function

    Public Shared Function GetROHumanResourceList(HumanResourceID As Integer?) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(HumanResourceID))

    End Function

    Public Shared Function GetROHumanResourceList(FetchAll As Boolean) As ROHumanResourceList

      Return DataPortal.Fetch(Of ROHumanResourceList)(New Criteria(FetchAll))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResource.GetROHumanResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceList"
            cm.Parameters.AddWithValue("@Firstname", Singular.Strings.MakeEmptyDBNull(crit.Firstname))
            cm.Parameters.AddWithValue("@SecondName", Singular.Strings.MakeEmptyDBNull(crit.SecondName))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@EmployeeCode", Singular.Strings.MakeEmptyDBNull(crit.EmployeeCode))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@IDNo", Singular.Strings.MakeEmptyDBNull(crit.IDNo))
            cm.Parameters.AddWithValue("@ContractTypeID", NothingDBNull(crit.ContractTypeID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@CityID", NothingDBNull(crit.CityID))
            cm.Parameters.AddWithValue("@PositionID", NothingDBNull(crit.PositionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@FilterName", Singular.Strings.MakeEmptyDBNull(crit.FilterName))
            cm.Parameters.AddWithValue("@ManagersInd", crit.ManagerInd)
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@FetchAll", crit.FetchAll)
            cm.Parameters.AddWithValue("@DisciplineIDs", Singular.Strings.MakeEmptyDBNull(crit.DisciplineIDs))
            cm.Parameters.AddWithValue("@ProductionTypeIDs", Singular.Strings.MakeEmptyDBNull(crit.ProductionTypeIDs))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Singular.Strings.MakeEmptyDBNull(crit.ContractTypeIDs))
            'cm.Parameters.AddWithValue("@FreelancerInd", Singular.Misc.NothingDBNull(crit.FreelancerInd))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace