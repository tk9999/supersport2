﻿' Generated 16 May 2016 10:32 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceTravelReqList
    Inherits OBReadOnlyListBase(Of ROHumanResourceTravelReqList, ROHumanResourceTravelReq)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourceTravelReq

      For Each child As ROHumanResourceTravelReq In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing

      <Browsable(False)>
      Public Property HumanResourceID As Integer? = Nothing

      Public Overridable Property AtDate As DateTime? = Nothing

      <Display(Name:="Firstname"), TextField,
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.FirstnameSet(self)", , 250)>
      Public Overridable Property Firstname As String = ""

      <Display(Name:="Second Name"), TextField,
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.SecondNameSet(self)", , 250)>
      Public Overridable Property SecondName As String = ""

      <Display(Name:="Pref. Name"), TextField,
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.PreferredNameSet(self)", , 250)>
      Public Overridable Property PreferredName As String = ""

      <Display(Name:="Surname"), TextField,
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.SurnameSet(self)", , 250)>
      Public Overridable Property Surname As String = ""

      Public Overridable Property IDNo As String = ""
      Public Overridable Property EmployeeCode As String = ""
      Public Overridable Property ManagerInd As Boolean = False
      Public Overridable Property ManagersInd As Boolean = False

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Discipline", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList)),
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.DisciplineIDSet(self)")>
      Public Overridable Property DisciplineID As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle")
      <Display(Name:="Vehicle", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Vehicles.ReadOnly.ROVehicleList))>
      Public Overridable Property VehicleID As Integer?
        Get
          Return ReadProperty(VehicleIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(VehicleIDProperty, Value)
        End Set
      End Property

      Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="City", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCityList))>
      Public Overridable Property CityID As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "ContractType")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Contract Type", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.HR.ReadOnly.ROContractTypeList)),
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.ContractTypeIDSet(self)")>
      Public Overridable Property ContractTypeID As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Position", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList))>
      Public Overridable Property PositionID As Integer?
        Get
          Return ReadProperty(PositionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Overridable Property SystemID As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:="")>
      Public Overridable Property ProductionAreaID As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared KeyWordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="KeyWord"),
       Singular.DataAnnotations.SetExpression("ROHumanResourceTravelReqListCriteriaBO.KeyWordSet(self)", , 250), Singular.DataAnnotations.TextField, Singular.DataAnnotations.PrimarySearchField>
      Public Overridable Property KeyWord As String
        Get
          Return ReadProperty(KeyWordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeyWordProperty, Value)
        End Set
      End Property

      <Display(AutoGenerateField:=False), Browsable(True), DefaultValue(CType(Nothing, Boolean))>
      Public Overridable Property FreelancerInd As Boolean?

      <Display(Name:="Firstname"),
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.SystemIDsXmlSet(self)")>
      Public Property SystemIDsXml As String

      <Display(Name:="Firstname"),
     SetExpression("ROHumanResourceTravelReqListCriteriaBO.ProductionAreaIDsXmlSet(self)")>
      Public Property ProductionAreaIDsXml As String

      Public Sub New(DisciplineID As Integer?, AtDate As DateTime?)
        Me.DisciplineID = DisciplineID
        Me.AtDate = AtDate
      End Sub

      Public Sub New(Keyword As String)
        Me.Keyword = Keyword
      End Sub

      Public Sub New(HumanResourceID As Integer)
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

      Public Sub New(ByVal ContractTypeID As Integer?, ByVal CityID As Integer?, ByVal EmployeeCode As String, _
               ByVal IDNo As String, ByVal Surname As String, ByVal PreferredName As String, ByVal SecondName As String, _
               ByVal Firstname As String, ByVal DisciplineID As Integer?, ByVal PositionID As Integer?, _
               ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?)

        Me.Firstname = Firstname
        Me.CityID = CityID
        Me.ContractTypeID = ContractTypeID
        Me.EmployeeCode = EmployeeCode
        Me.IDNo = IDNo
        Me.Surname = Surname
        Me.PreferredName = PreferredName
        Me.SecondName = SecondName
        Me.DisciplineID = DisciplineID
        Me.PositionID = PositionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

    End Class

    Public Shared Function NewROHumanResourceTravelReqList() As ROHumanResourceTravelReqList

      Return New ROHumanResourceTravelReqList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROHumanResourceTravelReqList() As ROHumanResourceTravelReqList

      Return DataPortal.Fetch(Of ROHumanResourceTravelReqList)(New Criteria())

    End Function

    Public Shared Function GetROHumanResourceTravelReqList(DisciplineID As Integer?, AtDate As DateTime?) As ROHumanResourceTravelReqList

      Return DataPortal.Fetch(Of ROHumanResourceTravelReqList)(New Criteria(DisciplineID, AtDate))

    End Function

    Public Shared Function GetROHumanResourceTravelReqList(ByVal ContractTypeID As Integer?, ByVal CityID As Integer?, ByVal EmployeeCode As String, _
                                              ByVal IDNo As String, ByVal Surname As String, ByVal PreferredName As String, ByVal SecondName As String, _
                                              ByVal Firstname As String, ByVal DisciplineID As Integer?, ByVal PositionID As Integer?, _
                                              ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?) As ROHumanResourceTravelReqList

      Return DataPortal.Fetch(Of ROHumanResourceTravelReqList)(New Criteria(ContractTypeID, CityID, EmployeeCode, IDNo, Surname, PreferredName, SecondName, Firstname, DisciplineID, PositionID, SystemID, ProductionAreaID))

    End Function

    Public Shared Function GetROHumanResourceTravelReqList(Keyword As String) As ROHumanResourceTravelReqList

      Return DataPortal.Fetch(Of ROHumanResourceTravelReqList)(New Criteria(Keyword))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceTravelReq.GetROHumanResourceTravelReq(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceListTravelReq"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@IsManagerInd", NothingDBNull(crit.ManagerInd))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@VehicleID", NothingDBNull(crit.VehicleID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@FreelancerInd", Singular.Misc.NothingDBNull(crit.FreelancerInd))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@ContractTypeID", NothingDBNull(crit.ContractTypeID))
            cm.Parameters.AddWithValue("@ManagersInd", crit.ManagersInd)
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.Firstname))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@SystemIDsXML", Strings.MakeEmptyDBNull(crit.SystemIDsXml))
            cm.Parameters.AddWithValue("@ProductionAreaIDsXML", Strings.MakeEmptyDBNull(crit.ProductionAreaIDsXml))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
