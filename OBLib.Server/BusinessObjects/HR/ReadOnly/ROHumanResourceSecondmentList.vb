﻿' Generated 24 Mar 2015 08:58 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSecondmentList
    Inherits OBReadOnlyListBase(Of ROHumanResourceSecondmentList, ROHumanResourceSecondment)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceSecondmentID As Integer) As ROHumanResourceSecondment

      For Each child As ROHumanResourceSecondment In Me
        If child.HumanResourceSecondmentID = HumanResourceSecondmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Secondments"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceSecondmentList() As ROHumanResourceSecondmentList

      Return New ROHumanResourceSecondmentList()

    End Function

    Public Shared Sub BeginGetROHumanResourceSecondmentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSecondmentList)))

      Dim dp As New DataPortal(Of ROHumanResourceSecondmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROHumanResourceSecondmentList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSecondmentList)))

      Dim dp As New DataPortal(Of ROHumanResourceSecondmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceSecondmentList() As ROHumanResourceSecondmentList

      Return DataPortal.Fetch(Of ROHumanResourceSecondmentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceSecondment.GetROHumanResourceSecondment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceSecondmentList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace