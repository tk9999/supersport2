﻿' Generated 22 Aug 2014 15:18 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSystemList
    Inherits OBReadOnlyListBase(Of ROHumanResourceSystemList, ROHumanResourceSystem)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSystemID As Integer) As ROHumanResourceSystem

      For Each child As ROHumanResourceSystem In Me
        If child.HumanResourceSystemID = HumanResourceSystemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Systems"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property HumanResourceSystem As String

      Public Property HumanResourceID As Integer?

      Public Property ProductionAreaIDs As List(Of Integer) = New List(Of Integer)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceSystemList() As ROHumanResourceSystemList

      Return New ROHumanResourceSystemList()

    End Function

    Public Shared Sub BeginGetROHumanResourceSystemList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSystemList)))

      Dim dp As New DataPortal(Of ROHumanResourceSystemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourceSystemList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceSystemList)))

      Dim dp As New DataPortal(Of ROHumanResourceSystemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceSystemList() As ROHumanResourceSystemList

      Return DataPortal.Fetch(Of ROHumanResourceSystemList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceSystem.GetROHumanResourceSystem(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceSystemList"
            cm.Parameters.AddWithValue("@HumanResourceSystem", Strings.MakeEmptyDBNull(crit.HumanResourceSystem))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace