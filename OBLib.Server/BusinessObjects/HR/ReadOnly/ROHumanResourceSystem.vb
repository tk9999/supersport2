﻿' Generated 22 Aug 2014 15:18 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSystem
    Inherits OBReadOnlyBase(Of ROHumanResourceSystem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSystemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceSystemID() As Integer
      Get
        Return GetProperty(HumanResourceSystemIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource that belongs to a system")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system for which the human resource belongs to")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="True if the system is the human resource's primary system")>
    Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="The production area which the human resource belongs to")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public ReadOnly Property System() As String
      Get
        If Not IsNullNothing(SystemID) Then
          Dim ROSystem As Maintenance.ReadOnly.ROSystem = OBLib.CommonData.Lists.ROSystemList.GetItem(GetProperty(SystemIDProperty))
          If Not IsNullNothing(ROSystem) Then
            Return ROSystem.System
          End If
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property ProductionArea() As String
      Get
        If Not IsNullNothing(ProductionAreaID) Then
          Dim ROPA As Maintenance.Productions.Areas.ReadOnly.ROProductionArea = OBLib.CommonData.Lists.ROProductionAreaList.GetItem(GetProperty(ProductionAreaIDProperty))
          If Not IsNullNothing(ROPA) Then
            Return ROPA.ProductionArea
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared RequiresTeamsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTeams, "Requires Teams", False)
    ''' <summary>
    ''' Gets the Requires Teams value
    ''' </summary>
    Public ReadOnly Property RequiresTeams() As Boolean
      Get
        Return GetProperty(RequiresTeamsProperty)
      End Get
    End Property

    <Display(Name:="Working Area")>
    Public ReadOnly Property WorkingArea As String
      Get
        Return System & " (" & ProductionArea & ")"
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SystemID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceSystem(dr As SafeDataReader) As ROHumanResourceSystem

      Dim r As New ROHumanResourceSystem()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceSystemIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PrimaryIndProperty, .GetBoolean(3))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(RequiresTeamsProperty, .GetBoolean(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace