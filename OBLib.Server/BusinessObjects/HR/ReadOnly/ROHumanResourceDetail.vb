﻿' Generated 14 Apr 2014 14:00 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceDetail
    Inherits OBReadOnlyBase(Of ROHumanResourceDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname", "")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="First Name", Description:="The human resource's first name")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name", "")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="The human resource's second name")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="The human resource's preferred name")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="The human resource's surname")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="True indicates that this person is a contractor and not an employee")>
    Public ReadOnly Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="The human resource's email address")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address", "")
    ''' <summary>
    ''' Gets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alternative Email Address", Description:="An alternative email address for the human resource")>
    Public ReadOnly Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
    End Property

    Public Shared PhoneEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhoneEmailAddress, "Phone Email Address", "")
    ''' <summary>
    ''' Gets the Phone Email Address value
    ''' </summary>
    <Display(Name:="Phone Email Address", Description:="This is the email address that will be used to send scheduling information to crew. The crew must always be able to access this address from their phone")>
    Public ReadOnly Property PhoneEmailAddress() As String
      Get
        Return GetProperty(PhoneEmailAddressProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number", "")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="The human resource's cell phone number")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number", "")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="An alternative contact number for the human resource")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="The human resource's unique ID number")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="The city that this HR is based. Used to calculate travel for events")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared RaceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RaceID, "Race", Nothing)
    ''' <summary>
    ''' Gets the Race value
    ''' </summary>
    <Display(Name:="Race", Description:="The human resource's race")>
    Public ReadOnly Property RaceID() As Integer?
      Get
        Return GetProperty(RaceIDProperty)
      End Get
    End Property

    Public Shared GenderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GenderID, "Gender", Nothing)
    ''' <summary>
    ''' Gets the Gender value
    ''' </summary>
    <Display(Name:="Gender", Description:="The human resource's gender")>
    Public ReadOnly Property GenderID() As Integer?
      Get
        Return GetProperty(GenderIDProperty)
      End Get
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Active", True)
    ''' <summary>
    ''' Gets the Active value
    ''' </summary>
    <Display(Name:="Active", Description:="Tick indicates that this human resource is currently active and can be used for productions")>
    Public ReadOnly Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
    End Property

    Public Shared InactiveReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InactiveReason, "Inactive Reason", "")
    ''' <summary>
    ''' Gets the Inactive Reason value
    ''' </summary>
    <Display(Name:="Inactive Reason", Description:="If a Human Resource is marked as Not Active then an InactiveReason is required")>
    Public ReadOnly Property InactiveReason() As String
      Get
        Return GetProperty(InactiveReasonProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
    Public ReadOnly Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="The employee code from payroll")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared PayHalfMonthIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PayHalfMonthInd, "Pay Half Month", False)
    ''' <summary>
    ''' Gets the Pay Half Month value
    ''' </summary>
    <Display(Name:="Pay Half Month", Description:="")>
    Public ReadOnly Property PayHalfMonthInd() As Boolean
      Get
        Return GetProperty(PayHalfMonthIndProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier", 0)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Employed by a supplier")>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared NationalityCountyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NationalityCountyID, "Nationality County", Nothing)
    ''' <summary>
    ''' Gets the Nationality County value
    ''' </summary>
    <Display(Name:="Nationality County", Description:="")>
    Public ReadOnly Property NationalityCountyID() As Integer?
      Get
        Return GetProperty(NationalityCountyIDProperty)
      End Get
    End Property

    Public Shared ContractDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractDays, "Contract Days", 0)
    ''' <summary>
    ''' Gets the Contract Days value
    ''' </summary>
    <Display(Name:="Contract Days", Description:="")>
    Public ReadOnly Property ContractDays() As Integer
      Get
        Return GetProperty(ContractDaysProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceID2Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID2, "Manager Human Resource ID 2", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource ID 2 value
    ''' </summary>
    ''' 
    <Display(Name:="Manager Human Resource ID 2", Description:="A 2nd manager that is allowed to edit Human Resource Skill Rates")>
    Public ReadOnly Property ManagerHumanResourceID2() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceID2Property)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type", "")
    ''' <summary>
    ''' Gets the Address R 1 value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="Contract Type Name")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared LicenceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LicenceInd, "LicenceInd", False)
    ''' <summary>
    ''' Gets the Active value
    ''' </summary>
    <Display(Name:="LicenceInd")>
    Public ReadOnly Property LicenceInd() As Boolean
      Get
        Return GetProperty(LicenceIndProperty)
      End Get
    End Property

    Public Shared LicenceExpiryDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.LicenceExpDate, Nothing)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Licence Expiry Date", Description:="")>
    Public ReadOnly Property LicenceExpDate() As Date?
      Get
        Return GetProperty(LicenceExpiryDateProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City", "")
    ''' <summary>
    ''' Gets the Postal Code P value
    ''' </summary>
    <Display(Name:="City")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ''' <summary>
    ''' Gets the Postal Code P value
    ''' </summary>
    <Display(Name:="Supplier")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared NationalityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Nationality, "Nationality", "")
    ''' <summary>
    ''' Gets the Postal Code P value
    ''' </summary>
    <Display(Name:="Nationality")>
    Public ReadOnly Property Nationality() As String
      Get
        Return GetProperty(NationalityProperty)
      End Get
    End Property

    Public Shared ManagerNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerName, "Manager", "")
    ''' <summary>
    ''' Gets the Postal Code P value
    ''' </summary>
    <Display(Name:="Manager")>
    Public ReadOnly Property ManagerName() As String
      Get
        Return GetProperty(ManagerNameProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="ResourceID", Description:="")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared PrimarySystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PrimarySystemID, "PrimarySystemID", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="PrimarySystemID", Description:="")>
    Public ReadOnly Property PrimarySystemID() As Integer?
      Get
        Return GetProperty(PrimarySystemIDProperty)
      End Get
    End Property

    Public Shared PrimaryProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PrimaryProductionAreaID, "PrimaryProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets the PrimaryProductionAreaID value
    ''' </summary>
    <Display(Name:="PrimaryProductionAreaID", Description:="")>
    Public ReadOnly Property PrimaryProductionAreaID() As Integer?
      Get
        Return GetProperty(PrimaryProductionAreaIDProperty)
      End Get
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsFreelancer, "IsFreelancer", False)
    ''' <summary>
    ''' Gets and sets the Pay Half Month value
    ''' </summary>
    <Display(Name:="IsFreelancer")>
    Public Property IsFreelancer() As Boolean
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
      Set(ByVal Value As Boolean)
        LoadProperty(IsFreelancerProperty, Value)
      End Set
    End Property

    Public ReadOnly Property LicenceExpDateString As String
      Get
        If Not IsNullNothing(LicenceExpDate) Then
          Return LicenceExpDate.Value.ToString("dd MMM yyyy")
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceDetail(dr As SafeDataReader) As ROHumanResourceDetail

      Dim r As New ROHumanResourceDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(FirstnameProperty, .GetString(1))
        LoadProperty(SecondNameProperty, .GetString(2))
        LoadProperty(PreferredNameProperty, .GetString(3))
        LoadProperty(SurnameProperty, .GetString(4))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(EmailAddressProperty, .GetString(6))
        LoadProperty(AlternativeEmailAddressProperty, .GetString(7))
        LoadProperty(PhoneEmailAddressProperty, .GetString(8))
        LoadProperty(CellPhoneNumberProperty, .GetString(9))
        LoadProperty(AlternativeContactNumberProperty, .GetString(10))
        LoadProperty(IDNoProperty, .GetString(11))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(RaceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(GenderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(ActiveIndProperty, .GetBoolean(15))
        LoadProperty(InactiveReasonProperty, .GetString(16))
        LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(EmployeeCodeProperty, .GetString(18))
        LoadProperty(PayHalfMonthIndProperty, .GetBoolean(19))
        LoadProperty(SupplierIDProperty, .GetInt32(20))
        LoadProperty(NationalityCountyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(ContractDaysProperty, .GetInt32(22))
        LoadProperty(ManagerHumanResourceID2Property, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(ContractTypeProperty, .GetString(24))
        LoadProperty(LicenceIndProperty, .GetBoolean(25))
        LoadProperty(LicenceExpiryDateProperty, .GetValue(26))
        LoadProperty(CityProperty, .GetString(27))
        LoadProperty(SupplierProperty, .GetString(28))
        LoadProperty(NationalityProperty, .GetString(29))
        LoadProperty(ManagerNameProperty, .GetString(30))
        LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(31)))
        LoadProperty(PrimarySystemIDProperty, ZeroNothing(.GetInt32(32)))
        LoadProperty(PrimaryProductionAreaIDProperty, ZeroNothing(.GetInt32(33)))
        LoadProperty(IsFreelancerProperty, .GetBoolean(34))
      End With
    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace