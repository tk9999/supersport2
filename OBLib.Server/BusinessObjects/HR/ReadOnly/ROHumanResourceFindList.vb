﻿' Generated 25 Nov 2015 17:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceFindList
    Inherits OBReadOnlyListBase(Of ROHumanResourceFindList, ROHumanResourceFind)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourceFind

      For Each child As ROHumanResourceFind In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Overridable Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area")>
      Public Overridable Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "Firstname", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Firstname"),
       SetExpression("ROHumanResourceFindListCriteriaBO.FirstNameSet(self)", , 250), TextField>
      Public Overridable Property FirstName As String
        Get
          Return ReadProperty(FirstNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(FirstNameProperty, Value)
        End Set
      End Property

      Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Surname"),
       SetExpression("ROHumanResourceFindListCriteriaBO.SurnameSet(self)", , 250), TextField>
      Public Overridable Property Surname As String
        Get
          Return ReadProperty(SurnameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SurnameProperty, Value)
        End Set
      End Property

      Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Pref. Name"),
       SetExpression("ROHumanResourceFindListCriteriaBO.PreferredNameSet(self)", , 250), TextField>
      Public Overridable Property PreferredName As String
        Get
          Return ReadProperty(PreferredNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PreferredNameProperty, Value)
        End Set
      End Property

      Public Shared KeyWordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.KeyWord, "KeyWord", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="KeyWord"),
       SetExpression("ROHumanResourceFindListCriteriaBO.KeywordSet(self)", , 250), TextField>
      Public Overridable Property KeyWord As String
        Get
          Return ReadProperty(KeyWordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeyWordProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "HumanResource", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="HumanResource"),
       SetExpression("ROHumanResourceFindListCriteriaBO.HumanResourceSet(self)", , 250), TextField, PrimarySearchField>
      Public Overridable Property HumanResource As String
        Get
          Return ReadProperty(HumanResourceProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(HumanResourceProperty, Value)
        End Set
      End Property

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Discipline", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline"),
     SetExpression("ROHumanResourceFindListCriteriaBO.DisciplineIDSet(self)")>
      Public Overridable Property DisciplineID As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing)
      ''' <summary>
      ''' Gets and sets the Contract Type value
      ''' </summary>
      <Display(Name:="Contract Type", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type"),
     SetExpression("ROHumanResourceFindListCriteriaBO.ContractTypeIDSet(self)")>
      Public Overridable Property ContractTypeID As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemIDs, "SystemIDs", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="System IDs"), SetExpression("ROHumanResourceFindListCriteriaBO.SystemIDsSet(self)")>
      Public Property SystemIDs As String
        Get
          Return ReadProperty(SystemIDsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SystemIDsProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaIDs, "ProductionAreaIDs", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Area IDs"), SetExpression("ROHumanResourceFindListCriteriaBO.ProductionAreaIDsSet(self)")>
      Public Property ProductionAreaIDs As String
        Get
          Return ReadProperty(ProductionAreaIDsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionAreaIDsProperty, Value)
        End Set
      End Property

      Public Property DisciplineIDs As String
      Public Property ProductionTypeIDs As String
      Public Property ContractTypeIDs As String
      Public Property ProductionAreaIDList As List(Of Integer)
      Public Property SystemIDList As List(Of Integer)
      Public Property DisciplineIDList As List(Of Integer)
      Public Property ContractTypeIDList As List(Of Integer)


      Public Sub New(SystemID As Integer, ProductionAreaID As Integer, DisciplineIDs As String,
                     ProductionTypeIDs As String, ContractTypeIDs As String, PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.DisciplineIDs = DisciplineIDs
        Me.ProductionTypeIDs = ProductionTypeIDs
        Me.ContractTypeIDs = ContractTypeIDs
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      'Public Sub New(FilterName As String)
      '  Me.FilterName = FilterName
      'End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceFindList() As ROHumanResourceFindList

      Return New ROHumanResourceFindList()

    End Function

    Public Shared Sub BeginGetROHumanResourceFindList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceFindList)))

      Dim dp As New DataPortal(Of ROHumanResourceFindList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourceFindList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceFindList)))

      Dim dp As New DataPortal(Of ROHumanResourceFindList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceFindList() As ROHumanResourceFindList

      Return DataPortal.Fetch(Of ROHumanResourceFindList)(New Criteria())

    End Function

    Public Shared Function GetROHumanResourceFindList(ByVal SystemID As Integer?, ByVal ProductionAreaID As Integer?, ByVal DisciplineIDs As String, ByVal ProductionTypeIDs As String,
                                                ByVal ContractTypeIDs As String, PageNo As Integer, PageSize As Integer,
                           SortAsc As Boolean, SortColumn As String) As ROHumanResourceFindList

      Return DataPortal.Fetch(Of ROHumanResourceFindList)(New Criteria(SystemID, ProductionAreaID, DisciplineIDs, ProductionTypeIDs, ContractTypeIDs, PageNo, PageSize,
                           SortAsc, SortColumn))

    End Function

    'Public Shared Function GetROHumanResourceFindList(FilterName As String) As ROHumanResourceFindList

    '  Return DataPortal.Fetch(Of ROHumanResourceFindList)(New Criteria(FilterName))

    'End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceFind.GetROHumanResourceFind(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceFindList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.FirstName))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@KeyWord", Singular.Strings.MakeEmptyDBNull(crit.KeyWord))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@DisciplineIDs", Singular.Strings.MakeEmptyDBNull(crit.DisciplineIDs))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@ProductionTypeIDs", Singular.Strings.MakeEmptyDBNull(crit.ProductionTypeIDs))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Singular.Strings.MakeEmptyDBNull(crit.ContractTypeIDs))
            cm.Parameters.AddWithValue("@ContractTypeID", Singular.Strings.MakeEmptyDBNull(crit.ContractTypeID))
            cm.Parameters.AddWithValue("@SystemIDs", Singular.Strings.MakeEmptyDBNull(crit.SystemIDs))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Singular.Strings.MakeEmptyDBNull(crit.ProductionAreaIDs))
            cm.Parameters.AddWithValue("@SystemIDList", OBMisc.IntegerListToXML(crit.SystemIDList))
            cm.Parameters.AddWithValue("@ProductionAreaIDList", OBMisc.IntegerListToXML(crit.ProductionAreaIDList))
            cm.Parameters.AddWithValue("@HumanResource", Strings.MakeEmptyDBNull(crit.HumanResource))
            cm.Parameters.AddWithValue("@DisciplineIDList", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.DisciplineIDList)))
            cm.Parameters.AddWithValue("@ContractTypeIDList", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ContractTypeIDList)))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace