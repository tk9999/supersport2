﻿' Generated 09 Nov 2014 11:52 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHRQuickSearch
    Inherits SingularReadOnlyBase(Of ROHRQuickSearch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared AlreadyOnPatternIndProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AlreadyOnPatternInd, "Already On Pattern")
    ''' <summary>
    ''' Gets the Already On Pattern value
    ''' </summary>
    <Display(Name:="Already On Pattern?", Description:="")>
    Public ReadOnly Property AlreadyOnPatternInd() As Integer
      Get
        Return GetProperty(AlreadyOnPatternIndProperty)
      End Get
    End Property

    Public Shared AlreadyAddedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AlreadyAddedInd, "Already Selected")
    ''' <summary>
    ''' Gets the Already On Pattern value
    ''' </summary>
    <Display(Name:="Already Selected?", Description:="")>
    Public ReadOnly Property AlreadyAddedInd() As Boolean
      Get
        Return GetProperty(AlreadyAddedIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHRQuickSearch(dr As SafeDataReader) As ROHRQuickSearch

      Dim r As New ROHRQuickSearch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(IDNoProperty, .GetString(2))
        LoadProperty(EmployeeCodeProperty, .GetString(3))
        LoadProperty(AlreadyOnPatternIndProperty, .GetInt32(4))
        LoadProperty(AlreadyAddedIndProperty, .GetBoolean(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace