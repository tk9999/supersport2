﻿' Generated 16 Apr 2016 13:38 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceAvailability
    Inherits OBReadOnlyBase(Of ROHumanResourceAvailability)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROHumanResourceAvailabilityBO.ROHumanResourceAvailabilityBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared ImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ImageID, "ImageID", Nothing)
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ImageID", Description:="")>
    Public ReadOnly Property ImageID() As Integer?
      Get
        Return GetProperty(ImageIDProperty)
      End Get
    End Property

    Public Shared IsAvailableProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAvailable, "Is Available", False)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailable() As Boolean
      Get
        Return GetProperty(IsAvailableProperty)
      End Get
    End Property

    Public Shared IsAvailableStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsAvailableString, "Is Available", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailableString() As String
      Get
        Return GetProperty(IsAvailableStringProperty)
      End Get
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Required Hours")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public ReadOnly Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Total Hours")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
    Public ReadOnly Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
    End Property

    Public Shared HrsPercProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HrsPerc, "Hrs Perc")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Hrs Perc", Description:="")>
    Public ReadOnly Property HrsPerc() As Decimal
      Get
        Return GetProperty(HrsPercProperty)
      End Get
    End Property

    Public Shared HrsPercStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HrsPercString, "Hrs Perc String")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Hrs Perc String", Description:="")>
    Public ReadOnly Property HrsPercString() As String
      Get
        Return GetProperty(HrsPercStringProperty)
      End Get
    End Property

    Public Shared ProgressBarCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProgressBarCss, "ProgressBar Css")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ProgressBar Css", Description:="")>
    Public ReadOnly Property ProgressBarCss() As String
      Get
        Return GetProperty(ProgressBarCssProperty)
      End Get
    End Property

    Public Shared PublicHolidayCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PublicHolidayCount, "PH")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="PH", Description:="")>
    Public ReadOnly Property PublicHolidayCount() As Integer
      Get
        Return GetProperty(PublicHolidayCountProperty)
      End Get
    End Property

    Public Shared OffWeekendCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OffWeekendCount, "Off Wknds")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Off Wknds", Description:="")>
    Public ReadOnly Property OffWeekendCount() As Integer
      Get
        Return GetProperty(OffWeekendCountProperty)
      End Get
    End Property

    Public Shared ConsecutiveDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ConsecutiveDays, "Consec Days")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Consec Days", Description:="")>
    Public ReadOnly Property ConsecutiveDays() As Integer
      Get
        Return GetProperty(ConsecutiveDaysProperty)
      End Get
    End Property

    Public Shared ConsecutiveDaysCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ConsecutiveDaysCss, "Consec Days")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Consec Days", Description:="")>
    Public ReadOnly Property ConsecutiveDaysCss() As String
      Get
        Return GetProperty(ConsecutiveDaysCssProperty)
      End Get
    End Property

    Public Shared PrevShiftEndStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PrevShiftEndString, "PrevShiftEndString")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="PrevShiftEndString", Description:="")>
    Public ReadOnly Property PrevShiftEndString() As String
      Get
        Return GetProperty(PrevShiftEndStringProperty)
      End Get
    End Property

    Public Shared ShortfallPrevStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortfallPrevString, "Sfall Prev")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Sfall Prev", Description:="")>
    Public ReadOnly Property ShortfallPrevString() As String
      Get
        Return GetProperty(ShortfallPrevStringProperty)
      End Get
    End Property

    Public Shared ShortfallNextStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortfallNextString, "Sfall Next")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Sfall Next", Description:="")>
    Public ReadOnly Property ShortfallNextString() As String
      Get
        Return GetProperty(ShortfallNextStringProperty)
      End Get
    End Property

    Public Shared NextShiftStartStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextShiftStartString, "NextShiftStartString")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="NextShiftStartString", Description:="")>
    Public ReadOnly Property NextShiftStartString() As String
      Get
        Return GetProperty(NextShiftStartStringProperty)
      End Get
    End Property

    Public Shared ShortfallPrevCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortfallPrevCss, "ShortfallPrevCss")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ShortfallPrevCss", Description:="")>
    Public ReadOnly Property ShortfallPrevCss() As String
      Get
        Return GetProperty(ShortfallPrevCssProperty)
      End Get
    End Property

    Public Shared ShortfallNextCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortfallNextCss, "ShortfallNextCss")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ShortfallNextCss", Description:="")>
    Public ReadOnly Property ShortfallNextCss() As String
      Get
        Return GetProperty(ShortfallNextCssProperty)
      End Get
    End Property

    Public Shared ConsecutiveDaysStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ConsecutiveDaysString, "ConsecutiveDaysString")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ConsecutiveDaysString", Description:="")>
    Public ReadOnly Property ConsecutiveDaysString() As String
      Get
        Return GetProperty(ConsecutiveDaysStringProperty)
      End Get
    End Property

    Public Shared PublicHolidaysStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PublicHolidaysString, "PublicHolidaysString")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="PublicHolidaysString", Description:="")>
    Public ReadOnly Property PublicHolidaysString() As String
      Get
        Return GetProperty(PublicHolidaysStringProperty)
      End Get
    End Property

    Public Shared OffWeekendsStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffWeekendsString, "OffWeekendsString")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="OffWeekendsString", Description:="")>
    Public ReadOnly Property OffWeekendsString() As String
      Get
        Return GetProperty(OffWeekendsStringProperty)
      End Get
    End Property

    Public Shared ClashesCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClashesCss, "ClashesCss")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ClashesCss", Description:="")>
    Public ReadOnly Property ClashesCss() As String
      Get
        Return GetProperty(ClashesCssProperty)
      End Get
    End Property

    Public Shared ClashesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClashesTitle, "ClashesTitle")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ClashesTitle", Description:="")>
    Public ReadOnly Property ClashesTitle() As String
      Get
        Return GetProperty(ClashesTitleProperty)
      End Get
    End Property

    Public Shared ContractTypeCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractTypeCss, "ContractTypeCss")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ContractTypeCss", Description:="")>
    Public ReadOnly Property ContractTypeCss() As String
      Get
        Return GetProperty(ContractTypeCssProperty)
      End Get
    End Property

    Public Shared ImageFileNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageFileName, "ImageFileName")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="ImageFileName", Description:="")>
    Public ReadOnly Property ImageFileName() As String
      Get
        Return GetProperty(ImageFileNameProperty)
      End Get
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shifts")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Shifts", Description:="")>
    Public ReadOnly Property ShiftCount() As Integer
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="SystemID", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROHumanResourceAvailability(dr As SafeDataReader) As ROHumanResourceAvailability

      Dim r As New ROHumanResourceAvailability()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HRNameProperty, .GetString(2))
        LoadProperty(FirstnameProperty, .GetString(3))
        LoadProperty(PreferredNameProperty, .GetString(4))
        LoadProperty(SurnameProperty, .GetString(5))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ContractTypeProperty, .GetString(7))
        LoadProperty(ImageIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(IsAvailableProperty, .GetBoolean(9))
        LoadProperty(IsAvailableStringProperty, .GetString(10))
        LoadProperty(RequiredHoursProperty, .GetDecimal(11))
        LoadProperty(TotalHoursProperty, .GetDecimal(12))
        LoadProperty(HrsPercProperty, .GetDecimal(13))
        LoadProperty(HrsPercStringProperty, .GetString(14))
        LoadProperty(ProgressBarCssProperty, .GetString(15))
        LoadProperty(PublicHolidayCountProperty, .GetInt32(16))
        LoadProperty(OffWeekendCountProperty, .GetInt32(17))
        LoadProperty(ConsecutiveDaysProperty, .GetInt32(18))
        LoadProperty(ConsecutiveDaysCssProperty, .GetString(19))
        LoadProperty(PrevShiftEndStringProperty, .GetString(20))
        LoadProperty(ShortfallPrevStringProperty, .GetString(21))
        LoadProperty(ShortfallNextStringProperty, .GetString(22))
        LoadProperty(NextShiftStartStringProperty, .GetString(23))
        LoadProperty(ShortfallPrevCssProperty, .GetString(24))
        LoadProperty(ShortfallNextCssProperty, .GetString(25))
        LoadProperty(ConsecutiveDaysStringProperty, .GetString(26))
        LoadProperty(PublicHolidaysStringProperty, .GetString(27))
        LoadProperty(OffWeekendsStringProperty, .GetString(28))
        LoadProperty(ClashesCssProperty, .GetString(29))
        LoadProperty(ClashesTitleProperty, .GetString(30))
        LoadProperty(ContractTypeCssProperty, .GetString(31))
        LoadProperty(ImageFileNameProperty, .GetString(32))
        LoadProperty(ShiftCountProperty, .GetInt32(33))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(34)))
      End With

    End Sub

#End Region

  End Class

End Namespace