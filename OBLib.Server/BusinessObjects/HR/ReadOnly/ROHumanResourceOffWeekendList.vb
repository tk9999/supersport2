﻿' Generated 17 Apr 2015 12:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc


Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceOffWeekendList
    Inherits SingularReadOnlyListBase(Of ROHumanResourceOffWeekendList, ROHumanResourceOffWeekend)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourceOffWeekend

      For Each child As ROHumanResourceOffWeekend In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetListOfShiftPatterIDs() As List(Of Integer)

      Dim list As New List(Of Integer)
      For Each item As ROHumanResourceOffWeekend In Me.OrderBy(Function(c) c.ShiftPatternID)  'SortList("ShiftPatternID", ListSortDirection.Ascending)
        If Not list.Contains(item.ShiftPatternID) Then
          list.Add(item.ShiftPatternID)
        End If
      Next
      Return list

    End Function

    Public Function GetNames() As String

      Dim names As String = ""
      For i As Integer = 0 To Me.Count - 1
        Dim item As ROHumanResourceOffWeekend = Me(i)
        names &= item.Name
        If i < Me.Count - 1 Then
          names &= "/"
        End If
      Next
      Return names

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public StartDate As SmartDate
      Public EndDate As SmartDate
      Public DisciplineID As CommonData.Enums.Discipline

      Public Sub New()

      End Sub

      Public Sub New(StartDate As Date, EndDate As Date, DisciplineID As CommonData.Enums.Discipline)

        Me.StartDate = New SmartDate(StartDate)
        Me.EndDate = New SmartDate(EndDate)
        Me.DisciplineID = DisciplineID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceOffWeekendList() As ROHumanResourceOffWeekendList

      Return New ROHumanResourceOffWeekendList()

    End Function

    Public Shared Sub BeginGetROHumanResourceOffWeekendList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceOffWeekendList)))

      Dim dp As New DataPortal(Of ROHumanResourceOffWeekendList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHumanResourceOffWeekendList(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceOffWeekendList)))

      Dim dp As New DataPortal(Of ROHumanResourceOffWeekendList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceOffWeekendList(StartDate As Date, EndDate As Date, DisciplineID As CommonData.Enums.Discipline) As ROHumanResourceOffWeekendList

      Return DataPortal.Fetch(Of ROHumanResourceOffWeekendList)(New Criteria(StartDate, EndDate, DisciplineID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceOffWeekend.GetROHumanResourceOffWeekend(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceOffWeekendList"
            cm.Parameters.AddWithValue("@StartDate", crit.StartDate.DBValue)
            cm.Parameters.AddWithValue("@EndDate", crit.EndDate.DBValue)
            cm.Parameters.AddWithValue("@DisciplineID", crit.DisciplineID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace