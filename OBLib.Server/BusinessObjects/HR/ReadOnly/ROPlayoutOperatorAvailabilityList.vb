﻿' Generated 11 Sep 2016 22:54 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROPlayoutOperatorAvailabilityList
    Inherits OBReadOnlyListBase(Of ROPlayoutOperatorAvailabilityList, ROPlayoutOperatorAvailability)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROPlayoutOperatorAvailability

      For Each child As ROPlayoutOperatorAvailability In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property Controller As String = ""
      Public Property StartDateTime As DateTime? = Nothing
      Public Property EndDateTime As DateTime? = Nothing
      Public Property DisciplineID As Integer? = Nothing

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROPlayoutOperatorAvailabilityList() As ROPlayoutOperatorAvailabilityList

      Return New ROPlayoutOperatorAvailabilityList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROPlayoutOperatorAvailabilityList() As ROPlayoutOperatorAvailabilityList

      Return DataPortal.Fetch(Of ROPlayoutOperatorAvailabilityList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPlayoutOperatorAvailability.GetROPlayoutOperatorAvailability(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceListPlayoutControllerAvailability"
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@Controller", Strings.MakeEmptyDBNull(crit.Controller))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace