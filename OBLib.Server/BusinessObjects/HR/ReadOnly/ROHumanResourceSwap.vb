﻿' Generated 21 Sep 2015 09:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSwap
    Inherits OBReadOnlyBase(Of ROHumanResourceSwap)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSwapIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSwapID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceSwapID() As Integer
      Get
        Return GetProperty(HumanResourceSwapIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource requesting the swap")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SwapDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SwapDate, "Swap Date")
    ''' <summary>
    ''' Gets the Swap Date value
    ''' </summary>
    <Display(Name:="Swap Date", Description:="The date to swap")>
    Public ReadOnly Property SwapDate As DateTime?
      Get
        Return GetProperty(SwapDateProperty)
      End Get
    End Property

    Public Shared SwapWithHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SwapWithHumanResourceID, "Swap With Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Swap With Human Resource value
    ''' </summary>
    <Display(Name:="Swap With Human Resource", Description:="The human resource to swap with")>
    Public ReadOnly Property SwapWithHumanResourceID() As Integer?
      Get
        Return GetProperty(SwapWithHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ApprovedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Approved, "Approved", False)
    ''' <summary>
    ''' Gets the Approved value
    ''' </summary>
    <Display(Name:="Approved", Description:="Is the swap approved")>
    Public ReadOnly Property Approved() As Boolean
      Get
        Return GetProperty(ApprovedProperty)
      End Get
    End Property

    Public Shared ApprovedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ApprovedBy, "Approved By", Nothing)
    ''' <summary>
    ''' Gets the Approved By value
    ''' </summary>
    <Display(Name:="Approved By", Description:="The user who approved the swap")>
    Public ReadOnly Property ApprovedBy() As Integer?
      Get
        Return GetProperty(ApprovedByProperty)
      End Get
    End Property

    Public Shared ApprovedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ApprovedDateTime, "Approved Date Time")
    ''' <summary>
    ''' Gets the Approved Date Time value
    ''' </summary>
    <Display(Name:="Approved Date Time", Description:="The date and time at which the swap was approved")>
    Public ReadOnly Property ApprovedDateTime As DateTime?
      Get
        Return GetProperty(ApprovedDateTimeProperty)
      End Get
    End Property

    Public Shared SwapWithHumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SwapWithHumanResource, "Swap with", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Swap with")>
    Public ReadOnly Property SwapWithHumanResource() As String
      Get
        Return GetProperty(SwapWithHumanResourceProperty)
      End Get
    End Property

    Public Shared SwapWithDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SwapWithDate, "Swapped with Date")
    ''' <summary>
    ''' Gets and sets the Swap Date value
    ''' </summary>
    <Display(Name:="Swapped with Date")>
    Public ReadOnly Property SwapWithDate As DateTime?
      Get
        Return GetProperty(SwapWithDateProperty)
      End Get
    End Property

    Public Shared ApproverNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ApproverName, "Approved By", "")
    ''' <summary>
    ''' Gets and sets the Approved Date Time value
    ''' </summary>
    <Display(Name:="Approved By")>
    Public ReadOnly Property ApproverName As String
      Get
        Return GetProperty(ApproverNameProperty)
      End Get
    End Property

    <Display(Name:="Shift Date")>
    Public ReadOnly Property SwapDateString As String
      Get
        Return SwapDate.Value.ToString("ddd dd MMM yy")
      End Get
    End Property

    <Display(Name:="Swap with Date")>
    Public ReadOnly Property SwapWithDateString As String
      Get
        Return SwapWithDate.Value.ToString("ddd dd MMM yy")
      End Get
    End Property

    <Display(Name:="Status")>
    Public ReadOnly Property Status As String
      Get
        If Approved Then
          Return "Approved by " & ApproverName & " on " & ApprovedDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        Else
          Return "Awaiting Approval"
        End If
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSwapIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResourceSwapID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceSwap(dr As SafeDataReader) As ROHumanResourceSwap

      Dim r As New ROHumanResourceSwap()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceSwapIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SwapDateProperty, .GetValue(2))
        LoadProperty(SwapWithHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(SwapWithDateProperty, .GetValue(4))
        LoadProperty(ApprovedProperty, .GetBoolean(5))
        LoadProperty(ApprovedByProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ApprovedDateTimeProperty, .GetValue(7))
        LoadProperty(ApproverNameProperty, .GetString(8))
        LoadProperty(SwapWithHumanResourceProperty, .GetString(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace