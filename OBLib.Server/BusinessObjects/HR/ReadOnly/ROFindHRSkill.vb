﻿' Generated 22 Aug 2014 07:58 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROFindHRSkill
    Inherits OBReadOnlyBase(Of ROFindHRSkill)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
    Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    Public Shared SkillLevelProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SkillLevel, "Skill Level", "")
    ''' <summary>
    ''' Gets the Skill Level value
    ''' </summary>
    <Display(Name:="Skill Level", Description:="")>
    Public ReadOnly Property SkillLevel() As String
      Get
        Return GetProperty(SkillLevelProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DisciplineProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Discipline

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROFindHRSkill(dr As SafeDataReader) As ROFindHRSkill

      Dim r As New ROFindHRSkill()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DisciplineProperty, .GetString(0))
        LoadProperty(PrimaryIndProperty, .GetBoolean(1))
        LoadProperty(SkillLevelProperty, .GetString(2))
        LoadProperty(StartDateProperty, .GetValue(3))
        LoadProperty(EndDateProperty, .GetValue(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace