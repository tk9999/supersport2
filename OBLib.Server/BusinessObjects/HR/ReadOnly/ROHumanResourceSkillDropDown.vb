﻿' Generated 19 May 2016 11:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSkillDropDown
    Inherits OBReadOnlyBase(Of ROHumanResourceSkillDropDown)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROHumanResourceSkillDropDownBO.ROHumanResourceSkillDropDownBOToString(self)")

    Public Shared HumanResourceSkillIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSkillID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property HumanResourceSkillID() As Integer
      Get
        Return GetProperty(HumanResourceSkillIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
  Public ReadOnly Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
  Public ReadOnly Property EndDate As Date
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
  Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    '  Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    '  ''' <summary>
    '  ''' Gets the Created By value
    '  ''' </summary>
    '  <Display(AutoGenerateField:=False)>
    'Public ReadOnly Property CreatedBy() As Integer
    '    Get
    '      Return GetProperty(CreatedByProperty)
    '    End Get
    '  End Property

    '  Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    '  ''' <summary>
    '  ''' Gets the Created Date Time value
    '  ''' </summary>
    '  <Display(AutoGenerateField:=False)>
    'Public ReadOnly Property CreatedDateTime() As DateTime
    '    Get
    '      Return GetProperty(CreatedDateTimeProperty)
    '    End Get
    '  End Property

    '  Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    '  ''' <summary>
    '  ''' Gets the Modified By value
    '  ''' </summary>
    '  <Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ModifiedBy() As Integer
    '    Get
    '      Return GetProperty(ModifiedByProperty)
    '    End Get
    '  End Property

    '  Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    '  ''' <summary>
    '  ''' Gets the Modified Date Time value
    '  ''' </summary>
    '  <Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ModifiedDateTime() As DateTime
    '    Get
    '      Return GetProperty(ModifiedDateTimeProperty)
    '    End Get
    '  End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSkillIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResourceSkillID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROHumanResourceSkillDropDown(dr As SafeDataReader) As ROHumanResourceSkillDropDown

      Dim r As New ROHumanResourceSkillDropDown()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceSkillIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DisciplineProperty, .GetString(3))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(EndDateProperty, .GetValue(5))
        LoadProperty(PrimaryIndProperty, .GetBoolean(6))
        'LoadProperty(CreatedByProperty, .GetInt32(7))
        'LoadProperty(CreatedDateTimeProperty, .GetDateTime(8))
        'LoadProperty(ModifiedByProperty, .GetInt32(9))
        'LoadProperty(ModifiedDateTimeProperty, .GetDateTime(10))
      End With

    End Sub

#End Region

  End Class

End Namespace