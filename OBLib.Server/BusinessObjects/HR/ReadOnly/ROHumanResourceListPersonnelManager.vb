﻿' Generated 09 Nov 2014 12:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceListPersonnelManager
    Inherits OBReadOnlyListBase(Of ROHumanResourceListPersonnelManager, ROHumanResourcePersonnelManager)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHumanResourcePersonnelManager

      For Each child As ROHumanResourcePersonnelManager In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemIDs As List(Of Integer) = New List(Of Integer)
      Public Property ProductionAreaIDs As List(Of Integer) = New List(Of Integer)
      Public Property ContractTypeIDs As List(Of Integer) = New List(Of Integer)
      Public Property DisciplineIDs As List(Of Integer) = New List(Of Integer)
      Public Property SystemTeamNumberIDs As List(Of Integer) = New List(Of Integer)
      Public Property OnlyBookedInd As Boolean = False
      Public Property SelectAllHrInd As Boolean = False
      Public Property StartDateTime As Date? = Singular.Dates.DateMonthStart(Now)
      Public Property EndDateTime As Date? = Singular.Dates.DateMonthEnd(Now)

      Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "Firstname", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Firstname"),
       SetExpression("PersonnelManagers.FirstNameSet(self)", , 250), TextField>
      Public Overridable Property FirstName As String
        Get
          Return ReadProperty(FirstNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(FirstNameProperty, Value)
        End Set
      End Property

      Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Surname"),
       SetExpression("PersonnelManagers.SurnameSet(self)", , 250), TextField>
      Public Overridable Property Surname As String
        Get
          Return ReadProperty(SurnameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SurnameProperty, Value)
        End Set
      End Property

      Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Pref. Name"),
       SetExpression("PersonnelManagers.PreferredNameSet(self)", , 250), TextField>
      Public Overridable Property PreferredName As String
        Get
          Return ReadProperty(PreferredNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PreferredNameProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceListPersonnelManager() As ROHumanResourceListPersonnelManager

      Return New ROHumanResourceListPersonnelManager()

    End Function

    Public Shared Sub BeginGetROHumanResourceListPersonnelManager(CallBack As EventHandler(Of DataPortalResult(Of ROHumanResourceListPersonnelManager)))

      Dim dp As New DataPortal(Of ROHumanResourceListPersonnelManager)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceListPersonnelManager() As ROHumanResourceListPersonnelManager

      Return DataPortal.Fetch(Of ROHumanResourceListPersonnelManager)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROHumanResourcePersonnelManager.GetROHumanResourcePersonnelManager(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceListPersonnelManager"
            cm.Parameters.AddWithValue("@OnlyBookedInd", crit.OnlyBookedInd)
            cm.Parameters.AddWithValue("@FirstName", Singular.Strings.MakeEmptyDBNull(crit.FirstName))
            cm.Parameters.AddWithValue("@Surname", Singular.Strings.MakeEmptyDBNull(crit.Surname))
            cm.Parameters.AddWithValue("@PreferredName", Singular.Strings.MakeEmptyDBNull(crit.PreferredName))
            cm.Parameters.AddWithValue("@SystemIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ContractTypeIDs)))
            cm.Parameters.AddWithValue("@DisciplineIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.DisciplineIDs)))
            cm.Parameters.AddWithValue("@SystemTeamNumberIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemTeamNumberIDs)))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDateTime))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    'Friend Sub Update()

    '  Me.RaiseListChangedEvents = False
    '  Try
    '    ' Loop through each deleted child object and call its Update() method
    '    For Each Child As ROHumanResourcePersonnelManager In DeletedList
    '      Child.DeleteSelf()
    '    Next

    '    ' Then clear the list of deleted objects because they are truly gone now.
    '    DeletedList.Clear()

    '    ' Loop through each non-deleted child object and call its Update() method
    '    For Each Child As ROHumanResourcePersonnelManager In Me
    '      If Child.IsNew Then
    '        Child.Insert()
    '      Else
    '        Child.Update()
    '      End If
    '    Next
    '  Finally
    '    Me.RaiseListChangedEvents = True
    '  End Try

    'End Sub

    'Protected Overrides Sub DataPortal_Update()

    '  UpdateTransactional(AddressOf Update)

    'End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
