﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class ROHumanResourceUnAvailabilityList
    Inherits OBReadOnlyListBase(Of ROHumanResourceUnAvailabilityList, ROHumanResourceUnAvailability)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ROHumanResourceUnAvailabilityID As Integer) As ROHumanResourceUnAvailability

      For Each child As ROHumanResourceUnAvailability In Me
        If child.HumanResourceOffPeriodID = ROHumanResourceUnAvailabilityID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetOffPeriodBooking(ResourceBookingID As Integer) As OffPeriodBooking

    '  For Each child As ROHumanResourceUnAvailability In Me
    '    For Each b As OffPeriodBooking In child.OffPeriodBookingList
    '      If b.ResourceBookingID = ResourceBookingID Then
    '        Return b
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Off Periods"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property StartDate As Date?
      Public Property EndDate As Date?

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHumanResourceUnAvailabilityList() As ROHumanResourceUnAvailabilityList

      Return New ROHumanResourceUnAvailabilityList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHumanResourceUnAvailabilityList() As ROHumanResourceUnAvailabilityList

      Return DataPortal.Fetch(Of ROHumanResourceUnAvailabilityList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHumanResourceUnAvailability.GetROHumanResourceUnAvailability(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHumanResourceUnAvailabilityList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Public Shared Sub DeleteBulkIndependant(Items As List(Of ROHumanResourceUnAvailability))

      If Items.Count > 0 Then
        Dim ids As String = OBLib.OBMisc.StringArrayToXML(Items.Select(Function(d) d.HumanResourceOffPeriodID.ToString).ToArray)
        Using cn As New SqlConnection(Singular.Settings.ConnectionString)
          cn.Open()
          Try
            Using cm As SqlCommand = cn.CreateCommand
              cm.CommandType = CommandType.StoredProcedure
              cm.CommandText = "DelProcsWeb.delHumanResourceUnAvailabilityListBulk"
              cm.Parameters.AddWithValue("@HumanResourceOffPeriodIDs", Strings.MakeEmptyDBNull(ids))
              cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
              cm.ExecuteNonQuery()
            End Using
          Finally
            cn.Close()
          End Try
        End Using
      End If

    End Sub

#End Region

#End Region

  End Class

End Namespace