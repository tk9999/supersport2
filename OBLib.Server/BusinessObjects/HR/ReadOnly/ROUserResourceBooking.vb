﻿' Generated 20 Jun 2015 07:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace HR.PersonnelManager.ReadOnly

  <Serializable()> _
  Public Class ROUserResourceBooking
    Inherits SingularReadOnlyBase(Of ROUserResourceBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    Required(ErrorMessage:="Resource Booking Type required")>
    Public ReadOnly Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:=""),
    StringLength(500, ErrorMessage:="Resource Booking Description cannot be more than 500 characters")>
    Public ReadOnly Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
    Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:=""),
    Required(ErrorMessage:="Start Date Time required")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:=""),
    Required(ErrorMessage:="End Date Time required")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
    Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedBy, "Is Being Edited By", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Being Edited", Description:="")>
    Public ReadOnly Property IsBeingEditedBy() As String
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.InEditDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time Start value
    ''' </summary>
    <Display(Name:="Edit Time", Description:="")>
    Public ReadOnly Property InEditDateTime As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public ReadOnly Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "HumanResourceShiftID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public ReadOnly Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Being Edited", Description:="")>
    Public ReadOnly Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "HumanResourceOffPeriodID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public ReadOnly Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "HumanResourceSecondmentID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="HumanResourceSecondmentID", Description:="")>
    Public ReadOnly Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
    End Property

    Public Shared ReadOnly ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHRID, "ProductionHRID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ProductionHRID", Description:="")>
    Public ReadOnly Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "StatusCssClass", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:="")>
    Public ReadOnly Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
    End Property

    Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IgnoreClashes, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="IgnoreClashes", Description:="")>
    Public ReadOnly Property IgnoreClashes() As Boolean
      Get
        Return GetProperty(IgnoreClashesProperty)
      End Get
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "IgnoreClashesReason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:="")>
    Public ReadOnly Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "RoomScheduleID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="RoomScheduleID", Description:="")>
    Public ReadOnly Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared IsLockedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsLocked, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="IsLocked", Description:="")>
    Public ReadOnly Property IsLocked() As Boolean
      Get
        Return GetProperty(IsLockedProperty)
      End Get
    End Property

    Public Shared IsLockedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsLockedReason, "IsLockedReason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:="")>
    Public ReadOnly Property IsLockedReason() As String
      Get
        Return GetProperty(IsLockedReasonProperty)
      End Get
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsTBC, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="IsTBC", Description:="")>
    Public ReadOnly Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceBookingDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserResourceBooking(dr As SafeDataReader) As ROUserResourceBooking

      Dim r As New ROUserResourceBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
        LoadProperty(StartDateTimeProperty, .GetValue(5))
        LoadProperty(EndDateTimeProperty, .GetValue(6))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
        LoadProperty(IsBeingEditedByProperty, .GetString(8))
        LoadProperty(InEditDateTimeProperty, .GetValue(9))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(IsCancelledProperty, .GetBoolean(16))
        LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(StatusCssClassProperty, .GetString(20))
        LoadProperty(IgnoreClashesProperty, .GetBoolean(21))
        LoadProperty(IgnoreClashesReasonProperty, .GetString(22))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(IsLockedProperty, .GetBoolean(24))
        LoadProperty(IsLockedReasonProperty, .GetString(25))
        LoadProperty(IsTBCProperty, .GetBoolean(26))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace