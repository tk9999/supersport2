﻿' Generated 09 Mar 2016 03:13 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROOtherDisciplinesOnLeaveList
    Inherits OBReadOnlyListBase(Of ROOtherDisciplinesOnLeaveList, ROOtherDisciplinesOnLeave)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROOtherDisciplinesOnLeave

      For Each child As ROOtherDisciplinesOnLeave In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property StartDate As DateTime?
      Public Property EndDate As DateTime?

      Public Sub New(HumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?)
        Me.HumanResourceID = HumanResourceID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROOtherDisciplinesOnLeaveList() As ROOtherDisciplinesOnLeaveList

      Return New ROOtherDisciplinesOnLeaveList()

    End Function

    Public Shared Sub BeginGetROOtherDisciplinesOnLeaveList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROOtherDisciplinesOnLeaveList)))

      Dim dp As New DataPortal(Of ROOtherDisciplinesOnLeaveList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROOtherDisciplinesOnLeaveList(CallBack As EventHandler(Of DataPortalResult(Of ROOtherDisciplinesOnLeaveList)))

      Dim dp As New DataPortal(Of ROOtherDisciplinesOnLeaveList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROOtherDisciplinesOnLeaveList() As ROOtherDisciplinesOnLeaveList

      Return DataPortal.Fetch(Of ROOtherDisciplinesOnLeaveList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROOtherDisciplinesOnLeave.GetROOtherDisciplinesOnLeave(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROOtherDisciplinesOnLeaveList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace