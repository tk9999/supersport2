﻿' Generated 16 Apr 2016 13:38 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROUnitSupervisorList
    Inherits OBReadOnlyListBase(Of ROUnitSupervisorList, ROUnitSupervisor)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROUnitSupervisor

      For Each child As ROUnitSupervisor In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property AtDate As DateTime? = Nothing

      Public Sub New(AtDate As DateTime?)
        Me.AtDate = AtDate
      End Sub

    End Class

    Public Shared Function NewROUnitSupervisorList() As ROUnitSupervisorList

      Return New ROUnitSupervisorList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROUnitSupervisorList(AtDate As DateTime) As ROUnitSupervisorList

      Return DataPortal.Fetch(Of ROUnitSupervisorList)(New Criteria(AtDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUnitSupervisor.GetROUnitSupervisor(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROHumanResourceListUnitSupervisor]"
            cm.Parameters.AddWithValue("@AtDate", NothingDBNull(crit.AtDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace