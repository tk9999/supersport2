﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

    <Serializable()> _
    Public Class ROHumanResourceSkillOld
        Inherits OBReadOnlyBase(Of ROHumanResourceSkillOld)

#Region " Properties and Methods "

#Region " Properties "

        Public Shared HumanResourceSkillIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSkillID, "ID", 0)
        ''' <summary>
        ''' Gets the ID value
        ''' </summary>
        <Display(AutoGenerateField:=False), Key()>
        Public ReadOnly Property HumanResourceSkillID() As Integer
            Get
                Return GetProperty(HumanResourceSkillIDProperty)
            End Get
        End Property

        Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
        ''' <summary>
        ''' Gets the Human Resource value
        ''' </summary>
        <Display(AutoGenerateField:=False)>
        Public ReadOnly Property HumanResourceID() As Integer?
            Get
                Return GetProperty(HumanResourceIDProperty)
            End Get
        End Property

        Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
        ''' <summary>
        ''' Gets the Crew Type value
        ''' </summary>
        <Display(Name:="Crew Type", Description:="The type of crew that this human resource can be included in a production")>
        Public ReadOnly Property CrewTypeID() As Integer?
            Get
                Return GetProperty(CrewTypeIDProperty)
            End Get
        End Property

        Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
        ''' <summary>
        ''' Gets the Discipline value
        ''' </summary>
        <Display(Name:="Discipline", Description:="The discipline that this human resource is skill in")>
        Public ReadOnly Property DisciplineID() As Integer?
            Get
                Return GetProperty(DisciplineIDProperty)
            End Get
        End Property

        Public Shared SkillLevelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SkillLevelID, "Skill Level", Nothing)
        ''' <summary>
        ''' Gets the Skill Level value
        ''' </summary>
        <Display(Name:="Skill Level", Description:="The skill level of the human resource. Used when allocating human resource to productions")>
        Public ReadOnly Property SkillLevelID() As Integer?
            Get
                Return GetProperty(SkillLevelIDProperty)
            End Get
        End Property

        Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
        ''' <summary>
        ''' Gets the Start Date value
        ''' </summary>
        <Display(Name:="Start Date", Description:="Date the person started in this discipline. Used for skill level experience.")>
        Public ReadOnly Property StartDate As DateTime?
            Get
                Return GetProperty(StartDateProperty)
            End Get
        End Property

        Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
        ''' <summary>
        ''' Gets the End Date value
        ''' </summary>
        <Display(Name:="End Date", Description:="Last date on which Human Resource can be booked for this skill")>
        Public ReadOnly Property EndDate As DateTime?
            Get
                Return GetProperty(EndDateProperty)
            End Get
        End Property

        Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
        ''' <summary>
        ''' Gets the Primary value
        ''' </summary>
        <Display(Name:="Primary", Description:="Tick indicates the this discipline is the primary one for the HR")>
        Public ReadOnly Property PrimaryInd() As Boolean
            Get
                Return GetProperty(PrimaryIndProperty)
            End Get
        End Property

        Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
        ''' <summary>
        ''' Gets the Crew Leader value
        ''' </summary>
        <Display(Name:="Crew Leader", Description:="Tick indicates that this human resource is skilled to be a crew leader on a production for this discipline")>
        Public ReadOnly Property CrewLeaderInd() As Boolean
            Get
                Return GetProperty(CrewLeaderIndProperty)
            End Get
        End Property

        Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
        ''' <summary>
        ''' Gets the Created By value
        ''' </summary>
        <Display(AutoGenerateField:=False)>
        Public ReadOnly Property CreatedBy() As Integer?
            Get
                Return GetProperty(CreatedByProperty)
            End Get
        End Property

        Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
        ''' <summary>
        ''' Gets the Created Date Time value
        ''' </summary>
        <Display(AutoGenerateField:=False)>
        Public ReadOnly Property CreatedDateTime() As DateTime?
            Get
                Return GetProperty(CreatedDateTimeProperty)
            End Get
        End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
        ''' <summary>
        ''' Gets the Modified By value
        ''' </summary>
        <Display(AutoGenerateField:=False)>
        Public ReadOnly Property ModifiedBy() As Integer?
            Get
                Return GetProperty(ModifiedByProperty)
            End Get
        End Property

        Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
        ''' <summary>
        ''' Gets the Modified Date Time value
        ''' </summary>
        <Display(AutoGenerateField:=False)>
        Public ReadOnly Property ModifiedDateTime() As DateTime?
            Get
                Return GetProperty(ModifiedDateTimeProperty)
            End Get
        End Property

        Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
        Public ReadOnly Property Discipline() As String
            Get
                Return GetProperty(DisciplineProperty)
            End Get
        End Property

        Public Shared SkillLevelProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SkillLevel, "Skill Level", "")
        Public ReadOnly Property SkillLevel() As String
            Get
                Return GetProperty(SkillLevelProperty)
            End Get
        End Property

        Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
        Public ReadOnly Property CreatedByName() As String
            Get
                Return GetProperty(CreatedByNameProperty)
            End Get
        End Property

        <Display(Name:="Start Date")>
        Public ReadOnly Property StartDateDisplay() As String
            Get
                If StartDate IsNot Nothing Then
                    Return StartDate.Value.ToString("ddd dd MMM yyyy")
                End If
                Return ""
            End Get
        End Property

        <Display(Name:="End Date")>
        Public ReadOnly Property EndDateDisplay() As String
            Get
                If EndDate IsNot Nothing Then
                    Return EndDate.Value.ToString("ddd dd MMM yyyy")
                End If
                Return ""
            End Get
        End Property

        <Display(Name:="Creation Details")>
        Public ReadOnly Property CreateDetails() As String
            Get
                Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
            End Get
        End Property

        Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Bookings", 0)
        ''' <summary>
        ''' Gets the Discipline value
        ''' </summary>
        <Display(Name:="Bookings")>
        Public ReadOnly Property BookingCount() As Integer
            Get
                Return GetProperty(BookingCountProperty)
            End Get
        End Property

        Public Shared PositionCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionCount, "PositionCount", 0)
        ''' <summary>
        ''' Gets the Discipline value
        ''' </summary>
        <Display(Name:="Num. Positions")>
        Public ReadOnly Property PositionCount() As Integer
            Get
                Return GetProperty(PositionCountProperty)
            End Get
        End Property

        Public Shared RateCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RateCount, "RateCount", 0)
        ''' <summary>
        ''' Gets the Discipline value
        ''' </summary>
        <Display(Name:="Rate Count")>
        Public ReadOnly Property RateCount() As Integer
            Get
                Return GetProperty(RateCountProperty)
            End Get
        End Property

#End Region

#Region " Child Lists "

    Public Shared ROHumanResourceSkillPositionListOldProperty As PropertyInfo(Of ROHumanResourceSkillPositionListOld) = RegisterProperty(Of ROHumanResourceSkillPositionListOld)(Function(c) c.ROHumanResourceSkillPositionListOld, "RO Human Resource Skill Position List")

    Public ReadOnly Property ROHumanResourceSkillPositionListOld() As ROHumanResourceSkillPositionListOld
      Get
        If GetProperty(ROHumanResourceSkillPositionListOldProperty) Is Nothing Then
          LoadProperty(ROHumanResourceSkillPositionListOldProperty, HR.ReadOnly.ROHumanResourceSkillPositionListOld.NewROHumanResourceSkillPositionListOld())
        End If
        Return GetProperty(ROHumanResourceSkillPositionListOldProperty)
      End Get
    End Property

    '    Public Shared ROHumanResourceSkillOldRateListProperty As PropertyInfo(Of ROHumanResourceSkillOldRateList) = RegisterProperty(Of ROHumanResourceSkillOldRateList)(Function(c) c.ROHumanResourceSkillOldRateList, "RO Human Resource Skill Rate List")

    '    Public ReadOnly Property ROHumanResourceSkillOldRateList() As ROHumanResourceSkillOldRateList
    '      Get
    '        If GetProperty(ROHumanResourceSkillOldRateListProperty) Is Nothing Then
    '          LoadProperty(ROHumanResourceSkillOldRateListProperty, HR.ReadOnly.ROHumanResourceSkillOldRateList.NewROHumanResourceSkillOldRateList())
    '        End If
    '        Return GetProperty(ROHumanResourceSkillOldRateListProperty)
    '      End Get
    '    End Property

#End Region

#Region " Methods "

        Protected Overrides Function GetIdValue() As Object

            Return GetProperty(HumanResourceSkillIDProperty)

        End Function

        Public Overrides Function ToString() As String

            Return Me.CreatedDateTime.ToString()

        End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

        Friend Shared Function GetROHumanResourceSkillOld(dr As SafeDataReader) As ROHumanResourceSkillOld

            Dim r As New ROHumanResourceSkillOld()
            r.Fetch(dr)
            Return r

        End Function

        Protected Sub Fetch(sdr As SafeDataReader)

            With sdr
                LoadProperty(HumanResourceSkillIDProperty, .GetInt32(0))
                LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
                LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
                LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
                LoadProperty(SkillLevelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
                LoadProperty(StartDateProperty, .GetValue(5))
                LoadProperty(EndDateProperty, .GetValue(6))
                LoadProperty(PrimaryIndProperty, .GetBoolean(7))
                LoadProperty(CrewLeaderIndProperty, .GetBoolean(8))
                LoadProperty(CreatedByProperty, .GetInt32(9))
                LoadProperty(CreatedDateTimeProperty, .GetValue(10))
                LoadProperty(ModifiedByProperty, .GetInt32(11))
                LoadProperty(ModifiedDateTimeProperty, .GetValue(12))
                LoadProperty(DisciplineProperty, .GetString(13))
                LoadProperty(SkillLevelProperty, .GetString(14))
                LoadProperty(CreatedByNameProperty, .GetString(15))
                LoadProperty(BookingCountProperty, .GetInt32(17))
                LoadProperty(PositionCountProperty, .GetInt32(18))
                LoadProperty(RateCountProperty, .GetInt32(19))
            End With

        End Sub

#End If

#End Region

#End Region

    End Class

End Namespace