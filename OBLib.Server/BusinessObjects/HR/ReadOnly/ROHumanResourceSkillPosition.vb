﻿' Generated 26 Jul 2014 22:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROHumanResourceSkillPosition
    Inherits SingularReadOnlyBase(Of ROHumanResourceSkillPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSkillPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSkillPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceSkillPositionID() As Integer
      Get
        Return GetProperty(HumanResourceSkillPositionIDProperty)
      End Get
    End Property

    Public Shared HumanResourceSkillIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSkillID, "Human Resource Skill", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Skill value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceSkillID() As Integer?
      Get
        Return GetProperty(HumanResourceSkillIDProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The production type that this human resource is skill in for the position"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="The camera position that this human resource is skilled in"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Date the person started in this position. Used for skill level experience.")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PositionLevelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionLevelID, "Position Level", Nothing)
    ''' <summary>
    ''' Gets the Position Level value
    ''' </summary>
    <Display(Name:="Position Level", Description:="")>
    Public ReadOnly Property PositionLevelID() As Integer?
      Get
        Return GetProperty(PositionLevelIDProperty)
      End Get
    End Property

    Public ReadOnly Property ProductionType() As String
      Get
        If ProductionTypeID IsNot Nothing Then
          Dim ROProductionType As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType = CommonData.Lists.ROProductionTypeList.GetItem(GetProperty(ProductionTypeIDProperty))
          If Not IsNullNothing(ROProductionType) Then
            Return ROProductionType.ProductionType
          End If
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property Position() As String
      Get
        If PositionID IsNot Nothing Then
          Dim ROPosition As ROPosition = CommonData.Lists.ROPositionList.GetItem(GetProperty(PositionIDProperty))
          If Not IsNullNothing(ROPosition) Then
            Return ROPosition.Position
          End If
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSkillPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceSkillPosition(dr As SafeDataReader) As ROHumanResourceSkillPosition

      Dim r As New ROHumanResourceSkillPosition()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceSkillPositionIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceSkillIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(PositionLevelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace