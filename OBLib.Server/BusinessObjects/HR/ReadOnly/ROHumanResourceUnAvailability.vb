﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class ROHumanResourceUnAvailability
    Inherits OBReadOnlyBase(Of ROHumanResourceUnAvailability)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROHumanResourceUnAvailabilityBO.ROHumanResourceUnAvailabilityBOToString(self)")

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceOffPeriodID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property HumanResourceOffPeriodID() As Integer
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The start time for the off period")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Time", Description:="The end time for the off period")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Reason", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Reason"),
    StringLength(200)>
    Public ReadOnly Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
    End Property

    Public Shared StartDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartDateString, "Start Time", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Start Time")>
    Public ReadOnly Property StartDateString() As String
      Get
        Return GetProperty(StartDateStringProperty)
      End Get
    End Property

    Public Shared EndDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndDateString, "End Time", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="End Time")>
    Public ReadOnly Property EndDateString() As String
      Get
        Return GetProperty(EndDateStringProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceOffPeriodIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Detail

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Public Shared Function NewROHumanResourceUnAvailability() As ROHumanResourceUnAvailability

      Return DataPortal.CreateChild(Of ROHumanResourceUnAvailability)()

    End Function

    Public Sub New()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHumanResourceUnAvailability(dr As SafeDataReader) As ROHumanResourceUnAvailability

      Dim h As New ROHumanResourceUnAvailability()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceOffPeriodIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DetailProperty, .GetString(2))
        LoadProperty(StartDateProperty, .GetValue(3))
        LoadProperty(EndDateProperty, .GetValue(4))
        LoadProperty(StartDateStringProperty, .GetString(5))
        LoadProperty(EndDateStringProperty, .GetString(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace