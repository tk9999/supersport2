﻿' Generated 27 Oct 2014 08:27 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR

  <Serializable()> _
  Public Class ManagerSubstitute
    Inherits OBBusinessBase(Of ManagerSubstitute)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ManagerSubstituteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerSubstituteID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ManagerSubstituteID() As Integer
      Get
        Return GetProperty(ManagerSubstituteIDProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
  Public Property ManagerHumanResourceID() As Integer
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ManagerHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SubstituteManagerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SubstituteManagerID, "Substitute Manager", Nothing)
    ''' <summary>
    ''' Gets and sets the Substitute Manager value
    ''' </summary>
    <Display(Name:="Substitute Manager", Description:=""),
    Required(ErrorMessage:="Substitute Manager required")>
    Public Property SubstituteManagerID() As Integer?
      Get
        Return GetProperty(SubstituteManagerIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SubstituteManagerIDProperty, Value)
      End Set
    End Property

    Public Shared SubstituteManagerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubstituteManager, "Substitute Manager", "")
    ''' <summary>
    ''' Gets and sets the Substitute Manager value
    ''' </summary> 
    <Display(Name:="Substitute Manager", Description:="")>
    Public Property SubstituteManager() As String
      Get
        Return GetProperty(SubstituteManagerProperty)
      End Get
      Set(value As String)
        SetProperty(SubstituteManagerProperty, value)
      End Set
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PrimaryInd, False)
    ''' <summary>
    ''' Gets and sets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:=""),
    Required(ErrorMessage:="Primary required")>
  Public Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PrimaryIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "


    Public Function GetParent() As Manager

      Return CType(CType(Me.Parent, ManagerSubstituteList).Parent, Manager)

    End Function

    Public Function GetParentHumanResource() As HumanResource

      Return CType(CType(Me.Parent, ManagerSubstituteList).Parent, HumanResource)

    End Function
     
    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ManagerSubstituteIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Manager Substitute")
        Else
          Return String.Format("Blank {0}", "Manager Substitute")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewManagerSubstitute() method.

    End Sub

    Public Shared Function NewManagerSubstitute() As ManagerSubstitute

      Return DataPortal.CreateChild(Of ManagerSubstitute)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetManagerSubstitute(dr As SafeDataReader) As ManagerSubstitute

      Dim m As New ManagerSubstitute()
      m.Fetch(dr)
      Return m

    End Function
     
    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ManagerSubstituteIDProperty, .GetInt32(0))
          LoadProperty(ManagerHumanResourceIDProperty, .GetInt32(1))
          LoadProperty(SubstituteManagerIDProperty, .GetInt32(2))
          LoadProperty(PrimaryIndProperty, .GetBoolean(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(SubstituteManagerProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insManagerSubstitute"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updManagerSubstitute"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramManagerSubstituteID As SqlParameter = .Parameters.Add("@ManagerSubstituteID", SqlDbType.Int)
          paramManagerSubstituteID.Value = GetProperty(ManagerSubstituteIDProperty)
          If Me.IsNew Then
            paramManagerSubstituteID.Direction = ParameterDirection.Output
          End If
          If GetParentHumanResource() IsNot Nothing Then
            .Parameters.AddWithValue("@ManagerHumanResourceID", GetParentHumanResource.HumanResourceID)
          Else
            .Parameters.AddWithValue("@ManagerHumanResourceID", GetParent.HumanResourceID)
          End If

          .Parameters.AddWithValue("@SubstituteManagerID", GetProperty(SubstituteManagerIDProperty))
          .Parameters.AddWithValue("@PrimaryInd", GetProperty(PrimaryIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ManagerSubstituteIDProperty, paramManagerSubstituteID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delManagerSubstitute"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ManagerSubstituteID", GetProperty(ManagerSubstituteIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace