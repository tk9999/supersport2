﻿' Generated 01 Oct 2014 16:42 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR

  <Serializable()> _
  Public Class ManagerList
    Inherits OBBusinessListBase(Of ManagerList, Manager)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As Manager

      For Each child As Manager In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Managers"

    End Function


    Public Sub UpdateNoOfHR(HumanResourceID As Integer, Count As Integer)
      Dim Man = GetItem(HumanResourceID)
      Man.NoOfManagedPeople = Count
    End Sub


#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)


      Public HumanResourceID As Integer = OBLib.Security.Settings.CurrentUser.HumanResourceID


      Public Sub New(HumanResourceID As Integer)

        Me.HumanResourceID = HumanResourceID

      End Sub
 
      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewManagerList() As ManagerList

      Return New ManagerList()

    End Function

    Public Shared Sub BeginGetManagerList(CallBack As EventHandler(Of DataPortalResult(Of ManagerList)))

      Dim dp As New DataPortal(Of ManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetManagerList() As ManagerList

      Return DataPortal.Fetch(Of ManagerList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Manager.GetManager(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Manager = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ManagerSubstituteList.RaiseListChangedEvents = False
          parent.ManagerSubstituteList.Add(ManagerSubstitute.GetManagerSubstitute(sdr))
          parent.ManagerSubstituteList.RaiseListChangedEvents = True
        End While
      End If

      Me.AllowNew = False
      Me.AllowRemove = False

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getManagerList"
            cm.Parameters.AddWithValue("@HumanResourceID", crit.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try

        Me.AllowNew = False
        Me.AllowRemove = False

      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Manager In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Manager In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region
     

  End Class

End Namespace