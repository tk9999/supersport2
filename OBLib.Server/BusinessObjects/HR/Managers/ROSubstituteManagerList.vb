﻿' Generated 14 Oct 2014 15:15 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROSubstituteManagerList
    Inherits OBReadOnlyListBase(Of ROSubstituteManagerList, ROSubstituteManager)

#Region " Business Methods "

    Public Function GetItem(ManagerHumanResourceID As Integer) As ROSubstituteManager

      For Each child As ROSubstituteManager In Me
        If child.ManagerHumanResourceID = ManagerHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function ShowSubstituteMessage() As Boolean
       
        For Each child As ROSubstituteManager In Me
        If child.ManagerHumanResourceID = OBLib.Security.Settings.CurrentUser.HumanResourceID Then
          Return child.UseSubstituteInd
        End If
        Next 
      Return False

    End Function 

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSubstituteManagerList() As ROSubstituteManagerList

      Return New ROSubstituteManagerList()

    End Function

    Public Shared Sub BeginGetROSubstituteManagerList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSubstituteManagerList)))

      Dim dp As New DataPortal(Of ROSubstituteManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSubstituteManagerList(CallBack As EventHandler(Of DataPortalResult(Of ROSubstituteManagerList)))

      Dim dp As New DataPortal(Of ROSubstituteManagerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSubstituteManagerList() As ROSubstituteManagerList

      Return DataPortal.Fetch(Of ROSubstituteManagerList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSubstituteManager.GetROSubstituteManager(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSubstituteManagerList"
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", OBLib.Security.Settings.CurrentUser.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace