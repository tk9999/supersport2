﻿' Generated 01 Oct 2014 16:42 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.NSWTimesheets
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR

  <Serializable()> _
  Public Class Manager
    Inherits OBBusinessBase(Of Manager)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key,
    Required(ErrorMessage:="Manager is required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CurrentManagerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrentManager, "Current Manager", "")
    ''' <summary>
    ''' Gets the Current Manager value
    ''' </summary>
    <Display(Name:="Current Manager", Description:="")>
    Public ReadOnly Property CurrentManager() As String
      Get
        Return GetProperty(CurrentManagerProperty)
      End Get
    End Property

    Public Shared SubstituteManagerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SubstituteManagerID, "Substitute Manager", Nothing)
    ''' <summary>
    ''' Gets and sets the Substitute Manager value
    ''' </summary>', Singular.DataAnnotations.DropDownWeb("ViewModel.AllManagerList()", ValueMember:="HumanResourceID", DisplayMember:="CurrentManager", Source:=DropDownWeb.SourceType.ViewModel)>
    <Display(Name:="Substitute Manager", Description:="")>
    Public Property SubstituteManagerID() As Integer?
      Get
        Return GetProperty(SubstituteManagerIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SubstituteManagerIDProperty, Value)
      End Set
    End Property

    Public Shared SubstituteManagerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubstituteManager, "Substitute Manager", "")
    ''' <summary>
    ''' Gets and sets the Substitute Manager value
    ''' </summary>', Singular.DataAnnotations.DropDownWeb("ViewModel.AllManagerList()", ValueMember:="HumanResourceID", DisplayMember:="CurrentManager", Source:=DropDownWeb.SourceType.ViewModel)>
    <Display(Name:="Substitute Manager", Description:="")>
    Public Property SubstituteManager() As String
      Get
        Return GetProperty(SubstituteManagerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SubstituteManagerProperty, Value)
      End Set
    End Property

    Public Shared UseSubstituteIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UseSubstituteInd, "Use Substitute", False)
    ''' <summary>
    ''' Gets and sets the Use Substitute value
    ''' </summary>
    <Display(Name:="Use Substitute", Description:="")>
    Public Property UseSubstituteInd() As Boolean
      Get
        Return GetProperty(UseSubstituteIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UseSubstituteIndProperty, Value)
      End Set
    End Property

    Public Shared UseSubstituteEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.UseSubstituteEndDate, "Use Substitute End Date")
    ''' <summary>
    ''' Gets and sets the Use Substitute End Date value
    ''' </summary>
    <Display(Name:="Use Substitute End Date", Description:=""), Singular.DataAnnotations.DateField(MinDateProperty:="ViewModel.FutureDate()")>
    Public Property UseSubstituteEndDate As DateTime?
      Get
        Return GetProperty(UseSubstituteEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(UseSubstituteEndDateProperty, Value)
      End Set
    End Property

    Public Shared NoOfManagedPeopleProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfManagedPeople, "No Of Managed People", 0)
    ''' <summary>
    ''' Gets the No Of Managed People value
    ''' </summary>
    <Display(Name:="No of People", Description:="")>
    Public Property NoOfManagedPeople() As Integer
      Get
        Return GetProperty(NoOfManagedPeopleProperty)
      End Get
      Set(value As Integer)
        SetProperty(NoOfManagedPeopleProperty, value)
      End Set
    End Property

    Public Shared NewManagerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NewManagerID, "New Manager", Nothing)
    ''' <summary>
    ''' Gets and sets the New Manager value
    ''' </summary> 
    <Display(Name:="New Manager", Description:="")>
    Public Property NewManagerID() As Integer?
      Get
        Return GetProperty(NewManagerIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewManagerIDProperty, Value)
      End Set
    End Property

    Public Shared NewManagerNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewManagerName, "New Manager Name", "")
    ''' <summary>
    ''' Gets and sets the New Manager value
    ''' </summary> 
    <Display(Name:="New Manager", Description:="")>
    Public Property NewManagerName() As String
      Get
        Return GetProperty(NewManagerNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NewManagerNameProperty, Value)
      End Set
    End Property


    <DefaultValue(False)>
    Public Property Expanded As Boolean = False


#End Region

#Region " Child Lists "

    Public Shared ManagerSubstituteListProperty As PropertyInfo(Of ManagerSubstituteList) = RegisterProperty(Of ManagerSubstituteList)(Function(c) c.ManagerSubstituteList, "Manager Substitute List")

    Public ReadOnly Property ManagerSubstituteList() As ManagerSubstituteList
      Get
        If GetProperty(ManagerSubstituteListProperty) Is Nothing Then
          LoadProperty(ManagerSubstituteListProperty, HR.ManagerSubstituteList.NewManagerSubstituteList())
        End If
        Return GetProperty(ManagerSubstituteListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String
       
        Return Me.CurrentManager
 
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
 
       


      AddWebRule(SubstituteManagerProperty, Function(c) Singular.Misc.CompareSafe(c.SubstituteManagerID, c.HumanResourceID), Function(c) "Please Select a different Substitute Manager")
      AddWebRule(UseSubstituteIndProperty, Function(c) c.UseSubstituteInd AndAlso c.SubstituteManagerID Is Nothing, Function(c) "Substitute Manager is required")
      '.JavascriptRuleFunctionName = "CheckSubstituteManagerID"
      '.ServerRuleFunction = Function(mg)
      '                        If Singular.Misc.CompareSafe(mg.SubstituteManagerID, mg.HumanResourceID) Then
      '                          Return "Please Select a different Substitute Manager"

      '                        End If

      '                        If Singular.Misc.IsNullNothing(mg.SubstituteManagerID, True) AndAlso mg.UseSubstituteInd Then
      '                          Return "Substitute Manager is required"
      '                        End If

      '                        Return ""
      '                      End Function

      '.AffectedProperties.Add(UseSubstituteIndProperty)
      '.AddTriggerProperty(UseSubstituteIndProperty)
      'End With
    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewManager() method.

    End Sub

    Public Shared Function NewManager() As Manager

      Return DataPortal.CreateChild(Of Manager)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetManager(dr As SafeDataReader) As Manager

      Dim m As New Manager()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(CurrentManagerProperty, .GetString(1))
          LoadProperty(SubstituteManagerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(UseSubstituteIndProperty, .GetBoolean(3))
          LoadProperty(UseSubstituteEndDateProperty, .GetValue(4))
          LoadProperty(NoOfManagedPeopleProperty, .GetInt32(5))
          LoadProperty(SubstituteManagerProperty, .GetString(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insManager"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updManager"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
          paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
          If Me.IsNew Then
            paramHumanResourceID.Direction = ParameterDirection.Output
          End If 
          .Parameters.AddWithValue("@SubstituteManagerID", GetProperty(SubstituteManagerIDProperty))
          .Parameters.AddWithValue("@UseSubstituteInd", GetProperty(UseSubstituteIndProperty))
          .Parameters.AddWithValue("@UseSubstituteEndDate", (New SmartDate(GetProperty(UseSubstituteEndDateProperty))).DBValue) 
          .Parameters.AddWithValue("@NewManagerID", GetProperty(NewManagerIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
          End If
          ' update child objects
          ManagerSubstituteList.Update()
          MarkOld()
        End With
      Else

        ManagerSubstituteList.Update()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delManager"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace