﻿' Generated 14 Oct 2014 15:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROSubstituteManager
    Inherits OBReadOnlyBase(Of ROSubstituteManager)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ManagerHumanResourceID() As Integer
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared IsNSWManagerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsNSWManagerInd, "IsNSW Manager", False)
    ''' <summary>
    ''' Gets the IsNSW Manager value
    ''' </summary>
    <Display(Name:="Is NSW Manager", Description:="")>
    Public ReadOnly Property IsNSWManagerInd() As Boolean
      Get
        Return GetProperty(IsNSWManagerIndProperty)
      End Get
    End Property

    Public Shared UseSubstituteIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UseSubstituteInd, "UseSubstitute", False)
    ''' <summary>
    ''' Gets the IsNSW Manager value
    ''' </summary>
    <Display(Name:="Use Substitute", Description:="")>
    Public ReadOnly Property UseSubstituteInd() As Boolean
      Get
        Return GetProperty(UseSubstituteIndProperty)
      End Get
    End Property

    Public Shared SubstituteManagerHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubstituteManagerHumanResourceID, "Substitute Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Substitute Manager Human Resource value
    ''' </summary>
    <Display(Name:="Substitute Manager Human Resource", Description:="")>
  Public ReadOnly Property SubstituteManagerHumanResourceID() As Integer
      Get
        Return GetProperty(SubstituteManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ManagerNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerName, "Manager Name", "")
    ''' <summary>
    ''' Gets the Manager Name value
    ''' </summary>
    <Display(Name:="Manager Name", Description:="")>
  Public ReadOnly Property ManagerName() As String
      Get
        Return GetProperty(ManagerNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ManagerHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ManagerName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSubstituteManager(dr As SafeDataReader) As ROSubstituteManager

      Dim r As New ROSubstituteManager()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ManagerHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(IsNSWManagerIndProperty, .GetBoolean(1))
        LoadProperty(SubstituteManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ManagerNameProperty, .GetString(3))
        LoadProperty(UseSubstituteIndProperty, .GetBoolean(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace