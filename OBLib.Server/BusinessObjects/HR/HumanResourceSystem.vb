﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSystem
    Inherits OBBusinessBase(Of HumanResourceSystem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceSystemBO.HumanResourceSystemBOToString(self)")

    Public Shared HumanResourceSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSystemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceSystemID() As Integer
      Get
        Return GetProperty(HumanResourceSystemIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", OBLib.Security.Settings.CurrentUser.SystemID)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system for which the human resource belongs to"),
    Required(ErrorMessage:="System required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets and sets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="True if the system is the human resource's primary system"),
    Required(ErrorMessage:="Primary required")>
    Public Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PrimaryIndProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="The production area which the human resource belongs to"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As HumanResource

      Return CType(CType(Me.Parent, HumanResourceSystemList).Parent, HumanResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource System")
        Else
          Return String.Format("Blank {0}", "Human Resource System")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(SystemIDProperty)
        ' .JavascriptRuleFunctionName = "HumanResourceSystemBO.SystemIsValid"
        .ServerRuleFunction = AddressOf SystemIsValid
      End With

    End Sub

    Public Shared Function SystemIsValid(HRS As HumanResourceSystem) As String
      Dim ErrorString = ""
      '      If HRS.Parent IsNot Nothing Then
      '        Dim hrsList = HRS.GetParent().HumanResourceSystemList
      '        Dim len = hrsList.Count
      '        For i = 0 To len - 1
      '          If HRS IsNot hrsList.ElementAt(i) Then
      '            If HRS.SystemID = hrsList.ElementAt(i).SystemID AndAlso HRS.ProductionAreaID = hrsList.ElementAt(i).ProductionAreaID Then
      '              Dim System As String = CommonData.Lists.ROSystemList.GetItem(HRS.SystemID).System
      '              Dim ProductionArea As String = CommonData.Lists.ROProductionAreaList(HRS.ProductionAreaID).ProductionArea
      '              ErrorString = "Unique Sub-Dept and Area combination is required - " + System & " (" & ProductionArea & ") is already added"
      '              GoTo End_Loop
      '            End If
      '          End If
      '        Next
      '      End If
      'End_Loop:
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceSystem() method.

    End Sub

    Public Shared Function NewHumanResourceSystem() As HumanResourceSystem

      Return DataPortal.CreateChild(Of HumanResourceSystem)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceSystem(dr As SafeDataReader) As HumanResourceSystem

      Dim h As New HumanResourceSystem()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceSystemIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PrimaryIndProperty, .GetBoolean(3))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceSystem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceSystem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSystemID As SqlParameter = .Parameters.Add("@HumanResourceSystemID", SqlDbType.Int)
          paramHumanResourceSystemID.Value = GetProperty(HumanResourceSystemIDProperty)
          If Me.IsNew Then
            paramHumanResourceSystemID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", Me.GetParent().HumanResourceID)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@PrimaryInd", GetProperty(PrimaryIndProperty))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceSystemIDProperty, paramHumanResourceSystemID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceSystem"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceSystemID", GetProperty(HumanResourceSystemIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace