﻿' Generated 21 Sep 2015 09:03 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSwap
    Inherits OBBusinessBase(Of HumanResourceSwap)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSwapIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSwapID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceSwapID() As Integer
      Get
        Return GetProperty(HumanResourceSwapIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource requesting the swap"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SwapDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SwapDate, "My Date")
    ''' <summary>
    ''' Gets and sets the Swap Date value
    ''' </summary>
    <Display(Name:="My Shift Date"),
    Required(ErrorMessage:="My Shift Date is required"),
    DateField(AlwaysShow:=True)>
    Public Property SwapDate As DateTime?
      Get
        Return GetProperty(SwapDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SwapDateProperty, Value)
      End Set
    End Property

    Public Shared SwapWithHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SwapWithHumanResourceID, "Swap With Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Swap With Human Resource value
    ''' </summary>
    <Display(Name:="Swap With Human Resource", Description:="The human resource to swap with"),
    Required(ErrorMessage:="Swap With Human Resource required")>
    Public Property SwapWithHumanResourceID() As Integer?
      Get
        Return GetProperty(SwapWithHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SwapWithHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SwapWithDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SwapWithDate, "Their Shift Date")
    ''' <summary>
    ''' Gets and sets the Swap Date value
    ''' </summary>
    <Display(Name:="Their Shift Date"),
    Required(ErrorMessage:="Their Shift Date is required"),
    DateField(AlwaysShow:=True)>
    Public Property SwapWithDate As DateTime?
      Get
        Return GetProperty(SwapWithDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SwapWithDateProperty, Value)
      End Set
    End Property

    Public Shared ApprovedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Approved, "Approved", False)
    ''' <summary>
    ''' Gets and sets the Approved value
    ''' </summary>
    <Display(Name:="Approved", Description:="Is the swap approved"),
    Required(ErrorMessage:="Approved required")>
    Public Property Approved() As Boolean
      Get
        Return GetProperty(ApprovedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ApprovedProperty, Value)
      End Set
    End Property

    Public Shared ApprovedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ApprovedBy, "Approved By", Nothing)
    ''' <summary>
    ''' Gets and sets the Approved By value
    ''' </summary>
    <Display(Name:="Approved By", Description:="The user who approved the swap")>
    Public Property ApprovedBy() As Integer?
      Get
        Return GetProperty(ApprovedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ApprovedByProperty, Value)
      End Set
    End Property

    Public Shared ApprovedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ApprovedDateTime, "Approved Date Time")
    ''' <summary>
    ''' Gets and sets the Approved Date Time value
    ''' </summary>
    <Display(Name:="Approved Date Time", Description:="The date and time at which the swap was approved")>
    Public Property ApprovedDateTime As DateTime?
      Get
        Return GetProperty(ApprovedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ApprovedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ApprovedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ApprovedByName, "Approved By", "")
    ''' <summary>
    ''' Gets and sets the Approved By value
    ''' </summary>
    <Display(Name:="Approved By")>
    Public Property ApprovedByName() As String
      Get
        Return GetProperty(ApprovedByNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ApprovedByNameProperty, Value)
      End Set
    End Property

    Public Shared ApprovedDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ApprovedDateString, "Approved Date", "")
    ''' <summary>
    ''' Gets and sets the Approved Date Time value
    ''' </summary>
    <Display(Name:="Approved Date")>
    Public Property ApprovedDateString As String
      Get
        Return GetProperty(ApprovedDateStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ApprovedDateStringProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "My Name", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="My Name"),
    Required(ErrorMessage:="My Name is required")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Contract Type")>
    Public Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContractTypeProperty, Value)
      End Set
    End Property

    Public Shared SwapWithHumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SwapWithHumanResource, "Their Name", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Their Name"),
    Required(ErrorMessage:="Their Name is required")>
    Public Property SwapWithHumanResource() As String
      Get
        Return GetProperty(SwapWithHumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SwapWithHumanResourceProperty, Value)
      End Set
    End Property

    Public Shared SwapWithContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SwapWithContractType, "Contract Type", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Contract Type")>
    Public Property SwapWithContractType() As String
      Get
        Return GetProperty(SwapWithContractTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SwapWithContractTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSwapIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceSwapID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Swap")
        Else
          Return String.Format("Blank {0}", "Human Resource Swap")
        End If
      Else
        Return Me.HumanResourceSwapID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceSwap() method.

    End Sub

    Public Shared Function NewHumanResourceSwap() As HumanResourceSwap

      Return DataPortal.CreateChild(Of HumanResourceSwap)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceSwap(dr As SafeDataReader) As HumanResourceSwap

      Dim h As New HumanResourceSwap()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceSwapIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SwapDateProperty, .GetValue(2))
          LoadProperty(SwapWithHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ApprovedProperty, .GetBoolean(4))
          LoadProperty(ApprovedByProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ApprovedDateTimeProperty, .GetValue(6))
          LoadProperty(ApprovedByNameProperty, .GetString(7))
          LoadProperty(ApprovedDateStringProperty, If(ApprovedDateTime IsNot Nothing, ApprovedDateTime.Value.ToString("ddd dd MMM yy HH:mm"), ""))
          LoadProperty(SwapWithDateProperty, .GetValue(3))
          LoadProperty(HumanResourceProperty, .GetString(4))
          LoadProperty(ContractTypeProperty, .GetString(5))
          LoadProperty(SwapWithContractTypeProperty, .GetString(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceSwap"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceSwap"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSwapID As SqlParameter = .Parameters.Add("@HumanResourceSwapID", SqlDbType.Int)
          paramHumanResourceSwapID.Value = GetProperty(HumanResourceSwapIDProperty)
          If Me.IsNew Then
            paramHumanResourceSwapID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@SwapDate", (New SmartDate(GetProperty(SwapDateProperty))).DBValue)
          .Parameters.AddWithValue("@SwapWithHumanResourceID", GetProperty(SwapWithHumanResourceIDProperty))
          .Parameters.AddWithValue("@SwapWithDate", (New SmartDate(GetProperty(SwapWithDateProperty))).DBValue)
          .Parameters.AddWithValue("@Approved", GetProperty(ApprovedProperty))
          .Parameters.AddWithValue("@ApprovedBy", Singular.Misc.NothingDBNull(GetProperty(ApprovedByProperty)))
          .Parameters.AddWithValue("@ApprovedDateTime", (New SmartDate(GetProperty(ApprovedDateTimeProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceSwapIDProperty, paramHumanResourceSwapID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceSwap"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceSwapID", GetProperty(HumanResourceSwapIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace