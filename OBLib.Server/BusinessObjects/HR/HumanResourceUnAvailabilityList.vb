﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Resources

Namespace HR

  <Serializable()> _
  Public Class HumanResourceUnAvailabilityList
    Inherits OBBusinessListBase(Of HumanResourceUnAvailabilityList, HumanResourceUnAvailability)

#Region " Business Methods "

    Public Function GetItem(HumanResourceUnAvailabilityID As Integer) As HumanResourceUnAvailability

      For Each child As HumanResourceUnAvailability In Me
        If child.HumanResourceOffPeriodID = HumanResourceUnAvailabilityID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetOffPeriodBooking(ResourceBookingID As Integer) As OffPeriodBooking

    '  For Each child As HumanResourceUnAvailability In Me
    '    For Each b As OffPeriodBooking In child.OffPeriodBookingList
    '      If b.ResourceBookingID = ResourceBookingID Then
    '        Return b
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Off Periods"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property HumanresourceOffPeriodID As Integer?
      Public Property HumanresourceOffPeriodIDs As String = ""

      Public Sub New(HumanResourceOffPeriodIDSXML As String)
        Me.HumanresourceOffPeriodIDs = HumanResourceOffPeriodIDSXML
      End Sub

      Public Sub New(HumanResourceOffPeriodID As Integer?)
        Me.HumanresourceOffPeriodID = HumanResourceOffPeriodID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHumanResourceUnAvailabilityList() As HumanResourceUnAvailabilityList

      Return New HumanResourceUnAvailabilityList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHumanResourceUnAvailabilityList() As HumanResourceUnAvailabilityList

      Return DataPortal.Fetch(Of HumanResourceUnAvailabilityList)(New Criteria())

    End Function

    Public Shared Function GetHumanResourceUnAvailabilityList(HumanResourceOffPeriodID As Integer?) As HumanResourceUnAvailabilityList

      Return DataPortal.Fetch(Of HumanResourceUnAvailabilityList)(New Criteria(HumanResourceOffPeriodID))

    End Function

    Public Shared Function GetHumanResourceUnAvailabilityList(HumanResourceOffPeriodIDSXML As String) As HumanResourceUnAvailabilityList

      Return DataPortal.Fetch(Of HumanResourceUnAvailabilityList)(New Criteria(HumanResourceOffPeriodIDSXML))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceUnAvailability.GetHumanResourceUnAvailability(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentOffPeriod As HumanResourceUnAvailability = Nothing

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceUnAvailabilityList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            cm.Parameters.AddWithValue("@UserHumanResourceID", NothingDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanresourceOffPeriodID", NothingDBNull(crit.HumanresourceOffPeriodID))
            cm.Parameters.AddWithValue("@HumanresourceOffPeriodIDs", Strings.MakeEmptyDBNull(crit.HumanresourceOffPeriodIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResourceUnAvailability In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResourceUnAvailability In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End Region

#End Region

  End Class

End Namespace