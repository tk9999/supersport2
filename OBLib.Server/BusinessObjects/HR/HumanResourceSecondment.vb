﻿' Generated 24 Mar 2015 08:58 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBLib.Resources
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceSecondment
    Inherits OBBusinessBase(Of HumanResourceSecondment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSecondmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceSecondmentID() As Integer
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The link to the Human Resource"),
    Required>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="The country to which the human resource was seconded to"),
    Required(ErrorMessage:="Country is required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCountryList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDProperty, Value)
      End Set
    End Property

    'Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")
    ' ''' <summary>
    ' ''' Gets the Authorised By Name value
    ' ''' </summary>
    '<Display(Name:="Country")>
    'Public Property Country() As String
    '  Get
    '    Return GetProperty(CountryProperty)
    '  End Get
    '  Set(value As String)
    '    SetProperty(CountryProperty, value)
    '  End Set
    'End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor", Nothing)
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="The debtor to which the employee was seconded to"),
    Singular.DataAnnotations.DropDownWeb(GetType(RODebtorList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property

    'Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor", "")
    ' ''' <summary>
    ' ''' Gets the Authorised By Name value
    ' ''' </summary>
    '<Display(Name:="Debtor")>
    'Public Property Debtor() As String
    '  Get
    '    Return GetProperty(DebtorProperty)
    '  End Get
    '  Set(value As String)
    '    SetProperty(DebtorProperty, value)
    '  End Set
    'End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostCentreID, "Cost Centre", Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="The cost centre to which the human resource was seconded to")>
    Public Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostCentreIDProperty, Value)
      End Set
    End Property
    ',
    'Singular.DataAnnotations.DropDownWeb(GetType(ROCostCentreList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)

    Public Shared FromDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FromDate, "From Date")
    ''' <summary>
    ''' Gets and sets the From Date value
    ''' </summary>
    <Display(Name:="From Date", Description:="The date and time at which the secondment begins"),
    Required(ErrorMessage:="From Date required"),
    DateField(MaxDateProperty:="ToDate")>
    Public Property FromDate As DateTime?
      Get
        Return GetProperty(FromDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FromDateProperty, Value)
      End Set
    End Property

    Public Shared ToDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ToDate, "To Date")
    ''' <summary>
    ''' Gets and sets the To Date value
    ''' </summary>
    <Display(Name:="To Date", Description:="The date and time at which the secondment ends"),
    Required(ErrorMessage:="To Date required"),
    DateField(MinDateProperty:="FromDate")>
    Public Property ToDate As DateTime?
      Get
        Return GetProperty(ToDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ToDateProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="The user who authorised the secondment")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="Date the secondment was authorised"), Singular.DataAnnotations.DateField(FormatString:="dd-MMM-yy HH:mm")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ConfirmedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ConfirmedByUserID, Nothing)
    ''' <summary>
    ''' Gets and sets the Confirmed By User value
    ''' </summary>
    <Display(Name:="Confirmed By User", Description:="The user who confirmed the secondment")>
    Public Property ConfirmedByUserID() As Integer?
      Get
        Return GetProperty(ConfirmedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ConfirmedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared ConfirmedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ConfirmedDateTime, "Confirmed Date Time")
    ''' <summary>
    ''' Gets and sets the Confirmed Date Time value
    ''' </summary>
    <Display(Name:="Confirmed Date Time", Description:="Date the secondment was confirmed"), Singular.DataAnnotations.DateField(FormatString:="dd-MMM-yy HH:mm")>
    Public Property ConfirmedDateTime As DateTime?
      Get
        Return GetProperty(ConfirmedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ConfirmedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Second Name value
    ''' </summary>
    <Display(Name:="Description", Description:="The description of the secondment"),
    StringLength(50, ErrorMessage:="Description cannot be more than 500 characters"),
    Required(AllowEmptyStrings:=False)>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AuthorisedByName, "Authorised By", "")
    ''' <summary>
    ''' Gets the Authorised By Name value
    ''' </summary>
    <Display(Name:="Authorised By", Description:="Authorised By Name")>
    Public Property AuthorisedByName() As String
      Get
        Return GetProperty(AuthorisedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(AuthorisedByNameProperty, value)
      End Set
    End Property

    Public Shared ConfirmedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ConfirmedByName, "Confirmed By", "")
    ''' <summary>
    ''' Gets the Authorised By Name value
    ''' </summary>
    <Display(Name:="Confirmed By", Description:="Confirmed By Name")>
    Public Property ConfirmedByName() As String
      Get
        Return GetProperty(ConfirmedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(ConfirmedByNameProperty, value)
      End Set
    End Property

    Public Shared IsAuthorisedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsAuthorised, False) _
                                                                     .AddSetExpression("HumanResourceSecondmentBO.IsAuthorisedSet(self)")
    ''' <summary>
    ''' Gets and sets the Second Name value
    ''' </summary>
    <Display(Name:="Auth. Status", Description:="Authorised to be seconded?")>
    Public Property IsAuthorised() As Boolean
      Get
        Return GetProperty(IsAuthorisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsAuthorisedProperty, Value)
      End Set
    End Property

    Public Shared IsConfirmedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsConfirmed, False) _
                                                                     .AddSetExpression("HumanResourceSecondmentBO.IsConfirmedSet(self)")
    ''' <summary>
    ''' Gets and sets the Second Name value
    ''' </summary>
    <Display(Name:="Conf. Status", Description:="Confirmed to be seconded?")>
    Public Property IsConfirmed() As Boolean
      Get
        Return GetProperty(IsConfirmedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsConfirmedProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="ResourceID"),
    Required>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SecondmentBookingListProperty As PropertyInfo(Of SecondmentBookingList) = RegisterProperty(Of SecondmentBookingList)(Function(c) c.SecondmentBookingList, "Secondment Booking List")
    Public ReadOnly Property SecondmentBookingList() As SecondmentBookingList
      Get
        If GetProperty(SecondmentBookingListProperty) Is Nothing Then
          LoadProperty(SecondmentBookingListProperty, New SecondmentBookingList)
        End If
        Return GetProperty(SecondmentBookingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSecondmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDate.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Secondment")
        Else
          Return String.Format("Blank {0}", "Human Resource Secondment")
        End If
      Else
        Return Me.CreatedDate.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore As String()
      Get
        Return New String() {"CrewScheduleDetails", "ResourceBookings"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CountryIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceSecondmentBO.CountryIDValid"
        .ServerRuleFunction = AddressOf CountryIDValid
        '.AffectedProperties.Add(CountryProperty)
        '.AddTriggerProperty(CountryProperty)
      End With

      With AddWebRule(DebtorIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceSecondmentBO.DebtorIDValid"
        .ServerRuleFunction = AddressOf DebtorIDValid
        '.AffectedProperties.Add(DebtorProperty)
        '.AddTriggerProperty(DebtorProperty)
      End With

    End Sub

    Public Shared Function CountryIDValid(Secondment As HumanResourceSecondment) As String

      If Secondment.CountryID Is Nothing Then
        Return "Country is required"
      End If
      Return ""

    End Function

    Public Shared Function DebtorIDValid(Secondment As HumanResourceSecondment) As String

      If Secondment.DebtorID Is Nothing Then
        Return "Debtor is required"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceSecondment() method.

    End Sub

    Public Shared Function NewHumanResourceSecondment() As HumanResourceSecondment

      Return DataPortal.CreateChild(Of HumanResourceSecondment)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceSecondment(dr As SafeDataReader) As HumanResourceSecondment

      Dim h As New HumanResourceSecondment()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceSecondmentIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(FromDateProperty, .GetValue(5))
          LoadProperty(ToDateProperty, .GetValue(6))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(8))
          LoadProperty(ConfirmedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(ConfirmedDateTimeProperty, .GetValue(10))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateProperty, .GetSmartDate(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
          If GetProperty(AuthorisedByUserIDProperty) IsNot Nothing Then
            LoadProperty(IsAuthorisedProperty, True)
          End If
          If GetProperty(ConfirmedByUserIDProperty) IsNot Nothing Then
            LoadProperty(IsConfirmedProperty, True)
          End If
          LoadProperty(DescriptionProperty, .GetString(15))
          LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(16)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResourceSecondment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResourceSecondment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsAuthorised AndAlso Me.SecondmentBookingList.Count = 0 Then
        Me.GenerateBooking()
      ElseIf Not Me.IsAuthorised AndAlso Me.SecondmentBookingList.Count > 0 Then
        Me.RemoveBooking()
      End If

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSecondmentID As SqlParameter = .Parameters.Add("@HumanResourceSecondmentID", SqlDbType.Int)
          paramHumanResourceSecondmentID.Value = GetProperty(HumanResourceSecondmentIDProperty)
          If Me.IsNew Then
            paramHumanResourceSecondmentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@CountryID", GetProperty(CountryIDProperty))
          .Parameters.AddWithValue("@DebtorID", Singular.Misc.NothingDBNull(GetProperty(DebtorIDProperty)))
          .Parameters.AddWithValue("@CostCentreID", Singular.Misc.NothingDBNull(GetProperty(CostCentreIDProperty)))
          .Parameters.AddWithValue("@FromDate", (New SmartDate(GetProperty(FromDateProperty))).DBValue)
          .Parameters.AddWithValue("@ToDate", (New SmartDate(GetProperty(ToDateProperty))).DBValue)
          .Parameters.AddWithValue("@AuthorisedByUserID", Singular.Misc.NothingDBNull(GetProperty(AuthorisedByUserIDProperty)))
          .Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ConfirmedByUserID", Singular.Misc.NothingDBNull(GetProperty(ConfirmedByUserIDProperty)))
          .Parameters.AddWithValue("@ConfirmedDateTime", (New SmartDate(GetProperty(ConfirmedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceSecondmentIDProperty, paramHumanResourceSecondmentID.Value)
          End If
          ' update child objects
          SecondmentBookingList.Update()
          MarkOld()
        End With
      Else
        SecondmentBookingList.Update()
      End If

    End Sub

    Friend Sub DeleteSelf()
      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      'If (Singular.Security.HasAccess("Human Resources", "Can Remove Secondment")) AndAlso Not IsAuthorised Then
      For Each rb As SecondmentBooking In Me.SecondmentBookingList
        rb.DeleteChildren()
        rb.DeleteSelf()
      Next
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHumanResourceSecondment"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceSecondmentID", GetProperty(HumanResourceSecondmentIDProperty))
        DoDeleteChild(cm)
      End Using
      'End If
    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub GenerateBooking()
      Dim rb As SecondmentBooking = New SecondmentBooking
      Me.SecondmentBookingList.Add(rb)
      rb.ResourceBookingTypeID = OBLib.CommonData.Enums.ResourceBookingType.HRSecondment
      rb.ResourceID = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(Me.HumanResourceID).ResourceID
      rb.HumanResourceID = Me.HumanResourceID
      rb.StartDateTimeBuffer = Nothing
      rb.StartDateTime = Me.FromDate
      rb.EndDateTime = Me.ToDate
      rb.EndDateTimeBuffer = Nothing
      If Me.CountryID IsNot Nothing Then
        Dim ROCountry As ROCountry = OBLib.CommonData.Lists.ROCountryList.GetItem(Me.CountryID)
        If ROCountry IsNot Nothing Then
          rb.ResourceBookingDescription = "Seconded: " & Me.Description & " (" & ROCountry.Country & ")"
        Else
          rb.ResourceBookingDescription = ""
        End If
      Else
        rb.ResourceBookingDescription = ""
      End If
      rb.HumanResourceSecondmentID = Me.HumanResourceSecondmentID
      If Me.IsAuthorised Then
        rb.StatusCssClass = "secondment-authorised"
      ElseIf Not Me.IsAuthorised AndAlso Me.IsConfirmed Then
        rb.StatusCssClass = "secondment-confirmed"
      ElseIf Not Me.IsAuthorised AndAlso Not Me.IsConfirmed Then
        rb.StatusCssClass = "secondment-pending"
      End If
      Dim sd As Date = Me.FromDate
      While sd <= Me.ToDate
        Dim csd As CrewScheduleDetail = rb.CrewScheduleDetailList.AddNew
        csd.HumanResourceID = Me.HumanResourceID
        csd.HumanResourceSecondmentID = rb.HumanResourceSecondmentID
        csd.HumanResourceScheduleTypeID = OBLib.CommonData.Enums.HumanResourceScheduleType.Secondment
        csd.TimesheetDate = sd
        csd.StartDateTime = New Date(sd.Date.Year, sd.Date.Month, sd.Date.Day, 0, 0, 0)
        csd.EndDateTime = New Date(sd.Date.Year, sd.Date.Month, sd.Date.Day, 23, 59, 59)
        csd.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
        csd.ProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
        sd = sd.AddDays(1)
      End While
      Me.CheckRules()
    End Sub

    Sub RemoveBooking()
      Me.SecondmentBookingList.Clear()
    End Sub

  End Class

End Namespace