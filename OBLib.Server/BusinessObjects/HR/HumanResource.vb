﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports Singular.Emails
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports System.IO

Namespace HR

  <Serializable()>
  Public Class HumanResource
    Inherits OBBusinessBase(Of HumanResource)

#Region " Properties and Methods "
    Private mCanAuthoriseLeave As Boolean

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceBO.HumanResourceBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.HumanResourceID, 0)
    '_
    '.AddSetExpression("HumanResourceBO.GetHRHDuplicates(self)")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Firstname, "") _
                                                                 .AddSetExpression("HumanResourceBO.FirstNameSet(self)")
    ''' <summary>
    ''' Gets and sets the Firstname value
    ''' </summary>
    <Display(Name:="First Name", Description:="The human resource's first name"),
    Required(ErrorMessage:="First Name required"),
    StringLength(50, ErrorMessage:="First Name cannot be more than 50 characters")>
    Public Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FirstnameProperty, Value)
      End Set
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name", "")
    ''' <summary>
    ''' Gets and sets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="The human resource's second name"),
    StringLength(50, ErrorMessage:="Second Name cannot be more than 50 characters")>
    Public Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SecondNameProperty, Value)
      End Set
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
    ''' <summary>
    ''' Gets and sets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="The human resource's preferred name"),
    StringLength(50, ErrorMessage:="Preferred Name cannot be more than 50 characters")>
    Public Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PreferredNameProperty, Value)
      End Set
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Surname, "") _
                                                               .AddSetExpression("HumanResourceBO.SurnameSet(self)")
    ''' <summary>
    ''' Gets and sets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="The human resource's surname"),
    Required(ErrorMessage:="Surname required"),
    StringLength(50, ErrorMessage:="Surname cannot be more than 50 characters")>
    Public Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SurnameProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing) _
                                                                        .AddSetExpression("HumanResourceBO.ContractTypeIDSet(self)")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="True indicates that this person is a contractor and not an employee"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROContractTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Contract Type")>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email", Description:="The human resource's email address"),
    StringLength(100, ErrorMessage:="Email Address cannot be more than 100 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address", "")
    ''' <summary>
    ''' Gets and sets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alt. Email", Description:="An alternative email address for the human resource"),
    StringLength(100, ErrorMessage:="Alternative Email Address cannot be more than 100 characters")>
    Public Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AlternativeEmailAddressProperty, Value)
      End Set
    End Property

    Public Shared PhoneEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhoneEmailAddress, "Phone Email Address", "")
    ''' <summary>
    ''' Gets and sets the Phone Email Address value
    ''' </summary>
    <Display(Name:="Phone Email Address", Description:="This is the email address that will be used to send scheduling information to crew. The crew must always be able to access this address from their phone"),
    StringLength(100, ErrorMessage:="Phone Email Address cannot be more than 100 characters")>
    Public Property PhoneEmailAddress() As String
      Get
        Return GetProperty(PhoneEmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PhoneEmailAddressProperty, Value)
      End Set
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number", "")
    ''' <summary>
    ''' Gets and sets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Mobile Number", Description:="The human resource's cell phone number"),
    StringLength(20, ErrorMessage:="Cell Phone Number cannot be more than 20 characters"),
    SetExpression("HumanResourceBO.CellPhoneNumberSet(self)")>
    Public Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellPhoneNumberProperty, Value)
      End Set
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number", "")
    ''' <summary>
    ''' Gets and sets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="An alternative contact number for the human resource"),
    StringLength(20, ErrorMessage:="Alternative Contact Number cannot be more than 20 characters"),
    SetExpression("HumanResourceBO.AlternativeContactNumberSet(self)")>
    Public Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AlternativeContactNumberProperty, Value)
      End Set
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.IDNo, "") _
                                                            .AddSetExpression("HumanResourceBO.IDNoSet(self)")
    ''' <summary>
    ''' Gets and sets the ID No value
    ''' </summary>
    <Display(Name:="ID Num", Description:="The human resource's unique ID number"),
    Required(ErrorMessage:="ID Num. required"),
    StringLength(13, ErrorMessage:="ID No cannot be more than 13 characters")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IDNoProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CityID, Nothing)
    ''' <summary>
    ''' Gets and sets the City value
    ''' ,
    '''Singular.DataAnnotations.DropDownWeb(GetType(ROCityList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="City")
    ''' </summary>
    <Display(Name:="City", Description:="The city that this HR is based. Used to calculate travel for events"),
    DropDownWeb(GetType(ROCityPagedList), DropDownColumns:={"Country", "City"}, ValueMember:="CityID", DisplayMember:="City")>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared RaceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RaceID, "Race", Nothing)
    ''' <summary>
    ''' Gets and sets the Race value
    ''' </summary>
    <Display(Name:="Race"),
    Required(ErrorMessage:="Race required"),
    Singular.DataAnnotations.DropDownWeb(GetType(RORaceList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Select Race...")>
    Public Property RaceID() As Integer?
      Get
        Return GetProperty(RaceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RaceIDProperty, Value)
      End Set
    End Property

    Public Shared GenderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GenderID, "Gender", Nothing)
    ''' <summary>
    ''' Gets and sets the Gender value
    ''' </summary>
    <Display(Name:="Gender"),
    Required(ErrorMessage:="Gender required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROGenderList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Select Gender...")>
    Public Property GenderID() As Integer?
      Get
        Return GetProperty(GenderIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(GenderIDProperty, Value)
      End Set
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Is Active", True)
    ''' <summary>
    ''' Gets and sets the Active value
    ''' </summary>
    <Display(Name:="Is Active", Description:="Tick indicates that this human resource is currently active and can be used for productions"),
    Required(ErrorMessage:="Is Active is required")>
    Public Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ActiveIndProperty, Value)
      End Set
    End Property

    Public Shared InactiveReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InactiveReason, "Inactive Reason", "")
    ''' <summary>
    ''' Gets and sets the Inactive Reason value
    ''' </summary>
    <Display(Name:="Inactive Reason", Description:="If a Human Resource is marked as Not Active then an InactiveReason is required"),
    StringLength(200, ErrorMessage:="Inactive Reason cannot be more than 200 characters")>
    Public Property InactiveReason() As String
      Get
        Return GetProperty(InactiveReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(InactiveReasonProperty, Value)
      End Set
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Primary Manager", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Primary Manager", Description:="The person managing the human resource"),
    DropDownWeb(GetType(ROManagerList),
                  DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  DisplayMember:="HRName",
                  LookupMember:="ManagerHumanResourceName",
                  ValueMember:="HumanResourceID",
                  DropDownColumns:={"HRName", "CellPhoneNumber", "EmailAddress"},
                  BeforeFetchJS:="HumanResourceBO.setManagerIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="HumanResourceBO.triggerManagerIDAutoPopulate",
                  AfterFetchJS:="HumanResourceBO.afterManagerIDRefreshAjax",
                  OnItemSelectJSFunction:="HumanResourceBO.onManagerSelected",
                  DropDownCssClass:="room-dropdown"),
    SetExpression("HumanResourceBO.ManagerHumanResourceIDSet(self)")>
    Public Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ManagerHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EmployeeCode, "") _
                                                                    .AddSetExpression("HumanResourceBO.EmployeeCodeSet(self)")
    ''' <summary>
    ''' Gets and sets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="The employee code from payroll"),
    Required(ErrorMessage:="Employee Code required"),
    StringLength(30, ErrorMessage:="Employee Code cannot be more than 30 characters")>
    Public Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmployeeCodeProperty, Value)
      End Set
    End Property

    Public Shared ReportingColourProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportingColour, "Reporting Colour", "")
    ''' <summary>
    ''' Gets and sets the Reporting Colour value
    ''' </summary>
    <Display(Name:="Reporting Colour", Description:="The colour to use on the Board Report for Event Managers"),
    StringLength(50, ErrorMessage:="Reporting Colour cannot be more than 50 characters")>
    Public Property ReportingColour() As String
      Get
        Return GetProperty(ReportingColourProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ReportingColourProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PhotoPathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhotoPath, "Photo Path", "")
    ''' <summary>
    ''' Gets and sets the Photo Path value
    ''' </summary>
    <Display(Name:="Photo Path", Description:="File path where the photo is stored"),
    StringLength(400, ErrorMessage:="Photo Path cannot be more than 400 characters")>
    Public Property PhotoPath() As String
      Get
        Return GetProperty(PhotoPathProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PhotoPathProperty, Value)
      End Set
    End Property

    'Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the System value
    ' ''' </summary>
    '<Display(Name:="System", Description:="The system to which the human resource belongs to")>
    'Public Property SystemID() As Integer?
    '  Get
    '    Return GetProperty(SystemIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(SystemIDProperty, Value)
    '  End Set
    'End Property

    Public Shared PantSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PantSize, "Pant Size", "")
    ''' <summary>
    ''' Gets and sets the Pant Size value
    ''' </summary>
    <Display(Name:="Pant Size", Description:="Pants size of the human resource"),
    StringLength(20, ErrorMessage:="Pant Size cannot be more than 20 characters")>
    Public Property PantSize() As String
      Get
        Return GetProperty(PantSizeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PantSizeProperty, Value)
      End Set
    End Property

    Public Shared ShirtSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShirtSize, "Shirt Size", "")
    ''' <summary>
    ''' Gets and sets the Shirt Size value
    ''' </summary>
    <Display(Name:="Shirt Size", Description:="Shirt size of the human resource"),
    StringLength(20, ErrorMessage:="Shirt Size cannot be more than 20 characters")>
    Public Property ShirtSize() As String
      Get
        Return GetProperty(ShirtSizeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShirtSizeProperty, Value)
      End Set
    End Property

    Public Shared JacketSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.JacketSize, "Jacket Size", "")
    ''' <summary>
    ''' Gets and sets the Jacket Size value
    ''' </summary>
    <Display(Name:="Jacket Size", Description:="Jacket size of the human resource"),
    StringLength(20, ErrorMessage:="Jacket Size cannot be more than 20 characters")>
    Public Property JacketSize() As String
      Get
        Return GetProperty(JacketSizeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(JacketSizeProperty, Value)
      End Set
    End Property

    Public Shared SAAVoyagerNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SAAVoyagerNo, "SAA Voyager No", "")
    ''' <summary>
    ''' Gets and sets the SAA Voyager No value
    ''' </summary>
    <Display(Name:="SAA Voyager No", Description:="SAA voyager number of the human resource"),
    StringLength(50, ErrorMessage:="SAA Voyager No cannot be more than 50 characters")>
    Public Property SAAVoyagerNo() As String
      Get
        Return GetProperty(SAAVoyagerNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SAAVoyagerNoProperty, Value)
      End Set
    End Property

    Public Shared SAAVoyagerTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SAAVoyagerTypeID, "SAA Voyager Type", Nothing)
    ''' <summary>
    ''' Gets and sets the SAA Voyager Type value
    ''' </summary>
    <Display(Name:="SAA Voyager Type", Description:="SAA voyager type")>
    Public Property SAAVoyagerTypeID() As Integer?
      Get
        Return GetProperty(SAAVoyagerTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SAAVoyagerTypeIDProperty, Value)
      End Set
    End Property

    Public Shared BAVoyagerNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BAVoyagerNo, "BA Voyager No", "")
    ''' <summary>
    ''' Gets and sets the BA Voyager No value
    ''' </summary>
    <Display(Name:="BA Voyager No", Description:="BA voyager number fo the human resource"),
    StringLength(50, ErrorMessage:="BA Voyager No cannot be more than 50 characters")>
    Public Property BAVoyagerNo() As String
      Get
        Return GetProperty(BAVoyagerNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BAVoyagerNoProperty, Value)
      End Set
    End Property

    Public Shared BAVoyagerTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BAVoyagerTypeID, "BA Voyager Type", Nothing)
    ''' <summary>
    ''' Gets and sets the BA Voyager Type value
    ''' </summary>
    <Display(Name:="BA Voyager Type", Description:="BA voyager type")>
    Public Property BAVoyagerTypeID() As Integer?
      Get
        Return GetProperty(BAVoyagerTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BAVoyagerTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PayHalfMonthIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PayHalfMonthInd, "Pay Half Month", False)
    ''' <summary>
    ''' Gets and sets the Pay Half Month value
    ''' </summary>
    <Display(Name:="Pay Half Month", Description:="True if the human resource is paid half monthly"),
    Required(ErrorMessage:="Pay Half Month required")>
    Public Property PayHalfMonthInd() As Boolean
      Get
        Return GetProperty(PayHalfMonthIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PayHalfMonthIndProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Employed by a supplier"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSupplierList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Supplier")>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    'Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ' ''' <summary>
    ' ''' Gets and sets the Supplier value
    ' ''' </summary>
    '<Display(Name:="Supplier", Description:="Employed by a supplier")>
    'Public Property Supplier() As String
    '  Get
    '    Return GetProperty(SupplierProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(SupplierProperty, Value)
    '  End Set
    'End Property

    Public Shared NationalityCountyIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.NationalityCountyID, Nothing) _
                                                                    .AddSetExpression("HumanResourceBO.NationalityCountyIDSet(self)")
    ''' <summary>
    ''' Gets and sets the Nationality Country value
    '''Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCountryList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Nationality"),
    ''' </summary>
    <Display(Name:="Nationality", Description:="Country of birth of the human resource"),
    DropDownWeb(GetType(Maintenance.Locations.ReadOnly.ROCountryList), DisplayMember:="Country", ValueMember:="CountryID")>
    Public Property NationalityCountyID() As Integer?
      Get
        Return GetProperty(NationalityCountyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NationalityCountyIDProperty, Value)
      End Set
    End Property

    Public Shared ContractDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractDays, "Contract Days", 0)
    ''' <summary>
    ''' Gets and sets the Contract Days value
    ''' </summary>
    <Display(Name:="Contract Days", Description:="Number of days the human resource is contracted for")>
    Public Property ContractDays() As Integer
      Get
        Return GetProperty(ContractDaysProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ContractDaysProperty, Value)
      End Set
    End Property

    Public Shared AddressR1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR1, "Address R 1", "")
    ''' <summary>
    ''' Gets and sets the Address R 1 value
    ''' </summary>
    <Display(Name:="Address R 1", Description:="Residential Address line 1"),
    StringLength(250, ErrorMessage:="Address R 1 cannot be more than 250 characters")>
    Public Property AddressR1() As String
      Get
        Return GetProperty(AddressR1Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressR1Property, Value)
      End Set
    End Property

    Public Shared AddressR2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR2, "Address R 2", "")
    ''' <summary>
    ''' Gets and sets the Address R 2 value
    ''' </summary>
    <Display(Name:="Address R 2", Description:="Residential Address line 2"),
    StringLength(250, ErrorMessage:="Address R 2 cannot be more than 250 characters")>
    Public Property AddressR2() As String
      Get
        Return GetProperty(AddressR2Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressR2Property, Value)
      End Set
    End Property

    Public Shared AddressR3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR3, "Address R 3", "")
    ''' <summary>
    ''' Gets and sets the Address R 3 value
    ''' </summary>
    <Display(Name:="Address R 3", Description:="Residential Address line 3"),
    StringLength(250, ErrorMessage:="Address R 3 cannot be more than 250 characters")>
    Public Property AddressR3() As String
      Get
        Return GetProperty(AddressR3Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressR3Property, Value)
      End Set
    End Property

    Public Shared AddressR4Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR4, "Address R 4", "")
    ''' <summary>
    ''' Gets and sets the Address R 4 value
    ''' </summary>
    <Display(Name:="Address R 4", Description:="Residential Address line 4"),
    StringLength(250, ErrorMessage:="Address R 4 cannot be more than 250 characters")>
    Public Property AddressR4() As String
      Get
        Return GetProperty(AddressR4Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressR4Property, Value)
      End Set
    End Property

    Public Shared PostalCodeRProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalCodeR, "Postal Code R", "")
    ''' <summary>
    ''' Gets and sets the Postal Code R value
    ''' </summary>
    <Display(Name:="Postal Code R", Description:="Residential Address Postal Code"),
    StringLength(50, ErrorMessage:="Postal Code R cannot be more than 50 characters")>
    Public Property PostalCodeR() As String
      Get
        Return GetProperty(PostalCodeRProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PostalCodeRProperty, Value)
      End Set
    End Property

    Public Shared AddressP1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP1, "Address P 1", "")
    ''' <summary>
    ''' Gets and sets the Address P 1 value
    ''' </summary>
    <Display(Name:="Address P 1", Description:="Postal Address line 1"),
    StringLength(250, ErrorMessage:="Address P 1 cannot be more than 250 characters")>
    Public Property AddressP1() As String
      Get
        Return GetProperty(AddressP1Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressP1Property, Value)
      End Set
    End Property

    Public Shared AddressP2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP2, "Address P 2", "")
    ''' <summary>
    ''' Gets and sets the Address P 2 value
    ''' </summary>
    <Display(Name:="Address P 2", Description:="Postal Address line 2"),
    StringLength(250, ErrorMessage:="Address P 2 cannot be more than 250 characters")>
    Public Property AddressP2() As String
      Get
        Return GetProperty(AddressP2Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressP2Property, Value)
      End Set
    End Property

    Public Shared AddressP3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP3, "Address P 3", "")
    ''' <summary>
    ''' Gets and sets the Address P 3 value
    ''' </summary>
    <Display(Name:="Address P 3", Description:="Postal Address line 3"),
    StringLength(250, ErrorMessage:="Address P 3 cannot be more than 250 characters")>
    Public Property AddressP3() As String
      Get
        Return GetProperty(AddressP3Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressP3Property, Value)
      End Set
    End Property

    Public Shared AddressP4Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP4, "Address P 4", "")
    ''' <summary>
    ''' Gets and sets the Address P 4 value
    ''' </summary>
    <Display(Name:="Address P 4", Description:="Postal Address line 4"),
    StringLength(250, ErrorMessage:="Address P 4 cannot be more than 250 characters")>
    Public Property AddressP4() As String
      Get
        Return GetProperty(AddressP4Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressP4Property, Value)
      End Set
    End Property

    Public Shared PostalCodePProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalCodeP, "Postal Code P", "")
    ''' <summary>
    ''' Gets and sets the Postal Code P value
    ''' </summary>
    <Display(Name:="Postal Code P", Description:="Postal Address Postal Code"),
    StringLength(50, ErrorMessage:="Postal Code P cannot be more than 50 characters")>
    Public Property PostalCodeP() As String
      Get
        Return GetProperty(PostalCodePProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PostalCodePProperty, Value)
      End Set
    End Property

    Public Shared ManagerHumanResourceID2Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID2, "Secondary Manager", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Human Resource ID 2 value
    ''' </summary>
    <Display(Name:="Secondary Manager", Description:="A 2nd manager that is allowed to perform certain actions as the primary manager"),
    DropDownWeb(GetType(ROManagerList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                DisplayMember:="HRName", LookupMember:="SecondaryManagerHumanResourceName", ValueMember:="HumanResourceID",
                DropDownColumns:={"HRName", "CellPhoneNumber", "EmailAddress"},
                BeforeFetchJS:="HumanResourceBO.setSecondaryManagerIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceBO.triggerSecondaryManagerIDAutoPopulate",
                AfterFetchJS:="HumanResourceBO.afterSecondaryManagerIDRefreshAjax",
                OnItemSelectJSFunction:="HumanResourceBO.onSecondaryManagerSelected"),
    SetExpression("HumanResourceBO.SecondaryManagerHumanResourceIDSet(self)")>
    Public Property ManagerHumanResourceID2() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceID2Property)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ManagerHumanResourceID2Property, Value)
      End Set
    End Property

    Public Shared TaxNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TaxNumber, "Tax Number", "")
    ''' <summary>
    ''' Gets and sets the Tax Number value
    ''' </summary>
    <Display(Name:="Tax Number", Description:="Tax number of the human resource"),
    StringLength(20, ErrorMessage:="Tax Number cannot be more than 20 characters")>
    Public Property TaxNumber() As String
      Get
        Return GetProperty(TaxNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TaxNumberProperty, Value)
      End Set
    End Property

    Public Shared InvoiceSupplierIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InvoiceSupplierInd, "Invoice Supplier", False)
    ''' <summary>
    ''' Gets and sets the Invoice Supplier value
    ''' </summary>
    <Display(Name:="Invoice Supplier", Description:=""),
    Required(ErrorMessage:="Invoice Supplier required")>
    Public Property InvoiceSupplierInd() As Boolean
      Get
        Return GetProperty(InvoiceSupplierIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InvoiceSupplierIndProperty, Value)
      End Set
    End Property

    Public Shared ManagerHumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerHumanResourceName, "THuman Resource Name", "")
    ''' <summary>
    ''' Gets the Human Resource Name value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="Human Resource Name")>
    Public ReadOnly Property ManagerHumanResourceName() As String
      Get
        Return GetProperty(ManagerHumanResourceNameProperty)
      End Get
    End Property

    Public Shared SecondaryManagerHumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondaryManagerHumanResourceName, "Secondary Manager", "")
    ''' <summary>
    ''' Gets and sets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Secondary Manager", Description:="The 2nd person managing the human resource")>
    Public Property SecondaryManagerHumanResourceName() As String
      Get
        Return GetProperty(SecondaryManagerHumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SecondaryManagerHumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared UseSubstituteIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UseSubstituteInd, "Use Substitute Manager", False)
    ''' <summary>
    ''' Retrieves whether or not a substitute manager is selected
    ''' </summary>
    <Display(Name:="Use Substitute Manager", Description:="Select whether a substitute manager will be selected")>
    Public ReadOnly Property UseSubstituteInd() As Boolean
      Get
        Return GetProperty(UseSubstituteIndProperty)
      End Get
    End Property

    Public Shared SubstituteManagerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubstituteManagerID, "Use Substitute Manager", 0)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Substitute Manager ID", Description:="return the substitute manager ID")>
    Public ReadOnly Property SubstituteManagerID() As Integer
      Get
        Return GetProperty(SubstituteManagerIDProperty)
      End Get
    End Property

    Public Shared UseSubstituteEndDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.UseSubstituteEndDate, "End date for manager substitution", 0)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Substitute Manager ID", Description:="return the substitute manager ID")>
    Public ReadOnly Property UseSubstituteEndDate() As DateTime
      Get
        Return GetProperty(UseSubstituteEndDateProperty)
      End Get
    End Property

    Public Shared LicenceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LicenceInd, "Has Drivers Licence?", False)
    ''' <summary>
    ''' Gets and sets the Pay Half Month value
    ''' </summary>
    <Display(Name:="Has Drivers Licence?", Description:="True if the human resource has a drivers licence")>
    Public Property LicenceInd() As Boolean
      Get
        Return GetProperty(LicenceIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(LicenceIndProperty, Value)
      End Set
    End Property

    Public Shared LicenceExpiryDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LicenceExpiryDate, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="Licence Expiry Date", Description:="The Expiry date of the human resource licence")>
    Public Property LicenceExpiryDate As DateTime?
      Get
        Return GetProperty(LicenceExpiryDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LicenceExpiryDateProperty, Value)
      End Set
    End Property

    Public Shared ContractStartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ContractStartDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Contract Start Date value
    ''' </summary>
    <Display(Name:="Contract Start Date", Description:="The Start date of the contract")>
    Public Property ContractStartDate As DateTime?
      Get
        Return GetProperty(ContractStartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ContractStartDateProperty, Value)
      End Set
    End Property

    Public Shared ContractEndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ContractEndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Contract End Date value
    ''' </summary>
    <Display(Name:="Contract End Date", Description:="The End date of the contract")>
    Public Property ContractEndDate As DateTime?
      Get
        Return GetProperty(ContractEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ContractEndDateProperty, Value)
      End Set
    End Property

    Public Shared AccreditationImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccreditationImageID, "Accreditation Image", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Accreditation Image")>
    Public Property AccreditationImageID() As Integer?
      Get
        Return GetProperty(AccreditationImageIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccreditationImageIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ResourceID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsFreelancer, "IsFreelancer", False)
    ''' <summary>
    ''' Gets and sets the Pay Half Month value
    ''' </summary>
    <Display(Name:="IsFreelancer")>
    Public Property IsFreelancer() As Boolean
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsFreelancerProperty, Value)
      End Set
    End Property

    Public Shared PrimarySystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PrimarySystemID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Sub-Dept"), Required(ErrorMessage:="Primary Sub-Dept is required"),
    DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property PrimarySystemID() As Integer?
      Get
        Return GetProperty(PrimarySystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PrimarySystemIDProperty, value)
      End Set
    End Property

    Public Shared PrimaryProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PrimaryProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Area"), Required(ErrorMessage:="Primary Area is required"),
    DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", FilterMethodName:="HumanResourceBO.FilteredPrimaryAreaList")>
    Public Property PrimaryProductionAreaID() As Integer?
      Get
        Return GetProperty(PrimaryProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PrimaryProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared PrimarySystemAreaCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PrimarySystemAreaCount, "Primary Area Count", 0)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Primary Area Count")>
    Public Property PrimarySystemAreaCount() As Integer
      Get
        Return GetProperty(PrimarySystemAreaCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(PrimarySystemAreaCountProperty, value)
      End Set
    End Property

    Public Shared DuplicateCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DuplicateCount, "Duplicate Count", 0)
    <Display(Name:="Duplicate Count")>
    Public Property DuplicateCount() As Integer
      Get
        Return GetProperty(DuplicateCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(DuplicateCountProperty, value)
      End Set
    End Property

    Public Shared PrimarySkillIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PrimarySkillID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property PrimarySkillID() As Integer?
      Get
        Return GetProperty(PrimarySkillIDProperty)
      End Get
    End Property

    Public Shared PrimarySkillDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PrimarySkillDisciplineID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Discipline"), Required(ErrorMessage:="Primary Discipline is required"),
    DropDownWeb(GetType(RODisciplineList))>
    Public Property PrimarySkillDisciplineID() As Integer?
      Get
        Return GetProperty(PrimarySkillDisciplineIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PrimarySkillDisciplineIDProperty, value)
      End Set
    End Property

    Public Shared PrimarySkillPositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PrimarySkillPositionID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Position"),
    DropDownWeb(GetType(ROPositionList), FilterMethodName:="HumanResourceBO.FilteredPrimarySkillPositionList")>
    Public Property PrimarySkillPositionID() As Integer?
      Get
        Return GetProperty(PrimarySkillPositionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PrimarySkillPositionIDProperty, value)
      End Set
    End Property

    Public Shared SkillCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SkillCount, "Skill Count", 0)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Skill Count")>
    Public ReadOnly Property SkillCount() As Integer
      Get
        Return GetProperty(SkillCountProperty)
      End Get
    End Property

    Public Shared PrimarySkillCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PrimarySkillCount, "Skill Count", 0)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Skill Count")>
    Public ReadOnly Property PrimarySkillCount() As Integer
      Get
        Return GetProperty(PrimarySkillCountProperty)
      End Get
    End Property

    Public Shared MissingRateCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MissingRateCount, "Missing Rate Count", 0)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Missing Rate Count")>
    Public ReadOnly Property MissingRateCount() As Integer
      Get
        Return GetProperty(MissingRateCountProperty)
      End Get
    End Property

    Public Shared CurrentUserIsHRManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CurrentUserIsHRManager, "Manager?", False)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Manager?")>
    Public ReadOnly Property CurrentUserIsHRManager() As Boolean
      Get
        Return GetProperty(CurrentUserIsHRManagerProperty)
      End Get
    End Property

    Public Shared IsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManager, "Is a Manager?", False)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="Is a Manager?")>
    Public Property IsManager() As Boolean
      Get
        Return GetProperty(IsManagerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsManagerProperty, value)
      End Set
    End Property

    Public Shared IsInternProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsIntern, "Is an Intern?", False)
    ''' <summary>
    ''' Retrieves the IsIntern property
    ''' </summary>
    <Display(Name:="Is an Intern?")>
    Public Property IsIntern() As Boolean
      Get
        Return GetProperty(IsInternProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsInternProperty, value)
      End Set
    End Property

    Public Shared IsOfficeWorkerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOfficeWorker, "Is an Office Worker?", False)
    ''' <summary>
    ''' Retrieves the IsOfficeWorker property
    ''' </summary>
    <Display(Name:="Is an Office Worker?")>
    Public Property IsOfficeWorker() As Boolean
      Get
        Return GetProperty(IsOfficeWorkerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsOfficeWorkerProperty, value)
      End Set
    End Property

    Public Shared VendorNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VendorNumber, "", "")
    ''' <summary>
    ''' Gets and sets the Employee Code value
    ''' </summary>
    <Display(Name:="Vendor Number")>
    Public Property VendorNumber() As String
      Get
        Return GetProperty(VendorNumberProperty)
      End Get
      Set(value As String)
        SetProperty(VendorNumberProperty, value)
      End Set
    End Property

    Public Shared IsLoadingImageProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLoadingImage, "IsLoadingImage", False)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="IsLoadingImage"), AlwaysClean>
    Public Property IsLoadingImage() As Boolean
      Get
        Return GetProperty(IsLoadingImageProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsLoadingImageProperty, value)
      End Set
    End Property

    Public Shared IsCheckingDuplicatesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCheckingDuplicates, "IsCheckingDuplicates", False)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="IsCheckingDuplicates"), AlwaysClean>
    Public Property IsCheckingDuplicates() As Boolean
      Get
        Return GetProperty(IsCheckingDuplicatesProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsCheckingDuplicatesProperty, value)
      End Set
    End Property

    Public Shared DuplicatesCheckedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DuplicatesChecked, "IsCheckingDuplicates", False)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="IsCheckingDuplicates"), AlwaysClean>
    Public Property DuplicatesChecked() As Boolean
      Get
        Return GetProperty(DuplicatesCheckedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(DuplicatesCheckedProperty, value)
      End Set
    End Property

    Public Shared HumanResourceImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceImageID, "HumanResourceImageID", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="HumanResourceImageID")>
    Public Property HumanResourceImageID() As Integer?
      Get
        Return GetProperty(HumanResourceImageIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceImageIDProperty, Value)
      End Set
    End Property

    Public Shared OrigPrimarySystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OrigPrimarySystemID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Sub-Dept")>
    Public Property OrigPrimarySystemID() As Integer?
      Get
        Return GetProperty(OrigPrimarySystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(OrigPrimarySystemIDProperty, value)
      End Set
    End Property

    Public Shared OrigPrimaryProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OrigPrimaryProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Area")>
    Public Property OrigPrimaryProductionAreaID() As Integer?
      Get
        Return GetProperty(OrigPrimaryProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(OrigPrimaryProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared OrigPrimarySkillIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OrigPrimarySkillID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property OrigPrimarySkillID() As Integer?
      Get
        Return GetProperty(OrigPrimarySkillIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(OrigPrimarySkillIDProperty, value)
      End Set
    End Property

    Public Shared OrigPrimarySkillDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OrigPrimarySkillDisciplineID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Discipline")>
    Public Property OrigPrimarySkillDisciplineID() As Integer?
      Get
        Return GetProperty(OrigPrimarySkillDisciplineIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(OrigPrimarySkillDisciplineIDProperty, value)
      End Set
    End Property

    Public Shared OrigPrimarySkillPositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OrigPrimarySkillPositionID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Primary Position")>
    Public Property OrigPrimarySkillPositionID() As Integer?
      Get
        Return GetProperty(OrigPrimarySkillPositionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(OrigPrimarySkillPositionIDProperty, value)
      End Set
    End Property

    Public Shared HasMissingBasicsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasMissingBasics, "HasMissingBasics", True)
    ''' <summary>
    ''' Retrieves the substitute manager ID
    ''' </summary>
    <Display(Name:="HasMissingBasics")>
    Public Property HasMissingBasics() As Boolean
      Get
        Return GetProperty(HasMissingBasicsProperty)
      End Get
      Set(value As Boolean)
        SetProperty(HasMissingBasicsProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared HRSystemSelectListProperty As PropertyInfo(Of HRSystemSelectList) = RegisterProperty(Of HRSystemSelectList)(Function(c) c.HRSystemSelectList, "Human Resource System List")
    Public ReadOnly Property HRSystemSelectList() As HRSystemSelectList
      Get
        If GetProperty(HRSystemSelectListProperty) Is Nothing Then
          LoadProperty(HRSystemSelectListProperty, HR.HRSystemSelectList.NewHRSystemSelectList())
        End If
        Return GetProperty(HRSystemSelectListProperty)
      End Get
    End Property

    Public Shared ROHRDuplicateListProperty As PropertyInfo(Of ROHRDuplicateList) = RegisterProperty(Of ROHRDuplicateList)(Function(c) c.ROHRDuplicateList, "Production HumanResources TBC List")
    <AlwaysClean, InitialDataOnly>
    Public ReadOnly Property ROHRDuplicateList() As ROHRDuplicateList
      Get
        If GetProperty(ROHRDuplicateListProperty) Is Nothing Then
          LoadProperty(ROHRDuplicateListProperty, HR.ROHRDuplicateList.NewROHRDuplicateList())
        End If
        Return GetProperty(ROHRDuplicateListProperty)
      End Get
    End Property

    'Public Shared ManagerSubstituteListProperty As PropertyInfo(Of ManagerSubstituteList) = RegisterProperty(Of ManagerSubstituteList)(Function(c) c.ManagerSubstituteList, "Manager Substitute List")
    'Public ReadOnly Property ManagerSubstituteList() As ManagerSubstituteList
    '  Get
    '    If GetProperty(ManagerSubstituteListProperty) Is Nothing Then
    '      LoadProperty(ManagerSubstituteListProperty, HR.ManagerSubstituteList.NewManagerSubstituteList())
    '    End If
    '    Return GetProperty(ManagerSubstituteListProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Public Function isAuthorized() As Boolean

      If Security.Settings.CurrentUser.SystemID = CInt(CommonData.Enums.System.ProductionServices) Then
        If Security.Settings.CurrentUser.HumanResourceID = Me.ManagerHumanResourceID Then
          Return True
        ElseIf Me.UseSubstituteInd AndAlso Me.UseSubstituteEndDate < Today Then
          If Security.Settings.CurrentUser.HumanResourceID = Me.SubstituteManagerID Then
            Return True
          Else
            Return False
          End If
        Else
          Return False
        End If
      Else
        Return False
      End If
    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Firstname.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource")
        Else
          Return String.Format("Blank {0}", "Human Resource")
        End If
      Else
        Return Me.Firstname
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"HumanResourceSkills", "HumanResourceBankingDetails", "HumanResourceOffPeriods", "HumanResourceShiftPatterns", "HumanResourceSystems", "HRDocuments", "HumanResourceSecondments"}
      End Get
    End Property

    'Sub PopulateDuplicates()

    '  'HumanResourceDuplicateList = OBLib.HR.HumanResourceDuplicateList.NewHumanResourceDuplicateList()
    '  For Each ropd As ROHRDuplicate In ROHRDuplicateList
    '    Dim hrd As HumanResourceDuplicate = HumanResourceDuplicate.NewHumanResourceDuplicate()
    '    hrd.HumanResourceID = Me.HumanResourceID
    '    hrd.DuplicateHumanResourceID = ropd.OHumanResourceID
    '    hrd.MatchTypeID = ropd.MatchTypeID
    '    hrd.MatchType = ropd.MatchType
    '    hrd.EmployeeCode = ropd.EmployeeCode
    '    hrd.IDNo = ropd.IDNo
    '    hrd.FirstName = ropd.FirstName
    '    hrd.Surname = ropd.Surname
    '    hrd.Systems = ropd.System
    '    hrd.Ordinal = ropd.Ordinal
    '    HumanResourceDuplicateList.Add(hrd)
    '  Next

    'End Sub



#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EmailAddressProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.EmailAddressValid"
        .ServerRuleFunction = AddressOf EmailValid
      End With

      With AddWebRule(AlternativeEmailAddressProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.AltEmailAddressValid"
        .ServerRuleFunction = AddressOf AltEmailAddressValid
      End With

      With AddWebRule(InactiveReasonProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.InActiveReasonRequired"
        .ServerRuleFunction = AddressOf InactiveReasonValid
        .AddTriggerProperty(ActiveIndProperty)
        .AffectedProperties.Add(ActiveIndProperty)
      End With

      'With AddWebRule(InvoiceSupplierIndProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceBO.InvoiceSupplierValid"
      '  .ServerRuleFunction = AddressOf InvoiceSupplierValid
      '  .AffectedProperties.Add(SupplierIDProperty)
      'End With

      With AddWebRule(LicenceExpiryDateProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.LicenceExpDateValid"
        .ServerRuleFunction = AddressOf LicenceExpiryValid
        .AddTriggerProperty(LicenceIndProperty)
        .AffectedProperties.Add(LicenceIndProperty)
      End With

      With AddWebRule(IDNoProperty)
        .ASyncBusyText = "Checking ID Number..."
        '.JavascriptRuleFunctionName = "HumanResourceBO.IDNumberValid"
        .AddTriggerProperty(NationalityCountyIDProperty)
        .ServerRuleFunction = AddressOf CheckIDNumber
      End With

      With AddWebRule(DuplicateCountProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.DuplicateCountValid"
        .AddTriggerProperties(FirstnameProperty, SurnameProperty, IDNoProperty, EmployeeCodeProperty, NationalityCountyIDProperty)
        .AffectedProperties.AddRange({FirstnameProperty, SurnameProperty, IDNoProperty, EmployeeCodeProperty, NationalityCountyIDProperty})
        .ASyncBusyText = "Checking Possible Duplicates"
        .ServerRuleFunction = AddressOf DuplicateCountValid
      End With
      '

      'With AddWebRule(TaxNumberProperty)
      '  .ASyncBusyText = "Checking Tax Number"
      '  .ServerRuleFunction = AddressOf TaxNumberValid
      'End With

      With AddWebRule(ContractTypeIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.ContractTypeIDValid"
        .ServerRuleFunction = AddressOf ContractTypeIDValid
      End With

      With AddWebRule(CityIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.CityIDValid"
        .ServerRuleFunction = AddressOf CityIDValid
      End With

      With AddWebRule(SupplierIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.SupplierIDValid"
        .ServerRuleFunction = AddressOf SupplierIDValid
        '.AddTriggerProperty(SupplierProperty)
        .AddTriggerProperty(InvoiceSupplierIndProperty)
        '.AffectedProperties.Add(SupplierProperty)
      End With

      With AddWebRule(NationalityCountyIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.NationalityCountryIDValid"
        .ServerRuleFunction = AddressOf NationalityCountryIDValid
      End With

      With AddWebRule(PrimarySystemIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.PrimarySystemAndAreaValid"
        .ServerRuleFunction = AddressOf PrimarySystemAndAreaValid
        .AddTriggerProperty(PrimaryProductionAreaIDProperty)
        '.AddTriggerProperty(PrimaryAreaNameProperty)
        .AffectedProperties.Add(PrimaryProductionAreaIDProperty)
        '.AffectedProperties.Add(PrimaryAreaNameProperty)
      End With

      With AddWebRule(PrimarySkillIDProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.PrimarySkillIDValid"
        .ServerRuleFunction = AddressOf PrimarySkillIDValid
        .AddTriggerProperties({PrimarySkillDisciplineIDProperty})
        .AddTriggerProperties({PrimarySkillPositionIDProperty})
        .AffectedProperties.Add(PrimarySkillDisciplineIDProperty)
        .AffectedProperties.Add(PrimarySkillPositionIDProperty)
        '.AffectedProperties.Add(PrimarySkillDisciplineProperty)
      End With

      'With AddWebRule(SkillCountProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceBO.SkillCountValid"
      '  .ServerRuleFunction = AddressOf SkillCountValid
      '  .AffectedProperties.Add(PrimarySkillDisciplineIDProperty)
      '  .AffectedProperties.Add(PrimarySkillDisciplineProperty)
      'End With

      'With AddWebRule(PrimarySkillCountProperty)
      '  .JavascriptRuleFunctionName = "HumanResourceBO.PrimarySkillCountValid"
      '  .ServerRuleFunction = AddressOf PrimarySkillCountValid
      '  .AffectedProperties.Add(PrimarySkillDisciplineIDProperty)
      '  .AffectedProperties.Add(PrimarySkillDisciplineProperty)
      'End With

      With AddWebRule(MissingRateCountProperty)
        .JavascriptRuleFunctionName = "HumanResourceBO.MissingRateCountValid"
        .ServerRuleFunction = AddressOf MissingRateCountValid
        .AffectedProperties.Add(PrimarySkillDisciplineIDProperty)
        '.AffectedProperties.Add(PrimarySkillDisciplineProperty)
      End With

      'With AddWebRule(UseSubstituteIndProperty)
      '  .ASyncBusyText = "Substitute being checked"
      'End With

    End Sub

    Public Shared Function PrimarySkillIDValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = ""
      'If Not HumanResource.IsNew Then
      '  If HumanResource.PrimarySkillID Is Nothing Then
      '    ErrorDescription = "Primary Skill is required"
      '  End If
      'Else
      If HumanResource.PrimarySkillDisciplineID Is Nothing Then
        ErrorDescription = "Primary Skill is required"
      End If
      'End If
      Return ErrorDescription

    End Function

    Public Shared Function SkillCountValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = ""
      'If Not HumanResource.IsNew AndAlso HumanResource.SkillCount = 0 Then
      '  ErrorDescription = "Skills are required"
      'End If
      Return ErrorDescription

    End Function

    Public Shared Function PrimarySkillCountValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = ""
      'If Not HumanResource.IsNew Then
      '  If HumanResource.PrimarySkillCount = 0 Then
      '    ErrorDescription = "A primary skill is required"
      '  ElseIf HumanResource.PrimarySkillCount > 1 Then
      '    ErrorDescription = "There are too many primary skills specified"
      '  End If
      'End If
      Return ErrorDescription

    End Function

    Public Shared Function MissingRateCountValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = ""
      If Not HumanResource.IsNew AndAlso Not IsNullNothing(HumanResource.ContractTypeID) Then
        Dim ct As ROContractType = OBLib.CommonData.Lists.ROContractTypeList.GetItem(HumanResource.ContractTypeID)
        If ct IsNot Nothing AndAlso ct.FreelancerInd Then
          If HumanResource.CurrentUserIsHRManager AndAlso HumanResource.MissingRateCount > 0 Then
            ErrorDescription = "Some skills have missing rates"
          End If
        End If
      End If
      Return ErrorDescription

    End Function

    Public Shared Function EmailValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = ""
      If Not Singular.Emails.ValidEmailAddress(HumanResource.EmailAddress.Trim) Then
        ErrorDescription = "Please enter a valid email address"
      End If

      Return ErrorDescription

    End Function

    Public Shared Function AltEmailAddressValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = ""
      If HumanResource.AlternativeEmailAddress.Trim.Length > 0 Then
        If Not Singular.Emails.ValidEmailAddress(HumanResource.AlternativeEmailAddress.Trim) Then
          ErrorDescription = "Please enter a valid email address"
        Else
          ErrorDescription = ""
        End If
      End If
      Return ErrorDescription

    End Function

    Public Shared Function InactiveReasonValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = "Inactive Reason Required"
      If Not HumanResource.ActiveInd AndAlso HumanResource.InactiveReason.Trim = "" Then
        Return ErrorDescription
      Else
        Return ""
      End If

    End Function

    Public Shared Function InvoiceSupplierValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = "Supplier must be specified"
      If HumanResource.InvoiceSupplierInd AndAlso HumanResource.SupplierID Is Nothing Then
        Return ErrorDescription
      Else
        Return ""
      End If

    End Function

    Public Shared Function LicenceExpiryValid(HumanResource As HumanResource) As String

      Dim ErrorDescription As String = "Licence Expiry Date Required"
      If HumanResource.LicenceInd AndAlso HumanResource.LicenceExpiryDate Is Nothing Then
        Return ErrorDescription
      Else
        Return ""
      End If

    End Function

    Public Shared Function CheckIDNumber(HumanResource As HumanResource) As String

      'AndAlso HumanResource.ActiveInd
      Dim ErrorDescription As String = ""
      If Not IsNullNothing(HumanResource.NationalityCountyID) Then
        Dim ROCountry As Maintenance.Locations.ReadOnly.ROCountry = CommonData.Lists.ROCountryList.GetItem(HumanResource.NationalityCountyID)
        If CompareSafe(ROCountry.CountryID, CType(CommonData.Enums.Country.SouthAfrica, Integer)) Then
          Singular.Misc.IDNumber.ValidIDNumber(HumanResource.IDNo, ErrorDescription)
        End If
      End If
      Return ErrorDescription

    End Function

    Public Shared Function ContractTypeIDValid(HumanResource As HumanResource) As String

      Dim HR = HumanResource
      Dim Supplier
      Dim ContractType
      If HR.SupplierID IsNot Nothing Then
        Supplier = True
      Else
        Supplier = False
      End If
      If HR.ContractTypeID IsNot Nothing Then
        ContractType = True
      Else
        ContractType = False
      End If
      If Supplier AndAlso ContractType AndAlso Not HumanResource.InvoiceSupplierInd Then
        Return "Supplier Human Resources cannot have a contract type"
      ElseIf Not Supplier AndAlso Not ContractType Then
        Return "Contract Type is required"
      End If
      Return ""

    End Function

    Public Shared Function CityIDValid(HumanResource As HumanResource) As String

      If HumanResource.CityID Is Nothing Then
        Return "Home City is required"
      End If
      Return ""

    End Function

    Public Shared Function SupplierIDValid(HumanResource As HumanResource) As String
      If HumanResource.InvoiceSupplierInd Then
        If HumanResource.SupplierID Is Nothing Then
          Return "Supplier is required"
        End If
      End If
      Return ""
    End Function

    Public Shared Function NationalityCountryIDValid(HumanResource As HumanResource) As String

      If HumanResource.NationalityCountyID Is Nothing Then
        Return "Nationality is required"
      End If
      Return ""

    End Function

    Public Shared Function DuplicateCountValid(HumanResource As HumanResource) As String

      If Not HumanResource.DuplicatesChecked Then
        HumanResource.DuplicateCount = HumanResource.GetDuplicates(HumanResource)
      End If
      If HumanResource.DuplicateCount > 0 Then
        Return "Duplicates Exist!"
      End If
      Return ""

    End Function

    Public Shared Function PrimarySystemAndAreaValid(HumanResource As HumanResource) As String

      Dim ErrorString As String = ""

      'If Not HumanResource.IsNew Then
      '  If HumanResource.PrimarySystemAreaCount > 1 Then
      '    ErrorString = "Only 1 Primary Area is allowed"
      '  ElseIf HumanResource.PrimarySystemAreaCount = 0 Then
      '    ErrorString = "A Primary Area is required to be specified"
      '  End If
      'Else
      If HumanResource.PrimarySystemID Is Nothing Or HumanResource.PrimaryProductionAreaID Is Nothing Then
        If ErrorString.Length > 0 Then
          ErrorString = ErrorString & vbCrLf & "Both Primary Sub-Dept and Area are required"
        Else
          ErrorString = "Both Primary Sub-Dept and Area are required"
        End If
      End If
      'End If

      Return ErrorString

    End Function

    Public Shared Function GetDuplicates(HumanResource As HumanResource) As Integer

      Dim DuplicateCount As Integer = 0
      If HumanResource.Firstname.Trim.Length > 0 Or HumanResource.Surname.Trim.Length > 0 Or HumanResource.IDNo.Trim.Length > 0 Or HumanResource.EmployeeCode.Trim.Length > 0 Then
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdCheckPossibleHRDuplicates",
                                            New String() {"HumanResourceID", "MatchTypeID", "FirstName", "Surname", "IDNo", "EmployeeCode"},
                                            New Object() {Singular.Misc.ZeroDBNull(HumanResource.HumanResourceID),
                                                          DBNull.Value,
                                                          HumanResource.Firstname,
                                                          HumanResource.Surname,
                                                          HumanResource.IDNo,
                                                          HumanResource.EmployeeCode})
        cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmd = cmd.Execute

        HumanResource.ROHRDuplicateList.Clear()
        For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
          Dim newPB As HR.ROHRDuplicate = HR.ROHRDuplicate.NewROHRDuplicate(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6), dr(7), dr(8))
          HumanResource.ROHRDuplicateList.Add(newPB)
        Next
      End If

      HumanResource.DuplicateCount = HumanResource.ROHRDuplicateList.Where(Function(d) {OBLib.CommonData.Enums.EmployeeMatch.EmployeeCodematch, OBLib.CommonData.Enums.EmployeeMatch.IDNummatch}.Contains(d.MatchTypeID)).Count

      Return HumanResource.DuplicateCount

    End Function

    Public Function RefreshDuplicates() As Integer

      Dim DuplicateCount As Integer = 0
      If Me.Firstname.Trim.Length > 0 Or Me.Surname.Trim.Length > 0 Or Me.IDNo.Trim.Length > 0 Or Me.EmployeeCode.Trim.Length > 0 Then
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdCheckPossibleHRDuplicates",
                                            New String() {"HumanResourceID", "MatchTypeID", "FirstName", "Surname", "IDNo", "EmployeeCode"},
                                            New Object() {Singular.Misc.ZeroDBNull(Me.HumanResourceID),
                                                          DBNull.Value,
                                                          Me.Firstname,
                                                          Me.Surname,
                                                          Me.IDNo,
                                                          Me.EmployeeCode})
        cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmd = cmd.Execute

        Me.ROHRDuplicateList.Clear()
        For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
          Dim newPB As HR.ROHRDuplicate = HR.ROHRDuplicate.NewROHRDuplicate(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6), dr(7), dr(8))
          Me.ROHRDuplicateList.Add(newPB)
        Next
      End If

      Me.DuplicateCount = Me.ROHRDuplicateList.Where(Function(d) {OBLib.CommonData.Enums.EmployeeMatch.EmployeeCodematch, OBLib.CommonData.Enums.EmployeeMatch.IDNummatch}.Contains(d.MatchTypeID)).Count

      Return Me.DuplicateCount

    End Function

    Public Function IsIDNumberValid(HumanResource As HumanResource) As String

      'AndAlso HumanResource.ActiveInd
      Dim ErrorDescription As String = ""
      If Not IsNullNothing(HumanResource.NationalityCountyID) Then
        Dim ROCountry As Maintenance.Locations.ReadOnly.ROCountry = CommonData.Lists.ROCountryList.GetItem(HumanResource.NationalityCountyID)
        If CompareSafe(ROCountry.CountryID, CType(CommonData.Enums.Country.SouthAfrica, Integer)) Then
          Singular.Misc.IDNumber.ValidIDNumber(HumanResource.IDNo, ErrorDescription)
        End If
      End If
      Return ErrorDescription

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResource() method.

    End Sub

    Public Shared Function NewHumanResource() As HumanResource

      Return DataPortal.CreateChild(Of HumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResource(dr As SafeDataReader) As HumanResource

      Dim h As New HumanResource()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(FirstnameProperty, .GetString(1))
          LoadProperty(SecondNameProperty, .GetString(2))
          LoadProperty(PreferredNameProperty, .GetString(3))
          LoadProperty(SurnameProperty, .GetString(4))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(EmailAddressProperty, .GetString(6))
          LoadProperty(AlternativeEmailAddressProperty, .GetString(7))
          LoadProperty(PhoneEmailAddressProperty, .GetString(8))
          LoadProperty(CellPhoneNumberProperty, .GetString(9))
          LoadProperty(AlternativeContactNumberProperty, .GetString(10))
          LoadProperty(IDNoProperty, .GetString(11))
          LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(RaceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(GenderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(ActiveIndProperty, .GetBoolean(15))
          LoadProperty(InactiveReasonProperty, .GetString(16))
          LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(EmployeeCodeProperty, .GetString(18))
          LoadProperty(ReportingColourProperty, .GetString(19))
          LoadProperty(CreatedByProperty, .GetInt32(20))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(21))
          LoadProperty(ModifiedByProperty, .GetInt32(22))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(23))
          LoadProperty(PhotoPathProperty, .GetString(24))
          'LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          LoadProperty(PantSizeProperty, .GetString(26))
          LoadProperty(ShirtSizeProperty, .GetString(27))
          LoadProperty(JacketSizeProperty, .GetString(28))
          LoadProperty(SAAVoyagerNoProperty, .GetString(29))
          LoadProperty(SAAVoyagerTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
          LoadProperty(BAVoyagerNoProperty, .GetString(31))
          LoadProperty(BAVoyagerTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(32)))
          LoadProperty(PayHalfMonthIndProperty, .GetBoolean(33))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(34)))
          LoadProperty(NationalityCountyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(35)))
          LoadProperty(ContractDaysProperty, .GetInt32(36))
          LoadProperty(AddressR1Property, .GetString(37))
          LoadProperty(AddressR2Property, .GetString(38))
          LoadProperty(AddressR3Property, .GetString(39))
          LoadProperty(AddressR4Property, .GetString(40))
          LoadProperty(PostalCodeRProperty, .GetString(41))
          LoadProperty(AddressP1Property, .GetString(42))
          LoadProperty(AddressP2Property, .GetString(43))
          LoadProperty(AddressP3Property, .GetString(44))
          LoadProperty(AddressP4Property, .GetString(45))
          LoadProperty(PostalCodePProperty, .GetString(46))
          LoadProperty(ManagerHumanResourceID2Property, Singular.Misc.ZeroNothing(.GetInt32(47)))
          LoadProperty(TaxNumberProperty, .GetString(48))
          LoadProperty(InvoiceSupplierIndProperty, .GetBoolean(49))
          LoadProperty(ManagerHumanResourceNameProperty, .GetString(50))
          LoadProperty(SecondaryManagerHumanResourceNameProperty, .GetString(51))
          LoadProperty(UseSubstituteIndProperty, .GetBoolean(52))
          LoadProperty(SubstituteManagerIDProperty, .GetInt32(53))
          LoadProperty(UseSubstituteEndDateProperty, .GetDateTime(54))
          LoadProperty(LicenceIndProperty, .GetBoolean(55))
          LoadProperty(LicenceExpiryDateProperty, .GetValue(56))
          LoadProperty(AccreditationImageIDProperty, ZeroNothing(.GetInt32(57)))
          LoadProperty(ResourceIDProperty, .GetInt32(58))
          LoadProperty(IsFreelancerProperty, .GetBoolean(59))
          'LoadProperty(CityProperty, .GetString(60)) --HomeCity
          'LoadProperty(CountryProperty, .GetString(61)) --Nationality
          LoadProperty(ContractStartDateProperty, .GetValue(62))
          LoadProperty(ContractEndDateProperty, .GetValue(63))
          LoadProperty(IsManagerProperty, .GetBoolean(64))
          LoadProperty(IsInternProperty, .GetBoolean(65))
          LoadProperty(IsOfficeWorkerProperty, ZeroNothing(.GetBoolean(66)))
          LoadProperty(PrimarySkillIDProperty, ZeroNothing(.GetInt32(67)))
          LoadProperty(PrimarySkillDisciplineIDProperty, ZeroNothing(.GetInt32(68)))
          LoadProperty(SkillCountProperty, .GetInt32(69))
          LoadProperty(PrimarySkillCountProperty, .GetInt32(70))
          LoadProperty(MissingRateCountProperty, .GetInt32(71))
          LoadProperty(CurrentUserIsHRManagerProperty, .GetBoolean(72))
          LoadProperty(VendorNumberProperty, .GetString(73))
          LoadProperty(HumanResourceImageIDProperty, ZeroNothing(.GetInt32(74)))
          LoadProperty(PrimarySystemIDProperty, ZeroNothing(.GetInt32(75)))
          LoadProperty(PrimaryProductionAreaIDProperty, ZeroNothing(.GetInt32(76)))
          LoadProperty(HasMissingBasicsProperty, .GetBoolean(77))
          LoadProperty(PrimarySkillPositionIDProperty, ZeroNothing(.GetInt32(78)))

          LoadProperty(OrigPrimarySkillIDProperty, PrimarySkillID)
          LoadProperty(OrigPrimarySkillDisciplineIDProperty, PrimarySkillDisciplineID)
          LoadProperty(OrigPrimarySkillPositionIDProperty, PrimarySkillDisciplineID)
          LoadProperty(OrigPrimarySystemIDProperty, PrimarySystemID)
          LoadProperty(OrigPrimaryProductionAreaIDProperty, PrimaryProductionAreaID)
        End With
      End Using
      LoadProperty(DuplicateCountProperty, HumanResource.GetDuplicates(Me))
      LoadProperty(DuplicatesCheckedProperty, True)
      LoadProperty(IsCheckingDuplicatesProperty, False)
      LoadProperty(DuplicatesCheckedProperty, True)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
          paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
          If Me.IsNew Then
            paramHumanResourceID.Direction = ParameterDirection.Output
          End If
          Dim paramResourceID As SqlParameter = .Parameters.Add("@ResourceID", SqlDbType.Int)
          paramResourceID.Value = NothingDBNull(GetProperty(ResourceIDProperty))
          If Me.IsNew Then
            paramResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Firstname", GetProperty(FirstnameProperty))
          .Parameters.AddWithValue("@SecondName", GetProperty(SecondNameProperty))
          .Parameters.AddWithValue("@PreferredName", GetProperty(PreferredNameProperty))
          .Parameters.AddWithValue("@Surname", GetProperty(SurnameProperty))
          .Parameters.AddWithValue("@ContractTypeID", Singular.Misc.NothingDBNull(GetProperty(ContractTypeIDProperty)))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@AlternativeEmailAddress", GetProperty(AlternativeEmailAddressProperty))
          .Parameters.AddWithValue("@PhoneEmailAddress", GetProperty(PhoneEmailAddressProperty))
          .Parameters.AddWithValue("@CellPhoneNumber", GetProperty(CellPhoneNumberProperty))
          .Parameters.AddWithValue("@AlternativeContactNumber", GetProperty(AlternativeContactNumberProperty))
          .Parameters.AddWithValue("@IDNo", GetProperty(IDNoProperty))
          .Parameters.AddWithValue("@CityID", GetProperty(CityIDProperty))
          .Parameters.AddWithValue("@RaceID", GetProperty(RaceIDProperty))
          .Parameters.AddWithValue("@GenderID", GetProperty(GenderIDProperty))
          .Parameters.AddWithValue("@ActiveInd", GetProperty(ActiveIndProperty))
          .Parameters.AddWithValue("@InactiveReason", GetProperty(InactiveReasonProperty))
          .Parameters.AddWithValue("@ManagerHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(ManagerHumanResourceIDProperty)))
          .Parameters.AddWithValue("@EmployeeCode", GetProperty(EmployeeCodeProperty))
          .Parameters.AddWithValue("@ReportingColour", GetProperty(ReportingColourProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@PhotoPath", GetProperty(PhotoPathProperty))
          '.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@PantSize", GetProperty(PantSizeProperty))
          .Parameters.AddWithValue("@ShirtSize", GetProperty(ShirtSizeProperty))
          .Parameters.AddWithValue("@JacketSize", GetProperty(JacketSizeProperty))
          .Parameters.AddWithValue("@SAAVoyagerNo", GetProperty(SAAVoyagerNoProperty))
          .Parameters.AddWithValue("@SAAVoyagerTypeID", Singular.Misc.NothingDBNull(GetProperty(SAAVoyagerTypeIDProperty)))
          .Parameters.AddWithValue("@BAVoyagerNo", GetProperty(BAVoyagerNoProperty))
          .Parameters.AddWithValue("@BAVoyagerTypeID", Singular.Misc.NothingDBNull(GetProperty(BAVoyagerTypeIDProperty)))
          .Parameters.AddWithValue("@PayHalfMonthInd", GetProperty(PayHalfMonthIndProperty))
          .Parameters.AddWithValue("@SupplierID", Singular.Misc.NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@NationalityCountyID", Singular.Misc.NothingDBNull(GetProperty(NationalityCountyIDProperty)))
          .Parameters.AddWithValue("@ContractDays", GetProperty(ContractDaysProperty))
          .Parameters.AddWithValue("@AddressR1", GetProperty(AddressR1Property))
          .Parameters.AddWithValue("@AddressR2", GetProperty(AddressR2Property))
          .Parameters.AddWithValue("@AddressR3", GetProperty(AddressR3Property))
          .Parameters.AddWithValue("@AddressR4", GetProperty(AddressR4Property))
          .Parameters.AddWithValue("@PostalCodeR", GetProperty(PostalCodeRProperty))
          .Parameters.AddWithValue("@AddressP1", GetProperty(AddressP1Property))
          .Parameters.AddWithValue("@AddressP2", GetProperty(AddressP2Property))
          .Parameters.AddWithValue("@AddressP3", GetProperty(AddressP3Property))
          .Parameters.AddWithValue("@AddressP4", GetProperty(AddressP4Property))
          .Parameters.AddWithValue("@PostalCodeP", GetProperty(PostalCodePProperty))
          .Parameters.AddWithValue("@ManagerHumanResourceID2", Singular.Misc.NothingDBNull(GetProperty(ManagerHumanResourceID2Property)))
          .Parameters.AddWithValue("@TaxNumber", GetProperty(TaxNumberProperty))
          .Parameters.AddWithValue("@InvoiceSupplierInd", GetProperty(InvoiceSupplierIndProperty))
          .Parameters.AddWithValue("@LicenceInd", GetProperty(LicenceIndProperty))
          .Parameters.AddWithValue("@LicenceExpiryDate", NothingDBNull(GetProperty(LicenceExpiryDateProperty)))
          .Parameters.AddWithValue("@AccreditationImageID", NothingDBNull(GetProperty(AccreditationImageIDProperty)))
          .Parameters.AddWithValue("@PrimarySystemID", NothingDBNull(GetProperty(PrimarySystemIDProperty)))
          .Parameters.AddWithValue("@PrimaryProductionAreaID", NothingDBNull(GetProperty(PrimaryProductionAreaIDProperty)))
          .Parameters.AddWithValue("@PrimarySkillDisciplineID", NothingDBNull(GetProperty(PrimarySkillDisciplineIDProperty)))
          .Parameters.AddWithValue("@PrimarySkillPositionID", NothingDBNull(GetProperty(PrimarySkillPositionIDProperty)))
          .Parameters.AddWithValue("@IsManager", GetProperty(IsManagerProperty))
          .Parameters.AddWithValue("@IsIntern", GetProperty(IsInternProperty))
          .Parameters.AddWithValue("@IsOfficeWorker", GetProperty(IsOfficeWorkerProperty))
          .Parameters.AddWithValue("@VendorNumber", GetProperty(VendorNumberProperty))
          .Parameters.AddWithValue("@ContractStartDate", NothingDBNull(GetProperty(ContractStartDateProperty)))
          .Parameters.AddWithValue("@ContractEndDate", NothingDBNull(GetProperty(ContractEndDateProperty)))
          .Parameters.AddWithValue("@HumanResourceImageID", NothingDBNull(GetProperty(HumanResourceImageIDProperty)))
          .Parameters.AddWithValue("@OrigPrimarySkillDisciplineID", NothingDBNull(GetProperty(OrigPrimarySkillDisciplineIDProperty)))
          .Parameters.AddWithValue("@OrigPrimarySkillPositionID", NothingDBNull(GetProperty(OrigPrimarySkillPositionIDProperty)))
          .Parameters.AddWithValue("@OrigPrimarySystemID", NothingDBNull(GetProperty(OrigPrimarySystemIDProperty)))
          .Parameters.AddWithValue("@OrigPrimaryProductionAreaID", NothingDBNull(GetProperty(OrigPrimaryProductionAreaIDProperty)))
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
            LoadProperty(ResourceIDProperty, paramResourceID.Value)
          End If
          ' update child objects
          UpdateChildList()

          'If IsNew Then
          '  PopulateDuplicates()
          '  If GetProperty(HumanResourceDuplicateListProperty) IsNot Nothing Then
          '    Me.HumanResourceDuplicateList.Update()
          '  End If
          'End If
          UpdateChildList()

          MarkOld()
        End With
      Else
        ' update child objects
        UpdateChildList()

      End If

    End Sub

    Private Sub UpdateChildList()

      'If GetProperty(HumanResourceSkillListProperty) IsNot Nothing Then
      '  Me.HumanResourceSkillList.Update()
      'End If
      'If GetProperty(HumanResourceBankingDetailListProperty) IsNot Nothing Then
      '  Me.HumanResourceBankingDetailList.Update()
      'End If
      'If GetProperty(HumanResourceOffPeriodListProperty) IsNot Nothing Then
      '  Me.HumanResourceOffPeriodList.Update()
      'End If
      'If GetProperty(HumanResourceShiftPatternListProperty) IsNot Nothing Then
      '  Me.HumanResourceShiftPatternList.Update()
      'End If
      If GetProperty(HRSystemSelectListProperty) IsNot Nothing Then
        Me.HRSystemSelectList.Update()
      End If
      'If GetProperty(HRDocumentListProperty) IsNot Nothing Then
      '  Me.HRDocumentList.Update()
      'End If
      'If GetProperty(ProductionHumanResourcesTBCListProperty) IsNot Nothing Then
      '  Me.ProductionHumanResourcesTBCList.Update()
      'End If
      'If GetProperty(ManagerSubstituteListProperty) IsNot Nothing Then
      '  Me.ManagerSubstituteList.Update()
      'End If
      'If GetProperty(HumanResourceSecondmentListProperty) IsNot Nothing Then
      '  Me.HumanResourceSecondmentList.Update()
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      If Not Security.Settings.CurrentUser.SystemID = 1 Then
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "DelProcsWeb.delHumanResource"
          cm.CommandType = CommandType.StoredProcedure
          cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          DoDeleteChild(cm)
        End Using
      End If
    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub SetupAfterFetch()
      LoadProperty(PrimarySystemAreaCountProperty, Me.HRSystemSelectList.Where(Function(d) d.IsPrimary).Count)
    End Sub

  End Class

End Namespace