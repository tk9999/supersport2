﻿' Generated 20 Jan 2016 12:23 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.TeamManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly

Namespace HR

  <Serializable()> _
  Public Class HRSystemSelect
    Inherits OBBusinessBase(Of HRSystemSelect)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaID() As Integer
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DepartmentID, "Department")
    ''' <summary>
    ''' Gets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property DepartmentID() As Integer
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared DepartmentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Department, "Department")
    ''' <summary>
    ''' Gets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property Department() As String
      Get
        Return GetProperty(DepartmentProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDept, "Sub Dept")
    ''' <summary>
    ''' Gets the Sub Dept value
    ''' </summary>
    <Display(Name:="Sub Dept", Description:="")>
    Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area")
    ''' <summary>
    ''' Gets the Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared IsAddedProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.IsAdded, False) _
                                                                .AddSetExpression("HRSystemSelectBO.IsAddedSet(self)", False)
    ''' <summary>
    ''' Gets the Is Added value
    ''' </summary>
    <Display(Name:="Is Added", Description:="")>
    Public Property IsAdded() As Integer
      Get
        Return GetProperty(IsAddedProperty)
      End Get
      Set(value As Integer)
        SetProperty(IsAddedProperty, value)
      End Set
    End Property

    Public Shared IsPrimaryProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsPrimary, False) _
                                                                .AddSetExpression("HRSystemSelectBO.IsPrimarySet(self)", False)
    ''' <summary>
    ''' Gets the Is Primary value
    ''' </summary>
    <Display(Name:="Is Primary", Description:="")>
    Public Property IsPrimary() As Boolean
      Get
        Return GetProperty(IsPrimaryProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsPrimaryProperty, value)
      End Set
    End Property

    Public Shared HumanResourceSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSystemID, "Human Resource System")
    ''' <summary>
    ''' Gets the Human Resource System value
    ''' </summary>
    <Display(Name:="Human Resource System", Description:="")>
    Public Property HumanResourceSystemID() As Integer?
      Get
        Return GetProperty(HumanResourceSystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceSystemIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DepRnkProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.DepRnk, "Dep Rnk")
    ''' <summary>
    ''' Gets the Dep Rnk value
    ''' </summary>
    <Display(Name:="Dep Rnk", Description:="")>
    Public ReadOnly Property DepRnk() As Int64
      Get
        Return GetProperty(DepRnkProperty)
      End Get
    End Property

    Public Shared SysRnkProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.SysRnk, "Sys Rnk")
    ''' <summary>
    ''' Gets the Sys Rnk value
    ''' </summary>
    <Display(Name:="Sys Rnk", Description:="")>
    Public ReadOnly Property SysRnk() As Int64
      Get
        Return GetProperty(SysRnkProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    <Display(Name:="Creation Details")>
    Public ReadOnly Property CreateDetails() As String
      Get
        If CreatedBy <> 0 Then
          Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamNumberID, "Primary Team", Nothing)
    ''' <summary>
    ''' Gets the Human Resource System value
    ''' </summary>
    <Display(Name:="Primary Team", Description:=""),
    DropDownWeb(GetType(ROSystemTeamNumberList), ThisFilterMember:="SystemID")>
    Public Property SystemTeamNumberID() As Integer?
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemTeamNumberIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Department.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "HR System Select")
        Else
          Return String.Format("Blank {0}", "HR System Select")
        End If
      Else
        Return Me.Department
      End If

    End Function

    Public Function GetParent() As HumanResource

      Dim lst As HumanResourceSystemList = Me.Parent
      If lst IsNot Nothing Then
        Dim prnt As HumanResource = lst.Parent
        Return prnt
      End If
      Return Nothing

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHRSystemSelect() method.

    End Sub

    Public Shared Function NewHRSystemSelect() As HRSystemSelect

      Return DataPortal.CreateChild(Of HRSystemSelect)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHRSystemSelect(dr As SafeDataReader) As HRSystemSelect

      Dim h As New HRSystemSelect()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaIDProperty, .GetInt32(0))
          LoadProperty(DepartmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(DepartmentProperty, .GetString(4))
          LoadProperty(SubDeptProperty, .GetString(5))
          LoadProperty(AreaProperty, .GetString(6))
          LoadProperty(IsAddedProperty, .GetInt32(7))
          LoadProperty(IsPrimaryProperty, .GetBoolean(8))
          LoadProperty(HumanResourceSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(14))
          LoadProperty(CreatedByNameProperty, .GetString(15))
          '16 ModifiedByName
          LoadProperty(SystemTeamNumberIDProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(DepRnkProperty, .GetInt32(18))
          LoadProperty(SysRnkProperty, .GetInt32(19))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHRSystemSelect"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHRSystemSelect"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceSystemID As SqlParameter = .Parameters.Add("@HumanResourceSystemID", SqlDbType.Int)
          paramHumanResourceSystemID.Value = ZeroDBNull(GetProperty(HumanResourceSystemIDProperty))
          If Me.IsNew Then
            paramHumanResourceSystemID.Direction = ParameterDirection.Output
          End If
          '.Parameters.AddWithValue("@DepartmentID", GetProperty(DepartmentIDProperty))
          '.Parameters.AddWithValue("@Department", GetProperty(DepartmentProperty))
          '.Parameters.AddWithValue("@SubDept", GetProperty(SubDeptProperty))
          '.Parameters.AddWithValue("@Area", GetProperty(AreaProperty))
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@PrimaryInd", GetProperty(IsPrimaryProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@IsAdded", GetProperty(IsAddedProperty))
          .Parameters.AddWithValue("@SystemTeamNumberID", NothingDBNull(GetProperty(SystemTeamNumberIDProperty)))
          '.Parameters.AddWithValue("@DepRnk", GetProperty(DepRnkProperty))
          '.Parameters.AddWithValue("@SysRnk", GetProperty(SysRnkProperty))

          .ExecuteNonQuery()

          If Me.IsDirty Then
            If IsNullNothing(paramHumanResourceSystemID.Value) Then
              LoadProperty(HumanResourceSystemIDProperty, Nothing)
            Else
              LoadProperty(HumanResourceSystemIDProperty, paramHumanResourceSystemID.Value)
            End If
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHRSystemSelect"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace