﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR

  <Serializable()> _
  Public Class HumanResourceList
    Inherits OBBusinessListBase(Of HumanResourceList, HumanResource)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As HumanResource

      For Each child As HumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resources"

    End Function

    'Public Function GetHumanResourceSkill(HumanResourceSkillID As Integer) As HumanResourceSkill

    '  Dim obj As HumanResourceSkill = Nothing
    '  For Each parent As HumanResource In Me
    '    obj = parent.HumanResourceSkillList.GetItem(HumanResourceSkillID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetHumanResourceBankingDetail(HumanResourceBankingDetailID As Integer) As HumanResourceBankingDetail

    '  Dim obj As HumanResourceBankingDetail = Nothing
    '  For Each parent As HumanResource In Me
    '    obj = parent.HumanResourceBankingDetailList.GetItem(HumanResourceBankingDetailID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetHumanResourceOffPeriod(HumanResourceOffPeriodID As Integer) As HumanResourceOffPeriod

    '  Dim obj As HumanResourceOffPeriod = Nothing
    '  For Each parent As HumanResource In Me
    '    obj = parent.HumanResourceOffPeriodList.GetItem(HumanResourceOffPeriodID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetHumanResourceShiftPattern(HumanResourceShiftPatternID As Integer) As HumanResourceShiftPattern

    '  Dim obj As HumanResourceShiftPattern = Nothing
    '  For Each parent As HumanResource In Me
    '    obj = parent.HumanResourceShiftPatternList.GetItem(HumanResourceShiftPatternID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetHumanResourceSystem(HumanResourceSystemID As Integer) As HumanResourceSystem

    '  Dim obj As HumanResourceSystem = Nothing
    '  For Each parent As HumanResource In Me
    '    obj = parent.HumanResourceSystemList.GetItem(HumanResourceSystemID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetHRDocument(HRDocumentID As Integer) As HRDocument

    '  Dim obj As HRDocument = Nothing
    '  For Each parent As HumanResource In Me
    '    obj = parent.HRDocumentList.GetItem(HRDocumentID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    'Public Function GetHumanResourceSecondment(HumanResourceSecondmentID As Integer) As HumanResourceSecondment
    '  Dim obj As HumanResourceSecondment = Nothing
    '  For Each Parent As HumanResource In Me
    '    obj = Parent.HumanResourceSecondmentList.GetItem(HumanResourceSecondmentID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing
    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(HumanResourceID As Integer?)
        Me.HumanResourceID = HumanResourceID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHumanResourceList() As HumanResourceList

      Return New HumanResourceList()

    End Function

    Public Shared Sub BeginGetHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of HumanResourceList)))

      Dim dp As New DataPortal(Of HumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHumanResourceList(HumanResourceID As Integer?) As HumanResourceList

      Return DataPortal.Fetch(Of HumanResourceList)(New Criteria(HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResource.GetHumanResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As HumanResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(10) Then
            parent = Me.GetItem(sdr.GetInt32(10))
          End If
          parent.HRSystemSelectList.RaiseListChangedEvents = False
          parent.HRSystemSelectList.Add(HRSystemSelect.GetHRSystemSelect(sdr))
          parent.HRSystemSelectList.RaiseListChangedEvents = True
        End While
      End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.HumanResourceSkillList.RaiseListChangedEvents = False
      '    parent.HumanResourceSkillList.Add(HumanResourceSkill.GetHumanResourceSkill(sdr))
      '    parent.HumanResourceSkillList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As HumanResourceSkill = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.HumanResourceSkillID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetHumanResourceSkill(sdr.GetInt32(1))
      '    End If
      '    parentChild.HumanResourceSkillPositionList.RaiseListChangedEvents = False
      '    parentChild.HumanResourceSkillPositionList.Add(HumanResourceSkillPosition.GetHumanResourceSkillPosition(sdr))
      '    parentChild.HumanResourceSkillPositionList.RaiseListChangedEvents = True
      '  End While
      'End If
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.HumanResourceSkillID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetHumanResourceSkill(sdr.GetInt32(1))
      '    End If
      '    parentChild.HumanResourceSkillRateList.RaiseListChangedEvents = False
      '    parentChild.HumanResourceSkillRateList.Add(HumanResourceSkillRate.GetHumanResourceSkillRate(sdr))
      '    parentChild.HumanResourceSkillRateList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.HumanResourceBankingDetailList.RaiseListChangedEvents = False
      '    parent.HumanResourceBankingDetailList.Add(HumanResourceBankingDetail.GetHumanResourceBankingDetail(sdr))
      '    parent.HumanResourceBankingDetailList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.HumanResourceOffPeriodList.RaiseListChangedEvents = False
      '    parent.HumanResourceOffPeriodList.Add(HumanResourceOffPeriod.GetHumanResourceOffPeriod(sdr))
      '    parent.HumanResourceOffPeriodList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    'parent.HumanResourceShiftPatternList.RaiseListChangedEvents = False
      '    'parent.HumanResourceShiftPatternList.Add(HumanResourceShiftPattern.GetHumanResourceShiftPattern(sdr))
      '    'parent.HumanResourceShiftPatternList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(4) Then
      '      parent = Me.GetItem(sdr.GetInt32(4))
      '    End If
      '    parent.HRDocumentList.RaiseListChangedEvents = False
      '    parent.HRDocumentList.Add(HRDocument.GetHRDocument(sdr))
      '    parent.HRDocumentList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ProductionHumanResourcesTBCList.RaiseListChangedEvents = False
      '    parent.ProductionHumanResourcesTBCList.Add(ProductionHumanResourcesTBC.GetProductionHumanResourcesTBC(sdr))
      '    parent.ProductionHumanResourcesTBCList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ManagerSubstituteList.RaiseListChangedEvents = False
      '    parent.ManagerSubstituteList.Add(ManagerSubstitute.GetManagerSubstitute(sdr))
      '    parent.ManagerSubstituteList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.HumanResourceSecondmentList.RaiseListChangedEvents = False
      '    parent.HumanResourceSecondmentList.Add(HumanResourceSecondment.GetHumanResourceSecondment(sdr))
      '    parent.HumanResourceSecondmentList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(10) Then
      '      parent = Me.GetItem(sdr.GetInt32(10))
      '    End If
      '    parent.ROHRDuplicateList.RaiseListChangedEvents = False
      '    parent.ROHRDuplicateList.Add(ROHRDuplicate.GetROHRDuplicate(sdr))
      '    parent.ROHRDuplicateList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As HumanResource In Me
        child.SetupAfterFetch()
        child.CheckRules()
        For Each HRSystemSelect As HRSystemSelect In child.HRSystemSelectList
          HRSystemSelect.CheckRules()
        Next
        'For Each HumanResourceSkill As HumanResourceSkill In child.HumanResourceSkillList
        '  HumanResourceSkill.CheckRules()

        '  For Each HumanResourceSkillPosition As HumanResourceSkillPosition In HumanResourceSkill.HumanResourceSkillPositionList
        '    HumanResourceSkillPosition.CheckRules()
        '  Next
        '  For Each HumanResourceSkillRate As HumanResourceSkillRate In HumanResourceSkill.HumanResourceSkillRateList
        '    HumanResourceSkillRate.CheckRules()
        '  Next
        'Next
        'For Each HumanResourceBankingDetail As HumanResourceBankingDetail In child.HumanResourceBankingDetailList
        '  HumanResourceBankingDetail.CheckRules()
        'Next
        'For Each HumanResourceOffPeriod As HumanResourceOffPeriod In child.HumanResourceOffPeriodList
        '  HumanResourceOffPeriod.CheckRules()
        'Next
        'For Each HumanResourceShiftPattern As HumanResourceShiftPattern In child.HumanResourceShiftPatternList
        '  HumanResourceShiftPattern.CheckRules()
        'Next
        'For Each HRDocument As HRDocument In child.HRDocumentList
        '  HRDocument.CheckRules()
        'Next
        'For Each ManagerSubstitute As ManagerSubstitute In child.ManagerSubstituteList
        '  ManagerSubstitute.CheckRules()
        'Next
        'For Each HumanResourceSecondment As HumanResourceSecondment In child.HumanResourceSecondmentList
        '  HumanResourceSecondment.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@LoggedInHumanResourceID", NothingDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace