﻿' Generated 08 Sep 2015 10:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceAccommodation
    Inherits SingularReadOnlyBase(Of ROResourceAccommodation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AccommodationHumanResourceID() As Integer
      Get
        Return GetProperty(AccommodationHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
  Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
  Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared AccommodationProviderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationProvider, "Accommodation Provider")
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:="")>
  Public ReadOnly Property AccommodationProvider() As String
      Get
        Return GetProperty(AccommodationProviderProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
  Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared CheckInDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckInDate, "Check In Date")
    ''' <summary>
    ''' Gets the Check In Date value
    ''' </summary>
    <Display(Name:="Check In Date", Description:="")>
  Public ReadOnly Property CheckInDate As DateTime?
      Get
        Return GetProperty(CheckInDateProperty)
      End Get
    End Property

    Public Shared CheckOutDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckOutDate, "Check Out Date")
    ''' <summary>
    ''' Gets the Check Out Date value
    ''' </summary>
    <Display(Name:="Check Out Date", Description:="")>
  Public ReadOnly Property CheckOutDate As DateTime?
      Get
        Return GetProperty(CheckOutDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Comments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceAccommodation(dr As SafeDataReader) As ROResourceAccommodation

      Dim r As New ROResourceAccommodation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccommodationHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(CommentsProperty, .GetString(2))
        LoadProperty(DescriptionProperty, .GetString(3))
        LoadProperty(AccommodationProviderProperty, .GetString(4))
        LoadProperty(CityProperty, .GetString(5))
        LoadProperty(CheckInDateProperty, .GetValue(6))
        LoadProperty(CheckOutDateProperty, .GetValue(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace