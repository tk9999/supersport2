﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceBookingHolder
    Inherits SingularReadOnlyBase(Of ROResourceBookingHolder)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name")
    ''' <summary>
    ''' Gets the Resource Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:="")>
  Public ReadOnly Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeID, "Resource Type")
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
  Public ReadOnly Property ResourceTypeID() As Integer
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle")
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
  Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "Equipment")
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
  Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "Channel")
    ''' <summary>
    ''' Gets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
  Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CustomResourceID, "Custom Resource")
    ''' <summary>
    ''' Gets the Custom Resource value
    ''' </summary>
    <Display(Name:="Custom Resource", Description:="")>
  Public ReadOnly Property CustomResourceID() As Integer
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROResourceScheduleListProperty As PropertyInfo(Of ROResourceScheduleList) = RegisterProperty(Of ROResourceScheduleList)(Function(c) c.ROResourceScheduleList, "RO Resource Schedule List")

    Public ReadOnly Property ROResourceScheduleList() As ROResourceScheduleList
      Get
        If GetProperty(ROResourceScheduleListProperty) Is Nothing Then
          LoadProperty(ROResourceScheduleListProperty, HR.Schedule.ROResourceScheduleList.NewROResourceScheduleList())
        End If
        Return GetProperty(ROResourceScheduleListProperty)
      End Get
    End Property

    Public Shared ROResourceTravelDayListProperty As PropertyInfo(Of ROResourceTravelDayList) = RegisterProperty(Of ROResourceTravelDayList)(Function(c) c.ROResourceTravelDayList, "RO Resource Travel Day List")

    Public ReadOnly Property ROResourceTravelDayList() As ROResourceTravelDayList
      Get
        If GetProperty(ROResourceTravelDayListProperty) Is Nothing Then
          LoadProperty(ROResourceTravelDayListProperty, HR.Schedule.ROResourceTravelDayList.NewROResourceTravelDayList())
        End If
        Return GetProperty(ROResourceTravelDayListProperty)
      End Get
    End Property

    Public Shared ROResourceFlightListProperty As PropertyInfo(Of ROResourceFlightList) = RegisterProperty(Of ROResourceFlightList)(Function(c) c.ROResourceFlightList, "RO Resource Flight List")

    Public ReadOnly Property ROResourceFlightList() As ROResourceFlightList
      Get
        If GetProperty(ROResourceFlightListProperty) Is Nothing Then
          LoadProperty(ROResourceFlightListProperty, HR.Schedule.ROResourceFlightList.NewROResourceFlightList())
        End If
        Return GetProperty(ROResourceFlightListProperty)
      End Get
    End Property

    Public Shared ROResourceAccommodationListProperty As PropertyInfo(Of ROResourceAccommodationList) = RegisterProperty(Of ROResourceAccommodationList)(Function(c) c.ROResourceAccommodationList, "RO Resource Accommodation List")

    Public ReadOnly Property ROResourceAccommodationList() As ROResourceAccommodationList
      Get
        If GetProperty(ROResourceAccommodationListProperty) Is Nothing Then
          LoadProperty(ROResourceAccommodationListProperty, HR.Schedule.ROResourceAccommodationList.NewROResourceAccommodationList())
        End If
        Return GetProperty(ROResourceAccommodationListProperty)
      End Get
    End Property

    Public Shared ROResourceRentalCarListProperty As PropertyInfo(Of ROResourceRentalCarList) = RegisterProperty(Of ROResourceRentalCarList)(Function(c) c.ROResourceRentalCarList, "RO Resource Rental Car List")

    Public ReadOnly Property ROResourceRentalCarList() As ROResourceRentalCarList
      Get
        If GetProperty(ROResourceRentalCarListProperty) Is Nothing Then
          LoadProperty(ROResourceRentalCarListProperty, HR.Schedule.ROResourceRentalCarList.NewROResourceRentalCarList())
        End If
        Return GetProperty(ROResourceRentalCarListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceName

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceBookingHolder(dr As SafeDataReader) As ROResourceBookingHolder

      Dim r As New ROResourceBookingHolder()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceNameProperty, .GetString(1))
        LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
        LoadProperty(CustomResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace