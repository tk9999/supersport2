﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceScheduleDetail
    Inherits SingularReadOnlyBase(Of ROResourceScheduleDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared HumanResourceScheduleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceScheduleType, "Human Resource Schedule Type")
    ''' <summary>
    ''' Gets the Human Resource Schedule Type value
    ''' </summary>
    <Display(Name:="Human Resource Schedule Type", Description:="")>
  Public ReadOnly Property HumanResourceScheduleType() As String
      Get
        Return GetProperty(HumanResourceScheduleTypeProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
  Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Production Ref No", Description:="")>
  Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="")>
  Public ReadOnly Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared AdHocBookingTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingTitle, "Ad Hoc Booking Title")
    ''' <summary>
    ''' Gets the Ad Hoc Booking Title value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Title", Description:="")>
  Public ReadOnly Property AdHocBookingTitle() As String
      Get
        Return GetProperty(AdHocBookingTitleProperty)
      End Get
    End Property

    Public Shared AdHocBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingDescription, "Ad Hoc Booking Description")
    ''' <summary>
    ''' Gets the Ad Hoc Booking Description value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Description", Description:="")>
  Public ReadOnly Property AdHocBookingDescription() As String
      Get
        Return GetProperty(AdHocBookingDescriptionProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingType, "Ad Hoc Booking Type")
    ''' <summary>
    ''' Gets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Type", Description:="")>
  Public ReadOnly Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffReason, "Off Reason")
    ''' <summary>
    ''' Gets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="")>
  Public ReadOnly Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
    End Property

    Public Shared LeaveStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LeaveStatus, "Leave Status")
    ''' <summary>
    ''' Gets the Leave Status value
    ''' </summary>
    <Display(Name:="Leave Status", Description:="")>
  Public ReadOnly Property LeaveStatus() As String
      Get
        Return GetProperty(LeaveStatusProperty)
      End Get
    End Property

    Public Shared LeaveDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LeaveDetail, "Leave Detail")
    ''' <summary>
    ''' Gets the Leave Detail value
    ''' </summary>
    <Display(Name:="Leave Detail", Description:="")>
  Public ReadOnly Property LeaveDetail() As String
      Get
        Return GetProperty(LeaveDetailProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
  Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResourceScheduleType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceScheduleDetail(dr As SafeDataReader) As ROResourceScheduleDetail

      Dim r As New ROResourceScheduleDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceScheduleTypeProperty, .GetString(1))
        LoadProperty(DisciplineProperty, .GetString(2))
        LoadProperty(ProductionDescriptionProperty, .GetString(3))
        LoadProperty(ProductionRefNoProperty, .GetString(4))
        LoadProperty(ProductionTimelineTypeProperty, .GetString(5))
        LoadProperty(StartDateTimeProperty, .GetValue(6))
        LoadProperty(EndDateTimeProperty, .GetValue(7))
        LoadProperty(AdHocBookingTitleProperty, .GetString(8))
        LoadProperty(AdHocBookingDescriptionProperty, .GetString(9))
        LoadProperty(AdHocBookingTypeProperty, .GetString(10))
        LoadProperty(OffReasonProperty, .GetString(11))
        LoadProperty(LeaveStatusProperty, .GetString(12))
        LoadProperty(LeaveDetailProperty, .GetString(13))
        LoadProperty(ShiftTypeProperty, .GetString(14))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace