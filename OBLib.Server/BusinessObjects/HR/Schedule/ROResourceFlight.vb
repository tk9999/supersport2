﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceFlight
    Inherits SingularReadOnlyBase(Of ROResourceFlight)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FlightHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FlightHumanResourceID() As Integer
      Get
        Return GetProperty(FlightHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared FlightClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightClass, "Flight Class")
    ''' <summary>
    ''' Gets the Flight Class value
    ''' </summary>
    <Display(Name:="Flight Class", Description:="")>
  Public ReadOnly Property FlightClass() As String
      Get
        Return GetProperty(FlightClassProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
  Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared TravelDirectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelDirection, "Travel Direction")
    ''' <summary>
    ''' Gets the Travel Direction value
    ''' </summary>
    <Display(Name:="Travel Direction", Description:="")>
  Public ReadOnly Property TravelDirection() As String
      Get
        Return GetProperty(TravelDirectionProperty)
      End Get
    End Property

    Public Shared AirportFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AirportFrom, "Airport From")
    ''' <summary>
    ''' Gets the Airport From value
    ''' </summary>
    <Display(Name:="Airport From", Description:="")>
  Public ReadOnly Property AirportFrom() As String
      Get
        Return GetProperty(AirportFromProperty)
      End Get
    End Property

    Public Shared FromCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromCity, "From City")
    ''' <summary>
    ''' Gets the From City value
    ''' </summary>
    <Display(Name:="From City", Description:="")>
  Public ReadOnly Property FromCity() As String
      Get
        Return GetProperty(FromCityProperty)
      End Get
    End Property

    Public Shared DepartureDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DepartureDateTime, "Departure Date Time")
    ''' <summary>
    ''' Gets the Departure Date Time value
    ''' </summary>
    <Display(Name:="Departure Date Time", Description:="")>
  Public ReadOnly Property DepartureDateTime As DateTime?
      Get
        Return GetProperty(DepartureDateTimeProperty)
      End Get
    End Property

    Public Shared DepartureDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DepartureDate, "Departure Date")
    ''' <summary>
    ''' Gets the Departure Date value
    ''' </summary>
    <Display(Name:="Departure Date", Description:="")>
  Public ReadOnly Property DepartureDate() As String
      Get
        Return GetProperty(DepartureDateProperty)
      End Get
    End Property

    Public Shared DepartureTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DepartureTime, "Departure Time")
    ''' <summary>
    ''' Gets the Departure Time value
    ''' </summary>
    <Display(Name:="Departure Time", Description:="")>
  Public ReadOnly Property DepartureTime() As String
      Get
        Return GetProperty(DepartureTimeProperty)
      End Get
    End Property

    Public Shared AirportToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AirportTo, "Airport To")
    ''' <summary>
    ''' Gets the Airport To value
    ''' </summary>
    <Display(Name:="Airport To", Description:="")>
  Public ReadOnly Property AirportTo() As String
      Get
        Return GetProperty(AirportToProperty)
      End Get
    End Property

    Public Shared ToCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ToCity, "To City")
    ''' <summary>
    ''' Gets the To City value
    ''' </summary>
    <Display(Name:="To City", Description:="")>
  Public ReadOnly Property ToCity() As String
      Get
        Return GetProperty(ToCityProperty)
      End Get
    End Property

    Public Shared ArrivalDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ArrivalDateTime, "Arrival Date Time")
    ''' <summary>
    ''' Gets the Arrival Date Time value
    ''' </summary>
    <Display(Name:="Arrival Date Time", Description:="")>
  Public ReadOnly Property ArrivalDateTime As DateTime?
      Get
        Return GetProperty(ArrivalDateTimeProperty)
      End Get
    End Property

    Public Shared ArrivalDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ArrivalDate, "Arrival Date")
    ''' <summary>
    ''' Gets the Arrival Date value
    ''' </summary>
    <Display(Name:="Arrival Date", Description:="")>
  Public ReadOnly Property ArrivalDate() As String
      Get
        Return GetProperty(ArrivalDateProperty)
      End Get
    End Property

    Public Shared ArrivalTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ArrivalTime, "Arrival Time")
    ''' <summary>
    ''' Gets the Arrival Time value
    ''' </summary>
    <Display(Name:="Arrival Time", Description:="")>
  Public ReadOnly Property ArrivalTime() As String
      Get
        Return GetProperty(ArrivalTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FlightClass

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceFlight(dr As SafeDataReader) As ROResourceFlight

      Dim r As New ROResourceFlight()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FlightHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FlightClassProperty, .GetString(2))
        LoadProperty(CommentsProperty, .GetString(3))
        LoadProperty(TravelDirectionProperty, .GetString(4))
        LoadProperty(AirportFromProperty, .GetString(5))
        LoadProperty(FromCityProperty, .GetString(6))
        LoadProperty(DepartureDateTimeProperty, .GetValue(7))
        LoadProperty(DepartureDateProperty, .GetString(8))
        LoadProperty(DepartureTimeProperty, .GetString(9))
        LoadProperty(AirportToProperty, .GetString(10))
        LoadProperty(ToCityProperty, .GetString(11))
        LoadProperty(ArrivalDateTimeProperty, .GetValue(12))
        LoadProperty(ArrivalDateProperty, .GetString(13))
        LoadProperty(ArrivalTimeProperty, .GetString(14))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace