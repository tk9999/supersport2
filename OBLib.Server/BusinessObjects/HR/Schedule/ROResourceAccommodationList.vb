﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceAccommodationList
    Inherits SingularReadOnlyListBase(Of ROResourceAccommodationList, ROResourceAccommodation)

#Region " Parent "

    <NotUndoable()> Private mParent As ROResourceBookingHolder
#End Region

#Region " Business Methods "

    Public Function GetItem(AccommodationHumanResourceID As Integer) As ROResourceAccommodation

      For Each child As ROResourceAccommodation In Me
        If child.AccommodationHumanResourceID = AccommodationHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROResourceAccommodationList() As ROResourceAccommodationList

      Return New ROResourceAccommodationList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace