﻿' Generated 08 Sep 2015 10:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceRentalCar
    Inherits SingularReadOnlyBase(Of ROResourceRentalCar)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RentalCarHumanResourceID() As Integer
      Get
        Return GetProperty(RentalCarHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared DriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverInd, "Driver", False)
    ''' <summary>
    ''' Gets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
  Public ReadOnly Property DriverInd() As Boolean
      Get
        Return GetProperty(DriverIndProperty)
      End Get
    End Property

    Public Shared CoDriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoDriverInd, "Co Driver", False)
    ''' <summary>
    ''' Gets the Co Driver value
    ''' </summary>
    <Display(Name:="Co Driver", Description:="")>
  Public ReadOnly Property CoDriverInd() As Boolean
      Get
        Return GetProperty(CoDriverIndProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
  Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgent, "Rental Car Agent")
    ''' <summary>
    ''' Gets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Rental Car Agent", Description:="")>
  Public ReadOnly Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
    End Property

    Public Shared CarTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CarType, "Car Type")
    ''' <summary>
    ''' Gets the Car Type value
    ''' </summary>
    <Display(Name:="Car Type", Description:="")>
  Public ReadOnly Property CarType() As String
      Get
        Return GetProperty(CarTypeProperty)
      End Get
    End Property

    Public Shared FromBranchProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromBranch, "From Branch")
    ''' <summary>
    ''' Gets the From Branch value
    ''' </summary>
    <Display(Name:="From Branch", Description:="")>
  Public ReadOnly Property FromBranch() As String
      Get
        Return GetProperty(FromBranchProperty)
      End Get
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PickupDateTime, "Pickup Date Time")
    ''' <summary>
    ''' Gets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="Pickup Date Time", Description:="")>
  Public ReadOnly Property PickupDateTime As DateTime?
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
    End Property

    Public Shared PickupDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PickupDate, "Pickup Date")
    ''' <summary>
    ''' Gets the Pickup Date value
    ''' </summary>
    <Display(Name:="Pickup Date", Description:="")>
  Public ReadOnly Property PickupDate() As String
      Get
        Return GetProperty(PickupDateProperty)
      End Get
    End Property

    Public Shared PickupTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PickupTime, "Pickup Time")
    ''' <summary>
    ''' Gets the Pickup Time value
    ''' </summary>
    <Display(Name:="Pickup Time", Description:="")>
  Public ReadOnly Property PickupTime() As String
      Get
        Return GetProperty(PickupTimeProperty)
      End Get
    End Property

    Public Shared ToBranchProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ToBranch, "To Branch")
    ''' <summary>
    ''' Gets the To Branch value
    ''' </summary>
    <Display(Name:="To Branch", Description:="")>
  Public ReadOnly Property ToBranch() As String
      Get
        Return GetProperty(ToBranchProperty)
      End Get
    End Property

    Public Shared DropOffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DropOffDateTime, "Drop Off Date Time")
    ''' <summary>
    ''' Gets the Drop Off Date Time value
    ''' </summary>
    <Display(Name:="Drop Off Date Time", Description:="")>
  Public ReadOnly Property DropOffDateTime As DateTime?
      Get
        Return GetProperty(DropOffDateTimeProperty)
      End Get
    End Property

    Public Shared DropOffDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DropOffDate, "Drop Off Date")
    ''' <summary>
    ''' Gets the Drop Off Date value
    ''' </summary>
    <Display(Name:="Drop Off Date", Description:="")>
  Public ReadOnly Property DropOffDate() As String
      Get
        Return GetProperty(DropOffDateProperty)
      End Get
    End Property

    Public Shared DropOffTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DropOffTime, "Drop Off Time")
    ''' <summary>
    ''' Gets the Drop Off Time value
    ''' </summary>
    <Display(Name:="Drop Off Time", Description:="")>
  Public ReadOnly Property DropOffTime() As String
      Get
        Return GetProperty(DropOffTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Comments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceRentalCar(dr As SafeDataReader) As ROResourceRentalCar

      Dim r As New ROResourceRentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DriverIndProperty, .GetBoolean(2))
        LoadProperty(CoDriverIndProperty, .GetBoolean(3))
        LoadProperty(CommentsProperty, .GetString(4))
        LoadProperty(RentalCarAgentProperty, .GetString(5))
        LoadProperty(CarTypeProperty, .GetString(6))
        LoadProperty(FromBranchProperty, .GetString(7))
        LoadProperty(PickupDateTimeProperty, .GetValue(8))
        LoadProperty(PickupDateProperty, .GetString(9))
        LoadProperty(PickupTimeProperty, .GetString(10))
        LoadProperty(ToBranchProperty, .GetString(11))
        LoadProperty(DropOffDateTimeProperty, .GetValue(12))
        LoadProperty(DropOffDateProperty, .GetString(13))
        LoadProperty(DropOffTimeProperty, .GetString(14))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace