﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceScheduleList
    Inherits SingularReadOnlyListBase(Of ROResourceScheduleList, ROResourceSchedule)

#Region " Parent "

    <NotUndoable()> Private mParent As ROResourceBookingHolder
#End Region

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As ROResourceSchedule

      For Each child As ROResourceSchedule In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROResourceScheduleDetail(ResourceBookingID As Integer) As ROResourceScheduleDetail

      Dim obj As ROResourceScheduleDetail = Nothing
      For Each parent As ROResourceSchedule In Me
        obj = parent.ROResourceScheduleDetailList.GetItem(ResourceBookingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROResourceScheduleList() As ROResourceScheduleList

      Return New ROResourceScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace