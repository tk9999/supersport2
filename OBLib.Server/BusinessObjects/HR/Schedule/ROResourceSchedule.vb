﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceSchedule
    Inherits SingularReadOnlyBase(Of ROResourceSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingType, "Resource Booking Type")
    ''' <summary>
    ''' Gets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:="")>
  Public ReadOnly Property ResourceBookingType() As String
      Get
        Return GetProperty(ResourceBookingTypeProperty)
      End Get
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ''' <summary>
    ''' Gets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:="")>
  Public ReadOnly Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
  Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
  Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROResourceScheduleDetailListProperty As PropertyInfo(Of ROResourceScheduleDetailList) = RegisterProperty(Of ROResourceScheduleDetailList)(Function(c) c.ROResourceScheduleDetailList, "RO Resource Schedule Detail List")

    Public ReadOnly Property ROResourceScheduleDetailList() As ROResourceScheduleDetailList
      Get
        If GetProperty(ROResourceScheduleDetailListProperty) Is Nothing Then
          LoadProperty(ROResourceScheduleDetailListProperty, HR.Schedule.ROResourceScheduleDetailList.NewROResourceScheduleDetailList())
        End If
        Return GetProperty(ROResourceScheduleDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceBookingType

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceSchedule(dr As SafeDataReader) As ROResourceSchedule

      Dim r As New ROResourceSchedule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceBookingTypeProperty, .GetString(2))
        LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
        LoadProperty(StartDateTimeProperty, .GetValue(5))
        LoadProperty(EndDateTimeProperty, .GetValue(6))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace