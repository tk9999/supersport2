﻿' Generated 08 Sep 2015 10:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceBookingHolderList
    Inherits SingularReadOnlyListBase(Of ROResourceBookingHolderList, ROResourceBookingHolder)

#Region " Business Methods "

    Public Function GetItem(ResourceID As Integer) As ROResourceBookingHolder

      For Each child As ROResourceBookingHolder In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROResourceSchedule(ResourceBookingID As Integer) As ROResourceSchedule

      Dim obj As ROResourceSchedule = Nothing
      For Each parent As ROResourceBookingHolder In Me
        obj = parent.ROResourceScheduleList.GetItem(ResourceBookingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROResourceTravelDay(ResourceID As Integer) As ROResourceTravelDay

      Dim obj As ROResourceTravelDay = Nothing
      For Each parent As ROResourceBookingHolder In Me
        obj = parent.ROResourceTravelDayList.GetItem(ResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROResourceFlight(FlightHumanResourceID As Integer) As ROResourceFlight

      Dim obj As ROResourceFlight = Nothing
      For Each parent As ROResourceBookingHolder In Me
        obj = parent.ROResourceFlightList.GetItem(FlightHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROResourceAccommodation(AccommodationHumanResourceID As Integer) As ROResourceAccommodation

      Dim obj As ROResourceAccommodation = Nothing
      For Each parent As ROResourceBookingHolder In Me
        obj = parent.ROResourceAccommodationList.GetItem(AccommodationHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROResourceRentalCar(RentalCarHumanResourceID As Integer) As ROResourceRentalCar

      Dim obj As ROResourceRentalCar = Nothing
      For Each parent As ROResourceBookingHolder In Me
        obj = parent.ROResourceRentalCarList.GetItem(RentalCarHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Required(ErrorMessage:="Start Date required")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Required(ErrorMessage:="End Date required")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
      ''' <summary>
      ''' Gets and sets the Off Reason value
      ''' </summary>
      Public Property ResourceID() As Integer?
        Get
          Return ReadProperty(ResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ResourceIDProperty, Value)
        End Set
      End Property

      Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "UserID", Nothing)
      ''' <summary>
      ''' Gets and sets the Off Reason value
      ''' </summary>
      Public Property UserID() As Integer?
        Get
          Return ReadProperty(UserIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(UserIDProperty, Value)
        End Set
      End Property

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?, ResourceID As Integer?, UserID As Integer?)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ResourceID = ResourceID
        Me.UserID = UserID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROResourceBookingHolderList() As ROResourceBookingHolderList

      Return New ROResourceBookingHolderList()

    End Function

    Public Shared Sub BeginGetROResourceBookingHolderList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROResourceBookingHolderList)))

      Dim dp As New DataPortal(Of ROResourceBookingHolderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROResourceBookingHolderList(CallBack As EventHandler(Of DataPortalResult(Of ROResourceBookingHolderList)))

      Dim dp As New DataPortal(Of ROResourceBookingHolderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROResourceBookingHolderList() As ROResourceBookingHolderList

      Return DataPortal.Fetch(Of ROResourceBookingHolderList)(New Criteria())

    End Function

    Public Shared Function GetROResourceBookingHolderList(ResourceID As Integer?, UserID As Integer?, StartDate As DateTime?, EndDate As DateTime?) As ROResourceBookingHolderList

      Return DataPortal.Fetch(Of ROResourceBookingHolderList)(New Criteria(StartDate, EndDate, ResourceID, UserID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROResourceBookingHolder.GetROResourceBookingHolder(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROResourceBookingHolder = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROResourceScheduleList.RaiseListChangedEvents = False
          parent.ROResourceScheduleList.Add(ROResourceSchedule.GetROResourceSchedule(sdr))
          parent.ROResourceScheduleList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ROResourceSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.ResourceBookingID <> sdr.GetInt32(0) Then
            parentChild = Me.GetROResourceSchedule(sdr.GetInt32(0))
          End If
          parentChild.ROResourceScheduleDetailList.RaiseListChangedEvents = False
          parentChild.ROResourceScheduleDetailList.Add(ROResourceScheduleDetail.GetROResourceScheduleDetail(sdr))
          parentChild.ROResourceScheduleDetailList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ROResourceTravelDayList.RaiseListChangedEvents = False
          parent.ROResourceTravelDayList.Add(ROResourceTravelDay.GetROResourceTravelDay(sdr))
          parent.ROResourceTravelDayList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROResourceFlightList.RaiseListChangedEvents = False
          parent.ROResourceFlightList.Add(ROResourceFlight.GetROResourceFlight(sdr))
          parent.ROResourceFlightList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROResourceAccommodationList.RaiseListChangedEvents = False
          parent.ROResourceAccommodationList.Add(ROResourceAccommodation.GetROResourceAccommodation(sdr))
          parent.ROResourceAccommodationList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROResourceRentalCarList.RaiseListChangedEvents = False
          parent.ROResourceRentalCarList.Add(ROResourceRentalCar.GetROResourceRentalCar(sdr))
          parent.ROResourceRentalCarList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROResourceBookingHolderList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(crit.ResourceID))
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace