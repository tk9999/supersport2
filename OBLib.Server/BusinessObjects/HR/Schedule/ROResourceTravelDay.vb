﻿' Generated 08 Sep 2015 10:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.Schedule

  <Serializable()> _
  Public Class ROResourceTravelDay
    Inherits SingularReadOnlyBase(Of ROResourceTravelDay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared DateOfTravelProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DateOfTravel, "Date")
    ''' <summary>
    ''' Gets the Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public ReadOnly Property DateOfTravel As DateTime?
      Get
        Return GetProperty(DateOfTravelProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared TotalFlightsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalFlights, "Total Flights")
    ''' <summary>
    ''' Gets the Total Flights value
    ''' </summary>
    <Display(Name:="Total Flights", Description:="")>
    Public ReadOnly Property TotalFlights() As Integer
      Get
        Return GetProperty(TotalFlightsProperty)
      End Get
    End Property

    Public Shared TotalAccommodationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalAccommodation, "Total Accommodation")
    ''' <summary>
    ''' Gets the Total Accommodation value
    ''' </summary>
    <Display(Name:="Total Accommodation", Description:="")>
    Public ReadOnly Property TotalAccommodation() As Integer
      Get
        Return GetProperty(TotalAccommodationProperty)
      End Get
    End Property

    Public Shared TotalCarsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalCars, "Total Cars")
    ''' <summary>
    ''' Gets the Total Cars value
    ''' </summary>
    <Display(Name:="Total Cars", Description:="")>
    Public ReadOnly Property TotalCars() As Integer
      Get
        Return GetProperty(TotalCarsProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceTravelDay(dr As SafeDataReader) As ROResourceTravelDay

      Dim r As New ROResourceTravelDay()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceIDProperty, .GetInt32(0))
        LoadProperty(DateOfTravelProperty, .GetValue(1))
        LoadProperty(StartDateTimeProperty, .GetValue(2))
        LoadProperty(EndDateTimeProperty, .GetValue(3))
        LoadProperty(TotalFlightsProperty, .GetInt32(4))
        LoadProperty(TotalAccommodationProperty, .GetInt32(5))
        LoadProperty(TotalCarsProperty, .GetInt32(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace