﻿' Generated 11 Aug 2014 14:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly
Imports Singular.Emails
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.HR.ReadOnly
Imports System.Data.SqlClient
Imports Singular
Imports Singular.Misc
Imports System.IO
Imports OBLib.OutsideBroadcast

Namespace HR

  <Serializable()>
  Public Class HumanResourceAddToBooking
    Inherits OBBusinessBase(Of HumanResourceAddToBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key, Required(ErrorMessage:="Human Resource is required"), SetExpression("HumanResourceAddToBookingBO.HumanResourceIDSet(self)")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResourceName, "")
    ''' <summary>
    ''' Gets and sets the Firstname value
    ''' </summary>
    <Display(Name:="Name", Description:="The human resource's first name")>
    Public Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceSystemID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Working Area", Description:=""),
      Required(ErrorMessage:="Working Area is required"),
      SetExpression("HumanResourceAddToBookingBO.HumanResourceSystemIDSet(self)"),
      DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceSystemList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="HumanResourceAddToBookingBO.setHumanResourceSystemIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="HumanResourceAddToBookingBO.triggerHumanResourceSystemIDAutoPopulate",
                  AfterFetchJS:="HumanResourceAddToBookingBO.afterHumanResourceSystemIDRefreshAjax",
                  OnItemSelectJSFunction:="HumanResourceAddToBookingBO.onHumanResourceSystemIDSelected",
                  LookupMember:="HumanResourceSystem", DisplayMember:="WorkingArea", ValueMember:="HumanResourceSystemID",
                  DropDownColumns:={"WorkingArea"})>
    Public Property HumanResourceSystemID() As Integer?
      Get
        Return GetProperty(HumanResourceSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceSystemIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceSystem, "Working Area")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Working Area", Description:="")>
    Public Property HumanResourceSystem() As String
      Get
        Return GetProperty(HumanResourceSystemProperty)
      End Get
      Set(value As String)
        SetProperty(HumanResourceSystemProperty, value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required"),
      SetExpression("HumanResourceAddToBookingBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required"),
      SetExpression("HumanResourceAddToBookingBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    'Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
    '''' <summary>
    '''' Gets the ID value
    '''' </summary>
    '<Display(Name:="Discipline", Description:="The human resource's first name"),
    '  Required(ErrorMessage:="Discipline is required"),
    '  SetExpression("HumanResourceAddToBookingBO.DisciplineIDSet(self)")>
    'Public Property DisciplineID() As Integer?
    '  Get
    '    Return GetProperty(DisciplineIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(DisciplineIDProperty, Value)
    '  End Set
    'End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Event/Booking", Description:=""),
      Required(ErrorMessage:="Event/Booking is required"),
      SetExpression("HumanResourceAddToBookingBO.ProductionSystemAreaIDSet(self)"),
      DropDownWeb(GetType(OBLib.Notifications.Sms.ReadOnly.ROPSAListSms), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="HumanResourceAddToBookingBO.setProductionSystemAreaCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceAddToBookingBO.triggerProductionSystemAreaAutoPopulate",
                AfterFetchJS:="HumanResourceAddToBookingBO.afterProductionSystemAreaRefreshAjax",
                OnItemSelectJSFunction:="HumanResourceAddToBookingBO.onProductionSystemAreaIDSelected",
                LookupMember:="ProductionSystemAreaName", DisplayMember:="ProductionSystemAreaName", ValueMember:="ProductionSystemAreaID",
                DropDownColumns:={"RefNum", "ProductionSystemAreaName", "Room", "MinSD", "MaxED"})>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSystemAreaName, "Event")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Event", Description:="")>
    Public Property ProductionSystemAreaName() As String
      Get
        Return GetProperty(ProductionSystemAreaNameProperty)
      End Get
      Set(value As String)
        SetProperty(ProductionSystemAreaNameProperty, value)
      End Set
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Discipline & Position", Description:=""),
      Required(ErrorMessage:="Booking is required"),
      SetExpression("HumanResourceAddToBookingBO.ProductionHumanResourceIDSet(self)"),
      DropDownWeb(GetType(OBLib.Productions.Crew.ReadOnly.ROProductionHumanResourceList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="HumanResourceAddToBookingBO.setProductionHumanResourceIDCriteriaBeforeRefresh",
                  PreFindJSFunction:="HumanResourceAddToBookingBO.triggerProductionHumanResourceIDAutoPopulate",
                  AfterFetchJS:="HumanResourceAddToBookingBO.afterProductionHumanResourceIDRefreshAjax",
                  OnItemSelectJSFunction:="HumanResourceAddToBookingBO.onProductionHumanResourceIDSelected",
                  LookupMember:="DisciplinePosition", DisplayMember:="DisciplinePosition", ValueMember:="ProductionHumanResourceID",
                  DropDownColumns:={"Discipline", "Position"})>
    Public Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplinePositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DisciplinePosition, "Discipline & Position")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Discipline & Position", Description:="")>
    Public Property DisciplinePosition() As String
      Get
        Return GetProperty(DisciplinePositionProperty)
      End Get
      Set(value As String)
        SetProperty(DisciplinePositionProperty, value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
      Required(ErrorMessage:="Start Date is required"),
      SetExpression("HumanResourceAddToBookingBO.StartDateSet(self)")>
    Public Property StartDate() As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
      Required(ErrorMessage:="End Date is required"),
      SetExpression("HumanResourceAddToBookingBO.EndDateSet(self)")>
    Public Property EndDate() As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Other Properties "

    Public Property OBBooking As ProductionHROBSimple

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource")
        Else
          Return String.Format("Blank {0}", "Human Resource")
        End If
      Else
        Return Me.HumanResourceName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResource() method.

    End Sub

    Public Shared Function NewHumanResource() As HumanResource

      Return DataPortal.CreateChild(Of HumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHumanResourceAddToBooking(dr As SafeDataReader) As HumanResourceAddToBooking

      Dim h As New HumanResourceAddToBooking()
      'h.Fetch(dr)
      Return h

    End Function

#End If

#End Region

#End Region

  End Class

End Namespace