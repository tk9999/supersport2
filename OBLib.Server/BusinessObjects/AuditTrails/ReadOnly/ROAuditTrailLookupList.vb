﻿' Generated 21 Mar 2015 20:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailLookupList
    Inherits SingularReadOnlyListBase(Of ROAuditTrailLookupList, ROAuditTrailLookup)

#Region " Business Methods "

    Public Function GetItem(AuditTrailLookupID As Integer) As ROAuditTrailLookup

      For Each child As ROAuditTrailLookup In Me
        If child.AuditTrailLookupID = AuditTrailLookupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Audit Trail Lookups"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAuditTrailLookupList() As ROAuditTrailLookupList

      Return New ROAuditTrailLookupList()

    End Function

    Public Shared Sub BeginGetROAuditTrailLookupList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAuditTrailLookupList)))

      Dim dp As New DataPortal(Of ROAuditTrailLookupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAuditTrailLookupList(CallBack As EventHandler(Of DataPortalResult(Of ROAuditTrailLookupList)))

      Dim dp As New DataPortal(Of ROAuditTrailLookupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAuditTrailLookupList() As ROAuditTrailLookupList

      Return DataPortal.Fetch(Of ROAuditTrailLookupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAuditTrailLookup.GetROAuditTrailLookup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAuditTrailLookupList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace