﻿' Generated 21 Mar 2015 20:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailLookup
    Inherits SingularReadOnlyBase(Of ROAuditTrailLookup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailLookupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailLookupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AuditTrailLookupID() As Integer
      Get
        Return GetProperty(AuditTrailLookupIDProperty)
      End Get
    End Property

    Public Shared AuditTrailTableIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuditTrailTableID, "Audit Trail Table", Nothing)
    ''' <summary>
    ''' Gets the Audit Trail Table value
    ''' </summary>
    <Display(Name:="Audit Trail Table", Description:="")>
  Public ReadOnly Property AuditTrailTableID() As Integer?
      Get
        Return GetProperty(AuditTrailTableIDProperty)
      End Get
    End Property

    Public Shared ColumnNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ColumnName, "Column Name", "")
    ''' <summary>
    ''' Gets the Column Name value
    ''' </summary>
    <Display(Name:="Column Name", Description:="")>
  Public ReadOnly Property ColumnName() As String
      Get
        Return GetProperty(ColumnNameProperty)
      End Get
    End Property

    Public Shared LookupTableProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LookupTable, "Lookup Table", "")
    ''' <summary>
    ''' Gets the Lookup Table value
    ''' </summary>
    <Display(Name:="Lookup Table", Description:="")>
  Public ReadOnly Property LookupTable() As String
      Get
        Return GetProperty(LookupTableProperty)
      End Get
    End Property

    Public Shared LookupColumnsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LookupColumns, "Lookup Columns", "")
    ''' <summary>
    ''' Gets the Lookup Columns value
    ''' </summary>
    <Display(Name:="Lookup Columns", Description:="")>
  Public ReadOnly Property LookupColumns() As String
      Get
        Return GetProperty(LookupColumnsProperty)
      End Get
    End Property

    Public Shared LookupIDColumnProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LookupIDColumn, "Lookup ID Column", "")
    ''' <summary>
    ''' Gets the Lookup ID Column value
    ''' </summary>
    <Display(Name:="Lookup ID Column", Description:="")>
  Public ReadOnly Property LookupIDColumn() As String
      Get
        Return GetProperty(LookupIDColumnProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailLookupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ColumnName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrailLookup(dr As SafeDataReader) As ROAuditTrailLookup

      Dim r As New ROAuditTrailLookup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailLookupIDProperty, .GetInt32(0))
        LoadProperty(AuditTrailTableIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ColumnNameProperty, .GetString(2))
        LoadProperty(LookupTableProperty, .GetString(3))
        LoadProperty(LookupColumnsProperty, .GetString(4))
        LoadProperty(LookupIDColumnProperty, .GetString(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace