﻿' Generated 21 Mar 2015 21:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailTimelineDetail
    Inherits SingularReadOnlyBase(Of ROAuditTrailTimelineDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AuditTrailID() As Integer
      Get
        Return GetProperty(AuditTrailIDProperty)
      End Get
    End Property

    Public Shared UserNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UserName, "User Name")
    ''' <summary>
    ''' Gets the User Name value
    ''' </summary>
    <Display(Name:="User Name", Description:="")>
  Public ReadOnly Property UserName() As String
      Get
        Return GetProperty(UserNameProperty)
      End Get
    End Property

    Public Shared MachineNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MachineName, "Machine Name")
    ''' <summary>
    ''' Gets the Machine Name value
    ''' </summary>
    <Display(Name:="Machine Name", Description:="")>
  Public ReadOnly Property MachineName() As String
      Get
        Return GetProperty(MachineNameProperty)
      End Get
    End Property

    Public Shared ChangeDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChangeDateTime, "Change Date Time")
    ''' <summary>
    ''' Gets the Change Date Time value
    ''' </summary>
    <Display(Name:="Change Date Time", Description:="")>
  Public ReadOnly Property ChangeDateTime As DateTime?
      Get
        Return GetProperty(ChangeDateTimeProperty)
      End Get
    End Property

    Public Shared ColumnNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ColumnName, "Column Name")
    ''' <summary>
    ''' Gets the Column Name value
    ''' </summary>
    <Display(Name:="Column Name", Description:="")>
  Public ReadOnly Property ColumnName() As String
      Get
        Return GetProperty(ColumnNameProperty)
      End Get
    End Property

    Public Shared PreviousValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousValue, "Previous Value", "")
    ''' <summary>
    ''' Gets the Previous Value value
    ''' </summary>
    <Display(Name:="Previous Value", Description:="")>
    Public ReadOnly Property PreviousValue() As String
      Get
        Return GetProperty(PreviousValueProperty)
      End Get
    End Property

    Public Shared NewValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewValue, "New Value", "")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="New Value", Description:="")>
    Public ReadOnly Property NewValue() As String
      Get
        Return GetProperty(NewValueProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrailTimelineDetail(dr As SafeDataReader) As ROAuditTrailTimelineDetail

      Dim r As New ROAuditTrailTimelineDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailIDProperty, .GetInt32(0))
        LoadProperty(UserNameProperty, .GetString(1))
        LoadProperty(MachineNameProperty, .GetString(2))
        LoadProperty(ChangeDateTimeProperty, .GetValue(3))
        LoadProperty(ColumnNameProperty, .GetString(4))
        LoadProperty(PreviousValueProperty, .GetString(5))
        LoadProperty(NewValueProperty, .GetString(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace