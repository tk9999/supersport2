﻿' Generated 21 Mar 2015 21:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailTimelineDetailList
    Inherits SingularReadOnlyListBase(Of ROAuditTrailTimelineDetailList, ROAuditTrailTimelineDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROAuditTrailTimeline
#End Region

#Region " Business Methods "

    Public Function GetItem(AuditTrailID As Integer) As ROAuditTrailTimelineDetail

      For Each child As ROAuditTrailTimelineDetail In Me
        If child.AuditTrailID = AuditTrailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROAuditTrailTimelineDetailList() As ROAuditTrailTimelineDetailList

      Return New ROAuditTrailTimelineDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace