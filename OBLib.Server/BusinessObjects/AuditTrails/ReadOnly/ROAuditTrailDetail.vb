﻿' Generated 21 Mar 2015 20:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailDetail
    Inherits SingularReadOnlyBase(Of ROAuditTrailDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AuditTrailDetailID() As Integer
      Get
        Return GetProperty(AuditTrailDetailIDProperty)
      End Get
    End Property

    Public Shared AuditTrailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuditTrailID, "Audit Trail", Nothing)
    ''' <summary>
    ''' Gets the Audit Trail value
    ''' </summary>
    <Display(Name:="Audit Trail", Description:="")>
  Public ReadOnly Property AuditTrailID() As Integer?
      Get
        Return GetProperty(AuditTrailIDProperty)
      End Get
    End Property

    Public Shared ColumnNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ColumnName, "Column Name", "")
    ''' <summary>
    ''' Gets the Column Name value
    ''' </summary>
    <Display(Name:="Column Name", Description:="")>
  Public ReadOnly Property ColumnName() As String
      Get
        Return GetProperty(ColumnNameProperty)
      End Get
    End Property

    Public Shared PreviousValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousValue, "Previous Value")
    ''' <summary>
    ''' Gets the Previous Value value
    ''' </summary>
    <Display(Name:="Previous Value", Description:="")>
    Public ReadOnly Property PreviousValue() As String
      Get
        Return GetProperty(PreviousValueProperty)
      End Get
    End Property

    Public Shared NewValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewValue, "New Value")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="New Value", Description:="")>
    Public ReadOnly Property NewValue() As String
      Get
        Return GetProperty(NewValueProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ColumnName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrailDetail(dr As SafeDataReader) As ROAuditTrailDetail

      Dim r As New ROAuditTrailDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailDetailIDProperty, .GetInt32(0))
        LoadProperty(AuditTrailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ColumnNameProperty, .GetString(2))
        LoadProperty(PreviousValueProperty, .GetString(3))
        LoadProperty(NewValueProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace