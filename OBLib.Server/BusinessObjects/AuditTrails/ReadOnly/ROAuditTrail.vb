﻿' Generated 21 Mar 2015 20:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrail
    Inherits SingularReadOnlyBase(Of ROAuditTrail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AuditTrailID() As Integer
      Get
        Return GetProperty(AuditTrailIDProperty)
      End Get
    End Property

    Public Shared AuditTrailTableIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuditTrailTableID, "Audit Trail Table", Nothing)
    ''' <summary>
    ''' Gets the Audit Trail Table value
    ''' </summary>
    <Display(Name:="Audit Trail Table", Description:="")>
  Public ReadOnly Property AuditTrailTableID() As Integer?
      Get
        Return GetProperty(AuditTrailTableIDProperty)
      End Get
    End Property

    Public Shared KeyValueProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.KeyValue, "Key Value", 0)
    ''' <summary>
    ''' Gets the Key Value value
    ''' </summary>
    <Display(Name:="Key Value", Description:="")>
  Public ReadOnly Property KeyValue() As Integer
      Get
        Return GetProperty(KeyValueProperty)
      End Get
    End Property

    Public Shared AuditTrailTypeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailType, "Audit Trail Type", 0)
    ''' <summary>
    ''' Gets the Audit Trail Type value
    ''' </summary>
    <Display(Name:="Audit Trail Type", Description:="")>
  Public ReadOnly Property AuditTrailType() As Integer
      Get
        Return GetProperty(AuditTrailTypeProperty)
      End Get
    End Property

    Public Shared AuditTrailUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuditTrailUserID, "Audit Trail User", Nothing)
    ''' <summary>
    ''' Gets the Audit Trail User value
    ''' </summary>
    <Display(Name:="Audit Trail User", Description:="")>
  Public ReadOnly Property AuditTrailUserID() As Integer?
      Get
        Return GetProperty(AuditTrailUserIDProperty)
      End Get
    End Property

    Public Shared ChangeDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChangeDateTime, "Change Date Time")
    ''' <summary>
    ''' Gets the Change Date Time value
    ''' </summary>
    <Display(Name:="Change Date Time", Description:="")>
  Public ReadOnly Property ChangeDateTime As DateTime?
      Get
        Return GetProperty(ChangeDateTimeProperty)
      End Get
    End Property

    Public Shared InsertIDProperty As PropertyInfo(Of Guid) = RegisterProperty(Of Guid)(Function(c) c.InsertID, "Insert", Guid.Empty)
    ''' <summary>
    ''' Gets the Insert value
    ''' </summary>
    <Display(Name:="Insert", Description:="ID used to help link audit trail details to audit trails")>
  Public ReadOnly Property InsertID() As Guid
      Get
        Return GetProperty(InsertIDProperty)
      End Get
    End Property

    Public Shared DeletedDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DeletedDescription, "Deleted Description", "")
    ''' <summary>
    ''' Gets the Deleted Description value
    ''' </summary>
    <Display(Name:="Deleted Description", Description:="")>
  Public ReadOnly Property DeletedDescription() As String
      Get
        Return GetProperty(DeletedDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.DeletedDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrail(dr As SafeDataReader) As ROAuditTrail

      Dim r As New ROAuditTrail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailIDProperty, .GetInt32(0))
        LoadProperty(AuditTrailTableIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(KeyValueProperty, .GetInt32(2))
        LoadProperty(AuditTrailTypeProperty, .GetByte(3))
        LoadProperty(AuditTrailUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ChangeDateTimeProperty, .GetValue(5))
        LoadProperty(InsertIDProperty, .GetGuid(6))
        LoadProperty(DeletedDescriptionProperty, .GetString(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace