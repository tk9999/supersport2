﻿' Generated 21 Mar 2015 21:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailTimelineList
    Inherits SingularReadOnlyListBase(Of ROAuditTrailTimelineList, ROAuditTrailTimeline)

#Region " Business Methods "

    Public Function GetItem(AuditTrailID As Integer) As ROAuditTrailTimeline

      For Each child As ROAuditTrailTimeline In Me
        If child.AuditTrailID = AuditTrailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROAuditTrailTimelineDetail(AuditTrailID As Integer) As ROAuditTrailTimelineDetail

      Dim obj As ROAuditTrailTimelineDetail = Nothing
      For Each parent As ROAuditTrailTimeline In Me
        obj = parent.ROAuditTrailTimelineDetailList.GetItem(AuditTrailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
      <Display(Name:="Start Date", Description:=""), Required()>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
      <Display(Name:="End Date", Description:=""), Required()>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared AuditTrailTableIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuditTrailTableID, "Table", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Table", Description:=""),
      Required(ErrorMessage:="Table is required"),
      DropDownWeb(GetType(ROAuditTrailTableList))>
      Public Property AuditTrailTableID() As Integer?
        Get
          Return ReadProperty(AuditTrailTableIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(AuditTrailTableIDProperty, Value)
        End Set
      End Property

      Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="User", Description:="")>
      Public Property UserID() As Integer?
        Get
          Return ReadProperty(UserIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(UserIDProperty, Value)
        End Set
      End Property

      Public Shared FieldsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Fields, "Fields", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Fields", Description:="")>
      Public Property Fields() As String
        Get
          Return ReadProperty(FieldsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(FieldsProperty, Value)
        End Set
      End Property

      Public Shared PreviousValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousValue, "PreviousValue", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Previous Value", Description:="")>
      Public Property PreviousValue() As String
        Get
          Return ReadProperty(PreviousValueProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PreviousValueProperty, Value)
        End Set
      End Property

      Public Shared NewValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewValue, "NewValue", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="New Value", Description:="")>
      Public Property NewValue() As String
        Get
          Return ReadProperty(NewValueProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(NewValueProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAuditTrailTimelineList() As ROAuditTrailTimelineList

      Return New ROAuditTrailTimelineList()

    End Function

    Public Shared Sub BeginGetROAuditTrailTimelineList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAuditTrailTimelineList)))

      Dim dp As New DataPortal(Of ROAuditTrailTimelineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAuditTrailTimelineList(CallBack As EventHandler(Of DataPortalResult(Of ROAuditTrailTimelineList)))

      Dim dp As New DataPortal(Of ROAuditTrailTimelineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAuditTrailTimelineList() As ROAuditTrailTimelineList

      Return DataPortal.Fetch(Of ROAuditTrailTimelineList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAuditTrailTimeline.GetROAuditTrailTimeline(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROAuditTrailTimeline = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AuditTrailID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          If parent IsNot Nothing Then
            parent.ROAuditTrailTimelineDetailList.RaiseListChangedEvents = False
            parent.ROAuditTrailTimelineDetailList.Add(ROAuditTrailTimelineDetail.GetROAuditTrailTimelineDetail(sdr))
            parent.ROAuditTrailTimelineDetailList.RaiseListChangedEvents = True
          End If
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcs].[getAuditTrailHeaderList]"
            cm.Parameters.AddWithValue("@Fields", Strings.MakeEmptyDBNull(crit.Fields))
            cm.Parameters.AddWithValue("@NewValue", Strings.MakeEmptyDBNull(crit.NewValue))
            cm.Parameters.AddWithValue("@PreviousValue", Strings.MakeEmptyDBNull(crit.PreviousValue))
            cm.Parameters.AddWithValue("@AuditTrailTableID", NothingDBNull(crit.AuditTrailTableID))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace