﻿' Generated 21 Mar 2015 21:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailTimeline
    Inherits SingularReadOnlyBase(Of ROAuditTrailTimeline)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AuditTrailID() As Integer
      Get
        Return GetProperty(AuditTrailIDProperty)
      End Get
    End Property

    Public Shared AuditTrailTableIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailTableID, "Audit Trail Table")
    ''' <summary>
    ''' Gets the Audit Trail Table value
    ''' </summary>
    <Display(Name:="Audit Trail Table", Description:="")>
    Public ReadOnly Property AuditTrailTableID() As Integer
      Get
        Return GetProperty(AuditTrailTableIDProperty)
      End Get
    End Property

    Public Shared KeyValueProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.KeyValue, "Key Value")
    ''' <summary>
    ''' Gets the Key Value value
    ''' </summary>
    <Display(Name:="Key Value", Description:="")>
    Public ReadOnly Property KeyValue() As Integer
      Get
        Return GetProperty(KeyValueProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared AuditTrailTypeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailType, "Audit Trail Type")
    ''' <summary>
    ''' Gets the Audit Trail Type value
    ''' </summary>
    <Display(Name:="Audit Trail Type", Description:="")>
    Public ReadOnly Property AuditTrailType() As Integer
      Get
        Return GetProperty(AuditTrailTypeProperty)
      End Get
    End Property

    Public Shared ChangeDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChangeDateTime, "Change Date Time")
    ''' <summary>
    ''' Gets the Change Date Time value
    ''' </summary>
    <Display(Name:="Change Date Time", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property ChangeDateTime As DateTime?
      Get
        Return GetProperty(ChangeDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "Login Name")
    ''' <summary>
    ''' Gets the Login Name value
    ''' </summary>
    <Display(Name:="Login Name", Description:="")>
    Public ReadOnly Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
    End Property

    Public Shared ChangeCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChangeCount, "Change Count")
    ''' <summary>
    ''' Gets the Change Count value
    ''' </summary>
    <Display(Name:="Change Count", Description:="")>
    Public ReadOnly Property ChangeCount() As Integer
      Get
        Return GetProperty(ChangeCountProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROAuditTrailTimelineDetailListProperty As PropertyInfo(Of ROAuditTrailTimelineDetailList) = RegisterProperty(Of ROAuditTrailTimelineDetailList)(Function(c) c.ROAuditTrailTimelineDetailList, "RO Audit Trail Timeline Detail List")

    Public ReadOnly Property ROAuditTrailTimelineDetailList() As ROAuditTrailTimelineDetailList
      Get
        If GetProperty(ROAuditTrailTimelineDetailListProperty) Is Nothing Then
          LoadProperty(ROAuditTrailTimelineDetailListProperty, AuditTrails.ReadOnly.ROAuditTrailTimelineDetailList.NewROAuditTrailTimelineDetailList())
        End If
        Return GetProperty(ROAuditTrailTimelineDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Description

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrailTimeline(dr As SafeDataReader) As ROAuditTrailTimeline

      Dim r As New ROAuditTrailTimeline()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailIDProperty, .GetInt32(0))
        LoadProperty(AuditTrailTableIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(KeyValueProperty, .GetInt32(2))
        LoadProperty(DescriptionProperty, .GetString(3))
        LoadProperty(AuditTrailTypeProperty, .GetInt32(4))
        LoadProperty(ChangeDateTimeProperty, .GetValue(5))
        LoadProperty(LoginNameProperty, .GetString(6))
        LoadProperty(ChangeCountProperty, .GetInt32(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace