﻿' Generated 21 Mar 2015 20:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailUser
    Inherits SingularReadOnlyBase(Of ROAuditTrailUser)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailUserID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AuditTrailUserID() As Integer
      Get
        Return GetProperty(AuditTrailUserIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "User", 0)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
  Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "Login Name", "")
    ''' <summary>
    ''' Gets the Login Name value
    ''' </summary>
    <Display(Name:="Login Name", Description:="")>
  Public ReadOnly Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
    End Property

    Public Shared MachineNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MachineName, "Machine Name", "")
    ''' <summary>
    ''' Gets the Machine Name value
    ''' </summary>
    <Display(Name:="Machine Name", Description:="")>
  Public ReadOnly Property MachineName() As String
      Get
        Return GetProperty(MachineNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailUserIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.LoginName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrailUser(dr As SafeDataReader) As ROAuditTrailUser

      Dim r As New ROAuditTrailUser()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailUserIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, .GetInt32(1))
        LoadProperty(LoginNameProperty, .GetString(2))
        LoadProperty(MachineNameProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace