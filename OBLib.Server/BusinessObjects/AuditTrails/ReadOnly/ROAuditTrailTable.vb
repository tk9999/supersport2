﻿' Generated 21 Mar 2015 20:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AuditTrails.ReadOnly

  <Serializable()> _
  Public Class ROAuditTrailTable
    Inherits SingularReadOnlyBase(Of ROAuditTrailTable)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AuditTrailTableIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailTableID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AuditTrailTableID() As Integer
      Get
        Return GetProperty(AuditTrailTableIDProperty)
      End Get
    End Property

    Public Shared TableNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TableName, "Table Name", "")
    ''' <summary>
    ''' Gets the Table Name value
    ''' </summary>
    <Display(Name:="Table Name", Description:="")>
  Public ReadOnly Property TableName() As String
      Get
        Return GetProperty(TableNameProperty)
      End Get
    End Property

    Public Shared PKColumnProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PKColumn, "PK Column", "")
    ''' <summary>
    ''' Gets the PK Column value
    ''' </summary>
    <Display(Name:="PK Column", Description:="")>
  Public ReadOnly Property PKColumn() As String
      Get
        Return GetProperty(PKColumnProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
  Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared ParentTableIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentTableID, "Parent Table", Nothing)
    ''' <summary>
    ''' Gets the Parent Table value
    ''' </summary>
    <Display(Name:="Parent Table", Description:="")>
  Public ReadOnly Property ParentTableID() As Integer?
      Get
        Return GetProperty(ParentTableIDProperty)
      End Get
    End Property

    Public Shared ParentPKColumnProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ParentPKColumn, "Parent PK Column", "")
    ''' <summary>
    ''' Gets the Parent PK Column value
    ''' </summary>
    <Display(Name:="Parent PK Column", Description:="")>
  Public ReadOnly Property ParentPKColumn() As String
      Get
        Return GetProperty(ParentPKColumnProperty)
      End Get
    End Property

    Public Shared TableDisplayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TableDisplayName, "Table Display Name", "")
    ''' <summary>
    ''' Gets the Table Display Name value
    ''' </summary>
    <Display(Name:="Table Display Name", Description:="")>
  Public ReadOnly Property TableDisplayName() As String
      Get
        Return GetProperty(TableDisplayNameProperty)
      End Get
    End Property

    Public Shared TableDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TableDescription, "Table Description", "")
    ''' <summary>
    ''' Gets the Table Description value
    ''' </summary>
    <Display(Name:="Table Description", Description:="")>
  Public ReadOnly Property TableDescription() As String
      Get
        Return GetProperty(TableDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailTableIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TableName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAuditTrailTable(dr As SafeDataReader) As ROAuditTrailTable

      Dim r As New ROAuditTrailTable()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailTableIDProperty, .GetInt32(0))
        LoadProperty(TableNameProperty, .GetString(1))
        LoadProperty(PKColumnProperty, .GetString(2))
        LoadProperty(DescriptionProperty, .GetString(3))
        LoadProperty(ParentTableIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ParentPKColumnProperty, .GetString(5))
        LoadProperty(TableDisplayNameProperty, .GetString(6))
        LoadProperty(TableDescriptionProperty, .GetString(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace