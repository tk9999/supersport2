﻿' Generated 17 Sep 2014 07:02 - Singular Systems Object Generator Version 2.1.674
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace MealReimbursements

  <Serializable()> _
  Public Class ROMealReimbursement
    Inherits OBReadOnlyBase(Of ROMealReimbursement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FromModuleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FromModule, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property FromModule() As String
      Get
        Return GetProperty(FromModuleProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
  Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
  Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
  Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
  Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared PeriodStartProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.PeriodStart, "Period Start")
    ''' <summary>
    ''' Gets the Period Start value
    ''' </summary>
    <Display(Name:="Period Start", Description:="")>
    Public ReadOnly Property PeriodStart As DateTime
      Get
        Return GetProperty(PeriodStartProperty)
      End Get
    End Property

    Public Shared PeriodEndProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.PeriodEnd, "Period End")
    ''' <summary>
    ''' Gets the Period End value
    ''' </summary>
    <Display(Name:="Period End", Description:="")>
    Public ReadOnly Property PeriodEnd As DateTime
      Get
        Return GetProperty(PeriodEndProperty)
      End Get
    End Property

    Public Shared AuthorisedHoursWorkedProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AuthorisedHoursWorked, "Hours Worked")
    ''' <summary>
    ''' Gets the Hours Worked value
    ''' </summary>
    <Display(Name:="Authorised Hours Worked", Description:="")>
    Public ReadOnly Property AuthorisedHoursWorked() As Decimal
      Get
        Return GetProperty(AuthorisedHoursWorkedProperty)
      End Get
    End Property

    Public Shared NumberOfMealVouchersProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NumberOfMealVouchers, "Number Of Meal Vouchers")
    ''' <summary>
    ''' Gets the Number Of Meal Vouchers value
    ''' </summary>
    <Display(Name:="Number Of Meal Vouchers", Description:="")>
    Public ReadOnly Property NumberOfMealVouchers() As Integer
      Get
        Return GetProperty(NumberOfMealVouchersProperty)
      End Get
    End Property

    Public Shared NumberOfSpecialMealVouchersProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NumberOfSpecialMealVouchers, "Number Of Meal Vouchers")
    ''' <summary>
    ''' Gets the Number Of Meal Vouchers value
    ''' </summary>
    <Display(Name:="Number Of Meal Vouchers", Description:="")>
    Public ReadOnly Property NumberOfSpecialMealVouchers() As Integer
      Get
        Return GetProperty(NumberOfSpecialMealVouchersProperty)
      End Get
    End Property

    Public Shared AmountPayableProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AmountPayable, "Amount Payable")
    ''' <summary>
    ''' Gets the Amount Payable value
    ''' </summary>
    <Display(Name:="Amount Payable", Description:="")>
    Public ReadOnly Property AmountPayable() As Decimal
      Get
        Return GetProperty(AmountPayableProperty)
      End Get
    End Property

    Public Shared SubDepartmentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDepartment, " Sub-Department")
    ''' <summary>
    ''' Gets the SubDepartment value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:="")>
    Public ReadOnly Property SubDepartment() As String
      Get
        Return GetProperty(SubDepartmentProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, " Area")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FromModuleProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FromModule

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROMealReimbursement(dr As SafeDataReader) As ROMealReimbursement

      Dim r As New ROMealReimbursement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FromModuleProperty, .GetString(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FirstNameProperty, .GetString(2))
        LoadProperty(SurnameProperty, .GetString(3))
        LoadProperty(EmployeeCodeProperty, .GetString(4))
        LoadProperty(IDNoProperty, .GetString(5))
        LoadProperty(PeriodStartProperty, .GetValue(6))
        LoadProperty(PeriodEndProperty, .GetValue(7))
        LoadProperty(AuthorisedHoursWorkedProperty, .GetDecimal(8))
        LoadProperty(NumberOfMealVouchersProperty, .GetInt32(9))
        LoadProperty(NumberOfSpecialMealVouchersProperty, .GetInt32(10))
        LoadProperty(AmountPayableProperty, .GetDecimal(11))
        LoadProperty(SubDepartmentProperty, .GetString(12))
        LoadProperty(AreaProperty, .GetString(13))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace