﻿' Generated 17 Sep 2014 07:02 - Singular Systems Object Generator Version 2.1.674
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace MealReimbursements

  'Public Class MealReimbursement

  '  <
  '  Public ReadOnly Property FromModule() As String
  '    Get
  '      Return GetProperty(FromModuleProperty)
  '    End Get
  '  End Property

  '  Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
  '  ''' <summary>
  '  ''' Gets the Human Resource value
  '  ''' </summary>
  '  <Display(Name:="Human Resource", Description:=""), Key>
  '  Public ReadOnly Property HumanResourceID() As Integer
  '    Get
  '      Return GetProperty(HumanResourceIDProperty)
  '    End Get
  '  End Property

  '  Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name")
  '  ''' <summary>
  '  ''' Gets the First Name value
  '  ''' </summary>
  '  <Display(Name:="First Name", Description:="")>
  '  Public ReadOnly Property FirstName() As String
  '    Get
  '      Return GetProperty(FirstNameProperty)
  '    End Get
  '  End Property

  '  Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
  '  ''' <summary>
  '  ''' Gets the Surname value
  '  ''' </summary>
  '  <Display(Name:="Surname", Description:="")>
  '  Public ReadOnly Property Surname() As String
  '    Get
  '      Return GetProperty(SurnameProperty)
  '    End Get
  '  End Property

  '  Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
  '  ''' <summary>
  '  ''' Gets the Employee Code value
  '  ''' </summary>
  '  <Display(Name:="Employee Code", Description:="")>
  '  Public ReadOnly Property EmployeeCode() As String
  '    Get
  '      Return GetProperty(EmployeeCodeProperty)
  '    End Get
  '  End Property

  '  Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
  '  ''' <summary>
  '  ''' Gets the ID No value
  '  ''' </summary>
  '  <Display(Name:="ID No", Description:="")>
  '  Public ReadOnly Property IDNo() As String
  '    Get
  '      Return GetProperty(IDNoProperty)
  '    End Get
  '  End Property

  '  Public Shared PeriodStartProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.PeriodStart, "Period Start")
  '  ''' <summary>
  '  ''' Gets the Period Start value
  '  ''' </summary>
  '  <Display(Name:="Period Start", Description:="")>
  '  Public ReadOnly Property PeriodStart As DateTime
  '    Get
  '      Return GetProperty(PeriodStartProperty)
  '    End Get
  '  End Property

  '  Public Shared PeriodEndProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.PeriodEnd, "Period End")
  '  ''' <summary>
  '  ''' Gets the Period End value
  '  ''' </summary>
  '  <Display(Name:="Period End", Description:="")>
  '  Public ReadOnly Property PeriodEnd As DateTime
  '    Get
  '      Return GetProperty(PeriodEndProperty)
  '    End Get
  '  End Property

  '  Public Shared HoursWorkedProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursWorked, "Hours Worked")
  '  ''' <summary>
  '  ''' Gets the Hours Worked value
  '  ''' </summary>
  '  <Display(Name:="Hours Worked", Description:="")>
  '  Public ReadOnly Property HoursWorked() As Decimal
  '    Get
  '      Return GetProperty(HoursWorkedProperty)
  '    End Get
  '  End Property

  '  Public Shared NumberOfMealVouchersProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NumberOfMealVouchers, "Number Of Meal Vouchers")
  '  ''' <summary>
  '  ''' Gets the Number Of Meal Vouchers value
  '  ''' </summary>
  '  <Display(Name:="Number Of Meal Vouchers", Description:="")>
  '  Public ReadOnly Property NumberOfMealVouchers() As Integer
  '    Get
  '      Return GetProperty(NumberOfMealVouchersProperty)
  '    End Get
  '  End Property

  '  Public Shared AmountPayableProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AmountPayable, "Amount Payable")
  '  ''' <summary>
  '  ''' Gets the Amount Payable value
  '  ''' </summary>
  '  <Display(Name:="Amount Payable", Description:="")>
  '  Public ReadOnly Property AmountPayable() As Decimal
  '    Get
  '      Return GetProperty(AmountPayableProperty)
  '    End Get
  '  End Property


  'End Class

  <Serializable()> _
  Public Class ROMealReimbursementList
    Inherits OBReadOnlyListBase(Of ROMealReimbursementList, ROMealReimbursement)

#Region " Business Methods "



    Public Function GetItem(FromModule As String) As ROMealReimbursement

      For Each child As ROMealReimbursement In Me
        If child.FromModule = FromModule Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROMealReimbursementList() As ROMealReimbursementList

      Return New ROMealReimbursementList()

    End Function

    Public Shared Sub BeginGetROMealReimbursementList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROMealReimbursementList)))

      Dim dp As New DataPortal(Of ROMealReimbursementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROMealReimbursementList(CallBack As EventHandler(Of DataPortalResult(Of ROMealReimbursementList)))

      Dim dp As New DataPortal(Of ROMealReimbursementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROMealReimbursementList(CloseMonth As Boolean) As ROMealReimbursementList

      Return DataPortal.Fetch(Of ROMealReimbursementList)(New SingleCriteria(Of Boolean)(CloseMonth))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROMealReimbursement.GetROMealReimbursement(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As SingleCriteria(Of Boolean) = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getOpenMonthMealReimbursementList"

            cm.Parameters.AddWithValue("@CloseMonthInd", crit.Value)

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace