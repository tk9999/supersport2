﻿' Generated 02 Mar 2015 09:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentService
    Inherits OBBusinessBase(Of EquipmentService)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentServiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentServiceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EquipmentServiceID() As Integer
      Get
        Return GetProperty(EquipmentServiceIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Date the piece of equipments service started. The equipment will be unavailable for a production for the duration"),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="Date the piece of equipments service ended"),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ServiceDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ServiceDescription, "Service Description", "")
    ''' <summary>
    ''' Gets and sets the Service Description value
    ''' </summary>
    <Display(Name:="Service Description", Description:=""),
    StringLength(50, ErrorMessage:="Service Description cannot be more than 50 characters"),
    Required(ErrorMessage:="Description required")>
    Public Property ServiceDescription() As String
      Get
        Return GetProperty(ServiceDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ServiceDescriptionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Equipment

      Return CType(CType(Me.Parent, EquipmentServiceList).Parent, Equipment)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentServiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ServiceDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Service")
        Else
          Return String.Format("Blank {0}", "Equipment Service")
        End If
      Else
        Return Me.ServiceDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateProperty)
        .JavascriptRuleFunctionName = "CheckDates"
        .ServerRuleFunction = Function(es)
                                If es.EndDate < es.StartDate Then
                                  Return "Start Date must be less than end date"
                                End If
                                Return ""
                              End Function

        .AffectedProperties.Add(EndDateProperty)
        .AddTriggerProperty(EndDateProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentService() method.

    End Sub

    Public Shared Function NewEquipmentService() As EquipmentService

      Return DataPortal.CreateChild(Of EquipmentService)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentService(dr As SafeDataReader) As EquipmentService

      Dim e As New EquipmentService()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentServiceIDProperty, .GetInt32(0))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(StartDateProperty, .GetValue(2))
          LoadProperty(EndDateProperty, .GetValue(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ServiceDescriptionProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentService"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentService"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEquipmentServiceID As SqlParameter = .Parameters.Add("@EquipmentServiceID", SqlDbType.Int)
          paramEquipmentServiceID.Value = GetProperty(EquipmentServiceIDProperty)
          If Me.IsNew Then
            paramEquipmentServiceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentID", Me.GetParent().EquipmentID)
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ServiceDescription", GetProperty(ServiceDescriptionProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentServiceIDProperty, paramEquipmentServiceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentService"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentServiceID", GetProperty(EquipmentServiceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace