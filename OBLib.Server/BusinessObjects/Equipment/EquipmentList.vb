﻿' Generated 02 Mar 2015 09:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentList
    Inherits OBBusinessListBase(Of EquipmentList, Equipment)

#Region " Business Methods "

    Public Function GetItem(EquipmentID As Integer) As Equipment

      For Each child As Equipment In Me
        If child.EquipmentID = EquipmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipments"

    End Function

    'Public Function GetEquipmentService(EquipmentServiceID As Integer) As EquipmentService

    '  Dim obj As EquipmentService = Nothing
    '  For Each parent As Equipment In Me
    '    obj = parent.EquipmentServiceList.GetItem(EquipmentServiceID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property EquipmentID As Object
      Public Property EquipmentIDs As String = ""
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(EquipmentIDs As String, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.EquipmentIDs = EquipmentIDs
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(EquipmentID As String)
        Me.EquipmentID = EquipmentID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEquipmentList() As EquipmentList

      Return New EquipmentList()

    End Function

    Public Shared Sub BeginGetEquipmentList(CallBack As EventHandler(Of DataPortalResult(Of EquipmentList)))

      Dim dp As New DataPortal(Of EquipmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEquipmentList(EquipmentID As Object) As EquipmentList

      Return DataPortal.Fetch(Of EquipmentList)(New Criteria(EquipmentID))

    End Function

    Public Shared Function GetEquipmentList() As EquipmentList

      Return DataPortal.Fetch(Of EquipmentList)(New Criteria())

    End Function

    Public Shared Function GetSatOpsEquipmentList(SystemID As Integer?, ProductionAreaID As Integer?) As EquipmentList

      Return DataPortal.Fetch(Of EquipmentList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Equipment.GetEquipment(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As Equipment = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.EquipmentID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.EquipmentServiceList.RaiseListChangedEvents = False
      '    parent.EquipmentServiceList.Add(EquipmentService.GetEquipmentService(sdr))
      '    parent.EquipmentServiceList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ParentEquipmentID <> sdr.GetInt32(12) Then
      '      parent = Me.GetItem(sdr.GetInt32(12))
      '    End If
      '    parent.ChildEquipmentList.RaiseListChangedEvents = False
      '    parent.ChildEquipmentList.Add(Equipment.GetEquipment(sdr))
      '    parent.ChildEquipmentList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As Equipment In Me
        child.CheckRules()
        'For Each EquipmentService As EquipmentService In child.EquipmentServiceList
        '  EquipmentService.CheckRules()
        'Next
        'For Each ChildEquipment As Equipment In child.ChildEquipmentList
        '  ChildEquipment.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEquipmentList"
            cm.Parameters.AddWithValue("@EquipmentID", Singular.Misc.NothingDBNull(crit.EquipmentID))
            cm.Parameters.AddWithValue("@EquipmentIDs", Singular.Strings.MakeEmptyDBNull(crit.EquipmentIDs))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Equipment In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Equipment In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace