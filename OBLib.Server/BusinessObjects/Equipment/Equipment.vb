﻿' Generated 02 Mar 2015 09:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Equipment

  <Serializable()> _
  Public Class Equipment
    Inherits OBBusinessBase(Of Equipment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared EquipmentDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentDescription, "Equipment Name", "")
    ''' <summary>
    ''' Gets and sets the Equipment Description value
    ''' </summary>
    <Display(Name:="Equipment Name", Description:="Description for the piece of equipment"),
    Required(ErrorMessage:="Equipment Name required"),
    StringLength(100, ErrorMessage:="Equipment Name cannot be more than 100 characters")>
    Public Property EquipmentDescription() As String
      Get
        Return GetProperty(EquipmentDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentDescriptionProperty, Value)
      End Set
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Equipment Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="The general type for this piece of equipment"),
    Required(ErrorMessage:="Equipment Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROEquipmentTypeList), ValueMember:="EquipmentTypeID", DisplayMember:="EquipmentType")>
    Public Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="The specific type for this piece of equipment"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROEquipmentSubTypeList), ThisFilterMember:="EquipmentTypeID", ValueMember:="EquipmentSubTypeID", DisplayMember:="EquipmentSubType")>
    Public Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SerialProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Serial, "Serial", "")
    ''' <summary>
    ''' Gets and sets the Serial value
    ''' </summary>
    <Display(Name:="Serial", Description:="The Serial Number for this piece of equipment"),
    StringLength(50, ErrorMessage:="Serial cannot be more than 50 characters")>
    Public Property Serial() As String
      Get
        Return GetProperty(SerialProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SerialProperty, Value)
      End Set
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Is Active?", True)
    ''' <summary>
    ''' Gets and sets the Active value
    ''' </summary>
    <Display(Name:="Is Active?", Description:="'No' indicates that this piece of equipment is inactive (broken) and/or cannot be booked"),
    Required(ErrorMessage:="Is Active required")>
    Public Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ActiveIndProperty, Value)
      End Set
    End Property

    Public Shared AssetTagNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AssetTagNumber, "Asset Tag Number", "")
    ''' <summary>
    ''' Gets and sets the Asset Tag Number value
    ''' </summary>
    <Display(Name:="Asset Tag Number", Description:="The asset number as used in the asset register"),
    StringLength(50, ErrorMessage:="Asset Tag Number cannot be more than 50 characters")>
    Public Property AssetTagNumber() As String
      Get
        Return GetProperty(AssetTagNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AssetTagNumberProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier this piece of equipment is sourced from"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSupplierList))>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ParentEquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentEquipmentID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property ParentEquipmentID() As Integer?
      Get
        Return GetProperty(ParentEquipmentIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ParentEquipmentIDProperty, value)
      End Set
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International?", False)
    ''' <summary>
    ''' Gets and sets the Active value
    ''' </summary>
    <Display(Name:="International?", Description:="True indicates that this piece of equipment is located internationally")>
    Public Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InternationalIndProperty, Value)
      End Set
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Country", Description:="The country in which the equipment is located"),
    DropDownWeb(GetType(ROCountryList), ValueMember:="CountryID", DisplayMember:="Country")>
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(CountryIDProperty, value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="ResourceID", Description:="The general type for this piece of equipment")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceIDProperty, value)
      End Set
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared EquipmentServiceListProperty As PropertyInfo(Of EquipmentServiceList) = RegisterProperty(Of EquipmentServiceList)(Function(c) c.EquipmentServiceList, "Equipment Service List")
    '    Public ReadOnly Property EquipmentServiceList() As EquipmentServiceList
    '      Get
    '        If GetProperty(EquipmentServiceListProperty) Is Nothing Then
    '          LoadProperty(EquipmentServiceListProperty, OBLib.Equipment.EquipmentServiceList.NewEquipmentServiceList())
    '        End If
    '        Return GetProperty(EquipmentServiceListProperty)
    '      End Get
    '    End Property

    '    Public Shared ChildEquipmentListProperty As PropertyInfo(Of EquipmentList) = RegisterProperty(Of EquipmentList)(Function(c) c.ChildEquipmentList, "Equipment List")
    '    Public ReadOnly Property ChildEquipmentList() As EquipmentList
    '      Get
    '        If GetProperty(ChildEquipmentListProperty) Is Nothing Then
    '          LoadProperty(ChildEquipmentListProperty, OBLib.Equipment.EquipmentList.NewEquipmentList())
    '        End If
    '        Return GetProperty(ChildEquipmentListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EquipmentDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment")
        Else
          Return String.Format("Blank {0}", "Equipment")
        End If
      Else
        Return Me.EquipmentDescription
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"EquipmentServices"}
      End Get
    End Property

    Public Function GetParentList() As EquipmentList
      If Me.Parent IsNot Nothing Then
        Return CType(Me.Parent, EquipmentList)
      Else
        Return Nothing
      End If
    End Function

    Public Function GetParent() As Equipment

      If GetParentList() IsNot Nothing Then
        Return CType(GetParentList().Parent, Equipment)
      Else
        Return Nothing
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(ChildEquipmentListProperty)
      '  .JavascriptRuleFunctionName = "EquipmentBO.ChildEquipmentValid"
      '  .ServerRuleFunction = AddressOf ChildEquipmentValid
      '  .AddTriggerProperty(EquipmentTypeIDProperty)
      'End With

      'With AddWebRule(CountryIDProperty)
      '  .JavascriptRuleFunctionName = "EquipmentBO.CountryIDValid"
      '  .ServerRuleFunction = AddressOf CountryIDValid
      '  .AffectedProperties.Add(InternationalIndProperty)
      '  .AddTriggerProperty(InternationalIndProperty)
      'End With

    End Sub

    Public Function ChildEquipmentValid(Equipment As Equipment) As String
      Dim ErrorMsg As String = ""
      'If Equipment.EquipmentTypeID IsNot Nothing Then
      '  If Equipment.EquipmentTypeID = OBLib.CommonData.Enums.EquipmentType.Combination AndAlso Equipment.ChildEquipmentList.Count = 0 Then
      '    ErrorMsg = "At least one child equipment must be selected"
      '  End If
      'End If
      Return ErrorMsg
    End Function

    Public Function CountryIDValid(Equipment As Equipment) As String
      Dim ErrorMsg As String = ""
      'If Equipment.InternationalInd AndAlso Equipment.CountryID Is Nothing Then
      '  ErrorMsg &= "Country is required"
      'End If
      'If Equipment.InternationalInd AndAlso Equipment.CountryID IsNot Nothing AndAlso Equipment.CountryID = CType(OBLib.CommonData.Enums.Country.SouthAfrica, Integer) Then
      '  ErrorMsg &= "Country cannot be South Africa for international equipment"
      'End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipment() method.

    End Sub

    Public Shared Function NewEquipment() As Equipment

      Return DataPortal.CreateChild(Of Equipment)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipment(dr As SafeDataReader) As Equipment

      Dim e As New Equipment()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentIDProperty, .GetInt32(0))
          LoadProperty(EquipmentDescriptionProperty, .GetString(1))
          LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SerialProperty, .GetString(4))
          LoadProperty(ActiveIndProperty, .GetBoolean(5))
          LoadProperty(AssetTagNumberProperty, .GetString(6))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ParentEquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(InternationalIndProperty, .GetBoolean(13))
          LoadProperty(CountryIDProperty, ZeroNothing(.GetInt32(14)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      If Singular.Security.HasAccess("Equipment.Can Insert Equipment") Then
        ' if we're not dirty then don't update the database
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "InsProcsWeb.insEquipment"
          DoInsertUpdateChild(cm)
        End Using
      End If

    End Sub

    Friend Sub Update()

      If Singular.Security.HasAccess("Equipment.Can Update Equipment") Then
        ' if we're not dirty then don't update the database
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "UpdProcsWeb.updEquipment"
          DoInsertUpdateChild(cm)
        End Using
      End If

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEquipmentID As SqlParameter = .Parameters.Add("@EquipmentID", SqlDbType.Int)
          paramEquipmentID.Value = GetProperty(EquipmentIDProperty)
          Dim paramResourceID As SqlParameter = .Parameters.Add("@ResourceID", SqlDbType.Int)
          paramResourceID.Value = GetProperty(ResourceIDProperty)
          If Me.IsNew Then
            paramEquipmentID.Direction = ParameterDirection.Output
            paramResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentDescription", GetProperty(EquipmentDescriptionProperty))
          .Parameters.AddWithValue("@EquipmentTypeID", GetProperty(EquipmentTypeIDProperty))
          .Parameters.AddWithValue("@EquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(EquipmentSubTypeIDProperty)))
          .Parameters.AddWithValue("@Serial", GetProperty(SerialProperty))
          .Parameters.AddWithValue("@ActiveInd", GetProperty(ActiveIndProperty))
          .Parameters.AddWithValue("@AssetTagNumber", GetProperty(AssetTagNumberProperty))
          .Parameters.AddWithValue("@SupplierID", Singular.Misc.NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          If Me.GetParent IsNot Nothing Then
            .Parameters.AddWithValue("@ParentEquipmentID", Singular.Misc.NothingDBNull(Me.GetParent.EquipmentID))
          Else
            .Parameters.AddWithValue("@ParentEquipmentID", Singular.Misc.NothingDBNull(Nothing))
          End If
          .Parameters.AddWithValue("@InternationalInd", GetProperty(InternationalIndProperty))
          .Parameters.AddWithValue("@CountryID", NothingDBNull(GetProperty(CountryIDProperty)))
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(EquipmentIDProperty, paramEquipmentID.Value)
            LoadProperty(ResourceIDProperty, paramResourceID.Value)
          End If
          '' update child objects
          'If GetProperty(EquipmentServiceListProperty) IsNot Nothing Then
          '  Me.EquipmentServiceList.Update()
          'End If
          'If GetProperty(ChildEquipmentListProperty) IsNot Nothing Then
          '  Me.ChildEquipmentList.Update()
          'End If
          MarkOld()
        End With
      Else
        '' update child objects
        'If GetProperty(ChildEquipmentListProperty) IsNot Nothing Then
        '  Me.ChildEquipmentList.Update()
        'End If
        'If GetProperty(EquipmentServiceListProperty) IsNot Nothing Then
        '  Me.EquipmentServiceList.Update()
        'End If
      End If
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      If Singular.Security.HasAccess("Equipment.Can Delete Equipment") Then
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "DelProcsWeb.delEquipment"
          cm.CommandType = CommandType.StoredProcedure
          cm.Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
          DoDeleteChild(cm)
        End Using
      End If

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace