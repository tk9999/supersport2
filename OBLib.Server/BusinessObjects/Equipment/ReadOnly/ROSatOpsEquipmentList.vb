﻿' Generated 01 Feb 2016 07:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment.ReadOnly

  <Serializable()> _
  Public Class ROSatOpsEquipmentList
    Inherits OBReadOnlyListBase(Of ROSatOpsEquipmentList, ROSatOpsEquipment)

#Region " Business Methods "

    Public Function GetItem(EquipmentID As Integer) As ROSatOpsEquipment

      For Each child As ROSatOpsEquipment In Me
        If child.EquipmentID = EquipmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSatOpsEquipmentList() As ROSatOpsEquipmentList

      Return New ROSatOpsEquipmentList()

    End Function

    Public Shared Sub BeginGetROSatOpsEquipmentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSatOpsEquipmentList)))

      Dim dp As New DataPortal(Of ROSatOpsEquipmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSatOpsEquipmentList(CallBack As EventHandler(Of DataPortalResult(Of ROSatOpsEquipmentList)))

      Dim dp As New DataPortal(Of ROSatOpsEquipmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSatOpsEquipmentList() As ROSatOpsEquipmentList

      Return DataPortal.Fetch(Of ROSatOpsEquipmentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSatOpsEquipment.GetROSatOpsEquipment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSatOpsEquipmentList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace