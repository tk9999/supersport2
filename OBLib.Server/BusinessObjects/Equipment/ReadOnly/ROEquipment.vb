﻿' Generated 02 Mar 2015 15:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment.ReadOnly

  <Serializable()> _
  Public Class ROEquipment
    Inherits OBReadOnlyBase(Of ROEquipment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared EquipmentDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentDescription, "Equipment Description", "")
    ''' <summary>
    ''' Gets the Equipment Description value
    ''' </summary>
    <Display(Name:="Equipment Description", Description:="Description for the piece of equipment")>
    Public ReadOnly Property EquipmentDescription() As String
      Get
        Return GetProperty(EquipmentDescriptionProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Equipment Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="The general type for this piece of equipment")>
    Public ReadOnly Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentType, "Equipment Type", "")
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="The general type for this piece of equipment")>
    Public ReadOnly Property EquipmentType() As String
      Get
        Return GetProperty(EquipmentTypeProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="The specific type for this piece of equipment")>
    Public ReadOnly Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
    End Property

    Public Shared SerialProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Serial, "Serial", "")
    ''' <summary>
    ''' Gets the Serial value
    ''' </summary>
    <Display(Name:="Serial", Description:="The Serial Number for this piece of equipment")>
    Public ReadOnly Property Serial() As String
      Get
        Return GetProperty(SerialProperty)
      End Get
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Active", True)
    ''' <summary>
    ''' Gets the Active value
    ''' </summary>
    <Display(Name:="Active", Description:="False indicates that this piece of equipment is inactive (broken) and cannot be used on a production")>
    Public ReadOnly Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
    End Property

    Public Shared AssetTagNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AssetTagNumber, "Asset Tag Number", "")
    ''' <summary>
    ''' Gets the Asset Tag Number value
    ''' </summary>
    <Display(Name:="Asset Tag Number", Description:="The asset number as used in the asset register")>
    Public ReadOnly Property AssetTagNumber() As String
      Get
        Return GetProperty(AssetTagNumberProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier this piece of equipment is sourced from")>
    Public ReadOnly Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "VehicleID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary> 
    Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Equipment Description value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="ResourceID", Description:="The general type for this piece of equipment")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EquipmentDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEquipment(dr As SafeDataReader) As ROEquipment

      Dim r As New ROEquipment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EquipmentIDProperty, .GetInt32(0))
        LoadProperty(EquipmentDescriptionProperty, .GetString(1))
        LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(SerialProperty, .GetString(4))
        LoadProperty(ActiveIndProperty, .GetBoolean(5))
        LoadProperty(AssetTagNumberProperty, .GetString(6))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        LoadProperty(VehicleIDProperty, .GetInt32(12))
        LoadProperty(VehicleNameProperty, .GetString(13))
        LoadProperty(EquipmentTypeProperty, .GetString(14))
        'LoadProperty(ParentEquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        'LoadProperty(InternationalIndProperty, .GetBoolean(16))
        'LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace