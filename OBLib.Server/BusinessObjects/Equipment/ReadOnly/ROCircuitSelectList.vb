﻿' Generated 16 Apr 2016 13:38 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment.ReadOnly

  <Serializable()> _
  Public Class ROCircuitSelectList
    Inherits OBReadOnlyListBase(Of ROCircuitSelectList, ROCircuitSelect)

#Region " Business Methods "

    Public Function GetItem(EquipmentID As Integer) As ROCircuitSelect

      For Each child As ROCircuitSelect In Me
        If child.EquipmentID = EquipmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      ', Browsable(False) 'Display(AutoGenerateField:=True), 
      <Singular.DataAnnotations.PrimarySearchField>
      Public Property EquipmentName As String
      Public Property StartDateTime As DateTime? = Nothing
      Public Property EndDateTime As DateTime? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROCircuitSelectList() As ROCircuitSelectList

      Return New ROCircuitSelectList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCircuitSelect.GetROCircuitSelect(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROCircuitSelectList]"
            cm.Parameters.AddWithValue("@EquipmentName", Strings.MakeEmptyDBNull(crit.EquipmentName))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace