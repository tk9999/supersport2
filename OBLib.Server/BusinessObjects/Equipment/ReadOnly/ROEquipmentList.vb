﻿' Generated 02 Mar 2015 15:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentList
    Inherits OBReadOnlyListBase(Of ROEquipmentList, ROEquipment)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(EquipmentID As Integer) As ROEquipment

      For Each child As ROEquipment In Me
        If child.EquipmentID = EquipmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipments"

    End Function

#End Region

#Region " Data Access "


    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      'Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      <TextField(False, False, False, 1)>
      Public Property EquipmentName As String = ""
      Public Property AvailableInd As Object = Nothing
      Public Property EquipmentID As Object
      Public Property EquipmentIDs As String = ""
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(EquipmentIDs As String, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.EquipmentIDs = EquipmentIDs
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(EquipmentID As String)
        Me.EquipmentID = EquipmentID
      End Sub

      Public Sub New(EquipmentName As String, AvailableInd As Object)
        Me.EquipmentName = EquipmentName
        Me.AvailableInd = AvailableInd
      End Sub

      Public Sub New()
      End Sub

    End Class


#Region " Common "

    Public Shared Function NewROEquipmentList() As ROEquipmentList

      Return New ROEquipmentList()

    End Function

    Public Shared Sub BeginGetROEquipmentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentList)))

      Dim dp As New DataPortal(Of ROEquipmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentList)))

      Dim dp As New DataPortal(Of ROEquipmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentList() As ROEquipmentList

      Return DataPortal.Fetch(Of ROEquipmentList)(New Criteria())

    End Function

    'Public Shared Function GetSatOpsEquipmentList(SystemID As Integer?, ProductionAreaID As Integer?) As ROEquipmentList
    '  Dim sids As Integer() = {1435, 1436, 1437, 1438, 1439, 1440}
    '  Dim EquipmentIDs As String = OBLib.Helpers.MiscHelper.IntegerArrayToXML(sids)
    '  Return DataPortal.Fetch(Of ROEquipmentList)(New Criteria(EquipmentIDs, SystemID, ProductionAreaID))

    'End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()


      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipment.GetROEquipment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEquipmentList"
            cm.Parameters.AddWithValue("@FilterName", Singular.Strings.MakeEmptyDBNull(crit.EquipmentName))
            cm.Parameters.AddWithValue("@AvailableInd", Singular.Misc.NothingDBNull(crit.AvailableInd))
            cm.Parameters.AddWithValue("@EquipmentID", Singular.Misc.NothingDBNull(crit.EquipmentID))
            cm.Parameters.AddWithValue("@EquipmentIDs", Singular.Strings.MakeEmptyDBNull(crit.EquipmentIDs))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace