﻿' Generated 01 Feb 2016 07:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment.ReadOnly

  <Serializable()> _
  Public Class ROSatOpsEquipment
    Inherits OBReadOnlyBase(Of ROSatOpsEquipment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared EquipmentDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentDescription, "Equipment Description")
    ''' <summary>
    ''' Gets the Equipment Description value
    ''' </summary>
    <Display(Name:="Equipment Description", Description:="")>
  Public ReadOnly Property EquipmentDescription() As String
      Get
        Return GetProperty(EquipmentDescriptionProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentTypeID, "Equipment Type")
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="")>
  Public ReadOnly Property EquipmentTypeID() As Integer
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentSubTypeID, "Equipment Sub Type")
    ''' <summary>
    ''' Gets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="")>
  Public ReadOnly Property EquipmentSubTypeID() As Integer
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
    End Property

    Public Shared SerialProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Serial, "Serial")
    ''' <summary>
    ''' Gets the Serial value
    ''' </summary>
    <Display(Name:="Serial", Description:="")>
  Public ReadOnly Property Serial() As String
      Get
        Return GetProperty(SerialProperty)
      End Get
    End Property

    Public Shared ActiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActiveInd, "Active", False)
    ''' <summary>
    ''' Gets the Active value
    ''' </summary>
    <Display(Name:="Active", Description:="")>
  Public ReadOnly Property ActiveInd() As Boolean
      Get
        Return GetProperty(ActiveIndProperty)
      End Get
    End Property

    Public Shared AssetTagNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AssetTagNumber, "Asset Tag Number")
    ''' <summary>
    ''' Gets the Asset Tag Number value
    ''' </summary>
    <Display(Name:="Asset Tag Number", Description:="")>
  Public ReadOnly Property AssetTagNumber() As String
      Get
        Return GetProperty(AssetTagNumberProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
  Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ParentEquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentEquipmentID, "Parent Equipment")
    ''' <summary>
    ''' Gets the Parent Equipment value
    ''' </summary>
    <Display(Name:="Parent Equipment", Description:="")>
  Public ReadOnly Property ParentEquipmentID() As Integer
      Get
        Return GetProperty(ParentEquipmentIDProperty)
      End Get
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets the International value
    ''' </summary>
    <Display(Name:="International", Description:="")>
  Public ReadOnly Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "Country")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
  Public ReadOnly Property CountryID() As Integer
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentType, "Equipment Type")
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="")>
  Public ReadOnly Property EquipmentType() As String
      Get
        Return GetProperty(EquipmentTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EquipmentDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSatOpsEquipment(dr As SafeDataReader) As ROSatOpsEquipment

      Dim r As New ROSatOpsEquipment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EquipmentIDProperty, .GetInt32(0))
        LoadProperty(EquipmentDescriptionProperty, .GetString(1))
        LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(SerialProperty, .GetString(4))
        LoadProperty(ActiveIndProperty, .GetBoolean(5))
        LoadProperty(AssetTagNumberProperty, .GetString(6))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
        LoadProperty(ParentEquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(InternationalIndProperty, .GetBoolean(13))
        LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(EquipmentTypeProperty, .GetString(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace