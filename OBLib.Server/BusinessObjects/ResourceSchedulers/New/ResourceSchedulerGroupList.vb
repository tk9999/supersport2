﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.New

  <Serializable()> _
  Public Class ResourceSchedulerGroupList
    Inherits OBBusinessListBase(Of ResourceSchedulerGroupList, ResourceSchedulerGroup)

#Region " Business Methods "

    Public Function GetItem(ResourceSchedulerGroupID As Integer) As ResourceSchedulerGroup

      For Each child As ResourceSchedulerGroup In Me
        If child.ResourceSchedulerGroupID = ResourceSchedulerGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Scheduler Groups"

    End Function

    Public Function GetResourceSchedulerSubGroup(ResourceSchedulerSubGroupID As Integer) As ResourceSchedulerSubGroup

      Dim obj As ResourceSchedulerSubGroup = Nothing
      For Each parent As ResourceSchedulerGroup In Me
        obj = parent.ResourceSchedulerSubGroupList.GetItem(ResourceSchedulerSubGroupID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function GetResourceSchedulerGroupList(ResourceSchedulerGroupID As Integer?) As ResourceSchedulerGroupList
      Return DataPortal.Fetch(Of ResourceSchedulerGroupList)(New Criteria(ResourceSchedulerGroupID))
    End Function

    Public Shared Function NewResourceSchedulerGroupList() As ResourceSchedulerGroupList

      Return New ResourceSchedulerGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property ResourceSchedulerGroupID As Integer? = Nothing
      Public Property UserID As Integer? = Nothing
      Public Property FetchSubGroups As Boolean = False
      Public Property FetchSubGroupResources As Boolean = False

      Public Sub New(ResourceSchedulerGroupID As Integer?)
        Me.ResourceSchedulerGroupID = ResourceSchedulerGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceSchedulerGroup.GetResourceSchedulerGroup(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As ResourceScheduler = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ResourceSchedulerID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ResourceSchedulerGroupList.RaiseListChangedEvents = False
      '    parent.ResourceSchedulerGroupList.Add(ResourceSchedulerGroup.GetResourceSchedulerGroup(sdr))
      '    parent.ResourceSchedulerGroupList.RaiseListChangedEvents = True
      '  End While
      'End If

      Dim parent As ResourceSchedulerGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceSchedulerGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ResourceSchedulerSubGroupList.RaiseListChangedEvents = False
          parent.ResourceSchedulerSubGroupList.Add(ResourceSchedulerSubGroup.GetResourceSchedulerSubGroup(sdr))
          parent.ResourceSchedulerSubGroupList.RaiseListChangedEvents = True
        End While
      End If

      'Dim parentGrandChild As ResourceSchedulerSubGroup = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentGrandChild Is Nothing OrElse parentGrandChild.ResourceSchedulerSubGroupID <> sdr.GetInt32(1) Then
      '      parentGrandChild = Me.GetResourceSchedulerSubGroup(sdr.GetInt32(1))
      '    End If
      '    parentGrandChild.ResourceSchedulerSubGroupResourceList.RaiseListChangedEvents = False
      '    parentGrandChild.ResourceSchedulerSubGroupResourceList.Add(ResourceSchedulerSubGroupResource.GetResourceSchedulerSubGroupResource(sdr))
      '    parentGrandChild.ResourceSchedulerSubGroupResourceList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As ResourceSchedulerGroup In Me
        child.CheckRules()
        For Each ResourceSchedulerSubGroup As ResourceSchedulerSubGroup In child.ResourceSchedulerSubGroupList
          ResourceSchedulerSubGroup.CheckRules()
          For Each ResourceSchedulerSubGroupResource As ResourceSchedulerSubGroupResource In ResourceSchedulerSubGroup.ResourceSchedulerSubGroupResourceList
            ResourceSchedulerSubGroupResource.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceSchedulerGroupList"
            cm.Parameters.AddWithValue("@ResourceSchedulerID", NothingDBNull(crit.ResourceSchedulerID))
            cm.Parameters.AddWithValue("@ResourceSchedulerGroupID", NothingDBNull(crit.ResourceSchedulerGroupID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@FetchSubGroups", crit.FetchSubGroups)
            cm.Parameters.AddWithValue("@FetchSubGroupResources", crit.FetchSubGroupResources)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Public Function UpdateOrdering() As Singular.Web.Result

      Try
        'Groups
        Dim GroupsString As String = ""
        GroupsString = (New XElement("DataSet", (From group In Me
                                                      Select New XElement("Table",
                                                                          New XAttribute("ResourceSchedulerGroupID", group.ResourceSchedulerGroupID),
                                                                          New XAttribute("GroupOrder", group.GroupOrder)
                                                     )).ToArray())).ToString


        'Sub Groups
        Dim sgl As New ResourceSchedulerSubGroupList
        For Each grp As ResourceSchedulerGroup In Me
          sgl.AddRange(grp.ResourceSchedulerSubGroupList)
        Next
        Dim SubGroupsString As String = ""
        SubGroupsString = (New XElement("DataSet", (From subGroup In sgl
                                                      Select New XElement("Table",
                                                                          New XAttribute("ResourceSchedulerSubGroupID", subGroup.ResourceSchedulerSubGroupID),
                                                                          New XAttribute("SubGroupOrder", subGroup.SubGroupOrder)
                                                     )).ToArray())).ToString


        'Resources
        Dim sgrl As New ResourceSchedulerSubGroupResourceList
        For Each sgr As ResourceSchedulerSubGroup In sgl
          sgrl.AddRange(sgr.ResourceSchedulerSubGroupResourceList)
        Next
        Dim ResourcesString As String = ""
        ResourcesString = (New XElement("DataSet", (From subGroupResource In sgrl
                                                      Select New XElement("Table",
                                                                          New XAttribute("ResourceSchedulerSubGroupResourceID", subGroupResource.ResourceSchedulerSubGroupResourceID),
                                                                          New XAttribute("ResourceOrder", subGroupResource.ResourceOrder)
                                                     )).ToArray())).ToString

        Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerUpdateOrdering",
                                            New String() {"@GroupsXML", "@SubGroupsXML", "@SubGroupResourcesXML", "@ModifiedBy"},
                                            New Object() {GroupsString, SubGroupsString, ResourcesString, OBLib.Security.Settings.CurrentUserID})
        cmd = cmd.Execute
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

#End Region

  End Class

End Namespace