﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.New

  <Serializable()> _
  Public Class ResourceSchedulerList
    Inherits OBBusinessListBase(Of ResourceSchedulerList, ResourceScheduler)

#Region " Business Methods "

    Public Function GetItem(ResourceSchedulerID As Integer) As ResourceScheduler

      For Each child As ResourceScheduler In Me
        If child.ResourceSchedulerID = ResourceSchedulerID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Schedulers"

    End Function

    Public Function GetResourceSchedulerGroup(ResourceSchedulerGroupID As Integer) As ResourceSchedulerGroup

      Dim obj As ResourceSchedulerGroup = Nothing
      For Each parent As ResourceScheduler In Me
        obj = parent.ResourceSchedulerGroupList.GetItem(ResourceSchedulerGroupID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetResourceSchedulerSubGroup(ResourceSchedulerSubGroupID As Integer) As ResourceSchedulerSubGroup

      Dim obj As ResourceSchedulerSubGroup = Nothing
      For Each parent As ResourceScheduler In Me
        For Each child As ResourceSchedulerGroup In parent.ResourceSchedulerGroupList
          obj = child.ResourceSchedulerSubGroupList.GetItem(ResourceSchedulerSubGroupID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property UserID As Integer? = Nothing
      Public Property FetchGroups As Boolean = True
      Public Property FetchSubGroups As Boolean = True
      Public Property FetchSubGroupResources As Boolean = True

      Public Sub New(ResourceSchedulerID As Integer?, UserID As Integer?, FetchGroups As Boolean, FetchSubGroups As Boolean, FetchSubGroupResources As Boolean)
        Me.ResourceSchedulerID = ResourceSchedulerID
        Me.UserID = UserID
        Me.FetchGroups = FetchGroups
        Me.FetchSubGroups = FetchSubGroups
        Me.FetchSubGroupResources = FetchSubGroupResources
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewResourceSchedulerList() As ResourceSchedulerList

      Return New ResourceSchedulerList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetResourceSchedulerList() As ResourceSchedulerList

      Return DataPortal.Fetch(Of ResourceSchedulerList)(New Criteria())

    End Function

    Public Shared Function GetResourceSchedulerList(ResourceSchedulerID As Integer?, USerID As Integer?, FetchGroups As Boolean, FetchSubGroups As Boolean, FetchSubGroupResources As Boolean) As ResourceSchedulerList
      Return DataPortal.Fetch(Of ResourceSchedulerList)(New Criteria(ResourceSchedulerID, USerID, FetchGroups, FetchSubGroups, FetchSubGroupResources))
    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceScheduler.GetResourceScheduler(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ResourceScheduler = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceSchedulerID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ResourceSchedulerGroupList.RaiseListChangedEvents = False
          parent.ResourceSchedulerGroupList.Add(ResourceSchedulerGroup.GetResourceSchedulerGroup(sdr))
          parent.ResourceSchedulerGroupList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ResourceSchedulerGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.ResourceSchedulerGroupID <> sdr.GetInt32(1) Then
            parentChild = Me.GetResourceSchedulerGroup(sdr.GetInt32(1))
          End If
          parentChild.ResourceSchedulerSubGroupList.RaiseListChangedEvents = False
          parentChild.ResourceSchedulerSubGroupList.Add(ResourceSchedulerSubGroup.GetResourceSchedulerSubGroup(sdr))
          parentChild.ResourceSchedulerSubGroupList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentGrandChild As ResourceSchedulerSubGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentGrandChild Is Nothing OrElse parentGrandChild.ResourceSchedulerSubGroupID <> sdr.GetInt32(1) Then
            parentGrandChild = Me.GetResourceSchedulerSubGroup(sdr.GetInt32(1))
          End If
          parentGrandChild.ResourceSchedulerSubGroupResourceList.RaiseListChangedEvents = False
          parentGrandChild.ResourceSchedulerSubGroupResourceList.Add(ResourceSchedulerSubGroupResource.GetResourceSchedulerSubGroupResource(sdr))
          parentGrandChild.ResourceSchedulerSubGroupResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As ResourceScheduler In Me
        child.CheckRules()
        For Each ResourceSchedulerGroup As ResourceSchedulerGroup In child.ResourceSchedulerGroupList
          ResourceSchedulerGroup.CheckRules()

          For Each ResourceSchedulerSubGroup As ResourceSchedulerSubGroup In ResourceSchedulerGroup.ResourceSchedulerSubGroupList
            ResourceSchedulerSubGroup.CheckRules()

            For Each ResourceSchedulerSubGroupResource As ResourceSchedulerSubGroupResource In ResourceSchedulerSubGroup.ResourceSchedulerSubGroupResourceList
              ResourceSchedulerSubGroupResource.CheckRules()
            Next
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceSchedulerList"
            cm.Parameters.AddWithValue("@ResourceSchedulerID", NothingDBNull(crit.ResourceSchedulerID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@FetchGroups", crit.FetchGroups)
            cm.Parameters.AddWithValue("@FetchSubGroups", crit.FetchSubGroups)
            cm.Parameters.AddWithValue("@FetchSubGroupResources", crit.FetchSubGroupResources)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace