﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Resources.ReadOnly
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.TeamManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

Namespace ResourceSchedulers.New

  <Serializable()>
  Public Class ResourceSchedulerSubGroup
    Inherits OBBusinessBase(Of ResourceSchedulerSubGroup)

#Region " Properties and Methods "

#Region " Properties "

    <AlwaysClean>
    Public Property IsExpanded As Boolean = False

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ResourceSchedulerSubGroupBO.ResourceSchedulerSubGroupBOToString(self)")

    Public Shared ResourceSchedulerSubGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerSubGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceSchedulerSubGroupID() As Integer
      Get
        Return GetProperty(ResourceSchedulerSubGroupIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceSchedulerSubGroupIDProperty, value)
      End Set
    End Property

    Public Shared ResourceSchedulerGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceSchedulerGroupID, "Resource Scheduler Group", Nothing)
    ''' <summary>
    ''' Gets the Resource Scheduler Group value
    ''' </summary>
    Public Property ResourceSchedulerGroupID() As Integer?
      Get
        Return GetProperty(ResourceSchedulerGroupIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceSchedulerGroupIDProperty, value)
      End Set
    End Property

    Public Shared ResourceSchedulerSubGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceSchedulerSubGroup, "Resource Scheduler Sub Group", "")
    ''' <summary>
    ''' Gets and sets the Resource Scheduler Sub Group value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(150, ErrorMessage:="Sub Group Name cannot be more than 150 characters"),
    Required(AllowEmptyStrings:=False)>
    Public Property ResourceSchedulerSubGroup() As String
      Get
        Return GetProperty(ResourceSchedulerSubGroupProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceSchedulerSubGroupProperty, Value)
      End Set
    End Property

    Public Shared SubGroupOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubGroupOrder, "Sub Group Order", 99)
    ''' <summary>
    ''' Gets and sets the Sub Group Order value
    ''' </summary>
    <Display(Name:="Order", Description:=""),
    Required(ErrorMessage:="Sub Group Order required")>
    Public Property SubGroupOrder() As Integer
      Get
        Return GetProperty(SubGroupOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SubGroupOrderProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect),
                BeforeFetchJS:="ResourceSchedulerSubGroupBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ResourceSchedulerSubGroupBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="ResourceSchedulerSubGroupBO.afterDisciplineIDRefreshAjax",
                OnItemSelectJSFunction:="ResourceSchedulerSubGroupBO.onDisciplineIDSelected",
                LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Discipline"},
                UnselectedText:="Discipline"),
    SetExpression("ResourceSchedulerSubGroupBO.DisciplineIDSet(self)")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared FreelancersProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.Freelancers, "Contract Type", CType(Nothing, Boolean))
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Contract Type", Description:=""),
    SetExpression("ResourceSchedulerSubGroupBO.FreelancersSet(self)")>
    Public Property Freelancers() As Boolean?
      Get
        Return GetProperty(FreelancersProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(FreelancersProperty, Value)
      End Set
    End Property

    'Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Contract Type value
    ' ''' </summary>
    '<Display(Name:="Contract Type", Description:=""),
    'DropDownWeb(GetType(ROContractTypeList))>
    'Public Property ContractTypeID() As Integer?
    '  Get
    '    Return GetProperty(ContractTypeIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ContractTypeIDProperty, Value)
    '  End Set
    'End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:=""),
    DropDownWeb(GetType(RORoomTypeList), UnselectedText:="Type"),
    SetExpression("ResourceSchedulerSubGroupBO.RoomTypeIDSet(self)")>
    Public Property RoomTypeID() As Integer?
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Selected By Default?", False)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Selected By Default?", Description:=""),
    SetExpression("ResourceSchedulerSubGroupBO.SelectedByDefaultSet(self)")>
    Public Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedByDefaultProperty, Value)
      End Set
    End Property

    Public Shared ParentResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentResourceTypeID, "Parent Resource Type ID", Nothing)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Parent Resource Type ID", Description:="")>
    Public Property ParentResourceTypeID() As Integer?
      Get
        Return GetProperty(ParentResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HRProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HRProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList), ValueMember:="ProductionAreaID", DisplayMember:="ProductionArea", UnselectedText:="Area"),
    SetExpression("ResourceSchedulerSubGroupBO.HRProductionAreaIDSet(self)")>
    Public Property HRProductionAreaID() As Integer?
      Get
        Return GetProperty(HRProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HRProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    DropDownWeb(GetType(ROEquipmentTypeList), UnselectedText:="Type"),
    SetExpression("ResourceSchedulerSubGroupBO.EquipmentTypeIDSet(self)")>
    Public Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CustomResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    DropDownWeb(GetType(ROCustomResourceList))>
    Public Property CustomResourceID() As Integer?
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CustomResourceIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleTypeID, "VehicleTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:=""),
    DropDownWeb(GetType(ROVehicleTypeList),
                BeforeFetchJS:="ResourceSchedulerSubGroupBO.setVehicleTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ResourceSchedulerSubGroupBO.triggerVehicleTypeIDAutoPopulate",
                AfterFetchJS:="ResourceSchedulerSubGroupBO.afterVehicleTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ResourceSchedulerSubGroupBO.onVehicleTypeIDSelected",
                LookupMember:="VehicleType", DisplayMember:="VehicleType", ValueMember:="VehicleTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"VehicleType"}),
    SetExpression("ResourceSchedulerSubGroupBO.VehicleTypeIDSet(self)")>
    Public Property VehicleTypeID() As Integer?
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
    Public Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VehicleTypeProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamNumberID, "SystemTeamNumberID", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Team Number", Description:=""),
    DropDownWeb(GetType(ROSystemTeamNumberList),
                BeforeFetchJS:="ResourceSchedulerSubGroupBO.setSystemTeamNumberIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ResourceSchedulerSubGroupBO.triggerSystemTeamNumberIDAutoPopulate",
                AfterFetchJS:="ResourceSchedulerSubGroupBO.afterSystemTeamNumberIDRefreshAjax",
                OnItemSelectJSFunction:="ResourceSchedulerSubGroupBO.onSystemTeamNumberIDSelected",
                LookupMember:="SystemTeamNumber", DisplayMember:="SystemTeamNumber", ValueMember:="SystemTeamNumberID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"SystemTeamNumber", "SystemTeamNumberName"}),
    SetExpression("ResourceSchedulerSubGroupBO.SystemTeamNumberIDSet(self)")>
    Public Property SystemTeamNumberID() As Integer?
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamNumberIDProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemTeamNumber, "Team Number", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Team Number")>
    Public Property SystemTeamNumber() As String
      Get
        Return GetProperty(SystemTeamNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemTeamNumberProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ResourceSchedulerSubGroupResourceListProperty As PropertyInfo(Of ResourceSchedulerSubGroupResourceList) = RegisterProperty(Of ResourceSchedulerSubGroupResourceList)(Function(c) c.ResourceSchedulerSubGroupResourceList, "Resource Scheduler Sub Group Resource List")
    Public ReadOnly Property ResourceSchedulerSubGroupResourceList() As ResourceSchedulerSubGroupResourceList
      Get
        If GetProperty(ResourceSchedulerSubGroupResourceListProperty) Is Nothing Then
          LoadProperty(ResourceSchedulerSubGroupResourceListProperty, ResourceSchedulers.New.ResourceSchedulerSubGroupResourceList.NewResourceSchedulerSubGroupResourceList())
        End If
        Return GetProperty(ResourceSchedulerSubGroupResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ResourceSchedulerGroup

      Return CType(CType(Me.Parent, ResourceSchedulerSubGroupList).Parent, ResourceSchedulerGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceSchedulerSubGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceSchedulerSubGroup.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Scheduler Sub Group")
        Else
          Return String.Format("Blank {0}", "Resource Scheduler Sub Group")
        End If
      Else
        Return Me.ResourceSchedulerSubGroup
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ResourceSchedulerSubGroupResources"}
      End Get
    End Property

    Public Function PopulateResourcesForSubGroup(SystemID As Integer?,
                                                 ResourceTypeID As Integer?,
                                                 SiteID As Integer?) As Web.Result

      Dim cmd As New Singular.CommandProc("",
                                          New String() {"@SystemID", "@ResourceTypeID", "@SiteID",
                                                        "@DisciplineID", "@Freelancers",
                                                        "@RoomTypeID", "@HRProductionAreaID",
                                                        "@EquipmentTypeID"},
                                          New Object() {
                                                          NothingDBNull(SystemID),
                                                          NothingDBNull(ResourceTypeID),
                                                          NothingDBNull(SiteID),
                                                          NothingDBNull(GetProperty(DisciplineIDProperty)),
                                                          NothingDBNull(GetProperty(FreelancersProperty)),
                                                          NothingDBNull(GetProperty(RoomTypeIDProperty)),
                                                          NothingDBNull(GetProperty(HRProductionAreaIDProperty)),
                                                          NothingDBNull(GetProperty(EquipmentTypeIDProperty))
                                                       })

      'cm.Parameters.AddWithValue("@ResourceSchedulerSubGroup", GetProperty(ResourceSchedulerSubGroupProperty))
      'cm.Parameters.AddWithValue("@SubGroupOrder", GetProperty(SubGroupOrderProperty))
      'cm.Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
      'cm.Parameters.AddWithValue("@Freelancers", Singular.Misc.NothingDBNull(GetProperty(FreelancersProperty)))
      'cm.Parameters.AddWithValue("@RoomTypeID", Singular.Misc.NothingDBNull(GetProperty(RoomTypeIDProperty)))
      'cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      'cm.Parameters.AddWithValue("@HRProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(HRProductionAreaIDProperty)))
      'cm.Parameters.AddWithValue("@EquipmentTypeID", Singular.Misc.NothingDBNull(GetProperty(EquipmentTypeIDProperty)))

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceSchedulerSubGroup() method.

    End Sub

    Public Shared Function NewResourceSchedulerSubGroup() As ResourceSchedulerSubGroup

      Return DataPortal.CreateChild(Of ResourceSchedulerSubGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetResourceSchedulerSubGroup(dr As SafeDataReader) As ResourceSchedulerSubGroup

      Dim r As New ResourceSchedulerSubGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceSchedulerSubGroupIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceSchedulerSubGroupProperty, .GetString(2))
          LoadProperty(SubGroupOrderProperty, .GetInt32(3))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(FreelancersProperty, IIf(.GetValue(5) Is Nothing, Nothing, .GetBoolean(5)))
          LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(11))
          LoadProperty(ParentResourceTypeIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(HRProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(EquipmentTypeIDProperty, ZeroNothing(.GetInt32(14)))
          LoadProperty(DisciplineProperty, .GetString(15))
          LoadProperty(CustomResourceIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(VehicleTypeIDProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(VehicleTypeProperty, .GetString(18))
          LoadProperty(SystemTeamNumberIDProperty, ZeroNothing(.GetInt32(19)))
          LoadProperty(SystemTeamNumberProperty, .GetString(20))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ResourceSchedulerSubGroupIDProperty)

      If Me.GetParent() Is Nothing Then
        cm.Parameters.AddWithValue("@ResourceSchedulerGroupID", GetProperty(ResourceSchedulerGroupIDProperty))
      Else
        cm.Parameters.AddWithValue("@ResourceSchedulerGroupID", Me.GetParent().ResourceSchedulerGroupID)
      End If
      cm.Parameters.AddWithValue("@ResourceSchedulerSubGroup", GetProperty(ResourceSchedulerSubGroupProperty))
      cm.Parameters.AddWithValue("@SubGroupOrder", GetProperty(SubGroupOrderProperty))
      cm.Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
      cm.Parameters.AddWithValue("@Freelancers", Singular.Misc.NothingDBNull(GetProperty(FreelancersProperty)))
      cm.Parameters.AddWithValue("@RoomTypeID", Singular.Misc.NothingDBNull(GetProperty(RoomTypeIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@HRProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(HRProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@EquipmentTypeID", Singular.Misc.NothingDBNull(GetProperty(EquipmentTypeIDProperty)))
      cm.Parameters.AddWithValue("@SelectedByDefault", GetProperty(SelectedByDefaultProperty))
      cm.Parameters.AddWithValue("@CustomResourceID", Singular.Misc.NothingDBNull(GetProperty(CustomResourceIDProperty)))
      cm.Parameters.AddWithValue("@VehicleTypeID", Singular.Misc.NothingDBNull(GetProperty(VehicleTypeIDProperty)))
      cm.Parameters.AddWithValue("@SystemTeamNumberID", Singular.Misc.NothingDBNull(GetProperty(SystemTeamNumberIDProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ResourceSchedulerSubGroupIDProperty, cm.Parameters("@ResourceSchedulerSubGroupID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(ResourceSchedulerSubGroupResourceListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ResourceSchedulerSubGroupID", GetProperty(ResourceSchedulerSubGroupIDProperty))
    End Sub

#End Region

  End Class

End Namespace