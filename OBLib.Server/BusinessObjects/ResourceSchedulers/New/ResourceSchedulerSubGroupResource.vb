﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.New

  <Serializable()>
  Public Class ResourceSchedulerSubGroupResource
    Inherits OBBusinessBase(Of ResourceSchedulerSubGroupResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ResourceSchedulerSubGroupResourceBO.ResourceSchedulerSubGroupResourceBOToString(self)")

    Public Shared ResourceSchedulerSubGroupResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerSubGroupResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceSchedulerSubGroupResourceID() As Integer
      Get
        Return GetProperty(ResourceSchedulerSubGroupResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceSchedulerSubGroupResourceIDProperty, value)
      End Set
    End Property

    Public Shared ResourceSchedulerSubGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceSchedulerSubGroupID, "Resource Scheduler Sub Group", Nothing)
    ''' <summary>
    ''' Gets the Resource Scheduler Sub Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ResourceSchedulerSubGroupID() As Integer?
      Get
        Return GetProperty(ResourceSchedulerSubGroupIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceOrder, "Resource Order", 0)
    ''' <summary>
    ''' Gets and sets the Resource Order value
    ''' </summary>
    <Display(Name:="Resource Order", Description:=""),
    Required(ErrorMessage:="Resource Order required")>
    Public Property ResourceOrder() As Integer
      Get
        Return GetProperty(ResourceOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceOrderProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Name", "")
    ''' <summary>
    ''' Gets and sets the Resource Scheduler Sub Group value
    ''' </summary>
    <Display(Name:="Name", Description:="")>
    Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameProperty, Value)
      End Set
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Default Selection", False)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Default Selection", Description:="")>
    Public Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedByDefaultProperty, Value)
      End Set
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Resource Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CustomResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property CustomResourceID() As Integer?
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CustomResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CanAddBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanAddBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Add Bookings", Description:="")>
    Public Property CanAddBookings() As Boolean
      Get
        Return GetProperty(CanAddBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanAddBookingsProperty, Value)
      End Set
    End Property

    Public Shared CanMoveBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanMoveBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Move Bookings", Description:="")>
    Public Property CanMoveBookings() As Boolean
      Get
        Return GetProperty(CanMoveBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanMoveBookingsProperty, Value)
      End Set
    End Property

    Public Shared CanDeleteBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanDeleteBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Delete Bookings", Description:="")>
    Public Property CanDeleteBookings() As Boolean
      Get
        Return GetProperty(CanDeleteBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanDeleteBookingsProperty, Value)
      End Set
    End Property

    Public Shared RoomOwnedBySystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomOwnedBySystemID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property RoomOwnedBySystemID() As Integer?
      Get
        Return GetProperty(RoomOwnedBySystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomOwnedBySystemIDProperty, Value)
      End Set
    End Property

    Public Shared RoomOwnedByProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomOwnedByProductionAreaID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property RoomOwnedByProductionAreaID() As Integer?
      Get
        Return GetProperty(RoomOwnedByProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomOwnedByProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "ContractType", Nothing)
    ''' <summary>
    ''' Gets and sets the ContractType value
    ''' </summary>
    <Display(Name:="ContractType")>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.IsFreelancer, "Is Freelancer", CType(Nothing, Boolean))
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Freelancer")>
    Public Property IsFreelancer() As Boolean?
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(IsFreelancerProperty, Value)
      End Set
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShortNameProperty, Value)
      End Set
    End Property

    Public Shared ReqShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReqShifts, "Req Shifts")
    ''' <summary>
    ''' Gets and sets the Req Shifts value
    ''' </summary>
    <Display(Name:="Req Shifts", Description:="")>
    Public Property ReqShifts() As Integer
      Get
        Return GetProperty(ReqShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReqShiftsProperty, Value)
      End Set
    End Property

    Public Shared ReqHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReqHours, "Req Hours")
    ''' <summary>
    ''' Gets and sets the Req Hours value
    ''' </summary>
    <Display(Name:="Req Hours", Description:="")>
    Public Property ReqHours() As Integer
      Get
        Return GetProperty(ReqHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReqHoursProperty, Value)
      End Set
    End Property

    Public Shared TotalShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShifts, "Total Shifts")
    ''' <summary>
    ''' Gets and sets the Total Shifts value
    ''' </summary>
    <Display(Name:="Total Shifts", Description:="")>
    Public Property TotalShifts() As Integer
      Get
        Return GetProperty(TotalShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TotalShiftsProperty, Value)
      End Set
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Total Hours")
    ''' <summary>
    ''' Gets and sets the Total Hours value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
    Public Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalHoursProperty, Value)
      End Set
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours")
    ''' <summary>
    ''' Gets and sets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime Hours", Description:="")>
    Public Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(OvertimeHoursProperty, Value)
      End Set
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours")
    ''' <summary>
    ''' Gets and sets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall Hours", Description:="")>
    Public Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShortfallHoursProperty, Value)
      End Set
    End Property

    Public Shared PrevBalanceHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PrevBalanceHours, "Prev Balance Hours")
    ''' <summary>
    ''' Gets and sets the Prev Balance Hours value
    ''' </summary>
    <Display(Name:="Prev Balance Hours", Description:="")>
    Public Property PrevBalanceHours() As Decimal
      Get
        Return GetProperty(PrevBalanceHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(PrevBalanceHoursProperty, Value)
      End Set
    End Property

    Public ReadOnly Property PercUsed As Decimal
      Get
        If Me.ReqShifts > 0 And Me.ReqHours > 0 Then
          Dim ShiftUtil As Decimal = (Me.TotalShifts / Me.ReqShifts) * 100
          Dim HrsUtil As Decimal = (Me.TotalHours / Me.ReqHours) * 100
          If ShiftUtil > HrsUtil Then
            Return ShiftUtil
          Else
            Return HrsUtil
          End If
        End If
        Return 0
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ResourceSchedulerSubGroup

      Return CType(CType(Me.Parent, ResourceSchedulerSubGroupResourceList).Parent, ResourceSchedulerSubGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceSchedulerSubGroupResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Scheduler Sub Group Resource")
        Else
          Return String.Format("Blank {0}", "Resource Scheduler Sub Group Resource")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceSchedulerSubGroupResource() method.

    End Sub

    Public Shared Function NewResourceSchedulerSubGroupResource() As ResourceSchedulerSubGroupResource

      Return DataPortal.CreateChild(Of ResourceSchedulerSubGroupResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetResourceSchedulerSubGroupResource(dr As SafeDataReader) As ResourceSchedulerSubGroupResource

      Dim r As New ResourceSchedulerSubGroupResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Dim IsNewOverride As Boolean = False
      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceSchedulerSubGroupResourceIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerSubGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceOrderProperty, .GetInt32(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ResourceNameProperty, .GetString(8))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(9))
          LoadProperty(ResourceTypeIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(HumanResourceIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(VehicleIDProperty, ZeroNothing(.GetInt32(13)))
          LoadProperty(EquipmentIDProperty, ZeroNothing(.GetInt32(14)))
          LoadProperty(ChannelIDProperty, ZeroNothing(.GetInt32(15)))
          LoadProperty(CustomResourceIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(ContractTypeIDProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(IsFreelancerProperty, .GetBoolean(18))
          LoadProperty(ReqShiftsProperty, .GetInt32(19))
          LoadProperty(ReqHoursProperty, .GetInt32(20))
          LoadProperty(TotalShiftsProperty, .GetInt32(21))
          LoadProperty(TotalHoursProperty, .GetDecimal(22))
          LoadProperty(OvertimeHoursProperty, .GetDecimal(23))
          LoadProperty(ShortfallHoursProperty, .GetDecimal(24))
          LoadProperty(PrevBalanceHoursProperty, .GetDecimal(25))
          LoadProperty(CanAddBookingsProperty, .GetBoolean(26))
          LoadProperty(CanMoveBookingsProperty, .GetBoolean(27))
          LoadProperty(CanDeleteBookingsProperty, .GetBoolean(28))
          LoadProperty(RoomOwnedBySystemIDProperty, ZeroNothing(.GetInt32(29)))
          LoadProperty(RoomOwnedByProductionAreaIDProperty, ZeroNothing(.GetInt32(30)))
          LoadProperty(ShortNameProperty, .GetString(31))
          IsNewOverride = .GetBoolean(32)
        End With
      End Using

      MarkAsChild()
      If IsNewOverride Then
        MarkNew()
        MarkDirty()
      Else
        MarkOld()
      End If
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ResourceSchedulerSubGroupResourceIDProperty)

      cm.Parameters.AddWithValue("@ResourceSchedulerSubGroupID", Me.GetParent().ResourceSchedulerSubGroupID)
      cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceOrder", GetProperty(ResourceOrderProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      cm.Parameters.AddWithValue("@SelectedByDefault", GetProperty(SelectedByDefaultProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ResourceSchedulerSubGroupResourceIDProperty, cm.Parameters("@ResourceSchedulerSubGroupResourceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ResourceSchedulerSubGroupResourceID", GetProperty(ResourceSchedulerSubGroupResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace