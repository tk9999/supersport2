﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.Resources.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib.Maintenance.General.ReadOnly

Namespace ResourceSchedulers.New

  <Serializable()>
  Public Class ResourceSchedulerGroup
    Inherits OBBusinessBase(Of ResourceSchedulerGroup)

#Region " Properties and Methods "

#Region " Properties "

    <AlwaysClean>
    Public Property IsExpanded As Boolean = False

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ResourceSchedulerGroupBO.ResourceSchedulerGroupBOToString(self)")

    Public Shared ResourceSchedulerGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceSchedulerGroupID() As Integer
      Get
        Return GetProperty(ResourceSchedulerGroupIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceSchedulerGroupIDProperty, value)
      End Set
    End Property

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceSchedulerID, "Resource Scheduler", Nothing)
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    ''' >
    Public Property ResourceSchedulerID() As Integer?
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceSchedulerIDProperty, value)
      End Set
    End Property

    Public Shared ResourceSchedulerGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceSchedulerGroup, "Name", "")
    ''' <summary>
    ''' Gets and sets the Resource Scheduler Group value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(150, ErrorMessage:="Name cannot be more than 150 characters"),
    Required(AllowEmptyStrings:=False)>
    Public Property ResourceSchedulerGroup() As String
      Get
        Return GetProperty(ResourceSchedulerGroupProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceSchedulerGroupProperty, Value)
      End Set
    End Property

    'Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the System value
    ' ''' </summary>
    '<Display(Name:="For Sub-Dept", Description:=""),
    'RequiredIf(ConditionLogicJS:="self.ResourceTypeID() == 1"),
    'DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList))>
    'Public Property SystemID() As Integer?
    '  Get
    '    Return GetProperty(SystemIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(SystemIDProperty, Value)
    '  End Set
    'End Property
    ''    Required(ErrorMessage:="For Sub-Dept required"),

    'Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Production Area value
    ' ''' </summary>
    '<Display(Name:="For Area", Description:=""),
    'RequiredIf(ConditionLogicJS:="self.ResourceTypeID() == 1"),
    'DropDownWeb(GetType(ROSystemProductionAreaList), ThisFilterMember:="SystemID", ValueMember:="ProductionAreaID", DisplayMember:="ProductionArea")>
    'Public Property ProductionAreaID() As Integer?
    '  Get
    '    Return GetProperty(ProductionAreaIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ProductionAreaIDProperty, Value)
    '  End Set
    'End Property
    ''    Required(ErrorMessage:="For Area required"),

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    Required(ErrorMessage:="Type required"),
    DropDownWeb(GetType(ROResourceTypeList))>
    Public Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared GroupOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupOrder, "Order", 99)
    ''' <summary>
    ''' Gets and sets the Group Order value
    ''' </summary>
    <Display(Name:="Order", Description:=""),
    Required(ErrorMessage:="Order required")>
    Public Property GroupOrder() As Integer
      Get
        Return GetProperty(GroupOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(GroupOrderProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Default?", False)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Default?", Description:=""),
    SetExpression("ResourceSchedulerGroupBO.SelectedByDefaultSet(self)")>
    Public Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedByDefaultProperty, Value)
      End Set
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SiteID, "Site", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Site"),
    Required(ErrorMessage:="Site is required"),
    DropDownWeb(GetType(ROSiteList))>
    Public Property SiteID() As Integer?
      Get
        Return GetProperty(SiteIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SiteIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ResourceSchedulerSubGroupListProperty As PropertyInfo(Of ResourceSchedulerSubGroupList) = RegisterProperty(Of ResourceSchedulerSubGroupList)(Function(c) c.ResourceSchedulerSubGroupList, "Resource Scheduler Sub Group List")

    Public ReadOnly Property ResourceSchedulerSubGroupList() As ResourceSchedulerSubGroupList
      Get
        If GetProperty(ResourceSchedulerSubGroupListProperty) Is Nothing Then
          LoadProperty(ResourceSchedulerSubGroupListProperty, ResourceSchedulers.New.ResourceSchedulerSubGroupList.NewResourceSchedulerSubGroupList())
        End If
        Return GetProperty(ResourceSchedulerSubGroupListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ResourceScheduler

      Return CType(CType(Me.Parent, ResourceSchedulerGroupList).Parent, ResourceScheduler)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceSchedulerGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceSchedulerGroup.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Scheduler Group")
        Else
          Return String.Format("Blank {0}", "Resource Scheduler Group")
        End If
      Else
        Return Me.ResourceSchedulerGroup
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ResourceSchedulerSubGroups"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceSchedulerGroup() method.

    End Sub

    Public Shared Function NewResourceSchedulerGroup() As ResourceSchedulerGroup

      Return DataPortal.CreateChild(Of ResourceSchedulerGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetResourceSchedulerGroup(dr As SafeDataReader) As ResourceSchedulerGroup

      Dim r As New ResourceSchedulerGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceSchedulerGroupIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceSchedulerGroupProperty, .GetString(2))
          'LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(SiteIDProperty, ZeroNothing(.GetInt32(3)))
          LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(GroupOrderProperty, .GetInt32(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    'Protected Overrides Sub InsertUpdate(cm As SqlCommand)
    '  MyBase.InsertUpdate(cm)
    'End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ResourceSchedulerGroupIDProperty)

      If Me.GetParent() Is Nothing Then
        cm.Parameters.AddWithValue("@ResourceSchedulerID", GetProperty(ResourceSchedulerIDProperty))
      Else
        cm.Parameters.AddWithValue("@ResourceSchedulerID", Me.GetParent().ResourceSchedulerID)
      End If
      cm.Parameters.AddWithValue("@ResourceSchedulerGroup", GetProperty(ResourceSchedulerGroupProperty))
      'cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      'cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@ResourceTypeID", GetProperty(ResourceTypeIDProperty))
      cm.Parameters.AddWithValue("@GroupOrder", GetProperty(GroupOrderProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@SiteID", NothingDBNull(GetProperty(SiteIDProperty)))
      cm.Parameters.AddWithValue("@SelectedByDefault", GetProperty(SelectedByDefaultProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ResourceSchedulerGroupIDProperty, cm.Parameters("@ResourceSchedulerGroupID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(ResourceSchedulerSubGroupListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ResourceSchedulerGroupID", GetProperty(ResourceSchedulerGroupIDProperty))
    End Sub

#End Region

  End Class

End Namespace