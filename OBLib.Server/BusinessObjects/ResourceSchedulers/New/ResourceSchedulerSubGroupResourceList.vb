﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.New

  <Serializable()>
  Public Class ResourceSchedulerSubGroupResourceList
    Inherits OBBusinessListBase(Of ResourceSchedulerSubGroupResourceList, ResourceSchedulerSubGroupResource)

#Region " Business Methods "

    Public Function GetItem(ResourceSchedulerSubGroupResourceID As Integer) As ResourceSchedulerSubGroupResource

      For Each child As ResourceSchedulerSubGroupResource In Me
        If child.ResourceSchedulerSubGroupResourceID = ResourceSchedulerSubGroupResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Scheduler Sub Group Resources"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewResourceSchedulerSubGroupResourceList() As ResourceSchedulerSubGroupResourceList

      Return New ResourceSchedulerSubGroupResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property ResourceTypeID As Integer? = Nothing
      Public Property SiteID As Integer? = Nothing
      Public Property Freelancers As Boolean? = Nothing
      Public Property DisciplineID As Integer? = Nothing
      Public Property RoomTypeID As Integer? = Nothing
      Public Property HRProductionAreaID As Integer? = Nothing
      Public Property EquipmentTypeID As Integer? = Nothing
      Public Property VehicleTypeID As Integer? = Nothing
      Public Property SystemTeamNumberID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New()

      End Sub

    End Class

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceSchedulerSubGroupResource.GetResourceSchedulerSubGroupResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

      For Each child As ResourceSchedulerSubGroupResource In Me
        child.CheckRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceSchedulerSubGroupResourceList"
            cm.Parameters.AddWithValue("@ResourceSchedulerID", NothingDBNull(crit.ResourceSchedulerID))
            cm.Parameters.AddWithValue("@ResourceTypeID", NothingDBNull(crit.ResourceTypeID))
            cm.Parameters.AddWithValue("@SiteID", NothingDBNull(crit.SiteID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@RoomTypeID", NothingDBNull(crit.RoomTypeID))
            cm.Parameters.AddWithValue("@HRProductionAreaID", NothingDBNull(crit.HRProductionAreaID))
            cm.Parameters.AddWithValue("@EquipmentTypeID", NothingDBNull(crit.EquipmentTypeID))
            cm.Parameters.AddWithValue("@VehicleTypeID", NothingDBNull(crit.VehicleTypeID))
            cm.Parameters.AddWithValue("@SystemTeamNumberID", NothingDBNull(crit.SystemTeamNumberID))
            cm.Parameters.AddWithValue("@Freelancers", NothingDBNull(crit.Freelancers))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace