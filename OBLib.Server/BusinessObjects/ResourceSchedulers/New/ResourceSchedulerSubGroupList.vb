﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.New

  <Serializable()> _
  Public Class ResourceSchedulerSubGroupList
    Inherits OBBusinessListBase(Of ResourceSchedulerSubGroupList, ResourceSchedulerSubGroup)

#Region " Business Methods "

    Public Function GetItem(ResourceSchedulerSubGroupID As Integer) As ResourceSchedulerSubGroup

      For Each child As ResourceSchedulerSubGroup In Me
        If child.ResourceSchedulerSubGroupID = ResourceSchedulerSubGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Scheduler Sub Groups"

    End Function

    Public Function GetResourceSchedulerSubGroupResource(ResourceSchedulerSubGroupResourceID As Integer) As ResourceSchedulerSubGroupResource

      Dim obj As ResourceSchedulerSubGroupResource = Nothing
      For Each parent As ResourceSchedulerSubGroup In Me
        obj = parent.ResourceSchedulerSubGroupResourceList.GetItem(ResourceSchedulerSubGroupResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "
    Public Shared Function GetResourceSchedulerSubGroupList(ResourceSchedulerSubGroupID As Integer?) As ResourceSchedulerSubGroupList
      Return DataPortal.Fetch(Of ResourceSchedulerSubGroupList)(New Criteria(ResourceSchedulerSubGroupID))
    End Function

    Public Shared Function NewResourceSchedulerSubGroupList() As ResourceSchedulerSubGroupList

      Return New ResourceSchedulerSubGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property ResourceSchedulerGroupID As Integer? = Nothing
      Public Property ResourceSchedulerSubGroupID As Integer? = Nothing
      Public Property UserID As Integer? = Nothing
      Public Property FetchSubGroupResources As Boolean = False

      Public Sub New(ResourceSchedulerSubGroupID As Integer?)
        Me.ResourceSchedulerSubGroupID = ResourceSchedulerSubGroupID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceSchedulerSubGroup.GetResourceSchedulerSubGroup(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As ResourceScheduler = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ResourceSchedulerID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ResourceSchedulerGroupList.RaiseListChangedEvents = False
      '    parent.ResourceSchedulerGroupList.Add(ResourceSchedulerGroup.GetResourceSchedulerGroup(sdr))
      '    parent.ResourceSchedulerGroupList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As ResourceSchedulerGroup = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.ResourceSchedulerGroupID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetResourceSchedulerGroup(sdr.GetInt32(1))
      '    End If
      '    parentChild.ResourceSchedulerSubGroupList.RaiseListChangedEvents = False
      '    parentChild.ResourceSchedulerSubGroupList.Add(ResourceSchedulerSubGroup.GetResourceSchedulerSubGroup(sdr))
      '    parentChild.ResourceSchedulerSubGroupList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentGrandChild As ResourceSchedulerSubGroup = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentGrandChild Is Nothing OrElse parentGrandChild.ResourceSchedulerSubGroupID <> sdr.GetInt32(1) Then
      '      parentGrandChild = Me.GetResourceSchedulerSubGroup(sdr.GetInt32(1))
      '    End If
      '    parentGrandChild.ResourceSchedulerSubGroupResourceList.RaiseListChangedEvents = False
      '    parentGrandChild.ResourceSchedulerSubGroupResourceList.Add(ResourceSchedulerSubGroupResource.GetResourceSchedulerSubGroupResource(sdr))
      '    parentGrandChild.ResourceSchedulerSubGroupResourceList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As ResourceSchedulerSubGroup In Me
        child.CheckRules()
        'For Each ResourceSchedulerGroup As ResourceSchedulerGroup In child.ResourceSchedulerGroupList
        '  ResourceSchedulerGroup.CheckRules()

        '  For Each ResourceSchedulerSubGroup As ResourceSchedulerSubGroup In ResourceSchedulerGroup.ResourceSchedulerSubGroupList
        '    ResourceSchedulerSubGroup.CheckRules()

        '    For Each ResourceSchedulerSubGroupResource As ResourceSchedulerSubGroupResource In ResourceSchedulerSubGroup.ResourceSchedulerSubGroupResourceList
        '      ResourceSchedulerSubGroupResource.CheckRules()
        '    Next
        '  Next
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceSchedulerSubGroupList"
            cm.Parameters.AddWithValue("@ResourceSchedulerID", NothingDBNull(crit.ResourceSchedulerID))
            cm.Parameters.AddWithValue("@ResourceSchedulerGroupID", NothingDBNull(crit.ResourceSchedulerGroupID))
            cm.Parameters.AddWithValue("@ResourceSchedulerSubGroupID", NothingDBNull(crit.ResourceSchedulerSubGroupID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@FetchSubGroupResources", crit.FetchSubGroupResources)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace