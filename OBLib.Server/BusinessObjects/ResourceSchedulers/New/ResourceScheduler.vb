﻿' Generated 13 Apr 2016 15:52 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.New

  <Serializable()> _
  Public Class ResourceScheduler
    Inherits OBBusinessBase(Of ResourceScheduler)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ResourceSchedulerBO.ResourceSchedulerBOToString(self)")

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceSchedulerID() As Integer
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceSchedulerIDProperty, value)
      End Set
    End Property

    Public Shared ResourceSchedulerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceScheduler, "Resource Scheduler", "")
    ''' <summary>
    ''' Gets and sets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(150, ErrorMessage:="Name cannot be more than 150 characters")>
    Public Property ResourceScheduler() As String
      Get
        Return GetProperty(ResourceSchedulerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceSchedulerProperty, Value)
      End Set
    End Property

    Public Shared DefaultDateViewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultDateViewTypeID, "Default Date View Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Default Date View Type value
    ''' </summary>
    <Display(Name:="Default Date View Type", Description:=""),
    Required(ErrorMessage:="Default Date View Type required")>
    Public Property DefaultDateViewTypeID() As Integer?
      Get
        Return GetProperty(DefaultDateViewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultDateViewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PageNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PageName, "Page Name", "")
    ''' <summary>
    ''' Gets and sets the Page Name value
    ''' </summary>
    <Display(Name:="Page Name", Description:=""),
    StringLength(50, ErrorMessage:="Page Name cannot be more than 50 characters")>
    Public Property PageName() As String
      Get
        Return GetProperty(PageNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PageNameProperty, Value)
      End Set
    End Property

    Public Shared DefaultDateViewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DefaultDateViewType, "DefaultDateViewType", "")
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    Public Property DefaultDateViewType() As String
      Get
        Return GetProperty(DefaultDateViewTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DefaultDateViewTypeProperty, Value)
      End Set
    End Property

    Public Shared MaxDaysBeforeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxDaysBefore, "MaxDaysBefore", 0)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public Property MaxDaysBefore() As Integer
      Get
        Return GetProperty(MaxDaysBeforeProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxDaysBeforeProperty, Value)
      End Set
    End Property

    Public Shared MaxDaysAfterProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxDaysAfter, "MaxDaysAfter", 0)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public Property MaxDaysAfter() As Integer
      Get
        Return GetProperty(MaxDaysAfterProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxDaysAfterProperty, Value)
      End Set
    End Property

    Public Shared CurrentStartDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CurrentStartDate, "Start Date", Now)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public Property CurrentStartDate() As DateTime
      Get
        Return GetProperty(CurrentStartDateProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(CurrentStartDateProperty, Value)
      End Set
    End Property

    Public Shared CurrentEndDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CurrentEndDate, "End Date", Now)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property CurrentEndDate() As DateTime
      Get
        Return GetProperty(CurrentEndDateProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(CurrentEndDateProperty, Value)
      End Set
    End Property

    Public Shared MaxStartDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.MaxStartDate, "Start Date", Now)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public Property MaxStartDate() As DateTime
      Get
        Return GetProperty(MaxStartDateProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(MaxStartDateProperty, Value)
      End Set
    End Property

    Public Shared MaxEndDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.MaxEndDate, "End Date", Now)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property MaxEndDate() As DateTime
      Get
        Return GetProperty(MaxEndDateProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(MaxEndDateProperty, Value)
      End Set
    End Property

    Public Shared IsAdministratorProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsAdministrator, False)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="CanImportFromSynergy", Description:="")>
    Public Property IsAdministrator() As Boolean
      Get
        Return GetProperty(IsAdministratorProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsAdministratorProperty, Value)
      End Set
    End Property

    Public Shared ReadOnlyAccessProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.ReadOnlyAccess, True)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="CanImportFromSynergy", Description:="")>
    Public Property ReadOnlyAccess() As Boolean
      Get
        Return GetProperty(ReadOnlyAccessProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReadOnlyAccessProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Default Date View Type value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ShowHRStatsProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.ShowHRStats, True)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Show HR Stats", Description:="")>
    Public Property ShowHRStats() As Boolean
      Get
        Return GetProperty(ShowHRStatsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShowHRStatsProperty, Value)
      End Set
    End Property

    Public Shared MinViewGroupHeightProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MinViewGroupHeight, "Default Item Height", 20)
    ''' <summary>
    ''' Gets and sets the Default Date View Type value
    ''' </summary>
    <Display(Name:="Default Item Height", Description:=""),
    Range(15, 30, ErrorMessage:="Min of 15, Max of 30")>
    Public Property MinViewGroupHeight() As Integer
      Get
        Return GetProperty(MinViewGroupHeightProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MinViewGroupHeightProperty, Value)
      End Set
    End Property

    Public Shared ShowResourcesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.ShowResources, True)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Show Resources", Description:="")>
    Public Property ShowResources() As Boolean
      Get
        Return GetProperty(ShowResourcesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShowResourcesProperty, Value)
      End Set
    End Property

    Public Shared IncludeAvailabilityProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IncludeAvailability, False)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Include Availability", Description:="")>
    Public Property IncludeAvailability() As Boolean
      Get
        Return GetProperty(IncludeAvailabilityProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IncludeAvailabilityProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ResourceSchedulerGroupListProperty As PropertyInfo(Of ResourceSchedulerGroupList) = RegisterProperty(Of ResourceSchedulerGroupList)(Function(c) c.ResourceSchedulerGroupList, "Resource Scheduler Group List")

    Public ReadOnly Property ResourceSchedulerGroupList() As ResourceSchedulerGroupList
      Get
        If GetProperty(ResourceSchedulerGroupListProperty) Is Nothing Then
          LoadProperty(ResourceSchedulerGroupListProperty, ResourceSchedulers.New.ResourceSchedulerGroupList.NewResourceSchedulerGroupList())
        End If
        Return GetProperty(ResourceSchedulerGroupListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceSchedulerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceScheduler.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Scheduler")
        Else
          Return String.Format("Blank {0}", "Resource Scheduler")
        End If
      Else
        Return Me.ResourceScheduler
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ResourceSchedulerGroups"}
      End Get
    End Property

    Public Sub SetupView()

      Select Case DefaultDateViewTypeID
        Case OBLib.CommonData.Enums.DefaultViewType.Today
          SetupTodayView()
        Case OBLib.CommonData.Enums.DefaultViewType.ThreeDays
          SetupThreeDaysView()
        Case OBLib.CommonData.Enums.DefaultViewType.CurrentWeek
          SetupWeekView()
        Case OBLib.CommonData.Enums.DefaultViewType.CurrentMonth
          SetupMonthView()
      End Select

    End Sub

    Public Sub SetupTodayView()
      CurrentStartDate = New Date(Now.Year, Now.Month, Now.Day, 0, 0, 0) 'Now 'New Date(2015, 8, Now.Day, 0, 0, 0)
      CurrentEndDate = New Date(Now.Year, Now.Month, Now.Day, 23, 59, 59) 'Now 'New Date(2015, 8, Now.Day, 23, 59, 59)
      MaxStartDate = CurrentStartDate.AddDays(-MaxDaysBefore)
      MaxEndDate = CurrentEndDate.AddDays(MaxDaysAfter)
    End Sub

    Public Sub SetupThreeDaysView()
      'Now.Year, Now.Month, Now.Day
      CurrentStartDate = New Date(Now.Year, Now.Month, Now.Day, 0, 0, 0).AddDays(-1)
      CurrentEndDate = New Date(Now.Year, Now.Month, Now.Day, 23, 59, 59).AddDays(1)
      'CurrentStartDate = Now.AddDays(-1)
      'CurrentEndDate = Now.AddDays(1)
      MaxStartDate = CurrentStartDate.AddDays(-MaxDaysBefore)
      MaxEndDate = CurrentEndDate.AddDays(MaxDaysAfter)
    End Sub

    Public Sub SetupWeekView()
      CurrentStartDate = Singular.Dates.DateWeekStart(Now, DayOfWeek.Monday) 'Singular.Dates.DateWeekStart(New Date(2015, 8, Now.Day, 0, 0, 0), DayOfWeek.Monday)
      CurrentEndDate = Singular.Dates.DateWeekEnd(Now, True) 'Singular.Dates.DateWeekEnd(New Date(2015, 8, Now.Day, 23, 59, 59), True)
      MaxStartDate = CurrentStartDate.AddDays(-MaxDaysBefore)
      MaxEndDate = CurrentEndDate.AddDays(MaxDaysAfter)
    End Sub

    Public Sub SetupMonthView()
      CurrentStartDate = Singular.Dates.DateMonthStart(Now) 'Singular.Dates.DateMonthStart(New Date(2015, 8, Now.Day, 0, 0, 0))
      CurrentEndDate = Singular.Dates.DateMonthEnd(Now) 'Singular.Dates.DateMonthEnd(New Date(2015, 8, Now.Day, 23, 59, 59))
      MaxStartDate = CurrentStartDate.AddDays(-MaxDaysBefore)
      MaxEndDate = CurrentEndDate.AddDays(MaxDaysAfter)
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceScheduler() method.

    End Sub

    Public Shared Function NewResourceScheduler() As ResourceScheduler

      Return DataPortal.CreateChild(Of ResourceScheduler)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetResourceScheduler(dr As SafeDataReader) As ResourceScheduler

      Dim r As New ResourceScheduler()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceSchedulerIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerProperty, .GetString(1))
          LoadProperty(DefaultDateViewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          'LoadProperty(CanImportFromSynergyProperty, .GetBoolean(3))
          LoadProperty(PageNameProperty, .GetString(3))
          'LoadProperty(GroupWidthOverrideProperty, .GetString(6))
          LoadProperty(DefaultDateViewTypeProperty, .GetString(4))
          LoadProperty(MaxDaysBeforeProperty, .GetInt32(5))
          LoadProperty(MaxDaysAfterProperty, .GetInt32(6))
          LoadProperty(IsAdministratorProperty, .GetBoolean(7))
          LoadProperty(ReadOnlyAccessProperty, .GetBoolean(8))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(ShowHRStatsProperty, .GetBoolean(10))
          LoadProperty(MinViewGroupHeightProperty, .GetInt32(11))
          LoadProperty(ShowResourcesProperty, .GetBoolean(12))
        End With
      End Using

      SetupView()
      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ResourceSchedulerIDProperty)

      cm.Parameters.AddWithValue("@ResourceScheduler", GetProperty(ResourceSchedulerProperty))
      cm.Parameters.AddWithValue("@DefaultDateViewTypeID", GetProperty(DefaultDateViewTypeIDProperty))
      'cm.Parameters.AddWithValue("@CanImportFromSynergy", GetProperty(CanImportFromSynergyProperty))
      'cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
      cm.Parameters.AddWithValue("@PageName", GetProperty(PageNameProperty))
      'cm.Parameters.AddWithValue("@GroupWidthOverride", GetProperty(GroupWidthOverrideProperty))
      cm.Parameters.AddWithValue("@ShowHRStats", GetProperty(ShowHRStatsProperty))
      cm.Parameters.AddWithValue("@MinViewGroupHeight", GetProperty(MinViewGroupHeightProperty))
      cm.Parameters.AddWithValue("@ShowResources", GetProperty(ShowResourcesProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ResourceSchedulerIDProperty, cm.Parameters("@ResourceSchedulerID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(ResourceSchedulerGroupListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ResourceSchedulerID", GetProperty(ResourceSchedulerIDProperty))
    End Sub

#End Region

  End Class

End Namespace