﻿' Generated 20 Jan 2016 05:48 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class RORSSubGroupResource
    Inherits OBReadOnlyBase(Of RORSSubGroupResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RSSubGroupResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSSubGroupResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RSSubGroupResourceID() As Integer
      Get
        Return GetProperty(RSSubGroupResourceIDProperty)
      End Get
    End Property

    Public Shared RSSubGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSSubGroupID, "RS Sub Group")
    ''' <summary>
    ''' Gets the RS Sub Group value
    ''' </summary>
    <Display(Name:="RS Sub Group", Description:="")>
  Public ReadOnly Property RSSubGroupID() As Integer
      Get
        Return GetProperty(RSSubGroupIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceOrder, "Resource Order")
    ''' <summary>
    ''' Gets the Resource Order value
    ''' </summary>
    <Display(Name:="Resource Order", Description:="")>
  Public ReadOnly Property ResourceOrder() As Integer
      Get
        Return GetProperty(ResourceOrderProperty)
      End Get
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Selected By Default", False)
    ''' <summary>
    ''' Gets the Selected By Default value
    ''' </summary>
    <Display(Name:="Selected By Default", Description:="")>
  Public ReadOnly Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name")
    ''' <summary>
    ''' Gets the Resource Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:="")>
  Public ReadOnly Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeID, "Resource Type")
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
  Public ReadOnly Property ResourceTypeID() As Integer
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle")
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
  Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "Equipment")
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
  Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "Channel")
    ''' <summary>
    ''' Gets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
  Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CustomResourceID, "Custom Resource")
    ''' <summary>
    ''' Gets the Custom Resource value
    ''' </summary>
    <Display(Name:="Custom Resource", Description:="")>
  Public ReadOnly Property CustomResourceID() As Integer
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
  Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared CanAddBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanAddBookings, "Can Add Bookings", False)
    ''' <summary>
    ''' Gets the Can Add Bookings value
    ''' </summary>
    <Display(Name:="Can Add Bookings", Description:="")>
  Public ReadOnly Property CanAddBookings() As Boolean
      Get
        Return GetProperty(CanAddBookingsProperty)
      End Get
    End Property

    Public Shared CanMoveBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanMoveBookings, "Can Move Bookings", False)
    ''' <summary>
    ''' Gets the Can Move Bookings value
    ''' </summary>
    <Display(Name:="Can Move Bookings", Description:="")>
  Public ReadOnly Property CanMoveBookings() As Boolean
      Get
        Return GetProperty(CanMoveBookingsProperty)
      End Get
    End Property

    Public Shared CanDeleteBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanDeleteBookings, "Can Delete Bookings", False)
    ''' <summary>
    ''' Gets the Can Delete Bookings value
    ''' </summary>
    <Display(Name:="Can Delete Bookings", Description:="")>
  Public ReadOnly Property CanDeleteBookings() As Boolean
      Get
        Return GetProperty(CanDeleteBookingsProperty)
      End Get
    End Property

    Public Shared RoomOwnedBySystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomOwnedBySystemID, "Room Owned By System")
    ''' <summary>
    ''' Gets the Room Owned By System value
    ''' </summary>
    <Display(Name:="Room Owned By System", Description:="")>
  Public ReadOnly Property RoomOwnedBySystemID() As Integer
      Get
        Return GetProperty(RoomOwnedBySystemIDProperty)
      End Get
    End Property

    Public Shared RoomOwnedByProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomOwnedByProductionAreaID, "Room Owned By Production Area")
    ''' <summary>
    ''' Gets the Room Owned By Production Area value
    ''' </summary>
    <Display(Name:="Room Owned By Production Area", Description:="")>
  Public ReadOnly Property RoomOwnedByProductionAreaID() As Integer
      Get
        Return GetProperty(RoomOwnedByProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RSSubGroupResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORSSubGroupResource(dr As SafeDataReader) As RORSSubGroupResource

      Dim r As New RORSSubGroupResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RSSubGroupResourceIDProperty, .GetInt32(0))
        LoadProperty(RSSubGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ResourceOrderProperty, .GetInt32(3))
        LoadProperty(SelectedByDefaultProperty, .GetBoolean(4))
        LoadProperty(ResourceNameProperty, .GetString(5))
        LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(CustomResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(CanAddBookingsProperty, .GetBoolean(14))
        LoadProperty(CanMoveBookingsProperty, .GetBoolean(15))
        LoadProperty(CanDeleteBookingsProperty, .GetBoolean(16))
        LoadProperty(RoomOwnedBySystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(RoomOwnedByProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace