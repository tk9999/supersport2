﻿' Generated 10 Apr 2016 13:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers.ReadOnly

  <Serializable()> _
  Public Class RODefaultDateViewType
    Inherits OBReadOnlyBase(Of RODefaultDateViewType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DefaultDateViewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultDateViewTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property DefaultDateViewTypeID() As Integer
      Get
        Return GetProperty(DefaultDateViewTypeIDProperty)
      End Get
    End Property

    Public Shared DefaultDateViewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DefaultDateViewType, "Default Date View Type", "")
    ''' <summary>
    ''' Gets the Default Date View Type value
    ''' </summary>
    <Display(Name:="Default Date View Type", Description:="")>
  Public ReadOnly Property DefaultDateViewType() As String
      Get
        Return GetProperty(DefaultDateViewTypeProperty)
      End Get
    End Property

    Public Shared MaxDaysBeforeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxDaysBefore, "Max Days Before", 0)
    ''' <summary>
    ''' Gets the Max Days Before value
    ''' </summary>
    <Display(Name:="Max Days Before", Description:="")>
  Public ReadOnly Property MaxDaysBefore() As Integer
      Get
        Return GetProperty(MaxDaysBeforeProperty)
      End Get
    End Property

    Public Shared MaxDaysAfterProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxDaysAfter, "Max Days After", 0)
    ''' <summary>
    ''' Gets the Max Days After value
    ''' </summary>
    <Display(Name:="Max Days After", Description:="")>
  Public ReadOnly Property MaxDaysAfter() As Integer
      Get
        Return GetProperty(MaxDaysAfterProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DefaultDateViewTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.DefaultDateViewType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRODefaultDateViewType(dr As SafeDataReader) As RODefaultDateViewType

      Dim r As New RODefaultDateViewType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DefaultDateViewTypeIDProperty, .GetInt32(0))
        LoadProperty(DefaultDateViewTypeProperty, .GetString(1))
        LoadProperty(MaxDaysBeforeProperty, .GetInt32(2))
        LoadProperty(MaxDaysAfterProperty, .GetInt32(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace