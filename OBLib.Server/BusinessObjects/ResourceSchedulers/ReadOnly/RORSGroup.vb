﻿' Generated 20 Jan 2016 05:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class RORSGroup
    Inherits OBReadOnlyBase(Of RORSGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RSGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSGroupID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RSGroupID() As Integer
      Get
        Return GetProperty(RSGroupIDProperty)
      End Get
    End Property

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerID, "Resource Scheduler")
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Resource Scheduler", Description:="")>
  Public ReadOnly Property ResourceSchedulerID() As Integer
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeID, "Resource Type")
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
  Public ReadOnly Property ResourceTypeID() As Integer
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
    End Property

    Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupName, "Group Name")
    ''' <summary>
    ''' Gets the Group Name value
    ''' </summary>
    <Display(Name:="Group Name", Description:="")>
  Public ReadOnly Property GroupName() As String
      Get
        Return GetProperty(GroupNameProperty)
      End Get
    End Property

    Public Shared GroupOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupOrder, "Group Order")
    ''' <summary>
    ''' Gets the Group Order value
    ''' </summary>
    <Display(Name:="Group Order", Description:="")>
  Public ReadOnly Property GroupOrder() As Integer
      Get
        Return GetProperty(GroupOrderProperty)
      End Get
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Selected By Default", False)
    ''' <summary>
    ''' Gets the Selected By Default value
    ''' </summary>
    <Display(Name:="Selected By Default", Description:="")>
  Public ReadOnly Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
    End Property

    Public Shared ResourceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceType, "Resource Type")
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
  Public ReadOnly Property ResourceType() As String
      Get
        Return GetProperty(ResourceTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RSGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.GroupName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORSGroup(dr As SafeDataReader) As RORSGroup

      Dim r As New RORSGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RSGroupIDProperty, .GetInt32(0))
        LoadProperty(ResourceSchedulerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(GroupNameProperty, .GetString(3))
        LoadProperty(GroupOrderProperty, .GetInt32(4))
        LoadProperty(SelectedByDefaultProperty, .GetBoolean(5))
        LoadProperty(ResourceTypeProperty, .GetString(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace