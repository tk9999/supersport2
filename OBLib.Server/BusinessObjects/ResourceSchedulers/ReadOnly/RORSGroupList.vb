﻿' Generated 20 Jan 2016 05:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class RORSGroupList
    Inherits OBReadOnlyListBase(Of RORSGroupList, RORSGroup)

#Region " Business Methods "

    Public Function GetItem(RSGroupID As Integer) As RORSGroup

      For Each child As RORSGroup In Me
        If child.RSGroupID = RSGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property ResourceGroupID As Integer? = Nothing

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORSGroupList() As RORSGroupList

      Return New RORSGroupList()

    End Function

    Public Shared Sub BeginGetRORSGroupList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORSGroupList)))

      Dim dp As New DataPortal(Of RORSGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORSGroupList(CallBack As EventHandler(Of DataPortalResult(Of RORSGroupList)))

      Dim dp As New DataPortal(Of RORSGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORSGroupList() As RORSGroupList

      Return DataPortal.Fetch(Of RORSGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORSGroup.GetRORSGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORSGroupList"
            cm.Parameters.AddWithValue("@ResourceSchedulerID", NothingDBNull(crit.ResourceSchedulerID))
            cm.Parameters.AddWithValue("@ResourceGroupID", NothingDBNull(crit.ResourceGroupID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace