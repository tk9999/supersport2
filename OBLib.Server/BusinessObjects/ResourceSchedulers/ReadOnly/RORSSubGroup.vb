﻿' Generated 20 Jan 2016 05:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class RORSSubGroup
    Inherits OBReadOnlyBase(Of RORSSubGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RSSubGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSSubGroupID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RSSubGroupID() As Integer
      Get
        Return GetProperty(RSSubGroupIDProperty)
      End Get
    End Property

    Public Shared RSGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSGroupID, "RS Group")
    ''' <summary>
    ''' Gets the RS Group value
    ''' </summary>
    <Display(Name:="RS Group", Description:="")>
  Public ReadOnly Property RSGroupID() As Integer
      Get
        Return GetProperty(RSGroupIDProperty)
      End Get
    End Property

    Public Shared SubGroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubGroupName, "Sub Group Name")
    ''' <summary>
    ''' Gets the Sub Group Name value
    ''' </summary>
    <Display(Name:="Sub Group Name", Description:="")>
  Public ReadOnly Property SubGroupName() As String
      Get
        Return GetProperty(SubGroupNameProperty)
      End Get
    End Property

    Public Shared SubGroupOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubGroupOrder, "Sub Group Order")
    ''' <summary>
    ''' Gets the Sub Group Order value
    ''' </summary>
    <Display(Name:="Sub Group Order", Description:="")>
  Public ReadOnly Property SubGroupOrder() As Integer
      Get
        Return GetProperty(SubGroupOrderProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomTypeID, "Room Type")
    ''' <summary>
    ''' Gets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:="")>
  Public ReadOnly Property RoomTypeID() As Integer
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Selected By Default", False)
    ''' <summary>
    ''' Gets the Selected By Default value
    ''' </summary>
    <Display(Name:="Selected By Default", Description:="")>
  Public ReadOnly Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared RoomTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomType, "Room Type")
    ''' <summary>
    ''' Gets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:="")>
  Public ReadOnly Property RoomType() As String
      Get
        Return GetProperty(RoomTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RSSubGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SubGroupName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORSSubGroup(dr As SafeDataReader) As RORSSubGroup

      Dim r As New RORSSubGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RSSubGroupIDProperty, .GetInt32(0))
        LoadProperty(RSGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SubGroupNameProperty, .GetString(2))
        LoadProperty(SubGroupOrderProperty, .GetInt32(3))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(SelectedByDefaultProperty, .GetBoolean(6))
        LoadProperty(DisciplineProperty, .GetString(7))
        LoadProperty(RoomTypeProperty, .GetString(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace