﻿' Generated 20 Jan 2016 05:47 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class RORSSubGroupList
    Inherits OBReadOnlyListBase(Of RORSSubGroupList, RORSSubGroup)

#Region " Business Methods "

    Public Function GetItem(RSSubGroupID As Integer) As RORSSubGroup

      For Each child As RORSSubGroup In Me
        If child.RSSubGroupID = RSSubGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RSGroupID As Integer? = Nothing
      Public Property RSSubGroupID As Integer? = Nothing

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORSSubGroupList() As RORSSubGroupList

      Return New RORSSubGroupList()

    End Function

    Public Shared Sub BeginGetRORSSubGroupList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORSSubGroupList)))

      Dim dp As New DataPortal(Of RORSSubGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORSSubGroupList(CallBack As EventHandler(Of DataPortalResult(Of RORSSubGroupList)))

      Dim dp As New DataPortal(Of RORSSubGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORSSubGroupList() As RORSSubGroupList

      Return DataPortal.Fetch(Of RORSSubGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORSSubGroup.GetRORSSubGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORSSubGroupList"
            cm.Parameters.AddWithValue("@RSGroupID", NothingDBNull(crit.RSGroupID))
            cm.Parameters.AddWithValue("@RSSubGroupID", NothingDBNull(crit.RSSubGroupID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace