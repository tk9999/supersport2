﻿' Generated 19 Feb 2016 20:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources

  <Serializable()> _
  Public Class RSResourceList
    Inherits OBBusinessListBase(Of RSResourceList, RSResource)

    Public Property TotalRecords As Integer = 0
    Public Property TotalPages As Integer = 0

#Region " Business Methods "

    Public Function GetItem(ResourceID As Integer) As RSResource

      For Each child As RSResource In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetRSResourceBooking(ResourceBookingID As Integer) As RSResourceBooking

      Dim obj As RSResourceBooking = Nothing
      For Each parent As RSResource In Me
        obj = parent.RSResourceBookingList.GetItem(ResourceBookingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits OBCriteriaBase(Of Criteria)

      Public Property ResourceID As Object = Nothing
      Public Property ResourceIDs As String = ""
      Public Property ResourceBookingID As Integer? = Nothing
      Public Property ResourceBookingIDs As String = ""
      Public Property StartDateBuffer As DateTime? = Nothing
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property EndDateBuffer As DateTime? = Nothing
      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property IncludeAvailability As Boolean = False
      Public Property IncludeCancelled As Boolean = False
      'Public Property HRStatsMode As Integer? = Nothing
      'Public Property SystemID As Integer? = Nothing
      'Public Property ProductionAreaID As Integer? = Nothing
      'Public Property DisciplineID As Integer?
      'Public Property PositionID As Integer?
      'Public Property FirstName As String
      'Public Property Surname As String
      'Public Property PreferredName As String
      'Public Property PageNo As Integer?
      'Public Property PageSize As Integer?

      Public Sub New(ResourceID As Object)

        Me.ResourceID = ResourceID

      End Sub

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?,
                     ResourceSchedulerID As Integer?)
        'DisciplineID As Integer?, PositionID As Integer?,
        'FirstName As String, Surname As String, PreferredName As String,
        'PageNo As Integer?, PageSize As Integer?, 
        'HRStatsMode As Integer?,
        'SystemID As Integer?, 
        'ProductionAreaID As Integer?
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        'Me.DisciplineID = DisciplineID
        'Me.PositionID = PositionID
        'Me.FirstName = FirstName
        'Me.Surname = Surname
        'Me.PreferredName = PreferredName
        'Me.PageNo = PageNo
        'Me.PageSize = PageSize
        Me.ResourceSchedulerID = ResourceSchedulerID
        'Me.HRStatsMode = HRStatsMode
        'Me.SystemID = SystemID
        'Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRSResourceList() As RSResourceList

      Return New RSResourceList()

    End Function

    Public Shared Sub BeginGetRSResourceList(CallBack As EventHandler(Of DataPortalResult(Of RSResourceList)))

      Dim dp As New DataPortal(Of RSResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRSResourceList() As RSResourceList

      Return DataPortal.Fetch(Of RSResourceList)(New Criteria())

    End Function

    Public Shared Function GetRSResourceList(StartDate As DateTime?, EndDate As DateTime?,
                                             DisciplineID As Integer?, PositionID As Integer?,
                                             FirstName As String, Surname As String, PreferredName As String,
                                             PageNo As Integer?, PageSize As Integer?, ResourceSchedulerID As Integer?) As RSResourceList

      'HRStatsMode As Integer?,
      'SystemID As Integer?, 
      'ProductionAreaID As Integer?

      Return DataPortal.Fetch(Of RSResourceList)(New Criteria(StartDate, EndDate, ResourceSchedulerID))
      'DisciplineID, PositionID,
      '                                                        FirstName, Surname, PreferredName,
      '                                                        PageNo, PageSize, 
      ', HRStatsMode, SystemID, ProductionAreaID

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      TotalRecords = sdr.GetInt32(0)
      TotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      While sdr.Read
        Me.Add(RSResource.GetRSResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As RSResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          Dim ResourceIDOverride = ZeroNothing(sdr.GetInt32(32))
          If ResourceIDOverride IsNot Nothing Then
            If parent Is Nothing OrElse Not CompareSafe(parent.ResourceID, ResourceIDOverride) Then
              parent = Me.GetItem(ResourceIDOverride)
            End If
          Else
            If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
              parent = Me.GetItem(sdr.GetInt32(1))
            End If
          End If
          If parent IsNot Nothing Then
            parent.RSResourceBookingList.RaiseListChangedEvents = False
            parent.RSResourceBookingList.Add(RSResourceBooking.GetRSResourceBooking(sdr))
            parent.RSResourceBookingList.RaiseListChangedEvents = True
          End If
        End While
      End If

      'Dim parentChild As RSResourceBooking = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.ResourceBookingID <> sdr.GetInt32(0) Then
      '      parentChild = Me.GetRSResourceBooking(sdr.GetInt32(0))
      '    End If
      '    parentChild.RSResourceBookingAreaList.RaiseListChangedEvents = False
      '    parentChild.RSResourceBookingAreaList.Add(RSResourceBookingArea.GetRSResourceBookingArea(sdr))
      '    parentChild.RSResourceBookingAreaList.RaiseListChangedEvents = True
      '  End While
      'End If

      'For Each child As RSResource In Me
      '  child.CheckRules()
      '  For Each RSResourceBooking As RSResourceBooking In child.RSResourceBookingList
      '    RSResourceBooking.CheckRules()

      '    For Each RSResourceBookingArea As RSResourceBookingArea In RSResourceBooking.RSResourceBookingAreaList
      '      RSResourceBookingArea.CheckRules()
      '    Next
      '  Next
      'Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRSResourceList"
            cm.Parameters.AddWithValue("@ResourceID", Singular.Misc.NothingDBNull(crit.ResourceID))
            cm.Parameters.AddWithValue("@ResourceIDs", Singular.Strings.MakeEmptyDBNull(crit.ResourceIDs))
            cm.Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(crit.ResourceBookingID))
            cm.Parameters.AddWithValue("@ResourceBookingIDs", Singular.Strings.MakeEmptyDBNull(crit.ResourceBookingIDs))
            cm.Parameters.AddWithValue("@StartDateBuffer", Singular.Misc.NothingDBNull(crit.StartDateBuffer))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@EndDateBuffer", Singular.Misc.NothingDBNull(crit.EndDateBuffer))
            cm.Parameters.AddWithValue("@ResourceSchedulerID", Singular.Misc.NothingDBNull(crit.ResourceSchedulerID))
            'cm.Parameters.AddWithValue("@HRStatsMode", Singular.Misc.NothingDBNull(crit.HRStatsMode))
            'cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            'cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            'cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            'cm.Parameters.AddWithValue("@PositionID", NothingDBNull(crit.PositionID))
            'cm.Parameters.AddWithValue("@FirstName", Strings.MakeEmptyDBNull(crit.FirstName))
            'cm.Parameters.AddWithValue("@Surname", Strings.MakeEmptyDBNull(crit.Surname))
            'cm.Parameters.AddWithValue("@PreferredName", Strings.MakeEmptyDBNull(crit.PreferredName))
            'cm.Parameters.AddWithValue("@PageNo", NothingDBNull(crit.PageNo))
            'cm.Parameters.AddWithValue("@PageSize", NothingDBNull(crit.PageSize))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@IncludeAvailability", crit.IncludeAvailability)
            cm.Parameters.AddWithValue("@IncludeCancelled", crit.IncludeCancelled)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As RSResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As RSResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace