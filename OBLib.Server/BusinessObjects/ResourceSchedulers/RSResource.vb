﻿' Generated 19 Feb 2016 20:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources

  <Serializable()> _
  Public Class RSResource
    Inherits OBBusinessBase(Of RSResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:="")>
  Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Resource Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "ContractType", Nothing)
    ''' <summary>
    ''' Gets and sets the ContractType value
    ''' </summary>
    <Display(Name:="ContractType")>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.IsFreelancer, "Is Freelancer", CType(Nothing, Boolean))
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Freelancer")>
    Public Property IsFreelancer() As Boolean?
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(IsFreelancerProperty, Value)
      End Set
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShortNameProperty, Value)
      End Set
    End Property

    Public Shared ReqShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReqShifts, "Req Shifts")
    ''' <summary>
    ''' Gets and sets the Req Shifts value
    ''' </summary>
    <Display(Name:="Req Shifts", Description:="")>
  Public Property ReqShifts() As Integer
      Get
        Return GetProperty(ReqShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReqShiftsProperty, Value)
      End Set
    End Property

    Public Shared ReqHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ReqHours, "Req Hours")
    ''' <summary>
    ''' Gets and sets the Req Hours value
    ''' </summary>
    <Display(Name:="Req Hours", Description:="")>
    Public Property ReqHours() As Decimal
      Get
        Return GetProperty(ReqHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ReqHoursProperty, Value)
      End Set
    End Property

    Public Shared TotalShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShifts, "Total Shifts")
    ''' <summary>
    ''' Gets and sets the Total Shifts value
    ''' </summary>
    <Display(Name:="Total Shifts", Description:="")>
  Public Property TotalShifts() As Integer
      Get
        Return GetProperty(TotalShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TotalShiftsProperty, Value)
      End Set
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Total Hours")
    ''' <summary>
    ''' Gets and sets the Total Hours value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
  Public Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalHoursProperty, Value)
      End Set
    End Property

    Public Shared StartingBalanceHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.StartingBalanceHours, "Starting Balance")
    ''' <summary>
    ''' Gets and sets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Starting Balance", Description:="")>
    Public Property StartingBalanceHours() As Decimal
      Get
        Return GetProperty(StartingBalanceHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(StartingBalanceHoursProperty, Value)
      End Set
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours")
    ''' <summary>
    ''' Gets and sets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall Hours", Description:="")>
  Public Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShortfallHoursProperty, Value)
      End Set
    End Property

    Public Shared PrevBalanceHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PrevBalanceHours, "Prev Balance Hours")
    ''' <summary>
    ''' Gets and sets the Prev Balance Hours value
    ''' </summary>
    <Display(Name:="Prev Balance Hours", Description:="")>
  Public Property PrevBalanceHours() As Decimal
      Get
        Return GetProperty(PrevBalanceHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(PrevBalanceHoursProperty, Value)
      End Set
    End Property

    Public ReadOnly Property PercUsed As Decimal
      Get
        If Me.ReqShifts > 0 And Me.ReqHours > 0 Then
          Dim ShiftUtil As Decimal = (Me.TotalShifts / Me.ReqShifts) * 100
          Dim HrsUtil As Decimal = (Me.TotalHours / Me.ReqHours) * 100
          If ShiftUtil > HrsUtil Then
            Return ShiftUtil
          Else
            Return HrsUtil
          End If
        End If
        Return 0
      End Get
    End Property

    Public Shared CanAddBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanAddBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Add Bookings", Description:="")>
    Public Property CanAddBookings() As Boolean
      Get
        Return GetProperty(CanAddBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanAddBookingsProperty, Value)
      End Set
    End Property

    Public Shared CanMoveBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanMoveBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Move Bookings", Description:="")>
    Public Property CanMoveBookings() As Boolean
      Get
        Return GetProperty(CanMoveBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanMoveBookingsProperty, Value)
      End Set
    End Property

    Public Shared CanDeleteBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanDeleteBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Delete Bookings", Description:="")>
    Public Property CanDeleteBookings() As Boolean
      Get
        Return GetProperty(CanDeleteBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanDeleteBookingsProperty, Value)
      End Set
    End Property

    Public Shared OwningSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OwningSystemID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property OwningSystemID() As Integer?
      Get
        Return GetProperty(OwningSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OwningSystemIDProperty, Value)
      End Set
    End Property

    Public Shared OwningProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OwningProductionAreaID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property OwningProductionAreaID() As Integer?
      Get
        Return GetProperty(OwningProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OwningProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HRHoursCalcultationModeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HRHoursCalcultationModeID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property HRHoursCalcultationModeID() As Integer?
      Get
        Return GetProperty(HRHoursCalcultationModeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HRHoursCalcultationModeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceOrderProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceOrder, "ResourceOrder", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="ResourceOrder", Description:="")>
    Public Property ResourceOrder() As Integer?
      Get
        Return GetProperty(ResourceOrderProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceOrderProperty, Value)
      End Set
    End Property

    Public Shared IsStageHandProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsStageHand, "Is Stage Hand", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Stage Hand", Description:="")>
    Public Property IsStageHand() As Boolean
      Get
        Return GetProperty(IsStageHandProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsStageHandProperty, Value)
      End Set
    End Property

    Public Shared TargetHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TargetHours, "Tgt Hrs")
    ''' <summary>
    ''' Gets and sets the Req Hours value
    ''' </summary>
    <Display(Name:="Tgt Hours", Description:="")>
    Public Property TargetHours() As Decimal
      Get
        Return GetProperty(TargetHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TargetHoursProperty, Value)
      End Set
    End Property

    Public Shared TargetShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TargetShifts, "Tgt Shifts")
    ''' <summary>
    ''' Gets and sets the Req Shifts value
    ''' </summary>
    <Display(Name:="Tgt Shifts", Description:="")>
    Public Property TargetShifts() As Integer
      Get
        Return GetProperty(TargetShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TargetShiftsProperty, Value)
      End Set
    End Property

    Public Shared UtilPercProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.UtilPerc, "Perc")
    ''' <summary>
    ''' Gets and sets the Req Shifts value
    ''' </summary>
    <Display(Name:="Perc", Description:="")>
    Public Property UtilPerc() As Decimal
      Get
        Return GetProperty(UtilPercProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(UtilPercProperty, Value)
      End Set
    End Property

    Public Shared DatesDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DatesDescription, "Dates")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Dates", Description:="")>
    Public ReadOnly Property DatesDescription() As String
      Get
        Return GetProperty(DatesDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RSResourceBookingListProperty As PropertyInfo(Of RSResourceBookingList) = RegisterProperty(Of RSResourceBookingList)(Function(c) c.RSResourceBookingList, "RS Resource Booking List")
    Public ReadOnly Property RSResourceBookingList() As RSResourceBookingList
      Get
        If GetProperty(RSResourceBookingListProperty) Is Nothing Then
          LoadProperty(RSResourceBookingListProperty, Resources.RSResourceBookingList.NewRSResourceBookingList())
        End If
        Return GetProperty(RSResourceBookingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "RS Resource")
        Else
          Return String.Format("Blank {0}", "RS Resource")
        End If
      Else
        Return Me.ResourceName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRSResource() method.

    End Sub

    Public Shared Function NewRSResource() As RSResource

      Return DataPortal.CreateChild(Of RSResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRSResource(dr As SafeDataReader) As RSResource

      Dim r As New RSResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceIDProperty, .GetInt32(0))
          LoadProperty(ResourceNameProperty, .GetString(1))
          LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(IsFreelancerProperty, IsNull(.GetBoolean(9), Nothing))
          'ContractType ShortName
          'LoadProperty(CreatedByProperty, .GetInt32(8))
          'LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
          'LoadProperty(ModifiedByProperty, .GetInt32(10))
          'LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
          LoadProperty(ReqShiftsProperty, .GetInt32(10))
          LoadProperty(ReqHoursProperty, .GetDecimal(11))
          LoadProperty(TotalShiftsProperty, .GetInt32(12))
          LoadProperty(TotalHoursProperty, .GetDecimal(13))
          LoadProperty(StartingBalanceHoursProperty, .GetDecimal(14))
          'LoadProperty(ShortfallHoursProperty, .GetDecimal(15))
          'LoadProperty(PrevBalanceHoursProperty, .GetDecimal(16))
          LoadProperty(CanAddBookingsProperty, .GetBoolean(15))
          LoadProperty(CanMoveBookingsProperty, .GetBoolean(16))
          LoadProperty(CanDeleteBookingsProperty, .GetBoolean(17))
          LoadProperty(OwningSystemIDProperty, ZeroNothing(.GetInt32(18)))
          LoadProperty(OwningProductionAreaIDProperty, ZeroNothing(.GetInt32(19)))
          LoadProperty(ShortNameProperty, .GetString(20))
          LoadProperty(ResourceOrderProperty, ZeroNothing(.GetInt32(21)))
          LoadProperty(IsStageHandProperty, .GetBoolean(22))
          LoadProperty(TargetHoursProperty, .GetDecimal(23))
          LoadProperty(TargetShiftsProperty, .GetInt32(24))
          LoadProperty(UtilPercProperty, .GetDecimal(25))
          LoadProperty(DatesDescriptionProperty, .GetString(26))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRSResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRSResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceID As SqlParameter = .Parameters.Add("@ResourceID", SqlDbType.Int)
          paramResourceID.Value = GetProperty(ResourceIDProperty)
          If Me.IsNew Then
            paramResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceName", GetProperty(ResourceNameProperty))
          .Parameters.AddWithValue("@ResourceTypeID", GetProperty(ResourceTypeIDProperty))
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
          .Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
          '.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ReqShifts", GetProperty(ReqShiftsProperty))
          .Parameters.AddWithValue("@ReqHours", GetProperty(ReqHoursProperty))
          .Parameters.AddWithValue("@TotalShifts", GetProperty(TotalShiftsProperty))
          .Parameters.AddWithValue("@TotalHours", GetProperty(TotalHoursProperty))
          .Parameters.AddWithValue("@StartingBalanceHours", GetProperty(StartingBalanceHoursProperty))
          .Parameters.AddWithValue("@ShortfallHours", GetProperty(ShortfallHoursProperty))
          .Parameters.AddWithValue("@PrevBalanceHours", GetProperty(PrevBalanceHoursProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceIDProperty, paramResourceID.Value)
          End If
          ' update child objects
          If GetProperty(RSResourceBookingListProperty) IsNot Nothing Then
            Me.RSResourceBookingList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(RSResourceBookingListProperty) IsNot Nothing Then
          Me.RSResourceBookingList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRSResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace