﻿' Generated 19 Feb 2016 20:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.Bookings

  <Serializable()> _
  Public Class SchedulerResourceBookingList
    Inherits OBBusinessListBase(Of SchedulerResourceBookingList, SchedulerResourceBooking)

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As SchedulerResourceBooking

      For Each child As SchedulerResourceBooking In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    'Public Function GetSchedulerResourceBookingArea(ResourceBookingID As Integer) As SchedulerResourceBookingArea

    '  Dim obj As SchedulerResourceBookingArea = Nothing
    '  For Each parent As SchedulerResourceBooking In Me
    '    obj = parent.SchedulerResourceBookingAreaList.GetItem(ResourceBookingID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewSchedulerResourceBookingList() As SchedulerResourceBookingList

      Return New SchedulerResourceBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SchedulerResourceBooking In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SchedulerResourceBooking In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace