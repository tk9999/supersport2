﻿' Generated 19 Feb 2016 20:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.Bookings

  <Serializable()> _
  Public Class SchedulerResourceList
    Inherits OBBusinessListBase(Of SchedulerResourceList, SchedulerResource)

    'Public Property TotalRecords As Integer = 0
    'Public Property TotalPages As Integer = 0

#Region " Business Methods "

    Public Function GetItem(ResourceID As Integer) As SchedulerResource

      For Each child As SchedulerResource In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSchedulerResourceBooking(ResourceBookingID As Integer) As SchedulerResourceBooking

      Dim obj As SchedulerResourceBooking = Nothing
      For Each parent As SchedulerResource In Me
        obj = parent.SchedulerResourceBookingList.GetItem(ResourceBookingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits OBCriteriaBase(Of Criteria)

      Public Property ResourceSchedulerID As Integer? = Nothing
      Public Property ResourceIDs As New List(Of Integer)
      Public Property StartDateBuffer As DateTime? = Nothing
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property EndDateBuffer As DateTime? = Nothing

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?,
                     ResourceSchedulerID As Integer?)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ResourceSchedulerID = ResourceSchedulerID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSchedulerResourceList() As SchedulerResourceList

      Return New SchedulerResourceList()

    End Function

    Public Shared Sub BeginGetSchedulerResourceList(CallBack As EventHandler(Of DataPortalResult(Of SchedulerResourceList)))

      Dim dp As New DataPortal(Of SchedulerResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSchedulerResourceList() As SchedulerResourceList

      Return DataPortal.Fetch(Of SchedulerResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SchedulerResource.GetSchedulerResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SchedulerResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          Dim ResourceIDOverride = ZeroNothing(sdr.GetInt32(32))
          If ResourceIDOverride IsNot Nothing Then
            If parent Is Nothing OrElse Not CompareSafe(parent.ResourceID, ResourceIDOverride) Then
              parent = Me.GetItem(ResourceIDOverride)
            End If
          Else
            If parent Is Nothing OrElse parent.ResourceID <> sdr.GetInt32(1) Then
              parent = Me.GetItem(sdr.GetInt32(1))
            End If
          End If
          If parent IsNot Nothing Then
            parent.SchedulerResourceBookingList.RaiseListChangedEvents = False
            parent.SchedulerResourceBookingList.Add(SchedulerResourceBooking.GetSchedulerResourceBooking(sdr))
            parent.SchedulerResourceBookingList.RaiseListChangedEvents = True
          End If
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSchedulerResourceList"
            cm.Parameters.AddWithValue("@ResourceSchedulerID", Singular.Misc.NothingDBNull(crit.ResourceSchedulerID))
            cm.Parameters.AddWithValue("@ResourceIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceIDs)))
            cm.Parameters.AddWithValue("@StartDateBuffer", Singular.Misc.NothingDBNull(crit.StartDateBuffer))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@EndDateBuffer", Singular.Misc.NothingDBNull(crit.EndDateBuffer))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SchedulerResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SchedulerResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace