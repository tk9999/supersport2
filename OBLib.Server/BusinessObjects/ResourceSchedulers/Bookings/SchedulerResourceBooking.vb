﻿' Generated 19 Feb 2016 20:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.Bookings

  <Serializable()> _
  Public Class SchedulerResourceBooking
    Inherits OBBusinessBase(Of SchedulerResourceBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    ''' 
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceBookingIDProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type")>
    Public Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedBy, "Is Being Edited By")
    ''' <summary>
    ''' Gets and sets the Is Being Edited By value
    ''' </summary>
    <Display(Name:="Is Being Edited By", Description:="")>
    Public Property IsBeingEditedBy() As String
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsBeingEditedByProperty, Value)
      End Set
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.InEditDateTime, "In Edit Date Time")
    ''' <summary>
    ''' Gets and sets the In Edit Date Time value
    ''' </summary>
    <Display(Name:="In Edit Date Time", Description:="")>
    Public Property InEditDateTime As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(InEditDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="")>
    Public Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
    Public Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceSecondmentIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHRID, "Production HR", Nothing)
    ''' <summary>
    ''' Gets and sets the Production HR value
    ''' </summary>
    <Display(Name:="Production HR", Description:="")>
    Public Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class")
    ''' <summary>
    ''' Gets and sets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css Class", Description:="")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IgnoreClashes, "Ignore Clashes", False)
    ''' <summary>
    ''' Gets and sets the Ignore Clashes value
    ''' </summary>
    <Display(Name:="Ignore Clashes", Description:="")>
    Public Property IgnoreClashes() As Boolean
      Get
        Return GetProperty(IgnoreClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreClashesProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "Ignore Clashes Reason")
    ''' <summary>
    ''' Gets and sets the Ignore Clashes Reason value
    ''' </summary>
    <Display(Name:="Ignore Clashes Reason", Description:="")>
    Public Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IgnoreClashesReasonProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared IsLockedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLocked, "Is Locked", False)
    ''' <summary>
    ''' Gets and sets the Is Locked value
    ''' </summary>
    <Display(Name:="Is Locked", Description:="")>
    Public Property IsLocked() As Boolean
      Get
        Return GetProperty(IsLockedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsLockedProperty, Value)
      End Set
    End Property

    Public Shared IsLockedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsLockedReason, "Is Locked Reason")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Is Locked Reason", Description:="")>
    Public Property IsLockedReason() As String
      Get
        Return GetProperty(IsLockedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsLockedReasonProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:="")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleID, "Equipment Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Schedule value
    ''' </summary>
    <Display(Name:="Equipment Schedule", Description:="")>
    Public Property EquipmentScheduleID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ParentResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentResourceBookingID, "Parent Resource Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Resource Booking value
    ''' </summary>
    <Display(Name:="Parent Resource Booking", Description:="")>
    Public Property ParentResourceBookingID() As Integer?
      Get
        Return GetProperty(ParentResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Popover Content")>
    Public Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PopoverContentProperty, Value)
      End Set
    End Property

    Public Shared IsSharedTypeProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSharedType, "Is Shared Type", False)
    ''' <summary>
    ''' Gets and sets the Is Shared Type value
    ''' </summary>
    <Display(Name:="Is Shared Type", Description:="")>
    Public Property IsSharedType() As Boolean
      Get
        Return GetProperty(IsSharedTypeProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsSharedTypeProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDOverrideProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDOverride, "ResourceID Override", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="ResourceID Override")>
    Public Property ResourceIDOverride() As Integer?
      Get
        Return GetProperty(ResourceIDOverrideProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDOverrideProperty, Value)
      End Set
    End Property

    Public Shared HasAreaAccessProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasAreaAccess, "HasAreaAccess", False)
    ''' <summary>
    ''' Gets and sets the Is Shared Type value
    ''' </summary>
    <Display(Name:="HasAreaAccess", Description:="")>
    Public Property HasAreaAccess() As Boolean
      Get
        Return GetProperty(HasAreaAccessProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasAreaAccessProperty, Value)
      End Set
    End Property

    Public Shared PSASystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PSASystemID, "PSASystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="PSASystemID", Description:="")>
    Public Property PSASystemID() As Integer?
      Get
        Return GetProperty(PSASystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PSASystemIDProperty, Value)
      End Set
    End Property

    Public Shared PSAProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PSAProductionAreaID, "PSAProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="PSAProductionAreaID", Description:="")>
    Public Property PSAProductionAreaID() As Integer?
      Get
        Return GetProperty(PSAProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PSAProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisplayInBackgroundProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DisplayInBackground, "DisplayInBackground", False)
    ''' <summary>
    ''' Gets and sets the Is Shared Type value
    ''' </summary>
    <Display(Name:="DisplayInBackground", Description:="")>
    Public Property DisplayInBackground() As Boolean
      Get
        Return GetProperty(DisplayInBackgroundProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DisplayInBackgroundProperty, Value)
      End Set
    End Property

    Public Shared AreaNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AreaName, "Area")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public Property AreaName() As String
      Get
        Return GetProperty(AreaNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AreaNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Non Database Properties "

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Resource")>
    Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceNameProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingBaseTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingBaseType, "Type", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Type")>
    Public Property ResourceBookingBaseType() As String
      Get
        Return GetProperty(ResourceBookingBaseTypeProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceBookingBaseTypeProperty, value)
      End Set
    End Property

    Public Shared IsReceivingUpdateProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsReceivingUpdate, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Receiving Update", Description:="")>
    Public Property IsReceivingUpdate() As Boolean
      Get
        Return GetProperty(IsReceivingUpdateProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsReceivingUpdateProperty, Value)
      End Set
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.HasClashes, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="HasClashes"),
    InitialDataOnly>
    Public Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesProperty, Value)
      End Set
    End Property

    <InitialDataOnly>
    Public Property IsClientValid As Boolean
      Get
        Return Me.IsValid
      End Get
      Set(value As Boolean)

      End Set
    End Property

    Public Shared OriginalStartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalStartDateTimeBuffer, "Original Start Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Original Start Date Time Buffer")>
    Public ReadOnly Property OriginalStartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(OriginalStartDateTimeBufferProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(OriginalStartDateTimeBufferProperty, Value)
      'End Set
    End Property

    Public Shared OriginalStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalStartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property OriginalStartDateTime As DateTime?
      Get
        Return GetProperty(OriginalStartDateTimeProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(StartDateTimeProperty, Value)
      'End Set
    End Property

    Public Shared OriginalEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalEndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property OriginalEndDateTime As DateTime?
      Get
        Return GetProperty(OriginalEndDateTimeProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(EndDateTimeProperty, Value)
      'End Set
    End Property

    Public Shared OriginalEndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalEndDateTimeBuffer, "Original End Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Original End Date Time Buffer")>
    Public ReadOnly Property OriginalEndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(OriginalEndDateTimeBufferProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(OriginalEndDateTimeBufferProperty, Value)
      'End Set
    End Property

    Public Shared StartBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartBufferMinutes, "StartBufferMinutes", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="StartBufferMinutes", Description:="")>
    Public Property StartBufferMinutes() As Integer
      Get
        Return GetProperty(StartBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartBufferMinutesProperty, Value)
      End Set
    End Property

    Public Shared EndBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndBufferMinutes, "EndBufferMinutes", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="EndBufferMinutes", Description:="")>
    Public Property EndBufferMinutes() As Integer
      Get
        Return GetProperty(EndBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EndBufferMinutesProperty, Value)
      End Set
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared SchedulerResourceBookingAreaListProperty As PropertyInfo(Of SchedulerResourceBookingAreaList) = RegisterProperty(Of SchedulerResourceBookingAreaList)(Function(c) c.SchedulerResourceBookingAreaList, "RS Resource Booking Area List")

    '    Public ReadOnly Property SchedulerResourceBookingAreaList() As SchedulerResourceBookingAreaList
    '      Get
    '        If GetProperty(SchedulerResourceBookingAreaListProperty) Is Nothing Then
    '          LoadProperty(SchedulerResourceBookingAreaListProperty, Resources.SchedulerResourceBookingAreaList.NewSchedulerResourceBookingAreaList())
    '        End If
    '        Return GetProperty(SchedulerResourceBookingAreaListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Public Function GetParent() As SchedulerResource

      Return CType(CType(Me.Parent, SchedulerResourceBookingList).Parent, SchedulerResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "RS Resource Booking")
        Else
          Return String.Format("Blank {0}", "RS Resource Booking")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Function GetStartBufferMinutes() As Integer
      If Me.StartDateTimeBuffer IsNot Nothing Then
        Return Me.StartDateTime.Value.Subtract(Me.StartDateTimeBuffer.Value).TotalMinutes
      Else
        Return 0
      End If
    End Function

    Public Function GetEndBufferMinutes() As Integer
      If Me.EndDateTimeBuffer IsNot Nothing Then
        Return Me.EndDateTimeBuffer.Value.Subtract(Me.EndDateTime.Value).TotalMinutes
      Else
        Return 0
      End If
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSchedulerResourceBooking() method.

    End Sub

    Public Shared Function NewSchedulerResourceBooking() As SchedulerResourceBooking

      Return DataPortal.CreateChild(Of SchedulerResourceBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSchedulerResourceBooking(dr As SafeDataReader) As SchedulerResourceBooking

      Dim r As New SchedulerResourceBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
          LoadProperty(IsBeingEditedByProperty, .GetString(8))
          LoadProperty(InEditDateTimeProperty, .GetValue(9))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(IsCancelledProperty, .GetBoolean(16))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(StatusCssClassProperty, .GetString(20))
          LoadProperty(IgnoreClashesProperty, .GetBoolean(21))
          LoadProperty(IgnoreClashesReasonProperty, .GetString(22))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(IsLockedProperty, .GetBoolean(24))
          LoadProperty(IsLockedReasonProperty, .GetString(25))
          LoadProperty(IsTBCProperty, .GetBoolean(26))
          LoadProperty(EquipmentScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
          LoadProperty(ParentResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(29)))
          LoadProperty(PopoverContentProperty, .GetString(30))
          LoadProperty(IsSharedTypeProperty, .GetBoolean(31))
          LoadProperty(ResourceIDOverrideProperty, ZeroNothing(.GetInt32(32)))
          LoadProperty(HasAreaAccessProperty, .GetBoolean(33))
          LoadProperty(PSASystemIDProperty, ZeroNothing(.GetInt32(34)))
          LoadProperty(PSAProductionAreaIDProperty, ZeroNothing(.GetInt32(35)))
          LoadProperty(DisplayInBackgroundProperty, .GetBoolean(36))
          LoadProperty(ResourceNameProperty, .GetString(37))
          LoadProperty(AreaNameProperty, .GetString(38))
        End With
      End Using
      LoadProperty(OriginalStartDateTimeBufferProperty, StartDateTimeBuffer)
      LoadProperty(OriginalStartDateTimeProperty, StartDateTime)
      LoadProperty(OriginalEndDateTimeProperty, EndDateTime)
      LoadProperty(OriginalEndDateTimeBufferProperty, EndDateTimeBuffer)
      LoadProperty(StartBufferMinutesProperty, GetStartBufferMinutes)
      LoadProperty(EndBufferMinutesProperty, GetEndBufferMinutes)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSchedulerResourceBooking"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSchedulerResourceBooking"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceID", Me.GetParent().ResourceID)
          .Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
          .Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@IsBeingEditedBy", GetProperty(IsBeingEditedByProperty))
          .Parameters.AddWithValue("@InEditDateTime", (New SmartDate(GetProperty(InEditDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
          .Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
          .Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
          .Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
          .Parameters.AddWithValue("@HumanResourceOffPeriodID", GetProperty(HumanResourceOffPeriodIDProperty))
          .Parameters.AddWithValue("@HumanResourceSecondmentID", GetProperty(HumanResourceSecondmentIDProperty))
          .Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
          .Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
          .Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
          .Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
          .Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
          .Parameters.AddWithValue("@IsLocked", GetProperty(IsLockedProperty))
          .Parameters.AddWithValue("@IsLockedReason", GetProperty(IsLockedReasonProperty))
          .Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
          .Parameters.AddWithValue("@EquipmentScheduleID", GetProperty(EquipmentScheduleIDProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
          .Parameters.AddWithValue("@ParentResourceBookingID", GetProperty(ParentResourceBookingIDProperty))
          .Parameters.AddWithValue("@IsSharedType", GetProperty(IsSharedTypeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          '' update child objects
          'If GetProperty(SchedulerResourceBookingAreaListProperty) IsNot Nothing Then
          '  Me.SchedulerResourceBookingAreaList.Update()
          'End If
          MarkOld()
        End With
      Else
        '' update child objects
        'If GetProperty(SchedulerResourceBookingAreaListProperty) IsNot Nothing Then
        '  Me.SchedulerResourceBookingAreaList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSchedulerResourceBooking"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace