﻿' Generated 19 Feb 2016 20:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources

  <Serializable()> _
  Public Class RSResourceBookingArea
    Inherits OBBusinessBase(Of RSResourceBookingArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared BookingPSAIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BookingPSAID, "Booking PSA", Nothing)
    ''' <summary>
    ''' Gets and sets the Booking PSA value
    ''' </summary>
    <Display(Name:="Booking PSA", Description:="")>
    Public Property BookingPSAID() As Integer?
      Get
        Return GetProperty(BookingPSAIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BookingPSAIDProperty, Value)
      End Set
    End Property

    Public Shared AreaPSAIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AreaPSAID, "Area PSA", Nothing)
    ''' <summary>
    ''' Gets and sets the Area PSA value
    ''' </summary>
    <Display(Name:="Area PSA", Description:="")>
    Public Property AreaPSAID() As Integer?
      Get
        Return GetProperty(AreaPSAIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AreaPSAIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Production Area Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
  Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared PositionCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionCount, "Position Count")
    ''' <summary>
    ''' Gets and sets the Position Count value
    ''' </summary>
    <Display(Name:="Position Count", Description:="")>
  Public Property PositionCount() As Integer
      Get
        Return GetProperty(PositionCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PositionCountProperty, Value)
      End Set
    End Property

    Public Shared NotAssignedCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotAssignedCount, "Not Assigned Count")
    ''' <summary>
    ''' Gets and sets the Not Assigned Count value
    ''' </summary>
    <Display(Name:="Not Assigned Count", Description:="")>
  Public Property NotAssignedCount() As Integer
      Get
        Return GetProperty(NotAssignedCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NotAssignedCountProperty, Value)
      End Set
    End Property

    Public Shared AssignedCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AssignedCount, "Assigned Count")
    ''' <summary>
    ''' Gets and sets the Assigned Count value
    ''' </summary>
    <Display(Name:="Assigned Count", Description:="")>
  Public Property AssignedCount() As Integer
      Get
        Return GetProperty(AssignedCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AssignedCountProperty, Value)
      End Set
    End Property

    Public Shared HasMissingRequirementsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasMissingRequirements, "Has Missing Requirements", False)
    ''' <summary>
    ''' Gets and sets the Has Missing Requirements value
    ''' </summary>
    <Display(Name:="Has Missing Requirements", Description:="")>
  Public Property HasMissingRequirements() As Boolean
      Get
        Return GetProperty(HasMissingRequirementsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasMissingRequirementsProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RSResourceBooking

      Return CType(CType(Me.Parent, RSResourceBookingAreaList).Parent, RSResourceBooking)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.System.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "RS Resource Booking Area")
        Else
          Return String.Format("Blank {0}", "RS Resource Booking Area")
        End If
      Else
        Return Me.System
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRSResourceBookingArea() method.

    End Sub

    Public Shared Function NewRSResourceBookingArea() As RSResourceBookingArea

      Return DataPortal.CreateChild(Of RSResourceBookingArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRSResourceBookingArea(dr As SafeDataReader) As RSResourceBookingArea

      Dim r As New RSResourceBookingArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(BookingPSAIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AreaPSAIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SystemProperty, .GetString(4))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionAreaProperty, .GetString(6))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(ProductionAreaStatusProperty, .GetString(8))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(PositionCountProperty, .GetInt32(10))
          LoadProperty(NotAssignedCountProperty, .GetInt32(11))
          LoadProperty(AssignedCountProperty, .GetInt32(12))
          LoadProperty(HasMissingRequirementsProperty, .GetBoolean(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRSResourceBookingArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRSResourceBookingArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@BookingPSAID", GetProperty(BookingPSAIDProperty))
          .Parameters.AddWithValue("@AreaPSAID", GetProperty(AreaPSAIDProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@System", GetProperty(SystemProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ProductionArea", GetProperty(ProductionAreaProperty))
          .Parameters.AddWithValue("@ProductionAreaStatusID", GetProperty(ProductionAreaStatusIDProperty))
          .Parameters.AddWithValue("@ProductionAreaStatus", GetProperty(ProductionAreaStatusProperty))
          .Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
          .Parameters.AddWithValue("@PositionCount", GetProperty(PositionCountProperty))
          .Parameters.AddWithValue("@NotAssignedCount", GetProperty(NotAssignedCountProperty))
          .Parameters.AddWithValue("@AssignedCount", GetProperty(AssignedCountProperty))
          .Parameters.AddWithValue("@HasMissingRequirements", GetProperty(HasMissingRequirementsProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRSResourceBookingArea"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace