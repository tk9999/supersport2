﻿' Generated 23 Oct 2015 11:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace ResourceSchedulers.Base

  <Serializable()> _
  Public MustInherit Class ResourceSchedulerBase(Of T As ResourceSchedulerBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ResourceSchedulerID() As Integer
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
    End Property

    Public Shared ResourceSchedulerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceScheduler, "Name", "")
    ''' <summary>
    ''' Gets and sets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(150, ErrorMessage:="Name cannot be more than 150 characters")>
    Public Property ResourceScheduler() As String
      Get
        Return GetProperty(ResourceSchedulerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceSchedulerProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    '    Public Shared ResourceSchedulerSystemAreaListProperty As PropertyInfo(Of ResourceSchedulerSystemAreaList) = RegisterProperty(Of ResourceSchedulerSystemAreaList)(Function(c) c.ResourceSchedulerSystemAreaList, "Resource Scheduler System Area List")
    '    Public ReadOnly Property ResourceSchedulerSystemAreaList() As ResourceSchedulerSystemAreaList
    '      Get
    '        If GetProperty(ResourceSchedulerSystemAreaListProperty) Is Nothing Then
    '          LoadProperty(ResourceSchedulerSystemAreaListProperty, ResourceSchedulers.Base.ResourceSchedulerSystemAreaList.NewResourceSchedulerSystemAreaList())
    '        End If
    '        Return GetProperty(ResourceSchedulerSystemAreaListProperty)
    '      End Get
    '    End Property

    '    Public Shared RSGroupBaseListProperty As PropertyInfo(Of RSGroupBaseList) = RegisterProperty(Of RSGroupBaseList)(Function(c) c.RSGroupBaseList, "RS Group Base List")
    '    Public ReadOnly Property RSGroupBaseList() As RSGroupBaseList
    '      Get
    '        If GetProperty(RSGroupBaseListProperty) Is Nothing Then
    '          LoadProperty(RSGroupBaseListProperty, ResourceSchedulers.Base.RSGroupBaseList.NewRSGroupBaseList())
    '        End If
    '        Return GetProperty(RSGroupBaseListProperty)
    '      End Get
    '    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceSchedulerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceScheduler.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Scheduler Base")
        Else
          Return String.Format("Blank {0}", "Resource Scheduler Base")
        End If
      Else
        Return Me.ResourceScheduler
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ResourceSchedulerSystemAreas", "RSGroups"}
      End Get
    End Property

#End Region

#Region " Base Properties and Methods"

    <Browsable(False)>
    Public Overridable Property UpdateCommand As String = "UpsProcsWeb.updResourceSchedulerBase"

    <Browsable(False)>
    Public Overridable Property InsertCommand As String = "InsProcsWeb.insResourceSchedulerBase"

    <Browsable(False)>
    Public Overridable Property DeleteCommand As String = "DelProcsWeb.delResourceSchedulerBase"

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()
    Public MustOverride Function GetResourceScheduler(dr As SafeDataReader) As ResourceSchedulerBase(Of T)

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceSchedulerBase() method.

    End Sub

    Public Shared Function NewResourceSchedulerBase() As ResourceSchedulerBase(Of T)

      Return DataPortal.CreateChild(Of ResourceSchedulerBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " .Net Data Access "

    'Friend Shared Function GetResourceSchedulerBase(rsb As ResourceSchedulerBase(Of T), dr As SafeDataReader) As ResourceSchedulerBase(Of T)

    '  'Return ResourceSchedulerBase.GetResourceScheduler(dr)
    '  'Dim r As ResourceSchedulerBase(Of T)
    '  rsb.Fetch(dr)
    '  Return rsb

    'End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceSchedulerIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerProperty, .GetString(1))
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsertCommand
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdateCommand
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceSchedulerID As SqlParameter = .Parameters.Add("@ResourceSchedulerID", SqlDbType.Int)
          paramResourceSchedulerID.Value = GetProperty(ResourceSchedulerIDProperty)
          If Me.IsNew Then
            paramResourceSchedulerID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceScheduler", GetProperty(ResourceSchedulerProperty))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceSchedulerIDProperty, paramResourceSchedulerID.Value)
          End If
          ' update child objects
          'If GetProperty(ResourceSchedulerSystemAreaListProperty) IsNot Nothing Then
          '  Me.ResourceSchedulerSystemAreaList.Update()
          'End If
          'If GetProperty(RSGroupBaseListProperty) IsNot Nothing Then
          '  Me.RSGroupBaseList.Update()
          'End If
          UpdateChildLists()
          MarkOld()
        End With
      Else
        ' update child objects
        'If GetProperty(ResourceSchedulerSystemAreaListProperty) IsNot Nothing Then
        '  Me.ResourceSchedulerSystemAreaList.Update()
        'End If
        'If GetProperty(RSGroupBaseListProperty) IsNot Nothing Then
        '  Me.RSGroupBaseList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DeleteCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceSchedulerID", GetProperty(ResourceSchedulerIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End Region

#End Region

  End Class

End Namespace