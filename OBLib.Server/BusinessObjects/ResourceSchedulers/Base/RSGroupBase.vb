﻿' Generated 23 Oct 2015 11:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace ResourceSchedulers.Base

  <Serializable()> _
  Public MustInherit Class RSGroupBase(Of T As RSGroupBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RSGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key>
    Public ReadOnly Property RSGroupID() As Integer
      Get
        Return GetProperty(RSGroupIDProperty)
      End Get
    End Property

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceSchedulerID, "Resource Scheduler", Nothing)
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    Public Property ResourceSchedulerID() As Integer?
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceSchedulerIDProperty, value)
      End Set
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    DropDownWeb(GetType(ROResourceTypeList)),
    Required(ErrorMessage:="Resource Type is required")>
    Public Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupName, "Name", "")
    ''' <summary>
    ''' Gets and sets the Group Name value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(50, ErrorMessage:="Group Name cannot be more than 50 characters"),
    Required(AllowEmptyStrings:=False)>
    Public Property GroupName() As String
      Get
        Return GetProperty(GroupNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GroupNameProperty, Value)
      End Set
    End Property

    Public Shared GroupOrderProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupOrder, "Order", Nothing)
    ''' <summary>
    ''' Gets and sets the Group Order value
    ''' </summary>
    <Display(Name:="Order", Description:=""),
    Required(ErrorMessage:="Order required")>
    Public Property GroupOrder() As Integer?
      Get
        Return GetProperty(GroupOrderProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(GroupOrderProperty, Value)
      End Set
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Default Selection", False)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Default Selection", Description:=""),
    Required(ErrorMessage:="Default Selection required")>
    Public Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedByDefaultProperty, Value)
      End Set
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared RSSubGroupBaseListProperty As PropertyInfo(Of RSSubGroupBaseList) = RegisterProperty(Of RSSubGroupBaseList)(Function(c) c.RSSubGroupBaseList, "RS Sub Group Base List")

    '    Public ReadOnly Property RSSubGroupBaseList() As RSSubGroupBaseList
    '      Get
    '        If GetProperty(RSSubGroupBaseListProperty) Is Nothing Then
    '          LoadProperty(RSSubGroupBaseListProperty, ResourceSchedulers.Base.RSSubGroupBaseList.NewRSSubGroupBaseList())
    '        End If
    '        Return GetProperty(RSSubGroupBaseListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    'Public Function GetParent() As ResourceSchedulerBase

    '  Return CType(CType(Me.Parent, RSGroupBaseList).Parent, ResourceSchedulerBase)

    'End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RSGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GroupName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "RS Group Base")
        Else
          Return String.Format("Blank {0}", "RS Group Base")
        End If
      Else
        Return Me.GroupName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"RSSubGroups"}
      End Get
    End Property

#End Region

#Region " Base Properties and Methods"

    <Browsable(False)>
    Public Overridable Property UpdateCommand As String = "UpdProcsWeb.updRSGroupBase"
    <Browsable(False)>
    Public Overridable Property InsertCommand As String = "InsProcsWeb.insRSGroupBase"
    <Browsable(False)>
    Public Overridable Property DeleteCommand As String = "DelProcsWeb.delRSGroupBase"

    Public MustOverride Function GetParent() As Object
    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()
    Public MustOverride Function GetRSGroup(dr As SafeDataReader) As RSGroupBase(Of T)

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRSGroupBase() method.

    End Sub

    Public Shared Function NewRSGroupBase() As RSGroupBase(Of T)

      Return DataPortal.CreateChild(Of RSGroupBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Friend Shared Function GetRSGroupBase(dr As SafeDataReader) As RSGroupBase

    '  Dim r As New RSGroupBase()
    '  r.Fetch(dr)
    '  Return r

    'End Function

    Public Sub PublicFetch(sdr As SafeDataReader)
      Using BypassPropertyChecks
        With sdr
          LoadProperty(RSGroupIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceTypeIDProperty, ZeroNothing(.GetInt32(2)))
          LoadProperty(GroupNameProperty, .GetString(3))
          LoadProperty(GroupOrderProperty, ZeroNothing(.GetInt32(4)))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(5))
        End With
      End Using
      FetchExtraProperties(sdr)
      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()
    End Sub

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RSGroupIDProperty, .GetInt32(0))
          LoadProperty(ResourceSchedulerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceTypeIDProperty, .GetInt32(2))
          LoadProperty(GroupNameProperty, .GetString(3))
          LoadProperty(GroupOrderProperty, .GetInt32(4))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(5))
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsertCommand
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdateCommand
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRSGroupID As SqlParameter = .Parameters.Add("@RSGroupID", SqlDbType.Int)
          paramRSGroupID.Value = GetProperty(RSGroupIDProperty)
          If Me.IsNew Then
            paramRSGroupID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent() Is Nothing Then
            .Parameters.AddWithValue("@ResourceSchedulerID", NothingDBNull(GetProperty(ResourceSchedulerIDProperty)))
          Else
            .Parameters.AddWithValue("@ResourceSchedulerID", Me.GetParent().ResourceSchedulerID)
          End If
          .Parameters.AddWithValue("@ResourceTypeID", GetProperty(ResourceTypeIDProperty))
          .Parameters.AddWithValue("@GroupName", GetProperty(GroupNameProperty))
          .Parameters.AddWithValue("@GroupOrder", GetProperty(GroupOrderProperty))
          .Parameters.AddWithValue("@SelectedByDefault", GetProperty(SelectedByDefaultProperty))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RSGroupIDProperty, paramRSGroupID.Value)
          End If
          '' update child objects
          'If GetProperty(RSSubGroupBaseListProperty) IsNot Nothing Then
          '  Me.RSSubGroupBaseList.Update()
          'End If
          UpdateChildLists()
          MarkOld()
        End With
      Else
        '' update child objects
        'If GetProperty(RSSubGroupBaseListProperty) IsNot Nothing Then
        '  Me.RSSubGroupBaseList.Update()
        'End If
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DeleteCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RSGroupID", GetProperty(RSGroupIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace