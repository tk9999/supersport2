﻿' Generated 23 Oct 2015 11:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace ResourceSchedulers.Base

  <Serializable()> _
  Public MustInherit Class RSSubGroupResourceBase(Of T As RSSubGroupResourceBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RSSubGroupResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSSubGroupResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RSSubGroupResourceID() As Integer
      Get
        Return GetProperty(RSSubGroupResourceIDProperty)
      End Get
    End Property

    Public Shared RSSubGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RSSubGroupID, "RS Sub Group", Nothing)
    ''' <summary>
    ''' Gets the RS Sub Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property RSSubGroupID() As Integer?
      Get
        Return GetProperty(RSSubGroupIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceOrder, "Resource Order", 0)
    ''' <summary>
    ''' Gets and sets the Resource Order value
    ''' </summary>
    <Display(Name:="Resource Order", Description:=""),
    Required(ErrorMessage:="Resource Order required")>
    Public Property ResourceOrder() As Integer
      Get
        Return GetProperty(ResourceOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceOrderProperty, Value)
      End Set
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Selected By Default", False)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Selected By Default", Description:=""),
    Required(ErrorMessage:="Selected By Default required")>
    Public Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedByDefaultProperty, Value)
      End Set
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name", "")
    ''' <summary>
    ''' Gets and sets the Group Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:=""),
    StringLength(50, ErrorMessage:="Group Name cannot be more than 50 characters")>
    Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Resource Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CustomResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property CustomResourceID() As Integer?
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CustomResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CanAddBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanAddBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Add Bookings", Description:="")>
    Public Property CanAddBookings() As Boolean
      Get
        Return GetProperty(CanAddBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanAddBookingsProperty, Value)
      End Set
    End Property

    Public Shared CanMoveBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanMoveBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Move Bookings", Description:="")>
    Public Property CanMoveBookings() As Boolean
      Get
        Return GetProperty(CanMoveBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanMoveBookingsProperty, Value)
      End Set
    End Property

    Public Shared CanDeleteBookingsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanDeleteBookings, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Can Delete Bookings", Description:="")>
    Public Property CanDeleteBookings() As Boolean
      Get
        Return GetProperty(CanDeleteBookingsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanDeleteBookingsProperty, Value)
      End Set
    End Property

    Public Shared RoomOwnedBySystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomOwnedBySystemID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property RoomOwnedBySystemID() As Integer?
      Get
        Return GetProperty(RoomOwnedBySystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomOwnedBySystemIDProperty, Value)
      End Set
    End Property

    Public Shared RoomOwnedByProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomOwnedByProductionAreaID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property RoomOwnedByProductionAreaID() As Integer?
      Get
        Return GetProperty(RoomOwnedByProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomOwnedByProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "ContractType", Nothing)
    ''' <summary>
    ''' Gets and sets the ContractType value
    ''' </summary>
    <Display(Name:="ContractType")>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.IsFreelancer, "Is Freelancer", CType(Nothing, Boolean))
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Freelancer")>
    Public Property IsFreelancer() As Boolean?
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(IsFreelancerProperty, Value)
      End Set
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShortNameProperty, Value)
      End Set
    End Property

    Public Shared ReqShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReqShifts, "Req Shifts")
    ''' <summary>
    ''' Gets and sets the Req Shifts value
    ''' </summary>
    <Display(Name:="Req Shifts", Description:="")>
    Public Property ReqShifts() As Integer
      Get
        Return GetProperty(ReqShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReqShiftsProperty, Value)
      End Set
    End Property

    Public Shared ReqHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReqHours, "Req Hours")
    ''' <summary>
    ''' Gets and sets the Req Hours value
    ''' </summary>
    <Display(Name:="Req Hours", Description:="")>
    Public Property ReqHours() As Integer
      Get
        Return GetProperty(ReqHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReqHoursProperty, Value)
      End Set
    End Property

    Public Shared TotalShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShifts, "Total Shifts")
    ''' <summary>
    ''' Gets and sets the Total Shifts value
    ''' </summary>
    <Display(Name:="Total Shifts", Description:="")>
    Public Property TotalShifts() As Integer
      Get
        Return GetProperty(TotalShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TotalShiftsProperty, Value)
      End Set
    End Property

    Public Shared TotalHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalHours, "Total Hours")
    ''' <summary>
    ''' Gets and sets the Total Hours value
    ''' </summary>
    <Display(Name:="Total Hours", Description:="")>
    Public Property TotalHours() As Decimal
      Get
        Return GetProperty(TotalHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalHoursProperty, Value)
      End Set
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours")
    ''' <summary>
    ''' Gets and sets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime Hours", Description:="")>
    Public Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(OvertimeHoursProperty, Value)
      End Set
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours")
    ''' <summary>
    ''' Gets and sets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall Hours", Description:="")>
    Public Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShortfallHoursProperty, Value)
      End Set
    End Property

    Public Shared PrevBalanceHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PrevBalanceHours, "Prev Balance Hours")
    ''' <summary>
    ''' Gets and sets the Prev Balance Hours value
    ''' </summary>
    <Display(Name:="Prev Balance Hours", Description:="")>
    Public Property PrevBalanceHours() As Decimal
      Get
        Return GetProperty(PrevBalanceHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(PrevBalanceHoursProperty, Value)
      End Set
    End Property

    Public ReadOnly Property PercUsed As Decimal
      Get
        If Me.ReqShifts > 0 And Me.ReqHours > 0 Then
          Dim ShiftUtil As Decimal = (Me.TotalShifts / Me.ReqShifts) * 100
          Dim HrsUtil As Decimal = (Me.TotalHours / Me.ReqHours) * 100
          If ShiftUtil > HrsUtil Then
            Return ShiftUtil
          Else
            Return HrsUtil
          End If
        End If
        Return 0
      End Get
    End Property

#End Region

#Region " Methods "

    'Public Function GetParent() As RSSubGroupBase(OF 

    '  Return CType(CType(Me.Parent, RSSubGroupResourceBaseList).Parent, RSSubGroupBase)

    'End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RSSubGroupResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RSSubGroupResourceID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "RS Sub Group Resource Base")
        Else
          Return String.Format("Blank {0}", "RS Sub Group Resource Base")
        End If
      Else
        Return Me.RSSubGroupResourceID.ToString()
      End If

    End Function

#End Region

#Region " Base Properties and Methods"

    <Browsable(False)>
    Public Overridable Property UpdateCommand As String = "UpdProcsWeb.updRSSubGroupResourceBase"
    <Browsable(False)>
    Public Overridable Property InsertCommand As String = "InsProcsWeb.insRSSubGroupResourceBase"
    <Browsable(False)>
    Public Overridable Property DeleteCommand As String = "DelProcsWeb.delRSSubGroupResourceBase"

    Public MustOverride Function GetParent() As Object
    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()
    Public MustOverride Function GetRSSubGroupResource(dr As SafeDataReader) As RSSubGroupResourceBase(Of T)

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRSSubGroupResourceBase() method.

    End Sub

    Public Shared Function NewRSSubGroupResourceBase() As RSSubGroupResourceBase(Of T)

      Return DataPortal.CreateChild(Of RSSubGroupResourceBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Function GetRSSubGroupResourceBase(dr As SafeDataReader) As RSSubGroupResourceBase(Of T)
      Me.Fetch(dr)
      Return Me
    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RSSubGroupResourceIDProperty, .GetInt32(0))
          LoadProperty(RSSubGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceOrderProperty, .GetInt32(3))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(4))
          LoadProperty(ResourceNameProperty, .GetString(5))
          LoadProperty(ResourceTypeIDProperty, ZeroNothing(.GetInt32(6)))
          LoadProperty(HumanResourceIDProperty, ZeroNothing(.GetInt32(7)))
          LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(8)))
          LoadProperty(VehicleIDProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(EquipmentIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(ChannelIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(CustomResourceIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(ContractTypeIDProperty, ZeroNothing(.GetInt32(13)))
          LoadProperty(IsFreelancerProperty, .GetBoolean(14))
          LoadProperty(ReqShiftsProperty, .GetInt32(15))
          LoadProperty(ReqHoursProperty, .GetInt32(16))
          LoadProperty(TotalShiftsProperty, .GetInt32(17))
          LoadProperty(TotalHoursProperty, .GetDecimal(18))
          LoadProperty(OvertimeHoursProperty, .GetDecimal(19))
          LoadProperty(ShortfallHoursProperty, .GetDecimal(20))
          LoadProperty(PrevBalanceHoursProperty, .GetDecimal(21))
          LoadProperty(CanAddBookingsProperty, .GetBoolean(22))
          LoadProperty(CanMoveBookingsProperty, .GetBoolean(23))
          LoadProperty(CanDeleteBookingsProperty, .GetBoolean(24))
          LoadProperty(RoomOwnedBySystemIDProperty, ZeroNothing(.GetInt32(25)))
          LoadProperty(RoomOwnedByProductionAreaIDProperty, ZeroNothing(.GetInt32(26)))
          LoadProperty(ShortNameProperty, .GetString(27))
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsertCommand

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdateCommand

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRSSubGroupResourceID As SqlParameter = .Parameters.Add("@RSSubGroupResourceID", SqlDbType.Int)
          paramRSSubGroupResourceID.Value = GetProperty(RSSubGroupResourceIDProperty)
          If Me.IsNew Then
            paramRSSubGroupResourceID.Direction = ParameterDirection.Output
          End If
          '.Parameters.AddWithValue("@RSSubGroupID", Me.GetParent().RSSubGroupID)
          .Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
          .Parameters.AddWithValue("@ResourceOrder", GetProperty(ResourceOrderProperty))
          .Parameters.AddWithValue("@SelectedByDefault", GetProperty(SelectedByDefaultProperty))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RSSubGroupResourceIDProperty, paramRSSubGroupResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          UpdateChildLists()
          MarkOld()
        End With
      Else
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DeleteCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RSSubGroupResourceID", GetProperty(RSSubGroupResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace