﻿' Generated 23 Oct 2015 11:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace ResourceSchedulers.Base

  <Serializable()> _
  Public MustInherit Class RSSubGroupBaseList(Of T As RSSubGroupBaseList(Of T, C), C As RSSubGroupBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(RSSubGroupID As Integer) As RSSubGroupBase(Of C)

      For Each child As RSSubGroupBase(Of C) In Me
        If child.RSSubGroupID = RSSubGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RS Sub Groups"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As RSSubGroupBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As RSSubGroupBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

#Region " Base Methods "

    <Browsable(False)>
    Public Overridable Property GetProcName As String = "[GetProcsWeb].[getRSSubGroupBaseList]"

    Public MustOverride Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub Fetch(sdr As SafeDataReader)

#End Region

  End Class

End Namespace