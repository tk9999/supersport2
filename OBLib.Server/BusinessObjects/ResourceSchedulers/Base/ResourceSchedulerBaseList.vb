﻿' Generated 23 Oct 2015 11:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace ResourceSchedulers.Base

  <Serializable()> _
  Public MustInherit Class ResourceSchedulerBaseList(Of T As ResourceSchedulerBaseList(Of T, C), C As ResourceSchedulerBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

    <Browsable(False)>
    Public MustOverride Property GetProcName As String

#Region " Business Methods "

    Public Function GetItem(ResourceSchedulerID As Integer) As ResourceSchedulerBase(Of C)

      For Each child As ResourceSchedulerBase(Of C) In Me
        If child.ResourceSchedulerID = ResourceSchedulerID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Schedulers"

    End Function

    'Public Function GetResourceSchedulerSystemArea(ResourceSchedulerSystemAreaID As Integer) As ResourceSchedulerSystemArea

    '  Dim obj As ResourceSchedulerSystemArea = Nothing
    '  For Each parent As ResourceSchedulerBase(Of C) In Me
    '    obj = parent.ResourceSchedulerSystemAreaList.GetItem(ResourceSchedulerSystemAreaID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function
    'Public MustOverride Function GetResourceSchedulerSystemArea(ResourceSchedulerSystemAreaID As Integer) As ResourceSchedulerSystemArea

    'Public MustOverride Function GetRSGroup(RSGroupID As Integer) As RSGroupBase(Of T)

    'Public Function GetRSGroupBase(RSGroupID As Integer) As RSGroupBase

    '  Dim obj As RSGroupBase = Nothing
    '  For Each parent As ResourceSchedulerBase(Of C) In Me
    '    obj = parent.RSGroupBaseList.GetItem(RSGroupID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " .Net Data Access "

    'Public MustOverride Function GetList(ResourceSchedulerID As Integer?, USerID As Integer?) As T
    Public MustOverride Sub Fetch(sdr As SafeDataReader)
    Protected Overrides Sub DataPortal_Fetch(criteria As Object)
    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ResourceSchedulerBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ResourceSchedulerBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Public MustOverride Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

#End Region

#End Region

  End Class

End Namespace