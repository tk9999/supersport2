﻿' Generated 23 Oct 2015 11:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace ResourceSchedulers.Base

  <Serializable()> _
  Public MustInherit Class RSSubGroupBase(Of T As RSSubGroupBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RSSubGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RSSubGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RSSubGroupID() As Integer
      Get
        Return GetProperty(RSSubGroupIDProperty)
      End Get
    End Property

    Public Shared RSGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RSGroupID, "RS Group", Nothing)
    ''' <summary>
    ''' Gets the RS Group value
    ''' </summary>
    Public Property RSGroupID() As Integer?
      Get
        Return GetProperty(RSGroupIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RSGroupIDProperty, value)
      End Set
    End Property

    Public Shared SubGroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubGroupName, "Name", "")
    ''' <summary>
    ''' Gets and sets the Sub Group Name value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(50, ErrorMessage:="Sub Group Name cannot be more than 50 characters"),
    Required(AllowEmptyStrings:=False)>
    Public Property SubGroupName() As String
      Get
        Return GetProperty(SubGroupNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SubGroupNameProperty, Value)
      End Set
    End Property

    Public Shared SubGroupOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubGroupOrder, "Order", 0)
    ''' <summary>
    ''' Gets and sets the Sub Group Order value
    ''' </summary>
    <Display(Name:="Order", Description:=""),
    Required(ErrorMessage:="Order required")>
    Public Property SubGroupOrder() As Integer
      Get
        Return GetProperty(SubGroupOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SubGroupOrderProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(RODisciplineList))>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomTypeID, "Room Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:=""),
    DropDownWeb(GetType(RORoomTypeList))>
    Public Property RoomTypeID() As Integer?
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SelectedByDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedByDefault, "Default Selection?", False)
    ''' <summary>
    ''' Gets and sets the Selected By Default value
    ''' </summary>
    <Display(Name:="Default Selection?", Description:=""),
    Required(ErrorMessage:="Selected By Default required")>
    Public Property SelectedByDefault() As Boolean
      Get
        Return GetProperty(SelectedByDefaultProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedByDefaultProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:=""),
    DropDownWeb(GetType(ROContractTypeList))>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared RSSubGroupResourceBaseListProperty As PropertyInfo(Of RSSubGroupResourceBaseList) = RegisterProperty(Of RSSubGroupResourceBaseList)(Function(c) c.RSSubGroupResourceBaseList, "RS Sub Group Resource Base List")

    '    Public ReadOnly Property RSSubGroupResourceBaseList() As RSSubGroupResourceBaseList
    '      Get
    '        If GetProperty(RSSubGroupResourceBaseListProperty) Is Nothing Then
    '          LoadProperty(RSSubGroupResourceBaseListProperty, ResourceSchedulers.Base.RSSubGroupResourceBaseList.NewRSSubGroupResourceBaseList())
    '        End If
    '        Return GetProperty(RSSubGroupResourceBaseListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RSSubGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SubGroupName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "RS Sub Group Base")
        Else
          Return String.Format("Blank {0}", "RS Sub Group Base")
        End If
      Else
        Return Me.SubGroupName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"RSSubGroupResources"}
      End Get
    End Property

#End Region

#Region " Base Properties and Methods"

    <Browsable(False)>
    Public Overridable Property UpdateCommand As String = "UpdProcsWeb.updRSSubGroupBase"
    <Browsable(False)>
    Public Overridable Property InsertCommand As String = "InsProcsWeb.insRSSubGroupBase"
    <Browsable(False)>
    Public Overridable Property DeleteCommand As String = "DelProcsWeb.delRSSubGroupBase"

    Public MustOverride Function GetParent() As Object
    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()
    Public MustOverride Function GetRSSubGroup(dr As SafeDataReader) As RSSubGroupBase(Of T)

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRSSubGroupBase() method.

    End Sub

    Public Shared Function NewRSSubGroupBase() As RSSubGroupBase(Of T)

      Return DataPortal.CreateChild(Of RSSubGroupBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Friend Shared Function GetRSSubGroupBase(dr As SafeDataReader) As RSSubGroupBase(Of T)

    '  Dim r As New RSSubGroupBase(Of T)()
    '  r.Fetch(dr)
    '  Return r

    'End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RSSubGroupIDProperty, .GetInt32(0))
          LoadProperty(RSGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SubGroupNameProperty, .GetString(2))
          LoadProperty(SubGroupOrderProperty, .GetInt32(3))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(SelectedByDefaultProperty, .GetBoolean(6))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsertCommand
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdateCommand
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRSSubGroupID As SqlParameter = .Parameters.Add("@RSSubGroupID", SqlDbType.Int)
          paramRSSubGroupID.Value = GetProperty(RSSubGroupIDProperty)
          If Me.IsNew Then
            paramRSSubGroupID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent() Is Nothing Then
            .Parameters.AddWithValue("@RSGroupID", NothingDBNull(GetProperty(RSGroupIDProperty)))
          Else
            .Parameters.AddWithValue("@RSGroupID", Me.GetParent().RSGroupID)
          End If
          .Parameters.AddWithValue("@SubGroupName", GetProperty(SubGroupNameProperty))
          .Parameters.AddWithValue("@SubGroupOrder", GetProperty(SubGroupOrderProperty))
          .Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@RoomTypeID", Singular.Misc.NothingDBNull(GetProperty(RoomTypeIDProperty)))
          .Parameters.AddWithValue("@SelectedByDefault", GetProperty(SelectedByDefaultProperty))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@ContractTypeID", Singular.Misc.NothingDBNull(GetProperty(ContractTypeIDProperty)))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RSSubGroupIDProperty, paramRSSubGroupID.Value)
          End If
          '' update child objects
          'If GetProperty(RSSubGroupResourceBaseListProperty) IsNot Nothing Then
          '  Me.RSSubGroupResourceBaseList.Update()
          'End If
          UpdateChildLists()
          MarkOld()
        End With
      Else
        '' update child objects
        'If GetProperty(RSSubGroupResourceBaseListProperty) IsNot Nothing Then
        '  Me.RSSubGroupResourceBaseList.Update()
        'End If
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DeleteCommand
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RSSubGroupID", GetProperty(RSSubGroupIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace