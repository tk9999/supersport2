﻿' Generated 10 Apr 2016 13:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ResourceSchedulers

  <Serializable()> _
  Public Class DefaultDateViewType
    Inherits OBBusinessBase(Of DefaultDateViewType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DefaultDateViewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultDateViewTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property DefaultDateViewTypeID() As Integer
      Get
        Return GetProperty(DefaultDateViewTypeIDProperty)
      End Get
    End Property

    Public Shared DefaultDateViewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DefaultDateViewType, "Default Date View Type", "")
    ''' <summary>
    ''' Gets and sets the Default Date View Type value
    ''' </summary>
    <Display(Name:="Default Date View Type", Description:=""),
    StringLength(50, ErrorMessage:="Default Date View Type cannot be more than 50 characters")>
  Public Property DefaultDateViewType() As String
      Get
        Return GetProperty(DefaultDateViewTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DefaultDateViewTypeProperty, Value)
      End Set
    End Property

    Public Shared MaxDaysBeforeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxDaysBefore, "Max Days Before", 0)
    ''' <summary>
    ''' Gets and sets the Max Days Before value
    ''' </summary>
    <Display(Name:="Max Days Before", Description:=""),
    Required(ErrorMessage:="Max Days Before required")>
  Public Property MaxDaysBefore() As Integer
      Get
        Return GetProperty(MaxDaysBeforeProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxDaysBeforeProperty, Value)
      End Set
    End Property

    Public Shared MaxDaysAfterProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxDaysAfter, "Max Days After", 0)
    ''' <summary>
    ''' Gets and sets the Max Days After value
    ''' </summary>
    <Display(Name:="Max Days After", Description:=""),
    Required(ErrorMessage:="Max Days After required")>
  Public Property MaxDaysAfter() As Integer
      Get
        Return GetProperty(MaxDaysAfterProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxDaysAfterProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DefaultDateViewTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.DefaultDateViewType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Default Date View Type")
        Else
          Return String.Format("Blank {0}", "Default Date View Type")
        End If
      Else
        Return Me.DefaultDateViewType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewDefaultDateViewType() method.

    End Sub

    Public Shared Function NewDefaultDateViewType() As DefaultDateViewType

      Return DataPortal.CreateChild(Of DefaultDateViewType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetDefaultDateViewType(dr As SafeDataReader) As DefaultDateViewType

      Dim d As New DefaultDateViewType()
      d.Fetch(dr)
      Return d

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DefaultDateViewTypeIDProperty, .GetInt32(0))
          LoadProperty(DefaultDateViewTypeProperty, .GetString(1))
          LoadProperty(MaxDaysBeforeProperty, .GetInt32(2))
          LoadProperty(MaxDaysAfterProperty, .GetInt32(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insDefaultDateViewType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updDefaultDateViewType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramDefaultDateViewTypeID As SqlParameter = .Parameters.Add("@DefaultDateViewTypeID", SqlDbType.Int)
          paramDefaultDateViewTypeID.Value = GetProperty(DefaultDateViewTypeIDProperty)
          If Me.IsNew Then
            paramDefaultDateViewTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@DefaultDateViewType", GetProperty(DefaultDateViewTypeProperty))
          .Parameters.AddWithValue("@MaxDaysBefore", GetProperty(MaxDaysBeforeProperty))
          .Parameters.AddWithValue("@MaxDaysAfter", GetProperty(MaxDaysAfterProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(DefaultDateViewTypeIDProperty, paramDefaultDateViewTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delDefaultDateViewType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@DefaultDateViewTypeID", GetProperty(DefaultDateViewTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace