﻿' Generated 29 Nov 2016 13:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets

  <Serializable()> _
  Public Class TimesheetRequirementHRList
    Inherits OBBusinessListBase(Of TimesheetRequirementHRList, TimesheetRequirementHR)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As TimesheetRequirementHR

      For Each child As TimesheetRequirementHR In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private Sub AddRequirementSettingsParameterValues(command As SqlCommand, TimesheetRequirement As TimesheetRequirement)

      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!ORDER OF COLUMNS IN THIS DEFINITION MUST MATCH THE ORDER OF THE COLUMNS IN THE TABLE TYPE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Dim RequirementTable As New DataTable
      RequirementTable.Columns.Add("TimesheetRequirementID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RequirementTable.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RequirementTable.Columns.Add("StartDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RequirementTable.Columns.Add("EndDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RequirementTable.Columns.Add("SystemTeamNumberID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!ORDER OF COLUMNS IN THIS DEFINITION MUST MATCH THE ORDER OF THE COLUMNS IN THE TABLE TYPE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Dim row As DataRow = RequirementTable.NewRow
      row("TimesheetRequirementID") = NothingDBNull(TimesheetRequirement.TimesheetRequirementID)
      row("SystemID") = NothingDBNull(TimesheetRequirement.SystemID)
      If IsNullNothing(TimesheetRequirement.StartDate) Then
        row("StartDate") = DBNull.Value
      Else
        row("StartDate") = TimesheetRequirement.StartDate.Value.ToString("yyyy MMMM dd")
      End If
      If IsNullNothing(TimesheetRequirement.EndDate) Then
        row("EndDate") = DBNull.Value
      Else
        row("EndDate") = TimesheetRequirement.EndDate.Value.ToString("yyyy MMMM dd")
      End If
      row("SystemTeamNumberID") = NothingDBNull(TimesheetRequirement.SystemTeamNumberID)
      RequirementTable.Rows.Add(row)


      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!ORDER OF COLUMNS IN THIS DEFINITION MUST MATCH THE ORDER OF THE COLUMNS IN THE TABLE TYPE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Dim RequirementSettings As New DataTable
      RequirementSettings.Columns.Add("TimesheetRequirementSettingID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RequirementSettings.Columns.Add("TimesheetRequirementID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RequirementSettings.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RequirementSettings.Columns.Add("ContractTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RequirementSettings.Columns.Add("DisciplineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!ORDER OF COLUMNS IN THIS DEFINITION MUST MATCH THE ORDER OF THE COLUMNS IN THE TABLE TYPE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      For Each rb As TimesheetRequirementSetting In TimesheetRequirement.TimesheetRequirementSettingList
        Dim row1 As DataRow = RequirementSettings.NewRow
        row1("TimesheetRequirementSettingID") = NothingDBNull(rb.TimesheetRequirementSettingID)
        row1("TimesheetRequirementID") = NothingDBNull(rb.TimesheetRequirementID)
        row1("ProductionAreaID") = NothingDBNull(rb.ProductionAreaID)
        row1("ContractTypeID") = NothingDBNull(rb.ContractTypeID)
        row1("DisciplineID") = NothingDBNull(rb.DisciplineID)
        RequirementSettings.Rows.Add(row1)
      Next

      With command.Parameters.AddWithValue("@TimesheetRequirementOverride", RequirementTable)
        .SqlDbType = System.Data.SqlDbType.Structured
      End With

      With command.Parameters.AddWithValue("@TimesheetRequirementSettingsOverride", RequirementSettings)
        .SqlDbType = System.Data.SqlDbType.Structured
      End With

    End Sub

    Public Sub AddHRTableParameter(ByRef command As SqlCommand)

      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!ORDER OF COLUMNS IN THIS DEFINITION MUST MATCH THE ORDER OF THE COLUMNS IN THE TABLE TYPE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      Dim RequirementHR As New DataTable
      RequirementHR.Columns.Add("HumanResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("HRName", GetType(String)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("ContractTypeID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("ContractType", GetType(String)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("IsRequired", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("HasTimesheetRequirement", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("HumanResourceTimesheetID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("IsClosed", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("ClosedBy", GetType(String)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("IsClosedDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("IsClosedDateTimeString", GetType(String)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("TimesheetRequirementID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("RequirementStartDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RequirementHR.Columns.Add("RequirementEndDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!ORDER OF COLUMNS IN THIS DEFINITION MUST MATCH THE ORDER OF THE COLUMNS IN THE TABLE TYPE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      For Each rb As TimesheetRequirementHR In Me
        Dim row1 As DataRow = RequirementHR.NewRow
        row1("HumanResourceID") = rb.HumanResourceID
        row1("HRName") = rb.HRName
        row1("ContractTypeID") = rb.ContractTypeID
        row1("ContractType") = rb.ContractType
        row1("IsRequired") = rb.IsRequired
        row1("HasTimesheetRequirement") = rb.HasTimesheetRequirement
        row1("HumanResourceTimesheetID") = NothingDBNull(rb.HumanResourceTimesheetID)
        row1("IsClosed") = rb.IsClosed
        row1("ClosedBy") = rb.ClosedBy
        If IsNullNothing(rb.IsClosedDateTime) Then
          row1("IsClosedDateTime") = DBNull.Value
        Else
          row1("IsClosedDateTime") = rb.IsClosedDateTime.Value.ToString("yyyy MMMM dd")
        End If
        row1("IsClosedDateTimeString") = rb.IsClosedDateTimeString
        row1("TimesheetRequirementID") = NothingDBNull(rb.TimesheetRequirementID)
        If IsNullNothing(rb.GetParent.StartDate) Then
          row1("RequirementStartDate") = DBNull.Value
        Else
          row1("RequirementStartDate") = rb.GetParent.StartDate.Value.ToString("yyyy MMMM dd")
        End If
        If IsNullNothing(rb.GetParent.EndDate) Then
          row1("RequirementEndDate") = DBNull.Value
        Else
          row1("RequirementEndDate") = rb.GetParent.EndDate.Value.ToString("yyyy MMMM dd")
        End If
        RequirementHR.Rows.Add(row1)
      Next

      With Command.Parameters.AddWithValue("@TimesheetRequirementHR", RequirementHR)
        .SqlDbType = System.Data.SqlDbType.Structured
        .TypeName = "TimesheetRequirementHRType"
      End With

      command.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      'With command.Parameters.AddWithValue("@TimesheetRequirementHR", RequirementHR)
      '  .SqlDbType = System.Data.SqlDbType.Structured
      'End With

      'With command.Parameters.AddWithValue("@TimesheetRequirementSettingsOverride", RequirementSettings)
      '  .SqlDbType = System.Data.SqlDbType.Structured
      'End With

      'tbale.columns.remove()
      'Dim ds As DataSet = Singular.CSLALib.GetDatasetFromBusinessListBase(TimesheetRequirement.TimesheetRequirementHRList, False, True)
      'Dim x As Object = Nothing

    End Sub

#End Region

#Region " Data Access "

    'Protected Overrides Sub UpdateGeneric()
    '  'MyBase.UpdateGeneric()
    '  Me.BulkSave(Sub(proc As SqlClient.SqlCommand)
    '                proc.CommandText = "UpdProcsWeb.updTimesheetRequirementHRBulk"
    '                Me.AddHRTableParameter(proc)
    '              End Sub)
    'End Sub


    'Protected Overrides Sub DataPortal_Update()
    '  '' MyBase.DataPortal_Update()
    '  'Me.BulkSave(Sub(proc As SqlClient.SqlCommand)
    '  '              proc.CommandText = "UpdProcsWeb.updTimesheetRequirementHRBulk"
    '  '              Me.AddHRTableParameter(proc)
    '  '              proc.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
    '  '              proc.ExecuteNonQuery()
    '  '            End Sub)
    'End Sub

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TimesheetRequirementID As Integer? = Nothing
      Public Property TimesheetRequirement As TimesheetRequirement

      Public Sub New(TimesheetRequirement As TimesheetRequirement)
        Me.TimesheetRequirement = TimesheetRequirement
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewTimesheetRequirementHRList() As TimesheetRequirementHRList

      Return New TimesheetRequirementHRList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetTimesheetRequirementHRList() As TimesheetRequirementHRList

      Return DataPortal.Fetch(Of TimesheetRequirementHRList)(New Criteria())

    End Function

    Public Shared Function GetTimesheetRequirementHRList(TimesheetRequirement As TimesheetRequirement) As TimesheetRequirementHRList

      Return DataPortal.Fetch(Of TimesheetRequirementHRList)(New Criteria(TimesheetRequirement))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TimesheetRequirementHR.GetTimesheetRequirementHR(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTimesheetRequirementHRList"
            cm.Parameters.AddWithValue("@TimesheetRequirementID", Singular.Misc.NothingDBNull(crit.TimesheetRequirementID))
            If crit.TimesheetRequirement Is Nothing Then
              cm.Parameters.AddWithValue("@Override", False)
              cm.Parameters.AddWithValue("@TimesheetRequirementOverride", DBNull.Value)
              cm.Parameters.AddWithValue("@TimesheetRequirementSettingsOverride", DBNull.Value)
            Else
              cm.Parameters.AddWithValue("@Override", True)
              AddRequirementSettingsParameterValues(cm, crit.TimesheetRequirement)
            End If
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace