﻿' Generated 26 Jan 2015 06:36 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Timesheets

  <Serializable()> _
  Public Class TimesheetCategory
    Inherits SingularBusinessBase(Of TimesheetCategory)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetCategoryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property TimesheetCategoryID() As Integer
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetCategory, "Timesheet Category", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:=""),
    StringLength(100, ErrorMessage:="Timesheet Category cannot be more than 100 characters")>
  Public Property TimesheetCategory() As String
      Get
        Return GetProperty(TimesheetCategoryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimesheetCategoryProperty, Value)
      End Set
    End Property

    Public Shared ShortfallCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShortfallCalcInd, "Shortfall Calc", True)
    ''' <summary>
    ''' Gets and sets the Shortfall Calc value
    ''' </summary>
    <Display(Name:="Shortfall Calc", Description:=""),
    Required(ErrorMessage:="Shortfall Calc required")>
  Public Property ShortfallCalcInd() As Boolean
      Get
        Return GetProperty(ShortfallCalcIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShortfallCalcIndProperty, Value)
      End Set
    End Property

    Public Shared OvertimeCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OvertimeCalcInd, "Overtime Calc", True)
    ''' <summary>
    ''' Gets and sets the Overtime Calc value
    ''' </summary>
    <Display(Name:="Overtime Calc", Description:=""),
    Required(ErrorMessage:="Overtime Calc required")>
  Public Property OvertimeCalcInd() As Boolean
      Get
        Return GetProperty(OvertimeCalcIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OvertimeCalcIndProperty, Value)
      End Set
    End Property

    Public Shared ActualHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActualHoursInd, "Actual Hours", True)
    ''' <summary>
    ''' Gets and sets the Actual Hours value
    ''' </summary>
    <Display(Name:="Actual Hours", Description:=""),
    Required(ErrorMessage:="Actual Hours required")>
  Public Property ActualHoursInd() As Boolean
      Get
        Return GetProperty(ActualHoursIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ActualHoursIndProperty, Value)
      End Set
    End Property

    Public Shared AccumulateHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccumulateHoursInd, "Accumulate Hours", True)
    ''' <summary>
    ''' Gets and sets the Accumulate Hours value
    ''' </summary>
    <Display(Name:="Accumulate Hours", Description:=""),
    Required(ErrorMessage:="Accumulate Hours required")>
  Public Property AccumulateHoursInd() As Boolean
      Get
        Return GetProperty(AccumulateHoursIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AccumulateHoursIndProperty, Value)
      End Set
    End Property

    Public Shared MinHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MinHours, "Min Hours", 0)
    ''' <summary>
    ''' Gets and sets the Min Hours value
    ''' </summary>
    <Display(Name:="Min Hours", Description:="")>
  Public Property MinHours() As Decimal
      Get
        Return GetProperty(MinHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(MinHoursProperty, Value)
      End Set
    End Property

    Public Shared MaxHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MaxHours, "Max Hours", 0)
    ''' <summary>
    ''' Gets and sets the Max Hours value
    ''' </summary>
    <Display(Name:="Max Hours", Description:="")>
  Public Property MaxHours() As Decimal
      Get
        Return GetProperty(MaxHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(MaxHoursProperty, Value)
      End Set
    End Property

    Public Shared MinMaxHoursCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MinMaxHoursCalcInd, "Min Max Hours Calc", False)
    ''' <summary>
    ''' Gets and sets the Min Max Hours Calc value
    ''' </summary>
    <Display(Name:="Min Max Hours Calc", Description:=""),
    Required(ErrorMessage:="Min Max Hours Calc required")>
  Public Property MinMaxHoursCalcInd() As Boolean
      Get
        Return GetProperty(MinMaxHoursCalcIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MinMaxHoursCalcIndProperty, Value)
      End Set
    End Property

    Public Shared DefaultStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultStartTime, "Default Start Time")
    ''' <summary>
    ''' Gets and sets the Default Start Time value
    ''' </summary>
    <Display(Name:="Default Start Time", Description:="")>
  Public Property DefaultStartTime() As Object
      Get
        Dim value = GetProperty(DefaultStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(DefaultStartTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(DefaultStartTimeProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(DefaultStartTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared DefaultEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultEndTime, "Default End Time")
    ''' <summary>
    ''' Gets and sets the Default End Time value
    ''' </summary>
    <Display(Name:="Default End Time", Description:="")>
  Public Property DefaultEndTime() As Object
      Get
        Dim value = GetProperty(DefaultEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(DefaultEndTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(DefaultEndTimeProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(DefaultEndTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared FromDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FromDate, "From Date")
    ''' <summary>
    ''' Gets and sets the From Date value
    ''' </summary>
    <Display(Name:="From Date", Description:=""),
    Required(ErrorMessage:="From Date required")>
  Public Property FromDate As DateTime?
      Get
        If Not FieldManager.FieldExists(FromDateProperty) Then
          LoadProperty(FromDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(FromDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FromDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetCategoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TimesheetCategory.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Category")
        Else
          Return String.Format("Blank {0}", "Timesheet Category")
        End If
      Else
        Return Me.TimesheetCategory
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetCategory() method.

    End Sub

    Public Shared Function NewTimesheetCategory() As TimesheetCategory

      Return DataPortal.CreateChild(Of TimesheetCategory)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTimesheetCategory(dr As SafeDataReader) As TimesheetCategory

      Dim t As New TimesheetCategory()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TimesheetCategoryIDProperty, .GetInt32(0))
          LoadProperty(TimesheetCategoryProperty, .GetString(1))
          LoadProperty(ShortfallCalcIndProperty, .GetBoolean(2))
          LoadProperty(OvertimeCalcIndProperty, .GetBoolean(3))
          LoadProperty(ActualHoursIndProperty, .GetBoolean(4))
          LoadProperty(AccumulateHoursIndProperty, .GetBoolean(5))
          LoadProperty(MinHoursProperty, .GetDecimal(6))
          LoadProperty(MaxHoursProperty, .GetDecimal(7))
          LoadProperty(MinMaxHoursCalcIndProperty, .GetBoolean(8))
          If .IsDBNull(9) Then
            LoadProperty(DefaultStartTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(DefaultStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(9))))
          End If
          If .IsDBNull(10) Then
            LoadProperty(DefaultEndTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(DefaultEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(10))))
          End If
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(FromDateProperty, .GetValue(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTimesheetCategory"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTimesheetCategory"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTimesheetCategoryID As SqlParameter = .Parameters.Add("@TimesheetCategoryID", SqlDbType.Int)
          paramTimesheetCategoryID.Value = GetProperty(TimesheetCategoryIDProperty)
          If Me.IsNew Then
            paramTimesheetCategoryID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TimesheetCategory", GetProperty(TimesheetCategoryProperty))
          .Parameters.AddWithValue("@ShortfallCalcInd", GetProperty(ShortfallCalcIndProperty))
          .Parameters.AddWithValue("@OvertimeCalcInd", GetProperty(OvertimeCalcIndProperty))
          .Parameters.AddWithValue("@ActualHoursInd", GetProperty(ActualHoursIndProperty))
          .Parameters.AddWithValue("@AccumulateHoursInd", GetProperty(AccumulateHoursIndProperty))
          .Parameters.AddWithValue("@MinHours", GetProperty(MinHoursProperty))
          .Parameters.AddWithValue("@MaxHours", GetProperty(MaxHoursProperty))
          .Parameters.AddWithValue("@MinMaxHoursCalcInd", GetProperty(MinMaxHoursCalcIndProperty))
          .Parameters.AddWithValue("@DefaultStartTime", (New SmartDate(GetProperty(DefaultStartTimeProperty))).DBValue)
          .Parameters.AddWithValue("@DefaultEndTime", (New SmartDate(GetProperty(DefaultEndTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@FromDate", (New SmartDate(GetProperty(FromDateProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TimesheetCategoryIDProperty, paramTimesheetCategoryID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTimesheetCategory"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TimesheetCategoryID", GetProperty(TimesheetCategoryIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace