﻿' Generated 26 Jan 2015 06:36 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Timesheets

  <Serializable()> _
  Public Class TimesheetMonth
    Inherits SingularBusinessBase(Of TimesheetMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property TimesheetMonthID() As Integer
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
    End Property

    Public Shared TimesheetMonthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonth, "Timesheet Month", 1)
    ''' <summary>
    ''' Gets and sets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="Month of the year for the timesheet"),
    Required(ErrorMessage:="Timesheet Month required")>
  Public Property TimesheetMonth() As Integer
      Get
        Return GetProperty(TimesheetMonthProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetMonthProperty, Value)
      End Set
    End Property

    Public Shared TimesheetYearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetYear, "Timesheet Year", 2012)
    ''' <summary>
    ''' Gets and sets the Timesheet Year value
    ''' </summary>
    <Display(Name:="Timesheet Year", Description:="Year of the timesheet"),
    Required(ErrorMessage:="Timesheet Year required")>
  Public Property TimesheetYear() As Integer
      Get
        Return GetProperty(TimesheetYearProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetYearProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Start Date for the timesheet month"),
    Required(ErrorMessage:="Start Date required")>
  Public Property StartDate As DateTime?
      Get
        If Not FieldManager.FieldExists(StartDateProperty) Then
          LoadProperty(StartDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="End Date for the timesheet month"),
    Required(ErrorMessage:="End Date required")>
  Public Property EndDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EndDateProperty) Then
          LoadProperty(EndDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared HoursBeforeOvertimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HoursBeforeOvertime, "Hours Before Overtime", 180)
    ''' <summary>
    ''' Gets and sets the Hours Before Overtime value
    ''' </summary>
    <Display(Name:="Hours Before Overtime", Description:="Hours before overtime for the timesheet month"),
    Required(ErrorMessage:="Hours Before Overtime required")>
  Public Property HoursBeforeOvertime() As Integer
      Get
        Return GetProperty(HoursBeforeOvertimeProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HoursBeforeOvertimeProperty, Value)
      End Set
    End Property

    Public Shared ClosedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ClosedInd, "Closed", False)
    ''' <summary>
    ''' Gets and sets the Closed value
    ''' </summary>
    <Display(Name:="Closed", Description:="True if the timesheet month is closed"),
    Required(ErrorMessage:="Closed required")>
  Public Property ClosedInd() As Boolean
      Get
        Return GetProperty(ClosedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ClosedIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Month")
        Else
          Return String.Format("Blank {0}", "Timesheet Month")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetMonth() method.

    End Sub

    Public Shared Function NewTimesheetMonth() As TimesheetMonth

      Return DataPortal.CreateChild(Of TimesheetMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTimesheetMonth(dr As SafeDataReader) As TimesheetMonth

      Dim t As New TimesheetMonth()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TimesheetMonthIDProperty, .GetInt32(0))
          LoadProperty(TimesheetMonthProperty, .GetInt32(1))
          LoadProperty(TimesheetYearProperty, .GetInt32(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(HoursBeforeOvertimeProperty, .GetInt32(5))
          LoadProperty(ClosedIndProperty, .GetBoolean(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTimesheetMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTimesheetMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTimesheetMonthID As SqlParameter = .Parameters.Add("@TimesheetMonthID", SqlDbType.Int)
          paramTimesheetMonthID.Value = GetProperty(TimesheetMonthIDProperty)
          If Me.IsNew Then
            paramTimesheetMonthID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TimesheetMonth", GetProperty(TimesheetMonthProperty))
          .Parameters.AddWithValue("@TimesheetYear", GetProperty(TimesheetYearProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@HoursBeforeOvertime", GetProperty(HoursBeforeOvertimeProperty))
          .Parameters.AddWithValue("@ClosedInd", GetProperty(ClosedIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TimesheetMonthIDProperty, paramTimesheetMonthID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTimesheetMonth"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TimesheetMonthID", GetProperty(TimesheetMonthIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace