﻿' Generated 26 Jan 2015 09:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetCategory
    Inherits SingularReadOnlyBase(Of ROTimesheetCategory)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetCategoryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property TimesheetCategoryID() As Integer
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetCategory, "Timesheet Category", "")
    ''' <summary>
    ''' Gets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:="")>
  Public ReadOnly Property TimesheetCategory() As String
      Get
        Return GetProperty(TimesheetCategoryProperty)
      End Get
    End Property

    Public Shared ShortfallCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShortfallCalcInd, "Shortfall Calc", True)
    ''' <summary>
    ''' Gets the Shortfall Calc value
    ''' </summary>
    <Display(Name:="Shortfall Calc", Description:="")>
  Public ReadOnly Property ShortfallCalcInd() As Boolean
      Get
        Return GetProperty(ShortfallCalcIndProperty)
      End Get
    End Property

    Public Shared OvertimeCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OvertimeCalcInd, "Overtime Calc", True)
    ''' <summary>
    ''' Gets the Overtime Calc value
    ''' </summary>
    <Display(Name:="Overtime Calc", Description:="")>
  Public ReadOnly Property OvertimeCalcInd() As Boolean
      Get
        Return GetProperty(OvertimeCalcIndProperty)
      End Get
    End Property

    Public Shared ActualHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActualHoursInd, "Actual Hours", True)
    ''' <summary>
    ''' Gets the Actual Hours value
    ''' </summary>
    <Display(Name:="Actual Hours", Description:="")>
  Public ReadOnly Property ActualHoursInd() As Boolean
      Get
        Return GetProperty(ActualHoursIndProperty)
      End Get
    End Property

    Public Shared AccumulateHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccumulateHoursInd, "Accumulate Hours", True)
    ''' <summary>
    ''' Gets the Accumulate Hours value
    ''' </summary>
    <Display(Name:="Accumulate Hours", Description:="")>
  Public ReadOnly Property AccumulateHoursInd() As Boolean
      Get
        Return GetProperty(AccumulateHoursIndProperty)
      End Get
    End Property

    Public Shared MinHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MinHours, "Min Hours", 0)
    ''' <summary>
    ''' Gets the Min Hours value
    ''' </summary>
    <Display(Name:="Min Hours", Description:="")>
  Public ReadOnly Property MinHours() As Decimal
      Get
        Return GetProperty(MinHoursProperty)
      End Get
    End Property

    Public Shared MaxHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MaxHours, "Max Hours", 0)
    ''' <summary>
    ''' Gets the Max Hours value
    ''' </summary>
    <Display(Name:="Max Hours", Description:="")>
  Public ReadOnly Property MaxHours() As Decimal
      Get
        Return GetProperty(MaxHoursProperty)
      End Get
    End Property

    Public Shared MinMaxHoursCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MinMaxHoursCalcInd, "Min Max Hours Calc", False)
    ''' <summary>
    ''' Gets the Min Max Hours Calc value
    ''' </summary>
    <Display(Name:="Min Max Hours Calc", Description:="")>
  Public ReadOnly Property MinMaxHoursCalcInd() As Boolean
      Get
        Return GetProperty(MinMaxHoursCalcIndProperty)
      End Get
    End Property

    Public Shared DefaultStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultStartTime, "Default Start Time")
    ''' <summary>
    ''' Gets the Default Start Time value
    ''' </summary>
    <Display(Name:="Default Start Time", Description:="")>
  Public ReadOnly Property DefaultStartTime() As Object
      Get
        Dim value = GetProperty(DefaultStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared DefaultEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultEndTime, "Default End Time")
    ''' <summary>
    ''' Gets the Default End Time value
    ''' </summary>
    <Display(Name:="Default End Time", Description:="")>
  Public ReadOnly Property DefaultEndTime() As Object
      Get
        Dim value = GetProperty(DefaultEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared FromDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FromDate, "From Date")
    ''' <summary>
    ''' Gets the From Date value
    ''' </summary>
    <Display(Name:="From Date", Description:="")>
  Public ReadOnly Property FromDate As DateTime?
      Get
        If Not FieldManager.FieldExists(FromDateProperty) Then
          LoadProperty(FromDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(FromDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetCategoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TimesheetCategory

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTimesheetCategory(dr As SafeDataReader) As ROTimesheetCategory

      Dim r As New ROTimesheetCategory()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TimesheetCategoryIDProperty, .GetInt32(0))
        LoadProperty(TimesheetCategoryProperty, .GetString(1))
        LoadProperty(ShortfallCalcIndProperty, .GetBoolean(2))
        LoadProperty(OvertimeCalcIndProperty, .GetBoolean(3))
        LoadProperty(ActualHoursIndProperty, .GetBoolean(4))
        LoadProperty(AccumulateHoursIndProperty, .GetBoolean(5))
        LoadProperty(MinHoursProperty, .GetDecimal(6))
        LoadProperty(MaxHoursProperty, .GetDecimal(7))
        LoadProperty(MinMaxHoursCalcIndProperty, .GetBoolean(8))
        If .IsDBNull(9) Then
          LoadProperty(DefaultStartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(DefaultStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(9))))
        End If
        If .IsDBNull(10) Then
          LoadProperty(DefaultEndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(DefaultEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(10))))
        End If
        LoadProperty(CreatedByProperty, .GetInt32(11))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
        LoadProperty(ModifiedByProperty, .GetInt32(13))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
        LoadProperty(FromDateProperty, .GetValue(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace