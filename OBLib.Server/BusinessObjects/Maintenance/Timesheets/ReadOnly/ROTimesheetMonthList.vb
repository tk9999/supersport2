﻿' Generated 26 Jan 2015 09:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports Singular.Web

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Timesheets.ReadOnly

  <Serializable()>
  Public Class ROTimesheetMonthList
    Inherits SingularReadOnlyListBase(Of ROTimesheetMonthList, ROTimesheetMonth)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(TimesheetMonthID As Integer) As ROTimesheetMonth

      For Each child As ROTimesheetMonth In Me
        If child.TimesheetMonthID = TimesheetMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByDate(ByVal [Date] As Date) As ROTimesheetMonth

      Dim month As Integer = [Date].Month
      Dim year As Integer = [Date].Year

      Dim query As List(Of ROTimesheetMonth) = Me.Where(Function(tm) [Date].Date >= tm.StartDate And [Date].Date <= tm.EndDate).ToList

      If query.Count = 1 Then
        Return query(0)
      Else
        Return Nothing
      End If

    End Function

    Function GetCurrentTimesheetMonth() As ROTimesheetMonth

      Return Me.Where(Function(a) Not a.ClosedInd).OrderBy(Function(d) d.StartDate).FirstOrDefault

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Months"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ForDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ForDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="For Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property ForDate As DateTime?
        Get
          Return ReadProperty(ForDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(ForDateProperty, Value)
        End Set
      End Property

      Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TimesheetMonthID, Nothing)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Timesheet Month", Description:="")>
      Public Property TimesheetMonthID As Integer?
        Get
          Return ReadProperty(TimesheetMonthIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(TimesheetMonthIDProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Shared TimesheetMonthProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.TimesheetMonth, "")
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Month", Description:=""),
        PrimarySearchField>
      Public Property TimesheetMonth As String
        Get
          Return ReadProperty(TimesheetMonthProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TimesheetMonthProperty, Value)
        End Set
      End Property

      Public Sub New(All As Boolean)
        Me.PageNo = 1
        Me.PageSize = 1000
        Me.SortColumn = "StartDate"
        Me.SortAsc = False
      End Sub

      Public Sub New(TimesheetMonthID As Integer?)
        Me.PageNo = 1
        Me.PageSize = 15
        Me.SortColumn = "StartDate"
        Me.SortAsc = False
        Me.TimesheetMonthID = TimesheetMonthID
      End Sub

      Public Sub New(ForDate As DateTime?)
        Me.PageNo = 1
        Me.PageSize = 15
        Me.SortColumn = "StartDate"
        Me.SortAsc = False
        Me.ForDate = ForDate
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTimesheetMonthList() As ROTimesheetMonthList

      Return New ROTimesheetMonthList()

    End Function

    Public Shared Sub BeginGetROTimesheetMonthList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTimesheetMonthList)))

      Dim dp As New DataPortal(Of ROTimesheetMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTimesheetMonthList(CallBack As EventHandler(Of DataPortalResult(Of ROTimesheetMonthList)))

      Dim dp As New DataPortal(Of ROTimesheetMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTimesheetMonthList() As ROTimesheetMonthList

      Return DataPortal.Fetch(Of ROTimesheetMonthList)(New Criteria())

    End Function

    Public Shared Function GetROTimesheetMonthList(ForDate As DateTime?) As ROTimesheetMonthList

      Return DataPortal.Fetch(Of ROTimesheetMonthList)(New Criteria(ForDate))

    End Function

    Public Shared Function GetROTimesheetMonthList(TimesheetMonthID As Integer?) As ROTimesheetMonthList

      Return DataPortal.Fetch(Of ROTimesheetMonthList)(New Criteria(TimesheetMonthID))

    End Function

    Public Shared Function GetROTimesheetMonthListAll() As ROTimesheetMonthList

      Return DataPortal.Fetch(Of ROTimesheetMonthList)(New Criteria(True))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTimesheetMonth.GetROTimesheetMonth(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTimesheetMonthList"
            cm.Parameters.AddWithValue("@ForDate", NothingDBNull(crit.ForDate))
            cm.Parameters.AddWithValue("@TimesheetMonthID", NothingDBNull(crit.TimesheetMonthID))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@TimesheetMonth", Strings.MakeEmptyDBNull(crit.TimesheetMonth))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace