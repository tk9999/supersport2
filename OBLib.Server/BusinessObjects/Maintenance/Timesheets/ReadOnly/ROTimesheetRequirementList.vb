﻿' Generated 21 Jun 2016 06:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetRequirementList
    Inherits OBReadOnlyListBase(Of ROTimesheetRequirementList, ROTimesheetRequirement)

#Region " Business Methods "

    Public Function GetItem(TimesheetRequirementID As Integer) As ROTimesheetRequirement

      For Each child As ROTimesheetRequirement In Me
        If child.TimesheetRequirementID = TimesheetRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Requirements"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField()>
      Public Property TimesheetRequirement As String = ""
      Public Property HumanResourceID As Integer? = Nothing
      Public Property TimesheetRequirementID As Integer? = Nothing
      Public Property IsClosed As Boolean? = Nothing

      Public Sub New(TimesheetRequirementID As Integer?)
        Me.TimesheetRequirementID = TimesheetRequirementID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROTimesheetRequirementList() As ROTimesheetRequirementList

      Return New ROTimesheetRequirementList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTimesheetRequirementList() As ROTimesheetRequirementList

      Return DataPortal.Fetch(Of ROTimesheetRequirementList)(New Criteria())

    End Function

    Public Shared Function GetROTimesheetRequirementList(TimesheetRequirementID As Integer?) As ROTimesheetRequirementList

      Return DataPortal.Fetch(Of ROTimesheetRequirementList)(New Criteria(TimesheetRequirementID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTimesheetRequirement.GetROTimesheetRequirement(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTimesheetRequirementList"
            cm.Parameters.AddWithValue("@TimesheetRequirementID", NothingDBNull(crit.TimesheetRequirementID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@TimesheetRequirement", Strings.MakeEmptyDBNull(crit.TimesheetRequirement))
            cm.Parameters.AddWithValue("@IsClosed", NothingDBNull(crit.IsClosed))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace