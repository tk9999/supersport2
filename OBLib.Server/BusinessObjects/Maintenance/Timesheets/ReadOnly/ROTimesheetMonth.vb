﻿' Generated 26 Jan 2015 09:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetMonth
    Inherits SingularReadOnlyBase(Of ROTimesheetMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property TimesheetMonthID() As Integer
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
    End Property

    Public Shared TimesheetMonthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonth, "Timesheet Month", 1)
    ''' <summary>
    ''' Gets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="Month of the year for the timesheet")>
  Public ReadOnly Property TimesheetMonth() As Integer
      Get
        Return GetProperty(TimesheetMonthProperty)
      End Get
    End Property

    Public Shared TimesheetYearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetYear, "Timesheet Year", 2012)
    ''' <summary>
    ''' Gets the Timesheet Year value
    ''' </summary>
    <Display(Name:="Timesheet Year", Description:="Year of the timesheet")>
  Public ReadOnly Property TimesheetYear() As Integer
      Get
        Return GetProperty(TimesheetYearProperty)
      End Get
    End Property

    Public Shared MonthYearProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MonthYear, "Month Year", "")
    ''' <summary>
    ''' Gets the Timesheet Year value
    ''' </summary>
    <Display(Name:="Description", Description:="Month and Year of the timesheet")>
    Public ReadOnly Property MonthYear() As String
      Get
        Return GetProperty(MonthYearProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Start Date for the timesheet month")>
  Public ReadOnly Property StartDate As DateTime?
      Get
        If Not FieldManager.FieldExists(StartDateProperty) Then
          LoadProperty(StartDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="End Date for the timesheet month")>
  Public ReadOnly Property EndDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EndDateProperty) Then
          LoadProperty(EndDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared HoursBeforeOvertimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HoursBeforeOvertime, "Hours Before Overtime", 180)
    ''' <summary>
    ''' Gets the Hours Before Overtime value
    ''' </summary>
    <Display(Name:="Hours Before Overtime", Description:="Hours before overtime for the timesheet month")>
  Public ReadOnly Property HoursBeforeOvertime() As Integer
      Get
        Return GetProperty(HoursBeforeOvertimeProperty)
      End Get
    End Property

    Public Shared ClosedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ClosedInd, "Closed", False)
    ''' <summary>
    ''' Gets the Closed value
    ''' </summary>
    <Display(Name:="Closed", Description:="True if the timesheet month is closed")>
  Public ReadOnly Property ClosedInd() As Boolean
      Get
        Return GetProperty(ClosedIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public ReadOnly Property Description As String
      Get
        Return Me.ToString()
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.StartDate.Value.ToString("dd MMM yyyy") & " - " & Me.EndDate.Value.ToString("dd MMM yyyy")

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTimesheetMonth(dr As SafeDataReader) As ROTimesheetMonth

      Dim r As New ROTimesheetMonth()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TimesheetMonthIDProperty, .GetInt32(0))
        LoadProperty(TimesheetMonthProperty, .GetInt32(1))
        LoadProperty(TimesheetYearProperty, .GetInt32(2))
        LoadProperty(StartDateProperty, .GetValue(3))
        LoadProperty(EndDateProperty, .GetValue(4))
        LoadProperty(HoursBeforeOvertimeProperty, .GetInt32(5))
        LoadProperty(ClosedIndProperty, .GetBoolean(6))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(MonthYearProperty, MonthName(Me.TimesheetMonth).ToString & " " & TimesheetYear.ToString)
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace