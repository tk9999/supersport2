﻿' Generated 21 Jun 2016 06:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetRequirementPagedList
    Inherits OBReadOnlyListBase(Of ROTimesheetRequirementPagedList, ROTimesheetRequirementPaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROTimesheetRequirementPagedList As ROTimesheetRequirementPagedList
    'Public Property ROTimesheetRequirementPagedListCriteria As ROTimesheetRequirementPagedList.Criteria
    'Public Property ROTimesheetRequirementPagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROTimesheetRequirementPagedList = New ROTimesheetRequirementPagedList
    'ROTimesheetRequirementPagedListCriteria = New ROTimesheetRequirementPagedList.Criteria
    'ROTimesheetRequirementPagedListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROTimesheetRequirementPagedList, Function(d) d.ROTimesheetRequirementPagedListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(TimesheetRequirementID As Integer) As ROTimesheetRequirementPaged

      For Each child As ROTimesheetRequirementPaged In Me
        If child.TimesheetRequirementID = TimesheetRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Requirements"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TimesheetRequirement As String
      Public Property SystemID As Integer?

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROTimesheetRequirementPagedList() As ROTimesheetRequirementPagedList

      Return New ROTimesheetRequirementPagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTimesheetRequirementPagedList() As ROTimesheetRequirementPagedList

      Return DataPortal.Fetch(Of ROTimesheetRequirementPagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTimesheetRequirementPaged.GetROTimesheetRequirementPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTimesheetRequirementListPaged"
            cm.Parameters.AddWithValue("@TimesheetRequirement", Strings.MakeEmptyDBNull(crit.TimesheetRequirement))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace