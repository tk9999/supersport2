﻿' Generated 21 Jun 2016 06:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Timesheets.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetRequirement
    Inherits OBReadOnlyBase(Of ROTimesheetRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTimesheetRequirementBO.ROTimesheetRequirementBOToString(self)")

    Public Shared TimesheetRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property TimesheetRequirementID() As Integer
      Get
        Return GetProperty(TimesheetRequirementIDProperty)
      End Get
    End Property

    Public Shared TimesheetRequirementProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetRequirement, "Requirement Name", "")
    ''' <summary>
    ''' Gets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Requirement Name", Description:="")>
    Public ReadOnly Property TimesheetRequirement() As String
      Get
        Return GetProperty(TimesheetRequirementProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    DateField(FormatString:="ddd dd MMM yy")>
    Public ReadOnly Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    DateField(FormatString:="ddd dd MMM yy")>
    Public ReadOnly Property EndDate As Date
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    'Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ' ''' <summary>
    ' ''' Gets the Production Area value
    ' ''' </summary>
    '<Display(Name:="Area", Description:="")>
    'Public ReadOnly Property ProductionAreaID() As Integer?
    '  Get
    '    Return GetProperty(ProductionAreaIDProperty)
    '  End Get
    'End Property

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamNumberID, "Team Number", Nothing)
    ''' <summary>
    ''' Gets the System Team Number value
    ''' </summary>
    <Display(Name:="Team Number", Description:="")>
    Public ReadOnly Property SystemTeamNumberID() As Integer?
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Required Hours", CDec(0))
    ''' <summary>
    ''' Gets the Required Hours value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public ReadOnly Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
    End Property

    Public Shared RequiredShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredShifts, "Required Shifts", 0)
    ''' <summary>
    ''' Gets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:="")>
    Public ReadOnly Property RequiredShifts() As Integer
      Get
        Return GetProperty(RequiredShiftsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Dept", "")
    ''' <summary>
    ''' Gets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    'Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    ' ''' <summary>
    ' ''' Gets the Timesheet Requirement value
    ' ''' </summary>
    '<Display(Name:="Area", Description:="")>
    'Public ReadOnly Property ProductionArea() As String
    '  Get
    '    Return GetProperty(ProductionAreaProperty)
    '  End Get
    'End Property

    Public Shared TeamNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamName, "Team", "")
    ''' <summary>
    ''' Gets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Team", Description:="")>
    Public ReadOnly Property TeamName() As String
      Get
        Return GetProperty(TeamNameProperty)
      End Get
    End Property

    Public Shared IsClosedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsClosed, "Required Shifts", False)
    ''' <summary>
    ''' Gets and sets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:="")>
    Public ReadOnly Property IsClosed() As Boolean
      Get
        Return GetProperty(IsClosedProperty)
      End Get
    End Property

    Public Shared ClosedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ClosedByUserID, "ClosedByUserID", Nothing)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="ClosedByUserID")>
    Public ReadOnly Property ClosedByUserID() As Integer?
      Get
        Return GetProperty(ClosedByUserIDProperty)
      End Get
    End Property

    Public Shared ClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ClosedDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="Closed Time", Description:="")>
    Public ReadOnly Property ClosedDateTime As DateTime?
      Get
        Return GetProperty(ClosedDateTimeProperty)
      End Get
    End Property

    Public Shared ClosedDescriptionShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClosedDescriptionShort, "ClosedDescriptionShort", "")
    ''' <summary>
    ''' Gets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="ClosedDescriptionShort", Description:="")>
    Public ReadOnly Property ClosedDescriptionShort() As String
      Get
        Return GetProperty(ClosedDescriptionShortProperty)
      End Get
    End Property

    Public Shared ClosedDescriptionLongProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClosedDescriptionLong, "ClosedDescriptionLong", "")
    ''' <summary>
    ''' Gets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="ClosedDescriptionLong", Description:="")>
    Public ReadOnly Property ClosedDescriptionLong() As String
      Get
        Return GetProperty(ClosedDescriptionLongProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TimesheetRequirement

    End Function

    Public Sub RunTimesheets()
      If Me.IsClosed Then
        Throw New Exception("Timesheet Requirement is closed")
      Else
        Dim cproc As New Singular.CommandProc("CmdProcs.cmdTimesheetsRecalculateAll",
                                      New String() {"@AtDate", "@TimesheetRequirementID", "@HumanResourceID", "@CurrentUserID"},
                                      New Object() {NothingDBNull(Me.StartDate), Me.TimesheetRequirementID, NothingDBNull(Nothing), OBLib.Security.Settings.CurrentUserID})
        cproc.CommandTimeout = 0
        cproc = cproc.Execute
      End If
    End Sub

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTimesheetRequirement(dr As SafeDataReader) As ROTimesheetRequirement

      Dim r As New ROTimesheetRequirement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TimesheetRequirementIDProperty, .GetInt32(0))
        LoadProperty(TimesheetRequirementProperty, .GetString(1))
        LoadProperty(StartDateProperty, .GetValue(2))
        LoadProperty(EndDateProperty, .GetValue(3))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(SystemTeamNumberIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(RequiredHoursProperty, .GetDecimal(6))
        LoadProperty(RequiredShiftsProperty, .GetInt32(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        LoadProperty(SystemProperty, .GetString(12))
        'LoadProperty(ProductionAreaProperty, .GetString(14))
        LoadProperty(TeamNameProperty, .GetString(13))
        LoadProperty(IsClosedProperty, .GetBoolean(14))
        LoadProperty(ClosedByUserIDProperty, ZeroNothing(.GetInt32(15)))
        LoadProperty(ClosedDateTimeProperty, .GetValue(16))
        LoadProperty(ClosedDescriptionShortProperty, .GetString(17))
        LoadProperty(ClosedDescriptionLongProperty, .GetString(18))
      End With

    End Sub

#End Region

  End Class

End Namespace