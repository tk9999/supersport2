﻿' Generated 24 Aug 2016 12:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets

  <Serializable()> _
  Public Class TimesheetRequirementSettingList
    Inherits OBBusinessListBase(Of TimesheetRequirementSettingList, TimesheetRequirementSetting)

#Region " Business Methods "

    Public Function GetItem(TimesheetRequirementSettingID As Integer) As TimesheetRequirementSetting

      For Each child As TimesheetRequirementSetting In Me
        If child.TimesheetRequirementSettingID = TimesheetRequirementSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Requirement Settings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewTimesheetRequirementSettingList() As TimesheetRequirementSettingList

      Return New TimesheetRequirementSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetTimesheetRequirementSettingList() As TimesheetRequirementSettingList

      Return DataPortal.Fetch(Of TimesheetRequirementSettingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TimesheetRequirementSetting.GetTimesheetRequirementSetting(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTimesheetRequirementSettingList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace