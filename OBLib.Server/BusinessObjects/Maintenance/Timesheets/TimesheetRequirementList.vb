﻿' Generated 21 Jun 2016 06:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets

  <Serializable()> _
  Public Class TimesheetRequirementList
    Inherits OBBusinessListBase(Of TimesheetRequirementList, TimesheetRequirement)

#Region " Business Methods "

    Public Function GetItem(TimesheetRequirementID As Integer) As TimesheetRequirement

      For Each child As TimesheetRequirement In Me
        If child.TimesheetRequirementID = TimesheetRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Requirements"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TimesheetRequirementID As Integer?

      Public Sub New(TimesheetRequirementID As Integer?)
        Me.TimesheetRequirementID = TimesheetRequirementID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewTimesheetRequirementList() As TimesheetRequirementList

      Return New TimesheetRequirementList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetTimesheetRequirementList() As TimesheetRequirementList

      Return DataPortal.Fetch(Of TimesheetRequirementList)(New Criteria())

    End Function

    Public Shared Function GetTimesheetRequirementList(TimesheetRequirementID As Integer?) As TimesheetRequirementList

      Return DataPortal.Fetch(Of TimesheetRequirementList)(New Criteria(TimesheetRequirementID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TimesheetRequirement.GetTimesheetRequirement(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As TimesheetRequirement = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.TimesheetRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.TimesheetRequirementSettingList.RaiseListChangedEvents = False
          parent.TimesheetRequirementSettingList.Add(TimesheetRequirementSetting.GetTimesheetRequirementSetting(sdr))
          parent.TimesheetRequirementSettingList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.TimesheetRequirementID <> sdr.GetInt32(11) Then
            parent = Me.GetItem(sdr.GetInt32(11))
          End If
          parent.TimesheetRequirementHRList.RaiseListChangedEvents = False
          parent.TimesheetRequirementHRList.Add(TimesheetRequirementHR.GetTimesheetRequirementHR(sdr))
          parent.TimesheetRequirementHRList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTimesheetRequirementList"
            cm.Parameters.AddWithValue("@TimesheetRequirementID", NothingDBNull(crit.TimesheetRequirementID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace