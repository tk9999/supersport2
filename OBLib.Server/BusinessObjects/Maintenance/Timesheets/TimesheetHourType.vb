﻿' Generated 26 Jan 2015 06:36 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Timesheets

  <Serializable()> _
  Public Class TimesheetHourType
    Inherits SingularBusinessBase(Of TimesheetHourType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetHourTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetHourTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property TimesheetHourTypeID() As Integer
      Get
        Return GetProperty(TimesheetHourTypeIDProperty)
      End Get
    End Property

    Public Shared TimesheetHourTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetHourType, "Timesheet Hour Type", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Hour Type value
    ''' </summary>
    <Display(Name:="Timesheet Hour Type", Description:="description of hour type"),
    StringLength(50, ErrorMessage:="Timesheet Hour Type cannot be more than 50 characters")>
  Public Property TimesheetHourType() As String
      Get
        Return GetProperty(TimesheetHourTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimesheetHourTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Rate, "Rate", CDec(1))
    ''' <summary>
    ''' Gets and sets the Rate value
    ''' </summary>
    <Display(Name:="Rate", Description:=""),
    Required(ErrorMessage:="Rate required")>
  Public Property Rate() As Decimal
      Get
        Return GetProperty(RateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RateProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
  Public Property EffectiveDate As DateTime?
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetHourTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TimesheetHourType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Hour Type")
        Else
          Return String.Format("Blank {0}", "Timesheet Hour Type")
        End If
      Else
        Return Me.TimesheetHourType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetHourType() method.

    End Sub

    Public Shared Function NewTimesheetHourType() As TimesheetHourType

      Return DataPortal.CreateChild(Of TimesheetHourType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTimesheetHourType(dr As SafeDataReader) As TimesheetHourType

      Dim t As New TimesheetHourType()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TimesheetHourTypeIDProperty, .GetInt32(0))
          LoadProperty(TimesheetHourTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(RateProperty, .GetDecimal(6))
          LoadProperty(EffectiveDateProperty, .GetValue(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTimesheetHourType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTimesheetHourType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTimesheetHourTypeID As SqlParameter = .Parameters.Add("@TimesheetHourTypeID", SqlDbType.Int)
          paramTimesheetHourTypeID.Value = GetProperty(TimesheetHourTypeIDProperty)
          If Me.IsNew Then
            paramTimesheetHourTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TimesheetHourType", GetProperty(TimesheetHourTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@Rate", GetProperty(RateProperty))
          .Parameters.AddWithValue("@EffectiveDate", (New SmartDate(GetProperty(EffectiveDateProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TimesheetHourTypeIDProperty, paramTimesheetHourTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTimesheetHourType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TimesheetHourTypeID", GetProperty(TimesheetHourTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace