﻿' Generated 21 Jun 2016 06:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.TeamManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets

  <Serializable()> _
  Public Class TimesheetRequirement
    Inherits OBBusinessBase(Of TimesheetRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return TimesheetRequirementBO.TimesheetRequirementBOToString(self)")

    Public Shared TimesheetRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property TimesheetRequirementID() As Integer
      Get
        Return GetProperty(TimesheetRequirementIDProperty)
      End Get
    End Property

    Public Shared TimesheetRequirementProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetRequirement, "Requirement Name", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Requirement Name", Description:=""),
    StringLength(250, ErrorMessage:="Requirement Name cannot be more than 250 characters"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Requirement name is required")>
    Public Property TimesheetRequirement() As String
      Get
        Return GetProperty(TimesheetRequirementProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimesheetRequirementProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required"),
    SetExpression("TimesheetRequirementBO.StartDateSet(self)")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required"),
    SetExpression("TimesheetRequirementBO.EndDateSet(self)")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    Required(ErrorMessage:="Sub-Dept is required"),
    SetExpression("TimesheetRequirementBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    'Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Production Area value
    ' ''' </summary>
    '<Display(Name:="Area", Description:=""),
    'SetExpression("TimesheetRequirementBO.ProductionAreaIDSet(self)")>
    'Public Property ProductionAreaID() As Integer?
    '  Get
    '    Return GetProperty(ProductionAreaIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ProductionAreaIDProperty, Value)
    '  End Set
    'End Property

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamNumberID, "Team Number", Nothing)
    ''' <summary>
    ''' Gets and sets the System Team Number value
    ''' </summary>
    <Display(Name:="Team Number", Description:=""),
    DropDownWeb(GetType(ROSystemTeamNumberList), DisplayMember:="SystemTeamNumber", ValueMember:="SystemTeamNumberID",
                DropDownColumns:={"SystemTeamNumberName", "SystemTeamNumber"},
                ThisFilterMember:="SystemID"),
    SetExpression("TimesheetRequirementBO.SystemTeamNumberIDSet(self)")>
    Public Property SystemTeamNumberID() As Integer?
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamNumberIDProperty, Value)
      End Set
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Required Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the Required Hours value
    ''' </summary>
    <Display(Name:="Required Hours", Description:=""),
    Required(ErrorMessage:="Required Hours required")>
    Public Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursProperty, Value)
      End Set
    End Property

    Public Shared RequiredShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredShifts, "Required Shifts", 0)
    ''' <summary>
    ''' Gets and sets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:=""),
    Required(ErrorMessage:="Required Shifts required")>
    Public Property RequiredShifts() As Integer
      Get
        Return GetProperty(RequiredShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RequiredShiftsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TotalHumanResourcesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalHumanResources, "Total Human Resources", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Total Human Resources"), AlwaysClean>
    Public ReadOnly Property TotalHumanResources() As Integer?
      Get
        Return GetProperty(TotalHumanResourcesProperty)
      End Get
    End Property

    Public Shared TotalHumanResourceTimesheetsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalHumanResourceTimesheets, "Total Human Resources", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Total Human Resources"), AlwaysClean>
    Public ReadOnly Property TotalHumanResourceTimesheets() As Integer?
      Get
        Return GetProperty(TotalHumanResourceTimesheetsProperty)
      End Get
    End Property

    Public Shared ConflictingHumanResourceTimesheetsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ConflictingHumanResourceTimesheets, "Total Human Resources", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Total Human Resources"), AlwaysClean>
    Public ReadOnly Property ConflictingHumanResourceTimesheets() As Integer?
      Get
        Return GetProperty(ConflictingHumanResourceTimesheetsProperty)
      End Get
    End Property

    Public Shared IsClosedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsClosed, "Required Shifts", False)
    ''' <summary>
    ''' Gets and sets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:="")>
    Public Property IsClosed() As Boolean
      Get
        Return GetProperty(IsClosedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsClosedProperty, Value)
      End Set
    End Property

    Public Shared ClosedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ClosedByUserID, "ClosedByUserID", Nothing)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="ClosedByUserID")>
    Public Property ClosedByUserID() As Integer?
      Get
        Return GetProperty(ClosedByUserIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ClosedByUserIDProperty, value)
      End Set
    End Property

    Public Shared ClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ClosedDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="Closed Time", Description:="")>
    Public Property ClosedDateTime As DateTime?
      Get
        Return GetProperty(ClosedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ClosedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberMonthGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamNumberMonthGroupID, "SystemTeamNumberMonthGroupID", Nothing)
    ''' <summary>
    ''' Gets and sets the System Team Number value
    ''' </summary>
    <Display(Name:="SystemTeamNumberMonthGroupID", Description:="")>
    Public Property SystemTeamNumberMonthGroupID() As Integer?
      Get
        Return GetProperty(SystemTeamNumberMonthGroupIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamNumberMonthGroupIDProperty, Value)
      End Set
    End Property

    Public Shared SystemYearMonthGroupMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemYearMonthGroupMonthID, "SystemYearMonthGroupMonthID", Nothing)
    ''' <summary>
    ''' Gets and sets the System Team Number value
    ''' </summary>
    <Display(Name:="SystemYearMonthGroupMonthID", Description:="")>
    Public Property SystemYearMonthGroupMonthID() As Integer?
      Get
        Return GetProperty(SystemYearMonthGroupMonthIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemYearMonthGroupMonthIDProperty, Value)
      End Set
    End Property

    Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupName, "Group Requirement", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Group Requirement", Description:="")>
    Public Property GroupName() As String
      Get
        Return GetProperty(GroupNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GroupNameProperty, Value)
      End Set
    End Property

    Public Shared IsTeamBasedRequirmentProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTeamBasedRequirment, "Is this requirement for a Team?", False)
    ''' <summary>
    ''' Gets and sets the Required Hours value
    ''' </summary>
    <Display(Name:="Is this requirement for a Team?", Description:="")>
    Public Property IsTeamBasedRequirment() As Boolean
      Get
        Return GetProperty(IsTeamBasedRequirmentProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTeamBasedRequirmentProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TimesheetRequirement.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Requirement")
        Else
          Return String.Format("Blank {0}", "Timesheet Requirement")
        End If
      Else
        Return Me.TimesheetRequirement
      End If

    End Function

#End Region

#Region " Child Lists "

    Public Shared TimesheetRequirementSettingListProperty As PropertyInfo(Of TimesheetRequirementSettingList) = RegisterProperty(Of TimesheetRequirementSettingList)(Function(c) c.TimesheetRequirementSettingList, "Timesheet Requirement Settings List")
    Public ReadOnly Property TimesheetRequirementSettingList() As TimesheetRequirementSettingList
      Get
        If GetProperty(TimesheetRequirementSettingListProperty) Is Nothing Then
          LoadProperty(TimesheetRequirementSettingListProperty, Timesheets.TimesheetRequirementSettingList.NewTimesheetRequirementSettingList())
        End If
        Return GetProperty(TimesheetRequirementSettingListProperty)
      End Get
    End Property

    Public Shared TimesheetRequirementHRListProperty As PropertyInfo(Of TimesheetRequirementHRList) = RegisterProperty(Of TimesheetRequirementHRList)(Function(c) c.TimesheetRequirementHRList, "Timesheet Requirement HR List")
    Public ReadOnly Property TimesheetRequirementHRList() As TimesheetRequirementHRList
      Get
        If GetProperty(TimesheetRequirementHRListProperty) Is Nothing Then
          LoadProperty(TimesheetRequirementHRListProperty, Timesheets.TimesheetRequirementHRList.NewTimesheetRequirementHRList())
        End If
        Return GetProperty(TimesheetRequirementHRListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ConflictingHumanResourceTimesheetsProperty)
        .ServerRuleFunction = AddressOf ConflictingHumanResourceTimesheetsValid
        .JavascriptRuleFunctionName = "TimesheetRequirementBO.conflictingHumanResourceTimesheetsValid"
        .AddTriggerProperties({SystemIDProperty, StartDateProperty, EndDateProperty, SystemTeamNumberIDProperty})
        .AffectedProperties.AddRange({SystemIDProperty, StartDateProperty, EndDateProperty, SystemTeamNumberIDProperty})
        .Severity = Singular.Rules.RuleSeverity.Warning
      End With

      With AddWebRule(TotalHumanResourcesProperty)
        .ServerRuleFunction = AddressOf TimesheetCountsValid
        .JavascriptRuleFunctionName = "TimesheetRequirementBO.timesheetCountsValid"
        .AddTriggerProperties({TotalHumanResourceTimesheetsProperty})
        .AffectedProperties.AddRange({TotalHumanResourceTimesheetsProperty})
        .Severity = Singular.Rules.RuleSeverity.Warning
      End With

    End Sub

    Public Shared Function ConflictingHumanResourceTimesheetsValid(TimesheetRequirement As TimesheetRequirement) As String
      If TimesheetRequirement.ConflictingHumanResourceTimesheets > 0 Then
        Return "There are timesheets which conflict with this setting"
      End If
      Return ""
    End Function

    Public Shared Function TimesheetCountsValid(TimesheetRequirement As TimesheetRequirement) As String
      If TimesheetRequirement.TotalHumanResourceTimesheets < TimesheetRequirement.TotalHumanResources Then
        Return "Some qualifying Human Resources do not have this requirement"
      Else
        Return "Some Human Resources should not have this requirement"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetRequirement() method.

    End Sub

    Public Shared Function NewTimesheetRequirement() As TimesheetRequirement

      Return DataPortal.CreateChild(Of TimesheetRequirement)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetTimesheetRequirement(dr As SafeDataReader) As TimesheetRequirement

      Dim t As New TimesheetRequirement()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TimesheetRequirementIDProperty, .GetInt32(0))
          LoadProperty(TimesheetRequirementProperty, .GetString(1))
          LoadProperty(StartDateProperty, .GetValue(2))
          LoadProperty(EndDateProperty, .GetValue(3))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(SystemTeamNumberIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(RequiredHoursProperty, .GetDecimal(6))
          LoadProperty(RequiredShiftsProperty, .GetInt32(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(IsClosedProperty, .GetBoolean(12))
          LoadProperty(ClosedByUserIDProperty, ZeroNothing(.GetInt32(13)))
          LoadProperty(ClosedDateTimeProperty, .GetValue(14))
          LoadProperty(SystemTeamNumberMonthGroupIDProperty, ZeroNothing(.GetInt32(15)))
          LoadProperty(SystemYearMonthGroupMonthIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(GroupNameProperty, .GetString(17))
          LoadProperty(IsTeamBasedRequirmentProperty, .GetBoolean(18))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, TimesheetRequirementIDProperty)

      cm.Parameters.AddWithValue("@TimesheetRequirement", GetProperty(TimesheetRequirementProperty))
      cm.Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
      cm.Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
      cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
      'cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@SystemTeamNumberID", Singular.Misc.NothingDBNull(GetProperty(SystemTeamNumberIDProperty)))
      cm.Parameters.AddWithValue("@RequiredHours", GetProperty(RequiredHoursProperty))
      cm.Parameters.AddWithValue("@RequiredShifts", GetProperty(RequiredShiftsProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@IsClosed", GetProperty(IsClosedProperty))
      cm.Parameters.AddWithValue("@ClosedByUserID", Singular.Misc.NothingDBNull(GetProperty(ClosedByUserIDProperty)))
      cm.Parameters.AddWithValue("@ClosedDateTime", (New SmartDate(GetProperty(ClosedDateTimeProperty))).DBValue)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(TimesheetRequirementIDProperty, cm.Parameters("@TimesheetRequirementID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
      UpdateChild(GetProperty(TimesheetRequirementSettingListProperty))
      'UpdateChild(GetProperty(TimesheetRequirementHRListProperty))
      'TimesheetRequirementHRList.BulkUpdate(BulkUpdateMethod.AllRecords, , Sub(proc As Singular.CommandProc)
      '                                                                       proc.CommandText = "UpdProcsWeb.updTimesheetRequirementHRBulk"
      '                                                                     End Sub)
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@TimesheetRequirementID", GetProperty(TimesheetRequirementIDProperty))
    End Sub

#End Region

    Private Function CreateSettingsTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("TimesheetRequirementSettingID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TimesheetRequirementID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ContractTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DisciplineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As TimesheetRequirementSetting In Me.TimesheetRequirementSettingList
        Dim row As DataRow = RBTable.NewRow
        row("TimesheetRequirementSettingID") = NothingDBNull(rb.TimesheetRequirementSettingID)
        row("TimesheetRequirementID") = NothingDBNull(rb.TimesheetRequirementID)
        row("ProductionAreaID") = NothingDBNull(rb.ProductionAreaID)
        row("ContractTypeID") = NothingDBNull(rb.ContractTypeID)
        row("DisciplineID") = NothingDBNull(rb.DisciplineID)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Public Sub RefreshStats()

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdTimesheetsRefreshRequirementStats",
                                         {"@TimesheetRequirementID",
                                          "@StartDate",
                                          "@EndDate",
                                          "@SystemID",
                                          "@SystemTeamNumberID",
                                          "@ModifiedBy"},
                                         {ZeroDBNull(TimesheetRequirementID),
                                          NothingDBNull(StartDate),
                                          NothingDBNull(EndDate),
                                          NothingDBNull(SystemID),
                                          NothingDBNull(SystemTeamNumberID),
                                          OBLib.Security.Settings.CurrentUserID})

      Dim settingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      settingsParam.Name = "@TimesheetRequirementSettings"
      settingsParam.SqlType = SqlDbType.Structured
      settingsParam.Value = CreateSettingsTableParameter()
      cmd.Parameters.Add(settingsParam)

      cmd.FetchType = CommandProc.FetchTypes.DataRow
      cmd = cmd.Execute

      'Header Results
      Dim resultsRow As DataRow = cmd.DataRow

      SetProperty(TotalHumanResourcesProperty, resultsRow(0))
      SetProperty(TotalHumanResourceTimesheetsProperty, resultsRow(1))
      SetProperty(ConflictingHumanResourceTimesheetsProperty, resultsRow(2))

      Dim timesheetRequirementHRList As TimesheetRequirementHRList = OBLib.Timesheets.TimesheetRequirementHRList.GetTimesheetRequirementHRList(Me)
      LoadProperty(TimesheetRequirementHRListProperty, timesheetRequirementHRList)

    End Sub

    Sub ApplyRequirement()

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdTimesheetsApplyRequirement",
                                         {"@TimesheetRequirementID",
                                          "@ModifiedBy"},
                                         {ZeroDBNull(TimesheetRequirementID),
                                          OBLib.Security.Settings.CurrentUserID})
      cmd.FetchType = CommandProc.FetchTypes.DataRow
      cmd = cmd.Execute

    End Sub

  End Class

End Namespace