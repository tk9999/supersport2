﻿' Generated 29 Nov 2016 13:42 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Timesheets

  <Serializable()> _
  Public Class TimesheetRequirementHR
    Inherits OBBusinessBase(Of TimesheetRequirementHR)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return TimesheetRequirementHRBO.TimesheetRequirementHRBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets and sets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRNameProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContractTypeProperty, Value)
      End Set
    End Property

    Public Shared IsRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsRequired, "Is Required", False)
    ''' <summary>
    ''' Gets and sets the Is Required value
    ''' </summary>
    <Display(Name:="Is Required", Description:="")>
    Public Property IsRequired() As Boolean
      Get
        Return GetProperty(IsRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsRequiredProperty, Value)
      End Set
    End Property

    Public Shared HasTimesheetRequirementProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasTimesheetRequirement, "Has Timesheet Requirement", False)
    ''' <summary>
    ''' Gets and sets the Has Timesheet Requirement value
    ''' </summary>
    <Display(Name:="Has Timesheet Requirement", Description:="")>
    Public Property HasTimesheetRequirement() As Boolean
      Get
        Return GetProperty(HasTimesheetRequirementProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasTimesheetRequirementProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceTimesheetID, "Human Resource Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Timesheet value
    ''' </summary>
    <Display(Name:="Human Resource Timesheet", Description:=""),
    Key>
    Public Property HumanResourceTimesheetID() As Integer?
      Get
        Return GetProperty(HumanResourceTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared IsClosedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsClosed, "Is Closed", False)
    ''' <summary>
    ''' Gets and sets the Is Closed value
    ''' </summary>
    <Display(Name:="Is Closed", Description:="")>
    Public Property IsClosed() As Boolean
      Get
        Return GetProperty(IsClosedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsClosedProperty, Value)
      End Set
    End Property

    Public Shared ClosedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ClosedBy, "Closed By", "")
    ''' <summary>
    ''' Gets and sets the Closed By value
    ''' </summary>
    <Display(Name:="Closed By", Description:="")>
    Public Property ClosedBy() As String
      Get
        Return GetProperty(ClosedByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClosedByProperty, Value)
      End Set
    End Property

    Public Shared IsClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.IsClosedDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Is Closed Date Time value
    ''' </summary>
    <Display(Name:="Is Closed Date Time", Description:="")>
    Public Property IsClosedDateTime As DateTime?
      Get
        Return GetProperty(IsClosedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(IsClosedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared IsClosedDateTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsClosedDateTimeString, "Closed Time", "")
    ''' <summary>
    ''' Gets and sets the Is Closed Date Time String value
    ''' </summary>
    <Display(Name:="Closed Time")>
    Public Property IsClosedDateTimeString() As String
      Get
        Return GetProperty(IsClosedDateTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsClosedDateTimeStringProperty, Value)
      End Set
    End Property

    Public Shared TimesheetRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetRequirementID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property TimesheetRequirementID() As Integer?
      Get
        Return GetProperty(TimesheetRequirementIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TimesheetRequirementIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HRName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Requirement HR")
        Else
          Return String.Format("Blank {0}", "Timesheet Requirement HR")
        End If
      Else
        Return Me.HRName
      End If

    End Function

    Public Function GetParent() As TimesheetRequirement
      If Me.Parent IsNot Nothing Then
        Return CType(CType(Me.Parent, TimesheetRequirementHRList).Parent, TimesheetRequirement)
      End If
      Return Nothing
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetRequirementHR() method.

    End Sub

    Public Shared Function NewTimesheetRequirementHR() As TimesheetRequirementHR

      Return DataPortal.CreateChild(Of TimesheetRequirementHR)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetTimesheetRequirementHR(dr As SafeDataReader) As TimesheetRequirementHR

      Dim t As New TimesheetRequirementHR()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(HRNameProperty, .GetString(1))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ContractTypeProperty, .GetString(3))
          LoadProperty(IsRequiredProperty, .GetBoolean(4))
          LoadProperty(HasTimesheetRequirementProperty, .GetBoolean(5))
          LoadProperty(HumanResourceTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(IsClosedProperty, .GetBoolean(7))
          LoadProperty(ClosedByProperty, .GetString(8))
          LoadProperty(IsClosedDateTimeProperty, .GetValue(9))
          LoadProperty(IsClosedDateTimeStringProperty, .GetString(10))
          LoadProperty(TimesheetRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceIDProperty)

      cm.Parameters.AddWithValue("@HRName", GetProperty(HRNameProperty))
      cm.Parameters.AddWithValue("@ContractTypeID", GetProperty(ContractTypeIDProperty))
      cm.Parameters.AddWithValue("@ContractType", GetProperty(ContractTypeProperty))
      cm.Parameters.AddWithValue("@IsRequired", GetProperty(IsRequiredProperty))
      cm.Parameters.AddWithValue("@HasTimesheetRequirement", GetProperty(HasTimesheetRequirementProperty))
      cm.Parameters.AddWithValue("@HumanResourceTimesheetID", NothingDBNull(GetProperty(HumanResourceTimesheetIDProperty)))
      cm.Parameters.AddWithValue("@IsClosed", GetProperty(IsClosedProperty))
      cm.Parameters.AddWithValue("@ClosedBy", GetProperty(ClosedByProperty))
      cm.Parameters.AddWithValue("@IsClosedDateTime", New SmartDate(IsClosedDateTime).DBValue)
      cm.Parameters.AddWithValue("@IsClosedDateTimeString", GetProperty(IsClosedDateTimeStringProperty))
      cm.Parameters.AddWithValue("@TimesheetRequirementID", Me.GetParent.TimesheetRequirementID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceIDProperty, cm.Parameters("@HumanResourceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace