﻿' Generated 26 Jan 2015 06:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.HR

  <Serializable()> _
  Public Class HRDocumentType
    Inherits SingularBusinessBase(Of HRDocumentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HRDocumentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRDocumentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property HRDocumentTypeID() As Integer
      Get
        Return GetProperty(HRDocumentTypeIDProperty)
      End Get
    End Property

    Public Shared HRDocumentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRDocumentType, "HR Document Type", "")
    ''' <summary>
    ''' Gets and sets the HR Document Type value
    ''' </summary>
    <Display(Name:="HR Document Type", Description:="Name of the HR document type"),
    StringLength(50, ErrorMessage:="HR Document Type cannot be more than 50 characters")>
  Public Property HRDocumentType() As String
      Get
        Return GetProperty(HRDocumentTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRDocumentTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HRDocumentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HRDocumentType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "HR Document Type")
        Else
          Return String.Format("Blank {0}", "HR Document Type")
        End If
      Else
        Return Me.HRDocumentType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHRDocumentType() method.

    End Sub

    Public Shared Function NewHRDocumentType() As HRDocumentType

      Return DataPortal.CreateChild(Of HRDocumentType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHRDocumentType(dr As SafeDataReader) As HRDocumentType

      Dim h As New HRDocumentType()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HRDocumentTypeIDProperty, .GetInt32(0))
          LoadProperty(HRDocumentTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHRDocumentType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHRDocumentType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHRDocumentTypeID As SqlParameter = .Parameters.Add("@HRDocumentTypeID", SqlDbType.Int)
          paramHRDocumentTypeID.Value = GetProperty(HRDocumentTypeIDProperty)
          If Me.IsNew Then
            paramHRDocumentTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HRDocumentType", GetProperty(HRDocumentTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HRDocumentTypeIDProperty, paramHRDocumentTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delHRDocumentType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HRDocumentTypeID", GetProperty(HRDocumentTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace