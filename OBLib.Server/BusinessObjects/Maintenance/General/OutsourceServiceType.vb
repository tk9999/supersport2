﻿' Generated 20 Jul 2014 17:56 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class OutsourceServiceType
    Inherits SingularBusinessBase(Of OutsourceServiceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OutsourceServiceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property OutsourceServiceTypeID() As Integer
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared OutsourceServiceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OutsourceServiceType, "Outsource Service Type", "")
    ''' <summary>
    ''' Gets and sets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Outsource Service Type", Description:="Description for the Outsource Service Type"),
    Required(ErrorMessage:="Outsource Service Type required"),
    StringLength(50, ErrorMessage:="Outsource Service Type cannot be more than 50 characters")>
    Public Property OutsourceServiceType() As String
      Get
        Return GetProperty(OutsourceServiceTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OutsourceServiceTypeProperty, Value)
      End Set
    End Property

    Public Shared ServiceTimesRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ServiceTimesRequiredInd, "Service Times Required", False)
    ''' <summary>
    ''' Gets and sets the Service Times Required value
    ''' </summary>
    <Display(Name:="Service Times Required", Description:="Tick indicates that start dates and end dates must be captured"),
    Required(ErrorMessage:="Service Times Required required")>
    Public Property ServiceTimesRequiredInd() As Boolean
      Get
        Return GetProperty(ServiceTimesRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ServiceTimesRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Tick indicates that this OutsourceServiceType is core to the system"),
    Required(ErrorMessage:="System required")>
    Public Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemIndProperty, Value)
      End Set
    End Property

    Public Shared AlwaysRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AlwaysRequiredInd, "Always Required", False)
    ''' <summary>
    ''' Gets and sets the Always Required value
    ''' </summary>
    <Display(Name:="Always Required", Description:="Tick indicates that the sevce type must always be allocated to the production"),
    Required(ErrorMessage:="Always Required required")>
    Public Property AlwaysRequiredInd() As Boolean
      Get
        Return GetProperty(AlwaysRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AlwaysRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OutsourceServiceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.OutsourceServiceType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Outsource Service Type")
        Else
          Return String.Format("Blank {0}", "Outsource Service Type")
        End If
      Else
        Return Me.OutsourceServiceType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewOutsourceServiceType() method.

    End Sub

    Public Shared Function NewOutsourceServiceType() As OutsourceServiceType

      Return DataPortal.CreateChild(Of OutsourceServiceType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetOutsourceServiceType(dr As SafeDataReader) As OutsourceServiceType

      Dim o As New OutsourceServiceType()
      o.Fetch(dr)
      Return o

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(OutsourceServiceTypeIDProperty, .GetInt32(0))
          LoadProperty(OutsourceServiceTypeProperty, .GetString(1))
          LoadProperty(ServiceTimesRequiredIndProperty, .GetBoolean(2))
          LoadProperty(SystemIndProperty, .GetBoolean(3))
          LoadProperty(AlwaysRequiredIndProperty, .GetBoolean(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insOutsourceServiceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updOutsourceServiceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramOutsourceServiceTypeID As SqlParameter = .Parameters.Add("@OutsourceServiceTypeID", SqlDbType.Int)
          paramOutsourceServiceTypeID.Value = GetProperty(OutsourceServiceTypeIDProperty)
          If Me.IsNew Then
            paramOutsourceServiceTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@OutsourceServiceType", GetProperty(OutsourceServiceTypeProperty))
          .Parameters.AddWithValue("@ServiceTimesRequiredInd", GetProperty(ServiceTimesRequiredIndProperty))
          .Parameters.AddWithValue("@SystemInd", GetProperty(SystemIndProperty))
          .Parameters.AddWithValue("@AlwaysRequiredInd", GetProperty(AlwaysRequiredIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(OutsourceServiceTypeIDProperty, paramOutsourceServiceTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delOutsourceServiceType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@OutsourceServiceTypeID", GetProperty(OutsourceServiceTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace