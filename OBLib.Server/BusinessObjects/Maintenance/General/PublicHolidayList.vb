﻿' Generated 20 Sep 2014 21:14 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class PublicHolidayList
    Inherits OBBusinessListBase(Of PublicHolidayList, PublicHoliday)

#Region " Business Methods "

    Public Function GetItem(PublicHolidayID As Integer) As PublicHoliday

      For Each child As PublicHoliday In Me
        If child.PublicHolidayID = PublicHolidayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function FilterHolidayList(Startdate As Date, Enddate As Date) As PublicHolidayList

      Dim List = New PublicHolidayList

      For Each child As PublicHoliday In Me
        If child.HolidayDate >= Startdate And child.HolidayDate <= Enddate Then
          List.Add(child)
        End If
      Next
      Return List

    End Function

    Public Function IsHoliday(Holidaydate As Date) As Boolean

      Dim List = New PublicHolidayList

      For Each child As PublicHoliday In Me
        If Singular.Misc.CompareSafe(child.HolidayDate, Holidaydate.Date) Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Overrides Function ToString() As String

      Return "Public Holidays"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewPublicHolidayList() As PublicHolidayList

      Return New PublicHolidayList()

    End Function

    Public Shared Sub BeginGetPublicHolidayList(CallBack As EventHandler(Of DataPortalResult(Of PublicHolidayList)))

      Dim dp As New DataPortal(Of PublicHolidayList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetPublicHolidayList() As PublicHolidayList

      Return DataPortal.Fetch(Of PublicHolidayList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(PublicHoliday.GetPublicHoliday(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getPublicHolidayList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As PublicHoliday In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As PublicHoliday In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace