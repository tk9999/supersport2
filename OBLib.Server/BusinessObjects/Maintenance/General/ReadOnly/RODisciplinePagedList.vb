﻿' Generated 30 Jun 2014 14:07 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODisciplinePagedList
    Inherits SingularReadOnlyListBase(Of RODisciplinePagedList, RODisciplinePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(DisciplineID As Integer) As RODisciplinePaged

      For Each child As RODisciplinePaged In Me
        If child.DisciplineID = DisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Disciplines"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Overridable Property SystemID As Integer? = Nothing

      Public Sub New(SystemID As Integer?)
        Me.SystemID = SystemID
      End Sub

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline ID", Nothing)

      <Display(Name:="Discipline ID")>
      Public Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")

      <Display(Name:="Discipline")>
      Public Property Discipline() As String
        Get
          Return ReadProperty(DisciplineProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(DisciplineProperty, Value)
        End Set
      End Property

      Public Sub New(Discipline As String)
        Me.Discipline = Discipline
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRODisciplineList() As RODisciplinePagedList

      Return New RODisciplinePagedList()

    End Function

    Public Shared Sub BeginGetRODisciplineList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RODisciplinePagedList)))

      Dim dp As New DataPortal(Of RODisciplinePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRODisciplineList(CallBack As EventHandler(Of DataPortalResult(Of RODisciplinePagedList)))

      Dim dp As New DataPortal(Of RODisciplinePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRODisciplineList() As RODisciplinePagedList

      Return DataPortal.Fetch(Of RODisciplinePagedList)(New Criteria())

    End Function

    Public Shared Function GetRODisciplinePagedList(SystemID As Integer?) As RODisciplinePagedList

      Return DataPortal.Fetch(Of RODisciplinePagedList)(New Criteria(SystemID))

    End Function

    Public Shared Function GetRODisciplineList(DisciplineID As Integer?) As RODisciplinePagedList

      Return DataPortal.Fetch(Of RODisciplinePagedList)(New Criteria(DisciplineID))

    End Function

    Public Shared Function GetRODisciplineList(Discipline As String) As RODisciplinePagedList

      Return DataPortal.Fetch(Of RODisciplinePagedList)(New Criteria(Discipline))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODisciplinePaged.GetRODisciplinePaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODisciplinePagedList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@Discipline", Singular.Strings.MakeEmptyDBNull(crit.Discipline))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace