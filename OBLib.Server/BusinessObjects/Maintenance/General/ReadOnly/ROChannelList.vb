﻿' Generated 13 Feb 2014 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROChannelList
    Inherits OBReadOnlyListBase(Of ROChannelList, ROChannel)

#Region " Business Methods "

    Public Function GetItem(ChannelID As Integer) As ROChannel

      For Each child As ROChannel In Me
        If child.ChannelID = ChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Channels"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ChannelID As Integer? = Nothing

      <Display(Name:="ChannelName", Description:=""), PrimarySearchField>
      Public Property ChannelName() As String

      Public Property UserID As Integer?

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROChannelList() As ROChannelList

      Return New ROChannelList()

    End Function

    Public Shared Sub BeginGetROChannelList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROChannelList)))

      Dim dp As New DataPortal(Of ROChannelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROChannelList(CallBack As EventHandler(Of DataPortalResult(Of ROChannelList)))

      Dim dp As New DataPortal(Of ROChannelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROChannelList() As ROChannelList

      Return DataPortal.Fetch(Of ROChannelList)(New Criteria())

    End Function

    Public Shared Function GetUserDefaultChannels(UserID As Integer) As ROChannelList
      Return DataPortal.Fetch(Of ROChannelList)(New Criteria() With {.UserID = UserID})
    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROChannel.GetROChannel(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getROChannelList"
            cm.Parameters.AddWithValue("@UserID", ZeroNothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@ChannelName", NothingDBNull(crit.ChannelName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace