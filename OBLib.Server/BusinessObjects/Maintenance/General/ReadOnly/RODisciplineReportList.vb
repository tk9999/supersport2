﻿' Generated 09 Sep 2016 21:21 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODisciplineReportList
    Inherits OBReadOnlyListBase(Of RODisciplineReportList, RODisciplineReport)

#Region " Business Methods "

    Public Function GetItem(DisciplineID As Integer) As RODisciplineReport

      For Each child As RODisciplineReport In Me
        If child.DisciplineID = DisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemIDs As List(Of Integer)
      Public Property ProductionAreaIDs As List(Of Integer)

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRODisciplineReportList() As RODisciplineReportList

      Return New RODisciplineReportList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRODisciplineReportList() As RODisciplineReportList

      Return DataPortal.Fetch(Of RODisciplineReportList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODisciplineReport.GetRODisciplineReport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODisciplineListReport"
            cm.Parameters.AddWithValue("@SystemIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace