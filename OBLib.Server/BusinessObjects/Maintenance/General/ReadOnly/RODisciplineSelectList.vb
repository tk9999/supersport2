﻿' Generated 18 Mar 2016 12:02 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODisciplineSelectList
    Inherits OBReadOnlyListBase(Of RODisciplineSelectList, RODisciplineSelect)

#Region " Business Methods "

    Public Function GetItem(DisciplineID As Integer) As RODisciplineSelect

      For Each child As RODisciplineSelect In Me
        If child.DisciplineID = DisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property Discipline As String = ""
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemIDs As List(Of Integer)
      Public Property ProductionAreaIDs As List(Of Integer)

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRODisciplineSelectList() As RODisciplineSelectList

      Return New RODisciplineSelectList()

    End Function

    Public Shared Sub BeginGetRODisciplineSelectList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RODisciplineSelectList)))

      Dim dp As New DataPortal(Of RODisciplineSelectList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRODisciplineSelectList(CallBack As EventHandler(Of DataPortalResult(Of RODisciplineSelectList)))

      Dim dp As New DataPortal(Of RODisciplineSelectList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRODisciplineSelectList() As RODisciplineSelectList

      Return DataPortal.Fetch(Of RODisciplineSelectList)(New Criteria())

    End Function

    Public Shared Function GetRODisciplineSelectList(SystemID As Integer?) As RODisciplineSelectList

      Return DataPortal.Fetch(Of RODisciplineSelectList)(New Criteria(SystemID, Nothing))

    End Function


    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODisciplineSelect.GetRODisciplineSelect(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODisciplineListSelect"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace