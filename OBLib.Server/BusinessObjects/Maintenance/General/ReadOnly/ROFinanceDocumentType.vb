﻿' Generated 26 Jan 2015 06:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROFinanceDocumentType
    Inherits SingularReadOnlyBase(Of ROFinanceDocumentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FinanceDocumentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FinanceDocumentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FinanceDocumentTypeID() As Integer
      Get
        Return GetProperty(FinanceDocumentTypeIDProperty)
      End Get
    End Property

    Public Shared FinanceDocumentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FinanceDocumentType, "Finance Document Type", "")
    ''' <summary>
    ''' Gets the Finance Document Type value
    ''' </summary>
    <Display(Name:="Finance Document Type", Description:="")>
  Public ReadOnly Property FinanceDocumentType() As String
      Get
        Return GetProperty(FinanceDocumentTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FinanceDocumentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FinanceDocumentType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROFinanceDocumentType(dr As SafeDataReader) As ROFinanceDocumentType

      Dim r As New ROFinanceDocumentType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FinanceDocumentTypeIDProperty, .GetInt32(0))
        LoadProperty(FinanceDocumentTypeProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace