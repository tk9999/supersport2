﻿' Generated 15 Aug 2014 11:22 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROShiftPattern
    Inherits OBReadOnlyBase(Of ROShiftPattern)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftPatternID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ShiftPatternID() As Integer
      Get
        Return GetProperty(ShiftPatternIDProperty)
      End Get
    End Property

    Public Shared ShiftPatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftPatternName, "Shift Pattern Name", "")
    ''' <summary>
    ''' Gets the Shift Pattern Name value
    ''' </summary>
    <Display(Name:="Shift Pattern Name", Description:="The name of the shift pattern")>
    Public ReadOnly Property ShiftPatternName() As String
      Get
        Return GetProperty(ShiftPatternNameProperty)
      End Get
    End Property

    Public Shared ShiftPatternWeekendDurationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftPatternWeekendDurationID, "Shift Pattern Weekend Duration", Nothing)
    ''' <summary>
    ''' Gets the Shift Pattern Weekend Duration value
    ''' </summary>
    <Display(Name:="Shift Pattern Weekend Duration", Description:="The weekend duration for this shift pattern")>
    Public ReadOnly Property ShiftPatternWeekendDurationID() As Integer?
      Get
        Return GetProperty(ShiftPatternWeekendDurationIDProperty)
      End Get
    End Property

    Public Shared OffWeekendNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OffWeekendNumber, "Off Weekend Number", 0)
    ''' <summary>
    ''' Gets the Off Weekend Number value
    ''' </summary>
    <Display(Name:="Off Weekend Number", Description:="The 1st, 2nd ect weekend that the worker will be off. The 1st Friday of the month will identify the first weekend of the month. Zero indicates that this pattern does not have off weekends.")>
    Public ReadOnly Property OffWeekendNumber() As Integer
      Get
        Return GetProperty(OffWeekendNumberProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ShiftPatternName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROShiftPattern(dr As SafeDataReader) As ROShiftPattern

      Dim r As New ROShiftPattern()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ShiftPatternIDProperty, .GetInt32(0))
        LoadProperty(ShiftPatternNameProperty, .GetString(1))
        LoadProperty(ShiftPatternWeekendDurationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(OffWeekendNumberProperty, .GetInt32(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace