﻿' Generated 29 Dec 2014 11:55 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROSupplierHumanResource
    Inherits OBReadOnlyBase(Of ROSupplierHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SupplierHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SupplierHumanResourceID() As Integer
      Get
        Return GetProperty(SupplierHumanResourceIDProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier for which this human resource belongs to")>
    Public ReadOnly Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname", "")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="Firstname of the person")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="Surname of the person")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="ID No. of the person")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number", "")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="Cellphone No. of the person")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSupplierHumanResource(dr As SafeDataReader) As ROSupplierHumanResource

      Dim r As New ROSupplierHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SupplierHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FirstnameProperty, .GetString(2))
        LoadProperty(SurnameProperty, .GetString(3))
        LoadProperty(IDNoProperty, .GetString(4))
        LoadProperty(CellPhoneNumberProperty, .GetString(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace