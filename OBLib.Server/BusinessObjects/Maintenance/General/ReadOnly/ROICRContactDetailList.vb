﻿' Generated 27 Jan 2015 11:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROICRContactDetailList
    Inherits OBReadOnlyListBase(Of ROICRContactDetailList, ROICRContactDetail)

#Region " Business Methods "

    Public Function GetItem(ICRContactDetailID As Integer) As ROICRContactDetail

      For Each child As ROICRContactDetail In Me
        If child.ICRContactDetailID = ICRContactDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "ICR Contact Details"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROICRContactDetailList() As ROICRContactDetailList

      Return New ROICRContactDetailList()

    End Function

    Public Shared Sub BeginGetROICRContactDetailList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROICRContactDetailList)))

      Dim dp As New DataPortal(Of ROICRContactDetailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROICRContactDetailList(CallBack As EventHandler(Of DataPortalResult(Of ROICRContactDetailList)))

      Dim dp As New DataPortal(Of ROICRContactDetailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROICRContactDetailList() As ROICRContactDetailList

      Return DataPortal.Fetch(Of ROICRContactDetailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROICRContactDetail.GetROICRContactDetail(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROICRContactDetailList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace