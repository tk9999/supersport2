﻿' Generated 09 Sep 2016 21:21 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODisciplineReport
    Inherits OBReadOnlyBase(Of RODisciplineReport)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RODisciplineReportBO.RODisciplineReportBOToString(self)")

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Discipline

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetRODisciplineReport(dr As SafeDataReader) As RODisciplineReport

      Dim r As New RODisciplineReport()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DisciplineIDProperty, .GetInt32(0))
        LoadProperty(DisciplineProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace