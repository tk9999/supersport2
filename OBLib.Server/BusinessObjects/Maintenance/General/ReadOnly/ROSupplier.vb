﻿' Generated 21 May 2014 13:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROSupplier
    Inherits SingularReadOnlyBase(Of ROSupplier)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Name of the Supplier")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number for the Supplier")>
    Public ReadOnly Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Fax number for the Supplier")>
    Public ReadOnly Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="Name of the contact person")>
    Public ReadOnly Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address for the Supplier")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared VatNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatNo, "Vat No", "")
    ''' <summary>
    ''' Gets the Vat No value
    ''' </summary>
    <Display(Name:="Vat No", Description:="VAT number for the Supplier")>
    Public ReadOnly Property VatNo() As String
      Get
        Return GetProperty(VatNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Supplier

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSupplier(dr As SafeDataReader) As ROSupplier

      Dim r As New ROSupplier()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SupplierIDProperty, .GetInt32(0))
        LoadProperty(SupplierProperty, .GetString(1))
        LoadProperty(TelNoProperty, .GetString(2))
        LoadProperty(FaxNoProperty, .GetString(3))
        LoadProperty(ContactNameProperty, .GetString(4))
        LoadProperty(EmailAddressProperty, .GetString(5))
        LoadProperty(VatNoProperty, .GetString(6))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace