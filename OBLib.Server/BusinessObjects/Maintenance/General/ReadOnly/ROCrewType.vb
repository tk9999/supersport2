﻿' Generated 29 Jun 2014 17:10 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROCrewType
    Inherits SingularReadOnlyBase(Of ROCrewType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CrewTypeID() As Integer
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type", "")
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="Description for the Crew Type")>
    Public ReadOnly Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
    End Property

    Public Shared RequiresVehicleIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresVehicleInd, "Requires Vehicle", False)
    ''' <summary>
    ''' Gets the Requires Vehicle value
    ''' </summary>
    <Display(Name:="Requires Vehicle", Description:="True indicates that this crew type required a vehicle")>
    Public ReadOnly Property RequiresVehicleInd() As Boolean
      Get
        Return GetProperty(RequiresVehicleIndProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared TravelScreenDisplayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelScreenDisplayName, "Travel Screen Display Name", "")
    ''' <summary>
    ''' Gets the Travel Screen Display Name value
    ''' </summary>
    <Display(Name:="Travel Screen Display Name", Description:="")>
    Public ReadOnly Property TravelScreenDisplayName() As String
      Get
        Return GetProperty(TravelScreenDisplayNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CrewType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCrewType(dr As SafeDataReader) As ROCrewType

      Dim r As New ROCrewType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CrewTypeIDProperty, .GetInt32(0))
        LoadProperty(CrewTypeProperty, .GetString(1))
        LoadProperty(RequiresVehicleIndProperty, .GetBoolean(2))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(TravelScreenDisplayNameProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace