﻿' Generated 26 Mar 2014 13:06 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROProductionChannelStatus
    Inherits SingularReadOnlyBase(Of ROProductionChannelStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionChannelStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionChannelStatusID() As Integer
      Get
        Return GetProperty(ProductionChannelStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionChannelStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionChannelStatus, "Production Channel Status", "")
    ''' <summary>
    ''' Gets the Production Channel Status value
    ''' </summary>
    <Display(Name:="Production Channel Status", Description:="")>
    Public ReadOnly Property ProductionChannelStatus() As String
      Get
        Return GetProperty(ProductionChannelStatusProperty)
      End Get
    End Property

    Public Shared ProductionChannelStatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionChannelStatusCode, "Production Channel Status Code", "")
    ''' <summary>
    ''' Gets the Production Channel Status Code value
    ''' </summary>
    <Display(Name:="Production Channel Status Code", Description:="")>
    Public ReadOnly Property ProductionChannelStatusCode() As String
      Get
        Return GetProperty(ProductionChannelStatusCodeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionChannelStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionChannelStatus

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionChannelStatus(dr As SafeDataReader) As ROProductionChannelStatus

      Dim r As New ROProductionChannelStatus()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionChannelStatusIDProperty, .GetInt32(0))
        LoadProperty(ProductionChannelStatusProperty, .GetString(1))
        LoadProperty(ProductionChannelStatusCodeProperty, .GetString(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace