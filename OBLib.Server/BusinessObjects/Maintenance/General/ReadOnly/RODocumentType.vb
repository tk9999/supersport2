﻿' Generated 24 Jul 2014 16:01 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODocumentType
    Inherits SingularReadOnlyBase(Of RODocumentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DocumentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DocumentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DocumentTypeID() As Integer
      Get
        Return GetProperty(DocumentTypeIDProperty)
      End Get
    End Property

    Public Shared DocumentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DocumentType, "Document Type", "")
    ''' <summary>
    ''' Gets the Document Type value
    ''' </summary>
    <Display(Name:="Document Type", Description:="")>
    Public ReadOnly Property DocumentType() As String
      Get
        Return GetProperty(DocumentTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DocumentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.DocumentType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRODocumentType(dr As SafeDataReader) As RODocumentType

      Dim r As New RODocumentType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DocumentTypeIDProperty, .GetInt32(0))
        LoadProperty(DocumentTypeProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace