﻿' Generated 25 Jan 2016 07:05 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROShiftDuration
    Inherits OBReadOnlyBase(Of ROShiftDuration)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ShiftDurationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftDuration, "Shift Duration")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ShiftDuration() As Integer
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
    End Property

    Public Shared ShiftDurationNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftDurationName, "Shift Duration Name")
    ''' <summary>
    ''' Gets the Shift Duration Name value
    ''' </summary>
    <Display(Name:="Shift Duration Name", Description:="")>
    Public ReadOnly Property ShiftDurationName() As String
      Get
        Return GetProperty(ShiftDurationNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ShiftDurationProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ShiftDurationName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROShiftDuration(dr As SafeDataReader) As ROShiftDuration

      Dim r As New ROShiftDuration()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ShiftDurationProperty, .GetInt32(0))
        LoadProperty(ShiftDurationNameProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
