﻿' Generated 20 Jul 2014 17:56 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROOutsourceServiceType
    Inherits SingularReadOnlyBase(Of ROOutsourceServiceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OutsourceServiceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property OutsourceServiceTypeID() As Integer
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared OutsourceServiceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OutsourceServiceType, "Outsource Service Type", "")
    ''' <summary>
    ''' Gets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Outsource Service Type", Description:="Description for the Outsource Service Type")>
    Public ReadOnly Property OutsourceServiceType() As String
      Get
        Return GetProperty(OutsourceServiceTypeProperty)
      End Get
    End Property

    Public Shared ServiceTimesRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ServiceTimesRequiredInd, "Service Times Required", False)
    ''' <summary>
    ''' Gets the Service Times Required value
    ''' </summary>
    <Display(Name:="Service Times Required", Description:="Tick indicates that start dates and end dates must be captured")>
    Public ReadOnly Property ServiceTimesRequiredInd() As Boolean
      Get
        Return GetProperty(ServiceTimesRequiredIndProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Tick indicates that this OutsourceServiceType is core to the system")>
    Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public Shared AlwaysRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AlwaysRequiredInd, "Always Required", False)
    ''' <summary>
    ''' Gets the Always Required value
    ''' </summary>
    <Display(Name:="Always Required", Description:="Tick indicates that the sevce type must always be allocated to the production")>
    Public ReadOnly Property AlwaysRequiredInd() As Boolean
      Get
        Return GetProperty(AlwaysRequiredIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OutsourceServiceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.OutsourceServiceType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROOutsourceServiceType(dr As SafeDataReader) As ROOutsourceServiceType

      Dim r As New ROOutsourceServiceType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(OutsourceServiceTypeIDProperty, .GetInt32(0))
        LoadProperty(OutsourceServiceTypeProperty, .GetString(1))
        LoadProperty(ServiceTimesRequiredIndProperty, .GetBoolean(2))
        LoadProperty(SystemIndProperty, .GetBoolean(3))
        LoadProperty(AlwaysRequiredIndProperty, .GetBoolean(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace