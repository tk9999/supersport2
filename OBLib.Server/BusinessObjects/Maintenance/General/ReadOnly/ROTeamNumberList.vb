﻿' Generated 25 Jan 2016 12:18 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROTeamNumberList
    Inherits OBReadOnlyListBase(Of ROTeamNumberList, ROTeamNumber)

#Region " Business Methods "

    Public Function GetItem(TeamNumber As Integer) As ROTeamNumber

      For Each child As ROTeamNumber In Me
        If child.TeamNumber = TeamNumber Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTeamNumberList() As ROTeamNumberList

      Return New ROTeamNumberList()

    End Function

    Public Shared Sub BeginGetROTeamNumberList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTeamNumberList)))

      Dim dp As New DataPortal(Of ROTeamNumberList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTeamNumberList(CallBack As EventHandler(Of DataPortalResult(Of ROTeamNumberList)))

      Dim dp As New DataPortal(Of ROTeamNumberList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTeamNumberList() As ROTeamNumberList

      Return DataPortal.Fetch(Of ROTeamNumberList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTeamNumber.GetROTeamNumber(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTeamNumberList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace