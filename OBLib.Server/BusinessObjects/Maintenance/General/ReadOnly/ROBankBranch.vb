﻿' Generated 12 Aug 2014 13:34 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROBankBranch
    Inherits OBReadOnlyBase(Of ROBankBranch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared BankBranchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BankBranchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property BankBranchID() As Integer
      Get
        Return GetProperty(BankBranchIDProperty)
      End Get
    End Property

    Public Shared BankIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BankID, "Bank", Nothing)
    ''' <summary>
    ''' Gets the Bank value
    ''' </summary>
    <Display(Name:="Bank", Description:="Bank this branch belongs to")>
    Public ReadOnly Property BankID() As Integer?
      Get
        Return GetProperty(BankIDProperty)
      End Get
    End Property

    Public Shared BranchCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BranchCode, "Branch Code", "")
    ''' <summary>
    ''' Gets the Branch Code value
    ''' </summary>
    <Display(Name:="Branch Code", Description:="Unique branch code")>
    Public ReadOnly Property BranchCode() As String
      Get
        Return GetProperty(BranchCodeProperty)
      End Get
    End Property

    Public Shared BranchProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Branch, "Branch", "")
    ''' <summary>
    ''' Gets the Branch value
    ''' </summary>
    <Display(Name:="Branch", Description:="Name of the branch")>
    Public ReadOnly Property Branch() As String
      Get
        Return GetProperty(BranchProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(BankBranchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BranchCode

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROBankBranch(dr As SafeDataReader) As ROBankBranch

      Dim r As New ROBankBranch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(BankBranchIDProperty, .GetInt32(0))
        LoadProperty(BankIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(BranchCodeProperty, .GetString(2))
        LoadProperty(BranchProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace