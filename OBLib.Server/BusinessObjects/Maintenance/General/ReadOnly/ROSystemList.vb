﻿' Generated 30 May 2014 11:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROSystemList
    Inherits OBReadOnlyListBase(Of ROSystemList, ROSystem)

#Region " Business Methods "

    Public Function GetItem(SystemID As Integer) As ROSystem

      For Each child As ROSystem In Me
        If child.SystemID = SystemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Systems"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property DepartmentID As Integer?

      Public Sub New(DepartmentID As Integer?)
        Me.DepartmentID = DepartmentID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemList() As ROSystemList

      Return New ROSystemList()

    End Function

    Public Shared Sub BeginGetROSystemList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemList)))

      Dim dp As New DataPortal(Of ROSystemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemList)))

      Dim dp As New DataPortal(Of ROSystemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemList() As ROSystemList

      Return DataPortal.Fetch(Of ROSystemList)(New Criteria())

    End Function

    Public Shared Function GetROSystemList(DepartmentID As Integer?) As ROSystemList

      Return DataPortal.Fetch(Of ROSystemList)(New Criteria(DepartmentID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystem.GetROSystem(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemList"
            cm.Parameters.AddWithValue("@DepartmentID", Singular.Misc.NothingDBNull(crit.DepartmentID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace