﻿' Generated 05 Jan 2016 14:05 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General

  <Serializable()> _
  Public Class ROPagedChannelList
    Inherits OBReadOnlyListBase(Of ROPagedChannelList, ROPagedChannel)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ChannelID As Integer) As ROPagedChannel

      For Each child As ROPagedChannel In Me
        If child.ChannelID = ChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Sub New()


      End Sub

      Public Sub New(KeyWord As String, HDInd As Boolean?)

        Me.KeyWord = KeyWord
        Me.HDInd = HDInd

      End Sub

      Public Property KeyWord As String = Nothing
      Public Property HDInd As Boolean? = Nothing

    End Class

#Region " Common "

    Public Shared Function NewROPagedChannelList() As ROPagedChannelList

      Return New ROPagedChannelList()

    End Function

    Public Shared Sub BeginGetROPagedChannelList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPagedChannelList)))

      Dim dp As New DataPortal(Of ROPagedChannelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPagedChannelList(CallBack As EventHandler(Of DataPortalResult(Of ROPagedChannelList)))

      Dim dp As New DataPortal(Of ROPagedChannelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPagedChannelList() As ROPagedChannelList

      Return DataPortal.Fetch(Of ROPagedChannelList)(New Criteria())

    End Function

    Public Shared Function GetROPagedChannelList(KeyWord As String, HDInd As Boolean?) As ROPagedChannelList

      Return DataPortal.Fetch(Of ROPagedChannelList)(New Criteria(KeyWord, HDInd))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPagedChannel.GetROPagedChannel(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPagedChannelList"
            cm.Parameters.AddWithValue("@KeyWord", NothingDBNull(crit.KeyWord))
            cm.Parameters.AddWithValue("@HDInd", NothingDBNull(crit.HDInd))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
