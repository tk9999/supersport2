﻿' Generated 30 Jun 2014 14:07 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODiscipline
    Inherits OBReadOnlyBase(Of RODiscipline)

#Region " Properties and Methods "

    <Singular.DataAnnotations.ClientOnly()>
    Public Property SelectedInd As Boolean

#Region " Properties "

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="Description for the Discipline")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionRequiredInd, "Position Required", False)
    ''' <summary>
    ''' Gets the Position Required value
    ''' </summary>
    <Display(Name:="Position Required", Description:="Tick indicates that Human Resource Skill Position(s) are required when creating a Human Resource Skill for a person")>
    Public ReadOnly Property PositionRequiredInd() As Boolean
      Get
        Return GetProperty(PositionRequiredIndProperty)
      End Get
    End Property

    Public Shared SkillLevelRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SkillLevelRequiredInd, "Skill Level Required", False)
    ''' <summary>
    ''' Gets the Skill Level Required value
    ''' </summary>
    <Display(Name:="Skill Level Required", Description:="Tick indicates that a Skill Level is required for Human Resource Skill(s) that are linked to this discipline")>
    Public ReadOnly Property SkillLevelRequiredInd() As Boolean
      Get
        Return GetProperty(SkillLevelRequiredIndProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Tick indicates the a discipline is a system discipline and may not be modified")>
    Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No", 0)
    ''' <summary>
    ''' Gets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
    Public ReadOnly Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ShiftWorkerIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.ShiftWorkerInd, "Shift Worker", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Shift Worker value
    ''' </summary>
    <Display(Name:="Shift Worker", Description:="")>
    Public ReadOnly Property ShiftWorkerInd() As Boolean?
      Get
        Return GetProperty(ShiftWorkerIndProperty)
      End Get
    End Property

    Public Shared NonShiftWorkerIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.NonShiftWorkerInd, "Non-Shift Worker", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Non-Shift Worker value
    ''' </summary>
    <Display(Name:="Non-Shift Worker", Description:="")>
    Public ReadOnly Property NonShiftWorkerInd() As Boolean?
      Get
        Return GetProperty(NonShiftWorkerIndProperty)
      End Get
    End Property

    Public Shared OperationalIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.OperationalInd, "Operational", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Operational value
    ''' </summary>
    <Display(Name:="Operational", Description:="")>
    Public ReadOnly Property OperationalInd() As Boolean?
      Get
        Return GetProperty(OperationalIndProperty)
      End Get
    End Property

    Public Shared ManagerIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.ManagerInd, "Manager", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Manager value
    ''' </summary>
    <Display(Name:="Manager", Description:="")>
    Public ReadOnly Property ManagerInd() As Boolean?
      Get
        Return GetProperty(ManagerIndProperty)
      End Get
    End Property

    Public Shared MealReimbursementManagerIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.MealReimbursementManagerInd, "Meal Reimbursement", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Meal Reimbursement value
    ''' </summary>
    <Display(Name:="Meal Reimbursement", Description:="")>
    Public ReadOnly Property MealReimbursementManagerInd() As Boolean?
      Get
        Return GetProperty(MealReimbursementManagerIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Discipline

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRODiscipline(dr As SafeDataReader) As RODiscipline

      Dim r As New RODiscipline()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DisciplineIDProperty, .GetInt32(0))
        LoadProperty(DisciplineProperty, .GetString(1))
        LoadProperty(PositionRequiredIndProperty, .GetBoolean(2))
        LoadProperty(SkillLevelRequiredIndProperty, .GetBoolean(3))
        LoadProperty(SystemIndProperty, .GetBoolean(4))
        LoadProperty(OrderNoProperty, .GetInt32(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ShiftWorkerIndProperty, .GetBoolean(11))
        LoadProperty(NonShiftWorkerIndProperty, .GetBoolean(12))
        LoadProperty(OperationalIndProperty, .GetBoolean(13))
        LoadProperty(ManagerIndProperty, .GetBoolean(14))
        LoadProperty(MealReimbursementManagerIndProperty, .GetBoolean(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace