﻿' Generated 30 Jun 2014 14:07 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class RODisciplineList
    Inherits SingularReadOnlyListBase(Of RODisciplineList, RODiscipline)

#Region " Business Methods "

    Public Function GetItem(DisciplineID As Integer) As RODiscipline

      For Each child As RODiscipline In Me
        If child.DisciplineID = DisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Disciplines"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Overridable Property SystemID As Integer? = Nothing

      Public Sub New(SystemID As Integer?)
        Me.SystemID = SystemID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Class ReportCriteria
      Inherits Criteria

      <Browsable(False)>
      Public Overrides Property SystemID As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

    End Class

#Region " Common "

    Public Shared Function NewRODisciplineList() As RODisciplineList

      Return New RODisciplineList()

    End Function

    Public Shared Sub BeginGetRODisciplineList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RODisciplineList)))

      Dim dp As New DataPortal(Of RODisciplineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRODisciplineList(CallBack As EventHandler(Of DataPortalResult(Of RODisciplineList)))

      Dim dp As New DataPortal(Of RODisciplineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRODisciplineList() As RODisciplineList

      Return DataPortal.Fetch(Of RODisciplineList)(New Criteria())

    End Function

    Public Shared Function GetRODisciplineList(SystemID As Integer?) As RODisciplineList

      Return DataPortal.Fetch(Of RODisciplineList)(New Criteria(SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODiscipline.GetRODiscipline(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODisciplineList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace