﻿' Generated 03 Mar 2015 06:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROSystemParameter
    Inherits SingularReadOnlyBase(Of ROSystemParameter)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemParameterIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemParameterID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemParameterID() As Integer
      Get
        Return GetProperty(SystemParameterIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="The date that the parameters will be effective from. If NOW is after this date the record cannot be edited")>
  Public ReadOnly Property EffectiveDate As DateTime?
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared MaxVehicleTravelHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxVehicleTravelHours, "Max Vehicle Travel Hours", 10)
    ''' <summary>
    ''' Gets the Max Vehicle Travel Hours value
    ''' </summary>
    <Display(Name:="Max Vehicle Travel Hours", Description:="The maximum number of hours that continous travel is allowed. This can be overriden by the event planner")>
  Public ReadOnly Property MaxVehicleTravelHours() As Integer
      Get
        Return GetProperty(MaxVehicleTravelHoursProperty)
      End Get
    End Property

    Public Shared MinCrewTravelDayHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MinCrewTravelDayHours, "Min Crew Travel Day Hours", 5)
    ''' <summary>
    ''' Gets the Min Crew Travel Day Hours value
    ''' </summary>
    <Display(Name:="Min Crew Travel Day Hours", Description:="The minimum number of hours that will be credited to a human resource should they only travel on a day")>
  Public ReadOnly Property MinCrewTravelDayHours() As Integer
      Get
        Return GetProperty(MinCrewTravelDayHoursProperty)
      End Get
    End Property

    Public Shared MinCrewProductionDayHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MinCrewProductionDayHours, "Min Crew Production Day Hours", 10)
    ''' <summary>
    ''' Gets the Min Crew Production Day Hours value
    ''' </summary>
    <Display(Name:="Min Crew Production Day Hours", Description:="The minimum number of hours that will be credited to a human resource should they only work on a production")>
  Public ReadOnly Property MinCrewProductionDayHours() As Integer
      Get
        Return GetProperty(MinCrewProductionDayHoursProperty)
      End Get
    End Property

    Public Shared CrewLeaveDayHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewLeaveDayHours, "Crew Leave Day Hours", 9)
    ''' <summary>
    ''' Gets the Crew Leave Day Hours value
    ''' </summary>
    <Display(Name:="Crew Leave Day Hours", Description:="Hours credited to crew member for leave days")>
  Public ReadOnly Property CrewLeaveDayHours() As Integer
      Get
        Return GetProperty(CrewLeaveDayHoursProperty)
      End Get
    End Property

    Public Shared DayAwayHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DayAwayHours, "Day Away Hours", 10)
    ''' <summary>
    ''' Gets the Day Away Hours value
    ''' </summary>
    <Display(Name:="Day Away Hours", Description:="The number of hours that will be credited should the human resource stay at the away venue between productions")>
  Public ReadOnly Property DayAwayHours() As Integer
      Get
        Return GetProperty(DayAwayHoursProperty)
      End Get
    End Property

    Public Shared ShortfallMinHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShortfallMinHours, "Shortfall Min Hours", 12)
    ''' <summary>
    ''' Gets the Shortfall Min Hours value
    ''' </summary>
    <Display(Name:="Shortfall Min Hours", Description:="The minimum number of hours that the system not allocate additional hours")>
  Public ReadOnly Property ShortfallMinHours() As Integer
      Get
        Return GetProperty(ShortfallMinHoursProperty)
      End Get
    End Property

    Public Shared MaxLateDriveKMProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxLateDriveKM, "Max Late Drive KM", 100)
    ''' <summary>
    ''' Gets the Max Late Drive KM value
    ''' </summary>
    <Display(Name:="Max Late Drive KM", Description:="The maximum number of KM that a human resource is allowed to drive after the max travel cutoff departure time")>
  Public ReadOnly Property MaxLateDriveKM() As Integer
      Get
        Return GetProperty(MaxLateDriveKMProperty)
      End Get
    End Property

    Public Shared MaxCutoffArrivalTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.MaxCutoffArrivalTime, "Max Cutoff Arrival Time")
    ''' <summary>
    ''' Gets the Max Cutoff Arrival Time value
    ''' </summary>
    <Display(Name:="Max Cutoff Arrival Time", Description:="No human resource is allowed to be travelling after this time if the travel time between the venue and the home base results in the human resource arriving after the MaxCuttoffArrivalTime")>
  Public ReadOnly Property MaxCutoffArrivalTime() As Object
      Get
        Dim value = GetProperty(MaxCutoffArrivalTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared SnTBreakfastTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.SnTBreakfastTime, "Sn T Breakfast Time")
    ''' <summary>
    ''' Gets the Sn T Breakfast Time value
    ''' </summary>
    <Display(Name:="Sn T Breakfast Time", Description:="")>
  Public ReadOnly Property SnTBreakfastTime() As Object
      Get
        Dim value = GetProperty(SnTBreakfastTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared SnTLunchStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.SnTLunchStartTime, "Sn T Lunch Start Time")
    ''' <summary>
    ''' Gets the Sn T Lunch Start Time value
    ''' </summary>
    <Display(Name:="Sn T Lunch Start Time", Description:="")>
  Public ReadOnly Property SnTLunchStartTime() As Object
      Get
        Dim value = GetProperty(SnTLunchStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared SnTLunchEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.SnTLunchEndTime, "Sn T Lunch End Time")
    ''' <summary>
    ''' Gets the Sn T Lunch End Time value
    ''' </summary>
    <Display(Name:="Sn T Lunch End Time", Description:="")>
  Public ReadOnly Property SnTLunchEndTime() As Object
      Get
        Dim value = GetProperty(SnTLunchEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared SnTDinnerTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.SnTDinnerTime, "Sn T Dinner Time")
    ''' <summary>
    ''' Gets the Sn T Dinner Time value
    ''' </summary>
    <Display(Name:="Sn T Dinner Time", Description:="")>
  Public ReadOnly Property SnTDinnerTime() As Object
      Get
        Dim value = GetProperty(SnTDinnerTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared OutsourceServicesRequiredDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OutsourceServicesRequiredDays, "Outsource Services Required Days", 7)
    ''' <summary>
    ''' Gets the Outsource Services Required Days value
    ''' </summary>
    <Display(Name:="Outsource Services Required Days", Description:="Days from the first transmission day where the system will require the required outsource services")>
  Public ReadOnly Property OutsourceServicesRequiredDays() As Integer
      Get
        Return GetProperty(OutsourceServicesRequiredDaysProperty)
      End Get
    End Property

    Public Shared MonthHoursNoOvertimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MonthHoursNoOvertime, "Month Hours No Overtime", 180)
    ''' <summary>
    ''' Gets the Month Hours No Overtime value
    ''' </summary>
    <Display(Name:="Month Hours No Overtime", Description:="")>
  Public ReadOnly Property MonthHoursNoOvertime() As Integer
      Get
        Return GetProperty(MonthHoursNoOvertimeProperty)
      End Get
    End Property

    Public Shared DefaultWorkStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultWorkStartTime, "Default Work Start Time")
    ''' <summary>
    ''' Gets the Default Work Start Time value
    ''' </summary>
    <Display(Name:="Default Work Start Time", Description:="")>
  Public ReadOnly Property DefaultWorkStartTime() As Object
      Get
        Dim value = GetProperty(DefaultWorkStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared DefaultWorkEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultWorkEndTime, "Default Work End Time")
    ''' <summary>
    ''' Gets the Default Work End Time value
    ''' </summary>
    <Display(Name:="Default Work End Time", Description:="")>
  Public ReadOnly Property DefaultWorkEndTime() As Object
      Get
        Dim value = GetProperty(DefaultWorkEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared BaseCityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BaseCityID, "Base City", Nothing)
    ''' <summary>
    ''' Gets the Base City value
    ''' </summary>
    <Display(Name:="Base City", Description:="The city that is base (this should never change from Randburg)")>
  Public ReadOnly Property BaseCityID() As Integer?
      Get
        Return GetProperty(BaseCityIDProperty)
      End Get
    End Property

    Public Shared TimesheetMonthEndWorkingDayLimitProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonthEndWorkingDayLimit, "Timesheet Month End Working Day Limit", 1)
    ''' <summary>
    ''' Gets the Timesheet Month End Working Day Limit value
    ''' </summary>
    <Display(Name:="Timesheet Month End Working Day Limit", Description:="")>
  Public ReadOnly Property TimesheetMonthEndWorkingDayLimit() As Integer
      Get
        Return GetProperty(TimesheetMonthEndWorkingDayLimitProperty)
      End Get
    End Property

    Public Shared ReturnFlightWarningNextProductionHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReturnFlightWarningNextProductionHours, "Return Flight Warning Next Production Hours", 48)
    ''' <summary>
    ''' Gets the Return Flight Warning Next Production Hours value
    ''' </summary>
    <Display(Name:="Return Flight Warning Next Production Hours", Description:="")>
  Public ReadOnly Property ReturnFlightWarningNextProductionHours() As Integer
      Get
        Return GetProperty(ReturnFlightWarningNextProductionHoursProperty)
      End Get
    End Property

    Public Shared MaxWorkingHoursPerDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxWorkingHoursPerDay, "Max Working Hours Per Day", 10)
    ''' <summary>
    ''' Gets the Max Working Hours Per Day value
    ''' </summary>
    <Display(Name:="Max Working Hours Per Day", Description:="The maximun number of hours that people can work in a day")>
  Public ReadOnly Property MaxWorkingHoursPerDay() As Integer
      Get
        Return GetProperty(MaxWorkingHoursPerDayProperty)
      End Get
    End Property

    Public Shared MaxRentalCarKMsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxRentalCarKMs, "Max Rental Car K Ms", 0)
    ''' <summary>
    ''' Gets the Max Rental Car K Ms value
    ''' </summary>
    <Display(Name:="Max Rental Car K Ms", Description:="The maximum number of KMs that the system will use a rental car befor switching to filght when auto generating travel")>
  Public ReadOnly Property MaxRentalCarKMs() As Integer
      Get
        Return GetProperty(MaxRentalCarKMsProperty)
      End Get
    End Property

    Public Shared MinRequiredWorkingHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MinRequiredWorkingHours, "Min Required Working Hours", 0)
    ''' <summary>
    ''' Gets the Min Required Working Hours value
    ''' </summary>
    <Display(Name:="Min Required Working Hours", Description:="The minimum working hours required in order to be credited for a full days work")>
  Public ReadOnly Property MinRequiredWorkingHours() As Integer
      Get
        Return GetProperty(MinRequiredWorkingHoursProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ISPOvertimeHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ISPOvertimeHours, "ISP Overtime Hours", 10)
    ''' <summary>
    ''' Gets the ISP Overtime Hours value
    ''' </summary>
    <Display(Name:="ISP Overtime Hours", Description:="")>
  Public ReadOnly Property ISPOvertimeHours() As Integer
      Get
        Return GetProperty(ISPOvertimeHoursProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemParameterIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemParameter(dr As SafeDataReader) As ROSystemParameter

      Dim r As New ROSystemParameter()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemParameterIDProperty, .GetInt32(0))
        LoadProperty(EffectiveDateProperty, .GetValue(1))
        LoadProperty(MaxVehicleTravelHoursProperty, .GetInt32(2))
        LoadProperty(MinCrewTravelDayHoursProperty, .GetInt32(3))
        LoadProperty(MinCrewProductionDayHoursProperty, .GetInt32(4))
        LoadProperty(CrewLeaveDayHoursProperty, .GetInt32(5))
        LoadProperty(DayAwayHoursProperty, .GetInt32(6))
        LoadProperty(ShortfallMinHoursProperty, .GetInt32(7))
        LoadProperty(MaxLateDriveKMProperty, .GetInt32(8))
        If .IsDBNull(9) Then
          LoadProperty(MaxCutoffArrivalTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(MaxCutoffArrivalTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(9))))
        End If
        If .IsDBNull(10) Then
          LoadProperty(SnTBreakfastTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(SnTBreakfastTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(10))))
        End If
        If .IsDBNull(11) Then
          LoadProperty(SnTLunchStartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(SnTLunchStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(11))))
        End If
        If .IsDBNull(12) Then
          LoadProperty(SnTLunchEndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(SnTLunchEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(12))))
        End If
        If .IsDBNull(13) Then
          LoadProperty(SnTDinnerTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(SnTDinnerTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(13))))
        End If
        LoadProperty(OutsourceServicesRequiredDaysProperty, .GetInt32(14))
        LoadProperty(MonthHoursNoOvertimeProperty, .GetInt32(15))
        If .IsDBNull(16) Then
          LoadProperty(DefaultWorkStartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(DefaultWorkStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(16))))
        End If
        If .IsDBNull(17) Then
          LoadProperty(DefaultWorkEndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(DefaultWorkEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(17))))
        End If
        LoadProperty(BaseCityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(TimesheetMonthEndWorkingDayLimitProperty, .GetInt32(19))
        LoadProperty(ReturnFlightWarningNextProductionHoursProperty, .GetInt32(20))
        LoadProperty(MaxWorkingHoursPerDayProperty, .GetInt32(21))
        LoadProperty(MaxRentalCarKMsProperty, .GetInt32(22))
        LoadProperty(MinRequiredWorkingHoursProperty, .GetInt32(23))
        LoadProperty(CreatedByProperty, .GetInt32(24))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(25))
        LoadProperty(ModifiedByProperty, .GetInt32(26))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(27))
        LoadProperty(ISPOvertimeHoursProperty, .GetInt32(28))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace