﻿' Generated 05 Jul 2014 12:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROPosition
    Inherits SingularReadOnlyBase(Of ROPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline that this position falls under")>
    Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="Description for the Position")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared PositionCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionCode, "Position Code", "")
    ''' <summary>
    ''' Gets the Position Code value
    ''' </summary>
    <Display(Name:="Position Code", Description:="Two character code for this position")>
    Public ReadOnly Property PositionCode() As String
      Get
        Return GetProperty(PositionCodeProperty)
      End Get
    End Property

    Public Shared UnMannedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UnMannedInd, "Unmanned", False)
    ''' <summary>
    ''' Gets the Unmanned value
    ''' </summary>
    <Display(Name:="Unmanned", Description:="Tick indicates that this position is unmanned")>
    Public ReadOnly Property UnMannedInd() As Boolean
      Get
        Return GetProperty(UnMannedIndProperty)
      End Get
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority", 0)
    ''' <summary>
    ''' Gets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
    Public ReadOnly Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Position

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPosition(dr As SafeDataReader) As ROPosition

      Dim r As New ROPosition()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PositionIDProperty, .GetInt32(0))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(PositionProperty, .GetString(2))
        LoadProperty(PositionCodeProperty, .GetString(3))
        LoadProperty(UnmannedIndProperty, .GetBoolean(4))
        LoadProperty(PriorityProperty, .GetInt32(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(PositionTypeIDProperty, ZeroNothing(.GetInt32(11)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace