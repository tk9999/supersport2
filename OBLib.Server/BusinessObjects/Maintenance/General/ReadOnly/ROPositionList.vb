﻿' Generated 05 Jul 2014 12:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROPositionList
    Inherits SingularReadOnlyListBase(Of ROPositionList, ROPosition)

#Region " Business Methods "

    Public Function GetItem(PositionID As Integer) As ROPosition

      For Each child As ROPosition In Me
        If child.PositionID = PositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Positions"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing

      <Display(Name:="Position", Description:=""), PrimarySearchField>
      Public Property Position() As String

      Public Sub New(SystemID As Integer?)
        Me.SystemID = SystemID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPositionList() As ROPositionList

      Return New ROPositionList()

    End Function

    Public Shared Sub BeginGetROPositionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPositionList)))

      Dim dp As New DataPortal(Of ROPositionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPositionList(CallBack As EventHandler(Of DataPortalResult(Of ROPositionList)))

      Dim dp As New DataPortal(Of ROPositionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPositionList() As ROPositionList

      Return DataPortal.Fetch(Of ROPositionList)(New Criteria())

    End Function

    Public Shared Function GetROPositionList(SystemID As Integer?) As ROPositionList

      Return DataPortal.Fetch(Of ROPositionList)(New Criteria(SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPosition.GetROPosition(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPositionList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@Position", NothingDBNull(crit.Position))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace