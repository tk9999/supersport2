﻿' Generated 05 Jan 2016 14:05 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General

  <Serializable()> _
  Public Class ROPagedChannel
    Inherits OBReadOnlyBase(Of ROPagedChannel)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel Name")
    ''' <summary>
    ''' Gets the Channel Name value
    ''' </summary>
    <Display(Name:="Channel Name", Description:="")>
    Public ReadOnly Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="")>
    Public ReadOnly Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
    End Property

    Public Shared HDIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDInd, "HD", False)
    ''' <summary>
    ''' Gets the HD value
    ''' </summary>
    <Display(Name:="HD", Description:="")>
    Public ReadOnly Property HDInd() As Boolean
      Get
        Return GetProperty(HDIndProperty)
      End Get
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:="")>
    Public ReadOnly Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
    Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority")
    ''' <summary>
    ''' Gets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
    Public ReadOnly Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ChannelName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPagedChannel(dr As SafeDataReader) As ROPagedChannel

      Dim r As New ROPagedChannel()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ChannelIDProperty, .GetInt32(0))
        LoadProperty(ChannelNameProperty, .GetString(1))
        LoadProperty(ChannelShortNameProperty, .GetString(2))
        LoadProperty(HDIndProperty, .GetBoolean(3))
        LoadProperty(ImportedIndProperty, .GetBoolean(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(8))
        LoadProperty(PrimaryIndProperty, .GetBoolean(9))
        LoadProperty(PriorityProperty, .GetInt32(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
