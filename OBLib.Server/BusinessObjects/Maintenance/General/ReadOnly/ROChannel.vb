﻿' Generated 13 Feb 2014 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROChannel
    Inherits OBReadOnlyBase(Of ROChannel)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel Name", "")
    ''' <summary>
    ''' Gets the Channel Name value
    ''' </summary>
    <Display(Name:="Channel Name", Description:="The name of the channel")>
    Public ReadOnly Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name", "")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="abbreviated version of channel name, similar to a channel code")>
    Public ReadOnly Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
    End Property

    Public Shared HDIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDInd, "HD", False)
    ''' <summary>
    ''' Gets the HD value
    ''' </summary>
    <Display(Name:="HD", Description:="Is this channel an HD channel?")>
    Public ReadOnly Property HDInd() As Boolean
      Get
        Return GetProperty(HDIndProperty)
      End Get
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:="")>
    Public ReadOnly Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
    Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority", 1000)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
    Public ReadOnly Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
    End Property

    Public Shared IsDefaultProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsDefault, "IsDefault", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="IsDefault", Description:="")>
    Public ReadOnly Property IsDefault() As Boolean
      Get
        Return GetProperty(IsDefaultProperty)
      End Get
    End Property

    Public Shared CssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CssClass, "Css Class")
    ''' <summary>
    ''' Gets and sets the Css Class value
    ''' </summary>
    <Display(Name:="Css Class", Description:="")>
    Public ReadOnly Property CssClass() As String
      Get
        Return GetProperty(CssClassProperty)
      End Get
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SiteID, "Site")
    ''' <summary>
    ''' Gets and sets the Site value
    ''' </summary>
    <Display(Name:="Site", Description:=""),
    Required(ErrorMessage:="Site required")>
    Public ReadOnly Property SiteID() As Integer
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared SiteNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SiteName, "Site Name")
    ''' <summary>
    ''' Gets and sets the Site Name value
    ''' </summary>
    <Display(Name:="Site Name", Description:="")>
    Public ReadOnly Property SiteName() As String
      Get
        Return GetProperty(SiteNameProperty)
      End Get
    End Property

    'Public Shared IsMaximoProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsMaximo, "Is Maximo", False)
    ' ''' <summary>
    ' ''' Gets and sets the Is Maximo value
    ' ''' </summary>
    '<Display(Name:="Is Maximo", Description:=""),
    'Required(ErrorMessage:="Is Maximo required")>
    'Public ReadOnly Property IsMaximo() As Boolean
    '  Get
    '    Return GetProperty(IsMaximoProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ChannelName

    End Function

    Sub New()

      CType(IsSelectedProperty, Singular.SPropertyInfo(Of Boolean, ROChannel)).AddSetExpression("ROChannelBO.IsSelectedSet(self)")

    End Sub

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROChannel(dr As SafeDataReader) As ROChannel

      Dim r As New ROChannel()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ChannelIDProperty, .GetInt32(0))
        LoadProperty(ChannelNameProperty, .GetString(1))
        LoadProperty(ChannelShortNameProperty, .GetString(2))
        LoadProperty(HDIndProperty, .GetBoolean(3))
        LoadProperty(ImportedIndProperty, .GetBoolean(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(PrimaryIndProperty, .GetBoolean(9))
        LoadProperty(PriorityProperty, .GetInt32(10))
        'LoadProperty(CssClassProperty, .GetString(11))
        LoadProperty(SiteIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(SiteNameProperty, .GetString(13))
        LoadProperty(IsDefaultProperty, .GetBoolean(14))
        'LoadProperty(IsMaximoProperty, .GetBoolean(14))
      End With

    End Sub

#End If

#End Region

#End Region

    Sub SetSelected()
      LoadProperty(IsSelectedProperty, True)
    End Sub

  End Class

End Namespace