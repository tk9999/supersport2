﻿' Generated 03 Mar 2015 06:35 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROCalculationChangedDateList
    Inherits SingularReadOnlyListBase(Of ROCalculationChangedDateList, ROCalculationChangedDate)

#Region " Business Methods "

    Public Function GetItem(CalculationChangedDateID As Integer) As ROCalculationChangedDate

      For Each child As ROCalculationChangedDate In Me
        If child.CalculationChangedDateID = CalculationChangedDateID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(CalculationChange As CommonData.Enums.CalculationChange) As ROCalculationChangedDate

      For Each child As ROCalculationChangedDate In Me
        If child.CalculationChangedDateID = CalculationChange Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Calculation Changed Dates"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCalculationChangedDateList() As ROCalculationChangedDateList

      Return New ROCalculationChangedDateList()

    End Function

    Public Shared Sub BeginGetROCalculationChangedDateList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCalculationChangedDateList)))

      Dim dp As New DataPortal(Of ROCalculationChangedDateList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCalculationChangedDateList(CallBack As EventHandler(Of DataPortalResult(Of ROCalculationChangedDateList)))

      Dim dp As New DataPortal(Of ROCalculationChangedDateList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCalculationChangedDateList() As ROCalculationChangedDateList

      Return DataPortal.Fetch(Of ROCalculationChangedDateList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCalculationChangedDate.GetROCalculationChangedDate(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCalculationChangedDateList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace