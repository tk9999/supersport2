﻿' Generated 26 Jan 2015 06:33 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROISPCancellationRuleList
    Inherits SingularReadOnlyListBase(Of ROISPCancellationRuleList, ROISPCancellationRule)

#Region " Business Methods "

    Public Function GetItem(ISPCancellationRuleID As Integer) As ROISPCancellationRule

      For Each child As ROISPCancellationRule In Me
        If child.ISPCancellationRuleID = ISPCancellationRuleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "ISP Cancellation Rules"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROISPCancellationRuleList() As ROISPCancellationRuleList

      Return New ROISPCancellationRuleList()

    End Function

    Public Shared Sub BeginGetROISPCancellationRuleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROISPCancellationRuleList)))

      Dim dp As New DataPortal(Of ROISPCancellationRuleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROISPCancellationRuleList(CallBack As EventHandler(Of DataPortalResult(Of ROISPCancellationRuleList)))

      Dim dp As New DataPortal(Of ROISPCancellationRuleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROISPCancellationRuleList() As ROISPCancellationRuleList

      Return DataPortal.Fetch(Of ROISPCancellationRuleList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROISPCancellationRule.GetROISPCancellationRule(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROISPCancellationRuleList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace