﻿' Generated 26 Jan 2015 06:33 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROISPCancellationRule
    Inherits SingularReadOnlyBase(Of ROISPCancellationRule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ISPCancellationRuleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ISPCancellationRuleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ISPCancellationRuleID() As Integer
      Get
        Return GetProperty(ISPCancellationRuleIDProperty)
      End Get
    End Property

    Public Shared DaysFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DaysFrom, "Days From", 0)
    ''' <summary>
    ''' Gets the Days From value
    ''' </summary>
    <Display(Name:="Days From", Description:="Start number of days until 8am on transmission day (from this day the ISP will be paid a percentage of their rate)")>
  Public ReadOnly Property DaysFrom() As Integer
      Get
        Return GetProperty(DaysFromProperty)
      End Get
    End Property

    Public Shared DaysToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DaysTo, "Days To", 0)
    ''' <summary>
    ''' Gets the Days To value
    ''' </summary>
    <Display(Name:="Days To", Description:="End number of days until 8am on transmission day (from this day the ISP will be paid a percentage of their rate)")>
  Public ReadOnly Property DaysTo() As Integer
      Get
        Return GetProperty(DaysToProperty)
      End Get
    End Property

    Public Shared PercentageOfRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PercentageOfRate, "Percentage Of Rate", 0)
    ''' <summary>
    ''' Gets the Percentage Of Rate value
    ''' </summary>
    <Display(Name:="Percentage Of Rate", Description:="The percentage of the rate the must be paid if the cancellation is between the days from and days to")>
  Public ReadOnly Property PercentageOfRate() As Decimal
      Get
        Return GetProperty(PercentageOfRateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ISPCancellationRuleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROISPCancellationRule(dr As SafeDataReader) As ROISPCancellationRule

      Dim r As New ROISPCancellationRule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ISPCancellationRuleIDProperty, .GetInt32(0))
        LoadProperty(DaysFromProperty, .GetInt32(1))
        LoadProperty(DaysToProperty, .GetInt32(2))
        LoadProperty(PercentageOfRateProperty, .GetDecimal(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace