﻿' Generated 25 Jan 2016 12:18 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROTeamNumber
    Inherits OBReadOnlyBase(Of ROTeamNumber)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TeamNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TeamNumber, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TeamNumber() As Integer
      Get
        Return GetProperty(TeamNumberProperty)
      End Get
    End Property

    Public Shared TeamNumberDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamNumberDescription, "Team Number Description")
    ''' <summary>
    ''' Gets the Team Number Description value
    ''' </summary>
    <Display(Name:="Team Number Description", Description:="")>
    Public ReadOnly Property TeamNumberDescription() As String
      Get
        Return GetProperty(TeamNumberDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TeamNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TeamNumberDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTeamNumber(dr As SafeDataReader) As ROTeamNumber

      Dim r As New ROTeamNumber()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TeamNumberProperty, .GetInt32(0))
        LoadProperty(TeamNumberDescriptionProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
