﻿' Generated 27 Sep 2014 15:28 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROPublicHolidayList
    Inherits OBReadOnlyListBase(Of ROPublicHolidayList, ROPublicHoliday)

#Region " Business Methods "

    Public Function GetItem(PublicHolidayID As Integer) As ROPublicHoliday

      For Each child As ROPublicHoliday In Me
        If child.PublicHolidayID = PublicHolidayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Public Holidays"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?)

        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPublicHolidayList() As ROPublicHolidayList

      Return New ROPublicHolidayList()

    End Function

    Public Shared Sub BeginGetROPublicHolidayList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPublicHolidayList)))

      Dim dp As New DataPortal(Of ROPublicHolidayList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPublicHolidayList(CallBack As EventHandler(Of DataPortalResult(Of ROPublicHolidayList)))

      Dim dp As New DataPortal(Of ROPublicHolidayList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPublicHolidayList(StartDate As DateTime?, EndDate As DateTime?) As ROPublicHolidayList

      Return DataPortal.Fetch(Of ROPublicHolidayList)(New Criteria(StartDate, EndDate))

    End Function

    Public Shared Function GetROPublicHolidayList() As ROPublicHolidayList

      Return DataPortal.Fetch(Of ROPublicHolidayList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPublicHoliday.GetROPublicHoliday(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPublicHolidayList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace