﻿' Generated 26 Jan 2015 06:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROFinanceDocumentTypeList
    Inherits SingularReadOnlyListBase(Of ROFinanceDocumentTypeList, ROFinanceDocumentType)

#Region " Business Methods "

    Public Function GetItem(FinanceDocumentTypeID As Integer) As ROFinanceDocumentType

      For Each child As ROFinanceDocumentType In Me
        If child.FinanceDocumentTypeID = FinanceDocumentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Finance Document Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROFinanceDocumentTypeList() As ROFinanceDocumentTypeList

      Return New ROFinanceDocumentTypeList()

    End Function

    Public Shared Sub BeginGetROFinanceDocumentTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROFinanceDocumentTypeList)))

      Dim dp As New DataPortal(Of ROFinanceDocumentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROFinanceDocumentTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROFinanceDocumentTypeList)))

      Dim dp As New DataPortal(Of ROFinanceDocumentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROFinanceDocumentTypeList() As ROFinanceDocumentTypeList

      Return DataPortal.Fetch(Of ROFinanceDocumentTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFinanceDocumentType.GetROFinanceDocumentType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFinanceDocumentTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace