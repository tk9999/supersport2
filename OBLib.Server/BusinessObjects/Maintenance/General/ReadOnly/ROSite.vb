﻿' Generated 07 May 2016 20:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROSite
    Inherits OBReadOnlyBase(Of ROSite)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSiteBO.ROSiteBOToString(self)")

    Public Shared SiteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SiteID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SiteID() As Integer
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared SiteNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SiteName, "Site Name", "")
    ''' <summary>
    ''' Gets the Site Name value
    ''' </summary>
    <Display(Name:="Site Name", Description:="")>
  Public ReadOnly Property SiteName() As String
      Get
        Return GetProperty(SiteNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SiteIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SiteName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSite(dr As SafeDataReader) As ROSite

      Dim r As New ROSite()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SiteIDProperty, .GetInt32(0))
        LoadProperty(SiteNameProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace