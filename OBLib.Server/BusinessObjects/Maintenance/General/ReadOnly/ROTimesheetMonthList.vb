﻿' Generated 03 Oct 2014 08:52 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetMonthListOld
    Inherits OBReadOnlyListBase(Of ROTimesheetMonthListOld, ROTimesheetMonthOld)

#Region " Business Methods "

    Public Function GetItem(TimesheetMonthID As Integer) As ROTimesheetMonthOld

      For Each child As ROTimesheetMonthOld In Me
        If child.TimesheetMonthID = TimesheetMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(ByVal [Date] As Date) As ROTimesheetMonthOld

      Dim month As Integer = [Date].Month
      Dim year As Integer = [Date].Year

      Dim query As List(Of ROTimesheetMonthOld) = Me.Where(Function(tm) [Date].Date >= tm.StartDate And [Date].Date <= tm.EndDate).ToList

      If query.Count = 1 Then
        Return query(0)
      Else
        Return Nothing
      End If

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Months"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTimesheetMonthList() As ROTimesheetMonthListOld

      Return New ROTimesheetMonthListOld()

    End Function

    Public Shared Sub BeginGetROTimesheetMonthList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTimesheetMonthListOld)))

      Dim dp As New DataPortal(Of ROTimesheetMonthListOld)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTimesheetMonthList(CallBack As EventHandler(Of DataPortalResult(Of ROTimesheetMonthListOld)))

      Dim dp As New DataPortal(Of ROTimesheetMonthListOld)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTimesheetMonthList() As ROTimesheetMonthListOld

      Return DataPortal.Fetch(Of ROTimesheetMonthListOld)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTimesheetMonthOld.GetROTimesheetMonth(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTimesheetMonthListOld"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace