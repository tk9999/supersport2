﻿' Generated 27 Sep 2014 15:28 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROPublicHoliday
    Inherits OBReadOnlyBase(Of ROPublicHoliday)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PublicHolidayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PublicHolidayID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PublicHolidayID() As Integer
      Get
        Return GetProperty(PublicHolidayIDProperty)
      End Get
    End Property

    Public Shared PublicHolidayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PublicHoliday, "Public Holiday", "")
    ''' <summary>
    ''' Gets the Public Holiday value
    ''' </summary>
    <Display(Name:="Public Holiday", Description:="Public holiday name")>
    Public ReadOnly Property PublicHoliday() As String
      Get
        Return GetProperty(PublicHolidayProperty)
      End Get
    End Property

    Public Shared HolidayDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.HolidayDate, "Holiday Date")
    ''' <summary>
    ''' Gets the Holiday Date value
    ''' </summary>
    <Display(Name:="Holiday Date", Description:="Public holiday date")>
    Public ReadOnly Property HolidayDate As DateTime?
      Get
        Return GetProperty(HolidayDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PublicHolidayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PublicHoliday

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPublicHoliday(dr As SafeDataReader) As ROPublicHoliday

      Dim r As New ROPublicHoliday()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PublicHolidayIDProperty, .GetInt32(0))
        LoadProperty(PublicHolidayProperty, .GetString(1))
        LoadProperty(HolidayDateProperty, .GetValue(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace