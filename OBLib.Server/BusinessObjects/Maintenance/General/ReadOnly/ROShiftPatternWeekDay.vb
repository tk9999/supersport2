﻿' Generated 25 Jan 2016 06:48 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROShiftPatternWeekDay
    Inherits OBReadOnlyBase(Of ROShiftPatternWeekDay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared WeekDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekDay, "Week Day")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property WeekDay() As Integer
      Get
        Return GetProperty(WeekDayProperty)
      End Get
    End Property

    Public Shared WeekDayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WeekDayName, "Week Day Name")
    ''' <summary>
    ''' Gets the Week Day Name value
    ''' </summary>
    <Display(Name:="Week Day Name", Description:="")>
    Public ReadOnly Property WeekDayName() As String
      Get
        Return GetProperty(WeekDayNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(WeekDayProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.WeekDayName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROShiftPatternWeekDay(dr As SafeDataReader) As ROShiftPatternWeekDay

      Dim r As New ROShiftPatternWeekDay()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(WeekDayProperty, .GetInt32(0))
        LoadProperty(WeekDayNameProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
