﻿' Generated 30 May 2014 11:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROSystem
    Inherits OBReadOnlyBase(Of ROSystem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System", "")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The name of the system")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared SystemCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemCode, "System Code", "")
    ''' <summary>
    ''' Gets the System Code value
    ''' </summary>
    <Display(Name:="System Code", Description:="")>
    Public ReadOnly Property SystemCode() As String
      Get
        Return GetProperty(SystemCodeProperty)
      End Get
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DepartmentID, "Department", Nothing)
    ''' <summary>
    ''' Gets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property DepartmentID() As Integer?
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

    Public Shared RequiresTeamsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTeams, "RequiresTeams", False)
    ''' <summary>
    ''' Gets the Requires Teams value
    ''' </summary>
    Public ReadOnly Property RequiresTeams() As Boolean
      Get
        Return GetProperty(RequiresTeamsProperty)
      End Get
    End Property

    'Public Shared DefaultLeaveDurationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultLeaveDuration, "Default Leave Duration", 12)
    ' ''' <summary>
    ' ''' Gets the Department value
    ' ''' </summary>
    'Public ReadOnly Property DefaultLeaveDuration() As Integer
    '  Get
    '    Return GetProperty(DefaultLeaveDurationProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.System

    End Function

    Shared Sub New()

      'CType(IsSelectedProperty, Singular.SPropertyInfo(Of Boolean, ROSystem)).AddSetExpression("ROSystemBO.IsSelectedSet(self)")

    End Sub

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystem(dr As SafeDataReader) As ROSystem

      Dim r As New ROSystem()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemIDProperty, .GetInt32(0))
        LoadProperty(SystemProperty, .GetString(1))
        LoadProperty(SystemCodeProperty, .GetString(2))
        LoadProperty(DepartmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(RequiresTeamsProperty, .GetBoolean(4))
        'LoadProperty(DefaultLeaveDurationProperty, .GetInt32(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace