﻿' Generated 20 Sep 2014 21:15 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class PublicHoliday
    Inherits OBBusinessBase(Of PublicHoliday)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PublicHolidayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PublicHolidayID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PublicHolidayID() As Integer
      Get
        Return GetProperty(PublicHolidayIDProperty)
      End Get
    End Property

    Public Shared PublicHolidayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PublicHoliday, "Public Holiday", "")
    ''' <summary>
    ''' Gets and sets the Public Holiday value
    ''' </summary>
    <Display(Name:="Public Holiday", Description:="Public holiday name"),
    Required(ErrorMessage:="Public Holiday required"),
    StringLength(50, ErrorMessage:="Public Holiday cannot be more than 50 characters")>
    Public Property PublicHoliday() As String
      Get
        Return GetProperty(PublicHolidayProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PublicHolidayProperty, Value)
      End Set
    End Property

    Public Shared HolidayDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.HolidayDate, "Holiday Date")
    ''' <summary>
    ''' Gets and sets the Holiday Date value
    ''' </summary>
    <Display(Name:="Holiday Date", Description:="Public holiday date"),
    Required(ErrorMessage:="Holiday Date required")>
    Public Property HolidayDate As DateTime?
      Get
        Return GetProperty(HolidayDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(HolidayDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PublicHolidayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PublicHoliday.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Public Holiday")
        Else
          Return String.Format("Blank {0}", "Public Holiday")
        End If
      Else
        Return Me.PublicHoliday
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPublicHoliday() method.

    End Sub

    Public Shared Function NewPublicHoliday() As PublicHoliday

      Return DataPortal.CreateChild(Of PublicHoliday)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPublicHoliday(dr As SafeDataReader) As PublicHoliday

      Dim p As New PublicHoliday()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PublicHolidayIDProperty, .GetInt32(0))
          LoadProperty(PublicHolidayProperty, .GetString(1))
          LoadProperty(HolidayDateProperty, .GetValue(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPublicHoliday"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPublicHoliday"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPublicHolidayID As SqlParameter = .Parameters.Add("@PublicHolidayID", SqlDbType.Int)
          paramPublicHolidayID.Value = GetProperty(PublicHolidayIDProperty)
          If Me.IsNew Then
            paramPublicHolidayID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PublicHoliday", GetProperty(PublicHolidayProperty))
          .Parameters.AddWithValue("@HolidayDate", (New SmartDate(GetProperty(HolidayDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PublicHolidayIDProperty, paramPublicHolidayID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPublicHoliday"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PublicHolidayID", GetProperty(PublicHolidayIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace